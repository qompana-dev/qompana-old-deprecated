package com.devqube.crmnotificationservice.notification;

import com.devqube.crmnotificationservice.notification.exception.BadNotificationMessageException;
import com.devqube.crmnotificationservice.notification.model.Notification;
import com.devqube.crmnotificationservice.notification.model.NotificationType;
import com.devqube.crmnotificationservice.notification.model.UserNotification;
import com.devqube.crmnotificationservice.notification.repository.KafkaErrorRepository;
import com.devqube.crmnotificationservice.notification.repository.NotificationRepository;
import com.devqube.crmnotificationservice.notification.repository.UserNotificationRepository;
import com.devqube.crmnotificationservice.websocket.WebsocketService;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import com.devqube.crmshared.kafka.dto.KafkaMessageData;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.kafka.util.KafkaMessageDataBuilder;
import com.devqube.crmshared.kafka.util.KafkaUtil;
import com.devqube.crmshared.notification.NotificationMsgType;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceTest {

    @Mock
    private WebsocketService websocketService;
    @Mock
    private NotificationRepository notificationRepository;
    @Mock
    private UserNotificationRepository userNotificationRepository;
    @Mock
    private KafkaErrorRepository kafkaErrorRepository;

    @InjectMocks
    private NotificationService notificationService;

    @Test(expected = BadNotificationMessageException.class)
    public void shouldThrowBadNotificationExceptionIfNotificationMsgTypeIsNullWhenSavingNotification() throws BadNotificationMessageException, KafkaSendMessageException {
        KafkaMessage kafkaMessage = getKafkaMessage(new HashSet<>());
        notificationService.addNotification(kafkaMessage);
    }

    @Test(expected = BadNotificationMessageException.class)
    public void shouldThrowBadNotificationExceptionIfNotificationMsgTypeNotExistInEnumWhenSavingNotification() throws BadNotificationMessageException, KafkaSendMessageException {
        KafkaMessage kafkaMessage = getKafkaMessage(KafkaMessageDataBuilder.builder().add("notificationMsgType", "NOT_EXISTING_TYPE").build());
        notificationService.addNotification(kafkaMessage);
    }

    @Test(expected = BadNotificationMessageException.class)
    public void shouldThrowBadNotificationExceptionIfNotificationNotFoundByTypeWhenSavingNotification() throws BadNotificationMessageException, KafkaSendMessageException {
        KafkaMessage kafkaMessage = getKafkaMessage(KafkaMessageDataBuilder.builder().add("notificationMsgType", NotificationMsgType.USER_ADDED.name()).build());
        when(notificationRepository.findByMsgType(eq(NotificationMsgType.USER_ADDED))).thenReturn(Optional.empty());
        notificationService.addNotification(kafkaMessage);
    }

    @Test(expected = BadNotificationMessageException.class)
    public void shouldThrowBadNotificationExceptionIfAccountIdIsNullWhenSavingNotification() throws BadNotificationMessageException, KafkaSendMessageException {
        KafkaMessage kafkaMessage = getKafkaMessage(KafkaMessageDataBuilder.builder().add("notificationMsgType", NotificationMsgType.USER_ADDED.name()).build());
        kafkaMessage.setAccountId(null);
        notificationService.addNotification(kafkaMessage);
    }

    @Test
    public void shouldSaveNotification() throws BadNotificationMessageException, KafkaSendMessageException {
        KafkaMessage kafkaMessage = getKafkaMessage(KafkaMessageDataBuilder.builder()
                .add("notificationMsgType", NotificationMsgType.USER_ADDED.name()).build());
        when(notificationRepository.findByMsgType(eq(NotificationMsgType.USER_ADDED))).thenReturn(Optional.ofNullable(getNotification()));
        when(userNotificationRepository.save(any())).thenReturn(getUserNotification());

        notificationService.addNotification(kafkaMessage);

        verify(userNotificationRepository, times(2)).save(any());
        verify(websocketService, times(1)).sendNotificationToUser(eq(kafkaMessage.getAccountId().toString()));
    }


    @Test(expected = BadNotificationMessageException.class)
    public void shouldThrowBadNotificationExceptionIfKafkaErrorTypeIsNullWhenReceivingKafkaError() throws Exception {
        String childMsg = getChildKafkaMsg();
        KafkaMessage kafkaMessage = getKafkaMessage(KafkaMessageDataBuilder.builder()
                .add("notificationMsgType", NotificationMsgType.KAFKA_CALENDAR_DELETE_EVENT_ERROR.name())
                .add("kafkaErrorMsg", childMsg).build());

        when(notificationRepository.findByMsgType(eq(NotificationMsgType.KAFKA_CALENDAR_DELETE_EVENT_ERROR))).thenReturn(Optional.ofNullable(getNotification()));
        when(userNotificationRepository.save(any())).thenReturn(getUserNotification());

        notificationService.receiveKafkaError(kafkaMessage);
    }
    @Test(expected = BadNotificationMessageException.class)
    public void shouldThrowBadNotificationExceptionIfKafkaErrorMsgIsNullWhenReceivingKafkaError() throws Exception {
        KafkaMessage kafkaMessage = getKafkaMessage(KafkaMessageDataBuilder.builder()
                .add("notificationMsgType", NotificationMsgType.KAFKA_CALENDAR_DELETE_EVENT_ERROR.name())
                .add("kafkaErrorType", KafkaMsgType.CALENDAR_DELETE_EVENT.name()).build());

        when(notificationRepository.findByMsgType(eq(NotificationMsgType.KAFKA_CALENDAR_DELETE_EVENT_ERROR))).thenReturn(Optional.ofNullable(getNotification()));
        when(userNotificationRepository.save(any())).thenReturn(getUserNotification());

        notificationService.receiveKafkaError(kafkaMessage);
    }

    @Test(expected = BadNotificationMessageException.class)
    public void shouldThrowBadNotificationExceptionIfKafkaErrorTypeNotExistInEnumWhenReceivingKafkaError() throws Exception {
        String childMsg = getChildKafkaMsg();
        KafkaMessage kafkaMessage = getKafkaMessage(KafkaMessageDataBuilder.builder()
                .add("notificationMsgType", NotificationMsgType.KAFKA_CALENDAR_DELETE_EVENT_ERROR.name())
                .add("kafkaErrorMsg", childMsg)
                .add("kafkaErrorType", "NOT_EXISTING_TYPE").build());

        when(notificationRepository.findByMsgType(eq(NotificationMsgType.KAFKA_CALENDAR_DELETE_EVENT_ERROR))).thenReturn(Optional.ofNullable(getNotification()));
        when(userNotificationRepository.save(any())).thenReturn(getUserNotification());

        notificationService.receiveKafkaError(kafkaMessage);
    }

    @Test
    public void shouldReceiveKafkaError() throws Exception {
        String childMsg = getChildKafkaMsg();
        KafkaMessage kafkaMessage = getKafkaMessage(KafkaMessageDataBuilder.builder()
                .add("notificationMsgType", NotificationMsgType.KAFKA_CALENDAR_DELETE_EVENT_ERROR.name())
                .add("kafkaErrorMsg", childMsg)
                .add("kafkaErrorType", KafkaMsgType.CALENDAR_DELETE_EVENT.name()).build());

        when(notificationRepository.findByMsgType(eq(NotificationMsgType.KAFKA_CALENDAR_DELETE_EVENT_ERROR))).thenReturn(Optional.ofNullable(getNotification()));
        when(userNotificationRepository.save(any())).thenReturn(getUserNotification());

        notificationService.receiveKafkaError(kafkaMessage);

        verify(userNotificationRepository, times(3)).save(any());
        verify(websocketService, times(1)).sendNotificationToUser(eq(kafkaMessage.getAccountId().toString()));
        verify(kafkaErrorRepository, times(1)).save(any());
    }

    private KafkaMessage getKafkaMessage(Set<KafkaMessageData> data) {
        return new KafkaMessage(-1L, KafkaMsgType.NOTIFICATION_ADD_NOTIFICATION, data);
    }

    private String getChildKafkaMsg() throws JsonProcessingException {
        return KafkaUtil.getStringFromKafkaMessage(new KafkaMessage(-1L, KafkaMsgType.CALENDAR_DELETE_EVENT, KafkaMessageDataBuilder.builder()
                .add("taskId", "100").add("accountId", "-1").build()));

    }

    private Notification getNotification() {
        return Notification.builder()
                .id(-9L)
                .name("notifications.type.userAdded")
                .type(NotificationType.SUCCESS)
                .msgType(NotificationMsgType.USER_ADDED)
                .groupNotification(true).build();
    }

    private UserNotification getUserNotification() {
        return UserNotification.builder()
                .id(-1L)
                .accountId(-1L)
                .created(LocalDateTime.now())
                .notification(getNotification()).build();
    }
}
