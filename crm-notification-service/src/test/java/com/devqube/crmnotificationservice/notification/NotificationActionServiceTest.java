package com.devqube.crmnotificationservice.notification;

import com.devqube.crmnotificationservice.notification.model.KafkaError;
import com.devqube.crmnotificationservice.notification.model.UserNotification;
import com.devqube.crmnotificationservice.notification.repository.KafkaErrorRepository;
import com.devqube.crmnotificationservice.notification.repository.UserNotificationRepository;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NotificationActionServiceTest {

    @Mock
    private KafkaErrorRepository kafkaErrorRepository;

    @Mock
    private UserNotificationRepository userNotificationRepository;

    @Mock
    private KafkaService kafkaService;

    @InjectMocks
    private NotificationActionService notificationActionService;


    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowEntityNotFoundExceptionIfUserNotificationNotFoundWhenRetryingMessage() throws KafkaSendMessageException, EntityNotFoundException {
        when(userNotificationRepository.findById(anyLong())).thenReturn(Optional.empty());
        notificationActionService.retryMessage(getUserNotification().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowEntityNotFoundExceptionIfKafkaErrorNotFoundWhenRetryingMessage() throws KafkaSendMessageException, EntityNotFoundException {
        when(userNotificationRepository.findById(anyLong())).thenReturn(Optional.ofNullable(getUserNotification()));
        when(kafkaErrorRepository.findByUserNotificationId(anyLong())).thenReturn(Optional.empty());
        notificationActionService.retryMessage(getUserNotification().getId());
    }

    @Test
    public void shouldResentKafkaMessage() throws KafkaSendMessageException, EntityNotFoundException {
        when(userNotificationRepository.findById(anyLong())).thenReturn(Optional.ofNullable(getUserNotification()));
        when(kafkaErrorRepository.findByUserNotificationId(anyLong())).thenReturn(Optional.ofNullable(getKafkaError()));
        notificationActionService.retryMessage(getUserNotification().getId());
        verify(kafkaService, times(1)).send(eq(getKafkaError().getKafkaMsgType()), eq(getKafkaError().getKafkaMessage()));
        verify(kafkaErrorRepository, times(1)).deleteByUserNotificationId(eq(getUserNotification().getId()));
        verify(userNotificationRepository, times(1)).deleteById(eq(getUserNotification().getId()));
    }

    @Test
    public void shouldDeleteKafkaMessage() {
        notificationActionService.deleteMessage(getUserNotification().getId());
        verify(kafkaErrorRepository, times(1)).deleteByUserNotificationId(eq(getUserNotification().getId()));
        verify(userNotificationRepository, times(1)).deleteById(eq(getUserNotification().getId()));
    }

    private KafkaError getKafkaError() {
        return KafkaError.builder()
                .id(-1L)
                .kafkaMsgType(KafkaMsgType.CALENDAR_DELETE_EVENT)
                .userNotification(getUserNotification())
                .kafkaMessage("test").build();
    }
    private UserNotification getUserNotification() {
        return UserNotification.builder()
                .id(-1L)
                .accountId(1L)
                .created(LocalDateTime.now())
                .read(null).build();
    }

}
