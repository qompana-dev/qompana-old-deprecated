package com.devqube.crmnotificationservice.notification.web;

import com.devqube.crmnotificationservice.AbstractIntegrationTest;
import com.devqube.crmnotificationservice.kafka.KafkaServiceImpl;
import com.devqube.crmnotificationservice.notification.model.*;
import com.devqube.crmnotificationservice.notification.repository.KafkaErrorRepository;
import com.devqube.crmnotificationservice.notification.repository.NotificationRepository;
import com.devqube.crmnotificationservice.notification.repository.UserNotificationRepository;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.kafka.util.KafkaMessageDataBuilder;
import com.devqube.crmshared.kafka.util.KafkaUtil;
import com.devqube.crmshared.notification.NotificationMsgType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class NotificationControllerIntegrationTest extends AbstractIntegrationTest {
    @Autowired
    private KafkaServiceImpl kafkaService;
    @Autowired
    private UserNotificationRepository userNotificationRepository;
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private KafkaErrorRepository kafkaErrorRepository;

    @Before
    public void setUp() {
        super.setUp();
    }

    @After
    public void cleanUp() {
        this.kafkaErrorRepository.deleteAll();
        this.userNotificationRepository.deleteAll();
        this.notificationRepository.deleteAll();
    }

    @Test
    public void shouldReceiveKafkaError() throws Exception {
        String childMsg = KafkaUtil.getStringFromKafkaMessage(new KafkaMessage(-1L, KafkaMsgType.CALENDAR_DELETE_EVENT, KafkaMessageDataBuilder.builder()
                .add("taskId", "100").add("accountId", "-1").build()));

        KafkaMessage kafkaMessage = new KafkaMessage(-1L, KafkaMsgType.NOTIFICATION_KAFKA_ERROR, KafkaMessageDataBuilder.builder()
                .add("notificationMsgType", NotificationMsgType.KAFKA_CALENDAR_DELETE_EVENT_ERROR.name())
                .add("insertedValue", "test")
                .add("kafkaErrorMsg", childMsg)
                .add("kafkaErrorType", KafkaMsgType.CALENDAR_DELETE_EVENT.name()).build());

        Notification newNotification = notificationRepository.save(getNotification());
        List<NotificationAction> actions = new ArrayList<>();
        actions.add(getRetryAction(newNotification));
        actions.add(getDeleteAction(newNotification));
        newNotification.setActions(actions);
        newNotification = notificationRepository.save(newNotification);

        kafkaService.receiveKafkaError(kafkaMessage);

        List<KafkaError> allKafkaErrors = kafkaErrorRepository.findAll();
        List<UserNotification> allUserNotification = userNotificationRepository.findAll();

        assertEquals(1, allKafkaErrors.size());
        assertEquals(1, allUserNotification.size());

        KafkaError kafkaError = allKafkaErrors.get(0);
        UserNotification userNotification = allUserNotification.get(0);

        // user notification
        assertEquals(kafkaMessage.getAccountId(), userNotification.getAccountId());
        assertTrue(LocalDateTime.now().minusMinutes(1).isBefore(userNotification.getCreated()));
        assertTrue(LocalDateTime.now().plusMinutes(1).isAfter(userNotification.getCreated()));
        assertNotNull(userNotification.getNotification());
        assertEquals(newNotification.getId(), userNotification.getNotification().getId());

        //message paramse

        assertNotNull(userNotification.getMessageParams());

        Set<NotificationMessageParam> messageParams = userNotification.getMessageParams();
        assertEquals(3, messageParams.size());

        Optional<NotificationMessageParam> taskId = messageParams.stream().filter(c -> c.getKey().equals("taskId")).findFirst();
        Optional<NotificationMessageParam> accountId = messageParams.stream().filter(c -> c.getKey().equals("accountId")).findFirst();
        Optional<NotificationMessageParam> insertedValue = messageParams.stream().filter(c -> c.getKey().equals("insertedValue")).findFirst();

        assertTrue(taskId.isPresent());
        assertTrue(accountId.isPresent());
        assertTrue(insertedValue.isPresent());

        assertEquals("100", taskId.get().getValue());
        assertEquals("-1", accountId.get().getValue());
        assertEquals("test", insertedValue.get().getValue());

        // kafka error
        assertEquals(KafkaMsgType.CALENDAR_DELETE_EVENT, kafkaError.getKafkaMsgType());
        assertEquals(childMsg, kafkaError.getKafkaMessage());
        assertNotNull(kafkaError.getUserNotification());
        assertEquals(userNotification.getId(), kafkaError.getUserNotification().getId());
    }

    @Test
    public void shouldAddNotification() {
        KafkaMessage kafkaMessage = new KafkaMessage(-1L, KafkaMsgType.NOTIFICATION_KAFKA_ERROR, KafkaMessageDataBuilder.builder()
                .add("notificationMsgType", NotificationMsgType.USER_ADDED.name())
                .add("insertedValue", "test").build());

        Notification newNotification = getNotification();
        newNotification.setMsgType(NotificationMsgType.USER_ADDED);
        newNotification = notificationRepository.save(newNotification);

        kafkaService.addNotification(kafkaMessage);

        List<UserNotification> allUserNotification = userNotificationRepository.findAll();

        assertEquals(1, allUserNotification.size());

        UserNotification userNotification = allUserNotification.get(0);

        // user notification
        assertEquals(kafkaMessage.getAccountId(), userNotification.getAccountId());
        assertTrue(LocalDateTime.now().minusMinutes(1).isBefore(userNotification.getCreated()));
        assertTrue(LocalDateTime.now().plusMinutes(1).isAfter(userNotification.getCreated()));
        assertNotNull(userNotification.getNotification());
        assertEquals(newNotification.getId(), userNotification.getNotification().getId());

        //message paramse

        assertNotNull(userNotification.getMessageParams());

        Set<NotificationMessageParam> messageParams = userNotification.getMessageParams();
        assertEquals(1, messageParams.size());

        Optional<NotificationMessageParam> insertedValue = messageParams.stream().filter(c -> c.getKey().equals("insertedValue")).findFirst();

        assertTrue(insertedValue.isPresent());


        assertEquals("test", insertedValue.get().getValue());
    }

    private Notification getNotification() {
        return Notification.builder()
                .name("notifications.type.kafkaDeleteEventError")
                .type(NotificationType.ERROR)
                .msgType(NotificationMsgType.KAFKA_CALENDAR_DELETE_EVENT_ERROR)
                .groupNotification(false).build();
    }

    private NotificationAction getRetryAction(Notification notification) {
        return NotificationAction.builder()
                .notification(notification)
                .restUrl("crm-notification-service/notifications/action/retry")
                .name("notifications.action.retry").build();
    }

    private NotificationAction getDeleteAction(Notification notification) {
        return NotificationAction.builder()
                .notification(notification)
                .restUrl("crm-notification-service/notifications/action/delete")
                .name("notifications.action.delete").build();
    }
}
