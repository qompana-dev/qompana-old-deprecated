package com.devqube.crmnotificationservice.firebase.fcm;

import com.devqube.crmnotificationservice.firebase.model.PushNotificationRequest;
import com.devqube.crmnotificationservice.notification.model.NotificationMessageParam;
import com.google.firebase.messaging.*;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;

@Slf4j
@Service
public class FCMService {

    public void sendMessage(PushNotificationRequest request) {
        Map<String, String> data = new HashMap<>();
        data.put("name", request.getUserNotification().getNotification().getName());

        if (request.getUserNotification().getMessageParams().size() > 0) {
            data.put("size", String.valueOf(request.getUserNotification().getMessageParams().size()));
            var ref = new Object() {
                int i = 0;
            };
            request.getUserNotification().getMessageParams().forEach(param -> {
                data.put("value." + ref.i, param.getValue());
                data.put("key." + ref.i, param.getKey());
                ref.i++;
            });
        }
        Message message = getPreconfiguredMessageWithData(data, request);
        String response = sendAndGetResponse(message);
        if(!response.isBlank()) {
            log.info("Sent message with data. Token: " + request.getToken() + ", " + response);
        }
    }

    private String sendAndGetResponse(Message message) {
        try {
            return FirebaseMessaging.getInstance().sendAsync(message).get();
        } catch (InterruptedException | ExecutionException e) {
            log.warn(e.getMessage());
            return "";
        }
    }

    private AndroidConfig getAndroidConfig(String topic) {
        return AndroidConfig.builder()
                .setTtl(Duration.ofMinutes(2).toMillis()).setCollapseKey(topic)
                .setPriority(AndroidConfig.Priority.HIGH)
                .setNotification(AndroidNotification.builder().setSound(NotificationParameter.SOUND.getValue())
                        .setColor(NotificationParameter.COLOR.getValue()).setTag(topic).build()).build();
    }

    private ApnsConfig getApnsConfig(String topic) {
        return ApnsConfig.builder()
                .setAps(Aps.builder().setCategory(topic).setThreadId(topic).build()).build();
    }

//    private Message getPreconfiguredMessageToTopicWithoutData(PushNotificationRequest request) {
//        return getPreconfiguredMessageBuilder(request)
//                .setTopic(!request.getTopic().isEmpty() ? request.getTopic() : "")
//                .build();
//    }
//
//    private Message getPreconfiguredMessageToTopicWithData(Map<String, String> data, PushNotificationRequest request) {
//        return getPreconfiguredMessageBuilder(request)
//                .putAllData(data)
//                .setTopic(!request.getTopic().isEmpty() ? request.getTopic() : "")
//                .build();
//    }

    private Message getPreconfiguredMessageWithoutData(PushNotificationRequest request) {
        return getPreconfiguredMessageBuilder(request)
                .setToken(request.getToken())
                .build();
    }

    private Message getPreconfiguredMessageWithData(Map<String, String> data, PushNotificationRequest request) {
        return getPreconfiguredMessageBuilder(request)
                .putAllData(data)
                .setToken(request.getToken())
                .build();
    }

    private Message.Builder getPreconfiguredMessageBuilder(PushNotificationRequest request) {
        AndroidConfig androidConfig = getAndroidConfig(request.getTopic());
        ApnsConfig apnsConfig = getApnsConfig(request.getTopic());
        return Message.builder()
                .setApnsConfig(apnsConfig).setAndroidConfig(androidConfig).setNotification(
                        new Notification(request.getTitle(), request.getMessage()));
    }


}
