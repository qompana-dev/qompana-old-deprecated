package com.devqube.crmnotificationservice.firebase;

import com.devqube.crmnotificationservice.firebase.fcm.FCMService;
import com.devqube.crmnotificationservice.firebase.model.PushNotificationRequest;
import com.devqube.crmnotificationservice.notification.model.FirebaseToken;
import com.devqube.crmnotificationservice.notification.model.UserNotification;
import com.devqube.crmnotificationservice.notification.repository.FirebaseTokenRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class PushNotificationService {

    private final FCMService fcmService;
    private final FirebaseTokenRepository firebaseTokenRepository;

    public PushNotificationService(FCMService fcmService, FirebaseTokenRepository firebaseTokenRepository) {
        this.fcmService = fcmService;
        this.firebaseTokenRepository = firebaseTokenRepository;
    }

    public void sendPushNotification(UserNotification userNotification) {
        if (userNotification.getAccountId() != null) {
            List<FirebaseToken> byAccountId = firebaseTokenRepository.findByAccountId(userNotification.getAccountId());
            if (!byAccountId.isEmpty()) {
                for (FirebaseToken token : byAccountId) {
                    PushNotificationRequest request = new PushNotificationRequest(userNotification);
                    request.setToken(token.getToken());
                    fcmService.sendMessage(request);
                }
            }
        }
    }

}
