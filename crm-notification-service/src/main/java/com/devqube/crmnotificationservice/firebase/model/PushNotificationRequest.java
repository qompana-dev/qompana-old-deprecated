package com.devqube.crmnotificationservice.firebase.model;

import com.devqube.crmnotificationservice.notification.model.UserNotification;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PushNotificationRequest {

    private String title;
    private String message;
    private String topic;
    private String token;
    private Long accountId;
    private UserNotification userNotification;

    public PushNotificationRequest() {
    }

    public PushNotificationRequest(String title, String messageBody, String topicName) {
        this.title = title;
        this.message = messageBody;
        this.topic = topicName;
    }

    public PushNotificationRequest(UserNotification userNotification) {
        this.title = userNotification.getNotification().getMappedTitle();
        this.message = "";
        this.accountId = userNotification.getAccountId();
        this.userNotification = userNotification;
    }
}
