package com.devqube.crmnotificationservice.firebase.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TokenUserRequest {
    private Long accountId;
    private String token;

}
