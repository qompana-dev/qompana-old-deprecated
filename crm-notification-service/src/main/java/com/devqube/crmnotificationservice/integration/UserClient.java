package com.devqube.crmnotificationservice.integration;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "crm-user-service", url = "${crm-user-service.url}")
public interface UserClient {
    @RequestMapping(value = "/accounts/me/id", consumes = { "application/json" }, method = RequestMethod.GET)
    public Long getMyAccountId(@RequestHeader("Logged-Account-Email") String email);


    @RequestMapping(value = "/internal/accounts/{accountId}/email", consumes = { "application/json" }, method = RequestMethod.GET)
    public String getAccountEmail(@PathVariable("accountId") String accountId);

}
