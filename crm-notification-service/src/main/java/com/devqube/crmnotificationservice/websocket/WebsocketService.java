package com.devqube.crmnotificationservice.websocket;

import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class WebsocketService {
    private final KafkaService kafkaService;

    public WebsocketService(@Lazy KafkaService kafkaService) {
        this.kafkaService = kafkaService;
    }

    public void sendNotificationToUser(KafkaMessage kafkaMessage) throws KafkaSendMessageException {
        Optional<String> accountId = kafkaMessage.getValueByKey("accountId");
        if (accountId.isEmpty()) {
            log.error("accountId is empty");
            return;
        }
        sendNotificationToUser(accountId.get());
    }

    public void sendNotificationToUser(String accountId) throws KafkaSendMessageException {
        kafkaService.sendWebsocket("/topic/notification/change/" + accountId, Boolean.TRUE.toString());
    }
}
