package com.devqube.crmnotificationservice.notification.model;

public enum NotificationType {
    SUCCESS, INFO, ERROR, WARNING
}
