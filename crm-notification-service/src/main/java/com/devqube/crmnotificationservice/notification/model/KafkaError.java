package com.devqube.crmnotificationservice.notification.model;

import com.devqube.crmshared.kafka.type.KafkaMsgType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class KafkaError {
    @Id
    @SequenceGenerator(name="kafka_error_seq", sequenceName="kafka_error_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="kafka_error_seq")
    private Long id;

    @NotNull
    @Column(name = "kafka_msg_type")
    @Enumerated(EnumType.STRING)
    private KafkaMsgType kafkaMsgType;

    @NotNull
    @Column(name = "kafka_message")
    @Type(type = "text")
    private String kafkaMessage;

    @NotNull
    @OneToOne
    @JoinColumn(name = "user_notification_id", unique = true)
    private UserNotification userNotification;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KafkaError that = (KafkaError) o;
        return Objects.equals(id, that.id) &&
                kafkaMsgType == that.kafkaMsgType &&
                Objects.equals(kafkaMessage, that.kafkaMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, kafkaMsgType, kafkaMessage);
    }

    @Override
    public String toString() {
        return "KafkaError{" +
                "id=" + id +
                ", kafkaMsgType=" + kafkaMsgType +
                ", kafkaMessage='" + kafkaMessage + '\'' +
                '}';
    }
}
