package com.devqube.crmnotificationservice.notification.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UnreadNotificationCountDto {
    private Long allCount;
    private Long onlyMineCount;
    private Long forAcceptanceCount;
}
