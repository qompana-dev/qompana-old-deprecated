package com.devqube.crmnotificationservice.notification.model;

import com.devqube.crmshared.notification.NotificationMsgType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserNotification {

    @Id
    @SequenceGenerator(name="user_notification_seq", sequenceName="user_notification_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="user_notification_seq")
    private Long id;

    @NotNull
    @Column(name = "account_id")
    private Long accountId;

    @NotNull
    @Column(name = "created")
    private LocalDateTime created;

    @Column(name = "read")
    private LocalDateTime read;

    @NotNull
    @OneToOne
    @JoinColumn(name = "notification_id")
    private Notification notification;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userNotification")
    private Set<NotificationMessageParam> messageParams;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserNotification that = (UserNotification) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(accountId, that.accountId) &&
                Objects.equals(created, that.created) &&
                Objects.equals(read, that.read);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountId, created, read);
    }

    @Override
    public String toString() {
        return "UserNotification{" +
                "id=" + id +
                ", accountId=" + accountId +
                ", created=" + created +
                ", read=" + read +
                '}';
    }
}
