package com.devqube.crmnotificationservice.notification.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FirebaseToken {
    @JsonProperty("id")
    @Id
    @SequenceGenerator(name="firebase_token_seq", sequenceName="firebase_token_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="firebase_token_seq")
    private Long id;

    @NotNull
    @Column(name = "account_id")
    private Long accountId;

    @NotNull
    @Column(name = "token")
    private String token;

    public FirebaseToken(Long accountId, String token) {
        this.accountId = accountId;
        this.token = token;
    }

}
