package com.devqube.crmnotificationservice.notification.exception;

public class BadNotificationMessageException extends Exception {
    public BadNotificationMessageException() {
    }

    public BadNotificationMessageException(String message) {
        super(message);
    }
}
