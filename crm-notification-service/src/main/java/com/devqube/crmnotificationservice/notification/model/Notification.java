package com.devqube.crmnotificationservice.notification.model;

import com.devqube.crmshared.notification.NotificationMsgType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Notification {
    @Id
    @SequenceGenerator(name="notification_seq", sequenceName="notification_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="notification_seq")
    private Long id;

    @Length(max = 300)
    @NotNull
    @Column(name = "name")
    private String name;

    @Column(name = "msg_type", unique = true)
    @Enumerated(EnumType.STRING)
    private NotificationMsgType msgType;

    @Column(name = "type")
    @Enumerated(EnumType.ORDINAL)
    private NotificationType type;

    @Column(name = "group_notification")
    private boolean groupNotification;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "notification")
    private List<NotificationAction> actions;

    @Transient
    private String mappedTitle;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notification that = (Notification) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                msgType == that.msgType &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, msgType, type);
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", msgType=" + msgType +
                ", type=" + type +
                ", groupNotification=" + groupNotification +
                '}';
    }
}
