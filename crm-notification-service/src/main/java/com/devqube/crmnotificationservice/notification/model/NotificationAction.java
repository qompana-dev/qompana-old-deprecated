package com.devqube.crmnotificationservice.notification.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NotificationAction {
    @Id
    @SequenceGenerator(name="notification_action_seq", sequenceName="notification_action_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="notification_action_seq")
    private Long id;

    @Length(max = 300)
    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "rest_url")
    @Type(type = "text")
    private String restUrl;

    @NotNull
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "notification_id")
    private Notification notification;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NotificationAction that = (NotificationAction) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(restUrl, that.restUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, restUrl);
    }

    @Override
    public String toString() {
        return "NotificationAction{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", restUrl='" + restUrl + '\'' +
                '}';
    }
}
