package com.devqube.crmnotificationservice.notification.web;

import com.devqube.crmnotificationservice.firebase.model.TokenUserRequest;
import com.devqube.crmnotificationservice.notification.model.FirebaseToken;
import com.devqube.crmnotificationservice.notification.model.UserNotification;
import com.devqube.crmnotificationservice.notification.web.dto.UnreadNotificationCountDto;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.notification.NotificationActionDto;
import com.devqube.crmshared.notification.NotificationMsgType;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "notifications")
public interface NotificationsApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Logged user notifications", nickname = "getNotifications", notes = "Method used to return logged user's notifications", response = UserNotification.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "notification list", response = UserNotification.class, responseContainer = "List") })
    @RequestMapping(value = "/notifications",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<List<UserNotification>> getNotifications() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"accountId\" : 6,  \"notification\" : {    \"msgType\" : \"msgType\",    \"name\" : \"name\",    \"id\" : 1,    \"type\" : \"SUCCESS\",    \"actions\" : [ {      \"restUrl\" : \"restUrl\",      \"name\" : \"name\",      \"id\" : 5    }, {      \"restUrl\" : \"restUrl\",      \"name\" : \"name\",      \"id\" : 5    } ]  },  \"read\" : \"2000-01-23T04:56:07.000+00:00\",  \"created\" : \"2000-01-23T04:56:07.000+00:00\",  \"id\" : 0,  \"messageParams\" : [ {    \"id\" : 5,    \"value\" : \"value\",    \"key\" : \"key\"  }, {    \"id\" : 5,    \"value\" : \"value\",    \"key\" : \"key\"  } ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    @ApiOperation(value = "get notifications for mobile", nickname = "getNotificationsForMobile", notes = "get page of all notifications for mobile", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "notification page retrieved successfully", response = org.springframework.data.domain.Page.class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/mobile/notifications",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Page> getNotificationsForMobile(@ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable,
                                                           @ApiParam(value = "only mine notifications") @Valid @RequestParam(value = "onlyMine", required = false) Boolean onlyMine,
                                                           @ApiParam(value = "only notifications for acceptance") @Valid @RequestParam(value = "forAcceptance", required = false) Boolean forAcceptance) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get unread notifications count", nickname = "getUnreadNotificationCount", notes = "get page unread notification count", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "notification count retrieved successfully", response = UnreadNotificationCountDto.class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/mobile/notifications/count",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<UnreadNotificationCountDto> getUnreadNotificationCount() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "delete kafka message", nickname = "deleteMessage", notes = "Method used to delete kafka message")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Message has been deleted")})
    @RequestMapping(value = "/notifications/action/delete",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Void> deleteMessage(@ApiParam(value = "Notification id", required = true) @Valid @RequestBody NotificationActionDto notificationActionDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    @ApiOperation(value = "retry kafka message", nickname = "retryMessage", notes = "Method used to run again kafka message")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Message has been restarted"),
            @ApiResponse(code = 404, message = "Message not found") })
    @RequestMapping(value = "/notifications/action/retry",
            consumes = { "application/json" },
            method = RequestMethod.POST)
    default ResponseEntity<Void> retryMessage(@ApiParam(value = "Notification id" ,required=true )  @Valid @RequestBody NotificationActionDto notificationActionDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    @ApiOperation(value = "mark notification as read", nickname = "markAsRead", notes = "Method used to mark notification as read", response = UserNotification.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "marked as read", response = UserNotification.class),
            @ApiResponse(code = 404, message = "user notification not found") })
    @RequestMapping(value = "/notifications/{userNotificationId}/read",
            produces = { "application/json" },
            method = RequestMethod.POST)
    default ResponseEntity<UserNotification> markAsRead(@ApiParam(value = "The user notification ID",required=true) @PathVariable("userNotificationId") Long userNotificationId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"accountId\" : 6,  \"notification\" : {    \"msgType\" : \"msgType\",    \"name\" : \"name\",    \"id\" : 1,    \"type\" : \"SUCCESS\",    \"actions\" : [ {      \"restUrl\" : \"restUrl\",      \"name\" : \"name\",      \"id\" : 5    }, {      \"restUrl\" : \"restUrl\",      \"name\" : \"name\",      \"id\" : 5    } ]  },  \"read\" : \"2000-01-23T04:56:07.000+00:00\",  \"created\" : \"2000-01-23T04:56:07.000+00:00\",  \"id\" : 0,  \"messageParams\" : [ {    \"id\" : 5,    \"value\" : \"value\",    \"key\" : \"key\"  }, {    \"id\" : 5,    \"value\" : \"value\",    \"key\" : \"key\"  } ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    @ApiOperation(value = "mark user all notification as read", nickname = "markAllUserAsRead", notes = "Method used to mark all user notification as read", response = UserNotification.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = " user all notification marked as read")})
    @RequestMapping(value = "/notifications/readAll",
            produces = { "application/json" },
            method = RequestMethod.POST)
    default ResponseEntity<Void> markUserAllAsRead() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    @ApiOperation(value = "Logged user notification by id", nickname = "getNotificationById", notes = "Method used to return logged user notification by id", response = UserNotification.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "notification by id", response = UserNotification.class),
            @ApiResponse(code = 404, message = "notification not found") })
    @RequestMapping(value = "/notifications/{id}",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<UserNotification> getNotificationById(@ApiParam(value = "user notification id",required=true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"accountId\" : 6,  \"notification\" : {    \"msgType\" : \"msgType\",    \"name\" : \"name\",    \"id\" : 1,    \"type\" : \"SUCCESS\",    \"actions\" : [ {      \"restUrl\" : \"restUrl\",      \"name\" : \"name\",      \"id\" : 5    }, {      \"restUrl\" : \"restUrl\",      \"name\" : \"name\",      \"id\" : 5    } ]  },  \"read\" : \"2000-01-23T04:56:07.000+00:00\",  \"created\" : \"2000-01-23T04:56:07.000+00:00\",  \"id\" : 0,  \"messageParams\" : [ {    \"id\" : 5,    \"value\" : \"value\",    \"key\" : \"key\"  }, {    \"id\" : 5,    \"value\" : \"value\",    \"key\" : \"key\"  } ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "id of user notification", response = UserNotification.class),
            @ApiResponse(code = 404, message = "notification not found") })
    @RequestMapping(value = "/notifications/accountId/{accountId}/msgType/{msgType}/msgParam/{msgParam}",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<Long> getUserNotificationIdByAccountIdAndNotificationMsgTypeAndMessageParam(
            @PathVariable("accountId") Long accountId, @PathVariable("msgType") NotificationMsgType msgType,
            @PathVariable("msgParam") String msgParam) throws EntityNotFoundException {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"accountId\" : 6,  \"notification\" : {    \"msgType\" : \"msgType\",    \"name\" : \"name\",    \"id\" : 1,    \"type\" : \"SUCCESS\",    \"actions\" : [ {      \"restUrl\" : \"restUrl\",      \"name\" : \"name\",      \"id\" : 5    }, {      \"restUrl\" : \"restUrl\",      \"name\" : \"name\",      \"id\" : 5    } ]  },  \"read\" : \"2000-01-23T04:56:07.000+00:00\",  \"created\" : \"2000-01-23T04:56:07.000+00:00\",  \"id\" : 0,  \"messageParams\" : [ {    \"id\" : 5,    \"value\" : \"value\",    \"key\" : \"key\"  }, {    \"id\" : 5,    \"value\" : \"value\",    \"key\" : \"key\"  } ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    @ApiOperation(value = "Save firebase token", nickname = "saveFirebaseToken", notes = "Method used to save firebase token", response = FirebaseToken.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "saved firebase token successfully", response = FirebaseToken.class),
            @ApiResponse(code = 400, message = "validation error", response = String.class)})
    @RequestMapping(value = "/firebase/save",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<FirebaseToken> saveFirebaseToken(
            @RequestBody TokenUserRequest request
            ) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


}
