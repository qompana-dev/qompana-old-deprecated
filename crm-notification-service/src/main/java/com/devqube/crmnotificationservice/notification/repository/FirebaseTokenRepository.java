package com.devqube.crmnotificationservice.notification.repository;

import com.devqube.crmnotificationservice.notification.model.FirebaseToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FirebaseTokenRepository extends JpaRepository<FirebaseToken, Long> {
    List<FirebaseToken> findByAccountId(Long accountId);
    Long countByAccountIdAndToken(Long accountId, String token);

}
