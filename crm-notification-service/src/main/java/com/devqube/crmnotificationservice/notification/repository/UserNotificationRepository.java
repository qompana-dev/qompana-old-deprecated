package com.devqube.crmnotificationservice.notification.repository;

import com.devqube.crmnotificationservice.notification.model.UserNotification;
import com.devqube.crmshared.notification.NotificationMsgType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@Repository
public interface UserNotificationRepository extends JpaRepository<UserNotification, Long> {

    List<UserNotification> findAllByAccountIdAndReadIsNull(Long accountId);

    List<UserNotification> findAllByReadIsNullAndCreatedBefore(LocalDateTime created);

    List<UserNotification> findAllByAccountIdAndReadIsNullOrderByCreatedDesc(Long accountId);

    List<UserNotification> findTop20ByAccountIdAndReadNotNullOrderByCreatedDesc(Long accountId);

    @Query("select un from UserNotification un where un.accountId = :accountId order by un.read desc, un.created desc")
    Page<UserNotification> findAllForMobile(@Param("accountId") Long accountId, Pageable pageable);

    @Query("select un from UserNotification un " +
            "where un.accountId = :accountId  and un.notification.groupNotification = false " +
            "order by un.read desc, un.created desc")
    Page<UserNotification> findAllOnlyForUserForMobile(@Param("accountId") Long accountId, Pageable pageable);

    @Query("select un from UserNotification un " +
            "where un.accountId = :accountId  and " +
            "(un.notification.msgType in :msgTypes) " +
            "order by un.read desc, un.created desc")
    Page<UserNotification> findAllForAcceptanceForMobile(@Param("accountId") Long accountId,
                                                         @Param("msgTypes") Set<NotificationMsgType> msTypes,
                                                         Pageable pageable);


    @Query("select count(un) from UserNotification un " +
            "where un.accountId = :accountId and un.read is null")
    Long countAllUnread(@Param("accountId") Long accountId);

    @Query("select count(un) from UserNotification un " +
            "where un.accountId = :accountId and un.notification.groupNotification = false and un.read is null")
    Long countUnreadOnlyForUser(@Param("accountId") Long accountId);

    @Query("select count(un) from UserNotification un " +
            "where un.accountId = :accountId and " +
            "(un.notification.msgType in :msgTypes) and un.read is null")
    Long countUnreadForAcceptance(@Param("accountId") Long accountId,
                                  @Param("msgTypes") Set<NotificationMsgType> msTypes);

    Optional<UserNotification> findById(Long aLong);

    void deleteById(Long id);

    @Query("select un.id from UserNotification un inner join un.messageParams nmp where un.accountId = :accountId" +
            " and un.notification.msgType = :msgType and nmp.key = 'processInstanceId' and nmp.value = :processInstanceId")
    Optional<Long> findUserNotificationIdByAccountIdAndNotificationMsgTypeAndMessageParam(@Param("accountId") Long accountId,
                                                                                          @Param("msgType") NotificationMsgType msgType,
                                                                                          @Param("processInstanceId") String processInstanceId);

    void deleteAllByReadNotNullAndReadBefore(LocalDateTime time);
}
