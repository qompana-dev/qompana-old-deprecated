package com.devqube.crmnotificationservice.notification.repository;

import com.devqube.crmnotificationservice.notification.model.KafkaError;
import com.devqube.crmnotificationservice.notification.model.UserNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface KafkaErrorRepository extends JpaRepository<KafkaError, Long> {
    Optional<KafkaError> findByUserNotificationId(Long userNotificationId);
    void deleteByUserNotificationId(Long userNotificationId);
}
