package com.devqube.crmnotificationservice.notification;

import com.devqube.crmnotificationservice.firebase.PushNotificationService;
import com.devqube.crmnotificationservice.integration.UserClient;
import com.devqube.crmnotificationservice.notification.exception.BadNotificationMessageException;
import com.devqube.crmnotificationservice.notification.exception.LoggedUserNotFoundException;
import com.devqube.crmnotificationservice.notification.model.KafkaError;
import com.devqube.crmnotificationservice.notification.model.Notification;
import com.devqube.crmnotificationservice.notification.model.NotificationMessageParam;
import com.devqube.crmnotificationservice.notification.model.UserNotification;
import com.devqube.crmnotificationservice.notification.repository.KafkaErrorRepository;
import com.devqube.crmnotificationservice.notification.repository.NotificationRepository;
import com.devqube.crmnotificationservice.notification.repository.UserNotificationRepository;
import com.devqube.crmnotificationservice.notification.web.dto.UnreadNotificationCountDto;
import com.devqube.crmnotificationservice.websocket.WebsocketService;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.kafka.util.KafkaUtil;
import com.devqube.crmshared.notification.NotificationMsgType;
import com.devqube.crmshared.web.CurrentRequestUtil;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@EnableScheduling
public class NotificationService {
    private final static List<String> notificationIgnoredArgs = Arrays.asList("kafkaErrorType", "kafkaErrorMsg", "notificationMsgType");
    private final Set<NotificationMsgType> msgTypesForAcceptance = new HashSet<>(Arrays.asList(
            NotificationMsgType.LEAD_TASK_ACCEPTANCE,
            NotificationMsgType.OPPORTUNITY_TASK_ACCEPTANCE,
            NotificationMsgType.EXPENSE_ACCEPTANCE));

    private final NotificationRepository notificationRepository;
    private final KafkaErrorRepository kafkaErrorRepository;
    private final UserNotificationRepository userNotificationRepository;
    private final WebsocketService websocketService;
    private final UserClient userClient;
    private final PushNotificationService pushNotificationService;

    public NotificationService(NotificationRepository notificationRepository, KafkaErrorRepository kafkaErrorRepository,
                               UserNotificationRepository userNotificationRepository, WebsocketService websocketService,
                               UserClient userClient, PushNotificationService pushNotificationService) {
        this.notificationRepository = notificationRepository;
        this.kafkaErrorRepository = kafkaErrorRepository;
        this.userNotificationRepository = userNotificationRepository;
        this.websocketService = websocketService;
        this.userClient = userClient;
        this.pushNotificationService = pushNotificationService;
    }


    //    @Scheduled(cron = "0 30 2 * * ?")
    @Scheduled(cron = "55 12 * * * *")
    @Transactional
    public void deleteAllReadBeforeParamDate() {
        System.out.println("Wykonuje usuwanie starych");
        markAllWhichCreatedBefore(LocalDateTime.now().minusDays(7));
        userNotificationRepository.deleteAllByReadNotNullAndReadBefore(LocalDateTime.now().minusDays(30));
    }

    @Transactional(rollbackFor = LoggedUserNotFoundException.class)
    public List<UserNotification> getUserNotifications() throws LoggedUserNotFoundException {
        try {
            Long loggedUserAccountId = userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail());
            List<UserNotification> result = userNotificationRepository.findAllByAccountIdAndReadIsNullOrderByCreatedDesc(loggedUserAccountId);
            result.addAll(userNotificationRepository.findTop20ByAccountIdAndReadNotNullOrderByCreatedDesc(loggedUserAccountId));
            return result;
        } catch (FeignException e) {
            throw new LoggedUserNotFoundException();
        }
    }

    @Transactional(rollbackFor = LoggedUserNotFoundException.class)
    public Page<UserNotification> getUserNotificationsForMobile(Pageable pageable) throws LoggedUserNotFoundException {
        try {
            Long loggedUserAccountId = userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail());
            return userNotificationRepository.findAllForMobile(loggedUserAccountId, pageable);
        } catch (FeignException e) {
            throw new LoggedUserNotFoundException();
        }
    }

    @Transactional(rollbackFor = LoggedUserNotFoundException.class)
    public Page<UserNotification> getOnlyUserNotificationsForMobile(Pageable pageable) throws LoggedUserNotFoundException {
        try {
            Long loggedUserAccountId = userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail());
            return userNotificationRepository.findAllOnlyForUserForMobile(loggedUserAccountId, pageable);
        } catch (FeignException e) {
            throw new LoggedUserNotFoundException();
        }
    }

    @Transactional(rollbackFor = LoggedUserNotFoundException.class)
    public Page<UserNotification> getUserNotificationsForAcceptanceForMobile(Pageable pageable) throws LoggedUserNotFoundException {
        try {
            Long loggedUserAccountId = userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail());

            return userNotificationRepository.findAllForAcceptanceForMobile(loggedUserAccountId, msgTypesForAcceptance, pageable);
        } catch (FeignException e) {
            throw new LoggedUserNotFoundException();
        }
    }

    public UnreadNotificationCountDto getUnreadNotificationCount() throws LoggedUserNotFoundException {
        try {
            Long loggedUserAccountId = userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail());
            UnreadNotificationCountDto result = new UnreadNotificationCountDto();
            result.setAllCount(userNotificationRepository.countAllUnread(loggedUserAccountId));
            result.setOnlyMineCount(userNotificationRepository.countUnreadOnlyForUser(loggedUserAccountId));
            result.setForAcceptanceCount(userNotificationRepository.countUnreadForAcceptance(loggedUserAccountId, msgTypesForAcceptance));
            return result;
        } catch (FeignException e) {
            throw new LoggedUserNotFoundException();
        }
    }

    public Long getUserNotificationIdByAccountIdAndNotificationMsgTypeAndMessageParam(
            Long accountId, NotificationMsgType msgType, String processInstanceId) throws EntityNotFoundException {
        return this.userNotificationRepository.findUserNotificationIdByAccountIdAndNotificationMsgTypeAndMessageParam(
                accountId, msgType, processInstanceId).orElseThrow(EntityNotFoundException::new);
    }

    public UserNotification getUserNotificationById(Long id) throws EntityNotFoundException {
        return this.userNotificationRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    public UserNotification markAsRead(Long userNotificationId) throws EntityNotFoundException, PermissionDeniedException, KafkaSendMessageException {
        UserNotification userNotification = userNotificationRepository.findById(userNotificationId).orElseThrow(EntityNotFoundException::new);
        Long accountId = userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail());
        if (!userNotification.getAccountId().equals(accountId)) {
            throw new PermissionDeniedException();
        }
        userNotification.setRead(LocalDateTime.now());
        UserNotification result = userNotificationRepository.save(userNotification);
        websocketService.sendNotificationToUser(accountId.toString());
        return result;
    }

    public void markUserAllAsRead() throws LoggedUserNotFoundException {
        try {
            Long loggedUserAccountId = userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail());
            List<UserNotification> result = userNotificationRepository.findAllByAccountIdAndReadIsNull(loggedUserAccountId);
            LocalDateTime now = LocalDateTime.now();
            result.forEach(notification -> notification.setRead(now));
            userNotificationRepository.saveAll(result);
        } catch (FeignException e) {
            throw new LoggedUserNotFoundException();
        }
    }

    private void markAllWhichCreatedBefore(LocalDateTime timeBefore) {
        List<UserNotification> result = userNotificationRepository.findAllByReadIsNullAndCreatedBefore(timeBefore);
        LocalDateTime now = LocalDateTime.now();
        result.forEach(notification -> notification.setRead(now));
        userNotificationRepository.saveAll(result);
    }

    @Transactional(rollbackFor = BadNotificationMessageException.class)
    public void receiveKafkaError(KafkaMessage kafkaMessage) throws BadNotificationMessageException, KafkaSendMessageException {
        UserNotification userNotification = saveNotification(kafkaMessage);

        String kafkaErrorType = kafkaMessage.getValueByKey("kafkaErrorType").orElseThrow(BadNotificationMessageException::new);
        String kafkaErrorMsg = kafkaMessage.getValueByKey("kafkaErrorMsg").orElseThrow(BadNotificationMessageException::new);

        if (Arrays.stream(KafkaMsgType.values()).map(Enum::name).noneMatch(c -> c.equals(kafkaErrorType))) {
            throw new BadNotificationMessageException("type '" + kafkaErrorType + "' not exist in KafkaMsgType enum");
        }

        KafkaError kafkaError = KafkaError.builder()
                .kafkaMsgType(KafkaMsgType.valueOf(kafkaErrorType))
                .kafkaMessage(kafkaErrorMsg)
                .userNotification(userNotification).build();

        addAdditionalParamFromKafkaErrorMessage(kafkaErrorMsg, KafkaMsgType.valueOf(kafkaErrorType).getErrorMsg(), userNotification);

        kafkaErrorRepository.save(kafkaError);

        websocketService.sendNotificationToUser(kafkaMessage.getAccountId().toString());
    }

    public void addNotification(KafkaMessage kafkaMessage) throws BadNotificationMessageException, KafkaSendMessageException {
        UserNotification userNotification = saveNotification(kafkaMessage);
        websocketService.sendNotificationToUser(kafkaMessage.getAccountId().toString());

        Optional<NotificationMessageParam> optional = userNotification.getMessageParams()
                .stream()
                .filter(notificationMessageParam -> notificationMessageParam.getKey().equals("name"))
                .findFirst();

        boolean sendPushNotification = false;
        StringBuilder sb = new StringBuilder("Powiadomienie o ");

        switch (userNotification.getNotification().getMsgType()) {
            case LEAD_TASK_ACCEPTANCE:
            case LEAD_TASK_ACCEPTED:
            case LEAD_TASK_DECLINED:
            case LEAD_TASK_FINISHED:
            case LEAD_TASK_EXPIRED:
                sendPushNotification = true;
                sb.append("LEAD");
                break;
            case OPPORTUNITY_TASK_ACCEPTANCE:
            case OPPORTUNITY_TASK_ACCEPTED:
            case OPPORTUNITY_TASK_DECLINED:
            case OPPORTUNITY_TASK_FINISHED:
            case OPPORTUNITY_TASK_EXPIRED:
                sendPushNotification = true;
                sb.append("szansie sprzedaży");
                break;
            case EXPENSE_ACCEPTANCE:
            case EXPENSE_ACCEPTED:
            case EXPENSE_DECLINED:
                sendPushNotification = true;
                sb.append("wydatku");
                break;
            case USER_ADDED:
                sendPushNotification = false;
                sb = new StringBuilder("Dodano nowe użytkownika");
                break;
            case EVENT_NOTIFICATION:
                sendPushNotification = true;
                sb = new StringBuilder("Przypomnienie o terminie");
                break;
            case NEW_MAIL:
                sendPushNotification = true;
                sb = new StringBuilder("Nowy email");
                break;
        }

        if (sendPushNotification) {

            if (optional.isPresent()) {
                sb.append(" (").append(optional.get().getValue()).append(")");
            }
            userNotification.getNotification().setMappedTitle(sb.toString());
            pushNotificationService.sendPushNotification(userNotification);
        }
    }

    private UserNotification saveNotification(KafkaMessage kafkaMessage) throws BadNotificationMessageException {
        String notificationMsgType = kafkaMessage.getValueByKey("notificationMsgType").orElseThrow(() -> new BadNotificationMessageException("'notificationMsgType' argument not found"));

        if (Arrays.stream(NotificationMsgType.values()).map(Enum::name).noneMatch(c -> c.equals(notificationMsgType))) {
            throw new BadNotificationMessageException("type '" + notificationMsgType + "' not exist in NotificationMsgType enum");
        }
        if (kafkaMessage.getAccountId() == null) {
            throw new BadNotificationMessageException("accountId is null");
        }

        Notification notification = notificationRepository
                .findByMsgType(NotificationMsgType.valueOf(notificationMsgType))
                .orElseThrow(BadNotificationMessageException::new);

        UserNotification userNotification = UserNotification.builder()
                .accountId(kafkaMessage.getAccountId())
                .created(LocalDateTime.now())
                .notification(notification).build();

        UserNotification saved = userNotificationRepository.save(userNotification);
        saved.setMessageParams(getMessageParams(kafkaMessage, saved));
        return userNotificationRepository.save(saved);
    }

    private void addAdditionalParamFromKafkaErrorMessage(String kafkaErrorMsg, NotificationMsgType notificationMsgType, UserNotification notification) throws BadNotificationMessageException {
        KafkaMessage childKafkaMessage = KafkaUtil.getKafkaObject(kafkaErrorMsg).orElseThrow(BadNotificationMessageException::new);

        switch (notificationMsgType) {
            case KAFKA_CALENDAR_DELETE_EVENT_ERROR:
                notification.getMessageParams().addAll(getAdditionalKafkaErrorMessageParams(childKafkaMessage, notification, "accountId", "taskId"));
                break;
            case KAFKA_CALENDAR_NEW_EVENT_ERROR:
            case KAFKA_CALENDAR_MODIFIED_EVENT_ERROR:
                notification.getMessageParams().addAll(getAdditionalKafkaErrorMessageParams(childKafkaMessage, notification, "accountId", "subject"));
                break;
            case KAFKA_SEND_ACTIVATION_MAIL_ERROR:
            case KAFKA_RESENT_ACTIVATION_MAIL_ERROR:
                notification.getMessageParams().addAll(getAdditionalKafkaErrorMessageParams(childKafkaMessage, notification, "email"));
                break;
            default:
                return;
        }

        userNotificationRepository.save(notification);
    }

    private Set<NotificationMessageParam> getAdditionalKafkaErrorMessageParams(KafkaMessage kafkaMessage, UserNotification userNotification, String... keys) {
        Set<NotificationMessageParam> result = new HashSet<>();
        Arrays.stream(keys).forEach(key -> kafkaMessage.getValueByKey(key)
                .ifPresent(value -> result.add(getNotificationParam(key, value, userNotification))));
        return result;
    }


    private Set<NotificationMessageParam> getMessageParams(KafkaMessage kafkaMessage, UserNotification notification) {
        if (kafkaMessage == null || kafkaMessage.getData() == null) {
            return new HashSet<>();
        }
        return kafkaMessage.getData().stream()
                .filter(c -> !notificationIgnoredArgs.contains(c.getKey()))
                .map(c -> getNotificationParam(c.getKey(), c.getValue(), notification))
                .collect(Collectors.toSet());
    }

    private NotificationMessageParam getNotificationParam(String key, String value, UserNotification userNotification) {
        return NotificationMessageParam.builder()
                .key(key)
                .value(value)
                .userNotification(userNotification).build();
    }
}
