package com.devqube.crmnotificationservice.notification.exception;

public class LoggedUserNotFoundException extends Exception {

    public LoggedUserNotFoundException() {
    }

    public LoggedUserNotFoundException(String message) {
        super(message);
    }
}
