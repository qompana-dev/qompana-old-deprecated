package com.devqube.crmnotificationservice.notification.web;

import com.devqube.crmnotificationservice.firebase.model.TokenUserRequest;
import com.devqube.crmnotificationservice.notification.NotificationActionService;
import com.devqube.crmnotificationservice.notification.NotificationService;
import com.devqube.crmnotificationservice.notification.exception.LoggedUserNotFoundException;
import com.devqube.crmnotificationservice.notification.model.FirebaseToken;
import com.devqube.crmnotificationservice.notification.model.UserNotification;
import com.devqube.crmnotificationservice.notification.repository.FirebaseTokenRepository;
import com.devqube.crmnotificationservice.notification.web.dto.UnreadNotificationCountDto;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmshared.kafka.dto.KafkaMessageData;
import com.devqube.crmshared.notification.NotificationActionDto;
import com.devqube.crmshared.notification.NotificationMsgType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
public class NotificationsController implements NotificationsApi {

    private final NotificationService notificationService;
    private final NotificationActionService notificationActionService;
    private final FirebaseTokenRepository firebaseTokenRepository;

    public NotificationsController(NotificationService notificationService, NotificationActionService notificationActionService, FirebaseTokenRepository firebaseTokenRepository) {
        this.notificationService = notificationService;
        this.notificationActionService = notificationActionService;
        this.firebaseTokenRepository = firebaseTokenRepository;
    }

    @Override
    public ResponseEntity<List<UserNotification>> getNotifications() {
        try {
            return new ResponseEntity<>(notificationService.getUserNotifications(), HttpStatus.OK);
        } catch (LoggedUserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity<Page> getNotificationsForMobile(@Valid Pageable pageable, @Valid Boolean onlyMine, @Valid Boolean forAcceptance) {
        try {
            if (onlyMine != null && onlyMine) {
                return new ResponseEntity<>(notificationService.getOnlyUserNotificationsForMobile(pageable), HttpStatus.OK);
            } else if (forAcceptance != null && forAcceptance) {
                return new ResponseEntity<>(notificationService.getUserNotificationsForAcceptanceForMobile(pageable), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(notificationService.getUserNotificationsForMobile(pageable), HttpStatus.OK);
            }
        } catch (LoggedUserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity<UnreadNotificationCountDto> getUnreadNotificationCount() {
        try {
            return new ResponseEntity<>(notificationService.getUnreadNotificationCount(), HttpStatus.OK);
        } catch (LoggedUserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity<Void> retryMessage(@Valid NotificationActionDto notificationActionDto) {
        try {
            notificationActionService.retryMessage(notificationActionDto.getUserNotificationId());
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return ResponseEntity.notFound().build();
        } catch (KafkaSendMessageException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> deleteMessage(@Valid NotificationActionDto notificationActionDto) {
        notificationActionService.deleteMessage(notificationActionDto.getUserNotificationId());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<UserNotification> markAsRead(Long userNotificationId) {
        try {
            return new ResponseEntity<>(notificationService.markAsRead(userNotificationId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return ResponseEntity.notFound().build();
        } catch (PermissionDeniedException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (KafkaSendMessageException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> markUserAllAsRead() {
        try {
            notificationService.markUserAllAsRead();
            return ResponseEntity.ok().build();
        } catch (LoggedUserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity<UserNotification> getNotificationById(Long id) {
        try {
            return new ResponseEntity<>(notificationService.getUserNotificationById(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Long> getUserNotificationIdByAccountIdAndNotificationMsgTypeAndMessageParam(Long accountId, NotificationMsgType msgType, String msgParam) throws EntityNotFoundException {
        return new ResponseEntity<>(notificationService.getUserNotificationIdByAccountIdAndNotificationMsgTypeAndMessageParam(
                accountId, msgType, msgParam), HttpStatus.OK);
    }


    @Override
    public ResponseEntity<FirebaseToken> saveFirebaseToken(TokenUserRequest request) {
        if(firebaseTokenRepository.countByAccountIdAndToken(request.getAccountId(), request.getToken()) > 0 ) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else {
            return new ResponseEntity<>(firebaseTokenRepository.save(new FirebaseToken(request.getAccountId(), request.getToken())), HttpStatus.OK);
        }
    }

    @Autowired
    private KafkaService kafkaService;

    @RequestMapping(value = "/firebase/test/lead",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.GET)
    public void testLead(@RequestBody Map<String, String> map) throws KafkaSendMessageException {
        HashSet<KafkaMessageData> set = new HashSet<>();

        map.entrySet().stream().forEach(stringStringEntry -> {
            KafkaMessageData kafkaMessageData = new KafkaMessageData();
            kafkaMessageData.setKey(stringStringEntry.getKey());
            kafkaMessageData.setValue(stringStringEntry.getValue());
            set.add(kafkaMessageData);
        });

        kafkaService.addNotification(-1L, NotificationMsgType.LEAD_TASK_ACCEPTED, set);
    }

    @RequestMapping(value = "/firebase/test/oppo",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.GET)
    public void testOppu(@RequestBody Map<String, String> map) throws KafkaSendMessageException {
        HashSet<KafkaMessageData> set = new HashSet<>();

        map.entrySet().stream().forEach(stringStringEntry -> {
            KafkaMessageData kafkaMessageData = new KafkaMessageData();
            kafkaMessageData.setKey(stringStringEntry.getKey());
            kafkaMessageData.setValue(stringStringEntry.getValue());
            set.add(kafkaMessageData);
        });

        kafkaService.addNotification(-1L, NotificationMsgType.OPPORTUNITY_TASK_ACCEPTED, set);
    }

    @RequestMapping(value = "/firebase/test/exp",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.GET)
    public void testExp(@RequestBody Map<String, String> map) throws KafkaSendMessageException {
        HashSet<KafkaMessageData> set = new HashSet<>();

        map.entrySet().stream().forEach(stringStringEntry -> {
            KafkaMessageData kafkaMessageData = new KafkaMessageData();
            kafkaMessageData.setKey(stringStringEntry.getKey());
            kafkaMessageData.setValue(stringStringEntry.getValue());
            set.add(kafkaMessageData);
        });

        kafkaService.addNotification(-1L, NotificationMsgType.EXPENSE_ACCEPTED, set);
    }

    @RequestMapping(value = "/firebase/test/new_mail",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.GET)
    public void testNewMail(@RequestBody Map<String, String> map) throws KafkaSendMessageException {
        HashSet<KafkaMessageData> set = new HashSet<>();

        map.entrySet().stream().forEach(stringStringEntry -> {
            KafkaMessageData kafkaMessageData = new KafkaMessageData();
            kafkaMessageData.setKey(stringStringEntry.getKey());
            kafkaMessageData.setValue(stringStringEntry.getValue());
            set.add(kafkaMessageData);
        });

        kafkaService.addNotification(-1L, NotificationMsgType.NEW_MAIL, set);
    }


}
