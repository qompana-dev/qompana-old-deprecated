package com.devqube.crmnotificationservice.notification.repository;

import com.devqube.crmnotificationservice.notification.model.Notification;
import com.devqube.crmshared.notification.NotificationMsgType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {
    Optional<Notification> findByMsgType(NotificationMsgType msgType);
}
