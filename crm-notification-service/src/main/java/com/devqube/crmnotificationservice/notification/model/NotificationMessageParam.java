package com.devqube.crmnotificationservice.notification.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NotificationMessageParam {
    @Id
    @SequenceGenerator(name="notification_message_param_seq", sequenceName="notification_message_param_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="notification_message_param_seq")
    private Long id;

    @Length(max = 300)
    @NotNull
    @Column(name = "key")
    private String key;

    @Length(max = 300)
    @Column(name = "value")
    private String value;

    @NotNull
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_notification_id")
    private UserNotification userNotification;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NotificationMessageParam that = (NotificationMessageParam) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(key, that.key) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, key, value);
    }

    @Override
    public String toString() {
        return "NotificationMessageParam{" +
                "id=" + id +
                ", key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
