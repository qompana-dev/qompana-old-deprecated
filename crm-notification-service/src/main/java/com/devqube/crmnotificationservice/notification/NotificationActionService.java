package com.devqube.crmnotificationservice.notification;

import com.devqube.crmnotificationservice.notification.model.KafkaError;
import com.devqube.crmnotificationservice.notification.model.UserNotification;
import com.devqube.crmnotificationservice.notification.repository.KafkaErrorRepository;
import com.devqube.crmnotificationservice.notification.repository.UserNotificationRepository;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.KafkaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class NotificationActionService {

    private final KafkaErrorRepository kafkaErrorRepository;
    private final UserNotificationRepository userNotificationRepository;
    private final KafkaService kafkaService;

    public NotificationActionService(KafkaErrorRepository kafkaErrorRepository, UserNotificationRepository userNotificationRepository, KafkaService kafkaService) {
        this.kafkaErrorRepository = kafkaErrorRepository;
        this.userNotificationRepository = userNotificationRepository;
        this.kafkaService = kafkaService;
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class, KafkaSendMessageException.class})
    public void retryMessage(Long userNotificationId) throws EntityNotFoundException, KafkaSendMessageException {
        UserNotification userNotification = userNotificationRepository.findById(userNotificationId).orElseThrow(EntityNotFoundException::new);
        KafkaError kafkaError = kafkaErrorRepository.findByUserNotificationId(userNotification.getId()).orElseThrow(EntityNotFoundException::new);

        kafkaService.send(kafkaError.getKafkaMsgType(), kafkaError.getKafkaMessage());

        deleteMessage(userNotificationId);
    }

    @Transactional
    public void deleteMessage(Long userNotificationId) {
        kafkaErrorRepository.deleteByUserNotificationId(userNotificationId);
        userNotificationRepository.deleteById(userNotificationId);
    }

}
