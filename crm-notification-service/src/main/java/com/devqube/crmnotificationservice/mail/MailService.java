package com.devqube.crmnotificationservice.mail;

import com.devqube.crmnotificationservice.integration.UserClient;
import com.devqube.crmnotificationservice.mail.model.MailTemplate;
import com.devqube.crmnotificationservice.mail.model.MailTemplateLangEnum;
import com.devqube.crmnotificationservice.mail.model.MailTypeEnum;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import com.devqube.crmshared.exception.EntityNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class MailService {
    private final MailTemplateRepository mailTemplateRepository;
    private final TemplateEngine templateEngine;
    private final JavaMailSender mailSender;
    private final UserClient userClient;

    @Value("${client.url}")
    private String clientGuiUrl;

    public MailService(MailTemplateRepository mailTemplateRepository, TemplateEngine templateEngine, JavaMailSender mailSender, UserClient userClient) {
        this.mailTemplateRepository = mailTemplateRepository;
        this.templateEngine = templateEngine;
        this.mailSender = mailSender;
        this.userClient = userClient;
    }

    public void sendTokenMail(MailTypeEnum typeEnum, KafkaMessage kafkaMessage) throws MessagingException {
        Optional<String> email = kafkaMessage.getValueByKey("email");
        Optional<String> token = kafkaMessage.getValueByKey("url");
        if (email.isEmpty() || token.isEmpty()) {
            log.error("incorrect token message. token or email is empty");
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("url", clientGuiUrl + token.get());
        sendMailByTemplate(typeEnum, email.get(), params);
    }

    public void sendEventNotificationMail(KafkaMessage kafkaMessage) throws EntityNotFoundException, MessagingException {
        String accountId = kafkaMessage.getValueByKey("owner").orElseThrow(EntityNotFoundException::new);
        String accountEmail = userClient.getAccountEmail(accountId);

        Map<String, String> map = kafkaMessage.getValuesMap("place", "timeToEvent", "subject")
                .orElseThrow(EntityNotFoundException::new);

        sendMailByTemplate(MailTypeEnum.EVENT_NOTIFICATION, MailTemplateLangEnum.PL, accountEmail, map);
    }

    public void sendMailByTemplate(MailTypeEnum typeEnum, String email, Map<String, String> params) throws MessagingException {
        sendMailByTemplate(typeEnum, MailTemplateLangEnum.PL, email, params);
    }

    private void sendMailByTemplate(MailTypeEnum typeEnum, MailTemplateLangEnum lang, String email, Map<String, String> params) throws MessagingException {
        MailTemplate template = mailTemplateRepository.findOneByLangAndType(lang, typeEnum);
        String html = getMailFromTemplate(template.getTemplate(), params);
        sendMail(template.getSubject(), html, email);
    }

    private void sendMail(String title, String htmlContent, String email) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
        mimeMessage.setHeader("Content-Type", "text/plain; charset=UTF-8");
        mimeMessage.setContent(htmlContent, "text/html; charset=UTF-8");
        helper.setTo(email);
        helper.setSubject(title);
        mailSender.send(mimeMessage);
    }

    private String getMailFromTemplate(String templateHtml, Map<String, String> params) {
        Context context = new Context();
        params.keySet().forEach(c -> context.setVariable(c, params.get(c)));
        return templateEngine.process(templateHtml, context);
    }
}
