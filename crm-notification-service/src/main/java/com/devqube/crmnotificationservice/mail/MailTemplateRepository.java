package com.devqube.crmnotificationservice.mail;

import com.devqube.crmnotificationservice.mail.model.MailTemplate;
import com.devqube.crmnotificationservice.mail.model.MailTemplateLangEnum;
import com.devqube.crmnotificationservice.mail.model.MailTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MailTemplateRepository extends JpaRepository<MailTemplate, Long> {
    MailTemplate findOneByLangAndType(MailTemplateLangEnum lang, MailTypeEnum type);
}
