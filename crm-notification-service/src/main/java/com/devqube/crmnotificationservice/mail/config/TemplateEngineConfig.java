package com.devqube.crmnotificationservice.mail.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.StringTemplateResolver;

@Configuration
public class TemplateEngineConfig {
    @Autowired
    private TemplateEngine templateEngine;

    @Bean
    public TemplateEngine getTemplateEngine() {
        if (templateEngine.getTemplateResolvers().stream().noneMatch(c -> c.getClass().equals(StringTemplateResolver.class))) {
            StringTemplateResolver templateResolver = new StringTemplateResolver();
            templateResolver.setTemplateMode(TemplateMode.HTML);
            templateEngine.setTemplateResolver(templateResolver);
        }
        return templateEngine;
    }
}
