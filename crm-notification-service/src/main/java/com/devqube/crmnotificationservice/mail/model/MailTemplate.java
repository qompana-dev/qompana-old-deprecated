package com.devqube.crmnotificationservice.mail.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MailTemplate {
    @JsonProperty("id")
    @Id
    @SequenceGenerator(name="mail_template_seq", sequenceName="mail_template_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="mail_template_seq")
    private Long id;

    @JsonProperty("lang")
    @Enumerated(EnumType.ORDINAL)
    private MailTemplateLangEnum lang;

    @JsonProperty("subject")
    private String subject;

    @JsonProperty("template")
    @Type(type = "text")
    private String template;

    @JsonProperty("type")
    @Enumerated(EnumType.ORDINAL)
    private MailTypeEnum type;
}
