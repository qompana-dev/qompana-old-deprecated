package com.devqube.crmnotificationservice.mail.model;

public enum MailTypeEnum {
    ACTIVATION_MAIL,
    RESENT_ACTIVATION_MAIL,
    RESET_PASSWORD,
    EVENT_NOTIFICATION
}
