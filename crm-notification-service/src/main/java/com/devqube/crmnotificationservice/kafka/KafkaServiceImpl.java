package com.devqube.crmnotificationservice.kafka;

import com.devqube.crmnotificationservice.mail.MailService;
import com.devqube.crmnotificationservice.mail.model.MailTypeEnum;
import com.devqube.crmnotificationservice.notification.NotificationService;
import com.devqube.crmnotificationservice.notification.exception.BadNotificationMessageException;
import com.devqube.crmnotificationservice.websocket.WebsocketService;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.AbstractKafkaService;
import com.devqube.crmshared.kafka.ServiceKafkaAddr;
import com.devqube.crmshared.kafka.annotation.KafkaReceiver;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.kafka.type.ServiceType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Slf4j
@Service
public class KafkaServiceImpl extends AbstractKafkaService {
    private final WebsocketService websocketService;
    private final NotificationService notificationService;
    private final MailService mailService;

    public KafkaServiceImpl(MailService mailService, NotificationService notificationService, @Lazy WebsocketService websocketService) {
        this.mailService = mailService;
        this.notificationService = notificationService;
        this.websocketService = websocketService;
    }

    @KafkaListener(topics = ServiceKafkaAddr.CRM_NOTIFICATION_SERVICE)
    public void processMessage(String content) {
        super.processMessage(content);
    }

    @KafkaReceiver(from = KafkaMsgType.NOTIFICATION_KAFKA_ERROR)
    public void receiveKafkaError(KafkaMessage kafkaMessage) {
        log.info(kafkaMessage.getDestination().name());
        try {
            notificationService.receiveKafkaError(kafkaMessage);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @KafkaReceiver(from = KafkaMsgType.NOTIFICATION_ADD_NOTIFICATION)
    public void addNotification(KafkaMessage kafkaMessage) {
        log.info(kafkaMessage.getDestination().name());
        try {
            notificationService.addNotification(kafkaMessage);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @KafkaReceiver(from = KafkaMsgType.NOTIFICATION_RESENT_ACTIVATION_MAIL)
    public void resentActivationMail(KafkaMessage kafkaMessage) throws MessagingException {
        log.info(kafkaMessage.getDestination().name());
        mailService.sendTokenMail(MailTypeEnum.RESENT_ACTIVATION_MAIL, kafkaMessage);
    }

    @KafkaReceiver(from = KafkaMsgType.NOTIFICATION_SEND_ACTIVATION_MAIL)
    public void sendActivationMail(KafkaMessage kafkaMessage) throws MessagingException {
        log.info(kafkaMessage.getDestination().name());
        mailService.sendTokenMail(MailTypeEnum.RESENT_ACTIVATION_MAIL, kafkaMessage);

    }

    @KafkaReceiver(from = KafkaMsgType.NOTIFICATION_SEND_RESET_PASSWORD_MAIL)
    public void resetPasswordMail(KafkaMessage kafkaMessage) throws MessagingException {
        log.info(kafkaMessage.getDestination().name());
        mailService.sendTokenMail(MailTypeEnum.RESET_PASSWORD, kafkaMessage);
    }

    @KafkaReceiver(from = KafkaMsgType.NOTIFICATION_WS_USER_NOTIFICATION_CHANGE)
    public void wsUserNotificationChange(KafkaMessage kafkaMessage) throws KafkaSendMessageException {
        log.info(kafkaMessage.getDestination().name());
        websocketService.sendNotificationToUser(kafkaMessage);
    }

    @KafkaReceiver(from = KafkaMsgType.NOTIFICATION_SEND_EVENT_NOTIFICATION_MAIL)
    public void sendEventNotificationMail(KafkaMessage kafkaMessage) throws MessagingException, EntityNotFoundException {
        log.info(kafkaMessage.getDestination().name());
        mailService.sendEventNotificationMail(kafkaMessage);
    }

    @Override
    public void receiverNotFound(KafkaMessage kafkaMessage) {
        log.info("receiverNotFound" + kafkaMessage.getDestination().name());
    }
}
