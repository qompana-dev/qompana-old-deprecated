create table mail_template
(
    id       bigint PRIMARY KEY,
    lang     int8 NOT NULL,
    subject  VARCHAR(200),
    template text,
    type     int8 NOT NULL,
    UNIQUE (lang, type)
);

COMMENT ON COLUMN mail_template.lang IS '0 - PL;';
COMMENT ON COLUMN mail_template.type IS '0 - ACTIVATION_MAIL; 1 - RESENT_ACTIVATION_MAIL; 2 - RESET_PASSWORD;';

create sequence mail_template_seq start 1;

-- notification


create TABLE notification
(
    id                 bigint PRIMARY KEY,
    name               VARCHAR(300)        NOT NULL,
    msg_type           VARCHAR(200) UNIQUE NOT NULL,
    group_notification boolean             NOT NULL,
    type               int8                NOT NULL
);
COMMENT ON COLUMN notification.group_notification IS 'if the notification is a group notification then should not contain any actions';

COMMENT ON COLUMN notification.type IS '0 - SUCCESS; 1 - INFO; 2 - ERROR;';
create sequence notification_seq start 1;

create TABLE user_notification
(
    id              bigint PRIMARY KEY,
    account_id      bigint    NOT NULL,
    created         TIMESTAMP NOT NULL,
    read            TIMESTAMP,
    notification_id bigint    NOT NULL,
    CONSTRAINT user_notification_notification_id FOREIGN KEY (notification_id)
        REFERENCES notification (id)
);

create sequence user_notification_seq start 1;

create TABLE notification_message_param
(
    id                   bigint PRIMARY KEY,
    key                  VARCHAR(300) NOT NULL,
    value                VARCHAR(300),
    user_notification_id bigint       NOT NULL,
    CONSTRAINT message_param_user_notification_id FOREIGN KEY (user_notification_id)
        REFERENCES user_notification (id)
);

create sequence notification_message_param_seq start 1;

create TABLE notification_action
(
    id              bigint PRIMARY KEY,
    rest_url        text         NOT NULL,
    name            VARCHAR(300) NOT NULL,
    notification_id bigint       NOT NULL,
    CONSTRAINT notification_action_notification_id FOREIGN KEY (notification_id)
        REFERENCES notification (id)
);

create sequence notification_action_seq start 1;


create TABLE kafka_error
(
    id                   bigint PRIMARY KEY,
    kafka_message        text         NOT NULL,
    kafka_msg_type       VARCHAR(200) NOT NULL,
    user_notification_id bigint       NOT NULL,
    CONSTRAINT kafka_error_user_notification_id FOREIGN KEY (user_notification_id)
        REFERENCES user_notification (id)
);

create sequence kafka_error_seq start 1;

