create table firebase_token
(
    id         bigint primary key,
    account_id bigint not null,
    token      varchar(200) not null
);

create sequence firebase_token_seq start 1;
