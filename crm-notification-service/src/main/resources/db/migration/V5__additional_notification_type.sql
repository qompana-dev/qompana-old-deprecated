INSERT INTO notification(id, name, msg_type, type, group_notification) values (nextval('notification_seq'), 'notifications.type.leadTaskExpired', 'LEAD_TASK_EXPIRED', 0, false);
INSERT INTO notification(id, name, msg_type, type, group_notification) values (nextval('notification_seq'), 'notifications.type.opportunityTaskExpired', 'OPPORTUNITY_TASK_EXPIRED', 0, false);
