insert into mail_template(id, lang, subject, type, template)
values(-1, 0, 'Potwierdzenie maila', 0, '<!DOCTYPE html><html lang="pl" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"><head>    <meta charset="utf-8">    <meta http-equiv="Content-Type: text/html;charset=utf-8" /></head><body><h1>Link potwierdzający</h1><p>Aby aktywować swoje konto kliknij w poniższy link <br/>    <a th:href="${url}"><span th:text="${url}"></span></a></p></body></html>');
insert into mail_template(id, lang, subject, type, template)
values(-2, 0, 'Potwierdzenie maila', 1, '<!DOCTYPE html><html lang="pl" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"><head>    <meta charset="utf-8">    <meta http-equiv="Content-Type: text/html;charset=utf-8" /></head><body><h1>Link potwierdzający</h1><p>Aby aktywować swoje konto kliknij w poniższy link <br/>    <a th:href="${url}"><span th:text="${url}"></span></a></p></body></html>');
insert into mail_template(id, lang, subject, type, template)
values(-3, 0, 'Resetowanie hasła', 2, '<!DOCTYPE html><html lang="pl" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"><head>    <meta charset="utf-8">    <meta http-equiv="Content-Type: text/html;charset=utf-8" /></head><body><h1>Resetowanie hasła</h1><p>Aby zresetować hasło do swojego konta kliknij w poniższy link <br/>    <a th:href="${url}"><span th:text="${url}"></span></a></p></body></html>');
insert into mail_template(id, lang, subject, type, template)
values(-4, 0, 'Wydarzenie', 3, '<!DOCTYPE html><html lang="pl" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"><head>    <meta charset="utf-8">    <meta http-equiv="Content-Type: text/html;charset=utf-8" /></head><body><h1>Przypomnienie o wydarzeniu</h1><p>Przypominamy o zbliżającym się wydarzeniu ''<span th:text="${subject}"></span>'' w miejscu ''<span th:text="${place}"></span>'' za <span th:text="${timeToEvent}"></span>.</p></body></html>');


-- KAFKA_ERROR

-- Błąd wysłania wiadomości na kafkę
INSERT INTO notification(id, name, msg_type, type, group_notification) values (-1, 'notifications.type.kafkaGenericError', 'KAFKA_GENERIC_ERROR', 2, false);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-1, 'crm-notification-service/notifications/action/retry', 'notifications.action.retry', -1);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-2, 'crm-notification-service/notifications/action/delete', 'notifications.action.delete', -1);


INSERT INTO notification(id, name, msg_type, type, group_notification) values (-2, 'notifications.type.kafkaAddNotificationError', 'KAFKA_ADD_NOTIFICATION_ERROR', 2, false);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-3, 'crm-notification-service/notifications/action/retry', 'notifications.action.retry', -2);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-4, 'crm-notification-service/notifications/action/delete', 'notifications.action.delete', -2);

INSERT INTO notification(id, name, msg_type, type, group_notification) values (-3, 'notifications.type.kafkaActivationMailError', 'KAFKA_SEND_ACTIVATION_MAIL_ERROR', 2, false);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-7, 'crm-notification-service/notifications/action/retry', 'notifications.action.retry', -3);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-8, 'crm-notification-service/notifications/action/delete', 'notifications.action.delete', -3);

INSERT INTO notification(id, name, msg_type, type, group_notification) values (-4, 'notifications.type.kafkaResentActivationMailError', 'KAFKA_RESENT_ACTIVATION_MAIL_ERROR', 2, false);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-9, 'crm-notification-service/notifications/action/retry', 'notifications.action.retry', -4);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-10, 'crm-notification-service/notifications/action/delete', 'notifications.action.delete', -4);

INSERT INTO notification(id, name, msg_type, type, group_notification) values (-5, 'notifications.type.kafkaSendResetPasswordMailError', 'KAFKA_SEND_RESET_PASSWORD_MAIL_ERROR', 2, false);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-11, 'crm-notification-service/notifications/action/retry', 'notifications.action.retry', -5);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-12, 'crm-notification-service/notifications/action/delete', 'notifications.action.delete', -5);

INSERT INTO notification(id, name, msg_type, type, group_notification) values (-6, 'notifications.type.kafkaNewEventError', 'KAFKA_CALENDAR_NEW_EVENT_ERROR', 2, false);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-13, 'crm-notification-service/notifications/action/retry', 'notifications.action.retry', -6);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-14, 'crm-notification-service/notifications/action/delete', 'notifications.action.delete', -6);

INSERT INTO notification(id, name, msg_type, type, group_notification) values (-7, 'notifications.type.kafkaDeleteEventError', 'KAFKA_CALENDAR_DELETE_EVENT_ERROR', 2, false);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-15, 'crm-notification-service/notifications/action/retry', 'notifications.action.retry', -7);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-16, 'crm-notification-service/notifications/action/delete', 'notifications.action.delete', -7);

INSERT INTO notification(id, name, msg_type, type, group_notification) values (-8, 'notifications.type.kafkaModifyEventError', 'KAFKA_CALENDAR_MODIFIED_EVENT_ERROR', 2, false);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-17, 'crm-notification-service/notifications/action/retry', 'notifications.action.retry', -8);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (-18, 'crm-notification-service/notifications/action/delete', 'notifications.action.delete', -8);

INSERT INTO notification(id, name, msg_type, type, group_notification) values (-9, 'notifications.type.userAdded', 'USER_ADDED', 0, true);

INSERT INTO notification(id, name, msg_type, type, group_notification) values (-10, 'notifications.type.eventNotification', 'EVENT_NOTIFICATION', 1, false);

INSERT INTO notification(id, name, msg_type, type, group_notification) values (-11, 'notifications.type.leadTaskFinished', 'LEAD_TASK_FINISHED', 1, true);

INSERT INTO notification(id, name, msg_type, type, group_notification) values (nextval('notification_seq'), 'notifications.type.leadTaskAcceptance', 'LEAD_TASK_ACCEPTANCE', 1, false);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (nextval('notification_action_seq'), 'crm-business-service/process-engine/accept-task', 'notifications.action.accept', currval('notification_seq'));
INSERT INTO notification_action(id, rest_url, name, notification_id) values (nextval('notification_action_seq'), 'crm-business-service/process-engine/decline-task', 'notifications.action.decline', currval('notification_seq'));

INSERT INTO notification(id, name, msg_type, type, group_notification) values (nextval('notification_seq'), 'notifications.type.leadTaskAccepted', 'LEAD_TASK_ACCEPTED', 0, true);
INSERT INTO notification(id, name, msg_type, type, group_notification) values (nextval('notification_seq'), 'notifications.type.leadTaskDeclined', 'LEAD_TASK_DECLINED', 2, true);

INSERT INTO notification(id, name, msg_type, type, group_notification) values (nextval('notification_seq'), 'notifications.type.opportunityTaskFinished', 'OPPORTUNITY_TASK_FINISHED', 1, true);

INSERT INTO notification(id, name, msg_type, type, group_notification) values (nextval('notification_seq'), 'notifications.type.opportunityTaskAcceptance', 'OPPORTUNITY_TASK_ACCEPTANCE', 1, false);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (nextval('notification_action_seq'), 'crm-business-service/process-engine/accept-task', 'notifications.action.accept', currval('notification_seq'));
INSERT INTO notification_action(id, rest_url, name, notification_id) values (nextval('notification_action_seq'), 'crm-business-service/process-engine/decline-task', 'notifications.action.decline', currval('notification_seq'));

INSERT INTO notification(id, name, msg_type, type, group_notification) values (nextval('notification_seq'), 'notifications.type.opportunityTaskAccepted', 'OPPORTUNITY_TASK_ACCEPTED', 0, true);
INSERT INTO notification(id, name, msg_type, type, group_notification) values (nextval('notification_seq'), 'notifications.type.opportunityTaskDeclined', 'OPPORTUNITY_TASK_DECLINED', 2, true);


INSERT INTO notification(id, name, msg_type, type, group_notification) values (nextval('notification_seq'), 'notifications.type.expenseAcceptance', 'EXPENSE_ACCEPTANCE', 1, false);
INSERT INTO notification_action(id, rest_url, name, notification_id) values (nextval('notification_action_seq'), 'crm-business-service/expense/accept', 'notifications.action.accept', currval('notification_seq'));
INSERT INTO notification_action(id, rest_url, name, notification_id) values (nextval('notification_action_seq'), 'crm-business-service/expense/decline', 'notifications.action.decline', currval('notification_seq'));

INSERT INTO notification(id, name, msg_type, type, group_notification) values (nextval('notification_seq'), 'notifications.type.expenseAccepted', 'EXPENSE_ACCEPTED', 0, true);
INSERT INTO notification(id, name, msg_type, type, group_notification) values (nextval('notification_seq'), 'notifications.type.expenseDeclined', 'EXPENSE_DECLINED', 2, true);

INSERT INTO notification(id, name, msg_type, type, group_notification) values (nextval('notification_seq'), 'notifications.type.newMail', 'NEW_MAIL', 1, false);
