import sys
import os
import shutil
import fileinput

if len(sys.argv) != 5:
    print("Usage: qompanaDeploy.py namespace domain instance_id image_tag [init_user_name] [license_url]")
    exit(2)

namespace = sys.argv[1]
domain = sys.argv[2]
instance_id = sys.argv[3]
image_tag = sys.argv[4]
init_user_name = "admin@admin.com"
license_url = "https://devapps.doublecloud.pl:8094/"

if len(sys.argv) > 6:
    license_url = sys.argv[6]

print("Creating configuration for", domain)
print("Namespace:", namespace)
print("Instance ID:", instance_id)

shutil.copytree("template", namespace)

for filename in os.listdir(namespace):
    if os.path.isfile(os.path.join(namespace, filename)):
        with fileinput.FileInput(os.path.join(namespace, filename), inplace=True) as file:
            for line in file:
                line = line.replace("!--NAMESPACE--!", namespace)
                line = line.replace("!--INSTANCEID--!", instance_id)
                line = line.replace("!--IMAGETAG--!", image_tag)
                line = line.replace("!--LICENSEURL--!", license_url)
                line = line.replace("!--INITUSERNAME--!", init_user_name)
                print(line.replace("!--DOMAIN--!", domain), end="")

for filename in os.listdir(os.path.join(namespace, "secret")):
    if os.path.isfile(os.path.join(namespace, "secret", filename)):
        with fileinput.FileInput(os.path.join(namespace, "secret", filename), inplace=True) as file:
            for line in file:
                line = line.replace("!--NAMESPACE--!", namespace)
                line = line.replace("!--INSTANCEID--!", instance_id)
                line = line.replace("!--IMAGETAG--!", image_tag)
                line = line.replace("!--LICENSEURL--!", license_url)
                line = line.replace("!--INITUSERNAME--!", init_user_name)
                print(line.replace("!--DOMAIN--!", domain), end="")

print("Done.")
