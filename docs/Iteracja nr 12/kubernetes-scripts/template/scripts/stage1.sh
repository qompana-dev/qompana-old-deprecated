#!/bin/bash

kubectl apply -f ./0000-crm-namespace.yaml
kubectl apply -f ./0001-crm-storageclass.yaml
kubectl apply -f ./0002-instance-config.yaml
kubectl apply -f ./0003-crm-config.yaml
kubectl apply -f ./0101-crm-zookeeper-service.yaml
kubectl apply -f ./0102-crm-kafka-service.yaml
kubectl apply -f ./0103-crm-zipkin-service.yaml
kubectl apply -f ./0104-crm-eureka-service.yaml
kubectl apply -f ./0202-crm-business-service-db.yaml