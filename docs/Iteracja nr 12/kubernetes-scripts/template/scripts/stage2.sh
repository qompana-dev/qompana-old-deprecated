#!/bin/bash

kubectl apply -f ./0301-crm-gateway-service.yaml
kubectl apply -f ./0302-crm-user-service.yaml
kubectl apply -f ./0303-crm-association-service.yaml
kubectl apply -f ./0304-crm-frontend.yaml
kubectl apply -f ./0305-crm-mail-service.yaml
kubectl apply -f ./0306-crm-bugtracker-service.yaml
kubectl apply -f ./0307-crm-notification-service.yaml
kubectl apply -f ./0308-crm-business-service.yaml
kubectl apply -f ./0309-crm-calendar-service.yaml
kubectl apply -f ./0310-crm-task-service.yaml
kubectl apply -f ./0311-crm-report-service.yaml
kubectl apply -f ./0312-crm-file-service.yaml
kubectl apply -f ./0313-crm-history-service.yaml
kubectl apply -f ./0901-igress-frontend.yaml
kubectl apply -f ./0902-igress-api.yaml