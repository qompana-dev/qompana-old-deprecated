export const environment = {
  production: true,
  toastTime: 5000,
  url: 'https://devapps.doublecloud.pl:8094/api',
};
