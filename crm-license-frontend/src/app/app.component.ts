import {Component, OnDestroy, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {LoginService} from './login/login.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();

  loggedIn: boolean;

  constructor(private translateService: TranslateService,
              private loginService: LoginService) {}

  ngOnInit(): void {
    this.setDefaultConfig();
    const locale = localStorage.getItem('locale');
    this.translateService.setDefaultLang(locale ? locale : 'pl');
    this.loggedIn = this .loginService.isLoggedIn();
    this.loginService.loginStatusChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.loggedIn = this.loginService.isLoggedIn());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private setDefaultConfig(): void {
    localStorage.setItem('locale', 'pl');
    localStorage.setItem('timeZone', 'Europe/Warsaw');
    localStorage.setItem('dateFormat', 'YYYY/MM/DD');
    localStorage.setItem('hourFormat', '12');
  }
}
