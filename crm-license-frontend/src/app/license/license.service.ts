import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
  CreateLicenseDto, InstanceIdDto,
  License,
  LicenseList,
  NewPaymentDto,
  UpdateLicenseDto,
  UpdatePaymentDto
} from './license.model';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LicenseService {

  constructor(private http: HttpClient) {
  }

  getLicenseList(): Observable<LicenseList[]> {
    return this.http.get<LicenseList[]>(`${environment.url}/licenses`);
  }

  getLicense(id: number): Observable<License> {
    return this.http.get<License>(`${environment.url}/license/${id}`);
  }

  updateLicense(id: number, updateLicense: UpdateLicenseDto): Observable<License> {
    return this.http.put<License>(`${environment.url}/license/${id}`, updateLicense);
  }

  register(updateLicense: UpdateLicenseDto, addTrial = false): Observable<License> {
    const toSend: CreateLicenseDto = updateLicense;
    toSend.addTrial = addTrial;
    return this.http.post<License>(`${environment.url}/register`, toSend);
  }

  createLicense(updateLicense: UpdateLicenseDto, addTrial = false): Observable<License> {
    const toSend: CreateLicenseDto = updateLicense;
    toSend.addTrial = addTrial;
    return this.http.post<License>(`${environment.url}/license`, toSend);
  }

  changeLicenseActivation(id: number, activation: boolean): Observable<License> {
    return this.http.post<License>(`${environment.url}/license/${id}/activation`, {active: activation});
  }

  addPayment(newPaymentDto: NewPaymentDto): Observable<License> {
    return this.http.post<License>(`${environment.url}/license/payment`, newPaymentDto);
  }

  updatePayment(id: number, updatePayment: UpdatePaymentDto): Observable<License> {
    return this.http.put<License>(`${environment.url}/license/payment/${id}`, updatePayment);
  }

  deletePayment(id: number): Observable<void> {
    return this.http.delete<void>(`${environment.url}/license/payment/${id}`);
  }

  createInstance(licenseId: number): Observable<InstanceIdDto> {
    return this.http.post<InstanceIdDto>(`${environment.url}/license/${licenseId}/instance`, null);
  }

  removeInstance(licenseId: number): Observable<void> {
    return this.http.delete<void>(`${environment.url}/license/${licenseId}/instance`);
  }
}
