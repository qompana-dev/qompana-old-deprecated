export interface LicenseList {
  id: number;
  name: string;
  phone: string;
  mail: string;
  instanceId: string;
  domain: string;
  type: LicenseTypeEnum;
  lastPayment: string;
  expirationDate: string;
  locked: string;
}

export interface License {
  id: number;
  name: string;
  phone: string;
  mail: string;
  locked: boolean;
  licenseCheckResult: boolean;
  instanceId: string;
  localInstance: boolean;
  domain: string;
  payments: Payment[];
}

export interface Payment {
  id: number;
  licenseType: LicenseTypeEnum;
  paymentDate: string;
  numberOfDays: number;
  numberOfUsers: number;
  expiredDate: string;
}

export enum LicenseTypeEnum {
  FULL = 'FULL',
  TRIAL = 'TRIAL'
}

export interface UpdateLicenseDto {
  name: string;
  phone: string;
  mail: string;
  domain: string;
  instanceId?: string;
}

export interface CreateLicenseDto extends UpdateLicenseDto {
  addTrial?: boolean;
}

export interface UpdatePaymentDto {
  date: string;
  numberOfDays: number;
  numberOfUsers: number;
  licenseType: LicenseTypeEnum;
}

export interface NewPaymentDto extends UpdatePaymentDto{
  licenseId?: number;
}

export interface InstanceIdDto {
  instanceId: string;
}

