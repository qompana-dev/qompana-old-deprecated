import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {License, Payment} from '../../license.model';
import {Subject} from 'rxjs';
import {LicenseService} from '../../license.service';
import {takeUntil} from 'rxjs/operators';
import {NotificationService, NotificationType} from '../../../shared/notification.service';

@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.scss']
})
export class PaymentListComponent implements OnInit, OnChanges, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();

  @Input() license: License;
  @Output() licenseChanged: EventEmitter<License> = new EventEmitter<License>();
  @Output() changePaymentForm: EventEmitter<Payment> = new EventEmitter<Payment>();

  paymentSubject: Subject<Payment[]> = new Subject<Payment[]>();

  columns: string[] = ['paymentDate', 'licenseType', 'numberOfDays', 'numberOfUsers', 'expiredDate', 'action'];

  constructor(private licenseService: LicenseService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.license != null) {
      if (!this.license || !this.license.payments) {
        this.paymentSubject.next([]);
      } else {
        this.paymentSubject.next(this.license.payments);
      }
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  newPayment(): void {
    this.changePaymentForm.emit(undefined);
  }

  editPayment(id: number): void {
    if (this.license.payments) {
      const selected = this.license.payments.find(c => c.id === id);
      this.changePaymentForm.emit(selected);
    }
  }

  deletePayment(id: number): void {
    this.licenseService.deletePayment(id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.notificationService.notify('license.form.paymentList.paymentRemoved', NotificationType.SUCCESS);
        this.license.payments = this.license.payments.filter(c => c.id !== id);
        this.paymentSubject.next(this.license.payments);
      }, () => this.notificationService.notify('license.form.paymentList.paymentRemoveError', NotificationType.ERROR));
  }
}
