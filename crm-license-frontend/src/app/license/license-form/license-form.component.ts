import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LicenseService} from '../license.service';
import {InstanceIdDto, License, Payment, UpdateLicenseDto} from '../license.model';
import {FormUtil} from '../../shared/util/form.util';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {SliderComponent} from '../../shared/slider/slider.component';

@Component({
  selector: 'app-license-form',
  templateUrl: './license-form.component.html',
  styleUrls: ['./license-form.component.scss']
})
export class LicenseFormComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();

  @ViewChild('paymentForm', {static: false}) paymentForm: SliderComponent;

  licenseForm: FormGroup;

  licenseForEdit: License;
  editMode = false;
  editLicenseId: number;

  selectedPayment: Payment;

  instanceRequestInProgress = false;

  constructor(private $router: Router,
              private fb: FormBuilder,
              private notificationService: NotificationService,
              private licenseService: LicenseService,
              private route: ActivatedRoute) {
    this.createForm();
  }

  // @formatter:off
  get name(): AbstractControl {return this.licenseForm.get('name'); }
  get phone(): AbstractControl {return this.licenseForm.get('phone'); }
  get email(): AbstractControl {return this.licenseForm.get('email'); }
  get instanceId(): AbstractControl {return this.licenseForm.get('instanceId'); }
  get domain(): AbstractControl {return this.licenseForm.get('domain'); }
  // @formatter:on

  ngOnInit(): void {
    this.route.params
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((obj: Params) => {
        this.editMode = obj.id != null;
        this.editLicenseId = obj.id;
        this.updateForm();
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  save(): void {
    FormUtil.setTouched(this.licenseForm);
    if (this.licenseForm.valid) {
      (this.editMode) ? this.updateLicense() : this.createLicense();
    }
  }

  createLicense(): void {
    this.licenseService.createLicense(this.getLicenseForm(), false)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((license: License) => {
        this.editMode = true;
        this.editLicenseId = license.id;
        this.setLicenseToForm(license);
        this.notificationService.notify('license.form.notification.createdSuccessfully', NotificationType.SUCCESS);
      }, () => this.notificationService.notify('license.form.notification.saveError', NotificationType.ERROR));
  }

  updateLicense(): void {
    this.licenseService.updateLicense(this.editLicenseId, this.getLicenseForm())
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((license: License) => {
        this.setLicenseToForm(license);
        this.notificationService.notify('license.form.notification.updatedSuccessfully', NotificationType.SUCCESS);
      }, () => this.notificationService.notify('license.form.notification.saveError', NotificationType.ERROR));
  }


  changeActivate(): void {
    this.licenseService.changeLicenseActivation(this.editLicenseId, this.licenseForEdit.locked)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((license: License) => {
        this.setLicenseToForm(license);
        this.notificationService.notify('license.form.notification.updatedSuccessfully', NotificationType.SUCCESS);
      }, (err) => {
        if (err.status === 409) {
          this.notificationService.notify('license.form.notification.domainOrEmailExist', NotificationType.ERROR);
        } else {
          this.notificationService.notify('license.form.notification.saveError', NotificationType.ERROR);
        }
      });
  }

  createInstance(): void {
    if (this.editMode && this.editLicenseId && !this.instanceRequestInProgress) {
      this.instanceRequestInProgress = true;
      this.licenseService.createInstance(this.editLicenseId)
        .subscribe((instanceIdDto: InstanceIdDto) => {
          this.instanceRequestInProgress = false;
          this.notificationService.notify('license.form.notification.instanceCreated', NotificationType.SUCCESS);
          this.licenseForEdit.instanceId = instanceIdDto.instanceId;
          this.licenseForEdit.localInstance = false;
          this.instanceId.patchValue(instanceIdDto.instanceId);
          this.instanceId.disable();
        }, (err) => {
          this.instanceRequestInProgress = false;
          if (err.status === 409) {
            this.notificationService.notify('license.form.notification.domainOrEmailExist', NotificationType.ERROR);
          } else {
            this.notificationService.notify('license.form.notification.saveError', NotificationType.ERROR);
          }
        });
    }
  }

  removeInstance(): void {
    if (this.editMode && this.editLicenseId && !this.instanceRequestInProgress) {
      this.instanceRequestInProgress = true;
      this.licenseService.removeInstance(this.editLicenseId)
        .subscribe(() => {
          this.instanceRequestInProgress = false;
          this.notificationService.notify('license.form.notification.instanceRemoved', NotificationType.SUCCESS);
          this.licenseForEdit.instanceId = undefined;
          this.licenseForEdit.localInstance = true;
          this.instanceId.patchValue(undefined);
          this.instanceId.enable();
        }, () => {
          this.instanceRequestInProgress = false;
          this.notificationService.notify('license.form.notification.saveError', NotificationType.ERROR);
        });
    }
  }

  cancel(): void {
    this.updateForm();
    FormUtil.setUntouched(this.licenseForm);
  }

  openPaymentForm(): void {
    this.paymentForm.open();
  }

  closePaymentForm(): void {
    this.paymentForm.close();
  }

  private getLicenseForm(): UpdateLicenseDto {
    return {
      name: this.name.value,
      phone: this.phone.value,
      mail: this.email.value,
      domain: this.domain.value,
      instanceId: this.instanceId.value
    };
  }

  private updateForm(): void {
    (this.editMode) ? this.setLicenseForEdit() : this.setForm();
  }

  private setForm(name?: string, phone?: string, email?: string, instanceId?: string, domain?: string): void {
    this.name.patchValue(name);
    this.phone.patchValue(phone);
    this.email.patchValue(email);
    this.instanceId.patchValue(instanceId);
    (!this.editMode || this.licenseForEdit.localInstance) ? this.instanceId.enable() : this.instanceId.disable();
    this.domain.patchValue(domain);
  }

  private setLicenseForEdit(): void {
    if (this.editMode && this.editLicenseId) {
      this.licenseService.getLicense(this.editLicenseId)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((license: License) => {
          this.setLicenseToForm(license);
        });
    }
  }

  setLicenseToForm(license: License): void {
    this.licenseForEdit = license;
    this.setForm(license.name, license.phone, license.mail, license.instanceId, license.domain);
    this.updateSelectedPayment();
  }

  private createForm(): void {
    this.licenseForm = this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      phone: ['', Validators.compose([Validators.required])],
      domain: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      instanceId: [null]
    });
  }

  private updateSelectedPayment() {
    let newPayment: Payment;
    if (this.selectedPayment && this.licenseForEdit && this.licenseForEdit.payments) {
      newPayment = this.licenseForEdit.payments.find(c => c.id === this.selectedPayment.id);
    }
    this.selectedPayment = undefined;
    this.selectedPayment = newPayment;
  }
}
