import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {License, LicenseTypeEnum, NewPaymentDto, Payment, UpdatePaymentDto} from '../../license.model';
import {Subject} from 'rxjs';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LicenseService} from '../../license.service';
import {takeUntil} from 'rxjs/operators';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {FormUtil} from '../../../shared/util/form.util';
// @ts-ignore
import {DateUtil} from '../../../shared/util/date.util';

@Component({
  selector: 'app-payment-form',
  templateUrl: './payment-form.component.html',
  styleUrls: ['./payment-form.component.scss']
})
export class PaymentFormComponent implements OnInit, OnChanges, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();

  paymentForm: FormGroup;
  @Input() licenseId: number;
  @Input() payment: Payment;
  @Output() licenseChanged: EventEmitter<License> = new EventEmitter<License>();
  @Output() cancelEvent: EventEmitter<any> = new EventEmitter<any>();

  licenseTypeEnum = LicenseTypeEnum;

  constructor(private fb: FormBuilder,
              private notificationService: NotificationService,
              private licenseService: LicenseService) {
    this.createForm();
  }

  // @formatter:off
  get paymentDate(): AbstractControl {return this.paymentForm.get('paymentDate'); }
  get licenseType(): AbstractControl {return this.paymentForm.get('licenseType'); }
  get numberOfDays(): AbstractControl {return this.paymentForm.get('numberOfDays'); }
  get numberOfUsers(): AbstractControl {return this.paymentForm.get('numberOfUsers'); }
  get expiredDate(): AbstractControl {return this.paymentForm.get('expiredDate'); }
  // @formatter:on

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.payment != null) {
      this.setForm();
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  save(): void {
    FormUtil.setTouched(this.paymentForm);
    if (this.paymentForm.valid) {
      (this.payment == null) ? this.addPayment() : this.editPayment();
    }
  }

  editPayment(): void {
    this.licenseService.updatePayment(this.payment.id, this.getPayment())
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((license: License) => {
        this.licenseChanged.next(license);
        this.notificationService.notify('license.form.paymentForm.notification.paymentModified', NotificationType.SUCCESS);
      }, () => this.notificationService.notify('license.form.paymentForm.notification.saveError', NotificationType.ERROR));
  }

  addPayment(): void {
    const newPayment: NewPaymentDto = this.getPayment();
    newPayment.licenseId = this.licenseId;
    this.licenseService.addPayment(newPayment)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((license: License) => {
        this.licenseChanged.next(license);
        this.setForm();
        this.notificationService.notify('license.form.paymentForm.notification.paymentAdded', NotificationType.SUCCESS);
      }, () => this.notificationService.notify('license.form.paymentForm.notification.saveError', NotificationType.ERROR));
  }

  private getPayment(): UpdatePaymentDto {
    return {
      date: DateUtil.getDateToSend(this.paymentDate.value),
      numberOfDays: this.numberOfDays.value,
      numberOfUsers: this.numberOfUsers.value,
      licenseType: this.licenseType.value
    };
  }

  cancel(): void {
    this.setForm();
    this.cancelEvent.next();
  }

  private setForm(): void {
    if (this.payment) {
      this.paymentDate.patchValue(this.payment.paymentDate);
      this.licenseType.patchValue(this.payment.licenseType);
      this.numberOfDays.patchValue(this.payment.numberOfDays);
      this.numberOfUsers.patchValue(this.payment.numberOfUsers);
      this.expiredDate.patchValue(this.payment.expiredDate);
    } else {
      this.createForm();
    }
  }

  private createForm(): void {
    this.paymentForm = this.fb.group({
      paymentDate: ['', Validators.compose([Validators.required])],
      licenseType: ['', Validators.compose([Validators.required])],
      numberOfDays: ['', Validators.compose([Validators.required])],
      numberOfUsers: ['', Validators.compose([Validators.required])],
      expiredDate: ['', Validators.compose([Validators.required])]
    });
    this.expiredDate.disable();
  }
}
