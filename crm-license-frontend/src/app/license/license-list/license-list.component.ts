import {Component, OnDestroy, OnInit} from '@angular/core';
import {LicenseService} from '../license.service';
import {LoginService} from '../../login/login.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {LicenseList} from '../license.model';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-instance-list',
  templateUrl: './license-list.component.html',
  styleUrls: ['./license-list.component.scss']
})
export class LicenseListComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();
  licenses: LicenseList[] = [];
  licenseSubject: Subject<LicenseList[]> = new Subject<LicenseList[]>();

  columns: string[] = ['name', 'phone', 'email', 'lastPayment', 'type', 'instanceId', 'expirationDate', 'locked', 'domain', 'action'];

  constructor(private loginService: LoginService,
              private $router: Router,
              private notificationService: NotificationService,
              private licenseService: LicenseService) {
  }

  ngOnInit() {
    this.getLicenses();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  getLicenses(): void {
    this.licenseService.getLicenseList()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((licenses: LicenseList[]) => {
        this.licenses = licenses;
        this.licenseSubject.next(this.licenses);
      }, () => this.notificationService.notify('license.list.notification.getLicensesError', NotificationType.ERROR));
  }

  goToLicense(id: number): void {
    this.$router.navigate(['/form', id]);
  }

  newLicense(): void {
    this.$router.navigate(['/form']);
  }
}
