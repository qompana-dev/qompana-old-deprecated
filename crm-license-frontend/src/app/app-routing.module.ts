import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {LicenseListComponent} from './license/license-list/license-list.component';
import {LicenseFormComponent} from './license/license-form/license-form.component';
import {RegisterComponent} from './register/register.component';
import {SuccessfulComponent} from './register/successful/successful.component';


const routes: Routes = [
  {path: '', redirectTo: '/register', pathMatch: 'full'},
  {path: 'instances', component: LicenseListComponent},
  {path: 'form/:id', component: LicenseFormComponent},
  {path: 'form', component: LicenseFormComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'register/successful', component: SuccessfulComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
