import {FormGroup} from '@angular/forms';

export interface SaveForm {
  save(): void;
  cancel(): void;
}


export class FormUtil {
  static setTouched(formGroup: FormGroup): void {
    if (formGroup) {
      formGroup.markAsTouched();
      (Object as any).values(formGroup.controls).forEach(control => control.markAsTouched());
    }
  }
  static setUntouched(formGroup: FormGroup): void {
    if (formGroup) {
      formGroup.markAsUntouched();
      (Object as any).values(formGroup.controls).forEach(control => control.markAsUntouched());
    }
  }
  static updateValidity(formGroup: FormGroup): void {
    if (formGroup) {
      (Object as any).values(formGroup.controls).forEach(control => control.updateValueAndValidity());
    }
  }
}
