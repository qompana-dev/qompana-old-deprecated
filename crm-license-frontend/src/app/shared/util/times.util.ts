// @ts-ignore
import moment, {Moment} from 'moment';

export class TimesUtil {
  static fromUTCToCurrentTimeZone(utcTime: string): Moment {
    if (utcTime == null) {
      return undefined;
    }
    console.log('moment', moment, utcTime);
    console.log('moment', moment.utc(utcTime));
    return moment.utc(utcTime).locale(localStorage.getItem('locale'));
  }
}
