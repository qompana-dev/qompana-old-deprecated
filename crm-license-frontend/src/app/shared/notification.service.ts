import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {MatSnackBar} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private translateService: TranslateService,
              private snackBar: MatSnackBar) {
  }

  notify(key: string, type: NotificationType, params?: any): void {
    this.translateService.get(key, params)
      .subscribe((text) => this.showToast(text, NotificationType[type]));
  }

  notifyError(errorTypes: ErrorTypes, errorCode: number): void {
    let notified = false;
    for (const errorType of errorTypes.errors) {
      if (errorType.code === errorCode) {
        this.translateService.get(errorTypes.base + errorType.text).subscribe(
          ((text) => this.showToast(text, 'error'))
        );
        notified = true;
        break;
      }
    }
    if (!notified) {
      this.translateService.get(errorTypes.base + errorTypes.defaultText).subscribe(
        (text) => this.showToast(text, 'error'));
    }
  }

  private showToast(text: string, type: string): void {
    this.snackBar.open(text, undefined, {
      duration: environment.toastTime,
      panelClass: [`notification-${type.toLowerCase()}`]
    });
  }
}

export enum NotificationType {
  SUCCESS, ERROR
}

export interface ErrorTypes {
  base: string;
  errors: ErrorType[];
  defaultText: string;
}

export interface ErrorType {
  code: number;
  text: string;
}
