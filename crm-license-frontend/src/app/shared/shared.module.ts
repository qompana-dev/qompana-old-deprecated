import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialDesignModule} from './material-design.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {FormatDatePipe} from './pipe/formatDate.pipe';
import {SliderComponent} from './slider/slider.component';


@NgModule({
  declarations: [
    FormatDatePipe,
    SliderComponent
  ],
  imports: [
    CommonModule,
    MaterialDesignModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    MaterialDesignModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    FormatDatePipe,
    SliderComponent
  ]
})
export class SharedModule { }
