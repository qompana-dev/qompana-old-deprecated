import {MatDateFormats} from '@angular/material';

export const crmDateFormat = 'D/MM/YYYY';
export const crmTimeFormat = 'HH:mm';
export const britishCrmTimeFormat = 'hh:mm A';
export const crmDateTimeFormat = 'D/MM/YYYY, HH:mm';

export const CRM_MOMENT_DATE_FORMATS: MatDateFormats = {
  parse: {
    dateInput: crmDateFormat
  },
  display: {
    dateInput: crmDateFormat,
    monthYearLabel: 'MMMM Y',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM Y'
  }
};
