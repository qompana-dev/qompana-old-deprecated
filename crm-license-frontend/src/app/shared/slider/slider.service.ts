import {Injectable, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SliderService implements OnDestroy {
  // slider -> component
  sliderStateChanged = new Subject<SliderStateChange>();
  sliderWidthChanged = new Subject<SliderWidth>();
  moreInfoClicked = new Subject<string>();
  closeClicked = new Subject<string>();


  // component -> slider
  closeSliderSubject = new Subject<void>();
  changeSliderWidthSubject = new Subject<SliderWidth>();
  changeMoreInfoVisibleSubject = new Subject<SliderMoreInfo>();

  // slider -> slider
  sliderInGroupChanged = new Subject<SliderGroupInfo>();

  constructor() { }

  ngOnDestroy(): void {
    this.sliderStateChanged.complete();
    this.closeSliderSubject.complete();
  }

  stateChanged(key: string, opened: boolean): void {
    this.sliderStateChanged.next({key, opened});
  }

  changeWidth(key: string, width: string): void {
    this.changeSliderWidthSubject.next({key, width});
  }

  changeMoreInfoVisible(key: string, visible: boolean): void {
    this.changeMoreInfoVisibleSubject.next({key, visible});
  }

  closeSlider(): void {
    this.closeSliderSubject.next();
  }
}

export class SliderStateChange {
  key: string;
  opened: boolean;
}

export class SliderWidth {
  key: string;
  width: string;
}

export class SliderMoreInfo {
  key: string;
  visible: boolean;
}
export class SliderGroupInfo {
  group: string;
  key: string;
  open: boolean;
  width: string;
}

