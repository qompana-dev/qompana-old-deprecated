import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {NotificationService, NotificationType} from '../shared/notification.service';
import {takeUntil} from 'rxjs/operators';
import {CreateLicenseDto, License, UpdateLicenseDto} from '../license/license.model';
import {FormUtil} from '../shared/util/form.util';
import {LicenseService} from '../license/license.service';
import {LoginService} from '../login/login.service';
import {LocalStorageModel} from '../shared/local-storage.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();

  createLicenseDto: UpdateLicenseDto;

  instanceRequestInProgress = false;

  registerForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    phone: new FormControl('', [Validators.required]),
    domain: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
  });

  constructor(private $router: Router,
              private loginService: LoginService,
              private licenseService: LicenseService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    if (sessionStorage.getItem(LocalStorageModel.BASIC_AUTH)) {
      this.loginService.logout()
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => this.loginService.loginStatusChanged.next());
    }
    this.createLicenseDto = {} as CreateLicenseDto;
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  register(): void {
    FormUtil.setTouched(this.registerForm);
    if (this.registerForm.valid && !this.instanceRequestInProgress) {
      this.instanceRequestInProgress = true;
      this.licenseService.register(this.createLicenseDto, true)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((license: License) => {
          this.instanceRequestInProgress = false;
          this.notificationService.notify('register.form.notification.createdSuccessfully', NotificationType.SUCCESS);
          this.$router.navigate(['/register', 'successful']);
        }, (err) => {
          this.instanceRequestInProgress = false;
          if (err.status === 409) {
            this.notificationService.notify('register.form.notification.domainOrEmailExist', NotificationType.ERROR);
          } else {
            this.notificationService.notify('register.form.notification.saveError', NotificationType.ERROR);
          }
        });
    }
  }

  goToLogin(): void {
    this.$router.navigate(['/login']);
  }
}
