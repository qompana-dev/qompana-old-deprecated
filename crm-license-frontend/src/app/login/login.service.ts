import {Injectable} from '@angular/core';
import {Credentials} from './login.component';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../environments/environment';
import {LocalStorageModel} from '../shared/local-storage.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  loginStatusChanged: Subject<void> = new Subject<void>();

  constructor(private http: HttpClient) { }

  isLoggedIn(): boolean {
    return sessionStorage.getItem(LocalStorageModel.BASIC_AUTH) != null;
  }

  login(credential: Credentials): Observable<string> {
    const toLogin = 'Basic ' + btoa( `${credential.username}:${credential.password}`);
    return this.http.post<void>(`${environment.url}/login`, null, {headers: {Authorization: toLogin}})
      .pipe(map(c => toLogin));
  }

  logout(): Observable<void> {
    sessionStorage.removeItem(LocalStorageModel.BASIC_AUTH);
    return this.http.post<void>(`${environment.url}/logout`, undefined);
  }
}
