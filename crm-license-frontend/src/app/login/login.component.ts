import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {LoginService} from './login.service';
import {NotificationService, NotificationType} from '../shared/notification.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {LocalStorageModel} from '../shared/local-storage.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();

  credentials: Credentials;


  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(private loginService: LoginService,
              private translateService: TranslateService,
              private $router: Router,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    if (this.loginService.isLoggedIn()) {
      this.$router.navigate(['']);
    }
    this.credentials = new Credentials();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  login(): void {
    if (this.loginForm.valid) {
      this.loginService.login(this.credentials)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((auth) => {
          sessionStorage.setItem(LocalStorageModel.BASIC_AUTH, auth);
          this.loginService.loginStatusChanged.next();
          this.$router.navigate(['/instances']);
        }, () => {
          this.notificationService.notify('login.notification.loginFailed', NotificationType.ERROR);
        });
    }
  }
}

export class Credentials {
  username: string;
  password: string;
}
