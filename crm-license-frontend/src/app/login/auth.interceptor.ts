import {Injectable} from '@angular/core';

import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {LocalStorageModel} from '../shared/local-storage.model';
import {Router} from '@angular/router';
import {LoginService} from './login.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private $router: Router,
              private loginService: LoginService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url !== `${environment.url}/login`) {
      return this.logoutIf401(next.handle(this.addToHeaders(req, sessionStorage.getItem(LocalStorageModel.BASIC_AUTH))));
    }
    return next.handle(req);
  }

  private logoutIf401(observable: Observable<any>): Observable<any> {
    return observable.pipe(catchError(err => {
      if (err.status === 401) {
        sessionStorage.removeItem(LocalStorageModel.BASIC_AUTH);
        this.loginService.loginStatusChanged.next();
        this.$router.navigate(['/']);
      }
      return throwError(err);
    }));
  }

  private addToHeaders(req: HttpRequest<any>, auth: string): any {
    if (auth == null) {
      return req;
    }
    const request = req.clone({
      headers: req.headers.set('Authorization', auth)
    });
    return request;
  }
}
