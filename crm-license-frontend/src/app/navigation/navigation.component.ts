import {LoginService} from '../login/login.service';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';
import {NotificationService, NotificationType} from '../shared/notification.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, OnDestroy {
  navUserName = '';
  navUserRole = '';
  navUserAvatar = '';

  search = false;

  private componentDestroyed: Subject<void> = new Subject();

  constructor(private $router: Router,
              private notificationService: NotificationService,
              private translateService: TranslateService,
              private loginService: LoginService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  logout(): void {
    this.loginService.logout()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.notificationService.notify('login.notification.logoutSuccessful', NotificationType.SUCCESS);
        this.loginService.loginStatusChanged.next();
        this.$router.navigate(['/login']);
      });
  }

  goToMainPage(): void {
    this.$router.navigate(['/']);
  }
}
