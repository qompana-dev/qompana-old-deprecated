#!/bin/bash
if [ "$1" != "" ]; then
    echo "Starting Docker script for container: $1"
else
    echo "Script need be run with one argument as container name"
    exit -1
fi

if [ "$2" != "" ]; then
    echo "Starting Postgres script for container: $2"
    docker cp src/main/resources/schema.sql $2:/schema.sql
    docker cp src/main/resources/data.sql $2:/data.sql
    docker exec $2 psql -U postgres -d postgres -c "DROP DATABASE $3"
    docker exec $2 psql -u postgres psql $3 postgres -f /schema.sql
    docker exec $2 psql -u postgres psql $3 postgres -f /data.sql

    echo "End postgres script"
else
    echo "No second argument was given as database container name"
fi

echo `hostname`
echo "Stopping container $1"
docker stop $1
echo "Removing old container $1"
docker rm $1 -f
echo "Cleaning untaged images"
docker rmi $(docker images --filter "dangling=true" -q --no-trunc) -f
echo "Script finish"
