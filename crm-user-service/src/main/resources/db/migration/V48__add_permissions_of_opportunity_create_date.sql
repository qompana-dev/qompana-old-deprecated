insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.createDateField', 'permissions.names.opportunityCreateCreateDateField', 2, 1, 0, false, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.createDateField', 'permissions.names.opportunityEditCreateDateField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.createDate', 'permissions.names.opportunityDetailsCardComponentCreateDate', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.createDate', 'permissions.names.opportunityDetailsCreateDate', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

update web_control set frontend_id = 'OpportunityListComponent.createDateColumn' where frontend_id = 'OpportunityListComponent.createdColumn';


