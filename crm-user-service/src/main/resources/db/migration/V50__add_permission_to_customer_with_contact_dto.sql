insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.nip', 'permissions.names.customerDetailsComponentNip', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.activeClient', 'permissions.names.customerDetailsComponentActiveClient', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.activeDelivery', 'permissions.names.customerDetailsComponentActiveDelivery', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.contacts', 'permissions.names.customerDetailsComponentContacts', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.employeesNumber', 'permissions.names.customerDetailsComponentEmployeesNumber', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.companyType', 'permissions.names.customerDetailsComponentCompanyType', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.income', 'permissions.names.customerDetailsComponentIncome', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.foreignCompany', 'permissions.names.customerDetailsComponentForeignCompany', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);




