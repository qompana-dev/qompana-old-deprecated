TRUNCATE dashboard_widget;

ALTER TABLE dashboard_widget
    RENAME row TO rows;

ALTER TABLE dashboard_widget
    RENAME "column" TO cols;

alter table dashboard_widget
    add column x int8 NOT NULL default 0;
alter table dashboard_widget
    add column y int8 NOT NULL default 0;