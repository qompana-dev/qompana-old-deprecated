insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactDetailsComponent.attachedEmails', 'permissions.names.contactDetailsAttachedEmails', 1, 0, 0, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactDetailsComponent.detachEmail', 'permissions.names.contactDetailsDetachEmail', 2, 3, 1, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 2);

