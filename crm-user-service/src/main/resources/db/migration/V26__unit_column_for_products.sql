insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListInPriceBookComponent.unit', 'permissions.names.productListInPriceBookComponentUnit', 2, 0, 0, false, -21);

insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -21, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListComponent.unit', 'permissions.names.productListComponentUnit', 2, 0, 0, false, -15);

insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -15, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'AddProductFromPriceBookComponent.unit', 'permissions.names.addProductFromPriceBookComponentUnit', 2, 0, 0, false, -33);

insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

