insert into module(id, name, module_category_id) values (-46, 'permissions.module.customerOpportunities', -2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values
(nextval('web_control_seq'), 'CustomerOpportunity.view', 'permissions.names.relatedOpportunityInCustomerDetailView', 2, 0, 0, false, -46),
(nextval('web_control_seq'), 'CustomerOpportunity.save', 'permissions.names.relatedOpportunityInCustomerDetailAdd', 2, 1, 1, false, -46),
(nextval('web_control_seq'), 'CustomerOpportunity.edit', 'permissions.names.relatedOpportunityInCustomerDetailEdit', 2, 2, 1, false, -46),
(nextval('web_control_seq'), 'CustomerOpportunity.delete', 'permissions.names.relatedOpportunityInCustomerDetailDelete', 2, 3, 1, false, -46),
(nextval('web_control_seq'), 'CustomerOpportunity.name', 'permissions.names.relatedOpportunityInCustomerDetailName', 1, 0, 0, false, -46),
(nextval('web_control_seq'), 'CustomerOpportunity.status', 'permissions.names.relatedOpportunityInCustomerDetailStatus', 1, 0, 0, false, -46),
(nextval('web_control_seq'), 'CustomerOpportunity.amount', 'permissions.names.relatedOpportunityInCustomerDetailAmount', 1, 0, 0, false, -46),
(nextval('web_control_seq'), 'CustomerOpportunity.finishDate', 'permissions.names.relatedOpportunityInCustomerDetailFinishDate', 1, 0, 0, false, -46)
;
