INSERT INTO web_control (id, frontend_id, module_id, name, default_state, depends_on, type, required)
VALUES (nextval('web_control_seq'), 'LeadShared.changeKeeper', -10, 'permissions.names.leadChangeKeeper', 2, 2, 1, false);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -10, currval('web_control_seq'), 2);