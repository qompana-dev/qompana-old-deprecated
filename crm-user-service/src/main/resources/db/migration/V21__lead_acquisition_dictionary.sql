INSERT INTO module (id, module_category_id, name) VALUES (-47, -13, 'permissions.module.dictionaries');


INSERT INTO module_permission (id, module_id, profile_id, view, "create", edit, delete) VALUES (-47, -47, -1, true, true, true, true);


INSERT INTO web_control (id, frontend_id, module_id, name, default_state, depends_on, type, required)
VALUES (nextval('web_control_seq'), 'LeadShared.addSourceOfAcquisition', -10, 'permissions.names.leadAddSourceOfAcquisition', 2, 1, 1, false);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -10, currval('web_control_seq'), 2);

INSERT INTO web_control (id, frontend_id, module_id, name, default_state, depends_on, type, required)
VALUES (nextval('web_control_seq'), 'OpportunityShared.addSourceOfAcquisition', -27, 'permissions.names.opportunityAddSourceOfAcquisition', 2, 1, 1, false);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -27, currval('web_control_seq'), 2);

INSERT INTO web_control (id, frontend_id, module_id, name, default_state, depends_on, type, required)
VALUES (nextval('web_control_seq'), 'NavigationComponent.dictionaries.management', -47, 'permissions.names.dictionariesManagement', 2, 0, 1, false);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -47, currval('web_control_seq'), 2);
