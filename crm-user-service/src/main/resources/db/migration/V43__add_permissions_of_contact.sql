insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactListComponent.addressColumn', 'permissions.names.contactListAddressColumn', 2, 0, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactAddComponent.addressField', 'permissions.names.contactAddAddressField', 2, 1, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.addressField', 'permissions.names.contactEditAddressField', 2, 2, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactInContactInfoComponent.addressField', 'permissions.names.contactInContactInfoAddressField', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.addressField', 'permissions.names.contactEditAddressField', 2, 2, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.addressField', 'permissions.names.contactEditAddressField', 2, 2, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerListComponent.firstToContactAddressColumn', 'permissions.names.customerListFirstContactAddressColumn', 1, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 1);
