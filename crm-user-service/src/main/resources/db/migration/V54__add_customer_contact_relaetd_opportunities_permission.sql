insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'RelatedOpportunities.addOpportunity', 'permissions.names.relatedOpportunitiesAddOpportunity', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'RelatedOpportunities.editOpportunity', 'permissions.names.relatedOpportunitiesEditOpportunity', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'RelatedOpportunities.deleteOpportunity', 'permissions.names.relatedOpportunitiesDeleteOpportunity', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);