update web_control set frontend_id = 'PersonListComponent.role.nameColumn' where name = 'permissions.names.userListRoleName';

update web_control set frontend_id = 'DocumentsListComponent.authorFullName' where frontend_id = 'DocumentsListComponent.addedBy';
update web_control set frontend_id = 'DocumentsEditComponent.authorFullName' where frontend_id = 'DocumentsEditComponent.addedBy';
update web_control set frontend_id = 'DocumentsListInLeadComponent.authorFullName' where frontend_id = 'DocumentsListInLeadComponent.addedBy';
update web_control set frontend_id = 'DocumentsEditInLeadComponent.authorFullName' where frontend_id = 'DocumentsEditInLeadComponent.addedBy';
update web_control set frontend_id = 'DocumentsListInOpportunityComponent.authorFullName' where frontend_id = 'DocumentsListInOpportunityComponent.addedBy';
update web_control set frontend_id = 'DocumentsEditInOpportunityComponent.authorFullName' where frontend_id = 'DocumentsEditInOpportunityComponent.addedBy';
