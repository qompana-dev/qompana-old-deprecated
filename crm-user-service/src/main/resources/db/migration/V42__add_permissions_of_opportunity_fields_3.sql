insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.processDefinitionField', 'permissions.names.opportunityDetailsCardComponentProcessDefinitionField', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);
