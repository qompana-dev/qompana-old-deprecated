insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.priority', 'permissions.names.opportunityDetailsPriority', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.priorityField', 'permissions.names.opportunityDetailsCardComponentPriority', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.ownerField', 'permissions.names.opportunityDetailsCardComponentOwner', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.estimatedAmount', 'permissions.names.opportunityDetailsCardEstimatedAmount', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.productAmount', 'permissions.names.opportunityDetailsCardProductAmount', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.profitAmount', 'permissions.names.opportunityDetailsCardProfitAmount', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

INSERT INTO web_control (id, frontend_id, module_id, name, default_state, depends_on, type, required)
VALUES (nextval('web_control_seq'), 'OpportunityShared.addSubjectOfInterest', -27, 'permissions.names.opportunityAddSubjectOfInterest', 2, 1, 1, false);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -27, currval('web_control_seq'), 2);