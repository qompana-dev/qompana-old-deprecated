update web_control
set frontend_id = 'CustomerAddComponent.industry',
    name        = 'permissions.names.customerAddIndustry'
where frontend_id = 'CustomerAddComponent.companyType';

update web_control
set frontend_id = 'CustomerEditComponent.industry',
    name        = 'permissions.names.customerEditIndustry'
where frontend_id = 'CustomerEditComponent.companyType';

update web_control
set frontend_id = 'CustomerDetailsComponent.industry',
    name        = 'permissions.names.customerDetailsComponentIndustry'
where frontend_id = 'CustomerDetailsComponent.companyType';

