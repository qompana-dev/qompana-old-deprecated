insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListComponent.addFile', 'permissions.names.documentsListComponentAddFile', 2, 1, 1, false, -12);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListComponent.view', 'permissions.names.documentsListComponentView', 1, 0, 0, false, -12);
