insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityListComponent.priorityColumn', 'permissions.names.opportunityListPriorityColumn', 2, 0, 0, false, -26);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -26, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityListComponent.avatarColumn', 'permissions.names.opportunityListAvatarColumn', 2, 0, 0, false, -26);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -26, currval('web_control_seq'), 2);
