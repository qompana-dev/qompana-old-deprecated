alter table dashboard_widget drop constraint column_check;
alter table dashboard_widget drop constraint row_check;
alter table dashboard_widget drop constraint dashboard_widget_account_id_row_column_key;

alter table dashboard_widget add constraint cols_check check (cols >= 1 and cols < 13);
alter table dashboard_widget add constraint rows_check check (rows >= 1 and rows < 13);
alter table dashboard_widget add constraint x_check check (x >= 0 and x < 12);
alter table dashboard_widget add constraint y_check check (y >= 0 and y < 12);
alter table dashboard_widget add constraint unique_account_widget UNIQUE (account_id, cols, rows, x, y);
