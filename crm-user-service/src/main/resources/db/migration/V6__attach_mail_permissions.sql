
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.attachedEmails', 'permissions.names.leadDetailsAttachedEmails', 1, 0, 0, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.detachEmail', 'permissions.names.leadDetailsDetachEmail', 2, 3, 1, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);



insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.attachedEmails', 'permissions.names.opportunityDetailsAttachedEmails', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.detachEmail', 'permissions.names.opportunityDetailsDetachEmail', 2, 3, 1, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);
