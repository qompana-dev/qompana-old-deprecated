update web_control
set frontend_id = 'LeadEditComponent.priorityField'
where frontend_id = 'LeadEditComponent.statusField';
update web_control
set name = 'permissions.names.leadEditPriorityField'
where frontend_id = 'LeadEditComponent.priorityField';

update web_control
set frontend_id = 'LeadCreateComponent.priorityField'
where frontend_id = 'LeadCreateComponent.statusField';
update web_control
set name = 'permissions.names.leadCreatePriorityField'
where frontend_id = 'LeadCreateComponent.priorityField';

update web_control
set frontend_id = 'OpportunityEditComponent.priorityField'
where frontend_id = 'OpportunityEditComponent.typeField';
update web_control
set name = 'permissions.names.opportunityEditPriorityField'
where frontend_id = 'OpportunityEditComponent.priorityField';

update web_control
set frontend_id = 'OpportunityCreateComponent.priorityField'
where frontend_id = 'OpportunityCreateComponent.typeField';
update web_control
set name = 'permissions.names.opportunityCreatePriorityField'
where frontend_id = 'OpportunityCreateComponent.priorityField';
