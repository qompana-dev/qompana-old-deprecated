
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadFilesMiniComponent.delete', 'permissions.names.leadMiniFilesDelete', 2, 3, 1, false, -24);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -24, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadFilesMiniComponent.preview', 'permissions.names.leadMiniFilesPreview', 2, 0, 1, false, -24);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -24, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityFilesMiniComponent.delete', 'permissions.names.opportunityMiniFilesDelete', 2, 3, 1, false, -30);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -30, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityFilesMiniComponent.preview', 'permissions.names.opportunityMiniFilesPreview', 2, 0, 1, false, -30);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -30, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListComponent.delete', 'permissions.names.documentsListComponentDelete', 2, 3, 1, false, -30);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListComponent.preview', 'permissions.names.documentsListComponentPreview', 2, 0, 1, false, -30);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);


INSERT INTO module (id, module_category_id, name) VALUES (-48, -7, 'permissions.module.productMiniFiles');
INSERT INTO module_permission (id, module_id, profile_id, view, "create", edit, delete) VALUES (-48, -48, -1, true, true, true, true);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductFilesMiniComponent.delete', 'permissions.names.productMiniFilesDelete', 2, 3, 1, false, -48);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -48, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductFilesMiniComponent.preview', 'permissions.names.productMiniFilesPreview', 2, 0, 1, false, -48);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -48, currval('web_control_seq'), 2);

update web_control
set module_id = -48
where frontend_id in (
     'ProductFilesMiniComponent.addFile',
     'ProductFilesMiniComponent.showMore',
     'ProductFilesMiniComponent.view'
    );
