INSERT INTO web_control (id, frontend_id, module_id, name, default_state, depends_on, type, required)
VALUES (nextval('web_control_seq'), 'ProductEditComponent.unit', -17, 'permissions.names.productEditComponentUnit', 2, 2, 0, false);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

INSERT INTO web_control (id, frontend_id, module_id, name, default_state, depends_on, type, required)
VALUES (nextval('web_control_seq'), 'ProductAddComponent.unit', -16, 'permissions.names.productAddComponentUnit', 2, 1, 0, false);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

INSERT INTO web_control (id, frontend_id, module_id, name, default_state, depends_on, type, required)
VALUES (nextval('web_control_seq'), 'OpportunityProductsCard.unit', -33, 'permissions.names.opportunityProductsUnit', 2, 1, 0, true);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);
