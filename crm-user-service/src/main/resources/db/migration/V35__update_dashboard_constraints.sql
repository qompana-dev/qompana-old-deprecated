alter table dashboard_widget drop constraint cols_check;
alter table dashboard_widget drop constraint rows_check;
alter table dashboard_widget drop constraint x_check;
alter table dashboard_widget drop constraint y_check;
alter table dashboard_widget drop constraint unique_account_widget;

alter table dashboard_widget add constraint cols_check check (cols >= 2 and cols < 25);
alter table dashboard_widget add constraint rows_check check (rows >= 2 and rows < 25);
alter table dashboard_widget add constraint x_check check (x >= 0 and x < 24);
alter table dashboard_widget add constraint y_check check (y >= 0 and y < 24);
alter table dashboard_widget add constraint unique_account_widget UNIQUE (account_id, cols, rows, x, y);
