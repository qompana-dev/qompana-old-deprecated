insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerListComponent.archive', 'permissions.names.customerListArchive', 2, 3, 1, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactListComponent.archive', 'permissions.names.contactListArchive', 2, 3, 1, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);
