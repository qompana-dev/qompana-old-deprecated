delete
from account_configuration
where account_id < 0
  and account_id > -8
  and (select count(p) from person as p) = 8;
delete
from dashboard_widget
where account_id < 0
  and account_id > -8
  and (select count(p) from person as p) = 8;
delete
from token
where account_id < 0
  and account_id > -8
  and (select count(p) from person as p) = 8;
delete
from account2team
where account_id < 0
  and account_id > -8
  and (select count(p) from person as p) = 8;
delete
from account
where id < 0
  and id > -8
  and (select count(p) from person as p) = 8;
delete
from person
where id < 0
  and id > -8
  and (select count(p) from person as p) = 8;
