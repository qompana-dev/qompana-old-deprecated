create TABLE role
(
    id   bigint PRIMARY KEY,
    name VARCHAR(100) UNIQUE NOT NULL
);

create sequence role_seq start 1;

create TABLE role_children
(
    parent_id bigint NOT NULL,
    child_id  bigint NOT NULL,
    CONSTRAINT role_children_parent_fk FOREIGN KEY (parent_id)
        REFERENCES role (id) ON delete CASCADE,
    CONSTRAINT role_children_child_fk FOREIGN KEY (child_id)
        REFERENCES role (id) ON delete CASCADE
);


create TABLE profile
(
    id          bigint PRIMARY KEY,
    name        VARCHAR(100) UNIQUE NOT NULL,
    description VARCHAR(500)
);

create sequence profile_seq start 1;

create TABLE profile2role
(
    profile_id bigint NOT NULL,
    role_id    bigint NOT NULL,
    CONSTRAINT profile2role_role_fk FOREIGN KEY (role_id)
        REFERENCES role (id) ON delete CASCADE,
    CONSTRAINT profile2role_profile_fk FOREIGN KEY (profile_id)
        REFERENCES profile (id) ON delete CASCADE
);

create table module_category
(
    id   bigint primary key,
    name VARCHAR(200)
);

create sequence module_category_seq start 1;

create table module
(
    id                 bigint primary key,
    module_category_id bigint not null,
    name               VARCHAR(200),
    CONSTRAINT module_module_category_id FOREIGN KEY (module_category_id)
        REFERENCES module_category (id)
);

create sequence module_seq start 1;

create table module_permission
(
    id         bigint PRIMARY KEY,
    module_id  bigint  NOT NULL,
    profile_id bigint  NOT NULL,
    "view"     boolean NOT NULL,
    "create"   boolean NOT NULL,
    "edit"     boolean NOT NULL,
    "delete"   boolean NOT NULL,
    CONSTRAINT module_permission_module_id FOREIGN KEY (module_id)
        REFERENCES module (id),
    CONSTRAINT module_permission_profile_id FOREIGN KEY (profile_id)
        REFERENCES profile (id)
);

create sequence module_permission_seq start 1;

create TABLE web_control
(
    id            bigint PRIMARY KEY,
    frontend_id   VARCHAR(200) NOT NULL,
    module_id     bigint       not null,
    name          VARCHAR(200) NOT NULL,
    default_state int8         NOT NULL,
    depends_on    int8         NOT NULL,
    type          int8         NOT NULL,
    required      boolean      NOT NULL,
    CONSTRAINT web_control_module_fk FOREIGN KEY (module_id)
        REFERENCES module (id)
);

COMMENT ON COLUMN web_control.default_state IS '0 - INVISIBLE; 1 - READ_ONLY; 2 - WRITE;';
COMMENT ON COLUMN web_control.depends_on IS '0 - VIEW; 1 - CREATE; 2 - EDIT; 3 - DELETE;';
COMMENT ON COLUMN web_control.type IS '0 - FIELD; 1 - TOOL';


create sequence web_control_seq start 1;

create TABLE permission
(
    id                   bigint PRIMARY KEY,
    module_permission_id bigint NOT NULL,
    web_control_id       bigint NOT NULL,
    state                int8   NOT NULL,
    CONSTRAINT permission_module_permission_id FOREIGN KEY (module_permission_id)
        REFERENCES module_permission (id),
    CONSTRAINT permission_web_control_id FOREIGN KEY (web_control_id)
        REFERENCES web_control (id)
);

COMMENT ON COLUMN permission.state IS '0 - INVISIBLE; 1 - READ_ONLY; 2 - WRITE;';

create sequence permission_seq start 1;

create TABLE person
(
    id              bigint PRIMARY KEY,
    name            VARCHAR(200)        NOT NULL,
    surname         VARCHAR(200)        NOT NULL,
    description     VARCHAR(500),
    phone           VARCHAR(11),
    email           VARCHAR(100) UNIQUE NOT NULL,
    additional_data text,
    password        VARCHAR(100),
    avatar          TEXT
);

create sequence person_seq start 1;

create TABLE account
(
    id        bigint PRIMARY KEY,
    person_id bigint,
    role_id   bigint,
    activated boolean,
    deleted   boolean,
    version   integer,
    CONSTRAINT account_person_id_fk FOREIGN KEY (person_id)
        REFERENCES person (id),
    CONSTRAINT account_role_id_fk FOREIGN KEY (role_id)
        REFERENCES role (id)
);

create sequence account_seq start 1;

create TABLE team
(
    id   bigint PRIMARY KEY,
    name varchar(200)
);

create sequence team_seq start 1;

create TABLE account2team
(
    account_id bigint NOT NULL,
    team_id    bigint NOT NULL,
    CONSTRAINT account2profile_account_fk FOREIGN KEY (account_id)
        REFERENCES account (id),
    CONSTRAINT account2profile_team_fk FOREIGN KEY (team_id)
        REFERENCES team (id)
);

create TABLE team2profile
(
    team_id    bigint NOT NULL,
    profile_id bigint NOT NULL,
    CONSTRAINT account2profile_profile_fk FOREIGN KEY (profile_id)
        REFERENCES profile (id),
    CONSTRAINT account2profile_team_fk FOREIGN KEY (team_id)
        REFERENCES team (id)
);

create TABLE token
(
    id         bigint PRIMARY KEY,
    type       int8        NOT NULL,
    token      varchar(36) NOT NULL,
    account_id bigint      NOT NULL,
    expired    TIMESTAMP   NOT NULL,
    UNIQUE (type, account_id),
    CONSTRAINT token_account_fk FOREIGN KEY (account_id)
        REFERENCES account (id)
);

COMMENT ON COLUMN token.type IS '0 - RESET_PASSWORD; 1 - SEND_ACTIVATION_CODE; 2 - RESEND_ACTIVATION_CODE;';

create sequence token_seq start 1;

create TABLE dashboard_widget
(
    id               bigint PRIMARY KEY,
    account_id       bigint NOT NULL,
    row              int8   NOT NULL,
    "column"         int8   NOT NULL,
    widget_type_enum bigint NOT NULL,
    widget_id        bigint,
    UNIQUE (account_id, row, "column"),
    constraint row_check check (row >= 0 and row < 3),
    constraint column_check check ("column" >= 0 and "column" < 3),
    CONSTRAINT dashboard_widget_account_fk FOREIGN KEY (account_id)
        REFERENCES account (id)
);

COMMENT ON COLUMN dashboard_widget.widget_type_enum IS '0 - GOAL; 1 - REPORT; 2 - CALENDAR; 3 - NOTE; 4 - ACTIVITY; 5 - FILE;';

create sequence dashboard_widget_seq start 1;


create table global_configuration
(
    id    bigint PRIMARY KEY,
    key   VARCHAR(200) UNIQUE NOT NULL,
    value TEXT
);

create sequence global_configuration_seq start 1;

create table account_configuration
(
    id                bigint PRIMARY KEY,
    account_id        bigint       NOT NULL unique,
    locale            varchar(30)  NOT NULL default 'pl',
    time_zone         varchar(100) NOT NULL default 'Europe/Warsaw',
    date_format       varchar(30)  NOT NULL default 'YYYY/MM/DD',
    first_day_of_week varchar(30)  NOT NULL default '0',
    first_hour_of_day varchar(10)  NOT NULL default '9',
    hour_format       varchar(10)  NOT NULL default '12',
    CONSTRAINT configuration_account_fk FOREIGN KEY (account_id)
        REFERENCES account (id)
);

create sequence account_configuration_seq start 1;
