
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.scoringField', 'permissions.names.leadEditScoringField', 1, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.scoringField', 'permissions.names.leadCreateScoringField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);
