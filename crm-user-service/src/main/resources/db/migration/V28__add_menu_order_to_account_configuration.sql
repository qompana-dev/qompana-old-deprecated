ALTER TABLE account_configuration
    ADD  menu_order text default '[]' not null;
update account_configuration
set menu_order = '[
  {
    "order": 0,
    "name": "dashboard",
    "visible": true
  },
  {
    "order": 1,
    "name": "leads",
    "visible": true
  },
  {
    "order": 2,
    "name": "opportunity",
    "visible": true
  },
  {
    "order": 3,
    "name": "calendar",
    "visible": true
  },
  {
    "order": 4,
    "name": "customers",
    "visible": true
  },
  {
    "order": 5,
    "name": "contacts",
    "visible": true
  },
  {
    "order": 6,
    "name": "documents",
    "visible": true
  },
  {
    "order": 7,
    "name": "products",
    "visible": true
  },
  {
    "order": 8,
    "name": "price-book",
    "visible": true
  },
  {
    "order": 9,
    "name": "bpmn",
    "visible": true
  },
  {
    "order": 10,
    "name": "manufacturers-and-suppliers",
    "visible": true
  },
  {
    "order": 11,
    "name": "goals",
    "visible": true
  },
  {
    "order": 12,
    "name": "mail",
    "visible": true
  }
]';