insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseAddComponent.responsible', 'permissions.names.expenseAddResponsible', 2, 1, 0, false, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseListComponent.responsible', 'permissions.names.expenseListResponsible', 1, 0, 0, false, -39);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -39, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseEditComponent.responsible', 'permissions.names.expenseEditResponsible', 2, 2, 0, false, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);
