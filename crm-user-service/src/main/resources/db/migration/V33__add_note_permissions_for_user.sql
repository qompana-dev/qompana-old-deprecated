
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'UserNotes.view', 'permissions.names.userNotesView', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'UserNotes.add', 'permissions.names.userNotesAdd', 2, 1, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'UserNotes.edit', 'permissions.names.userNotesEdit', 2, 2, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'UserNotes.remove', 'permissions.names.userNotesRemove', 2, 3, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);
