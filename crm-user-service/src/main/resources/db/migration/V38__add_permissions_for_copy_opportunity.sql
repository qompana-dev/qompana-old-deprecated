insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityShared.copy', 'permissions.names.opportunityCopy', 2, 3, 1, false, -27);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -27, currval('web_control_seq'), 2);
