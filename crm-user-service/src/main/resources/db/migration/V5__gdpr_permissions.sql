insert into module(id, name, module_category_id) values (-45, 'permissions.module.contactGdpr', -11);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-45, -45, true, true, true, true, -1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactGdprComponent.shareGdpr', 'permissions.names.contactGdprShare', 2, 1, 0, false, -45);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -45, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactGdprComponent.manualChangeGdpr', 'permissions.names.contactGdprManualChangeGdpr', 2, 2, 0, false, -45);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -45, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactGdprComponent.viewGdprLink', 'permissions.names.contactGdprViewLink', 1, 0, 0, false, -45);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -45, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactGdprComponent.createGdprLink', 'permissions.names.contactGdprCreateLink', 2, 1, 1, false, -45);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -45, currval('web_control_seq'), 2);




