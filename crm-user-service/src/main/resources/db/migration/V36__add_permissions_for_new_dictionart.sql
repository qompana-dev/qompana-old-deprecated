INSERT INTO web_control (id, frontend_id, module_id, name, default_state, depends_on, type, required)
VALUES (nextval('web_control_seq'), 'LeadShared.addPosition', -10, 'permissions.names.leadAddPosition', 2, 1, 1, false);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -10, currval('web_control_seq'), 2);

INSERT INTO web_control (id, frontend_id, module_id, name, default_state, depends_on, type, required)
VALUES (nextval('web_control_seq'), 'LeadShared.addCompanyType', -10, 'permissions.names.leadAddCompanyType', 2, 1, 1, false);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -10, currval('web_control_seq'), 2);

