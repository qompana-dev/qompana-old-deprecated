insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityListComponent.subjectOfInterestColumn', 'permissions.names.opportunityListSubjectOfInterestColumn', 2, 0, 0, false, -26);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -26, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.subjectOfInterestField', 'permissions.names.opportunityCreateSubjectOfInterestField', 2, 1, 0, true, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.subjectOfInterestField', 'permissions.names.opportunityEditSubjectOfInterestField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.subjectOfInterest', 'permissions.names.opportunityDetailsCardComponentSubjectOfInterest', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.subjectOfInterestField', 'permissions.names.opportunityEditSubjectOfInterestField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.subjectOfInterest', 'permissions.names.opportunityDetailsCardComponentSubjectOfInterest', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.subjectOfInterest', 'permissions.names.opportunityDetailsSubjectOfInterest', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);