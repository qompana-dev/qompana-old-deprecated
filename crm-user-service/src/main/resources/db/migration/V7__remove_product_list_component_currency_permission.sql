-- insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
-- values (nextval('web_control_seq'), 'ProductListComponent.currency', 'permissions.names.productListComponentCurrency', 2, 0, 0, false, -15);
-- insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -15, currval('web_control_seq'), 2);

delete from permission where web_control_id in (select id from web_control where frontend_id = 'ProductListComponent.currency');
delete from web_control where frontend_id = 'ProductListComponent.currency';

