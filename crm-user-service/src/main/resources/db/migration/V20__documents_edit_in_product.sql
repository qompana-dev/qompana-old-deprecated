insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.history', 'permissions.names.leadDetailsHistory', 1, 0, 0, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

-- documents edit in product

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInProductComponent.description', 'permissions.names.documentsEditInProductComponentDescription', 2, 2, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInProductComponent.tags', 'permissions.names.documentsEditInProductComponentTags', 2, 2, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInProductComponent.authorFullName', 'permissions.names.documentsEditInProductComponentAddedBy', 2, 2, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInProductComponent.assignedTo', 'permissions.names.documentsEditInProductComponentAssignedTo', 2, 2, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInProductComponent.save', 'permissions.names.documentsEditInProductComponentSave', 2, 2, 1, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

--documents list in product
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInProductComponent.originalName', 'permissions.names.documentsListInProductComponentOriginalName', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInProductComponent.created', 'permissions.names.documentsListInProductComponentCreated', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInProductComponent.format', 'permissions.names.documentsListInOpportunityProductFormat', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInProductComponent.authorFullName', 'permissions.names.documentsListInProductComponentAddedBy', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInProductComponent.contractName', 'permissions.names.documentsListInProductComponentContractName', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInProductComponent.status', 'permissions.names.documentsListInProductComponentStatus', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);
-- documents add in product

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddInProductComponent.description', 'permissions.names.documentsAddInProductComponentDescription', 2, 1, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddInProductComponent.tags', 'permissions.names.documentsAddInProductComponentTags', 2, 1, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddInProductComponent.assignedTo', 'permissions.names.documentsAddInProductComponentAssignedTo', 2, 1, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddInProductComponent.save', 'permissions.names.documentsAddInProductComponentSave', 2, 1, 1, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

-- product files mini view
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductFilesMiniComponent.addFile', 'permissions.names.productMiniFilesAddNew', 2, 1, 1, false, -30);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -30, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductFilesMiniComponent.view', 'permissions.names.productMiniFilesView', 1, 0, 0, false, -30);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -30, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductFilesMiniComponent.showMore', 'permissions.names.productMiniFilesShowMore', 2, 0, 1, false, -30);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -30, currval('web_control_seq'), 2);
