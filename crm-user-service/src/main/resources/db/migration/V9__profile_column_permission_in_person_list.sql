insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonListComponent.role.profiles.nameColumn', 'permissions.names.userListProfileName', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);
