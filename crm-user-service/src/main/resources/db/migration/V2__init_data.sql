insert into profile(id, name, description)
values(-1, 'admin', 'Użytkownik będący administratorem, ma dostęp do wszystkich zakładek');

insert into person(id, name, surname, description, phone, email, password)
values(-1, 'Jan', 'Kowalski', 'Admin user', '48111111111', 'jan.kowalski@devqube.com', '$2a$10$KTAR2efAl6zocmh5KqX.jOdrYOxlbys/RDvFnckvg/cQqUJ4HLlw2');

insert into person(id, name, surname, description, phone, email, password)
values(-7, 'Mateusz', 'Fiślak', 'Admin user', '48111111111', 'mateusz.fislak@devqube.com', '$2a$10$KTAR2efAl6zocmh5KqX.jOdrYOxlbys/RDvFnckvg/cQqUJ4HLlw2');

insert into person(id, name, surname, description, phone, email, password)
values(-2, 'Janusz', 'Polak', 'Regular User', '48222222222', 'janusz.polak@devqube.com', '$2a$10$KTAR2efAl6zocmh5KqX.jOdrYOxlbys/RDvFnckvg/cQqUJ4HLlw2');

insert into person(id, name, surname, description, phone, email, password)
values(-3, 'Marek', 'Nowak', 'Seller user', '48333333333', 'marek.nowak@devqube.com', '$2a$10$KTAR2efAl6zocmh5KqX.jOdrYOxlbys/RDvFnckvg/cQqUJ4HLlw2');

insert into person(id, name, surname, description, phone, email, password)
values(-4, 'Artur', 'Gospodarczyk', 'Engave', '22222222222', 'artur.gospodarczyk@engave.pl', '$2a$10$VH8dbPJ3AM9AZCFBXJQ/lem5DFLKSMkZsdmhG1scjWksy6OS.apM6');

insert into person(id, name, surname, description, phone, email, password)
values(-5, 'Piotr', 'Cel', 'New', '12345678976', 'piotr.celinski@engave.pl', '$2a$10$Y/ukHXDXQMqLgCr1q9RgPOc88OHEmCKGSgMRe3vN9sEm.aAu26LB6');

insert into person(id, name, surname, description, phone, email, password)
values(-6, 'Krzysztof', 'Krzysztof', 'Test', '65432345654', 'p.celinski1986@gmail.com', '$2a$10$kAOKeznA0Mia5HrRRD3Av.KGfmTaW9oYFLQKD.uM8fTe53pUDeWvy');

insert into global_configuration(id, key, value)
values(-1, 'passwordPolicyActive', 'true');

insert into global_configuration(id, key, value)
values(-2, 'passwordPolicyRegex', '(?=.{9,})(?=.*?[^\w\s])(?=.*?[0-9])(?=.*?[A-Z]).*?[a-z].*');

insert into global_configuration(id, key, value)
values(-3, 'passwordPolicyDescription', 'Hasło musi zawierać co najmniej 9 znaków, w tym jedną małą literę, jedną wielką literę, jedną cyfrę oraz jeden znak specjalny');

insert into global_configuration(id, key, value)
values(-4, 'expenseAcceptanceThreshold', '500');

insert into global_configuration(id, key, value)
values(-5, 'customerRadiusInKm', '100');

insert into global_configuration(id, key, value)
values(-6, 'cityRadiusInKm', '100');

insert into global_configuration(id, key, value)
values(-7, 'currencies', 'PLN;EUR;USD;GBP;CNY');

insert into global_configuration(id, key, value)
values(-8, 'systemCurrency', 'PLN');

insert into global_configuration(id, key, value)
values(-9, 'rateOfMainCurrencyToPLN', '0.1');

insert into global_configuration(id, key, value)
values(-10, 'rateOfMainCurrencyToEUR', '0.2');

insert into global_configuration(id, key, value)
values(-11, 'rateOfMainCurrencyToUSD', '0.3');

insert into global_configuration(id, key, value)
values(-12, 'rateOfMainCurrencyToGBP', '0.4');

insert into global_configuration(id, key, value)
values(-13, 'rateOfMainCurrencyToCNY', '0.5');

insert into global_configuration(id, key, value)
values(-14, 'timeDifferenceInD', '3');

insert into global_configuration(id, key, value)
values(-15, 'companyConfig', '{}');

insert into role(id, name) values(-1, 'CEO');
insert into role(id, name) values(-2, 'Kierownik działu 1');
insert into role(id, name) values(-3, 'Kierownik działu 2');
insert into role(id, name) values(-4, 'Pracownik działu 1');
insert into role(id, name) values(-5, 'Pracownik działu 2');

insert into profile2role(profile_id, role_id) values (-1, -1);
insert into profile2role(profile_id, role_id) values (-1, -2);
insert into profile2role(profile_id, role_id) values (-1, -3);
insert into profile2role(profile_id, role_id) values (-1, -4);
insert into profile2role(profile_id, role_id) values (-1, -5);

insert into account(id, person_id, activated, deleted, version, role_id)
values(-1, -1, true, false, 0, -1);

insert into account(id, person_id, activated, deleted, version, role_id)
values(-2, -2, true, false, 0, -4);

insert into account(id, person_id, activated, deleted, version, role_id)
values(-3, -3, false, false, 0, -3);

insert into account(id, person_id, activated, deleted, version, role_id)
values(-4, -4, true, false, 0, -2);

insert into account(id, person_id, activated, deleted, version, role_id)
values(-5, -5, true, false, 1, -4);

insert into account(id, person_id, activated, deleted, version, role_id)
values(-6, -6, true, false, 0, -5);

insert into account(id, person_id, activated, deleted, version, role_id)
values(-7, -7, true, false, 0, -4);

insert into account_configuration(id, account_id, locale, time_zone)
values(-1, -1, 'pl', 'Europe/Warsaw');

insert into account_configuration(id, account_id, locale, time_zone)
values(-2, -2, 'pl', 'Europe/Warsaw');

insert into account_configuration(id, account_id, locale, time_zone)
values(-3, -3, 'pl', 'Europe/Warsaw');

insert into account_configuration(id, account_id, locale, time_zone)
values(-4, -4, 'pl', 'Europe/Warsaw');

insert into account_configuration(id, account_id, locale, time_zone)
values(-5, -5, 'pl', 'Europe/Warsaw');

insert into account_configuration(id, account_id, locale, time_zone)
values(-6, -6, 'pl', 'Europe/Warsaw');

insert into account_configuration(id, account_id, locale, time_zone)
values(-7, -7, 'pl', 'Europe/Warsaw');

insert into role_children(parent_id, child_id) values(-1, -2);
insert into role_children(parent_id, child_id) values(-1, -3);

insert into role_children(parent_id, child_id) values(-2, -4);
insert into role_children(parent_id, child_id) values(-3, -5);

--permissions

insert into module_category(id, name) values (-1, 'permissions.moduleCategory.userManagement');

insert into module_category(id, name) values (-2, 'permissions.moduleCategory.customerManagement');

insert into module_category(id, name) values (-3, 'permissions.moduleCategory.leadManagement');

insert into module_category(id, name) values (-4, 'permissions.moduleCategory.businessProcessManagement');

insert into module_category(id, name) values (-5, 'permissions.moduleCategory.documentsManagement');

insert into module_category(id, name) values (-6, 'permissions.moduleCategory.navigation');

insert into module_category(id, name) values (-7, 'permissions.moduleCategory.productManagement');

insert into module_category(id, name) values (-8, 'permissions.moduleCategory.priceBookManagement');

insert into module_category(id, name) values (-9, 'permissions.moduleCategory.calendarManagement');

insert into module_category(id, name) values (-10, 'permissions.moduleCategory.opportunityManagement');

insert into module_category(id, name) values (-11, 'permissions.moduleCategory.contactManagement');

insert into module_category(id, name) values (-12, 'permissions.moduleCategory.expenseManagement');

insert into module_category(id, name) values (-13, 'permissions.moduleCategory.systemManagement');

insert into module_category(id, name) values (-14, 'permissions.moduleCategory.goalManagement');

insert into module(id, name, module_category_id) values (-1, 'permissions.module.users', -1);

insert into module(id, name, module_category_id) values (-2, 'permissions.module.permissionsManagement', -1);

insert into module(id, name, module_category_id) values (-3, 'permissions.module.customer', -2);

insert into module(id, name, module_category_id) values (-4, 'permissions.module.contact', -11);

insert into module(id, name, module_category_id) values (-5, 'permissions.module.leadDetails', -3);

insert into module(id, name, module_category_id) values (-6, 'permissions.module.businessProcess', -4);

insert into module(id, name, module_category_id) values (-7, 'permissions.module.leadList', -3);

insert into module(id, name, module_category_id) values (-8, 'permissions.module.leadAdd', -3);

insert into module(id, name, module_category_id) values (-9, 'permissions.module.leadEdit', -3);

insert into module(id, name, module_category_id) values (-10, 'permissions.module.leadShared', -3);

insert into module(id, name, module_category_id) values (-11, 'permissions.module.leadKanban', -3);

insert into module(id, name, module_category_id) values (-12, 'permissions.module.documents', -5);

insert into module(id, name, module_category_id) values (-13, 'permissions.module.search', -6);

insert into module(id, name, module_category_id) values (-14, 'permissions.module.sideMenu', -6);

insert into module(id, name, module_category_id) values (-15, 'permissions.module.productList', -7);

insert into module(id, name, module_category_id) values (-16, 'permissions.module.productAdd', -7);

insert into module(id, name, module_category_id) values (-17, 'permissions.module.productEdit', -7);

insert into module(id, name, module_category_id) values (-18, 'permissions.module.priceBookList', -8);

insert into module(id, name, module_category_id) values (-19, 'permissions.module.priceBookAdd', -8);

insert into module(id, name, module_category_id) values (-20, 'permissions.module.priceBookEdit', -8);

insert into module(id, name, module_category_id) values (-21, 'permissions.module.productListInPriceBook', -8);

insert into module(id, name, module_category_id) values (-22, 'permissions.module.calendarShared', -9);

insert into module(id, name, module_category_id) values (-23, 'permissions.module.calendarView', -9);

insert into module(id, name, module_category_id) values (-24, 'permissions.module.filesMiniView', -3);

insert into module(id, name, module_category_id) values (-25, 'permissions.module.filesFullView', -3);

insert into module(id, name, module_category_id) values (-26, 'permissions.module.opportunityList', -10);

insert into module(id, name, module_category_id) values (-27, 'permissions.module.opportunityShared', -10);

insert into module(id, name, module_category_id) values (-28, 'permissions.module.opportunityAdd', -10);

insert into module(id, name, module_category_id) values (-29, 'permissions.module.opportunityEdit', -10);

insert into module(id, name, module_category_id) values (-30, 'permissions.module.filesMiniView', -10);

insert into module(id, name, module_category_id) values (-31, 'permissions.module.filesFullView', -10);

insert into module(id, name, module_category_id) values (-32, 'permissions.module.opportunityDetails', -10);

insert into module(id, name, module_category_id) values (-33, 'permissions.module.opportunityProducts', -10);

insert into module(id, name, module_category_id) values (-34, 'permissions.module.contactDetails', -11);

insert into module(id, name, module_category_id) values (-35, 'permissions.module.contactLink', -11);

insert into module(id, name, module_category_id) values (-36, 'permissions.module.contactOpportunities', -11);

insert into module(id, name, module_category_id) values (-37, 'permissions.module.contactInfo', -11);

insert into module(id, name, module_category_id) values (-38, 'permissions.module.whitelist', -2);

insert into module(id, name, module_category_id) values (-39, 'permissions.module.expenseList', -12);

insert into module(id, name, module_category_id) values (-40, 'permissions.module.expenseAdd', -12);

insert into module(id, name, module_category_id) values (-41, 'permissions.module.relatedCustomers', -11);

insert into module(id, name, module_category_id) values (-42, 'permissions.module.administration', -13);

insert into module(id, name, module_category_id) values (-43, 'permissions.module.goalList', -14);

insert into module(id, name, module_category_id) values (-44, 'permissions.module.relatedContacts', -2);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-1, -1, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-2, -2, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-3, -3, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-4, -4, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-5, -5, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-6, -6, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-7, -7, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-8, -8, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-9, -9, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-10, -10, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-11, -11, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-12, -12, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-13, -13, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-14, -14, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-15, -15, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-16, -16, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-17, -17, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-18, -18, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-19, -19, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-20, -20, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-21, -21, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-22, -22, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-23, -23, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-24, -24, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-25, -25, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-26, -26, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-27, -27, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-28, -28, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-29, -29, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-30, -30, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-31, -31, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-32, -32, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-33, -33, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-34, -34, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-35, -35, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-36, -36, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-37, -37, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-38, -38, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-39, -39, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-40, -40, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-41, -41, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-42, -42, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-43, -43, true, true, true, true, -1);

insert into module_permission(id, module_id, view, "create", edit, delete, profile_id) values (-44, -44, true, true, true, true, -1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonAddComponent.roleField', 'permissions.names.addUserRoleForm', 2, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationComponent.permissions', 'permissions.names.permissions', 2, 0, 1, false, -2);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -2, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonInfoComponent.roleLabel', 'permissions.names.roleLabel', 1, 0, 0, false, -2);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -2, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonAddComponent.addForm', 'permissions.names.addUserNewFrom', 2, 0, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonAddComponent.save', 'permissions.names.addUserSave', 2, 0, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.addForm', 'permissions.names.editUserNewFrom', 2, 0, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.sendActivationLink', 'permissions.names.editUserResendActivationLink', 2, 0, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.changeActivated', 'permissions.names.editUserChangeActivated', 2, 0, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.deleteUser', 'permissions.names.editUserDeleteUser', 2, 0, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.submitForm', 'permissions.names.editUser', 2, 0, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonInfoComponent.idLabel', 'permissions.names.personPreviewId', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonInfoComponent.nameLabel', 'permissions.names.personPreviewName', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonInfoComponent.avatarLabel', 'permissions.names.personPreviewAvatar', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonInfoComponent.surnameLabel', 'permissions.names.personPreviewSurname', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonInfoComponent.descriptionLabel', 'permissions.names.personPreviewDescription', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonInfoComponent.phoneLabel', 'permissions.names.personPreviewPhone', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonInfoComponent.emailLabel', 'permissions.names.personPreviewEmail', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonInfoComponent.isActivatedLabel', 'permissions.names.personPreviewActivated', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonInfoComponent.additionalDataLabel', 'permissions.names.personPreviewAdditionalData', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonInfoComponent.changePasswordButton', 'permissions.names.changePassword', 2, 0, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonListComponent.idColumn', 'permissions.names.userListId', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonListComponent.person.nameColumn', 'permissions.names.userListName', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonListComponent.person.avatarColumn', 'permissions.names.userListAvatar', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonListComponent.person.surnameColumn', 'permissions.names.userListSurname', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonListComponent.person.role.nameColumn', 'permissions.names.userListRoleName', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonListComponent.person.descriptionColumn', 'permissions.names.userListDescription', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonListComponent.person.descriptionColumn', 'permissions.names.userListDescription', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonListComponent.person.phoneColumn', 'permissions.names.userListPhone', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonListComponent.person.emailColumn', 'permissions.names.userListEmail', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonListComponent.activatedColumn', 'permissions.names.userListActive', 1, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.idField', 'permissions.names.editUserId', 1, 0, 0, true, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.nameField', 'permissions.names.editUserName', 2, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.surnameField', 'permissions.names.editUserSurname', 2, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.descriptionField', 'permissions.names.editUserDescription', 2, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.phoneField', 'permissions.names.editUserPhone', 2, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.emailField', 'permissions.names.editUserEmail', 1, 0, 0, true, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.additionalDataField', 'permissions.names.editUserAdditionalData', 2, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.deleteForm', 'permissions.names.editUserDeleteAdditionalDataForm', 2, 0, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonAddComponent.idField', 'permissions.names.addUserId', 1, 0, 0, true, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonAddComponent.nameField', 'permissions.names.addUserName', 2, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonAddComponent.surnameField', 'permissions.names.addUserSurname', 2, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonAddComponent.descriptionField', 'permissions.names.addUserDescription', 2, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonAddComponent.phoneField', 'permissions.names.addUserPhone', 2, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonAddComponent.emailField', 'permissions.names.addUserEmail', 2, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonAddComponent.additionalDataField', 'permissions.names.addUserAdditionalData', 2, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonAddComponent.deleteForm', 'permissions.names.addUserDeleteAdditionalDataForm', 2, 0, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonListComponent.addUser', 'permissions.names.navAddPerson', 2, 0, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationComponent.settingItem.info', 'permissions.names.settingListInfo', 2, 0, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationComponent.settingItem.user', 'permissions.names.settingListUser', 2, 0, 1, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PersonDetailComponent.roleField', 'permissions.names.editUserRoleForm', 2, 0, 0, false, -1);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -1, currval('web_control_seq'), 2);

--CUSTOMER
--add
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.nipField', 'permissions.names.customerAddNipField', 2, 1, 0, true, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.regonField', 'permissions.names.customerAddRegonField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.nameField', 'permissions.names.customerAddNameField', 2, 1, 0, true, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.descriptionField', 'permissions.names.customerAddDescriptionField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.typeField', 'permissions.names.customerAddTypeField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.phoneField', 'permissions.names.customerAddPhoneField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.countryField', 'permissions.names.customerAddCountryField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.cityField', 'permissions.names.customerAddCityField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.zipCodeField', 'permissions.names.customerAddZipCodeField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.streetField', 'permissions.names.customerAddStreetField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.buildNumberField', 'permissions.names.customerAddBuildNumberField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.flatNumberField', 'permissions.names.customerAddFlatNumberField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.employeesNumber', 'permissions.names.customerAddEmployeesNumber', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.companyType', 'permissions.names.customerAddCompanyType', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.income', 'permissions.names.customerAddIncome', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

--edit

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.nipField', 'permissions.names.customerEditNipField', 2, 2, 0, true, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.regonField', 'permissions.names.customerEditRegonField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.nameField', 'permissions.names.customerEditNameField', 2, 2, 0, true, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.descriptionField', 'permissions.names.customerEditDescriptionField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.typeField', 'permissions.names.customerEditTypeField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.phoneField', 'permissions.names.customerEditPhoneField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.countryField', 'permissions.names.customerEditCountryField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.cityField', 'permissions.names.customerEditCityField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.zipCodeField', 'permissions.names.customerEditZipCodeField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.streetField', 'permissions.names.customerEditStreetField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.buildNumberField', 'permissions.names.customerEditBuildNumberField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.flatNumberField', 'permissions.names.customerEditFlatNumberField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.employeesNumber', 'permissions.names.customerEditEmployeesNumber', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.companyType', 'permissions.names.customerEditCompanyType', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.income', 'permissions.names.customerEditIncome', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

--list
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerListComponent.nameColumn', 'permissions.names.customerListNameColumn', 1, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerListComponent.phoneColumn', 'permissions.names.customerListPhoneColumn', 1, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 1);

--tools

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.save', 'permissions.names.customerAddSave', 2, 1, 1, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.save', 'permissions.names.customerEditSave', 2, 2, 1, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerListComponent.remove', 'permissions.names.customerListRemove', 2, 3, 1, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerFormComponent.getFromGus', 'permissions.names.getFromGus', 2, 3, 1, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

--CONTACTS

--list
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactListComponent.nameColumn', 'permissions.names.contactListNameColumn', 2, 0, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactListComponent.ownerColumn', 'permissions.names.contactListOwnerColumn', 2, 0, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactListComponent.positionColumn', 'permissions.names.contactListPositionColumn', 2, 0, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactListComponent.customer.nameColumn', 'permissions.names.contactListCompanyColumn', 2, 0, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactListComponent.phoneColumn', 'permissions.names.contactListPhoneColumn', 2, 0, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactListComponent.emailColumn', 'permissions.names.contactListEmailColumn', 2, 0, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

--actions
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactAddComponent.save', 'permissions.names.contactAddSave', 2, 1, 1, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.save', 'permissions.names.contactEditSave', 2, 2, 1, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactListComponent.remove', 'permissions.names.contactListRemove', 2, 3, 1, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactListComponent.changeOwner', 'permissions.names.contactListChangeOwner', 2, 3, 1, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

--add
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactAddComponent.nameField', 'permissions.names.contactAddNameField', 2, 1, 0, true, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactAddComponent.surnameField', 'permissions.names.contactAddSurnameField', 2, 1, 0, true, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactAddComponent.positionField', 'permissions.names.contactAddPositionField', 2, 1, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactAddComponent.companyField', 'permissions.names.contactAddCompanyField', 2, 1, 0, true, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactAddComponent.ownerInfo', 'permissions.names.contactAddOwnerInfo', 2, 1, 0, true, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactAddComponent.phoneField', 'permissions.names.contactAddPhoneField', 2, 1, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactAddComponent.emailField', 'permissions.names.contactAddEmailField', 2, 1, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactAddComponent.socialMediaField', 'permissions.names.contactAddSocialMediaField', 2, 1, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

--edit
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.nameField', 'permissions.names.contactEditNameField', 2, 2, 0, true, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.surnameField', 'permissions.names.contactEditSurnameField', 2, 2, 0, true, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.positionField', 'permissions.names.contactEditPositionField', 2, 2, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.companyField', 'permissions.names.contactEditCompanyField', 2, 2, 0, true, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.ownerInfo', 'permissions.names.contactEditOwnerInfo', 2, 2, 0, true, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.phoneField', 'permissions.names.contactEditPhoneField', 2, 2, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.emailField', 'permissions.names.contactEditEmailField', 2, 2, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.socialMediaField', 'permissions.names.contactEditSocialMediaField', 2, 2, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.coordinatesField', 'permissions.names.customerAddCoordinatesField', 1, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.coordinatesField', 'permissions.names.customerEditCoordinatesField', 1, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactAddComponent.coordinatesField', 'permissions.names.contactAddCoordinatesField', 1, 1, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.coordinatesField', 'permissions.names.contactEditCoordinatesField', 1, 2, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactAddComponent.relationTime', 'permissions.names.contactAddRelationTimeField', 1, 1, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.relationTime', 'permissions.names.contactEditRelationTimeField', 1, 2, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactAddComponent.note', 'permissions.names.contactAddNoteField', 1, 1, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactEditComponent.note', 'permissions.names.contactEditNoteField', 1, 2, 0, false, -4);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -4, currval('web_control_seq'), 2);



insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.firstToContactField', 'permissions.names.customerAddFirstContact', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.firstToContactField', 'permissions.names.customerEditFirstContact', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerListComponent.firstToContactNameColumn', 'permissions.names.customerListFirstContactNameColumn', 1, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerListComponent.firstToContactEmailColumn', 'permissions.names.customerListFirstContactEmailColumn', 1, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerListComponent.firstToContactPhoneColumn', 'permissions.names.customerListFirstContactPhoneColumn', 1, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerListComponent.typeColumn', 'permissions.names.customerListTypeColumn', 1, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerListComponent.ownerColumn', 'permissions.names.customerListOwnerColumn', 1, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerListComponent.changeOwner', 'permissions.names.customerListChangeOwner', 2, 3, 1, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

--LEADS

--shared

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadShared.edit', 'permissions.names.leadEdit', 2, 2, 1, false, -10);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -10, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadShared.delete', 'permissions.names.leadDelete', 2, 3, 1, false, -10);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -10, currval('web_control_seq'), 2);

--details

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.image', 'permissions.names.leadDetailsImage', 1, 0, 0, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.mainInfo', 'permissions.names.leadDetailsMainInfo', 1, 0, 0, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.company', 'permissions.names.leadDetailsCompany', 1, 0, 0, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.phone', 'permissions.names.leadDetailsPhone', 1, 0, 0, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.email', 'permissions.names.leadDetailsEmail', 1, 0, 0, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.salesOpportunity', 'permissions.names.leadDetailsSalesOpportunity', 1, 0, 0, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.refresh', 'permissions.names.leadDetailsRefresh', 2, 0, 1, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.completeTask', 'permissions.names.completeTask', 2, 0, 1, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.activity', 'permissions.names.leadDetailsActivity', 1, 0, 0, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.expenses', 'permissions.names.leadDetailsExpenses', 1, 0, 0, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);
--list
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadListComponent.avatarColumn', 'permissions.names.leadListAvatarColumn', 2, 0, 0, false, -7);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -7, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadListComponent.firstNameColumn', 'permissions.names.leadListNameColumn', 2, 0, 0, false, -7);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -7, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadListComponent.companyNameColumn', 'permissions.names.leadListCompanyColumn', 2, 0, 0, false, -7);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -7, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadListComponent.cityColumn', 'permissions.names.leadListCityColumn', 2, 0, 0, false, -7);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -7, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadListComponent.phoneColumn', 'permissions.names.leadListPhoneColumn', 2, 0, 0, false, -7);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -7, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadListComponent.emailColumn', 'permissions.names.leadListEmailColumn', 2, 0, 0, false, -7);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -7, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadListComponent.createdColumn', 'permissions.names.leadListCreationDateColumn', 2, 0, 0, false, -7);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -7, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadListComponent.statusColumn', 'permissions.names.leadListStatusColumn', 2, 0, 0, false, -7);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -7, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadListComponent.leedKeeperColumn', 'permissions.names.leadListLeedKeeperColumn', 2, 0, 0, false, -7);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -7, currval('web_control_seq'), 2);

-- edit

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.companyTypeField', 'permissions.names.leadEditCompanyTypeField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.companyNameField', 'permissions.names.leadEditCompanyNameField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.wwwAddressField', 'permissions.names.leadEditWwwAddressField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.streetAndNumberField', 'permissions.names.leadEditStreetAndNumberField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.cityField', 'permissions.names.leadEditCityField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.postalCodeField', 'permissions.names.leadEditPostalCodeField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.provinceField', 'permissions.names.leadEditProvinceField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.countryField', 'permissions.names.leadEditCountryField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.notesField', 'permissions.names.leadEditNotesField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.salesOpportunityField', 'permissions.names.leadEditSalesOpportunityField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.statusField', 'permissions.names.leadEditStatusField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.firstNameField', 'permissions.names.leadEditFirstNameField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.lastNameField', 'permissions.names.leadEditLastNameField', 2, 2, 0, true, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.positionField', 'permissions.names.leadEditPositionField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.emailField', 'permissions.names.leadEditEmailField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.phoneField', 'permissions.names.leadEditPhoneField', 2, 2, 0, true, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.leadKeeperField', 'permissions.names.leadEditLeadKeeperField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.sourceOfAcquisitionField', 'permissions.names.leadEditSourceOfAcquisitionField', 2, 2, 0, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadEditComponent.showOnMap', 'permissions.names.leadEditShowOnMap', 2, 2, 1, false, -9);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -9, currval('web_control_seq'), 2);

-- create

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.processDefinitionField', 'permissions.names.leadCreateProcessDefinitionField', 2, 1, 0, true, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.companyTypeField', 'permissions.names.leadCreateCompanyTypeField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.companyNameField', 'permissions.names.leadCreateCompanyNameField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.wwwAddressField', 'permissions.names.leadCreateWwwAddressField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.streetAndNumberField', 'permissions.names.leadCreateStreetAndNumberField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.cityField', 'permissions.names.leadCreateCityField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.postalCodeField', 'permissions.names.leadCreatePostalCodeField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.provinceField', 'permissions.names.leadCreateProvinceField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.countryField', 'permissions.names.leadCreateCountryField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.notesField', 'permissions.names.leadCreateNotesField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.salesOpportunityField', 'permissions.names.leadCreateSalesOpportunityField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.statusField', 'permissions.names.leadCreateStatusField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.firstNameField', 'permissions.names.leadCreateFirstNameField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.lastNameField', 'permissions.names.leadCreateLastNameField', 2, 1, 0, true, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.positionField', 'permissions.names.leadCreatePositionField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.emailField', 'permissions.names.leadCreateEmailField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.phoneField', 'permissions.names.leadCreatePhoneField', 2, 1, 0, true, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.leadKeeperField', 'permissions.names.leadCreateLeadKeeperField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.sourceOfAcquisitionField', 'permissions.names.leadCreateSourceOfAcquisitionField', 2, 1, 0, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.save', 'permissions.names.leadCreateSave', 2, 1, 1, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadCreateComponent.showOnMap', 'permissions.names.leadCreateShowOnMap', 2, 1, 1, false, -8);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -8, currval('web_control_seq'), 2);

-- BUSINESS PROCESS


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'BpmnComponent.add', 'permissions.names.bpmnAdd', 2, 1, 1, false, -6);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -6, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'BpmnComponent.edit', 'permissions.names.bpmnEdit', 2, 2, 1, false, -6);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -6, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'BpmnComponent.delete', 'permissions.names.bpmnDelete', 2, 3, 1, false, -6);
insert into permission(id, module_permission_id, web_control_id, state)
values (nextval('permission_seq'), -6, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'BpmnComponent.createBasedOnOld', 'permissions.names.bpmnCreateBasedOnOld', 2, 1, 1,
        false, -6);
insert into permission(id, module_permission_id, web_control_id, state)
values (nextval('permission_seq'), -6, currval('web_control_seq'), 2);

--LEADS

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.goBack', 'permissions.names.goBack', 2, 0, 1, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadListComponent.positionColumn', 'permissions.names.leadListLeedKeeperColumn', 2, 0, 0, false, -7);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -7, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadDetailsComponent.previewTask', 'permissions.names.leadDetailsPreviewTask', 2, 0, 1, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);


-- Leads Kanban

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadKanbanComponent.avatarField', 'permissions.names.leadKanbanAvatarField', 2, 0, 0, false, -11);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -11, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadKanbanComponent.nameField', 'permissions.names.leadKanbanNameField', 2, 0, 0, false, -11);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -11, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadKanbanComponent.positionField', 'permissions.names.leadKanbanPositionField', 2, 0, 0, false, -11);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -11, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadKanbanComponent.companyField', 'permissions.names.leadKanbanCompanyField', 2, 0, 0, false, -11);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -11, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadKanbanComponent.phoneField', 'permissions.names.leadKanbanPhoneField', 2, 0, 0, false, -11);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -11, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadKanbanComponent.processNameField', 'permissions.names.leadKanbanProcessNameField', 2, 0, 0, false, -11);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -11, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadKanbanComponent.stepNameField', 'permissions.names.leadKanbanStepNameField', 2, 0, 0, false, -11);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -11, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadKanbanComponent.stepWeightField', 'permissions.names.leadKanbanStepWeightField', 2, 0, 0, false, -11);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -11, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadKanbanComponent.numberLeadsInTaskField', 'permissions.names.leadKanbanNumberLeadsInTaskField', 2, 0, 0, false, -11);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -11, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadKanbanComponent.changeStep', 'permissions.names.leadKanbanChangeStep', 2, 1, 1, false, -11);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -11, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadKanbanComponent.processNumberField', 'permissions.names.leadKanbanProcessNumberField', 2, 0, 0, false, -11);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -11, currval('web_control_seq'), 2);

-- customer new values

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.websiteField', 'permissions.names.customerAddWebsiteField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.websiteField', 'permissions.names.customerEditWebsiteField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.ownerField', 'permissions.names.customerAddOwnerField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.ownerField', 'permissions.names.customerEditOwnerField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.contactNameField', 'permissions.names.customerAddContactNameField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.contactNameField', 'permissions.names.customerEditContactNameField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.contactSurnameField', 'permissions.names.customerAddContactSurnameField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.contactSurnameField', 'permissions.names.customerEditContactSurnameField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.contactEmailField', 'permissions.names.customerAddContactEmailField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.contactEmailField', 'permissions.names.customerEditContactEmailField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.contactPhoneField', 'permissions.names.customerAddContactPhoneField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.contactPhoneField', 'permissions.names.customerEditContactPhoneField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.contactPositionField', 'permissions.names.customerAddContactPositionField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.contactPositionField', 'permissions.names.customerEditContactPositionField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);


-- Customer details
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.name', 'permissions.names.customerDetailsComponentName', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.webpage', 'permissions.names.customerDetailsComponentWebpage', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.address', 'permissions.names.customerDetailsComponentAddress', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.phone', 'permissions.names.customerDetailsComponentPhone', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.type', 'permissions.names.customerDetailsComponentType', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.cooperationStart', 'permissions.names.customerDetailsComponentCooperationStart', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.owner', 'permissions.names.customerDetailsComponentOwner', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

-- Customer contacts list

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerContactsComponent.avatar', 'permissions.names.customerContactsComponentAvatar', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerContactsComponent.name', 'permissions.names.customerContactsComponentName', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerContactsComponent.role', 'permissions.names.customerContactsComponentRole', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerContactsComponent.department', 'permissions.names.customerContactsComponentDepartment', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerContactsComponent.phone', 'permissions.names.customerContactsComponentPhone', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerContactsComponent.email', 'permissions.names.customerContactsComponentEmail', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerContactsComponent.owner', 'permissions.names.customerContactsComponentOwner', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerContactsComponent.position', 'permissions.names.customerContactsComponentPosition', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerContactsComponent.delete', 'permissions.names.customerContactsComponentDelete', 2, 3, 1, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerContactsComponent.edit', 'permissions.names.customerContactsComponentEdit', 2, 3, 1, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerContactsComponent.markMainContact', 'permissions.names.customerContactsComponentMarkMainContact', 2, 2, 1, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerContactsComponent.notes', 'permissions.names.customerContactsComponentNotes', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

-- Customer related contacts

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'RelatedContactsInCustomerDetail.view', 'permissions.names.relatedContactsInCustomerDetailView', 2, 0, 1, false, -44);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -44, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'RelatedContactsInCustomerDetail.edit', 'permissions.names.relatedContactsInCustomerDetailEdit', 2, 2, 1, false, -44);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -44, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'RelatedContactsInCustomerDetail.addDirect', 'permissions.names.relatedContactsInCustomerDetailAddDirect', 2, 1, 1, false, -44);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -44, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'RelatedContactsInCustomerDetail.addIndirect', 'permissions.names.relatedContactsInCustomerDetailAddIndirect', 2, 1, 1, false, -44);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -44, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'RelatedContactsInCustomerDetail.delete', 'permissions.names.relatedContactsInCustomerDetailDelete', 2, 3, 1, false, -44);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -44, currval('web_control_seq'), 2);

--documents

--documents list
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListComponent.originalName', 'permissions.names.documentsListComponentOriginalName', 2, 0, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListComponent.created', 'permissions.names.documentsListComponentCreated', 2, 0, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListComponent.format', 'permissions.names.documentsListComponentFormat', 2, 0, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListComponent.addedBy', 'permissions.names.documentsListComponentAddedBy', 2, 0, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListComponent.contractName', 'permissions.names.documentsListComponentContractName', 2, 0, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListComponent.status', 'permissions.names.documentsListComponentStatus', 2, 0, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

--document history
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsHistoryComponent.changeDate', 'permissions.names.documentsHistoryComponentChangeDate', 2, 0, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsHistoryComponent.changeAuthor', 'permissions.names.documentsHistoryComponentChangeAuthor', 2, 0, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

-- documents edit

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditComponent.description', 'permissions.names.documentsEditComponentDescription', 2, 2, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditComponent.tags', 'permissions.names.documentsEditComponentTags', 2, 2, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditComponent.addedBy', 'permissions.names.documentsEditComponentAddedBy', 2, 2, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditComponent.assignedTo', 'permissions.names.documentsEditComponentAssignedTo', 2, 2, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditComponent.save', 'permissions.names.documentsEditComponentSave', 2, 2, 1, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);


-- documents add

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddComponent.description', 'permissions.names.documentsAddComponentDescription', 2, 1, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddComponent.tags', 'permissions.names.documentsAddComponentTags', 2, 1, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddComponent.assignedTo', 'permissions.names.documentsAddComponentAssignedTo', 2, 1, 0, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddComponent.save', 'permissions.names.documentsAddComponentSave', 2, 1, 1, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);


-- document other actions

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsComponent.delete', 'permissions.names.documentsComponentDelete', 2, 3, 1, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 0);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsComponent.uploadNewVersion', 'permissions.names.documentsComponentUploadNewVersion', 2, 2, 1, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsComponent.preview', 'permissions.names.documentsComponentPreview', 2, 0, 1, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsComponent.download', 'permissions.names.documentsComponentDownload', 2, 0, 1, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsComponent.showDetails', 'permissions.names.documentsComponentShowDetails', 2, 0, 1, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsComponent.showHistory', 'permissions.names.documentsComponentShowHistory', 2, 0, 1, false, -12);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -12, currval('web_control_seq'), 2);

-- navigation search

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'SearchComponent.searchBy.lead', 'permissions.names.searchComponentSearchBylead', 2, 0, 1, false, -13);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -13, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'SearchComponent.searchBy.customer', 'permissions.names.searchComponentSearchByCustomer', 2, 0, 1, false, -13);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -13, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'SearchComponent.searchBy.file', 'permissions.names.searchComponentSearchByFile', 2, 0, 1, false, -13);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -13, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'SearchComponent.searchBy.opportunity', 'permissions.names.searchComponentSearchByOpporunity', 2, 0, 1, false, -13);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -13, currval('web_control_seq'), 2);

-- navigation side menu

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationMenu.leads', 'permissions.names.navigationMenuLeads', 2, 0, 1, false, -14);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -14, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationMenu.opportunity', 'permissions.names.navigationMenuOpportunity', 2, 0, 1, false, -14);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -14, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationMenu.calendar', 'permissions.names.navigationMenuCalendar', 2, 0, 1, false, -14);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -14, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationMenu.customers', 'permissions.names.navigationMenuCustomers', 2, 0, 1, false, -14);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -14, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationMenu.contacts', 'permissions.names.navigationMenuContacts', 2, 0, 1, false, -14);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -14, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationMenu.documents', 'permissions.names.navigationMenuDocuments', 2, 0, 1, false, -14);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -14, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationMenu.products', 'permissions.names.navigationMenuProducts', 2, 0, 1, false, -14);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -14, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationMenu.bpmn', 'permissions.names.navigationMenuBpmn', 2, 0, 1, false, -14);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -14, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationMenu.priceBook', 'permissions.names.navigationMenuPriceBook', 2, 0, 1, false, -14);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -14, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationMenu.manufacturersAndSuppliers', 'permissions.names.navigationMenuManufacturersAndSuppliers', 2, 0, 1, false, -14);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -14, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationMenu.goals', 'permissions.names.navigationMenuGoals', 2, 0, 1, false, -14);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -14, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationMenu.mail', 'permissions.names.navigationMenuMail', 2, 0, 1, false, -14);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -14, currval('web_control_seq'), 2);

-- products actions

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListComponent.saveProduct', 'permissions.names.productListSave', 2, 0, 1, false, -15);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -15, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListComponent.deleteProduct', 'permissions.names.productListDelete', 2, 0, 1, false, -15);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -15, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListComponent.importProducts', 'permissions.names.productListImport', 2, 0, 1, false, -15);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -15, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListComponent.editProduct', 'permissions.names.productListEdit', 2, 0, 1, false, -15);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -15, currval('web_control_seq'), 2);

-- products list

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListComponent.name', 'permissions.names.productListComponentName', 2, 0, 0, false, -15);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -15, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListComponent.category', 'permissions.names.productListComponentCategory', 2, 0, 0, false, -15);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -15, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListComponent.subcategory', 'permissions.names.productListComponentSubcategory', 2, 0, 0, false, -15);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -15, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListComponent.code', 'permissions.names.productListComponentCode', 2, 0, 0, false, -15);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -15, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListComponent.sellPriceNet', 'permissions.names.productListComponentSellPriceNet', 2, 0, 0, false, -15);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -15, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListComponent.currency', 'permissions.names.productListComponentCurrency', 2, 0, 0, false, -15);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -15, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListComponent.editCategory', 'permissions.names.productListComponentEditCategory', 2, 0, 1, false, -15);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -15, currval('web_control_seq'), 2);

-- products add

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.name', 'permissions.names.productAddComponentName', 2, 1, 0, true, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.code', 'permissions.names.productAddComponentCode', 2, 1, 0, true, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.skuCode', 'permissions.names.productAddComponentSkuCode', 2, 1, 0, true, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.manufacturer', 'permissions.names.productAddComponentManufacturer', 2, 1, 0, false, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.supplier', 'permissions.names.productAddComponentSupplier', 2, 1, 0, false, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.brand', 'permissions.names.productAddComponentBrand', 2, 1, 0, false, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.purchasePrice', 'permissions.names.productAddComponentPurchasePrice', 2, 1, 0, true, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.currency', 'permissions.names.productAddComponentCurrency', 2, 1, 0, true, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.sellPriceNet', 'permissions.names.productAddComponentSellPriceNet', 2, 1, 0, true, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.vat', 'permissions.names.productAddComponentVat', 2, 1, 0, true, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.active', 'permissions.names.productAddComponentActive', 2, 1, 0, true, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.description', 'permissions.names.productAddComponentDescription', 2, 1, 0, false, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.specification', 'permissions.names.productAddComponentSpecification', 2, 1, 0, false, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.notes', 'permissions.names.productAddComponentNotes', 2, 1, 0, false, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.photos', 'permissions.names.productAddComponentPhotos', 2, 1, 0, false, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.productPrices', 'permissions.names.productAddComponentProductPrices', 2, 1, 0, false, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.category', 'permissions.names.productAddComponentCategory', 2, 1, 0, false, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductAddComponent.subcategory', 'permissions.names.productAddComponentSubcategory', 2, 1, 0, false, -16);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -16, currval('web_control_seq'), 2);

-- products edit

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.name', 'permissions.names.productEditComponentName', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.code', 'permissions.names.productEditComponentCode', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.skuCode', 'permissions.names.productEditComponentSkuCode', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.manufacturer', 'permissions.names.productEditComponentManufacturer', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.supplier', 'permissions.names.productEditComponentSupplier', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.brand', 'permissions.names.productEditComponentBrand', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.purchasePrice', 'permissions.names.productEditComponentPurchasePrice', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.currency', 'permissions.names.productEditComponentCurrency', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.sellPriceNet', 'permissions.names.productEditComponentSellPriceNet', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.vat', 'permissions.names.productEditComponentVat', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.active', 'permissions.names.productEditComponentActive', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.description', 'permissions.names.productEditComponentDescription', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.specification', 'permissions.names.productEditComponentSpecification', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.notes', 'permissions.names.productEditComponentNotes', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.photos', 'permissions.names.productEditComponentPhotos', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.productPrices', 'permissions.names.productEditComponentProductPrices', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.category', 'permissions.names.productEditComponentCategory', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductEditComponent.subcategory', 'permissions.names.productEditComponentSubcategory', 2, 2, 0, false, -17);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -17, currval('web_control_seq'), 2);

-- price book actions

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookListComponent.savePriceBook', 'permissions.names.priceBookListSave', 2, 0, 1, false, -18);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -18, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookListComponent.deletePriceBook', 'permissions.names.priceBookListDelete', 2, 0, 1, false, -18);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -18, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookListComponent.editPriceBook', 'permissions.names.priceBookListEdit', 2, 0, 1, false, -18);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -18, currval('web_control_seq'), 2);

-- price book list

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookListComponent.name', 'permissions.names.priceBookListComponentName', 2, 0, 0, false, -18);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -18, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookListComponent.description', 'permissions.names.priceBookListComponentDescription', 2, 0, 0, false, -18);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -18, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookListComponent.updated', 'permissions.names.priceBookListComponentUpdated', 2, 0, 0, false, -18);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -18, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookListComponent.active', 'permissions.names.priceBookListComponentActive', 2, 0, 0, false, -18);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -18, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookListComponent.currency', 'permissions.names.priceBookListComponentCurrency', 2, 0, 0, false, -18);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -18, currval('web_control_seq'), 2);

-- price book add

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookAddComponent.name', 'permissions.names.priceBookAddComponentName', 2, 1, 0, true, -19);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -19, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookAddComponent.description', 'permissions.names.priceBookAddComponentDescription', 2, 1, 0, false, -19);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -19, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookAddComponent.currency', 'permissions.names.priceBookAddComponentCurrency', 2, 1, 0, false, -19);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -19, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookAddComponent.active', 'permissions.names.priceBookAddComponentActive', 2, 1, 0, false, -19);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -19, currval('web_control_seq'), 2);

-- price book edit

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookEditComponent.name', 'permissions.names.priceBookEditComponentName', 2, 2, 0, false, -20);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -20, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookEditComponent.description', 'permissions.names.priceBookEditComponentDescription', 2, 2, 0, false, -20);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -20, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookEditComponent.currency', 'permissions.names.priceBookEditComponentCurrency', 2, 2, 0, false, -20);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -20, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'PriceBookEditComponent.active', 'permissions.names.priceBookEditComponentActive', 2, 2, 0, false, -20);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -20, currval('web_control_seq'), 2);


-- products list in price book

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListInPriceBookComponent.name', 'permissions.names.productListInPriceBookComponentName', 2, 0, 0, false, -21);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -21, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListInPriceBookComponent.category', 'permissions.names.productListInPriceBookComponentCategory', 2, 0, 0, false, -21);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -21, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListInPriceBookComponent.subcategory', 'permissions.names.productListInPriceBookComponentSubcategory', 2, 0, 0, false, -21);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -21, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListInPriceBookComponent.code', 'permissions.names.productListInPriceBookComponentCode', 2, 0, 0, false, -21);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -21, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ProductListInPriceBookComponent.sellPriceNet', 'permissions.names.productListInPriceBookComponentSellPriceNet', 2, 0, 0, false, -21);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -21, currval('web_control_seq'), 2);

--calendar shared
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CalendarComponent.addTask', 'permissions.names.calendarAddTask', 2, 1, 1, false, -22);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -22, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CalendarComponent.editTask', 'permissions.names.calendarEditTask', 2, 2, 1, false, -22);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -22, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CalendarComponent.removeTask', 'permissions.names.calendarRemoveTask', 2, 3, 1, false, -22);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -22, currval('web_control_seq'), 2);

--calendar view
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CalendarComponent.tasks', 'permissions.names.calendarViewTasks', 1, 0, 0, false, -23);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -23, currval('web_control_seq'), 2);


--customer notes

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerNotes.view', 'permissions.names.customerNotesView', 1, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerNotes.add', 'permissions.names.customerNotesAdd', 2, 1, 1, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerNotes.edit', 'permissions.names.customerNotesEdit', 2, 2, 1, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerNotes.remove', 'permissions.names.customerNotesRemove', 2, 3, 1, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

--lead notes

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadNotes.view', 'permissions.names.leadNotesView', 1, 0, 0, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadNotes.add', 'permissions.names.leadNotesAdd', 2, 1, 1, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadNotes.edit', 'permissions.names.leadNotesEdit', 2, 2, 1, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadNotes.remove', 'permissions.names.leadNotesRemove', 2, 3, 1, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

-- lead files mini view
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadFilesMiniComponent.addFile', 'permissions.names.leadMiniFilesAddNew', 2, 1, 1, false, -24);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -24, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadFilesMiniComponent.view', 'permissions.names.leadMiniFilesView', 1, 0, 0, false, -24);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -24, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadFilesMiniComponent.showMore', 'permissions.names.leadMiniFilesShowMore', 2, 0, 1, false, -24);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -24, currval('web_control_seq'), 2);


-- lead files full view
--documents list in leads
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInLeadComponent.originalName', 'permissions.names.documentsListInLeadComponentOriginalName', 2, 0, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInLeadComponent.created', 'permissions.names.documentsListInLeadComponentCreated', 2, 0, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInLeadComponent.format', 'permissions.names.documentsListInLeadComponentFormat', 2, 0, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInLeadComponent.addedBy', 'permissions.names.documentsListInLeadComponentAddedBy', 2, 0, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInLeadComponent.contractName', 'permissions.names.documentsListInLeadComponentContractName', 2, 0, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInLeadComponent.status', 'permissions.names.documentsListInLeadComponentStatus', 2, 0, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

--document history in leads
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsHistoryInLeadComponent.changeDate', 'permissions.names.documentsHistoryInLeadComponentChangeDate', 2, 0, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsHistoryInLeadComponent.changeAuthor', 'permissions.names.documentsHistoryInLeadComponentChangeAuthor', 2, 0, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

-- documents edit in leads

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInLeadComponent.description', 'permissions.names.documentsEditInLeadComponentDescription', 2, 2, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInLeadComponent.tags', 'permissions.names.documentsEditInLeadComponentTags', 2, 2, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInLeadComponent.addedBy', 'permissions.names.documentsEditInLeadComponentAddedBy', 2, 2, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInLeadComponent.assignedTo', 'permissions.names.documentsEditInLeadComponentAssignedTo', 2, 2, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInLeadComponent.save', 'permissions.names.documentsEditInLeadComponentSave', 2, 2, 1, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);


-- documents add in leads

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddInLeadComponent.description', 'permissions.names.documentsAddInLeadComponentDescription', 2, 1, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddInLeadComponent.tags', 'permissions.names.documentsAddInLeadComponentTags', 2, 1, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddInLeadComponent.assignedTo', 'permissions.names.documentsAddInLeadComponentAssignedTo', 2, 1, 0, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddInLeadComponent.save', 'permissions.names.documentsAddInLeadComponentSave', 2, 1, 1, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);


-- document other actions in leads

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsInLeadComponent.delete', 'permissions.names.documentsInLeadComponentDelete', 2, 3, 1, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 0);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsInLeadComponent.uploadNewVersion', 'permissions.names.documentsInLeadComponentUploadNewVersion', 2, 2, 1, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsInLeadComponent.preview', 'permissions.names.documentsInLeadComponentPreview', 2, 0, 1, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsInLeadComponent.download', 'permissions.names.documentsInLeadComponentDownload', 2, 0, 1, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsInLeadComponent.showDetails', 'permissions.names.documentsInLeadComponentShowDetails', 2, 0, 1, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsInLeadComponent.showHistory', 'permissions.names.documentsInLeadComponentShowHistory', 2, 0, 1, false, -25);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -25, currval('web_control_seq'), 2);

--OPPORTUNITY
--shared

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityShared.edit', 'permissions.names.opportunityEdit', 2, 2, 1, false, -27);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -27, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityShared.delete', 'permissions.names.opportunityDelete', 2, 3, 1, false, -27);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -27, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityShared.changeOpportunityKeeper', 'permissions.names.opportunityChangeOpportunityKeeper', 2, 2, 1, false, -27);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -27, currval('web_control_seq'), 2);

--list
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityListComponent.nameColumn', 'permissions.names.opportunityListNameColumn', 2, 0, 0, false, -26);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -26, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityListComponent.customerNameColumn', 'permissions.names.opportunityListCompanyColumn', 2, 0, 0, false, -26);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -26, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityListComponent.finishDateColumn', 'permissions.names.opportunityListClosingDateColumn', 2, 0, 0, false, -26);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -26, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityListComponent.statusColumn', 'permissions.names.opportunityListStatusColumn', 2, 0, 0, false, -26);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -26, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityListComponent.keeperColumn', 'permissions.names.opportunityListOpportunityKeeperColumn', 2, 0, 0, false, -26);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -26, currval('web_control_seq'), 2);

-- create

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.save', 'permissions.names.opportunityCreateSave', 2, 1, 1, false, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.nameField', 'permissions.names.opportunityCreateNameField', 2, 1, 0, true, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.customerField', 'permissions.names.opportunityCreateCustomerField', 2, 1, 0, true, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.contactField', 'permissions.names.opportunityCreateContactField', 2, 1, 0, true, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.typeField', 'permissions.names.opportunityCreateTypeField', 2, 1, 0, false, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.amountField', 'permissions.names.opportunityCreateAmountField', 2, 1, 0, false, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.currencyField', 'permissions.names.opportunityCreateCurrencyField', 2, 1, 0, false, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.probabilityField', 'permissions.names.opportunityCreateProbabilityField', 2, 1, 0, false, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.finishDateField', 'permissions.names.opportunityCreateFinishDateField', 2, 1, 0, false, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.processDefinitionField', 'permissions.names.opportunityCreateProcessDefinitionField', 2, 1, 0, true, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.descriptionField', 'permissions.names.opportunityCreateDescriptionField', 2, 1, 0, false, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.sourceOfAcquisitionField', 'permissions.names.opportunityCreateSourceOfAcquisitionField', 2, 1, 0, true, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityCreateComponent.ownerField', 'permissions.names.opportunityCreateOwnerField', 2, 1, 0, true, -28);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -28, currval('web_control_seq'), 2);

-- edit
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.nameField', 'permissions.names.opportunityEditNameField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.customerField', 'permissions.names.opportunityEditCustomerField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.contactField', 'permissions.names.opportunityEditContactField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.typeField', 'permissions.names.opportunityEditTypeField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.amountField', 'permissions.names.opportunityEditAmountField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.currencyField', 'permissions.names.opportunityEditCurrencyField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.probabilityField', 'permissions.names.opportunityEditProbabilityField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.finishDateField', 'permissions.names.opportunityEditFinishDateField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.descriptionField', 'permissions.names.opportunityEditDescriptionField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.sourceOfAcquisitionField', 'permissions.names.opportunityEditSourceOfAcquisitionField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityEditComponent.ownerField', 'permissions.names.opportunityEditOwnerField', 2, 2, 0, false, -29);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -29, currval('web_control_seq'), 2);

-- OPPORTUNITY

--opportunity details


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.image', 'permissions.names.opportunityDetailsImage', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.mainInfo', 'permissions.names.opportunityDetailsMainInfo', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.owner', 'permissions.names.opportunityDetailsOwner', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.customer', 'permissions.names.opportunityDetailsCustomer', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.amount', 'permissions.names.opportunityDetailsAmount', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.finishDate', 'permissions.names.opportunityDetailsFinishDate', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.createOffer', 'permissions.names.opportunityDetailsCreateOffer', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.name', 'permissions.names.opportunityDetailsCardComponentName', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.finishDate', 'permissions.names.opportunityDetailsCardComponentFinishDate', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.customer', 'permissions.names.opportunityDetailsCardComponentCustomer', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.probability', 'permissions.names.opportunityDetailsCardComponentProbability', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.contact', 'permissions.names.opportunityDetailsCardComponentContact', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.amount', 'permissions.names.opportunityDetailsCardComponentAmount', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.currency', 'permissions.names.opportunityDetailsCardComponentCurrency', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.description', 'permissions.names.opportunityDetailsCardComponentDescription', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.createdBy', 'permissions.names.opportunityDetailsCardComponentCreatedBy', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsCardComponent.sourceOfAcquisition', 'permissions.names.opportunityDetailsCardComponentSourceOfAcquisition', 2, 2, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.refresh', 'permissions.names.opportunityDetailsRefresh', 2, 0, 1, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.completeTask', 'permissions.names.completeTask', 2, 0, 1, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.goBack', 'permissions.names.goBack', 2, 0, 1, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.previewTask', 'permissions.names.opportunityDetailsPreviewTask', 2, 0, 1, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.activity', 'permissions.names.opportunityDetailsActivity', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.history', 'permissions.names.opportunityDetailsHistory', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityDetailsComponent.expenses', 'permissions.names.opportunityDetailsExpenses', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

--opportunity notes

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityNotes.view', 'permissions.names.opportunityNotesView', 1, 0, 0, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityNotes.add', 'permissions.names.opportunityNotesAdd', 2, 1, 1, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityNotes.edit', 'permissions.names.opportunityNotesEdit', 2, 2, 1, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityNotes.remove', 'permissions.names.opportunityNotesRemove', 2, 3, 1, false, -32);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -32, currval('web_control_seq'), 2);

-- opportunity files mini view
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityFilesMiniComponent.addFile', 'permissions.names.opportunityMiniFilesAddNew', 2, 1, 1, false, -30);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -30, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityFilesMiniComponent.view', 'permissions.names.opportunityMiniFilesView', 1, 0, 0, false, -30);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -30, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityFilesMiniComponent.showMore', 'permissions.names.opportunityMiniFilesShowMore', 2, 0, 1, false, -30);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -30, currval('web_control_seq'), 2);


-- opportunity files full view
--documents list in opportunity
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInOpportunityComponent.originalName', 'permissions.names.documentsListInOpportunityComponentOriginalName', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInOpportunityComponent.created', 'permissions.names.documentsListInOpportunityComponentCreated', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInOpportunityComponent.format', 'permissions.names.documentsListInOpportunityComponentFormat', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInOpportunityComponent.addedBy', 'permissions.names.documentsListInOpportunityComponentAddedBy', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInOpportunityComponent.contractName', 'permissions.names.documentsListInOpportunityComponentContractName', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInOpportunityComponent.status', 'permissions.names.documentsListInOpportunityComponentStatus', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);
--document history in opportunity
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsHistoryInOpportunityComponent.changeDate', 'permissions.names.documentsHistoryInOpportunityComponentChangeDate', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsHistoryInOpportunityComponent.changeAuthor', 'permissions.names.documentsHistoryInOpportunityComponentChangeAuthor', 2, 0, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

-- documents edit in opportunity

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInOpportunityComponent.description', 'permissions.names.documentsEditInOpportunityComponentDescription', 2, 2, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInOpportunityComponent.tags', 'permissions.names.documentsEditInOpportunityComponentTags', 2, 2, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInOpportunityComponent.addedBy', 'permissions.names.documentsEditInOpportunityComponentAddedBy', 2, 2, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInOpportunityComponent.assignedTo', 'permissions.names.documentsEditInOpportunityComponentAssignedTo', 2, 2, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsEditInOpportunityComponent.save', 'permissions.names.documentsEditInOpportunityComponentSave', 2, 2, 1, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

-- documents add in opportunity

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddInOpportunityComponent.description', 'permissions.names.documentsAddInOpportunityComponentDescription', 2, 1, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddInOpportunityComponent.tags', 'permissions.names.documentsAddInOpportunityComponentTags', 2, 1, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddInOpportunityComponent.assignedTo', 'permissions.names.documentsAddInOpportunityComponentAssignedTo', 2, 1, 0, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsAddInOpportunityComponent.save', 'permissions.names.documentsAddInOpportunityComponentSave', 2, 1, 1, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

-- document other actions in opportunity

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsInOpportunityComponent.delete', 'permissions.names.documentsInOpportunityComponentDelete', 2, 3, 1, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 0);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsInOpportunityComponent.uploadNewVersion', 'permissions.names.documentsInOpportunityComponentUploadNewVersion', 2, 2, 1, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsInOpportunityComponent.preview', 'permissions.names.documentsInOpportunityComponentPreview', 2, 0, 1, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsInOpportunityComponent.download', 'permissions.names.documentsInOpportunityComponentDownload', 2, 0, 1, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsInOpportunityComponent.showDetails', 'permissions.names.documentsInOpportunityComponentShowDetails', 2, 0, 1, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsInOpportunityComponent.showHistory', 'permissions.names.documentsInOpportunityComponentShowHistory', 2, 0, 1, false, -31);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -31, currval('web_control_seq'), 2);

-- opportunity products

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.product.name', 'permissions.names.opportunityProductsCardProductName', 2, 0, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.category', 'permissions.names.opportunityProductsCardCategory', 2, 0, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.subcategory', 'permissions.names.opportunityProductsCardSubcategory', 2, 0, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.product.code', 'permissions.names.opportunityProductsCardProductCode', 2, 0, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.price', 'permissions.names.opportunityProductsCardPrice', 2, 0, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.quantity', 'permissions.names.opportunityProductsCardQuantity', 2, 0, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.priceSummed', 'permissions.names.opportunityProductsCardPriceSummed', 2, 0, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

-- actions

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.edit', 'permissions.names.opportunityProductsEdit', 2, 2, 1, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.delete', 'permissions.names.opportunityProductsDelete', 2, 3, 1, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.manualAdd', 'permissions.names.opportunityProductsManualAdd', 2, 1, 1, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.addFromPriceBook', 'permissions.names.opportunityProductsAddFromPriceBook', 2, 1, 1, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

-- add from price book - list

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'AddProductFromPriceBookComponent.name', 'permissions.names.addProductFromPriceBookComponentName', 2, 0, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'AddProductFromPriceBookComponent.category', 'permissions.names.addProductFromPriceBookComponentCategory', 2, 0, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'AddProductFromPriceBookComponent.subcategory', 'permissions.names.addProductFromPriceBookComponentSubcategory', 2, 0, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'AddProductFromPriceBookComponent.code', 'permissions.names.addProductFromPriceBookComponentCode', 2, 0, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'AddProductFromPriceBookComponent.sellPriceNet', 'permissions.names.addProductFromPriceBookComponentSellPriceNet', 2, 0, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

-- opportunity product manual add

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.name', 'permissions.names.opportunityProductsName', 2, 1, 0, true, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.code', 'permissions.names.opportunityProductsCode', 2, 1, 0, true, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.skuCode', 'permissions.names.opportunityProductsSkuCode', 2, 1, 0, true, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.categoryIds', 'permissions.names.opportunityProductsCategoryIds', 2, 1, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.subcategoryIds', 'permissions.names.opportunityProductsSubcategoryIds', 2, 1, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.supplier', 'permissions.names.opportunityProductsSupplier', 2, 1, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.manufacturer', 'permissions.names.opportunityProductsManufacturer', 2, 1, 0, false, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.purchasePrice', 'permissions.names.opportunityProductsPurchasePrice', 2, 1, 0, true, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.sellPrice', 'permissions.names.opportunityProductsSellPrice', 2, 1, 0, true, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'OpportunityProductsCard.quantity', 'permissions.names.opportunityProductsQuantity', 2, 1, 0, true, -33);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -33, currval('web_control_seq'), 2);

-- lead to opportunity conversion

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadConversionComponent.convert', 'permissions.names.leadConversionComponentConvert', 2, 1, 1, false, -5);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -5, currval('web_control_seq'), 2);

-- contact details

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactDetailsComponent.image', 'permissions.names.contactDetailsImage', 1, 0, 0, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactDetailsComponent.name', 'permissions.names.contactDetailsName', 1, 0, 0, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactDetailsComponent.position', 'permissions.names.contactDetailsPosition', 1, 0, 0, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactDetailsComponent.customer', 'permissions.names.contactDetailsCustomer', 1, 0, 0, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactDetailsComponent.phone', 'permissions.names.contactDetailsPhone', 1, 0, 0, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactDetailsComponent.email', 'permissions.names.contactDetailsEmail', 1, 0, 0, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactDetailsComponent.created', 'permissions.names.contactDetailsCreated', 1, 0, 0, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactDetailsComponent.owner', 'permissions.names.contactDetailsOwner', 1, 0, 0, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 2);

-- contact notes

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactNotes.view', 'permissions.names.contactNotesView', 1, 0, 0, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactNotes.add', 'permissions.names.contactNotesAdd', 2, 1, 1, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactNotes.edit', 'permissions.names.contactNotesEdit', 2, 2, 1, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactNotes.remove', 'permissions.names.contactNotesRemove', 2, 3, 1, false, -34);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -34, currval('web_control_seq'), 2);

-- contact links add
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkAddComponent.contact', 'permissions.names.addContactLinkContact', 2, 1, 0, true, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkAddComponent.customer', 'permissions.names.addContactLinkCustomer', 2, 1, 0, false, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkAddComponent.note', 'permissions.names.addContactLinkNote', 2, 1, 0, false, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkAddComponent.add', 'permissions.names.addContactLinkAdd', 2, 1, 1, false, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 2);

-- contact links edit
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkEditComponent.contact', 'permissions.names.editContactLinkContact', 1, 2, 0, true, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkEditComponent.customer', 'permissions.names.editContactLinkCustomer', 1, 2, 0, true, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkEditComponent.note', 'permissions.names.editContactLinkNote', 2, 2, 0, false, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 2);

-- contact links list

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkListComponent.contactColumn', 'permissions.names.contactLinkListContactColumn', 2, 0, 0, false, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkListComponent.customerColumn', 'permissions.names.contactLinkListCustomerColumn', 2, 0, 0, false, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkListComponent.positionColumn', 'permissions.names.contactLinkListPositionColumn', 2, 0, 0, false, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkListComponent.phoneColumn', 'permissions.names.contactLinkListPhoneColumn', 2, 0, 0, false, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkListComponent.emailColumn', 'permissions.names.contactLinkListEmailColumn', 2, 0, 0, false, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkListComponent.notePanel', 'permissions.names.contactLinkNotePanel', 2, 0, 0, false, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkListComponent.remove', 'permissions.names.addContactLinkRemove', 2, 1, 1, false, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactLinkListComponent.edit', 'permissions.names.addContactLinkEdit', 2, 1, 1, false, -35);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -35, currval('web_control_seq'), 2);

--ContactSalesOpportunityCardComponent

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactSalesOpportunityCardComponent.card', 'permissions.names.contactSalesOpportunityCard', 1, 0, 0, false, -36);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -36, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactSalesOpportunityCardComponent.name', 'permissions.names.contactSalesOpportunityName', 1, 0, 0, false, -36);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -36, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactSalesOpportunityCardComponent.status', 'permissions.names.contactSalesOpportunityStatus', 1, 0, 0, false, -36);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -36, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactSalesOpportunityCardComponent.amount', 'permissions.names.contactSalesOpportunityAmount', 1, 0, 0, false, -36);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -36, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactSalesOpportunityCardComponent.finishDate', 'permissions.names.contactSalesOpportunityFinishDate', 1, 0, 0, false, -36);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -36, currval('web_control_seq'), 1);


-- contact in contact address tab

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactInContactInfoComponent.save', 'permissions.names.contactInContactInfoSave', 2, 2, 1, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactInContactInfoComponent.nameField', 'permissions.names.contactInContactInfoNameField', 2, 2, 0, true, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactInContactInfoComponent.surnameField', 'permissions.names.contactInContactInfoSurnameField', 2, 2, 0, true, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactInContactInfoComponent.positionField', 'permissions.names.contactInContactInfoPositionField', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactInContactInfoComponent.companyField', 'permissions.names.contactInContactInfoCompanyField', 2, 2, 0, true, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactInContactInfoComponent.ownerInfo', 'permissions.names.contactInContactInfoOwnerInfo', 2, 2, 0, true, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactInContactInfoComponent.phoneField', 'permissions.names.contactInContactInfoPhoneField', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactInContactInfoComponent.emailField', 'permissions.names.contactInContactInfoEmailField', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ContactInContactInfoComponent.socialMediaField', 'permissions.names.contactInContactInfoSocialMediaField', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

-- customer in contact address tab

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerInContactInfoComponent.nipField', 'permissions.names.customerInContactInfoNipField', 2, 2, 0, true, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerInContactInfoComponent.regonField', 'permissions.names.customerInContactInfoRegonField', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerInContactInfoComponent.nameField', 'permissions.names.customerInContactInfoNameField', 2, 2, 0, true, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerInContactInfoComponent.phoneField', 'permissions.names.customerInContactInfoPhoneField', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerInContactInfoComponent.countryField', 'permissions.names.customerInContactInfoCountryField', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerInContactInfoComponent.cityField', 'permissions.names.customerInContactInfoCityField', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerInContactInfoComponent.zipCodeField', 'permissions.names.customerInContactInfoZipCodeField', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerInContactInfoComponent.streetField', 'permissions.names.customerInContactInfoStreetField', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerInContactInfoComponent.save', 'permissions.names.customerInContactInfoSave', 2, 2, 1, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerInContactInfoComponent.websiteField', 'permissions.names.customerInContactInfoWebsiteField', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

--whitelist

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'WhitelistStatusInCustomer.view', 'permissions.names.whitelistInCustomerView', 1, 0, 0, false, -38);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -38, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'WhitelistStatusInCustomer.refresh', 'permissions.names.whitelistInCustomerRefresh', 2, 0, 1, false, -38);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -38, currval('web_control_seq'), 2);

--expenses

--list
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseListComponent.date', 'permissions.names.expenseListDate', 1, 0, 0, false, -39);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -39, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseListComponent.name', 'permissions.names.expenseListName', 1, 0, 0, false, -39);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -39, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseListComponent.description', 'permissions.names.expenseListDescription', 1, 0, 0, false, -39);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -39, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseListComponent.amount', 'permissions.names.expenseListAmount', 1, 0, 0, false, -39);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -39, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseListComponent.type', 'permissions.names.expenseListType', 1, 0, 0, false, -39);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -39, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseListComponent.status', 'permissions.names.expenseListStatus', 1, 0, 0, false, -39);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -39, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseListComponent.pin', 'permissions.names.expenseListPin', 1, 0, 0, false, -39);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -39, currval('web_control_seq'), 2);


insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseListComponent.edit', 'permissions.names.expenseListEdit', 1, 2, 1, false, -39);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -39, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseListComponent.add', 'permissions.names.expenseListAdd', 1, 1, 1, false, -39);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -39, currval('web_control_seq'), 2);

-- add expense
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseAddComponent.name', 'permissions.names.expenseAddName', 2, 1, 0, true, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseAddComponent.description', 'permissions.names.expenseAddDescription', 2, 1, 0, false, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseAddComponent.date', 'permissions.names.expenseAddDate', 2, 1, 0, false, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseAddComponent.amount', 'permissions.names.expenseAddAmount', 2, 1, 0, true, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseAddComponent.currency', 'permissions.names.expenseAddCurrency', 2, 1, 0, true, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseAddComponent.type', 'permissions.names.expenseAddType', 2, 1, 0, false, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseAddComponent.activity', 'permissions.names.expenseAddActivity', 2, 1, 0, false, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

-- edit expense
insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseEditComponent.name', 'permissions.names.expenseEditName', 2, 2, 0, true, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseEditComponent.description', 'permissions.names.expenseEditDescription', 2, 2, 0, false, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseEditComponent.date', 'permissions.names.expenseEditDate', 2, 2, 0, false, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseEditComponent.amount', 'permissions.names.expenseEditAmount', 2, 2, 0, true, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseEditComponent.currency', 'permissions.names.expenseEditCurrency', 2, 2, 0, true, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseEditComponent.type', 'permissions.names.expenseEditType', 2, 2, 0, false, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'ExpenseEditComponent.activity', 'permissions.names.expenseEditActivity', 2, 2, 0, false, -40);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -40, currval('web_control_seq'), 2);

-- related customers

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'RelatedCustomerInContactDetail.customerPreview', 'permissions.names.relatedCustomerInContactDetailCustomerPreview', 1, 0, 0, false, -41);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -41, currval('web_control_seq'), 1);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'RelatedCustomerInContactDetail.view', 'permissions.names.relatedCustomerInContactDetailCustomerView', 2, 0, 1, false, -41);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -41, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'RelatedCustomerInContactDetail.edit', 'permissions.names.relatedCustomerInContactDetailCustomerEdit', 2, 2, 1, false, -41);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -41, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'RelatedCustomerInContactDetail.add', 'permissions.names.relatedCustomerInContactDetailCustomerAdd', 2, 1, 1, false, -41);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -41, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'RelatedCustomerInContactDetail.delete', 'permissions.names.relatedCustomerInContactDetailCustomerDelete', 2, 3, 1, false, -41);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -41, currval('web_control_seq'), 2);



-- administration panel

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'NavigationComponent.settingItem.administration', 'permissions.names.settingListAdministration', 2, 0, 1, false, -42);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -42, currval('web_control_seq'), 2);

-- products list

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'GoalTableComponent.name', 'permissions.names.goalTableComponentName', 2, 0, 0, false, -43);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -43, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'GoalTableComponent.category', 'permissions.names.goalTableComponentCategory', 2, 0, 0, false, -43);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -43, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'GoalTableComponent.type', 'permissions.names.goalTableComponentType', 2, 0, 0, false, -43);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -43, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'GoalTableComponent.interval', 'permissions.names.goalTableComponentInterval', 2, 0, 0, false, -43);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -43, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'GoalTableComponent.intervalNumber', 'permissions.names.goalTableComponentIntervalNumber', 2, 0, 0, false, -43);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -43, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'GoalTableComponent.startIndex', 'permissions.names.goalTableComponentStartIndex', 2, 0, 0, false, -43);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -43, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'GoalTableComponent.startYear', 'permissions.names.goalTableComponentStartYear', 2, 0, 0, false, -43);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -43, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'GoalTableComponent.goalCompleted', 'permissions.names.goalTableComponentGoalCompleted', 2, 0, 0, false, -43);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -43, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'GoalTableComponent.deficitOrExcess', 'permissions.names.goalTableComponentDeficitOrExcess', 2, 0, 0, false, -43);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -43, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'GoalTableComponent.addGoal', 'permissions.names.goalTableComponentAddGoal', 2, 1, 1, false, -43);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -43, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'GoalTableComponent.editGoal', 'permissions.names.goalTableComponentEditGoal', 2, 2, 1, false, -43);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -43, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'GoalTableComponent.deleteGoal', 'permissions.names.goalTableComponentDeleteGoal', 2, 3, 1, false, -43);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -43, currval('web_control_seq'), 2);
