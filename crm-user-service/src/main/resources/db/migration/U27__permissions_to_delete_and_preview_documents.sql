DELETE FROM permission WHERE id = (SELECT id FROM web_control WHERE frontend_id = 'LeadFilesMiniComponent.delete');
DELETE FROM web_control WHERE frontend_id = 'LeadFilesMiniComponent.delete';
DELETE FROM permission WHERE id = (SELECT id FROM web_control WHERE frontend_id = 'LeadFilesMiniComponent.preview');
DELETE FROM web_control WHERE frontend_id = 'LeadFilesMiniComponent.preview';

DELETE FROM permission WHERE id = (SELECT id FROM web_control WHERE frontend_id = 'OpportunityFilesMiniComponent.delete');
DELETE FROM web_control WHERE frontend_id = 'OpportunityFilesMiniComponent.delete';
DELETE FROM permission WHERE id = (SELECT id FROM web_control WHERE frontend_id = 'OpportunityFilesMiniComponent.preview');
DELETE FROM web_control WHERE frontend_id = 'OpportunityFilesMiniComponent.preview';

DELETE FROM permission WHERE id = (SELECT id FROM web_control WHERE frontend_id = 'DocumentsListComponent.delete');
DELETE FROM web_control WHERE frontend_id = 'DocumentsListComponent.delete';
DELETE FROM permission WHERE id = (SELECT id FROM web_control WHERE frontend_id = 'DocumentsListComponent.preview');
DELETE FROM web_control WHERE frontend_id = 'DocumentsListComponent.preview';

DELETE FROM permission WHERE id = (SELECT id FROM web_control WHERE frontend_id = 'ProductFilesMiniComponent.delete');
DELETE FROM web_control WHERE frontend_id = 'ProductFilesMiniComponent.delete';
DELETE FROM permission WHERE id = (SELECT id FROM web_control WHERE frontend_id = 'ProductFilesMiniComponent.preview');
DELETE FROM web_control WHERE frontend_id = 'ProductFilesMiniComponent.preview';

update web_control
set module_id = -30
where frontend_id in (
     'ProductFilesMiniComponent.addFile',
     'ProductFilesMiniComponent.showMore',
     'ProductFilesMiniComponent.view'
    );

DELETE FROM module_permission WHERE id = (SELECT id FROM module WHERE name = 'permissions.module.productMiniFiles');
DELETE FROM module WHERE name = 'permissions.module.productMiniFiles';
