insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerAddComponent.linkedInField', 'permissions.names.customerAddLinkedInField', 2, 1, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerEditComponent.linkedInField', 'permissions.names.customerEditLinkedInField', 2, 2, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerInContactInfoComponent.linkedInField', 'permissions.names.customerInContactInfoLinkedInField', 2, 2, 0, false, -37);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -37, currval('web_control_seq'), 2);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'CustomerDetailsComponent.linkedIn', 'permissions.names.customerDetailsComponentLinkedIn', 2, 0, 0, false, -3);
insert into permission(id, module_permission_id, web_control_id, state) values (nextval('permission_seq'), -3, currval('web_control_seq'), 2);
