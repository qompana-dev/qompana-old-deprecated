insert into person(id, name, surname, description, phone, email, password)
values(-100, 'Admin', 'Admin', 'Admin user', '48111111111', 'admin@admin.com', '$2a$10$KTAR2efAl6zocmh5KqX.jOdrYOxlbys/RDvFnckvg/cQqUJ4HLlw2');

insert into role(id, name) values(-100, 'Admin');
insert into profile2role(profile_id, role_id) values (-1, -100);

insert into account(id, person_id, activated, deleted, version, role_id)
values(-100, -100, true, false, -1, -100);

insert into account_configuration(id, account_id, locale, time_zone)
values(-100, -100, 'pl', 'Europe/Warsaw');
