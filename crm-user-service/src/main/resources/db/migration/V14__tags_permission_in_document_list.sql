insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListComponent.tags', 'permissions.names.documentsListComponentTags', 2, 0, 0, false, -12);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInLeadComponent.tags', 'permissions.names.documentsListInLeadComponentTags', 2, 0, 0, false, -25);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'DocumentsListInOpportunityComponent.tags', 'permissions.names.documentsListInOpportunityComponentTags', 2, 0, 0, false, -31);
