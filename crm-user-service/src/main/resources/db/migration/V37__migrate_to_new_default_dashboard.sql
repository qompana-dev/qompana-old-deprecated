TRUNCATE TABLE dashboard_widget;
DO
$$
    DECLARE
        account_id bigint;
    BEGIN
        FOR account_id IN (select id
                           from account)
            LOOP
                --CALENDAR--
                insert into dashboard_widget(id, account_id, rows, cols, widget_type_enum, widget_id, x, y)
                values (nextval('dashboard_widget_seq'), account_id, 18, 3, 2, null, 12, 0);

                --NOTE--
                insert into dashboard_widget(id, account_id, rows, cols, widget_type_enum, widget_id, x, y)
                values (nextval('dashboard_widget_seq'), account_id, 6, 4, 3, null, 8, 12);

                --TOTAL_SALES_OPPORTUNITY--
                insert into dashboard_widget(id, account_id, rows, cols, widget_type_enum, widget_id, x, y)
                values (nextval('dashboard_widget_seq'), account_id, 7, 3, 6, null, 0, 0);

                --OPENED_SALES_OPPORTUNITY--
                insert into dashboard_widget(id, account_id, rows, cols, widget_type_enum, widget_id, x, y)
                values (nextval('dashboard_widget_seq'), account_id, 11, 8, 7, null, 0, 7);

                --LEAD_SOURCES--
                insert into dashboard_widget(id, account_id, rows, cols, widget_type_enum, widget_id, x, y)
                values (nextval('dashboard_widget_seq'), account_id, 7, 5, 8, null, 3, 0);

                --NOTIFICATIONS--
                insert into dashboard_widget(id, account_id, rows, cols, widget_type_enum, widget_id, x, y)
                values (nextval('dashboard_widget_seq'), account_id, 12, 4, 9, null, 8, 0);

            end loop;

    end;
$$;

CREATE OR REPLACE FUNCTION set_default_dashboard() RETURNS trigger AS $set_default_dashboard$
BEGIN
    --CALENDAR--
    insert into dashboard_widget(id, account_id, rows, cols, widget_type_enum, widget_id, x, y)
    values (nextval('dashboard_widget_seq'), NEW.id, 18, 3, 2, null, 12, 0);

    --NOTE--
    insert into dashboard_widget(id, account_id, rows, cols, widget_type_enum, widget_id, x, y)
    values (nextval('dashboard_widget_seq'), NEW.id, 6, 4, 3, null, 8, 12);

    --TOTAL_SALES_OPPORTUNITY--
    insert into dashboard_widget(id, account_id, rows, cols, widget_type_enum, widget_id, x, y)
    values (nextval('dashboard_widget_seq'), NEW.id, 7, 3, 6, null, 0, 0);

    --OPENED_SALES_OPPORTUNITY--
    insert into dashboard_widget(id, account_id, rows, cols, widget_type_enum, widget_id, x, y)
    values (nextval('dashboard_widget_seq'), NEW.id, 11, 8, 7, null, 0, 7);

    --LEAD_SOURCES--
    insert into dashboard_widget(id, account_id, rows, cols, widget_type_enum, widget_id, x, y)
    values (nextval('dashboard_widget_seq'), NEW.id, 7, 5, 8, null, 3, 0);

    --NOTIFICATIONS--
    insert into dashboard_widget(id, account_id, rows, cols, widget_type_enum, widget_id, x, y)
    values (nextval('dashboard_widget_seq'), NEW.id, 12, 4, 9, null, 8, 0);

    RETURN NEW;
END;
$set_default_dashboard$ LANGUAGE plpgsql;

CREATE TRIGGER default_dashboard_for_new_user
    AFTER INSERT ON account
    FOR ROW EXECUTE PROCEDURE set_default_dashboard();
