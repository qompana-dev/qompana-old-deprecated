insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadShared.import', 'permissions.names.leadImport', 2, 1, 1, false, -10);

insert into web_control (id, frontend_id, name, default_state, depends_on, type, required, module_id)
values (nextval('web_control_seq'), 'LeadShared.export', 'permissions.names.leadExport', 2, 0, 1, false, -10);
