package com.devqube.crmuserservice;

import com.devqube.crmshared.license.LicenseCheck;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;
import java.io.InputStream;

@EnableEurekaClient
@SpringBootApplication
@Slf4j
@Import({LicenseCheck.class})
@EnableFeignClients(basePackages = {"com.devqube.crmuserservice", "com.devqube.crmshared.association", "com.devqube.crmshared.file"})
public class CrmUserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrmUserServiceApplication.class, args);
    }

    @Bean
    @Primary
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public ModelMapper strictModelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        return modelMapper;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public String defaultMenuOrderJson(@Value("classpath:DefaultMenu.json") Resource resource) {
        try (InputStream is = resource.getInputStream()) {
            log.debug("Default menu order loaded. ");
            return IOUtils.toString(is);
        } catch (IOException exc) {
            log.error("IOException while getting default menu tabs", exc);
        }
        return null;
    }
}
