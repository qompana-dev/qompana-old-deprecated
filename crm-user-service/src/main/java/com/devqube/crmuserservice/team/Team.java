package com.devqube.crmuserservice.team;

import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.permissions.profile.Profile;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Team {
    @Id
    @SequenceGenerator(name="team_seq", sequenceName="team_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="team_seq")
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "teams")
    private Set<Account> accounts;

    @ManyToMany
    @JoinTable(
            name = "team2profile",
            joinColumns = @JoinColumn(name = "team_id"),
            inverseJoinColumns = @JoinColumn(name = "profile_id")
    )
    private Set<Profile> profiles;
}
