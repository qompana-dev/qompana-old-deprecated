package com.devqube.crmuserservice.widgetSettings;

import com.devqube.crmshared.dashboard.dto.DashboardWidgetDto;
import com.devqube.crmshared.dashboard.dto.SingleDashboardWidgetDto;
import com.devqube.crmshared.dashboard.model.WidgetTypeEnum;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.devqube.crmuserservice.account.AccountsRepository;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.globalConfiguration.integrations.BusinessClient;
import com.devqube.crmuserservice.widgetSettings.model.DashboardWidget;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class WidgetSettingsService {
    private final AccountsRepository accountsRepository;
    private final DashboardWidgetRepository dashboardWidgetRepository;
    private final ModelMapper strictModelMapper;
    private final BusinessClient businessClient;

    public WidgetSettingsService(AccountsRepository accountsRepository, DashboardWidgetRepository dashboardWidgetRepository, @Qualifier("strictModelMapper") ModelMapper strictModelMapper, BusinessClient businessClient) {
        this.accountsRepository = accountsRepository;
        this.dashboardWidgetRepository = dashboardWidgetRepository;
        this.strictModelMapper = strictModelMapper;
        this.businessClient = businessClient;
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class})
    public List<DashboardWidgetDto> swapWidgets(List<DashboardWidgetDto> widgetDtoList) throws EntityNotFoundException {
        Account byPersonEmail = accountsRepository.findByPersonEmail(CurrentRequestUtil.getLoggedInAccountEmail());
        if (byPersonEmail == null) {
            throw new EntityNotFoundException();
        }

        Map<WidgetTypeEnum, Set<Long>> widgetsToRemove = getWidgetsToRemove(widgetDtoList, byPersonEmail);

        dashboardWidgetRepository.deleteAllByAccountId(byPersonEmail.getId());
        dashboardWidgetRepository.flush();
        List<DashboardWidget> toSave = widgetDtoList.stream().map(c -> toModel(c, byPersonEmail, strictModelMapper)).collect(Collectors.toList());
        List<DashboardWidget> result = dashboardWidgetRepository.saveAll(toSave);
        widgetsToRemove.keySet().forEach(c -> removeByIds(widgetsToRemove.get(c), c));
        return result.stream().map(c -> strictModelMapper.map(c, DashboardWidgetDto.class)).collect(Collectors.toList());
    }

    public List<DashboardWidgetDto> getDashboard() throws EntityNotFoundException {
        Account byPersonEmail = accountsRepository.findByPersonEmail(CurrentRequestUtil.getLoggedInAccountEmail());
        if (byPersonEmail == null) {
            throw new EntityNotFoundException();
        }
        List<DashboardWidget> result = dashboardWidgetRepository.findAllByAccountId(byPersonEmail.getId());
        return result.stream().map(c -> strictModelMapper.map(c, DashboardWidgetDto.class)).collect(Collectors.toList());
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class})
    public void removeWidgetById(Long dashboardWidgetId) throws EntityNotFoundException {
        Account byPersonEmail = accountsRepository.findByPersonEmail(CurrentRequestUtil.getLoggedInAccountEmail());
        if (byPersonEmail == null) {
            throw new EntityNotFoundException();
        }
        Optional<DashboardWidget> current = dashboardWidgetRepository.findById(dashboardWidgetId);
        dashboardWidgetRepository.deleteByAccountIdAndId(byPersonEmail.getId(), dashboardWidgetId);
        current.ifPresent(dashboardWidget -> removeByIds(new HashSet<>(Collections.singletonList(dashboardWidget.getWidgetId())), dashboardWidget.getWidgetTypeEnum()));
    }

    @Transactional(rollbackFor = {BadRequestException.class, EntityNotFoundException.class})
    public DashboardWidgetDto saveWidget(SingleDashboardWidgetDto singleDashboardWidgetDto) throws BadRequestException, EntityNotFoundException {
        Account byPersonEmail = accountsRepository.findByPersonEmail(CurrentRequestUtil.getLoggedInAccountEmail());
        if (byPersonEmail == null) {
            throw new EntityNotFoundException();
        }
        //TODO(VALIDATE is it possible to add a widget. Does he intersect with someone )
        DashboardWidget result = dashboardWidgetRepository.save(toModel(singleDashboardWidgetDto, byPersonEmail, strictModelMapper));
        return strictModelMapper.map(result, DashboardWidgetDto.class);
    }

    private Map<WidgetTypeEnum, Set<Long>> getWidgetsToRemove(List<DashboardWidgetDto> widgetDtoList, Account byPersonEmail) {
        Set<Long> newIds = widgetDtoList.stream().map(DashboardWidgetDto::getWidgetId).collect(Collectors.toSet());
        List<DashboardWidget> allByAccountId = dashboardWidgetRepository.findAllByAccountId(byPersonEmail.getId());
        Set<Long> toRemove = dashboardWidgetRepository.findAllByAccountId(byPersonEmail.getId()).stream()
                .map(DashboardWidget::getWidgetId).filter(Objects::nonNull)
                .filter(c -> !newIds.contains(c)).collect(Collectors.toSet());
        Map<WidgetTypeEnum, Set<Long>> result = new HashMap<>();
        Arrays.stream(WidgetTypeEnum.values()).forEach(c -> result.put(c, getWidgetToRemoveByWidgetType(toRemove, allByAccountId, c)));
        return result;
    }

    private Set<Long> getWidgetToRemoveByWidgetType(Set<Long> toRemove, List<DashboardWidget> allByAccountId, WidgetTypeEnum type) {
        Set<Long> widgetsByType = allByAccountId.stream().filter(c -> c.getWidgetTypeEnum().equals(type)).map(DashboardWidget::getWidgetId).collect(Collectors.toSet());
        return toRemove.stream().filter(widgetsByType::contains).collect(Collectors.toSet());
    }


    private DashboardWidget toModel(SingleDashboardWidgetDto singleDashboardWidgetDto, Account account, ModelMapper modelMapper) {
        DashboardWidget dashboardWidget = modelMapper.map(singleDashboardWidgetDto, DashboardWidget.class);
        dashboardWidget.setAccount(account);
        return dashboardWidget;
    }


    private DashboardWidget toModel(DashboardWidgetDto dashboardWidgetDto, Account account, ModelMapper modelMapper) {
        DashboardWidget dashboardWidget = modelMapper.map(dashboardWidgetDto, DashboardWidget.class);
        dashboardWidget.setAccount(account);
        return dashboardWidget;
    }

    private void removeByIds(Set<Long> toRemove, WidgetTypeEnum widgetTypeEnum) {
        if (toRemove != null && toRemove.size() > 0) {
            switch (widgetTypeEnum) {
                case GOAL:
                    businessClient.deleteGoalWidgets(toRemove);
                    break;
                case REPORT:
                    businessClient.deleteReportWidgets(toRemove);
            }
        }
    }
}
