package com.devqube.crmuserservice.widgetSettings.web;

import com.devqube.crmshared.dashboard.dto.DashboardWidgetDto;
import com.devqube.crmshared.dashboard.dto.SingleDashboardWidgetDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface WidgetSettingsApi {

    @ApiOperation(value = "Save dashboard")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Dashboard updated successfully", response = DashboardWidgetDto[].class),
            @ApiResponse(code = 404, message = "User not found")
    })
    ResponseEntity<List<DashboardWidgetDto>> swapWidgets(List<DashboardWidgetDto> widgetDtoList) throws EntityNotFoundException;

    @ApiOperation(value = "Save widget on position")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Widget saved successfully", response = DashboardWidgetDto.class),
            @ApiResponse(code = 400, message = "incorrect row or column"),
            @ApiResponse(code = 404, message = "User not found")
    })
    ResponseEntity<DashboardWidgetDto> saveWidget(SingleDashboardWidgetDto singleDashboardWidgetDto)
            throws BadRequestException, EntityNotFoundException;

    @ApiOperation(value = "Remove widget on position")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Widget removed successfully"),
            @ApiResponse(code = 404, message = "User not found")
    })
    ResponseEntity<Void> removeWidgetById(Long dashboardWidgetId) throws EntityNotFoundException;

    @ApiOperation(value = "Get my dashboard")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Dashboard retrieved successfully", response = DashboardWidgetDto[].class),
            @ApiResponse(code = 404, message = "User not found")
    })
    ResponseEntity<List<DashboardWidgetDto>> getMyDashboard() throws EntityNotFoundException;
}
