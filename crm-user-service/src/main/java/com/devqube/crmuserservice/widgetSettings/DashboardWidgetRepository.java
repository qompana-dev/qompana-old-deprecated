package com.devqube.crmuserservice.widgetSettings;

import com.devqube.crmuserservice.widgetSettings.model.DashboardWidget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DashboardWidgetRepository extends JpaRepository<DashboardWidget, Long> {
    void deleteAllByAccountId(Long accountId);

    void deleteAllByAccountIdAndRowsAndCols(Long accountId, Integer row, Integer column);

    List<DashboardWidget> findAllByAccountIdAndRowsAndCols(Long accountId, Integer row, Integer column);

    List<DashboardWidget> findAllByAccountId(Long accountId);

    void deleteByAccountIdAndId(Long accountId, Long dashboardWidgetId);
}
