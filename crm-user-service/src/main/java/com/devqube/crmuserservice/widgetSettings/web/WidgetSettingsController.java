package com.devqube.crmuserservice.widgetSettings.web;

import com.devqube.crmshared.dashboard.dto.DashboardWidgetDto;
import com.devqube.crmshared.dashboard.dto.SingleDashboardWidgetDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmuserservice.widgetSettings.WidgetSettingsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/widget")
@Slf4j
public class WidgetSettingsController implements WidgetSettingsApi {
    private final WidgetSettingsService widgetSettingsService;

    public WidgetSettingsController(WidgetSettingsService widgetSettingsService) {
        this.widgetSettingsService = widgetSettingsService;
    }

    @PutMapping("/dashboard/swap") // TODO [PW] 08.01.2020: add permissions
    public ResponseEntity<List<DashboardWidgetDto>> swapWidgets(@RequestBody List<DashboardWidgetDto> widgetDtoList) throws EntityNotFoundException {
        return ResponseEntity.ok(widgetSettingsService.swapWidgets(widgetDtoList));
    }

    @PostMapping("/dashboard") // TODO [PW] 08.01.2020: add permissions
    public ResponseEntity<DashboardWidgetDto> saveWidget(@RequestBody SingleDashboardWidgetDto widgetDto) throws BadRequestException, EntityNotFoundException {
        return ResponseEntity.ok(widgetSettingsService.saveWidget(widgetDto));
    }

    @DeleteMapping("/dashboard/id/{id}") // TODO [PW] 08.01.2020: add permissions
    public ResponseEntity<Void> removeWidgetById(@PathVariable("id") Long dashboardWidgetId) throws EntityNotFoundException {
        widgetSettingsService.removeWidgetById(dashboardWidgetId);
        return ResponseEntity.noContent().build();
    }

    @Override
    @GetMapping("/dashboard") // TODO [PW] 08.01.2020: add permissions
    public ResponseEntity<List<DashboardWidgetDto>> getMyDashboard() throws EntityNotFoundException {
        return ResponseEntity.ok(widgetSettingsService.getDashboard());
    }
}
