package com.devqube.crmuserservice.widgetSettings.model;

import com.devqube.crmshared.dashboard.model.WidgetTypeEnum;
import com.devqube.crmuserservice.account.model.Account;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DashboardWidget {
    @Id
    @SequenceGenerator(name = "dashboard_widget_seq", sequenceName = "dashboard_widget_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dashboard_widget_seq")
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(
            name = "account_id",
            referencedColumnName = "id")
    private Account account;

    @Min(2)
    @Max(24)
    @NotNull
    private Integer rows;

    @Min(2)
    @Max(24)
    @NotNull
    private Integer cols;

    @Min(0)
    @Max(23)
    @NotNull
    private Integer x;

    @Min(0)
    @Max(23)
    @NotNull
    private Integer y;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    private WidgetTypeEnum widgetTypeEnum;

    private Long widgetId;
}
