package com.devqube.crmuserservice.permissions.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);
    @Query(value = "select * from role r left join role_children c on r.id = c.child_id where c.child_id is null", nativeQuery = true)
    List<Role> findAllOriginRoles();

    @Modifying
    @Query(value = "insert into role_children(parent_id, child_id) values(?1, ?2)", nativeQuery = true)
    @Transactional
    void addParent(Long parentId, Long childId);

    @Modifying
    @Query(value = "delete from role_children c where c.child_id = ?1", nativeQuery = true)
    @Transactional
    void deleteParent(Long id);

    Role findOneById(Long id);

    @Query(value = "select c.parent_id from role_children c where c.child_id = ?1", nativeQuery = true)
    Long getParentId(Long childRoleId);

    @Query("SELECT r FROM Role r WHERE LOWER(r.name) LIKE LOWER(concat('%', ?1, '%'))")
    List<Role> findByNameContaining(String pattern);

    @Query("SELECT r FROM Role r WHERE r.id in ?1")
    List<Role> findByIds(List<Long> ids);
}
