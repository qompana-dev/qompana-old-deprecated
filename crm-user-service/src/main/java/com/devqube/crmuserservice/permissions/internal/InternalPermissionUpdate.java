package com.devqube.crmuserservice.permissions.internal;

import com.devqube.crmuserservice.permissions.module.Module;
import com.devqube.crmuserservice.permissions.module.ModuleRepository;
import com.devqube.crmuserservice.permissions.modulePermission.ModulePermission;
import com.devqube.crmuserservice.permissions.modulePermission.ModulePermissionRepository;
import com.devqube.crmuserservice.permissions.permission.Permission;
import com.devqube.crmuserservice.permissions.permission.PermissionRepository;
import com.devqube.crmuserservice.permissions.profile.Profile;
import com.devqube.crmuserservice.permissions.profile.ProfileRepository;
import com.devqube.crmuserservice.permissions.webControl.WebControl;
import com.devqube.crmuserservice.permissions.webControl.WebControlRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Don't use this class in the project
 * The component will be launched automatically at startup and will update permissions after db migration
 */
@Slf4j
@Component
class InternalPermissionUpdate {
    private static final boolean DEFAULT_MODULE_PERMISSION_VIEW_STATE = true;
    private static final boolean DEFAULT_MODULE_PERMISSION_CREATE_STATE = true;
    private static final boolean DEFAULT_MODULE_PERMISSION_EDIT_STATE = true;
    private static final boolean DEFAULT_MODULE_PERMISSION_DELETE_STATE = true;

    private final ProfileRepository profileRepository;
    private final ModulePermissionRepository modulePermissionRepository;
    private final ModuleRepository moduleRepository;
    private final PermissionRepository permissionRepository;
    private final WebControlRepository webControlRepository;

    InternalPermissionUpdate(ProfileRepository profileRepository,
                             ModulePermissionRepository modulePermissionRepository,
                             ModuleRepository moduleRepository,
                             PermissionRepository permissionRepository,
                             WebControlRepository webControlRepository) {
        this.profileRepository = profileRepository;
        this.modulePermissionRepository = modulePermissionRepository;
        this.moduleRepository = moduleRepository;
        this.permissionRepository = permissionRepository;
        this.webControlRepository = webControlRepository;

        updateAfterDatabaseMigration();
    }

    private void updateAfterDatabaseMigration() {
        log.info("updating permissions after database migration");

        log.info("checking module permissions");
        updateModulePermissions();
        log.info("checking permissions");
        updatePermissions();
    }


    // permissions

    private void updatePermissions() {
        List<Profile> profiles = profileRepository.findAll();

        for (Profile profile : profiles) {
            updatePermissionsForProfile(profile);
        }
    }

    private void updatePermissionsForProfile(Profile profile) {
        List<ModulePermission> modulePermissionsByProfile = modulePermissionRepository.findAllByProfileId(profile.getId());

        for (ModulePermission modulePermission : modulePermissionsByProfile) {
            updatePermissionForModulePermission(modulePermission, profile);
        }
    }

    private void updatePermissionForModulePermission(ModulePermission modulePermission, Profile profile) {
        List<WebControl> webControls = webControlRepository.findAllByModuleId(modulePermission.getModule().getId());
        List<Long> webControlIdsByModulePermissionId = permissionRepository.findAllWebControlIdsByModulePermissionId(modulePermission.getId());
        if (webControlIdsByModulePermissionId.size() != webControls.size()) {
            for (WebControl webControl : webControls) {
                if (!webControlIdsByModulePermissionId.contains(webControl.getId())) {
                    log.info("adding permission for webControl {} in profile {}", webControl.getFrontendId(), profile.getName());
                    permissionRepository.save(getPermissionToSave(webControl, modulePermission));
                }
            }
        }
    }

    private Permission getPermissionToSave(WebControl webControl, ModulePermission modulePermission) {
        Permission permission = new Permission();
        permission.setState(webControl.getDefaultState());
        permission.setModulePermission(modulePermission);
        permission.setWebControl(webControl);
        return permission;
    }


    // module permissions

    private void updateModulePermissions() {
        List<Module> modules = moduleRepository.findAll();
        List<Profile> profiles = profileRepository.findAll();

        for (Profile profile : profiles) {
            updateModulePermissionsForProfile(profile, modules);
        }
    }

    private void updateModulePermissionsForProfile(Profile profile, List<Module> modules) {
        List<Long> moduleIdsByProfileId = modulePermissionRepository.findAllModuleIdByProfileId(profile.getId());

        if (modules.size() != moduleIdsByProfileId.size()) {
            for (Module module : modules) {
                if (!moduleIdsByProfileId.contains(module.getId())) {
                    log.info("adding module permission for profile {} and module {}", profile.getName(), module.getName());
                    modulePermissionRepository.save(getModulePermissionToSave(module, profile));
                }
            }
        }
    }

    private ModulePermission getModulePermissionToSave(Module module, Profile profile) {
        ModulePermission modulePermission = new ModulePermission();
        modulePermission.setModule(module);
        modulePermission.setProfile(profile);
        modulePermission.setView(DEFAULT_MODULE_PERMISSION_VIEW_STATE);
        modulePermission.setCreate(DEFAULT_MODULE_PERMISSION_CREATE_STATE);
        modulePermission.setEdit(DEFAULT_MODULE_PERMISSION_EDIT_STATE);
        modulePermission.setDelete(DEFAULT_MODULE_PERMISSION_DELETE_STATE);
        modulePermission.setPermissions(new ArrayList<>());
        return modulePermission;
    }
}
