package com.devqube.crmuserservice.permissions.role.web;

import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.user.dto.AccountEmail;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.permissions.role.Role;
import com.devqube.crmuserservice.permissions.role.RoleService;
import com.devqube.crmuserservice.permissions.role.web.dto.RoleDTO;
import com.devqube.crmuserservice.permissions.role.web.dto.RoleTreeDTO;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class RoleController implements RoleApi {

    private final RoleService roleService;
    private final ModelMapper modelMapper;

    public RoleController(RoleService roleService, ModelMapper modelMapper) {
        this.roleService = roleService;
        this.modelMapper = modelMapper;
    }

    @Override
    @AuthController(actionFrontendId = "NavigationComponent.permissions")
    public ResponseEntity<Role> addRole(@Valid RoleDTO roleDTO) {
        try {
            return new ResponseEntity<>(roleService.save(modelMapper.map(roleDTO, Role.class), roleDTO.getSupervisor()), HttpStatus.CREATED);
        } catch (BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<List<RoleTreeDTO>> getRoles() {
        return new ResponseEntity<>(roleService.getAllAsTree()
                .stream()
                .map(e -> modelMapper.map(e, RoleTreeDTO.class))
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<AccountEmail>> getAccountEmails(final Long accountId) {
        try {
            return new ResponseEntity<>(roleService.getAccountEmails(accountId)
                    .stream()
                    .map(this::mapToAccountEmail)
                    .collect(Collectors.toList()), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<List<RoleDTO>> getRoles(@Valid Boolean onlyPossibleRoles) {
        List<Role> roles = roleService.getAllAsFlatList(onlyPossibleRoles);
        return new ResponseEntity<>(roles
                .stream()
                .map(e -> modelMapper.map(e, RoleDTO.class))
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @Override
    @AuthController(actionFrontendId = "NavigationComponent.permissions")
    public ResponseEntity<RoleDTO> getRole(Long id) {
        try {
            Role role = roleService.getById(id);
            RoleDTO roleDTO = modelMapper.map(role, RoleDTO.class);
            roleDTO.setHasChildren(role.getChildren() != null && !role.getChildren().isEmpty());
            return new ResponseEntity<>(roleDTO, HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(actionFrontendId = "NavigationComponent.permissions")
    public ResponseEntity<Void> deleteRole(Long id) {
        try {
            roleService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(actionFrontendId = "NavigationComponent.permissions")
    public ResponseEntity<Role> modifyRole(@Valid RoleDTO roleDTO) {
        try {
            Role saved = roleService.modifyRole(modelMapper.map(roleDTO, Role.class), roleDTO.getSupervisor());
            return new ResponseEntity<>(saved, HttpStatus.OK);
        } catch (BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(actionFrontendId = "NavigationComponent.permissions")
    public ResponseEntity<Void> moveRole(@NotNull @Valid Long parentId, @NotNull @Valid Long childId) {
        try {
            roleService.moveRole(childId, parentId);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<Long>> getAccountsForRoleChild(String roleName) {
        try {
            return new ResponseEntity<>(roleService.getAccountsForRoleChild(roleName), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<List<Long>> getAccountRoleIdWithChildren(String email) {
        try {
            return new ResponseEntity<>(roleService.getAccountRoleIdWithChildren(email), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Long> getSupervisorIdByAccountEmail(String email) {
        try {
            return new ResponseEntity<>(roleService.getSupervisorId(email), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<List<CrmObject>> getRoleForSearch(@NotNull @Valid String term) {
        return new ResponseEntity<>(roleService.findAllContaining(term)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.role, Strings.nullToEmpty(e.getName()), null))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }


    @Override
    public ResponseEntity<List<CrmObject>> getRoleAsCrmObject(@Valid List<Long> ids) {
        return new ResponseEntity<>(roleService.findByIds(ids)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.role, Strings.nullToEmpty(e.getName()), null))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    private AccountEmail mapToAccountEmail(Account account) {
        AccountEmail email = new AccountEmail();
        email.setId(account.getId().intValue());
        email.setEmail(account.getPerson().getEmail());
        email.setName(account.getPerson().getName());
        email.setSurname(account.getPerson().getSurname());
        return email;
    }
}
