package com.devqube.crmuserservice.permissions.role.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * RoleTreeDTO
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-10-24T12:47:04.725339+02:00[Europe/Warsaw]")

public class RoleTreeDTO {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("accounts")
    @Valid
    private List<IdAndEmailDTO> accounts = null;

    @JsonProperty("children")
    @Valid
    private List<RoleTreeDTO> children = null;

    public RoleTreeDTO id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     */
    @ApiModelProperty(value = "")


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoleTreeDTO name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     */
    @ApiModelProperty(value = "")


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleTreeDTO accounts(List<IdAndEmailDTO> accounts) {
        this.accounts = accounts;
        return this;
    }

    public RoleTreeDTO addAccountsItem(IdAndEmailDTO accountsItem) {
        if (this.accounts == null) {
            this.accounts = new ArrayList<>();
        }
        this.accounts.add(accountsItem);
        return this;
    }

    /**
     * Get accounts
     *
     * @return accounts
     */
    @ApiModelProperty(value = "")

    @Valid

    public List<IdAndEmailDTO> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<IdAndEmailDTO> accounts) {
        this.accounts = accounts;
    }

    public RoleTreeDTO children(List<RoleTreeDTO> children) {
        this.children = children;
        return this;
    }

    public RoleTreeDTO addChildrenItem(RoleTreeDTO childrenItem) {
        if (this.children == null) {
            this.children = new ArrayList<>();
        }
        this.children.add(childrenItem);
        return this;
    }

    /**
     * Get children
     *
     * @return children
     */
    @ApiModelProperty(value = "")

    @Valid

    public List<RoleTreeDTO> getChildren() {
        return children;
    }

    public void setChildren(List<RoleTreeDTO> children) {
        this.children = children;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RoleTreeDTO roleTreeDTO = (RoleTreeDTO) o;
        return Objects.equals(this.id, roleTreeDTO.id) &&
                Objects.equals(this.name, roleTreeDTO.name) &&
                Objects.equals(this.accounts, roleTreeDTO.accounts) &&
                Objects.equals(this.children, roleTreeDTO.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, accounts, children);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class RoleTreeDTO {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    accounts: ").append(toIndentedString(accounts)).append("\n");
        sb.append("    children: ").append(toIndentedString(children)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

