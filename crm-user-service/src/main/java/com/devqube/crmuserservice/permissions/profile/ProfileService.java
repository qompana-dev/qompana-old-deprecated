package com.devqube.crmuserservice.permissions.profile;

import com.devqube.crmshared.access.model.ProfileDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.devqube.crmuserservice.account.AccountsRepository;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.permissions.module.Module;
import com.devqube.crmuserservice.permissions.module.ModuleRepository;
import com.devqube.crmuserservice.permissions.modulePermission.ModulePermission;
import com.devqube.crmuserservice.permissions.permission.Permission;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileListDTO;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileSaveDTO;
import com.devqube.crmuserservice.permissions.role.RoleUtil;
import com.devqube.crmuserservice.permissions.webControl.WebControl;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProfileService {
    private final ProfileRepository profileRepository;
    private final ModuleRepository moduleRepository;
    private final AccountsRepository accountsRepository;
    private final ModelMapper modelMapper;

    public ProfileService(ProfileRepository profileRepository, ModuleRepository moduleRepository, AccountsRepository accountsRepository, ModelMapper modelMapper) {
        this.profileRepository = profileRepository;
        this.moduleRepository = moduleRepository;
        this.accountsRepository = accountsRepository;
        this.modelMapper = modelMapper;
    }

    public Profile save(Profile profile) throws BadRequestException {
        if(profileRepository.findByName(profile.getName()) != null) {
            throw new BadRequestException("Profile with this name already exists");
        }
        mapModulePermissionsWithProfile(profile);
        return profileRepository.save(profile);
    }

    public Profile update(Profile profile) throws BadRequestException {
        Profile byName = profileRepository.findByName(profile.getName());
        if(byName != null && !byName.getId().equals(profile.getId())) {
            throw new BadRequestException("Profile with this name already exists");
        }
        mapModulePermissionsWithProfile(profile);
        return profileRepository.save(profile);
    }

    public Page<ProfileListDTO> getAll(Pageable pageable) {
        return profileRepository.findAll(pageable).map(e -> modelMapper.map(e, ProfileListDTO.class));
    }

    public List<ProfileListDTO> getAll() {
        return profileRepository.findAll()
                .stream()
                .map(e -> modelMapper.map(e, ProfileListDTO.class))
                .collect(Collectors.toList());
    }

    public Profile getUserProfile() {
        return getProfileByAccountId(accountsRepository.findByPersonEmail(CurrentRequestUtil.getLoggedInAccountEmail()).getId());
    }

    public ProfileDto getUserProfileDto() {
        return modelMapper.map(getUserProfile(), ProfileDto.class);
    }

    @Cacheable(value = "profileByAccountId", key = "#id")
    public Profile getProfileByAccountId(Long id) {
        Optional<Account> account = accountsRepository.findById(id);
        if(account.isPresent() && account.get().getRole() != null) {
            return RoleUtil.getProfile(account.get().getRole());
        } else {
            return null;
        }
    }

    public ProfileSaveDTO getNewProfileTemplate() {
        ProfileSaveDTO profile = new ProfileSaveDTO();
        profile.setModulePermissions(createModulePermissionSet());
        return profile;
    }

    public void removeProfile(Long id) throws EntityNotFoundException {
        Optional<Profile> fromDb = this.profileRepository.findById(id);
        if (fromDb.isEmpty()) {
            throw new EntityNotFoundException();
        }
        profileRepository.deleteById(id);
    }

    public Profile getById(Long id) throws EntityNotFoundException {
        Optional<Profile> fromDb = this.profileRepository.findById(id);
        if (fromDb.isEmpty()) {
            throw new EntityNotFoundException();
        }
        return fromDb.get();
    }

    private List<ModulePermission> createModulePermissionSet() {
        List<ModulePermission> modulePermissions = new ArrayList<>();
        for (Module module : moduleRepository.findAll()) {
            ModulePermission modulePermission = createModulePermissionFor(module);
            modulePermissions.add(modulePermission);
        }
        return modulePermissions;
    }

    private ModulePermission createModulePermissionFor(Module module) {
        ModulePermission modulePermission = new ModulePermission();
        modulePermission.setModule(module);
        modulePermission.setPermissions(createPermissionsFor(module));
        return modulePermission;
    }

    private List<Permission> createPermissionsFor(Module module) {
        List<Permission> permissions = new ArrayList<>();
        for (WebControl webControl : module.getWebControls()) {
            Permission permission = new Permission();
            permission.setWebControl(webControl);
            permission.setState(webControl.getDefaultState());
            permissions.add(permission);
        }
        return permissions;
    }

    private void mapModulePermissionsWithProfile(Profile profile) {
        if(profile.getModulePermissions() != null) {
            for (ModulePermission modulePermission : profile.getModulePermissions()) {
                modulePermission.setProfile(profile);
                if(modulePermission.getPermissions() != null) {
                    for (Permission permission : modulePermission.getPermissions()) {
                        permission.setModulePermission(modulePermission);
                    }
                }
            }
        }
    }
}
