package com.devqube.crmuserservice.permissions.moduleCategory;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ModuleCategoryRepository extends JpaRepository<ModuleCategory, Long> {
}
