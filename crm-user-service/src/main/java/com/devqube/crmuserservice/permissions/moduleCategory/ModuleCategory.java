package com.devqube.crmuserservice.permissions.moduleCategory;

import com.devqube.crmuserservice.permissions.module.Module;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ModuleCategory {
    @Id
    @SequenceGenerator(name="module_category_seq", sequenceName="module_category_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="module_category_seq")
    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @OneToMany(mappedBy = "category")
    @JsonIgnore
    private Set<Module> modules;

    @Override
    public String toString() {
        return "ModuleCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModuleCategory that = (ModuleCategory) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
