package com.devqube.crmuserservice.permissions.role;

import com.devqube.crmuserservice.permissions.modulePermission.ModulePermission;
import com.devqube.crmuserservice.permissions.permission.Permission;
import com.devqube.crmuserservice.permissions.profile.Profile;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RoleUtil {
    public static Profile getProfile(Role role) {
        Set<Profile> allProfiles = extractAllProfiles(role);
        if(allProfiles.isEmpty()) {
            return null;
        } else if (allProfiles.size() == 1){
            return allProfiles.iterator().next();
        } else {
            return mergeAllProfiles(allProfiles);
        }
    }

    static Set<Profile> extractAllProfiles(Role role) {
        Set<Profile> allProfiles = new HashSet<>();
        if(role.getProfiles() != null) {
            allProfiles.addAll(role.getProfiles());
        }
        if(role.getChildren() != null) {
            role.getChildren().forEach(e -> allProfiles.addAll(extractAllProfiles(e)));
        }
        allProfiles.forEach(RoleUtil::sortModulePermissionsAndPermissions);
        return allProfiles;
    }

    private static void sortModulePermissionsAndPermissions(Profile profile) {
        if(profile.getModulePermissions() != null) {
            profile.getModulePermissions().sort(Comparator.comparing((ModulePermission p) -> p.getModule().getName()));
            profile.getModulePermissions().forEach(e -> e.getPermissions().sort(Comparator.comparing((Permission p) -> p.getWebControl().getName())));
        }
    }

    private static Profile mergeAllProfiles(Set<Profile> allProfiles) {
        Profile mergingObject = allProfiles.iterator().next();
        for (Profile profile : allProfiles) {
            for (int moduleIndex = 0; moduleIndex < profile.getModulePermissions().size(); moduleIndex++) {
                mergeModulePermission(mergingObject, profile, moduleIndex);
            }
        }
        return mergingObject;
    }

    private static void mergeModulePermission(Profile mergingObject, Profile profile, int moduleIndex) {
        ModulePermission mergedModulePermission = mergingObject.getModulePermissions().get(moduleIndex);
        ModulePermission modulePermission = profile.getModulePermissions().get(moduleIndex);
        mergeModuleStates(mergedModulePermission, modulePermission);
        List<Permission> permissions = profile.getModulePermissions().get(moduleIndex).getPermissions();
        for (int permissionIndex = 0; permissionIndex < permissions.size(); permissionIndex++) {
            mergePermissionState(mergingObject, permissions, moduleIndex, permissionIndex);
        }
    }

    private static void mergeModuleStates(ModulePermission mergedModulePermission, ModulePermission modulePermission) {
        mergedModulePermission.setView(mergedModulePermission.isView() || modulePermission.isView());
        mergedModulePermission.setCreate(mergedModulePermission.isCreate() || modulePermission.isCreate());
        mergedModulePermission.setDelete(mergedModulePermission.isDelete() || modulePermission.isDelete());
        mergedModulePermission.setEdit(mergedModulePermission.isEdit() || modulePermission.isEdit());
    }

    private static void mergePermissionState(Profile merged, List<Permission> permissions, int moduleIndex, int permissionIndex) {
        Permission mergedPermission = merged.getModulePermissions().get(moduleIndex).getPermissions().get(permissionIndex);
        Permission permission = permissions.get(permissionIndex);
        if(permission.getState().equals(Permission.State.WRITE) || mergedPermission.getState().equals(Permission.State.WRITE))
            mergedPermission.setState(Permission.State.WRITE);
        else if(permission.getState().equals(Permission.State.READ_ONLY) || mergedPermission.getState().equals(Permission.State.READ_ONLY))
            mergedPermission.setState(Permission.State.READ_ONLY);
        else
            mergedPermission.setState(Permission.State.INVISIBLE);
    }
}
