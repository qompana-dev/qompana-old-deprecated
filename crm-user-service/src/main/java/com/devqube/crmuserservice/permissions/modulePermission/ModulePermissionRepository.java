package com.devqube.crmuserservice.permissions.modulePermission;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ModulePermissionRepository extends JpaRepository<ModulePermission, Long> {
    List<ModulePermission> findAllByProfileId(Long profileId);

    @Query("select distinct(m.id) from ModulePermission mp " +
            "left join mp.module m " +
            "left join mp.profile p " +
            "where p.id = :profileId")
    List<Long> findAllModuleIdByProfileId(@Param("profileId") Long profileId);


}
