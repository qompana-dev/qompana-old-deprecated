package com.devqube.crmuserservice.permissions.permission;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PermissionRepository extends JpaRepository<Permission, Long> {
    @Query("select distinct(wc.id) from Permission p " +
            "left join p.modulePermission mp " +
            "left join p.webControl wc " +
            "where mp.id = :modulePermissionId")
    public List<Long> findAllWebControlIdsByModulePermissionId(@Param("modulePermissionId") Long modulePermissionId);
}
