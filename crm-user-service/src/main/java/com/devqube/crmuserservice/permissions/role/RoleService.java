package com.devqube.crmuserservice.permissions.role;

import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmuserservice.account.AccountsRepository;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.permissions.profile.ProfileRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RoleService {
    private final RoleRepository roleRepository;
    private final ProfileRepository profileRepository;
    private final AccountsRepository accountsRepository;

    public RoleService(RoleRepository roleRepository, ProfileRepository profileRepository, AccountsRepository accountsRepository) {
        this.roleRepository = roleRepository;
        this.profileRepository = profileRepository;
        this.accountsRepository = accountsRepository;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Role save(Role role, Long parentId) throws BadRequestException, EntityNotFoundException {
        if (roleRepository.findByName(role.getName()) != null) {
            throw new BadRequestException("Role with this name already exists");
        }
        Role saved = roleRepository.save(role);
        addParentToRole(parentId, saved);
        if (role.getAccounts() != null) {
            setRoleToAllAccounts(role, saved);
        }
        return saved;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void moveRole(Long childId, Long parentId) throws EntityNotFoundException, BadRequestException {
        this.deletePreviousParent(childId);
        this.addParent(parentId, childId);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Role modifyRole(Role role, Long parentId) throws BadRequestException, EntityNotFoundException {
        Role byId = roleRepository.findById(role.getId()).orElseThrow(EntityNotFoundException::new);
        Role byName = roleRepository.findByName(role.getName());
        if (!byId.getName().equals(role.getName()) && byName != null && !byId.equals(byName)) {
            throw new BadRequestException("Role with this name already exist");
        }
        role.setProfiles(role.getProfiles()
                .stream()
                .map(e -> e = profileRepository.findById(e.getId()).orElse(null))
                .collect(Collectors.toSet()));
        role.setChildren(byId.getChildren());
        deleteRoleFromOldAccountsAndSetItToNewAccounts(role, byId);

        Role saved = roleRepository.save(role);
        this.deletePreviousParent(saved.getId());
        addParentToRole(parentId, saved);
        return saved;
    }

    public List<Role> getAllAsTree() {
        return roleRepository.findAllOriginRoles();
    }

    public List<Account> getAccountEmails(final Long accountId) throws EntityNotFoundException {
        final Set<Role> allAsTree = new HashSet<>(getAllAsTree());
        final Map<Long, Set<Account>> levelToAccounts = new HashMap<>();
        Long searchLevel = treeTraversal(allAsTree, levelToAccounts, 0L, accountId);

        if (searchLevel == null) {
            throw new EntityNotFoundException("Account was not found");
        }

        final Set<Account> result = new HashSet<>();

        getAccountsFromLevel(levelToAccounts, result, searchLevel - 1);
        getAccountsFromLevel(levelToAccounts, result, searchLevel);
        getAccountsFromLevel(levelToAccounts, result, searchLevel + 1);

        return new ArrayList<>(result);
    }

    private void getAccountsFromLevel(final Map<Long, Set<Account>> levelToAccounts, final Set<Account> result,
                                      long level) {
        if (levelToAccounts.containsKey(level)) {
            Set<Account> accounts = levelToAccounts.get(level);
            if (accounts != null) {
                result.addAll(accounts);
            }
        }
    }

    private Long treeTraversal(final Set<Role> allAsTree, final Map<Long, Set<Account>> levelToAccounts,
                               final long level, final @NonNull Long searchAccountId) {
        Long valueToReturn = null;

        for (Role role : allAsTree) {
            if (levelToAccounts.containsKey(level)) {
                levelToAccounts.get(level).addAll(role.getAccounts());
            } else {
                levelToAccounts.put(level, role.getAccounts());
            }

            Long returnedValue = treeTraversal(role.getChildren(), levelToAccounts, level + 1, searchAccountId);
            if (returnedValue == null)
            {
                for (Account account : role.getAccounts()) {
                    if (searchAccountId.equals(account.getId())) {
                        valueToReturn = level;
                    }
                }
            } else {
                valueToReturn = returnedValue;
            }
        }

        return valueToReturn;
    }

    public List<Role> getAllAsFlatList(Boolean onlyPossibleRoles) {
        List<Role> roles = roleRepository.findAll();
        if (onlyPossibleRoles != null && onlyPossibleRoles) {
            roles = roles
                    .stream()
                    .filter(e -> e.getChildren().isEmpty() || e.getAccounts().isEmpty())
                    .collect(Collectors.toList());
        }
        roles.forEach(e -> e.setChildren(null));
        return roles;
    }

    public void delete(Long id) throws EntityNotFoundException, BadRequestException {
        Role role = roleRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        if (role.getAccounts() != null && !role.getAccounts().isEmpty()) {
            throw new BadRequestException("Some users are assigned to this role. Cannot delete it.");
        }
        roleRepository.deleteById(id);
    }

    public Role getById(Long id) throws EntityNotFoundException {
        Optional<Role> role = roleRepository.findById(id);
        if (role.isEmpty()) {
            throw new EntityNotFoundException("Role was not found");
        }
        return role.get();
    }

    public List<Long> getAccountsForRoleChild(String roleName) throws EntityNotFoundException {
        Role role = roleRepository.findByName(roleName);
        if (role == null) {
            throw new EntityNotFoundException();
        }

        return getChildren(role).stream()
                .filter(r -> r.getAccounts() != null)
                .map(Role::getAccounts)
                .flatMap(Set::stream)
                .map(Account::getId)
                .collect(Collectors.toList());
    }

    private void addParentToRole(Long parentId, Role saved) throws BadRequestException, EntityNotFoundException {
        if (parentId != null) {
            this.addParent(parentId, saved.getId());
        }
    }

    private void setRoleToAllAccounts(Role role, Role saved) throws EntityNotFoundException {
        for (Account account : role.getAccounts()) {
            Account byId = accountsRepository.findById(account.getId()).orElseThrow(EntityNotFoundException::new);
            byId.setRole(saved);
        }
    }

    private void deleteRoleFromOldAccountsAndSetItToNewAccounts(Role role, Role byId) throws BadRequestException, EntityNotFoundException {
        List<Account> allByRoleId = accountsRepository.findAllByRoleId(byId.getId());
        if (allByRoleId != null) {
            allByRoleId.forEach(account -> account.setRole(null));
        }
        if (role.getAccounts() != null) {
            if (byId.getChildren().size() > 0 && role.getAccounts().size() > 1) {
                throw new BadRequestException("Manager role can only have one account assigned");
            }
            setRoleToAllAccounts(role, byId);
        }
    }

    private Set<Role> getChildren(Role role) {
        if (role == null || role.getChildren() == null) {
            return new HashSet<>();
        }
        Set<Role> children = new HashSet<>();
        role.getChildren().forEach(c -> children.addAll(getChildren(c)));
        children.addAll(role.getChildren());
        return children;
    }

    private void addParent(Long parentId, Long childId) throws EntityNotFoundException, BadRequestException {
        Optional<Role> parentRole = roleRepository.findById(parentId);
        if (parentRole.isEmpty() || roleRepository.findById(childId).isEmpty()) {
            throw new EntityNotFoundException("One of roles was not found");
        }
        if (parentRole.get().getAccounts() != null && parentRole.get().getAccounts().size() > 1) {
            throw new BadRequestException("This role cannot be parent. It has more than one user assigned");
        }
        roleRepository.addParent(parentId, childId);
    }

    private void deletePreviousParent(Long id) throws EntityNotFoundException {
        if (roleRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException("Role was not found");
        }
        roleRepository.deleteParent(id);
    }

    public List<Long> getAccountRoleIdWithChildren(String email) throws EntityNotFoundException {
        List<Long> ids = new ArrayList<>();
        Role role = getRoleByAccountEmail(email);
        ids.add(role.getId());
        ids.addAll(getChildrenIds(role));
        return ids;
    }

    private List<Long> getChildrenIds(Role role) {
        List<Long> ids = new ArrayList<>();
        if (role.getChildren() != null) {
            for (Role child : role.getChildren()) {
                ids.add(child.getId());
                ids.addAll(getChildrenIds(child));
            }
        }
        return ids;
    }

    private Role getRoleByAccountEmail(String email) throws EntityNotFoundException {
        Account account = accountsRepository.findByPersonEmail(email);
        if (account == null) {
            throw new EntityNotFoundException("Account with this email not found");
        }
        Role role = account.getRole();
        if (role == null) {
            throw new EntityNotFoundException("Account doesn't have role");
        }
        return role;
    }

    public Long getSupervisorId(String email) throws EntityNotFoundException {
        Role role = getRoleByAccountEmail(email);
        Long parentId = roleRepository.getParentId(role.getId());
        if (parentId != null) {
            Role supervisorRole = roleRepository.getOne(parentId);
            if (supervisorRole.getAccounts() != null && supervisorRole.getAccounts().size() == 1) {
                return supervisorRole.getAccounts().iterator().next().getId();
            }
        }
        return null;
    }

    public List<Role> findAllContaining(String term) {
        return roleRepository.findByNameContaining(term);
    }


    public List<Role> findByIds(List<Long> ids) {
        return roleRepository.findByIds(ids);
    }
}
