package com.devqube.crmuserservice.permissions.profile.web;

import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.access.model.ProfileDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmuserservice.permissions.profile.Profile;
import com.devqube.crmuserservice.permissions.profile.ProfileService;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileListDTO;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileSaveDTO;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
public class ProfileController implements ProfileApi {
    private final ProfileService profileService;
    private final ModelMapper modelMapper;

    public ProfileController(ProfileService profileService, ModelMapper modelMapper) {
        this.profileService = profileService;
        this.modelMapper = modelMapper;
    }

    @Override
    public ResponseEntity<Profile> getProfile() {
        return new ResponseEntity<>(profileService.getUserProfile(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ProfileDto> getBasicProfile(String email) {
        return new ResponseEntity<>(profileService.getUserProfileDto(), HttpStatus.OK);
    }

    @Override
    @AuthController(actionFrontendId = "NavigationComponent.permissions")
    public ResponseEntity<Page> getProfiles(@Valid Pageable pageable) {
        return new ResponseEntity<>(profileService.getAll(pageable), HttpStatus.OK);
    }

    @Override
    @AuthController(actionFrontendId = "NavigationComponent.permissions")
    public ResponseEntity<List<ProfileListDTO>> getAllProfiles() {
        return new ResponseEntity<>(profileService.getAll(), HttpStatus.OK);
    }

    @Override
    @AuthController(actionFrontendId = "NavigationComponent.permissions")
    public ResponseEntity<ProfileSaveDTO> getNewProfileTemplate() {
        return new ResponseEntity<>(profileService.getNewProfileTemplate(), HttpStatus.OK);
    }

    @Override
    @AuthController(actionFrontendId = "NavigationComponent.permissions")
    public ResponseEntity<ProfileSaveDTO> saveProfile(@Valid ProfileSaveDTO profileSaveDTO) {
        try {
            Profile savedProfile = profileService.save(modelMapper.map(profileSaveDTO, Profile.class));
            return new ResponseEntity<>(modelMapper.map(savedProfile, ProfileSaveDTO.class), HttpStatus.OK);
        } catch (BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(actionFrontendId = "NavigationComponent.permissions")
    public ResponseEntity<Void> deleteProfile(Long id) {
        try {
            this.profileService.removeProfile(id);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ProfileSaveDTO> getProfile(Long id) {
        try {
            return new ResponseEntity<>(modelMapper.map(profileService.getById(id), ProfileSaveDTO.class), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(actionFrontendId = "NavigationComponent.permissions")
    public ResponseEntity<ProfileSaveDTO> updateProfile(@Valid ProfileSaveDTO profileSaveDTO) {
        try {
            Profile savedProfile = profileService.update(modelMapper.map(profileSaveDTO, Profile.class));
            return new ResponseEntity<>(modelMapper.map(savedProfile, ProfileSaveDTO.class), HttpStatus.OK);
        } catch (BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
