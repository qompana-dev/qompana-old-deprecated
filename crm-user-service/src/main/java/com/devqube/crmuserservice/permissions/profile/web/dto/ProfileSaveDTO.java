package com.devqube.crmuserservice.permissions.profile.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * ProfileSaveDTO
 */

public class ProfileSaveDTO {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("modulePermissions")
    @Valid
    private List<com.devqube.crmuserservice.permissions.modulePermission.ModulePermission> modulePermissions = new ArrayList<>();

    public ProfileSaveDTO id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     */
    @ApiModelProperty(value = "")


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProfileSaveDTO name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     */
    @ApiModelProperty(required = true, value = "")
    @NotNull


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProfileSaveDTO description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Get description
     *
     * @return description
     */
    @ApiModelProperty(value = "")


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProfileSaveDTO modulePermissions(List<com.devqube.crmuserservice.permissions.modulePermission.ModulePermission> modulePermissions) {
        this.modulePermissions = modulePermissions;
        return this;
    }

    public ProfileSaveDTO addModulePermissionsItem(com.devqube.crmuserservice.permissions.modulePermission.ModulePermission modulePermissionsItem) {
        this.modulePermissions.add(modulePermissionsItem);
        return this;
    }

    /**
     * Get modulePermissions
     *
     * @return modulePermissions
     */
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid
    @Size(min = 1)
    public List<com.devqube.crmuserservice.permissions.modulePermission.ModulePermission> getModulePermissions() {
        return modulePermissions;
    }

    public void setModulePermissions(List<com.devqube.crmuserservice.permissions.modulePermission.ModulePermission> modulePermissions) {
        this.modulePermissions = modulePermissions;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProfileSaveDTO profileSaveDTO = (ProfileSaveDTO) o;
        return Objects.equals(this.id, profileSaveDTO.id) &&
                Objects.equals(this.name, profileSaveDTO.name) &&
                Objects.equals(this.description, profileSaveDTO.description) &&
                Objects.equals(this.modulePermissions, profileSaveDTO.modulePermissions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, modulePermissions);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ProfileSaveDTO {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("    modulePermissions: ").append(toIndentedString(modulePermissions)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

