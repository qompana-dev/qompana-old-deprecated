package com.devqube.crmuserservice.permissions.webControl;

import com.devqube.crmuserservice.permissions.permission.Permission;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WebControl {
    @Id
    @SequenceGenerator(name="web_control_seq", sequenceName="web_control_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="web_control_seq")
    @JsonProperty("id")
    private Long id;

    @JsonProperty("frontendId")
    private String frontendId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("defaultState")
    private Permission.State defaultState;

    @JsonProperty("dependsOn")
    private ModuleOperation dependsOn;

    @JsonProperty("required")
    private boolean required;

    @Enumerated(value = EnumType.ORDINAL)
    @JsonProperty("type")
    private Type type;

    @ManyToOne
    @JoinColumn(name = "module_id")
    @JsonIgnore
    private com.devqube.crmuserservice.permissions.module.Module module;

    public enum ModuleOperation {
        VIEW("VIEW"),

        CREATE("CREATE"),

        EDIT("EDIT"),

        DELETE("DELETE");

        private String value;

        ModuleOperation(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        public String toString() {
            return String.valueOf(value);
        }

        @JsonCreator
        public static ModuleOperation fromValue(String text) {
            for (ModuleOperation b : ModuleOperation.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + text + "'");
        }
    }

    public enum Type {
        FIELD("FIELD"),

        TOOL("TOOL");

        private String value;

        Type(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        public String toString() {
            return String.valueOf(value);
        }

        @JsonCreator
        public static Type fromValue(String text) {
            for (Type b : Type.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + text + "'");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WebControl that = (WebControl) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(frontendId, that.frontendId) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, frontendId, name);
    }

    @Override
    public String toString() {
        return "WebControl{" +
                "id=" + id +
                ", frontendId='" + frontendId + '\'' +
                ", name='" + name + '\'' +
                ", defaultState=" + defaultState +
                ", dependsOn=" + dependsOn +
                ", required=" + required +
                ", type=" + type +
                '}';
    }
}
