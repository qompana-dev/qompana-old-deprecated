package com.devqube.crmuserservice.permissions.profile.web;

import com.devqube.crmshared.access.model.ProfileDto;
import com.devqube.crmuserservice.ApiUtil;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileListDTO;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileSaveDTO;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "profile")
public interface ProfileApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "delete profile", nickname = "deleteProfile", notes = "Method used to delete profile")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "profile was deleted"),
            @ApiResponse(code = 404, message = "profile not found")})
    @RequestMapping(value = "/profile/{id}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteProfile(@ApiParam(value = "", required = true) @PathVariable("id") Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get logged user basic profile", nickname = "getBasicProfile", notes = "Method used to get logged in user profile.", response = ProfileDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "list of profiles", response = ProfileListDTO.class, responseContainer = "List")})
    @RequestMapping(value = "/profile/all",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<ProfileListDTO>> getAllProfiles() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"name\" : \"name\",  \"description\" : \"description\",  \"id\" : 1}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get logged user basic profile", nickname = "getBasicProfile", notes = "Method used to get logged in user profile.", response = com.devqube.crmshared.access.model.ProfileDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "retrieved logged in user profile", response = com.devqube.crmshared.access.model.ProfileDto.class)})
    @RequestMapping(value = "/profile/basic/mine",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<com.devqube.crmshared.access.model.ProfileDto> getBasicProfile(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get profile template for save", nickname = "getNewProfileTemplate", notes = "Method used to get profile save template.", response = ProfileSaveDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "new profile template", response = ProfileSaveDTO.class)})
    @RequestMapping(value = "/profile/save",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<ProfileSaveDTO> getNewProfileTemplate() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"modulePermissions\" : [ \"{}\", \"{}\" ],  \"name\" : \"name\",  \"id\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    @ApiOperation(value = "Get logged user profile", nickname = "getProfile", notes = "Method used to get logged in user profile.", response = com.devqube.crmuserservice.permissions.profile.Profile.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "retrieved logged in user profile", response = com.devqube.crmuserservice.permissions.profile.Profile.class)})
    @RequestMapping(value = "/profile/mine",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<com.devqube.crmuserservice.permissions.profile.Profile> getProfile() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    @ApiOperation(value = "get profile", nickname = "getProfile", notes = "Method used to get profile", response = ProfileSaveDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "profile was found", response = ProfileSaveDTO.class),
            @ApiResponse(code = 404, message = "profile not found")})
    @RequestMapping(value = "/profile/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<ProfileSaveDTO> getProfile(@ApiParam(value = "", required = true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"modulePermissions\" : [ \"{}\", \"{}\" ],  \"name\" : \"name\",  \"id\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get all profiles", nickname = "getProfiles", notes = "Method used to get all profiles", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "list of profiles", response = org.springframework.data.domain.Page.class)})
    @RequestMapping(value = "/profile",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<org.springframework.data.domain.Page> getProfiles(@ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    @ApiOperation(value = "save profile", nickname = "saveProfile", notes = "Method used to save profile.", response = ProfileSaveDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "saved profile", response = ProfileSaveDTO.class),
            @ApiResponse(code = 400, message = "validation error")})
    @RequestMapping(value = "/profile/save",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<ProfileSaveDTO> saveProfile(@ApiParam(value = "Created profile object", required = true) @Valid @RequestBody ProfileSaveDTO profileSaveDTO) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"modulePermissions\" : [ \"{}\", \"{}\" ],  \"name\" : \"name\",  \"id\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "modify profile", nickname = "updateProfile", notes = "Method used to modify profile.", response = ProfileSaveDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "updated profile", response = ProfileSaveDTO.class),
            @ApiResponse(code = 400, message = "validation error")})
    @RequestMapping(value = "/profile/{id}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<ProfileSaveDTO> updateProfile(@ApiParam(value = "Updated profile object", required = true) @Valid @RequestBody ProfileSaveDTO profileSaveDTO) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"modulePermissions\" : [ \"{}\", \"{}\" ],  \"name\" : \"name\",  \"id\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
