package com.devqube.crmuserservice.permissions.profile;

import com.devqube.crmuserservice.permissions.modulePermission.ModulePermission;
import com.devqube.crmuserservice.permissions.role.Role;
import com.devqube.crmuserservice.team.Team;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Profile {
    @JsonProperty("id")
    @Id
    @SequenceGenerator(name = "profile_seq", sequenceName = "profile_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "profile_seq")
    private Long id;

    @JsonProperty("name")
    @NotNull
    @Column(unique = true)
    private String name;

    @JsonProperty("description")
    private String description;

    @Valid
    @ManyToMany(mappedBy = "profiles")
    @JsonIgnore
    private Set<Role> roles;

    @OneToMany(mappedBy = "profile", cascade = CascadeType.ALL)
    private List<ModulePermission> modulePermissions;

    @ManyToMany(mappedBy = "profiles")
    private Set<Team> teams;

    public Profile(@NotNull Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profile profile = (Profile) o;
        return Objects.equals(id, profile.id) &&
                Objects.equals(name, profile.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}

