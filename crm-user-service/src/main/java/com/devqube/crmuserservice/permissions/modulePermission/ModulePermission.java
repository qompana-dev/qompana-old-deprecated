package com.devqube.crmuserservice.permissions.modulePermission;

import com.devqube.crmuserservice.permissions.module.Module;
import com.devqube.crmuserservice.permissions.permission.Permission;
import com.devqube.crmuserservice.permissions.profile.Profile;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ModulePermission {
    @Id
    @SequenceGenerator(name="module_permission_seq", sequenceName="module_permission_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="module_permission_seq")
    @JsonProperty("id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "module_id")
    @JsonProperty("module")
    @NotNull
    private Module module;

    @ManyToOne
    @JoinColumn(name = "profile_id")
    @JsonIgnore
    private Profile profile;

    @JsonProperty("view")
    @NotNull
    private boolean view;

    @JsonProperty("create")
    @NotNull
    @Column(name = "\"create\"")
    private boolean create;

    @JsonProperty("edit")
    @NotNull
    private boolean edit;

    @JsonProperty("delete")
    @NotNull
    private boolean delete;

    @OneToMany(mappedBy = "modulePermission", cascade = CascadeType.ALL)
    @JsonProperty("permissions")
    @NotNull
    @Valid
    private List<Permission> permissions;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModulePermission that = (ModulePermission) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(module, that.module);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, module);
    }

    @Override
    public String toString() {
        return "ModulePermission{" +
                "id=" + id +
                ", view=" + view +
                ", create=" + create +
                ", edit=" + edit +
                ", delete=" + delete +
                '}';
    }
}
