package com.devqube.crmuserservice.permissions.module;

import com.devqube.crmuserservice.permissions.moduleCategory.ModuleCategory;
import com.devqube.crmuserservice.permissions.webControl.WebControl;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Module {
    @Id
    @SequenceGenerator(name="module_seq", sequenceName="module_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="module_seq")
    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "module_category_id")
    @JsonProperty("category")
    private ModuleCategory category;

    @OneToMany(mappedBy = "module", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<WebControl> webControls;

    @Override
    public String toString() {
        return "Module{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Module module = (Module) o;
        return Objects.equals(id, module.id) &&
                Objects.equals(name, module.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
