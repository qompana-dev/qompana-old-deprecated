package com.devqube.crmuserservice.permissions.permission;

import com.devqube.crmuserservice.permissions.modulePermission.ModulePermission;
import com.devqube.crmuserservice.permissions.webControl.WebControl;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Permission {
    @Id
    @SequenceGenerator(name="permission_seq", sequenceName="permission_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="permission_seq")
    @JsonProperty("id")
    private Long id;

    @Enumerated(value = EnumType.ORDINAL)
    @JsonProperty("state")
    @NotNull
    private State state;

    @ManyToOne
    @JoinColumn(name = "module_permission_id")
    @JsonIgnore
    private ModulePermission modulePermission;

    @ManyToOne
    @JoinColumn(name = "web_control_id")
    @JsonProperty("webControl")
    @NotNull
    private WebControl webControl;

    public enum State {
        INVISIBLE("INVISIBLE"),

        READ_ONLY("READ_ONLY"),

        WRITE("WRITE");

        private String value;

        State(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        public String toString() {
            return String.valueOf(value);
        }

        @JsonCreator
        public static State fromValue(String text) {
            for (State b : State.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + text + "'");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Permission that = (Permission) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(webControl, that.webControl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, webControl);
    }

    @Override
    public String toString() {
        return "Permission{" +
                "id=" + id +
                ", state=" + state +
                '}';
    }
}



