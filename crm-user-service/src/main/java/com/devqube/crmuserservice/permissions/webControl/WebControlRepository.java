package com.devqube.crmuserservice.permissions.webControl;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WebControlRepository extends JpaRepository<WebControl, Long> {
    List<WebControl> findAllByModuleId(Long moduleId);
}
