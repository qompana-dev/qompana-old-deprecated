package com.devqube.crmuserservice.permissions.role;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.association.RemoveCrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.permissions.profile.Profile;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
//@AuthFilterEnabled
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Role {
    @Id
    @SequenceGenerator(name="role_seq", sequenceName="role_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="role_seq")
    @AuthFields(list = {
            @AuthField(frontendId = "PersonAddComponent.roleField", controller = "createAccount"),
            @AuthField(frontendId = "PersonDetailComponent.roleField", controller = "getAccount"),
            @AuthField(frontendId = "PersonDetailComponent.roleField", controller = "updateAccount")
    })
    private Long id;

    @NotNull
    @Column(unique = true)
    @AuthFields(list = {
            @AuthField(frontendId = "PersonAddComponent.roleField", controller = "createAccount"),
            @AuthField(frontendId = "PersonDetailComponent.roleField", controller = "updateAccount")
    })
    private String name;

    @ManyToMany(cascade = CascadeType.MERGE )
    @JoinTable(
            name = "profile2role",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "profile_id"))
    @AuthFields(list = {
            @AuthField(frontendId = "PersonAddComponent.roleField", controller = "createAccount"),
            @AuthField(frontendId = "PersonDetailComponent.roleField", controller = "updateAccount")
    })
    private Set<Profile> profiles;

    @OneToMany(mappedBy = "role")
    @JsonIgnore
    @AuthFields(list = {
            @AuthField(frontendId = "PersonAddComponent.roleField", controller = "createAccount"),
            @AuthField(frontendId = "PersonDetailComponent.roleField", controller = "updateAccount")
    })
    private Set<Account> accounts;

    @OneToMany
    @JoinTable(
            name="role_children",
            joinColumns = @JoinColumn(name="parent_id"),
            inverseJoinColumns = @JoinColumn(name="child_id")
    )
    @AuthFields(list = {
            @AuthField(frontendId = "PersonAddComponent.roleField", controller = "createAccount"),
            @AuthField(frontendId = "PersonDetailComponent.roleField", controller = "updateAccount")
    })
    private Set<Role> children;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return id.equals(role.id) &&
                name.equals(role.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }


    @PreRemove
    public void preRemove() {
        if (this.id != null) {
            RemoveCrmObject.addObjectToRemove(CrmObjectType.role, this.id);
        }
    }
}
