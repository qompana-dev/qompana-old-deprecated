package com.devqube.crmuserservice.permissions.role.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * IdAndEmailDTO
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-10-24T12:47:04.725339+02:00[Europe/Warsaw]")

public class IdAndEmailDTO {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("personEmail")
    private String personEmail;

    public IdAndEmailDTO id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     */
    @ApiModelProperty(value = "")


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IdAndEmailDTO personEmail(String personEmail) {
        this.personEmail = personEmail;
        return this;
    }

    /**
     * Get personEmail
     *
     * @return personEmail
     */
    @ApiModelProperty(value = "")


    public String getPersonEmail() {
        return personEmail;
    }

    public void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IdAndEmailDTO idAndEmailDTO = (IdAndEmailDTO) o;
        return Objects.equals(this.id, idAndEmailDTO.id) &&
                Objects.equals(this.personEmail, idAndEmailDTO.personEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, personEmail);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class IdAndEmailDTO {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    personEmail: ").append(toIndentedString(personEmail)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

