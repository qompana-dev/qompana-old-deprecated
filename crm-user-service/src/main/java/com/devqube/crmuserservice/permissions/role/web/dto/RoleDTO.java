package com.devqube.crmuserservice.permissions.role.web.dto;

import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileListDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * RoleDTO
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-10-24T12:47:04.725339+02:00[Europe/Warsaw]")

public class RoleDTO {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("supervisor")
    private Long supervisor;

    @JsonProperty("hasChildren")
    private Boolean hasChildren;

    @JsonProperty("profiles")
    @Valid
    private List<ProfileListDTO> profiles = null;

    @JsonProperty("accounts")
    @Valid
    private List<IdAndEmailDTO> accounts = null;

    public RoleDTO id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     */
    @ApiModelProperty(value = "")


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoleDTO name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     */
    @ApiModelProperty(required = true, value = "")
    @NotNull


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleDTO supervisor(Long supervisor) {
        this.supervisor = supervisor;
        return this;
    }

    /**
     * Get supervisor
     *
     * @return supervisor
     */
    @ApiModelProperty(value = "")


    public Long getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Long supervisor) {
        this.supervisor = supervisor;
    }

    public RoleDTO hasChildren(Boolean hasChildren) {
        this.hasChildren = hasChildren;
        return this;
    }

    /**
     * Get hasChildren
     *
     * @return hasChildren
     */
    @ApiModelProperty(value = "")


    public Boolean getHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(Boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public RoleDTO profiles(List<ProfileListDTO> profiles) {
        this.profiles = profiles;
        return this;
    }

    public RoleDTO addProfilesItem(ProfileListDTO profilesItem) {
        if (this.profiles == null) {
            this.profiles = new ArrayList<>();
        }
        this.profiles.add(profilesItem);
        return this;
    }

    /**
     * Get profiles
     *
     * @return profiles
     */
    @ApiModelProperty(value = "")

    @Valid

    public List<ProfileListDTO> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<ProfileListDTO> profiles) {
        this.profiles = profiles;
    }

    public RoleDTO accounts(List<IdAndEmailDTO> accounts) {
        this.accounts = accounts;
        return this;
    }

    public RoleDTO addAccountsItem(IdAndEmailDTO accountsItem) {
        if (this.accounts == null) {
            this.accounts = new ArrayList<>();
        }
        this.accounts.add(accountsItem);
        return this;
    }

    /**
     * Get accounts
     *
     * @return accounts
     */
    @ApiModelProperty(value = "")

    @Valid

    public List<IdAndEmailDTO> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<IdAndEmailDTO> accounts) {
        this.accounts = accounts;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RoleDTO roleDTO = (RoleDTO) o;
        return Objects.equals(this.id, roleDTO.id) &&
                Objects.equals(this.name, roleDTO.name) &&
                Objects.equals(this.supervisor, roleDTO.supervisor) &&
                Objects.equals(this.hasChildren, roleDTO.hasChildren) &&
                Objects.equals(this.profiles, roleDTO.profiles) &&
                Objects.equals(this.accounts, roleDTO.accounts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, supervisor, hasChildren, profiles, accounts);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class RoleDTO {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    supervisor: ").append(toIndentedString(supervisor)).append("\n");
        sb.append("    hasChildren: ").append(toIndentedString(hasChildren)).append("\n");
        sb.append("    profiles: ").append(toIndentedString(profiles)).append("\n");
        sb.append("    accounts: ").append(toIndentedString(accounts)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

