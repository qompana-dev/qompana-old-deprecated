package com.devqube.crmuserservice.account;

public enum ThemeEnum {
    LIGHT,
    DARK,
    PROFESSIONAL
}
