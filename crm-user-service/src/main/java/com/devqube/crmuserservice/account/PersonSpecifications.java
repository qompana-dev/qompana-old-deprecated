package com.devqube.crmuserservice.account;

import com.devqube.crmuserservice.account.model.Person;
import org.springframework.data.jpa.domain.Specification;

public class PersonSpecifications {

    public static Specification<Person> findByNameOrSurnameOrEmail(String toSearch) {
        return (root, query, cb) ->
                cb.or(
                        cb.like(cb.lower(root.get("name")), "%" + toSearch.toLowerCase() + "%"),
                        cb.like(cb.lower(root.get("surname")), "%" + toSearch.toLowerCase() + "%"),
                        cb.like(cb.lower(root.get("email")), "%" + toSearch.toLowerCase() + "%"),
                        cb.like(cb.lower(cb.concat(root.get("name"), cb.concat(" ", root.get("surname")))), "%" + toSearch.toLowerCase() + "%")
                );
    }

    public static Specification<Person> findByEmail(String email) {
        return (root, query, cb) ->
                cb.equal(root.get("email"), email);
    }

}
