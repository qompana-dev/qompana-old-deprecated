package com.devqube.crmuserservice.account.web;

import com.devqube.crmuserservice.account.model.Person;
import com.devqube.crmuserservice.account.web.dto.AccountTaskDtoOut;
import org.springframework.data.domain.Page;

public class AccountDtoMapper {

    public static AccountTaskDtoOut toAccountTaskDtoOut(Person person) {
        return new AccountTaskDtoOut()
                .setId(person.getId())
                .setOwnerAccountId(person.getAccount().getId())
                .setName(person.getName() + " " + person.getSurname())
                .setEmail(person.getEmail())
                .setAvatar(person.getAvatar())
                .setPhone(person.getPhone());

    }

    public static Page<AccountTaskDtoOut> toAccountTaskDtoOut(Page<Person> personPage) {
        return personPage.map(AccountDtoMapper::toAccountTaskDtoOut);
    }

}
