package com.devqube.crmuserservice.account.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MenuOrderDto {
    private Long order;
    private String name;
    private Boolean visible;
}
