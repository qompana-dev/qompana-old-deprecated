package com.devqube.crmuserservice.account.model;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@AuthFilterEnabled
public class Person {
    @JsonProperty("id")
    @Id
    @SequenceGenerator(name="person_seq", sequenceName="person_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="person_seq")
    @AuthFields(list = {
            @AuthField(frontendId = "PersonListComponent.personIdColumn", controller = "getAccounts"),
            @AuthField(frontendId = "PersonDetailComponent.personIdField", controller = "getAccount"),
            @AuthField(frontendId = "PersonDetailComponent.personIdField", controller = "updateAccount"),
            @AuthField(frontendId = "PersonAddComponent.personIdField", controller = "createAccount"),
            @AuthField(frontendId = "PersonInfoComponent.personIdLabel", controller = "getMyAccount")
    })
    private Long id;

    @JsonProperty("name")
    @Length(min = 1, max = 200)
    @Column(name = "name", length = 200)
    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "PersonListComponent.person.nameColumn", controller = "getAccounts"),
            @AuthField(frontendId = "PersonDetailComponent.nameField", controller = "getAccount"),
            @AuthField(frontendId = "PersonDetailComponent.nameField", controller = "updateAccount"),
            @AuthField(frontendId = "PersonAddComponent.nameField", controller = "createAccount"),
            @AuthField(frontendId = "PersonInfoComponent.nameLabel", controller = "getMyAccount")
    })
    private String name;

    @JsonProperty("avatar")
    @Column(name = "avatar")
    @AuthFields(list = {
            @AuthField(controller = "getAccount"),
            @AuthField(frontendId = "PersonListComponent.person.avatarColumn", controller = "getAccounts"),
            @AuthField(frontendId = "PersonInfoComponent.avatarLabel", controller = "getMyAccount")
    })
    private String avatar;

    @JsonProperty("surname")
    @Length(min = 1, max = 200)
    @Column(name = "surname", length = 200)
    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "PersonListComponent.person.surnameColumn", controller = "getAccounts"),
            @AuthField(frontendId = "PersonDetailComponent.surnameField", controller = "getAccount"),
            @AuthField(frontendId = "PersonDetailComponent.surnameField", controller = "updateAccount"),
            @AuthField(frontendId = "PersonAddComponent.surnameField", controller = "createAccount"),
            @AuthField(frontendId = "PersonInfoComponent.surnameLabel", controller = "getMyAccount")
    })
    private String surname;

    @JsonProperty("description")
    @Length(max = 500)
    @Column(name = "description", length = 500)
    @AuthFields(list = {
            @AuthField(frontendId = "PersonListComponent.person.descriptionColumn", controller = "getAccounts"),
            @AuthField(frontendId = "PersonDetailComponent.descriptionField", controller = "getAccount"),
            @AuthField(frontendId = "PersonDetailComponent.descriptionField", controller = "updateAccount"),
            @AuthField(frontendId = "PersonAddComponent.descriptionField", controller = "createAccount"),
            @AuthField(frontendId = "PersonInfoComponent.descriptionLabel", controller = "getMyAccount")
    })
    private String description;

    @JsonProperty("phone")
    @Length(max = 128)
    @Column(name = "phone", length = 128)
    @AuthFields(list = {
            @AuthField(frontendId = "PersonListComponent.person.phoneColumn", controller = "getAccounts"),
            @AuthField(frontendId = "PersonDetailComponent.phoneField", controller = "getAccount"),
            @AuthField(frontendId = "PersonDetailComponent.phoneField", controller = "updateAccount"),
            @AuthField(frontendId = "PersonAddComponent.phoneField", controller = "createAccount"),
            @AuthField(frontendId = "PersonInfoComponent.phoneLabel", controller = "getMyAccount")
    })
    private String phone;

    @JsonProperty("email")
    @Length(min = 1, max = 100)
    @Column(name = "email", unique = true, length = 100)
    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "PersonListComponent.person.emailColumn", controller = "getAccounts"),
            @AuthField(frontendId = "PersonDetailComponent.emailField", controller = "getAccount"),
            @AuthField(frontendId = "PersonDetailComponent.emailField", controller = "updateAccount"),
            @AuthField(frontendId = "PersonAddComponent.emailField", controller = "createAccount"),
            @AuthField(frontendId = "PersonInfoComponent.emailLabel", controller = "getMyAccount")
    })
    private String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name = "password")
    private String password;

    @JsonProperty("additionalData")
    @Type(type = "text")
    @AuthFields(list = {
            @AuthField(frontendId = "PersonListComponent.additionalDataColumn", controller = "getAccounts"),
            @AuthField(frontendId = "PersonDetailComponent.additionalDataField", controller = "getAccount"),
            @AuthField(frontendId = "PersonDetailComponent.additionalDataField", controller = "updateAccount"),
            @AuthField(frontendId = "PersonAddComponent.additionalDataField", controller = "createAccount"),
            @AuthField(frontendId = "PersonInfoComponent.additionalDataLabel", controller = "getMyAccount")
    })
    private String additionalData;

    @JsonIgnore
    @OneToOne(mappedBy = "person", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Account account;

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", description='" + description + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return email.equals(person.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}

