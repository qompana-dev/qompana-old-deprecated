package com.devqube.crmuserservice.account.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.devqube.crmuserservice.account.model.Account;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountInfoDto {
    private Long id;
    private String email;
    private String name;
    private String surname;
    private Long roleId;
    private String roleName;
    private String avatar;

    public AccountInfoDto(Account account) {
        this.id = account.getId();
        if (account.getPerson() != null) {
            this.email = account.getPerson().getEmail();
            this.name = account.getPerson().getName();
            this.surname = account.getPerson().getSurname();
            this.avatar = account.getPerson().getAvatar();
        }
        if (account.getRole() != null) {
            this.roleId = account.getRole().getId();
            this.roleName = account.getRole().getName();
        }
    }
}
