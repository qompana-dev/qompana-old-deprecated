package com.devqube.crmuserservice.account.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import java.util.Objects;

/**
 * Account
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-10-24T12:47:04.725339+02:00[Europe/Warsaw]")

public class Account {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("person")
    private Person person = null;

    @JsonProperty("isActivated")
    private Boolean isActivated;

    @JsonProperty("role")
    private com.devqube.crmuserservice.permissions.role.Role role = null;

    @JsonProperty("version")
    private Long version;

    @JsonProperty("deleted")
    private Boolean deleted;

    public Account id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     */
    @ApiModelProperty(value = "")


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account person(Person person) {
        this.person = person;
        return this;
    }

    /**
     * Get person
     *
     * @return person
     */
    @ApiModelProperty(value = "")

    @Valid

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Account isActivated(Boolean isActivated) {
        this.isActivated = isActivated;
        return this;
    }

    /**
     * Get isActivated
     *
     * @return isActivated
     */
    @ApiModelProperty(value = "")


    public Boolean getIsActivated() {
        return isActivated;
    }

    public void setIsActivated(Boolean isActivated) {
        this.isActivated = isActivated;
    }

    public Account role(com.devqube.crmuserservice.permissions.role.Role role) {
        this.role = role;
        return this;
    }

    /**
     * Get role
     *
     * @return role
     */
    @ApiModelProperty(value = "")

    @Valid

    public com.devqube.crmuserservice.permissions.role.Role getRole() {
        return role;
    }

    public void setRole(com.devqube.crmuserservice.permissions.role.Role role) {
        this.role = role;
    }

    public Account version(Long version) {
        this.version = version;
        return this;
    }

    /**
     * Get version
     *
     * @return version
     */
    @ApiModelProperty(value = "")


    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Account deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    /**
     * Get deleted
     *
     * @return deleted
     */
    @ApiModelProperty(value = "")


    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return Objects.equals(this.id, account.id) &&
                Objects.equals(this.person, account.person) &&
                Objects.equals(this.isActivated, account.isActivated) &&
                Objects.equals(this.role, account.role) &&
                Objects.equals(this.version, account.version) &&
                Objects.equals(this.deleted, account.deleted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, person, isActivated, role, version, deleted);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Account {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    person: ").append(toIndentedString(person)).append("\n");
        sb.append("    isActivated: ").append(toIndentedString(isActivated)).append("\n");
        sb.append("    role: ").append(toIndentedString(role)).append("\n");
        sb.append("    version: ").append(toIndentedString(version)).append("\n");
        sb.append("    deleted: ").append(toIndentedString(deleted)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

