package com.devqube.crmuserservice.account;

import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.association.RemoveCrmObject;
import com.devqube.crmshared.exception.*;
import com.devqube.crmshared.license.LicenseCheck;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.search.EmailSearch;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.devqube.crmuserservice.account.exception.TooManyUserException;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.account.model.Person;
import com.devqube.crmuserservice.account.web.dto.*;
import com.devqube.crmuserservice.accountConfiguration.AccountConfiguration;
import com.devqube.crmuserservice.accountConfiguration.AccountConfigurationService;
import com.devqube.crmuserservice.globalConfiguration.GlobalConfigurationService;
import com.devqube.crmuserservice.permissions.profile.Profile;
import com.devqube.crmuserservice.permissions.role.Role;
import com.devqube.crmuserservice.permissions.role.RoleRepository;
import com.devqube.crmuserservice.token.TokenService;
import com.devqube.crmuserservice.token.model.Token;
import com.devqube.crmuserservice.token.model.TokenTypeEnum;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.OperationNotSupportedException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AccountsService {
    private static final long MAX_LICENCE_DAYS_TO_EXPIRE = 7;
    private final ModelMapper strictModelMapper;
    private final AccountsRepository accountsRepository;
    private final PersonRepository personRepository;
    private final GlobalConfigurationService configService;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final TokenService tokenService;
    private final AccessService accessService;
    private final AccountConfigurationService accountConfigurationService;
    private final LicenseCheck licenseCheck;
    private final String defaultMenuOrderJson;

    public AccountsService(@Qualifier("strictModelMapper") ModelMapper strictModelMapper,
                           AccountsRepository accountsRepository, PersonRepository personRepository,
                           GlobalConfigurationService configService, RoleRepository roleRepository,
                           PasswordEncoder passwordEncoder, TokenService tokenService, AccessService accessService,
                           AccountConfigurationService accountConfigurationService, LicenseCheck licenseCheck,
                           String defaultMenuOrderJson) {
        this.strictModelMapper = strictModelMapper;
        this.accountsRepository = accountsRepository;
        this.personRepository = personRepository;
        this.configService = configService;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.tokenService = tokenService;
        this.accessService = accessService;
        this.accountConfigurationService = accountConfigurationService;
        this.licenseCheck = licenseCheck;
        this.defaultMenuOrderJson = defaultMenuOrderJson;
    }

    @Transactional(rollbackFor = BadRequestException.class)
    public Account save(Account account) throws UserEmailOccupiedException, BadRequestException, EntityNotFoundException, KafkaSendMessageException {
        if (account.getRole() == null) {
            throw new BadRequestException();
        }
        String email = account.getPerson().getEmail();
        if (accountsRepository.countByPersonEmail(email) == 0) {
            String password = account.getPerson().getPassword();
            if (StringUtils.isNotEmpty(password))
                account.getPerson().setPassword(EncodingUtil.encodePassword(password));

            if (isAdditionalDataNotValid(account)) {
                throw new BadRequestException();
            }
            account = accessService.assignAndVerify(account);
            account.setActivated(false);
            account.setDeleted(false);
            account.setVersion(0L);

            assignRoleToAccount(account);

            Account result = this.accountsRepository.save(account);

            saveDefaultAccountConfiguration(result);

            sendActivationEmail(result);
            return result;
        }
        throw new UserEmailOccupiedException("Email " + email + " is occupied.");
    }

    public void changePersonAvatar(PersonAvatarDto personAvatarDto) throws EntityNotFoundException {
        Account account = getMyAccount();
        account.getPerson().setAvatar(personAvatarDto.getAvatar());
        accountsRepository.save(account);
    }

    public PersonAvatarDto getMyAvatar() throws EntityNotFoundException {
        Account account = getMyAccount();
        return new PersonAvatarDto(account.getPerson().getAvatar());
    }

    private void saveDefaultAccountConfiguration(Account account) throws BadRequestException {
        this.accountConfigurationService.save(new AccountConfiguration(account, "pl", "YYYY/MM/DD",
                "0", "9", "12", defaultMenuOrderJson, ThemeEnum.LIGHT.name()));
    }

    public List<Account> getAll(Boolean onlyWithoutRole) {
        if (onlyWithoutRole != null && onlyWithoutRole) {
            return accountsRepository.findAllByDeletedFalseAndRoleNullOrderById();
        } else {
            return accountsRepository.findAllByDeletedFalseOrderById();
        }
    }

    public Page<PersonListDto> getAll(Pageable pageable) {
        return accountsRepository.findAllByDeletedFalse(pageable).map(this::mapToAccountListDto);
    }

    public void modifyAccount(Long accountId, Account account) throws EntityNotFoundException, DifferentVersionException,
            OperationNotSupportedException, BadRequestException {
        if (account.getRole() == null) {
            throw new BadRequestException();
        }

        Account fromDb = accountsRepository.findById(accountId).orElseThrow(EntityNotFoundException::new);

        account = accessService.assignAndVerify(account);
        updateAccount(account, fromDb);
        assignRoleToAccount(account);

        validateModification(account, fromDb);

        account.setVersion(fromDb.getVersion() + 1);
        accountsRepository.save(account);
    }

    private void updateAccount(Account account, Account fromDb){
        account.setId(fromDb.getId());
        account.setActivated(fromDb.getActivated());
        account.setDeleted(fromDb.getDeleted());
        account.setTokens(fromDb.getTokens());
        account.setTeams(fromDb.getTeams());
        account.getPerson().setId(fromDb.getPerson().getId());
        account.getPerson().setPassword(fromDb.getPerson().getPassword());
        account.getPerson().setEmail(fromDb.getPerson().getEmail());
        account.getPerson().setAvatar(fromDb.getPerson().getAvatar());
        account.getPerson().setAccount(account);
    }

    private void validateModification(Account account, Account fromDb) throws DifferentVersionException, OperationNotSupportedException, BadRequestException {
        if (!fromDb.getVersion().equals(account.getVersion())) {
            throw new DifferentVersionException();
        }
        if (!fromDb.getPerson().getEmail().equals(account.getPerson().getEmail())) {
            throw new OperationNotSupportedException("You can't change the e-mail.");
        }
        if (fromDb.getId().equals(getAccountIdByEmail(CurrentRequestUtil.getLoggedInAccountEmail()))
                && fromDb.getActivated() != account.getActivated()) {
            throw new OperationNotSupportedException("You can't lock/unlock your own account.");
        }
        if (isAdditionalDataNotValid(account)) {
            throw new BadRequestException();
        }
    }

    public Long getMyAccountId() throws EntityNotFoundException {
        return Optional.ofNullable(getAccountIdByEmail(CurrentRequestUtil.getLoggedInAccountEmail())).orElseThrow(EntityNotFoundException::new);
    }

    public Account getMyAccount() throws EntityNotFoundException {
        return getAccount(getAccountIdByEmail(CurrentRequestUtil.getLoggedInAccountEmail()));
    }

    public List<AccountInfoDto> getAccountsInfoByIds(List<Long> ids) {
        return this.accountsRepository.findByIdIn(ids).stream().map(AccountInfoDto::new).collect(Collectors.toList());
    }

    public List<AccountInfoDto> getAccountInfoChildListForLoggedUserRole() throws EntityNotFoundException {
        Account account = this.accountsRepository.findByPersonEmail(CurrentRequestUtil.getLoggedInAccountEmail());
        if (account == null) {
            throw new EntityNotFoundException();
        }
        Role role = account.getRole();
        if (role == null) {
            throw new EntityNotFoundException();
        }
        List<AccountInfoDto> result = new ArrayList<>();
        if (role.getChildren() != null) {
            role.getChildren().forEach(childRole -> result.addAll(getAccountInfoListByRoleId(childRole.getId())));
        }
        result.add(new AccountInfoDto(account));
        return result;
    }

    private List<AccountInfoDto> getAccountInfoListByRoleId(Long roleId) {
        return accountsRepository.findAllByRoleId(roleId)
                .stream().map(AccountInfoDto::new).collect(Collectors.toList());

    }

    public Account getAccount(Long accountId) throws EntityNotFoundException {
        Optional<Account> fromDb = this.accountsRepository.findById(accountId);

        if (fromDb.isEmpty() || fromDb.get().getDeleted()) {
            throw new EntityNotFoundException();
        }

        return fromDb.get();
    }

    public Account getAccountByEmail(String email) throws EntityNotFoundException {
        Account fromDb = this.accountsRepository.findByPersonEmail(email);

        if (fromDb == null || fromDb.getDeleted()) {
            throw new EntityNotFoundException();
        }
        return fromDb;
    }

    public AccountDTO getAccountDtoByEmail(String email) throws EntityNotFoundException {
        return mapToAccountDTO(getAccountByEmail(email));
    }

    public String getRoleNameByAccountEmail(String email) throws EntityNotFoundException {
        Account fromDb = this.accountsRepository.findByPersonEmail(email);

        if (fromDb == null || fromDb.getDeleted()) {
            throw new EntityNotFoundException();
        }

        return fromDb.getRole().getName();
    }

    @Transactional
    public void removeAccount(Long accountId) throws EntityNotFoundException, BadRequestException {
        Optional<Account> fromDb = this.accountsRepository.findById(accountId);

        if (fromDb.isEmpty()) {
            throw new EntityNotFoundException();
        }

        if (accountId.equals(getAccountIdByEmail(CurrentRequestUtil.getLoggedInAccountEmail()))) {
            throw new BadRequestException();
        }

        Account account = fromDb.get();
        account.setDeleted(true);
        account.setTokens(new HashSet<>());
        account.setPerson(setEverythingButNameAndSurnameNull(account.getPerson()));

        accountsRepository.save(account);
        RemoveCrmObject.addObjectToRemove(CrmObjectType.user, accountId);
    }

    private Person setEverythingButNameAndSurnameNull(Person person) {
        person.setAvatar(null);
        person.setDescription(null);
        person.setPhone(null);
        person.setEmail(UUID.randomUUID().toString());
        person.setPassword(null);
        person.setAdditionalData(null);
        return person;
    }

    void sendActivationEmail(Account account) throws KafkaSendMessageException {
        this.tokenService.sendEmailWithToken(account, TokenTypeEnum.SEND_ACTIVATION_CODE);
    }

    public void resendActivationEmail(Long accountId) throws KafkaSendMessageException {
        Account account = accountsRepository.getOne(accountId);
        this.tokenService.sendEmailWithToken(account, TokenTypeEnum.RESEND_ACTIVATION_CODE);
    }

    public void sendResetPasswordLink(String email) throws KafkaSendMessageException {
        Account account = accountsRepository.findByPersonEmail(email);
        if (account != null) {
            this.tokenService.sendEmailWithToken(account, TokenTypeEnum.RESET_PASSWORD);
        }
    }

    public void setAccountPassword(String token, SetPasswordRequest newPassword) throws BadRequestException, EntityNotFoundException, PasswordPolicyNotFulfilledException {
        Token tokenObj = tokenService.getValidTokenObject(token, TokenTypeEnum.SEND_ACTIVATION_CODE, TokenTypeEnum.RESEND_ACTIVATION_CODE, TokenTypeEnum.RESET_PASSWORD);
        if (this.isPasswordPolicyNotFulfilled(newPassword.getPassword())) {
            throw new PasswordPolicyNotFulfilledException("Password doesn't match the regex");
        }

        tokenObj.getAccount()
                .getPerson().setPassword(passwordEncoder.encode(newPassword.getPassword()));

        if ((tokenObj.getType().equals(TokenTypeEnum.SEND_ACTIVATION_CODE) ||
                tokenObj.getType().equals(TokenTypeEnum.RESEND_ACTIVATION_CODE)) &&
                !Boolean.TRUE.equals(tokenObj.getAccount().getActivated())) {
            tokenObj.getAccount().setActivated(isLessActiveAccountsThenCarryLicenseMaxUser(true));
        }
        accountsRepository.save(tokenObj.getAccount());

        tokenService.removeToken(tokenObj.getId());
    }

    public void changePassword(Long accountId, Passwords passwords)
            throws EntityNotFoundException, BadOldPasswordException, PasswordPolicyNotFulfilledException {
        Account fromDb = getAccount(accountId);
        if (!passwordEncoder.matches(passwords.getOldPassword(), fromDb.getPerson().getPassword())) {
            throw new BadOldPasswordException("Old password is different than given one.");
        }

        if (this.isPasswordPolicyNotFulfilled(passwords.getNewPassword())) {
            throw new PasswordPolicyNotFulfilledException("Password doesn't match the regex");
        }

        fromDb.getPerson().setPassword(passwordEncoder.encode(passwords.getNewPassword()));
        accountsRepository.save(fromDb);
    }

    private AccountDTO mapToAccountDTO(Account fromDb) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(fromDb.getId());
        accountDTO.setEmail(fromDb.getPerson().getEmail());
        accountDTO.setPassword(fromDb.getPerson().getPassword());
        accountDTO.setActivated(fromDb.getActivated());
        accountDTO.setCredentialsExpired(tooMeanUserIsActive(fromDb.getActivated(), false) && isNotAdminRole(fromDb.getRole()));
        return accountDTO;
    }

    private boolean isNotAdminRole(Role role){
        return role != null && roleRepository.findByNameContaining("admin")
                .stream()
                .noneMatch(findRole -> findRole.getId().equals(role.getId()));
    }

    private Long getAccountIdByEmail(String email) {
        Account account = accountsRepository.findByPersonEmail(email);
        if (account == null) {
            return null;
        }
        return account.getId();
    }

    private boolean isPasswordPolicyNotFulfilled(String password) {
        if (configService.isPasswordPolicyActive()) {
            Pattern p = Pattern.compile(configService.getPasswordPolicyRegex());
            return !p.matcher(password).find();
        }
        return false;
    }

    private boolean isAdditionalDataNotValid(Account account) {
        if (account != null && account.getPerson() != null) {
            return !isAdditionalDataValid(account.getPerson().getAdditionalData());
        }
        return true;
    }

    private boolean isAdditionalDataValid(String additionalData) {
        if (additionalData == null || additionalData.length() == 0) {
            return true;
        }
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.readValue(additionalData, objectMapper.getTypeFactory().constructType(List.class));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private boolean isLessActiveAccountsThenCarryLicenseMaxUser(Boolean refreshMaxUserCount){
        long maxUserCount = refreshMaxUserCount ? licenseCheck.refreshMaxUserCount() : licenseCheck.getMaxUserCount();
        return accountsRepository.countByDeletedFalseAndActivatedTrue() <= maxUserCount;
    }

    private boolean tooMeanUserIsActive(Boolean activated, Boolean refreshMaxUserCount){
        return Boolean.TRUE.equals(activated) && !isLessActiveAccountsThenCarryLicenseMaxUser(refreshMaxUserCount);
    }

    public void changeActivate(Long accountId, Boolean activated) throws EntityNotFoundException, TooManyUserException {
        Account fromDb = getAccount(accountId);

        if (tooMeanUserIsActive(activated, true)){
            fromDb.setActivated(false);
            accountsRepository.save(fromDb);
            throw new TooManyUserException();
        } else {
            fromDb.setActivated(activated);
            accountsRepository.save(fromDb);
        }
    }

    private void assignRoleToAccount(Account account) throws EntityNotFoundException, BadRequestException {
        if (account.getRole() == null || account.getRole().getId() == null) {
            throw new BadRequestException();
        }
        Role role = roleRepository.findOneById(account.getRole().getId());
        if (role == null) {
            throw new EntityNotFoundException();
        }
        if (role.getAccounts() != null && !role.getAccounts().isEmpty()
                && role.getChildren() != null && !role.getChildren().isEmpty()) {
            Optional<Account> modifyingAccount = role.getAccounts().stream().filter(e -> e.getId().equals(account.getId())).findFirst();
            if (modifyingAccount.isEmpty()) {
                throw new BadRequestException("Cannot assign account to this role. Role is occupied and has subroles" +
                        " - only one account can be assigned to it.");
            }
        }
        account.setRole(role);

    }

    public List<Account> findAllContaining(String term) {
        return accountsRepository.findByNameContaining(term);
    }

    public Page<Person> findAll(Specification<Person> spec, Pageable pageable) {
        return personRepository.findAll(spec, pageable);
    }

    public List<EmailSearch> findByEmailContaining(String term, String email) {
        return accountsRepository.findByEmailContaining(term)
                .stream()
                .filter(e -> !email.equalsIgnoreCase(e.getPerson().getEmail()))
                .map(e -> new EmailSearch(CrmObjectType.user,
                        String.format("%s %s", e.getPerson().getName(), e.getPerson().getSurname()),
                        e.getPerson().getEmail()))
                .collect(Collectors.toList());
    }

    public List<Account> findAllByIds(List<Long> ids) {
        return accountsRepository.findByIdIn(ids);
    }


    private PersonListDto mapToAccountListDto(Account account) {
        if (account == null) {
            return null;
        }
        PersonListDto result = strictModelMapper.map(account.getPerson(), PersonListDto.class);
        result.setId(account.getId());
        result.setActivated(account.getActivated());
        if (account.getRole() != null) {
            result.setRoleName(account.getRole().getName());

            Set<Profile> profiles = account.getRole().getProfiles();
            if (profiles != null && profiles.size() > 0) {
                String profileValue = profiles.stream().map(Profile::getName).sorted().collect(Collectors.joining(", "));
                result.setProfileName(profileValue);
            }
        }

        return result;
    }

    public boolean refreshLicence(){
        return licenseCheck.refreshLicence();
    }

    public Long getAndRefreshLicenceExpiredDate(){
        long licenceDaysToExpire = licenseCheck.refreshLicenseDaysLeft();
        if (licenceDaysToExpire <= MAX_LICENCE_DAYS_TO_EXPIRE) {
            return licenceDaysToExpire;
        } else {
            return null;
        }
    }

    public Long getAndRefreshLicenceNumberOfUsers(){
        long result =  licenseCheck.refreshMaxUserCount() - accountsRepository.countByDeletedFalseAndActivatedTrue();
        if (result < 0) {
            return Math.abs(result);
        } else {
            return null;
        }
    }
}
