package com.devqube.crmuserservice.account.web;

import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.EmailSearch;
import com.devqube.crmshared.user.dto.AccountEmail;
import com.devqube.crmuserservice.ApiUtil;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.account.web.dto.*;
import com.devqube.crmuserservice.accountConfiguration.AccountConfiguration;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "accounts")
public interface AccountsApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Change avatar", nickname = "changeAvatar", notes = "Method used to change account's avatar")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "avatar changed"),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/account/avatar",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> changeAvatar(@ApiParam(value = "Account object with new avatar", required = true) @Valid @RequestBody PersonAvatarDto personAvatarDto) throws PermissionDeniedException, EntityNotFoundException {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get my avatar", nickname = "getMyAvatar", notes = "Method used to get logged user avatar")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "avatar", response = PersonAvatarDto.class),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/account/avatar",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<PersonAvatarDto> getMyAvatar() throws EntityNotFoundException {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Create account", nickname = "createAccount", notes = "Method used to create new account", response = Account.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "added account successfully", response = Account.class),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 480, message = "TooManyUserException"),
            @ApiResponse(code = 481, message = "UserEmailOccupiedException")})
    @RequestMapping(value = "/accounts",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Account> createAccount(@ApiParam(value = "Created account object", required = true) @Valid @RequestBody Account account) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"person\" : {    \"password\" : \"password\",    \"phone\" : \"phone\",    \"surname\" : \"surname\",    \"name\" : \"name\",    \"description\" : \"description\",    \"id\" : 6,    \"email\" : \"email\"  },  \"profiles\" : [ {    \"name\" : \"name\",    \"id\" : 1  }, {    \"name\" : \"name\",    \"id\" : 1  } ],  \"id\" : 0,  \"activated\" : true}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get account emails", nickname = "getAccountEmails", notes = "Method used to get account emails.", response = AccountEmail.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "list of account emails", response = AccountEmail.class, responseContainer = "List")})
    @RequestMapping(value = "/accounts/emails",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<AccountEmail>> getAccountEmails(@ApiParam(value = "") @Valid @RequestParam(value = "onlyWithoutRole", required = false) Boolean onlyWithoutRole) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"surname\" : \"surname\",  \"name\" : \"name\",  \"id\" : 0,  \"email\" : \"email\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get account info list", nickname = "getAccountInfoChildListForLoggedUserRole", notes = "Method used to get account child list for logged user role.", response = AccountInfoDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "list of account info", response = AccountEmail.class, responseContainer = "List")})
    @RequestMapping(value = "/accounts/me/child-accounts",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<AccountInfoDto>> getAccountInfoChildListForLoggedUserRole() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get account info list by ids", nickname = "getAccountsInfoByIds", notes = "Method used to get accounts info by ids.", response = AccountInfoDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "list of account info", response = AccountInfoDto[].class, responseContainer = "List")})
    @RequestMapping(value = "/accounts/info/{ids}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<AccountInfoDto>> getAccountsInfoByIds(@PathVariable List<Long> ids) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get my account id", nickname = "getMyAccountId", notes = "Method used to get logged user aaccount id", response = Long.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account id", response = Long.class),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/accounts/me/id",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Long> getMyAccountId() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get account id by email", nickname = "getAccountIdByEmail", notes = "Method used to get account id by email", response = Long.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account id", response = Long.class),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/accounts/id",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Long> getAccountIdByEmail(@ApiParam(value = "email", required = true) @Valid @NotNull @RequestParam(value = "email", required = true) String email) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get my account", nickname = "getMyAccount", notes = "Method used to get logged user account", response = com.devqube.crmuserservice.account.web.dto.Account.class, authorizations = {
            @Authorization(value = "bearerAuth")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account update", response = com.devqube.crmuserservice.account.web.dto.Account.class),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/accounts/me",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Account> getMyAccount() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"role\" : {    \"name\" : \"name\",    \"profiles\" : [ {      \"permissions\" : [ {        \"view\" : true,        \"edit\" : true,        \"name\" : \"name\",        \"create\" : true,        \"id\" : 5,        \"type\" : \"type\",        \"delete\" : true      }, {        \"view\" : true,        \"edit\" : true,        \"name\" : \"name\",        \"create\" : true,        \"id\" : 5,        \"type\" : \"type\",        \"delete\" : true      } ],      \"name\" : \"name\",      \"id\" : 5    }, {      \"permissions\" : [ {        \"view\" : true,        \"edit\" : true,        \"name\" : \"name\",        \"create\" : true,        \"id\" : 5,        \"type\" : \"type\",        \"delete\" : true      }, {        \"view\" : true,        \"edit\" : true,        \"name\" : \"name\",        \"create\" : true,        \"id\" : 5,        \"type\" : \"type\",        \"delete\" : true      } ],      \"name\" : \"name\",      \"id\" : 5    } ],    \"id\" : 1  },  \"deleted\" : true,  \"person\" : {    \"password\" : \"password\",    \"phone\" : \"phone\",    \"surname\" : \"surname\",    \"name\" : \"name\",    \"description\" : \"description\",    \"id\" : 6,    \"additionalData\" : \"additionalData\",    \"email\" : \"email\"  },  \"id\" : 0,  \"isActivated\" : true,  \"version\" : 2}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get account by id", nickname = "getAccount", notes = "Method used to get account", response = com.devqube.crmuserservice.account.web.dto.Account.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account update", response = com.devqube.crmuserservice.account.web.dto.Account.class),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/accounts/{accountId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Account> getAccount(@ApiParam(value = "The account ID", required = true) @PathVariable("accountId") Long accountId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"person\" : {    \"password\" : \"password\",    \"phone\" : \"phone\",    \"surname\" : \"surname\",    \"name\" : \"name\",    \"description\" : \"description\",    \"id\" : 6,    \"email\" : \"email\"  },  \"profiles\" : [ {    \"name\" : \"name\",    \"id\" : 1  }, {    \"name\" : \"name\",    \"id\" : 1  } ],  \"id\" : 0,  \"activated\" : true,  \"version\" : 5}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get accounts", nickname = "getAccounts", notes = "Method used to get accounts", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "list of accounts", response = org.springframework.data.domain.Page.class)})
    @RequestMapping(value = "/accounts",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<org.springframework.data.domain.Page> getAccounts(@ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update account", nickname = "updateAccount", notes = "Method used to modify account")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account update"),
            @ApiResponse(code = 400, message = "validation error", response = String.class),
            @ApiResponse(code = 404, message = "account not found"),
            @ApiResponse(code = 409, message = "account not modified. Version is incorrect"),
            @ApiResponse(code = 423, message = "operation not supported")})
    @RequestMapping(value = "/accounts/{accountId}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> updateAccount(@ApiParam(value = "The account ID", required = true) @PathVariable("accountId") Long accountId, @ApiParam(value = "Modified account object", required = true) @Valid @RequestBody Account account) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Delete account", nickname = "removeAccount", notes = "Method used to remove account")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account removed"),
            @ApiResponse(code = 400, message = "account to delete is your own account"),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/accounts/{accountId}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> removeAccount(@ApiParam(value = "The account ID", required = true) @PathVariable("accountId") Long accountId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Change password", nickname = "changePassword", notes = "Method used to change account's password")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account update"),
            @ApiResponse(code = 400, message = "password policy not fulfilled", response = String.class),
            @ApiResponse(code = 404, message = "account not found"),
            @ApiResponse(code = 409, message = "bad old password")})
    @RequestMapping(value = "/accounts/{accountId}/password",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> changePassword(@ApiParam(value = "The account ID", required = true) @PathVariable("accountId") Long accountId, @ApiParam(value = "Account object with new password", required = true) @Valid @RequestBody Passwords passwords) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "resend activation link", nickname = "resendActivationLink", notes = "Method used to resend activation link", authorizations = {
            @Authorization(value = "bearerAuth")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "mail resent"),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/accounts/{accountId}/resend-activation-link",
            method = RequestMethod.GET)
    default ResponseEntity<Void> resendActivationLink(@ApiParam(value = "The account ID", required = true) @PathVariable("accountId") Long accountId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "send reset password link", nickname = "sendResetPasswordLink", notes = "Method used to send reset password link")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "mail resent")})
    @RequestMapping(value = "/accounts/reset-password",
            method = RequestMethod.GET)
    default ResponseEntity<Void> sendResetPasswordLink(@NotNull @ApiParam(value = "The account ID", required = true) @Valid @RequestParam(value = "email", required = true) String email) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "set password", nickname = "setPassword", notes = "Method used to set password for new account")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "password changed"),
            @ApiResponse(code = 400, message = "token expired or password is incorrect"),
            @ApiResponse(code = 404, message = "token not found")})
    @RequestMapping(value = "/accounts/password/{token}",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Void> setPassword(@ApiParam(value = "token", required = true) @PathVariable("token") String token, @ApiParam(value = "Password object", required = true) @Valid @RequestBody SetPasswordRequest setPasswordRequest) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Change activate", nickname = "changeActivate", notes = "Method used to modify account activate value.", authorizations = {
            @Authorization(value = "bearerAuth")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "activate status changed"),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/accounts/{accountId}/activated/{activated}",
            method = RequestMethod.PUT)
    default ResponseEntity<Void> changeActivate(@ApiParam(value = "The account ID", required = true) @PathVariable("accountId") Long accountId, @ApiParam(value = "The activated value", required = true) @PathVariable("activated") Boolean activated) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get account by email", nickname = "getAccountByEmail", notes = "Method used to get account by e-mail", response = AccountDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account found", response = AccountDTO.class),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/accounts/by-email/{email}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<AccountDTO> getAccountByEmail(@ApiParam(value = "The account email", required = true) @PathVariable("email") String email) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"password\" : \"password\",  \"id\" : 0,  \"email\" : \"email\",  \"activated\" : true}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get AccountEmail object by email", nickname = "getAccountEmailByEmail", notes = "Method used to get AccountEmail object by e-mail", response = AccountEmail.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account found", response = AccountEmail.class),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/internal/accounts/emails/by-email/{email}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<AccountEmail> getAccountEmailByEmail(@ApiParam(value = "The account email", required = true) @PathVariable("email") String email) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"name\" : \"name\", \"surname\" : \"surname\",  \"id\" : 0,  \"email\" : \"email\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "update configuration", nickname = "modifyAccountConfiguration", notes = "Method used to update account's configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "account configuration"),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/accounts/{accountId}/configuration",
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> modifyAccountConfiguration(
            @ApiParam(value = "The account ID", required = true) @PathVariable("accountId") Long accountId,
            @ApiParam(value = "Modified account object", required = true) @Valid @RequestBody AccountConfiguration accountConfiguration) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "update menu order", nickname = "modifyAccountMenuOrder",
            notes = "Method used to update the order of the menu tabs")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "menu has been configured"),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/accounts/{accountId}/menu/configuration",
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> modifyAccountMenuOrder(
            @ApiParam(value = "The account ID", required = true) @PathVariable("accountId") Long accountId,
            @ApiParam(value = "menu configuration", required = true) @Valid @RequestBody List<MenuOrderDto> menuOrder) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "update theme", nickname = "modifyAccountTheme",
            notes = "Method used to update theme")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "theme has been configured"),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/accounts/{accountId}/configuration/theme",
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> modifyAccountTheme(
            @ApiParam(value = "The account ID", required = true) @PathVariable("accountId") Long accountId,
            @ApiParam(value = "theme", required = true) @Valid @RequestBody ThemeDtoIn themeDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get configuration for account", nickname = "getAccountConfiguration", notes = "Method used to get account's configuration", response = AccountConfiguration.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account configuration", response = AccountConfiguration.class),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/accounts/{accountId}/configuration",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<AccountConfiguration> getAccountConfiguration(@ApiParam(value = "The account ID", required = true) @PathVariable("accountId") Long accountId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"account_id\" : 0,  \"timeZone\" : \"timeZone\",  \"locale\" : \"locale\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get accounts list for search", nickname = "getAccountForSearch", notes = "Method used to get accounts list", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all customer names", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/accounts/search",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmshared.search.CrmObject>> getAccountForSearch(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get accounts page", nickname = "getAccountPage", notes = "Method used to get accounts page", response = AccountTaskDtoOut.class, responseContainer = "Page")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "accounts page", response = AccountTaskDtoOut.class, responseContainer = "Page")})
    @RequestMapping(value = "/accounts/search/task",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Page<AccountTaskDtoOut>> getAccountPage(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term,
                                                                   @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get accounts list by ids", nickname = "getAccountAsCrmObject", notes = "Method used to get accounts as crm objects by ids", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found accounts", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/accounts/crm-object",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmshared.search.CrmObject>> getAccountAsCrmObject(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "get email by account id", nickname = "getAccountEmail", notes = "Method used to get account's email", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account email", response = String.class),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/internal/accounts/{accountId}/email",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<String> getAccountEmail(@ApiParam(value = "The account ID", required = true) @PathVariable("accountId") Long accountId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    @ApiOperation(value = "get role for user with email", nickname = "getRoleForUserWithEmail", notes = "Method used to get account's role by email", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account role name", response = String.class),
            @ApiResponse(code = 404, message = "account not found")})
    @RequestMapping(value = "/internal/accounts/{email}/role",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<String> getRoleForUserWithEmail(@PathVariable("email") String email) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "search mails")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account emails", response = List.class)
    })
    List<EmailSearch> searchEmails(String email, String term);

    @ApiOperation(value = "try to refresh licence", nickname = "getExpiredDate",
            notes = "Method used to refresh licence", response = Boolean.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "refresh licence", response = String.class),
            @ApiResponse(code = 404, message = "licence not found")})
    @RequestMapping(value = "/accounts/licence/refresh",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Boolean> getLicenceRefresh() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get days left to licence expired date", nickname = "getExpiredDate",
            notes = "Method used to get number of licence expired date", response = Integer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "expired date", response = String.class),
            @ApiResponse(code = 404, message = "expired date not found")})
    @RequestMapping(value = "/accounts/licence/expiredDate",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Long> getAndRefreshLicenceExpiredDate() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get number of excessive active users", nickname = "getNumberOfUsers",
            notes = "Method used to get number of excessive active users", response = Integer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "number of users", response = String.class),
            @ApiResponse(code = 404, message = "number of users not found")})
    @RequestMapping(value = "/accounts/licence/numberOfUsers",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Long> getAndRefreshLicenceNumberOfUsers() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
