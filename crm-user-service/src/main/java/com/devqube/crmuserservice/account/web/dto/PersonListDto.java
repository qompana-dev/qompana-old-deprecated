package com.devqube.crmuserservice.account.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PersonListDto {

    //person

    @AuthField(frontendId = "PersonListComponent.personIdColumn", controller = "getAccounts")
    private Long id;

    @AuthField(frontendId = "PersonListComponent.person.nameColumn", controller = "getAccounts")
    private String name;

    @AuthField(frontendId = "PersonListComponent.person.avatarColumn", controller = "getAccounts")
    private String avatar;

    @AuthField(frontendId = "PersonListComponent.person.surnameColumn", controller = "getAccounts")
    private String surname;

    @AuthField(frontendId = "PersonListComponent.person.descriptionColumn", controller = "getAccounts")
    private String description;

    @AuthField(frontendId = "PersonListComponent.person.phoneColumn", controller = "getAccounts")
    private String phone;

    @AuthField(frontendId = "PersonListComponent.person.emailColumn", controller = "getAccounts")
    private String email;

    @AuthField(frontendId = "PersonListComponent.additionalDataColumn", controller = "getAccounts")
    private String additionalData;

    //account

    @JsonProperty("isActivated")
    @AuthField(frontendId = "PersonListComponent.activatedColumn", controller = "getAccounts")
    private Boolean activated = false;

    //role

    @AuthField(frontendId = "PersonListComponent.role.nameColumn", controller = "getAccounts")
    private String roleName;

    //profile

    // todo: add new permission
//    @AuthField(frontendId = "PersonListComponent.role.profiles.nameColumn", controller = "getAccounts")
    @AuthField(controller = "getAccounts")
    private String profileName;
}
