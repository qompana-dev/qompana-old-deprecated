package com.devqube.crmuserservice.account.web.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;

@Data
@Accessors(chain = true)
public class ThemeDtoIn {

    @NotEmpty
    private String theme;

}
