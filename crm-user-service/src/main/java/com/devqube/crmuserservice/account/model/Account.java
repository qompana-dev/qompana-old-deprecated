package com.devqube.crmuserservice.account.model;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import com.devqube.crmuserservice.permissions.role.Role;
import com.devqube.crmuserservice.team.Team;
import com.devqube.crmuserservice.token.model.Token;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@AuthFilterEnabled
public class Account {
    @JsonProperty("id")
    @Id
    @Access(AccessType.PROPERTY)
    @SequenceGenerator(name="account_seq", sequenceName="account_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="account_seq")
    @AuthFields(list = {
            @AuthField(frontendId = "PersonListComponent.idColumn", controller = "getAccounts"),
            @AuthField(frontendId = "PersonDetailComponent.idField", controller = "getAccount"),
            @AuthField(frontendId = "PersonInfoComponent.idLabel", controller = "getMyAccount")
    })
    private Long id;

    @JsonProperty("person")
    @OneToOne(cascade = CascadeType.ALL)
    @Valid
    @JoinColumn(
            name = "person_id",
            referencedColumnName = "id",
            unique = true)
    @AuthFields(list = {
            @AuthField(controller = "getAccounts"),
            @AuthField(controller = "updateAccount"),
            @AuthField(controller = "createAccount"),
            @AuthField(controller = "getMyAccount"),
            @AuthField(controller = "getAccount")
    })
    private Person person;

    @JsonProperty("isActivated")
    @AuthFields(list = {
            @AuthField(frontendId = "PersonDetailComponent.changeActivated", controller = "getAccount"),
            @AuthField(frontendId = "PersonListComponent.activatedColumn", controller = "getAccounts"),
            @AuthField(frontendId = "PersonInfoComponent.isActivatedLabel", controller = "getMyAccount")
    })
    private Boolean activated = false;

    @JsonProperty("role")
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "role_id")
    @AuthFields(list = {
            @AuthField(frontendId = "PersonAddComponent.roleField", controller = "createAccount"),
            @AuthField(frontendId = "PersonDetailComponent.roleField", controller = "getAccount"),
            @AuthField(frontendId = "PersonInfoComponent.roleLabel", controller = "getMyAccount"),
            @AuthField(frontendId = "PersonDetailComponent.roleField", controller = "updateAccount"),
            @AuthField(frontendId = "PersonListComponent.role.nameColumn", controller = "getAccounts")
    })
    @NotNull
    private Role role;

    @JsonProperty("version")
    @Column(name = "version")
    @AuthFields(list = {
            @AuthField(controller = "updateAccount"),
            @AuthField(controller = "getAccount")
    })
    private Long version = 0L;

    @Column(name = "deleted")
    private Boolean deleted = false;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account")
    private Set<Token> tokens;

    @ManyToMany
    @JoinTable(
            name = "account2team",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "team_id")
    )
    Set<Team> teams;

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", person=" + person +
                ", activated=" + activated +
                ", version=" + version +
                ", deleted=" + deleted +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id.equals(account.id) &&
                Objects.equals(person, account.person);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, person);
    }
}

