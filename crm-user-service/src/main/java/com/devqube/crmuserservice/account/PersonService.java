package com.devqube.crmuserservice.account;

import com.devqube.crmuserservice.account.model.Person;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.devqube.crmuserservice.account.PersonSpecifications.findByEmail;

@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository repository;

    public Person findPersonByEmail(String email) {
        return repository.findOne(findByEmail(email))
                .orElseThrow(() -> new IllegalArgumentException(String.format("Person with email: %s is not found.", email)));
    }

}
