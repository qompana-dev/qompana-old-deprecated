package com.devqube.crmuserservice.account.web.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AccountTaskDtoOut {

    private Long id;
    private final String type = "ACCOUNT";
    private Long ownerAccountId;
    private String name;
    private String email;
    private String avatar;
    private String phone;

}
