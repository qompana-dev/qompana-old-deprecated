package com.devqube.crmuserservice.account;

import com.devqube.crmuserservice.account.model.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountsRepository extends JpaRepository<Account, Long> {

    @Query("select a from Account a left join a.person p where LOWER(p.email) = :email")
    Account findByPersonEmail(@Param("email") String email);

    Long countByPersonEmail(String email);

    Page<Account> findAllByDeletedFalse(Pageable pageable);

    List<Account> findAllByDeletedFalseOrderById();

    @Query("SELECT c FROM Account c WHERE (LOWER(c.person.name) LIKE LOWER(concat('%', ?1, '%')) OR LOWER(c.person.surname) LIKE LOWER(concat('%', ?1, '%'))) AND (c.deleted is null or c.deleted = false)")
    List<Account> findByNameContaining(String pattern);

    @Query("SELECT c FROM Account c WHERE (LOWER(c.person.email) LIKE LOWER(concat('%', ?1, '%'))) AND (c.deleted is null or c.deleted = false)")
    List<Account> findByEmailContaining(String pattern);

    List<Account> findByIdIn(List<Long>ids);


    List<Account> findAllByDeletedFalseAndRoleNullOrderById();

    Long countByDeletedFalseAndActivatedTrue();

    List<Account> findAllByRoleId(Long id);
}
