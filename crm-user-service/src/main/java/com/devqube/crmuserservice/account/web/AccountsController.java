package com.devqube.crmuserservice.account.web;

import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.*;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.search.EmailSearch;
import com.devqube.crmshared.user.dto.AccountEmail;
import com.devqube.crmuserservice.account.AccountsService;
import com.devqube.crmuserservice.account.PersonSpecifications;
import com.devqube.crmuserservice.account.exception.TooManyUserException;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.account.web.dto.*;
import com.devqube.crmuserservice.accountConfiguration.AccountConfiguration;
import com.devqube.crmuserservice.accountConfiguration.AccountConfigurationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.OperationNotSupportedException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.OK;

@RestController
@Slf4j
public class AccountsController implements AccountsApi {

    private final AccountsService accountsService;
    private final AccountConfigurationService accountConfigurationService;

    public AccountsController(AccountsService accountsService, AccountConfigurationService accountConfigurationService) {
        this.accountsService = accountsService;
        this.accountConfigurationService = accountConfigurationService;
    }

    @Override
    public ResponseEntity<Void> changeAvatar(@Valid PersonAvatarDto personAvatarDto) throws PermissionDeniedException, EntityNotFoundException {
        accountsService.changePersonAvatar(personAvatarDto);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<PersonAvatarDto> getMyAvatar() throws EntityNotFoundException {
        return ResponseEntity.ok(accountsService.getMyAvatar());
    }

    @Override
    @AuthController(name = "createAccount", actionFrontendId = "PersonAddComponent.save")
    public ResponseEntity<Account> createAccount(@Valid Account account) {
        try {
            return new ResponseEntity<>(accountsService.save(account), HttpStatus.CREATED);
        } catch (EntityNotFoundException | BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (KafkaSendMessageException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (UserEmailOccupiedException e) {
            log.info(e.getMessage(), e);
            return ResponseEntity.status(481).build();
        }
    }

    @Override
    @AuthController(name = "getAccounts")
    public ResponseEntity<Page> getAccounts(@Valid Pageable pageable) {
        return new ResponseEntity<>(accountsService.getAll(pageable), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getAccountForSearch(@NotNull @Valid String term) {
        return new ResponseEntity<>(accountsService.findAllContaining(term)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.user,
                        String.format("%s %s",
                                Strings.nullToEmpty(e.getPerson().getName()),
                                Strings.nullToEmpty(e.getPerson().getSurname())), null))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Page<AccountTaskDtoOut>> getAccountPage(@NotNull @Valid String term,
                                                                  @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable) {
        return new ResponseEntity<>(AccountDtoMapper.toAccountTaskDtoOut(
                accountsService.findAll(PersonSpecifications.findByNameOrSurnameOrEmail(term), pageable)),
                OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getAccountAsCrmObject(List<Long> ids) {
        return new ResponseEntity<>(accountsService.findAllByIds(ids)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.user,
                        String.format("%s %s",
                                Strings.nullToEmpty(e.getPerson().getName()),
                                Strings.nullToEmpty(e.getPerson().getSurname())), null))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<AccountEmail>> getAccountEmails(@Valid Boolean onlyWithoutRole) {
        return new ResponseEntity<>(accountsService.getAll(onlyWithoutRole)
                .stream()
                .map(this::mapToAccountEmail)
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<AccountEmail> getAccountEmailByEmail(String email) {
        try {
            return new ResponseEntity<>(mapToAccountEmail(accountsService.getAccountByEmail(email)), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "updateAccount", actionFrontendId = "PersonDetailComponent.submitForm")
    public ResponseEntity<Void> updateAccount(Long accountId, @Valid Account account) {
        try {
            accountsService.modifyAccount(accountId, account);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (DifferentVersionException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (OperationNotSupportedException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.LOCKED);
        } catch (BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @AuthController(name = "getAccount")
    public ResponseEntity<Account> getAccount(Long accountId) {
        try {
            return new ResponseEntity<>(accountsService.getAccount(accountId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<AccountDTO> getAccountByEmail(String email) {
        try {
            return new ResponseEntity<>(accountsService.getAccountDtoByEmail(email), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override // todo: add permission
    public ResponseEntity<List<AccountInfoDto>> getAccountInfoChildListForLoggedUserRole() {
        try {
            return new ResponseEntity<>(accountsService.getAccountInfoChildListForLoggedUserRole(), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<List<AccountInfoDto>> getAccountsInfoByIds(List<Long> ids) {
        return new ResponseEntity<>(accountsService.getAccountsInfoByIds(ids), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Account> getMyAccount() {
        try {
            return new ResponseEntity<>(accountsService.getMyAccount(), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Long> getMyAccountId() {
        try {
            return new ResponseEntity<>(accountsService.getMyAccountId(), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Long> getAccountIdByEmail(@Valid @NotNull String email) {
        try {
            return new ResponseEntity<>(accountsService.getAccountByEmail(email).getId(), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "removeAccount", actionFrontendId = "PersonDetailComponent.deleteUser")
    public ResponseEntity<Void> removeAccount(Long accountId) {
        try {
            this.accountsService.removeAccount(accountId);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @AuthController(name = "resendActivationLink", actionFrontendId = "PersonDetailComponent.sendActivationLink")
    public ResponseEntity<Void> resendActivationLink(Long accountId) {
        try {
            accountsService.resendActivationEmail(accountId);
        } catch (KafkaSendMessageException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> sendResetPasswordLink(@NotNull @Valid String email) {
        try {
            this.accountsService.sendResetPasswordLink(email);
        } catch (KafkaSendMessageException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @AuthController(actionFrontendId = "PersonInfoComponent.changePasswordButton")
    public ResponseEntity<Void> changePassword(Long accountId, @Valid Passwords passwords) {
        try {
            accountsService.changePassword(accountId, passwords);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PasswordPolicyNotFulfilledException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (BadOldPasswordException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @Override
    public ResponseEntity<Void> setPassword(String token, @Valid SetPasswordRequest setPasswordRequest) {
        try {
            accountsService.setAccountPassword(token, setPasswordRequest);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (BadRequestException | PasswordPolicyNotFulfilledException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(actionFrontendId = "PersonDetailComponent.changeActivated")
    public ResponseEntity<Void> changeActivate(Long accountId, Boolean activated) {
        try {
            accountsService.changeActivate(accountId, activated);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (TooManyUserException e) {
            log.info(e.getMessage(), e);
            return ResponseEntity.status(480).build();
        }
    }

    @Override
    public ResponseEntity<Void> modifyAccountConfiguration(Long accountId, @Valid AccountConfiguration accountConfiguration) {
        try {
            this.accountConfigurationService.modify(accountId, accountConfiguration);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<Void> modifyAccountMenuOrder(Long accountId, @Valid List<MenuOrderDto> menuOrder) {
        try {
            this.accountConfigurationService.modifyMenuOrder(accountId, menuOrder);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> modifyAccountTheme(Long accountId, @Valid ThemeDtoIn themeDto) {
        try {
            this.accountConfigurationService.modifyTheme(accountId, themeDto.getTheme());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<AccountConfiguration> getAccountConfiguration(Long accountId) {
        try {
            return new ResponseEntity<>(this.accountConfigurationService.getByAccountId(accountId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<String> getAccountEmail(Long accountId) {
        try {
            return new ResponseEntity<>(accountsService.getAccount(accountId).getPerson().getEmail(), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @GetMapping(value = "/accounts/search-emails")
    public List<EmailSearch> searchEmails(
            @RequestHeader(value = "Logged-Account-Email") String email,
            @RequestParam(value = "term") String term) {
        return accountsService.findByEmailContaining(term, email);
    }

    @Override
    public ResponseEntity<String> getRoleForUserWithEmail(String email) {
        try {
            return new ResponseEntity<>(accountsService.getRoleNameByAccountEmail(email), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    private AccountEmail mapToAccountEmail(Account account) {
        AccountEmail email = new AccountEmail();
        email.setId(account.getId().intValue());
        email.setEmail(account.getPerson().getEmail());
        email.setName(account.getPerson().getName());
        email.setSurname(account.getPerson().getSurname());
        return email;
    }

    @Override
    public ResponseEntity<Boolean> getLicenceRefresh() {
        try {
            return new ResponseEntity<>(accountsService.refreshLicence(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Long> getAndRefreshLicenceExpiredDate() {
        try {
            return new ResponseEntity<>(accountsService.getAndRefreshLicenceExpiredDate(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Long> getAndRefreshLicenceNumberOfUsers() {
        try {
            return new ResponseEntity<>(accountsService.getAndRefreshLicenceNumberOfUsers(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
