package com.devqube.crmuserservice.accountConfiguration;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountConfigurationRepository extends JpaRepository<AccountConfiguration, Long> {
    AccountConfiguration getByAccountId(Long accountId);
}
