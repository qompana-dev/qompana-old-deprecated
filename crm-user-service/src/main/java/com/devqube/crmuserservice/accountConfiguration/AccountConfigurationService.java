package com.devqube.crmuserservice.accountConfiguration;

import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmuserservice.account.AccountsRepository;
import com.devqube.crmuserservice.account.ThemeEnum;
import com.devqube.crmuserservice.account.web.dto.MenuOrderDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountConfigurationService {

    private final AccountConfigurationRepository repository;
    private final AccountsRepository accountsRepository;

    public AccountConfigurationService(AccountConfigurationRepository repository, AccountsRepository accountsRepository) {
        this.repository = repository;
        this.accountsRepository = accountsRepository;
    }

    public void save(AccountConfiguration accountConfiguration) throws BadRequestException {
        AccountConfiguration byAccountId = this.repository.getByAccountId(accountConfiguration.getAccount().getId());
        if (byAccountId != null) {
            throw new BadRequestException("configuration for this account is already present");
        }
        this.repository.save(accountConfiguration);
    }

    public void modify(Long accountId, AccountConfiguration accountConfiguration) throws EntityNotFoundException {
        AccountConfiguration fromDb = this.repository.getByAccountId(accountId);
        if (fromDb == null) {
            throw new EntityNotFoundException("configuration for this account does not exist");
        }
        copyValuesFromDtoToObjectFromDb(accountConfiguration, fromDb);
        this.repository.save(fromDb);
    }

    public void modifyMenuOrder(Long accountId, List<MenuOrderDto> menuOrder) throws EntityNotFoundException, JsonProcessingException {
        AccountConfiguration fromDb = this.repository.getByAccountId(accountId);
        if (fromDb == null) {
            throw new EntityNotFoundException("configuration for this account does not exist");
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String menuOrderJson = objectMapper.writeValueAsString(menuOrder);
        fromDb.setMenuOrder(menuOrderJson);
        this.repository.save(fromDb);
    }

    public void modifyTheme(Long accountId, String theme) throws EntityNotFoundException, JsonProcessingException {
        AccountConfiguration fromDb = this.repository.getByAccountId(accountId);
        ThemeEnum.valueOf(theme.toUpperCase());
        if (fromDb == null) {
            throw new EntityNotFoundException("configuration for this account does not exist");
        }

        fromDb.setTheme(theme.toUpperCase());
        this.repository.save(fromDb);
    }

    private void copyValuesFromDtoToObjectFromDb(AccountConfiguration accountConfiguration, AccountConfiguration fromDb) {
        fromDb.setLocale(accountConfiguration.getLocale());
        fromDb.setDateFormat(accountConfiguration.getDateFormat());
        fromDb.setFirstDayOfWeek(accountConfiguration.getFirstDayOfWeek());
        fromDb.setFirstHourOfDay(accountConfiguration.getFirstHourOfDay());
        fromDb.setHourFormat(accountConfiguration.getHourFormat());
    }

    public AccountConfiguration getByAccountId(Long accountId) throws EntityNotFoundException {
        AccountConfiguration byAccountId = this.repository.getByAccountId(accountId);
        if (byAccountId == null) {
            throw new EntityNotFoundException("Account configuration by account id not found");
        }
        return byAccountId;
    }
}
