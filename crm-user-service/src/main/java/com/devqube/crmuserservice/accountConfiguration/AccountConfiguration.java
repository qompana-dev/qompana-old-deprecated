package com.devqube.crmuserservice.accountConfiguration;

import com.devqube.crmuserservice.account.model.Account;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountConfiguration {

    @Id
    @SequenceGenerator(name = "account_configuration_seq", sequenceName = "account_configuration_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_configuration_seq")
    private Long id;

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Account account;

    private String locale;

    private String dateFormat;

    private String firstDayOfWeek;

    private String firstHourOfDay;

    private String hourFormat;

    private String menuOrder;

    private String theme;

    public AccountConfiguration(Account account, String locale, String dateFormat, String firstDayOfWeek,
                                String firstHourOfDay, String hourFormat, String menuOrder, String theme) {
        this.account = account;
        this.locale = locale;
        this.dateFormat = dateFormat;
        this.firstDayOfWeek = firstDayOfWeek;
        this.firstHourOfDay = firstHourOfDay;
        this.hourFormat = hourFormat;
        this.menuOrder = menuOrder;
        this.theme = theme;
    }
}
