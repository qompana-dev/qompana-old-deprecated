package com.devqube.crmuserservice.kafka;

import com.devqube.crmshared.association.RemoveCrmObject;
import com.devqube.crmshared.kafka.KafkaService;
import org.springframework.stereotype.Component;

@Component
public class ObjectRemoveComponentImpl extends RemoveCrmObject.RemoveComponent {
    public ObjectRemoveComponentImpl(KafkaService kafkaService) {
        super(kafkaService);
    }
}
