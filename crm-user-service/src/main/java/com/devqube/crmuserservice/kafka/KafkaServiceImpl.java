package com.devqube.crmuserservice.kafka;

import com.devqube.crmshared.kafka.AbstractKafkaService;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import org.springframework.stereotype.Service;

@Service
public class KafkaServiceImpl extends AbstractKafkaService {
    public void processMessage(String content) {
        super.processMessage(content);
    }

    @Override
    public void receiverNotFound(KafkaMessage kafkaMessage) {

    }
}
