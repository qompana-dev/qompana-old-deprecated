package com.devqube.crmuserservice.globalConfiguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GlobalConfiguration {
    @Id
    @SequenceGenerator(name="global_configuration_seq", sequenceName="global_configuration_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="global_configuration_seq")
    private Long id;
    private String key;
    private String value;
}
