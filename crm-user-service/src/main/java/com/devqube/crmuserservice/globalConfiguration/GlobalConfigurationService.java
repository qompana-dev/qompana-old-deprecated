package com.devqube.crmuserservice.globalConfiguration;

import com.devqube.crmshared.currency.dto.CurrencyDto;
import com.devqube.crmshared.currency.dto.RateOfMainCurrencyDto;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmuserservice.globalConfiguration.integrations.BusinessClient;
import feign.FeignException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;

@Service
public class GlobalConfigurationService {

    private final GlobalConfigurationRepository repository;
    private final BusinessClient businessClient;

    public GlobalConfigurationService(GlobalConfigurationRepository repository, BusinessClient businessClient) {
        this.repository = repository;
        this.businessClient = businessClient;
    }

    public boolean isPasswordPolicyActive() {
        GlobalConfiguration passwordPolicyActive = repository.findByKey("passwordPolicyActive");
        return passwordPolicyActive != null ? Boolean.valueOf(passwordPolicyActive.getValue()) : false;
    }

    public String getPasswordPolicyRegex() {
        GlobalConfiguration passwordPolicyRegex = repository.findByKey("passwordPolicyRegex");
        return passwordPolicyRegex != null ? passwordPolicyRegex.getValue() : null;
    }

    GlobalConfiguration findByKey(String key) throws EntityNotFoundException {
        GlobalConfiguration byKey = repository.findByKey(key);
        if (byKey == null) {
            throw new EntityNotFoundException("Global param with key " + key + " not found");
        }
        return byKey;
    }

    String getPasswordPolicyDescription() {
        GlobalConfiguration passwordPolicyDescription = repository.findByKey("passwordPolicyDescription");
        return passwordPolicyDescription != null ? passwordPolicyDescription.getValue() : null;
    }

    @Transactional(rollbackFor = FeignException.class)
    public GlobalConfiguration save(String key, String value) {
        if ("systemCurrency".equals(key)) {
            this.businessClient.updateDefaultPriceBooksCurrency(value);
        }
        GlobalConfiguration byKey = repository.findByKey(key);
        GlobalConfiguration toSave = byKey != null ? byKey : new GlobalConfiguration();
        toSave.setKey(key);
        toSave.setValue(value);
        return repository.save(toSave);
    }

    public CurrencyDto getCurrenciesWithRateOfMainCurrency() throws EntityNotFoundException {
        CurrencyDto result = new CurrencyDto();
        result.setAllCurrencies(Arrays.asList(findByKey("currencies").getValue().split(";")));
        result.setSystemCurrency(findByKey("systemCurrency").getValue());

        result.setRatesOfMainCurrency(new ArrayList<>());
        for (String currency : result.getAllCurrencies()) {
            double value = Double.parseDouble(findByKey("rateOfMainCurrencyTo" + currency).getValue());
            result.getRatesOfMainCurrency().add(new RateOfMainCurrencyDto(currency, value));
        }

        return result;
    }


}
