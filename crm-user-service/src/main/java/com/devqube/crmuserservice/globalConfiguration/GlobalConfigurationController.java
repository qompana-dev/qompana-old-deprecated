package com.devqube.crmuserservice.globalConfiguration;

import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.currency.dto.CurrencyDto;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmuserservice.globalConfiguration.dto.GlobalConfigurationDto;
import com.devqube.crmuserservice.globalConfiguration.dto.PasswordPolicyResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class GlobalConfigurationController implements ConfigurationApi {
    private final GlobalConfigurationService configurationService;

    public GlobalConfigurationController(GlobalConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Override
    public ResponseEntity<PasswordPolicyResponse> getPasswordPolicyRegex() {
        if (configurationService.isPasswordPolicyActive()) {
            return new ResponseEntity<>(new PasswordPolicyResponse()
                    .regex(configurationService.getPasswordPolicyRegex())
                    .description(configurationService.getPasswordPolicyDescription()), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


    @Override
    public ResponseEntity<GlobalConfiguration> getGlobalParamByKey(String key) {
        try {
            return new ResponseEntity<>(configurationService.findByKey(key), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @AuthController(actionFrontendId = "NavigationComponent.settingItem.administration")
    @Override
    public ResponseEntity<GlobalConfiguration> setGlobalParam(String key, String value) {
        return new ResponseEntity<>(configurationService.save(key, value), HttpStatus.OK);
    }

    @AuthController(actionFrontendId = "NavigationComponent.settingItem.administration")
    @Override
    public ResponseEntity<GlobalConfiguration> setGlobalParam(GlobalConfigurationDto globalConfiguration) {
        return new ResponseEntity<>(configurationService.save(globalConfiguration.getKey(), globalConfiguration.getValue()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CurrencyDto> getCurrencies() {
        try {
            return new ResponseEntity<>(configurationService.getCurrenciesWithRateOfMainCurrency(), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
