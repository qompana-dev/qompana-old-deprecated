package com.devqube.crmuserservice.globalConfiguration.integrations;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Set;

@FeignClient(name = "crm-business-service", url = "${crm-business-service.url}")
public interface BusinessClient {
    @RequestMapping(value = "/internal/price-books/default/currency/{currency}", method = RequestMethod.PUT)
    void updateDefaultPriceBooksCurrency(@PathVariable("currency") String currency);

    @DeleteMapping("/widget/report/internal/{widgetIds}")
    void deleteReportWidgets(@PathVariable Set<Long> widgetIds);

    @DeleteMapping("/internal/widget-goals/{widgetIds}")
    void deleteGoalWidgets(@PathVariable Set<Long> widgetIds);
}
