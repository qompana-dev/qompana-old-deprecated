
package com.devqube.crmuserservice.globalConfiguration;

import com.devqube.crmshared.currency.dto.CurrencyDto;
import com.devqube.crmuserservice.ApiUtil;
import com.devqube.crmuserservice.globalConfiguration.dto.GlobalConfigurationDto;
import com.devqube.crmuserservice.globalConfiguration.dto.PasswordPolicyResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-10-24T12:47:04.725339+02:00[Europe/Warsaw]")

@Validated
@Api(value = "configuration", description = "the configuration API")
public interface ConfigurationApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Retrieve password policy regex", nickname = "getPasswordPolicyRegex", notes = "Method used to retrieve password policy regex", response = PasswordPolicyResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "retrieved regex", response = PasswordPolicyResponse.class),
            @ApiResponse(code = 204, message = "password policy is turned off", response = String.class)})
    @RequestMapping(value = "/configuration/password-policy",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<PasswordPolicyResponse> getPasswordPolicyRegex() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"regex\" : \"regex\",  \"description\" : \"description\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Retrieve global param", nickname = "getGlobalParam", notes = "Method used to retrieve global param by key", response = GlobalConfiguration.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "retrieved global param", response = GlobalConfiguration.class),
            @ApiResponse(code = 404, message = "global param not found")})
    @RequestMapping(value = "/configuration/{key}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<GlobalConfiguration> getGlobalParamByKey(@PathVariable(value = "key") String key) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Set global param", nickname = "setGlobalParam", notes = "Method used to set global param", response = GlobalConfiguration.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "saved global param", response = GlobalConfiguration.class)})
    @RequestMapping(value = "/configuration/{key}/{value}",
            produces = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<GlobalConfiguration> setGlobalParam(@PathVariable(value = "key") String key, @PathVariable(value = "value") String value) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Set global param", nickname = "setGlobalParam", notes = "Method used to set global param", response = GlobalConfiguration.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "saved global param", response = GlobalConfiguration.class)})
    @RequestMapping(value = "/configuration",
            produces = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<GlobalConfiguration> setGlobalParam(@RequestBody GlobalConfigurationDto globalConfiguration) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get currencies", nickname = "getCurrencies", notes = "Method used to get currencies", response = CurrencyDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "retrieved currencies", response = CurrencyDto.class),
            @ApiResponse(code = 404, message = "global param not found")})
    @RequestMapping(value = "/internal/configuration/currencies",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<CurrencyDto> getCurrencies() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
