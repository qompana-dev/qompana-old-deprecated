package com.devqube.crmuserservice.globalConfiguration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * PasswordPolicyResponse
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-10-24T12:47:04.725339+02:00[Europe/Warsaw]")

public class PasswordPolicyResponse {
    @JsonProperty("regex")
    private String regex;

    @JsonProperty("description")
    private String description;

    public PasswordPolicyResponse regex(String regex) {
        this.regex = regex;
        return this;
    }

    /**
     * Get regex
     *
     * @return regex
     */
    @ApiModelProperty(value = "")


    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public PasswordPolicyResponse description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Get description
     *
     * @return description
     */
    @ApiModelProperty(value = "")


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PasswordPolicyResponse passwordPolicyResponse = (PasswordPolicyResponse) o;
        return Objects.equals(this.regex, passwordPolicyResponse.regex) &&
                Objects.equals(this.description, passwordPolicyResponse.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(regex, description);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class PasswordPolicyResponse {\n");

        sb.append("    regex: ").append(toIndentedString(regex)).append("\n");
        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

