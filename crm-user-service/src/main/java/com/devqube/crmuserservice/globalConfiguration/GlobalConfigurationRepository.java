package com.devqube.crmuserservice.globalConfiguration;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GlobalConfigurationRepository extends JpaRepository<GlobalConfiguration, Long> {
    GlobalConfiguration findByKey(String key);
}
