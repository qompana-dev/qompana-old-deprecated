package com.devqube.crmuserservice.token.model;

import com.devqube.crmuserservice.account.model.Account;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Token {
    @JsonProperty("id")
    @Id
    @SequenceGenerator(name="token_seq", sequenceName="token_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="token_seq")
    private Long id;

    @JsonProperty("type")
    @Enumerated(EnumType.ORDINAL)
    private TokenTypeEnum type;

    @JsonProperty("account")
    @ManyToOne
    @JoinColumn(
            name = "account_id",
            referencedColumnName = "id")
    private Account account;

    @JsonProperty("type")
    private String token;

    @JsonProperty("expired")
    private LocalDateTime expired;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token = (Token) o;
        return type == token.type &&
                account.equals(token.account);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, account);
    }
}
