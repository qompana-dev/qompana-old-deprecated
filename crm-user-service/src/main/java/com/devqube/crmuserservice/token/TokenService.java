package com.devqube.crmuserservice.token;

import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.kafka.util.KafkaMessageDataBuilder;
import com.devqube.crmuserservice.account.AccountsService;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.token.model.Token;
import com.devqube.crmuserservice.token.model.TokenTypeEnum;
import com.devqube.crmuserservice.token.web.dto.TokenResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;

@Slf4j
@Service
@EnableScheduling
public class TokenService {
    private final TokenRepository tokenRepository;
    private final AccountsService accountsService;
    private final KafkaService kafkaService;

    public TokenService(TokenRepository tokenRepository, @Lazy AccountsService accountsService, KafkaService kafkaService) {
        this.tokenRepository = tokenRepository;
        this.accountsService = accountsService;
        this.kafkaService = kafkaService;
    }

    public void sendEmailWithToken(Account account, TokenTypeEnum tokenTypeEnum) throws KafkaSendMessageException {
        KafkaMsgType msgType = getMsgType(tokenTypeEnum);
        if (msgType == null) {
            log.info("unsupported token type");
            return;
        }

        Token token = TokenUtil.generateNewAccountToken(account, tokenTypeEnum);
        addToken(token);

        long accountId = account.getId();
        try {
            accountId = accountsService.getMyAccountId();
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
        }

        kafkaService.send(accountId, msgType, KafkaMessageDataBuilder.builder()
                .add("url", "token/" + token.getToken())
                .add("email", account.getPerson().getEmail()).build());
    }

    @Transactional
    public void addToken(Token token) {
        Token oldToken = tokenRepository.findOneByTypeAndAccount(token.getType(), token.getAccount());
        if (oldToken != null) {
            tokenRepository.deleteById(oldToken.getId());
        }
        tokenRepository.save(token);
    }

    public Token getValidTokenObject(String token, TokenTypeEnum... availableTypes) throws BadRequestException, EntityNotFoundException {
        Token result = getValidTokenObject(token);
        if (availableTypes == null || Arrays.stream(availableTypes).noneMatch(c -> c.equals(result.getType()))) {
            throw new BadRequestException();
        }
        return result;
    }

    public TokenResult retrieveToken(String token) throws EntityNotFoundException, BadRequestException {
        Token oneByToken = tokenRepository.findOneByToken(token);
        if (oneByToken == null) {
            throw new EntityNotFoundException();
        }
        if (TokenUtil.tokenExpired(token, oneByToken.getType())) {
            throw new BadRequestException();
        }
        return TokenResult.builder().token(token).type(oneByToken.getType()).build();
    }

    public void removeToken(Long tokenId) {
        tokenRepository.deleteById(tokenId);
    }

    public void removeTokensByAccountId(Long accountId) {
        this.tokenRepository.deleteByAccountId(accountId);
    }

    @Transactional
    @Scheduled(cron = "0 30 2 * * ?")
    public void deleteOldTokens() {
        tokenRepository.deleteAllByExpiredBefore(LocalDateTime.now());
    }

    private Token getValidTokenObject(String token) throws EntityNotFoundException, BadRequestException {
        Token tokenObj = this.tokenRepository.findOneByToken(token);
        if (tokenObj == null) {
            throw new EntityNotFoundException();
        }
        if (TokenUtil.tokenExpired(tokenObj.getToken(), tokenObj.getType())) {
            throw new BadRequestException();
        }
        return tokenObj;
    }

    private KafkaMsgType getMsgType(TokenTypeEnum tokenTypeEnum) {
        switch (tokenTypeEnum) {
            case RESET_PASSWORD:
                return KafkaMsgType.NOTIFICATION_SEND_RESET_PASSWORD_MAIL;
            case SEND_ACTIVATION_CODE:
                return KafkaMsgType.NOTIFICATION_SEND_ACTIVATION_MAIL;
            case RESEND_ACTIVATION_CODE:
                return KafkaMsgType.NOTIFICATION_RESENT_ACTIVATION_MAIL;
        }
        return null;
    }
}
