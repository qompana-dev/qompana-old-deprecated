package com.devqube.crmuserservice.token.web.dto;

import com.devqube.crmuserservice.token.model.TokenTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TokenResult {
    private TokenTypeEnum type;
    private String token;
}
