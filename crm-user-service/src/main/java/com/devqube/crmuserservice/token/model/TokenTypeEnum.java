package com.devqube.crmuserservice.token.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

@Getter
@AllArgsConstructor
public enum TokenTypeEnum {
    RESET_PASSWORD(1, ChronoUnit.DAYS),
    SEND_ACTIVATION_CODE(1, ChronoUnit.DAYS),
    RESEND_ACTIVATION_CODE(1, ChronoUnit.DAYS);

    private long expiredAfterValue;
    private TemporalUnit expiredAfterUnit;
}
