package com.devqube.crmuserservice.token;

import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.token.model.Token;
import com.devqube.crmuserservice.token.model.TokenTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {
    Token findOneByToken(String token);

    Token findOneByTypeAndAccount(TokenTypeEnum type, Account account);

    void deleteAllByExpiredBefore(LocalDateTime now);

    void deleteByAccountId(Long accountId);
}
