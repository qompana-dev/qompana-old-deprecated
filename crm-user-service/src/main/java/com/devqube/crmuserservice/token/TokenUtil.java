package com.devqube.crmuserservice.token;

import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.token.model.Token;
import com.devqube.crmuserservice.token.model.TokenTypeEnum;
import com.fasterxml.uuid.Generators;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;
import java.util.UUID;

public class TokenUtil {

    public static Token generateNewAccountToken(Account account, TokenTypeEnum type) {
        String uuid = generateTimeBasedUUID();
        return Token.builder()
                .account(account)
                .expired(getTimeFromUUID(uuid).plus(type.getExpiredAfterValue(), type.getExpiredAfterUnit()))
                .token(uuid)
                .type(type).build();
    }

    public static String generateTimeBasedUUID() {
        return Generators.timeBasedGenerator().generate().toString();
    }

    static LocalDateTime getTimeFromUUID(String uuid) {
        if (uuid == null) {
            return null;
        }
        UUID tuid = UUID.fromString(uuid);
        final long START_OF_UUID_RELATIVE_TO_UNIX_EPOCH_MILLIS = -12219292800L * 1000L;
        Instant instant = Instant.ofEpochMilli((tuid.timestamp() / 10000L) + START_OF_UUID_RELATIVE_TO_UNIX_EPOCH_MILLIS);
        return LocalDateTime.ofInstant(instant, TimeZone.getDefault().toZoneId());
    }

    static boolean tokenExpired(String uuid, TokenTypeEnum type) {
        return tokenExpired(uuid, type, LocalDateTime.now());
    }

    static boolean tokenExpired(String uuid, TokenTypeEnum type, LocalDateTime now) {
        if (uuid == null || now == null || type == null) {
            return true;
        }
        LocalDateTime timeFromUUID = getTimeFromUUID(uuid);
        return timeFromUUID.plus(type.getExpiredAfterValue(), type.getExpiredAfterUnit()).isBefore(now);
    }
}
