package com.devqube.crmuserservice;

import com.devqube.crmshared.association.RemoveCrmObject;
import com.devqube.crmshared.exception.IncorrectNotificationTypeException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.history.dto.ObjectHistoryType;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmshared.kafka.dto.KafkaMessageData;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.notification.NotificationMsgType;
import com.devqube.crmshared.search.CrmObjectType;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

public class DefaultKafkaServiceToRemoveCrmObject {

    public void setDefault() {
        RemoveCrmObject.getInstance().setKafkaService(new KafkaService() {
            @Override
            public void send(@NotNull Long aLong, @NotNull KafkaMsgType kafkaMsgType, Set<KafkaMessageData> set) throws KafkaSendMessageException {}
            @Override
            public void send(KafkaMsgType kafkaMsgType, String s) throws KafkaSendMessageException {}
            @Override
            public void addNotification(@NotNull Long aLong, @NotNull NotificationMsgType notificationMsgType, Set<KafkaMessageData> set) throws KafkaSendMessageException {}
            @Override
            public void addGroupNotification(@NotNull Set<Long> set, @NotNull NotificationMsgType notificationMsgType, Set<KafkaMessageData> set1) throws KafkaSendMessageException, IncorrectNotificationTypeException {}
            @Override
            public void sendWebsocket(String s, String s1) throws KafkaSendMessageException {}
            @Override
            public void sendRemoveAssociation(CrmObjectType crmObjectType, Long aLong) throws KafkaSendMessageException {}
            @Override
            public void saveHistory(ObjectHistoryType type, String id, String json, LocalDateTime modifiedTime, String email) throws KafkaSendMessageException {}
        });
    }
}
