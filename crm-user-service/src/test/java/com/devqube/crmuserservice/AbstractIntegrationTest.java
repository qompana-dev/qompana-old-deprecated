package com.devqube.crmuserservice;

import com.devqube.crmuserservice.account.AccountsRepository;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.account.model.Person;
import com.devqube.crmuserservice.accountConfiguration.AccountConfigurationRepository;
import com.devqube.crmuserservice.permissions.role.Role;
import com.devqube.crmuserservice.permissions.role.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import java.util.UUID;

@SpringBootTest(classes = {CrmUserServiceApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
public abstract class AbstractIntegrationTest {
    public TestRestTemplate restTemplate;

    private static final Long LOGGED_USER_ACCOUNT_ID = 1L;
    private static final Long LOGGED_USER_PERSON_ID = 1L;
    private static final Long LOGGED_USER_ROLE_ID = 1L;
    private static final String LOGGED_USER_LOGIN = "jan.kowalski@devqube.com";
    private static final String LOGGED_USER_PASSWORD_ENCODED = "$2a$10$KTAR2efAl6zocmh5KqX.jOdrYOxlbys/RDvFnckvg/cQqUJ4HLlw2";

    public String baseUrl;
    @LocalServerPort
    private int port;

    @Autowired
    private AccountsRepository accountsRepository;
    @Autowired
    private AccountConfigurationRepository accountConfigurationRepository;
    @Autowired
    private RoleRepository roleRepository;

    protected void setUp() {
        this.accountConfigurationRepository.deleteAll();
        this.accountsRepository.deleteAll();
        Role role = Role.builder().id(LOGGED_USER_ROLE_ID).name(UUID.randomUUID().toString()).build();
        Role savedRole = roleRepository.save(role);
        Account account = Account.builder()
            .id(LOGGED_USER_ACCOUNT_ID)
            .activated(true)
            .role(savedRole)
            .person(Person.builder()
                    .id(LOGGED_USER_PERSON_ID)
                    .name("n")
                    .surname("s")
                    .phone("12345123451")
                    .password(LOGGED_USER_PASSWORD_ENCODED)
                    .email(LOGGED_USER_LOGIN)
                    .build())
            .version(1L)
            .deleted(false)
            .build();

        baseUrl = "http://localhost:" + port;
        restTemplate = new TestRestTemplate();
        accountsRepository.save(account);
    }

    protected void cleanUp() {
        this.accountsRepository.deleteAll();
    }
}
