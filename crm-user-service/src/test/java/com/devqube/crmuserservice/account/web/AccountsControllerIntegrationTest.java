package com.devqube.crmuserservice.account.web;

import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.web.RestPageImpl;
import com.devqube.crmuserservice.AbstractIntegrationTest;
import com.devqube.crmuserservice.AccountsUtil;
import com.devqube.crmuserservice.TokenUtil;
import com.devqube.crmuserservice.account.AccountsRepository;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.account.web.dto.PersonListDto;
import com.devqube.crmuserservice.account.web.dto.SetPasswordRequest;
import com.devqube.crmuserservice.accountConfiguration.AccountConfigurationRepository;
import com.devqube.crmuserservice.globalConfiguration.GlobalConfigurationRepository;
import com.devqube.crmuserservice.permissions.role.Role;
import com.devqube.crmuserservice.permissions.role.RoleRepository;
import com.devqube.crmuserservice.token.TokenRepository;
import com.devqube.crmuserservice.token.model.Token;
import com.devqube.crmuserservice.token.model.TokenTypeEnum;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class AccountsControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private AccountsRepository accountsRepository;
    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private AccountConfigurationRepository accountConfigurationRepository;

    private Account account;
    private Token token;

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private GlobalConfigurationRepository globalConfigurationRepository;

    @Before
    public void setUp() {
        super.setUp();
        account = AccountsUtil.createAccount();
        token = TokenUtil.createToken();
        globalConfigurationRepository.deleteAll();
    }

    @After
    public void cleanUp() {
        accountConfigurationRepository.deleteAll();
        accountsRepository.deleteAll();
        tokenRepository.deleteAll();
    }

    @Test
    public void shouldReturnListOfAccounts() {
        accountsRepository.save(account);
        ResponseEntity<RestPageImpl<PersonListDto>> response = restTemplate.exchange(baseUrl + "/accounts", HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<PersonListDto>>() {});
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getContent().get(1));
        assertEquals(response.getBody().getContent().get(1).getEmail(), account.getPerson().getEmail());
    }

    @Test
    public void shouldReturnListOfAccountsContainingInputInNameOrSurname() {
        accountsRepository.save(account);
        ResponseEntity<List<CrmObject>> response = restTemplate.exchange(
                baseUrl + "/accounts/search?term=nam",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<CrmObject>>(){});
        List<CrmObject> result = response.getBody();
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(result);
        assertNotNull(result.get(0));
        assertEquals(result.get(0).getType(), CrmObjectType.user);
        assertEquals(result.get(0).getLabel(), "name surname");
    }

    @Test
    public void shouldReturnEmptyListOfAccountsBecauseNoneContainsInputTerm() {
        accountsRepository.save(account);
        ResponseEntity<List<CrmObject>> response = restTemplate.exchange(
                baseUrl + "/accounts/search?term=uyiuy8768",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<CrmObject>>(){});
        List<CrmObject> result = response.getBody();
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void shouldReturnListOfCrmObjectsById() {
        Account save = accountsRepository.save(account);

        ResponseEntity<List<CrmObject>> response = restTemplate.exchange(
                String.format("%s/accounts/crm-object?ids=%s", baseUrl, save.getId()),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<CrmObject>>(){});
        List<CrmObject> result = response.getBody();
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(result);
        assertNotNull(result.get(0));
        assertEquals(result.get(0).getType(), CrmObjectType.user);
        assertEquals(result.get(0).getLabel(), "name surname");
    }

    @Test
    public void shouldReturnEmptyListOfCrmObjectsBecauseWrongId() {
        accountsRepository.save(account);
        ResponseEntity<List<CrmObject>> response = restTemplate.exchange(
                String.format("%s/accounts/crm-object?ids=%s", baseUrl, 8768768L),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<CrmObject>>(){});
        List<CrmObject> result = response.getBody();
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void shouldCreateUser() {
        account.getPerson().setEmail("newEmail@devqube.com");
        ResponseEntity<Account> response = restTemplate.postForEntity(baseUrl + "/accounts", account, Account.class);
        assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getPerson());
        assertEquals(response.getBody().getPerson().getEmail(), account.getPerson().getEmail());
    }

    @Test
    public void shouldCreateUserWithRoleId() {
        Role newRole = new Role();

        Role roleToSave = new Role();
        roleToSave.setName("Nazwa2");

        Role savedRole = roleRepository.save(roleToSave);
        newRole.setId(savedRole.getId());

        account.getPerson().setEmail("newEmail@devqube.com");
        account.setRole(newRole);


        ResponseEntity<Account> response = restTemplate.postForEntity(baseUrl + "/accounts", account, Account.class);

        assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getRole());
        assertEquals(savedRole.getId(), response.getBody().getRole().getId());
        assertEquals(savedRole.getName(), response.getBody().getRole().getName());
    }

    @Test
    public void shouldThrowBadRequestIfRoleNotFound() {

        Role newRole = new Role();

        Role roleToSave = new Role();
        roleToSave.setName("Nazwa");

        Role savedRole = roleRepository.save(roleToSave);
        newRole.setId(savedRole.getId() + 1L);

        account.getPerson().setEmail("newEmail@devqube.com");
        account.setRole(newRole);


        ResponseEntity<Account> response = restTemplate.postForEntity(baseUrl + "/accounts", account, Account.class);

        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldReturnBadRequestWhenUsernameOccupied() {
        accountsRepository.save(account);
        ResponseEntity<Account> response = restTemplate.postForEntity(baseUrl + "/accounts", account, Account.class);
        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldReturnBadRequestWhenValidationFailed() {
        account.getPerson().setName(null);
        ResponseEntity<Account> response = restTemplate.postForEntity(baseUrl + "/accounts", account, Account.class);
        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldReturnAccount() {
        Long id = accountsRepository.save(account).getId();
        ResponseEntity<Account> response = restTemplate.getForEntity(baseUrl + "/accounts/" + id, Account.class);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(response.getBody());
        assertEquals(response.getBody().getActivated(), account.getActivated());
        assertEquals(response.getBody().getPerson().getEmail(), account.getPerson().getEmail());
    }

    @Test
    public void shouldUpdateAccountPassword() {
        SetPasswordRequest passwordAfterChanged = new SetPasswordRequest().password("passwordAfterChanged");

        account.setActivated(false);
        Account addedAccount = accountsRepository.save(account);
        token.setAccount(addedAccount);
        token.setType(TokenTypeEnum.RESET_PASSWORD);
        tokenRepository.save(token);

        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/accounts/password/" + token.getToken(), passwordAfterChanged, Void.class);

        assertEquals(response.getStatusCode(), HttpStatus.OK);

        Account byId = accountsRepository.getOne(addedAccount.getId());
        assertEquals(byId.getActivated(), false);
        assertTrue(passwordEncoder.matches(passwordAfterChanged.getPassword(), byId.getPerson().getPassword()));
        Token oneByToken = tokenRepository.findOneByToken(token.getToken());
        assertNull(oneByToken);
    }

    @Test
    public void shouldUpdateAndActivateAccountPassword() {
        cleanUp();
        SetPasswordRequest passwordAfterChanged = new SetPasswordRequest().password("passwordAfterChanged");

        // for send
        account.getPerson().setPassword("test");
        account.setActivated(false);
        Account addedAccount = accountsRepository.save(account);
        token.setAccount(addedAccount);
        token.setType(TokenTypeEnum.SEND_ACTIVATION_CODE);
        tokenRepository.save(token);

        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/accounts/password/" + token.getToken(), passwordAfterChanged, Void.class);

        assertEquals(response.getStatusCode(), HttpStatus.OK);

        Account byId = accountsRepository.getOne(addedAccount.getId());
        assertEquals(byId.getActivated(), true);
        assertTrue(passwordEncoder.matches(passwordAfterChanged.getPassword(), byId.getPerson().getPassword()));
        Token oneByToken = tokenRepository.findOneByToken(token.getToken());
        assertNull(oneByToken);


        // for resend
        byId.setActivated(false);
        byId.getPerson().setPassword("test");
        addedAccount = accountsRepository.save(byId);
        token.setAccount(addedAccount);
        token.setType(TokenTypeEnum.RESEND_ACTIVATION_CODE);
        tokenRepository.save(token);

        response = restTemplate.postForEntity(baseUrl + "/accounts/password/" + token.getToken(), passwordAfterChanged, Void.class);

        assertEquals(response.getStatusCode(), HttpStatus.OK);

        byId = accountsRepository.getOne(addedAccount.getId());
        assertEquals(byId.getActivated(), true);
        assertTrue(passwordEncoder.matches(passwordAfterChanged.getPassword(), byId.getPerson().getPassword()));
        oneByToken = tokenRepository.findOneByToken(token.getToken());
        assertNull(oneByToken);
    }
}
