package com.devqube.crmuserservice.account;

import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.exception.*;
import com.devqube.crmshared.license.LicenseCheck;
import com.devqube.crmuserservice.AccountsUtil;
import com.devqube.crmuserservice.DefaultKafkaServiceToRemoveCrmObject;
import com.devqube.crmuserservice.TokenUtil;
import com.devqube.crmuserservice.account.exception.TooManyUserException;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.account.web.dto.Passwords;
import com.devqube.crmuserservice.account.web.dto.SetPasswordRequest;
import com.devqube.crmuserservice.accountConfiguration.AccountConfigurationService;
import com.devqube.crmuserservice.globalConfiguration.GlobalConfigurationService;
import com.devqube.crmuserservice.permissions.role.Role;
import com.devqube.crmuserservice.permissions.role.RoleRepository;
import com.devqube.crmuserservice.token.TokenService;
import com.devqube.crmuserservice.token.model.Token;
import com.devqube.crmuserservice.token.model.TokenTypeEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.naming.OperationNotSupportedException;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountsServiceTest {

    @Mock
    private AccountsRepository accountsRepository;
    @Mock
    private PersonRepository personRepository;
    @Mock
    private GlobalConfigurationService configService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private TokenService tokenService;
    @Mock
    private AccessService accessService;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private AccountConfigurationService accountConfigurationService;
    @Mock
    private LicenseCheck licenseCheck;

    @InjectMocks
    private AccountsService accountsService;

    private Account account;
    private Role role;

    @Before
    public void setUp() throws BadRequestException {
        this.account = AccountsUtil.createAccount();
        this.role = AccountsUtil.getRole();
        new DefaultKafkaServiceToRemoveCrmObject().setDefault();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Logged-Account-Email", this.account.getPerson().getEmail());
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        when(passwordEncoder.encode(anyString())).thenAnswer(e -> e.getArguments()[0]);
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        when(accessService.assignAndVerify(account)).thenReturn(account);
    }

    @Test
    public void shouldSaveUserWhenEmailNotOccupied() throws KafkaSendMessageException, BadRequestException, EntityNotFoundException, TooManyUserException {
        when(accountsRepository.save(any())).thenReturn(account);
        when(accessService.assignAndVerify(any())).thenReturn(account);
        when(roleRepository.findOneById(anyLong())).thenReturn(role);
        when(accountsRepository.countByPersonEmail(any())).thenReturn(0L);
        when(licenseCheck.refreshMaxUserCount()).thenReturn(1000L);
        assertEquals(accountsService.save(account), account);
    }

    @Test(expected = UserEmailOccupiedException.class)
    public void shouldThrowExceptionWhenEmailOccupied() throws KafkaSendMessageException, BadRequestException, EntityNotFoundException, TooManyUserException {
        when(accountsRepository.countByPersonEmail(any())).thenReturn(1L);
        when(licenseCheck.refreshMaxUserCount()).thenReturn(1000L);
        accountsService.save(account);
    }

    @Test
    public void shouldModifyAccount() throws DifferentVersionException, EntityNotFoundException, OperationNotSupportedException, BadRequestException {
        account.getPerson().setAdditionalData("[]");
        when(accountsRepository.findById(any())).thenReturn(Optional.of(account));
        when(roleRepository.findOneById(anyLong())).thenReturn(role);
        when(accountsRepository.save(any())).thenReturn(account);
        when(accessService.assignAndVerify(any(), any())).thenReturn(account);
        accountsService.modifyAccount(1L, account);
        verify(accountsRepository, times(1)).save(any());
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestIfAdditionalDataIsIncorrectJsonWhenModifyUser() throws DifferentVersionException, BadRequestException, OperationNotSupportedException, EntityNotFoundException {
        account.getPerson().setAdditionalData("==");
        when(accountsRepository.findById(any())).thenReturn(Optional.of(account));
        accountsService.modifyAccount(1L, account);
        verify(accountsRepository, times(1)).save(any());
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestIfAdditionalDataIsIncorrectJsonWhenCreateUser() throws BadRequestException, EntityNotFoundException, KafkaSendMessageException, TooManyUserException {
        account.getPerson().setAdditionalData("==");
        when(licenseCheck.refreshMaxUserCount()).thenReturn(1000L);
        accountsService.save(account);
        verify(accountsRepository, times(1)).save(any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowNotFoundWhenModifyAccount() throws DifferentVersionException, EntityNotFoundException, OperationNotSupportedException, BadRequestException {
        when(accountsRepository.findById(any())).thenReturn(Optional.empty());
        accountsService.modifyAccount(1L, account);
    }

    @Test(expected = DifferentVersionException.class)
    public void shouldThrowConflictWhenModifyAccount() throws DifferentVersionException, EntityNotFoundException, OperationNotSupportedException, BadRequestException {
        Account newAccount = Account.builder()
                .id(1L)
                .activated(true)
                .version(1L)
                .build();

        account.setVersion(2L);
        when(accountsRepository.findById(any())).thenReturn(Optional.of(account));
        accountsService.modifyAccount(1L, newAccount);
    }

    @Test(expected = OperationNotSupportedException.class)
    public void shouldThrowExceptionWhenActivationOwnAccount() throws EntityNotFoundException, OperationNotSupportedException, DifferentVersionException, BadRequestException {
        Account newAccount = AccountsUtil.createAccount();
        newAccount.setActivated(false);
        when(accountsRepository.findById(any())).thenReturn(Optional.of(account));
        when(accountsRepository.findByPersonEmail(any())).thenReturn(account);
        accountsService.modifyAccount(newAccount.getId(), newAccount);
    }


    @Test(expected = OperationNotSupportedException.class)
    public void shouldThrowExceptionWhenModifyingEmail() throws EntityNotFoundException, OperationNotSupportedException, DifferentVersionException, BadRequestException {
        Account newAccount = AccountsUtil.createAccount();
        newAccount.getPerson().setEmail("newEmail@devqube.com");
        when(accountsRepository.findById(any())).thenReturn(Optional.of(account));
        accountsService.modifyAccount(newAccount.getId(), newAccount);
    }

    @Test
    public void shouldSaveUserWithoutPassword() throws KafkaSendMessageException, BadRequestException, EntityNotFoundException, TooManyUserException {
        account.getPerson().setPassword(null);
        when(accountsRepository.save(any())).thenReturn(account);
        when(roleRepository.findOneById(anyLong())).thenReturn(role);
        when(accessService.assignAndVerify(any())).thenReturn(account);
        when(licenseCheck.refreshMaxUserCount()).thenReturn(1000L);
        String passwordAfterSave = accountsService.save(account).getPerson().getPassword();
        assertNull(passwordAfterSave);
    }

    @Test
    public void shouldHashPasswordWhenPresent() throws KafkaSendMessageException, BadRequestException, EntityNotFoundException, TooManyUserException {
        when(accountsRepository.save(any())).thenReturn(account);
        when(roleRepository.findOneById(anyLong())).thenReturn(role);
        when(accessService.assignAndVerify(any())).thenReturn(account);
        when(licenseCheck.refreshMaxUserCount()).thenReturn(1000L);
        String passwordAfterSave = accountsService.save(account).getPerson().getPassword();
        assertNotNull(passwordAfterSave);
    }

    @Test
    public void shouldRemoveAccount() throws BadRequestException, EntityNotFoundException {
        when(accountsRepository.findById(any())).thenReturn(Optional.ofNullable(account));
        accountsService.removeAccount(account.getId());
        verify(accountsRepository, times(1)).save(any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowNotFoundWhenRemoveAccount() throws EntityNotFoundException, BadRequestException {
        when(accountsRepository.findById(any())).thenReturn(Optional.empty());
        accountsService.removeAccount(account.getId());
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestWhenRemoveAccount() throws EntityNotFoundException, BadRequestException {
        when(accountsRepository.findById(any())).thenReturn(Optional.ofNullable(account));
        when(accountsRepository.findByPersonEmail(any())).thenReturn(account);
        accountsService.removeAccount(account.getId());
    }

    @Test
    public void shouldChangePasswordWhenPasswordPolicyIsNotActive()
            throws EntityNotFoundException, BadOldPasswordException, PasswordPolicyNotFulfilledException {
        when(configService.isPasswordPolicyActive()).thenReturn(false);
        when(accountsRepository.findById(any())).thenReturn(Optional.of(account));
        accountsService.changePassword(account.getId(), new Passwords()
                .oldPassword(account.getPerson().getPassword())
                .newPassword("123"));
        assertEquals("123", account.getPerson().getPassword());
    }

    @Test
    public void shouldChangePasswordWhenPasswordPolicyActiveAndPasswordOK()
            throws EntityNotFoundException, BadOldPasswordException, PasswordPolicyNotFulfilledException {
        when(configService.isPasswordPolicyActive()).thenReturn(true);
        when(accountsRepository.findById(any())).thenReturn(Optional.of(account));
        when(configService.getPasswordPolicyRegex()).thenReturn("(?=.{9,})(?=.*?[^\\w\\s])(?=.*?[0-9])(?=.*?[A-Z]).*?[a-z].*");
        accountsService.changePassword(account.getId(), new Passwords()
                .oldPassword(account.getPerson().getPassword())
                .newPassword("HasloPolityczniePopr@wn3"));
        assertEquals("HasloPolityczniePopr@wn3", account.getPerson().getPassword());
    }

    @Test(expected = PasswordPolicyNotFulfilledException.class)
    public void shouldThrowErrorWhenPasswordPolicyActiveAndPasswordNotOK()
            throws EntityNotFoundException, BadOldPasswordException, PasswordPolicyNotFulfilledException {
        when(configService.isPasswordPolicyActive()).thenReturn(true);
        when(accountsRepository.findById(any())).thenReturn(Optional.of(account));
        when(configService.getPasswordPolicyRegex()).thenReturn("(?=.{9,})(?=.*?[^\\w\\s])(?=.*?[0-9])(?=.*?[A-Z]).*?[a-z].*");
        accountsService.changePassword(account.getId(), new Passwords()
                .oldPassword(account.getPerson().getPassword())
                .newPassword("123"));
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenChangingPasswordOfNotExistingUser()
            throws EntityNotFoundException, BadOldPasswordException, PasswordPolicyNotFulfilledException {
        when(accountsRepository.findById(any())).thenReturn(Optional.empty());
        accountsService.changePassword(account.getId(), new Passwords()
                .oldPassword(account.getPerson().getPassword())
                .newPassword("123"));
    }

    @Test(expected = BadOldPasswordException.class)
    public void shouldThrowExceptionWhenBadOldPassword()
            throws EntityNotFoundException, BadOldPasswordException, PasswordPolicyNotFulfilledException {
        when(accountsRepository.findById(any())).thenReturn(Optional.of(account));
        when(passwordEncoder.matches(any(), any())).thenReturn(false);
        accountsService.changePassword(account.getId(), new Passwords()
                .oldPassword("badPassword")
                .newPassword("123"));
    }

    @Test
    public void shouldSendActivationEmail() throws KafkaSendMessageException {
        accountsService.sendActivationEmail(account);
        verify(tokenService, times(1)).sendEmailWithToken(eq(account), eq(TokenTypeEnum.SEND_ACTIVATION_CODE));
    }

    @Test
    public void shouldResendActivationEmail() throws KafkaSendMessageException {
        when(accountsRepository.getOne(account.getId())).thenReturn(account);
        accountsService.resendActivationEmail(account.getId());
        verify(tokenService, times(1)).sendEmailWithToken(eq(account), eq(TokenTypeEnum.RESEND_ACTIVATION_CODE));
    }

    @Test
    public void shouldSendResetPasswordEmail() throws KafkaSendMessageException {
        when(accountsRepository.findByPersonEmail(any())).thenReturn(account);
        accountsService.sendResetPasswordLink(account.getPerson().getEmail());
        verify(tokenService, times(1)).sendEmailWithToken(eq(account), eq(TokenTypeEnum.RESET_PASSWORD));
    }

    @Test
    public void shouldSetPasswordAndRemoveToken() throws BadRequestException, EntityNotFoundException, PasswordPolicyNotFulfilledException {
        Token token = TokenUtil.createToken();
        SetPasswordRequest passwordRequest = new SetPasswordRequest().password("test2");
        when(tokenService.getValidTokenObject(any(), any())).thenReturn(token);


        accountsService.setAccountPassword(token.getToken(), passwordRequest);

        verify(accountsRepository, times(1)).save(any());
        verify(tokenService, times(1)).removeToken(token.getId());
    }

    @Test
    public void shouldSetNewRoleIfRoleWasNotPresent() throws EntityNotFoundException, BadRequestException, KafkaSendMessageException, TooManyUserException {
        Role role = new Role(1L, "role", null, null, null);
        when(roleRepository.findOneById(1L)).thenReturn(role);
        account.setRole(role);
        when(licenseCheck.refreshMaxUserCount()).thenReturn(1000L);
        accountsService.save(account);
        verify(accountsRepository, times(1)).save(any());
    }

    @Test
    public void shouldDeleteRoleFromAccount() throws EntityNotFoundException, BadRequestException, KafkaSendMessageException,
            DifferentVersionException, OperationNotSupportedException, TooManyUserException {
        Role role = new Role(1L, "role", null, null, null);
        when(roleRepository.findOneById(1L)).thenReturn(role);
        when(this.accountsRepository.findById(1L)).thenReturn(Optional.of(account));
        when(this.accountsRepository.save(account)).thenReturn(account);
        when(accessService.assignAndVerify(any(), any())).thenReturn(account);
        account.setRole(role);
        when(licenseCheck.refreshMaxUserCount()).thenReturn(1000L);
        accountsService.save(account);
        account.setRole(null);
        accountsService.modifyAccount(1L, account);
        verify(accountsRepository, times(2)).save(any());
    }

    @Test(expected = BadRequestException.class)
    public void shouldNotSetNewRoleWhenRoleIsOccupiedAndHasSubRoles() throws EntityNotFoundException, BadRequestException, KafkaSendMessageException, TooManyUserException {
        Role role = new Role(1L, "role", null, Set.of(AccountsUtil.createAccount(10000L)), Set.of(
                new Role(2L, "role2", null, null, null),
                new Role(3L, "role3", null, null, null)));
        when(roleRepository.findOneById(1L)).thenReturn(role);
        account.setRole(role);
        when(licenseCheck.refreshMaxUserCount()).thenReturn(1000L);
        accountsService.save(account);
    }

    @Test
    public void shouldSetNewRoleWhenRoleIsOccupiedAndDoesntHaveSubRoles() throws EntityNotFoundException, BadRequestException, KafkaSendMessageException, TooManyUserException {
        Role role = new Role(1L, "role", null, Set.of(new Account()), null);
        when(roleRepository.findOneById(1L)).thenReturn(role);
        account.setRole(role);
        when(licenseCheck.refreshMaxUserCount()).thenReturn(1000L);
        accountsService.save(account);
    }

    @Test
    public void shouldSetNewRoleWhenRoleIsNotOccupiedAndHasSubRoles() throws EntityNotFoundException, BadRequestException, KafkaSendMessageException, TooManyUserException {
        Role role = new Role(1L, "role", null, null, Set.of(
                new Role(2L, "role2", null, null, null),
                new Role(3L, "role3", null, null, null)));
        when(roleRepository.findOneById(1L)).thenReturn(role);
        when(licenseCheck.refreshMaxUserCount()).thenReturn(1000L);
        account.setRole(role);
        accountsService.save(account);
    }

    @Test
    public void shouldSetNewRoleWhenRoleIsNotOccupiedAndDoesntHaveSubRoles() throws EntityNotFoundException, BadRequestException, KafkaSendMessageException, TooManyUserException {
        Role role = new Role(1L, "role", null, null, null);
        when(roleRepository.findOneById(1L)).thenReturn(role);
        account.setRole(role);
        when(licenseCheck.refreshMaxUserCount()).thenReturn(1000L);
        accountsService.save(account);
    }
}
