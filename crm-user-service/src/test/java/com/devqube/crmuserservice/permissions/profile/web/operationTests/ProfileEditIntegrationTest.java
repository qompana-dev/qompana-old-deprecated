package com.devqube.crmuserservice.permissions.profile.web.operationTests;

import com.devqube.crmuserservice.permissions.module.Module;
import com.devqube.crmuserservice.permissions.moduleCategory.ModuleCategory;
import com.devqube.crmuserservice.permissions.modulePermission.ModulePermission;
import com.devqube.crmuserservice.permissions.permission.Permission;
import com.devqube.crmuserservice.permissions.profile.web.ProfileControllerIntegrationTest;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileSaveDTO;
import com.devqube.crmuserservice.permissions.webControl.WebControl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class ProfileEditIntegrationTest extends ProfileControllerIntegrationTest {
    @Test
    public void shouldEditProfileName() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");
        profile.setName("new name");
        ResponseEntity<ProfileSaveDTO> response = restTemplate.exchange(baseUrl + "/profile/" + profile.getId(),
                HttpMethod.PUT, new HttpEntity<>(profile), ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(profile.getName(), response.getBody().getName());
    }

    @Test
    public void shouldNotEditProfileNameWhenNameOccupied() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");
        saveProfileAndReturnIt("name2");
        profile.setName("name2");
        ResponseEntity<ProfileSaveDTO> response = restTemplate.exchange(baseUrl + "/profile/" + profile.getId(),
                HttpMethod.PUT, new HttpEntity<>(profile), ProfileSaveDTO.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldEditModulePermissionBooleans() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");
        ModulePermission modulePermission = profile.getModulePermissions().get(0);
        modulePermission.setCreate(true);
        profile.setModulePermissions(List.of(modulePermission));

        ResponseEntity<ProfileSaveDTO> response = restTemplate.exchange(baseUrl + "/profile/" + profile.getId(),
                HttpMethod.PUT, new HttpEntity<>(profile), ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        ModulePermission savedModulePermission = response.getBody().getModulePermissions().get(0);
        assertTrue(savedModulePermission.isCreate());
    }

    @Test
    public void shouldEditPermissionState() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");
        Permission permission = profile.getModulePermissions().get(0)
                                       .getPermissions().get(0);
        permission.setState(Permission.State.INVISIBLE);
        profile.getModulePermissions().get(0).setPermissions(List.of(permission));

        ResponseEntity<ProfileSaveDTO> response = restTemplate.exchange(baseUrl + "/profile/" + profile.getId(),
                HttpMethod.PUT, new HttpEntity<>(profile), ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        Permission savedPermission = response.getBody().getModulePermissions().get(0)
                                                       .getPermissions().get(0);
        assertEquals(permission.getState(), savedPermission.getState());
    }

    @Test
    public void shouldNotEditModuleCategory() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");
        ModuleCategory category = profile.getModulePermissions().get(0)
                                         .getModule()
                                         .getCategory();
        category.setName("new name");

        ResponseEntity<ProfileSaveDTO> response = restTemplate.exchange(baseUrl + "/profile/" + profile.getId(),
                HttpMethod.PUT, new HttpEntity<>(profile), ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(moduleCategoryRepository.findById(category.getId()).isPresent());
        assertEquals(module.getCategory().getName(), moduleCategoryRepository.findById(category.getId()).get().getName());
    }

    @Test
    public void shouldNotEditModule() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");
        Module editedModule = profile.getModulePermissions().iterator().next()
                                     .getModule();
        editedModule.setName("new name");

        ResponseEntity<ProfileSaveDTO> response = restTemplate.exchange(baseUrl + "/profile/" + profile.getId(),
                HttpMethod.PUT, new HttpEntity<>(profile), ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        Optional<Module> fromDb = moduleRepository.findById(editedModule.getId());
        assertTrue(fromDb.isPresent());
        assertEquals(module.getName(), fromDb.get().getName());
    }

    @Test
    public void shouldNotEditWebControl() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");
        WebControl editedWebControl = profile.getModulePermissions().get(0)
                                             .getPermissions().get(0)
                                             .getWebControl();
        String oldName = editedWebControl.getName();
        editedWebControl.setName("new name");

        ResponseEntity<ProfileSaveDTO> response = restTemplate.exchange(baseUrl + "/profile/" + profile.getId(),
                HttpMethod.PUT, new HttpEntity<>(profile), ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(oldName, response.getBody().getModulePermissions().get(0)
                                                .getPermissions().get(0)
                                                .getWebControl()
                                                .getName());
    }
}
