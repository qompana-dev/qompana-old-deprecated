package com.devqube.crmuserservice.permissions.profile;

import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmuserservice.account.AccountsRepository;
import com.devqube.crmuserservice.account.model.Account;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProfileServiceTest {
    @Mock
    private ProfileRepository profileRepository;

    @Mock
    private AccountsRepository accountsRepository;

    @InjectMocks
    private ProfileService profileService;

    private Profile profile;

    @Before
    public void setUp() {
        this.profile = new Profile();
        profile.setName("name");
    }

    @Test
    public void shouldAddProfileWhenNotExists() throws BadRequestException {
        when(profileRepository.findByName(anyString())).thenReturn(null);
        when(profileRepository.save(any())).thenReturn(profile);
        assertEquals(profileService.save(profile).getName(), profile.getName());
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenProfileExists() throws BadRequestException {
        when(profileRepository.findByName(anyString())).thenReturn(profile);
        profileService.save(profile);
    }

    @Test
    public void shouldReturnNullWhenAccountDoesntExist() {
        when(accountsRepository.findById(any())).thenReturn(Optional.empty());
        assertNull(profileService.getProfileByAccountId(1L));
    }

    @Test
    public void shouldReturnNullWhenUserDoesntHaveRole() {
        Account account = new Account();
        when(accountsRepository.findById(any())).thenReturn(Optional.of(account));
        assertNull(profileService.getProfileByAccountId(1L));
    }
}
