package com.devqube.crmuserservice.permissions.role;

import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmuserservice.account.AccountsRepository;
import com.devqube.crmuserservice.account.model.Account;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RoleServiceTest {
    @Mock
    private RoleRepository roleRepository;

    @Mock
    private AccountsRepository accountsRepository;

    @InjectMocks
    private RoleService roleService;

    private Role role;

    @Before
    public void setUp() {
        this.role = new Role();
        role.setId(1L);
        role.setName("name");
    }

    @Test
    public void shouldAddProfileWhenNotExists() throws BadRequestException, EntityNotFoundException {
        when(roleRepository.findByName(anyString())).thenReturn(null);
        when(roleRepository.save(any())).thenReturn(role);
        assertEquals(roleService.save(role, null).getName(), role.getName());
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenProfileExists() throws BadRequestException, EntityNotFoundException {
        when(roleRepository.findByName(anyString())).thenReturn(role);
        roleService.save(role, null);
    }

    @Test
    public void shouldAddProfileWithItsParent() throws BadRequestException, EntityNotFoundException {
        Role parentRole = new Role();
        parentRole.setId(2L);
        parentRole.setName("parent");
        when(roleRepository.save(any())).thenReturn(role);
        when(roleRepository.findById(any())).thenReturn(Optional.of(role));
        assertEquals(roleService.save(role, 2L).getName(), role.getName());
        verify(roleRepository, times(1)).addParent(any(), any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldNotMoveRoleWhenParentNotFound() throws EntityNotFoundException, BadRequestException {
        when(roleRepository.findById(any())).thenReturn(Optional.empty());
        roleService.moveRole(1L, 2L);
        verify(roleRepository, times(1)).deleteParent(any());
        verify(roleRepository, times(0)).addParent(any(), any());
    }

    @Test
    public void shouldNotMoveRoleWhenEverythingOK() throws EntityNotFoundException, BadRequestException {
        when(roleRepository.findById(any())).thenReturn(Optional.of(role));
        roleService.moveRole(1L, 2L);
        verify(roleRepository, times(1)).deleteParent(any());
        verify(roleRepository, times(1)).addParent(any(), any());
    }

    @Test(expected = BadRequestException.class)
    public void shouldNotModifyRoleWhenChangedNameToExisting() throws BadRequestException, EntityNotFoundException {
        Role anotherRole = new Role();
        anotherRole.setId(2L);
        anotherRole.setName(role.getName());
        Role roleBeforeChangingName = new Role();
        roleBeforeChangingName.setId(3L);
        roleBeforeChangingName.setName("oldName");
        role.setProfiles(new HashSet<>());
        anotherRole.setName(role.getName());
        when(roleRepository.findById(any())).thenReturn(Optional.of(roleBeforeChangingName));
        when(roleRepository.findByName(role.getName())).thenReturn(anotherRole);
        roleService.modifyRole(role, null);
    }

    @Test
    public void shouldModifyRoleWhenNewNameIsNotOccupied() throws BadRequestException, EntityNotFoundException {
        role.setProfiles(new HashSet<>());
        when(roleRepository.findById(any())).thenReturn(Optional.of(role));
        when(roleRepository.save(role)).thenReturn(role);
        roleService.modifyRole(role, null);
    }

    @Test
    public void shouldDeleteRoleWhenExists() throws EntityNotFoundException, BadRequestException {
        when(roleRepository.findById(any())).thenReturn(Optional.of(role));
        roleService.delete(1L);
        verify(roleRepository, times(1)).deleteById(any());
    }

    @Test(expected = BadRequestException.class)
    public void shouldNotDeleteRoleWhenUserIsAssigned() throws EntityNotFoundException, BadRequestException {
        role.setAccounts(Set.of(new Account()));
        when(roleRepository.findById(any())).thenReturn(Optional.of(role));
        roleService.delete(1L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenRoleDoesntExists() throws EntityNotFoundException, BadRequestException {
        when(roleRepository.findById(any())).thenReturn(Optional.empty());
        roleService.delete(1L);
    }

    @Test
    public void shouldRetrieveRoleWhenExists() throws EntityNotFoundException {
        when(roleRepository.findById(any())).thenReturn(Optional.of(role));
        assertEquals(role.getId(), roleService.getById(1L).getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldNotRetrieveRoleWhenDoesntExists() throws EntityNotFoundException {
        when(roleRepository.findById(any())).thenReturn(Optional.empty());
        roleService.getById(1L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenRoleNotExistWhenGettingAccountsByRole() throws EntityNotFoundException {
        String roleName = "roleName";
        when(roleRepository.findByName(eq(roleName))).thenReturn(null);
        roleService.getAccountsForRoleChild(roleName);
    }

    @Test
    public void shouldReturnEmptyListIfRoleNotContainChildWhenGettingAccountsByRole() throws EntityNotFoundException {
        String roleName = "role2Lvl2";
        when(roleRepository.findByName(eq(roleName))).thenReturn(Role.builder().id(3L).name("role2Lvl2").accounts(getAccount(4L, 5L)).build());

        List<Long> accountsForRoleChild = roleService.getAccountsForRoleChild(roleName);
        assertNotNull(accountsForRoleChild);
        assertEquals(0, accountsForRoleChild.size());
    }

    @Test
    public void shouldReturnEmptyListIfRoleNotContainAccountWhenGettingAccountsByRole() throws EntityNotFoundException {
        String roleName = "role1Lvl3";
        when(roleRepository.findByName(eq(roleName))).thenReturn(Role.builder().id(4L).name("role1Lvl3").build());

        List<Long> accountsForRoleChild = roleService.getAccountsForRoleChild(roleName);
        assertNotNull(accountsForRoleChild);
        assertEquals(0, accountsForRoleChild.size());
    }

    @Test
    public void shouldReturnListAllAccountWhenGettingAccountsByRole() throws EntityNotFoundException {
        Role roleLvl1 = Role.builder().id(1L).name("roleLvl1").accounts(getAccount(1L)).build();
        Role role1Lvl2 = Role.builder().id(2L).name("role1Lvl2").accounts(getAccount(2L)).build();
        Role role2Lvl2 = Role.builder().id(3L).name("role2Lvl2").accounts(getAccount(3L)).build();
        Role role3Lvl2 = Role.builder().id(4L).name("role3Lvl2").accounts(getAccount(4L, 5L)).build();
        Role role1Lvl3 = Role.builder().id(5L).name("role1Lvl3").build();
        Role role2Lvl3 = Role.builder().id(6L).name("role2Lvl3").accounts(getAccount(6L, 7L)).build();

        Set<Role> lvl3Set = new HashSet<>();
        lvl3Set.add(role1Lvl3);
        lvl3Set.add(role2Lvl3);
        role1Lvl2.setChildren(lvl3Set);
        Set<Role> lvl2Set = new HashSet<>();
        lvl2Set.add(role1Lvl2);
        lvl2Set.add(role2Lvl2);
        lvl2Set.add(role3Lvl2);
        roleLvl1.setChildren(lvl2Set);

        String roleName = "roleLvl1";
        when(roleRepository.findByName(eq(roleName))).thenReturn(roleLvl1);

        List<Long> accountsForRoleChild = roleService.getAccountsForRoleChild(roleName);
        assertNotNull(accountsForRoleChild);
        assertEquals(6, accountsForRoleChild.size());

        for (Integer i = 2; i <= 7; i++) {
            assertTrue(accountsForRoleChild.contains(i.longValue()));
        }
    }

    @Test
    public void shouldReturnOnlyMyRoleIdWhenNoChildrenPresent() throws EntityNotFoundException {
        Account account = new Account();
        account.setRole(Role.builder().id(1L).name("role1").build());
        when(accountsRepository.findByPersonEmail(anyString())).thenReturn(account);

        List<Long> ids = roleService.getAccountRoleIdWithChildren("someEmail");
        assertEquals(1L, ids.size());
        assertTrue(ids.contains(1L));

    }

    @Test
    public void shouldReturnRoleIdAndChildrenIdsWhenPresent() throws EntityNotFoundException {
        Account account = new Account();
        account.setRole(Role.builder().id(1L).name("role1").children(
                Set.of(Role.builder().id(2L).name("role2").children(
                        Set.of(Role.builder().id(3L).name("role3").build())
                ).build())
        ).build());
        when(accountsRepository.findByPersonEmail(anyString())).thenReturn(account);

        List<Long> ids = roleService.getAccountRoleIdWithChildren("someEmail");
        assertEquals(3L, ids.size());
        assertTrue(ids.containsAll(Set.of(1L, 2L, 3L)));

    }

    private Set<Account> getAccount(long... ids) {
        Set<Account> result = new HashSet<>();
        for (int i = 0; i < ids.length; i++) {
            result.add(Account.builder().id(ids[i]).build());
        }
        return result;
    }
}
