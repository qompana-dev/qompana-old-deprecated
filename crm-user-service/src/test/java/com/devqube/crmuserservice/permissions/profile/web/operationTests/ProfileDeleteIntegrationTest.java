package com.devqube.crmuserservice.permissions.profile.web.operationTests;

import com.devqube.crmuserservice.permissions.modulePermission.ModulePermission;
import com.devqube.crmuserservice.permissions.profile.web.ProfileControllerIntegrationTest;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileSaveDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class ProfileDeleteIntegrationTest extends ProfileControllerIntegrationTest {
    @Test
    public void shouldReturn404WhenProfileNotFound() {
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/profile/-100", HttpMethod.DELETE,
                null, Void.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldDeleteProfile() {
        Long profileId = saveProfileAndReturnIt("name").getId();

        HttpStatus httpStatus = deleteProfile(profileId);
        assertEquals(HttpStatus.OK, httpStatus);
        assertTrue(profileRepository.findById(profileId).isEmpty());
    }


    @Test
    public void shouldDeleteModulePermissionAlongWithProfile() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");

        deleteProfile(profile.getId());
        ModulePermission modulePermission = profile.getModulePermissions().get(0);
        assertTrue(modulePermissionRepository.findById(modulePermission.getId()).isEmpty());
    }

    @Test
    public void shouldDeletePermissionAlongWithModulePermission() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");

        deleteProfile(profile.getId());
        Long permissionId = profile.getModulePermissions().get(0)
                                   .getPermissions().get(0)
                                   .getId();
        assertTrue(permissionRepository.findById(permissionId).isEmpty());
    }

    @Test
    public void shouldNotDeleteModuleCategoryWithProfile() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");

        deleteProfile(profile.getId());
        Long moduleCategoryId = profile.getModulePermissions().get(0)
                                       .getModule()
                                       .getCategory()
                                       .getId();
        assertTrue(moduleCategoryRepository.findById(moduleCategoryId).isPresent());
    }

    @Test
    public void shouldNotDeleteModuleWithProfile() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");

        deleteProfile(profile.getId());
        Long moduleId = profile.getModulePermissions().get(0)
                               .getModule()
                               .getId();
        assertTrue(moduleRepository.findById(moduleId).isPresent());
    }

    @Test
    public void shouldNotDeleteWebControlWithProfile() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");

        deleteProfile(profile.getId());
        Long webControlId = profile.getModulePermissions().get(0)
                                   .getPermissions().get(0)
                                   .getWebControl()
                                   .getId();
        assertTrue(webControlRepository.findById(webControlId).isPresent());
    }

    private HttpStatus deleteProfile(Long id) {
        return restTemplate.exchange(baseUrl + "/profile/" + id, HttpMethod.DELETE,
                null, Void.class).getStatusCode();
    }
}
