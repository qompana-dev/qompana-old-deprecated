package com.devqube.crmuserservice.permissions.role.web;

import com.devqube.crmuserservice.AbstractIntegrationTest;
import com.devqube.crmuserservice.AccountsUtil;
import com.devqube.crmuserservice.account.AccountsRepository;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.account.model.Person;
import com.devqube.crmuserservice.accountConfiguration.AccountConfigurationRepository;
import com.devqube.crmuserservice.permissions.profile.Profile;
import com.devqube.crmuserservice.permissions.profile.ProfileRepository;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileListDTO;
import com.devqube.crmuserservice.permissions.role.Role;
import com.devqube.crmuserservice.permissions.role.RoleRepository;
import com.devqube.crmuserservice.permissions.role.web.dto.IdAndEmailDTO;
import com.devqube.crmuserservice.permissions.role.web.dto.RoleDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class RoleControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private AccountsRepository accountsRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private AccountConfigurationRepository accountConfigurationRepository;

    private Role role;

    @Before
    public void setUp() {
        super.setUp();
        role = AccountsUtil.getRole();
    }

    @After
    public void cleanUp() {
        this.accountConfigurationRepository.deleteAll();
        this.accountsRepository.deleteAll();
        this.roleRepository.deleteAll();
        this.profileRepository.deleteAll();
    }

    @Test
    public void shouldSaveProfile() {
        ResponseEntity<Role> response = restTemplate.postForEntity(baseUrl + "/role", role, Role.class);
        assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        assertNotNull(response.getBody());
        assertEquals(role.getName(), response.getBody().getName());
    }

    @Test
    public void shouldNotSaveProfileWhenNoName() {
        role.setName(null);
        ResponseEntity<Role> response = restTemplate.postForEntity(baseUrl + "/role", role, Role.class);
        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldNotSaveProfileWhenNameOccupied() {
        roleRepository.save(role);
        ResponseEntity<Role> response = restTemplate.postForEntity(baseUrl + "/role", role, Role.class);
        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldReturn404IfRoleNotExistWhenGettingAccountIdsByRoleName() {
        ResponseEntity<Long[]> response = restTemplate.getForEntity(baseUrl + "/role/name/not%20existed%20role/child/accounts/id", Long[].class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldSaveProfileWithParentWhenEverythingSet() {
        Role parent = roleRepository.save(AccountsUtil.getRoleWithoutId());
        RoleDTO newRole = createNewRoleWithParent(parent.getId());
        ResponseEntity<Role> response = restTemplate.postForEntity(baseUrl + "/role", newRole, Role.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void shouldNotSaveProfileWhenParentNotFound() {
        RoleDTO newRole = createNewRoleWithParent(-100L);
        ResponseEntity<Role> response = restTemplate.postForEntity(baseUrl + "/role", newRole, Role.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldReturnRolesAsTree() {
        Role savedRole = roleRepository.save(AccountsUtil.getRoleWithoutId());
        Role role = addRoleWithParent(savedRole);
        ResponseEntity<Role[]> response = restTemplate.getForEntity(baseUrl + "/role?flat=false", Role[].class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        Optional<Role> first = Arrays.stream(response.getBody()).filter(c -> c.getId().equals(savedRole.getId())).findFirst();
        assertFalse(first.isEmpty());
        assertEquals(1, first.get().getChildren().size());
        assertTrue(first.get().getChildren().contains(role));
    }

    @Test
    public void shouldReturnRolesAsFlatList() {
        addRoleWithParent(roleRepository.save(AccountsUtil.getRoleWithoutId()));
        ResponseEntity<Role[]> response = restTemplate.getForEntity(baseUrl + "/role/flat-list", Role[].class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(3L, response.getBody().length);
        assertNull(response.getBody()[0].getChildren());
        assertNull(response.getBody()[1].getChildren());
    }

    @Test
    public void shouldReturnExistingRole() {
        Role role = roleRepository.save(this.role);
        ResponseEntity<Role> response = restTemplate.getForEntity(baseUrl + "/role/" + role.getId(), Role.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(role.getId(), response.getBody().getId());
    }

    @Test
    public void shouldReturn404WhenRoleDoesntExist() {
        ResponseEntity<Role> response = restTemplate.getForEntity(baseUrl + "/role/1002", Role.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldDeleteRoleWhenItExists() {
        Role newRole = AccountsUtil.getRole();
        newRole.setId(null);
        Role role = roleRepository.save(newRole);
        restTemplate.delete(baseUrl + "/role/" + newRole.getId());
        assertTrue(roleRepository.findById(role.getId()).isEmpty());
    }

    @Test
    public void shouldReturn404WhenDeletingNotExistingRole() {
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/role/1000", HttpMethod.DELETE, null, Void.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldModifyRoleWithNoParentAndSameName() {
        Role role = roleRepository.save(this.role);
        role.setProfiles(Collections.emptySet());
        ResponseEntity<Role> response = restTemplate.exchange(baseUrl + "/role", HttpMethod.PUT,
                new HttpEntity<>(modelMapper.map(role, RoleDTO.class)), Role.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(role.getId(), response.getBody().getId());

    }

    @Test
    public void shouldModifyRoleWithNoParentAndNewName() {
        Role role = roleRepository.save(AccountsUtil.getRoleWithoutId());
        role.setName("some new name5");
        role.setProfiles(Collections.emptySet());
        ResponseEntity<Role> response = restTemplate.exchange(baseUrl + "/role", HttpMethod.PUT,
                new HttpEntity<>(modelMapper.map(role, RoleDTO.class)), Role.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(role.getId(), response.getBody().getId());
    }

    @Test
    public void shouldModifyRoleWithParent() {
        Role save = roleRepository.save(AccountsUtil.getRoleWithoutId());
        this.role.setName("some new name1");
        Role savedRoleToUpdate = this.roleRepository.save(this.role);
        savedRoleToUpdate.setProfiles(Collections.emptySet());
        ResponseEntity<Role> response = restTemplate.exchange(baseUrl + "/role", HttpMethod.PUT,
                new HttpEntity<>(savedRoleToUpdate), Role.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(savedRoleToUpdate.getId(), response.getBody().getId());
    }

    @Test
    public void shouldModifyRoleWithNewProfiles() {
        Role parent = roleRepository.save(AccountsUtil.getRoleWithoutId());
        Profile profile = new Profile();
        profile.setName("test profile");
        profileRepository.save(profile);

        RoleDTO toUpdate = modelMapper.map(parent, RoleDTO.class);
        toUpdate.setProfiles(Collections.singletonList(modelMapper.map(profile, ProfileListDTO.class)));
        ResponseEntity<Role> response = restTemplate.exchange(baseUrl + "/role", HttpMethod.PUT,
                new HttpEntity<>(toUpdate), Role.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(parent.getId(), response.getBody().getId());
        assertNotNull(response.getBody().getProfiles());
        assertEquals(1, response.getBody().getProfiles().size());
    }

    @Test
    public void shouldNotModifyRoleWhenNameIsOccupied() {
        Role parent = roleRepository.save(AccountsUtil.getRoleWithoutId());
        Role newRole = new Role();
        newRole.setName("some new name2");
        roleRepository.save(newRole);

        RoleDTO toUpdate = modelMapper.map(parent, RoleDTO.class);
        toUpdate.setName("some new name2");
        toUpdate.setProfiles(Collections.emptyList());

        ResponseEntity<Role> response = restTemplate.exchange(baseUrl + "/role", HttpMethod.PUT,
                new HttpEntity<>(toUpdate), Role.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertTrue(roleRepository.findById(parent.getId()).isPresent());
        assertEquals(parent.getName(), roleRepository.findById(parent.getId()).get().getName());
    }

    @Test
    public void shouldNotModifyRoleWhenParentNotFound() {
        Role parent = roleRepository.save(AccountsUtil.getRoleWithoutId());
        RoleDTO toUpdate = modelMapper.map(parent, RoleDTO.class);
        toUpdate.setId(this.role.getId());
        toUpdate.setProfiles(Collections.emptyList());
        toUpdate.setName("some new name4");
        toUpdate.setSupervisor(-100L);
        ResponseEntity<Role> response = restTemplate.exchange(baseUrl + "/role", HttpMethod.PUT,
                new HttpEntity<>(toUpdate), Role.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertTrue(roleRepository.findById(parent.getId()).isPresent());
        assertEquals(parent.getName(), roleRepository.findById(parent.getId()).get().getName());
    }

    @Test
    public void shouldMoveRoleWhenOK() {
        Role parent = roleRepository.save(role);
        Role child = new Role();
        child.setName("child");
        child = roleRepository.save(child);
        ResponseEntity<Void> response = restTemplate.getForEntity(baseUrl + "/role/move?childId=" + child.getId()
                + "&parentId=" + parent.getId(), Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void shouldReturn404WhenMovingNotExistingRole() {
        Role parent = roleRepository.save(role);
        ResponseEntity<Void> response = restTemplate.getForEntity(baseUrl + "/role/move?childId=-200" +
                "&parentId=" + parent.getId(), Void.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }


    @Test
    public void shouldReturn404WhenMovingToNonExistingParent() {
        Role role = roleRepository.save(this.role);
        ResponseEntity<Void> response = restTemplate.getForEntity(baseUrl + "/role/move?childId=" + role.getId() +
                "&parentId=-300", Void.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldAddRoleWithOneAccountAssigned() {
        Account account = accountsRepository.save(AccountsUtil.createAccount());
        RoleDTO roleDTO = new RoleDTO()
                .name("role8")
                .profiles(Collections.emptyList())
                .accounts(List.of(new IdAndEmailDTO().id(account.getId())));
        ResponseEntity<Role> response = restTemplate.postForEntity(baseUrl + "/role", roleDTO, Role.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        Optional<Account> byId = accountsRepository.findById(account.getId());
        assertTrue(byId.isPresent());
        assertEquals(response.getBody().getId(), byId.get().getRole().getId());
    }

    @Test
    public void shouldAddRoleWithTwoAccountAssigned() {
        Account account = accountsRepository.save(AccountsUtil.createAccount());
        Account account2 = accountsRepository.save(AccountsUtil.createAccount(11L));
        RoleDTO roleDTO = new RoleDTO()
                .name("role9")
                .profiles(Collections.emptyList())
                .accounts(List.of(
                        new IdAndEmailDTO().id(account.getId()),
                        new IdAndEmailDTO().id(account2.getId())));
        ResponseEntity<Role> response = restTemplate.postForEntity(baseUrl + "/role", roleDTO, Role.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        Optional<Account> byId = accountsRepository.findById(account.getId());
        Optional<Account> byId2 = accountsRepository.findById(account2.getId());
        assertTrue(byId.isPresent());
        assertTrue(byId2.isPresent());
        assertEquals(response.getBody().getId(), byId.get().getRole().getId());
        assertEquals(response.getBody().getId(), byId2.get().getRole().getId());
    }

    @Test
    public void shouldSetSupervisorWhenSupervisorHasZeroAccountsAssigned() {
        Role supervisor = new Role();
        supervisor.setName("supervisor3");
        supervisor = roleRepository.save(supervisor);

        RoleDTO roleDTO = new RoleDTO().name("role10").supervisor(supervisor.getId());
        ResponseEntity<Role> response = restTemplate.postForEntity(baseUrl + "/role", roleDTO, Role.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        supervisor = roleRepository.findOneById(supervisor.getId());
        assertEquals(1L, supervisor.getChildren().size());
        assertEquals(response.getBody().getId(), supervisor.getChildren().iterator().next().getId());
    }

    @Test
    public void shouldSetSupervisorWhenSupervisorHasOneAccountsAssigned() {
        Role supervisor = new Role();
        supervisor.setName("supervisor4");
        supervisor = roleRepository.save(supervisor);

        Account account = new Account();
        account.setRole(supervisor);
        accountsRepository.save(account);

        RoleDTO roleDTO = new RoleDTO().name("role11").supervisor(supervisor.getId());
        ResponseEntity<Role> response = restTemplate.postForEntity(baseUrl + "/role", roleDTO, Role.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        supervisor = roleRepository.findOneById(supervisor.getId());
        assertEquals(1L, supervisor.getChildren().size());
        assertEquals(response.getBody().getId(), supervisor.getChildren().iterator().next().getId());
    }

    @Test
    public void shouldNotSetSupervisorWhenSupervisorHasTwoAccountsAssigned() {
        Role supervisor = new Role();
        supervisor.setName("supervisor1");
        supervisor = roleRepository.save(supervisor);

        Account account = new Account();
        account.setRole(supervisor);
        accountsRepository.save(account);
        Account account2 = new Account();
        account2.setRole(supervisor);
        accountsRepository.save(account2);

        RoleDTO roleDTO = new RoleDTO().name("role12").supervisor(supervisor.getId());
        ResponseEntity<Role> response = restTemplate.postForEntity(baseUrl + "/role", roleDTO, Role.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldAssignAccountsToRoleWithoutSubRole() {
        Role role = new Role();
        role.setName("role1");
        role = roleRepository.save(role);

        Account account = AccountsUtil.createAccount();
        account = accountsRepository.save(account);
        Account account2 = AccountsUtil.createAccount(11L);
        account2 = accountsRepository.save(account2);

        RoleDTO roleDTO = new RoleDTO()
                .id(role.getId())
                .name(role.getName())
                .profiles(Collections.emptyList())
                .accounts(List.of(
                        new IdAndEmailDTO().id(account.getId()),
                        new IdAndEmailDTO().id(account2.getId())));

        ResponseEntity<Role> response = restTemplate.exchange(baseUrl + "/role", HttpMethod.PUT,
                new HttpEntity<>(roleDTO), Role.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        Optional<Account> byId = accountsRepository.findById(account.getId());
        Optional<Account> byId2 = accountsRepository.findById(account2.getId());
        assertTrue(byId.isPresent());
        assertTrue(byId2.isPresent());
        assertEquals(role.getId(), byId.get().getRole().getId());
        assertEquals(role.getId(), byId2.get().getRole().getId());
    }

    @Test
    public void shouldAssignOneAccountToRoleWithSubRole() {
        Role role = new Role();
        role.setName("role2");
        role = roleRepository.save(role);

        Role subRole = new Role();
        subRole.setName("subRole2");
        subRole = roleRepository.save(subRole);
        roleRepository.addParent(role.getId(), subRole.getId());

        Account account = new Account();
        Role save = roleRepository.save(AccountsUtil.getRoleWithoutId());
        account.setRole(save);
        accountsRepository.save(account);

        RoleDTO roleDTO = new RoleDTO()
                .id(role.getId())
                .name(role.getName())
                .profiles(Collections.emptyList())
                .accounts(List.of(new IdAndEmailDTO().id(account.getId())));

        ResponseEntity<Role> response = restTemplate.exchange(baseUrl + "/role", HttpMethod.PUT,
                new HttpEntity<>(roleDTO), Role.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        Optional<Account> byId = accountsRepository.findById(account.getId());
        assertTrue(byId.isPresent());
        assertEquals(role.getId(), byId.get().getRole().getId());
    }

    @Test
    public void shouldNotAssignTwoAccountsToRoleWithSubRole() {
        Role role = new Role();
        role.setName("role3");
        role = roleRepository.save(role);

        Role subRole = new Role();
        subRole.setName("subRole1");
        subRole = roleRepository.save(subRole);
        roleRepository.addParent(role.getId(), subRole.getId());

        Account account = AccountsUtil.createAccount();
        accountsRepository.save(account);
        Account account2 = AccountsUtil.createAccount(11L);
        accountsRepository.save(account2);

        RoleDTO roleDTO = new RoleDTO()
                .id(role.getId())
                .name(role.getName())
                .profiles(Collections.emptyList())
                .accounts(List.of(
                        new IdAndEmailDTO().id(account.getId()),
                        new IdAndEmailDTO().id(account2.getId())));

        ResponseEntity<Role> response = restTemplate.exchange(baseUrl + "/role", HttpMethod.PUT,
                new HttpEntity<>(roleDTO), Role.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldDeleteAllAccountsFromRole() {
        Account account = AccountsUtil.createAccount();
        accountsRepository.save(account);
        Account account2 = AccountsUtil.createAccount(11L);
        accountsRepository.save(account2);

        Role role = new Role();
        role.setName("role4");
        role.setAccounts(Set.of(account, account2));
        role = roleRepository.save(role);

        RoleDTO roleDTO = modelMapper.map(role, RoleDTO.class);
        roleDTO.setProfiles(Collections.emptyList());
        roleDTO.setAccounts(Collections.emptyList());

        ResponseEntity<Role> response = restTemplate.exchange(baseUrl + "/role", HttpMethod.PUT,
                new HttpEntity<>(roleDTO), Role.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(0L, roleRepository.findOneById(roleDTO.getId()).getAccounts().size());
    }

    @Test
    public void shouldDeleteAllAccountsAndAddNewOneToRole() {
        Account account = AccountsUtil.createAccount();
        account = accountsRepository.save(account);
        Account account2 = AccountsUtil.createAccount(11L);
        account2 = accountsRepository.save(account2);

        Role role = new Role();
        role.setName("role5");
        role.setAccounts(Set.of(account, account2));
        role = roleRepository.save(role);

        RoleDTO roleDTO = modelMapper.map(role, RoleDTO.class);
        roleDTO.setProfiles(Collections.emptyList());
        roleDTO.setAccounts(List.of(new IdAndEmailDTO().id(account.getId())));

        ResponseEntity<Role> response = restTemplate.exchange(baseUrl + "/role", HttpMethod.PUT,
                new HttpEntity<>(roleDTO), Role.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1L, roleRepository.findOneById(roleDTO.getId()).getAccounts().size());
        assertEquals(roleDTO.getId(), roleRepository.findOneById(roleDTO.getId()).getAccounts().iterator().next().getRole().getId());
    }

    @Test
    public void shouldReturnNullWhenRoleDoesntHaveSupervisor() {
        Role role = new Role();
        role.setName("role6");
        role = roleRepository.save(role);

        Account account = AccountsUtil.createAccount();
        account.getPerson().setEmail("test");
        account.setRole(role);
        restTemplate.postForEntity(baseUrl + "/accounts", account, Account.class);

        ResponseEntity<Long> response = restTemplate.getForEntity(baseUrl + "/role/email/test/supervisor/id", Long.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    public void shouldReturnSupervisorIdWhenPresent() {
        Role supervisorRole = new Role();
        supervisorRole.setName("supervisor2");

        Role role = new Role();
        role.setName("role7");
        supervisorRole.setChildren(Set.of(roleRepository.save(role)));
        roleRepository.save(supervisorRole);
        Account supervisor = saveAccount(supervisorRole, "supervisor");
        saveAccount(role, "test");

        ResponseEntity<Long> response = restTemplate.getForEntity(baseUrl + "/role/email/test/supervisor/id", Long.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(supervisor.getId(), response.getBody());
    }

    private Account saveAccount(Role role, String test) {
        Account supervisor = new Account();
        supervisor.setRole(role);
        supervisor.setPerson(new Person());
        supervisor.getPerson().setEmail(test);
        supervisor.getPerson().setName("name");
        supervisor.getPerson().setSurname("surname");
        supervisor = accountsRepository.save(supervisor);
        return supervisor;
    }

    private Role addRoleWithParent(Role parent) {
        RoleDTO newRole = createNewRoleWithParent(parent.getId());
        return restTemplate.postForEntity(baseUrl + "/role", newRole, Role.class).getBody();
    }

    private RoleDTO createNewRoleWithParent(Long id) {
        RoleDTO newRole = modelMapper.map(role, RoleDTO.class);
        newRole.setId(null);
        newRole.setName("newName");
        newRole.setSupervisor(id);
        return newRole;
    }
}
