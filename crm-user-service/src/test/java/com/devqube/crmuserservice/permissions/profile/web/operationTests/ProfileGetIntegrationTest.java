package com.devqube.crmuserservice.permissions.profile.web.operationTests;

import com.devqube.crmuserservice.permissions.profile.web.ProfileControllerIntegrationTest;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileSaveDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
public class ProfileGetIntegrationTest extends ProfileControllerIntegrationTest {
    @Test
    public void shouldReturn404WhenNoProfileFound() {
        ResponseEntity<ProfileSaveDTO> response = restTemplate.getForEntity(baseUrl + "/profile/-100", ProfileSaveDTO.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldReturnProfileWhenFound() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");
        ResponseEntity<ProfileSaveDTO> response = restTemplate.getForEntity(baseUrl + "/profile/" + profile.getId(), ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(profile.getId(), response.getBody().getId());
    }

    @Test
    public void shouldReturnModulePermissionsWithProfile() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");
        ResponseEntity<ProfileSaveDTO> response = restTemplate.getForEntity(baseUrl + "/profile/" + profile.getId(), ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getModulePermissions());
        assertEquals(1L, response.getBody().getModulePermissions().size());
    }

    @Test
    public void shouldReturnModuleWithModulePermissions() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");
        ResponseEntity<ProfileSaveDTO> response = restTemplate.getForEntity(baseUrl + "/profile/" + profile.getId(), ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(module.getName(), response.getBody().getModulePermissions().get(0)
                                                         .getModule()
                                                         .getName());
    }

    @Test
    public void shouldReturnModuleCategoryWithModule() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");
        ResponseEntity<ProfileSaveDTO> response = restTemplate.getForEntity(baseUrl + "/profile/" + profile.getId(), ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(module.getCategory().getName(), response.getBody().getModulePermissions().get(0)
                                                                       .getModule()
                                                                       .getCategory()
                                                                       .getName());
    }

    @Test
    public void shouldReturnPermissionsWithModulePermissions() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");
        ResponseEntity<ProfileSaveDTO> response = restTemplate.getForEntity(baseUrl + "/profile/" + profile.getId(), ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getModulePermissions().iterator().next().getPermissions());
        assertEquals(1L, response.getBody().getModulePermissions().get(0).getPermissions().size());
    }

    @Test
    public void shouldReturnWebControlWithPermissions() {
        ProfileSaveDTO profile = saveProfileAndReturnIt("name");
        ResponseEntity<ProfileSaveDTO> response = restTemplate.getForEntity(baseUrl + "/profile/" + profile.getId(), ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getModulePermissions().get(0)
                                        .getPermissions().get(0)
                                        .getWebControl());
    }
}
