package com.devqube.crmuserservice.permissions.profile.web;

import com.devqube.crmuserservice.AbstractIntegrationTest;
import com.devqube.crmuserservice.accountConfiguration.AccountConfigurationRepository;
import com.devqube.crmuserservice.permissions.module.Module;
import com.devqube.crmuserservice.permissions.module.ModuleRepository;
import com.devqube.crmuserservice.permissions.moduleCategory.ModuleCategory;
import com.devqube.crmuserservice.permissions.moduleCategory.ModuleCategoryRepository;
import com.devqube.crmuserservice.permissions.modulePermission.ModulePermissionRepository;
import com.devqube.crmuserservice.permissions.permission.Permission;
import com.devqube.crmuserservice.permissions.permission.PermissionRepository;
import com.devqube.crmuserservice.permissions.profile.ProfileRepository;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileSaveDTO;
import com.devqube.crmuserservice.permissions.webControl.WebControlRepository;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static com.devqube.crmuserservice.permissions.profile.web.ProfileTestUtil.createFilledWebControl;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@Ignore
public class ProfileControllerIntegrationTest extends AbstractIntegrationTest {
    @Autowired
    protected ProfileRepository profileRepository;
    @Autowired
    protected ModulePermissionRepository modulePermissionRepository;
    @Autowired
    protected PermissionRepository permissionRepository;
    @Autowired
    protected ModuleRepository moduleRepository;
    @Autowired
    protected ModuleCategoryRepository moduleCategoryRepository;
    @Autowired
    protected WebControlRepository webControlRepository;
    @Autowired
    private AccountConfigurationRepository accountConfigurationRepository;


    protected Module module;

    @Before
    public void setUp() {
        this.profileRepository.deleteAll();
        this.moduleRepository.deleteAll();
        module = new Module();
        module.setName("module1");
        module.setWebControls(createFilledWebControl(module));
        ModuleCategory category = new ModuleCategory();
        category.setName("category");
        category.setModules(Collections.singleton(module));
        module.setCategory(category);
        moduleRepository.save(module);
        super.setUp();
    }

    protected ProfileSaveDTO createCorrectProfileSaveDTO() {
        ProfileSaveDTO profile = restTemplate.getForEntity(baseUrl + "/profile/save",
                ProfileSaveDTO.class).getBody();
        assertNotNull(profile);
        profile.setName("name");
        Permission permissionToSet = profile.getModulePermissions().get(0)
                                            .getPermissions().get(0);
        permissionToSet.setState(Permission.State.WRITE);
        profile.getModulePermissions().get(0).setPermissions(List.of(permissionToSet));
        return profile;
    }

    protected ProfileSaveDTO saveProfileAndReturnIt(String name) {
        ProfileSaveDTO profile = createCorrectProfileSaveDTO(name);
        return restTemplate.postForEntity(baseUrl + "/profile/save", profile, ProfileSaveDTO.class).getBody();
    }

    private ProfileSaveDTO createCorrectProfileSaveDTO(String name) {
        ProfileSaveDTO profile = restTemplate.getForEntity(baseUrl + "/profile/save",
                ProfileSaveDTO.class).getBody();
        assertNotNull(profile);
        profile.setName(name);
        Permission permissionToSet = profile.getModulePermissions().get(0)
                                            .getPermissions().get(0);
        permissionToSet.setState(Permission.State.WRITE);
        profile.getModulePermissions().get(0).setPermissions(List.of(permissionToSet));
        return profile;
    }
}
