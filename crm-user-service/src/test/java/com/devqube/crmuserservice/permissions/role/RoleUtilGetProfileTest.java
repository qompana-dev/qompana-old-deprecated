package com.devqube.crmuserservice.permissions.role;

import com.devqube.crmuserservice.permissions.module.Module;
import com.devqube.crmuserservice.permissions.modulePermission.ModulePermission;
import com.devqube.crmuserservice.permissions.permission.Permission;
import com.devqube.crmuserservice.permissions.profile.Profile;
import com.devqube.crmuserservice.permissions.webControl.WebControl;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class RoleUtilGetProfileTest {
    @Test
    public void shouldReturnNullWhenAllRolesDontHaveProfiles() {
        Role role = Role
                .builder()
                .id(1L)
                .name("parent")
                .children(Set.of(
                        Role
                                .builder()
                                .id(2L)
                                .name("child1")
                                .children(Set.of(
                                        Role
                                                .builder()
                                                .id(3L)
                                                .name("grandchild1")
                                                .build(),
                                        Role
                                                .builder()
                                                .id(4L)
                                                .name("grandchild2")
                                                .build()
                                ))
                                .build(),
                        Role
                                .builder()
                                .id(5L)
                                .name("child2")
                                .children(Set.of(
                                        Role
                                                .builder()
                                                .id(6L)
                                                .name("grandchild3")
                                                .children(Set.of(
                                                        Role
                                                                .builder()
                                                                .id(7L)
                                                                .name("grandgrandchild1")
                                                                .build()
                                                ))
                                                .build()
                                ))
                                .build()))
                .build();
        assertNull(RoleUtil.getProfile(role));
    }

    @Test
    public void shouldReturnProfileWhenOnlyOneProfile() {
        Profile profile = generateProfile("1", List.of(
                ModulePermission.builder().create(false).delete(true).edit(true).view(false).build(),
                ModulePermission.builder().create(false).delete(true).edit(false).view(true).build(),
                ModulePermission.builder().create(false).delete(true).edit(true).view(false).build()
        ), new Permission.State[][] {
                {Permission.State.WRITE, Permission.State.READ_ONLY, Permission.State.INVISIBLE},
                {Permission.State.WRITE, Permission.State.READ_ONLY, Permission.State.INVISIBLE},
                {Permission.State.WRITE, Permission.State.READ_ONLY, Permission.State.INVISIBLE}
        });
        Role role = Role
                .builder()
                .id(8L)
                .name("parent")
                .profiles(Set.of(profile))
                .build();
        assertEquals(profile, RoleUtil.getProfile(role));
    }

    @Test
    public void shouldSetCreateInModulePermissionProperly() {
        Role role = createRoleForModulePermissionTests();
        Profile profile = RoleUtil.getProfile(role);
        assertNotNull(profile);
        List<ModulePermission> modulePermissions = profile.getModulePermissions();
        assertFalse(modulePermissions.get(0).isCreate());
        assertTrue(modulePermissions.get(1).isCreate());
        assertTrue(modulePermissions.get(2).isCreate());
    }

    @Test
    public void shouldSetDeleteInModulePermissionProperly() {
        Role role = createRoleForModulePermissionTests();
        Profile profile = RoleUtil.getProfile(role);
        assertNotNull(profile);
        List<ModulePermission> modulePermissions = profile.getModulePermissions();
        assertTrue(modulePermissions.get(0).isDelete());
        assertTrue(modulePermissions.get(1).isDelete());
        assertFalse(modulePermissions.get(2).isDelete());
    }

    @Test
    public void shouldSetEditInModulePermissionProperly() {
        Role role = createRoleForModulePermissionTests();
        Profile profile = RoleUtil.getProfile(role);
        assertNotNull(profile);
        List<ModulePermission> modulePermissions = profile.getModulePermissions();
        assertTrue(modulePermissions.get(0).isEdit());
        assertFalse(modulePermissions.get(1).isEdit());
        assertTrue(modulePermissions.get(2).isEdit());
    }


    @Test
    public void shouldSetViewInModulePermissionProperly() {
        Role role = createRoleForModulePermissionTests();
        Profile profile = RoleUtil.getProfile(role);
        assertNotNull(profile);
        List<ModulePermission> modulePermissions = profile.getModulePermissions();
        assertTrue(modulePermissions.get(0).isView());
        assertFalse(modulePermissions.get(1).isView());
        assertTrue(modulePermissions.get(2).isView());
    }

    @Test
    public void shouldSetWriteStateWhenOneWrite() {
        Role role = createRoleForModulePermissionTests();
        Profile profile = RoleUtil.getProfile(role);
        assertNotNull(profile);
        assertEquals(Permission.State.WRITE, profile.getModulePermissions().get(0).getPermissions().get(0).getState());
    }

    @Test
    public void shouldSetWriteStateWhenTwoWrites() {
        Role role = createRoleForModulePermissionTests();
        Profile profile = RoleUtil.getProfile(role);
        assertNotNull(profile);
        assertEquals(Permission.State.WRITE, profile.getModulePermissions().get(0).getPermissions().get(1).getState());
    }

    @Test
    public void shouldSetWriteStateWhenThreeWrites() {
        Role role = createRoleForModulePermissionTests();
        Profile profile = RoleUtil.getProfile(role);
        assertNotNull(profile);
        assertEquals(Permission.State.WRITE, profile.getModulePermissions().get(0).getPermissions().get(2).getState());
    }

    @Test
    public void shouldSetReadOnlyStateWhenOneReadOnly() {
        Role role = createRoleForModulePermissionTests();
        Profile profile = RoleUtil.getProfile(role);
        assertNotNull(profile);
        assertEquals(Permission.State.READ_ONLY, profile.getModulePermissions().get(1).getPermissions().get(0).getState());
    }

    @Test
    public void shouldSetReadOnlyStateWhenTwoReadOnly() {
        Role role = createRoleForModulePermissionTests();
        Profile profile = RoleUtil.getProfile(role);
        assertNotNull(profile);
        assertEquals(Permission.State.READ_ONLY, profile.getModulePermissions().get(1).getPermissions().get(1).getState());
    }

    @Test
    public void shouldSetReadOnlyStateWhenThreeReadOnly() {
        Role role = createRoleForModulePermissionTests();
        Profile profile = RoleUtil.getProfile(role);
        assertNotNull(profile);
        assertEquals(Permission.State.READ_ONLY, profile.getModulePermissions().get(1).getPermissions().get(2).getState());
    }

    @Test
    public void shouldSetInvisibleStateWhenThreeInvisible() {
        Role role = createRoleForModulePermissionTests();
        Profile profile = RoleUtil.getProfile(role);
        assertNotNull(profile);
        assertEquals(Permission.State.INVISIBLE, profile.getModulePermissions().get(2).getPermissions().get(2).getState());
    }

    @Test
    public void shouldWorkWhenOrderOfModulePermissionsIsNotSame() {
        Role role = createRoleForModulePermissionTests();
        Profile firstProfile = role.getProfiles().iterator().next();
        List<ModulePermission> modulePermissionsBefore = firstProfile.getModulePermissions();
        ModulePermission elementToMove = modulePermissionsBefore.get(2);
        modulePermissionsBefore.set(2, modulePermissionsBefore.get(1));
        modulePermissionsBefore.set(1, elementToMove);
        Profile profile = RoleUtil.getProfile(role);
        assertNotNull(profile);
        assertEquals(Permission.State.INVISIBLE, profile.getModulePermissions().get(2).getPermissions().get(2).getState());
    }

    @Test
    public void shouldWorkWhenOrderOfPermissionsIsNotSame() {
        Role role = createRoleForModulePermissionTests();
        Profile firstProfile = role.getProfiles().iterator().next();
        List<Permission> permissionsBefore = firstProfile.getModulePermissions().get(2).getPermissions();
        Permission elementToMove = permissionsBefore.get(2);
        permissionsBefore.set(2, permissionsBefore.get(1));
        permissionsBefore.set(1, elementToMove);
        Profile profile = RoleUtil.getProfile(role);
        assertNotNull(profile);
        assertEquals(Permission.State.INVISIBLE, profile.getModulePermissions().get(2).getPermissions().get(2).getState());
    }

    private Role createRoleForModulePermissionTests() {
        return Role
                .builder()
                .id(9L)
                .name("parent")
                .profiles(Set.of(
                        generateProfile("1", List.of(
                                ModulePermission.builder().create(false).delete(false).edit(true).view(false).build(),
                                ModulePermission.builder().create(true).delete(true).edit(false).view(false).build(),
                                ModulePermission.builder().create(false).delete(false).edit(true).view(true).build()
                        ), new Permission.State[][] {
                                {Permission.State.READ_ONLY, Permission.State.WRITE, Permission.State.WRITE},
                                {Permission.State.INVISIBLE, Permission.State.INVISIBLE, Permission.State.READ_ONLY},
                                {Permission.State.WRITE, Permission.State.READ_ONLY, Permission.State.INVISIBLE}
                        }),
                        generateProfile("2", List.of(
                                ModulePermission.builder().create(false).delete(true).edit(false).view(true).build(),
                                ModulePermission.builder().create(true).delete(false).edit(false).view(false).build(),
                                ModulePermission.builder().create(true).delete(false).edit(true).view(false).build()
                        ), new Permission.State[][] {
                                {Permission.State.INVISIBLE, Permission.State.READ_ONLY, Permission.State.WRITE},
                                {Permission.State.READ_ONLY, Permission.State.READ_ONLY, Permission.State.READ_ONLY},
                                {Permission.State.WRITE, Permission.State.READ_ONLY, Permission.State.INVISIBLE}
                        }),
                        generateProfile("3", List.of(
                                ModulePermission.builder().create(false).delete(true).edit(true).view(true).build(),
                                ModulePermission.builder().create(true).delete(true).edit(false).view(false).build(),
                                ModulePermission.builder().create(false).delete(false).edit(true).view(false).build()
                        ), new Permission.State[][] {
                                {Permission.State.WRITE, Permission.State.WRITE, Permission.State.WRITE},
                                {Permission.State.INVISIBLE, Permission.State.READ_ONLY, Permission.State.READ_ONLY},
                                {Permission.State.WRITE, Permission.State.READ_ONLY, Permission.State.INVISIBLE}
                        })))
                .build();
    }

    private Profile generateProfile(String name, List<ModulePermission> modulePermissions, Permission.State[][] states) {
        return Profile
                .builder()
                .name(name)
                .modulePermissions(Arrays.asList(
                        ModulePermission
                                .builder()
                                .module(generateModule("1"))
                                .create(modulePermissions.get(0).isCreate())
                                .edit(modulePermissions.get(0).isEdit())
                                .delete(modulePermissions.get(0).isDelete())
                                .view(modulePermissions.get(0).isView())
                                .permissions(Arrays.asList(
                                        Permission
                                                .builder()
                                                .webControl(generateWebControl("1"))
                                                .state(states[0][0])
                                                .build(),
                                        Permission
                                                .builder()
                                                .webControl(generateWebControl("2"))
                                                .state(states[0][1])
                                                .build(),
                                        Permission
                                                .builder()
                                                .webControl(generateWebControl("3"))
                                                .state(states[0][2])
                                                .build()
                                ))
                                .build(),
                        ModulePermission
                                .builder()
                                .module(generateModule("2"))
                                .create(modulePermissions.get(1).isCreate())
                                .edit(modulePermissions.get(1).isEdit())
                                .delete(modulePermissions.get(1).isDelete())
                                .view(modulePermissions.get(1).isView())
                                .permissions(Arrays.asList(
                                        Permission
                                                .builder()
                                                .webControl(generateWebControl("1"))
                                                .state(states[1][0])
                                                .build(),
                                        Permission
                                                .builder()
                                                .webControl(generateWebControl("2"))
                                                .state(states[1][1])
                                                .build(),
                                        Permission
                                                .builder()
                                                .webControl(generateWebControl("3"))
                                                .state(states[1][2])
                                                .build()
                                ))
                                .build(),
                        ModulePermission
                                .builder()
                                .module(generateModule("3"))
                                .create(modulePermissions.get(2).isCreate())
                                .edit(modulePermissions.get(2).isEdit())
                                .delete(modulePermissions.get(2).isDelete())
                                .view(modulePermissions.get(2).isView())
                                .permissions(Arrays.asList(
                                        Permission
                                                .builder()
                                                .webControl(generateWebControl("1"))
                                                .state(states[2][0])
                                                .build(),
                                        Permission
                                                .builder()
                                                .webControl(generateWebControl("2"))
                                                .state(states[2][1])
                                                .build(),
                                        Permission
                                                .builder()
                                                .webControl(generateWebControl("3"))
                                                .state(states[2][2])
                                                .build()
                                ))
                                .build()))
                .build();
    }

    private WebControl generateWebControl(String s) {
        return WebControl
                .builder()
                .name(s)
                .build();
    }

    private Module generateModule(String name) {
        return Module
                .builder()
                .name(name)
                .build();
    }
}
