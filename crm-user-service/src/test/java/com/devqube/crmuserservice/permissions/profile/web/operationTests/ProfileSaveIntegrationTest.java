package com.devqube.crmuserservice.permissions.profile.web.operationTests;

import com.devqube.crmuserservice.permissions.module.Module;
import com.devqube.crmuserservice.permissions.modulePermission.ModulePermission;
import com.devqube.crmuserservice.permissions.permission.Permission;
import com.devqube.crmuserservice.permissions.profile.Profile;
import com.devqube.crmuserservice.permissions.profile.web.ProfileControllerIntegrationTest;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileSaveDTO;
import com.devqube.crmuserservice.permissions.webControl.WebControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.constraints.NotNull;

import static com.devqube.crmuserservice.permissions.profile.web.ProfileTestUtil.createFilledWebControl;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class ProfileSaveIntegrationTest extends ProfileControllerIntegrationTest {

    @Before
    public void setUp() {
        super.setUp();
        this.profileRepository.deleteAll();
        this.moduleRepository.deleteAll();
    }

    @Test
    public void shouldNotSaveProfileWithoutModulePermissions() {
        ProfileSaveDTO profile = new ProfileSaveDTO();
        profile.setName("someName");
        ResponseEntity<ProfileSaveDTO> response = restTemplate.postForEntity(baseUrl + "/profile/save",
                profile, ProfileSaveDTO.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldNotSaveProfileWithOnlyModulePermission() {
        Module module = new Module();
        module.setName("module1");
        moduleRepository.save(module);

        ProfileSaveDTO profile = restTemplate.getForEntity(baseUrl + "/profile/save",
                ProfileSaveDTO.class).getBody();

        ResponseEntity<ProfileSaveDTO> response = restTemplate.postForEntity(baseUrl + "/profile/save",
                profile, ProfileSaveDTO.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNull(profileRepository.findByName(module.getName()));
    }

    @Test
    public void shouldSaveProfileWithModulePermission() {
        Module module = new Module();
        module.setName("module1");
        module.setWebControls(createFilledWebControl(module));
        moduleRepository.save(module);

        ProfileSaveDTO profile = createCorrectProfileSaveDTO();
        ResponseEntity<ProfileSaveDTO> response = restTemplate.postForEntity(baseUrl + "/profile/save",
                profile, ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Profile savedProfile = profileRepository.findByName(profile.getName());
        assertNotNull(savedProfile);
        assertNotNull(savedProfile.getModulePermissions());
        assertFalse(savedProfile.getModulePermissions().isEmpty());
    }

    @Test
    public void shouldSaveModulePermissionWithModule() {
        Module module = new Module();
        module.setName("module1");
        module.setWebControls(createFilledWebControl(module));
        moduleRepository.save(module);

        ProfileSaveDTO profile = createCorrectProfileSaveDTO();
        restTemplate.postForEntity(baseUrl + "/profile/save", profile, ProfileSaveDTO.class);

        ModulePermission modulePermission = profileRepository.findByName(profile.getName()).getModulePermissions().get(0);
        assertNotNull(modulePermission.getModule());
        assertEquals(module.getName(), modulePermission.getModule().getName());
    }


    @Test
    public void shouldSavePermissionWithWebControl() {
        Module module = new Module();
        module.setName("module1");
        module.setWebControls(createFilledWebControl(module));
        moduleRepository.save(module);

        ProfileSaveDTO profile = createCorrectProfileSaveDTO();
        restTemplate.postForEntity(baseUrl + "/profile/save", profile, ProfileSaveDTO.class);

        Permission permission = profileRepository.findByName(profile.getName())
                .getModulePermissions().iterator().next()
                .getPermissions().get(0);
        assertNotNull(permission);
        assertNotNull(permission.getWebControl());
        assertEquals("name", permission.getWebControl().getName());
    }

    @Test
    public void shouldSavePermissionWithEnumsInWebControlCorrectly() {
        Module module = new Module();
        module.setName("module1");
        module.setWebControls(createFilledWebControl(module));
        moduleRepository.save(module);

        ProfileSaveDTO profile = createCorrectProfileSaveDTO();
        restTemplate.postForEntity(baseUrl + "/profile/save", profile, ProfileSaveDTO.class);

        WebControl webControl = profileRepository.findByName(profile.getName())
                .getModulePermissions().get(0)
                .getPermissions().get(0).getWebControl();
        assertNotNull(webControl);
        assertEquals(WebControl.ModuleOperation.VIEW, webControl.getDependsOn());
        assertEquals(Permission.State.WRITE, webControl.getDefaultState());
    }

    @Test
    public void shouldSaveModulePermissionButNotModifyTheModule() {
        Module module = new Module();
        module.setName("module1");
        module.setWebControls(createFilledWebControl(module));
        moduleRepository.save(module);

        ProfileSaveDTO profile = createCorrectProfileSaveDTO();
        profile.getModulePermissions().get(0).getModule().setName("module2");
        restTemplate.postForEntity(baseUrl + "/profile/save", profile, ProfileSaveDTO.class);

        Module savedModule = profileRepository.findByName(profile.getName())
                .getModulePermissions().iterator().next()
                .getModule();
        assertNotNull(savedModule);
        assertEquals(module.getName(), savedModule.getName());
    }

    @Test
    public void shouldSavePermissionButNotModifyTheWebControl() {
        Module module = new Module();
        module.setName("module1");
        module.setWebControls(createFilledWebControl(module));
        moduleRepository.save(module);

        ProfileSaveDTO profile = createCorrectProfileSaveDTO();
        @NotNull WebControl webControl = profile.getModulePermissions().get(0)
                .getPermissions().get(0)
                .getWebControl();
        String oldName = webControl.getName();
        webControl.setName("some new name");
        restTemplate.postForEntity(baseUrl + "/profile/save", profile, ProfileSaveDTO.class);

        WebControl savedWebControl = profileRepository.findByName(profile.getName())
                .getModulePermissions().get(0)
                .getPermissions().get(0)
                .getWebControl();
        assertNotNull(savedWebControl);
        assertEquals(oldName, savedWebControl.getName());
    }
}
