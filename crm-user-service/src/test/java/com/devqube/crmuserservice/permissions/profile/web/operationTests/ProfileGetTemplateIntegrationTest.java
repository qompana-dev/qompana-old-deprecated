package com.devqube.crmuserservice.permissions.profile.web.operationTests;

import com.devqube.crmshared.web.RestPageImpl;
import com.devqube.crmuserservice.AbstractIntegrationTest;
import com.devqube.crmuserservice.permissions.module.Module;
import com.devqube.crmuserservice.permissions.module.ModuleRepository;
import com.devqube.crmuserservice.permissions.modulePermission.ModulePermission;
import com.devqube.crmuserservice.permissions.permission.Permission;
import com.devqube.crmuserservice.permissions.profile.Profile;
import com.devqube.crmuserservice.permissions.profile.ProfileRepository;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileListDTO;
import com.devqube.crmuserservice.permissions.profile.web.dto.ProfileSaveDTO;
import com.devqube.crmuserservice.permissions.webControl.WebControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Iterator;

import static com.devqube.crmuserservice.permissions.profile.web.ProfileTestUtil.create5WebControls;
import static com.devqube.crmuserservice.permissions.profile.web.ProfileTestUtil.createFilledWebControl;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class ProfileGetTemplateIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private ModuleRepository moduleRepository;
    private Profile profile;

    @Before
    public void setUp() {
        super.setUp();
        profile = new Profile();
        profile.setName("name");
        this.profileRepository.deleteAll();
        this.moduleRepository.deleteAll();
    }

    @Test
    public void shouldReturnEmptyListOfProfiles() {
        ResponseEntity<RestPageImpl<ProfileListDTO>> response = restTemplate.exchange(baseUrl + "/profile",
                HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<ProfileListDTO>>() {
                });
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(0L, response.getBody().getNumberOfElements());
    }

    @Test
    public void shouldReturnOneProfileListDTO() {
        profileRepository.save(profile);
        ResponseEntity<RestPageImpl<ProfileListDTO>> response = restTemplate.exchange(baseUrl + "/profile",
                HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<ProfileListDTO>>() {
                });
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1L, response.getBody().getNumberOfElements());
    }

    @Test
    public void shouldReturnTwoProfileListDTOs() {
        profileRepository.save(profile);
        Profile newProfile = new Profile();
        newProfile.setName("newName");
        profileRepository.save(newProfile);

        ResponseEntity<RestPageImpl<ProfileListDTO>> response = restTemplate.exchange(baseUrl + "/profile",
                HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<ProfileListDTO>>() {
                });
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2L, response.getBody().getNumberOfElements());
    }

    @Test
    public void shouldReturnEmptyModulePermissionsWhenNoConfiguration() {
        ResponseEntity<ProfileSaveDTO> response = restTemplate.getForEntity(baseUrl + "/profile/save",
                ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getModulePermissions());
        assertTrue(response.getBody().getModulePermissions().isEmpty());
    }

    @Test
    public void shouldReturnOneModulePermissionWithoutPermissions() {
        Module module = new Module();
        module.setName("module1");
        moduleRepository.save(module);

        ResponseEntity<ProfileSaveDTO> response = restTemplate.getForEntity(baseUrl + "/profile/save",
                ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getModulePermissions());
        assertEquals(1L, response.getBody().getModulePermissions().size());
        ModulePermission firstPermission = response.getBody().getModulePermissions().get(0);
        assertEquals(module.getName(), firstPermission.getModule().getName());
        assertTrue(firstPermission.getPermissions().isEmpty());
    }

    @Test
    public void shouldReturnOneModulePermissionWithPermissions() {
        Module module = new Module();
        module.setName("module1");
        module.setWebControls(create5WebControls(0, 5, module));
        moduleRepository.save(module);

        ResponseEntity<ProfileSaveDTO> response = restTemplate.getForEntity(baseUrl + "/profile/save",
                ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getModulePermissions());
        assertEquals(1L, response.getBody().getModulePermissions().size());
        ModulePermission firstPermission = response.getBody().getModulePermissions().get(0);
        assertEquals(module.getName(), firstPermission.getModule().getName());
        assertEquals(5L, firstPermission.getPermissions().size());
    }

    @Test
    public void shouldReturnTwoModulePermissionsOneWithPermissionsAndOneWithout() {
        Module module = new Module();
        module.setName("module1");
        module.setWebControls(create5WebControls(0, 5, module));
        moduleRepository.save(module);

        Module module2 = new Module();
        module2.setName("module2");
        moduleRepository.save(module2);

        ResponseEntity<ProfileSaveDTO> response = restTemplate.getForEntity(baseUrl + "/profile/save",
                ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getModulePermissions());
        assertEquals(2L, response.getBody().getModulePermissions().size());
        Iterator<ModulePermission> iterator = response.getBody().getModulePermissions().iterator();
        ModulePermission firstPermission = iterator.next();
        ModulePermission secondPermission = iterator.next();

        //map is returned, so we don't know the order
        assertTrue(module2.getName().equals(firstPermission.getModule().getName())
                || module.getName().equals(firstPermission.getModule().getName()));

        assertTrue(module2.getName().equals(secondPermission.getModule().getName())
                || module.getName().equals(secondPermission.getModule().getName()));
        assertEquals(5L, firstPermission.getPermissions().size() + secondPermission.getPermissions().size());
    }

    @Test
    public void shouldReturnTwoModulePermissionsBothWithPermissions() {
        Module module = new Module();
        module.setName("module1");
        module.setWebControls(create5WebControls(0, 5, module));
        moduleRepository.save(module);

        Module module2 = new Module();
        module2.setName("module2");
        module2.setWebControls(create5WebControls(5, 10, module2));
        moduleRepository.save(module2);

        ResponseEntity<ProfileSaveDTO> response = restTemplate.getForEntity(baseUrl + "/profile/save",
                ProfileSaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getModulePermissions());
        assertEquals(2L, response.getBody().getModulePermissions().size());
        Iterator<ModulePermission> iterator = response.getBody().getModulePermissions().iterator();
        ModulePermission firstPermission = iterator.next();
        ModulePermission secondPermission = iterator.next();

        //map is returned, so we don't know the order
        assertTrue(module2.getName().equals(firstPermission.getModule().getName())
                || module.getName().equals(firstPermission.getModule().getName()));

        assertTrue(module2.getName().equals(secondPermission.getModule().getName())
                || module.getName().equals(secondPermission.getModule().getName()));

        assertEquals(5L, firstPermission.getPermissions().size());
        assertEquals(5L, secondPermission.getPermissions().size());
    }

    @Test
    public void shouldMapEnumsCorrectly() {
        Module module = new Module();
        module.setName("module1");
        module.setWebControls(createFilledWebControl(module));
        moduleRepository.save(module);

        ResponseEntity<ProfileSaveDTO> response = restTemplate.getForEntity(baseUrl + "/profile/save",
                ProfileSaveDTO.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getModulePermissions());
        assertEquals(1L, response.getBody().getModulePermissions().size());
        ModulePermission firstPermission = response.getBody().getModulePermissions().iterator().next();
        assertEquals(1L, firstPermission.getPermissions().size());
        Permission permission = firstPermission.getPermissions().iterator().next();
        assertNotNull(permission);
        assertNotNull(permission.getWebControl());
        assertEquals(WebControl.ModuleOperation.VIEW, permission.getWebControl().getDependsOn());
        assertEquals(Permission.State.WRITE, permission.getWebControl().getDefaultState());
    }


}
