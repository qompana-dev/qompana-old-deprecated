package com.devqube.crmuserservice.permissions.role;

import com.devqube.crmuserservice.permissions.profile.Profile;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RoleUtilExtractionTest {

    private List<Profile> profiles = new ArrayList<>() {{
        for (int i = 0; i < 10; i++) {
            Profile profile = new Profile();
            profile.setId((long) i);
            profile.setName("profile" + i);
            add(profile);
        }
    }};

    @Test
    public void shouldReturnEmptySetWhenRoleWithoutChildrenAndProfiles() {
        assertTrue(RoleUtil.extractAllProfiles(new Role()).isEmpty());
    }

    @Test
    public void shouldReturnEmptySetWhenRoleWithOneChildWithoutProfiles() {
        Role role = new Role();
        role.setChildren(Collections.singleton(new Role()));
        assertTrue(RoleUtil.extractAllProfiles(role).isEmpty());
    }

    @Test
    public void shouldReturnEmptySetWhenRoleWithChildAndGrandChildWithoutProfiles() {
        Role role = Role
                .builder()
                .name("role1")
                .children(Set.of(
                        Role
                                .builder()
                                .name("child")
                                .children(Set.of(new Role()))
                                .build()))
                .build();
        assertTrue(RoleUtil.extractAllProfiles(role).isEmpty());
    }

    @Test
    public void shouldReturnTwoProfilesWhenRoleHasOneProfileAndOneChildWithAnotherProfile() {
        Role role = Role
                .builder()
                .profiles(Collections.singleton(profiles.get(0)))
                .children(Collections.singleton(
                        Role
                                .builder()
                                .profiles(Collections.singleton(profiles.get(1)))
                                .build()))
                .build();
        Set<Profile> allProfiles = RoleUtil.extractAllProfiles(role);
        assertEquals(2, allProfiles.size());
        assertTrue(allProfiles.containsAll(Set.of(profiles.get(0), profiles.get(1))));
    }

    @Test
    public void shouldReturnFiveProfilesWhenRoleHasTwoProfilesAndOneChildWithThreeDifferentProfiles() {
        Role role = Role
                .builder()
                .profiles(Set.of(profiles.get(0), profiles.get(1)))
                .children(Collections.singleton(
                        Role
                                .builder()
                                .profiles(Set.of(profiles.get(2), profiles.get(3), profiles.get(4)))
                                .build()))
                .build();
        Set<Profile> allProfiles = RoleUtil.extractAllProfiles(role);
        assertEquals(5, allProfiles.size());
        assertTrue(allProfiles.containsAll(Set.of(profiles.get(0), profiles.get(1), profiles.get(2),
                profiles.get(3), profiles.get(4))));
    }

    @Test
    public void shouldReturnSevenProfilesWhenRoleHasThreeProfilesAndTwoChildrenWithTwoDifferentProfilesEach() {
        Role role = Role
                .builder()
                .id(2L)
                .name("parent")
                .profiles(Set.of(profiles.get(0), profiles.get(1), profiles.get(2)))
                .children(Set.of(
                        Role
                                .builder()
                                .id(3L)
                                .name("child1")
                                .profiles(Set.of(profiles.get(3), profiles.get(4)))
                                .build(),
                        Role
                                .builder()
                                .id(4L)
                                .name("child2")
                                .profiles(Set.of(profiles.get(5), profiles.get(6)))
                                .build()))
                .build();
        Set<Profile> allProfiles = RoleUtil.extractAllProfiles(role);
        assertEquals(7, allProfiles.size());
        assertTrue(allProfiles.containsAll(Set.of(profiles.get(0), profiles.get(1), profiles.get(2),
                profiles.get(3), profiles.get(4), profiles.get(5), profiles.get(6))));
    }

    @Test
    public void shouldReturnTenProfilesWhenThereAreALotOfChildren() {
        Role role = Role
                .builder()
                .id(1L)
                .name("parent")
                .profiles(Set.of(profiles.get(0), profiles.get(1), profiles.get(2)))
                .children(Set.of(
                        Role
                                .builder()
                                .id(7L)
                                .name("child1")
                                .profiles(Set.of(profiles.get(2), profiles.get(3)))
                                .children(Set.of(
                                        Role
                                                .builder()
                                                .id(2L)
                                                .name("grandchild1")
                                                .profiles(Set.of(profiles.get(3), profiles.get(4)))
                                                .build(),
                                        Role
                                                .builder()
                                                .id(3L)
                                                .name("grandchild2")
                                                .profiles(Set.of(profiles.get(4), profiles.get(5)))
                                                .build()
                                ))
                                .build(),
                        Role
                                .builder()
                                .id(4L)
                                .name("child2")
                                .profiles(Set.of(profiles.get(6), profiles.get(7)))
                                .children(Set.of(
                                        Role
                                                .builder()
                                                .id(5L)
                                                .name("grandchild3")
                                                .profiles(Set.of(profiles.get(6), profiles.get(8)))
                                                .children(Set.of(
                                                        Role
                                                                .builder()
                                                                .id(6L)
                                                                .name("grandgrandchild1")
                                                                .profiles(Set.of(profiles.get(8), profiles.get(9)))
                                                                .build()
                                                ))
                                                .build()
                                ))
                                .build()))
                .build();
        Set<Profile> allProfiles = RoleUtil.extractAllProfiles(role);
        assertEquals(10, allProfiles.size());
        assertTrue(allProfiles.containsAll(profiles));
    }
}
