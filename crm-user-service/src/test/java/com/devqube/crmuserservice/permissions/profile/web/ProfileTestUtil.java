package com.devqube.crmuserservice.permissions.profile.web;

import com.devqube.crmuserservice.permissions.module.Module;
import com.devqube.crmuserservice.permissions.permission.Permission;
import com.devqube.crmuserservice.permissions.webControl.WebControl;
import org.junit.Ignore;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Ignore
public class ProfileTestUtil {
    public static Set<WebControl> createFilledWebControl(Module module) {
        WebControl webControl = new WebControl();
        webControl.setModule(module);
        webControl.setName("name");
        webControl.setDefaultState(Permission.State.WRITE);
        webControl.setDependsOn(WebControl.ModuleOperation.VIEW);
        webControl.setFrontendId("frontentId");
        return Collections.singleton(webControl);
    }

    public static Set<WebControl> create5WebControls(int idFrom, int idTo, Module module) {
        Set<WebControl> webControls = new HashSet<>();
        for (int i = idFrom; i < idTo; i++) {
            WebControl control = new WebControl();
            control.setName("control" + i);
            control.setModule(module);
            webControls.add(control);
        }
        return webControls;
    }
}
