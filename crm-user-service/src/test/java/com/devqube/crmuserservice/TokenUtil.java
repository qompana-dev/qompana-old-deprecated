package com.devqube.crmuserservice;

import com.devqube.crmuserservice.token.model.Token;
import com.devqube.crmuserservice.token.model.TokenTypeEnum;

public class TokenUtil {
    public static Token createToken() {
        return Token.builder()
                .id(-1L)
                .type(TokenTypeEnum.RESET_PASSWORD)
                .token(com.devqube.crmuserservice.token.TokenUtil.generateTimeBasedUUID())
                .account(AccountsUtil.createAccount())
                .build();
    }

    public static Token createExpiredToken(TokenTypeEnum type) {
        return Token.builder()
                .id(-1L)
                .type(type)
                .token("127d115d-2d15-11e9-a2cb-37f6f40a22e3") //2019-02-10T10:20:13.926
                .account(AccountsUtil.createAccount())
                .build();
    }
}
