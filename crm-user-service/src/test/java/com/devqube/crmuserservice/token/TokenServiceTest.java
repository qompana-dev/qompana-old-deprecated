package com.devqube.crmuserservice.token;

import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmuserservice.AccountsUtil;
import com.devqube.crmuserservice.TokenUtil;
import com.devqube.crmuserservice.account.AccountsService;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.token.model.Token;
import com.devqube.crmuserservice.token.model.TokenTypeEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TokenServiceTest {
    @Mock
    private TokenRepository tokenRepository;
    @Mock
    private KafkaService kafkaService;
    @Mock
    private AccountsService accountsService;

    @InjectMocks
    private TokenService tokenService;


    @Test
    public void shouldSendEmailWithToken() throws KafkaSendMessageException {
        when(tokenRepository.findOneByTypeAndAccount(any(), any())).thenReturn(null);
        Account account = AccountsUtil.createAccount();
        tokenService.sendEmailWithToken(account, TokenTypeEnum.RESET_PASSWORD);
        verify(tokenRepository, times(0)).deleteById(any());
    }

    @Test
    public void shouldRemoveTokenIfExistWhenSendEmailWithToken() throws KafkaSendMessageException {
        Token token = TokenUtil.createToken();
        when(tokenRepository.findOneByTypeAndAccount(any(), any())).thenReturn(token);
        Account account = AccountsUtil.createAccount();
        tokenService.sendEmailWithToken(account, TokenTypeEnum.RESET_PASSWORD);
        verify(tokenRepository, times(1)).deleteById(any());
    }

    @Test
    public void shouldReturnValidTokenObject() throws BadRequestException, EntityNotFoundException {
        Token token = TokenUtil.createToken();
        token.setType(TokenTypeEnum.RESET_PASSWORD);
        when(tokenRepository.findOneByToken(any())).thenReturn(token);

        Token returnedToken = tokenService.getValidTokenObject(token.getToken(), TokenTypeEnum.RESET_PASSWORD);
        assertEquals(token.getId(), returnedToken.getId());
        assertEquals(token.getAccount(), returnedToken.getAccount());
        assertEquals(token.getType(), returnedToken.getType());
        assertEquals(token.getToken(), returnedToken.getToken());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowEntityNotFoundExceptionIfTokenIsNull() throws BadRequestException, EntityNotFoundException {
        Token token = TokenUtil.createToken();
        when(tokenRepository.findOneByToken(any())).thenReturn(null);
        tokenService.getValidTokenObject(token.getToken());
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestExceptionIfTokenIsExpired() throws BadRequestException, EntityNotFoundException {
        Token token = TokenUtil.createExpiredToken(TokenTypeEnum.RESET_PASSWORD);
        when(tokenRepository.findOneByToken(any())).thenReturn(token);
        tokenService.getValidTokenObject(token.getToken());
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestExceptionIfTokenNotExistInAvailableTypes() throws BadRequestException, EntityNotFoundException {
        Token token = com.devqube.crmuserservice.token.TokenUtil.generateNewAccountToken(Account.builder().build(), TokenTypeEnum.RESET_PASSWORD);

        when(tokenRepository.findOneByToken(token.getToken())).thenReturn(token);
        tokenService.getValidTokenObject(token.getToken(), TokenTypeEnum.SEND_ACTIVATION_CODE);
    }

    @Test
    public void shouldRemoveToken() {
        tokenService.removeToken(1L);
        verify(tokenRepository).deleteById(eq(1L));
    }
}
