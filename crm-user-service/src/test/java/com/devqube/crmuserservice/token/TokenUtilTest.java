package com.devqube.crmuserservice.token;

import com.devqube.crmuserservice.AccountsUtil;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.token.model.Token;
import com.devqube.crmuserservice.token.model.TokenTypeEnum;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class TokenUtilTest {
    @Test
    public void shouldReturnDifferentValues() {
        String token1 = TokenUtil.generateTimeBasedUUID();
        String token2 = TokenUtil.generateTimeBasedUUID();
        String token3 = TokenUtil.generateTimeBasedUUID();
        Assert.assertNotEquals(token1, token2, token3);
    }

    @Test
    public void shouldReturnCorrectDateFromUuid() {
        final int secondsDifference = 2;

        LocalDateTime now = LocalDateTime.now();
        String token1 = TokenUtil.generateTimeBasedUUID();
        LocalDateTime timeFromUUID = TokenUtil.getTimeFromUUID(token1);

        Assert.assertTrue(timeFromUUID.isBefore(now.plusSeconds(secondsDifference)));
        Assert.assertTrue(timeFromUUID.isAfter(now.minusSeconds(secondsDifference)));
    }

    @Test
    public void shouldReturnNullIfTokenIsNull() {
        Assert.assertNull(TokenUtil.getTimeFromUUID(null));
    }

    @Test
    public void shouldReturnAccountTokenObject() {
        Account account = AccountsUtil.createAccount();
        Token token = TokenUtil.generateNewAccountToken(account, TokenTypeEnum.RESET_PASSWORD);

        Assert.assertEquals(account, token.getAccount());
        Assert.assertEquals(TokenTypeEnum.RESET_PASSWORD, token.getType());
        Assert.assertEquals(36, token.getToken().length());

        Assert.assertTrue(LocalDateTime.now()
                .plus(TokenTypeEnum.RESET_PASSWORD.getExpiredAfterValue(), TokenTypeEnum.RESET_PASSWORD.getExpiredAfterUnit())
                .plusSeconds(1).isAfter(token.getExpired()));
        Assert.assertTrue(LocalDateTime.now()
                .plus(TokenTypeEnum.RESET_PASSWORD.getExpiredAfterValue(), TokenTypeEnum.RESET_PASSWORD.getExpiredAfterUnit())
                .minusSeconds(1).isBefore(token.getExpired()));
    }

    @Test
    public void shouldReturnTokenExpired() {
        String token = TokenUtil.generateTimeBasedUUID();
        TokenTypeEnum resetPasswordType = TokenTypeEnum.RESET_PASSWORD;

        LocalDateTime nowPlusExpiredTime = LocalDateTime.now()
                .plus(resetPasswordType.getExpiredAfterValue(), resetPasswordType.getExpiredAfterUnit())
                .plusSeconds(1);
        LocalDateTime nowBeforeExpiredTime = LocalDateTime.now()
                .plus(resetPasswordType.getExpiredAfterValue(), resetPasswordType.getExpiredAfterUnit())
                .minusSeconds(1);

        Assert.assertTrue(TokenUtil.tokenExpired(token, resetPasswordType, nowPlusExpiredTime));
        Assert.assertFalse(TokenUtil.tokenExpired(token, resetPasswordType, nowBeforeExpiredTime));

        Assert.assertTrue(TokenUtil.tokenExpired(token, TokenTypeEnum.RESET_PASSWORD, null));
        Assert.assertTrue(TokenUtil.tokenExpired(token, null, nowBeforeExpiredTime));
        Assert.assertTrue(TokenUtil.tokenExpired(null, TokenTypeEnum.RESET_PASSWORD, nowBeforeExpiredTime));
    }
}
