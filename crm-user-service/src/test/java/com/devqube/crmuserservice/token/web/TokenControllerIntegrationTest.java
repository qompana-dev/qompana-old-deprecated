package com.devqube.crmuserservice.token.web;

import com.devqube.crmuserservice.AccountsUtil;
import com.devqube.crmuserservice.CrmUserServiceApplication;
import com.devqube.crmuserservice.TokenUtil;
import com.devqube.crmuserservice.account.AccountsRepository;
import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.permissions.profile.ProfileRepository;
import com.devqube.crmuserservice.token.TokenRepository;
import com.devqube.crmuserservice.token.TokenService;
import com.devqube.crmuserservice.token.model.Token;
import com.devqube.crmuserservice.token.model.TokenTypeEnum;
import com.devqube.crmuserservice.token.web.dto.TokenResult;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CrmUserServiceApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
public class TokenControllerIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private AccountsRepository accountsRepository;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private ProfileRepository profileRepository;

    private Account account;
    private Token token;
    private String baseUrl;
    private TestRestTemplate restTemplate;

    @Before
    public void setUp() {
        account = AccountsUtil.createAccount();
        token = TokenUtil.createToken();
        baseUrl = "http://localhost:" + port;
        restTemplate = new TestRestTemplate();
    }

    @After
    public void cleanUp() {
        accountsRepository.deleteAll();
        tokenRepository.deleteAll();
        profileRepository.deleteAll();
    }

    @Test
    public void shouldReturnTokenResult() {
        Account savedAccount = accountsRepository.save(account);
        token.setAccount(savedAccount);
        tokenRepository.save(token);
        ResponseEntity<TokenResult> response = restTemplate.getForEntity(baseUrl + "/token/" + token.getToken(), TokenResult.class);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(response.getBody());
        assertEquals(response.getBody().getToken(), token.getToken());
        assertEquals(response.getBody().getType(), token.getType());
    }

    @Test
    public void shouldReturnTokenNotFound() {
        ResponseEntity<TokenResult> response = restTemplate.getForEntity(baseUrl + "/token/" + token.getToken(), TokenResult.class);
        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void shouldReturnBadRequestIfTokenExpired() {
        Account savedAccount = accountsRepository.save(account);
        Token expiredToken = TokenUtil.createExpiredToken(TokenTypeEnum.RESET_PASSWORD);
        expiredToken.setAccount(savedAccount);
        tokenRepository.save(expiredToken);
        ResponseEntity<TokenResult> response = restTemplate.getForEntity(baseUrl + "/token/" + expiredToken.getToken(), TokenResult.class);
        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }


    @Test
    public void shouldCleanOldTokens() {
        Account account = AccountsUtil.createAccount();
        Account savedAccount = accountsRepository.save(account);

        Token tokenToSaveNowBeforeExpired = com.devqube.crmuserservice.token.TokenUtil.generateNewAccountToken(savedAccount, TokenTypeEnum.RESET_PASSWORD);
        tokenToSaveNowBeforeExpired.setExpired(LocalDateTime.now().plusMinutes(1));
        Token tokenToSaveNowAfterExpired = com.devqube.crmuserservice.token.TokenUtil.generateNewAccountToken(savedAccount, TokenTypeEnum.RESET_PASSWORD);
        tokenToSaveNowAfterExpired.setExpired(LocalDateTime.now().minusMinutes(1));

        Token tokenNowBeforeExpired = tokenRepository.save(tokenToSaveNowBeforeExpired);
        Token tokenNowAfterExpired = tokenRepository.save(tokenToSaveNowAfterExpired);


        tokenService.deleteOldTokens();

        List<Token> allTokens = tokenRepository.findAll();

        assertFalse(allTokens.isEmpty());
        assertTrue(allTokens.stream().noneMatch(c -> c.getId().equals(tokenNowAfterExpired.getId())));
        assertTrue(allTokens.stream().anyMatch(c -> c.getId().equals(tokenNowBeforeExpired.getId())));
    }
}
