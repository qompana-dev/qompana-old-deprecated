package com.devqube.crmuserservice;

import com.devqube.crmuserservice.account.model.Account;
import com.devqube.crmuserservice.account.model.Person;
import com.devqube.crmuserservice.permissions.role.Role;

import java.util.UUID;

public class AccountsUtil {
    public static Account createAccount(Long id) {
        return Account.builder()
                .id(id)
                .activated(true)
                .person(createPerson(id))
                .role(getRole())
                .version(1L)
                .deleted(false)
                .build();
    }

    public static Account createAccount() {
        return createAccount(10L);
    }

    private static Person createPerson(Long id) {
        return Person.builder()
                .id(id)
                .name("name")
                .surname("surname")
                .phone("12345123451")
                .password("12345")
                .email(id + "email@devqube.com")
                .build();
    }

    public static Role getRole() {
        return Role.builder().id(1L).name(UUID.randomUUID().toString()).build();
    }
    public static Role getRoleWithoutId() {
        return Role.builder().name(UUID.randomUUID().toString()).build();
    }
}
