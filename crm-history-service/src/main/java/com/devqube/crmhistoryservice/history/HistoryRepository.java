package com.devqube.crmhistoryservice.history;

import com.devqube.crmhistoryservice.history.model.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryRepository extends JpaRepository<History, Long>, JpaSpecificationExecutor<History> {

    List<History> findAllByObjectIdAndClassRefAndVersion(String objectId, String classRef, Long version);

    List<History> findAllByObjectIdAndClassRef(String objectId, String classRef);

    @Query("select max(h.version) from History h where h.objectId = :objectId and h.classRef = :classRef")
    Long findMaxVersion(@Param("objectId") String objectId, @Param("classRef") String classRef);

}
