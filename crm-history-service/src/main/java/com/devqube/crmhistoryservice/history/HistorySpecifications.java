package com.devqube.crmhistoryservice.history;

import com.devqube.crmhistoryservice.history.model.History;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public class HistorySpecifications {

    public static Specification<History> findByObjectId(String objectId) {
        return (root, query, cb) ->
                cb.equal(root.get("objectId"), objectId);
    }

    public static Specification<History> findByObjectId(List<String> objectIds) {
        return (root, query, cb) ->
                root.get("objectId").in(objectIds);
    }

    public static Specification<History> findByClassRef(String classRef) {
        return (root, query, cb) ->
                cb.equal(root.get("classRef"), classRef);
    }

    public static Specification<History> findByFieldNameValue(String value) {
        return (root, query, cb) ->
                cb.equal(root.get("fieldName"), value);
    }

    public static Specification<History> findByNewValueValue(String value) {
        return (root, query, cb) ->
                cb.equal(root.get("newValue"), value);
    }

}
