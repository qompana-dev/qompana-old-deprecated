package com.devqube.crmhistoryservice.history.web;

import com.devqube.crmhistoryservice.history.model.History;
import com.devqube.crmhistoryservice.history.web.dto.ShortHistoryChangeDto;
import com.devqube.crmhistoryservice.history.web.dto.ShortHistoryDto;
import com.devqube.crmshared.history.dto.ObjectHistoryType;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.devqube.crmhistoryservice.history.HistoryService.getLastValueForField;

public class HistoryDtoMapper {

    public static List<ShortHistoryDto> toShortHistoryDtoList(Map<LocalDateTime, List<History>> timeMap, Map<Long, List<History>> versionMap) {
        List<ShortHistoryDto> history = new ArrayList<>();

        timeMap.forEach((k, v) -> {
            if (v.size() > 0) {
                String email = v.get(0).getEmail();
                String objectType = Arrays.stream(ObjectHistoryType.values())
                        .filter(e -> e.getClassRef().equals(v.get(0).getClassRef())).findFirst().get().name();
                List<ShortHistoryChangeDto> changes = toShortHistoryChangeDtoList(v, versionMap);
                if (changes.size() > 0) {
                    history.add(new ShortHistoryDto()
                            .setEmail(email)
                            .setModifiedTime(k)
                            .setObjectType(objectType)
                            .setChanges(changes));
                }
            }
        });

        history.sort(Comparator.comparing(ShortHistoryDto::getModifiedTime));
        return history;
    }

    private static List<ShortHistoryChangeDto> toShortHistoryChangeDtoList(List<History> historyList, Map<Long, List<History>> versionMap) {
        return historyList.stream()
                .filter(c -> !(c.getVersion().equals(1L) &&
                        (c.getNewValue().equals("") || c.getNewValue().equals("[]"))))
                .map(history -> {
                    String oldValue = getLastValueForField(versionMap, history.getVersion(), history.getFieldName());
                    return toShortHistoryChangeDto(history, oldValue);
                })
                .collect(Collectors.toList());
    }

    private static ShortHistoryChangeDto toShortHistoryChangeDto(History history, String oldValue) {
        if (history == null) {
            return null;
        }

        return new ShortHistoryChangeDto()
                .setId(history.getId())
                .setFieldName(history.getFieldName())
                .setOldValue(oldValue)
                .setNewValue(history.getNewValue())
                .setVersion(history.getVersion())
                .setDeleted(history.getDeleted());
    }

}
