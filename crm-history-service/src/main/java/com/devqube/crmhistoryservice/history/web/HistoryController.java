package com.devqube.crmhistoryservice.history.web;


import com.devqube.crmhistoryservice.history.HistoryService;
import com.devqube.crmhistoryservice.history.web.dto.HistoryDto;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.access.annotation.AuthControllerCase;
import com.devqube.crmshared.access.annotation.AuthControllerType;
import com.devqube.crmshared.history.dto.ObjectHistoryType;
import com.devqube.crmshared.search.CrmObjectType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

import static com.devqube.crmshared.history.dto.ObjectHistoryType.*;

@RestController
@Slf4j
public class HistoryController implements HistoryApi {

    private final HistoryService historyService;

    public HistoryController(HistoryService historyService) {
        this.historyService = historyService;
    }

    @Override
    @AuthController(actionFrontendId = "accessDenied", controllerCase = {
            @AuthControllerCase(type = "OPPORTUNITY", readFrontendId = "OpportunityDetailsComponent.history"),
            @AuthControllerCase(type = "LEAD", readFrontendId = "LeadDetailsComponent.history"),
            @AuthControllerCase(type = "CUSTOMER", readFrontendId = "CustomerDetailsComponent.history"),
            @AuthControllerCase(type = "CONTACT", readFrontendId = "ContactDetailsComponent.history")
    })
    public HttpEntity<List<?>> getObjectChanges(@AuthControllerType ObjectHistoryType type, String id) {
        if (type.equals(LEAD)) {
            return new ResponseEntity<>(historyService.getTransactionalHistory(id, "lead", CrmObjectType.lead, LEAD), HttpStatus.OK);
        } else if (type.equals(CUSTOMER)) {
            return new ResponseEntity<>(historyService.getTransactionalHistory(id, "customer", CrmObjectType.customer, CUSTOMER), HttpStatus.OK);
        } else if (type.equals(CONTACT)) {
            return new ResponseEntity<>(historyService.getTransactionalHistory(id, "contact", CrmObjectType.contact, CONTACT), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(historyService.getObjectChanges(id, type), HttpStatus.OK);
        }
    }

    @Override
    @AuthController(actionFrontendId = "accessDenied")
    public ResponseEntity<Set<HistoryDto>> getObjectVersion(ObjectHistoryType type, String id, Long version) {
        return new ResponseEntity<>(historyService.getObjectVersion(id, type, version), HttpStatus.OK);
    }

    @Override
    @AuthController(actionFrontendId = "accessDenied")
    public ResponseEntity<Set<HistoryDto>> getObjectLastVersion(ObjectHistoryType type, String id) {
        return new ResponseEntity<>(historyService.getObjectLastVersion(id, type), HttpStatus.OK);
    }
}
