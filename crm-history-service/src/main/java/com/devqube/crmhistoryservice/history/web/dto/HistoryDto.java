package com.devqube.crmhistoryservice.history.web.dto;

import com.devqube.crmhistoryservice.history.model.History;
import lombok.*;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class HistoryDto {
    private Long id;
    @EqualsAndHashCode.Include
    private String fieldName;
    private String value;
    private String email;
    private LocalDateTime modifiedTime;
    @EqualsAndHashCode.Include
    private Boolean deleted;

    public HistoryDto(History history) {
        this.id = history.getId();
        this.fieldName = history.getFieldName();
        this.value = history.getNewValue();
        this.email = history.getEmail();
        this.modifiedTime = history.getModifiedTime();
        this.deleted = history.getDeleted();
    }
}
