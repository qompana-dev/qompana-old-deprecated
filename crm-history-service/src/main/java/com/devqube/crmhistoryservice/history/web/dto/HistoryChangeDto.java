package com.devqube.crmhistoryservice.history.web.dto;

import com.devqube.crmhistoryservice.history.model.History;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HistoryChangeDto {
    private Long id;
    private String fieldName;
    private String oldValue;
    private String newValue;
    private String email;
    private LocalDateTime modifiedTime;
    private Long version;
    private Boolean deleted;

    public HistoryChangeDto(History history, String oldValue) {
        this.id = history.getId();
        this.fieldName = history.getFieldName();
        this.oldValue = oldValue;
        this.newValue = history.getNewValue();
        this.email = history.getEmail();
        this.modifiedTime = history.getModifiedTime();
        this.version = history.getVersion();
        this.deleted = history.getDeleted();
    }
}
