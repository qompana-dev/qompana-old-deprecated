package com.devqube.crmhistoryservice.history.web;

import com.devqube.crmhistoryservice.history.web.dto.HistoryDto;
import com.devqube.crmshared.history.dto.ObjectHistoryType;
import io.swagger.annotations.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Validated
@Api(value = "History")
public interface HistoryApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "get object change history", nickname = "getObjectChanges", response = Object.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "object change history", response = Object.class, responseContainer = "List")})
    @RequestMapping(value = "/history/type/{type}/id/{id}/changes",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default HttpEntity<List<?>> getObjectChanges(@ApiParam(value = "The object type", required = true, defaultValue = "null") @PathVariable("type") ObjectHistoryType type, @ApiParam(value = "The object ID", required = true) @PathVariable("id") String id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get last object version", nickname = "getObjectLastVersion", response = com.devqube.crmhistoryservice.history.web.dto.HistoryDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "object fields in last version", response = com.devqube.crmhistoryservice.history.web.dto.HistoryDto.class, responseContainer = "List")})
    @RequestMapping(value = "/history/type/{type}/id/{id}/last-version",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Set<HistoryDto>> getObjectLastVersion(@ApiParam(value = "The object type", required = true, defaultValue = "null") @PathVariable("type") ObjectHistoryType type, @ApiParam(value = "The object ID", required = true) @PathVariable("id") String id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get object in version", nickname = "getObjectVersion", response = com.devqube.crmhistoryservice.history.web.dto.HistoryDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "object fields in specific version", response = com.devqube.crmhistoryservice.history.web.dto.HistoryDto.class, responseContainer = "List")})
    @RequestMapping(value = "/history/type/{type}/id/{id}/version/{version}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Set<com.devqube.crmhistoryservice.history.web.dto.HistoryDto>> getObjectVersion(@ApiParam(value = "The object type", required = true, defaultValue = "null") @PathVariable("type") ObjectHistoryType type, @ApiParam(value = "The object ID", required = true) @PathVariable("id") String id, @ApiParam(value = "The version", required = true) @PathVariable("version") Long version) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
