package com.devqube.crmhistoryservice.history.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class History {
    @Id
    @SequenceGenerator(name = "history_seq", sequenceName = "history_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "history_seq")
    private Long id;
    @NotNull
    private String classRef;
    @NotNull
    private String objectId;
    @NotNull
    private String email;
    private String newValue;
    private String fieldName;
    @NotNull
    private LocalDateTime modifiedTime;
    @NotNull
    private Long version;
    @NotNull
    private Boolean deleted;
}
