package com.devqube.crmhistoryservice.history;

import com.devqube.crmhistoryservice.history.model.History;
import com.devqube.crmhistoryservice.history.web.HistoryDtoMapper;
import com.devqube.crmhistoryservice.history.web.dto.HistoryChangeDto;
import com.devqube.crmhistoryservice.history.web.dto.HistoryDto;
import com.devqube.crmhistoryservice.history.web.dto.ShortHistoryDto;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.history.dto.ObjectHistoryType;
import com.devqube.crmshared.search.CrmObjectType;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static com.devqube.crmhistoryservice.history.HistorySpecifications.*;
import static com.devqube.crmshared.history.dto.ObjectHistoryType.*;

@Service
@RequiredArgsConstructor
public class HistoryService {

    private final HistoryRepository historyRepository;
    private final AssociationClient associationClient;

    public List<HistoryChangeDto> getObjectChanges(String objectId, ObjectHistoryType type) {
        List<History> all = historyRepository.findAllByObjectIdAndClassRef(objectId, type.getClassRef());
        Map<Long, List<History>> versionMap = new HashMap<>();
        all.forEach(history -> {
            versionMap.computeIfAbsent(history.getVersion(), k -> new ArrayList<>());
            versionMap.get(history.getVersion()).add(history);
        });

        List<HistoryChangeDto> result = new ArrayList<>();
        versionMap.forEach((key, value) -> result.addAll(getObjectChangesByVersion(versionMap, key)));
        return result.stream().filter(c -> !(c.getVersion().equals(1L) &&
                (c.getNewValue().equals("") || c.getNewValue().equals("[]")))).collect(Collectors.toList());
    }

    public List<ShortHistoryDto> getTransactionalHistory(String id, String fieldName, CrmObjectType crmObjectType, ObjectHistoryType objectHistoryType) {
        List<String> expensesObjectId = historyRepository.findAll(Specification.where(
                findByClassRef(EXPENSE.getClassRef()))
                .and(findByFieldNameValue(fieldName)
                        .and(findByNewValueValue(id)))).stream()
                .map(History::getObjectId)
                .collect(Collectors.toList());

        List<String> tasksObjectId = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(crmObjectType,
                CrmObjectType.activity, Long.parseLong(id)).stream()
                .map(AssociationDto::getDestinationId)
                .map(String::valueOf)
                .collect(Collectors.toList());

        Specification<History> historySpec = buildShortHistorySpec(id, objectHistoryType, expensesObjectId, tasksObjectId);

        List<History> historyList = historyRepository.findAll(historySpec, Sort.by("modifiedTime").ascending());

        historyList.forEach(history -> {
            LocalDateTime modifiedTime = history.getModifiedTime().truncatedTo(ChronoUnit.SECONDS);
            history.setModifiedTime(modifiedTime);
        });

        Map<Long, List<History>> versionMap = new HashMap<>();
        historyList.forEach(history -> {
            versionMap.computeIfAbsent(history.getVersion(), k -> new ArrayList<>());
            versionMap.get(history.getVersion()).add(history);
        });

        Map<LocalDateTime, List<History>> timeMap = new HashMap<>();
        historyList.forEach(history -> {
            timeMap.computeIfAbsent(history.getModifiedTime(), k -> new ArrayList<>());
            timeMap.get(history.getModifiedTime()).add(history);
        });

        return HistoryDtoMapper.toShortHistoryDtoList(timeMap, versionMap);
    }

    public static String getLastValueForField(Map<Long, List<History>> versionMap, Long currentVersion, String field) {
        for (Long i = currentVersion - 1; i > 0; i--) {
            Optional<History> first = versionMap.get(i).stream().filter(history -> history.getFieldName().equals(field)).findFirst();
            if (first.isPresent()) {
                return first.get().getNewValue();
            }
        }
        return "";
    }

    private Specification<History> buildShortHistorySpec(String id, ObjectHistoryType objectHistoryType, List<String> expensesObjectId, List<String> tasksObjectId) {
        Specification<History> spec = Specification.where(
                findByObjectId(id)).and(findByClassRef(objectHistoryType.getClassRef()));

        if (expensesObjectId.size() > 0) {
            Specification<History> expensesSpec = Specification.where(Specification.where(
                    findByClassRef(EXPENSE.getClassRef()).and(findByObjectId(expensesObjectId))));
            spec = spec.or(expensesSpec);
        }

        if (tasksObjectId.size() > 0) {
            Specification<History> tasksSpec = Specification.where(
                    findByClassRef(TASK.getClassRef()).and(findByObjectId(tasksObjectId)));
            spec = spec.or(tasksSpec);
        }

        return spec;
    }

    private List<HistoryChangeDto> getObjectChangesByVersion(Map<Long, List<History>> versionMap, Long currentVersion) {
        List<History> histories = versionMap.get(currentVersion);
        return histories.stream()
                .map(history -> new HistoryChangeDto(history, getLastValueForField(versionMap, currentVersion, history.getFieldName())))
                .collect(Collectors.toList());
    }

    public Set<HistoryDto> getObjectLastVersion(String objectId, ObjectHistoryType type) {
        return getLastVersion(type.getClassRef(), objectId).stream().map(HistoryDto::new).collect(Collectors.toSet());
    }

    public Set<HistoryDto> getObjectVersion(String objectId, ObjectHistoryType type, Long version) {
        return getVersion(type.getClassRef(), objectId, version).stream().map(HistoryDto::new).collect(Collectors.toSet());
    }

    public void saveObjectChangeHistory(String classRef, String objectId, String json, LocalDateTime modifiedTime, String email) {
        if (json != null && !json.equals("")) {
            JSONObject jsonObject = new JSONObject(json);
            Long currentVersion = getCurrentVersion(classRef, objectId);
            Set<String> keys = new HashSet<>();
            jsonObject.keys().forEachRemaining(keys::add);
            List<History> newValues = keys.stream()
                    .map(key -> toHistory(currentVersion, key, jsonObject, classRef, objectId, modifiedTime, email))
                    .collect(Collectors.toList());

            List<History> lastVersion = getLastVersion(classRef, objectId);
            List<History> toSave = getValuesToSave(lastVersion, newValues);
            historyRepository.saveAll(toSave);
        } else { // object is deleted
            saveObjectDeleted(classRef, objectId, modifiedTime, email);
        }
    }

    private void saveObjectDeleted(String classRef, String objectId, LocalDateTime modifiedTime, String email) {
        Long currentVersion = getCurrentVersion(classRef, objectId);
        History history = new History();
        history.setClassRef(classRef);
        history.setObjectId(objectId);
        history.setEmail(email);
        history.setModifiedTime(modifiedTime);
        history.setVersion(currentVersion);
        history.setDeleted(true);
        historyRepository.save(history);
    }

    private List<History> getValuesToSave(List<History> lastVersion, List<History> currentVersion) {
        Map<String, History> lastVersionMap = new HashMap<>();
        lastVersion.forEach(c -> lastVersionMap.put(c.getFieldName(), c));
        return currentVersion.stream()
                .filter(history -> lastVersionMap.get(history.getFieldName()) == null || !lastVersionMap.get(history.getFieldName()).getNewValue().equals(history.getNewValue()))
                .collect(Collectors.toList());
    }

    private List<History> getLastVersion(String classRef, String objectId) {
        Long currentVersion = getCurrentVersion(classRef, objectId);
        return getVersion(classRef, objectId, currentVersion);
    }

    private List<History> getVersion(String classRef, String objectId, Long version) {
        Map<String, History> valuesMap = new HashMap<>();
        for (Long i = 1L; i <= version; i++) {
            historyRepository.findAllByObjectIdAndClassRefAndVersion(objectId, classRef, i)
                    .forEach(history -> valuesMap.put(history.getFieldName(), history));
        }
        List<History> result = new ArrayList<>();
        valuesMap.forEach((key, value) -> {
            result.add(value);
        });
        return result;
    }

    private Long getCurrentVersion(String classRef, String objectId) {
        Long maxVersion = historyRepository.findMaxVersion(objectId, classRef);
        return (maxVersion == null) ? 1L : maxVersion + 1;
    }

    private History toHistory(Long currentVersion, String key, JSONObject jsonObject, String classRef, String objectId, LocalDateTime modifiedTime, String email) {
        Object o = jsonObject.get(key);
        if (o == null) {
            o = "";
        }
        History history = new History();
        history.setClassRef(classRef);
        history.setObjectId(objectId);
        history.setEmail(email);
        history.setNewValue(o.toString());
        history.setFieldName(key);
        history.setModifiedTime(modifiedTime);
        history.setVersion(currentVersion);
        history.setDeleted(false);
        return history;
    }

}
