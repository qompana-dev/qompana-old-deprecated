package com.devqube.crmhistoryservice.history.web.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ShortHistoryChangeDto {

    private Long id;
    private String fieldName;
    private String oldValue;
    private String newValue;
    private Long version;
    private Boolean deleted;

}
