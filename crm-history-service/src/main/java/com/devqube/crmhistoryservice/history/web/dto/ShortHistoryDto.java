package com.devqube.crmhistoryservice.history.web.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Accessors(chain = true)
public class ShortHistoryDto {

    private String email;
    private LocalDateTime modifiedTime;
    private String objectType;
    private List<ShortHistoryChangeDto> changes;

}
