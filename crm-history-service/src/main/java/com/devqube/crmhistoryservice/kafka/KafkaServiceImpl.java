package com.devqube.crmhistoryservice.kafka;

import com.devqube.crmhistoryservice.history.HistoryService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.kafka.AbstractKafkaService;
import com.devqube.crmshared.kafka.ServiceKafkaAddr;
import com.devqube.crmshared.kafka.annotation.KafkaReceiver;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.search.CrmObjectType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class KafkaServiceImpl extends AbstractKafkaService {

    @Value("${ignore.kafka:false}")
    private boolean ignoreKafka;

    private final HistoryService historyService;

    public KafkaServiceImpl(HistoryService historyService) {
        this.historyService = historyService;
    }

    @KafkaListener(topics = ServiceKafkaAddr.CRM_HISTORY_SERVICE)
    public void processMessage(String content) {
        super.processMessage(content);
    }

    @KafkaReceiver(from = KafkaMsgType.HISTORY_SAVE_CHANGES)
    public void removeAssociationsAndNotes(KafkaMessage kafkaMessage) {
        log.info(kafkaMessage.getDestination().name());
        try {
            String classRef = kafkaMessage.getValueByKey("classRef").orElseThrow(BadRequestException::new);
            String id = kafkaMessage.getValueByKey("id").orElseThrow(BadRequestException::new);
            String json = kafkaMessage.getValueByKey("json").orElseThrow(BadRequestException::new);
            String modifiedTime = kafkaMessage.getValueByKey("modifiedTime").orElseThrow(BadRequestException::new);
            String email = kafkaMessage.getValueByKey("email").orElseThrow(BadRequestException::new);

            historyService.saveObjectChangeHistory(classRef, id, json, LocalDateTime.parse(modifiedTime), email);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receiverNotFound(KafkaMessage kafkaMessage) {
        log.info("receiverNotFound" + kafkaMessage.getDestination().name());
    }

    @Override
    public boolean isKafkaIgnored() {
        return ignoreKafka;
    }
}
