package com.devqube.crmhistoryservice;

import com.devqube.crmshared.license.LicenseCheck;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

@SpringBootApplication
@EnableEurekaClient
@Import({LicenseCheck.class})
@EnableFeignClients(basePackages = {"com.devqube.crmhistoryservice", "com.devqube.crmshared.association"})
public class CrmHistoryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrmHistoryServiceApplication.class, args);
	}

}
