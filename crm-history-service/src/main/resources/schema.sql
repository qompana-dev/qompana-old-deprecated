create TABLE history
(
    id                  bigint PRIMARY KEY,
    class_ref           varchar(200) not null,
    object_id           varchar(200) not null,
    email               varchar(200) not null,
    new_value           text,
    field_name          varchar(200),
    modified_time       TIMESTAMP not null,
    version             bigint not null,
    deleted             boolean default false,
    constraint deleted_check
        check ((field_name is not null and new_value is not null and deleted = false) or deleted = true)
);
create sequence history_seq start 1;
