package com.devqube.crmhistoryservice.history;

import com.devqube.crmhistoryservice.AbstractIntegrationTest;
import com.devqube.crmhistoryservice.history.model.History;
import com.devqube.crmhistoryservice.history.web.dto.HistoryChangeDto;
import com.devqube.crmhistoryservice.history.web.dto.HistoryDto;
import com.devqube.crmshared.history.dto.ObjectHistoryType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ActiveProfiles({"test"})
public class HistoryServiceIntegrationTest extends AbstractIntegrationTest {
    @Autowired
    private HistoryService historyService;
    @Autowired
    private HistoryRepository historyRepository;

    @Before
    public void beforeEach() throws IOException {
        super.setUp();
    }

    @Test
    public void shouldSaveObjectCreateChangeHistory() throws JSONException {
        historyRepository.deleteAll();

        JSONObject json = new JSONObject();
        json.put("id", "id1");
        json.put("value1", "t1");
        json.put("value2", "t2");
        json.put("value3", "t3");

        LocalDateTime now = LocalDateTime.now();

        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", json.toString(), now, "mail");
        List<History> all = historyRepository.findAll();

        assertNotNull(all);
        assertEquals(4, all.size());
        assertTrue(all.stream().allMatch(c -> c.getClassRef().equals(ObjectHistoryType.TEST.getClassRef())));
        assertTrue(all.stream().allMatch(c -> c.getObjectId().equals("id1")));
        assertTrue(all.stream().allMatch(c -> c.getEmail().equals("mail")));
        assertTrue(all.stream().allMatch(c -> c.getModifiedTime().equals(now)));
        assertTrue(all.stream().allMatch(c -> c.getDeleted().equals(false)));
        assertTrue(all.stream().allMatch(c -> c.getVersion().equals(1L)));


        assertTrue(all.stream().anyMatch(c -> c.getFieldName().equals("value1") && c.getNewValue().equals("t1")));
        assertTrue(all.stream().anyMatch(c -> c.getFieldName().equals("value2") && c.getNewValue().equals("t2")));
        assertTrue(all.stream().anyMatch(c -> c.getFieldName().equals("value3") && c.getNewValue().equals("t3")));
    }
    @Test
    public void shouldSaveObjectChangeHistory() throws JSONException {
        historyRepository.deleteAll();

        JSONObject json = new JSONObject();
        json.put("id", "id1");
        json.put("value1", "t1");
        json.put("value2", "t2");
        json.put("value3", "t3");

        LocalDateTime now = LocalDateTime.now();
        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", json.toString(), now.minusHours(1), "mail");

        json.put("value2", "t2modified");
        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", json.toString(), now, "mail");
        List<History> all = historyRepository.findAll();

        assertNotNull(all);
        List<History> collect = all.stream().filter(c -> c.getVersion().equals(2L)).collect(Collectors.toList());

        assertEquals(1, collect.size());
        assertEquals(ObjectHistoryType.TEST.getClassRef(), collect.get(0).getClassRef());
        assertEquals("id1", collect.get(0).getObjectId());
        assertEquals("mail", collect.get(0).getEmail());
        assertEquals(now, collect.get(0).getModifiedTime());
        assertEquals(false, collect.get(0).getDeleted());
        assertEquals(2L, collect.get(0).getVersion().longValue());
        assertEquals("value2", collect.get(0).getFieldName());
        assertEquals("t2modified", collect.get(0).getNewValue());
    }
    @Test
    public void shouldSaveObjectDeleteHistory() throws JSONException {
        historyRepository.deleteAll();

        JSONObject json = new JSONObject();
        json.put("id", "id1");
        json.put("value1", "t1");
        json.put("value2", "t2");
        json.put("value3", "t3");

        LocalDateTime now = LocalDateTime.now();
        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", json.toString(), now.minusHours(1), "mail");
        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", null, now, "mail");
        List<History> all = historyRepository.findAll();

        assertNotNull(all);
        List<History> collect = all.stream().filter(c -> c.getVersion().equals(2L)).collect(Collectors.toList());

        assertEquals(1, collect.size());
        assertEquals(ObjectHistoryType.TEST.getClassRef(), collect.get(0).getClassRef());
        assertEquals("id1", collect.get(0).getObjectId());
        assertEquals("mail", collect.get(0).getEmail());
        assertEquals(now, collect.get(0).getModifiedTime());
        assertEquals(true, collect.get(0).getDeleted());
        assertEquals(2L, collect.get(0).getVersion().longValue());
        assertNull(collect.get(0).getFieldName());
        assertNull(collect.get(0).getNewValue());
    }

    @Test
    public void shouldGetObjectByIdAndTypeAndVersion() throws JSONException {
        historyRepository.deleteAll();

        JSONObject json = new JSONObject();
        json.put("id", "id1");
        json.put("value1", "t1");
        json.put("value2", "t2");
        json.put("value3", "t3");

        LocalDateTime now = LocalDateTime.now();
        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", json.toString(), now.minusHours(1), "mail");
        json.put("value2", "t2modified");
        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", json.toString(), now, "mail");
        json.put("value3", "t3modified");
        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", json.toString(), now, "mail");

        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id2", null, now, "mail");

        ResponseEntity<HistoryDto[]> response = restTemplate.getForEntity(baseUrl + "/history/type/" + ObjectHistoryType.TEST.toString() + "/id/" + "id1" + "/version/2", HistoryDto[].class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        List<HistoryDto> historyDtoList = Arrays.asList(response.getBody());
        assertEquals(4, historyDtoList.size());
        assertTrue(historyDtoList.stream().anyMatch(c -> c.getFieldName().equals("value1") && c.getValue().equals("t1")));
        assertTrue(historyDtoList.stream().anyMatch(c -> c.getFieldName().equals("value2") && c.getValue().equals("t2modified")));
        assertTrue(historyDtoList.stream().anyMatch(c -> c.getFieldName().equals("value3") && c.getValue().equals("t3")));
    }
    @Test
    public void shouldGetObjectByIdAndTypeAndLastVersion() throws JSONException {
        historyRepository.deleteAll();

        JSONObject json = new JSONObject();
        json.put("id", "id1");
        json.put("value1", "t1");
        json.put("value2", "t2");
        json.put("value3", "t3");

        LocalDateTime now = LocalDateTime.now();
        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", json.toString(), now.minusHours(1), "mail");
        json.put("value2", "t2modified");
        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", json.toString(), now, "mail");
        json.put("value3", "t3modified");
        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", json.toString(), now, "mail");

        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id2", null, now, "mail");

        ResponseEntity<HistoryDto[]> response = restTemplate.getForEntity(baseUrl + "/history/type/" + ObjectHistoryType.TEST.toString() + "/id/" + "id1" + "/last-version", HistoryDto[].class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        List<HistoryDto> historyDtoList = Arrays.asList(response.getBody());
        assertEquals(4, historyDtoList.size());
        assertTrue(historyDtoList.stream().anyMatch(c -> c.getFieldName().equals("value1") && c.getValue().equals("t1")));
        assertTrue(historyDtoList.stream().anyMatch(c -> c.getFieldName().equals("value2") && c.getValue().equals("t2modified")));
        assertTrue(historyDtoList.stream().anyMatch(c -> c.getFieldName().equals("value3") && c.getValue().equals("t3modified")));
    }

    @Test
    public void shouldGetObjectChangesByIdAndType() throws JSONException {
        historyRepository.deleteAll();

        JSONObject json = new JSONObject();
        json.put("id", "id1");
        json.put("value1", "t1");
        json.put("value2", "t2");
        json.put("value3", "t3");

        LocalDateTime now = LocalDateTime.now();
        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", json.toString(), now.minusHours(1), "mail");
        json.put("value2", "t2modified");
        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", json.toString(), now, "mail");
        json.put("value3", "t3modified");
        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id1", json.toString(), now.plusMinutes(2), "mail");

        historyService.saveObjectChangeHistory(ObjectHistoryType.TEST.getClassRef(), "id2", null, now, "mail");

        ResponseEntity<HistoryChangeDto[]> response = restTemplate.getForEntity(baseUrl + "/history/type/" + ObjectHistoryType.TEST.toString() + "/id/" + "id1" + "/changes", HistoryChangeDto[].class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        List<HistoryChangeDto> historyDtoList = Arrays.asList(response.getBody());
        assertEquals(6, historyDtoList.size());

        long version1ValuesCount = historyDtoList.stream().filter(c -> c.getEmail().equals("mail") && c.getModifiedTime().equals(now.minusHours(1)) && c.getVersion().equals(1L) && !c.getDeleted() && c.getOldValue().equals("")).count();
        assertEquals(4, version1ValuesCount);
        long version2ValuesCount = historyDtoList.stream().filter(c -> c.getEmail().equals("mail") && c.getModifiedTime().equals(now) && c.getVersion().equals(2L) && !c.getDeleted()).count();
        assertEquals(1, version2ValuesCount);
        long version3ValuesCount = historyDtoList.stream().filter(c -> c.getEmail().equals("mail") && c.getModifiedTime().equals(now.plusMinutes(2)) && c.getVersion().equals(3L) && !c.getDeleted()).count();
        assertEquals(1, version3ValuesCount);

        assertTrue(historyDtoList.stream().anyMatch(c -> c.getVersion().equals(1L) && c.getFieldName().equals("value1") && c.getNewValue().equals("t1")));
        assertTrue(historyDtoList.stream().anyMatch(c -> c.getVersion().equals(1L) && c.getFieldName().equals("value2") && c.getNewValue().equals("t2")));
        assertTrue(historyDtoList.stream().anyMatch(c -> c.getVersion().equals(1L) && c.getFieldName().equals("value3") && c.getNewValue().equals("t3")));

        assertTrue(historyDtoList.stream().anyMatch(c -> c.getVersion().equals(2L) && c.getFieldName().equals("value2") && c.getNewValue().equals("t2modified") && c.getOldValue().equals("t2")));

        assertTrue(historyDtoList.stream().anyMatch(c -> c.getVersion().equals(3L) && c.getFieldName().equals("value3") && c.getNewValue().equals("t3modified") && c.getOldValue().equals("t3")));
    }


    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ToTest {
        String value1;
        String value2;
        String value3;
    }
}
