package com.devqube.crmassociationservice.association;

import com.devqube.crmassociationservice.association.model.Association;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.search.CrmObjectType;

import java.util.ArrayList;
import java.util.Random;

public class AssociationTestUtil {
    public static AssociationDetailDto getAssociationDetail(Long sourceId, CrmObjectType sourceType, Long destinationId, CrmObjectType destinationType) {
        return AssociationDetailDto.builder()
                .sourceId(sourceId)
                .sourceType(sourceType)
                .destinationId(destinationId)
                .destinationType(destinationType)
                .properties(new ArrayList<>())
                .build();
    }
    public static Association getAssociation(Long sourceId, CrmObjectType sourceType, Long destinationId, CrmObjectType destinationType) {
        return Association.builder()
                .id(new Random().nextLong())
                .sourceId(sourceId)
                .sourceType(sourceType)
                .destinationId(destinationId)
                .destinationType(destinationType)
                .build();
    }
    public static AssociationDetailDto getInvertedAssociation(AssociationDetailDto association) {
        return AssociationDetailDto.builder()
                .destinationType(association.getSourceType())
                .destinationId(association.getSourceId())
                .sourceType(association.getDestinationType())
                .sourceId(association.getDestinationId())
                .properties(association.getProperties()).build();
    }
}
