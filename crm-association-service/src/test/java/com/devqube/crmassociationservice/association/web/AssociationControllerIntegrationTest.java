package com.devqube.crmassociationservice.association.web;


import com.devqube.crmassociationservice.AbstractIntegrationTest;
import com.devqube.crmassociationservice.association.AssociationRepository;
import com.devqube.crmassociationservice.association.AssociationTestUtil;
import com.devqube.crmassociationservice.association.model.Association;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.search.CrmObjectType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class AssociationControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private AssociationRepository associationRepository;

    @Before
    public void setUp() {
        super.setUp();
        this.associationRepository.deleteAll();
    }

    @Test
    public void shouldReturnListByType() {
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(4L, CrmObjectType.file, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(5L, CrmObjectType.contact, 2L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(6L, CrmObjectType.customer, 2L, CrmObjectType.lead));


        ResponseEntity<Association[]> response = restTemplate.getForEntity(baseUrl + "/associations/type/" + CrmObjectType.contact.name(), Association[].class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2, response.getBody().length);
        assertTrue(Arrays.stream(response.getBody()).allMatch(c -> c.getSourceType().equals(CrmObjectType.contact)));

    }

    @Test
    public void shouldReturnListByTypeAndId() {
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(4L, CrmObjectType.file, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(5L, CrmObjectType.contact, 2L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(6L, CrmObjectType.customer, 2L, CrmObjectType.lead));


        ResponseEntity<Association[]> response = restTemplate.getForEntity(baseUrl + "/associations/type/" + CrmObjectType.contact.name() + "/id/" + 1, Association[].class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().length);
        assertEquals(response.getBody()[0].getSourceType(), CrmObjectType.contact);
        assertEquals(response.getBody()[0].getSourceId().longValue(), 1L);
    }

    @Test
    public void shouldCreateNewTwoWayAssociations() {
        AssociationDetailDto associationDetail1 = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 2L, CrmObjectType.lead);
        AssociationDetailDto associationDetail2 = AssociationTestUtil.getAssociationDetail(3L, CrmObjectType.contact, 4L, CrmObjectType.lead);
        List<AssociationDetailDto> associationDetailList = Arrays.asList(associationDetail1, associationDetail2);
        ResponseEntity<Association[]> response = restTemplate.postForEntity(baseUrl + "/associations/list", associationDetailList, Association[].class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        List<Association> all = associationRepository.findAll();
        assertEquals(4, all.size());

        List<Association> normalAssociations = all.stream().filter(c -> c.getSourceType().equals(CrmObjectType.contact)).collect(Collectors.toList());

        assertNotNull(normalAssociations);
        assertEquals(2, normalAssociations.size());
        Optional<Association> normalAssociation1 = normalAssociations.stream().filter(c -> c.getSourceId().equals(1L)).findFirst();
        Optional<Association> normalAssociation2 = normalAssociations.stream().filter(c -> c.getSourceId().equals(3L)).findFirst();

        assertTrue(normalAssociation1.isPresent());
        assertEquals(1L, normalAssociation1.get().getSourceId().longValue());
        assertEquals(2L, normalAssociation1.get().getDestinationId().longValue());
        assertEquals(CrmObjectType.contact, normalAssociation1.get().getSourceType());
        assertEquals(CrmObjectType.lead, normalAssociation1.get().getDestinationType());

        assertTrue(normalAssociation2.isPresent());
        assertEquals(3L, normalAssociation2.get().getSourceId().longValue());
        assertEquals(4L, normalAssociation2.get().getDestinationId().longValue());
        assertEquals(CrmObjectType.contact, normalAssociation2.get().getSourceType());
        assertEquals(CrmObjectType.lead, normalAssociation2.get().getDestinationType());


        List<Association> invertedAssociations = all.stream().filter(c -> c.getSourceType().equals(CrmObjectType.lead)).collect(Collectors.toList());

        assertNotNull(invertedAssociations);
        assertEquals(2, invertedAssociations.size());
        Optional<Association> invertedAssociation1 = invertedAssociations.stream().filter(c -> c.getSourceId().equals(2L)).findFirst();
        Optional<Association> invertedAssociation2 = invertedAssociations.stream().filter(c -> c.getSourceId().equals(4L)).findFirst();

        assertTrue(invertedAssociation1.isPresent());
        assertEquals(2L, invertedAssociation1.get().getSourceId().longValue());
        assertEquals(1L, invertedAssociation1.get().getDestinationId().longValue());
        assertEquals(CrmObjectType.lead, invertedAssociation1.get().getSourceType());
        assertEquals(CrmObjectType.contact, invertedAssociation1.get().getDestinationType());

        assertTrue(invertedAssociation2.isPresent());
        assertEquals(4L, invertedAssociation2.get().getSourceId().longValue());
        assertEquals(3L, invertedAssociation2.get().getDestinationId().longValue());
        assertEquals(CrmObjectType.lead, invertedAssociation2.get().getSourceType());
        assertEquals(CrmObjectType.contact, invertedAssociation2.get().getDestinationType());
    }
    @Test
    public void shouldCreateNewTwoWayAssociation() {
        AssociationDetailDto associationDetail = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 2L, CrmObjectType.lead);
        ResponseEntity<Association> response = restTemplate.postForEntity(baseUrl + "/associations", associationDetail, Association.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        assertEquals(1L, response.getBody().getSourceId().longValue());
        assertEquals(2L, response.getBody().getDestinationId().longValue());
        assertEquals(CrmObjectType.contact, response.getBody().getSourceType());
        assertEquals(CrmObjectType.lead, response.getBody().getDestinationType());

        List<Association> all = associationRepository.findAll();
        assertEquals(2, all.size());

        Optional<Association> normalAssociation = all.stream().filter(c -> c.getSourceType().equals(CrmObjectType.contact)).findFirst();
        assertTrue(normalAssociation.isPresent());

        assertEquals(1L, normalAssociation.get().getSourceId().longValue());
        assertEquals(2L, normalAssociation.get().getDestinationId().longValue());
        assertEquals(CrmObjectType.contact, normalAssociation.get().getSourceType());
        assertEquals(CrmObjectType.lead, normalAssociation.get().getDestinationType());

        Optional<Association> invertedAssociation = all.stream().filter(c -> c.getSourceType().equals(CrmObjectType.lead)).findFirst();
        assertTrue(invertedAssociation.isPresent());

        assertEquals(2L, invertedAssociation.get().getSourceId().longValue());
        assertEquals(1L, invertedAssociation.get().getDestinationId().longValue());
        assertEquals(CrmObjectType.lead, invertedAssociation.get().getSourceType());
        assertEquals(CrmObjectType.contact, invertedAssociation.get().getDestinationType());
    }
    @Test
    public void shouldCreateNewTwoWayAssociationIfOneExist() {
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 2L, CrmObjectType.lead));

        AssociationDetailDto associationDetail = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 2L, CrmObjectType.lead);
        ResponseEntity<Association> response = restTemplate.postForEntity(baseUrl + "/associations", associationDetail, Association.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        assertEquals(1L, response.getBody().getSourceId().longValue());
        assertEquals(2L, response.getBody().getDestinationId().longValue());
        assertEquals(CrmObjectType.contact, response.getBody().getSourceType());
        assertEquals(CrmObjectType.lead, response.getBody().getDestinationType());

        //test if normal association exist
        List<Association> all = associationRepository.findAll();
        assertEquals(2, all.size());
        Optional<Association> normalAssociation = all.stream().filter(c -> c.getSourceType().equals(CrmObjectType.contact)).findFirst();
        assertTrue(normalAssociation.isPresent());
        assertEquals(1L, normalAssociation.get().getSourceId().longValue());
        assertEquals(2L, normalAssociation.get().getDestinationId().longValue());
        assertEquals(CrmObjectType.contact, normalAssociation.get().getSourceType());
        assertEquals(CrmObjectType.lead, normalAssociation.get().getDestinationType());
        Optional<Association> invertedAssociation = all.stream().filter(c -> c.getSourceType().equals(CrmObjectType.lead)).findFirst();
        assertTrue(invertedAssociation.isPresent());
        assertEquals(2L, invertedAssociation.get().getSourceId().longValue());
        assertEquals(1L, invertedAssociation.get().getDestinationId().longValue());
        assertEquals(CrmObjectType.lead, invertedAssociation.get().getSourceType());
        assertEquals(CrmObjectType.contact, invertedAssociation.get().getDestinationType());

        // second part
        associationRepository.deleteAll();
        associationRepository.save(AssociationTestUtil.getAssociation(2L, CrmObjectType.lead, 1L, CrmObjectType.contact));

        associationDetail = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 2L, CrmObjectType.lead);
        response = restTemplate.postForEntity(baseUrl + "/associations", associationDetail, Association.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        assertEquals(1L, response.getBody().getSourceId().longValue());
        assertEquals(2L, response.getBody().getDestinationId().longValue());
        assertEquals(CrmObjectType.contact, response.getBody().getSourceType());
        assertEquals(CrmObjectType.lead, response.getBody().getDestinationType());

        //test if inverted association exist
        all = associationRepository.findAll();
        assertEquals(2, all.size());
        normalAssociation = all.stream().filter(c -> c.getSourceType().equals(CrmObjectType.contact)).findFirst();
        assertTrue(normalAssociation.isPresent());
        assertEquals(1L, normalAssociation.get().getSourceId().longValue());
        assertEquals(2L, normalAssociation.get().getDestinationId().longValue());
        assertEquals(CrmObjectType.contact, normalAssociation.get().getSourceType());
        assertEquals(CrmObjectType.lead, normalAssociation.get().getDestinationType());
        invertedAssociation = all.stream().filter(c -> c.getSourceType().equals(CrmObjectType.lead)).findFirst();
        assertTrue(invertedAssociation.isPresent());
        assertEquals(2L, invertedAssociation.get().getSourceId().longValue());
        assertEquals(1L, invertedAssociation.get().getDestinationId().longValue());
        assertEquals(CrmObjectType.lead, invertedAssociation.get().getSourceType());
        assertEquals(CrmObjectType.contact, invertedAssociation.get().getDestinationType());
    }
    @Test
    public void shouldDeleteTwoWayAssociation() {
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 2L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(2L, CrmObjectType.lead, 1L, CrmObjectType.contact));
        AssociationDetailDto associationDetail = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 2L, CrmObjectType.lead);
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/associations", HttpMethod.DELETE, new HttpEntity<>(associationDetail), Void.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        List<Association> all = associationRepository.findAll();
        assertEquals(0, all.size());
    }
    @Test
    public void shouldDeleteTwoWayAssociations() {
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 2L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(2L, CrmObjectType.lead, 1L, CrmObjectType.contact));
        associationRepository.save(AssociationTestUtil.getAssociation(3L, CrmObjectType.contact, 4L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(4L, CrmObjectType.lead, 3L, CrmObjectType.contact));
        AssociationDetailDto associationDetail1 = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 2L, CrmObjectType.lead);
        AssociationDetailDto associationDetail2 = AssociationTestUtil.getAssociationDetail(3L, CrmObjectType.contact, 4L, CrmObjectType.lead);
        List<AssociationDetailDto> associationDetailList = Arrays.asList(associationDetail1, associationDetail2);
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/associations/list", HttpMethod.DELETE, new HttpEntity<>(associationDetailList), Void.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        List<Association> all = associationRepository.findAll();
        assertEquals(0, all.size());
    }
    @Test
    public void shouldDeleteTwoWayAssociationIfOneNotFound() {
        associationRepository.save(AssociationTestUtil.getAssociation(2L, CrmObjectType.lead, 1L, CrmObjectType.contact));
        AssociationDetailDto associationDetail = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 2L, CrmObjectType.lead);
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/associations", HttpMethod.DELETE, new HttpEntity<>(associationDetail), Void.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        List<Association> all = associationRepository.findAll();
        assertEquals(0, all.size());
    }

    @Test
    public void shouldReturnNotContendIfNotFoundAnyElementWhenDeletingAllByTypeAndId() {
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(4L, CrmObjectType.file, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 2L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(6L, CrmObjectType.contact, 2L, CrmObjectType.lead));

        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/associations/type/" + CrmObjectType.contact.name() + "/id/" + 1, HttpMethod.DELETE, null, Void.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        List<Association> all = associationRepository.findAll();
        assertEquals(2, all.size());
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.file)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.contact) && c.getSourceId().equals(6L)));
    }
    @Test
    public void shouldDeleteAllByTypeAndId() {
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/associations/type/" + CrmObjectType.contact.name() + "/id/" + 1, HttpMethod.DELETE, null, Void.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void shouldGetAllBySourceTypeAndDestinationTypeAndSourceId() {
        //getAllByTypeAndIdAndDestinationType
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(4L, CrmObjectType.file, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 2L, CrmObjectType.customer));
        associationRepository.save(AssociationTestUtil.getAssociation(6L, CrmObjectType.contact, 2L, CrmObjectType.customer));

        ResponseEntity<Association[]> response = restTemplate.getForEntity(baseUrl + "/associations/type/" + CrmObjectType.contact.name() + "/id/1/destination-type/" + CrmObjectType.customer.name(), Association[].class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().length);
        assertEquals(1L, response.getBody()[0].getSourceId().longValue());
        assertEquals(CrmObjectType.contact, response.getBody()[0].getSourceType());
        assertEquals(2L, response.getBody()[0].getDestinationId().longValue());
        assertEquals(CrmObjectType.customer, response.getBody()[0].getDestinationType());
    }

    @Test
    public void shouldGetAllBySourceTypeAndDestinationTypeAndSourceIds() {
        //getAllBySourceTypeAndSourceIdsAndDestinationType
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(4L, CrmObjectType.file, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 2L, CrmObjectType.customer));
        associationRepository.save(AssociationTestUtil.getAssociation(6L, CrmObjectType.contact, 3L, CrmObjectType.customer));

        ResponseEntity<Association[]> response = restTemplate.getForEntity(baseUrl + "/associations/type/" + CrmObjectType.contact.name() + "/ids/1,6/destination-type/" + CrmObjectType.customer.name(), Association[].class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2, response.getBody().length);
        List<Association> responseList = Arrays.asList(response.getBody());
        Optional<Association> association1 = responseList.stream().filter(c -> c.getSourceId().equals(1L)).findFirst();
        Optional<Association> association2 = responseList.stream().filter(c -> c.getSourceId().equals(6L)).findFirst();
        assertTrue(association1.isPresent());
        assertTrue(association2.isPresent());

        assertEquals(1L, association1.get().getSourceId().longValue());
        assertEquals(CrmObjectType.contact, association1.get().getSourceType());
        assertEquals(2L, association1.get().getDestinationId().longValue());
        assertEquals(CrmObjectType.customer, association1.get().getDestinationType());

        assertEquals(6L, association2.get().getSourceId().longValue());
        assertEquals(CrmObjectType.contact, association2.get().getSourceType());
        assertEquals(3L, association2.get().getDestinationId().longValue());
        assertEquals(CrmObjectType.customer, association2.get().getDestinationType());
    }

    @Test
    public void shouldUpdateAssociations() {
        AssociationDetailDto associationDetail1 = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 3L, CrmObjectType.file);
        AssociationDetailDto associationDetail2 = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 4L, CrmObjectType.file);
        List<AssociationDetailDto> associationDetailList = Arrays.asList(associationDetail1, associationDetail2);

        //updateAssociations
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(4L, CrmObjectType.file, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 2L, CrmObjectType.customer));
        associationRepository.save(AssociationTestUtil.getAssociation(6L, CrmObjectType.contact, 3L, CrmObjectType.customer));

        ResponseEntity<Association[]> response = restTemplate.exchange(baseUrl + "/associations/list/type/contact/id/1", HttpMethod.PUT, new HttpEntity<>(associationDetailList), Association[].class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2, response.getBody().length);

        List<Association> all = associationRepository.findAll();
        assertEquals(6, all.size());
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.file)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.contact) && c.getSourceId().equals(6L)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.contact)
                && c.getSourceId().equals(1L) && c.getDestinationId().equals(3L) && c.getDestinationType().equals(CrmObjectType.file)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.contact)
                && c.getSourceId().equals(1L) && c.getDestinationId().equals(4L) && c.getDestinationType().equals(CrmObjectType.file)));

        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.file)
                && c.getSourceId().equals(3L) && c.getDestinationId().equals(1L) && c.getDestinationType().equals(CrmObjectType.contact)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.file)
                && c.getSourceId().equals(3L) && c.getDestinationId().equals(1L) && c.getDestinationType().equals(CrmObjectType.contact)));
    }
    @Test
    public void shouldUpdateAssociationsWhenLinksAreEmpty() {
        //updateAssociations
        AssociationDetailDto associationDetail1 = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 3L, CrmObjectType.file);
        AssociationDetailDto associationDetail2 = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 4L, CrmObjectType.file);
        List<AssociationDetailDto> associationDetailList = Arrays.asList(associationDetail1, associationDetail2);

        //updateAssociations
        associationRepository.save(AssociationTestUtil.getAssociation(4L, CrmObjectType.file, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(6L, CrmObjectType.contact, 3L, CrmObjectType.customer));

        ResponseEntity<Association[]> response = restTemplate.exchange(baseUrl + "/associations/list/type/contact/id/1", HttpMethod.PUT, new HttpEntity<>(associationDetailList), Association[].class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2, response.getBody().length);

        List<Association> all = associationRepository.findAll();
        assertEquals(6, all.size());
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.file)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.contact) && c.getSourceId().equals(6L)));

        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.contact)
                && c.getSourceId().equals(1L) && c.getDestinationId().equals(3L) && c.getDestinationType().equals(CrmObjectType.file)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.contact)
                && c.getSourceId().equals(1L) && c.getDestinationId().equals(4L) && c.getDestinationType().equals(CrmObjectType.file)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.file)
                && c.getSourceId().equals(3L) && c.getDestinationId().equals(1L) && c.getDestinationType().equals(CrmObjectType.contact)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.file)
                && c.getSourceId().equals(3L) && c.getDestinationId().equals(1L) && c.getDestinationType().equals(CrmObjectType.contact)));
    }

    @Test
    public void shouldUpdateAssociationsForTypes() {
        //updateAssociationsForTypes
        AssociationDetailDto associationDetail1 = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 3L, CrmObjectType.file);
        AssociationDetailDto associationDetail2 = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 4L, CrmObjectType.file);
        AssociationDetailDto associationDetail3 = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.contact, 4L, CrmObjectType.product);
        List<AssociationDetailDto> associationDetailList = Arrays.asList(associationDetail1, associationDetail2, associationDetail3);

        //updateAssociations
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 1L, CrmObjectType.file));
        associationRepository.save(AssociationTestUtil.getAssociation(4L, CrmObjectType.file, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.contact, 2L, CrmObjectType.customer));
        associationRepository.save(AssociationTestUtil.getAssociation(6L, CrmObjectType.contact, 3L, CrmObjectType.customer));

        ResponseEntity<Association[]> response = restTemplate.exchange(baseUrl + "/associations/list/type/contact/id/1/for-types/file", HttpMethod.PUT, new HttpEntity<>(associationDetailList), Association[].class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2, response.getBody().length);

        List<Association> all = associationRepository.findAll();
        assertEquals(7, all.size());
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.file)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.contact) && c.getSourceId().equals(6L)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.contact)
                && c.getSourceId().equals(1L) && c.getDestinationId().equals(2L) && c.getDestinationType().equals(CrmObjectType.customer)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.contact)
                && c.getSourceId().equals(1L) && c.getDestinationId().equals(3L) && c.getDestinationType().equals(CrmObjectType.file)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.contact)
                && c.getSourceId().equals(1L) && c.getDestinationId().equals(4L) && c.getDestinationType().equals(CrmObjectType.file)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.file)
                && c.getSourceId().equals(3L) && c.getDestinationId().equals(1L) && c.getDestinationType().equals(CrmObjectType.contact)));
        assertTrue(all.stream().anyMatch(c -> c.getSourceType().equals(CrmObjectType.file)
                && c.getSourceId().equals(3L) && c.getDestinationId().equals(1L) && c.getDestinationType().equals(CrmObjectType.contact)));
    }


    @Test
    public void shouldCopyAssociations() {
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.lead, 10L, CrmObjectType.activity));
        associationRepository.save(AssociationTestUtil.getAssociation(10L, CrmObjectType.activity, 1L, CrmObjectType.lead));
        associationRepository.save(AssociationTestUtil.getAssociation(1L, CrmObjectType.lead, 20L, CrmObjectType.activity));
        associationRepository.save(AssociationTestUtil.getAssociation(20L, CrmObjectType.activity, 1L, CrmObjectType.lead));


        AssociationDetailDto associationDetail = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.lead, 200L, CrmObjectType.opportunity);
        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/internal/associations/copy", List.of(associationDetail), Void.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        List<Association> all = associationRepository.findAll();
        assertEquals(8, all.size());
        List<Association> opportunityConnections = associationRepository.findAllBySourceIdAndSourceType(200L, CrmObjectType.opportunity);
        assertEquals(2, opportunityConnections.size());
        assertTrue(opportunityConnections.stream().anyMatch(c -> c.getDestinationType().equals(CrmObjectType.activity) && c.getDestinationId().equals(10L)));
        assertTrue(opportunityConnections.stream().anyMatch(c -> c.getDestinationType().equals(CrmObjectType.activity) && c.getDestinationId().equals(20L)));


        List<Association> activity_1_Connections = associationRepository.findAllBySourceIdAndSourceType(10L,CrmObjectType.activity);
        assertEquals(2, activity_1_Connections.size());
        assertTrue(activity_1_Connections.stream().anyMatch(c -> c.getDestinationType().equals(CrmObjectType.lead) && c.getDestinationId().equals(1L)));
        assertTrue(activity_1_Connections.stream().anyMatch(c -> c.getDestinationType().equals(CrmObjectType.opportunity) && c.getDestinationId().equals(200L)));

        List<Association> activity_2_Connections = associationRepository.findAllBySourceIdAndSourceType(20L,CrmObjectType.activity);
        assertEquals(2, activity_2_Connections.size());
        assertTrue(activity_2_Connections.stream().anyMatch(c -> c.getDestinationType().equals(CrmObjectType.lead) && c.getDestinationId().equals(1L)));
        assertTrue(activity_2_Connections.stream().anyMatch(c -> c.getDestinationType().equals(CrmObjectType.opportunity) && c.getDestinationId().equals(200L)));
    }
}
