package com.devqube.crmassociationservice.association;

import com.devqube.crmassociationservice.association.model.Association;
import com.devqube.crmassociationservice.exception.AssociationExistException;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.search.CrmObjectType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AssociationServiceTest {

    @Mock
    private AssociationRepository associationRepository;

    @InjectMocks
    private AssociationService associationService;

    @Test
    public void shouldGetAssociationsByType() {
        ArrayList<Association> associations = new ArrayList<>();
        associations.add(AssociationTestUtil.getAssociation(1L, CrmObjectType.lead, 2L, CrmObjectType.contact));
        associations.add(AssociationTestUtil.getAssociation(2L, CrmObjectType.lead, 2L, CrmObjectType.contact));

        when(associationRepository.findAllBySourceType(eq(CrmObjectType.lead))).thenReturn(associations);

        List<Association> associationsByType = associationService.getAssociationsByType(CrmObjectType.lead);
        assertEquals(2, associationsByType.size());
    }

    @Test
    public void shouldGetAssociationsByTypeAndId() {
        ArrayList<Association> associations = new ArrayList<>();
        associations.add(AssociationTestUtil.getAssociation(1L, CrmObjectType.lead, 2L, CrmObjectType.contact));
        associations.add(AssociationTestUtil.getAssociation(1L, CrmObjectType.lead, 4L, CrmObjectType.contact));

        when(associationRepository.findAllBySourceIdAndSourceType(eq(1L), eq(CrmObjectType.lead))).thenReturn(associations);

        List<Association> associationsByType = associationService.getAssociationsByTypeAndId(1L, CrmObjectType.lead);
        assertEquals(2, associationsByType.size());
    }
    @Test
    public void shouldGetAssociationsByTypeAndIdAndDestinationType() {
        ArrayList<Association> associations = new ArrayList<>();
        associations.add(AssociationTestUtil.getAssociation(1L, CrmObjectType.lead, 2L, CrmObjectType.customer));
        associations.add(AssociationTestUtil.getAssociation(1L, CrmObjectType.lead, 4L, CrmObjectType.customer));

        List<Long> ids = Arrays.asList(1L, 2L);
        when(associationRepository.findAllBySourceIdInAndSourceTypeAndDestinationType(eq(ids), eq(CrmObjectType.lead), eq(CrmObjectType.customer))).thenReturn(associations);

        List<Association> associationsByType = associationService.getAllBySourceTypeAndSourceIdsAndDestinationType(CrmObjectType.lead, CrmObjectType.customer, ids);
        assertEquals(2, associationsByType.size());
    }
    @Test
    public void shouldUpdateAssociations() {
        AssociationDetailDto associationDetail1 = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.lead, 4L, CrmObjectType.contact);
        AssociationDetailDto associationDetail2 = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.lead, 54L, CrmObjectType.contact);
        List<AssociationDetailDto> associationDetailList = Arrays.asList(associationDetail1, associationDetail2);
        List<Association> associationsByType = associationService.updateAssociations(CrmObjectType.lead, 1L, associationDetailList);
        assertEquals(2, associationsByType.size());
        Mockito.verify(associationRepository, times(1)).deleteAllBySourceIdAndSourceType(eq(1L), eq(CrmObjectType.lead));
        Mockito.verify(associationRepository, times(1)).deleteAllByDestinationIdAndDestinationType(eq(1L), eq(CrmObjectType.lead));
        Mockito.verify(associationRepository, times(4)).save(any());
    }
    @Test
    public void shouldUpdateAssociationsForTypes() {
        List<CrmObjectType> crmObjectTypes = Arrays.asList(CrmObjectType.customer, CrmObjectType.contact);
        AssociationDetailDto associationDetail1 = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.lead, 4L, CrmObjectType.contact);
        AssociationDetailDto associationDetail2 = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.lead, 54L, CrmObjectType.contact);
        List<AssociationDetailDto> associationDetailList = Arrays.asList(associationDetail1, associationDetail2);
        List<Association> associationsByType = associationService.updateAssociationsForTypes(CrmObjectType.lead, 1L, crmObjectTypes, associationDetailList);
        assertEquals(2, associationsByType.size());
        Mockito.verify(associationRepository, times(1)).deleteAllBySourceIdAndSourceTypeAndDestinationType(eq(1L), eq(CrmObjectType.lead), eq(CrmObjectType.customer));
        Mockito.verify(associationRepository, times(1)).deleteAllByDestinationIdAndDestinationTypeAndSourceType(eq(1L), eq(CrmObjectType.lead), eq(CrmObjectType.customer));
        Mockito.verify(associationRepository, times(1)).deleteAllBySourceIdAndSourceTypeAndDestinationType(eq(1L), eq(CrmObjectType.lead), eq(CrmObjectType.contact));
        Mockito.verify(associationRepository, times(1)).deleteAllByDestinationIdAndDestinationTypeAndSourceType(eq(1L), eq(CrmObjectType.lead), eq(CrmObjectType.contact));
        Mockito.verify(associationRepository, times(4)).save(any());
    }
    @Test
    public void shouldGetAssociationsByTypeAndIdsAndDestinationType() {
        ArrayList<Association> associations = new ArrayList<>();
        associations.add(AssociationTestUtil.getAssociation(1L, CrmObjectType.lead, 2L, CrmObjectType.customer));
        associations.add(AssociationTestUtil.getAssociation(1L, CrmObjectType.lead, 4L, CrmObjectType.customer));

        when(associationRepository.findAllBySourceIdAndSourceTypeAndDestinationType(eq(1L), eq(CrmObjectType.lead), eq(CrmObjectType.customer))).thenReturn(associations);

        List<Association> associationsByType = associationService.getAssociationsBySourceTypeAndSourceIdAndDestinationType(CrmObjectType.lead, CrmObjectType.customer, 1L);
        assertEquals(2, associationsByType.size());
    }


    @Test
    public void shouldAddTwoWayAssociation() throws AssociationExistException {
        AssociationDetailDto associationDetail = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.lead, 4L, CrmObjectType.contact);
        AssociationDetailDto invertedAssociationDetail = AssociationTestUtil.getInvertedAssociation(associationDetail);

        when(associationRepository
                .findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(eq(1L), eq(CrmObjectType.lead), eq(4L), eq(CrmObjectType.contact)))
                .thenReturn(Optional.empty());
        when(associationRepository
                .findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(eq(4L), eq(CrmObjectType.contact), eq(1L), eq(CrmObjectType.lead)))
                .thenReturn(Optional.empty());

        associationService.addAssociation(associationDetail);

        Mockito.verify(associationRepository, times(2)).save(any());
    }

    @Test
    public void shouldAddTwoWayAssociationWhenAssociationExistAndInvertedNotExist() throws AssociationExistException {
        AssociationDetailDto associationDetail = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.lead, 4L, CrmObjectType.contact);
        AssociationDetailDto invertedAssociationDetail = AssociationTestUtil.getInvertedAssociation(associationDetail);

        when(associationRepository
                .findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(eq(1L), eq(CrmObjectType.lead), eq(4L), eq(CrmObjectType.contact)))
                .thenReturn(Optional.of(associationService.toModel(associationDetail)));
        when(associationRepository
                .findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(eq(4L), eq(CrmObjectType.contact), eq(1L), eq(CrmObjectType.lead)))
                .thenReturn(Optional.empty());

        associationService.addAssociation(associationDetail);

        Mockito.verify(associationRepository, times(1)).save(any());
    }

    @Test
    public void shouldAddTwoWayAssociationWhenInvertedAssociationExist() throws AssociationExistException {
        AssociationDetailDto associationDetail = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.lead, 4L, CrmObjectType.contact);
        AssociationDetailDto invertedAssociationDetail = AssociationTestUtil.getInvertedAssociation(associationDetail);

        when(associationRepository
                .findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(eq(1L), eq(CrmObjectType.lead), eq(4L), eq(CrmObjectType.contact)))
                .thenReturn(Optional.empty());
        when(associationRepository
                .findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(eq(4L), eq(CrmObjectType.contact), eq(1L), eq(CrmObjectType.lead)))
                .thenReturn(Optional.of(associationService.toModel(invertedAssociationDetail)));

        associationService.addAssociation(associationDetail);
        associationService.addAssociation(invertedAssociationDetail);

        Mockito.verify(associationRepository, times(2)).save(any());
    }

    @Test
    public void shouldRemoveAssociationTwoWayBinding() throws EntityNotFoundException {
        AssociationDetailDto associationDetail = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.lead, 4L, CrmObjectType.contact);
        AssociationDetailDto invertedAssociationDetail = AssociationTestUtil.getInvertedAssociation(associationDetail);
        when(associationRepository
                .findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(eq(1L), eq(CrmObjectType.lead), eq(4L), eq(CrmObjectType.contact)))
                .thenReturn(Optional.of(associationService.toModel(associationDetail)));
        when(associationRepository
                .findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(eq(4L), eq(CrmObjectType.contact), eq(1L), eq(CrmObjectType.lead)))
                .thenReturn(Optional.of(associationService.toModel(invertedAssociationDetail)));
        associationService.removeAssociation(associationDetail);
        Mockito.verify(associationRepository, times(2)).deleteById(any());
    }

    @Test
    public void shouldRemoveAssociationTwoWayBindingWhenOnlyFirstAssociationExist() throws EntityNotFoundException {
        AssociationDetailDto associationDetail = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.lead, 4L, CrmObjectType.contact);
        AssociationDetailDto invertedAssociationDetail = AssociationTestUtil.getInvertedAssociation(associationDetail);

        when(associationRepository
                .findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(eq(1L), eq(CrmObjectType.lead), eq(4L), eq(CrmObjectType.contact)))
                .thenReturn(Optional.of(associationService.toModel(associationDetail)));
        when(associationRepository
                .findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(eq(4L), eq(CrmObjectType.contact), eq(1L), eq(CrmObjectType.lead)))
                .thenReturn(Optional.empty());
        associationService.removeAssociation(associationDetail);

        Mockito.verify(associationRepository, times(1)).deleteById(any());
    }

    @Test
    public void shouldRemoveAssociationTwoWayBindingWhenOnlySecondAssociationExist() throws EntityNotFoundException {
        AssociationDetailDto associationDetail = AssociationTestUtil.getAssociationDetail(1L, CrmObjectType.lead, 4L, CrmObjectType.contact);
        AssociationDetailDto invertedAssociationDetail = AssociationTestUtil.getInvertedAssociation(associationDetail);

        when(associationRepository
                .findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(eq(1L), eq(CrmObjectType.lead), eq(4L), eq(CrmObjectType.contact)))
                .thenReturn(Optional.empty());
        when(associationRepository
                .findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(eq(4L), eq(CrmObjectType.contact), eq(1L), eq(CrmObjectType.lead)))
                .thenReturn(Optional.of(associationService.toModel(invertedAssociationDetail)));

        associationService.removeAssociation(associationDetail);
        Mockito.verify(associationRepository, times(1)).deleteById(any());
    }

    @Test
    public void shouldRemoveAssociationByIdAndType() {
        this.associationService.deleteAssociationsByTypeAndId(CrmObjectType.lead, 1L);
        Mockito.verify(associationRepository, times(1)).deleteAllBySourceIdAndSourceType(eq(1L), eq(CrmObjectType.lead));
    }
}

