package com.devqube.crmassociationservice.note;

import com.devqube.crmassociationservice.access.UserService;
import com.devqube.crmshared.access.model.PermissionDto;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.CrmObjectType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NoteServiceTest {

    @Mock
    private AccessService accessService;

    @Mock
    private NoteRepository noteRepository;

    @Mock
    private UserService userService;

    @InjectMocks
    private NoteService noteService;

    private Note note;

    private List<Note> notes;
    private List<Note> notesToCopy;

    @Before
    public void setUp() {
        note = new Note();
        note.setObjectType(CrmObjectType.customer);
        notes = List.of(
                new Note(1L, CrmObjectType.lead, 1L, LocalDateTime.now(), LocalDateTime.now(), "notatka_1", "notatka_1", 0L, 0L),
                new Note(2L, CrmObjectType.lead, 1L, LocalDateTime.now(), LocalDateTime.now(), "notatka_2", "notatka_2", 0L, 0L),
                new Note(3L, CrmObjectType.lead, 1L, LocalDateTime.now(), LocalDateTime.now(), "notatka_3", "notatka_3", 0L, 0L)
        );

        notesToCopy = List.of(
                new Note(null, CrmObjectType.opportunity, 2L, notes.get(0).getCreated(), notes.get(0).getUpdated(), "notatka_1", "notatka_1", 0L, 0L),
                new Note(null, CrmObjectType.opportunity, 2L, notes.get(1).getCreated(), notes.get(1).getUpdated(), "notatka_2", "notatka_2", 0L, 0L),
                new Note(null, CrmObjectType.opportunity, 2L, notes.get(2).getCreated(), notes.get(2).getUpdated(), "notatka_3", "notatka_3", 0L, 0L)
        );

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Logged-Account-Email", "jan.kowalski@devqube.com");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    @Test
    public void shouldSaveNewNote() throws PermissionDeniedException {
        when(accessService.hasAccess("CustomerNotes.add", PermissionDto.State.WRITE, true)).thenReturn(false);
        noteService.save(note);
        verify(noteRepository, times(1)).save(any());
    }

    @Test(expected = PermissionDeniedException.class)
    public void shouldNotSaveNewNoteWhenNoPermission() throws PermissionDeniedException {
        when(accessService.hasAccess("CustomerNotes.add", PermissionDto.State.WRITE, true)).thenReturn(true);
        noteService.save(note);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenModifyingNotExistingNote() throws EntityNotFoundException, PermissionDeniedException {
        when(accessService.hasAccess("CustomerNotes.edit", PermissionDto.State.WRITE, true)).thenReturn(false);
        note.setId(1L);
        when(noteRepository.findById(1L)).thenReturn(Optional.empty());
        noteService.modify(1L, note);
    }

    @Test(expected = PermissionDeniedException.class)
    public void shouldThrowExceptionWhenNoPermissionForEdit() throws EntityNotFoundException, PermissionDeniedException {
        when(accessService.hasAccess("CustomerNotes.edit", PermissionDto.State.WRITE, true)).thenReturn(true);
        noteService.modify(1L, note);
    }

    @Test
    public void shouldModifyNoteWhenEverythingOK() throws EntityNotFoundException, PermissionDeniedException {
        when(accessService.hasAccess("CustomerNotes.edit", PermissionDto.State.WRITE, true)).thenReturn(false);
        note.setId(1L);
        when(noteRepository.findById(1L)).thenReturn(Optional.of(note));
        noteService.modify(1L, note);
    }

    @Test
    public void shouldDeleteAllByObjectTypeAndObjectId() throws PermissionDeniedException, EntityNotFoundException {
        when(userService.getUserId(anyString())).thenReturn(-1L);
        noteService.deleteByObjectTypeAndObjectId(CrmObjectType.lead, 1L);
        verify(noteRepository, times(1)).deleteAllByObjectTypeAndObjectId(CrmObjectType.lead, 1L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenTryingToDeleteNotExistingNote() throws EntityNotFoundException, PermissionDeniedException {
        when(noteRepository.findById(1L)).thenReturn(Optional.empty());
        noteService.deleteById(1L);
    }

    @Test(expected = PermissionDeniedException.class)
    public void shouldThrowExceptionWhenNoPermissionForDelete() throws EntityNotFoundException, PermissionDeniedException {
        when(noteRepository.findById(1L)).thenReturn(Optional.of(note));
        when(accessService.hasAccess("CustomerNotes.remove", PermissionDto.State.WRITE, true)).thenReturn(true);
        noteService.deleteById(1L);
    }

    @Test
    public void shouldDeleteNoteWhenItExists() throws EntityNotFoundException, PermissionDeniedException {
        when(accessService.hasAccess("CustomerNotes.remove", PermissionDto.State.WRITE, true)).thenReturn(false);
        when(noteRepository.findById(1L)).thenReturn(Optional.of(note));
        when(userService.getUserId(anyString())).thenReturn(-1L);
        noteService.deleteById(1L);
        verify(noteRepository, times(1)).deleteById(1L);
    }

    @Test
    public void shouldReturnNotesWhenPermissionPresent() throws PermissionDeniedException, EntityNotFoundException {
        when(accessService.hasAccess("CustomerNotes.view", PermissionDto.State.INVISIBLE, false)).thenReturn(false);
        when(userService.getUserId(anyString())).thenReturn(-1L);
        when(noteRepository.findAllByObjectTypeAndObjectIdOrderByCreatedDesc(CrmObjectType.customer, 1L)).thenReturn(Collections.emptyList());
        assertTrue(noteService.getAllByObjectTypeAndObjectId(CrmObjectType.customer, 1L).isEmpty());
    }

    @Test(expected = PermissionDeniedException.class)
    public void shouldThrowExceptionWhenGettingListAndNoPermission() throws PermissionDeniedException, EntityNotFoundException {
        when(accessService.hasAccess("CustomerNotes.view", PermissionDto.State.INVISIBLE, false)).thenReturn(true);
        noteService.getAllByObjectTypeAndObjectId(CrmObjectType.customer, 1L);
    }

    @Test
    public void shouldCopyNotes() {
        when(noteRepository.findAllByObjectTypeAndObjectIdOrderByCreatedDesc(CrmObjectType.lead, 1L)).thenReturn(this.notes);
        AssociationDetailDto dto = new AssociationDetailDto(CrmObjectType.lead, 1L, CrmObjectType.opportunity, 2L);
        noteService.copyNotes(dto);
        verify(noteRepository, times(1)).saveAll(notesToCopy);
    }
}
