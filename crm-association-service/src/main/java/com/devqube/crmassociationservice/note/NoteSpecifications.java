package com.devqube.crmassociationservice.note;

import com.devqube.crmshared.search.CrmObjectType;
import org.springframework.data.jpa.domain.Specification;

public class NoteSpecifications {

    public static Specification<Note> findByObjectType(CrmObjectType objectType) {
        return (root, query, cb) ->
                cb.equal(root.get("objectType"), objectType);
    }

    public static Specification<Note> findByObjectId(Long objectId) {
        return (root, query, cb) ->
                cb.equal(root.get("objectId"), objectId);
    }

    public static Specification<Note> findByContent(String toSearch) {
        return (root, query, cb) -> {
            if (toSearch == null) {
                return cb.conjunction();
            }
            return cb.like(cb.lower(root.get("content")), "%" + toSearch.toLowerCase() + "%");
        };
    }
}
