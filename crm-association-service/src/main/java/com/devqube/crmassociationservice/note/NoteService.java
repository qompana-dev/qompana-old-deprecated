package com.devqube.crmassociationservice.note;

import com.devqube.crmassociationservice.access.UserService;
import com.devqube.crmshared.access.model.PermissionDto;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.web.CurrentRequestUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.devqube.crmassociationservice.note.NoteSpecifications.*;

@Service
public class NoteService {

    private final NoteRepository noteRepository;
    private final AccessService accessService;
    private final UserService userService;

    public NoteService(NoteRepository noteRepository, AccessService accessService, UserService userService) {
        this.noteRepository = noteRepository;
        this.accessService = accessService;
        this.userService = userService;
    }

    public Note save(Note note) throws PermissionDeniedException {
        if (isAddPermissionDenied(note.getObjectType())) {
            throw new PermissionDeniedException();
        }
        if (isUserNotePermissionDenied(note.getCreatedBy(), note.getObjectType(), note.getObjectId())) {
            throw new PermissionDeniedException();
        }
        return noteRepository.save(note);
    }

    public Note modify(Long id, Note note) throws EntityNotFoundException, PermissionDeniedException {
        if (isEditPermissionDenied(note.getObjectType())) {
            throw new PermissionDeniedException();
        }
        if (isUserNotePermissionDenied(note.getUpdatedBy(), note.getObjectType(), note.getObjectId())) {
            throw new PermissionDeniedException();
        }
        Note fromDb = noteRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        fromDb.setObjectId(note.getObjectId());
        fromDb.setObjectType(note.getObjectType());
        fromDb.setContent(note.getContent());
        fromDb.setFormattedContent(note.getFormattedContent());
        fromDb.setUpdatedBy(note.getUpdatedBy());
        return noteRepository.save(fromDb);
    }

    @Transactional
    public void deleteByObjectTypeAndObjectId(CrmObjectType objectType, Long objectId) throws PermissionDeniedException, EntityNotFoundException {
        Long userId = userService.getUserId(CurrentRequestUtil.getLoggedInAccountEmail());
        if (isUserNotePermissionDenied(userId, objectType, objectId)) {
            throw new PermissionDeniedException();
        }
        noteRepository.deleteAllByObjectTypeAndObjectId(objectType, objectId);
    }

    public Note getById(Long id) throws EntityNotFoundException, PermissionDeniedException {
        Note note = noteRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        if (isReadPermissionDenied(note.getObjectType())) {
            throw new PermissionDeniedException();
        }
        if (isUserNotePermissionDenied(note.getUpdatedBy(), note.getObjectType(), note.getObjectId())) {
            throw new PermissionDeniedException();
        }
        return note;
    }

    public void deleteById(Long id) throws EntityNotFoundException, PermissionDeniedException {
        Note note = noteRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        if (isDeletePermissionDenied(note.getObjectType())) {
            throw new PermissionDeniedException();
        }
        Long userId = userService.getUserId(CurrentRequestUtil.getLoggedInAccountEmail());
        if (isUserNotePermissionDenied(userId, note.getObjectType(), note.getObjectId())) {
            throw new PermissionDeniedException();
        }
        noteRepository.deleteById(id);
    }

    public List<Note> getAllByObjectTypeAndObjectId(CrmObjectType objectType, Long objectId) throws PermissionDeniedException, EntityNotFoundException {
        if (isReadPermissionDenied(objectType)) {
            throw new PermissionDeniedException();
        }
        Long userId = userService.getUserId(CurrentRequestUtil.getLoggedInAccountEmail());
        if (isUserNotePermissionDenied(userId, objectType, objectId)) {
            throw new PermissionDeniedException();
        }
        return noteRepository.findAllByObjectTypeAndObjectIdOrderByCreatedDesc(objectType, objectId);
    }

    public Page<Note> getAllByObjectTypeAndObjectIdAsPage(CrmObjectType objectType, Long objectId, String term, Pageable page) throws PermissionDeniedException, EntityNotFoundException {
        if (isReadPermissionDenied(objectType)) {
            throw new PermissionDeniedException();
        }
        Long userId = userService.getUserId(CurrentRequestUtil.getLoggedInAccountEmail());
        if (isUserNotePermissionDenied(userId, objectType, objectId)) {
            throw new PermissionDeniedException();
        }
        return noteRepository.findAll(Specification.where(findByObjectType(objectType)
                        .and(findByObjectId(objectId))
                        .and(findByContent(term))),
                page);
    }

    private boolean isReadPermissionDenied(CrmObjectType type) {
        switch (type) {
            case lead:
                return accessService.hasAccess("LeadNotes.view", PermissionDto.State.INVISIBLE, false);
            case customer:
                return accessService.hasAccess("CustomerNotes.view", PermissionDto.State.INVISIBLE, false);
            case contact:
                return accessService.hasAccess("ContactNotes.view", PermissionDto.State.INVISIBLE, false);
            case opportunity:
                return accessService.hasAccess("OpportunityNotes.view", PermissionDto.State.INVISIBLE, false);
            case user:
                return false; // isUserNotePermissionDenied()
            default:
                return true;
        }
    }

    private boolean isAddPermissionDenied(CrmObjectType type) {
        switch (type) {
            case lead:
                return accessService.hasAccess("LeadNotes.add", PermissionDto.State.WRITE, true);
            case customer:
                return accessService.hasAccess("CustomerNotes.add", PermissionDto.State.WRITE, true);
            case contact:
                return accessService.hasAccess("ContactNotes.add", PermissionDto.State.INVISIBLE, false);
            case opportunity:
                return accessService.hasAccess("OpportunityNotes.add", PermissionDto.State.WRITE, true);
            case user:
                return false; // isUserNotePermissionDenied()
            default:
                return true;
        }
    }

    private boolean isEditPermissionDenied(CrmObjectType type) {
        switch (type) {
            case lead:
                return accessService.hasAccess("LeadNotes.edit", PermissionDto.State.WRITE, true);
            case customer:
                return accessService.hasAccess("CustomerNotes.edit", PermissionDto.State.WRITE, true);
            case contact:
                return accessService.hasAccess("ContactNotes.edit", PermissionDto.State.INVISIBLE, false);
            case opportunity:
                return accessService.hasAccess("OpportunityNotes.edit", PermissionDto.State.WRITE, true);
            case user:
                return false; // isUserNotePermissionDenied()
            default:
                return true;
        }
    }

    private boolean isDeletePermissionDenied(CrmObjectType type) {
        switch (type) {
            case lead:
                return accessService.hasAccess("LeadNotes.remove", PermissionDto.State.WRITE, true);
            case customer:
                return accessService.hasAccess("CustomerNotes.remove", PermissionDto.State.WRITE, true);
            case contact:
                return accessService.hasAccess("ContactNotes.remove", PermissionDto.State.INVISIBLE, false);
            case opportunity:
                return accessService.hasAccess("OpportunityNotes.remove", PermissionDto.State.WRITE, true);
            case user:
                return false; // isUserNotePermissionDenied()
            default:
                return true;
        }
    }

    public void copyNotes(AssociationDetailDto dto) {
        List<Note> notes = noteRepository.findAllByObjectTypeAndObjectIdOrderByCreatedDesc(dto.getSourceType(), dto.getSourceId());
        List<Note> copy = notes.stream().map(note -> copyNote(note, dto.getDestinationType(), dto.getDestinationId())).collect(Collectors.toList());
        noteRepository.saveAll(copy);
    }

    public List<Note> findAllContaining(String pattern) throws EntityNotFoundException {
        Set<CrmObjectType> availableTypes = Stream.of(
                CrmObjectType.lead,
                CrmObjectType.customer,
                CrmObjectType.contact,
                CrmObjectType.opportunity
        ).filter(c -> !isReadPermissionDenied(c)).collect(Collectors.toSet());
        availableTypes.add(CrmObjectType.user);

        Long userId = userService.getUserId(CurrentRequestUtil.getLoggedInAccountEmail());

        return noteRepository.findByNameContainingAndNoteTypesForUser(pattern, availableTypes, userId);
    }

    private Note copyNote(Note source, CrmObjectType type, Long id) {
        return Note.builder()
                .objectType(type)
                .objectId(id)
                .updated(source.getUpdated())
                .created(source.getCreated())
                .content(source.getContent())
                .formattedContent(source.getFormattedContent())
                .build();
    }

    @Transactional
    public void copyNotes(List<AssociationDetailDto> dtos) {
        dtos.forEach(this::copyNotes);
    }


    private boolean isUserNotePermissionDenied(Long userId, CrmObjectType objectType, Long objectId) throws PermissionDeniedException {
        return (objectType == CrmObjectType.user && !userId.equals(objectId));
    }
}
