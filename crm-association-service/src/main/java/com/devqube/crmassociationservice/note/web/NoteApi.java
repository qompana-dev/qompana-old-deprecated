
package com.devqube.crmassociationservice.note.web;

import com.devqube.crmassociationservice.ApiUtil;
import com.devqube.crmassociationservice.note.Note;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.association.dto.NoteDTO;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "Note")
public interface NoteApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Create note", nickname = "createNote", notes = "Method used to create note", response = com.devqube.crmassociationservice.note.Note.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "note created successfully", response = com.devqube.crmassociationservice.note.Note.class),
            @ApiResponse(code = 400, message = "validation error")})
    @RequestMapping(value = "/note",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<com.devqube.crmassociationservice.note.Note> createNote(@ApiParam(value = "note to be saved", required = true) @Valid @RequestBody NoteDTO noteDTO, @RequestHeader(value = "Logged-Account-Email", required = true) String email) throws EntityNotFoundException, PermissionDeniedException {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Delete all notes by type and id", nickname = "deleteAllByObjectTypeAndObjectId", notes = "Method used to delete all notes by type and id")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "notes removed successfully")})
    @RequestMapping(value = "/note/type/{type}/id/{id}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteAllByObjectTypeAndObjectId(@ApiParam(value = "object type", required = true, defaultValue = "null") @PathVariable("type") CrmObjectType type, @ApiParam(value = "object ID", required = true) @PathVariable("id") Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Delete note by id", nickname = "deleteById", notes = "Method used to delete note by id")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "note removed successfully"),
            @ApiResponse(code = 404, message = "note not found")})
    @RequestMapping(value = "/note/{id}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteById(@ApiParam(value = "object ID", required = true) @PathVariable("id") Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Delete note by id", nickname = "deleteById", notes = "Method used to delete note by id")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "note removed successfully"),
            @ApiResponse(code = 404, message = "note not found")})
    @RequestMapping(value = "/internal/note/{id}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteByIdInternal(@ApiParam(value = "object ID", required = true) @PathVariable("id") Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "Get notes by type and id", nickname = "getAllByObjectTypeAndObjectId", notes = "Method used to get notes by type and id", response = com.devqube.crmassociationservice.note.Note.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all notes by type and id", response = com.devqube.crmassociationservice.note.Note.class, responseContainer = "List")})
    @RequestMapping(value = "/note/type/{type}/id/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmassociationservice.note.Note>> getAllByObjectTypeAndObjectId(@ApiParam(value = "object type", required = true, defaultValue = "null") @PathVariable("type") CrmObjectType type, @ApiParam(value = "object id", required = true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get note by id", nickname = "getById", notes = "Method used to get note by id", response = com.devqube.crmassociationservice.note.Note.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "note created successfully", response = com.devqube.crmassociationservice.note.Note.class),
            @ApiResponse(code = 404, message = "note not found")})
    @RequestMapping(value = "/note/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<com.devqube.crmassociationservice.note.Note> getById(@ApiParam(value = "object ID", required = true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "Get notes by type and id as page", nickname = "getAllByObjectTypeAndObjectId", notes = "Method used to get notes by type and id", response = com.devqube.crmassociationservice.note.Note.class, responseContainer = "Page")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all notes by type and id as page", response = com.devqube.crmassociationservice.note.Note.class, responseContainer = "List")})
    @GetMapping(value = "/note/type/{type}/id/{id}/page")
    ResponseEntity<Page<Note>> getAllByObjectTypeAndObjectIdAsPage(@PathVariable("type") CrmObjectType type,
                                                                   @PathVariable("id") Long id,
                                                                   @RequestParam(value = "term", required = false) String term,
                                                                   Pageable page);

    @ApiOperation(value = "Modify note", nickname = "modifyNote", notes = "Method used to modify note", response = com.devqube.crmassociationservice.note.Note.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "note modified successfully", response = com.devqube.crmassociationservice.note.Note.class),
            @ApiResponse(code = 400, message = "validation error")})
    @RequestMapping(value = "/note/{id}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<com.devqube.crmassociationservice.note.Note> modifyNote(@ApiParam(value = "object ID", required = true) @PathVariable("id") Long id, @ApiParam(value = "note to be modified", required = true) @Valid @RequestBody NoteDTO noteDTO, @RequestHeader(value = "Logged-Account-Email", required = true) String email) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    @ApiOperation(value = "copy notes", nickname = "copyNotes", notes = "copy notes from source to destination object")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "association created successfully")})
    @RequestMapping(value = "/internal/note/copy",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Void> copyNotes(@ApiParam(value = "association info", required = true) @Valid @RequestBody List<AssociationDetailDto> associationDetailDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    @ApiOperation(value = "Get note list for search", nickname = "getNotesForSearch", notes = "Method used to get note list", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all customer names", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/note/search",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CrmObject>> getNotesForSearch(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term) throws EntityNotFoundException {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}

