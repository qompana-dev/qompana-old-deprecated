package com.devqube.crmassociationservice.note.web;

import com.devqube.crmassociationservice.access.UserService;
import com.devqube.crmassociationservice.note.Note;
import com.devqube.crmassociationservice.note.NoteService;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.association.dto.NoteDTO;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class NoteController implements NoteApi {
    private final NoteService noteService;
    private final ModelMapper modelMapper;
    private final UserService userService;

    public NoteController(NoteService noteService, ModelMapper modelMapper, UserService userService) {
        this.noteService = noteService;
        this.modelMapper = modelMapper;
        this.userService = userService;
    }

    @Override
    public ResponseEntity<Note> createNote(@Valid NoteDTO noteDTO, String email) throws EntityNotFoundException, PermissionDeniedException {
        Note noteToSave = modelMapper.map(noteDTO, Note.class);
        Long userId = userService.getUserId(email);
        noteToSave.setCreatedBy(userId);
        noteToSave.setUpdatedBy(userId);
        noteToSave.setObjectType(CrmObjectType.valueOf(noteDTO.getObjectType().toString()));
        return new ResponseEntity<>(noteService.save(noteToSave), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> deleteById(Long id) {
        try {
            noteService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.info("Trying to delete not existing note with id {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    public ResponseEntity<Void> deleteByIdInternal(Long id) {
        try {
            noteService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.info("Trying to delete not existing note with id {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    public ResponseEntity<List<Note>> getAllByObjectTypeAndObjectId(CrmObjectType type, Long id) {
        try {
            return new ResponseEntity<>(noteService.getAllByObjectTypeAndObjectId(type, id), HttpStatus.OK);
        } catch (PermissionDeniedException | EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    public ResponseEntity<Page<Note>> getAllByObjectTypeAndObjectIdAsPage(CrmObjectType type, Long id, String term, Pageable page) {
        try {
            return new ResponseEntity<>(noteService.getAllByObjectTypeAndObjectIdAsPage(type, id, term, page), HttpStatus.OK);
        } catch (PermissionDeniedException | EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    public ResponseEntity<Note> modifyNote(Long id, @Valid NoteDTO noteDTO, String email) {
        try {
            Note noteToUpdate = modelMapper.map(noteDTO, Note.class);
            Long userId = userService.getUserId(email);
            noteToUpdate.setUpdatedBy(userId);
            noteToUpdate.setObjectType(CrmObjectType.valueOf(noteDTO.getObjectType().toString()));
            return new ResponseEntity<>(noteService.modify(id, noteToUpdate), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Trying to modify not existing note with id {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    public ResponseEntity<Void> copyNotes(@Valid List<AssociationDetailDto> associationDetailDto) {
        noteService.copyNotes(associationDetailDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getNotesForSearch(@NotNull @Valid String term) throws EntityNotFoundException {
        return new ResponseEntity<>(noteService.findAllContaining(term)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.note, getSubstring(e.getContent(), 30), e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Note> getById(Long id) {
        try {
            return new ResponseEntity<>(noteService.getById(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    private String getSubstring(String text, int maxLength) {
        return text.substring(0, Math.min(text.length(), maxLength));
    }
}
