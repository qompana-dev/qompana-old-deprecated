package com.devqube.crmassociationservice.note;

import com.devqube.crmshared.search.CrmObjectType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface NoteRepository extends JpaRepository<Note, Long>, JpaSpecificationExecutor<Note> {

    void deleteAllByObjectTypeAndObjectId(CrmObjectType objectType, Long objectId);

    List<Note> findAllByObjectTypeAndObjectIdOrderByCreatedDesc(CrmObjectType objectType, Long objectId);

    Page<Note> findAllByObjectTypeAndObjectId(CrmObjectType objectType, Long objectId, Pageable pageable);

    @Query("select n from Note n where " +
            "LOWER(n.content) LIKE LOWER(concat('%', :pattern, '%')) and " +
            "n.objectType in :types and " +
            "(n.objectType <> 'user' or n.objectId = :userId)")
    List<Note> findByNameContainingAndNoteTypesForUser(@Param("pattern") String pattern, @Param("types") Set<CrmObjectType> types, @Param("userId") Long userId);

}
