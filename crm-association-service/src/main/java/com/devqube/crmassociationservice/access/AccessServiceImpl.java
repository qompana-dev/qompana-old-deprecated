package com.devqube.crmassociationservice.access;

import com.devqube.crmshared.access.model.ProfileDto;
import com.devqube.crmshared.access.web.AbstractAccessService;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.web.CurrentRequestUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
@Aspect
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AccessServiceImpl extends AbstractAccessService implements AccessService {
    @Autowired
    @Lazy
    private UserClient userClient;

    @Value("${ignore.access.check:false}")
    private boolean ignoreAccessCheck;

    @Override
    public ProfileDto getLoggedUserProfile() {
        return userClient.getBasicProfile(CurrentRequestUtil.getLoggedInAccountEmail());
    }

    @Override
    public boolean getIgnoreAccessCheck() {
        return ignoreAccessCheck;
    }

    @Around(AbstractAccessService.AROUND_ASPECT)
    @Override
    public Object controllerMethodsAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        return super.controllerMethodsAdvice(joinPoint);
    }
}
