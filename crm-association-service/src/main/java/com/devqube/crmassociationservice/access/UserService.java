package com.devqube.crmassociationservice.access;

import com.devqube.crmshared.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserClient userClient;

    public UserService(UserClient userClient) {
        this.userClient = userClient;
    }

    public Long getUserId(String accountEmail) throws EntityNotFoundException {
        try {
            return userClient.getMyAccountId(accountEmail);
        } catch (Exception e) {
            throw new EntityNotFoundException("user not found");
        }
    }

}
