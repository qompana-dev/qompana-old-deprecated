package com.devqube.crmassociationservice.kafka;

import com.devqube.crmassociationservice.association.AssociationService;
import com.devqube.crmassociationservice.note.NoteService;
import com.devqube.crmshared.kafka.AbstractKafkaService;
import com.devqube.crmshared.kafka.ServiceKafkaAddr;
import com.devqube.crmshared.kafka.annotation.KafkaReceiver;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.search.CrmObjectType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class KafkaServiceImpl extends AbstractKafkaService {
    @Value("${ignore.kafka:false}")
    private boolean ignoreKafka;

    private final AssociationService associationService;
    private final NoteService noteService;

    public KafkaServiceImpl(AssociationService associationService, NoteService noteService) {
        this.associationService = associationService;
        this.noteService = noteService;
    }

    @Override
    public boolean isKafkaIgnored() {
        return ignoreKafka;
    }

    @KafkaListener(topics = ServiceKafkaAddr.CRM_ASSOCIATION_SERVICE)
    public void processMessage(String content) {
        super.processMessage(content);
    }

    @KafkaReceiver(from = KafkaMsgType.ASSOCIATION_REMOVE_ASSOCIATION)
    public void removeAssociationsAndNotes(KafkaMessage kafkaMessage) {
        log.info(kafkaMessage.getDestination().name());
        try {
            Optional<String> type = kafkaMessage.getValueByKey("type");
            Optional<String> id = kafkaMessage.getValueByKey("id");
            if (id.isPresent() && type.isPresent()) {
                if (Arrays.stream(CrmObjectType.values()).map(Enum::name).collect(Collectors.toList()).contains(type.get())) {
                    CrmObjectType objectType = CrmObjectType.valueOf(type.get());
                    long objectId = Long.parseLong(id.get());
                    associationService.deleteAssociationsByTypeAndId(objectType, objectId);
                    noteService.deleteByObjectTypeAndObjectId(objectType, objectId);
                } else {
                    log.error("type " + type.get() + " not exist in CrmObjectType");
                }
            } else {
                log.error("type or id is null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receiverNotFound(KafkaMessage kafkaMessage) {
        log.info("receiverNotFound" + kafkaMessage.getDestination().name());
    }
}
