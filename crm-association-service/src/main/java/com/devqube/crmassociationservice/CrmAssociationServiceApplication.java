package com.devqube.crmassociationservice;

import com.devqube.crmshared.license.LicenseCheck;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@EnableFeignClients
@Import({LicenseCheck.class})
public class CrmAssociationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrmAssociationServiceApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
