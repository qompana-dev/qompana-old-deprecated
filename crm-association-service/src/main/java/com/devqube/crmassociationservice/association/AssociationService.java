package com.devqube.crmassociationservice.association;

import com.devqube.crmassociationservice.association.model.Association;
import com.devqube.crmassociationservice.association.model.AssociationProperty;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.association.dto.AssociationPropertyDto;
import com.devqube.crmshared.search.CrmObjectType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AssociationService {
    private final AssociationRepository associationRepository;

    public AssociationService(AssociationRepository associationRepository) {
        this.associationRepository = associationRepository;
    }

    public List<Association> getAssociationsByType(CrmObjectType crmObjectType) {
        return associationRepository.findAllBySourceType(crmObjectType);
    }

    public List<Association> getAssociationsByTypeAndId(Long id, CrmObjectType crmObjectType) {
        return associationRepository.findAllBySourceIdAndSourceType(id, crmObjectType);
    }

    public List<Association> getAssociationsBySourceTypeAndSourceIdAndDestinationType(CrmObjectType sourceType, CrmObjectType destinationType, Long sourceId) {
        return associationRepository.findAllBySourceIdAndSourceTypeAndDestinationType(sourceId, sourceType, destinationType);
    }

    public List<Association> getAllBySourceTypeAndSourceIdsAndDestinationType(CrmObjectType type, CrmObjectType destinationType, List<Long> ids) {
        return associationRepository.findAllBySourceIdInAndSourceTypeAndDestinationType(ids, type, destinationType);
    }

    @Transactional
    public List<Association> updateAssociations(CrmObjectType type, Long id, List<AssociationDetailDto> associationDetailList) {
        associationRepository.deleteAllBySourceIdAndSourceType(id, type);
        associationRepository.deleteAllByDestinationIdAndDestinationType(id, type);
        return addAssociations(associationDetailList);
    }

    @Transactional
    public List<Association> updateAssociationsForTypes(CrmObjectType type, Long id, List<CrmObjectType> objectTypes, List<AssociationDetailDto> associationDetailList) {
        objectTypes.forEach(objectType -> {
            associationRepository.deleteAllBySourceIdAndSourceTypeAndDestinationType(id, type, objectType);
            associationRepository.deleteAllByDestinationIdAndDestinationTypeAndSourceType(id, type, objectType);
        });
        return addAssociations(associationDetailList.stream().filter(c -> objectTypes.contains(c.getSourceType()) || objectTypes.contains(c.getDestinationType())).collect(Collectors.toList()));
    }

    public List<Association> addAssociations(List<AssociationDetailDto> associations) {
        return associations.stream().map(this::addAssociation).collect(Collectors.toList());
    }

    @Transactional
    public Association addAssociation(AssociationDetailDto association) {
        Optional<Association> fromDb = findByAssociationDetail(association);

        AssociationDetailDto invertedAssociationDetail = getInvertedAssociation(association);
        Optional<Association> invertedAssociation = findByAssociationDetail(invertedAssociationDetail);

        if (invertedAssociation.isEmpty()) {
            associationRepository.save(toModel(invertedAssociationDetail));
        }
        return (fromDb.isEmpty()) ? associationRepository.save(toModel(association)) : fromDb.get();
    }

    @Transactional
    public void removeAssociations(List<AssociationDetailDto> association) {
        association.forEach(this::removeAssociation);
    }

    @Transactional
    public void removeAssociation(AssociationDetailDto association) {
        Optional<Association> fromDb = findByAssociationDetail(association);
        Optional<Association> invertedFromDb = findByAssociationDetail(getInvertedAssociation(association));

        fromDb.ifPresent(c -> associationRepository.deleteById(c.getId()));
        invertedFromDb.ifPresent(c -> associationRepository.deleteById(c.getId()));
    }

    @Transactional
    public void deleteAssociationsByTypeAndId(CrmObjectType type, Long id) {
        log.info("deleteAssociationsByTypeAndId type=" + type.name() + ", id=" + id);
        this.associationRepository.deleteAllBySourceIdAndSourceType(id, type);
        this.associationRepository.deleteAllByDestinationIdAndDestinationType(id, type);
    }
    @Transactional
    public void deleteAssociationsBySourceTypeAndSourceIdAndDestinationType(CrmObjectType sourceType, Long sourceId, CrmObjectType destinationType) {
        associationRepository.deleteAllBySourceIdAndSourceTypeAndDestinationType(sourceId, sourceType, destinationType);
        associationRepository.deleteAllByDestinationIdAndDestinationTypeAndSourceType(sourceId, sourceType, destinationType);
    }

    private Optional<Association> findByAssociationDetail(AssociationDetailDto association) {
        return associationRepository.findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(
                association.getSourceId(), association.getSourceType(),
                association.getDestinationId(), association.getDestinationType());
    }

    private AssociationDetailDto getInvertedAssociation(AssociationDetailDto association) {
        return AssociationDetailDto.builder()
                .destinationType(association.getSourceType())
                .destinationId(association.getSourceId())
                .sourceType(association.getDestinationType())
                .sourceId(association.getDestinationId())
                .properties(association.getProperties()).build();
    }

    public Association toModel(AssociationDetailDto detail) {
        Association result = Association.builder()
                .sourceId(detail.getSourceId())
                .sourceType(detail.getSourceType())
                .destinationId(detail.getDestinationId())
                .destinationType(detail.getDestinationType()).build();
        if (detail.getProperties() != null) {
            result.setAssociationProperties(detail.getProperties().stream().map(c -> toModel(c, result)).collect(Collectors.toList()));
        }
        return result;
    }

    private AssociationProperty toModel(AssociationPropertyDto property, Association association) {
        return new AssociationProperty(property.getId(), property.getType(), property.getValue(), association);
    }

    @Transactional
    public void copyAssociations(List<AssociationDetailDto> dtos) {
        dtos.forEach(this::copyAssociations);
    }

    public List<Association> copyAssociations(AssociationDetailDto dto) {
        List<Association> sourceAssociations = associationRepository.findAllBySourceIdAndSourceType(dto.getSourceId(), dto.getSourceType());
        List<AssociationDetailDto> newAssociations = sourceAssociations.stream().map(association -> new AssociationDetailDto(
                dto.getDestinationType(),
                dto.getDestinationId(),
                association.getDestinationType(),
                association.getDestinationId(),
                createAssociationPropertyDto(association.getAssociationProperties())
        )).collect(Collectors.toList());
        return this.addAssociations(newAssociations);
    }

    private List<AssociationPropertyDto> createAssociationPropertyDto(List<AssociationProperty> properties) {
        return properties.stream().map(
                property -> new AssociationPropertyDto(null, property.getType(), property.getValue()))
                .collect(Collectors.toList());
    }


}
