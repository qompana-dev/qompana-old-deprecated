package com.devqube.crmassociationservice.association.model;

import com.devqube.crmshared.search.CrmObjectType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Association {
    @JsonProperty("id")
    @Id
    @SequenceGenerator(name = "association_seq", sequenceName = "association_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "association_seq")
    @EqualsAndHashCode.Include
    private Long id;

    @Enumerated(EnumType.STRING)
    @NotNull
    private CrmObjectType sourceType;

    @NotNull
    private Long sourceId;

    @Enumerated(EnumType.STRING)
    @NotNull
    private CrmObjectType destinationType;

    @NotNull
    private Long destinationId;

    @OneToMany(mappedBy = "association", orphanRemoval = true, cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    private List<AssociationProperty> associationProperties = new ArrayList<>();
}
