package com.devqube.crmassociationservice.association.web;


import com.devqube.crmassociationservice.association.AssociationService;
import com.devqube.crmassociationservice.association.model.Association;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.search.CrmObjectType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
public class AssociationsController implements AssociationsApi {

    private final AssociationService associationService;

    public AssociationsController(AssociationService associationService) {
        this.associationService = associationService;
    }

    @Override
    public ResponseEntity<Association> createAssociation(@Valid AssociationDetailDto associationDetailDto) {
        return new ResponseEntity<>(associationService.addAssociation(associationDetailDto), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<Association>> createAssociations(@Valid List<AssociationDetailDto> associationDetailDto) {
        return new ResponseEntity<>(associationService.addAssociations(associationDetailDto), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> deleteAssociation(@Valid AssociationDetailDto associationDetailDto) {
        associationService.removeAssociation(associationDetailDto);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<Void> deleteAssociations(@Valid List<AssociationDetailDto> associationDetailDto) {
        associationService.removeAssociations(associationDetailDto);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<List<Association>> getAllByType(CrmObjectType type) {
        return new ResponseEntity<>(associationService.getAssociationsByType(type), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Association>> getAllByTypeAndId(CrmObjectType type, Long id) {
        return new ResponseEntity<>(associationService.getAssociationsByTypeAndId(id, type), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteAssociationsByTypeAndId(CrmObjectType type, Long id) {
        associationService.deleteAssociationsByTypeAndId(type, id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<List<Association>> getAllByTypeAndIdAndDestinationType(CrmObjectType type, CrmObjectType destinationType, Long id) {
        return new ResponseEntity<>(associationService.getAssociationsBySourceTypeAndSourceIdAndDestinationType(type, destinationType, id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Association>> getAllBySourceTypeAndSourceIdsAndDestinationType(CrmObjectType type, CrmObjectType destinationType, List<Long> ids) {
        return new ResponseEntity<>(associationService.getAllBySourceTypeAndSourceIdsAndDestinationType(type, destinationType, ids), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Association>> updateAssociations(CrmObjectType type, Long id, @Valid List<AssociationDetailDto> associationDetailDto) {
        return new ResponseEntity<>(associationService.updateAssociations(type, id, associationDetailDto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Association>> updateAssociationsForTypes(CrmObjectType type, List<CrmObjectType> objectTypes, Long id, @Valid List<AssociationDetailDto> associationDetailDto) {
        return new ResponseEntity<>(associationService.updateAssociationsForTypes(type, id, objectTypes, associationDetailDto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteAssociationsBySourceTypeAndSourceIdAndDestinationType(CrmObjectType sourceType, Long sourceId, CrmObjectType destinationType) {
        associationService.deleteAssociationsBySourceTypeAndSourceIdAndDestinationType(sourceType, sourceId, destinationType);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> copyAssociations(@Valid List<AssociationDetailDto> associationDetailDto) {
        associationService.copyAssociations(associationDetailDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
