package com.devqube.crmassociationservice.association;

import com.devqube.crmassociationservice.association.model.Association;
import com.devqube.crmshared.search.CrmObjectType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface AssociationRepository extends JpaRepository<Association, Long> {
    Optional<Association> findBySourceIdAndSourceTypeAndDestinationIdAndDestinationType(Long sourceId, CrmObjectType sourceType, Long destinationId, CrmObjectType destinationType);

    List<Association> findAllBySourceIdAndSourceType(Long sourceId, CrmObjectType sourceType);

    List<Association> findAllBySourceIdAndSourceTypeAndDestinationType(Long sourceId, CrmObjectType sourceType, CrmObjectType destinationType);

    List<Association> findAllBySourceIdInAndSourceTypeAndDestinationType(List<Long> sourceIds, CrmObjectType sourceType, CrmObjectType destinationType);

    void deleteAllBySourceIdAndSourceType(Long sourceId, CrmObjectType sourceType);

    void deleteAllBySourceIdAndSourceTypeAndDestinationType(Long sourceId, CrmObjectType sourceType, CrmObjectType destinationType);

    void deleteAllByDestinationIdAndDestinationType(Long destinationId, CrmObjectType destinationType);

    void deleteAllByDestinationIdAndDestinationTypeAndSourceType(Long destinationId, CrmObjectType destinationType, CrmObjectType sourceType);

    List<Association> findAllBySourceType(CrmObjectType sourceType);
}
