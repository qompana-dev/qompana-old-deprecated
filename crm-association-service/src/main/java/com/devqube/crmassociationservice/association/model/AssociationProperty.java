package com.devqube.crmassociationservice.association.model;

import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import com.devqube.crmshared.association.dto.AssociationPropertyTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@AuthFilterEnabled
public class AssociationProperty {

    @Id
    @SequenceGenerator(name = "association_property_seq", sequenceName = "association_property_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "association_property_seq")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private AssociationPropertyTypeEnum type;

    @NotNull
    private String value;

    @JsonIgnore
    @ManyToOne
    @ToString.Exclude
    private Association association;
}
