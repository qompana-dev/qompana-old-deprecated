create TABLE association
(
    id               bigint PRIMARY KEY,
    source_id        bigint       NOT NULL,
    source_type      VARCHAR(200) NOT NULL,
    destination_id   bigint       NOT NULL,
    destination_type VARCHAR(200) NOT NULL,
    UNIQUE (source_id, source_type, destination_id, destination_type)
);
create sequence association_seq start 1;

create TABLE association_property
(
    id             BIGINT PRIMARY KEY,
    association_id BIGINT  NOT NULL,
    type           VARCHAR NOT NULL,
    value          VARCHAR,
    CONSTRAINT association_id_fk FOREIGN KEY (association_id)
        REFERENCES association (id)
);
create sequence association_property_seq start 1;

create TABLE note
(
    id          bigint PRIMARY KEY,
    object_id   bigint        NOT NULL,
    object_type VARCHAR(200)  NOT NULL,
    content     VARCHAR(3000) NOT NULL,
    created     timestamp,
    updated     timestamp,
    updated_by  bigint,
    created_by  bigint
);
create sequence note_seq start 1;
