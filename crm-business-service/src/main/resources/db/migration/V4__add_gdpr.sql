CREATE TABLE gdpr
(
    id                    BIGINT PRIMARY KEY,
    contact_id            BIGINT NOT NULL unique,
    token                 VARCHAR(255) unique,
    token_expiration_date timestamp,
    CONSTRAINT token_validation_check
        CHECK ( token is null or (token is not null and token_expiration_date is not null) )
);
CREATE SEQUENCE gdpr_seq START 1;

CREATE TABLE gdpr_config
(
    id           BIGINT PRIMARY KEY,
    mail_title   VARCHAR(255) NOT NULL,
    mail_content TEXT         NOT NULL,
    form_content TEXT         NOT NULL
);
CREATE SEQUENCE gdpr_config_seq START 1;

insert into gdpr_config (id, mail_title, mail_content, form_content)
values (1, '', '', '');

CREATE TABLE gdpr_status
(
    id             BIGINT PRIMARY KEY,
    name           VARCHAR(255) NOT NULL unique,
    modifiable     BOOLEAN DEFAULT TRUE,
    default_status VARCHAR(55)  NOT NULL,
    CONSTRAINT default_status_check
        CHECK (default_status = 'NONE' OR default_status = 'CHECKED' OR default_status = 'UNCHECKED')
);
CREATE SEQUENCE gdpr_status_seq START 1;

insert into gdpr_status(id, name, modifiable, default_status)
VALUES (-4, 'Wyrażona', FALSE, 'CHECKED'),
       (-3, 'Niewyrażona', FALSE, 'UNCHECKED'),
       (-2, 'Nieustalona', FALSE, 'NONE'),
       (-1, 'Wycofana', FALSE, 'NONE');

CREATE TABLE gdpr_type
(
    id           BIGINT PRIMARY KEY,
    name         VARCHAR(255) NOT NULL unique,
    description  TEXT,
    is_mail_type BOOLEAN DEFAULT FALSE
);
CREATE SEQUENCE gdpr_type_seq START 1;


INSERT INTO gdpr_type(id, name, description, is_mail_type)
VALUES (-1, 'Zgoda na kontakt mailowy', 'Zgoda na kontakt mailowy', TRUE);

CREATE TABLE gdpr_approve_way
(
    id             BIGINT PRIMARY KEY,
    name           VARCHAR(255) NOT NULL unique,
    is_default_way BOOLEAN DEFAULT FALSE,
    modifiable     BOOLEAN DEFAULT TRUE
);
CREATE SEQUENCE gdpr_approve_way_seq START 1;

INSERT INTO gdpr_approve_way(id, name, is_default_way, modifiable)
VALUES (-1, 'Przez formularz', TRUE, FALSE);

CREATE TABLE gdpr_approve
(
    id                  BIGINT PRIMARY KEY,
    gdpr_id             BIGINT    NOT NULL,
    gdpr_status_id      BIGINT    NOT NULL,
    gdpr_type_id        BIGINT    NOT NULL,
    gdpr_approve_way_id BIGINT    NOT NULL,
    changed_by          BIGINT    NOT NULL,
    modified            TIMESTAMP NOT NULL,
    CONSTRAINT gdpr_approve2gdpr_id_fk FOREIGN KEY (gdpr_id)
        REFERENCES gdpr (id),
    CONSTRAINT gdpr_status_id2gdpr_id_fk FOREIGN KEY (gdpr_status_id)
        REFERENCES gdpr_status (id),
    CONSTRAINT gdpr_type_id2gdpr_id_fk FOREIGN KEY (gdpr_type_id)
        REFERENCES gdpr_type (id),
    CONSTRAINT gdpr_approve_way_id2gdpr_id_fk FOREIGN KEY (gdpr_approve_way_id)
        REFERENCES gdpr_approve_way (id)
);
CREATE SEQUENCE gdpr_approve_seq START 1;
