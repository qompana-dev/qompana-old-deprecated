create TABLE primary_process
(
    id         bigint PRIMARY KEY,
    process_id varchar(200) NOT NULL,
    type       varchar(200) NOT NULL,
    UNIQUE (process_id, type)
);
create sequence primary_process_seq start 1;