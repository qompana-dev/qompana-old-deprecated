alter table opportunity add product_amount double precision;

ALTER TABLE opportunity
    ADD CONSTRAINT opportunity_product_amount_check
        CHECK  (product_amount >= 0);