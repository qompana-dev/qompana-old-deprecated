alter table opportunity add estimated_amount double precision;

ALTER TABLE opportunity
    ADD CONSTRAINT opportunity_estimated_amount_check
        CHECK  (estimated_amount >= 0);