create TABLE supplier
(
    id   bigint PRIMARY KEY,
    name varchar(100) NOT NULL UNIQUE
);
create sequence supplier_seq start 1;

create TABLE manufacturer
(
    id   bigint PRIMARY KEY,
    name varchar(100) NOT NULL UNIQUE
);
create sequence manufacturer_seq start 1;

create TABLE product
(
    id                      bigint PRIMARY KEY,
    name                    varchar(200),
    code                    varchar(100) not null unique,
    sku_code                varchar(100) not null unique,
    manufacturer            bigint,
    supplier                bigint,
    brand                   varchar(200),
    purchase_price          double precision,
    purchase_price_currency varchar(30),
    currency                varchar(30),
    sell_price_net          double precision,
    vat                     bigint,
    active                  boolean default false,
    description             varchar(1000),
    specification           varchar(1000),
    notes                   varchar(1000),
    deleted                 boolean default false,
    created                 TIMESTAMP,
    updated                 TIMESTAMP,
    opportunity_product     boolean default false,
    CONSTRAINT product_manufacturer_fk FOREIGN KEY (manufacturer)
        REFERENCES manufacturer (id),
    CONSTRAINT product_supplier_fk FOREIGN KEY (supplier)
        REFERENCES supplier (id)
);
create sequence product_seq start 1;

create TABLE last_viewed_product
(
    id          bigint PRIMARY KEY,
    product_id  bigint    NOT NULL,
    user_id     bigint    NOT NULL,
    last_viewed TIMESTAMP NOT NULL,
    UNIQUE (user_id, product_id),
    CONSTRAINT last_viewed_product_id FOREIGN KEY (product_id)
        REFERENCES product (id)
);
create sequence last_viewed_product_seq start 1;

create TABLE product_category
(
    id   bigint PRIMARY KEY,
    name varchar UNIQUE
);
create sequence product_category_seq start 1;

create TABLE product_subcategory
(
    id                  bigint PRIMARY KEY,
    product_category_id bigint,
    name                varchar,
    initial             bool,
    UNIQUE (product_category_id, name),
    CONSTRAINT product_category_fk FOREIGN KEY (product_category_id)
        REFERENCES product_category (id)
);
create sequence product_subcategory_seq start 1;

create TABLE product2subcategory
(
    product_id bigint NOT NULL,
    subcategory_id  bigint NOT NULL,
    CONSTRAINT product_product_id2product_id_fk FOREIGN KEY (product_id)
        REFERENCES product (id),
    CONSTRAINT subcategory_subcategory_id2subcategory_id_fk FOREIGN KEY (subcategory_id)
        REFERENCES product_subcategory (id)
);

create TABLE price_book
(
    id          bigint PRIMARY KEY,
    name        varchar(200) not null unique,
    description varchar(1000),
    currency    varchar(30),
    active      boolean default false,
    is_default  boolean default false,
    created     TIMESTAMP,
    updated     TIMESTAMP,
    deleted     boolean default false
);
create sequence price_book_seq start 1;

create TABLE product_price
(
    id            bigint PRIMARY KEY,
    product_id    bigint           NOT NULL,
    price_book_id bigint           NOT NULL,
    price         double precision NOT NULL,
    active        boolean          NOT NULL default false,
    UNIQUE (product_id, price_book_id),
    CONSTRAINT product_price_product_id FOREIGN KEY (product_id)
        REFERENCES product (id),
    CONSTRAINT product_price_price_book_id FOREIGN KEY (price_book_id)
        REFERENCES price_book (id)
);
create sequence product_price_seq start 1;

create TABLE last_viewed_price_book
(
    id            bigint PRIMARY KEY,
    price_book_id bigint    NOT NULL,
    user_id       bigint    NOT NULL,
    last_viewed   TIMESTAMP NOT NULL,
    UNIQUE (user_id, price_book_id),
    CONSTRAINT last_viewed_product_id FOREIGN KEY (price_book_id)
        REFERENCES price_book (id)
);
create sequence last_viewed_price_book_seq start 1;







create table company_type
(
    id   bigint primary key,
    type varchar(300) not null unique
);

create sequence company_type_seq start 1;

create table company
(
    id                bigint primary key,
    company_type_id   bigint,
    company_name      varchar(300),
    www_address       varchar(200),
    street_and_number varchar(300),
    city              varchar(200),
    postal_code       varchar(20),
    province          varchar(200),
    country           varchar(200),
    notes             varchar(1000),
    constraint company_type_foreign_key foreign key (company_type_id)
        references company_type (id) ON DELETE SET NULL
);

create sequence company_seq start 1;

create table lead
(
    id                          bigint primary key,
    status                      varchar(200),
    first_name                  varchar(200),
    last_name                   varchar(200),
    position                    varchar(200),
    email                       varchar(200),
    phone                       varchar(200),
    lead_keeper_id              bigint,
    creator                     bigint not null,
    source_of_acquisition       varchar(300),
    sales_opportunity           varchar(200),
    company_id                  bigint,
    process_instance_id         varchar(200) unique,
    created                     timestamp,
    updated                     timestamp,
    acceptance_user_id          bigint,
    converted_by                bigint,
    converted_date_time         timestamp,
    deleted                     boolean default 'f',
    closed                      boolean default 'f',
    converted_to_opportunity    boolean default 'f',
    converted_to_opportunity_id bigint,
    converted_to_contact_id     bigint,
    converted_to_customer_id    bigint,
    avatar                      text,
    constraint company_foreign_key foreign key (company_id)
        references company (id),
    constraint lead_status_check
        check (status = 'NOT_ELIGIBLE' or status = 'NEW' or status = 'DURING_RECOGNITION' or
               status = 'UNDER_THE_CUSTODY'
            OR status = 'ELIGIBLE'
            ),
    CONSTRAINT sales_opportunity_check
        CHECK (sales_opportunity = 'NOT_SPECIFIED' OR sales_opportunity = 'HIGH' OR sales_opportunity = 'MEDIUM'
            OR sales_opportunity = 'LOW')
);

create sequence lead_seq start 1;

create TABLE last_viewed_lead
(
    id                         bigint PRIMARY KEY,
    last_viewed_lead_id bigint    NOT NULL,
    user_id                    bigint    NOT NULL,
    last_viewed                TIMESTAMP NOT NULL,
    UNIQUE (user_id, last_viewed_lead_id),
    CONSTRAINT last_viewed_lead_fk FOREIGN KEY (last_viewed_lead_id)
        REFERENCES lead (id)
);
create sequence last_viewed_lead_seq start 1;


create TABLE type
(
    id          bigint PRIMARY KEY,
    name        VARCHAR(200) UNIQUE NOT NULL,
    description VARCHAR(500)
);

create sequence type_seq start 1;

create TABLE address
(
    id        bigint PRIMARY KEY,
    longitude VARCHAR(20),
    latitude  VARCHAR(20),
    country   VARCHAR(100),
    city      VARCHAR(100),
    zip_code  VARCHAR(20),
    street    VARCHAR(200)
);

create sequence address_seq start 1;

create TABLE whitelist
(
    id             bigint PRIMARY KEY,
    nip            VARCHAR(10) UNIQUE  NOT NULL,
    status         VARCHAR(50) NOT NULL,
    registered     TIMESTAMP,
    updated        TIMESTAMP,
    refreshed      TIMESTAMP
);
create sequence whitelist_seq start 1;

create TABLE bank_account_number
(
    id              bigint PRIMARY KEY,
    whitelist_id    bigint not null,
    number          VARCHAR(100) NOT NULL,
    CONSTRAINT bank_account_number_whitelist_id FOREIGN KEY (whitelist_id)
        REFERENCES whitelist (id)
);
create sequence bank_account_number_seq start 1;

create TABLE customer
(
    id             bigint PRIMARY KEY,
    type_id        bigint,
    nip            VARCHAR(10) UNIQUE  NOT NULL,
    regon          VARCHAR(14),
    name           VARCHAR(200) UNIQUE NOT NULL,
    description    VARCHAR(500),
    address_id     bigint,
    whitelist_id   bigint,
    owner          bigint,
    creator        bigint,
    phone          VARCHAR(11),
    website        VARCHAR(200),
    created        TIMESTAMP,
    updated        TIMESTAMP,
    contacts_notes  VARCHAR(4096),
    employees_number bigint,
    company_type     VARCHAR(100),
    income           VARCHAR(100),
    avatar                      text,
    CONSTRAINT customer_type_id FOREIGN KEY (type_id)
        REFERENCES type (id),
    CONSTRAINT customer_address_fk FOREIGN KEY (address_id)
        REFERENCES address (id)
);

create sequence customer_seq start 1;

create TABLE last_viewed_customer
(
    id                      bigint PRIMARY KEY,
    last_viewed_customer_id bigint    NOT NULL,
    user_id                 bigint    NOT NULL,
    last_viewed             TIMESTAMP NOT NULL,
    UNIQUE (user_id, last_viewed_customer_id),
    CONSTRAINT contact_customer_id FOREIGN KEY (last_viewed_customer_id)
        REFERENCES customer (id)
);
create sequence last_viewed_customer_seq start 1;

create TABLE contact
(
    id              bigint PRIMARY KEY,
    customer_id     bigint,
    name            VARCHAR(200) NOT NULL,
    surname         VARCHAR(200) NOT NULL,
    department      VARCHAR(200),
    description     VARCHAR(500),
    email           VARCHAR(100),
    position        VARCHAR(100),
    phone           VARCHAR(11),
    created         TIMESTAMP,
    updated         TIMESTAMP,
    creator         bigint,
    facebook_link   varchar(1024),
    twitter_link    varchar(1024),
    linked_in_link  varchar(1024),
    main_contact    boolean DEFAULT false,
    owner           bigint,
    avatar          text,
    CONSTRAINT contact_customer_id FOREIGN KEY (customer_id)
        REFERENCES customer (id)
);
create sequence contact_seq start 1;

create TABLE last_viewed_contact
(
    id          bigint PRIMARY KEY,
    contact_id  bigint    NOT NULL,
    user_id     bigint    NOT NULL,
    last_viewed TIMESTAMP NOT NULL,
    UNIQUE (user_id, contact_id),
    CONSTRAINT last_viewed_contact_id FOREIGN KEY (contact_id)
        REFERENCES contact (id)
);
create sequence last_viewed_contact_seq start 1;

create TABLE contact_link
(
    id                bigint PRIMARY KEY,
    base_contact_id   bigint not null,
    linked_contact_id bigint not null,
    note              text,
    UNIQUE (base_contact_id, linked_contact_id),
    CONSTRAINT base_contact_fk FOREIGN KEY (base_contact_id) REFERENCES contact (id),
    CONSTRAINT linked_contact_fk FOREIGN KEY (linked_contact_id) REFERENCES contact (id)
);
create sequence contact_link_seq start 1;

create table opportunity
(
    id                     bigint primary key,
    name                   varchar(200),
    customer_id            bigint,
    contact_id             bigint,
    type                   varchar(200),
    amount                 double precision,
    currency               varchar(20) not null,
    probability            bigint,
    finish_date            timestamp,
    process_instance_id    varchar(200) unique,
    process_definition_id    varchar(200),
    description            varchar(3000),
    source_of_acquisition  varchar(100),
    finish_result          varchar(100),
    closing_date           timestamp,
    rejection_reason       varchar(100),
    owner_one_id           bigint,
    owner_one_percentage   bigint,
    owner_two_id           bigint,
    owner_two_percentage   bigint,
    owner_three_id         bigint,
    owner_three_percentage bigint,
    acceptance_user_id     bigint,
    creator                bigint,
    created                timestamp,
    updated                timestamp,
    converted_from_lead    boolean DEFAULT false,
    constraint opportunity_type_check check (type = 'NEW_BUSINESS' or type = 'EXISTING_BUSINESS'),
    constraint opportunity_amount_check check (amount >= 0),
    constraint opportunity_probability_check check (probability >= 0 and probability <= 100),
    constraint opportunity_source_of_acquisition_check
        check (source_of_acquisition = 'ADVERTISEMENT' or
               source_of_acquisition = 'EVENT' or
               source_of_acquisition = 'EMPLOYEE_REFERENCES' or
               source_of_acquisition = 'PARTNER' or
               source_of_acquisition = 'GOOGLE_AD_WORDS' or
               source_of_acquisition = 'OTHER'),
    CONSTRAINT opportunity_customer_id FOREIGN KEY (customer_id) REFERENCES customer (id),
    CONSTRAINT opportunity_contact_id FOREIGN KEY (contact_id) REFERENCES contact (id)
);

create sequence opportunity_seq start 1;

create TABLE last_viewed_opportunity
(
    id                         bigint PRIMARY KEY,
    last_viewed_opportunity_id bigint    NOT NULL,
    user_id                    bigint    NOT NULL,
    last_viewed                TIMESTAMP NOT NULL,
    UNIQUE (user_id, last_viewed_opportunity_id),
    CONSTRAINT last_viewed_opportunity_fk FOREIGN KEY (last_viewed_opportunity_id)
        REFERENCES opportunity (id)
);
create sequence last_viewed_opportunity_seq start 1;

create TABLE opportunity_product
(
    id             bigint PRIMARY KEY,
    opportunity_id bigint                NOT NULL,
    product_id     bigint                NOT NULL,
    price          double precision      not null,
    price_changed  boolean default false not null,
    quantity       bigint                not null,
    price_summed   double precision      not null,
    CONSTRAINT opportunity_product_product_id FOREIGN KEY (product_id)
        REFERENCES product (id),
    CONSTRAINT opportunity_product_opportunity_id FOREIGN KEY (opportunity_id)
        REFERENCES opportunity (id)
);
create sequence opportunity_product_seq start 1;

create table expense
(
    id             bigint primary key,
    name           varchar(200)     not null,
    description    varchar(1000),
    date           timestamp,
    amount         double precision not null,
    currency       varchar(50)      not null,
    type           varchar(30),
    state          varchar(30),
    lead_id        bigint,
    opportunity_id bigint,
    creator_id     bigint,
    acceptor_id    bigint,
    rejection_cause varchar(300),
    constraint expense_type_check
        check (type = 'INVOICE' or type = 'RECEIPT' or type = 'DELEGATION' or type = 'OTHER'),
    constraint expense_state_check
        check (state = 'TO_BE_ACCEPTED' or state = 'ACCEPTED' or state = 'REJECTED'),
    constraint expense_join_check
        check ((lead_id is null and opportunity_id is not null) or (lead_id is not null and opportunity_id is null))
);

create sequence expense_seq start 1;



create TABLE contact_customer_link
(
    id          bigint PRIMARY KEY,
    contact_id  bigint not null,
    customer_id bigint not null,
    relation_type varchar(200),
    position    varchar(200),
    start_date  TIMESTAMP,
    end_date    TIMESTAMP,
    note        text,
    CONSTRAINT contact_id_fk FOREIGN KEY (contact_id) REFERENCES contact (id),
    CONSTRAINT customer_id_fk FOREIGN KEY (customer_id) REFERENCES customer (id)
);
create sequence contact_customer_link_seq start 1;



create TABLE goal
(
    id              bigint PRIMARY KEY,
    name            varchar(200) not null,
    owner_id        bigint       not null,
    type            varchar(200) not null,
    interval        varchar(200) not null,
    interval_number int          not null,
    start_index     int          not null,
    start_year      int          not null,
    deleted         boolean      default 'f',
    currency        varchar(50)
);
create sequence goal_seq start 1;

create TABLE goal_to_subcategory
(
    goal_id        bigint not null,
    subcategory_id bigint not null,
    UNIQUE (goal_id, subcategory_id),
    CONSTRAINT goal_goal_to_subcategory_fk FOREIGN KEY (goal_id) REFERENCES goal (id),
    CONSTRAINT subcategory_goal_to_subcategory_fk FOREIGN KEY (subcategory_id) REFERENCES product_subcategory (id)
);
create sequence goal_to_subcategory_seq start 1;


create TABLE goal_to_product
(
    goal_id    bigint not null,
    product_id bigint not null,
    UNIQUE (goal_id, product_id),
    CONSTRAINT goal_goal_to_product_fk FOREIGN KEY (goal_id) REFERENCES goal (id),
    CONSTRAINT product_goal_to_product_fk FOREIGN KEY (product_id) REFERENCES product (id)
);
create sequence goal_to_product_seq start 1;

create TABLE user_goal
(
    id             bigint PRIMARY KEY,
    goal_id        bigint not null,
    user_id        bigint not null,
    parent_user_id bigint not null,
    UNIQUE (goal_id, user_id, parent_user_id),
    CONSTRAINT goal_user_goal_fk FOREIGN KEY (goal_id) REFERENCES goal (id)
);
create sequence user_goal_seq start 1;

create TABLE goal_interval
(
    id             bigint PRIMARY KEY,
    user_goal_id   bigint           not null,
    index          bigint           not null,
    expected_value double precision not null,
    UNIQUE (user_goal_id, index),
    CONSTRAINT user_goal_goal_interval_fk FOREIGN KEY (user_goal_id) REFERENCES user_goal (id)

);
create sequence goal_interval_seq start 1;

create table widget_goal
(
    id         bigint primary key,
    goal_id    bigint      not null,
    chart_type varchar(50) not null,
    constraint widget_goal_goal_fk foreign key (goal_id) references goal
);

create sequence widget_goal_seq start 1;

create table widget_goal_participant
(
    id             bigint primary key,
    widget_goal_id bigint  not null,
    user_id        bigint  not null,
    goal           boolean not null,
    constraint widget_goal_participant_widget_goal_fk foreign key (widget_goal_id) references widget_goal
);

create sequence widget_goal_participant_seq start 1;


create TABLE widget_report
(
    id                          bigint PRIMARY KEY,
    report_type                 VARCHAR(200) NOT NULL,
    widget_column               VARCHAR(200) NOT NULL,
    start                       TIMESTAMP NOT NULL,
    "end"                       TIMESTAMP,
    chart_type                  VARCHAR(200) NOT NULL,
    additional_value            VARCHAR(200),
    constraint widget_column_enum_check
        check (widget_column = 'CUSTOMER' or widget_column = 'CONTACT'
                or widget_column = 'TYPE' or widget_column = 'TASK'
                or widget_column = 'CURRENCY' or widget_column = 'PROCESS'
                or widget_column = 'FINISH_RESULT' or widget_column = 'REJECTION_RESULT'
                or widget_column = 'OWNER' or widget_column = 'COMPANY'
                or widget_column = 'LEAD_STATUS' or widget_column = 'PRIORITY'
                or widget_column = 'TASK_STATE' or widget_column = 'ACTIVITY_TYPE'),
    constraint chart_type_enum_check
        check (chart_type = 'CHART1' or chart_type = 'CHART2'
                or chart_type = 'CHART3' or chart_type = 'CHART4'
                or chart_type = 'CHART5')
);
create sequence widget_report_seq start 1;


create TABLE widget_report_user
(
    id               bigint PRIMARY KEY,
    widget_report_id bigint NOT NULL,
    account_id       bigint NOT NULL,
    CONSTRAINT widget_report_user_widget_report_id FOREIGN KEY (widget_report_id)
        REFERENCES widget_report (id)
);
create sequence widget_report_user_seq start 1;

create table offer
(
    id              bigint primary key,
    customer_name   varchar(500) not null,
    user_id         bigint       not null,
    uuid            varchar(500) not null,
    generation_time timestamp    not null
);

create sequence offer_seq start 1;
