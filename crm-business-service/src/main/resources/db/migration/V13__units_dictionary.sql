INSERT INTO dictionary (id, type, max_level) VALUES (2, 'UNIT', 1);

alter table word add locale varchar(30) default 'pl'::character varying not null;

INSERT INTO word (id, dictionary_id, parent_id, name, global, created, updated, deleted) VALUES
(nextval('word_seq'), 2, null, 'Sztuki', true, CURRENT_TIMESTAMP, null, false);

alter table product add unit bigint not null default currval('word_seq');

alter table product
	add constraint product_unit_fk
		foreign key (unit) references word
			on update cascade on delete cascade;

INSERT INTO word (id, dictionary_id, parent_id, name, global, created, updated, deleted) VALUES
(nextval('word_seq'), 2, null, 'Godziny', true, CURRENT_TIMESTAMP, null, false),
(nextval('word_seq'), 2, null, 'GB', true, CURRENT_TIMESTAMP, null, false),
(nextval('word_seq'), 2, null, 'MD', true, CURRENT_TIMESTAMP, null, false);

