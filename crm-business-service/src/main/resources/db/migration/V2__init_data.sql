insert into price_book (id, name, description, currency, active, is_default, created, updated, deleted)
values(-1, 'Cennik cen Standardowych', 'Cennik cen Standardowych', 'PLN', true, true, '2019-09-18 00:00:00.000000', '2019-09-18 00:00:00.000000', false);

insert into price_book (id, name, description, currency, active, is_default, created, updated, deleted)
values(-2, 'Cennik afryka', 'Cennik dla afryki', 'USD', true, false, '2019-09-18 00:00:00.000000', '2019-09-18 00:00:00.000000', false);

insert into company_type (id, type)
values (0, 'leads.form.companyTypes.it');

insert into company_type (id, type)
values (1, 'leads.form.companyTypes.biogenetics');

insert into company_type (id, type)
values (2, 'leads.form.companyTypes.hitech');

insert into company_type (id, type)
values (3, 'leads.form.companyTypes.services');

insert into company_type (id, type)
values (4, 'leads.form.companyTypes.finance');

insert into type(id, name, description)
values (-5, 'customer.types.client', 'customer.types.client.description');

insert into type(id, name, description)
values (-4, 'customer.types.integrator', 'customer.types.integrator.description');

insert into type(id, name, description)
values (-3, 'customer.types.investor', 'customer.types.investor.description');

insert into type(id, name, description)
values (-2, 'customer.types.partner', 'customer.types.partner.description');

insert into type(id, name, description)
values (-1, 'customer.types.none', 'customer.types.none.description');

insert into address(id, country, city, zip_code, street)
values(-3, 'Polska', 'Wrocław', '50-319', 'Bolesława Prusa 9e/3');

insert into address(id, country, city, zip_code, street)
values(-2, 'Polska', 'Międzyrzecz', '66-300', 'Piastowska 36c/4');

insert into customer(id, type_id, nip, regon, name, description, address_id, phone, owner, created)
values(-2, -5, '5961760186', '123123123', 'Google', 'Google Inc.', -3, '48111111111', -1, '2019-07-01 10:46:21.579645');

insert into customer(id, type_id, nip, regon, name, description, address_id, phone, owner, created)
values(-1, -4, '5961760187', '123123123', 'Facebook', 'Facebook Inc.', -2, '48111111111', -1, '2019-07-01 10:46:21.579645');
