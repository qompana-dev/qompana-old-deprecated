ALTER TABLE lead
    ADD priority int;
ALTER TABLE lead
    ADD CONSTRAINT lead_priority_validation_check
        CHECK ( priority is null or (priority >= 0 and priority <= 3) );
comment on column lead.priority is '0 - low; 1 - medium; 2 - high; 3 - urgent';

ALTER TABLE opportunity
    ADD priority int;
ALTER TABLE opportunity
    ADD CONSTRAINT opportunity_priority_validation_check
        CHECK ( priority is null or (priority >= 0 and priority <= 3) );
comment on column opportunity.priority is '0 - low; 1 - medium; 2 - high; 3 - urgent';

ALTER TABLE lead
    DROP COLUMN status;

ALTER TABLE opportunity
    DROP COLUMN type;
