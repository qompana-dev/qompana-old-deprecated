alter table product drop constraint product_unit_fk;
alter table product drop column unit;

DELETE FROM word WHERE dictionary_id = 2;
DELETE FROM dictionary WHERE id = 2;

alter table word drop column locale;