alter table opportunity_product add current_sell_price_net double precision;
update opportunity_product as op_to_update set current_sell_price_net = (select max(p.sell_price_net) from product as p where p.id = (select max(op.product_id) from opportunity_product as op where op.id = op_to_update.id));
alter table opportunity_product drop column price_changed;
