alter table customer
    ADD industry BIGINT;
alter table customer
    add constraint industry_foreign_key
        foreign key (industry) references word
    on update cascade on delete cascade;
alter table customer drop column company_type;