alter table contact
    add column address_id bigint;

alter table contact
    add constraint contact_address_fk
        foreign key (address_id) references address (id)
            on update cascade on delete cascade;
