alter table lead
    add constraint subject_of_interest_foreign_key
        foreign key (subject_of_interest_id) references word
            on update cascade on delete cascade;


insert into dictionary (id, type, max_level, is_words_deletable, is_words_hideable)
values (5, 'POSITION', 1, true, true);

DO
$$
    DECLARE
        lead_r    record;
        similar_count bigint;
    BEGIN
        FOR lead_r IN (select *
                       from lead
                       where position is not null)
            LOOP
                similar_count := (select count(*)
                                  from word
                                  where dictionary_id = 5
                                    and name is not null
                                    and lower(name) = lower(lead_r.position));
                if (similar_count = 0) then
                    insert into word (id, dictionary_id, parent_id, name, global, created, updated, deleted)
                    values (nextval('word_seq'), 5, null, lead_r.position, true, CURRENT_TIMESTAMP, null, false);
                    update lead set position = currval('word_seq')
                    where id = lead_r.id;
                    update lead set position = currval('word_seq')
                    where position is not null and lower(position) = lower(lead_r.position);
                END IF;
            end loop;

    end;
$$;

ALTER TABLE lead
    ALTER COLUMN position TYPE bigint
        USING position::integer;


insert into dictionary (id, type, max_level, is_words_deletable, is_words_hideable)
values (6, 'INDUSTRY', 1, true, true);

alter table company
    ADD industry BIGINT;
alter table company
    add constraint industry_foreign_key
        foreign key (industry) references word
            on update cascade on delete cascade;

insert into word (id, dictionary_id, parent_id, name, global, created, updated, deleted)
values (nextval('word_seq'), 6, null, 'IT', true, CURRENT_TIMESTAMP, null, false);
update company
set industry = currval('word_seq')
where company_type_id = 0;

insert into word (id, dictionary_id, parent_id, name, global, created, updated, deleted)
values (nextval('word_seq'), 6, null, 'Biogenetyka', true, CURRENT_TIMESTAMP, null, false);

update company
set industry = currval('word_seq')
where company_type_id = 1;

insert into word (id, dictionary_id, parent_id, name, global, created, updated, deleted)
values (nextval('word_seq'), 6, null, 'Hitech', true, CURRENT_TIMESTAMP, null, false);

update company
set industry = currval('word_seq')
where company_type_id = 2;

insert into word (id, dictionary_id, parent_id, name, global, created, updated, deleted)
values (nextval('word_seq'), 6, null, 'Usługi', true, CURRENT_TIMESTAMP, null, false);

update company
set industry = currval('word_seq')
where company_type_id = 3;

insert into word (id, dictionary_id, parent_id, name, global, created, updated, deleted)
values (nextval('word_seq'), 6, null, 'Finanse', true, CURRENT_TIMESTAMP, null, false);

update company
set industry = currval('word_seq')
where company_type_id = 4;

alter table company
    drop constraint company_type_foreign_key;
alter table company
    drop column company_type_id;
drop table company_type;
alter table lead
    drop constraint sales_opportunity_check;
alter table lead
    drop column sales_opportunity;

insert into dictionary (id, type, max_level, is_words_deletable, is_words_hideable)
values (7, 'LOCATION', 3, true, true);
