alter table opportunity add actual_user_task varchar(100);
alter table opportunity add change_user_task_date timestamp;
update opportunity set change_user_task_date = created;