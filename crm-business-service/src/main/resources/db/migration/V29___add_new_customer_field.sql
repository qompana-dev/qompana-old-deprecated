alter table customer add column archived boolean default 'f';
alter table customer add column active_client boolean default 'f';
alter table customer add column active_delivery boolean default 'f';

