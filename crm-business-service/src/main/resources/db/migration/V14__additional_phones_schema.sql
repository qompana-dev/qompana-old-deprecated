create sequence phone_number_seq start 1;

create table phone_number
(
    id                   bigint primary key,
    number               varchar(300) not null,
    phone_number_type_id bigint       not null,
    lead_id              bigint,
    customer_id          bigint,
    contact_id           bigint,
    constraint phone_number_phone_number_type_id foreign key (phone_number_type_id)
        references word (id),
    constraint phone_number_lead_id foreign key (lead_id)
        references lead (id),
    constraint phone_number_customer_id foreign key (customer_id)
        references customer (id),
    constraint phone_number_contact_id foreign key (contact_id)
        references contact (id)
);

alter table dictionary
    add column is_words_deletable boolean not null default false;
alter table dictionary
    add column is_words_hideable boolean not null default false;

insert into dictionary (id, type, max_level)
values (3, 'PHONE_NUMBER_TYPE', 1);

insert into word (id, dictionary_id, parent_id, name, global, created, updated, deleted)
values (nextval('word_seq'), 3, null, 'Służbowy', true, CURRENT_TIMESTAMP, null, false),
       (nextval('word_seq'), 3, null, 'Komórka', true, CURRENT_TIMESTAMP, null, false),
       (nextval('word_seq'), 3, null, 'Biuro', true, CURRENT_TIMESTAMP, null, false),
       (nextval('word_seq'), 3, null, 'Prywatny', true, CURRENT_TIMESTAMP, null, false);

update dictionary
set is_words_deletable = true,
    is_words_hideable  = true
where id = 1;

update dictionary
set is_words_deletable = true,
    is_words_hideable  = true
where id = 2;


