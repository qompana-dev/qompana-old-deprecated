alter table widget_report
    drop constraint widget_column_enum_check;

update widget_report
set widget_column = 'OPPORTUNITY_PRIORITY'
where widget_column = 'TYPE';
update widget_report
set widget_column = 'LEAD_PRIORITY'
where widget_column = 'LEAD_STATUS';

alter table widget_report
    add constraint widget_column_enum_check
        check (widget_column = 'CUSTOMER' or widget_column = 'CONTACT'
            or widget_column = 'OPPORTUNITY_PRIORITY' or widget_column = 'TASK'
            or widget_column = 'CURRENCY' or widget_column = 'PROCESS'
            or widget_column = 'FINISH_RESULT' or widget_column = 'REJECTION_RESULT'
            or widget_column = 'OWNER' or widget_column = 'COMPANY'
            or widget_column = 'LEAD_PRIORITY' or widget_column = 'PRIORITY'
            or widget_column = 'TASK_STATE' or widget_column = 'ACTIVITY_TYPE');

