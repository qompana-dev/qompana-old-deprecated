create table dictionary
(
    id        bigint      not null
        constraint dictionary_pkey
            primary key,
    type      varchar(32) not null
        constraint dictionary_type_key
            unique,
    max_level bigint      not null
);

alter table dictionary
    owner to postgres;

create table word
(
    id            bigint not null
        constraint word_pkey
            primary key,
    dictionary_id bigint
        constraint dictionary_foreign_key
            references dictionary,
    parent_id     bigint
        constraint word_parent_foreign_key
            references word,
    name          varchar(300),
    global        boolean default false,
    created       timestamp,
    updated       timestamp,
    deleted       boolean default false
);

alter table word
    owner to postgres;

create sequence dictionary_seq;
alter sequence dictionary_seq owner to postgres;

create sequence word_seq;
alter sequence word_seq owner to postgres;


alter table lead drop column source_of_acquisition;
alter table lead add source_of_acquisition bigint;
alter table lead add sub_source_of_acquisition bigint;

alter table lead
	add constraint source_of_acquisition_foreign_key
		foreign key (source_of_acquisition) references word
			on update cascade on delete cascade;

alter table lead
	add constraint sub_source_of_acquisition_foreign_key
		foreign key (sub_source_of_acquisition) references word
			on update cascade on delete cascade;

alter table opportunity drop column source_of_acquisition;
alter table opportunity add source_of_acquisition bigint;
alter table opportunity add sub_source_of_acquisition bigint;

alter table opportunity
	add constraint source_of_acquisition_foreign_key
		foreign key (source_of_acquisition) references word
			on update cascade on delete cascade;

alter table opportunity
	add constraint sub_source_of_acquisition_foreign_key
		foreign key (sub_source_of_acquisition) references word
			on update cascade on delete cascade;

INSERT INTO dictionary (id, type, max_level) VALUES (1, 'SOURCE_OF_ACQUISITION', 2);
