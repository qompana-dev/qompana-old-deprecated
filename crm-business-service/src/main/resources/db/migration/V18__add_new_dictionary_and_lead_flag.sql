insert into dictionary (id, type, max_level, is_words_deletable, is_words_hideable)
values (4, 'SUBJECT_OF_INTEREST', 1, true, true);

alter table lead ADD interested_in_cooperation  boolean not null default false;
alter table lead ADD subject_of_interest_id BIGINT;
alter table opportunity ADD subject_of_interest_id BIGINT;
