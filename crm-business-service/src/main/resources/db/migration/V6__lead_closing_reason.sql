ALTER TABLE lead
    ADD COLUMN close_result varchar(100),
    ADD COLUMN rejection_reason varchar(100);
