package com.devqube.crmbusinessservice;

import com.devqube.crmshared.license.LicenseCheck;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableFeignClients(basePackages = {"com.devqube.crmbusinessservice", "com.devqube.crmshared.association", "com.devqube.crmshared.file"})
@EnableSpringDataWebSupport
@EnableEurekaClient
@Import({LicenseCheck.class})
public class CrmBusinessServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrmBusinessServiceApplication.class, args);
	}

	@Bean
	@Primary
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
	public ModelMapper strictModelMapper() {
		return CrmBusinessServiceApplication.getStrictModelMapper();
	}


	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	public static ModelMapper getStrictModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		return modelMapper;
	}
}
