package com.devqube.crmbusinessservice.gdpr.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UnableToModifyException extends Exception {
    public UnableToModifyException() {
    }

    public UnableToModifyException(String message) {
        super(message);
    }
}
