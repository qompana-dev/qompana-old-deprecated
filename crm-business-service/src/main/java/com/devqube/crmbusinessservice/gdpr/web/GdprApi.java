package com.devqube.crmbusinessservice.gdpr.web;

import com.devqube.crmbusinessservice.gdpr.exception.TokenExpiredException;
import com.devqube.crmbusinessservice.gdpr.exception.UnableToModifyException;
import com.devqube.crmbusinessservice.gdpr.web.dto.*;
import com.devqube.crmbusinessservice.gpdr.web.dto.FormDataApproveUpdateDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.mail.gdpr.EmailCheckDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;
import java.util.Set;

public interface GdprApi {

    @ApiOperation(value = "Get gdpr types", nickname = "getAllTypes", notes = "Method used to get gdpr type list")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr types", response = GdprTypeDto[].class)})
    List<GdprTypeDto> getAllTypes();

    @ApiOperation(value = "Remove gdpr type", nickname = "deleteGdprType", notes = "Method used to remove gdpr type")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Gdpr type removed"),
            @ApiResponse(code = 404, message = "Gdpr type not found")})
    void deleteGdprType(Long id) throws EntityNotFoundException, UnableToModifyException;

    @ApiOperation(value = "Create gdpr type", nickname = "saveGdprType", notes = "Method used to save gdpr type")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Gdpr type created successfully"),
            @ApiResponse(code = 409, message = "Gdpr with given name exists")})
    ResponseEntity<Void> saveGdprType(GdprTypeDto gdprType);

    @ApiOperation(value = "Update gdpr type", nickname = "updateGdprType", notes = "Method used to update gdpr type")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Gdpr type updated successfully"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Gdpr type not found"),
            @ApiResponse(code = 409, message = "Gdpr with given name exists")})
    ResponseEntity<Void> updateGdprType(GdprTypeDto gdprType);

    @ApiOperation(value = "Get gdpr statuses", nickname = "getAllStatuses", notes = "Method used to get gdpr statuses list")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr statuses", response = GdprStatusDto[].class)})
    List<GdprStatusDto> getAllStatuses();

    @ApiOperation(value = "Remove gdpr status", nickname = "deleteGdprStatus", notes = "Method used to remove gdpr status")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Gdpr status removed"),
            @ApiResponse(code = 404, message = "Gdpr status not found")})
    void deleteGdprStatus(Long id) throws EntityNotFoundException, UnableToModifyException;

    @ApiOperation(value = "Create gdpr status", nickname = "saveGdprStatus", notes = "Method used to save gdpr status")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Gdpr status created successfully"),
            @ApiResponse(code = 404, message = "Cannot find previous status"),
            @ApiResponse(code = 409, message = "Gdpr status with given name exists")})
    ResponseEntity<Void> saveGdprStatus(GdprStatusDto gdprStatusDto);

    @ApiOperation(value = "Update gdpr status", nickname = "updateGdprStatus", notes = "Method used to update gdpr status")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Gdpr status updated successfully"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Gdpr status not found"),
            @ApiResponse(code = 409, message = "Gdpr status with given name exists")})
    ResponseEntity<Void> updateGdprStatus(GdprStatusDto gdprStatusDto);

    @ApiOperation(value = "Get gdpr approve ways", nickname = "getAllApproveWays", notes = "Method used to get gdpr approve way list")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr approve ways", response = GdprApproveWayDto[].class)})
    List<GdprApproveWayDto> getAllApproveWays();

    @ApiOperation(value = "Remove gdpr approve way", nickname = "deleteGdprApproveWay", notes = "Method used to remove gdpr approve way")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Gdpr approve way removed"),
            @ApiResponse(code = 404, message = "Gdpr approve way not found")})
    void deleteGdprApproveWay(Long id) throws EntityNotFoundException, UnableToModifyException;

    @ApiOperation(value = "Create gdpr approve way status", nickname = "saveGdprApproveWay", notes = "Method used to save gdpr approve way")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Gdpr approve way created successfully"),
            @ApiResponse(code = 409, message = "Gdpr approve way with given name exists")})
    ResponseEntity<Void> saveGdprApproveWay(GdprApproveWayDto gdprApproveWayDto);

    @ApiOperation(value = "Update gdpr approve way", nickname = "updateGdprApproveWay", notes = "Method used to update gdpr approve way")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Gdpr approve way updated successfully"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Gdpr approve way not found"),
            @ApiResponse(code = 409, message = "Gdpr approve way with given name exists")})
    ResponseEntity<Void> updateGdprApproveWay(GdprApproveWayDto gdprApproveWayDto);

    @ApiOperation(value = "Get gdpr approves for contact", nickname = "getAllApprovesForContact", notes = "Method used to get gdpr approve list")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr approve ways", response = GdprApproveDto[].class),
            @ApiResponse(code = 404, message = "contact not found")})
    List<GdprApproveDto> getAllApprovalsForContact(Long contactId);

    @ApiOperation(value = "Update gdpr approves for contact", nickname = "updateApprovalsForContact", notes = "Method used to update gdpr approve list")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr approve list updated successfully", response = GdprApproveDto[].class),
            @ApiResponse(code = 400, message = "List not contain all gdprApproves"),
            @ApiResponse(code = 404, message = "contact not found")})
    List<GdprApproveDto> updateApprovalsForContact(Long contactId, List<GdprApproveUpdateDto> list, String loggedAccountEmail) throws BadRequestException, EntityNotFoundException;

    @ApiOperation(value = "Update gdpr approve for contact", nickname = "updateApprovalForContact", notes = "Method used to update gdpr approve")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr approve updated successfully", response = GdprApproveDto.class),
            @ApiResponse(code = 400, message = "List not contain all gdprApproves"),
            @ApiResponse(code = 404, message = "contact not found")})
    GdprApproveDto updateApprovalForContact(Long contactId, GdprApproveUpdateDto approveUpdateDto, String loggedAccountEmail) throws BadRequestException, EntityNotFoundException;

    @ApiOperation(value = "Set gdpr types sent to contact", nickname = "setGdprTypesSentToContact", notes = "Method used to set gdpr types sent to contact")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr types sent to the user have been set successfully"),
            @ApiResponse(code = 404, message = "Copntact not found")
    })
    void setGdprTypesSentToContact(Long contactId, Set<Long> typeId, String loggedAccountEmail) throws EntityNotFoundException, BadRequestException;

    @ApiOperation(value = "Get gdpr form config", nickname = "getGdprFormConfig", notes = "Method used to get gdpr form config")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr form config", response = FormConfigDto.class)})
    FormConfigDto getGdprFormConfig();

    @ApiOperation(value = "Get gdpr mail config", nickname = "getGdprMailConfig", notes = "Method used to get gdpr mail config")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr mail config", response = MailConfigDto.class)})
    MailConfigDto getGdprMailConfig();

    @ApiOperation(value = "Save gdpr form config", nickname = "saveGdprFormConfig", notes = "Method used to save gdpr form config")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr form config save successfully", response = FormConfigDto.class),
            @ApiResponse(code = 400, message = "Gdpr form config is incorrect")})
    FormConfigDto saveGdprFormConfig(FormConfigDto formConfig);

    @ApiOperation(value = "Save gdpr mail config", nickname = "saveGdprMailConfig", notes = "Method used to save gdpr mail config")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr mail config saved successfully", response = MailConfigDto.class),
            @ApiResponse(code = 400, message = "Gdpr mail config is incorrect")})
    MailConfigDto saveGdprMailConfig(MailConfigDto mailConfig);

    @ApiOperation(value = "Get available gdpr email variables", nickname = "getAvailableGdprEmailVariables", notes = "Method used to get gdpr email variables")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr email variable list", response = String[].class)
    })
    List<String> getAvailableGdprEmailVariables();

    @ApiOperation(value = "Get available gdpr form variables", nickname = "getAvailableGdprFormVariables", notes = "Method used to get gdpr form variables")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr form variable list", response = String[].class)
    })
    List<String> getAvailableGdprFormVariables();

    @ApiOperation(value = "Get email by contactId", nickname = "getGdprEmailByContactId", notes = "Method used to get gdpr form by token")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr email", response = FormDataDto.class),
            @ApiResponse(code = 400, message = "Token expired"),
            @ApiResponse(code = 404, message = "Contact not found")
    })
    EmailDataDto getGdprEmailByContactId(Long contactId) throws EntityNotFoundException, TokenExpiredException;

    @ApiOperation(value = "Get gdpr form by token", nickname = "getGdprFormByToken", notes = "Method used to get gdpr form by token")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr form by token", response = FormDataDto.class),
            @ApiResponse(code = 400, message = "Token expired"),
            @ApiResponse(code = 404, message = "Token not found")
    })
    FormDataDto getGdprFormByToken(String token) throws EntityNotFoundException, TokenExpiredException;

    @ApiOperation(value = "update gdpr approval list for token", nickname = "updateGdprApprovalListForToken", notes = "Method used to update gdpr approval list for token")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr approval list updated successfully", response = FormDataDto.class),
            @ApiResponse(code = 400, message = "Token expired"),
            @ApiResponse(code = 404, message = "Token not found")
    })
    FormDataDto updateGdprApprovalListForToken(String token, List<FormDataApproveUpdateDto> approvalList) throws EntityNotFoundException, TokenExpiredException;


    @ApiOperation(value = "get gdpr token info by contact id", nickname = "getGdprTokenInfoByContactId", notes = "Method used to get gdpr token info by contact id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr token info", response = GdprTokenInfoDto.class),
            @ApiResponse(code = 404, message = "Contact not found")
    })
    GdprTokenInfoDto getGdprTokenInfoByContactId(Long contactId) throws EntityNotFoundException;

    @ApiOperation(value = "get gdpr token info by contact id", nickname = "createGdprTokenForContactId", notes = "Method used to get gdpr token info by contact id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gdpr token created successfully", response = GdprTokenInfoDto.class),
            @ApiResponse(code = 404, message = "Contact not found")
    })
    GdprTokenInfoDto createGdprTokenForContactId(Long contactId) throws EntityNotFoundException;

    @ApiOperation(value = "check agreement for email list" , nickname = "checkGdprInEmails", notes = "Method used to check agreement for emails")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "return list of email with gdpr", response = EmailCheckDto[].class)
    })
    List<EmailCheckDto> checkGdprInEmails(List<String> emails);
}
