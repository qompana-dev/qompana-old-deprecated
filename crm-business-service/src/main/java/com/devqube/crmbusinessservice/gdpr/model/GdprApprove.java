package com.devqube.crmbusinessservice.gdpr.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GdprApprove {

    @Id
    @SequenceGenerator(name = "gdpr_approve_seq", sequenceName = "gdpr_approve_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gdpr_approve_seq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "gdpr_id")
    private Gdpr gdpr;

    @ManyToOne
    @JoinColumn(name = "gdpr_status_id")
    private GdprStatus gdprStatus;

    @ManyToOne
    @JoinColumn(name = "gdpr_type_id")
    private GdprType gdprType;

    @ManyToOne
    @JoinColumn(name = "gdpr_approve_way_id")
    private GdprApproveWay gdprApproveWay;

    @NotNull
    private Long changedBy;

    @NotNull
    private LocalDateTime modified;

}
