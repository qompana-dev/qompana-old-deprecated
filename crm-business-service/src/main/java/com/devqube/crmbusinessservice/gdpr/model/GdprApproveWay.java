package com.devqube.crmbusinessservice.gdpr.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GdprApproveWay {

    @Id
    @SequenceGenerator(name = "gdpr_status_seq", sequenceName = "gdpr_status_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gdpr_status_seq")
    private Long id;

    @NotNull
    @Column(unique = true)
    private String name;

    @Column(columnDefinition = "default false")
    private Boolean isDefaultWay;

    @Column(columnDefinition = "default true")
    private Boolean modifiable;

    @ToString.Exclude
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "gdprApproveWay")
    private Set<GdprApprove> gdprApproveWayList;
}
