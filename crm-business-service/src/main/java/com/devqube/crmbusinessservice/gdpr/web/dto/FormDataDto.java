package com.devqube.crmbusinessservice.gdpr.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FormDataDto {
    private Long contactId;
    private String formContent;
    private String token;
    private List<FormDataGdprTypeDto> types;
}
