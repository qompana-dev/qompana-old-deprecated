package com.devqube.crmbusinessservice.gdpr;

import com.devqube.crmbusinessservice.gdpr.model.GdprType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface GdprTypeRepository extends JpaRepository<GdprType, Long> {

    @Query("select t from GdprType t where lower(t.name) = lower(:name)")
    List<GdprType> getCountAllByName(@Param("name") String name);

    List<GdprType> findAllByIdIn(Set<Long> ids);
}
