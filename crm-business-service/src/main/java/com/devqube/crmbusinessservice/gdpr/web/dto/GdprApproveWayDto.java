package com.devqube.crmbusinessservice.gdpr.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GdprApproveWayDto {

    private Long id;
    private String name;
    private Boolean isDefaultWay;
    private Boolean modifiable;

}
