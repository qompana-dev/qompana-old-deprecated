package com.devqube.crmbusinessservice.gdpr.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MailConfigDto {
    @NotNull
    private String mailTitle;
    @NotNull
    private String mailContent;
}
