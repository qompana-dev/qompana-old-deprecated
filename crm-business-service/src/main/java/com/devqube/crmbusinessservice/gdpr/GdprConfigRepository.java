package com.devqube.crmbusinessservice.gdpr;

import com.devqube.crmbusinessservice.gdpr.model.GdprConfig;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GdprConfigRepository extends JpaRepository<GdprConfig, Long> {
}
