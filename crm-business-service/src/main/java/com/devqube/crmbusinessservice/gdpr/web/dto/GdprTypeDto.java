package com.devqube.crmbusinessservice.gdpr.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GdprTypeDto {

    private Long id;
    private String name;
    private String description;
    private Boolean isMailType;
}
