package com.devqube.crmbusinessservice.gdpr;

import com.devqube.crmbusinessservice.gdpr.model.Gdpr;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GdprRepository extends JpaRepository<Gdpr, Long> {
    List<Gdpr> findAllByContactId(Long contactId);
    List<Gdpr> findAllByToken(String token);
}
