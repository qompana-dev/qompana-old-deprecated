package com.devqube.crmbusinessservice.gdpr.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GdprStatus {

    @Id
    @SequenceGenerator(name = "gdpr_status_seq", sequenceName = "gdpr_status_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gdpr_status_seq")
    private Long id;

    @NotNull
    @Column(unique = true)
    private String name;

    @Column(columnDefinition = "default true")
    private Boolean modifiable;

    @NotNull
    @Column(columnDefinition = "NONE")
    @Enumerated(EnumType.STRING)
    private GdprDefaultStatus defaultStatus;

    @ToString.Exclude
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "gdprStatus")
    private Set<GdprApprove> gdprStatusList;
}
