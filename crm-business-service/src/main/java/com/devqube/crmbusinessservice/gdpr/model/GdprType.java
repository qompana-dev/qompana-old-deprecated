package com.devqube.crmbusinessservice.gdpr.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GdprType {

    @Id
    @SequenceGenerator(name = "gdpr_type_seq", sequenceName = "gdpr_type_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gdpr_type_seq")
    private Long id;

    @NotNull
    @Column(unique = true)
    private String name;
    private String description;

    @Column(columnDefinition = "default false")
    private Boolean isMailType;

    @ToString.Exclude
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "gdprType")
    private Set<GdprApprove> gdprApproveList;

}
