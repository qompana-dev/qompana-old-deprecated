package com.devqube.crmbusinessservice.gdpr;

import com.devqube.crmbusinessservice.gdpr.model.GdprApproveWay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GdprApproveWayRepository extends JpaRepository<GdprApproveWay, Long> {
    List<GdprApproveWay> findAllByIsDefaultWayTrue();

    @Query("select aw from GdprApproveWay aw where lower(aw.name) = lower(:name)")
    List<GdprApproveWay> getCountAllByName(@Param("name") String name);


}
