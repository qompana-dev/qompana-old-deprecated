package com.devqube.crmbusinessservice.gdpr.model;

public enum  GdprDefaultStatus {
    NONE, CHECKED, UNCHECKED
}
