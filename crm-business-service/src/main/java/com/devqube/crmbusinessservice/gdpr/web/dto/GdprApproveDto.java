package com.devqube.crmbusinessservice.gdpr.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GdprApproveDto {
    private Long id;
    private GdprStatusDto gdprStatus;
    private GdprTypeDto gdprType;
    private GdprApproveWayDto gdprApproveWay;
    private Long changedBy;
    private LocalDateTime modified;
}
