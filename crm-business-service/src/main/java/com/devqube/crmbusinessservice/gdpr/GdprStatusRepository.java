package com.devqube.crmbusinessservice.gdpr;

import com.devqube.crmbusinessservice.gdpr.model.GdprDefaultStatus;
import com.devqube.crmbusinessservice.gdpr.model.GdprStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GdprStatusRepository extends JpaRepository<GdprStatus, Long> {
    List<GdprStatus> findAllByDefaultStatus(GdprDefaultStatus defaultStatus);

    @Query("select s from GdprStatus s where lower(s.name) = lower(:name)")
    List<GdprStatus> getCountAllByName(@Param("name") String name);

    @Query("select s from GdprStatus s where s.defaultStatus = :status")
    GdprStatus getByStatus(@Param("status") GdprDefaultStatus status);

}
