package com.devqube.crmbusinessservice.gdpr.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GdprVariable {
    CONTACT_NAME(true, true, "\\{\\{CONTACT_NAME}}", GdprValueGroup.CONTACT),
    CONTACT_SURNAME(true, true, "\\{\\{CONTACT_SURNAME}}", GdprValueGroup.CONTACT),
    CONTACT_POSITION(true, true, "\\{\\{CONTACT_POSITION}}", GdprValueGroup.CONTACT),
    CONTACT_EMAIL(true, true, "\\{\\{CONTACT_EMAIL}}", GdprValueGroup.CONTACT),
    CONTACT_PHONE(true, true, "\\{\\{CONTACT_PHONE}}", GdprValueGroup.CONTACT),

    COMPANY_NAME(true, true, "\\{\\{COMPANY_NAME}}", GdprValueGroup.CUSTOMER),
    COMPANY_WWW(true, true, "\\{\\{COMPANY_WWW}}", GdprValueGroup.CUSTOMER),
    COMPANY_PHONE(true, true, "\\{\\{COMPANY_PHONE}}", GdprValueGroup.CUSTOMER),

    GDPR_LIST(false, true, "\\{\\{GDPR_LIST}}", GdprValueGroup.GDPR),

    FORM_LINK(true, false, "\\{\\{FORM_LINK}}", GdprValueGroup.GDPR),
    FORM_EXPIRATION_DATE(true, false, "\\{\\{FORM_EXPIRATION_DATE}}", GdprValueGroup.GDPR);

    private boolean email;
    private boolean form;
    private String replaceRegex;
    private GdprValueGroup group;

    public enum GdprValueGroup {
        CONTACT, CUSTOMER, GDPR
    }
}
