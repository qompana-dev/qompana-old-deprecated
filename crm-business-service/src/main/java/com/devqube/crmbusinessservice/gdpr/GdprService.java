package com.devqube.crmbusinessservice.gdpr;

import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.exception.NameOccupiedException;
import com.devqube.crmbusinessservice.gdpr.exception.TokenExpiredException;
import com.devqube.crmbusinessservice.gdpr.exception.UnableToModifyException;
import com.devqube.crmbusinessservice.gdpr.model.*;
import com.devqube.crmbusinessservice.gdpr.util.GdprVariableUtil;
import com.devqube.crmbusinessservice.gdpr.web.dto.*;
import com.devqube.crmbusinessservice.gpdr.web.dto.FormDataApproveUpdateDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.mail.gdpr.EmailCheckDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class GdprService {
    private final static Long GDPR_CONFIG_ID = 1L;

    @Value("${crm.gdpr.expiration.days:5}")
    private Integer gdprTokenExpirationDays;
    @Value("${client.url}")
    private String clientUrl;


    private final ModelMapper strictModelMapper;
    private final GdprTypeRepository gdprTypeRepository;
    private final GdprApproveRepository gdprApproveRepository;
    private final GdprConfigRepository gdprConfigRepository;
    private final GdprStatusRepository gdprStatusRepository;
    private final GdprApproveWayRepository gdprApproveWayRepository;
    private final ContactRepository contactRepository;
    private final GdprRepository gdprRepository;
    private final UserService userService;

    public GdprService(@Qualifier("strictModelMapper") ModelMapper strictModelMapper,
                       GdprTypeRepository gdprTypeRepository,
                       GdprApproveRepository gdprApproveRepository,
                       GdprConfigRepository gdprConfigRepository,
                       GdprStatusRepository gdprStatusRepository,
                       GdprApproveWayRepository gdprApproveWayRepository,
                       ContactRepository contactRepository,
                       GdprRepository gdprRepository, UserService userService) {
        this.strictModelMapper = strictModelMapper;
        this.gdprTypeRepository = gdprTypeRepository;
        this.gdprApproveRepository = gdprApproveRepository;
        this.gdprConfigRepository = gdprConfigRepository;
        this.gdprStatusRepository = gdprStatusRepository;
        this.gdprApproveWayRepository = gdprApproveWayRepository;
        this.contactRepository = contactRepository;
        this.gdprRepository = gdprRepository;
        this.userService = userService;
    }

    public List<GdprTypeDto> getAllTypes() {
        return mapList(gdprTypeRepository.findAll(), GdprTypeDto.class);
    }

    public void deleteGdprType(Long id) throws EntityNotFoundException, UnableToModifyException {
        GdprType gdprType = gdprTypeRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        if (gdprType.getIsMailType() != null && gdprType.getIsMailType()) {
            throw new UnableToModifyException();
        }
        gdprTypeRepository.deleteById(id);
    }

    public GdprTypeDto saveGdprType(GdprTypeDto gdprType) throws NameOccupiedException {
        checkTypeNameUniqueness(gdprType);
        GdprType newType = strictModelMapper.map(gdprType, GdprType.class);
        newType.setIsMailType(false);
        GdprType saved = gdprTypeRepository.save(newType);
        return strictModelMapper.map(saved, GdprTypeDto.class);
    }

    public GdprTypeDto updateGdprType(GdprTypeDto gdprType) throws EntityNotFoundException, BadRequestException, NameOccupiedException {
        if (gdprType == null || gdprType.getId() == null) {
            throw new BadRequestException("id is null");
        }
        checkTypeNameUniqueness(gdprType);
        GdprType fromDb = gdprTypeRepository.findById(gdprType.getId()).orElseThrow(EntityNotFoundException::new);
        fromDb.setDescription(gdprType.getDescription());
        if (!fromDb.getIsMailType()) {
            fromDb.setName(gdprType.getName());
        }
        GdprType saved = gdprTypeRepository.save(fromDb);
        return strictModelMapper.map(saved, GdprTypeDto.class);
    }

    public List<GdprStatusDto> getAllStatuses() {
        return mapList(gdprStatusRepository.findAll(), GdprStatusDto.class);
    }

    public void deleteStatus(Long id) throws EntityNotFoundException, UnableToModifyException {
        GdprStatus gdprStatus = gdprStatusRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        if (!gdprStatus.getModifiable()) {
            throw new UnableToModifyException();
        }
        updateStatusOnDelete(id);
        gdprStatusRepository.deleteById(id);
    }

    public GdprStatusDto saveStatus(GdprStatusDto gdprStatusDto) throws NameOccupiedException, EntityNotFoundException {
        checkStatusNameUniqueness(gdprStatusDto);
        updateDefaultStatus(gdprStatusDto);
        gdprStatusDto.setModifiable(true);
        GdprStatus saved = gdprStatusRepository.save(strictModelMapper.map(gdprStatusDto, GdprStatus.class));
        return strictModelMapper.map(saved, GdprStatusDto.class);
    }

    public GdprStatusDto updateStatus(GdprStatusDto gdprStatusDto) throws EntityNotFoundException, BadRequestException, NameOccupiedException {
        if (gdprStatusDto == null || gdprStatusDto.getId() == null) {
            throw new BadRequestException("id is null");
        }
        checkStatusNameUniqueness(gdprStatusDto);
        updateDefaultStatus(gdprStatusDto);
        GdprStatus fromDb = gdprStatusRepository.findById(gdprStatusDto.getId()).orElseThrow(EntityNotFoundException::new);
        fromDb.setName(gdprStatusDto.getName());
        fromDb.setDefaultStatus(gdprStatusDto.getDefaultStatus());
        GdprStatus saved = gdprStatusRepository.save(fromDb);
        return strictModelMapper.map(saved, GdprStatusDto.class);
    }

    public List<GdprApproveWayDto> getAllApproveWays() {
        return mapList(gdprApproveWayRepository.findAll(), GdprApproveWayDto.class);
    }

    public void deleteApproveWay(Long id) throws UnableToModifyException, EntityNotFoundException {
        GdprApproveWay gdprApproveWay = gdprApproveWayRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        if (!gdprApproveWay.getModifiable()) {
            throw new UnableToModifyException();
        }
        gdprApproveWayRepository.deleteById(id);
    }

    public GdprApproveWayDto saveApproveWay(GdprApproveWayDto gdprApproveWayDto) throws NameOccupiedException {
        checkApproveWayNameUniqueness(gdprApproveWayDto);
        gdprApproveWayDto.setModifiable(true);
        GdprApproveWay saved = gdprApproveWayRepository.save(strictModelMapper.map(gdprApproveWayDto, GdprApproveWay.class));
        return strictModelMapper.map(saved, GdprApproveWayDto.class);
    }

    public GdprApproveWayDto updateApproveWay(GdprApproveWayDto gdprApproveWayDto) throws EntityNotFoundException, BadRequestException, NameOccupiedException {
        if (gdprApproveWayDto == null || gdprApproveWayDto.getId() == null) {
            throw new BadRequestException("id is null");
        }
        checkApproveWayNameUniqueness(gdprApproveWayDto);
        GdprApproveWay fromDb = gdprApproveWayRepository.findById(gdprApproveWayDto.getId()).orElseThrow(EntityNotFoundException::new);
        fromDb.setName(gdprApproveWayDto.getName());
        GdprApproveWay saved = gdprApproveWayRepository.save(fromDb);
        return strictModelMapper.map(saved, GdprApproveWayDto.class);
    }

    public List<GdprApproveDto> getApprovalsForContact(Long contactId) {
        List<GdprApprove> allByGdprContactId = gdprApproveRepository.findAllByGdprContactId(contactId);
        return mapList(allByGdprContactId, GdprApproveDto.class);
    }

    public List<GdprApproveDto> updateApprovalsForContact(Long contactId, List<GdprApproveUpdateDto> list, String accountEmail) throws BadRequestException, EntityNotFoundException {
        Long userId = userService.getUserId(accountEmail);
        contactRepository.findById(contactId).orElseThrow(EntityNotFoundException::new);
        List<GdprApprove> allByGdprContactId = gdprApproveRepository.findAllByGdprContactId(contactId);
        boolean isEqual = CollectionUtils.isEqualCollection(
                list.stream().map(GdprApproveUpdateDto::getId).collect(Collectors.toList()),
                allByGdprContactId.stream().map(GdprApprove::getId).collect(Collectors.toList())
        );
        if (!isEqual) {
            throw new BadRequestException();
        }

        List<GdprStatus> statuses = gdprStatusRepository.findAll();
        List<GdprApproveWay> approveWays = gdprApproveWayRepository.findAll();

        LocalDateTime now = LocalDateTime.now();
        List<GdprApprove> toSave = allByGdprContactId.stream()
                .map(c -> getUpdatedGdprApprove(c, statuses, approveWays, list, userId, now))
                .collect(Collectors.toList());

        return mapList(gdprApproveRepository.saveAll(toSave), GdprApproveDto.class);
    }

    public GdprApproveDto updateApprovalForContact(Long contactId, GdprApproveUpdateDto approveUpdateDto, String accountEmail) throws BadRequestException, EntityNotFoundException {
        Long userId = userService.getUserId(accountEmail);
        contactRepository.findById(contactId).orElseThrow(EntityNotFoundException::new);
        GdprApprove gdprApprove = gdprApproveRepository.findById(approveUpdateDto.getId()).orElseThrow(EntityNotFoundException::new);
        List<GdprStatus> statuses = gdprStatusRepository.findAll();
        List<GdprApproveWay> approveWays = gdprApproveWayRepository.findAll();
        GdprApprove toSave = getUpdatedGdprApprove(gdprApprove, statuses, approveWays, approveUpdateDto, userId, LocalDateTime.now());
        GdprApprove saved = gdprApproveRepository.save(toSave);
        return strictModelMapper.map(saved, GdprApproveDto.class);
    }

    private GdprApprove getUpdatedGdprApprove(GdprApprove fromDb, List<GdprStatus> statuses, List<GdprApproveWay> approveWays, List<GdprApproveUpdateDto> updated, Long userId, LocalDateTime now) {
        return updated.stream().filter(c -> c.getId().equals(fromDb.getId())).findFirst()
                .map(approveUpdateDto -> getUpdatedGdprApprove(fromDb, statuses, approveWays, approveUpdateDto, userId, now))
                .orElse(fromDb);
    }

    private GdprApprove getUpdatedGdprApprove(GdprApprove fromDb, List<GdprStatus> statuses, List<GdprApproveWay> approveWays, GdprApproveUpdateDto update, Long userId, LocalDateTime now) {
        if (!fromDb.getGdprStatus().getId().equals(update.getGdprStatusId())) {
            statuses.stream().filter(c -> c.getId().equals(update.getGdprStatusId())).findFirst().ifPresent(c -> {
                fromDb.setGdprStatus(c);
                fromDb.setModified(now);
                fromDb.setChangedBy(userId);
            });
        }
        if (!fromDb.getGdprApproveWay().getId().equals(update.getGdprApproveWayId())) {
            approveWays.stream().filter(c -> c.getId().equals(update.getGdprApproveWayId())).findFirst().ifPresent(c -> {
                fromDb.setGdprApproveWay(c);
                fromDb.setModified(now);
                fromDb.setChangedBy(userId);
            });
        }

        return fromDb;
    }

    public MailConfigDto getMailConfig() {
        return strictModelMapper.map(getGdprConfig(), MailConfigDto.class);
    }

    public FormConfigDto getFormConfig() {
        return strictModelMapper.map(getGdprConfig(), FormConfigDto.class);
    }

    public MailConfigDto saveMailConfig(MailConfigDto mailConfigDto) {
        GdprConfig gdprConfig = getGdprConfig();
        gdprConfig.setMailTitle(mailConfigDto.getMailTitle());
        gdprConfig.setMailContent(mailConfigDto.getMailContent());
        GdprConfig saved = gdprConfigRepository.save(gdprConfig);
        return strictModelMapper.map(saved, MailConfigDto.class);
    }

    public FormConfigDto saveFormConfig(FormConfigDto formConfigDto) {
        GdprConfig gdprConfig = getGdprConfig();
        gdprConfig.setFormContent(formConfigDto.getFormContent());
        GdprConfig saved = gdprConfigRepository.save(gdprConfig);
        return strictModelMapper.map(saved, FormConfigDto.class);
    }

    @Transactional
    public void updateDefaultStatus(GdprStatusDto gdprStatusDto) throws EntityNotFoundException {
        if (gdprStatusDto.getId() != null) {
            GdprStatus oldStatus = gdprStatusRepository.findById(gdprStatusDto.getId()).orElseThrow(EntityNotFoundException::new);
            if (GdprDefaultStatus.NONE != oldStatus.getDefaultStatus()) {
                restoreDefaultStatus(oldStatus);
            }
        }

        if (GdprDefaultStatus.NONE != gdprStatusDto.getDefaultStatus()) {
            GdprStatus fromDb = gdprStatusRepository.getByStatus(gdprStatusDto.getDefaultStatus());
            fromDb.setDefaultStatus(GdprDefaultStatus.NONE);
            gdprStatusRepository.save(fromDb);
        }
    }

    @Transactional
    public void updateStatusOnDelete(Long gdprStatusId) throws EntityNotFoundException {
        GdprStatus gdprStatus = gdprStatusRepository.findById(gdprStatusId).orElseThrow(EntityNotFoundException::new);
        if (GdprDefaultStatus.NONE != gdprStatus.getDefaultStatus() && gdprStatus.getModifiable()) {
            restoreDefaultStatus(gdprStatus);
        }
    }

    private void restoreDefaultStatus(GdprStatus oldStatus) {
        Optional<GdprStatus> fromDb;
        switch (oldStatus.getDefaultStatus()) {
            case CHECKED:
                fromDb = gdprStatusRepository.findById(-4L);
                if (fromDb.isPresent()) {
                    GdprStatus tmp = fromDb.get();
                    tmp.setDefaultStatus(GdprDefaultStatus.CHECKED);
                    gdprStatusRepository.save(tmp);
                }
                break;
            case UNCHECKED:
                fromDb = gdprStatusRepository.findById(-3L);
                if (fromDb.isPresent()) {
                    GdprStatus tmp = fromDb.get();
                    tmp.setDefaultStatus(GdprDefaultStatus.UNCHECKED);
                    gdprStatusRepository.save(tmp);
                }
                break;
            default:
                break;
        }
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class})
    public void updateGdprApprovalListForToken(String token, List<FormDataApproveUpdateDto> approvalList) throws EntityNotFoundException {
        Gdpr gdpr = gdprRepository.findAllByToken(token).stream().findFirst().orElseThrow(EntityNotFoundException::new);

        GdprStatus defaultCheckedStatus = getDefaultCheckedStatus();
        GdprStatus defaultUncheckedStatus = getDefaultUncheckedStatus();
        GdprApproveWay defaultApproveWay = getDefaultApproveWay();

        List<GdprApprove> toSave = gdprApproveRepository.findAllByGdprId(gdpr.getId())
                .stream().map(c -> setNewValueForGdprApprove(c, approvalList, defaultCheckedStatus, defaultUncheckedStatus, defaultApproveWay))
                .collect(Collectors.toList());
        toSave.stream().map(c -> c.getGdprStatus().getName()).forEach(System.out::println);
        gdprApproveRepository.saveAll(toSave);
    }

    private GdprApprove setNewValueForGdprApprove(GdprApprove approve, List<FormDataApproveUpdateDto> approvalList,
                                                  GdprStatus defaultCheckedStatus, GdprStatus defaultUncheckedStatus, GdprApproveWay defaultApproveWay) {
        Long gdprTypeId = approve.getGdprType().getId();
        Optional<FormDataApproveUpdateDto> updatedApproval = approvalList.stream().filter(c -> c.getGdprTypeId().equals(gdprTypeId)).findFirst();
        if (updatedApproval.isPresent()) {
            if (updatedApproval.get().isChecked() && !approve.getGdprStatus().getDefaultStatus().equals(GdprDefaultStatus.CHECKED)) {
                approve.setGdprStatus(defaultCheckedStatus);
                approve.setGdprApproveWay(defaultApproveWay);
            }
            if (!updatedApproval.get().isChecked() && approve.getGdprStatus().getDefaultStatus().equals(GdprDefaultStatus.CHECKED)) {
                approve.setGdprStatus(defaultUncheckedStatus);
                approve.setGdprApproveWay(defaultApproveWay);
            }
        }
        return approve;
    }


    @Transactional(rollbackFor = {EntityNotFoundException.class, TokenExpiredException.class})
    public EmailDataDto getGdprEmailByContactId(Long contactId) throws EntityNotFoundException, TokenExpiredException {
        Gdpr gdpr = gdprRepository.findAllByContactId(contactId).stream().findFirst().orElseThrow(EntityNotFoundException::new);
        checkTokenExpired(gdpr);
        MailConfigDto mailConfig = getMailConfig();

        EmailDataDto emailDataDto = new EmailDataDto();
        emailDataDto.setContactId(contactId);
        emailDataDto.setToken(gdpr.getToken());
        emailDataDto.setEmailSubject(mailConfig.getMailTitle());
        emailDataDto.setEmailContent(GdprVariableUtil.getEmail(mailConfig.getMailContent(), gdpr, clientUrl));

        return emailDataDto;
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class, TokenExpiredException.class})
    public FormDataDto getGdprFormByToken(String token) throws EntityNotFoundException, TokenExpiredException {
        Gdpr gdpr = gdprRepository.findAllByToken(token).stream().findFirst().orElseThrow(EntityNotFoundException::new);
        checkTokenExpired(gdpr);
        FormDataDto result = new FormDataDto();
        result.setContactId(gdpr.getId());
        result.setToken(gdpr.getToken());
        result.setFormContent(GdprVariableUtil.getForm(getFormConfig().getFormContent(), gdpr, clientUrl));

        result.setTypes(gdprApproveRepository.findAllByGdprId(gdpr.getId())
                .stream().map(this::getFormDataGdprTypeDto).collect(Collectors.toList()));
        return result;
    }

    private void checkTokenExpired(Gdpr gdpr) throws TokenExpiredException {
        if (gdpr.getToken() == null || gdpr.getTokenExpirationDate().isBefore(LocalDateTime.now())) {
            throw new TokenExpiredException();
        }
    }

    private FormDataGdprTypeDto getFormDataGdprTypeDto(GdprApprove gdprApprove) {
        FormDataGdprTypeDto result = new FormDataGdprTypeDto();
        result.setGdprTypeId(gdprApprove.getGdprType().getId());
        result.setName(gdprApprove.getGdprType().getName());
        result.setDescription(gdprApprove.getGdprType().getDescription());
        result.setChecked(gdprApprove.getGdprStatus().getDefaultStatus().equals(GdprDefaultStatus.CHECKED));
        return result;
    }

    private <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        List<T> list = new ArrayList<>();
        for (S s : source) {
            list.add(strictModelMapper.map(s, targetClass));
        }
        return list;
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class})
    public void setGdprTypesSentToContact(Long contactId, Set<Long> typeIds, String accountEmail) throws EntityNotFoundException, BadRequestException {
        Long accountId = userService.getUserId(accountEmail);
        Contact contact = contactRepository.findById(contactId).orElseThrow(EntityNotFoundException::new);
        List<Gdpr> byContactId = gdprRepository.findAllByContactId(contactId);
        List<GdprType> types = gdprTypeRepository.findAllByIdIn(typeIds);
        Gdpr gdpr = (byContactId.size() == 0) ? addGdpr(contact) : byContactId.get(0);

        List<GdprType> existingTypes = gdprApproveRepository.findAllByGdprId(gdpr.getId()).stream()
                .map(GdprApprove::getGdprType).collect(Collectors.toList());

        List<GdprType> typesToAdd = getTypesToAdd(types, existingTypes);

        gdprApproveRepository.deleteAllByGdprIdAndGdprTypeIdIn(gdpr.getId(),
                getTypesToRemove(types, existingTypes).stream().map(GdprType::getId).collect(Collectors.toSet()));

        GdprStatus defaultGdprStatus = getDefaultUncheckedStatus();
        GdprApproveWay defaultGdprApproveWay = getDefaultApproveWay();

        LocalDateTime now = LocalDateTime.now();
        List<GdprApprove> toSave = typesToAdd.stream()
                .map(c -> getGdprApprove(gdpr, c, defaultGdprStatus, defaultGdprApproveWay, accountId, now))
                .collect(Collectors.toList());
        gdprApproveRepository.saveAll(toSave);
    }


    public GdprTokenInfoDto getGdprTokenInfoByContactId(Long contactId) throws EntityNotFoundException {
        contactRepository.findById(contactId).orElseThrow(EntityNotFoundException::new);
        GdprTokenInfoDto result = gdprRepository.findAllByContactId(contactId)
                .stream().findFirst()
                .map(value -> new GdprTokenInfoDto(value.getToken(), value.getTokenExpirationDate()))
                .orElseGet(GdprTokenInfoDto::new);
        if (result.getToken() != null && result.getTokenExpirationDate().isBefore(LocalDateTime.now())) {
            log.info("token expired");
            return new GdprTokenInfoDto();
        }
        return result;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class)
    public GdprTokenInfoDto createGdprTokenForContactId(Long contactId) throws EntityNotFoundException {
        Contact contact = contactRepository.findById(contactId).orElseThrow(EntityNotFoundException::new);
        Optional<Gdpr> gdpr = gdprRepository.findAllByContactId(contactId).stream().findFirst();

        Gdpr saved;
        if (gdpr.isEmpty()) {
            saved = addGdpr(contact);
        } else {
            Gdpr toSave = gdpr.get();
            toSave.setToken(getToken());
            toSave.setTokenExpirationDate(getNewTokenExpirationTime());
            saved = gdprRepository.save(toSave);
        }
        return new GdprTokenInfoDto(saved.getToken(), saved.getTokenExpirationDate());
    }

    public List<EmailCheckDto> checkGdprInEmails(List<String> emails) {
        return emails.stream().map(email -> new EmailCheckDto(email, canSendEmail(email))).collect(Collectors.toList());
    }

    private boolean canSendEmail(String email) {
        Long contacts = contactRepository.countAllByEmail(email);
        if (contacts > 0) {
            Long gdprApproveCount = gdprApproveRepository.countApprovingAgreementsByContactEmail(email);
            return gdprApproveCount > 0;
        }
        return true;
    }

    private GdprStatus getDefaultUncheckedStatus() throws EntityNotFoundException {
        return gdprStatusRepository.findAllByDefaultStatus(GdprDefaultStatus.UNCHECKED)
                .stream().findFirst().orElseThrow(EntityNotFoundException::new);
    }

    private GdprStatus getDefaultCheckedStatus() throws EntityNotFoundException {
        return gdprStatusRepository.findAllByDefaultStatus(GdprDefaultStatus.CHECKED)
                .stream().findFirst().orElseThrow(EntityNotFoundException::new);
    }

    private GdprApproveWay getDefaultApproveWay() throws EntityNotFoundException {
        return gdprApproveWayRepository.findAllByIsDefaultWayTrue()
                .stream().findFirst().orElseThrow(EntityNotFoundException::new);
    }

    private GdprApprove getGdprApprove(Gdpr gdpr, GdprType gdprType, GdprStatus defaultStatus, GdprApproveWay defaultApproveWay, Long accountId, LocalDateTime now) {
        GdprApprove result = new GdprApprove();

        result.setGdpr(gdpr);
        result.setGdprType(gdprType);
        result.setGdprStatus(defaultStatus);
        result.setGdprApproveWay(defaultApproveWay);
        result.setChangedBy(accountId);
        result.setModified(now);

        return result;
    }

    private List<GdprType> getTypesToAdd(List<GdprType> newTypes, List<GdprType> existingTypes) {
        List<Long> existingTypeIds = existingTypes.stream().map(GdprType::getId).collect(Collectors.toList());
        return newTypes.stream().filter(c -> !existingTypeIds.contains(c.getId())).collect(Collectors.toList());
    }

    private List<GdprType> getTypesToRemove(List<GdprType> newTypes, List<GdprType> existingTypes) {
        List<Long> newTypesIds = newTypes.stream().map(GdprType::getId).collect(Collectors.toList());
        return existingTypes.stream().filter(c -> !newTypesIds.contains(c.getId())).collect(Collectors.toList());
    }

    private Gdpr addGdpr(Contact contact) {
        Gdpr gdpr = new Gdpr();
        gdpr.setContact(contact);
        gdpr.setToken(getToken());
        gdpr.setTokenExpirationDate(getNewTokenExpirationTime());
        return gdprRepository.save(gdpr);
    }

    private GdprConfig getGdprConfig() {
        Optional<GdprConfig> byId = gdprConfigRepository.findById(GdprService.GDPR_CONFIG_ID);
        if (byId.isEmpty()) {
            log.error("The gdpr configuration has been removed. I'm trying to add new configuration.");
            GdprConfig save = gdprConfigRepository.save(GdprConfig.builder().id(GdprService.GDPR_CONFIG_ID).formContent("").mailContent("").mailTitle("").build());
            gdprConfigRepository.flush();
            return save;
        }
        return byId.get();
    }

    private void checkTypeNameUniqueness(GdprTypeDto gdprTypeDto) throws NameOccupiedException {
        List<GdprType> foundElements = gdprTypeRepository.getCountAllByName(gdprTypeDto.getName());
        if (foundElements.size() > 0) {
            if (gdprTypeDto.getId() == null || (gdprTypeDto.getId() != null && foundElements.get(0).getId() != gdprTypeDto.getId())) {
                throw new NameOccupiedException("Agree type with this name already exists");
            }
        }
    }

    private void checkStatusNameUniqueness(GdprStatusDto gdprStatusDto) throws NameOccupiedException {
        List<GdprStatus> foundElements = gdprStatusRepository.getCountAllByName(gdprStatusDto.getName());
        if (foundElements.size() > 0) {
            if (gdprStatusDto.getId() == null || (gdprStatusDto.getId() != null && foundElements.get(0).getId() != gdprStatusDto.getId())) {
                throw new NameOccupiedException("Agree status with this name already exists");
            }
        }
    }

    private void checkApproveWayNameUniqueness(GdprApproveWayDto gdprApproveWayDto) throws NameOccupiedException {
        List<GdprApproveWay> foundElements = gdprApproveWayRepository.getCountAllByName(gdprApproveWayDto.getName());
        if (foundElements.size() > 0) {
            if (gdprApproveWayDto.getId() == null || (gdprApproveWayDto.getId() != null && foundElements.get(0).getId() != gdprApproveWayDto.getId())) {
                throw new NameOccupiedException("Agree approve way with this name already exists");
            }
        }
    }

    private LocalDateTime getNewTokenExpirationTime() {
        return LocalDateTime.now().plusDays(gdprTokenExpirationDays);
    }

    private String getToken() {
        return UUID.randomUUID().toString();
    }
}
