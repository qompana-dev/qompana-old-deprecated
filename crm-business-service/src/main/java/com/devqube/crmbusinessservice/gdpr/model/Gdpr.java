package com.devqube.crmbusinessservice.gdpr.model;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Gdpr {

    @Id
    @SequenceGenerator(name = "gdpr_seq", sequenceName = "gdpr_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gdpr_seq")
    private Long id;

    @ManyToOne
    @ToString.Exclude
    @JoinColumn(name = "contact_id", unique = true)
    private Contact contact;

    @ToString.Exclude
    @OneToMany(mappedBy = "gdpr", cascade = CascadeType.REMOVE)
    private Set<GdprApprove> gdprApproves;

    private String token;
    private LocalDateTime tokenExpirationDate;
}
