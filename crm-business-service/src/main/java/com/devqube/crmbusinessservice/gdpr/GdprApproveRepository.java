package com.devqube.crmbusinessservice.gdpr;

import com.devqube.crmbusinessservice.gdpr.model.GdprApprove;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface GdprApproveRepository extends JpaRepository<GdprApprove, Long> {
    List<GdprApprove> findAllByGdprId(Long gdprId);
    List<GdprApprove> findAllByGdprContactId(Long gdprContactId);


    void deleteAllByGdprIdAndGdprTypeIdIn(Long gdprId, Set<Long> gdprTypeIds);

    @Query("select count(ga) from GdprApprove ga " +
            "left join ga.gdprStatus s " +
            "left join ga.gdpr g " +
            "left join ga.gdprType gt " +
            "left join g.contact c " +
            "where c.email = :email and s.defaultStatus = com.devqube.crmbusinessservice.gdpr.model.GdprDefaultStatus.CHECKED " +
            "and gt.isMailType = true")
    Long countApprovingAgreementsByContactEmail(@Param("email") String email);
}
