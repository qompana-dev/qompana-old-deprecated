package com.devqube.crmbusinessservice.gdpr.util;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.gdpr.model.Gdpr;
import com.devqube.crmbusinessservice.gdpr.model.GdprVariable;

import static com.devqube.crmbusinessservice.gdpr.model.GdprVariable.GDPR_LIST;

public class GdprVariableUtil {

    public static String getForm(String baseTest, Gdpr gdpr, String clientUrl) {
        return replace(false, baseTest, gdpr, clientUrl);
    }

    public static String getEmail(String baseTest, Gdpr gdpr, String clientUrl) {
        return replace(true, baseTest, gdpr, clientUrl);
    }

    private static String replace(boolean isEmail, String text, Gdpr gdpr, String clientUrl) {
        String result = text;

        GdprVariable[] values = GdprVariable.values();
        for (GdprVariable value : values) {
            if ((isEmail && value.isEmail()) || (!isEmail && value.isForm())) {
                result = result.replaceAll(value.getReplaceRegex(), getReplaceValue(value, gdpr, clientUrl));
            }
        }
        return result;
    }

    private static String getReplaceValue(GdprVariable value, Gdpr gdpr, String clientUrl) {
        if (value.getGroup().equals(GdprVariable.GdprValueGroup.CONTACT)) {
            return getContactReplaceValue(value, gdpr.getContact());
        }
        switch (value.getGroup()) {
            case CONTACT:
                return getContactReplaceValue(value, gdpr.getContact());
            case CUSTOMER:
                return getCustomerReplaceValue(value, gdpr.getContact());
            case GDPR:
                return getGdprReplaceValue(value, gdpr, clientUrl);
            default:
                return "";
        }
    }

    private static String getGdprReplaceValue(GdprVariable value, Gdpr gdpr, String clientUrl) {
        if (gdpr == null) {
            return "";
        }
        switch (value) {
            case GDPR_LIST:
                return "{{" + GDPR_LIST.name() + "}}"; // replaced in GUI
            case FORM_LINK:
                String url = clientUrl + "gdpr-token/" + gdpr.getToken();
                return "<a href=\"" + url + "\">" + url + "</a>";
            case FORM_EXPIRATION_DATE:
                return gdpr.getTokenExpirationDate().toString();
            default:
                return "";
        }
    }

    private static String getCustomerReplaceValue(GdprVariable value, Contact contact) {
        if (contact == null || contact.getCustomer() == null) {
            return "";
        }
        switch (value) {
            case COMPANY_NAME:
                return contact.getCustomer().getName();
            case COMPANY_WWW:
                return contact.getCustomer().getWebsite();
            case COMPANY_PHONE:
                return contact.getCustomer().getPhone();
            default:
                return "";
        }
    }

    private static String getContactReplaceValue(GdprVariable value, Contact contact) {
        if (contact == null) {
            return "";
        }
        switch (value) {
            case CONTACT_NAME:
                return contact.getName();
            case CONTACT_SURNAME:
                return contact.getSurname();
            case CONTACT_POSITION:
                return contact.getPosition();
            case CONTACT_EMAIL:
                return contact.getEmail();
            case CONTACT_PHONE:
                return contact.getPhone();
            default:
                return "";
        }
    }
}
