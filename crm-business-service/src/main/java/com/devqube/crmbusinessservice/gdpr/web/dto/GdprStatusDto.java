package com.devqube.crmbusinessservice.gdpr.web.dto;

import com.devqube.crmbusinessservice.gdpr.model.GdprDefaultStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GdprStatusDto {

    private Long id;
    private String name;
    private Boolean modifiable;
    private GdprDefaultStatus defaultStatus;

}
