package com.devqube.crmbusinessservice.gdpr.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmailDataDto {
    private Long contactId;
    private String token;
    private String emailSubject;
    private String emailContent;
}
