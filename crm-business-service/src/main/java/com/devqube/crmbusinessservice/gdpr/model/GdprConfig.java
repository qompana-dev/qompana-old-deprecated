package com.devqube.crmbusinessservice.gdpr.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GdprConfig {

    @Id
    @SequenceGenerator(name = "gdpr_config_seq", sequenceName = "gdpr_config_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gdpr_config_seq")
    private Long id;

    @NotNull
    private String formContent;
    @NotNull
    private String mailTitle;
    @NotNull
    private String mailContent;

}
