package com.devqube.crmbusinessservice.gdpr.web;

import com.devqube.crmbusinessservice.customer.exception.NameOccupiedException;
import com.devqube.crmbusinessservice.gdpr.GdprService;
import com.devqube.crmbusinessservice.gdpr.exception.TokenExpiredException;
import com.devqube.crmbusinessservice.gdpr.exception.UnableToModifyException;
import com.devqube.crmbusinessservice.gdpr.model.GdprVariable;
import com.devqube.crmbusinessservice.gdpr.web.dto.*;
import com.devqube.crmbusinessservice.gpdr.web.dto.FormDataApproveUpdateDto;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.access.annotation.AuthControllerCase;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.mail.gdpr.EmailCheckDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RestController
public class GdprController implements GdprApi {

    private final GdprService gdprService;

    public GdprController(GdprService gdprService) {
        this.gdprService = gdprService;
    }

    @Override
    @GetMapping("/gdpr/types")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(readFrontendId = "accessDenied", controllerCase = {
            @AuthControllerCase(type = "contactView", readFrontendId = "ContactGdprComponent.shareGdpr"),
            @AuthControllerCase(type = "systemSettings", readFrontendId = "NavigationComponent.settingItem.administration"),
    })
    public List<GdprTypeDto> getAllTypes() { // TODO[DG]: GdprType -> GdprTypeDto
        return gdprService.getAllTypes();
    }

    @Override
    @DeleteMapping("/gdpr/types/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AuthController(actionFrontendId = "NavigationComponent.settingItem.administration")
    public void deleteGdprType(@PathVariable("id") Long id) throws EntityNotFoundException, UnableToModifyException {
        gdprService.deleteGdprType(id);
    }

    @Override
    @PostMapping("/gdpr/types")
    @ResponseStatus(HttpStatus.CREATED)
    @AuthController(actionFrontendId = "NavigationComponent.settingItem.administration")
    public ResponseEntity<Void> saveGdprType(@RequestBody GdprTypeDto gdprType) {
        try {
            gdprService.saveGdprType(gdprType);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NameOccupiedException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @Override
    @PutMapping("/gdpr/types")
    @ResponseStatus(HttpStatus.CREATED)
    @AuthController(actionFrontendId = "NavigationComponent.settingItem.administration")
    public ResponseEntity<Void> updateGdprType(@RequestBody GdprTypeDto gdprType) {
        try {
            gdprService.updateGdprType(gdprType);
            return new ResponseEntity<>(HttpStatus.OK);
        }  catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (NameOccupiedException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @GetMapping("/gdpr/statuses")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(readFrontendId = "accessDenied", controllerCase = {
            @AuthControllerCase(type = "contactView", readFrontendId = "ContactGdprComponent.manualChangeGdpr"),
            @AuthControllerCase(type = "systemSettings", readFrontendId = "NavigationComponent.settingItem.administration"),
    })
    public List<GdprStatusDto> getAllStatuses() {
        return gdprService.getAllStatuses();
    }

    @Override
    @DeleteMapping("/gdpr/statuses/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AuthController(actionFrontendId = "NavigationComponent.settingItem.administration")
    public void deleteGdprStatus(@PathVariable("id") Long id) throws EntityNotFoundException, UnableToModifyException {
        gdprService.deleteStatus(id);
    }

    @Override
    @PostMapping("/gdpr/statuses")
    @ResponseStatus(HttpStatus.CREATED)
    @AuthController(actionFrontendId = "NavigationComponent.settingItem.administration")
    public ResponseEntity<Void> saveGdprStatus(@RequestBody GdprStatusDto gdprStatusDto) {
        try {
            gdprService.saveStatus(gdprStatusDto);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NameOccupiedException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @PutMapping("/gdpr/statuses")
    @AuthController(actionFrontendId = "NavigationComponent.settingItem.administration")
    public ResponseEntity<Void> updateGdprStatus(@RequestBody GdprStatusDto gdprStatusDto) {
        try {
            gdprService.updateStatus(gdprStatusDto);
            return new ResponseEntity<>(HttpStatus.OK);
        }  catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (NameOccupiedException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @GetMapping("/gdpr/agree-way")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(readFrontendId = "accessDenied", controllerCase = {
            @AuthControllerCase(type = "contactView", readFrontendId = "ContactGdprComponent.manualChangeGdpr"),
            @AuthControllerCase(type = "systemSettings", readFrontendId = "NavigationComponent.settingItem.administration"),
    })
    public List<GdprApproveWayDto> getAllApproveWays() {
        return gdprService.getAllApproveWays();
    }

    @Override
    @DeleteMapping("/gdpr/agree-way/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AuthController(actionFrontendId = "NavigationComponent.settingItem.administration")
    public void deleteGdprApproveWay(@PathVariable("id") Long id) throws EntityNotFoundException, UnableToModifyException {
        gdprService.deleteApproveWay(id);
    }

    @Override
    @PostMapping("/gdpr/agree-way")
    @ResponseStatus(HttpStatus.CREATED)
    @AuthController(actionFrontendId = "NavigationComponent.settingItem.administration")
    public ResponseEntity<Void> saveGdprApproveWay(@RequestBody GdprApproveWayDto gdprApproveWayDto) {
        try {
            gdprService.saveApproveWay(gdprApproveWayDto);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NameOccupiedException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @Override
    @PutMapping("/gdpr/agree-way")
    @AuthController(actionFrontendId = "NavigationComponent.settingItem.administration")
    public ResponseEntity<Void> updateGdprApproveWay(@RequestBody GdprApproveWayDto gdprApproveWayDto) {
        try {
            gdprService.updateApproveWay(gdprApproveWayDto);
            return new ResponseEntity<>(HttpStatus.OK);
        }  catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (NameOccupiedException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @GetMapping("/gdpr/approvals/contact/{contactId}")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(readFrontendId = "accessDenied", controllerCase = {
            @AuthControllerCase(type = "contactViewShare", readFrontendId = "ContactGdprComponent.shareGdpr"),
            @AuthControllerCase(type = "contactViewManualChange", readFrontendId = "ContactGdprComponent.manualChangeGdpr"),
    })
    public List<GdprApproveDto> getAllApprovalsForContact(@PathVariable("contactId") Long contactId) {
        return gdprService.getApprovalsForContact(contactId);
    }

    @Override
    @PutMapping("/gdpr/approvals/contact/{contactId}")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(actionFrontendId = "ContactGdprComponent.manualChangeGdpr")
    public List<GdprApproveDto> updateApprovalsForContact(@PathVariable("contactId") Long contactId, @RequestBody List<GdprApproveUpdateDto> list, @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail) throws BadRequestException, EntityNotFoundException {
        return gdprService.updateApprovalsForContact(contactId, list, loggedAccountEmail);
    }

    @Override
    @PutMapping("/gdpr/approve/contact/{contactId}")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(actionFrontendId = "ContactGdprComponent.manualChangeGdpr")
    public GdprApproveDto updateApprovalForContact(@PathVariable("contactId") Long contactId, @RequestBody GdprApproveUpdateDto approveUpdateDto, @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail) throws BadRequestException, EntityNotFoundException {
        return gdprService.updateApprovalForContact(contactId, approveUpdateDto, loggedAccountEmail);
    }

    @Override
    @PostMapping("/gdpr/types/contact/{contactId}")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(actionFrontendId = "ContactGdprComponent.shareGdpr")
    public void setGdprTypesSentToContact(@PathVariable("contactId") Long contactId, @RequestBody Set<Long> typeId, @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail) throws EntityNotFoundException, BadRequestException {
        gdprService.setGdprTypesSentToContact(contactId, typeId, loggedAccountEmail);
    }

    @Override
    @GetMapping("/gdpr/config/form")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(readFrontendId = "NavigationComponent.settingItem.administration")
    public FormConfigDto getGdprFormConfig() {
        return gdprService.getFormConfig();
    }

    @Override
    @GetMapping("/gdpr/config/mail")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(readFrontendId = "NavigationComponent.settingItem.administration")
    public MailConfigDto getGdprMailConfig() {
        return gdprService.getMailConfig();
    }

    @Override
    @PostMapping("/gdpr/config/form")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(actionFrontendId = "NavigationComponent.settingItem.administration")
    public FormConfigDto saveGdprFormConfig(@RequestBody FormConfigDto formConfig) {
        return gdprService.saveFormConfig(formConfig);
    }

    @Override
    @PostMapping("/gdpr/config/mail")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(actionFrontendId = "NavigationComponent.settingItem.administration")
    public MailConfigDto saveGdprMailConfig(@RequestBody MailConfigDto mailConfig) {
        return gdprService.saveMailConfig(mailConfig);
    }

    @Override
    @GetMapping("/gdpr/variables/email")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(readFrontendId = "NavigationComponent.settingItem.administration")
    public List<String> getAvailableGdprEmailVariables() {
        return Arrays.stream(GdprVariable.values())
                .filter(GdprVariable::isEmail)
                .map(Enum::toString).collect(Collectors.toList());
    }

    @Override
    @GetMapping("/gdpr/variables/form")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(readFrontendId = "NavigationComponent.settingItem.administration")
    public List<String> getAvailableGdprFormVariables() {
        return Arrays.stream(GdprVariable.values())
                .filter(GdprVariable::isForm)
                .map(Enum::toString).collect(Collectors.toList());
    }

    @Override
    @GetMapping("/gdpr/email/contactId/{contactId}")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(readFrontendId = "ContactGdprComponent.viewGdprLink")
    public EmailDataDto getGdprEmailByContactId(@PathVariable("contactId") Long contactId) throws EntityNotFoundException, TokenExpiredException {
        return gdprService.getGdprEmailByContactId(contactId);
    }

    @Override
    @GetMapping("/gdpr/form/token/{token}")
    @ResponseStatus(HttpStatus.OK) // should be without permissions
    public FormDataDto getGdprFormByToken(@PathVariable("token") String token) throws EntityNotFoundException, TokenExpiredException {
        return gdprService.getGdprFormByToken(token);
    }

    @Override
    @PutMapping("/gdpr/form/token/{token}")
    @ResponseStatus(HttpStatus.OK) // should be without permissions
    public FormDataDto updateGdprApprovalListForToken(@PathVariable("token") String token, @RequestBody List<FormDataApproveUpdateDto> approvalList) throws EntityNotFoundException, TokenExpiredException {
        gdprService.updateGdprApprovalListForToken(token, approvalList);
        return gdprService.getGdprFormByToken(token);
    }

    @Override
    @GetMapping("/gdpr/token/contact/{contactId}")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(readFrontendId = "ContactGdprComponent.viewGdprLink")
    public GdprTokenInfoDto getGdprTokenInfoByContactId(@PathVariable("contactId") Long contactId) throws EntityNotFoundException {
        return gdprService.getGdprTokenInfoByContactId(contactId);
    }

    @Override
    @PostMapping("/gdpr/token/contact/{contactId}")
    @ResponseStatus(HttpStatus.OK)
    @AuthController(actionFrontendId = "ContactGdprComponent.createGdprLink")
    public GdprTokenInfoDto createGdprTokenForContactId(@PathVariable("contactId") Long contactId) throws EntityNotFoundException {
        return gdprService.createGdprTokenForContactId(contactId);
    }

    @Override
    @PostMapping("/internal/gdpr/email/check")
    @ResponseStatus(HttpStatus.OK) // internal endpoint
    public List<EmailCheckDto> checkGdprInEmails(@RequestBody List<String> emails) {
        return gdprService.checkGdprInEmails(emails);
    }
}
