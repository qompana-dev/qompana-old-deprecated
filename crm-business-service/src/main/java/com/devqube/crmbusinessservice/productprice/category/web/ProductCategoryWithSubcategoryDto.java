package com.devqube.crmbusinessservice.productprice.category.web;

import com.devqube.crmbusinessservice.productprice.category.ProductCategory;
import lombok.*;

import java.util.Set;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ProductCategoryWithSubcategoryDto {
    private Long id;
    private String name;
    private Set<ProductSubcategoryDto> subcategories;
    private boolean assignedToAnyProduct;

    public ProductCategoryWithSubcategoryDto(ProductCategory productCategory) {
        this.id = productCategory.getId();
        this.name = productCategory.getName();
        this.subcategories = productCategory.getSubcategories().stream().map(ProductSubcategoryDto::new).collect(Collectors.toSet());
        this.assignedToAnyProduct = false; // productCategory.getSubcategories().stream().anyMatch(sub -> !sub.getProducts().isEmpty());
    }

}
