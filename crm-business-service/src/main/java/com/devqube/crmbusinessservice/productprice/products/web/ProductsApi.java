package com.devqube.crmbusinessservice.productprice.products.web;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.productprice.products.web.dto.ProductDTO;
import com.devqube.crmshared.search.CrmObject;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Validated
@Api(value = "Products", description = "the Products API")
public interface ProductsApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "get product", nickname = "getProduct", notes = "Method used to get product by id", response = ProductDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "get product successfully", response = ProductDTO.class)})
    @RequestMapping(value = "/products/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<ProductDTO> getProduct(@ApiParam(value = "product id", required = true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"code\" : \"code\",  \"notes\" : \"notes\",  \"vat\" : 2,  \"active\" : true,  \"description\" : \"description\",  \"specification\" : \"specification\",  \"purchasePrice\" : 5.962133916683182377482808078639209270477294921875,  \"photos\" : [ \"\", \"\" ],  \"manufacturer\" : \"manufacturer\",  \"sellPriceNet\" : 5.63737665663332876420099637471139430999755859375,  \"supplier\" : \"supplier\",  \"name\" : \"name\",  \"subcategoryId\" : 1,  \"currency\" : \"currency\",  \"id\" : 0,  \"brand\" : \"brand\",  \"skuCode\" : \"skuCode\",  \"categoryId\" : 6}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get products", nickname = "getProducts", notes = "get page of all products", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "products page retrieved successfully", response = org.springframework.data.domain.Page.class)})
    @RequestMapping(value = "/products",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Page> getProducts(@ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable, @ApiParam(value = "only products added today") @Valid @RequestParam(value = "today", required = false) Boolean today, @ApiParam(value = "only last edited products") @Valid @RequestParam(value = "edited", required = false) Boolean edited, @ApiParam(value = "only last viewed products") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get products for mobile", nickname = "getProductsForMobile", notes = "get page of all products for mobile", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "products page retrieved successfully", response = org.springframework.data.domain.Page.class)})
    @RequestMapping(value = "/mobile/products",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<Page> getProductsForMobile(@ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable, @ApiParam(value = "only products added today") @Valid @RequestParam(value = "today", required = false) Boolean today, @ApiParam(value = "only last edited products") @Valid @RequestParam(value = "edited", required = false) Boolean edited, @ApiParam(value = "only last viewed products") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed, @ApiParam(value = "search") @Valid @RequestParam(value = "search", required = false) String search) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get product sales history", nickname = "getProductSalesHistory",
            notes = "Get all sales histories of a product by its id value, " +
                    "you can also get sales histories that ended with a specific finishResult: ERROR or SUCCESS.",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "product sales list retrieved successfully", response = List.class),
            @ApiResponse(code = 404, message = "this product doesn't exists")})
    @RequestMapping(value = "/product/{id}/salesHistory",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List> getProductSalesHistory(@ApiParam(value = "product id", required = true) @PathVariable("id") Long productId,
                                                        @ApiParam(value = "Only product sales that ended with status")
                                                        @Valid @RequestParam(value = "finishResult", required = false) Opportunity.FinishResult finishResult) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Add product", nickname = "saveProduct", notes = "Add single product", response = ProductDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "product added successfully", response = ProductDTO.class),
            @ApiResponse(code = 400, message = "product code is occupied")})
    @RequestMapping(value = "/products",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<ProductDTO> saveProduct(@ApiParam(value = "Product to add") @Valid @RequestBody ProductDTO productDTO) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"code\" : \"code\",  \"productBrand\" : \"productBrand\",  \"vat\" : 7,  \"active\" : true,  \"description\" : \"description\",  \"specification\" : \"specification\",  \"purchasePrice\" : 5.962133916683182377482808078639209270477294921875,  \"tradeMargin\" : 2,  \"photos\" : [ \"\", \"\" ],  \"manufacturer\" : \"manufacturer\",  \"unit\" : \"jednostka1\",  \"sellPriceNet\" : 5.63737665663332876420099637471139430999755859375,  \"name\" : \"name\",  \"subcategoryId\" : 1,  \"typeOfSupplier\" : \"producent1\",  \"currency\" : \"currency\",  \"id\" : 0,  \"profit\" : 9.301444243932575517419536481611430644989013671875,  \"skuCode\" : \"skuCode\",  \"categoryId\" : 6}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Delete product", nickname = "deleteProduct", notes = "Method used to delete product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "product removed"),
            @ApiResponse(code = 404, message = "product not found")})
    @RequestMapping(value = "/products/{id}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteProduct(@ApiParam(value = "The product ID", required = true) @PathVariable("id") Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "delete products", nickname = "deleteProducts", notes = "Method used to delete products")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "deleted products successfully"),
            @ApiResponse(code = 404, message = "one of contacts not found")})
    @RequestMapping(value = "/products",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteProducts(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update product", nickname = "updateProduct", notes = "Method used to modify product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "product updated"),
            @ApiResponse(code = 400, message = "validation error", response = String.class),
            @ApiResponse(code = 404, message = "product not found"),
            @ApiResponse(code = 409, message = "product with this name or nip already exists")})
    @RequestMapping(value = "/products/{id}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> updateProduct(@ApiParam(value = "The product ID", required = true) @PathVariable("id") Long id, @ApiParam(value = "Modified product object", required = true) @Valid @RequestBody ProductDTO productDTO) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get products for price book", nickname = "getProductsForPriceBook", notes = "get list of all products for price book", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "products list retrieved successfully", response = org.springframework.data.domain.Page.class)})
    @RequestMapping(value = "/products/for-price-book/{priceBookId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Page> getProductsForPriceBook(@ApiParam(value = "price book id", required = true) @PathVariable("priceBookId") Long priceBookId, @ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable, @ApiParam(value = "only products added today") @Valid @RequestParam(value = "today", required = false) Boolean today, @ApiParam(value = "only last edited products") @Valid @RequestParam(value = "edited", required = false) Boolean edited, @ApiParam(value = "only last viewed products") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "Get product list for search", nickname = "getProductsForSearch", notes = "Method used to get product list for search", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all product names", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List") })
    @RequestMapping(value = "/product/search",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<List<CrmObject>> getProductsForSearch(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get products list by ids", nickname = "getProductsAsCrmObject", notes = "Method used to get products as crm objects by ids", response = CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found leads", response = CrmObject.class, responseContainer = "List") })
    @RequestMapping(value = "/product/crm-object",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<List<CrmObject>> getProductsAsCrmObject(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "get all products with access for user", nickname = "getAllProductsWithAccess", notes = "Get all products that current user has access to", response = Long.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of products for specific user", response = Long.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Email not correct") })
    @RequestMapping(value = "/access/product",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<List<Long>> getAllProductsWithAccess(@ApiParam(value = "" ,required=true) @RequestHeader(value="Logged-Account-Email", required=true) String loggedAccountEmail) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "0");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
