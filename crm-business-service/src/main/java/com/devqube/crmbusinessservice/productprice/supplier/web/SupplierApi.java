package com.devqube.crmbusinessservice.productprice.supplier.web;

import com.devqube.crmbusinessservice.productprice.supplier.Supplier;
import com.devqube.crmshared.search.CrmObject;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
@Api(value = "Supplier")
public interface SupplierApi {
    @ApiOperation(value = "save new suppliers", nickname = "saveSuppliers", notes = "save new suppliers")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "supplier saved successfully", response = Supplier.class),
            @ApiResponse(code = 400, message = "one of suppliers name is occupied")
    })
    ResponseEntity save(@ApiParam(value="suppliers to save") List<Supplier> suppliers);

    @ApiOperation(value = "get page of suppliers", nickname = "findAllSuppliers", notes = "find all suppliers", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "page retrieved successfully", response = Page.class)
    })
    ResponseEntity<Page<Supplier>> findAll(Pageable pageable);

    @ApiOperation(value = "delete supplier by id", nickname = "deleteSupplier", notes = "delete supplier by id")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "supplier deleted"),
            @ApiResponse(code = 400, message = "cannot delete supplier, it's attached to products")
    })
    ResponseEntity deleteById(@ApiParam(value = "supplier id") Long id);

    @ApiOperation(value = "Get suppliers list for search", nickname = "getSuppliersForSearch", notes = "Method used to get suppliers list", response = CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all suppliers names", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List") })
    ResponseEntity<List<CrmObject>> getSuppliersForSearch(String term);

    @ApiOperation(value = "Get suppliers list by ids", nickname = "getSuppliersAsCrmObject", notes = "Method used to get suppliers as crm objects by ids", response = CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found manufacturer", response = CrmObject.class, responseContainer = "List") })
    ResponseEntity<List<CrmObject>> getSuppliersAsCrmObject(List<Long> ids);
}
