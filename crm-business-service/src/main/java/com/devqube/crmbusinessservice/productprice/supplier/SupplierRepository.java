package com.devqube.crmbusinessservice.productprice.supplier;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SupplierRepository extends JpaRepository<Supplier, Long> {
    @Query("select case when count(s)> 0 then true else false end from Supplier s where lower(s.name) = lower(:name)")
    boolean existsByName(@Param("name") String name);

    List<Supplier> findByIdIn(List<Long> ids);

    List<Supplier> findTop20ByNameContaining(String pattern);
}
