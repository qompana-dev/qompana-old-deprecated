package com.devqube.crmbusinessservice.productprice.products.opportunityProducts;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.dictionary.DictionaryService;
import com.devqube.crmbusinessservice.dictionary.Type;
import com.devqube.crmbusinessservice.dictionary.dto.DictionaryDTO;
import com.devqube.crmbusinessservice.dictionary.dto.WordDTO;
import com.devqube.crmbusinessservice.leadopportunity.AccountsService;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.productprice.exception.ProductCodeOccupiedException;
import com.devqube.crmbusinessservice.productprice.manufacturer.ManufacturerRepository;
import com.devqube.crmbusinessservice.productprice.products.ProductRepository;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductAddFromPriceBookDTO;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductDTO;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductEditDTO;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductSaveDTO;
import com.devqube.crmbusinessservice.productprice.supplier.SupplierRepository;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class OpportunityProductsService {

    private final OpportunityRepository opportunityRepository;
    private final OpportunityProductsRepository opportunityProductsRepository;
    private final UserClient userClient;
    private final OpportunityProductMapper opportunityProductMapper;
    private final ProductRepository productRepository;
    private final AccessService accessService;
    private final ManufacturerRepository manufacturerRepository;
    private final SupplierRepository supplierRepository;
    private final AccountsService accountsService;
    private final DictionaryService dictionaryService;

    public OpportunityProductsService(OpportunityRepository opportunityRepository,
                                      OpportunityProductsRepository opportunityProductsRepository,
                                      UserClient userClient,
                                      OpportunityProductMapper opportunityProductMapper,
                                      ProductRepository productRepository,
                                      AccessService accessService,
                                      ManufacturerRepository manufacturerRepository,
                                      SupplierRepository supplierRepository,
                                      AccountsService accountsService,
                                      DictionaryService dictionaryService) {
        this.opportunityRepository = opportunityRepository;
        this.opportunityProductsRepository = opportunityProductsRepository;
        this.userClient = userClient;
        this.opportunityProductMapper = opportunityProductMapper;
        this.productRepository = productRepository;
        this.accessService = accessService;
        this.manufacturerRepository = manufacturerRepository;
        this.supplierRepository = supplierRepository;
        this.accountsService = accountsService;
        this.dictionaryService = dictionaryService;
    }

    @Transactional
    public Page<OpportunityProductDTO> getOpportunityProducts(Long id, String loggedAccountEmail, Pageable pageable)
            throws BadRequestException, EntityNotFoundException, PermissionDeniedException {
        Opportunity opportunity = this.opportunityRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        checkIfHasPermissionAndThrowIfNot(userIds, opportunity);
        Page<OpportunityProduct> opportunityProducts = opportunityProductsRepository.findAllByOpportunityId(opportunity.getId(), pageable);
        Map<Long, String> units = getUnitsMap();
        return opportunityProducts.map(opportunityProduct -> opportunityProductMapper.mapToDTO(opportunityProduct, units));
    }

    @Transactional(rollbackFor = Exception.class)
    public void addOpportunityProductManually(Long id, String loggedAccountEmail, OpportunityProductSaveDTO dto)
            throws EntityNotFoundException, BadRequestException, PermissionDeniedException, ProductCodeOccupiedException {
        Opportunity opportunity = this.opportunityRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        checkIfHasPermissionAndThrowIfNot(userIds, opportunity);
        dto = accessService.assignAndVerify(dto);
        Product product = opportunityProductMapper.mapToProduct(dto);
        setManufacturerAndSupplier(product, dto.getManufacturerId(), dto.getSupplierId());
        checkCodesAreUniqueAndThrowIfNot(dto.getCode(), dto.getSkuCode());
        Product savedProduct = productRepository.save(product);
        productRepository.flush();
        OpportunityProduct opportunityProduct = opportunityProductMapper.mapToOpportunityProduct(dto, product, opportunity);
        opportunityProduct.setCurrentSellPriceNet(savedProduct.getSellPriceNet());
        opportunityProductsRepository.save(opportunityProduct);
        opportunityProductsRepository.flush();
        updateOpportunityProductAmount(id, false);
    }

    public void removeOpportunityProduct(Long opportunityId, Long productId, String loggedAccountEmail)
            throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Opportunity opportunity = this.opportunityRepository.findById(opportunityId).orElseThrow(EntityNotFoundException::new);
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        checkIfHasPermissionAndThrowIfNot(userIds, opportunity);
        OpportunityProduct opportunityProduct = this.opportunityProductsRepository.findById(productId).orElseThrow(EntityNotFoundException::new);
        deleteOpportunityProduct(opportunityProduct);
        updateOpportunityProductAmount(opportunityId, true);
    }

    public void removeOpportunityProductsByOpportunityId(Long opportunityId) throws EntityNotFoundException {
        Opportunity opportunity = this.opportunityRepository.findById(opportunityId).orElseThrow(EntityNotFoundException::new);
        opportunity.getOpportunityProducts().forEach(this::deleteOpportunityProduct);
    }

    public void editOpportunityProduct(Long opportunityId, Long productId, String loggedAccountEmail,
                                       OpportunityProductEditDTO opportunityProductEditDTO) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Opportunity opportunity = this.opportunityRepository.findById(opportunityId).orElseThrow(EntityNotFoundException::new);
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        checkIfHasPermissionAndThrowIfNot(userIds, opportunity);
        OpportunityProduct opportunityProduct = this.opportunityProductsRepository.findById(productId).orElseThrow(EntityNotFoundException::new);
        setAllChangesToOpportunityFromDB(opportunityProduct, opportunityProductEditDTO);
        opportunityProduct.setCurrentSellPriceNet(opportunityProduct.getProduct().getSellPriceNet());
        this.opportunityProductsRepository.save(opportunityProduct);
        updateOpportunityProductAmount(opportunityId, false);
    }

    public OpportunityProductEditDTO getOpportunityProductById(Long opportunityId, Long productId, String loggedAccountEmail)
            throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Opportunity opportunity = this.opportunityRepository.findById(opportunityId).orElseThrow(EntityNotFoundException::new);
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        checkIfHasPermissionAndThrowIfNot(userIds, opportunity);
        OpportunityProduct opportunityProduct = this.opportunityProductsRepository.findById(productId).orElseThrow(EntityNotFoundException::new);
        return new OpportunityProductEditDTO(
                opportunityProduct.getId(),
                opportunityProduct.getProduct().getName(),
                opportunityProduct.getPrice(),
                opportunityProduct.getQuantity(),
                opportunityProduct.getProduct().getUnitId(),
                opportunityProduct.getProduct().getOpportunityProduct()
        );
    }

    private void deleteOpportunityProduct(OpportunityProduct opportunityProduct) {
        Product product = opportunityProduct.getProduct();
        this.opportunityProductsRepository.deleteById(opportunityProduct.getId());
        if (product.getOpportunityProduct()) {
            product.setDeleted(true);
            product.setCode(UUID.randomUUID().toString());
            product.setSkuCode(UUID.randomUUID().toString());
            productRepository.save(product);
        }
    }

    public void addOpportunityProducts(Long id, String loggedAccountEmail, List<OpportunityProductAddFromPriceBookDTO> opportunityProductDtoList) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Opportunity opportunity = this.opportunityRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        checkIfHasPermissionAndThrowIfNot(userIds, opportunity);

        Map<Long, Product> productMap = getProductMapByIds(opportunityProductDtoList
                .stream().map(OpportunityProductAddFromPriceBookDTO::getId).collect(Collectors.toList()));

        List<OpportunityProduct> toSave = opportunityProductDtoList.stream()
                .map(c -> c.toModel(opportunity, productMap.get(c.getId())))
                .collect(Collectors.toList());

        opportunityProductsRepository.saveAll(toSave);
        updateOpportunityProductAmount(id, false);
    }

    private void setManufacturerAndSupplier(Product toSave, Long manufacturerId, Long supplierId) {
        if (manufacturerId != null) {
            toSave.setManufacturer(manufacturerRepository.getOne(manufacturerId));
        }
        if (supplierId != null) {
            toSave.setSupplier(supplierRepository.getOne(supplierId));
        }
    }

    private Map<Long, Product> getProductMapByIds(List<Long> productIds) throws EntityNotFoundException {
        List<Product> allById = productRepository.findAllById(productIds);
        if (productIds.size() != allById.size()) {
            throw new EntityNotFoundException();
        }
        Map<Long, Product> result = new HashMap<>();
        allById.forEach(c -> result.put(c.getId(), c));
        return result;
    }

    private Long getUserId(String accountEmail) throws BadRequestException {
        try {
            return userClient.getMyAccountId(accountEmail);
        } catch (Exception e) {
            throw new BadRequestException("user not found");
        }
    }

    private void checkIfHasPermissionAndThrowIfNot(List<Long> userIds, Opportunity fromDb) throws PermissionDeniedException {
        if (!userIds.contains(fromDb.getOwnerOneId()) && !userIds.contains(fromDb.getOwnerTwoId()) &&
                !userIds.contains(fromDb.getOwnerThreeId()) && !userIds.contains(fromDb.getCreator())) {
            throw new PermissionDeniedException();
        }
    }

    private void checkCodesAreUniqueAndThrowIfNot(String code, String skuCode) throws ProductCodeOccupiedException {
        Optional<Product> byProductCode = productRepository.findByCode(code);
        Optional<Product> bySkuCode = productRepository.findBySkuCode(skuCode);
        if (byProductCode.isPresent() || bySkuCode.isPresent()) {
            throw new ProductCodeOccupiedException("Product with this product code or sku code already exists");
        }
    }

    private void setAllChangesToOpportunityFromDB(OpportunityProduct opportunityProduct, OpportunityProductEditDTO dto) {
        opportunityProduct.setPrice(dto.getSellPrice());
        opportunityProduct.setQuantity(dto.getQuantity());
        opportunityProduct.setPriceSummed(dto.getQuantity() * dto.getSellPrice());

        if(dto.getOpportunityProduct()) {
            opportunityProduct.getProduct().setUnitId(dto.getUnit());
        }
    }

    private void updateOpportunityProductAmount(Long opportunityId, boolean fromRemove) throws EntityNotFoundException {
        Opportunity opportunity = opportunityRepository.findById(opportunityId).orElseThrow(EntityNotFoundException::new);
        if (opportunity.getOpportunityProducts() != null && (fromRemove || !opportunity.getOpportunityProducts().isEmpty())) {
            double sum = opportunity.getOpportunityProducts().stream().map(OpportunityProduct::getPriceSummed).mapToDouble(c -> c).sum();
            opportunity.setProductAmount(sum);
            opportunityRepository.save(opportunity);
        }
    }

    private Map<Long, String> getUnitsMap() {
        DictionaryDTO dictionaryDTO = dictionaryService.findAdminDictionary(Type.UNIT.getDictionaryId());
        return dictionaryDTO.getWords().stream().collect(Collectors.toMap(WordDTO::getId, WordDTO::getName));
    }
}
