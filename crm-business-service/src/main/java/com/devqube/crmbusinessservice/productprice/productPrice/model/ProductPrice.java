package com.devqube.crmbusinessservice.productprice.productPrice.model;

import com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ProductPrice {
    @Id
    @SequenceGenerator(name = "product_price_seq", sequenceName = "product_price_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_price_seq")
    private Long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "product_id")
    @NotNull
    private Product product;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "price_book_id")
    @NotNull
    private PriceBook priceBook;

    @NotNull
    private Double price;

    @NotNull
    private boolean active;
}
