package com.devqube.crmbusinessservice.productprice.productPrice.web.dto;

import com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook;
import com.devqube.crmbusinessservice.productprice.productPrice.model.ProductPrice;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductPriceDto {
    private Long id;
    private Double price;
    private PriceBook priceBook;
    private boolean active;

    public ProductPrice toModel(Product product) {
        ProductPrice productPrice = new ProductPrice();
        productPrice.setId(this.id);
        productPrice.setProduct(product);
        productPrice.setPriceBook(this.priceBook);
        productPrice.setPrice(this.price);
        productPrice.setActive(this.active);
        return productPrice;
    }

    public static ProductPriceDto fromModel(ProductPrice productPrice) {
        ProductPriceDto dto = new ProductPriceDto();
        dto.setId(productPrice.getId());
        dto.setPrice(productPrice.getPrice());
        dto.setActive(productPrice.isActive());
        dto.setPriceBook(productPrice.getPriceBook());
        return dto;
    }
}
