package com.devqube.crmbusinessservice.productprice.category;

import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ProductSubcategory {

    @Id
    @SequenceGenerator(name = "product_subcategory_seq", sequenceName = "product_subcategory_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_subcategory_seq")
    private Long id;

    private String name;

    @Column(updatable = false)
    private boolean initial;

    @JsonIgnore
    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private ProductCategory productCategory;

    @ManyToMany()
    @JoinTable(name = "product2subcategory",
            joinColumns = @JoinColumn(name = "subcategory_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<Product> products = new HashSet<>();

}

