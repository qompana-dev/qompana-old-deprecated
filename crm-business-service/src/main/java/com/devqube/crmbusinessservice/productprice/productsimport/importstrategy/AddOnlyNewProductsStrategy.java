package com.devqube.crmbusinessservice.productprice.productsimport.importstrategy;

import com.devqube.crmbusinessservice.productprice.productPrice.ProductPriceService;
import com.devqube.crmbusinessservice.productprice.products.ProductService;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmshared.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AddOnlyNewProductsStrategy implements ImportStrategy {

    private final ProductService productService;
    private final ProductPriceService productPriceService;

    public AddOnlyNewProductsStrategy(ProductService productService, ProductPriceService productPriceService) {
        this.productService = productService;
        this.productPriceService = productPriceService;
    }

    @Override
    @Transactional(rollbackFor = {EntityNotFoundException.class})
    public void save(List<Product> products) throws EntityNotFoundException {
        Set<String> inputOriginalIds = products.stream().map(Product::getCode).collect(Collectors.toSet());
        Set<String> existingOriginalIds = productService.findExistingProductCodes(inputOriginalIds);
        List<Product> productsToSave = filterOutExistingProducts(products, existingOriginalIds);
        List<Product> saved = productService.saveAll(productsToSave);
        productPriceService.setDefaultPriceBookForProducts(saved);
    }

    private List<Product> filterOutExistingProducts(List<Product> products, Set<String> existingOriginalIds) {
        return products.stream().filter(product -> !existingOriginalIds.contains(product.getCode())).collect(Collectors.toList());
    }
}
