package com.devqube.crmbusinessservice.productprice.util;

import org.modelmapper.ModelMapper;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ModelMapperUtils {

    private static ModelMapper modelMapper = new ModelMapper();

    public static <D, T> List<D> mapAll(final Collection<T> entityList, Class<D> outClass) {
        return entityList.stream()
                .map(entity -> modelMapper.map(entity, outClass))
                .collect(Collectors.toList());
    }

    public static <D> D map(Object source, Class<D> destinationType) {
        return modelMapper.map(source, destinationType);
    }

    public static <D, T> List<D> mapAll(final Collection<T> entityList, Function<T, D> mapper) {
        return entityList.stream()
                .map(mapper)
                .collect(Collectors.toList());
    }


}
