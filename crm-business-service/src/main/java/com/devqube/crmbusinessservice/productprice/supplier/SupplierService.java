package com.devqube.crmbusinessservice.productprice.supplier;

import com.devqube.crmbusinessservice.productprice.products.ProductRepository;
import com.devqube.crmshared.exception.BadRequestException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SupplierService {

    private final SupplierRepository supplierRepository;
    private final ProductRepository productRepository;

    public SupplierService(SupplierRepository supplierRepository, ProductRepository productRepository) {
        this.supplierRepository = supplierRepository;
        this.productRepository = productRepository;
    }

    public void saveAll(List<Supplier> suppliers) throws BadRequestException {
        if (suppliers.size() != suppliers.stream().map(c -> c.getName().toLowerCase()).collect(Collectors.toSet()).size()) {
            throw new BadRequestException("The list contain two or more suppliers with the same name");
        }

        for (Supplier supplier: suppliers) {
            if (supplierRepository.existsByName(supplier.getName())) {
                throw new BadRequestException("Supplier with this name already exists");
            }
        }

        supplierRepository.saveAll(suppliers);
    }

    public Page<Supplier> findAll(Pageable pageable) {
        return supplierRepository.findAll(pageable);
    }

    public void deleteById(Long id) throws BadRequestException {
        if (this.productRepository.existsBySupplierId(id)) {
            throw new BadRequestException("Cannot delete this supplier. It's used in products");
        }
        supplierRepository.deleteById(id);
    }

    public List<Supplier> findAllContaining(String pattern) {
        return supplierRepository.findTop20ByNameContaining(pattern);
    }

    public List<Supplier> findAllByIds(List<Long> ids) {
        return supplierRepository.findByIdIn(ids);
    }
}
