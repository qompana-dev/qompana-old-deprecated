package com.devqube.crmbusinessservice.productprice.supplier.web;

import com.devqube.crmbusinessservice.productprice.supplier.Supplier;
import com.devqube.crmbusinessservice.productprice.supplier.SupplierService;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/supplier")
@Slf4j
public class SupplierController implements SupplierApi {

    private final SupplierService supplierService;

    public SupplierController(SupplierService supplierService) {
        this.supplierService = supplierService;
    }

    @Override
    @PostMapping
    @AuthController(actionFrontendId = "NavigationMenu.manufacturersAndSuppliers")
    public ResponseEntity save(@RequestBody List<Supplier> suppliers) {
        try {
            supplierService.saveAll(suppliers);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @GetMapping
    @AuthController(actionFrontendId = "NavigationMenu.manufacturersAndSuppliers")
    public ResponseEntity<Page<Supplier>> findAll(Pageable pageable) {
        return new ResponseEntity<>(supplierService.findAll(pageable), HttpStatus.OK);
    }

    @Override
    @DeleteMapping("/{id}")
    @AuthController(actionFrontendId = "NavigationMenu.manufacturersAndSuppliers")
    public ResponseEntity deleteById(@PathVariable("id") Long id) {
        try {
            supplierService.deleteById(id);
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        } catch (BadRequestException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @GetMapping("/search")
    public ResponseEntity<List<CrmObject>> getSuppliersForSearch(String term) {
        return new ResponseEntity<>(supplierService.findAllContaining(term)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.supplier, Strings.nullToEmpty(e.getName()), null))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    @GetMapping("/crm-object")
    public ResponseEntity<List<CrmObject>> getSuppliersAsCrmObject(
            @RequestParam(value = "ids", defaultValue = "new ArrayList<>()") List<Long> ids) {
        return new ResponseEntity<>(supplierService.findAllByIds(ids)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.supplier, Strings.nullToEmpty(e.getName()), null))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }
}
