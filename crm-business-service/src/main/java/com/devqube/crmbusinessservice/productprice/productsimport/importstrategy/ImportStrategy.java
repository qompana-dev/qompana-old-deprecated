package com.devqube.crmbusinessservice.productprice.productsimport.importstrategy;

import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmshared.exception.EntityNotFoundException;

import java.util.List;

public interface ImportStrategy {
    void save(List<Product> products) throws EntityNotFoundException;
}
