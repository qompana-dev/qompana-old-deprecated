package com.devqube.crmbusinessservice.productprice.products.model;

import com.devqube.crmbusinessservice.productprice.category.ProductSubcategory;
import com.devqube.crmbusinessservice.productprice.manufacturer.Manufacturer;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.OpportunityProduct;
import com.devqube.crmbusinessservice.productprice.supplier.Supplier;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "product")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ReadProduct {

    @Id
    @SequenceGenerator(name = "product_seq", sequenceName = "product_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_seq")
    private Long id;

    private String name;

    @EqualsAndHashCode.Include
    @Column(name = "code", unique = true, nullable = false)
    private String code;

    @EqualsAndHashCode.Include
    @Column(name = "sku_code", unique = true, nullable = false)
    private String skuCode;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "manufacturer")
    private Manufacturer manufacturer;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "supplier")
    private Supplier supplier;

    private String brand;

    private Double purchasePrice;

    private String purchasePriceCurrency;

    private String currency;

    private Double sellPriceNet;

    private Integer vat;

    private Boolean active = false;

    private String description;

    private String specification;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "unit")
    private ProductUnit unit;

    private String notes;

    private Boolean deleted = false;

    @CreationTimestamp
    private LocalDateTime created;

    @UpdateTimestamp
    private LocalDateTime updated;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product", orphanRemoval = true)
    @ToString.Exclude
    private Set<LastViewedProduct> lastViewedProducts;

    @OneToMany(mappedBy = "product")
    @ToString.Exclude
    private List<OpportunityProduct> opportunityProducts;

    private Boolean opportunityProduct = false;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, mappedBy = "products")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<ProductSubcategory> subcategories = new ArrayList<>();
}
