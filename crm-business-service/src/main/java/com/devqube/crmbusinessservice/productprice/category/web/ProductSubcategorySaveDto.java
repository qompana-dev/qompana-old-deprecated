package com.devqube.crmbusinessservice.productprice.category.web;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ProductSubcategorySaveDto {
    private Long id;
    private Long categoryId;
    private String name;
    private boolean toDelete;
}
