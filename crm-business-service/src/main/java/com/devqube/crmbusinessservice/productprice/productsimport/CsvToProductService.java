package com.devqube.crmbusinessservice.productprice.productsimport;

import com.devqube.crmbusinessservice.productprice.productsimport.dto.ProductDto;
import com.devqube.crmshared.importexport.ImportObjLine;
import com.univocity.parsers.common.DataProcessingException;
import com.univocity.parsers.common.ParsingContext;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.List;

@Service
public class CsvToProductService {

    List<ProductDto> convertToProducts(InputStream csvFile, Boolean headerPresent, List<DataProcessingException> errors) throws DataProcessingException {
        BeanListProcessor<ProductDto> rowProcessor = getRowProcessor(ProductDto.class);
        CsvParserSettings parserSettings = new CsvParserSettings();
        parserSettings.getFormat().setLineSeparator("\n");
        parserSettings.setDelimiterDetectionEnabled(true, ';');
        parserSettings.setProcessor(rowProcessor);
        parserSettings.setProcessorErrorHandler((e, objects, context) -> errors.add(e));
        parserSettings.setHeaderExtractionEnabled(headerPresent);
        CsvParser parser = new CsvParser(parserSettings);
        parser.parse(csvFile, "Windows-1250");
        return rowProcessor.getBeans();
    }

    private static <T extends ImportObjLine> BeanListProcessor<T> getRowProcessor(Class<T> clazz) {
        return new BeanListProcessor<>(clazz) {
            @Override
            public void beanProcessed(T bean, ParsingContext context) {
                bean.setLine(context.currentLine());
                super.beanProcessed(bean, context);
            }
        };
    }
}
