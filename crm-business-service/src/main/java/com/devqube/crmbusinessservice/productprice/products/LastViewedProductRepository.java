package com.devqube.crmbusinessservice.productprice.products;

import com.devqube.crmbusinessservice.productprice.products.model.LastViewedProduct;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LastViewedProductRepository extends JpaRepository<LastViewedProduct, Long> {
    void deleteAllByProductIdIn(List<Long> productIds);
}
