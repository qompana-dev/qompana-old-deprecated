package com.devqube.crmbusinessservice.productprice.products.opportunityProducts;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OpportunityProductsRepository extends JpaRepository<OpportunityProduct, Long> {

    Page<OpportunityProduct> findAllByOpportunityId(Long id, Pageable pageable);

    List<OpportunityProduct> findAllByProductIdAndOpportunityFinishResultNotNull(Long productId);

    List<OpportunityProduct> findAllByProductIdAndOpportunityFinishResult(Long productId, Opportunity.FinishResult finishResult);
}
