package com.devqube.crmbusinessservice.productprice.category.web;

import com.devqube.crmbusinessservice.productprice.category.ProductCategory;
import com.devqube.crmbusinessservice.productprice.category.ProductCategoryService;
import com.devqube.crmbusinessservice.productprice.exception.ObjectUsedInGoalException;
import com.devqube.crmbusinessservice.productprice.util.ModelMapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
public class ProductCategoryController implements ProductCategoryApi {

    private final ProductCategoryService productCategoryService;

    public ProductCategoryController(ProductCategoryService productCategoryService) {
        this.productCategoryService = productCategoryService;
    }

    @Override
    public ResponseEntity<List<ProductCategoryDto>> getAllCategories() {
        List<ProductCategoryDto> categories = productCategoryService.findAllCategories();
        return ResponseEntity.ok(categories);
    }

    @Override
    public ResponseEntity<List<ProductCategoryWithSubcategoryDto>> getCategoriesWithSubcategories(List<Long> categoryIds) {
        List<ProductCategoryWithSubcategoryDto> categories = productCategoryService.findAllByIds(categoryIds);
        return ResponseEntity.ok(categories);
    }

    @Override
    public ResponseEntity<List<ProductCategoryDto>> saveCategories(@Valid List<ProductCategorySaveDto> categories) {
        try {
            List<ProductCategory> saved = productCategoryService.updateCategories(categories);
            return ResponseEntity.ok(ModelMapperUtils.mapAll(saved, ProductCategoryDto.class));
        } catch (ObjectUsedInGoalException e) {
            log.info(e.getMessage());
            return ResponseEntity.status(440).build();
        }
    }

    @Override
    public ResponseEntity<List<ProductCategoryWithSubcategoryDto>> saveCategoriesWithSubcategories(@Valid List<ProductCategoryWithSubcategorySaveDto> categories) {
        try {
            List<ProductCategory> saved = productCategoryService.saveCategoriesWithSubcategories(categories);
            return ResponseEntity.ok(ModelMapperUtils.mapAll(saved, ProductCategoryWithSubcategoryDto.class));
        } catch (ObjectUsedInGoalException e) {
            log.info(e.getMessage());
            return ResponseEntity.status(440).build();
        }
    }

    @Override
    public ResponseEntity<List<ProductCategoryWithSubcategoryDto>> saveSubcategories(@Valid List<ProductSubcategorySaveDto> subcategories) {
        try {
            List<ProductCategoryWithSubcategoryDto> saved = productCategoryService.saveSubcategories(subcategories);
            return ResponseEntity.ok(saved);
        } catch (ObjectUsedInGoalException e) {
            log.info(e.getMessage());
            return ResponseEntity.status(440).build();
        }
    }

    @Override
    public ResponseEntity<List<ProductCategoryWithSubcategoryDto>> getAllCategoriesWithSubcategories() {
        List<ProductCategoryWithSubcategoryDto> all = productCategoryService.findAllCategoriesWithSubcategories();
        return ResponseEntity.ok(all);
    }
}
