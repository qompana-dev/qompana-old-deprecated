package com.devqube.crmbusinessservice.productprice.products.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.OpportunityProduct;
import com.devqube.crmshared.access.annotation.AuthField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductSaleHistoryDTO {
    @AuthField(controller = "getProductSalesHistory")
    private LocalDateTime finishDate;
    @AuthField(controller = "getProductSalesHistory")
    private String clientName;
    @AuthField(controller = "getProductSalesHistory")
    private Long quantity;
    @AuthField(controller = "getProductSalesHistory")
    private String unit;
    @AuthField(controller = "getProductSalesHistory")
    private Double price;
    @AuthField(controller = "getProductSalesHistory")
    private Double priceSummed;
    @AuthField(controller = "getProductSalesHistory")
    private String currency;
    @AuthField(controller = "getProductSalesHistory")
    private Opportunity.FinishResult finishResult;

    // @AuthField(controller = "getProductSalesHistory")
    //private String contractNumber;

    public static ProductSaleHistoryDTO fromModel(OpportunityProduct opportunityProduct, Map<Long, String> units) {
        ProductSaleHistoryDTO dto = new ProductSaleHistoryDTO();
        dto.setPrice(opportunityProduct.getPrice());
        dto.setPriceSummed(opportunityProduct.getPriceSummed());
        dto.setQuantity(opportunityProduct.getQuantity());
        dto.setUnit(units.get(opportunityProduct.getProduct().getUnitId()));
        if (opportunityProduct.getOpportunity() != null) {
            dto.setCurrency(opportunityProduct.getOpportunity().getCurrency());
            dto.setFinishDate(opportunityProduct.getOpportunity().getClosingDate());
            dto.setFinishResult(opportunityProduct.getOpportunity().getFinishResult());
            if (opportunityProduct.getOpportunity().getCustomer() != null) {
                dto.setClientName(opportunityProduct.getOpportunity().getCustomer().getName());
            }
        }
        return dto;
    }
}
