package com.devqube.crmbusinessservice.productprice.manufacturer;

import com.devqube.crmbusinessservice.productprice.products.ProductRepository;
import com.devqube.crmshared.exception.BadRequestException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ManufacturerService {

    private final ManufacturerRepository manufacturerRepository;
    private final ProductRepository productRepository;

    public ManufacturerService(ManufacturerRepository manufacturerRepository, ProductRepository productRepository) {
        this.manufacturerRepository = manufacturerRepository;
        this.productRepository = productRepository;
    }

    public void saveAll(List<Manufacturer> manufacturers) throws BadRequestException {
        if (manufacturers.size() != manufacturers.stream().map(c -> c.getName().toLowerCase()).collect(Collectors.toSet()).size()) {
            throw new BadRequestException("The list contain two or more manufacturers with the same name");
        }

        for (Manufacturer manufacturer: manufacturers) {
            if (manufacturerRepository.existsByName(manufacturer.getName())) {
                throw new BadRequestException("Manufacturer with this name already exists");
            }
        }

        manufacturerRepository.saveAll(manufacturers);
    }

    public Page<Manufacturer> findAll(Pageable pageable) {
        return manufacturerRepository.findAll(pageable);
    }

    public void deleteById(Long id) throws BadRequestException {
        if (productRepository.existsByManufacturerId(id)) {
            throw new BadRequestException("Cannot delete manufacturer. It's used in products");
        }
        manufacturerRepository.deleteById(id);
    }

    public List<Manufacturer> findAllContaining(String pattern) {
        return manufacturerRepository.findTop20ByNameContaining(pattern);
    }

    public List<Manufacturer> findAllByIds(List<Long> ids) {
        return manufacturerRepository.findByIdIn(ids);
    }
}
