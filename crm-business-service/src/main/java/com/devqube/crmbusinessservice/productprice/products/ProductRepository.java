package com.devqube.crmbusinessservice.productprice.products;

import com.devqube.crmbusinessservice.productprice.products.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("select p.code from Product p where p.code in :codes")
    Set<String> findExistingProductCodes(Set<String> codes);

    @Query("select p.id from Product p where p.code not in :codes")
    List<Long> findAllNotExistingInCodes(Set<String> codes);

    void deleteAllByIdIn(List<Long> productIds);

    List<Product> findAllByCodeIn(Set<String> codes);

    Optional<Product> findByCode(String code);

    Optional<Product> findBySkuCode(String skuCode);

    @Query("select distinct p from Product p " +
            "left join p.subcategories ps " +
            "left join ps.productCategory pc " +
            "where p.deleted = false and p.opportunityProduct = false " +
            "and (" +
            "LOWER(p.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(p.code) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(ps.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(pc.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(p.currency) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(p.sellPriceNet as text)) LIKE LOWER(concat('%', :search, '%'))" +
            ")")
    Page<Product> findAllByDeletedFalseAndOpportunityProductFalseForMobile(Pageable pageable, @Param("search") String search);

    @Query("select distinct p from Product p " +
            "left join p.subcategories ps " +
            "left join ps.productCategory pc " +
            "where p.deleted = false and p.opportunityProduct = false and p.created > :date " +
            "and (" +
            "LOWER(p.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(p.code) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(ps.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(pc.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(p.currency) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(p.sellPriceNet as text)) LIKE LOWER(concat('%', :search, '%'))" +
            ")")
    Page<Product> findAllByDeletedFalseAndOpportunityProductFalseAndCreatedAfterForMobile(@Param("date") LocalDateTime date, Pageable pageable, @Param("search") String search);

    @Query("select distinct p from Product p " +
            "left join p.subcategories ps " +
            "left join ps.productCategory pc " +
            "where p.deleted = false and p.opportunityProduct = false and p.updated > :date " +
            "and (" +
            "LOWER(p.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(p.code) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(ps.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(pc.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(p.currency) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(p.sellPriceNet as text)) LIKE LOWER(concat('%', :search, '%'))" +
            ")")
    Page<Product> findAllByDeletedFalseAndOpportunityProductFalseAndUpdatedAfterForMobile(@Param("date") LocalDateTime date, Pageable pageable, @Param("search") String search);

    @Query("select distinct p from Product p " +
            "left join p.subcategories ps " +
            "left join ps.productCategory pc " +
            "where p.deleted = false and p.opportunityProduct = false and (select max(lvp.lastViewed) from LastViewedProduct lvp where lvp.product.id = p.id and lvp.userId = :userId) > :date " +
            "and (" +
            "LOWER(p.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(p.code) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(ps.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(pc.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(p.currency) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(p.sellPriceNet as text)) LIKE LOWER(concat('%', :search, '%'))" +
            ")")
    Page<Product> findAllByDeletedFalseAndLastViewedAfterForMobile(@Param("date") LocalDateTime date, @Param("userId") Long userId, Pageable pageable, @Param("search") String search);

    boolean existsByManufacturerId(Long id);

    boolean existsBySupplierId(Long id);

    Set<Product> findAllByIdIn(Set<Long> ids);

    @Query("select p from Product p where LOWER(p.name) LIKE LOWER(concat('%', :pattern, '%'))")
    List<Product> findByNameContaining(@Param("pattern") String pattern);

    List<Product> findAllByDeletedFalseAndOpportunityProductFalse();
}
