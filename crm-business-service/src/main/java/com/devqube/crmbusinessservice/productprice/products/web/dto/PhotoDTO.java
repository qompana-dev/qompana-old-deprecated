package com.devqube.crmbusinessservice.productprice.products.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PhotoDTO {
    @NotNull
    private String id;

    @NotNull
    private Boolean tempFile;

    @Min(1)
    @Max(4)
    @NotNull
    private Integer order;
}
