package com.devqube.crmbusinessservice.productprice.products.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class LastViewedProduct {
    @Id
    @SequenceGenerator(name = "last_viewed_product_seq", sequenceName = "last_viewed_product_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "last_viewed_product_seq")
    @EqualsAndHashCode.Include
    private Long id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "product_id")
    @NotNull
    private Product product;

    @Column(name = "user_id")
    @NotNull
    private Long userId;

    @Column(name = "last_viewed")
    @NotNull
    private LocalDateTime lastViewed;
}
