package com.devqube.crmbusinessservice.productprice.priceBook.web;

import com.devqube.crmbusinessservice.productprice.exception.PriceBookNameOccupiedException;
import com.devqube.crmbusinessservice.productprice.priceBook.PriceBookService;
import com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook;
import com.devqube.crmbusinessservice.productprice.priceBook.web.dto.PriceBookAssignDTO;
import com.devqube.crmbusinessservice.productprice.priceBook.web.dto.PriceBookDTO;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.web.CurrentRequestUtil;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@Slf4j
public class PriceBookController implements PriceBookApi {

    private final PriceBookService priceBookService;
    private final ModelMapper modelMapper;

    public PriceBookController(PriceBookService priceBookService, ModelMapper modelMapper) {
        this.priceBookService = priceBookService;
        this.modelMapper = modelMapper;
    }

    @Override
    @AuthController(name = "getPriceBooks")
    public ResponseEntity<Page> getPriceBooks(@Valid Pageable pageable, @Valid Boolean today, @Valid Boolean edited, @Valid Boolean viewed) {
        try {
            if (today != null && today) {
                return ResponseEntity.ok(priceBookService.findAllCreatedToday(pageable));
            } else if (edited != null && edited) {
                return ResponseEntity.ok(priceBookService.findAllRecentlyEdited(pageable));
            } else if (viewed != null && viewed) {
                return ResponseEntity.ok(priceBookService.findAllRecentlyViewed(pageable, CurrentRequestUtil.getLoggedInAccountEmail()));
            } else {
                return ResponseEntity.ok(priceBookService.findAll(pageable));
            }
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @AuthController(actionFrontendId = "PriceBookListComponent.savePriceBook", name = "savePriceBook")
    public ResponseEntity<PriceBook> savePriceBook(@Valid PriceBookDTO priceBookDTO) {
        try {
            PriceBook saved = priceBookService.save(modelMapper.map(priceBookDTO, PriceBook.class));
            return new ResponseEntity<>(saved, HttpStatus.CREATED);
        } catch (BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @Override
    @AuthController(actionFrontendId = "PriceBookListComponent.deletePriceBook")
    public ResponseEntity<Void> deletePriceBook(Long id) {
        try {
            priceBookService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Customer with id {} not found", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(actionFrontendId = "PriceBookListComponent.deletePriceBook")
    public ResponseEntity<Void> deletePriceBooks(@Valid List<Long> ids) {
        try {
            priceBookService.deleteByIds(ids);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getPriceBook")
    public ResponseEntity<PriceBook> getPriceBook(Long id) {
        try {
            return new ResponseEntity<>(priceBookService.findById(id), HttpStatus.OK);
        } catch (EntityNotFoundException | BadRequestException e) {
            log.info("Product with id {} not found", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(actionFrontendId = "PriceBookListComponent.editPriceBook", name = "editPriceBook")
    public ResponseEntity<Void> updatePriceBook(Long id, @Valid PriceBookDTO priceBookDTO) {
        try {
            priceBookService.modify(modelMapper.map(priceBookDTO, PriceBook.class), id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Product with id {} not found", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PriceBookNameOccupiedException e) {
            log.info("New name {} is occupied", id);
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<PriceBookAssignDTO>> getPriceBookToAssign() {
        List<PriceBookAssignDTO> toAssign = priceBookService
                .findAll()
                .stream()
                .map(e -> modelMapper.map(e, PriceBookAssignDTO.class))
                .collect(Collectors.toList());
        return new ResponseEntity<>(toAssign, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<PriceBookAssignDTO>> getPriceBookToAssignByCurrency(String currency) {
        List<PriceBookAssignDTO> toAssign = priceBookService
                .findAllByCurrency(currency)
                .stream()
                .map(e -> modelMapper.map(e, PriceBookAssignDTO.class))
                .collect(Collectors.toList());
        return new ResponseEntity<>(toAssign, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> updateDefaultPriceBooksCurrency(String currency) throws EntityNotFoundException {
        priceBookService.updateDefaultPriceBooksCurrency(currency);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
