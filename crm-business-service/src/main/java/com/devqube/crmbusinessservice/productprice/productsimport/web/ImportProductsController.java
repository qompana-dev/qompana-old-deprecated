package com.devqube.crmbusinessservice.productprice.productsimport.web;

import com.devqube.crmbusinessservice.productprice.productsimport.ImportProductsService;
import com.devqube.crmbusinessservice.productprice.productsimport.ParseException;
import com.devqube.crmbusinessservice.productprice.productsimport.exception.SkuCodeOrCodeConflictException;
import com.devqube.crmbusinessservice.productprice.productsimport.importstrategy.ProductsImportStrategy;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;

@RestController
public class ImportProductsController implements ImportProductsApi {


    private final ImportProductsService importProductService;

    public ImportProductsController(ImportProductsService importProductService) {
        this.importProductService = importProductService;
    }

    @Override
    @AuthController(actionFrontendId = "ProductListComponent.importProducts")
    public ResponseEntity<Object> importProducts(String loggedAccountEmail, @Valid MultipartFile file, ProductsImportStrategy strategy, Boolean headerPresent) {
        try {
            importProductService.saveProductFromCsv(file.getInputStream(), strategy, headerPresent);
            return ResponseEntity.noContent().build();
        } catch (SkuCodeOrCodeConflictException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        } catch (BadRequestException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (ParseException e) {
            return ResponseEntity.status(423).body(e.getErrorList());
        }
    }

//    @ExceptionHandler({DataProcessingException.class})
//    public ResponseEntity<CsvParsingErrorResponse> handleCsvParsingException(DataProcessingException e) {
//        CsvParsingErrorResponse csvParsingErrorResponse = new CsvParsingErrorResponse(e.getLineIndex(), (long) e.getColumnIndex(), e.getValue().toString());
//        return ResponseEntity.badRequest().body(csvParsingErrorResponse);
//    }
}
