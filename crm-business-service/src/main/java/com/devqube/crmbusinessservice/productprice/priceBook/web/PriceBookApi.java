package com.devqube.crmbusinessservice.productprice.priceBook.web;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.productprice.priceBook.web.dto.PriceBookAssignDTO;
import com.devqube.crmbusinessservice.productprice.priceBook.web.dto.PriceBookDTO;
import com.devqube.crmshared.exception.EntityNotFoundException;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "PriceBook", description = "the PriceBook API")
public interface PriceBookApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Delete price book", nickname = "deletePriceBook", notes = "Method used to delete price book")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "price book removed"),
            @ApiResponse(code = 404, message = "price book not found") })
    @RequestMapping(value = "/price-books/{id}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deletePriceBook(@ApiParam(value = "The price book ID",required=true) @PathVariable("id") Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "delete price books", nickname = "deletePriceBooks", notes = "Method used to delete price books")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "deleted price books successfully"),
            @ApiResponse(code = 404, message = "one of price books not found") })
    @RequestMapping(value = "/price-books",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deletePriceBooks(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue="new ArrayList<>()") List<Long> ids) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get price book", nickname = "getPriceBook", notes = "Method used to get price book", response = com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "get price book successfully", response = com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook.class),
            @ApiResponse(code = 404, message = "this price book doesn't exists") })
    @RequestMapping(value = "/price-books/{id}",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook> getPriceBook(@ApiParam(value = "price book id",required=true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get price book to assign", nickname = "getPriceBookToAssign", notes = "get list of all price book to assign", response = PriceBookAssignDTO.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "price book list to assign retrieved successfully", response = PriceBookAssignDTO.class, responseContainer = "List") })
    @RequestMapping(value = "/price-books-to-assign",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<List<PriceBookAssignDTO>> getPriceBookToAssign() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"isDefault\" : true,  \"name\" : \"name\",  \"currency\" : \"currency\",  \"id\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get price books", nickname = "getPriceBooks", notes = "get list of all price books", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "price book list retrieved successfully", response = org.springframework.data.domain.Page.class) })
    @RequestMapping(value = "/price-books",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<org.springframework.data.domain.Page> getPriceBooks(@ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable,@ApiParam(value = "only price books added today") @Valid @RequestParam(value = "today", required = false) Boolean today,@ApiParam(value = "only last edited price books") @Valid @RequestParam(value = "edited", required = false) Boolean edited,@ApiParam(value = "only last viewed price books") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Add price book", nickname = "savePriceBook", notes = "Add single price book", response = com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "price book added successfully", response = com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook.class),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 409, message = "price book name is occupied")})
    @RequestMapping(value = "/price-books",
            produces = { "application/json" },
            consumes = { "application/json" },
            method = RequestMethod.POST)
    default ResponseEntity<com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook> savePriceBook(@ApiParam(value = "Price book to be added") @Valid @RequestBody PriceBookDTO priceBookDTO) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update price book", nickname = "updatePriceBook", notes = "Method used to modify price book")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "price book updated"),
            @ApiResponse(code = 400, message = "validation error", response = String.class),
            @ApiResponse(code = 404, message = "price book not found"),
            @ApiResponse(code = 409, message = "price book with this name already exists") })
    @RequestMapping(value = "/price-books/{id}",
            produces = { "application/json" },
            consumes = { "application/json" },
            method = RequestMethod.PUT)
    default ResponseEntity<Void> updatePriceBook(@ApiParam(value = "The price book ID",required=true) @PathVariable("id") Long id,@ApiParam(value = "Modified price book object" ,required=true )  @Valid @RequestBody PriceBookDTO priceBookDTO) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get price book to assign by currency", nickname = "getPriceBookToAssignByCurrency", notes = "get list of all price book to assign by currency", response = PriceBookAssignDTO.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "price book list to assign by currency retrieved successfully", response = PriceBookAssignDTO.class, responseContainer = "List") })
    @RequestMapping(value = "/price-books-to-assign/currency/{currency}",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<List<PriceBookAssignDTO>> getPriceBookToAssignByCurrency(@ApiParam(value = "price book currency",required=true) @PathVariable("currency") String currency) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"isDefault\" : true,  \"name\" : \"name\",  \"currency\" : \"currency\",  \"id\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update currency of default price book", nickname = "updateCurrencyOfDefaultPriceBook", notes = "Method used to change default price book's currency")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "price book updated"),
            @ApiResponse(code = 400, message = "validation error", response = String.class),
            @ApiResponse(code = 404, message = "default price book not found")})
    @RequestMapping(value = "/internal/price-books/default/currency/{currency}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> updateDefaultPriceBooksCurrency(@PathVariable("currency") String currency) throws EntityNotFoundException {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
