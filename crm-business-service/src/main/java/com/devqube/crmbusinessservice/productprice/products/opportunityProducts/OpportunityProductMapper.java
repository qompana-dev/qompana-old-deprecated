package com.devqube.crmbusinessservice.productprice.products.opportunityProducts;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.productprice.category.CategoryUtil;
import com.devqube.crmbusinessservice.productprice.category.ProductSubcategory;
import com.devqube.crmbusinessservice.productprice.category.ProductSubcategoryRepository;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductDTO;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductSaveDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
class OpportunityProductMapper {

    private final ProductSubcategoryRepository productSubcategoryRepository;

    OpportunityProductMapper(ProductSubcategoryRepository productSubcategoryRepository) {
        this.productSubcategoryRepository = productSubcategoryRepository;
    }

    OpportunityProductDTO mapToDTO(OpportunityProduct opportunityProduct, Map<Long, String> units) {
        OpportunityProductDTO dto = new OpportunityProductDTO();
        dto.setId(opportunityProduct.getId());
        dto.setName(opportunityProduct.getProduct().getName());
        List<ProductSubcategory> subcategories = opportunityProduct.getProduct().getSubcategories();
        dto.setCategory(CategoryUtil.categoriesToString(subcategories));
        dto.setSubcategory(CategoryUtil.subcategoriesToString(subcategories));
        dto.setCode(opportunityProduct.getProduct().getCode());
        dto.setPrice(opportunityProduct.getPrice());
        dto.setCurrency(opportunityProduct.getProduct().getCurrency());
        dto.setQuantity(opportunityProduct.getQuantity());
        dto.setPriceSummed(opportunityProduct.getPriceSummed());

        dto.setPriceChanged(!opportunityProduct.getProduct().getSellPriceNet().equals(opportunityProduct.getCurrentSellPriceNet()));
        dto.setPriceChangedTo(opportunityProduct.getProduct().getSellPriceNet());
        dto.setPriceChangedFrom(opportunityProduct.getCurrentSellPriceNet());
        dto.setUnit(units.get(opportunityProduct.getProduct().getUnitId()));

        return dto;
    }

    Product mapToProduct(OpportunityProductSaveDTO dto) {
        Product product = new Product();
        product.setName(dto.getName());
        product.setCode(dto.getCode());
        product.setSkuCode(dto.getSkuCode());
        addCategories(product, dto.getSubcategoryIds());
        product.setSellPriceNet(dto.getSellPrice());
        product.setPurchasePrice(dto.getPurchasePrice());
        product.setCurrency(dto.getCurrency());
        product.setUnitId(dto.getUnit());
        product.setDeleted(false);
        product.setActive(true);
        product.setOpportunityProduct(true);
        return product;
    }

    OpportunityProduct mapToOpportunityProduct(OpportunityProductSaveDTO dto, Product product, Opportunity opportunity) {
        OpportunityProduct opportunityProduct = new OpportunityProduct();
        opportunityProduct.setProduct(product);
        opportunityProduct.setOpportunity(opportunity);
        opportunityProduct.setPrice(dto.getSellPrice());
        opportunityProduct.setQuantity(dto.getQuantity());
        opportunityProduct.setPriceSummed(dto.getQuantity() * dto.getSellPrice());
        return opportunityProduct;
    }

    private void addCategories(Product product, List<Long> subcategoriesIds) {
        List<ProductSubcategory> all = productSubcategoryRepository.findByProductsId(product.getId());
        all.forEach(sub -> sub.getProducts().removeIf(p -> p.getId().equals(product.getId())));
        List<ProductSubcategory> subcategories = productSubcategoryRepository.findAllById(subcategoriesIds);
        subcategories.forEach(s -> s.getProducts().add(product));
        product.setSubcategories(subcategories);
    }
}
