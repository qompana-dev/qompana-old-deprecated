package com.devqube.crmbusinessservice.productprice.category.web;

import com.devqube.crmbusinessservice.ApiUtil;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "ProductCategory")
public interface ProductCategoryApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "get productCategories", nickname = "getAllCategories", notes = "get all list by categories", response = com.devqube.crmbusinessservice.productprice.category.web.ProductCategoryDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "category list retrieved successfully", response = com.devqube.crmbusinessservice.productprice.category.web.ProductCategoryDto.class, responseContainer = "List")})
    @RequestMapping(value = "/products/categories",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<ProductCategoryDto>> getAllCategories() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get all categories with subcategories", nickname = "getAllCategoriesWithSubcategories", notes = "get all categories with subcategories", response = com.devqube.crmbusinessservice.productprice.category.web.ProductCategoryWithSubcategoryDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "category list retrieved successfully", response = com.devqube.crmbusinessservice.productprice.category.web.ProductCategoryWithSubcategoryDto.class, responseContainer = "List")})
    @RequestMapping(value = "/products/categories-with-subcategories",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<ProductCategoryWithSubcategoryDto>> getAllCategoriesWithSubcategories() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get subcategories for category", nickname = "getCategoriesWithSubcategories", notes = "get subcategories for category", response = com.devqube.crmbusinessservice.productprice.category.web.ProductCategoryWithSubcategoryDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "category list retrieved successfully", response = com.devqube.crmbusinessservice.productprice.category.web.ProductCategoryWithSubcategoryDto.class, responseContainer = "List")})
    @RequestMapping(value = "/products/categories-with-subcategories/{categoryIds}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<ProductCategoryWithSubcategoryDto>> getCategoriesWithSubcategories(@ApiParam(value = "", required = true, defaultValue = "new ArrayList<>()") @PathVariable("categoryIds") List<Long> categoryIds) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "save categories", nickname = "saveCategories", notes = "save categories", response = com.devqube.crmbusinessservice.productprice.category.web.ProductCategoryDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Categories saved successfully", response = com.devqube.crmbusinessservice.productprice.category.web.ProductCategoryDto.class, responseContainer = "List")})
    @RequestMapping(value = "/products/categories",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<List<ProductCategoryDto>> saveCategories(@ApiParam(value = "") @Valid @RequestBody List<ProductCategorySaveDto> comDevqubeCrmbusinessserviceProductpriceCategoryWebProductCategorySaveDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "save categories with subcategories", nickname = "saveCategoriesWithSubcategories", notes = "save categories with subcategories", response = com.devqube.crmbusinessservice.productprice.category.web.ProductCategoryWithSubcategoryDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Categories with subcategories saved successfully", response = com.devqube.crmbusinessservice.productprice.category.web.ProductCategoryWithSubcategoryDto.class, responseContainer = "List")})
    @RequestMapping(value = "/products/categories-with-subcategories",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<List<ProductCategoryWithSubcategoryDto>> saveCategoriesWithSubcategories(@ApiParam(value = "") @Valid @RequestBody List<ProductCategoryWithSubcategorySaveDto> comDevqubeCrmbusinessserviceProductpriceCategoryWebProductCategoryWithSubcategorySaveDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "save subcategories", nickname = "saveSubcategories", notes = "save subcategories", response = com.devqube.crmbusinessservice.productprice.category.web.ProductCategoryWithSubcategoryDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Subcategories saved successfully", response = com.devqube.crmbusinessservice.productprice.category.web.ProductCategoryWithSubcategoryDto.class, responseContainer = "List")})
    @RequestMapping(value = "/products/subcategories",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<List<ProductCategoryWithSubcategoryDto>> saveSubcategories(@ApiParam(value = "") @Valid @RequestBody List<ProductSubcategorySaveDto> comDevqubeCrmbusinessserviceProductpriceCategoryWebProductSubcategorySaveDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
