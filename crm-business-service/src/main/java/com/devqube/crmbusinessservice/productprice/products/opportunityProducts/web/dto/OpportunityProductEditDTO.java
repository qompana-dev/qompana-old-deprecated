package com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OpportunityProductEditDTO {
    private Long id;
    private String name;
    private Double sellPrice;
    private Long quantity;
    private Long unit;
    private Boolean opportunityProduct;
}
