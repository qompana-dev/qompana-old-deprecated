package com.devqube.crmbusinessservice.productprice.productPrice;

import com.devqube.crmbusinessservice.productprice.priceBook.PriceBookRepository;
import com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook;
import com.devqube.crmbusinessservice.productprice.productPrice.model.ProductPrice;
import com.devqube.crmbusinessservice.productprice.productPrice.model.ReadProductPrice;
import com.devqube.crmbusinessservice.productprice.productPrice.web.dto.ProductPriceDto;
import com.devqube.crmbusinessservice.productprice.products.ProductRepository;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductPriceService {
    private final ProductPriceRepository productPriceRepository;
    private final ReadProductPriceRepository readProductPriceRepository;
    private final ProductRepository productRepository;
    private final PriceBookRepository priceBookRepository;

    public ProductPriceService(ProductPriceRepository productPriceRepository,
                               ReadProductPriceRepository readProductPriceRepository,
                               ProductRepository productRepository,
                               PriceBookRepository priceBookRepository) {
        this.productPriceRepository = productPriceRepository;
        this.readProductPriceRepository = readProductPriceRepository;
        this.productRepository = productRepository;
        this.priceBookRepository = priceBookRepository;
    }

    public List<ProductPriceDto> getProductsPriceByProductId(Long productId) {
        return productPriceRepository.getAllByProductId(productId)
                .stream().map(ProductPriceDto::fromModel)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<ProductPriceDto> setProductPricesForProductId(Long productId, List<ProductPriceDto> productPriceList) throws EntityNotFoundException {
        if (productPriceList == null) {
            productPriceList = new ArrayList<>();
        }
        deleteProductPricesByProductId(productId);
        productPriceRepository.flush();
        Product product = productRepository.findById(productId).orElseThrow(EntityNotFoundException::new);
        List<ProductPrice> toSave = setDefaultProductPriceToList(product, productPriceList).stream().map(c -> c.toModel(product)).collect(Collectors.toList());
        return productPriceRepository.saveAll(toSave)
                .stream().map(ProductPriceDto::fromModel)
                .collect(Collectors.toList());
    }

    public List<ProductPrice> setDefaultPriceBookForProducts(List<Product> products) throws EntityNotFoundException {
        PriceBook defaultPriceBook = priceBookRepository.findAllByDeletedFalseAndIsDefaultTrue().stream().findFirst().orElseThrow(EntityNotFoundException::new);
        List<ProductPrice> toSave = products.stream().map(c -> getProductPriceDto(c, defaultPriceBook).toModel(c)).collect(Collectors.toList());
        return productPriceRepository.saveAll(toSave);
    }

    @Transactional
    public void deleteProductPricesByProductId(Long productId) {
        productPriceRepository.deleteAllByProductId(productId);
    }

    public Page<ReadProductPrice> findAllForPriceBook(Long priceBookId, Pageable pageable) {
        return this.readProductPriceRepository.findAllByDeletedFalseAndPriceBookId(priceBookId, pageable);
    }

    public Page<ReadProductPrice> findAllCreatedTodayForPriceBook(Long priceBookId, Pageable pageable) {
        return this.readProductPriceRepository.findAllByDeletedFalseAndCreatedAfterAndPriceBookId(priceBookId, LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), pageable);
    }

    public Page<ReadProductPrice> findAllRecentlyEditedForPriceBook(Long priceBookId, Pageable pageable) {
        return this.readProductPriceRepository.findAllByDeletedFalseAndUpdatedAfterAndPriceBookId(priceBookId, LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), pageable);
    }

    public Page<ReadProductPrice> findAllRecentlyViewedForPriceBook(Long priceBookId, Pageable pageable, Long userId) {
        return readProductPriceRepository.findAllByDeletedFalseAndLastViewedAfterAndPriceBookId(priceBookId, LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3), userId, pageable);
    }

    private List<ProductPriceDto> setDefaultProductPriceToList(Product product, List<ProductPriceDto> productPriceList) throws EntityNotFoundException {
        ProductPriceDto defaultProductPrice = getDefaultProductPrice(product);
        List<ProductPriceDto> result = productPriceList.stream().filter(c -> !c.getPriceBook().getId().equals(defaultProductPrice.getPriceBook().getId())).collect(Collectors.toList());
        result.add(defaultProductPrice);
        return result;
    }

    private ProductPriceDto getDefaultProductPrice(Product product) throws EntityNotFoundException {
        PriceBook defaultPriceBook = priceBookRepository.findAllByDeletedFalseAndIsDefaultTrue().stream().findFirst().orElseThrow(EntityNotFoundException::new);
        return getProductPriceDto(product, defaultPriceBook);
    }

    private ProductPriceDto getProductPriceDto(Product product, PriceBook defaultPriceBook) {
        ProductPriceDto productPriceDto = new ProductPriceDto();
        productPriceDto.setPrice(product.getSellPriceNet());
        productPriceDto.setActive(true);
        productPriceDto.setPriceBook(defaultPriceBook);
        return productPriceDto;
    }
}
