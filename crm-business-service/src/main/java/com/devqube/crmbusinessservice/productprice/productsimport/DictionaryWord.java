package com.devqube.crmbusinessservice.productprice.productsimport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@AllArgsConstructor
@Getter
class DictionaryWord {

    private Long id;
    private String name;
}
