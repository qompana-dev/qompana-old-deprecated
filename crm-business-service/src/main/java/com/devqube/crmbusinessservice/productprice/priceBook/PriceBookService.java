package com.devqube.crmbusinessservice.productprice.priceBook;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.productprice.exception.PriceBookNameOccupiedException;
import com.devqube.crmbusinessservice.productprice.priceBook.model.LastViewedPriceBook;
import com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook;
import com.devqube.crmbusinessservice.productprice.productPrice.ProductPriceRepository;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.web.CurrentRequestUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PriceBookService {

    private final PriceBookRepository priceBookRepository;
    private final LastViewedPriceBookRepository lastViewedPriceBookRepository;
    private final UserClient userClient;
    private final AccessService accessService;
    private final ProductPriceRepository productPriceRepository;

    public PriceBookService(PriceBookRepository priceBookRepository, LastViewedPriceBookRepository lastViewedPriceBookRepository, UserClient userClient, AccessService accessService, ProductPriceRepository productPriceRepository) {
        this.priceBookRepository = priceBookRepository;
        this.lastViewedPriceBookRepository = lastViewedPriceBookRepository;
        this.userClient = userClient;
        this.accessService = accessService;
        this.productPriceRepository = productPriceRepository;
    }

    public List<PriceBook> findAll() {
        return this.priceBookRepository.findAllByDeletedFalse();
    }
    public List<PriceBook> findAllByCurrency(String currency) {
        return this.priceBookRepository.findAllByCurrencyAndDeletedFalse(currency);
    }

    public Page<PriceBook> findAll(Pageable pageable) {
        return this.priceBookRepository.findAllByDeletedFalseOrderByIsDefaultDesc(pageable);
    }

    public Page<PriceBook> findAllCreatedToday(Pageable pageable) {
        return this.priceBookRepository.findAllByDeletedFalseAndCreatedAfterOrderByIsDefaultDesc(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT),pageable);
    }

    public Page<PriceBook> findAllRecentlyEdited(Pageable pageable) {
        return this.priceBookRepository.findAllByDeletedFalseAndUpdatedAfterOrderByIsDefaultDesc(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), pageable);
    }

    public Page<PriceBook> findAllRecentlyViewed(Pageable pageable, String loggedAccountEmail) throws BadRequestException {
        Long userId = getUserId(loggedAccountEmail);
        return priceBookRepository.findAllByDeletedFalseAndLastViewedAfter(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3), userId, pageable);
    }

    private Long getUserId(String loggedAccountEmail) throws BadRequestException {
        try {
            return userClient.getMyAccountId(loggedAccountEmail);
        } catch (Exception e) {
            throw new BadRequestException("user not found");
        }
    }

    public PriceBook save(PriceBook priceBook) throws BadRequestException {
        if (priceBookRepository.findByName(priceBook.getName()).isPresent()) {
            throw new BadRequestException("price book with this name already exists");
        }
        PriceBook toSave = this.accessService.assignAndVerify(priceBook);
        return priceBookRepository.save(toSave);
    }

    public PriceBook findById(Long id) throws EntityNotFoundException, BadRequestException {
        PriceBook priceBook = priceBookRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Product with id " + id + " was not found."));
        priceBook.setEmpty(priceBook.getPrices().isEmpty());
        saveLastViewed(priceBook, CurrentRequestUtil.getLoggedInAccountEmail());
        return priceBook;
    }

    public void modify(PriceBook priceBook, Long id) throws EntityNotFoundException, BadRequestException, PriceBookNameOccupiedException {
        if (priceBook == null || id == null) {
            throw new BadRequestException("id is null");
        }
        PriceBook fromDb = priceBookRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("price book with id " + id + " was not found"));
        Optional<PriceBook> byName = priceBookRepository.findByName(priceBook.getName());
        if (byName.isPresent() && !byName.get().getId().equals(id)) {
            throw new PriceBookNameOccupiedException();
        }
        PriceBook toUpdate = accessService.assignAndVerify(fromDb, priceBook);
        updatePriceBook(fromDb, toUpdate);
        priceBookRepository.save(fromDb);
    }

    private void updatePriceBook(PriceBook fromDb, PriceBook toUpdate) {
        fromDb.setName(toUpdate.getName());
        fromDb.setDescription(toUpdate.getDescription());
        if (fromDb.getPrices().isEmpty()) {
            fromDb.setCurrency(toUpdate.getCurrency());
        }
        fromDb.setActive(toUpdate.getActive());
    }

    @Transactional
    public void deleteById(Long id) throws EntityNotFoundException, BadRequestException {
        PriceBook priceBook = priceBookRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        if (priceBook.getDeleted()) {
            throw new EntityNotFoundException();
        }
        if (priceBook.getIsDefault()) {
            throw new BadRequestException();
        }
        priceBook.setName(UUID.randomUUID().toString());
        priceBook.setDeleted(true);
        priceBookRepository.save(priceBook);
        productPriceRepository.deleteAllByPriceBookId(id);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = { EntityNotFoundException.class, BadRequestException.class })
    public void deleteByIds(List<Long> ids) throws EntityNotFoundException, BadRequestException {
        for (Long id : ids) {
            deleteById(id);
        }
    }

    private void saveLastViewed(PriceBook priceBook, String loggedAccountEmail) throws BadRequestException {
        Long userId = getUserId(loggedAccountEmail);
        LastViewedPriceBook newLastViewed = LastViewedPriceBook.builder().userId(userId).priceBook(priceBook).build();
        LastViewedPriceBook lastViewedPriceBook = (priceBook.getLastViewedPriceBooks() == null) ? newLastViewed : priceBook.getLastViewedPriceBooks().stream()
                .filter(lv -> lv.getUserId().equals(userId)).findFirst()
                .orElse(newLastViewed);
        lastViewedPriceBook.setLastViewed(LocalDateTime.now());
        lastViewedPriceBookRepository.save(lastViewedPriceBook);
    }

    public void updateDefaultPriceBooksCurrency(String currency) throws EntityNotFoundException {
        PriceBook defaultPriceBook = priceBookRepository.findByIsDefaultTrue().orElseThrow(EntityNotFoundException::new);
        defaultPriceBook.setCurrency(currency);
        priceBookRepository.save(defaultPriceBook);
    }
}
