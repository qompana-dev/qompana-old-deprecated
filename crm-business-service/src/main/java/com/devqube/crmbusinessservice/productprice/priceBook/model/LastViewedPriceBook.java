package com.devqube.crmbusinessservice.productprice.priceBook.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class LastViewedPriceBook {
    @Id
    @SequenceGenerator(name = "last_viewed_price_book_seq", sequenceName = "last_viewed_price_book_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "last_viewed_price_book_seq")
    @EqualsAndHashCode.Include
    private Long id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "price_book_id")
    @NotNull
    private PriceBook priceBook;

    @Column(name = "user_id")
    @NotNull
    private Long userId;

    @Column(name = "last_viewed")
    @NotNull
    private LocalDateTime lastViewed;
}
