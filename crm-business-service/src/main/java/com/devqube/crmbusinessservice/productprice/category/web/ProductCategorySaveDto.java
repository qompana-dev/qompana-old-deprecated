package com.devqube.crmbusinessservice.productprice.category.web;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ProductCategorySaveDto {
    private Long id;
    private String name;
    private boolean toDelete;
}
