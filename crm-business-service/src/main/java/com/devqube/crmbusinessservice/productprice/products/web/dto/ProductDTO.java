package com.devqube.crmbusinessservice.productprice.products.web.dto;

import com.devqube.crmbusinessservice.productprice.productPrice.web.dto.ProductPriceDto;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@AuthFilterEnabled
public class ProductDTO {
  @EqualsAndHashCode.Include
  @AuthFields(list = {
          @AuthField(controller = "getProducts"),
          @AuthField(controller = "getProductsForPriceBook"),
          @AuthField(controller = "addProductFromPriceBook"),
          @AuthField(controller = "saveProduct"),
          @AuthField(controller = "editProduct"),
          @AuthField(controller = "getProduct")
  })
  private Long id;

  @NotNull
  @AuthFields(list = {
          @AuthField(frontendId = "ProductListComponent.name", controller = "getProducts"),
          @AuthField(frontendId = "ProductListInPriceBookComponent.name", controller = "getProductsForPriceBook"),
          @AuthField(frontendId = "AddProductFromPriceBookComponent.name", controller = "addProductFromPriceBook"),
          @AuthField(frontendId = "ProductAddComponent.name", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.name", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.name", controller = "getProduct")
  })
  private String name;

  @EqualsAndHashCode.Include
  @NotNull
  @AuthFields(list = {
          @AuthField(frontendId = "ProductListComponent.code", controller = "getProducts"),
          @AuthField(frontendId = "ProductListInPriceBookComponent.code", controller = "getProductsForPriceBook"),
          @AuthField(frontendId = "AddProductFromPriceBookComponent.code", controller = "addProductFromPriceBook"),
          @AuthField(frontendId = "ProductAddComponent.code", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.code", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.code", controller = "getProduct")
  })
  private String code;

  @EqualsAndHashCode.Include
  @NotNull
  @AuthFields(list = {
          @AuthField(frontendId = "ProductAddComponent.skuCode", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.skuCode", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.skuCode", controller = "getProduct")
  })
  private String skuCode;

  @AuthFields(list = {
          @AuthField(frontendId = "ProductListComponent.category", controller = "getProducts"),
          @AuthField(frontendId = "ProductListInPriceBookComponent.category", controller = "getProductsForPriceBook"),
          @AuthField(frontendId = "AddProductFromPriceBookComponent.category", controller = "addProductFromPriceBook")
  })
  private String categories;

  @AuthFields(list = {
          @AuthField(frontendId = "ProductListComponent.subcategory", controller = "getProducts"),
          @AuthField(frontendId = "ProductListInPriceBookComponent.subcategory", controller = "getProductsForPriceBook"),
          @AuthField(frontendId = "AddProductFromPriceBookComponent.subcategory", controller = "addProductFromPriceBook")
  })
  private String subcategories;

  @AuthFields(list = {
          @AuthField(frontendId = "ProductAddComponent.manufacturer", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.manufacturer", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.manufacturer", controller = "getProduct")
  })
  private Long manufacturerId;

  @AuthFields(list = {
          @AuthField(frontendId = "ProductAddComponent.supplier", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.supplier", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.supplier", controller = "getProduct")
  })
  private Long supplierId;

  @AuthFields(list = {
          @AuthField(frontendId = "ProductAddComponent.brand", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.brand", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.brand", controller = "getProduct")
  })
  private String brand;

  @NotNull
  @AuthFields(list = {
          @AuthField(frontendId = "ProductAddComponent.purchasePrice", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.purchasePrice", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.purchasePrice", controller = "getProduct")
  })
  private Double purchasePrice;

  @NotNull
  @AuthFields(list = {
          @AuthField(frontendId = "ProductListComponent.sellPriceNet", controller = "getProducts"),
          @AuthField(frontendId = "ProductListInPriceBookComponent.sellPriceNet", controller = "getProductsForPriceBook"),
          @AuthField(frontendId = "AddProductFromPriceBookComponent.sellPriceNet", controller = "addProductFromPriceBook"),
          @AuthField(frontendId = "ProductAddComponent.currency", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.currency", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.currency", controller = "getProduct")
  })
  private String purchasePriceCurrency;

  @NotNull
  @AuthFields(list = {
          @AuthField(frontendId = "ProductListComponent.sellPriceNet", controller = "getProducts"),
          @AuthField(frontendId = "ProductListInPriceBookComponent.sellPriceNet", controller = "getProductsForPriceBook"),
          @AuthField(frontendId = "ProductAddComponent.currency", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.currency", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.currency", controller = "getProduct")
  })
  private String currency;

  @NotNull
  @AuthFields(list = {
          @AuthField(frontendId = "ProductListComponent.sellPriceNet", controller = "getProducts"),
          @AuthField(frontendId = "ProductListInPriceBookComponent.sellPriceNet", controller = "getProductsForPriceBook"),
          @AuthField(frontendId = "AddProductFromPriceBookComponent.sellPriceNet", controller = "addProductFromPriceBook"),
          @AuthField(frontendId = "ProductAddComponent.sellPriceNet", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.sellPriceNet", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.sellPriceNet", controller = "getProduct")
  })
  private Double sellPriceNet;

  @NotNull
  @AuthFields(list = {
          @AuthField(frontendId = "ProductAddComponent.vat", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.vat", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.vat", controller = "getProduct")
  })
  private Integer vat;

  @AuthFields(list = {
          @AuthField(frontendId = "ProductAddComponent.active", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.active", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.active", controller = "getProduct")
  })
  private Boolean active;

  @AuthFields(list = {
          @AuthField(frontendId = "ProductAddComponent.description", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.description", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.description", controller = "getProduct")
  })
  private String description;

  @AuthFields(list = {
          @AuthField(frontendId = "ProductAddComponent.specification", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.specification", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.specification", controller = "getProduct")
  })
  private String specification;

  @AuthFields(list = {
          @AuthField(frontendId = "ProductAddComponent.notes", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.notes", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.notes", controller = "getProduct")
  })
  private String notes;


  @JsonProperty("photos")
  @AuthFields(list = {
          @AuthField(frontendId = "ProductAddComponent.photos", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.photos", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.photos", controller = "getProduct")
  })
  @Valid
  private List<PhotoDTO> photos = new ArrayList<>();

  @JsonProperty("productPrices")
  @AuthFields(list = {
          @AuthField(frontendId = "ProductAddComponent.productPrices", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.productPrices", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.productPrices", controller = "getProduct")
  })
  @Valid
  private List<ProductPriceDto> productPrices = new ArrayList<>();

  @AuthFields(list = {
          @AuthField(frontendId = "ProductListComponent.subcategory", controller = "saveProduct"),
          @AuthField(frontendId = "ProductListComponent.subcategory", controller = "editProduct"),
          @AuthField(frontendId = "ProductListComponent.subcategory", controller = "getProduct"),
          @AuthField(frontendId = "ProductListComponent.subcategory", controller = "getProducts"),
          @AuthField(frontendId = "ProductListInPriceBookComponent.subcategory", controller = "getProductsForPriceBook"),
          @AuthField(frontendId = "AddProductFromPriceBookComponent.subcategory", controller = "addProductFromPriceBook")
  })
  private List<Long> subcategoryIds;

  @AuthFields(list = {
          @AuthField(frontendId = "ProductListComponent.category", controller = "saveProduct"),
          @AuthField(frontendId = "ProductListComponent.category", controller = "editProduct"),
          @AuthField(frontendId = "ProductListComponent.category", controller = "getProduct")
  })
  private List<Long> categoryIds;

  @JsonProperty("unit")
  @NotNull
  @AuthFields(list = {
          @AuthField(frontendId = "ProductListComponent.unit", controller = "getProducts"),
          @AuthField(frontendId = "ProductListComponent.unit", controller = "getProduct"),
          @AuthField(frontendId = "ProductListComponent.unit", controller = "saveProduct"),
          @AuthField(frontendId = "ProductListComponent.unit", controller = "editProduct"),
          @AuthField(frontendId = "ProductListInPriceBookComponent.unit", controller = "getProductsForPriceBook"),
          @AuthField(frontendId = "AddProductFromPriceBookComponent.unit", controller = "addProductFromPriceBook"),
          @AuthField(frontendId = "ProductAddComponent.unit", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.unit", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.unit", controller = "getProduct")
  })
  private Long unitId;

  @AuthFields(list = {
          @AuthField(frontendId = "ProductListComponent.unit", controller = "getProducts"),
          @AuthField(frontendId = "ProductListComponent.unit", controller = "getProduct"),
          @AuthField(frontendId = "ProductListComponent.unit", controller = "saveProduct"),
          @AuthField(frontendId = "ProductListComponent.unit", controller = "editProduct"),
          @AuthField(frontendId = "ProductListInPriceBookComponent.unit", controller = "getProductsForPriceBook"),
          @AuthField(frontendId = "AddProductFromPriceBookComponent.unit", controller = "addProductFromPriceBook"),
          @AuthField(frontendId = "ProductAddComponent.unit", controller = "saveProduct"),
          @AuthField(frontendId = "ProductEditComponent.unit", controller = "editProduct"),
          @AuthField(frontendId = "ProductEditComponent.unit", controller = "getProduct")
  })
  private String unitName;

  public Product toModel(Product product) {
    product.setName(name);
    product.setCode(code);
    product.setSkuCode(skuCode);
    product.setBrand(brand);
    product.setPurchasePrice(purchasePrice);
    product.setPurchasePriceCurrency(purchasePriceCurrency);
    product.setCurrency(currency);
    product.setSellPriceNet(sellPriceNet);
    product.setVat(vat);
    product.setUnitId(unitId);
    product.setActive(active);
    product.setDescription(description);
    product.setSpecification(specification);
    product.setNotes(notes);
    return product;
  }
}

