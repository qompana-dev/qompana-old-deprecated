package com.devqube.crmbusinessservice.productprice.category.web;

import com.devqube.crmbusinessservice.productprice.category.ProductCategory;
import com.devqube.crmbusinessservice.productprice.category.ProductSubcategory;
import lombok.*;

import java.util.Optional;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ProductCategoryDto {
    private Long id;
    private String name;
    private Long initialSubcategory;
    private boolean assignedToAnyProduct;

    public ProductCategoryDto(ProductCategory category) {
        this.id = category.getId();
        this.name = category.getName();
        Optional<ProductSubcategory> initial = category.getSubcategories().stream().filter(ProductSubcategory::isInitial).findFirst();
        initial.ifPresent(subcategory -> this.initialSubcategory = subcategory.getId());
        this.assignedToAnyProduct = category.getSubcategories().stream().anyMatch(sub -> !sub.getProducts().isEmpty());
    }
}
