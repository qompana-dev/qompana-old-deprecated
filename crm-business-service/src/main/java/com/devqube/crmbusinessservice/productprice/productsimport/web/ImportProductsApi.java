
package com.devqube.crmbusinessservice.productprice.productsimport.web;

import com.devqube.crmbusinessservice.productprice.productsimport.importstrategy.ProductsImportStrategy;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Optional;

@Validated
@Api(value = "ImportProducts", description = "the ImportProducts API")
public interface ImportProductsApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Import products", nickname = "importProducts", notes = "Import product from csv file")
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "Products successfully imported"),
        @ApiResponse(code = 409, message = "Sku code or code conflict"),
        @ApiResponse(code = 423, message = "Error during field conversion"),
        @ApiResponse(code = 400, message = "Wrong file format") })
    @RequestMapping(value = "/products/import",
        consumes = { "multipart/form-data" },
        method = RequestMethod.POST)
    default ResponseEntity<Object> importProducts(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "file detail") @Valid @RequestPart("file") MultipartFile file, @ApiParam(value = "", required = true, defaultValue = "null") @RequestParam(value = "strategy", required = true) ProductsImportStrategy strategy, @ApiParam(value = "") @RequestParam(value = "headerPresent", required = false) Boolean headerPresent) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }
}
