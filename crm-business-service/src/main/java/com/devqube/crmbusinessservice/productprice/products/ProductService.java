package com.devqube.crmbusinessservice.productprice.products;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.dictionary.DictionaryService;
import com.devqube.crmbusinessservice.dictionary.Type;
import com.devqube.crmbusinessservice.dictionary.dto.DictionaryDTO;
import com.devqube.crmbusinessservice.dictionary.dto.WordDTO;
import com.devqube.crmbusinessservice.goal.GoalRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.productprice.category.CategoryUtil;
import com.devqube.crmbusinessservice.productprice.category.ProductSubcategory;
import com.devqube.crmbusinessservice.productprice.category.ProductSubcategoryRepository;
import com.devqube.crmbusinessservice.productprice.exception.ObjectUsedInGoalException;
import com.devqube.crmbusinessservice.productprice.exception.PhotoOrderException;
import com.devqube.crmbusinessservice.productprice.exception.ProductCodeOccupiedException;
import com.devqube.crmbusinessservice.productprice.exception.TooManyPhotosException;
import com.devqube.crmbusinessservice.productprice.manufacturer.ManufacturerRepository;
import com.devqube.crmbusinessservice.productprice.productPrice.ProductPriceRepository;
import com.devqube.crmbusinessservice.productprice.productPrice.ProductPriceService;
import com.devqube.crmbusinessservice.productprice.productPrice.model.ProductPrice;
import com.devqube.crmbusinessservice.productprice.productPrice.model.ReadProductPrice;
import com.devqube.crmbusinessservice.productprice.products.model.LastViewedProduct;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmbusinessservice.productprice.products.model.ReadProduct;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.OpportunityProduct;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.OpportunityProductsRepository;
import com.devqube.crmbusinessservice.productprice.products.web.dto.MobileProductListDTO;
import com.devqube.crmbusinessservice.productprice.products.web.dto.PhotoDTO;
import com.devqube.crmbusinessservice.productprice.products.web.dto.ProductDTO;
import com.devqube.crmbusinessservice.productprice.products.web.dto.ProductSaleHistoryDTO;
import com.devqube.crmbusinessservice.productprice.supplier.SupplierRepository;
import com.devqube.crmbusinessservice.productprice.util.ModelMapperUtils;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.association.RemoveCrmObject;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.association.dto.AssociationPropertyDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.file.InternalFileClient;
import com.devqube.crmshared.file.dto.FileInfoDto;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.google.common.base.Strings;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.devqube.crmshared.association.dto.AssociationPropertyTypeEnum.PRODUCT_PHOTO_ORDER;

@Service
public class ProductService {
    private static final Integer MAX_PHOTOS_IN_PRODUCT = 4;

    private final LastViewedProductRepository lastViewedProductRepository;
    private final ProductRepository productRepository;
    private final ReadProductRepository readproductRepository;
    private final OpportunityProductsRepository opportunityProductsRepository;
    private final ProductPriceRepository productPriceRepository;
    private final AccessService accessService;
    private final ModelMapper modelMapper;
    private final UserClient userClient;
    private final AssociationClient associationClient;
    private final InternalFileClient internalFileClient;
    private final ProductPriceService productPriceService;
    private final ManufacturerRepository manufacturerRepository;
    private final SupplierRepository supplierRepository;
    private final GoalRepository goalRepository;
    private final DictionaryService dictionaryService;

    private final ProductSubcategoryRepository productSubcategoryRepository;


    public ProductService(LastViewedProductRepository lastViewedProductRepository,
                          ProductRepository productRepository,
                          ReadProductRepository readproductRepository,
                          OpportunityProductsRepository opportunityProductsRepository,
                          ProductPriceRepository productPriceRepository,
                          AccessService accessService,
                          ModelMapper modelMapper,
                          UserClient userClient,
                          AssociationClient associationClient,
                          InternalFileClient internalFileClient,
                          ProductPriceService productPriceService,
                          ManufacturerRepository manufacturerRepository,
                          SupplierRepository supplierRepository,
                          GoalRepository goalRepository,
                          DictionaryService dictionaryService,
                          ProductSubcategoryRepository productSubcategoryRepository) {
        this.lastViewedProductRepository = lastViewedProductRepository;
        this.productRepository = productRepository;
        this.readproductRepository = readproductRepository;
        this.opportunityProductsRepository = opportunityProductsRepository;
        this.productPriceRepository = productPriceRepository;
        this.accessService = accessService;
        this.modelMapper = modelMapper;
        this.userClient = userClient;
        this.associationClient = associationClient;
        this.internalFileClient = internalFileClient;
        this.productPriceService = productPriceService;
        this.manufacturerRepository = manufacturerRepository;
        this.supplierRepository = supplierRepository;
        this.goalRepository = goalRepository;
        this.productSubcategoryRepository = productSubcategoryRepository;
        this.dictionaryService = dictionaryService;
    }

    public Set<String> findExistingProductCodes(Set<String> productCodes) {
        return this.productRepository.findExistingProductCodes(productCodes);
    }

    public List<Product> findAllByProductCodeIn(Set<String> productCodes) {
        return this.productRepository.findAllByCodeIn(productCodes);
    }

    public Page<ProductDTO> findAll(Pageable pageable) {
        Page<ReadProduct> page = this.readproductRepository.findAllByDeletedFalseAndOpportunityProductFalse(pageable);
        return page.map(this::mapToDto);
    }

    public Page<ProductDTO> findAllCreatedToday(Pageable pageable) {
        Page<ReadProduct> page = this.readproductRepository.findAllByDeletedFalseAndOpportunityProductFalseAndCreatedAfter(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), pageable);
        return page.map(this::mapToDto);
    }

    public Page<ProductDTO> findAllRecentlyEdited(Pageable pageable) {
        Page<ReadProduct> page = this.readproductRepository.findAllByDeletedFalseAndOpportunityProductFalseAndUpdatedAfter(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), pageable);
        return page.map(this::mapToDto);
    }

    public Page<ProductDTO> findAllRecentlyViewed(Pageable pageable, String loggedAccountEmail) throws BadRequestException {
        Long userId = getUserId(loggedAccountEmail);
        Page<ReadProduct> page = readproductRepository.findAllByDeletedFalseAndLastViewedAfter(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3), userId, pageable);
        return page.map(this::mapToDto);
    }

    public Page<MobileProductListDTO> findAllForMobile(Pageable pageable, String search) {
        Page<Product> page = this.productRepository.findAllByDeletedFalseAndOpportunityProductFalseForMobile(pageable, Strings.nullToEmpty(search));
        return page.map(MobileProductListDTO::new);
    }

    public Page<MobileProductListDTO> findAllCreatedTodayForMobile(Pageable pageable, String search) {
        Page<Product> page = this.productRepository.findAllByDeletedFalseAndOpportunityProductFalseAndCreatedAfterForMobile(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), pageable, Strings.nullToEmpty(search));
        return page.map(MobileProductListDTO::new);
    }

    public Page<MobileProductListDTO> findAllRecentlyEditedForMobile(Pageable pageable, String search) {
        Page<Product> page = this.productRepository.findAllByDeletedFalseAndOpportunityProductFalseAndUpdatedAfterForMobile(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), pageable, Strings.nullToEmpty(search));
        return page.map(MobileProductListDTO::new);
    }

    public Page<MobileProductListDTO> findAllRecentlyViewedForMobile(Pageable pageable, String loggedAccountEmail, String search) throws BadRequestException {
        Long userId = getUserId(loggedAccountEmail);
        Page<Product> page = productRepository.findAllByDeletedFalseAndLastViewedAfterForMobile(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3), userId, pageable, Strings.nullToEmpty(search));
        return page.map(MobileProductListDTO::new);
    }

    public Page<ProductDTO> findAllForPriceBook(Long priceBookId, Pageable pageable) {
        Page<ReadProductPrice> page = this.productPriceService.findAllForPriceBook(priceBookId, pageable);
        return page.map(this::mapToDtoFromPriceBook);
    }

    public Page<ProductDTO> findAllCreatedTodayForPriceBook(Long priceBookId, Pageable pageable) {
        Page<ReadProductPrice> page = this.productPriceService.findAllCreatedTodayForPriceBook(priceBookId, pageable);
        return page.map(this::mapToDtoFromPriceBook);
    }

    public Page<ProductDTO> findAllRecentlyEditedForPriceBook(Long priceBookId, Pageable pageable) {
        Page<ReadProductPrice> page = this.productPriceService.findAllRecentlyEditedForPriceBook(priceBookId, pageable);
        return page.map(this::mapToDtoFromPriceBook);
    }

    public Page<ProductDTO> findAllRecentlyViewedForPriceBook(Long priceBookId, Pageable pageable, String loggedAccountEmail) throws BadRequestException {
        Long userId = getUserId(loggedAccountEmail);
        Page<ReadProductPrice> page = productPriceService.findAllRecentlyViewedForPriceBook(priceBookId, pageable, userId);
        return page.map(this::mapToDtoFromPriceBook);
    }

    private Long getUserId(String loggedAccountEmail) throws BadRequestException {
        try {
            return userClient.getMyAccountId(loggedAccountEmail);
        } catch (Exception e) {
            throw new BadRequestException("user not found");
        }
    }

    public List<Product> saveAll(List<Product> products) {
        return this.productRepository.saveAll(products);
    }

    public void deleteAllNotExistingInCodes(Set<String> codes) {
        List<Long> allNotExistingInCodes = this.productRepository.findAllNotExistingInCodes(codes);
        this.lastViewedProductRepository.deleteAllByProductIdIn(allNotExistingInCodes);
        this.productPriceRepository.deleteAllByProductIdIn(allNotExistingInCodes);
        this.productRepository.deleteAllByIdIn(allNotExistingInCodes);
    }

    public void deleteAll() {
        this.productRepository.deleteAll();
    }

    public List<Product> findAll() {
        return this.productRepository.findAll();
    }


    public List<Product> findAllContaining(String pattern) {
        return productRepository.findByNameContaining(pattern);
    }

    public Set<Product> findByIds(List<Long> ids) {
        return productRepository.findAllByIdIn(new HashSet<>(ids));
    }

    @Transactional
    public Product save(ProductDTO productToAdd) throws ProductCodeOccupiedException, BadRequestException, PhotoOrderException, EntityNotFoundException {
        checkCodesAreUniqueAndThrowIfNot(productToAdd.getCode(), productToAdd.getSkuCode());
        productToAdd = accessService.assignAndVerify(productToAdd);
        Product toSave = setManufacturerAndSupplier(modelMapper.map(productToAdd, Product.class),
                productToAdd.getManufacturerId(), productToAdd.getSupplierId());
        Product result = this.productRepository.save(toSave);
        if (productToAdd.getPhotos() != null) {
            savePhotos(productToAdd.getPhotos(), result.getId());
        }
        addCategories(result, productToAdd.getSubcategoryIds());
        productPriceService.setProductPricesForProductId(result.getId(), productToAdd.getProductPrices());
        return result;
    }

    private Product setManufacturerAndSupplier(Product toSave, Long manufacturerId, Long supplierId) {
        toSave.setManufacturer(manufacturerId != null ? manufacturerRepository.getOne(manufacturerId) : null);
        toSave.setSupplier(supplierId != null ? supplierRepository.getOne(supplierId) : null);
        return toSave;
    }

    private void addCategories(Product product, List<Long> subcategoriesIds) {
        if (subcategoriesIds == null) {
            return;
        }
        List<ProductSubcategory> all = productSubcategoryRepository.findByProductsId(product.getId());
        all.forEach(sub -> sub.getProducts().removeIf(p -> p.getId().equals(product.getId())));
        List<ProductSubcategory> subcategories = productSubcategoryRepository.findAllById(subcategoriesIds);
        subcategories.forEach(s -> s.getProducts().add(product));
        product.setSubcategories(subcategories);
    }

    @Transactional
    public void modify(ProductDTO productDTO, Long id) throws BadRequestException, ProductCodeOccupiedException, EntityNotFoundException, PhotoOrderException, TooManyPhotosException {
        if (productDTO == null || id == null) {
            throw new BadRequestException("id is null");
        }
        Product fromDb = productRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        Optional<Product> byProductCode = productRepository.findByCode(productDTO.getCode());
        Optional<Product> bySkuCode = productRepository.findBySkuCode(productDTO.getSkuCode());
        if ((byProductCode.isPresent() && !byProductCode.get().getId().equals(id)) ||
                (bySkuCode.isPresent() && !bySkuCode.get().getId().equals(id))) {
            throw new ProductCodeOccupiedException("Product with this product code or sku code already exists");
        }
        ProductDTO dtoFromDb = modelMapper.map(fromDb, ProductDTO.class);
        dtoFromDb.setPhotos(getPhotos(id));
        dtoFromDb.setProductPrices(productPriceService.getProductsPriceByProductId(id));
        productDTO = accessService.assignAndVerify(dtoFromDb, productDTO);
        Product productToSave = productDTO.toModel(fromDb);
        setManufacturerAndSupplier(productToSave, productDTO.getManufacturerId(), productDTO.getSupplierId());
        addCategories(productToSave, productDTO.getSubcategoryIds());
        productRepository.save(productToSave);
        savePhotos(productDTO.getPhotos(), id);
        productPriceService.setProductPricesForProductId(id, productDTO.getProductPrices());
    }

    public void deleteById(Long id) throws EntityNotFoundException, BadRequestException, ObjectUsedInGoalException {
        Product product = productRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        if (isProductUsedInOpportunity(product)) {
            throw new BadRequestException("cannot delete product, it's used in opportunity");
        }
        if (product.getDeleted()) {
            throw new EntityNotFoundException();
        }
        if (goalRepository.countByProductsId(id) > 0) {
            throw new ObjectUsedInGoalException();
        }

        product.setDeleted(true);
        product.setCode(UUID.randomUUID().toString());
        product.setSkuCode(UUID.randomUUID().toString());
        productRepository.save(product);
        productPriceService.deleteProductPricesByProductId(id);
        RemoveCrmObject.addObjectToRemove(CrmObjectType.product, id);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = EntityNotFoundException.class)
    public void deleteByIds(List<Long> ids) throws EntityNotFoundException, BadRequestException, ObjectUsedInGoalException {
        for (Long id : ids) {
            deleteById(id);
        }
    }

    public ProductDTO getProduct(Long id) throws EntityNotFoundException, TooManyPhotosException, BadRequestException {
        Product product = productRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        ProductDTO result = modelMapper.map(product, ProductDTO.class);
        result.setPhotos(getPhotos(id));
        addCategoriesToDto(result, product);
        result.setProductPrices(productPriceService.getProductsPriceByProductId(id));
        saveLastViewed(product, CurrentRequestUtil.getLoggedInAccountEmail());
        return result;
    }

    public List<Long> getAllProductsWithAccess(String loggedAccountEmail) throws BadRequestException {
        Long userId = getUserId(loggedAccountEmail);
        List<Product> products = productRepository.findAllByDeletedFalseAndOpportunityProductFalse();
        if (!products.isEmpty()) {
            return products.stream().map(product -> product.getId()).collect(Collectors.toList());
        } else return new ArrayList<>();
    }

    private void checkCodesAreUniqueAndThrowIfNot(String code, String skuCode) throws ProductCodeOccupiedException {
        Optional<Product> byProductCode = productRepository.findByCode(code);
        Optional<Product> bySkuCode = productRepository.findBySkuCode(skuCode);
        if (byProductCode.isPresent() || bySkuCode.isPresent()) {
            throw new ProductCodeOccupiedException("Product with this product code or sku code already exists");
        }
    }

    private boolean isProductUsedInOpportunity(Product product) {
        return product.getOpportunityProducts() != null && !product.getOpportunityProducts().isEmpty();
    }

    private void saveLastViewed(Product product, String loggedAccountEmail) throws BadRequestException {
        Long userId = getUserId(loggedAccountEmail);
        LastViewedProduct newLastViewed = LastViewedProduct.builder().userId(userId).product(product).build();
        LastViewedProduct lastViewedProduct = (product.getLastViewedProducts() == null) ? newLastViewed : product.getLastViewedProducts().stream()
                .filter(lv -> lv.getUserId().equals(userId)).findFirst()
                .orElse(newLastViewed);
        lastViewedProduct.setLastViewed(LocalDateTime.now());
        lastViewedProductRepository.save(lastViewedProduct);
    }

    private List<PhotoDTO> savePhotos(List<PhotoDTO> photos, Long productId) throws PhotoOrderException {
        if (photos.stream().map(PhotoDTO::getOrder).collect(Collectors.toSet()).size() != photos.size()) {
            throw new PhotoOrderException();
        }
        List<AssociationDto> allAssociations = associationClient
                .getAllBySourceTypeAndDestinationTypeAndSourceId(CrmObjectType.product, CrmObjectType.file, productId)
                .stream().filter(c -> c.getAssociationProperties() != null && c.getAssociationProperties().size() > 0)
                .filter(c -> c.getAssociationProperties().stream().anyMatch(asso -> asso.getType().equals(PRODUCT_PHOTO_ORDER)))
                .collect(Collectors.toList());

        List<PhotoDTO> result = new ArrayList<>();
        for (PhotoDTO photo : photos) {
            if (photo.getTempFile().equals(true) && photo.getId().matches("[0-9]+")) {
                result.add(savePhoto(photo, productId, allAssociations));
            } else {
                result.add(photo);
            }
        }
        removeNotUsedPhotoAssociations(photos, allAssociations);
        return result;
    }

    private PhotoDTO savePhoto(PhotoDTO photo, Long productId, List<AssociationDto> allAssociations) {
        FileInfoDto fileResponse = movePhotoFromTemporaryFile(photo, productId);
        associationClient.createAssociation(createAssociationDetailForProductPhoto(photo, productId, fileResponse));
        removeOldAssociations(photo, allAssociations);
        return new PhotoDTO(fileResponse.getName(), false, photo.getOrder());
    }

    private FileInfoDto movePhotoFromTemporaryFile(PhotoDTO photo, Long productId) {
        Long tempFileId = Long.parseLong(photo.getId());
        FileInfoDto fileInfoDto = new FileInfoDto();
        fileInfoDto.setName("Product_" + productId + "_" + photo.getOrder());
        return internalFileClient.setTempFileInfo(tempFileId, fileInfoDto);
    }

    private AssociationDetailDto createAssociationDetailForProductPhoto(PhotoDTO photo, Long productId, FileInfoDto fileResponse) {
        AssociationPropertyDto associationPropertyDto = new AssociationPropertyDto();
        associationPropertyDto.setType(PRODUCT_PHOTO_ORDER);
        associationPropertyDto.setValue(photo.getOrder().toString());

        AssociationDetailDto associationDetail = new AssociationDetailDto();
        associationDetail.setSourceId(productId);
        associationDetail.setSourceType(CrmObjectType.product);
        associationDetail.setDestinationId(fileResponse.getFileId());
        associationDetail.setDestinationType(CrmObjectType.file);
        associationDetail.setProperties(Collections.singletonList(associationPropertyDto));
        return associationDetail;
    }

    private void removeNotUsedPhotoAssociations(List<PhotoDTO> photos, List<AssociationDto> allAssociations) {
        List<Integer> usedOrders = photos.stream().map(PhotoDTO::getOrder).collect(Collectors.toList());
        allAssociations.stream().filter(asso -> !usedOrders.contains(getPropertyForTypePhotoOrderFromAssociationProperties(asso.getAssociationProperties())))
                .forEach(associationDto -> internalFileClient.deleteFile(associationDto.getDestinationId()));
    }
    private Integer getPropertyForTypePhotoOrderFromAssociationProperties(List<AssociationPropertyDto> properties) {
        if (properties == null) {
            return null;
        }
        Optional<AssociationPropertyDto> first = properties.stream().filter(asso -> asso.getType().equals(PRODUCT_PHOTO_ORDER)).findFirst();
        return (first.isEmpty()) ? null : Integer.parseInt(first.get().getValue());
    }

    private void removeOldAssociations(PhotoDTO photo, List<AssociationDto> allAssociations) {
        allAssociations.stream()
                .filter(c -> c.getAssociationProperties().stream()
                        .anyMatch(d -> d.getType().equals(PRODUCT_PHOTO_ORDER) && d.getValue().equals(photo.getOrder().toString())))
                .findFirst().ifPresent(associationDto -> internalFileClient.deleteFile(associationDto.getDestinationId()));
    }

    private List<PhotoDTO> getPhotos(Long productId) throws TooManyPhotosException {
        List<AssociationDto> associationsList = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(CrmObjectType.product, CrmObjectType.file, productId)
                .stream().filter(c -> c.getAssociationProperties() != null && c.getAssociationProperties().stream().anyMatch(d -> d.getType().equals(PRODUCT_PHOTO_ORDER)))
                .collect(Collectors.toList());

        if (associationsList.size() > MAX_PHOTOS_IN_PRODUCT) {
            throw new TooManyPhotosException();
        }
        List<Long> ids = associationsList.stream().map(AssociationDto::getDestinationId).collect(Collectors.toList());
        if (ids.size() == 0) {
            return new ArrayList<>();
        }
        List<FileInfoDto> internalFilesInfo = internalFileClient.getFileInfo(ids);

        return associationsList.stream()
                .map(c -> getPhoto(c, internalFilesInfo))
                .filter(Objects::nonNull).collect(Collectors.toList());
    }

    private PhotoDTO getPhoto(AssociationDto association, List<FileInfoDto> files) {
        Optional<FileInfoDto> file = files.stream().filter(c -> c.getFileId().equals(association.getDestinationId())).findFirst();
        Optional<AssociationPropertyDto> associationProperty = association.getAssociationProperties()
                .stream().filter(c -> c.getType().equals(PRODUCT_PHOTO_ORDER)).findFirst();

        if (file.isEmpty() || associationProperty.isEmpty() || !associationProperty.get().getValue().matches("[0-9]+")) {
            return null;
        }

        return new PhotoDTO(file.get().getId(), false, Integer.parseInt(associationProperty.get().getValue()));
    }

    private ProductDTO mapToDtoFromPriceBook(ReadProductPrice productPrice) {
        ProductDTO dto = ModelMapperUtils.map(productPrice.getProduct(), ProductDTO.class);
        dto.setCurrency(productPrice.getPriceBook().getCurrency());
        dto.setSellPriceNet(productPrice.getPrice());
        dto.setUnitId(productPrice.getProduct().getUnit().getId());
        dto.setUnitName(productPrice.getProduct().getUnit().getName());
        return addCategoriesToDto(dto, productPrice.getProduct());
    }

    private ProductDTO mapToDto(ReadProduct product) {
        ProductDTO dto = ModelMapperUtils.map(product, ProductDTO.class);
        dto.setUnitId(product.getUnit().getId());
        dto.setUnitName(product.getUnit().getName());
        return addCategoriesToDto(dto, product);
    }

    private ProductDTO addCategoriesToDto(ProductDTO dto, Product product) {
        dto.setCategories(CategoryUtil.categoriesToString(product.getSubcategories()));
        dto.setSubcategories(CategoryUtil.subcategoriesToString(product.getSubcategories()));
        dto.setSubcategoryIds(CategoryUtil.getSubcategoryIds(product.getSubcategories()));
        dto.setCategoryIds(CategoryUtil.getCategoryIds(product.getSubcategories()));
        return dto;
    }

    private ProductDTO addCategoriesToDto(ProductDTO dto, ReadProduct product) {
        dto.setCategories(CategoryUtil.categoriesToString(product.getSubcategories()));
        dto.setSubcategories(CategoryUtil.subcategoriesToString(product.getSubcategories()));
        dto.setSubcategoryIds(CategoryUtil.getSubcategoryIds(product.getSubcategories()));
        dto.setCategoryIds(CategoryUtil.getCategoryIds(product.getSubcategories()));
        return dto;
    }

    public List<ProductSaleHistoryDTO> findAllSellHistoryByProductId(Long productId)
            throws BadRequestException {
        List<OpportunityProduct> opportunityProducts = opportunityProductsRepository.findAllByProductIdAndOpportunityFinishResultNotNull(productId);
        Map<Long, String> units = getUnitsMap();
        return opportunityProducts.stream().map(opportunityProduct -> ProductSaleHistoryDTO.fromModel(opportunityProduct, units))
                .sorted(Comparator.comparing(ProductSaleHistoryDTO::getFinishDate, Comparator.nullsLast(Comparator.reverseOrder())))
                .collect(Collectors.toList());
    }

    public List<ProductSaleHistoryDTO> findAllSellHistoryByProductIdAndFinishResult(Long productId, Opportunity.FinishResult finishResult)
            throws BadRequestException {
        List<OpportunityProduct> opportunityProducts = opportunityProductsRepository.findAllByProductIdAndOpportunityFinishResult(productId, finishResult);
        Map<Long, String> units = getUnitsMap();
        return opportunityProducts.stream().map(opportunityProduct -> ProductSaleHistoryDTO.fromModel(opportunityProduct, units))
                .sorted(Comparator.comparing(ProductSaleHistoryDTO::getFinishDate, Comparator.nullsLast(Comparator.reverseOrder())))
                .collect(Collectors.toList());
    }

    private Map<Long, String> getUnitsMap() {
        DictionaryDTO dictionaryDTO = dictionaryService.findAdminDictionary(Type.UNIT.getDictionaryId());
        return dictionaryDTO.getWords().stream().collect(Collectors.toMap(WordDTO::getId, WordDTO::getName));
    }
}
