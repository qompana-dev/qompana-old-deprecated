package com.devqube.crmbusinessservice.productprice.manufacturer.web;

import com.devqube.crmbusinessservice.productprice.manufacturer.Manufacturer;
import com.devqube.crmshared.search.CrmObject;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
@Api(value = "Manufacturer")
public interface ManufacturerApi {

    @ApiOperation(value = "save new manufacturers", nickname = "saveManufacturers", notes = "save new manufacturers")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "manufacturers saved successfully", response = Manufacturer.class),
            @ApiResponse(code = 400, message = "one of manufacturers name is occupied")
    })
    ResponseEntity save(@ApiParam(value="manufacturers to save") List<Manufacturer> manufacturers);

    @ApiOperation(value = "get page of manufacturers", nickname = "findAllManufacturers", notes = "find all manufacturers", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "page retrieved successfully", response = Page.class)
    })
    ResponseEntity<Page<Manufacturer>> findAll(Pageable pageable);

    @ApiOperation(value = "delete manufacturer by id", nickname = "deleteManufacturer", notes = "delete manufacturer by id")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "manufacturer deleted"),
            @ApiResponse(code = 400, message = "cannot delete manufacturer, it's attached to products")
    })
    ResponseEntity deleteById(@ApiParam(value = "manufacturer id") Long id);

    @ApiOperation(value = "Get manufacturers list for search", nickname = "getManufacturersForSearch", notes = "Method used to get manufacturers list", response = CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all manufacturers names", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List") })
    ResponseEntity<List<CrmObject>> getManufacturersForSearch(String term);

    @ApiOperation(value = "Get manufacturers list by ids", nickname = "getManufacturersAsCrmObject", notes = "Method used to get manufacturers as crm objects by ids", response = CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found manufacturer", response = CrmObject.class, responseContainer = "List") })
    ResponseEntity<List<CrmObject>> getManufacturersAsCrmObject(List<Long> ids);
}
