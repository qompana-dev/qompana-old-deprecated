package com.devqube.crmbusinessservice.productprice.productsimport;

import com.devqube.crmbusinessservice.productprice.productsimport.dto.ProductDto;
import com.univocity.parsers.common.DataProcessingException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
class ProductParseExceptionService {

    void throwParseExceptionIfIncorrect(List<DataProcessingException> exceptionList, List<ProductDto> productDtoList, List<DictionaryWord> units) throws ParseException {
        List<ParseExceptionDetailDto> result = new ArrayList<>();
        result.addAll(getParseExceptions(exceptionList));
        result.addAll(getUnitsExceptions(productDtoList, units));

        if (result.size() > 0) {
            throw new ParseException(result);
        }
    }

    private List<ParseExceptionDetailDto> getUnitsExceptions(List<ProductDto> productDtoList, List<DictionaryWord> units) {
        List<String> names = new ArrayList<>();
        units.forEach(unit -> names.add((unit.getName()).toLowerCase()));

        return productDtoList.stream().filter(product -> !names.contains(product.getUnit().toLowerCase()))
                .map(product -> new ParseExceptionDetailDto(product.getLine(), "unit", product.getUnit()))
                .collect(Collectors.toList());
    }

    private List<ParseExceptionDetailDto> getParseExceptions(List<DataProcessingException> exceptionList) {
        List<ParseExceptionDetailDto> result = new ArrayList<>();
        if (exceptionList != null && exceptionList.size() != 0) {
            for (DataProcessingException ex : exceptionList) {
                try {
                    ParseExceptionDetailDto parseExceptionDetailDto = new ParseExceptionDetailDto();
                    parseExceptionDetailDto.setLine(ex.getLineIndex());
                    parseExceptionDetailDto.setField(ex.getHeaders()[ex.getColumnIndex()]);
                    parseExceptionDetailDto.setValue(ex.getValue().toString());
                    result.add(parseExceptionDetailDto);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
