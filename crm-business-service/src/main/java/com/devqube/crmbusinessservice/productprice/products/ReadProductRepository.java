package com.devqube.crmbusinessservice.productprice.products;

import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmbusinessservice.productprice.products.model.ReadProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface ReadProductRepository extends JpaRepository<ReadProduct, Long> {

    Page<ReadProduct> findAllByDeletedFalseAndOpportunityProductFalseAndCreatedAfter(LocalDateTime date, Pageable pageable);

    Page<ReadProduct> findAllByDeletedFalseAndOpportunityProductFalseAndUpdatedAfter(LocalDateTime date, Pageable pageable);

    @Query("select p from ReadProduct p " +
            "where p.deleted = false and p.opportunityProduct = false and (select max(lvp.lastViewed) from LastViewedProduct lvp where lvp.product.id = p.id and lvp.userId = :userId) > :date ")
    Page<ReadProduct> findAllByDeletedFalseAndLastViewedAfter(@Param("date") LocalDateTime date, @Param("userId") Long userId, Pageable pageable);

    Page<ReadProduct> findAllByDeletedFalseAndOpportunityProductFalse(Pageable pageable);
}
