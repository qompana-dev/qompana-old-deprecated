package com.devqube.crmbusinessservice.productprice.products.web;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.productprice.exception.ObjectUsedInGoalException;
import com.devqube.crmbusinessservice.productprice.exception.PhotoOrderException;
import com.devqube.crmbusinessservice.productprice.exception.ProductCodeOccupiedException;
import com.devqube.crmbusinessservice.productprice.exception.TooManyPhotosException;
import com.devqube.crmbusinessservice.productprice.products.ProductService;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmbusinessservice.productprice.products.web.dto.ProductDTO;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.access.annotation.AuthControllerCase;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class ProductsController implements ProductsApi {

    private final ProductService productService;
    private final ModelMapper modelMapper;

    public ProductsController(ProductService productService, ModelMapper modelMapper) {
        this.productService = productService;
        this.modelMapper = modelMapper;
    }

    @Override
    @AuthController(name = "getProduct")
    public ResponseEntity<ProductDTO> getProduct(Long id) {
        try {
            return new ResponseEntity<>(productService.getProduct(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Product with id {} not found", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (TooManyPhotosException e) {
            log.info("Too many photos for product {}", id);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @AuthController(name = "getProducts")
    public ResponseEntity<Page> getProducts(@Valid Pageable pageable, @Valid Boolean today, @Valid Boolean edited, @Valid Boolean viewed) {
        try {
            if (today != null && today) {
                return ResponseEntity.ok(productService.findAllCreatedToday(pageable));
            } else if (edited != null && edited) {
                return ResponseEntity.ok(productService.findAllRecentlyEdited(pageable));
            } else if (viewed != null && viewed) {
                return ResponseEntity.ok(productService.findAllRecentlyViewed(pageable, CurrentRequestUtil.getLoggedInAccountEmail()));
            } else {
                return ResponseEntity.ok(productService.findAll(pageable));
            }
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @Override // todo: permissions
    public ResponseEntity<Page> getProductsForMobile(@Valid Pageable pageable, @Valid Boolean today, @Valid Boolean edited, @Valid Boolean viewed, @Valid String search) {
        try {
            if (today != null && today) {
                return ResponseEntity.ok(productService.findAllCreatedTodayForMobile(pageable, search));
            } else if (edited != null && edited) {
                return ResponseEntity.ok(productService.findAllRecentlyEditedForMobile(pageable, search));
            } else if (viewed != null && viewed) {
                return ResponseEntity.ok(productService.findAllRecentlyViewedForMobile(pageable, CurrentRequestUtil.getLoggedInAccountEmail(), search));
            } else {
                return ResponseEntity.ok(productService.findAllForMobile(pageable, search));
            }
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @AuthController(name = "getProductSalesHistory")
    public ResponseEntity<List> getProductSalesHistory(Long productId, @Valid Opportunity.FinishResult finishResult) {
        try {
            if (finishResult != null) {
                return ResponseEntity.ok(productService.findAllSellHistoryByProductIdAndFinishResult(productId, finishResult));
            } else {
                return ResponseEntity.ok(productService.findAllSellHistoryByProductId(productId));
            }

        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @AuthController(name = "getProductsForPriceBook", controllerCase = {
            @AuthControllerCase(type = "addProductFromPriceBook", name = "addProductFromPriceBook")
    })
    public ResponseEntity<Page> getProductsForPriceBook(Long priceBookId, @Valid Pageable pageable, @Valid Boolean today, @Valid Boolean edited, @Valid Boolean viewed) {
        try {
            if (today != null && today) {
                return ResponseEntity.ok(productService.findAllCreatedTodayForPriceBook(priceBookId, pageable));
            } else if (edited != null && edited) {
                return ResponseEntity.ok(productService.findAllRecentlyEditedForPriceBook(priceBookId, pageable));
            } else if (viewed != null && viewed) {
                return ResponseEntity.ok(productService.findAllRecentlyViewedForPriceBook(priceBookId, pageable, CurrentRequestUtil.getLoggedInAccountEmail()));
            } else {
                return ResponseEntity.ok(productService.findAllForPriceBook(priceBookId, pageable));
            }
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @AuthController(actionFrontendId = "ProductListComponent.saveProduct", name = "saveProduct")
    public ResponseEntity<ProductDTO> saveProduct(@Valid ProductDTO productDTO) {
        try {
            Product savedProduct = this.productService.save(productDTO);
            return new ResponseEntity<>(modelMapper.map(savedProduct, ProductDTO.class), HttpStatus.CREATED);
        } catch (ProductCodeOccupiedException | PhotoOrderException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(actionFrontendId = "ProductListComponent.editProduct", name = "editProduct")
    public ResponseEntity<Void> updateProduct(Long id, @Valid ProductDTO productDTO) {
        try {
            productService.modify(productDTO, id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Product with id {} not found", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (ProductCodeOccupiedException e) {
            log.info("New name {} is occupied", id);
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (BadRequestException | PhotoOrderException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (TooManyPhotosException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @AuthController(actionFrontendId = "ProductListComponent.deleteProduct")
    public ResponseEntity<Void> deleteProduct(Long id) {
        try {
            productService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Customer with id {} not found", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (ObjectUsedInGoalException e) {
            log.info(e.getMessage());
            return ResponseEntity.status(440).build();
        }
    }

    @Override
    @AuthController(actionFrontendId = "ProductListComponent.deleteProduct")
    public ResponseEntity<Void> deleteProducts(@Valid List<Long> ids) {
        try {
            productService.deleteByIds(ids);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (ObjectUsedInGoalException e) {
            log.info(e.getMessage());
            return ResponseEntity.status(440).build();
        }
    }

    @Override
    public ResponseEntity<List<CrmObject>> getProductsForSearch(@NotNull @Valid String term) {
        return new ResponseEntity<>(productService.findAllContaining(term)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.product, Strings.nullToEmpty(e.getName()), e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getProductsAsCrmObject(@Valid List<Long> ids) {
        return new ResponseEntity<>(productService.findByIds(ids)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.product, Strings.nullToEmpty(e.getName()), e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Long>> getAllProductsWithAccess(String loggedAccountEmail) {
        try {
            return new ResponseEntity<>(this.productService.getAllProductsWithAccess(loggedAccountEmail), HttpStatus.OK);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
