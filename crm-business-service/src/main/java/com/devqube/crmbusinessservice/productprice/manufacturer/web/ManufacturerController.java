package com.devqube.crmbusinessservice.productprice.manufacturer.web;

import com.devqube.crmbusinessservice.productprice.manufacturer.Manufacturer;
import com.devqube.crmbusinessservice.productprice.manufacturer.ManufacturerService;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/manufacturer")
@Slf4j
public class ManufacturerController implements ManufacturerApi {

    private final ManufacturerService manufacturerService;

    public ManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @Override
    @PostMapping
    @AuthController(actionFrontendId = "NavigationMenu.manufacturersAndSuppliers")
    public ResponseEntity save(@RequestBody List<Manufacturer> manufacturers) {
        try {
            manufacturerService.saveAll(manufacturers);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @GetMapping
    @AuthController(actionFrontendId = "NavigationMenu.manufacturersAndSuppliers")
    public ResponseEntity<Page<Manufacturer>> findAll(Pageable pageable) {
        return new ResponseEntity<>(manufacturerService.findAll(pageable), HttpStatus.OK);
    }

    @Override
    @DeleteMapping("/{id}")
    @AuthController(actionFrontendId = "NavigationMenu.manufacturersAndSuppliers")
    public ResponseEntity deleteById(@PathVariable("id") Long id) {
        try {
            manufacturerService.deleteById(id);
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        } catch (BadRequestException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @GetMapping("/search")
    public ResponseEntity<List<CrmObject>> getManufacturersForSearch(String term) {
        return new ResponseEntity<>(manufacturerService.findAllContaining(term)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.manufacturer, Strings.nullToEmpty(e.getName()), null))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    @GetMapping("/crm-object")
    public ResponseEntity<List<CrmObject>> getManufacturersAsCrmObject(
            @RequestParam(value = "ids", defaultValue = "new ArrayList<>()") List<Long> ids) {
        return new ResponseEntity<>(manufacturerService.findAllByIds(ids)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.manufacturer, Strings.nullToEmpty(e.getName()), null))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }
}
