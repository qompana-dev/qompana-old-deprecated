package com.devqube.crmbusinessservice.productprice.productsimport.importstrategy;

import com.devqube.crmbusinessservice.productprice.products.ProductService;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmshared.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OverrideProductsStrategy implements ImportStrategy {

    private final ProductService productService;
    private final AddOnlyNewProductsStrategy addOnlyNewProductsStrategy;
    private final UpdateExistingProductsStrategy updateExistingProductsStrategy;

    public OverrideProductsStrategy(ProductService productService, AddOnlyNewProductsStrategy addOnlyNewProductsStrategy, UpdateExistingProductsStrategy updateExistingProductsStrategy) {
        this.productService = productService;
        this.addOnlyNewProductsStrategy = addOnlyNewProductsStrategy;
        this.updateExistingProductsStrategy = updateExistingProductsStrategy;
    }

    @Override
    @Transactional(rollbackFor = {EntityNotFoundException.class})
    public void save(List<Product> products) throws EntityNotFoundException {
        this.addOnlyNewProductsStrategy.save(products);
        this.updateExistingProductsStrategy.save(products);
        this.productService.deleteAllNotExistingInCodes(products.stream().map(Product::getCode).collect(Collectors.toSet()));
    }
}
