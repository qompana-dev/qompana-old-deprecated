package com.devqube.crmbusinessservice.productprice.category.web;

import lombok.*;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ProductCategoryWithSubcategorySaveDto {
    private Long id;
    private String name;
    private Set<ProductSubcategorySaveDto> subcategories;
    private boolean toDelete;

}
