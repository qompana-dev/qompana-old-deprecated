package com.devqube.crmbusinessservice.productprice.productsimport.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CsvParsingErrorResponse {
    private Long lineIndex;
    private Long columnIndex;
    private String corruptedText;
}
