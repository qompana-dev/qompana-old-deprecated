package com.devqube.crmbusinessservice.productprice.exception;

public class ProductCodeOccupiedException extends Exception {
    public ProductCodeOccupiedException(String message) {
        super(message);
    }
}
