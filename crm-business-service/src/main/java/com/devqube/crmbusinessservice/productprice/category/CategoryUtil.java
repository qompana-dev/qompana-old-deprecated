package com.devqube.crmbusinessservice.productprice.category;

import java.util.List;
import java.util.stream.Collectors;

public class CategoryUtil {

    static public String categoriesToString(List<ProductSubcategory> subcategories){
        return subcategories.stream()
                .map(s -> s.getProductCategory().getName())
                .distinct()
                .collect(Collectors.joining(", "));
    }

    static public String subcategoriesToString(List<ProductSubcategory> subcategories){
        return subcategories.stream()
                .filter(s -> !s.isInitial())
                .map(ProductSubcategory::getName)
                .distinct()
                .collect(Collectors.joining(", "));
    }

    public static List<Long> getCategoryIds(List<ProductSubcategory> subcategories) {
        return subcategories.stream()
                .map(s -> s.getProductCategory().getId())
                .distinct()
                .collect(Collectors.toList());
    }

    public static List<Long> getSubcategoryIds(List<ProductSubcategory> subcategories) {
        return subcategories.stream()
                .map(ProductSubcategory::getId)
                .distinct()
                .collect(Collectors.toList());
    }
}
