package com.devqube.crmbusinessservice.productprice.exception;

public class ObjectUsedInGoalException extends Exception {
    public ObjectUsedInGoalException() {
    }

    public ObjectUsedInGoalException(String message) {
        super(message);
    }

    public ObjectUsedInGoalException(String message, Throwable cause) {
        super(message, cause);
    }

    public ObjectUsedInGoalException(Throwable cause) {
        super(cause);
    }

    public ObjectUsedInGoalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
