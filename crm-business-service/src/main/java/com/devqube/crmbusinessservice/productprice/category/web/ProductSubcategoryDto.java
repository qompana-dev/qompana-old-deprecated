package com.devqube.crmbusinessservice.productprice.category.web;

import com.devqube.crmbusinessservice.productprice.category.ProductCategory;
import com.devqube.crmbusinessservice.productprice.category.ProductSubcategory;
import lombok.*;

import java.util.Optional;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ProductSubcategoryDto {
    private Long id;
    private Long categoryId;
    private String categoryName;
    private String name;
    private boolean initial;
    private boolean assignedToAnyProduct;

    ProductSubcategoryDto(ProductSubcategory subcategory){
        this.id = subcategory.getId();
        this.categoryId = subcategory.getProductCategory() != null ? subcategory.getProductCategory().getId() : null;
        this.categoryName = Optional.ofNullable(subcategory.getProductCategory()).map(ProductCategory::getName).orElse(null);
        this.name = subcategory.getName();
        this.initial = subcategory.isInitial();
        this.assignedToAnyProduct = false; //!subcategory.getProducts().isEmpty();
    }
}
