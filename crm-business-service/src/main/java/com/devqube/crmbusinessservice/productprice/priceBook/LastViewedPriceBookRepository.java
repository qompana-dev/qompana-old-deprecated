package com.devqube.crmbusinessservice.productprice.priceBook;

import com.devqube.crmbusinessservice.productprice.priceBook.model.LastViewedPriceBook;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LastViewedPriceBookRepository extends JpaRepository<LastViewedPriceBook, Long> {
}
