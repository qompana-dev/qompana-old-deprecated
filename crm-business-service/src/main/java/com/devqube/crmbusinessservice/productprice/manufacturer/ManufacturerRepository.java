package com.devqube.crmbusinessservice.productprice.manufacturer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {
    @Query("select case when count(m)> 0 then true else false end from Manufacturer m where lower(m.name) = lower(:name)")
    boolean existsByName(@Param("name") String name);

    List<Manufacturer> findByIdIn(List<Long> ids);

    List<Manufacturer> findTop20ByNameContaining(String pattern);
}
