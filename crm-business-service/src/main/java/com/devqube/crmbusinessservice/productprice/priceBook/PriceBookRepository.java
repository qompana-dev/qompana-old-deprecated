package com.devqube.crmbusinessservice.productprice.priceBook;

import com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PriceBookRepository extends JpaRepository<PriceBook, Long> {

    Page<PriceBook> findAllByDeletedFalseOrderByIsDefaultDesc(Pageable pageable);

    Page<PriceBook> findAllByDeletedFalseAndCreatedAfterOrderByIsDefaultDesc(LocalDateTime date, Pageable pageable);

    Page<PriceBook> findAllByDeletedFalseAndUpdatedAfterOrderByIsDefaultDesc(LocalDateTime date, Pageable pageable);

    @Query("select p from PriceBook p " +
            "where p.deleted = false and (select max(lvp.lastViewed) from LastViewedPriceBook lvp where lvp.priceBook.id = p.id and lvp.userId = :userId) > :date order by p.isDefault desc")
    Page<PriceBook> findAllByDeletedFalseAndLastViewedAfter(LocalDateTime date, Long userId, Pageable pageable);

    Optional<PriceBook> findByName(String name);

    List<PriceBook> findAllByDeletedFalseAndIsDefaultTrue();

    List<PriceBook> findAllByDeletedFalse();

    List<PriceBook> findAllByCurrencyAndDeletedFalse(String currency);

    Optional<PriceBook> findByIsDefaultTrue();
}
