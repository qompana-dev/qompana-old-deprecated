package com.devqube.crmbusinessservice.productprice.category;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {

    List<ProductCategory> findByIdIn(Collection<Long> ids);

    void deleteByIdIn(List<Long> ids);

}
