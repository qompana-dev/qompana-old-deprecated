package com.devqube.crmbusinessservice.productprice.productPrice;

import com.devqube.crmbusinessservice.productprice.productPrice.model.ReadProductPrice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;

public interface ReadProductPriceRepository extends JpaRepository<ReadProductPrice, Long> {

    @Query("select pp from ReadProductPrice pp where pp.priceBook.id = :priceId and pp.product.deleted = false and pp.product.created > :date")
    Page<ReadProductPrice> findAllByDeletedFalseAndCreatedAfterAndPriceBookId(@Param("priceId") Long priceId, @Param("date") LocalDateTime date, Pageable pageable);

    @Query("select pp from ReadProductPrice pp where pp.priceBook.id = :priceId and pp.product.deleted = false and pp.product.updated > :date")
    Page<ReadProductPrice> findAllByDeletedFalseAndUpdatedAfterAndPriceBookId(@Param("priceId") Long priceId, @Param("date") LocalDateTime date, Pageable pageable);

    @Query("select pp from ReadProductPrice pp " +
            "where pp.priceBook.id = :priceId and pp.product.deleted = false and (select max(lvp.lastViewed) from LastViewedProduct lvp where lvp.product.id = pp.product.id and lvp.userId = :userId) > :date ")
    Page<ReadProductPrice> findAllByDeletedFalseAndLastViewedAfterAndPriceBookId(@Param("priceId") Long priceId, @Param("date") LocalDateTime date, @Param("userId") Long userId, Pageable pageable);

    @Query("select pp from ReadProductPrice pp where pp.priceBook.id = :priceId and pp.product.deleted = false")
    Page<ReadProductPrice> findAllByDeletedFalseAndPriceBookId(@Param("priceId") Long priceId, Pageable pageable);
}
