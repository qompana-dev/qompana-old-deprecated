package com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@AuthFilterEnabled
public class OpportunityProductSaveDTO {
  @AuthField(controller = "addOpportunityProductManually")
  private Long id;
  @AuthField(controller = "addOpportunityProductManually", frontendId = "OpportunityProductsCard.name")
  private String name;
  @AuthField(controller = "addOpportunityProductManually", frontendId = "OpportunityProductsCard.code")
  private String code;
  @AuthField(controller = "addOpportunityProductManually", frontendId = "OpportunityProductsCard.skuCode")
  private String skuCode;
  @AuthField(controller = "addOpportunityProductManually", frontendId = "OpportunityProductsCard.categoryIds")
  private List<Long> categoryIds = null;
  @AuthField(controller = "addOpportunityProductManually", frontendId = "OpportunityProductsCard.subcategoryIds")
  private List<Long> subcategoryIds = null;
  @AuthField(controller = "addOpportunityProductManually", frontendId = "OpportunityProductsCard.supplier")
  private Long supplierId;
  @AuthField(controller = "addOpportunityProductManually", frontendId = "OpportunityProductsCard.manufacturer")
  private Long manufacturerId;
  @AuthField(controller = "addOpportunityProductManually")
  private String currency;
  @AuthField(controller = "addOpportunityProductManually")
  private Long unit;
  @AuthField(controller = "addOpportunityProductManually", frontendId = "OpportunityProductsCard.purchasePrice")
  private Double purchasePrice;
  @AuthField(controller = "addOpportunityProductManually", frontendId = "OpportunityProductsCard.sellPrice")
  private Double sellPrice;
  @AuthField(controller = "addOpportunityProductManually", frontendId = "OpportunityProductsCard.quantity")
  private Long quantity;
}

