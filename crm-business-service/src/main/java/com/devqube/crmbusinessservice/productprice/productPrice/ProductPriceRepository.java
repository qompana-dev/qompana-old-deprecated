package com.devqube.crmbusinessservice.productprice.productPrice;

import com.devqube.crmbusinessservice.productprice.productPrice.model.ProductPrice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface ProductPriceRepository extends JpaRepository<ProductPrice, Long> {
    List<ProductPrice> getAllByProductId(Long id);
    void deleteAllByProductId(Long id);
    void deleteAllByProductIdIn(List<Long> id);
    void deleteAllByPriceBookId(Long id);
}
