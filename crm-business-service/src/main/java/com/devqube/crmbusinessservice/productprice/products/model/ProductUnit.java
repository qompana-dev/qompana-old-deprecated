package com.devqube.crmbusinessservice.productprice.products.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "word")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@Getter
public class ProductUnit {

    @Id
    private Long id;
    private String name;
}
