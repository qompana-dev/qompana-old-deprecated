package com.devqube.crmbusinessservice.productprice.productsimport;

import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmbusinessservice.productprice.productsimport.dto.ProductDto;
import com.devqube.crmshared.exception.BadRequestException;

import java.util.ArrayList;
import java.util.List;

class ProductFactory {

    static List<Product> convert(List<ProductDto> productDtoList, List<DictionaryWord> units) throws BadRequestException {
        List<Product> products = new ArrayList<>();
        for (ProductDto productDto : productDtoList) {
            products.add(convert(productDto, units));
        }
        return products;
    }

    private static Product convert(ProductDto productDto, List<DictionaryWord> units) throws BadRequestException {
        Product product = new Product();

        product.setName(productDto.getName());
        product.setCode(productDto.getCode());
        product.setSkuCode(productDto.getSkuCode());
        product.setBrand(productDto.getBrand());
        product.setPurchasePrice(productDto.getPurchasePrice());
        product.setPurchasePriceCurrency(productDto.getPurchasePriceCurrency());
        product.setCurrency(productDto.getCurrency());
        product.setSellPriceNet(productDto.getSellPriceNet());
        product.setVat(productDto.getVat());
        product.setActive(productDto.getActive());
        product.setDescription(productDto.getDescription());
        product.setSpecification(productDto.getSpecification());

        Long unitId = units.stream()
                .filter(dictionaryWord -> isUnitEqual(dictionaryWord, productDto.getUnit()))
                .findFirst()
                .map(DictionaryWord::getId)
                .orElseThrow(BadRequestException::new);

        product.setUnitId(unitId);

        return product;
    }

    private static boolean isUnitEqual(DictionaryWord dictionaryWord, String unit) {
        return dictionaryWord.getName().toLowerCase().equals(unit.toLowerCase());
    }
}
