package com.devqube.crmbusinessservice.productprice.category;

import com.devqube.crmbusinessservice.productprice.products.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ProductSubcategoryRepository extends JpaRepository<ProductSubcategory, Long> {

    void deleteByIdInAndInitialFalse(@Param("ids") List<Long> ids);

    boolean existsByProductCategoryIdAndAndInitialTrue(Long productCategoryId);

    List<ProductSubcategory> findByProductsId(Long productId);

    Set<ProductSubcategory> findAllByIdIn(Set<Long> ids);

}
