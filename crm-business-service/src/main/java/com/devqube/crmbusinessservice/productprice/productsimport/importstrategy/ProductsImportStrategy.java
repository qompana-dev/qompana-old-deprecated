package com.devqube.crmbusinessservice.productprice.productsimport.importstrategy;

public enum ProductsImportStrategy {
    OVERRIDE("overrideProductsStrategy"),
    ADD_ALL("addAllProductsStrategy"),
    ADD_ONLY_NEW("addOnlyNewProductsStrategy"),
    UPDATE_EXISTING("updateExistingProductsStrategy");

    private String name;

    ProductsImportStrategy(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
