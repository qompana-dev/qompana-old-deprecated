package com.devqube.crmbusinessservice.productprice.products.web.dto;

import com.devqube.crmbusinessservice.productprice.category.CategoryUtil;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@AuthFilterEnabled
public class MobileProductListDTO {
    private Long id;

    private String name;

    private String code;

    private String categories;

    private String subcategories;

    private String currency;

    private Double sellPriceNet;

    public MobileProductListDTO(Product product) {
        this.setId(product.getId());
        this.setName(product.getName());
        this.setCode(product.getCode());
        this.setCurrency(product.getCurrency());
        this.setSellPriceNet(product.getSellPriceNet());
        this.setCategories(CategoryUtil.categoriesToString(product.getSubcategories()));
        this.setSubcategories(CategoryUtil.subcategoriesToString(product.getSubcategories()));
    }
}

