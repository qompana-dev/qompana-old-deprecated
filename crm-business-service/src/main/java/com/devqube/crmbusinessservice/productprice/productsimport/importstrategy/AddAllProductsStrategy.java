package com.devqube.crmbusinessservice.productprice.productsimport.importstrategy;

import com.devqube.crmbusinessservice.productprice.productPrice.ProductPriceService;
import com.devqube.crmbusinessservice.productprice.products.ProductService;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmshared.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AddAllProductsStrategy implements ImportStrategy {

    private final ProductService productService;
    private final ProductPriceService productPriceService;

    public AddAllProductsStrategy(ProductService productService, ProductPriceService productPriceService) {
        this.productService = productService;
        this.productPriceService = productPriceService;
    }

    @Override
    @Transactional(rollbackFor = {EntityNotFoundException.class})
    public void save(List<Product> products) throws EntityNotFoundException {
        List<Product> saved = productService.saveAll(products);
        productPriceService.setDefaultPriceBookForProducts(products);
    }
}

