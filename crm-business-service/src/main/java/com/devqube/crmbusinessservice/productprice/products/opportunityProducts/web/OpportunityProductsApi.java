package com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductAddFromPriceBookDTO;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductEditDTO;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductSaveDTO;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "OpportunityProducts")
public interface OpportunityProductsApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "get opportunity's products", nickname = "getOpportunityProducts", notes = "get all opportunity's products", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "opportunity products retrieved successfully", response = org.springframework.data.domain.Page.class),
            @ApiResponse(code = 403, message = "current user has no access to this opportunity"),
            @ApiResponse(code = 404, message = "opportunity not found") })
    @RequestMapping(value = "/opportunity/{id}/products",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<org.springframework.data.domain.Page> getOpportunityProducts(@ApiParam(value = "opportunity id",required=true) @PathVariable("id") Long id,@ApiParam(value = "" ,required=true) @RequestHeader(value="Logged-Account-Email", required=true) String loggedAccountEmail,@ApiParam(value = "", defaultValue = "null") org.springframework.data.domain.Pageable pageable) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Add opportunity product manually", nickname = "addOpportunityProductManually", notes = "Method used to add opportunity product manually")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "opportunity product added"),
            @ApiResponse(code = 400, message = "validation failed"),
            @ApiResponse(code = 403, message = "no permission"),
            @ApiResponse(code = 404, message = "opportunity not found"),
            @ApiResponse(code = 409, message = "code or sku code is occupied") })
    @RequestMapping(value = "/opportunity/{id}/products/manual",
            consumes = { "application/json" },
            method = RequestMethod.POST)
    default ResponseEntity<Void> addOpportunityProductManually(@ApiParam(value = "",required=true) @PathVariable("id") Long id,@ApiParam(value = "" ,required=true) @RequestHeader(value="Logged-Account-Email", required=true) String loggedAccountEmail,@ApiParam(value = "Opportunity product to add"  )  @Valid @RequestBody OpportunityProductSaveDTO opportunityProductSaveDTO) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Add opportunity products", nickname = "addOpportunityProducts", notes = "Method used to add opportunity products")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "opportunity product added"),
            @ApiResponse(code = 400, message = "validation failed"),
            @ApiResponse(code = 403, message = "no permission"),
            @ApiResponse(code = 404, message = "opportunity not found"),
            @ApiResponse(code = 409, message = "code or sku code is occupied") })
    @RequestMapping(value = "/opportunity/{id}/products",
            consumes = { "application/json" },
            method = RequestMethod.POST)
    default ResponseEntity<Void> addOpportunityProducts(@ApiParam(value = "",required=true) @PathVariable("id") Long id,@ApiParam(value = "" ,required=true) @RequestHeader(value="Logged-Account-Email", required=true) String loggedAccountEmail,@ApiParam(value = "Opportunity product list to add"  )  @Valid @RequestBody List<OpportunityProductAddFromPriceBookDTO> opportunityProductAddFromPriceBookDTO) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Delete opportunity product", nickname = "removeOpportunityProduct", notes = "Method used to remove opportunity product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "opportunity product removed"),
            @ApiResponse(code = 403, message = "permission denied"),
            @ApiResponse(code = 404, message = "opportunity product not found")})
    @RequestMapping(value = "/opportunity/{opportunityId}/products/{productId}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> removeOpportunityProduct(@ApiParam(value = "", required = true) @PathVariable("opportunityId") Long opportunityId, @ApiParam(value = "", required = true) @PathVariable("productId") Long productId, @ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Edit opportunity product", nickname = "editOpportunityProduct", notes = "Method used to edit opportunity product")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "opportunity product edited"),
            @ApiResponse(code = 403, message = "no permission"),
            @ApiResponse(code = 404, message = "opportunity not found")})
    @RequestMapping(value = "/opportunity/{opportunityId}/products/{productId}",
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> editOpportunityProduct(@ApiParam(value = "", required = true) @PathVariable("opportunityId") Long opportunityId, @ApiParam(value = "", required = true) @PathVariable("productId") Long productId, @ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "Edited opportunity product") @Valid @RequestBody OpportunityProductEditDTO opportunityProductEditDTO) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get opportunity products by id", nickname = "getOpportunityProductById", notes = "get opportunity product by id", response = OpportunityProductEditDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "opportunity product retrieved successfully", response = OpportunityProductEditDTO.class),
            @ApiResponse(code = 403, message = "current user has no access to this opportunity"),
            @ApiResponse(code = 404, message = "opportunity or product not found") })
    @RequestMapping(value = "/opportunity/{opportunityId}/products/{productId}",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<OpportunityProductEditDTO> getOpportunityProductById(@ApiParam(value = "opportunity id",required=true) @PathVariable("opportunityId") Long opportunityId,@ApiParam(value = "opportunity id",required=true) @PathVariable("productId") Long productId,@ApiParam(value = "" ,required=true) @RequestHeader(value="Logged-Account-Email", required=true) String loggedAccountEmail) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"quantity\" : 1,  \"sellPrice\" : 6.02745618307040320615897144307382404804229736328125,  \"id\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
