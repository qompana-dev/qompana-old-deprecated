package com.devqube.crmbusinessservice.productprice.category;

import com.devqube.crmbusinessservice.goal.GoalRepository;
import com.devqube.crmbusinessservice.productprice.category.web.*;
import com.devqube.crmbusinessservice.productprice.exception.ObjectUsedInGoalException;
import com.devqube.crmbusinessservice.productprice.util.ModelMapperUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductCategoryService {

    private final ProductCategoryRepository productCategoryRepository;
    private final ProductSubcategoryRepository productSubcategoryRepository;
    private final GoalRepository goalRepository;

    public ProductCategoryService(ProductCategoryRepository productCategoryRepository, ProductSubcategoryRepository productSubcategoryRepository, GoalRepository goalRepository) {
        this.productCategoryRepository = productCategoryRepository;
        this.productSubcategoryRepository = productSubcategoryRepository;
        this.goalRepository = goalRepository;
    }
    @Transactional
    public List<ProductCategory> saveCategories(List<ProductCategory> categories) {
        List<ProductCategory> entities = productCategoryRepository.saveAll(categories);
        addNoSubcategory(entities);
        return entities;
    }

    @Transactional(rollbackFor = {ObjectUsedInGoalException.class})
    public List<ProductCategory> saveCategoriesWithSubcategories(List<ProductCategoryWithSubcategorySaveDto> categories) throws ObjectUsedInGoalException {
        Map<Boolean, List<ProductCategoryWithSubcategorySaveDto>> partition = categories.stream()
                .collect(Collectors.partitioningBy(ProductCategoryWithSubcategorySaveDto::isToDelete));


        List<ProductCategorySaveDto> toDelete = ModelMapperUtils.mapAll(partition.get(true), ProductCategorySaveDto.class);
        removeCategories(toDelete);

        List<ProductCategory> categoriesToSave = ModelMapperUtils.mapAll(partition.get(false), ProductCategory.class);
        List<ProductCategory> savedCategories = saveCategories(categoriesToSave);

        List<ProductSubcategorySaveDto> subcategoriesDto = mapToSubcategoryDto(savedCategories, partition.get(false));
        saveSubcategories(subcategoriesDto);

        return findAll();
    }

    @Transactional(rollbackFor = {ObjectUsedInGoalException.class})
    public List<ProductCategory> updateCategories(List<ProductCategorySaveDto> categories) throws ObjectUsedInGoalException {
        Map<Boolean, List<ProductCategorySaveDto>> partition = categories.stream().collect(Collectors.partitioningBy(ProductCategorySaveDto::isToDelete));
        removeCategories(partition.get(true));
        saveCategoriesFromDto(partition.get(false));
        return findAll();
    }

    @Transactional(rollbackFor = {ObjectUsedInGoalException.class})
    public List<ProductCategoryWithSubcategoryDto> saveSubcategories(List<ProductSubcategorySaveDto> subcategories) throws ObjectUsedInGoalException {
        Map<Boolean, List<ProductSubcategorySaveDto>> partition = subcategories.stream().collect(Collectors.partitioningBy(ProductSubcategorySaveDto::isToDelete));
        List<Long> categoryIds = subcategories.stream().map(ProductSubcategorySaveDto::getCategoryId).distinct().collect(Collectors.toList());
        removeSubcategories(partition.get(true));
        saveSubcategoriesFromDto(partition.get(false));
        productCategoryRepository.flush();
        return findAllByIds(categoryIds);
    }

    public List<ProductCategory> findAll() {
        return productCategoryRepository.findAll();
    }

    public List<ProductCategoryWithSubcategoryDto> findAllByIds(List<Long> categoryIds) {
        List<ProductCategory> categories = productCategoryRepository.findByIdIn(categoryIds);
        return mapToProductWithSubcategoryDto(categories);
    }

    public List<ProductCategoryDto> findAllCategories() {
        List<ProductCategory> all = findAll();
        return all.stream().map(ProductCategoryDto::new).collect(Collectors.toList());
    }

    private List<ProductCategory> saveCategoriesFromDto(List<ProductCategorySaveDto> categories) {
        List<ProductCategory> entities = ModelMapperUtils.mapAll(categories, ProductCategory.class);
        return saveCategories(entities);
    }

    private void removeCategories(List<ProductCategorySaveDto> categories) throws ObjectUsedInGoalException {
        List<Long> idsToDelete = categories.stream().map(ProductCategorySaveDto::getId).collect(Collectors.toList());
        if (goalRepository.countBySubcategoriesProductCategoryIdIn(idsToDelete) > 0) {
            throw new ObjectUsedInGoalException();
        }
        productCategoryRepository.deleteByIdIn(idsToDelete);
        productCategoryRepository.flush();
    }

    private void addNoSubcategory(List<ProductCategory> categories) {
        categories.forEach(category -> {
            boolean initialExists = productSubcategoryRepository.existsByProductCategoryIdAndAndInitialTrue(category.getId());
            if (!initialExists) {
                ProductSubcategory subcategory = ProductSubcategory.builder().productCategory(category).initial(true).name("category.withoutCategory").build();
                productSubcategoryRepository.save(subcategory);
            }
        });
    }

    private void saveSubcategoriesFromDto(List<ProductSubcategorySaveDto> subcategories) {
        for (ProductSubcategorySaveDto subcategory : subcategories) {
            Optional<ProductCategory> byId = productCategoryRepository.findById(subcategory.getCategoryId());
            byId.ifPresent(category -> {
                ProductSubcategory entity = ModelMapperUtils.map(subcategory, ProductSubcategory.class);
                entity.setProductCategory(category);
                productSubcategoryRepository.save(entity);
            });
        }
    }

    private void removeSubcategories(List<ProductSubcategorySaveDto> subcategories) throws ObjectUsedInGoalException {
        List<Long> idsToDelete = subcategories.stream().map(ProductSubcategorySaveDto::getId).collect(Collectors.toList());
        if (goalRepository.countBySubcategoriesIdIn(idsToDelete) > 0) {
            throw new ObjectUsedInGoalException();
        }
        productSubcategoryRepository.deleteByIdInAndInitialFalse(idsToDelete);
        productCategoryRepository.flush();
    }

    private List<ProductSubcategorySaveDto> mapToSubcategoryDto(List<ProductCategory> savedCategories, List<ProductCategoryWithSubcategorySaveDto> categories) {
        categories.forEach(category -> category.getSubcategories().forEach(s -> s.setCategoryId(getCategoryId(savedCategories, category))));
        return categories.stream()
                .flatMap(category -> category.getSubcategories().stream())
                .map(s -> ModelMapperUtils.map(s, ProductSubcategorySaveDto.class))
                .collect(Collectors.toList());
    }

    private Long getCategoryId(List<ProductCategory> savedCategories, ProductCategoryWithSubcategorySaveDto category) {
        return savedCategories.stream().filter(c -> c.getName().equals(category.getName())).findFirst().map(ProductCategory::getId).orElse(null);
    }

    private List<ProductCategoryWithSubcategoryDto> mapToProductWithSubcategoryDto(List<ProductCategory> categories) {
        return categories.stream().map(ProductCategoryWithSubcategoryDto::new).collect(Collectors.toList());
    }

    public List<ProductCategoryWithSubcategoryDto> findAllCategoriesWithSubcategories() {
        List<ProductCategory> all = findAll();
        return all.stream().map(ProductCategoryWithSubcategoryDto::new).collect(Collectors.toList());
    }
}
