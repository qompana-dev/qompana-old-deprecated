package com.devqube.crmbusinessservice.productprice.productsimport.dto;

import com.devqube.crmshared.importexport.ImportObjLine;
import com.univocity.parsers.annotations.Parsed;
import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ProductDto extends ImportObjLine {

    @Parsed
    private String name;

    @Parsed
    private String code;

    @Parsed
    private String skuCode;

    @Parsed
    private String brand;

    @Parsed
    private Double purchasePrice;

    @Parsed
    private String purchasePriceCurrency;

    @Parsed
    private String currency;

    @Parsed
    private Double sellPriceNet;

    @Parsed
    private Integer vat;

    @Parsed
    private Boolean active = false;

    @Parsed
    private String description;

    @Parsed
    private String specification;

    @Parsed
    private String unit;
}
