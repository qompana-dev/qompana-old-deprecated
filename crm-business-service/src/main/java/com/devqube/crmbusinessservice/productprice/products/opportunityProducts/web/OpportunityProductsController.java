package com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web;

import com.devqube.crmbusinessservice.productprice.exception.ProductCodeOccupiedException;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.OpportunityProductsService;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductAddFromPriceBookDTO;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductEditDTO;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductSaveDTO;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OpportunityProductsController implements OpportunityProductsApi {
    private final OpportunityProductsService opportunityProductsService;

    public OpportunityProductsController(OpportunityProductsService opportunityProductsService) {
        this.opportunityProductsService = opportunityProductsService;
    }

    @Override
    @AuthController(name = "getOpportunityProducts")
    public ResponseEntity<Page> getOpportunityProducts(Long id, String loggedAccountEmail, Pageable pageable) {
        try {
            return new ResponseEntity<>(opportunityProductsService.getOpportunityProducts(id, loggedAccountEmail, pageable),
                    HttpStatus.OK);
        } catch (EntityNotFoundException | BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @AuthController(name = "addOpportunityProductManually", actionFrontendId = "OpportunityProductsCard.manualAdd")
    public ResponseEntity<Void> addOpportunityProductManually(Long id, String loggedAccountEmail, @Valid OpportunityProductSaveDTO opportunityProductSaveDTO) {
        try {
            opportunityProductsService.addOpportunityProductManually(id, loggedAccountEmail, opportunityProductSaveDTO);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (EntityNotFoundException | BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (ProductCodeOccupiedException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @Override
    @AuthController(name = "removeOpportunityProduct", actionFrontendId = "OpportunityProductsCard.delete")
    public ResponseEntity<Void> removeOpportunityProduct(Long opportunityId, Long productId, String loggedAccountEmail) {
        try {
            opportunityProductsService.removeOpportunityProduct(opportunityId, productId, loggedAccountEmail);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @AuthController(name = "editOpportunityProduct", actionFrontendId = "OpportunityProductsCard.edit")
    public ResponseEntity<Void> editOpportunityProduct(Long opportunityId, Long productId, String loggedAccountEmail, @Valid OpportunityProductEditDTO opportunityProductEditDTO) {
        try {
            opportunityProductsService.editOpportunityProduct(opportunityId, productId, loggedAccountEmail, opportunityProductEditDTO);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @AuthController(name = "getOpportunityProductById", actionFrontendId = "OpportunityProductsCard.edit")
    public ResponseEntity<OpportunityProductEditDTO> getOpportunityProductById(Long opportunityId, Long productId, String loggedAccountEmail) {
        try {
            return new ResponseEntity<>(opportunityProductsService.getOpportunityProductById(opportunityId, productId, loggedAccountEmail), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @AuthController(actionFrontendId = "OpportunityProductsCard.addFromPriceBook")
    public ResponseEntity<Void> addOpportunityProducts(Long id, String loggedAccountEmail, @Valid List<OpportunityProductAddFromPriceBookDTO> opportunityProductAddFromPriceBookDTO) {
        try {
            opportunityProductsService.addOpportunityProducts(id, loggedAccountEmail, opportunityProductAddFromPriceBookDTO);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (EntityNotFoundException | BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }
}
