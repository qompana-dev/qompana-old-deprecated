package com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@AuthFilterEnabled
public class OpportunityProductDTO {
  @AuthField(controller = "getOpportunityProducts")
  private Long id;
  @AuthField(controller = "getOpportunityProducts", frontendId = "OpportunityProductsCard.product.name")
  private String name;
  @AuthField(controller = "getOpportunityProducts", frontendId = "OpportunityProductsCard.category")
  private String category;
  @AuthField(controller = "getOpportunityProducts", frontendId = "OpportunityProductsCard.subcategory")
  private String subcategory;
  @AuthField(controller = "getOpportunityProducts", frontendId = "OpportunityProductsCard.product.code")
  private String code;
  @AuthField(controller = "getOpportunityProducts", frontendId = "OpportunityProductsCard.price")
  private Double price;
  @AuthField(controller = "getOpportunityProducts")
  private Boolean priceChanged;
  @AuthField(controller = "getOpportunityProducts")
  private Double priceChangedFrom;
  @AuthField(controller = "getOpportunityProducts")
  private Double priceChangedTo;
  @AuthField(controller = "getOpportunityProducts")
  private String currency;
  @AuthField(controller = "getOpportunityProducts")
  private String unit;
  @AuthField(controller = "getOpportunityProducts", frontendId = "OpportunityProductsCard.quantity")
  private Long quantity;
  @AuthField(controller = "getOpportunityProducts", frontendId = "OpportunityProductsCard.priceSummed")
  private Double priceSummed;
}

