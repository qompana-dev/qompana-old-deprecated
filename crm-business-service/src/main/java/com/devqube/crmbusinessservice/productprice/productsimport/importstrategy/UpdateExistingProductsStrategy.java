package com.devqube.crmbusinessservice.productprice.productsimport.importstrategy;

import com.devqube.crmbusinessservice.productprice.products.ProductService;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UpdateExistingProductsStrategy implements ImportStrategy {

    private final ProductService productService;

    public UpdateExistingProductsStrategy(ProductService productService) {
        this.productService = productService;
    }


    @Override
    @Transactional
    public void save(List<Product> products) {
        Map<String, Product> newProducts = products.stream().collect(Collectors.toMap(Product::getCode, product -> product, (p1, p2) -> p2));
        Set<String> newProductsIds = products.stream().map(Product::getCode).collect(Collectors.toSet());
        List<Product> existingProducts = productService.findAllByProductCodeIn(newProductsIds);
        updateExistingProducts(existingProducts, newProducts);
        productService.saveAll(existingProducts);
    }

    private void updateExistingProducts(List<Product> existingProducts, Map<String, Product> productsMap) {
        existingProducts.forEach(
                product -> updateTargetWithSource(product, productsMap.get(product.getCode()))
        );
    }

    private void updateTargetWithSource(Product target, Product source) {
        target.setName(source.getName());
        target.setCode(source.getCode());
        target.setSkuCode(source.getSkuCode());
        target.setManufacturer(source.getManufacturer());
        target.setSupplier(source.getSupplier());
        target.setBrand(source.getBrand());
        target.setPurchasePrice(source.getPurchasePrice());
        target.setPurchasePriceCurrency(source.getPurchasePriceCurrency());
        target.setCurrency(source.getCurrency());
        target.setSellPriceNet(source.getSellPriceNet());
        target.setVat(source.getVat());
        target.setActive(source.getActive());
        target.setDescription(source.getDescription());
        target.setSpecification(source.getSpecification());
    }
}
