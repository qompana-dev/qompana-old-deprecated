package com.devqube.crmbusinessservice.productprice.productsimport;


import java.util.ArrayList;
import java.util.List;

public class ParseException extends Exception {
    private List<ParseExceptionDetailDto> errorList = new ArrayList<>();

    public ParseException(List<ParseExceptionDetailDto> errorList) {
        super();
        this.errorList = errorList;
    }

    public ParseException(String message, List<ParseExceptionDetailDto> errorList) {
        super(message);
        this.errorList = errorList;
    }

    public List<ParseExceptionDetailDto> getErrorList() {
        return errorList;
    }
}
