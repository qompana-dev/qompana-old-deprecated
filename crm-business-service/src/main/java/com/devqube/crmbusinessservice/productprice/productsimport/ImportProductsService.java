package com.devqube.crmbusinessservice.productprice.productsimport;

import com.devqube.crmbusinessservice.dictionary.DictionaryService;
import com.devqube.crmbusinessservice.dictionary.Type;
import com.devqube.crmbusinessservice.dictionary.dto.DictionaryDTO;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmbusinessservice.productprice.productsimport.dto.ProductDto;
import com.devqube.crmbusinessservice.productprice.productsimport.exception.SkuCodeOrCodeConflictException;
import com.devqube.crmbusinessservice.productprice.productsimport.importstrategy.ImportStrategy;
import com.devqube.crmbusinessservice.productprice.productsimport.importstrategy.ProductsImportStrategy;
import com.devqube.crmshared.exception.BadRequestException;
import com.univocity.parsers.common.DataProcessingException;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ImportProductsService {

    private final CsvToProductService csvToProductService;
    private final DictionaryService dictionaryService;
    private final ProductParseExceptionService productParseExceptionService;
    private final Map<String, ImportStrategy> importStrategyMap;

    public ImportProductsService(CsvToProductService csvToProductService,
                                 DictionaryService dictionaryService,
                                 ProductParseExceptionService productParseExceptionService,
                                 Map<String, ImportStrategy> importStrategyMap) {
        this.csvToProductService = csvToProductService;
        this.dictionaryService = dictionaryService;
        this.productParseExceptionService = productParseExceptionService;
        this.importStrategyMap = importStrategyMap;
    }

    public void saveProductFromCsv(InputStream csvFile, ProductsImportStrategy strategy, Boolean headerPresent) throws SkuCodeOrCodeConflictException, BadRequestException, ParseException {
        List<DataProcessingException> errors = new ArrayList<>();
        List<ProductDto> productDtoList = csvToProductService.convertToProducts(csvFile, headerPresent, errors);

        List<DictionaryWord> units = getUnitsMap();

        productParseExceptionService.throwParseExceptionIfIncorrect(errors, productDtoList, units);
        List<Product> products = ProductFactory.convert(productDtoList, units);

        ImportStrategy importStrategy = importStrategyMap.get(strategy.getName());
        try {
            importStrategy.save(products);
        } catch (Exception e) {
            throw new SkuCodeOrCodeConflictException();
        }
    }

    private List<DictionaryWord> getUnitsMap() {
        DictionaryDTO dictionaryDTO = dictionaryService.findAdminDictionary(Type.UNIT.getDictionaryId());
        return dictionaryDTO.getWords().stream()
                .map(wordDTO -> new DictionaryWord(wordDTO.getId(), wordDTO.getName()))
                .collect(Collectors.toList());
    }
}
