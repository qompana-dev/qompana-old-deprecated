package com.devqube.crmbusinessservice.productprice.category;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ProductCategory {

    @Id
    @SequenceGenerator(name = "product_category_seq", sequenceName = "product_category_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_category_seq")
    private Long id;

    private String name;

    @OneToMany(mappedBy = "productCategory", orphanRemoval = true)
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private Set<ProductSubcategory> subcategories = new HashSet<>();

}
