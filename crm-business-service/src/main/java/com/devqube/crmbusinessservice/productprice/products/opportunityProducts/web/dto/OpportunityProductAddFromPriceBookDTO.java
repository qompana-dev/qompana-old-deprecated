package com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.OpportunityProduct;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OpportunityProductAddFromPriceBookDTO {
    private Long id;
    private Double sellPrice;
    private Long quantity;

    public OpportunityProduct toModel(Opportunity opportunity, Product product) {
        OpportunityProduct opportunityProduct = new OpportunityProduct();
        opportunityProduct.setOpportunity(opportunity);
        opportunityProduct.setProduct(product);
        opportunityProduct.setQuantity(quantity);
        opportunityProduct.setPrice(sellPrice);
        opportunityProduct.setPriceSummed(sellPrice * quantity);
        opportunityProduct.setCurrentSellPriceNet(product.getSellPriceNet());
        return opportunityProduct;
    }
}
