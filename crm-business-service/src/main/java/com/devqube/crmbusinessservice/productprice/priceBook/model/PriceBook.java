package com.devqube.crmbusinessservice.productprice.priceBook.model;

import com.devqube.crmbusinessservice.productprice.productPrice.model.ProductPrice;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AuthFilterEnabled
public class PriceBook {

    @Id
    @SequenceGenerator(name = "price_book_seq", sequenceName = "price_book_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "price_book_seq")
    @EqualsAndHashCode.Include
    @AuthFields(list = {
            @AuthField(controller = "getPriceBooks"),
            @AuthField(controller = "getPriceBook"),
            @AuthField(controller = "savePriceBook"),
            @AuthField(controller = "editPriceBook")
    })
    private Long id;

    @EqualsAndHashCode.Include
    @Column(name = "name", unique = true, nullable = false)
    @AuthFields(list = {
            @AuthField(frontendId = "PriceBookListComponent.name", controller = "getPriceBooks"),
            @AuthField(frontendId = "PriceBookEditComponent.name", controller = "getPriceBook"),
            @AuthField(frontendId = "PriceBookEditComponent.name", controller = "editPriceBook"),
            @AuthField(frontendId = "PriceBookAddComponent.name", controller = "savePriceBook")
    })
    private String name;

    @AuthFields(list = {
            @AuthField(frontendId = "PriceBookListComponent.description", controller = "getPriceBooks"),
            @AuthField(frontendId = "PriceBookEditComponent.description", controller = "getPriceBook"),
            @AuthField(frontendId = "PriceBookEditComponent.description", controller = "editPriceBook"),
            @AuthField(frontendId = "PriceBookAddComponent.description", controller = "savePriceBook")
    })
    private String description;

    @AuthFields(list = {
            @AuthField(frontendId = "PriceBookListComponent.currency", controller = "getPriceBooks"),
            @AuthField(frontendId = "PriceBookEditComponent.currency", controller = "getPriceBook"),
            @AuthField(frontendId = "PriceBookEditComponent.currency", controller = "editPriceBook"),
            @AuthField(frontendId = "PriceBookAddComponent.currency", controller = "savePriceBook")
    })
    private String currency;

    @AuthFields(list = {
            @AuthField(frontendId = "PriceBookListComponent.active", controller = "getPriceBooks"),
            @AuthField(frontendId = "PriceBookEditComponent.active", controller = "getPriceBook"),
            @AuthField(frontendId = "PriceBookEditComponent.active", controller = "editPriceBook"),
            @AuthField(controller = "savePriceBook")
    })
    private Boolean active = false;

    @AuthFields(list = {
            @AuthField(controller = "getPriceBooks"),
            @AuthField(controller = "savePriceBook")
    })
    private Boolean isDefault = false;

    @AuthFields(list = {
            @AuthField(controller = "savePriceBook")
    })
    private Boolean deleted = false;

    @CreationTimestamp
    @JsonIgnore
    private LocalDateTime created;

    @UpdateTimestamp
    @AuthFields(list = {
            @AuthField(frontendId = "PriceBookListComponent.updated", controller = "getPriceBooks")
    })
    private LocalDateTime updated;

    @OneToMany(mappedBy = "priceBook")
    @JsonIgnore
    private List<ProductPrice> prices;

    @AuthField(controller = "getPriceBook")
    @Transient
    private Boolean empty;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "priceBook")
    @ToString.Exclude
    @JsonIgnore
    @AuthFields(list = {
            @AuthField(controller = "savePriceBook"),
            @AuthField(controller = "editPriceBook")
    })
    private Set<LastViewedPriceBook> lastViewedPriceBooks;
}
