package com.devqube.crmbusinessservice.productprice.products.opportunityProducts;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class OpportunityProduct {
    @Id
    @SequenceGenerator(name = "opportunity_product_seq", sequenceName = "opportunity_product_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "opportunity_product_seq")
    @EqualsAndHashCode.Include
    private Long id;

    @ManyToOne
    @JoinColumn(name = "opportunity_id")
    @EqualsAndHashCode.Include
    private Opportunity opportunity;

    @ManyToOne
    @JoinColumn(name = "product_id")
    @EqualsAndHashCode.Include
    private Product product;

    private Double price;

    private Long quantity;

    private Double priceSummed;

    private Double currentSellPriceNet;
}
