package com.devqube.crmbusinessservice.goal.web.dto;

import com.devqube.crmbusinessservice.goal.model.Goal;
import com.devqube.crmbusinessservice.goal.model.GoalIntervalType;
import com.devqube.crmbusinessservice.goal.model.GoalType;
import com.devqube.crmbusinessservice.goal.model.GoalTypeCategory;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class GoalListDto {
    @AuthFields(list = {
            @AuthField(controller = "getAllCreatedByMe"),
            @AuthField(controller = "getAllAssignedToMe")
    })
    private Long id;

    @AuthFields(list = {
            @AuthField(controller = "getAllCreatedByMe", frontendId = "GoalTableComponent.name"),
            @AuthField(controller = "getAllAssignedToMe", frontendId = "GoalTableComponent.name")
    })
    private String name;

    @AuthFields(list = {
            @AuthField(controller = "getAllCreatedByMe", frontendId = "GoalTableComponent.category"),
            @AuthField(controller = "getAllAssignedToMe", frontendId = "GoalTableComponent.category")
    })
    private GoalTypeCategory category;

    @AuthFields(list = {
            @AuthField(controller = "getAllCreatedByMe", frontendId = "GoalTableComponent.type"),
            @AuthField(controller = "getAllAssignedToMe", frontendId = "GoalTableComponent.type")
    })
    private GoalType type;

    @AuthFields(list = {
            @AuthField(controller = "getAllCreatedByMe", frontendId = "GoalTableComponent.interval"),
            @AuthField(controller = "getAllAssignedToMe", frontendId = "GoalTableComponent.interval")
    })
    private GoalIntervalType interval;

    @AuthFields(list = {
            @AuthField(controller = "getAllCreatedByMe", frontendId = "GoalTableComponent.intervalNumber"),
            @AuthField(controller = "getAllAssignedToMe", frontendId = "GoalTableComponent.intervalNumber")
    })
    private Integer intervalNumber;

    @AuthFields(list = {
            @AuthField(controller = "getAllCreatedByMe", frontendId = "GoalTableComponent.startIndex"),
            @AuthField(controller = "getAllAssignedToMe", frontendId = "GoalTableComponent.startIndex")
    })
    private Integer startIndex;

    @AuthFields(list = {
            @AuthField(controller = "getAllCreatedByMe", frontendId = "GoalTableComponent.startYear"),
            @AuthField(controller = "getAllAssignedToMe", frontendId = "GoalTableComponent.startYear")
    })
    private Integer startYear;

    @AuthFields(list = {
            @AuthField(controller = "getAllCreatedByMe", frontendId = "GoalTableComponent.goalCompleted"),
            @AuthField(controller = "getAllAssignedToMe", frontendId = "GoalTableComponent.goalCompleted")
    })
    private Long goalCompleted;

    @AuthFields(list = {
            @AuthField(controller = "getAllCreatedByMe", frontendId = "GoalTableComponent.deficitOrExcess"),
            @AuthField(controller = "getAllAssignedToMe", frontendId = "GoalTableComponent.deficitOrExcess")
    })
    private Long deficitOrExcess;

    public static GoalListDto fromModel(Goal goal, Map<Long, Double> completedValueMap) {
        GoalListDto result = fromModel(goal);
        Double completedValue = completedValueMap.get(result.getId());
        if (completedValue != null) {
            result.setGoalCompleted(Math.round(completedValue));
            result.setDeficitOrExcess(result.getGoalCompleted() - 100);
        }
        return result;
    }

    public static GoalListDto fromModel(Goal goal) {
        GoalListDto dto = new GoalListDto();
        dto.setId(goal.getId());
        dto.setName(goal.getName());
        if (goal.getType() != null) {
            dto.setCategory(goal.getType().getCategory());
        }
        dto.setType(goal.getType());
        dto.setInterval(goal.getInterval());
        dto.setIntervalNumber(goal.getIntervalNumber());
        dto.setStartIndex(goal.getStartIndex());
        dto.setStartYear(goal.getStartYear());
        return dto;
    }
}
