package com.devqube.crmbusinessservice.goal;

import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.task.ActivityTypeEnum;
import com.devqube.crmshared.task.TaskSummaryDto;
import com.devqube.crmshared.task.TaskTimeCategoryEnum;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;
import java.util.Set;

@FeignClient(name = "crm-task-service", url = "${crm-task-service.url}")
public interface TaskClient {

    @RequestMapping(value = "/internal/tasks/activity/user/{userIds}/count",
            consumes = {"application/json"}, method = RequestMethod.GET)
    Long getActivityCountForGoal(@PathVariable Set<Long> userIds, @RequestParam(value = "onlyPhone") Boolean onlyPhone,
                                 @RequestParam(value = "from") String from, @RequestParam(value = "to") String to);

    @RequestMapping(value = "/internal/tasks/{activityIds}/activity/types",
            produces = {"application/json"},
            method = RequestMethod.GET)
    Map<Long, ActivityTypeEnum> getActivityTypes(@PathVariable List<Long> activityIds);

    @RequestMapping(value = "/internal/tasks/activity/associated-with/type",
            produces = {"application/json"},
            method = RequestMethod.GET)
    Map<Long, TaskSummaryDto> getAllTaskSummary(@RequestParam(value = "type") CrmObjectType type,
                                                @RequestParam(value = "sourceIds") List<Long> sourceIds,
                                                @RequestParam(value = "timeCategory") List<TaskTimeCategoryEnum> timeCategory);


}