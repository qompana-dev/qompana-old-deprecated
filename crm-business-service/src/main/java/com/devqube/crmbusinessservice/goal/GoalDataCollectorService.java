package com.devqube.crmbusinessservice.goal;

import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.goal.model.Goal;
import com.devqube.crmbusinessservice.goal.model.UserGoal;
import com.devqube.crmbusinessservice.goal.web.dto.GoalIntervalDto;
import com.devqube.crmbusinessservice.goal.web.dto.IntervalRangeDto;
import com.devqube.crmbusinessservice.goal.web.dto.UserGoalDto;
import com.devqube.crmbusinessservice.goal.web.dto.UserPercentageDto;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.offer.OfferRepository;
import com.devqube.crmbusinessservice.productprice.category.ProductSubcategory;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

@Slf4j
@Service
public class GoalDataCollectorService {

    private final ModelMapper modelMapper;
    private final CustomerRepository customerRepository;
    private final ContactRepository contactRepository;
    private final TaskClient taskClient;
    private final LeadRepository leadRepository;
    private final OpportunityRepository opportunityRepository;
    private final OfferRepository offerRepository;


    public GoalDataCollectorService(ModelMapper modelMapper, CustomerRepository customerRepository, ContactRepository contactRepository,
                                    TaskClient taskClient, LeadRepository leadRepository, OpportunityRepository opportunityRepository,
                                    OfferRepository offerRepository) {
        this.modelMapper = modelMapper;
        this.customerRepository = customerRepository;
        this.contactRepository = contactRepository;
        this.taskClient = taskClient;
        this.leadRepository = leadRepository;
        this.opportunityRepository = opportunityRepository;
        this.offerRepository = offerRepository;
    }

    public Map<Long, Double> getGoalCompletedValues(List<Goal> goalList, Long userId) {
        Map<Long, Double> result = new HashMap<>();
        for(Goal goal : goalList) {
            try {
                result.put(goal.getId(), getGoalCompletedValue(goal, userId));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private Double getGoalCompletedValue(Goal goal, Long userId) {
        Set<UserGoalDto> userGoals = getUserGoals(goal, userId);
        List<GoalIntervalDto> intervals = userGoals.stream().map(UserGoalDto::getIntervals)
                .flatMap(Collection::stream).collect(Collectors.toList());
        Double expectedValue = intervals.stream().map(GoalIntervalDto::getExpectedValue).mapToDouble(c -> c).sum();
        Double currentValue = intervals.stream().map(GoalIntervalDto::getCurrentValue).mapToDouble(c -> c).sum();

        return (currentValue.equals(0.0) || expectedValue.equals(0.0)) ? 0.0 : (currentValue / expectedValue) * 100;
    }

    public Set<UserGoalDto> getUserGoals(Goal goal, Long userId) {
        Map<Integer, IntervalRangeDto> intervalRange = getIntervalRange(goal);
        Set<Long> userIds = getUserIdWithChild(userId, goal);
        return goal.getUsersGoals().stream()
                .filter(c -> c.getParentUserId().equals(userId))
                .map(c -> modelMapper.map(c, UserGoalDto.class))
                .map(c -> getResult(goal, c, intervalRange, userIds))
                .collect(Collectors.toSet());
    }

    private Set<Long> getUserIdWithChild(Long userId, Goal goal) {
        Set<Long> childIds = goal.getUsersGoals().stream()
                .filter(c -> c.getParentUserId().equals(userId))
                .map(UserGoal::getUserId).collect(Collectors.toSet());

        childIds.addAll(childIds.stream().filter(c -> !c.equals(userId)).map(c -> getUserIdWithChild(c, goal))
                .flatMap(Collection::stream).collect(Collectors.toSet()));

        return childIds;
    }

    private UserGoalDto getResult(Goal goal, UserGoalDto userGoal, Map<Integer, IntervalRangeDto> intervalRangeMap, Set<Long> userIds) {
        userGoal.getIntervals().forEach(interval -> {
            IntervalRangeDto intervalRangeDto = intervalRangeMap.get(interval.getIndex().intValue());
            if (intervalRangeDto != null) {
                interval.setCurrentValue(getCurrentValueForType(intervalRangeDto.getFrom(), intervalRangeDto.getTo(), goal, userGoal, userIds));
            }
        });

        userGoal.getIntervals().forEach(dto -> {
            if (dto.getCurrentValue() != null && dto.getExpectedValue() != null) {
                dto.setResult(dto.getCurrentValue() - dto.getExpectedValue());
            }
        });

        return userGoal;
    }

    private Double getCurrentValueForType(LocalDateTime from, LocalDateTime to, Goal goal, UserGoalDto userGoal, Set<Long> userIds) {
        switch (goal.getType()) {
            // CUSTOMER
            case ADDED_CUSTOMERS_NUMBER:
                return customerRepository.findForGoal(from, to, userIds).doubleValue();
            case ADDED_CONTACTS_NUMBER:
                return contactRepository.findForGoal(from, to, userIds).doubleValue();
            // CALENDAR
            case PHONE_CALLS_NUMBER:
                return taskClient.getActivityCountForGoal(userIds, true, formatDate(from), formatDate(to)).doubleValue();
            case CALENDAR_TASKS_NUMBER:
                return taskClient.getActivityCountForGoal(userIds, false, formatDate(from), formatDate(to)).doubleValue();
            // LEAD
            case CREATED_LEADS_NUMBER:
                return leadRepository.findCreatedLeadsForGoal(from, to, userIds).doubleValue();
            case CONVERTED_LEADS_NUMBER:
                return leadRepository.findConvertedLeadsForGoal(from, to, userIds).doubleValue();
            // OPPORTUNITY
            case CREATED_OPPORTUNITIES_NUMBER:
                return opportunityRepository.findCreatedOpportunityForGoal(from, to, userIds).doubleValue();
            case WON_OPPORTUNITIES_NUMBER:
                return getWonOpportunities(from, to, userIds, null)
                        .map(UserPercentageDto::getParticipation).mapToDouble(c -> c).sum();
            case WON_OPPORTUNITIES_AMOUNT:
                return getWonOpportunities(from, to, userIds, goal.getCurrency())
                        .map(UserPercentageDto::getAmount).mapToDouble(c -> c).sum();
            case WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT:
                List<Long> productIds = goal.getProducts().stream().map(Product::getId).collect(Collectors.toList());
                return getWonOpportunitiesSumForProductIds(from, to, goal, userGoal, productIds, userIds);
            case OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT:
                return getWonOpportunitiesSumForProductIds(from, to, goal, userGoal, getProductIdsFromGroup(goal), userIds);
            // OFFER
            case GENERATED_OFFERS_NUMBER:
                return offerRepository.findCreatedOffersNumberForGoal(from, to, userIds).doubleValue();
            case CUSTOMERS_WHO_RECEIVED_OFFER:
                return offerRepository.findCustomerWhoReceivedOfferNumberForGoal(from, to, userIds).doubleValue();
            default:
                return 0.0;
        }
    }

    private Double getWonOpportunitiesSumForProductIds(LocalDateTime from, LocalDateTime to, Goal goal, UserGoalDto userGoal, List<Long> productIds, Set<Long> userIds) {
        return getWonOpportunities(from, to, userIds, goal.getCurrency())
                .map(c -> getOpportunityProductsSum(c, productIds))
                .mapToDouble(d -> d).sum();
    }

    private List<Long> getProductIdsFromGroup(Goal goal) {
        List<Long> result = new ArrayList<>();
        for (ProductSubcategory subcategory : goal.getSubcategories()) {
            subcategory.getProducts().stream()
                    .filter(d -> d.getCurrency().equals(goal.getCurrency()))
                    .forEach(d -> result.add(d.getId()));
        }
        return result;
    }

    private Double getOpportunityProductsSum(UserPercentageDto userPercentageDto, List<Long> productIds) {
        return userPercentageDto.getOpportunity()
                .getOpportunityProducts().stream()
                .filter(d -> productIds.contains(d.getProduct().getId()))
                .map(d -> d.getPriceSummed() * userPercentageDto.getParticipation())
                .mapToDouble(d -> d).sum();
    }

    private Stream<UserPercentageDto> getWonOpportunities(LocalDateTime from, LocalDateTime to, Set<Long> userIds, String currency) {
        return opportunityRepository.findAllWonOpportunitiesForGoal(from, to, userIds, currency)
                .stream().map(c -> getUserPercentageList(c, userIds))
                .flatMap(Collection::stream)
                .filter(Objects::nonNull);
    }

    private Set<UserPercentageDto> getUserPercentageList(Opportunity opportunity, Set<Long> userIds) {
        return userIds.stream().map(c -> getUserPercentage(opportunity, c)).collect(Collectors.toSet());
    }

    private UserPercentageDto getUserPercentage(Opportunity opportunity, Long userId) {
        Long percentage = 0L;
        if (userId.equals(opportunity.getOwnerOneId())) {
            percentage = opportunity.getOwnerOnePercentage();
        } else if (userId.equals(opportunity.getOwnerTwoId())) {
            percentage = opportunity.getOwnerTwoPercentage();
        } else if (userId.equals(opportunity.getOwnerThreeId())) {
            percentage = opportunity.getOwnerThreePercentage();
        } else {
            return null;
        }
        return new UserPercentageDto(userId, percentage, opportunity);
    }

    public Map<Integer, IntervalRangeDto> getIntervalRange(Goal goal) {
        List<IntervalRangeDto> intervalRangeList = new ArrayList<>();

        switch (goal.getInterval()) {
            case YEAR:
                intervalRangeList.add(new IntervalRangeDto(0, getStart(goal.getStartYear(), 1), getEnd(goal.getStartYear(), 12)));
                break;
            case QUARTER:
                for (int i = goal.getStartIndex() - 1, j = 0; i < 4; i++, j++) {
                    intervalRangeList.add(new IntervalRangeDto(j, getStart(goal.getStartYear(), (i * 3) + 1), getEnd(goal.getStartYear(), ((i + 1) * 3))));
                }
                break;
            case MONTH:
                for (int i = goal.getStartIndex(), j = 0; i <= 12; i++, j++) {
                    intervalRangeList.add(new IntervalRangeDto(j, getStart(goal.getStartYear(), i), getEnd(goal.getStartYear(), i)));
                }
                break;
        }
        return intervalRangeList.stream().collect(Collectors.toMap(IntervalRangeDto::getIndex, c -> c));
    }

    private LocalDateTime getStart(int year, int month) {
        return LocalDateTime.of(year, month, 1, 0, 0, 0);
    }

    private LocalDateTime getEnd(int year, int month) {
        LocalDate ld = LocalDate.of(year, month, 1);
        int lastDay = ld.withDayOfMonth(ld.lengthOfMonth()).getDayOfMonth();
        return LocalDateTime.of(year, month, lastDay, 23, 59, 59);
    }

    private String formatDate(LocalDateTime date) {
        return date.format(ISO_DATE_TIME);
    }
}
