package com.devqube.crmbusinessservice.goal;

import com.devqube.crmbusinessservice.goal.model.Goal;
import com.devqube.crmbusinessservice.goal.model.GoalType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface GoalRepository extends JpaRepository<Goal, Long> {
    Page<Goal> findAllByOwnerIdAndDeletedFalse(Long ownerId, Pageable pageable);

    Set<Goal> findAllByOwnerIdAndDeletedFalseAndType(Long ownerId, GoalType type);

    @Query("select g from Goal g inner join g.usersGoals ug where ug.userId = :ownerId and ug.parentUserId = :ownerId and g.deleted = false")
    Page<Goal> findAllAssignedToUser(@Param("ownerId") Long ownerId, Pageable pageable);

    @Query("select g from Goal g inner join g.usersGoals ug where ug.userId = :ownerId and ug.parentUserId = :ownerId and g.deleted = false and g.type = :type")
    Set<Goal> findAllAssignedToUserAndType(@Param("ownerId") Long ownerId, @Param("type") GoalType type);

    Long countByProductsId(Long productId);

    Long countBySubcategoriesIdIn(List<Long> subcategoriesId);

    Long countBySubcategoriesProductCategoryIdIn(List<Long> subcategoriesProductCategoryId);
}
