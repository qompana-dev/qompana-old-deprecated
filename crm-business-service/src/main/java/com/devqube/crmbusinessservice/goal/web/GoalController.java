package com.devqube.crmbusinessservice.goal.web;

import com.devqube.crmbusinessservice.goal.GoalService;
import com.devqube.crmbusinessservice.goal.model.GoalType;
import com.devqube.crmbusinessservice.goal.web.dto.*;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/goals")
@Slf4j
public class GoalController implements GoalApi {

    private final GoalService goalService;

    public GoalController(GoalService goalService) {
        this.goalService = goalService;
    }

    @Override
    @PostMapping
    @AuthController(actionFrontendId = "GoalTableComponent.addGoal")
    public ResponseEntity<GoalSaveDto> save(@RequestBody GoalSaveDto goalSaveDto, @RequestHeader("Logged-Account-Email") String email) throws BadRequestException {
        GoalSaveDto goal = goalService.createGoal(goalSaveDto, email);
        return new ResponseEntity<>(goal, HttpStatus.CREATED);
    }

    @Override
    @GetMapping("/created-by-me")
    @AuthController(name = "getAllCreatedByMe")
    public ResponseEntity<Page<GoalListDto>> getAllCreatedByMe(Pageable pageable) {
        try {
            return new ResponseEntity<>(goalService.getGoalsCreatedByMe(pageable), HttpStatus.OK);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @GetMapping("/assigned-to-me")
    @AuthController(name = "getAllAssignedToMe")
    public ResponseEntity<Page<GoalListDto>> getAllAssignedToMe(Pageable pageable) {
        try {
            return new ResponseEntity<>(goalService.getGoalsAssignedToMe(pageable), HttpStatus.OK);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @GetMapping("/type/{type}/widget-preview")
    public ResponseEntity<List<GoalWidgetPreviewDto>> getAllForMeByType(@RequestHeader("Logged-Account-Email") String email, @PathVariable("type") GoalType type) {
        try {
            return new ResponseEntity<>(goalService.getAllForMeByType(email, type), HttpStatus.OK);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @DeleteMapping("/{id}")
    @AuthController(name = "deleteGoalById", actionFrontendId = "GoalTableComponent.deleteGoal")
    public ResponseEntity<Void> deleteById(@PathVariable("id") Long id) {
        try {
            goalService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (BadRequestException | PermissionDeniedException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @GetMapping("/for-edit/{id}")
    @AuthController(name = "getGoalForEdit", actionFrontendId = "GoalTableComponent.editGoal")
    public ResponseEntity<GoalEditDto> getGoalForEdit(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<>(goalService.getGoalForEdit(id), HttpStatus.OK);
        } catch (EntityNotFoundException | BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @GetMapping("/{id}/user-goals")
    public ResponseEntity<List<UserGoalDto>> getUserGoalsForWidgetCreator(@PathVariable("id") Long id) throws BadRequestException, EntityNotFoundException {
        return new ResponseEntity<>(goalService.getUserGoalsDto(id), HttpStatus.OK);
    }

    @Override
    @GetMapping("/{id}/assigned-to-me")
    public ResponseEntity<UserGoalDto> getUserGoalAssignedToMe(@PathVariable("id") Long id, @RequestHeader("Logged-Account-Email") String email) {
        try {
            return new ResponseEntity<>(goalService.getUserGoalAssignedToMe(id, email), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @PutMapping("/{id}")
    @AuthController(name = "updateGoal", actionFrontendId = "GoalTableComponent.editGoal")
    public ResponseEntity<GoalEditDto> update(@PathVariable("id") Long id, @RequestBody GoalEditDto dto, @RequestHeader("Logged-Account-Email") String email) throws BadRequestException {
        try {
            return new ResponseEntity<>(goalService.updateGoal(id, dto, email), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
