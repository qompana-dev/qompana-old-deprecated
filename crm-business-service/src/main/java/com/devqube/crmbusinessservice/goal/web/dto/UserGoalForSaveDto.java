package com.devqube.crmbusinessservice.goal.web.dto;

import lombok.Data;

import java.util.Set;

@Data
public class UserGoalForSaveDto {
    private Long userId;
    private Set<GoalIntervalForSaveDto> intervals;
}
