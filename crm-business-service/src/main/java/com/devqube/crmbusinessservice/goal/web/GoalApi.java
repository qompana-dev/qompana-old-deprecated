package com.devqube.crmbusinessservice.goal.web;

import com.devqube.crmbusinessservice.goal.model.GoalType;
import com.devqube.crmbusinessservice.goal.web.dto.*;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

public interface GoalApi {

    @ApiOperation(value = "Create new goal")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Goal created successfully", response = GoalDto.class),
            @ApiResponse(code = 400, message = "validation error")
    })
    ResponseEntity<GoalSaveDto> save(GoalSaveDto goalSaveDto, String email) throws BadRequestException;

    @ApiOperation(value = "Get page of goals created by me")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Goals retrieved successfully", response = GoalListDto.class)
    })
    ResponseEntity<Page<GoalListDto>> getAllCreatedByMe(Pageable pageable);

    @ApiOperation(value = "Get page of goals which are assigned to me")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Goals retrieved successfully", response = GoalListDto.class)
    })
    ResponseEntity<Page<GoalListDto>> getAllAssignedToMe(Pageable pageable);

    @ApiOperation(value = "Get goals for me (for widget preview)")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Goals retrieved successfully", response = GoalWidgetPreviewDto.class)
    })
    ResponseEntity<List<GoalWidgetPreviewDto>> getAllForMeByType(String loggedAccountEmail, GoalType type);

    @ApiOperation(value = "Delete goal")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Goal deleted successfully"),
            @ApiResponse(code = 401, message = "Not owner trying to delete goal")
    })
    ResponseEntity<Void> deleteById(Long id);

    @ApiOperation(value = "get goal for edit by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Goal retrieved successfully"),
            @ApiResponse(code = 404, message = "Goal not found")
    })
    ResponseEntity<GoalEditDto> getGoalForEdit(Long id);

    @ApiOperation(value = "get user goals for widget creator")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User goals retrieved successfully"),
            @ApiResponse(code = 404, message = "Goal not found")
    })
    ResponseEntity<List<UserGoalDto>> getUserGoalsForWidgetCreator(Long id) throws BadRequestException, EntityNotFoundException;

    @ApiOperation(value = "get userGoal assigned to user by goal id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User goal retrieved successfully"),
            @ApiResponse(code = 404, message = "User goal not found")
    })
    ResponseEntity<UserGoalDto> getUserGoalAssignedToMe(@PathVariable("id") Long id, @RequestHeader("Logged-Account-Email") String email);

    @ApiOperation(value = "Update goal")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Goal updated successfully", response = GoalEditDto.class),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 404, message = "goal not found")
    })
    ResponseEntity<GoalEditDto> update(Long id, GoalEditDto dto, String email) throws BadRequestException;
}
