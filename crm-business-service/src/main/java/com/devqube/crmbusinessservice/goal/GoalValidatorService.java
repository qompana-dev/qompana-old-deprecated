package com.devqube.crmbusinessservice.goal;

import com.devqube.crmbusinessservice.goal.model.*;
import com.devqube.crmshared.exception.BadRequestException;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Service
public class GoalValidatorService {

    public void assertValid(Goal goal) throws BadRequestException {
        if (!(areSelectedProductsValid(goal) &&
                areSelectedSubcategoriesValid(goal) &&
                areAssignedUsersValid(goal) &&
                areIntervalsValid(goal))) {
            throw new BadRequestException();
        }
    }

    public boolean isUserGoalsValid(Set<UserGoal> userGoals) {
        if (userGoals == null || userGoals.size() == 0) {
            return false;
        }
        return userGoals.stream()
                .allMatch(c -> c.getIntervals().stream().mapToDouble(GoalInterval::getExpectedValue).sum() > 0.0);
    }

    private boolean areSelectedProductsValid(Goal goal) {
        if (GoalType.WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT.equals(goal.getType())) {
            return !goal.getProducts().isEmpty();
        }
        return true;
    }

    private boolean areSelectedSubcategoriesValid(Goal goal) {
        if (GoalType.OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT.equals(goal.getType())) {
            return !goal.getSubcategories().isEmpty();
        }
        return true;
    }

    private boolean areAssignedUsersValid(Goal goal) {
        Set<Long> range = LongStream.rangeClosed(0, goal.getIntervalNumber() - 1).boxed().collect(Collectors.toSet());
        return !goal.getUsersGoals().isEmpty() && goal.getUsersGoals().stream().allMatch(userGoal -> isUserGoalValid(userGoal, range));

    }

    private boolean isUserGoalValid(UserGoal userGoal, Set<Long> range) {
        Set<Long> indexes = userGoal.getIntervals().stream().map(GoalInterval::getIndex).collect(Collectors.toSet());
        return range.equals(indexes);
    }


    private boolean areIntervalsValid(Goal goal) {
        boolean numbersValid = goal.getStartYear() >= Calendar.getInstance().get(Calendar.YEAR) && goal.getStartIndex() > 0 && goal.getIntervalNumber() > 0;
        boolean intervalValid = false;
        if (GoalIntervalType.YEAR.equals(goal.getInterval())) {
            intervalValid = goal.getIntervalNumber() == 1;
        } else if (GoalIntervalType.QUARTER.equals(goal.getInterval())) {
            intervalValid = goal.getStartIndex() + goal.getIntervalNumber() - 1 <= 4;
        } else if (GoalIntervalType.MONTH.equals(goal.getInterval())) {
            intervalValid = goal.getStartIndex() + goal.getIntervalNumber() - 1 <= 12;
        }
        return numbersValid && intervalValid;
    }

}
