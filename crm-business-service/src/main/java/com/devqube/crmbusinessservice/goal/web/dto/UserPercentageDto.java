package com.devqube.crmbusinessservice.goal.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class UserPercentageDto {
    private Long userId;
    private Long percentage;
    private Opportunity opportunity;

    public Double getParticipation() {
        return percentage * 0.01;
    }

    public Double getAmount() {
        return getParticipation() * opportunity.getAmount();
    }
}
