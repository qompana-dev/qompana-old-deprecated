package com.devqube.crmbusinessservice.goal.model;

public enum GoalIntervalType {
    YEAR, QUARTER, MONTH
}
