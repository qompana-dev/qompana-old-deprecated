package com.devqube.crmbusinessservice.goal.model;


import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GoalInterval {

    @Id
    @SequenceGenerator(name = "goal_interval_seq", sequenceName = "goal_interval_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "goal_interval_seq")
    private Long id;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private UserGoal userGoal;
    private Long index;
    private Double expectedValue = 0.0;

}
