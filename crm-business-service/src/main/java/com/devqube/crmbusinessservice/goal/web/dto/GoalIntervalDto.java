package com.devqube.crmbusinessservice.goal.web.dto;

import lombok.Data;

@Data
public class GoalIntervalDto {
    private Long index;
    private Double expectedValue;
    private Double currentValue;
    private Double result;
}
