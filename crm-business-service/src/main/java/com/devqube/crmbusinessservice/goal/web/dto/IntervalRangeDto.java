package com.devqube.crmbusinessservice.goal.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class IntervalRangeDto {
    private Integer index;
    private LocalDateTime from;
    private LocalDateTime to;
}
