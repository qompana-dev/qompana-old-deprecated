package com.devqube.crmbusinessservice.goal.web.dto;

import com.devqube.crmbusinessservice.goal.model.GoalIntervalType;
import com.devqube.crmbusinessservice.goal.model.GoalType;
import com.devqube.crmbusinessservice.productprice.category.ProductSubcategory;
import com.devqube.crmbusinessservice.productprice.category.web.ProductSubcategoryDto;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmbusinessservice.productprice.products.web.dto.ProductDTO;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class GoalEditDto {

    private Long id;
    private String name;
    private GoalType type;
    private GoalIntervalType interval;
    private Integer intervalNumber;
    private Integer startIndex;
    private Integer startYear;
    private String currency;

    private Set<UserGoalDto> usersGoals = new HashSet<>();

    private Set<ProductSubcategoryDto> subcategories = new HashSet<>();
    private Set<ProductDTO> products = new HashSet<>();
}
