package com.devqube.crmbusinessservice.goal.model;

import com.devqube.crmbusinessservice.productprice.category.ProductSubcategory;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Goal {
    @Id
    @SequenceGenerator(name = "goal_seq", sequenceName = "goal_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "goal_seq")
    private Long id;
    private String name;
    private Long ownerId;

    @Enumerated(EnumType.STRING)
    private GoalType type;
    @Enumerated(EnumType.STRING)
    private GoalIntervalType interval;
    private Integer intervalNumber;
    private Integer startIndex;
    private Integer startYear;
    private String currency;

    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "goal", orphanRemoval = true)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<UserGoal> usersGoals;

    @ManyToMany
    @JoinTable(name = "goal_to_subcategory", joinColumns = @JoinColumn(name = "goal_id"), inverseJoinColumns = @JoinColumn(name = "subcategory_id"))
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<ProductSubcategory> subcategories = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "goal_to_product", joinColumns = @JoinColumn(name = "goal_id"), inverseJoinColumns = @JoinColumn(name = "product_id"))
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Product> products = new HashSet<>();

    private boolean deleted;
}
