package com.devqube.crmbusinessservice.goal;

import com.devqube.crmbusinessservice.goal.model.UserGoal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface UserGoalRepository extends JpaRepository<UserGoal, Long> {
    Set<UserGoal> findAllByGoalIdAndParentUserIdAndUserIdNot(Long goalId, Long parentUserId, Long userId);
    Set<UserGoal> findAllByGoalId(Long goalId);

    void deleteAllByGoalIdAndParentUserId(Long goalId, Long parentUserId);

    Optional<UserGoal> findFirstByGoalIdAndUserIdAndParentUserIdNot(Long goalId, Long userId, Long parentId);
}
