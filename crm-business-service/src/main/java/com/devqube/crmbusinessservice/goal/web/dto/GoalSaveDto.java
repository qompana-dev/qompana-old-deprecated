package com.devqube.crmbusinessservice.goal.web.dto;

import com.devqube.crmbusinessservice.goal.model.GoalIntervalType;
import com.devqube.crmbusinessservice.goal.model.GoalType;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class GoalSaveDto {

    private String name;
    private GoalType type;
    private GoalIntervalType interval;
    private Integer intervalNumber;
    private Integer startIndex;
    private Integer startYear;
    private String currency;

    private Set<UserGoalForSaveDto> usersGoals;
    private Set<Long> associatedObjectsIds = new HashSet<>();

}
