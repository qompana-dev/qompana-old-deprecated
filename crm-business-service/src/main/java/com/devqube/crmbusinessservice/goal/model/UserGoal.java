package com.devqube.crmbusinessservice.goal.model;


import lombok.*;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserGoal {
    @Id
    @SequenceGenerator(name = "user_goal_seq", sequenceName = "user_goal_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_goal_seq")
    private Long id;

    @ManyToOne
    @ToString.Exclude
    private Goal goal;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userGoal", orphanRemoval = true)
    @ToString.Exclude
    private Set<GoalInterval> intervals;

    private Long userId;
    private Long parentUserId;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserGoal userGoal = (UserGoal) o;
        return Objects.equals(userId, userGoal.userId) &&
                Objects.equals(parentUserId, userGoal.parentUserId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, parentUserId);
    }
}
