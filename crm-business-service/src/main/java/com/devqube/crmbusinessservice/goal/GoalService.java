package com.devqube.crmbusinessservice.goal;

import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.goal.model.Goal;
import com.devqube.crmbusinessservice.goal.model.GoalInterval;
import com.devqube.crmbusinessservice.goal.model.GoalType;
import com.devqube.crmbusinessservice.goal.model.UserGoal;
import com.devqube.crmbusinessservice.goal.web.dto.*;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.web.CurrentRequestUtil;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class GoalService {

    private final GoalRepository goalRepository;
    private final UserService userService;
    private final GoalValidatorService goalValidatorService;
    private final GoalMapperService goalMapper;
    private final GoalDataCollectorService goalDataCollectorService;
    private final ModelMapper modelMapper;
    private final UserGoalRepository userGoalRepository;

    public GoalService(GoalRepository goalRepository,
                       UserService userService,
                       GoalValidatorService goalValidatorService,
                       GoalMapperService goalMapperService,
                       GoalDataCollectorService goalDataCollectorService,
                       ModelMapper modelMapper,
                       UserGoalRepository userGoalRepository) {
        this.goalRepository = goalRepository;
        this.userService = userService;
        this.goalValidatorService = goalValidatorService;
        this.goalMapper = goalMapperService;
        this.goalDataCollectorService = goalDataCollectorService;
        this.modelMapper = modelMapper;
        this.userGoalRepository = userGoalRepository;
    }

    @Transactional(rollbackFor = BadRequestException.class)
    public GoalSaveDto createGoal(GoalSaveDto dto, String userEmail) throws BadRequestException {
        Long accountId = userService.getUserId(userEmail);
        Goal goal = goalMapper.mapToGoal(dto, accountId);
        goalValidatorService.assertValid(goal);
        if (!goalValidatorService.isUserGoalsValid(goal.getUsersGoals())) {
            throw new BadRequestException();
        }
        List<UserGoal> toAdd = setUserGoalsToLastLevel(goal, goal.getUsersGoals());
        goal.getUsersGoals().addAll(toAdd);
        goalRepository.save(goal);
        return goalMapper.mapToSaveDto(goal);
    }

    public Page<GoalListDto> getGoalsCreatedByMe(Pageable pageable) throws BadRequestException {
        Long ownerId = userService.getUserId(CurrentRequestUtil.getLoggedInAccountEmail());
        Page<Goal> allByOwnerIdAndDeletedFalse = goalRepository.findAllByOwnerIdAndDeletedFalse(ownerId, pageable);
        Map<Long, Double> goalCompletedValues = goalDataCollectorService.getGoalCompletedValues(allByOwnerIdAndDeletedFalse.getContent(), ownerId);
        return allByOwnerIdAndDeletedFalse.map(c -> GoalListDto.fromModel(c, goalCompletedValues));
    }

    public Page<GoalListDto> getGoalsAssignedToMe(Pageable pageable) throws BadRequestException {
        Long ownerId = userService.getUserId(CurrentRequestUtil.getLoggedInAccountEmail());
        Page<Goal> allAssignedToUser = goalRepository.findAllAssignedToUser(ownerId, pageable);
        Map<Long, Double> goalCompletedValues = goalDataCollectorService.getGoalCompletedValues(allAssignedToUser.getContent(), ownerId);
        return allAssignedToUser.map(c -> GoalListDto.fromModel(c, goalCompletedValues));
    }

    public List<GoalWidgetPreviewDto> getAllForMeByType(String email, GoalType type) throws BadRequestException {
        Long userId = userService.getUserId(email);
        Set<Goal> goals = goalRepository.findAllByOwnerIdAndDeletedFalseAndType(userId, type);
        goals.addAll(goalRepository.findAllAssignedToUserAndType(userId, type));
        return goals.stream().map(e -> modelMapper.map(e, GoalWidgetPreviewDto.class)).collect(Collectors.toList());
    }

    public void deleteById(Long id) throws BadRequestException, PermissionDeniedException {
        Optional<Goal> byId = goalRepository.findById(id);
        if (byId.isPresent()) {
            Goal toDelete = byId.get();
            Long ownerId = userService.getUserId(CurrentRequestUtil.getLoggedInAccountEmail());
            if (!toDelete.getOwnerId().equals(ownerId)) {
                throw new PermissionDeniedException("Not owner trying to delete goal");
            }
            toDelete.setDeleted(true);
            goalRepository.save(toDelete);
        }
    }

    public GoalEditDto getGoalForEdit(Long goalId) throws EntityNotFoundException, BadRequestException {
        Goal goal = goalRepository.findById(goalId).orElseThrow(EntityNotFoundException::new);
        GoalEditDto result = modelMapper.map(goal, GoalEditDto.class);
        Long accountId = userService.getUserId(CurrentRequestUtil.getLoggedInAccountEmail());
        result.setUsersGoals(goalDataCollectorService.getUserGoals(goal, accountId));
        return result;
    }

    public List<UserGoalDto> getUserGoalsDto(Long goalId) throws EntityNotFoundException, BadRequestException {
        Goal goal = goalRepository.findById(goalId).orElseThrow(EntityNotFoundException::new);
        Long accountId = userService.getUserId(CurrentRequestUtil.getLoggedInAccountEmail());
        return new ArrayList<>(goalDataCollectorService.getUserGoals(goal, accountId));
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class, BadRequestException.class})
    public GoalEditDto updateGoal(Long goalId, GoalEditDto dto, String userEmail) throws EntityNotFoundException, BadRequestException {
        Long parentUserId = userService.getUserId(userEmail);

        Goal goal = goalRepository.findById(goalId).orElseThrow(EntityNotFoundException::new);
        Set<UserGoal> newUserGoals = goalMapper.getUserGoals(goal, dto, parentUserId);
        if (!goalValidatorService.isUserGoalsValid(newUserGoals)) {
            throw new BadRequestException();
        }
        Set<UserGoal> fromDbUserGoals = userGoalRepository.findAllByGoalIdAndParentUserIdAndUserIdNot(goalId, parentUserId, parentUserId);

        deleteUserGoalsNotPresentInNewUserGoals(goalId, newUserGoals, fromDbUserGoals);

        userGoalRepository.deleteAllByGoalIdAndParentUserId(goalId, parentUserId);
        userGoalRepository.flush();

        userGoalRepository.saveAll(newUserGoals);
        userGoalRepository.flush();

        Set<UserGoal> dbList = userGoalRepository.findAllByGoalId(goalId);
        List<UserGoal> toAdd = setUserGoalsToLastLevel(goal, dbList);
        dbList.addAll(toAdd);
        userGoalRepository.saveAll(dbList);
        userGoalRepository.flush();

        return getGoalForEdit(goalId);
    }

    private void deleteUserGoalsNotPresentInNewUserGoals(Long goalId, Set<UserGoal> newUserGoals, Set<UserGoal> fromDbUserGoals) {
        Set<UserGoal> toDelete = fromDbUserGoals
                .stream()
                .filter(e -> !newUserGoals.contains(e))
                .collect(Collectors.toSet());
        toDelete.addAll(getChildGoals(goalId, toDelete));
        userGoalRepository.deleteAll(toDelete);
        userGoalRepository.flush();
    }

    private Set<UserGoal> getChildGoals(Long goalId, Set<UserGoal> parentGoals) {
        Set<UserGoal> childGoals = new HashSet<>();
        for (UserGoal parentGoal : parentGoals) {
            Set<UserGoal> children = new HashSet<>(userGoalRepository.findAllByGoalIdAndParentUserIdAndUserIdNot
                    (goalId, parentGoal.getUserId(), parentGoal.getUserId()));
            if (!children.isEmpty()) {
                childGoals.addAll(children);
                childGoals.addAll(getChildGoals(goalId, children));
            }
        }
        return childGoals;
    }

    private List<UserGoal> setUserGoalsToLastLevel(Goal goal, Set<UserGoal> userGoals) {
        Set<Long> allParentIds = userGoals.stream()
                .map(UserGoal::getParentUserId).collect(Collectors.toSet());

        List<UserGoal> result = new ArrayList<>();

        userGoals.stream()
                .filter(c -> !allParentIds.contains(c.getUserId()))
                .forEach(c -> result.add(getUserGoalForLastLevel(goal, c)));

        return result;
    }

    private UserGoal getUserGoalForLastLevel(Goal goal, UserGoal userGoal) {
        UserGoal result = new UserGoal();
        result.setGoal(goal);
        result.setUserId(userGoal.getUserId());
        result.setParentUserId(userGoal.getUserId());

        Set<GoalInterval> intervals = new HashSet<>();

        userGoal.getIntervals().forEach(interval -> intervals.add(getGoalIntervalForLastLevel(interval, result)));
        result.setIntervals(intervals);
        return result;
    }

    private GoalInterval getGoalIntervalForLastLevel(GoalInterval interval, UserGoal userGoal) {
        GoalInterval result = new GoalInterval();
        result.setUserGoal(userGoal);
        result.setIndex(interval.getIndex());
        result.setExpectedValue(interval.getExpectedValue());
        return result;
    }

    public UserGoalDto getUserGoalAssignedToMe(Long goalId, String email) throws BadRequestException, EntityNotFoundException {
        Long accountId = userService.getUserId(email);
        Optional<UserGoal> userGoalOpt = userGoalRepository.findFirstByGoalIdAndUserIdAndParentUserIdNot(goalId, accountId, accountId);
        UserGoal userGoal = userGoalOpt.orElseThrow(EntityNotFoundException::new);
        return modelMapper.map(userGoal, UserGoalDto.class);
    }
}
