package com.devqube.crmbusinessservice.goal.web.dto;

import lombok.Data;

import java.util.Set;

@Data
public class UserGoalDto {
    private Long userId;
    private Set<GoalIntervalDto> intervals;
}
