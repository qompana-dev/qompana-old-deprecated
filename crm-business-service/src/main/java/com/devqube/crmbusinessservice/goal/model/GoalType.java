package com.devqube.crmbusinessservice.goal.model;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum GoalType {

    CREATED_LEADS_NUMBER(GoalTypeCategory.LEAD),
    CONVERTED_LEADS_NUMBER(GoalTypeCategory.LEAD),

    CREATED_OPPORTUNITIES_NUMBER(GoalTypeCategory.OPPORTUNITY),
    WON_OPPORTUNITIES_NUMBER(GoalTypeCategory.OPPORTUNITY),
    WON_OPPORTUNITIES_AMOUNT(GoalTypeCategory.OPPORTUNITY),
    WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT(GoalTypeCategory.OPPORTUNITY),
    OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT(GoalTypeCategory.OPPORTUNITY),

    PHONE_CALLS_NUMBER(GoalTypeCategory.CALENDAR),
    CALENDAR_TASKS_NUMBER(GoalTypeCategory.CALENDAR),

    ADDED_CUSTOMERS_NUMBER(GoalTypeCategory.CUSTOMER),
    ADDED_CONTACTS_NUMBER(GoalTypeCategory.CUSTOMER),

    GENERATED_OFFERS_NUMBER(GoalTypeCategory.OFFER),
    CUSTOMERS_WHO_RECEIVED_OFFER(GoalTypeCategory.OFFER);

    private GoalTypeCategory category;

    GoalType(GoalTypeCategory category) {
        this.category = category;
    }

    public GoalTypeCategory getCategory() {
        return this.category;
    }

    public List<GoalType> getGoalTypesOfCategory(GoalTypeCategory category) {
        return Stream.of(GoalType.values()).filter(goalType -> goalType.getCategory().equals(category)).collect(Collectors.toList());
    }

}
