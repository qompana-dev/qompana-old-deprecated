package com.devqube.crmbusinessservice.goal.model;

public enum GoalTypeCategory {
    LEAD, OPPORTUNITY, CALENDAR, CUSTOMER, OFFER
}
