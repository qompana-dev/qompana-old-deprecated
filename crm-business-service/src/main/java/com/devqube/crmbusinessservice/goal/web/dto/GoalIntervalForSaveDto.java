package com.devqube.crmbusinessservice.goal.web.dto;

import lombok.Data;

@Data
public class GoalIntervalForSaveDto {
    private Long index;
    private Double expectedValue;
}
