package com.devqube.crmbusinessservice.goal;

import com.devqube.crmbusinessservice.goal.model.Goal;
import com.devqube.crmbusinessservice.goal.model.GoalType;
import com.devqube.crmbusinessservice.goal.model.UserGoal;
import com.devqube.crmbusinessservice.goal.web.dto.GoalEditDto;
import com.devqube.crmbusinessservice.goal.web.dto.GoalSaveDto;
import com.devqube.crmbusinessservice.productprice.category.ProductSubcategory;
import com.devqube.crmbusinessservice.productprice.category.ProductSubcategoryRepository;
import com.devqube.crmbusinessservice.productprice.products.ProductRepository;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class GoalMapperService {

    private final ProductRepository productRepository;
    private final ProductSubcategoryRepository productSubcategoryRepository;
    private final ModelMapper modelMapper;

    public GoalMapperService(@Qualifier("strictModelMapper") ModelMapper modelMapper,
                             ProductRepository productRepository,
                             ProductSubcategoryRepository productSubcategoryRepository) {
        this.modelMapper = modelMapper;
        this.productRepository = productRepository;
        this.productSubcategoryRepository = productSubcategoryRepository;
    }

    public Goal mapToGoal(GoalSaveDto dto, Long accountId) {
        Goal goal = mapDtoToGoal(dto, accountId);

        if (GoalType.WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT.equals(goal.getType())) {
            setProducts(goal, dto);
        }
        if (GoalType.OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT.equals(goal.getType())) {
            setSubcategories(goal, dto);
        }

        return goal;
    }

    public GoalSaveDto mapToSaveDto(Goal goal) {
        GoalSaveDto dto = modelMapper.map(goal, GoalSaveDto.class);
        Set<Long> ids = Stream.concat(
                goal.getProducts().stream().map(Product::getId),
                goal.getSubcategories().stream().map(ProductSubcategory::getId))
                .collect(Collectors.toSet());
        dto.setAssociatedObjectsIds(ids);
        return dto;
    }

    public Set<UserGoal> getUserGoals(Goal goal, GoalEditDto edited, Long accountId) {
        Set<UserGoal> result = edited.getUsersGoals().stream().map(c -> modelMapper.map(c, UserGoal.class)).collect(Collectors.toSet());
        result.forEach(userGoal -> {
            userGoal.setGoal(goal);
            userGoal.setParentUserId(accountId);
            userGoal.getIntervals().forEach(goalInterval -> goalInterval.setUserGoal(userGoal));
        });
        return result;
    }


    private Goal mapDtoToGoal(GoalSaveDto dto, Long accountId) {
        Goal goal = modelMapper.map(dto, Goal.class);
        goal.setOwnerId(accountId);
        goal.getUsersGoals().forEach(userGoal -> {
            userGoal.setGoal(goal);
            userGoal.setParentUserId(accountId);
            userGoal.getIntervals().forEach(goalInterval -> goalInterval.setUserGoal(userGoal));
        });
        return goal;
    }

    private void setSubcategories(Goal goal, GoalSaveDto dto) {
        Set<ProductSubcategory> subcategories = productSubcategoryRepository.findAllByIdIn(dto.getAssociatedObjectsIds());
        goal.setSubcategories(subcategories);
    }

    private void setProducts(Goal goal, GoalSaveDto dto) {
        Set<Product> products = productRepository.findAllByIdIn(dto.getAssociatedObjectsIds());
        goal.setProducts(products);
    }
}
