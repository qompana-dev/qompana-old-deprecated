package com.devqube.crmbusinessservice.phoneNumber.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class PhoneNumberTypeDto {

    private Long id;
    private String name;

}
