package com.devqube.crmbusinessservice.phoneNumber;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.dictionary.Word;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.phoneNumber.web.dto.PhoneNumberDTO;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PhoneNumberService {

    private final PhoneNumberRepository repository;

    public PhoneNumberService(PhoneNumberRepository repository) {
        this.repository = repository;
    }

    public void setLeadAdditionalPhoneNumbers(Lead lead, Set<PhoneNumberDTO> additionalPhonesDTOSet) {
        Set<PhoneNumber> additionalPhones = PhoneNumberMapper.mapToLeadPhoneNumberSet(additionalPhonesDTOSet, lead);
        lead.setAdditionalPhones(additionalPhones);
    }

    public void setCustomerAdditionalPhoneNumbers(Customer customer, Set<PhoneNumberDTO> additionalPhonesDTOSet) {
        Set<PhoneNumber> additionalPhones = PhoneNumberMapper.mapToCustomerPhoneNumberSet(additionalPhonesDTOSet, customer);
        customer.setAdditionalPhones(additionalPhones);
    }

    public void setContactAdditionalPhoneNumbers(Contact contact, Set<PhoneNumberDTO> additionalPhonesDTOSet) {
        Set<PhoneNumber> additionalPhones = PhoneNumberMapper.mapToContactPhoneNumberSet(additionalPhonesDTOSet, contact);
        contact.setAdditionalPhones(additionalPhones);
    }

    public void modifyLeadAdditionalPhoneNumbers(Lead lead, Set<PhoneNumberDTO> additionalPhonesDTOSet) {
        lead.setAdditionalPhones(null);
        repository.deleteByLeadId(lead.getId());
        if (additionalPhonesDTOSet != null && additionalPhonesDTOSet.size() > 0) {
            setLeadAdditionalPhoneNumbers(lead, additionalPhonesDTOSet);
        }
    }

    public void modifyCustomerAdditionalPhoneNumbers(Customer customer, Set<PhoneNumberDTO> additionalPhonesDTOSet) {
        customer.setAdditionalPhones(null);
        repository.deleteByCustomerId(customer.getId());
        if (additionalPhonesDTOSet != null && additionalPhonesDTOSet.size() > 0) {
            setCustomerAdditionalPhoneNumbers(customer, additionalPhonesDTOSet);
        }
    }

    public void modifyContactAdditionalPhoneNumbers(Contact contact, Set<PhoneNumber> additionalPhonesDTOSet) {
        contact.setAdditionalPhones(null);
        repository.deleteByContactId(contact.getId());
        if (additionalPhonesDTOSet != null && additionalPhonesDTOSet.size() > 0) {
            Set<PhoneNumber> additionalPhonesSet = additionalPhonesDTOSet.stream()
                    .map(phoneDto -> {
                        Word type = new Word()
                                .setId(phoneDto.getType().getId());
                        PhoneNumber phone = new PhoneNumber();
                        phone.setNumber(phoneDto.getNumber());
                        phone.setType(type);
                        phone.setContact(contact);
                        return phone;
                    })
                    .collect(Collectors.toSet());
            contact.setAdditionalPhones(additionalPhonesSet);
        }
    }

}
