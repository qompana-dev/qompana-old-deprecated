package com.devqube.crmbusinessservice.phoneNumber;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Long>, JpaSpecificationExecutor<PhoneNumber> {

    void deleteByLeadId(Long id);

    void deleteByCustomerId(Long id);

    void deleteByContactId(Long id);

}
