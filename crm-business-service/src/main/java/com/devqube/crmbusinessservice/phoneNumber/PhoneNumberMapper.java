package com.devqube.crmbusinessservice.phoneNumber;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.dictionary.Word;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.phoneNumber.web.dto.PhoneNumberDTO;
import com.devqube.crmbusinessservice.phoneNumber.web.dto.PhoneNumberTypeDto;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class PhoneNumberMapper {

    public static Set<PhoneNumberDTO> mapToPhoneNumberDtoSet(Set<PhoneNumber> phoneNumberSet) {
        return phoneNumberSet.stream()
                .map(phone -> {
                    PhoneNumberDTO phoneDto = new PhoneNumberDTO();
                    phoneDto.setPhone(phone.getNumber());
                    phoneDto.setType(new PhoneNumberTypeDto()
                            .setId(phone.getType().getId())
                            .setName(phone.getType().getName()));
                    return phoneDto;
                })
                .collect(Collectors.toSet());
    }

    public static Set<PhoneNumber> mapToLeadPhoneNumberSet(Set<PhoneNumberDTO> phoneNumberDtoSet, Lead lead) {
        return phoneNumberDtoSet.stream()
                .map(phoneDto -> {
                    Word type = new Word()
                            .setId(phoneDto.getType().getId());
                    PhoneNumber phone = new PhoneNumber();
                    phone.setNumber(phoneDto.getPhone());
                    phone.setType(type);
                    phone.setLead(lead);
                    return phone;
                })
                .collect(Collectors.toSet());
    }

    public static Set<PhoneNumber> mapToCustomerPhoneNumberSet(Set<PhoneNumberDTO> phoneNumberDtoSet, Customer customer) {
        return phoneNumberDtoSet.stream()
                .map(phoneDto -> {
                    Word type = new Word()
                            .setId(phoneDto.getType().getId());
                    PhoneNumber phone = new PhoneNumber();
                    phone.setNumber(phoneDto.getPhone());
                    phone.setType(type);
                    phone.setCustomer(customer);
                    return phone;
                })
                .collect(Collectors.toSet());
    }

    public static Set<PhoneNumber> mapToContactPhoneNumberSet(Set<PhoneNumberDTO> phoneNumberDtoSet, Contact contact) {
        return phoneNumberDtoSet.stream()
                .map(phoneDto -> {
                    Word type = new Word()
                            .setId(phoneDto.getType().getId());
                    PhoneNumber phone = new PhoneNumber();
                    phone.setNumber(phoneDto.getPhone());
                    phone.setType(type);
                    phone.setContact(contact);
                    return phone;
                })
                .collect(Collectors.toSet());
    }

}
