package com.devqube.crmbusinessservice.customer.type;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeService {
    private final TypeRepository repository;

    public TypeService(TypeRepository repository) {
        this.repository = repository;
    }

    public List<Type> getAll() {
        return this.repository.findAll();
    }
}
