package com.devqube.crmbusinessservice.customer.whitelist.dto;

public enum StatusVatEnum {
    Czynny,
    Zwolniony,
    Niezarejestrowany
}
