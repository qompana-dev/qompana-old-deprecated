package com.devqube.crmbusinessservice.customer.contact.web.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ContactTaskDtoOut {

    private Long id;
    private final String type = "CONTACT";
    private Long ownerAccountId;
    private String name;
    private String email;
    private String avatar;

}
