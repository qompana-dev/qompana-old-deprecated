package com.devqube.crmbusinessservice.customer.whitelist.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WhitelistPersonDto {
    private String companyName;
    private String firstName;
    private String lastName;
    private String pesel;
    private String nip;
}
