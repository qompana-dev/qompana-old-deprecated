package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmbusinessservice.customer.customer.web.dto.CustomerSearchType;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
@AuthFilterEnabled
public class ContactSearchVariantsDtoOut {
    @AuthFields(list = {
            @AuthField(controller = "getSearchVariants"),
    })
    private ContactSearchType type;

    @AuthFields(list = {
            @AuthField(controller = "getSearchVariants"),
    })
    private List<String> variants = new ArrayList<>();
}
