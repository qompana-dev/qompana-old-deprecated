package com.devqube.crmbusinessservice.customer.customer.web;

import com.devqube.crmbusinessservice.customer.customer.CustomerService;
import com.devqube.crmbusinessservice.customer.customer.CustomerSpecifications;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.customer.web.dto.*;
import com.devqube.crmbusinessservice.customer.exception.NameOccupiedException;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.access.annotation.AuthControllerCase;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.OK;

@RestController
@Slf4j
public class CustomerController implements CustomerApi {
    private final CustomerService customerService;

    private final ModelMapper modelMapper;

    public CustomerController(CustomerService customerService, ModelMapper modelMapper) {
        this.customerService = customerService;
        this.modelMapper = modelMapper;
    }

    @Override
    @AuthController(name = "getCustomerWithContacts")
    public ResponseEntity<CustomerWithContactsDto> getCustomerWithContacts(String loggedAccountEmail, Long customerId) {
        try {
            Customer customer = customerService.findById(customerId);
            CustomerWithContactsDto customerWithContactsDto = CustomerWithContactsDto.fromModel(customer);
            customerService.saveLastViewed(customer, loggedAccountEmail);
            return ResponseEntity.ok(customerWithContactsDto);
        } catch (EntityNotFoundException e) {
            log.info("Customer with id {} not found", customerId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info("Logged user not found");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "markAsMainContact", actionFrontendId = "CustomerContactsComponent.markMainContact")
    public ResponseEntity<Void> markAsMainContact(Long customerId, Long contactId) {
        try {
            customerService.markAsMainContact(customerId, contactId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.info("Customer with id {} not found", customerId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "updateCustomerNotes", actionFrontendId = "CustomerContactsComponent.notes")
    public ResponseEntity<Void> updateCustomerNotes(Long customerId, @Valid String notes) {
        try {
            customerService.saveNotes(customerId, notes);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.info("Customer with id {} not found", customerId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @Override
    @AuthController(name = "createCustomer", actionFrontendId = "CustomerAddComponent.save")
    public ResponseEntity<CustomerWithContactsDto> createCustomer(@Valid CustomerWithContactsDto customer) {
        try {
            return new ResponseEntity<>(customerService.save(customer), HttpStatus.CREATED);
        } catch (NameOccupiedException e) {
            log.info("Customer with name {} already exists", customer.getName());
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "updateListOfCustomers", actionFrontendId = "CustomerEditComponent.save")
    public ResponseEntity<Void> updateListOfCustomers(List<CustomerWithContactsDto> customers) {
        try {
            customerService.modify(customers);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (NameOccupiedException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "updateCustomer", actionFrontendId = "CustomerEditComponent.save", controllerCase = {
            @AuthControllerCase(type = "contactInfo", name = "updateCustomerInContactInfo",
                    actionFrontendId = "CustomerInContactInfoComponent.save")
    })
    public ResponseEntity<Void> updateCustomer(Long customerId, @Valid CustomerWithContactsDto customer) {
        if (!customerId.equals(customer.getId())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        try {
            customerService.modify(customer);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Customer with id {} not found", customerId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (NameOccupiedException e) {
            log.info("New name {} is occupied", customerId);
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getCustomer")
    public ResponseEntity<Customer> getCustomer(String loggedAccountEmail, Long customerId) {
        try {
            return new ResponseEntity<>(customerService.findById(customerId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Customer with id {} not found", customerId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(readFrontendId = "RelatedCustomerInContactDetail.customerPreview")
    public ResponseEntity<CustomerPreviewDto> getCustomerPreview(String loggedAccountEmail, Long customerId) {
        try {
            return new ResponseEntity<>(customerService.getCustomerPreview(customerId, loggedAccountEmail), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Customer with id {} not found", customerId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info("user not found");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "archiveCustomer", actionFrontendId = "CustomerListComponent.archive")
    public ResponseEntity<Void> archiveCustomer(String loggedAccountEmail, Long customerId) {
        try {
            customerService.archiveCustomer(customerId);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Customer with id {} not found", customerId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "archiveCustomers", actionFrontendId = "CustomerListComponent.archive")
    public ResponseEntity<Void> archiveCustomers(String loggedAccountEmail, @Valid List<Long> ids) {
        try {
            customerService.archiveCustomers(ids);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getCustomerForEdit", controllerCase = {
            @AuthControllerCase(type = "contactInfo", name = "getCustomerForEditInContactInfo")
    })
    public ResponseEntity<CustomerWithContactsDto> getCustomerForEdit(Long customerId) {
        try {
            return new ResponseEntity<>(customerService.findCustomerById(customerId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Customer with id {} not found", customerId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "getCustomers")
    public ResponseEntity<Page<CustomerOnListDTO>> getCustomers(String loggedAccountEmail, @Valid Pageable pageable,
                                                                @Valid Boolean today, @Valid Boolean edited,
                                                                @Valid Boolean viewed, @Valid Boolean archived,
                                                                @Valid String search) {
        try {
            if (today != null && today) {
                return new ResponseEntity<>(customerService.findAllCreatedToday(search, pageable), HttpStatus.OK);
            } else if (edited != null && edited) {
                return new ResponseEntity<>(customerService.findAllRecentlyEdited(search, pageable), HttpStatus.OK);
            } else if (viewed != null && viewed) {
                return new ResponseEntity<>(customerService.findAllRecentlyViewed(loggedAccountEmail, search, pageable), HttpStatus.OK);
            } else if (archived != null && archived) {
                return new ResponseEntity<>(customerService.findAllArchived(search, pageable), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(customerService.findAll(search, pageable), HttpStatus.OK);
            }
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<CustomerAssociationDto>> getCustomersForMobileAssociationList() {
        return ResponseEntity.ok(customerService.getCustomersForMobileAssociationList());
    }

    @Override
    @AuthController(name = "getCustomersForMobile")
    public ResponseEntity<List<CustomerForMobileListDto>> getCustomersForMobile(String loggedAccountEmail, @Valid Boolean today, @Valid Boolean edited, @Valid Boolean viewed) {
        try {
            if (today != null && today) {
                return new ResponseEntity<>(customerService.findAllCreatedTodayMob(), HttpStatus.OK);
            } else if (edited != null && edited) {
                return new ResponseEntity<>(customerService.findAllRecentlyEditedMob(), HttpStatus.OK);
            } else if (viewed != null && viewed) {
                return new ResponseEntity<>(customerService.findAllRecentlyViewedMob(loggedAccountEmail), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(customerService.findAllMob(), HttpStatus.OK);
            }
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getCustomers")
    public ResponseEntity<List<CustomerOnListDTO>> getCustomersForZapier(String loggedAccountEmail, @Valid Pageable pageable,
                                                                         @Valid Boolean today, @Valid Boolean edited,
                                                                         @Valid Boolean viewed, @Valid String search) {
        try {
            if (today != null && today) {
                return new ResponseEntity<>(customerService.findAllCreatedToday(search,pageable).getContent(), HttpStatus.OK);
            } else if (edited != null && edited) {
                return new ResponseEntity<>(customerService.findAllRecentlyEdited(search,pageable).getContent(), HttpStatus.OK);
            } else if (viewed != null && viewed) {
                return new ResponseEntity<>(customerService.findAllRecentlyViewed(search,loggedAccountEmail, pageable).getContent(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(customerService.findAll(pageable).getContent(), HttpStatus.OK);
            }
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "removeCustomer", actionFrontendId = "CustomerListComponent.remove")
    public ResponseEntity<Void> removeCustomer(Long customerId) {
        try {
            customerService.deleteById(customerId);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Customer with id {} not found", customerId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<CustomerNameDTO>> getCustomerNames(String pattern) {
        return new ResponseEntity<>(customerService.findAllContaining(pattern)
                .stream()
                .map(e -> modelMapper.map(e, CustomerNameDTO.class))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getCustomersForSearch(@NotNull @Valid String term) {
        return new ResponseEntity<>(customerService.findAllContaining(term)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.customer, Strings.nullToEmpty(e.getName()), e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    @AuthController(name = "getSearchVariants")
    public ResponseEntity<List<CustomerSearchVariantsDtoOut>> getSearchVariants(@NotNull @Valid String toSearch) {
        try {
            return new ResponseEntity<>(customerService.findAllSearchVariants(toSearch), HttpStatus.OK);
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


    @Override
    public ResponseEntity<Page<CustomerTaskDtoOut>> getCustomerPage(@NotNull @Valid String term,
                                                                    @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable) {
        return new ResponseEntity<>(CustomerDtoMapper.toCustomerTaskDtoOut(
                customerService.findAll(CustomerSpecifications.findByName(term), pageable)),
                OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getCustomersAsCrmObject(@Valid List<Long> ids) {
        return new ResponseEntity<>(customerService.findAllByIds(ids)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.customer, Strings.nullToEmpty(e.getName()), e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    @AuthController(name = "removeCustomer", actionFrontendId = "CustomerListComponent.remove")
    public ResponseEntity<Void> deleteCustomers(@Valid List<Long> ids) {
        try {
            customerService.deleteCustomers(ids);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public Long checkNip(String customerNip, Long customerId) {
        return customerService.countByNipWithCustomer(customerNip, customerId);
    }

    @Override
    public ResponseEntity<List<Long>> getAllCustomerWithAccess(String loggedAccountEmail) {
        return ResponseEntity.ok(customerService.findAll());
    }

    @Override
    @GetMapping(value = "/customers/{customerId}/name")
    public String getCustomerName(@PathVariable("customerId") Long customerId) throws EntityNotFoundException {
        return customerService.getCustomerName(customerId);
    }


    @Override
    public ResponseEntity<CrmObject> getCustomerByContactId(@Valid Long contactId) {
        try {
            Customer customer = customerService.getCustomerByContactId(contactId);
            CrmObject crmObject = new CrmObject(customer.getId(), CrmObjectType.customer, Strings.nullToEmpty(customer.getName()), customer.getCreated());
            return ResponseEntity.ok(crmObject);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "getCustomerOpportunities")
    public ResponseEntity<List<CustomerSalesOpportunityDTO>> getCustomerOpportunities(String loggedAccountEmail, Long customerId) {
        try {
            return new ResponseEntity<>(customerService.getCustomerOpportunities(loggedAccountEmail, customerId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getCustomerOpportunitiesPreview")
    public ResponseEntity<CustomerSalesOpportunityPreviewDTO> getCustomerOpportunitiesPreview(String loggedAccountEmail, Long customerId) {
        try {
            return new ResponseEntity<>(customerService.getCustomerOpportunitiesPreview(loggedAccountEmail, customerId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
