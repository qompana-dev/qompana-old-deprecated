package com.devqube.crmbusinessservice.customer.whitelist.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WhitelistItemSubjectDto {
    private String name;
    private String nip;
    private StatusVatEnum statusVat;
    private String regon;
    private String pesel;
    private String krs;
    private String residenceAddress;
    private String workingAddress;

    private WhitelistPersonDto[] representatives;
    private WhitelistPersonDto[] authorizedClerks;
    private WhitelistPersonDto[] partners;

    private LocalDate registrationLegalDate;
    private String registrationDenialBasis;
    private LocalDate registrationDenialDate;
    private String restorationBasis;
    private LocalDate restorationDate;
    private String removalBasis;
    private LocalDate removalDate;

    private String[] accountNumbers;

    private Boolean hasVirtualAccounts;
}
