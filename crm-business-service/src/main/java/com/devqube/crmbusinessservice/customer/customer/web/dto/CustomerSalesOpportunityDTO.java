package com.devqube.crmbusinessservice.customer.customer.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@AuthFilterEnabled
public class CustomerSalesOpportunityDTO {
    @AuthFields(list = {
            @AuthField(controller = "getCustomerOpportunities"),
            @AuthField(controller = "getCustomerOpportunitiesPreview")
    })
    private Long id;

    @AuthFields(list = {
            @AuthField(controller = "getCustomerOpportunities"),
            @AuthField(controller = "getCustomerOpportunitiesPreview")
    })
    private String name;

    @AuthFields(list = {
            @AuthField(controller = "getCustomerOpportunities"),
            @AuthField(controller = "getCustomerOpportunitiesPreview")
    })
    private Long status;

    @AuthFields(list = {
            @AuthField(controller = "getCustomerOpportunities"),
            @AuthField(controller = "getCustomerOpportunitiesPreview")
    })
    private Long maxStatus;

    @AuthFields(list = {
            @AuthField(controller = "getCustomerOpportunities"),
            @AuthField(controller = "getCustomerOpportunitiesPreview")
    })
    private Double amount;

    @AuthFields(list = {
            @AuthField(controller = "getCustomerOpportunities"),
            @AuthField(controller = "getCustomerOpportunitiesPreview")
    })
    private String currency;

    @AuthFields(list = {
            @AuthField(controller = "getCustomerOpportunities"),
            @AuthField(controller = "getCustomerOpportunitiesPreview")
    })
    private LocalDate finishDate;
}
