package com.devqube.crmbusinessservice.customer.whitelist;

import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.exception.WhitelistItemNotFound;
import com.devqube.crmbusinessservice.customer.whitelist.dto.WhitelistItemDto;
import com.devqube.crmbusinessservice.customer.whitelist.dto.WhitelistItemSubjectDto;
import com.devqube.crmbusinessservice.customer.whitelist.model.BankAccountNumber;
import com.devqube.crmbusinessservice.customer.whitelist.model.Whitelist;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.KafkaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class WhitelistService {
    private static final String API_URL = "https://wl-api.mf.gov.pl/api";

    private final RestTemplate restTemplate;
    private final CustomerRepository customerRepository;
    private final WhitelistRepository whitelistRepository;
    private final KafkaService kafkaService;

    public WhitelistService(RestTemplate restTemplate, CustomerRepository customerRepository, WhitelistRepository whitelistRepository, KafkaService kafkaService) {
        this.restTemplate = restTemplate;
        this.customerRepository = customerRepository;
        this.whitelistRepository = whitelistRepository;
        this.kafkaService = kafkaService;
    }

    void saveLastWhitelistItems() throws WhitelistItemNotFound {
        List<String> nips = WhitelistQueue.getInstance().getLastItemList(30);
        Map<String, WhitelistItemSubjectDto> whitelistMap = new HashMap<>();
        try {
            getWhitelistItems(nips).forEach(c -> whitelistMap.put(c.getNip(), c));

            List<Whitelist> toSave = nips.stream()
                    .map(c -> updateWhitelist(c, whitelistMap.get(c)))
                    .collect(Collectors.toList());

            whitelistRepository.saveAll(toSave);
            toSave.forEach(c -> sendCustomerWhitelistUpdateWebsocket(c.getNip()));
        } catch (Exception e) {
            throw new WhitelistItemNotFound();
        }
    }
    private void sendCustomerWhitelistUpdateWebsocket(String nip) {
        try {
            kafkaService.sendWebsocket("/topic/customer/" + nip + "/whitelist/change", Boolean.TRUE.toString());
        } catch (KafkaSendMessageException e) {
            log.info("unable to send websocket message to '/topic/customer/{}/whitelist/change'", nip);
        }
    }

    private Whitelist updateWhitelist(String nip, WhitelistItemSubjectDto whitelist) {
        if (whitelist == null || nip == null) {
            return null;
        }
        Whitelist fromDb = whitelistRepository.findByNip(nip);
        if (fromDb == null) {
            fromDb = new Whitelist();
        }
        fromDb.setNip(nip);
        fromDb.setStatus(whitelist.getStatusVat());
        fromDb.setRegistered(whitelist.getRegistrationLegalDate());

        LocalDate updateTime = Stream.of(whitelist.getRemovalDate(), whitelist.getRegistrationLegalDate(), whitelist.getRestorationDate())
                .filter(Objects::nonNull).max(LocalDate::compareTo).orElse(null);
        fromDb.setUpdated(updateTime);
        fromDb.setRefreshed(LocalDateTime.now());

        List<BankAccountNumber> bankAccountNumbers = new ArrayList<>();
        for (String accountNumber: whitelist.getAccountNumbers()) {
            bankAccountNumbers.add(new BankAccountNumber(null, fromDb, accountNumber));
        }
        fromDb.setBankAccountNumbers(bankAccountNumbers);

        return fromDb;
    }

    public List<WhitelistItemSubjectDto> getWhitelistItems(List<String> nips) throws WhitelistItemNotFound {
        String url = API_URL + "/search/nips/" + String.join(",", nips);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("date", LocalDate.now().toString());
        ResponseEntity<WhitelistItemDto> response = restTemplate.getForEntity(builder.toUriString(), WhitelistItemDto.class);

        if (isWhitelistResultIncorrect(response)) {
            throw new WhitelistItemNotFound();
        }
        return response.getBody().getResult().getSubjects();
    }

    public WhitelistItemSubjectDto getWhitelistItem(String nip) throws WhitelistItemNotFound {
        String url = API_URL + "/search/nip/" + nip;
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("date", LocalDate.now().toString());
        ResponseEntity<WhitelistItemDto> response = restTemplate.getForEntity(builder.toUriString(), WhitelistItemDto.class);
        if (isWhitelistResultIncorrect(response)) {
            throw new WhitelistItemNotFound();
        }
        return response.getBody().getResult().getSubject();
    }

    private boolean isWhitelistResultIncorrect(ResponseEntity<WhitelistItemDto> response) {
        if (!response.getStatusCode().is2xxSuccessful() ||
                response.getBody() == null ||
                response.getBody().getResult() == null ||
                (response.getBody().getResult().getSubjects() == null && response.getBody().getResult().getSubject() == null)) {

            if (response.getBody().getMessage() != null && response.getBody().getCode() != null) {
                log.info("whitelist error, code = {}, message = {}", response.getBody().getCode(), response.getBody().getMessage());
            }
            return true;
        }
        return false;
    }

    public void refreshWhitelistStatus(String nip) {
        WhitelistQueue.getInstance().add(nip);
    }
    public Whitelist getWhitelistStatus(String nip) {
        Whitelist result = whitelistRepository.findByNip(nip);
        if (result == null) {
            WhitelistQueue.getInstance().add(nip);
        }
        return result;
    }
}
