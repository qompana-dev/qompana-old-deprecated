package com.devqube.crmbusinessservice.customer.contact;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.contact.model.ContactLink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactLinkRepository extends JpaRepository<ContactLink, Long> {
    void deleteAllByBaseContactAndLinkedContact(Contact baseContact, Contact linkedContact);
    List<ContactLink> findAllByBaseContactAndLinkedContact(Contact baseContact, Contact linkedContact);
    List<ContactLink> findAllByBaseContact(Contact baseContact);
    List<ContactLink> findTop3ByBaseContact(Contact baseContact);
    Long countAllByBaseContact(Contact baseContact);
}
