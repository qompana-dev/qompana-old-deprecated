package com.devqube.crmbusinessservice.customer.customer.web.dto;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Optional;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@AuthFilterEnabled
public class ContactDto   {
  @AuthFields(list = {
          @AuthField(controller = "createCustomer"),
          @AuthField(controller = "getCustomerForEdit"),
          @AuthField(controller = "updateCustomer"),
          @AuthField(controller = "getCustomers"),
          @AuthField(controller = "getCustomerForEditInContactInfo"),
          @AuthField(controller = "getContacts")
  })
  private Long id;

  @Length(max = 200)
  @NotNull
  @AuthFields(list = {
          @AuthField(frontendId = "CustomerAddComponent.contactNameField", controller = "createCustomer"),
          @AuthField(frontendId = "CustomerEditComponent.contactNameField", controller = "getCustomerForEdit"),
          @AuthField(frontendId = "CustomerEditComponent.contactNameField", controller = "updateCustomer"),
          @AuthField(controller = "getContacts"),
          @AuthField(controller = "getCustomers"),
          @AuthField(controller = "getCustomerForEditInContactInfo"),
          @AuthField(frontendId = "CustomerContactsComponent.name", controller = "getCustomerWithContacts"),
  })
  private String name;

  @Length(max = 200)
  @NotNull
  @AuthFields(list = {
          @AuthField(frontendId = "CustomerAddComponent.contactSurnameField", controller = "createCustomer"),
          @AuthField(frontendId = "CustomerEditComponent.contactSurnameField", controller = "getCustomerForEdit"),
          @AuthField(frontendId = "CustomerEditComponent.contactSurnameField", controller = "updateCustomer"),
          @AuthField(controller = "getContacts"),
          @AuthField(controller = "getCustomers"),
          @AuthField(controller = "getCustomerForEditInContactInfo"),
          @AuthField(frontendId = "CustomerContactsComponent.name", controller = "getCustomerWithContacts"),
  })
  private String surname;

  @Length(max = 100)
  @AuthFields(list = {
          @AuthField(frontendId = "CustomerAddComponent.contactEmailField", controller = "createCustomer"),
          @AuthField(frontendId = "CustomerEditComponent.contactEmailField", controller = "getCustomerForEdit"),
          @AuthField(frontendId = "CustomerEditComponent.contactEmailField", controller = "updateCustomer"),
          @AuthField(controller = "getContacts"),
          @AuthField(controller = "getCustomers"),
          @AuthField(frontendId = "CustomerContactsComponent.email", controller = "getCustomerWithContacts"),
  })
  private String email;

  @AuthFields(list = {
          @AuthField(frontendId = "CustomerAddComponent.contactPhoneField", controller = "createCustomer"),
          @AuthField(frontendId = "CustomerEditComponent.contactPhoneField", controller = "getCustomerForEdit"),
          @AuthField(frontendId = "CustomerEditComponent.contactPhoneField", controller = "updateCustomer"),
          @AuthField(controller = "getContacts"),
          @AuthField(controller = "getCustomers"),
          @AuthField(frontendId = "CustomerContactsComponent.phone", controller = "getCustomerWithContacts"),
  })
  private String phone;

  @AuthFields(list = {
          @AuthField(frontendId = "CustomerAddComponent.contactPositionField", controller = "createCustomer"),
          @AuthField(frontendId = "CustomerEditComponent.contactPositionField", controller = "getCustomerForEdit"),
          @AuthField(frontendId = "CustomerEditComponent.contactPositionField", controller = "updateCustomer"),
          @AuthField(controller = "getContacts"),
          @AuthField(controller = "getCustomers"),
          @AuthField(controller = "getCustomerForEditInContactInfo"),
          @AuthField(frontendId = "CustomerContactsComponent.position", controller = "getCustomerWithContacts"),
  })
  @NotNull
  private String position;

  @AuthFields(list = {
          @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "createCustomer"),
          @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "getCustomerForEdit"),
          @AuthField(frontendId = "CustomerAddComponent.firstToContactField", controller = "updateCustomer"),
          @AuthField(controller = "getContacts"),
          @AuthField(controller = "getCustomers"),
          @AuthField(controller = "getCustomerForEditInContactInfo"),
          @AuthField(controller = "getCustomerWithContacts"),
  })
  private Boolean mainContact;

  @AuthFields(list = {
          @AuthField(controller = "getContacts"),
          @AuthField(frontendId = "CustomerContactsComponent.owner", controller = "getCustomerWithContacts"),
  })
  private Long owner;

  @AuthFields(list = {
          @AuthField(controller = "createCustomer"),
          @AuthField(controller = "getCustomerForEdit"),
          @AuthField(controller = "updateCustomer"),
          @AuthField(controller = "getCustomerWithContacts")
  })
  private String avatar;

  public static ContactDto fromModel(Contact contact) {
    ContactDto dto = new ContactDto();
    dto.setId(contact.getId());
    dto.setName(contact.getName());
    dto.setSurname(contact.getSurname());
    dto.setEmail(contact.getEmail());
    dto.setPhone(contact.getPhone());
    dto.setMainContact(contact.getMainContact());
    dto.setOwner(contact.getOwner());
    dto.setPosition(contact.getPosition());
    dto.setAvatar(contact.getAvatar());
    return dto;
  }

  public Contact toModel(Contact fromDb, String customerName) {
    Contact contact = (fromDb == null) ? new Contact() : fromDb;
    contact.setId(this.getId());
    if (contact.getId() == null) {
      contact.setCreated(LocalDateTime.now());
    }
    contact.setName(this.getName());
    contact.setSurname(this.getSurname());
    contact.setEmail(this.getEmail());
    contact.setPhone(this.getPhone());
    contact.setMainContact(Optional.ofNullable(this.mainContact).orElse(false));
    contact.setOwner(this.getOwner());
    contact.setPosition(this.getPosition());
    contact.setAvatar(this.getAvatar());
    return contact;
  }

}

