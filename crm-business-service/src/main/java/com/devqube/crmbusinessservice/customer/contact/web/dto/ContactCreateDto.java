package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmbusinessservice.phoneNumber.web.dto.PhoneNumberDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactCreateDto {

    private Long customerId;
    @NotNull
    private String name;
    @NotNull
    private String surname;
    @NotNull
    private String position;
    private String phone;
    private Set<PhoneNumberDTO> additionalPhones = new HashSet<>();
    private String email;
    private String facebookLink;
    private String twitterLink;
    private String linkedInLink;
    private String avatar;
    private String additionalData;

}
