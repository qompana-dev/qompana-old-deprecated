package com.devqube.crmbusinessservice.customer.contact;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long>, JpaSpecificationExecutor<Contact> {
    Contact findContactById(Long id);

    @Query("select c from Contact c where c.id not in :ids")
    List<Contact> findAllOtherThan(@Param("ids") Set<Long> ids);

    Page<Contact> findAll(Pageable pageable);

    @Query("select c from Contact c where c.created > :date")
    List<Contact> findAllByCreatedAfterForMobile(@Param("date") LocalDateTime date);

    @Query("select c from Contact c where c.updated > :date")
    List<Contact> findAllByUpdatedAfterForMobile(@Param("date") LocalDateTime date);

    @Query("select c from Contact c " +
            "where (select max(lvc.lastViewed) from LastViewedContact lvc where lvc.contact.id = c.id and lvc.userId = :userId) > :date ")
    List<Contact> findAllByLastViewedAfterForMobile(@Param("date") LocalDateTime date, @Param("userId") Long userId);

    @Query("SELECT c FROM Contact c WHERE LOWER(c.name) LIKE LOWER(concat('%', ?1, '%')) or LOWER(c.surname) LIKE LOWER(concat('%', ?1, '%'))")
    List<Contact> findByNameOrSurnameContaining(String pattern);

    @Query("SELECT c FROM Contact c WHERE c.id in ?1")
    List<Contact> findByIds(List<Long> ids);

    @Query("SELECT c FROM Contact c WHERE " +
            "(LOWER(c.name) = lower(:firstName) and LOWER(c.surname) = lower(:lastName)) or " +
            "(c.email != '' and c.email is not null and LOWER(c.email) = lower(:email)) or " +
            "(c.phone != '' and c.phone is not null and LOWER(c.phone) = lower(:phone))")
    List<Contact> searchContactsForLeadCreation(@Param("firstName") String firstName,
                                                @Param("lastName") String lastName,
                                                @Param("email") String email,
                                                @Param("phone") String phone);

    @Query("select count(c) from Contact c where c.created >= :from and c.created <= :to and c.creator in :userIds")
    Long findForGoal(@Param("from") LocalDateTime from, @Param("to") LocalDateTime to, @Param("userIds") Set<Long> userIds);

    @Query("select c from Contact c " +
            "left join c.gdprList g " +
            "left join g.gdprApproves ga " +
            "left join ga.gdprStatus gs " +
            "left join ga.gdprType gt " +
            "where LOWER(c.email) LIKE LOWER(concat('%', ?1, '%')) " +
            "and (gs.defaultStatus = com.devqube.crmbusinessservice.gdpr.model.GdprDefaultStatus.CHECKED and gt.isMailType = true)")
    List<Contact> findByEmailContaining(String term);

    @Query("select distinct(c2.email) from Contact c2 where c2.email not in " +
            "(select distinct(c.email) from Contact c " +
            "left join c.gdprList g " +
            "left join g.gdprApproves ga " +
            "left join ga.gdprStatus gs " +
            "left join ga.gdprType gt " +
            "where c.email is not null " +
            "and (gs.defaultStatus = com.devqube.crmbusinessservice.gdpr.model.GdprDefaultStatus.CHECKED and gt.isMailType = true))")
    List<String> findAllContactMailWithoutEmailAgreements();

    Long countAllByEmail(String email);
}

