package com.devqube.crmbusinessservice.customer.type;

import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Type {
    @Id
    @SequenceGenerator(name="type_seq", sequenceName="type_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="type_seq")
    @EqualsAndHashCode.Include
    private Long id;

    @Column(name="name", unique = true)
    @Length(max = 200)
    @EqualsAndHashCode.Include
    private String name;

    @Length(max = 500)
    private String description;

    @OneToMany(mappedBy="type")
    @JsonIgnore
    private List<Customer> customers;

    @Override
    public String toString() {
        return "Type{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
