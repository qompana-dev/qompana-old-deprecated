package com.devqube.crmbusinessservice.customer.customer.web.dto;

import com.devqube.crmbusinessservice.customer.address.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerPreviewDto {
    private Long id;

    private String name;

    private String phone;

    private Long owner;

    private String ownerName;

    private String ownerSurname;

    private Address address;

    private String website;

    private List<CustomerPreviewOpportunityDto> previewOpportunityDtoList;
}
