package com.devqube.crmbusinessservice.customer.contact.web;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.contact.web.dto.*;
import com.devqube.crmbusinessservice.customer.customer.web.dto.ContactDto;
import com.devqube.crmbusinessservice.customer.customer.web.dto.CustomerSearchVariantsDtoOut;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.EmailSearch;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "Contacts")
public interface ContactsApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Create new contacts", nickname = "createContact", notes = "Method used to add new contact to user contacts list", response = Contact.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "added new contact successfully", response = Contact.class),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 404, message = "customer not found")})
    @RequestMapping(value = "/contacts",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Contact> createContact(@ApiParam(value = "Created customer object", required = true) @Valid @RequestBody ContactCreateDto contact) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"employmentHistory\" : [ {    \"positon\" : \"positon\",    \"company\" : \"company\",    \"id\" : 1,    \"startDate\" : \"startDate\",    \"stopDate\" : \"stopDate\"  }, {    \"positon\" : \"positon\",    \"company\" : \"company\",    \"id\" : 1,    \"startDate\" : \"startDate\",    \"stopDate\" : \"stopDate\"  } ],  \"phone\" : 6.02745618307040320615897144307382404804229736328125,  \"surname\" : \"surname\",  \"name\" : \"name\",  \"description\" : \"description\",  \"id\" : 0,  \"email\" : \"email\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "Create new contacts with relation")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "added new contact successfully", response = Contact.class),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 404, message = "customer not found")})
    ResponseEntity<ContactWithRelationDto> createContactWithRelation(@RequestBody ContactWithRelationDto contactWithRelationDto);

    @ApiOperation(value = "Create new contacts", nickname = "createContactForZapier", notes = "Method used to add new contact to user contacts list", response = Contact.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "added new contact successfully", response = Contact.class),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 404, message = "customer not found")})
    @RequestMapping(value = "/zapier/contacts",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<Contact> createContactForZapier(@ApiParam(value = "Created customer object", required = true) @Valid @RequestBody ContactCreateDto contact);

    @ApiOperation(value = "Create new contact link", nickname = "createContactLink", notes = "Method used to add new contact link", response = ContactLinkDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "added new contact link successfully", response = ContactLinkDto.class),
            @ApiResponse(code = 404, message = "contact not found"),
            @ApiResponse(code = 409, message = "conflict")})
    @RequestMapping(value = "/contacts/{contactId}/link/{linkedContactId}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<ContactLinkDto> createContactLink(@ApiParam(value = "The contact ID", required = true) @PathVariable("contactId") Long contactId, @ApiParam(value = "The linked contact ID", required = true) @PathVariable("linkedContactId") Long linkedContactId, @ApiParam(value = "Contact link detail", required = true) @Valid @RequestBody CreateContactLinkDto createContactLinkDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"note\" : \"note\",  \"linkedContactId\" : 1,  \"baseContactId\" : 6,  \"id\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Delete contact link", nickname = "deleteContactLink", notes = "Method used to delete contact link")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "contact link removed successfully"),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/contacts/{contactId}/link/{linkedContactId}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteContactLink(@ApiParam(value = "The contact ID", required = true) @PathVariable("contactId") Long contactId, @ApiParam(value = "The linked contact ID", required = true) @PathVariable("linkedContactId") Long linkedContactId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get all contacts", nickname = "getAllContacts", notes = "Method used to get all contacts", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all contacts", response = org.springframework.data.domain.Page.class)
    })
    @RequestMapping(value = "/contacts",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Page<ContactOnListDTO>> getAllContacts(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                                @ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable,
                                                @ApiParam(value = "only todays contacts") @Valid @RequestParam(value = "today", required = false) Boolean today,
                                                @ApiParam(value = "only last edited contacts") @Valid @RequestParam(value = "edited", required = false) Boolean edited,
                                                @ApiParam(value = "only last viewed contacts") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed,
                                                @ApiParam(value = "only archived contacts") @Valid @RequestParam(value = "archived", required = false) Boolean archived,
                                                @ApiParam(value = "searching") @Valid @RequestParam(value = "search", required = false) String search) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get all contacts for mobile application", nickname = "getAllContactsforMobile", notes = "Method used to get all contacts for mobile application", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all contacts", response = ContactForMobileListDto[].class)
    })
    @RequestMapping(value = "/mobile/contacts",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<ContactForMobileListDto>> getAllContactsForMobile(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "only todays tasks") @Valid @RequestParam(value = "today", required = false) Boolean today, @ApiParam(value = "only last edited tasks") @Valid @RequestParam(value = "edited", required = false) Boolean edited, @ApiParam(value = "only last viewed tasks") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get all contacts", nickname = "getAllContactsForZapier", notes = "Method used to get all contacts", response = Contact[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all contacts", response = Contact[].class)
    })
    @RequestMapping(value = "/zapier/contacts",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<ContactOnListDTO>> getAllContactsForZapier(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                                                  @ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable,
                                                                  @ApiParam(value = "only todays contacts") @Valid @RequestParam(value = "today", required = false) Boolean today,
                                                                  @ApiParam(value = "only last edited contacts") @Valid @RequestParam(value = "edited", required = false) Boolean edited,
                                                                  @ApiParam(value = "only last viewed contacts") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed,
                                                                  @ApiParam(value = "only archived contacts") @Valid @RequestParam(value = "archived", required = false) Boolean archived,
                                                                              @ApiParam(value = "searching") @Valid @RequestParam(value = "search", required = false) String search) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "delete contacts", nickname = "deleteContacts", notes = "Method used to delete contacts")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "deleted contacts successfully"),
            @ApiResponse(code = 400, message = "cannot delete contact cause it's used in opportunity"),
            @ApiResponse(code = 404, message = "one of contacts not found")})
    @RequestMapping(value = "/contacts",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteContacts(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get specific contact", nickname = "getContact", notes = "Method used to get specific contact by Id", response = Contact.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "contact found successfully", response = Contact.class),
            @ApiResponse(code = 400, message = "logged user not found"),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/contacts/{contactId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Contact> getContact(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "Contact ID", required = true) @PathVariable("contactId") Long contactId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"employmentHistory\" : [ {    \"positon\" : \"positon\",    \"company\" : \"company\",    \"id\" : 1,    \"startDate\" : \"startDate\",    \"stopDate\" : \"stopDate\"  }, {    \"positon\" : \"positon\",    \"company\" : \"company\",    \"id\" : 1,    \"startDate\" : \"startDate\",    \"stopDate\" : \"stopDate\"  } ],  \"phone\" : 6.02745618307040320615897144307382404804229736328125,  \"surname\" : \"surname\",  \"name\" : \"name\",  \"description\" : \"description\",  \"id\" : 0,  \"email\" : \"email\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Check access to contact group", nickname = "getContactAccess", notes = "Endpoint just for testing file upload. Will be implemented in upload file story", response = Boolean.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Access to group checked successfully", response = Boolean.class),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/access/contact/{contactId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Boolean> getContactAccess(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "Contact ID", required = true) @PathVariable("contactId") Long contactId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get customer's contacts", nickname = "getContacts", notes = "Method used to get customer's contacts", response = ContactDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all customer's contacts", response = ContactDto.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "customer not found")})
    @RequestMapping(value = "/customer/{customerId}/contacts",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<ContactDto>> getContacts(@ApiParam(value = "The customer ID", required = true) @PathVariable("customerId") Long customerId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"phone\" : \"phone\",  \"surname\" : \"surname\",  \"name\" : \"name\",  \"id\" : 5,  \"position\" : \"position\",  \"email\" : \"email\",  \"primary\" : true}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Remove contact", nickname = "removeContact", notes = "Method used to remove contact.")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "contact removed successfully"),
            @ApiResponse(code = 400, message = "cannot delete contact cause it's used in opportunity"),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/contacts/{contactId}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> removeContact(@ApiParam(value = "Contact ID", required = true) @PathVariable("contactId") Long contactId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update customer's contact", nickname = "updateContact", notes = "Method used to modify customer's contact.", response = Contact.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "contact updated successfully", response = Contact.class),
            @ApiResponse(code = 400, message = "validation error", response = String.class),
            @ApiResponse(code = 404, message = "customer or contact not found")})
    @RequestMapping(value = "/contacts/{contactId}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Contact> updateContact(@ApiParam(value = "Contact ID", required = true) @PathVariable("contactId") Long contactId, @ApiParam(value = "Modified contact object", required = true) @Valid @RequestBody Contact contact) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"employmentHistory\" : [ {    \"positon\" : \"positon\",    \"company\" : \"company\",    \"id\" : 1,    \"startDate\" : \"startDate\",    \"stopDate\" : \"stopDate\"  }, {    \"positon\" : \"positon\",    \"company\" : \"company\",    \"id\" : 1,    \"startDate\" : \"startDate\",    \"stopDate\" : \"stopDate\"  } ],  \"phone\" : 6.02745618307040320615897144307382404804229736328125,  \"surname\" : \"surname\",  \"name\" : \"name\",  \"description\" : \"description\",  \"id\" : 0,  \"email\" : \"email\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
    @ApiOperation(value = "archive contact.", nickname = "archiveContact", notes = "Method used to archive contact.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "contact archived"),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/contact/{contactId}/archive",
            method = RequestMethod.PUT)
    default ResponseEntity<Void> archiveContact(@ApiParam(value = "", required = true)
                                                 @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                                 @ApiParam(value = "The contact ID", required = true) @PathVariable("contactId") Long contactId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "archive contacts", nickname = "archiveContacts", notes = "Method used to archive several contacts.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "contact archived"),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/contact/archive",
            method = RequestMethod.PUT)
    default ResponseEntity<Void> archiveContacts(@ApiParam(value = "", required = true)
                                                  @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                                  @ApiParam(value = "", defaultValue = "new ArrayList<>()")
                                                  @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
    @ApiOperation(value = "Update customer's contact", nickname = "updateContactForZapier", notes = "Method used to modify customer's contact.", response = Contact.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "contact updated successfully", response = Contact.class),
            @ApiResponse(code = 400, message = "validation error", response = String.class),
            @ApiResponse(code = 404, message = "customer or contact not found")})
    @RequestMapping(value = "/zapier/contacts/{contactId}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    ResponseEntity<Contact> updateContactForZapier(@ApiParam(value = "Contact ID", required = true) @PathVariable("contactId") Long contactId, @ApiParam(value = "Modified contact object", required = true) @Valid @RequestBody ContactForZapierDto contact);

    @ApiOperation(value = "get all contacts with access for user", nickname = "getAllContactsWithAccess", notes = "Endpoint just for testing file upload. Will be implemented in upload file story", response = Long.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of contacts for specific user", response = Long.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Email not correct")})
    @RequestMapping(value = "/access/contact",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<Long>> getAllContactsWithAccess(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "0");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get all contacts possible to connect", nickname = "getAllPossibleLinks", notes = "Method used to get all contacts possible to connect", response = Contact.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "contacts", response = Contact.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/contacts/{contactId}/possible-links",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<Contact>> getAllPossibleLinks(@ApiParam(value = "The contact ID", required = true) @PathVariable("contactId") Long contactId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"employmentHistory\" : [ {    \"positon\" : \"positon\",    \"company\" : \"company\",    \"id\" : 1,    \"startDate\" : \"startDate\",    \"stopDate\" : \"stopDate\"  }, {    \"positon\" : \"positon\",    \"company\" : \"company\",    \"id\" : 1,    \"startDate\" : \"startDate\",    \"stopDate\" : \"stopDate\"  } ],  \"phone\" : 6.02745618307040320615897144307382404804229736328125,  \"surname\" : \"surname\",  \"name\" : \"name\",  \"description\" : \"description\",  \"id\" : 0,  \"email\" : \"email\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Check access to attach file to  contact group", nickname = "getContactPermissionToAttachFile", notes = "Endpoint just for testing file attaching file. Will be implemented in upload file story", response = Boolean.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Access to group checked successfully", response = Boolean.class),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/access/contact/{contactId}/attach",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Boolean> getContactPermissionToAttachFile(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "Contact ID", required = true) @PathVariable("contactId") Long contactId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Check access to detach file to  contact group", nickname = "getContactPermissionToDetachFile", notes = "Endpoint just for testing file detaching file. Will be implemented in upload file story", response = Boolean.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Access to group checked successfully", response = Boolean.class),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/access/contact/{contactId}/detach",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Boolean> getContactPermissionToDetachFile(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "Contact ID", required = true) @PathVariable("contactId") Long contactId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Check if the contact exist", nickname = "contactExist", notes = "Endpoint just for testing attaching file. Will be implemented in upload file story", response = Boolean.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Access to group checked successfully", response = Boolean.class)})
    @RequestMapping(value = "/access/contact/{contactId}/exist",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Boolean> contactExist(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "Contact ID", required = true) @PathVariable("contactId") Long contactId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "Get contacts list for search", nickname = "getContactsForSearch", notes = "Method used to get contact list", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all contact names", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/contacts/search",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CrmObject>> getContactsForSearch(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
    @ApiOperation(value = "Get list of contacts for search", nickname = "getSearchVariants",
            notes = "Method used to get search variants", response = ContactSearchVariantsDtoOut.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all search variants", response = ContactSearchVariantsDtoOut.class, responseContainer = "List")})
    @RequestMapping(value = "/contacts/search-variants",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<ContactSearchVariantsDtoOut>> getSearchVariants(
            @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "search") String toSearch) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get contacts page", nickname = "getContactsPage", notes = "Method used to get contact page", response = ContactTaskDtoOut.class, responseContainer = "Page")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "contacts page", response = ContactTaskDtoOut.class, responseContainer = "Page")})
    @RequestMapping(value = "/contacts/search/task",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Page<ContactTaskDtoOut>> getContactsPage(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term,
                                                                    @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get contacts list for search", nickname = "getContactsAndCustomerForSearch", notes = "Method used to get contact list for search", response = ContactAndCustomerNameDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all contact names", response = ContactAndCustomerNameDto.class, responseContainer = "List")})
    @RequestMapping(value = "/contacts/search/with-customer",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<ContactAndCustomerNameDto>> getContactsAndCustomerForSearch(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"contactName\" : \"contactName\",  \"id\" : 0,  \"customerName\" : \"customerName\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get contacts list for mobile association list", nickname = "getContactsForMobileAssociationList", response = ContactAssociationDto[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all contact names", response = ContactAssociationDto[].class)})
    @RequestMapping(value = "/mobile/contacts/for-association-list",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<ContactAssociationDto>> getContactsForMobileAssociationList() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get contacts list by ids", nickname = "getContactsAsCrmObject", notes = "Method used to get contacts as crm objects by ids", response = CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found contact", response = CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/contacts/crm-object",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CrmObject>> getContactsAsCrmObject(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get contact info for top bar", nickname = "getContactForTopBar", notes = "Method used to get contact info for top bar", response = com.devqube.crmbusinessservice.customer.contact.web.dto.ContactTopBarDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "contact found successfully", response = com.devqube.crmbusinessservice.customer.contact.web.dto.ContactTopBarDTO.class),
            @ApiResponse(code = 400, message = "logged user not found"),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/contacts/{contactId}/top-bar",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<com.devqube.crmbusinessservice.customer.contact.web.dto.ContactTopBarDTO> getContactForTopBar(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "Contact ID", required = true) @PathVariable("contactId") Long contactId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get opportunities for contact", nickname = "getContactOpportunities", notes = "Method used to get opportunities for contact", response = com.devqube.crmbusinessservice.customer.contact.web.dto.ContactSalesOpportunityDTO.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "contact's opportunities found successfully", response = com.devqube.crmbusinessservice.customer.contact.web.dto.ContactSalesOpportunityDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "logged user not found"),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/contacts/{contactId}/opportunities",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<ContactSalesOpportunityDTO>> getContactOpportunities(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "Contact ID", required = true) @PathVariable("contactId") Long contactId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get opportunities preview for contact", nickname = "getContactOpportunitiesPreview", notes = "Method used to get opportunities preview for contact", response = com.devqube.crmbusinessservice.customer.contact.web.dto.ContactSalesOpportunityPreviewDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "contact's opportunities found successfully", response = ContactSalesOpportunityPreviewDTO.class),
            @ApiResponse(code = 400, message = "logged user not found"),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/contacts/{contactId}/opportunities-preview",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<ContactSalesOpportunityPreviewDTO> getContactOpportunitiesPreview(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "Contact ID", required = true) @PathVariable("contactId") Long contactId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get links for contact", nickname = "getContactLinks", notes = "Method used to get contact links", response = ContactLinkDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all links types", response = ContactLinkDto.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/contacts/{contactId}/links",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<ContactLinkDto>> getContactLinks(@ApiParam(value = "The contact ID", required = true) @PathVariable("contactId") Long contactId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"linkedContactSurname\" : \"linkedContactSurname\",  \"linkedContactPhone\" : \"linkedContactPhone\",  \"note\" : \"note\",  \"linkedContactId\" : 1,  \"baseContactId\" : 6,  \"baseContactSurname\" : \"baseContactSurname\",  \"linkedContactName\" : \"linkedContactName\",  \"linkedCustomerName\" : \"linkedCustomerName\",  \"baseContactName\" : \"baseContactName\",  \"id\" : 0,  \"linkedContactEmail\" : \"linkedContactEmail\",  \"position\" : \"position\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get contact link", nickname = "getContactLink", notes = "Method used to get contact link;", response = ContactLinkDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "added new contact link successfully", response = ContactLinkDto.class),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/contacts/{contactId}/link/{linkedContactId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<ContactLinkDto> getContactLink(@ApiParam(value = "The contact ID", required = true) @PathVariable("contactId") Long contactId, @ApiParam(value = "The linked contact ID", required = true) @PathVariable("linkedContactId") Long linkedContactId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"linkedContactSurname\" : \"linkedContactSurname\",  \"linkedContactPhone\" : \"linkedContactPhone\",  \"note\" : \"note\",  \"linkedContactId\" : 1,  \"baseContactId\" : 6,  \"baseContactSurname\" : \"baseContactSurname\",  \"linkedContactName\" : \"linkedContactName\",  \"linkedContactPosition\" : \"linkedContactPosition\",  \"linkedCustomerName\" : \"linkedCustomerName\",  \"baseContactName\" : \"baseContactName\",  \"id\" : 0,  \"linkedContactEmail\" : \"linkedContactEmail\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update contact link", nickname = "updateContactLink", notes = "Method used to update contact link", response = ContactLinkDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "added new contact link successfully", response = ContactLinkDto.class),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/contacts/{contactId}/link/{linkedContactId}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<ContactLinkDto> updateContactLink(@ApiParam(value = "The contact ID", required = true) @PathVariable("contactId") Long contactId, @ApiParam(value = "The linked contact ID", required = true) @PathVariable("linkedContactId") Long linkedContactId, @ApiParam(value = "Contact link detail", required = true) @Valid @RequestBody CreateContactLinkDto createContactLinkDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"linkedContactSurname\" : \"linkedContactSurname\",  \"linkedContactPhone\" : \"linkedContactPhone\",  \"note\" : \"note\",  \"linkedContactId\" : 1,  \"baseContactId\" : 6,  \"baseContactSurname\" : \"baseContactSurname\",  \"linkedContactName\" : \"linkedContactName\",  \"linkedContactPosition\" : \"linkedContactPosition\",  \"linkedCustomerName\" : \"linkedCustomerName\",  \"baseContactName\" : \"baseContactName\",  \"id\" : 0,  \"linkedContactEmail\" : \"linkedContactEmail\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get 3 links for contact and total count", nickname = "getContactLinksPreview", notes = "Method used to get contact links preview", response = ContactLinkPreviewDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all links types", response = ContactLinkPreviewDto.class),
            @ApiResponse(code = 404, message = "contact not found")})
    @RequestMapping(value = "/contacts/{contactId}/links/preview",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<ContactLinkPreviewDto> getContactLinksPreview(@ApiParam(value = "The contact ID", required = true) @PathVariable("contactId") Long contactId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"count\" : 0,  \"links\" : [ {    \"linkedContactSurname\" : \"linkedContactSurname\",    \"linkedContactPhone\" : \"linkedContactPhone\",    \"note\" : \"note\",    \"linkedContactId\" : 5,    \"baseContactId\" : 1,    \"baseContactSurname\" : \"baseContactSurname\",    \"linkedContactName\" : \"linkedContactName\",    \"linkedContactPosition\" : \"linkedContactPosition\",    \"linkedCustomerName\" : \"linkedCustomerName\",    \"baseContactName\" : \"baseContactName\",    \"id\" : 6,    \"linkedContactEmail\" : \"linkedContactEmail\"  }, {    \"linkedContactSurname\" : \"linkedContactSurname\",    \"linkedContactPhone\" : \"linkedContactPhone\",    \"note\" : \"note\",    \"linkedContactId\" : 5,    \"baseContactId\" : 1,    \"baseContactSurname\" : \"baseContactSurname\",    \"linkedContactName\" : \"linkedContactName\",    \"linkedContactPosition\" : \"linkedContactPosition\",    \"linkedCustomerName\" : \"linkedCustomerName\",    \"baseContactName\" : \"baseContactName\",    \"id\" : 6,    \"linkedContactEmail\" : \"linkedContactEmail\"  } ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "search mails")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account emails", response = List.class)
    })
    List<EmailSearch> searchEmails(String email, String term);

    @ApiOperation(value = "get contact emails without email gdpr type")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "emails List", response = List.class)
    })
    List<String> getContactEmailWithoutGdprAgree();

    @ApiOperation(value = "get contact name")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "contact name", response = String.class)
    })
    String getContactName(Long contactId) throws EntityNotFoundException;


    @ApiOperation(value = "get contact with relation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "contact returned successfully"),
            @ApiResponse(code = 404, message = "contact not found")
    })
    ResponseEntity<ContactWithRelationDto> getContactWithRelation(Long contactId);

    @ApiOperation(value = "update contact with relation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account emails", response = List.class)
    })
    ResponseEntity<ContactWithRelationDto> updateContactWithRelation(Long contactId, ContactWithRelationDto dto);
}
