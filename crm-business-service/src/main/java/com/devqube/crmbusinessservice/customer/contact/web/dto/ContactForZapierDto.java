package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContactForZapierDto {
    @JsonProperty("id")
    @Id
    @SequenceGenerator(name = "contact_seq", sequenceName = "contact_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_seq")
    @AuthFields(list = {
            @AuthField(controller = "getAllContacts"),
            @AuthField(controller = "createContact"),
            @AuthField(controller = "updateContact"),
            @AuthField(controller = "updateContactInContactInfo"),
            @AuthField(controller = "getContact"),
            @AuthField(controller = "getContactInContactInfo"),
            @AuthField(frontendId = "CustomerAddComponent.firstToContactField", controller = "createCustomer"),
            @AuthField(controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateAccount"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateCustomer"),
    })
    private Long id;

    @NotNull
    @AuthFields(list = {
            @AuthField(controller = "createContact"),
            @AuthField(controller = "updateContact"),
            @AuthField(controller = "updateContactInContactInfo"),
            @AuthField(controller = "getContact"),
            @AuthField(controller = "getContactInContactInfo"),
            @AuthField(controller = "updateContact"),
            @AuthField(controller = "getAllContacts"),
            @AuthField(controller = "getContactForNewLinkPanel")
    })
    private Long customerId;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactListComponent.employmentHistoryColumn", controller = "getAllContacts"),
            @AuthField(frontendId = "ContactAddComponent.positionField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.positionField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.positionField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.positionField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.positionField", controller = "getContactInContactInfo"),
    })
    private String position;

    @JsonProperty("name")
    @Length(max = 200)
    @Column(name = "name")
    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "ContactListComponent.nameColumn", controller = "getAllContacts"),
            @AuthField(frontendId = "ContactAddComponent.nameField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.nameField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.nameField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.nameField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.nameField", controller = "getContactInContactInfo"),
            @AuthField(frontendId = "CustomerAddComponent.firstToContactField", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerListComponent.firstToContactNameColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateAccount"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateCustomer"),
    })
    private String name;

    @JsonProperty("surname")
    @Length(max = 200)
    @Column(name = "surname")
    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "ContactListComponent.nameColumn", controller = "getAllContacts"),
            @AuthField(frontendId = "ContactAddComponent.surnameField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.surnameField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.surnameField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.surnameField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.surnameField", controller = "getContactInContactInfo"),
            @AuthField(frontendId = "CustomerAddComponent.firstToContactField", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerListComponent.firstToContactNameColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateAccount"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateCustomer"),
    })
    private String surname;

    @JsonProperty("description")
    @Length(max = 500)
    @Column(name = "description")
    private String description;

    @JsonProperty("department")
    @Length(max = 200)
    @Column(name = "department")
    @AuthFields(list = {
            @AuthField(frontendId = "ContactAddComponent.departmentField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.departmentField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.departmentField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.departmentField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.departmentField", controller = "getContactInContactInfo")
    })
    private String department;

    @JsonProperty("email")
    @Length(max = 100)
    @Column(name = "email")
    @AuthFields(list = {
            @AuthField(frontendId = "ContactListComponent.emailColumn", controller = "getAllContacts"),
            @AuthField(frontendId = "ContactAddComponent.emailField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.emailField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.emailField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.emailField", controller = "getContact"),
            @AuthField(frontendId = "ContactEditComponent.emailField", controller = "getContactInContactInfo"),
            @AuthField(frontendId = "CustomerListComponent.firstToContactEmailColumn", controller = "getCustomers"),
    })
    private String email;

    @JsonProperty("phone")
    @Column(name = "phone")
    @AuthFields(list = {
            @AuthField(frontendId = "ContactListComponent.phoneColumn", controller = "getAllContacts"),
            @AuthField(frontendId = "ContactAddComponent.phoneField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.phoneField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.phoneField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.phoneField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.phoneField", controller = "getContactInContactInfo"),
            @AuthField(frontendId = "CustomerListComponent.firstToContactPhoneColumn", controller = "getCustomers"),
    })
    private String phone;


    @AuthFields(list = {
            @AuthField(controller = "getContact"),
            @AuthField(frontendId = "ContactListComponent.ownerColumn", controller = "getAllContacts"),
            @AuthField(controller = "getContactForNewLinkPanel")
    })
    private Long owner;

    @AuthFields(list = {
            @AuthField(controller = "getContact"),
            @AuthField(controller = "createContact")
    })
    @CreationTimestamp
    private LocalDateTime created;

    @AuthFields(list = {
            @AuthField(controller = "getContact"),
            @AuthField(controller = "createContact")
    })
    private Long creator;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactAddComponent.socialMediaField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.socialMediaField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.socialMediaField", controller = "getContactInContactInfo"),
    })
    private String facebookLink;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactAddComponent.socialMediaField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.socialMediaField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.socialMediaField", controller = "getContactInContactInfo"),
    })
    private String twitterLink;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactAddComponent.socialMediaField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.socialMediaField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.socialMediaField", controller = "getContactInContactInfo"),
    })
    private String linkedInLink;
}
