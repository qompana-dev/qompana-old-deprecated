package com.devqube.crmbusinessservice.customer.contact.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class LastViewedContact {
    @Id
    @SequenceGenerator(name = "last_viewed_contact_seq", sequenceName = "last_viewed_contact_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "last_viewed_contact_seq")
    @EqualsAndHashCode.Include
    private Long id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "contact_id")
    @NotNull
    private Contact contact;

    @Column(name = "user_id")
    @NotNull
    private Long userId;

    @Column(name = "last_viewed")
    @NotNull
    private LocalDateTime lastViewed;
}
