package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@AuthFilterEnabled
public class ContactLinkPreviewDto {
    @AuthField(controller = "getContactLinks")
    private Long count;
    @AuthField(controller = "getContactLinks")
    private List<ContactLinkDto> links = null;
}
