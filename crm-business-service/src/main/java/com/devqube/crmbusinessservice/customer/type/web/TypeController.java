package com.devqube.crmbusinessservice.customer.type.web;

import com.devqube.crmbusinessservice.customer.type.Type;
import com.devqube.crmbusinessservice.customer.type.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TypeController implements TypeApi {
    @Autowired
    private TypeService typeService;

    @Override
    public ResponseEntity<List<Type>> getTypes() {
        return new ResponseEntity<>(typeService.getAll(), HttpStatus.OK);
    }
}
