package com.devqube.crmbusinessservice.customer.contact;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.contact.web.dto.ContactWithRelationDto;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.ContactCustomerLink;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.ContactCustomerLinkRepository;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.RelationType;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class ContactCustomerRelationService {

    private final ContactCustomerLinkRepository contactCustomerLinkRepository;

    public ContactCustomerRelationService(ContactCustomerLinkRepository contactCustomerLinkRepository) {
        this.contactCustomerLinkRepository = contactCustomerLinkRepository;
    }

    public ContactCustomerLink saveNewLink(Contact contact, ContactWithRelationDto dto) {
        ContactCustomerLink link = new ContactCustomerLink();
        link.setPosition(null);
        link.setStartDate(dto.getStartDate());
        link.setRelationType(RelationType.DIRECT);
        link.setNote(dto.getNote());
        link.setCustomer(contact.getCustomer());
        link.setContact(contact);
        return this.contactCustomerLinkRepository.save(link);
    }

    public ContactCustomerLink saveNewLink(Contact contact) {
        return saveNewLink(contact.getCustomer(), contact);
    }

    public ContactCustomerLink saveNewLink(Customer customer, Contact contact) {
        ContactCustomerLink link = new ContactCustomerLink();
        link.setPosition(null);
        link.setStartDate(LocalDate.now());
        link.setRelationType(RelationType.DIRECT);
        link.setCustomer(customer);
        link.setContact(contact);
        return this.contactCustomerLinkRepository.save(link);
    }

    @Transactional
    public void finishCurrentMainLink(Long customerId, Long contactId, String position) {
        Optional<ContactCustomerLink> mainRelation = getMainRelation(customerId, contactId);
        mainRelation.ifPresent(relation -> {
            relation.setEndDate(LocalDate.now());
            relation.setPosition(position);
        });
    }

    public Optional<ContactCustomerLink> getMainRelation(Long customerId, Long contactId) {
        return contactCustomerLinkRepository.findMainRelation(customerId, contactId);
    }

    @Transactional
    public ContactCustomerLink updateMainRelation(@NotNull Long customerId, @NotNull Long contactId, ContactWithRelationDto dto) {
        Optional<ContactCustomerLink> linkOpt = contactCustomerLinkRepository.findMainRelation(customerId, contactId);
        if (linkOpt.isPresent()) {
            ContactCustomerLink link = linkOpt.get();
            link.setNote(dto.getNote());
            link.setStartDate(dto.getStartDate());
            return link;
        }else {
            return null;
        }
    }


}
