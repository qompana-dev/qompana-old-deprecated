package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@AuthFilterEnabled
public class ContactWithRelationDto {

    @AuthFields(list = {
            @AuthField(controller = "getContactWithRelation"),
            @AuthField(controller = "updateContactWithRelation"),
            @AuthField(controller = "createContactWithRelation"),
    })
    private Long customerId;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactEditComponent.nameField", controller = "getContactWithRelation"),
            @AuthField(frontendId = "ContactEditComponent.nameField", controller = "updateContactWithRelation"),
            @AuthField(frontendId = "ContactAddComponent.nameField", controller = "createContactWithRelation"),
    })
    private String name;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactEditComponent.surnameField", controller = "getContactWithRelation"),
            @AuthField(frontendId = "ContactEditComponent.surnameField", controller = "updateContactWithRelation"),
            @AuthField(frontendId = "ContactAddComponent.surnameField", controller = "createContactWithRelation"),
    })
    private String surname;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactEditComponent.emailField", controller = "getContactWithRelation"),
            @AuthField(frontendId = "ContactEditComponent.emailField", controller = "updateContactWithRelation"),
            @AuthField(frontendId = "ContactAddComponent.emailField", controller = "createContactWithRelation"),
    })
    private String email;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactEditComponent.phoneField", controller = "getContactWithRelation"),
            @AuthField(frontendId = "ContactEditComponent.phoneField", controller = "updateContactWithRelation"),
            @AuthField(frontendId = "ContactAddComponent.phoneField", controller = "createContactWithRelation"),
    })
    private String phone;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "getContactWithRelation"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "updateContactWithRelation"),
            @AuthField(frontendId = "ContactAddComponent.socialMediaField", controller = "createContactWithRelation"),
    })
    private String facebookLink;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "getContactWithRelation"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "updateContactWithRelation"),
            @AuthField(frontendId = "ContactAddComponent.socialMediaField", controller = "createContactWithRelation"),
    })
    private String twitterLink;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "getContactWithRelation"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "updateContactWithRelation"),
            @AuthField(frontendId = "ContactAddComponent.socialMediaField", controller = "createContactWithRelation"),
    })
    private String linkedInLink;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactEditComponent.positionField", controller = "getContactWithRelation"),
            @AuthField(frontendId = "ContactEditComponent.positionField", controller = "updateContactWithRelation"),
            @AuthField(frontendId = "ContactAddComponent.positionField", controller = "createContactWithRelation"),
    })
    private String position;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactEditComponent.relationTime", controller = "getContactWithRelation"),
            @AuthField(frontendId = "ContactEditComponent.relationTime", controller = "updateContactWithRelation"),
            @AuthField(frontendId = "ContactAddComponent.relationTime", controller = "createContactWithRelation"),
    })
    private LocalDate startDate;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactEditComponent.relationTime", controller = "getContactWithRelation"),
            @AuthField(frontendId = "ContactEditComponent.relationTime", controller = "updateContactWithRelation"),
            @AuthField(frontendId = "ContactAddComponent.relationTime", controller = "createContactWithRelation"),
    })
    private LocalDate endDate;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactEditComponent.note", controller = "getContactWithRelation"),
            @AuthField(frontendId = "ContactEditComponent.note", controller = "updateContactWithRelation"),
            @AuthField(frontendId = "ContactAddComponent.note", controller = "createContactWithRelation"),
    })
    private String note;

    @AuthFields(list = {
            @AuthField(controller = "getContactWithRelation"),
            @AuthField(controller = "updateContactWithRelation"),
            @AuthField(controller = "createContactWithRelation"),
    })
    private String avatar;
}
