package com.devqube.crmbusinessservice.customer.customer.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@AuthFilterEnabled
public class CustomerSalesOpportunityPreviewDTO {

    @AuthField(controller = "getCustomerOpportunitiesPreview")
    private Long totalNumber;
    @AuthField(controller = "getCustomerOpportunitiesPreview")
    private List<CustomerSalesOpportunityDTO> opportunities;

}
