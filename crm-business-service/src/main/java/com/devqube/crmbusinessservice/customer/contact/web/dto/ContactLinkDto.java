package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.contact.model.ContactLink;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Optional;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@AuthFilterEnabled
public class ContactLinkDto implements Serializable {

    private Long id;
    private Long baseContactId;
    private String baseContactName;
    private String baseContactSurname;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactLinkAddComponent.add", controller = "getContactLinkForNewLinkPanel"),
            @AuthField(frontendId = "ContactLinkListComponent.contactColumn", controller = "getContactLinks"),
            @AuthField(frontendId = "ContactLinkAddComponent.add", controller = "createContactLink"),
            @AuthField(frontendId = "ContactLinkListComponent.edit", controller = "updateContactLink"),
            @AuthField(controller = "getFromEditContactLinkComponent")
    })
    private Long linkedContactId;


    @AuthFields(list = {
            @AuthField(frontendId = "ContactLinkListComponent.contactColumn", controller = "getContactLinks"),
            @AuthField(frontendId = "ContactLinkAddComponent.add", controller = "createContactLink"),
            @AuthField(frontendId = "ContactLinkListComponent.edit", controller = "updateContactLink"),
            @AuthField(controller = "getFromEditContactLinkComponent")
    })
    private String linkedContactName;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactLinkListComponent.contactColumn", controller = "getContactLinks"),
            @AuthField(frontendId = "ContactLinkAddComponent.add", controller = "createContactLink"),
            @AuthField(frontendId = "ContactLinkListComponent.edit", controller = "updateContactLink"),
            @AuthField(controller = "getFromEditContactLinkComponent")
    })
    private String linkedContactSurname;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactLinkListComponent.phoneColumn", controller = "getContactLinks"),
            @AuthField(frontendId = "ContactLinkAddComponent.add", controller = "createContactLink"),
            @AuthField(frontendId = "ContactLinkListComponent.edit", controller = "updateContactLink"),
            @AuthField(controller = "getFromEditContactLinkComponent")
    })
    private String linkedContactPhone;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactLinkListComponent.emailColumn", controller = "getContactLinks"),
            @AuthField(frontendId = "ContactLinkAddComponent.add", controller = "createContactLink"),
            @AuthField(frontendId = "ContactLinkListComponent.edit", controller = "updateContactLink"),
            @AuthField(controller = "getFromEditContactLinkComponent")
    })
    private String linkedContactEmail;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactLinkListComponent.customerColumn", controller = "getContactLinks"),
            @AuthField(frontendId = "ContactLinkAddComponent.add", controller = "createContactLink"),
            @AuthField(frontendId = "ContactLinkListComponent.edit", controller = "updateContactLink"),
            @AuthField(controller = "getFromEditContactLinkComponent")
    })
    private String linkedCustomerName;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactLinkListComponent.positionColumn", controller = "getContactLinks"),
            @AuthField(frontendId = "ContactLinkAddComponent.add", controller = "createContactLink"),
            @AuthField(frontendId = "ContactLinkListComponent.edit", controller = "updateContactLink"),
            @AuthField(controller = "getFromEditContactLinkComponent")
    })
    private String linkedContactPosition;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactLinkListComponent.notePanel", controller = "getContactLinks"),
            @AuthField(frontendId = "ContactLinkListComponent.notePanel", controller = "createContactLink"),
            @AuthField(frontendId = "ContactLinkListComponent.notePanel", controller = "updateContactLink"),
            @AuthField(controller = "getFromEditContactLinkComponent")
    })
    private String note;

    public static ContactLinkDto fromModel(ContactLink contactLink) {
        if (contactLink == null) {
            return null;
        }
        return new ContactLinkDto(contactLink.getId(),
                contactLink.getBaseContact().getId(),
                contactLink.getBaseContact().getName(),
                contactLink.getBaseContact().getSurname(),
                contactLink.getLinkedContact().getId(),
                contactLink.getLinkedContact().getName(),
                contactLink.getLinkedContact().getSurname(),
                contactLink.getLinkedContact().getPhone(),
                contactLink.getLinkedContact().getEmail(),
                contactLink.getLinkedContact().getCustomerOptional().map(Customer::getName).orElse(""),
                contactLink.getLinkedContact().getPosition(),
                contactLink.getNote());
    }
}
