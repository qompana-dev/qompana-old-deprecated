package com.devqube.crmbusinessservice.customer.whitelist.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BankAccountNumber {
    @Id
    @SequenceGenerator(name = "bank_account_number_seq", sequenceName = "bank_account_number_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bank_account_number_seq")
    private Long id;
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    private Whitelist whitelist;
    private String number;
}
