package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmshared.access.annotation.AuthControllerCase;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@AuthFilterEnabled
public class CreateContactLinkDto {
    @AuthFields(list = {
            @AuthField(frontendId = "ContactLinkAddComponent.note", controller = "createContactLink"),
            @AuthField(frontendId = "ContactLinkEditComponent.note", controller = "updateContactLink")
    })
    private String note;
}
