package com.devqube.crmbusinessservice.customer.whitelist.model;

import com.devqube.crmbusinessservice.customer.whitelist.dto.StatusVatEnum;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Whitelist {
    @Id
    @SequenceGenerator(name = "whitelist_seq", sequenceName = "whitelist_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "whitelist_seq")
    private Long id;
    @Enumerated(EnumType.STRING)
    private StatusVatEnum status;

    @Column(name = "nip", unique = true)
    @NotNull
    @Length(max = 10)
    private String nip;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "whitelist", orphanRemoval = true)
    private List<BankAccountNumber> bankAccountNumbers;

    private LocalDate registered;
    private LocalDate updated;
    private LocalDateTime refreshed;
}
