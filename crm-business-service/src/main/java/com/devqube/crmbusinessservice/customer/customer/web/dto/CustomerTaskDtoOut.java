package com.devqube.crmbusinessservice.customer.customer.web.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CustomerTaskDtoOut {

    private Long id;
    private final String type = "CUSTOMER";
    private Long ownerAccountId;
    private String name;
    private String email;
    private String avatar;

}
