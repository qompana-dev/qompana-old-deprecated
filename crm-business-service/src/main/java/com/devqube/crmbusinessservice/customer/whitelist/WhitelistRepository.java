package com.devqube.crmbusinessservice.customer.whitelist;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.whitelist.model.Whitelist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface WhitelistRepository extends JpaRepository<Whitelist, Long> {
    Whitelist findByNip(String nip);
    List<Whitelist> findAllByNipIn(List<String>  nips);
    void deleteAllByNip(String nip);
    void deleteAllByNipIn(List<String> nips);
}
