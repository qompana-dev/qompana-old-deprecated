package com.devqube.crmbusinessservice.customer.contact;

import com.devqube.crmbusinessservice.customer.contact.model.LastViewedContact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LastViewedContactRepository extends JpaRepository<LastViewedContact, Long> {
    List<LastViewedContact> findAllByContact_Id(Long id);
}
