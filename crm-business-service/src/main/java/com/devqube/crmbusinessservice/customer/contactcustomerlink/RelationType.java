package com.devqube.crmbusinessservice.customer.contactcustomerlink;

public enum RelationType {
    DIRECT, INDIRECT
}
