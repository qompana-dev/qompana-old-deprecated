package com.devqube.crmbusinessservice.customer.customer.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
@AuthFilterEnabled
public class CustomerSearchVariantsDtoOut {

    @AuthFields(list = {
            @AuthField(controller = "getSearchVariants"),
    })
    private CustomerSearchType type;

    @AuthFields(list = {
            @AuthField(controller = "getSearchVariants"),
    })
    private List<String> variants = new ArrayList<>();
}
