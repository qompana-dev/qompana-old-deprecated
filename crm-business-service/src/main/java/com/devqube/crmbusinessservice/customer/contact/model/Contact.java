package com.devqube.crmbusinessservice.customer.contact.model;

import com.devqube.crmbusinessservice.customer.address.Address;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.ContactCustomerLink;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.gdpr.model.Gdpr;
import com.devqube.crmbusinessservice.phoneNumber.PhoneNumber;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import com.devqube.crmshared.association.RemoveCrmObject;
import com.devqube.crmshared.history.SaveCrmChangesListener;
import com.devqube.crmshared.search.CrmObjectType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@AuthFilterEnabled
@EntityListeners(SaveCrmChangesListener.class)
public class Contact {
    @JsonProperty("id")
    @Id
    @SequenceGenerator(name = "contact_seq", sequenceName = "contact_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_seq")
    @AuthFields(list = {
            @AuthField(controller = "getAllContacts"),
            @AuthField(controller = "createContact"),
            @AuthField(controller = "updateContact"),
            @AuthField(controller = "updateContactInContactInfo"),
            @AuthField(controller = "getContact"),
            @AuthField(controller = "getContactInContactInfo"),
            @AuthField(frontendId = "CustomerAddComponent.firstToContactField", controller = "createCustomer"),
            @AuthField(controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateAccount"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateCustomer"),
    })
    private Long id;

    @JsonProperty("customer")
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "customer_id")
    @AuthFields(list = {
            @AuthField(controller = "createContact"),
            @AuthField(controller = "updateContact"),
            @AuthField(controller = "updateContactInContactInfo"),
            @AuthField(controller = "getContact"),
            @AuthField(controller = "getContactInContactInfo"),
            @AuthField(controller = "updateContact"),
            @AuthField(controller = "getAllContacts"),
            @AuthField(controller = "getContactForNewLinkPanel")
    })
    private Customer customer;

    @JsonProperty("name")
    @Length(max = 200)
    @Column(name = "name")
    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "ContactListComponent.nameColumn", controller = "getAllContacts"),
            @AuthField(frontendId = "ContactAddComponent.nameField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.nameField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.nameField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.nameField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.nameField", controller = "getContactInContactInfo"),
            @AuthField(frontendId = "CustomerAddComponent.firstToContactField", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerListComponent.firstToContactNameColumn", controller = "getCustomers"),
            @AuthField(controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateAccount"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateCustomer"),
    })
    private String name;

    @JsonProperty("surname")
    @Length(max = 200)
    @Column(name = "surname")
    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "ContactListComponent.nameColumn", controller = "getAllContacts"),
            @AuthField(frontendId = "ContactAddComponent.surnameField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.surnameField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.surnameField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.surnameField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.surnameField", controller = "getContactInContactInfo"),
            @AuthField(frontendId = "CustomerAddComponent.firstToContactField", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerListComponent.firstToContactNameColumn", controller = "getCustomers"),
            @AuthField(controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateAccount"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateCustomer"),
    })
    private String surname;

    @JsonProperty("description")
    @Length(max = 500)
    @Column(name = "description")
    private String description;

    @JsonProperty("department")
    @Length(max = 200)
    @Column(name = "department")
    @AuthFields(list = {
            @AuthField(frontendId = "ContactAddComponent.departmentField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.departmentField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.departmentField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.departmentField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.departmentField", controller = "getContactInContactInfo")
    })
    private String department;

    @JsonProperty("avatar")
    @Column(name = "avatar")
    @AuthFields(list = {
            @AuthField(controller = "createContact"),
            @AuthField(controller = "updateContact"),
            @AuthField(controller = "getAllContacts"),
            @AuthField(controller = "getContact"),
            @AuthField(controller = "getContactInContactInfo"),
            @AuthField(controller = "createCustomer"),
            @AuthField(controller = "getCustomerWithContacts"),
            @AuthField(controller = "updateContactInContactInfo"),
            @AuthField(controller = "getCustomerForEdit"),
            @AuthField(controller = "getCustomer"),
            @AuthField(controller = "updateCustomer")
    })
    private String avatar;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    @AuthFields(list = {
            @AuthField(frontendId = "ContactListComponent.addressColumn", controller = "getAllContacts"),
            @AuthField(frontendId = "ContactAddComponent.addressField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.addressField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.addressField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.addressField", controller = "getContact"),
            @AuthField(frontendId = "ContactEditComponent.addressField", controller = "getContactInContactInfo"),
            @AuthField(frontendId = "CustomerListComponent.firstToContactAddressColumn", controller = "getCustomers"),
    })
    private Address address;

    @JsonProperty("email")
    @Length(max = 100)
    @Column(name = "email")
    @AuthFields(list = {
            @AuthField(frontendId = "ContactListComponent.emailColumn", controller = "getAllContacts"),
            @AuthField(frontendId = "ContactAddComponent.emailField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.emailField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.emailField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.emailField", controller = "getContact"),
            @AuthField(frontendId = "ContactEditComponent.emailField", controller = "getContactInContactInfo"),
            @AuthField(controller = "getCustomers"),
            @AuthField(frontendId = "CustomerListComponent.firstToContactEmailColumn", controller = "getCustomers"),
    })
    private String email;

    @JsonProperty("phone")
    @Column(name = "phone")
    @AuthFields(list = {
            @AuthField(frontendId = "ContactListComponent.phoneColumn", controller = "getAllContacts"),
            @AuthField(frontendId = "ContactAddComponent.phoneField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.phoneField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.phoneField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.phoneField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.phoneField", controller = "getContactInContactInfo"),
            @AuthField(controller = "getCustomers"),
            @AuthField(frontendId = "CustomerListComponent.firstToContactPhoneColumn", controller = "getCustomers"),
    })
    private String phone;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactListComponent.phoneColumn", controller = "getAllContacts"),
            @AuthField(frontendId = "ContactAddComponent.phoneField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.phoneField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.phoneField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.phoneField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.phoneField", controller = "getContactInContactInfo"),
            @AuthField(frontendId = "CustomerListComponent.firstToContactPhoneColumn", controller = "getCustomers"),
    })
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "contact",
            fetch = FetchType.EAGER)
    private Set<PhoneNumber> additionalPhones = new HashSet<>();

    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.firstToContactField", controller = "createCustomer"),
            @AuthField(controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateAccount"),
            @AuthField(frontendId = "CustomerEditComponent.firstToContactField", controller = "updateCustomer"),
            @AuthField(controller = "getFromGus")
    })
    @Column(name = "main_contact")
    private Boolean mainContact;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contact")
    @AuthField(controller = "getContact")
    private Set<LastViewedContact> lastViewedContacts;

    @JsonProperty("contactLinks")
    @OneToMany(mappedBy = "baseContact", cascade = CascadeType.REMOVE)
    @AuthFields(list = {
            @AuthField(frontendId = "ContactAddComponent.contactLinks", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.contactLinks", controller = "getContact")
    })
    private Set<ContactLink> contactLinks;

    @JsonIgnore
    @OneToMany(mappedBy = "linkedContact", cascade = CascadeType.REMOVE)
    @AuthFields(list = {
            @AuthField(frontendId = "ContactAddComponent.contactLinks", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.contactLinks", controller = "getContact")
    })
    private Set<ContactLink> linkedContactLinks;

    @AuthFields(list = {
            @AuthField(controller = "getContact"),
            @AuthField(frontendId = "ContactEditComponent.ownerInfo", controller = "updateContact"),
            @AuthField(frontendId = "ContactListComponent.ownerColumn", controller = "getAllContacts"),
            @AuthField(controller = "getContactForNewLinkPanel")
    })
    private Long owner;

    @AuthFields(list = {
            @AuthField(controller = "getContact"),
            @AuthField(controller = "createContact")
    })
    @CreationTimestamp
    private LocalDateTime created;

    @AuthFields(list = {
            @AuthField(controller = "getContact"),
            @AuthField(controller = "createContact")
    })
    private Long creator;

    @AuthFields(list = {
            @AuthField(controller = "updateContact"),
            @AuthField(controller = "updateContactInContactInfo")
    })
    @UpdateTimestamp
    private LocalDateTime updated;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactAddComponent.socialMediaField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.socialMediaField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.socialMediaField", controller = "getContactInContactInfo"),
    })
    private String facebookLink;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactAddComponent.socialMediaField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.socialMediaField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.socialMediaField", controller = "getContactInContactInfo"),
    })
    private String twitterLink;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactAddComponent.socialMediaField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.socialMediaField", controller = "updateContactInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.socialMediaField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.socialMediaField", controller = "getContactInContactInfo"),
    })
    private String linkedInLink;

    @AuthFields(list = {
            @AuthField(frontendId = "ContactListComponent.positionColumn", controller = "getAllContacts"),
            @AuthField(frontendId = "ContactAddComponent.positionField", controller = "createContact"),
            @AuthField(frontendId = "ContactEditComponent.positionField", controller = "updateContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.positionField", controller = "updateContactInContactInfo"),
            @AuthField(controller = "getCustomers"),
            @AuthField(frontendId = "ContactEditComponent.positionField", controller = "getContact"),
            @AuthField(frontendId = "ContactInContactInfoComponent.positionField", controller = "getContactInContactInfo"),
    })
    private String position;

    @JsonIgnore
    @OneToMany(mappedBy = "contact", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
    private List<ContactCustomerLink> customerLinks;

    @JsonIgnore
    @OneToMany(mappedBy = "contact", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
    private List<Gdpr> gdprList;

    @AuthFields(list = {
            @AuthField(controller = "getAllContacts"),
            @AuthField(controller = "createContact"),
            @AuthField(controller = "updateContact"),
            @AuthField(controller = "updateContactInContactInfo"),
            @AuthField(controller = "getContact"),
            @AuthField(controller = "getContactInContactInfo"),
    })
    private String additionalData;

    @AuthFields(list = {
            @AuthField(controller = "getAllContacts"),
            @AuthField(controller = "createContact"),
            @AuthField(controller = "updateContact"),
            @AuthField(controller = "updateContactInContactInfo"),
            @AuthField(controller = "getContact"),
            @AuthField(controller = "getContactInContactInfo"),
    })
    private Boolean archived = false;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return Objects.equals(id, contact.id) &&
                Objects.equals(name, contact.name) &&
                Objects.equals(surname, contact.surname) &&
                Objects.equals(description, contact.description) &&
                Objects.equals(email, contact.email) &&
                Objects.equals(phone, contact.phone) &&
                Objects.equals(mainContact, contact.mainContact) &&
                Objects.equals(owner, contact.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, description, email, phone, mainContact, owner);
    }

    @PreRemove
    public void preRemove() {
        if (this.id != null) {
            RemoveCrmObject.addObjectToRemove(CrmObjectType.contact, this.id);
        }
    }

    @JsonIgnore
    public Long getCustomerIdOrNull() {
        return Optional.ofNullable(this.customer).map(Customer::getId).orElse(null);
    }

    @JsonIgnore
    public Optional<Long> getCustomerIdOptional() {
        return Optional.ofNullable(this.customer).map(Customer::getId);
    }

    @JsonIgnore
    public Optional<Customer> getCustomerOptional() {
        return Optional.ofNullable(this.customer);
    }
}
