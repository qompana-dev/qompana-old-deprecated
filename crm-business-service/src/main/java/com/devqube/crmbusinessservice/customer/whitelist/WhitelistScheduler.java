package com.devqube.crmbusinessservice.customer.whitelist;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@EnableScheduling
public class WhitelistScheduler {
    private final WhitelistService whitelistService;
    private static boolean activeTask = false;

    public WhitelistScheduler(WhitelistService whitelistService) {
        this.whitelistService = whitelistService;
    }

    @Scheduled(fixedDelay = 2000)
    public void schedule() {
        if (!activeTask) {
            activeTask = true;
            try {
                whitelistService.saveLastWhitelistItems();
            } catch (Exception ignore) {
            }
            activeTask = false;
        }
    }
}
