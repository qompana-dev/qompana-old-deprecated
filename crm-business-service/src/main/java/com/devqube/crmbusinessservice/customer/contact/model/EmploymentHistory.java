package com.devqube.crmbusinessservice.customer.contact.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class EmploymentHistory {
    @JsonProperty("id")
    @Id
    @SequenceGenerator(name = "employment_history_seq", sequenceName = "employment_history_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employment_history_seq")
    @EqualsAndHashCode.Include
    private Long id;

    @JsonProperty("startDate")
    @Column(name = "start_date")
    private LocalDate startDate;

    @JsonProperty("stopDate")
    @Column(name = "stop_date")
    private LocalDate stopDate;

    @JsonProperty("company")
    @Length(max = 200)
    @Column(name = "company")
    private String company;

    @JsonProperty("position")
    @Length(max = 200)
    @Column(name = "position")
    private String position;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "contact_id")
    private Contact contact;

    @CreationTimestamp
    private LocalDateTime created;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmploymentHistory that = (EmploymentHistory) o;
        return id.equals(that.id) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(stopDate, that.stopDate) &&
                Objects.equals(company, that.company) &&
                Objects.equals(position, that.position) &&
                Objects.equals(created, that.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, startDate, stopDate, company, position, created);
    }
}
