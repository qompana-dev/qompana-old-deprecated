package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@AuthFilterEnabled
public class ContactSalesOpportunityPreviewDTO {
    @AuthField(controller = "getContactOpportunitiesPreview")
    private Long totalNumber;
    @AuthField(controller = "getContactOpportunitiesPreview")
    private List<ContactSalesOpportunityDTO> opportunities;
}
