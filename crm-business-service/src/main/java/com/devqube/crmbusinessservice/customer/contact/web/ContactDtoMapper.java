package com.devqube.crmbusinessservice.customer.contact.web;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.contact.web.dto.ContactTaskDtoOut;
import org.springframework.data.domain.Page;

public class ContactDtoMapper {

    public static ContactTaskDtoOut toContactTaskDtoOut(Contact contact) {
        return new ContactTaskDtoOut()
                .setId(contact.getId())
                .setOwnerAccountId(contact.getOwner())
                .setName(contact.getName() + " " + contact.getSurname())
                .setEmail(contact.getEmail())
                .setAvatar(contact.getAvatar());

    }

    public static Page<ContactTaskDtoOut> toContactTaskDtoOut(Page<Contact> contactPage) {
        return contactPage.map(ContactDtoMapper::toContactTaskDtoOut);
    }

}
