package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ContactAndCustomerNameDto {
    private Long id;
    private String contactName;
    private String customerName;

    public ContactAndCustomerNameDto(Contact contact) {
        this.id = contact.getId();
        this.contactName = contact.getName() + " " + contact.getSurname();
        this.customerName = contact.getCustomerOptional().map(Customer::getName).orElse("");
    }
}
