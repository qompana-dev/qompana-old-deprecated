package com.devqube.crmbusinessservice.customer.contact;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.contact.model.LastViewedContact;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.LastViewedLead;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import java.time.LocalDateTime;
import java.util.List;

import static javax.persistence.criteria.JoinType.LEFT;

public class ContactSpecifications {

    public static Specification<Contact> findByName(String toSearch) {
        return (root, query, cb) -> {
            if (toSearch == null) {
                return cb.conjunction();
            }

            return cb.like(cb.lower(root.get("name")), "%" + toSearch.toLowerCase() + "%");
        };
    }

    public static Specification<Contact> findBySurname(String toSearch) {
        return (root, query, cb) -> {
            if (toSearch == null) {
                return cb.conjunction();
            }
            return cb.like(cb.lower(root.get("surname")), "%" + toSearch.toLowerCase() + "%");
        };
    }

    public static Specification<Contact> findByPhone(String toSearch) {
        return (root, query, cb) -> {
            if (toSearch == null) {
                return cb.conjunction();
            }
            return cb.like(cb.lower(root.get("phone")), "%" + toSearch.toLowerCase() + "%");
        };
    }

    public static Specification<Contact> findByEmail(String toSearch) {
        return (root, query, cb) -> {
            if (toSearch == null) {
                return cb.conjunction();
            }
             return cb.like(cb.lower(root.get("email")), "%" + toSearch.toLowerCase() + "%");
        };
    }

    public static Specification<Contact> findByNameOrSurnameOrEmail(String toSearch) {
        return (root, query, cb) ->
        {
            if (toSearch == null) {
                return cb.conjunction();
            }
            return cb.or(
                    cb.like(cb.lower(root.get("name")), "%" + toSearch.toLowerCase() + "%"),
                    cb.like(cb.lower(root.get("surname")), "%" + toSearch.toLowerCase() + "%"),
                    cb.like(cb.lower(root.get("email")), "%" + toSearch.toLowerCase() + "%"),
                    cb.like(cb.lower(cb.concat(root.get("name"), cb.concat(" ", root.get("surname")))), "%" + toSearch.toLowerCase() + "%")
            );
        };
    }

    public static Specification<Contact> findCreatedAfterExclusive(LocalDateTime date) {
        return (root, query, cb) -> cb.greaterThan(root.get("created"), date);
    }

    public static Specification<Contact> findCreatedBeforeInclusive(LocalDateTime date) {
        return (root, query, cb) -> cb.lessThanOrEqualTo(root.get("created"), date);
    }

    public static Specification<Contact> findLastViewedAfterExclusive(LocalDateTime date, Long userId) {
        return (root, query, cb) -> {
            Join<Contact, LastViewedContact> lastViewedContacts = root.joinSet("lastViewedContacts", LEFT);
            return cb.and(
                    cb.equal(lastViewedContacts.get("userId"), userId),
                    cb.greaterThan(lastViewedContacts.get("lastViewed"), date));
        };
    }

    public static Specification<Contact> findUpdatedAfterExclusive(LocalDateTime date) {
        return (root, query, cb) -> cb.greaterThan(root.get("updated"), date);
    }

    public static Specification<Contact> findByArchived(boolean archived) {
        return (root, query, cb) -> cb.equal(root.get("archived"), archived);
    }

    public static Specification<Contact> searchByCustomFields(String search) {
        return findByNameOrSurnameOrEmail(search)
                .or(findByPhone(search));
    }

}
