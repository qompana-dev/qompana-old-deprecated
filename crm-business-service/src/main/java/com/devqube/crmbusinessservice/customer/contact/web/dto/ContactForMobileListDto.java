package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.customer.web.dto.CustomerAssociationDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContactForMobileListDto {

    private Long id;
    private String name;
    private String surname;
    private String position;
    private String phoneNumber;
    private String customerName;

    public ContactForMobileListDto(Contact contact) {
        this.id = contact.getId();
        this.name = contact.getName();
        this.surname = contact.getSurname();
        this.position = contact.getPosition();
        this.phoneNumber = contact.getPhone();
        if(contact.getCustomer()!=null){
            this.customerName = contact.getCustomer().getName();
        }
    }

}


