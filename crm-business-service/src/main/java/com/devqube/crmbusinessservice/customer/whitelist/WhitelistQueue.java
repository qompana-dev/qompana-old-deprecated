package com.devqube.crmbusinessservice.customer.whitelist;

import java.util.*;
import java.util.stream.Collectors;

public class WhitelistQueue {
    private static Queue<String> queue = new LinkedList<>();
    private static WhitelistQueue instance;

    private WhitelistQueue() {
    }

    public static WhitelistQueue getInstance() {
        if (instance == null) {
            instance = new WhitelistQueue();
        }
        return instance;
    }

    public void add(String nip) {
        queue.add(nip);
    }

    public void addAll(List<String> nips) {
        queue.addAll(nips);
    }

    public boolean isEmpty() {
        return getLast() == null;
    }

    public String getLast() {
        return queue.peek();
    }

    public String getLastAndRemove() {
        return queue.poll();
    }

    public List<String> getLastItemList(int count) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            result.add(getLastAndRemove());
        }
        return result.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

}
