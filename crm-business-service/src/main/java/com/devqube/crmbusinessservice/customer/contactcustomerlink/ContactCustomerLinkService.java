package com.devqube.crmbusinessservice.customer.contactcustomerlink;

import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.dto.ContactCustomerLinkDto;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.dto.ContactCustomerLinkSaveDto;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.productprice.util.ModelMapperUtils;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ContactCustomerLinkService {

    private final ContactRepository contactRepository;
    private final CustomerRepository customerRepository;
    private final ContactCustomerLinkRepository contactCustomerLinkRepository;

    public ContactCustomerLinkService(ContactRepository contactRepository, ContactCustomerLinkRepository contactCustomerLinkRepository, CustomerRepository customerRepository) {
        this.contactCustomerLinkRepository = contactCustomerLinkRepository;
        this.contactRepository = contactRepository;
        this.customerRepository = customerRepository;
    }

    @Transactional
    public ContactCustomerLinkSaveDto saveContactCustomerLink(Long contactId, Long customerId, ContactCustomerLinkSaveDto contactCustomerLinkSaveDto) throws EntityNotFoundException, BadRequestException {
        Contact contact = this.contactRepository.findById(contactId).orElseThrow(EntityNotFoundException::new);
        Customer customer = this.customerRepository.findById(customerId).orElseThrow(EntityNotFoundException::new);
        ContactCustomerLink entity = createContactCustomerLinkEntity(contact, customer, contactCustomerLinkSaveDto);
        ContactCustomerLink saved = contactCustomerLinkRepository.save(entity);
        return mapToSaveLinkDto(saved);
    }


    public void deleteContactCustomerLink(Long linkId) throws EntityNotFoundException {
        Optional<ContactCustomerLink> link = contactCustomerLinkRepository.findById(linkId);
        ContactCustomerLink contactCustomerLink = link.orElseThrow(EntityNotFoundException::new);
        contactCustomerLinkRepository.delete(contactCustomerLink);
    }

    @Transactional
    public List<ContactCustomerLinkDto> updateContactCustomerLinks(List<ContactCustomerLinkUpdateDto> dtos) {
        Map<Long, ContactCustomerLinkUpdateDto> dtosMap = dtos.stream().collect(Collectors.toMap(ContactCustomerLinkUpdateDto::getId, link -> link));
        List<Long> idsToUpdate = dtos.stream().map(ContactCustomerLinkUpdateDto::getId).collect(Collectors.toList());
        List<ContactCustomerLink> links = contactCustomerLinkRepository.findAllById(idsToUpdate);
        links.forEach(link -> updateLinkWithUpdateDto(link, dtosMap.get(link.getId())));
        return convertToContactCustomerLinkDto(links);
    }

    public List<ContactCustomerLinkDto> getContactLinks(Long contactId) throws EntityNotFoundException {
        Contact contact = this.contactRepository.findById(contactId).orElseThrow(EntityNotFoundException::new);
        List<ContactCustomerLink> customerLinks = contact.getCustomerLinks();
        setPositionForActiveContacts(customerLinks);
        List<ContactCustomerLink> sortedLinks = sortByEndDate(customerLinks);
        return convertToContactCustomerLinkDto(sortedLinks);
    }

    private List<ContactCustomerLinkDto> convertToContactCustomerLinkDto(List<ContactCustomerLink> sortedLinks) {
        return sortedLinks.stream().map(this::mapToDtoLinkDto).collect(Collectors.toList());
    }

    private List<ContactCustomerLink> sortByEndDate(List<ContactCustomerLink> customerLinks) {
        Comparator<ContactCustomerLink> comparatorByType = Comparator.comparing(o -> o.getRelationType().toString());
        Comparator<ContactCustomerLink> comparatorByDate = (o1, o2) -> Comparator.nullsFirst(LocalDate::compareTo).compare(o1.getEndDate(), o2.getEndDate());
        return customerLinks.stream().sorted(comparatorByType.thenComparing(comparatorByDate)).collect(Collectors.toList());
    }

    private ContactCustomerLinkDto mapToDtoLinkDto(ContactCustomerLink entity) {
        ContactCustomerLinkDto dto = ModelMapperUtils.map(entity, ContactCustomerLinkDto.class);
        dto.setCustomerName(entity.getCustomer().getName());
        dto.setContactName(String.format("%s %s", entity.getContact().getName(), entity.getContact().getSurname()));
        dto.setCustomerId(entity.getCustomer().getId());
        dto.setContactId(entity.getContact().getId());
        boolean isFirstToContact = entity.getCustomer().getFirstToContact().contains(entity.getContact());
        dto.setFirstToContact(isFirstToContact);
        Long customerId = entity.getContact().getCustomerIdOrNull();
        dto.setActive(entity.getCustomer().getId().equals(customerId)
                && entity.getRelationType().equals(RelationType.DIRECT) && entity.getEndDate() == null);
        return dto;
    }

    private ContactCustomerLinkSaveDto mapToSaveLinkDto(ContactCustomerLink saved) {
        return ModelMapperUtils.map(saved, ContactCustomerLinkSaveDto.class);
    }

    private ContactCustomerLink createContactCustomerLinkEntity(Contact contact, Customer customer, ContactCustomerLinkSaveDto dto) {
        return ContactCustomerLink.builder()
                .contact(contact)
                .customer(customer)
                .startDate(dto.getStartDate())
                .endDate(dto.getEndDate())
                .note(dto.getNote())
                .position(dto.getPosition())
                .relationType(dto.getRelationType() != null ? dto.getRelationType() : RelationType.INDIRECT)
                .build();
    }

    private void updateLinkWithUpdateDto(ContactCustomerLink link, ContactCustomerLinkUpdateDto dto) {
        link.setPosition(dto.getPosition());
        link.setNote(dto.getNote());
        link.setStartDate(dto.getStartDate());
        link.setEndDate(dto.getEndDate());
    }


    public List<ContactCustomerLinkDto> getCustomerLinks(Long customerId) throws EntityNotFoundException {
        Customer customer = this.customerRepository.findById(customerId).orElseThrow(EntityNotFoundException::new);
        List<ContactCustomerLink> customerLinks = customer.getContactLinks();
        setPositionForActiveContacts(customerLinks);
        List<ContactCustomerLink> sortedLinks = sortByEndDate(customerLinks);
        return convertToContactCustomerLinkDto(sortedLinks);
    }

    private void setPositionForActiveContacts(List<ContactCustomerLink> links) {
        links.stream().filter(link -> Objects.isNull(link.getEndDate()) && RelationType.DIRECT.equals(link.getRelationType()))
                .forEach(link -> link.setPosition(link.getContact().getPosition()));
    }
}
