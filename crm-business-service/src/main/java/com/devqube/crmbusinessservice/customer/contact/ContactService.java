package com.devqube.crmbusinessservice.customer.contact;

import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.contact.model.ContactLink;
import com.devqube.crmbusinessservice.customer.contact.model.LastViewedContact;
import com.devqube.crmbusinessservice.customer.contact.web.dto.*;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.ContactCustomerLink;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.customer.web.dto.CustomerNameDTO;
import com.devqube.crmbusinessservice.customer.exception.ContactLinkExistException;
import com.devqube.crmbusinessservice.dictionary.WordRepository;
import com.devqube.crmbusinessservice.dictionary.WordSpecifications;
import com.devqube.crmbusinessservice.leadopportunity.AccountsService;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityService;
import com.devqube.crmbusinessservice.phoneNumber.PhoneNumberService;
import com.devqube.crmbusinessservice.phoneNumber.web.dto.PhoneNumberDTO;
import com.devqube.crmshared.access.model.PermissionDto;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.search.EmailSearch;
import com.devqube.crmshared.web.CurrentRequestUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.devqube.crmbusinessservice.customer.contact.ContactSpecifications.*;

@Service
@Transactional
public class ContactService {


    private final ContactRepository contactRepository;
    private final CustomerRepository customerRepository;
    private final ContactLinkRepository contactLinkRepository;
    private final AccessService accessService;
    private final LastViewedContactRepository lastViewedContactRepository;
    private final OpportunityService opportunityService;
    private final ModelMapper modelMapper;
    private final ModelMapper strictModelMapper;
    private final UserService userService;
    private final ContactCustomerRelationService contactCustomerLinkService;
    private final AccountsService accountsService;
    private final PhoneNumberService phoneNumberService;
    private final WordRepository wordRepository;

    public ContactService(ContactRepository contactRepository, CustomerRepository customerRepository,
                          @Qualifier("strictModelMapper") ModelMapper strictModelMapper,
                          ContactLinkRepository contactLinkRepository,
                          AccessService accessService, LastViewedContactRepository lastViewedContactRepository,
                          OpportunityService opportunityService, ModelMapper modelMapper, UserService userService,
                          ContactCustomerRelationService contactCustomerLinkService, AccountsService accountsService, PhoneNumberService phoneNumberService, WordRepository wordRepository) {
        this.contactRepository = contactRepository;
        this.customerRepository = customerRepository;
        this.strictModelMapper = strictModelMapper;
        this.contactLinkRepository = contactLinkRepository;
        this.accessService = accessService;
        this.lastViewedContactRepository = lastViewedContactRepository;
        this.opportunityService = opportunityService;
        this.modelMapper = modelMapper;
        this.userService = userService;
        this.contactCustomerLinkService = contactCustomerLinkService;
        this.accountsService = accountsService;
        this.phoneNumberService = phoneNumberService;
        this.wordRepository = wordRepository;
    }


    @Transactional(rollbackFor = {BadRequestException.class})
    public Contact createContactWithRelation(Long customerId, Contact contact, Set<PhoneNumberDTO> phoneNumberDtoSet) throws BadRequestException {
        Contact verifiedContact = accessService.assignAndVerify(contact);
        if (phoneNumberDtoSet != null && phoneNumberDtoSet.size() > 0) {
            phoneNumberService.setContactAdditionalPhoneNumbers(verifiedContact, phoneNumberDtoSet);
        }
        Contact savedContact = this.createContact(customerId, verifiedContact);
        if (customerId != null) {
            contactCustomerLinkService.saveNewLink(savedContact);
        }
        savedContact.getAdditionalPhones().forEach(phone -> phone.setType(wordRepository.findOne(WordSpecifications.findByPhoneId(phone.getId())).get()));
        return savedContact;
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class, BadRequestException.class})
    public ContactWithRelationDto createContactWithRelation(ContactWithRelationDto dto) throws BadRequestException {
        ContactWithRelationDto verifiedDto = accessService.assignAndVerify(dto);
        Contact contact = this.strictModelMapper.map(verifiedDto, Contact.class);
        Contact savedContact = this.createContact(dto.getCustomerId(), contact);
        ContactCustomerLink link = null;
        if (dto.getCustomerId() != null) {
            link = contactCustomerLinkService.saveNewLink(savedContact, dto);
        }
        return mapToContactWithRelation(contact, link);
    }

    private Contact createContact(Long customerId, Contact contact) throws BadRequestException {
        Customer customer = Optional.ofNullable(customerId).flatMap(customerRepository::findById).orElse(null);
        contact.setCustomer(customer);
        contact.setCreated(LocalDateTime.now());
        Long ownerId = userService.getUserId(CurrentRequestUtil.getLoggedInAccountEmail());
        contact.setCreator(ownerId);
        contact.setOwner(ownerId);
        return contactRepository.save(contact);
    }

    public Set<Contact> getContacts(Long customerId) throws EntityNotFoundException {
        Optional<Customer> customerOpt = customerRepository.findById(customerId);
        if (customerOpt.isPresent()) {
            Customer customer = customerOpt.get();
            return customer.getContacts();
        } else {
            throw new EntityNotFoundException("Customer with id " + customerId + " not found");
        }
    }

    public void removeContact(Long contactId) throws EntityNotFoundException, BadRequestException {
        Optional<Contact> contactOpt = contactRepository.findById(contactId);
        if (contactOpt.isEmpty()) {
            throw new EntityNotFoundException("Contact with id " + contactId + " not found");
        }
        if (this.opportunityService.opportunityExistsByContactId(contactId)) {
            throw new BadRequestException("cannot remove contact; it's used in opportunity");
        }
        contactRepository.deleteById(contactId);
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class, BadRequestException.class})
    public Contact updateContactForZapier(Long contactId, ContactForZapierDto updatedContact) throws EntityNotFoundException, BadRequestException {
        Contact toSave = strictModelMapper.map(updatedContact, Contact.class);
        toSave.setId(contactId);
        toSave.setCustomer(Customer.builder().id(updatedContact.getCustomerId()).build());
        return updateAndVerifyContact(contactId, toSave);
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class, BadRequestException.class,})
    public Contact updateAndVerifyContact(Long contactId, Contact updatedContact) throws EntityNotFoundException, BadRequestException {
        Contact verified = accessService.assignAndVerify(updatedContact);
        return updateContact(contactId, verified);
    }

    private Contact updateContact(Long contactId, Contact updatedContact) throws EntityNotFoundException {
        Contact contact = contactRepository.findById(contactId).orElseThrow(() -> new EntityNotFoundException("Contact with id " + contactId + " not found"));

        Optional<Customer> customerOpt = updatedContact.getCustomerIdOptional().flatMap(customerRepository::findById);
        Optional<Long> previousCustomerIdOpt = contact.getCustomerIdOptional();
        if (!previousCustomerIdOpt.equals(customerOpt.map(Customer::getId))) {
            previousCustomerIdOpt.ifPresent(id -> contactCustomerLinkService.finishCurrentMainLink(id, contact.getId(), contact.getPosition()));
            customerOpt.ifPresent(customer -> contactCustomerLinkService.saveNewLink(customer, contact));
        }

        contact.setName(updatedContact.getName());
        contact.setSurname(updatedContact.getSurname());
        contact.setEmail(updatedContact.getEmail());
        contact.setPhone(updatedContact.getPhone());
        contact.setOwner(updatedContact.getOwner());
        contact.setTwitterLink(updatedContact.getTwitterLink());
        contact.setFacebookLink(updatedContact.getFacebookLink());
        contact.setLinkedInLink(updatedContact.getLinkedInLink());
        contact.setPosition(updatedContact.getPosition());
        contact.setCustomer(customerOpt.orElse(null));
        contact.setAvatar(updatedContact.getAvatar());
        contact.setAdditionalData(updatedContact.getAdditionalData());
        phoneNumberService.modifyContactAdditionalPhoneNumbers(contact, updatedContact.getAdditionalPhones());
        if (contact.getAdditionalPhones() != null && contact.getAdditionalPhones().size() > 0) {
            contact.getAdditionalPhones().forEach(phone -> phone.setType(wordRepository.findById(phone.getType().getId()).get()));
        }
        return contact;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteContacts(List<Long> ids) throws EntityNotFoundException, BadRequestException {
        for (Long id : ids) {
            removeContact(id);
        }
    }

    public Page<ContactOnListDTO> findAllCreatedToday(String search, Pageable pageable) {
        Specification<Contact> spec =
                findCreatedAfterExclusive(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT))
                        .and(findByArchived(false))
                        .and(searchByCustomFields(search));
        return contactRepository.findAll(spec, pageable).map(this::mapToContactOnListDTO);
    }

    public List<ContactAssociationDto> getContactsForMobileAssociationList() {
        return contactRepository.findAll().stream()
                .map(ContactAssociationDto::new)
                .collect(Collectors.toList());
    }

    public Page<ContactOnListDTO> findAllRecentlyEdited(String search, Pageable pageable) {
        Specification<Contact> spec =
                findUpdatedAfterExclusive(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT))
                        .and(findByArchived(false))
                        .and(searchByCustomFields(search));
        return contactRepository.findAll(spec, pageable)
                .map(this::mapToContactOnListDTO);
    }

    public Page<ContactOnListDTO> findAllRecentlyViewed(String search, String loggedAccountEmail, Pageable pageable) throws BadRequestException {
        Long userId = userService.getUserId(loggedAccountEmail);
        Specification<Contact> spec = findLastViewedAfterExclusive(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3), userId)
                .and(findByArchived(false))
                .and(searchByCustomFields(search));
        return contactRepository.findAll(spec, pageable)
                .map(this::mapToContactOnListDTO);
    }

    public Page<ContactOnListDTO> findAllArchived(String search, Pageable pageable) {
        Specification<Contact> spec = findByArchived(true).and(searchByCustomFields(search));
        return contactRepository.findAll(spec, pageable)
                .map(this::mapToContactOnListDTO);
    }

    public Page<Contact> findAll(Pageable pageable) {
        return contactRepository.findAll(pageable);
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class, BadRequestException.class, PermissionDeniedException.class, ContactLinkExistException.class})
    public ContactLinkDto saveLinkBetweenContacts(Long contactId, Long linkedContactId, CreateContactLinkDto createContactLinkDto) throws EntityNotFoundException, BadRequestException, PermissionDeniedException, ContactLinkExistException {
        if (accessService.hasAccess("ContactLinkAddComponent.contact", PermissionDto.State.WRITE, true)) {
            throw new PermissionDeniedException();
        }
        createContactLinkDto = accessService.assignAndVerify(createContactLinkDto);
        Contact contact = this.contactRepository.findContactById(contactId);
        Contact linkedContact = this.contactRepository.findContactById(linkedContactId);
        if (contact == null || linkedContact == null) {
            throw new EntityNotFoundException();
        }

        if (contact.getId().equals(linkedContact.getId())) {
            throw new BadRequestException();
        }
        if (contactLinkRepository.findAllByBaseContactAndLinkedContact(contact, linkedContact).size() != 0) {
            throw new ContactLinkExistException();
        }

        ContactLink build1 = ContactLink.builder().baseContact(contact).linkedContact(linkedContact).note(createContactLinkDto.getNote()).build();
        ContactLink build2 = ContactLink.builder().baseContact(linkedContact).linkedContact(contact).note(createContactLinkDto.getNote()).build();

        if (contactLinkRepository.findAllByBaseContactAndLinkedContact(linkedContact, contact).size() == 0) {
            contactLinkRepository.save(build2);
        }

        ContactLink saved = contactLinkRepository.save(build1);
        return ContactLinkDto.fromModel(saved);
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class, BadRequestException.class})
    public ContactLinkDto updateContactLink(Long contactId, Long linkedContactId, CreateContactLinkDto createContactLinkDto) throws EntityNotFoundException, BadRequestException {
        createContactLinkDto = accessService.assignAndVerify(createContactLinkDto);
        Contact contact = this.contactRepository.findContactById(contactId);
        Contact linkedContact = this.contactRepository.findContactById(linkedContactId);
        if (contact == null || linkedContact == null) {
            throw new EntityNotFoundException();
        }

        List<ContactLink> linksToSave = new ArrayList<>();
        linksToSave.addAll(contactLinkRepository.findAllByBaseContactAndLinkedContact(contact, linkedContact));
        linksToSave.addAll(contactLinkRepository.findAllByBaseContactAndLinkedContact(linkedContact, contact));

        if (linksToSave.size() == 0) {
            throw new BadRequestException();
        }
        String note = createContactLinkDto.getNote();
        List<ContactLink> result = contactLinkRepository.saveAll(linksToSave.stream().peek(c -> c.setNote(note)).collect(Collectors.toList()));
        return ContactLinkDto.fromModel(result.stream().filter(c -> c.getBaseContact().getId().equals(contactId)).findFirst().orElse(null));
    }

    public List<ContactLinkDto> getLinksForContact(Long contactId) throws EntityNotFoundException {
        Contact contact = contactRepository.findById(contactId).orElseThrow(EntityNotFoundException::new);
        return contactLinkRepository.findAllByBaseContact(contact).stream()
                .map(ContactLinkDto::fromModel)
                .collect(Collectors.toList());
    }

    public ContactLinkPreviewDto getContactLinksPreview(Long contactId) throws EntityNotFoundException {
        ContactLinkPreviewDto result = new ContactLinkPreviewDto();
        Contact contact = contactRepository.findById(contactId).orElseThrow(EntityNotFoundException::new);

        result.setCount(contactLinkRepository.countAllByBaseContact(contact));
        result.setLinks(contactLinkRepository.findTop3ByBaseContact(contact).stream()
                .map(ContactLinkDto::fromModel)
                .collect(Collectors.toList()));
        return result;
    }

    public ContactLinkDto getContactLink(Long contactId, Long linkedContactId) throws EntityNotFoundException {
        Contact contact = contactRepository.findById(contactId).orElseThrow(EntityNotFoundException::new);
        Contact linkedContact = contactRepository.findById(linkedContactId).orElseThrow(EntityNotFoundException::new);
        Optional<ContactLink> contactLink = contactLinkRepository.findAllByBaseContactAndLinkedContact(contact, linkedContact).stream().findFirst();

        if (contactLink.isEmpty()) {
            throw new EntityNotFoundException();
        }
        return ContactLinkDto.fromModel(contactLink.get());
    }

    @Transactional(rollbackFor = EntityNotFoundException.class)
    public void deleteContactLink(Long contactId, Long linkedContactId) throws EntityNotFoundException {
        Contact contact = this.contactRepository.findContactById(contactId);
        Contact linkedContact = this.contactRepository.findContactById(linkedContactId);
        if (contact == null || linkedContact == null) {
            throw new EntityNotFoundException();
        }

        contactLinkRepository.deleteAllByBaseContactAndLinkedContact(contact, linkedContact);
        contactLinkRepository.deleteAllByBaseContactAndLinkedContact(linkedContact, contact);
    }

    public Contact getContact(String loggedAccountEmail, Long contactId) throws EntityNotFoundException, BadRequestException {
        Contact contact = contactRepository.findById(contactId).orElseThrow(() -> new EntityNotFoundException("Contact with id " + contactId + " not found"));
        saveLastViewed(contact, loggedAccountEmail);
        return contact;
    }

    public List<Contact> getAllPossibleLinks(Long contactId) throws EntityNotFoundException {
        Contact contact = contactRepository.findById(contactId).orElseThrow(EntityNotFoundException::new);
        Set<Long> filteredIds = new HashSet<>();
        filteredIds.add(contactId);
        if (contact.getContactLinks() != null) {
            filteredIds.addAll(contact.getContactLinks().stream()
                    .map(link -> link.getLinkedContact().getId()).collect(Collectors.toSet()));
        }
        return contactRepository.findAllOtherThan(filteredIds);
    }

    public List<Contact> findAllContaining(String pattern) {
        return contactRepository.findByNameOrSurnameContaining(pattern);
    }

    public Page<ContactOnListDTO> findAll(String search, Pageable page) {
        Specification<Contact> spec = findByArchived(false).and(searchByCustomFields(search));
        return contactRepository.findAll(spec, page)
                .map(this::mapToContactOnListDTO);
    }
    public Page<Contact> findAll(Specification<Contact> spec, Pageable page) {
        return contactRepository.findAll(spec, page);
    }

    public List<Contact> findByIds(List<Long> ids) {
        return contactRepository.findByIds(ids);
    }

    public ContactTopBarDTO getContactForTopBar(String loggedAccountEmail, Long contactId) throws EntityNotFoundException, BadRequestException {
        Contact contact = contactRepository.findById(contactId).orElseThrow(() -> new EntityNotFoundException("Contact with id " + contactId + " not found"));
        saveLastViewed(contact, loggedAccountEmail);
        ContactTopBarDTO dto = modelMapper.map(contact, ContactTopBarDTO.class);
        return dto;
    }

    public String getContactName(Long contactId) throws EntityNotFoundException {
        Contact contact = contactRepository.findById(contactId).orElseThrow(() -> new EntityNotFoundException("Contact with id " + contactId + " not found"));
        return String.format("%s %s", contact.getName(), contact.getSurname());
    }

    public List<ContactSalesOpportunityDTO> getContactOpportunities(String loggedAccountEmail, Long contactId)
            throws BadRequestException, EntityNotFoundException {
        contactRepository.findById(contactId).orElseThrow(() -> new EntityNotFoundException("Contact with id " + contactId + " not found"));
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return opportunityService.findAllByContactId(contactId, userIds);
    }

    public ContactSalesOpportunityPreviewDTO getContactOpportunitiesPreview(String loggedAccountEmail, Long contactId)
            throws EntityNotFoundException, BadRequestException {
        contactRepository.findById(contactId).orElseThrow(() -> new EntityNotFoundException("Contact with id " + contactId + " not found"));
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        List<ContactSalesOpportunityDTO> all = opportunityService.findAllByContactId(contactId, userIds);
        ContactSalesOpportunityPreviewDTO preview = new ContactSalesOpportunityPreviewDTO();
        preview.setTotalNumber((long) all.size());
        preview.setOpportunities(all.stream().limit(4).collect(Collectors.toList()));
        return preview;
    }

    private void saveLastViewed(Contact contact, String loggedAccountEmail) throws BadRequestException {
        Long userId = userService.getUserId(loggedAccountEmail);
        LastViewedContact newLastViewed = LastViewedContact.builder().userId(userId).contact(contact).build();
        LastViewedContact lastViewedContact = (contact.getLastViewedContacts() == null) ? newLastViewed : contact.getLastViewedContacts().stream()
                .filter(lv -> lv.getUserId().equals(userId)).findFirst()
                .orElse(newLastViewed);
        lastViewedContact.setLastViewed(LocalDateTime.now());
        lastViewedContactRepository.save(lastViewedContact);
    }

    public List<EmailSearch> findByEmailContaining(String term, String email) {
        return contactRepository.findByEmailContaining(term)
                .stream()
                .map(entity -> new EmailSearch(CrmObjectType.contact, String.format("%s %s", entity.getName(), entity.getSurname()), entity.getEmail()))
                .collect(Collectors.toList());
    }

    public List<String> getContactEmailWithoutGdprAgree() {
        return contactRepository.findAllContactMailWithoutEmailAgreements();
    }

    private ContactWithRelationDto mapToContactWithRelation(Contact contact, ContactCustomerLink link) {
        ContactWithRelationDto result = this.strictModelMapper.map(contact, ContactWithRelationDto.class);
        result.setCustomerId(contact.getCustomerIdOrNull());
        if (link != null) {
            result.setStartDate(link.getStartDate());
            result.setEndDate(link.getEndDate());
            result.setNote(link.getNote());
        }
        return result;
    }

    public ContactWithRelationDto getContactWithRelation(Long contactId) throws EntityNotFoundException {
        Contact contact = contactRepository.findById(contactId).orElseThrow(() -> new EntityNotFoundException("Contact with id " + contactId + " not found"));
        Optional<Long> customerIdOpt = contact.getCustomerIdOptional();
        ContactCustomerLink link = customerIdOpt.flatMap(id -> contactCustomerLinkService.getMainRelation(id, contact.getId())).orElse(new ContactCustomerLink());
        return mapToContactWithRelation(contact, link);
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class, BadRequestException.class})
    public ContactWithRelationDto updateContactWithRelation(Long contactId, ContactWithRelationDto dto) throws EntityNotFoundException, BadRequestException {
        ContactWithRelationDto verifiedDto = accessService.assignAndVerify(dto);
        Contact contact = this.strictModelMapper.map(verifiedDto, Contact.class);
        Optional<Customer> customerOpt = Optional.ofNullable(verifiedDto.getCustomerId()).flatMap(customerRepository::findById);
        contact.setCustomer(customerOpt.orElse(null));
        contact = this.updateContact(contactId, contact);
        ContactCustomerLink link = null;
        if (customerOpt.isPresent()) {
            link = contactCustomerLinkService.updateMainRelation(customerOpt.get().getId(), contactId, verifiedDto);
        }
        return mapToContactWithRelation(contact, link);
    }

    public List<Long> getAllContactIdWithAccess(String loggedAccountEmail) {
        return contactRepository.findAll().stream().map(Contact::getId).collect(Collectors.toList());
    }

    public List<ContactForMobileListDto> findAllCreatedTodayMob() {
        return contactRepository.findAllByCreatedAfterForMobile(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT)).stream().map(ContactForMobileListDto::new).collect(Collectors.toList());
    }

    public List<ContactForMobileListDto> findAllRecentlyEditedMob() {
        return contactRepository.findAllByUpdatedAfterForMobile(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3)).stream().map(ContactForMobileListDto::new).collect(Collectors.toList());
    }

    public List<ContactForMobileListDto> findAllRecentlyViewedMob(String loggedAccountEmail) throws BadRequestException {
        Long userId = userService.getUserId(loggedAccountEmail);
        return contactRepository.findAllByLastViewedAfterForMobile(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3), userId).stream().map(ContactForMobileListDto::new).collect(Collectors.toList());
    }

    public List<ContactForMobileListDto> findAllMob() {
        return contactRepository.findAll().stream().map(ContactForMobileListDto::new).collect(Collectors.toList());
    }

    private <D, T> Page<T> mapPage(Page<D> inputPages, Class<T> outputClass) {
        return inputPages.map(entity -> modelMapper.map(entity, outputClass));
    }

    public List<ContactSearchVariantsDtoOut>  findAllSearchVariants(String toSearch)  throws BadRequestException {
        List<ContactSearchVariantsDtoOut> searchVariants = new ArrayList<>();

        searchVariants.add(new ContactSearchVariantsDtoOut()
                .setType(ContactSearchType.NAME)
                .setVariants(contactRepository.findAll(findByName(toSearch)).stream()
                        .map(Contact::getName)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        searchVariants.add(new ContactSearchVariantsDtoOut()
                .setType(ContactSearchType.SURNAME)
                .setVariants(contactRepository.findAll(findBySurname(toSearch)).stream()
                        .map(Contact::getSurname)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        searchVariants.add(new ContactSearchVariantsDtoOut()
                .setType(ContactSearchType.PHONE)
                .setVariants(contactRepository.findAll(findByPhone(toSearch)).stream()
                        .map(Contact::getPhone)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        searchVariants.add(new ContactSearchVariantsDtoOut()
                .setType(ContactSearchType.EMAIL)
                .setVariants(contactRepository.findAll(findByEmail(toSearch)).stream()
                        .map(Contact::getEmail)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        return searchVariants;
    }
    private ContactOnListDTO mapToContactOnListDTO(Contact contact){
        List<ContactCustomerLink> customerLinks = contact.getCustomerLinks();
        ContactOnListDTO contactOnListDTO = new ContactOnListDTO(contact);
        Optional.ofNullable(contact.getCustomer()).ifPresent(customer ->
                contactOnListDTO.setCustomer(modelMapper.map(customer, CustomerNameDTO.class)));
        List<CustomerNameDTO> collect = customerLinks.stream()
                .map(ContactCustomerLink::getCustomer)
                .distinct()
                .map(customer -> modelMapper.map(customer, CustomerNameDTO.class))
                .collect(Collectors.toList());
        collect.remove(contactOnListDTO.getCustomer());
        contactOnListDTO.setLinkedCustomers(collect);
        return contactOnListDTO;
    }

    public void archiveContact(Long contactId) throws EntityNotFoundException, BadRequestException {
        Optional<Contact> byId = contactRepository.findById(contactId);
        if (byId.isEmpty()) {
            throw new EntityNotFoundException("Contact with id " + contactId + " not found");
        }
        Contact contact = byId.get();
        if (contact.getArchived()) {
            throw new BadRequestException("cannot archive contact; it's already archived");
        }
        contact.setArchived(true);
        contactRepository.save(contact);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void archiveContacts(List<Long> ids) throws EntityNotFoundException, BadRequestException {
        for (Long id : ids) {
            archiveContact(id);
        }
    }
}
