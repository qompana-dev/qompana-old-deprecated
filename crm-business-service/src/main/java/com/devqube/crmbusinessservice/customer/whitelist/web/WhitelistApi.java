package com.devqube.crmbusinessservice.customer.whitelist.web;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.customer.customer.web.dto.CustomerWithContactsDto;
import com.devqube.crmbusinessservice.customer.whitelist.model.Whitelist;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

@Validated
@Api(value = "whitelist")
public interface WhitelistApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Refresh whitelist status", nickname = "refreshWhitelistStatus", notes = "Method used to refresh whitelist status")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "whitelist status refreshed")})
    @RequestMapping(value = "/whitelist/nip/{nip}/refresh",
            method = RequestMethod.POST)
    ResponseEntity<Void> refreshWhitelistStatus(@ApiParam(value = "", required = true) @PathVariable("nip") String nip);


    @ApiOperation(value = "Get whitelist status by nip", nickname = "getWhitelistStatusByNip", notes = "Method used to get whitelist status", response = Whitelist.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "whitelist status found", response = Whitelist.class) })
    @RequestMapping(value = "/whitelist/nip/{nip}",
            produces = { "application/json" },
            method = RequestMethod.GET)
    ResponseEntity<Whitelist> getWhitelistStatusByNip(@ApiParam(value = "nip", required = true) @PathVariable("nip") String nip);
}
