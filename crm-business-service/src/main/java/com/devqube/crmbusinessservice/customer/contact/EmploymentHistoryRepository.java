package com.devqube.crmbusinessservice.customer.contact;

import com.devqube.crmbusinessservice.customer.contact.model.EmploymentHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmploymentHistoryRepository extends JpaRepository<EmploymentHistory, Long> {
    void deleteAllByContactId(Long contactId);
}

