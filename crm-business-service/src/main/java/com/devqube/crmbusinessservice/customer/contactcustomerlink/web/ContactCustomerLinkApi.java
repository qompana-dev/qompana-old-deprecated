package com.devqube.crmbusinessservice.customer.contactcustomerlink.web;

import com.devqube.crmbusinessservice.customer.contact.web.dto.ContactLinkPreviewDto;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.ContactCustomerLinkUpdateDto;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.dto.ContactCustomerLinkDto;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.dto.ContactCustomerLinkSaveDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

@Validated
@Api
public interface ContactCustomerLinkApi {

    @PutMapping(value = "/contacts-customer-links")
    ResponseEntity<List<ContactCustomerLinkDto>> updateContactCustomerLinks(@RequestBody List<ContactCustomerLinkUpdateDto> dtos) throws EntityNotFoundException;

    @ApiOperation(value = "Get contact links")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of contact links to customer", response = ContactLinkPreviewDto.class),
            @ApiResponse(code = 404, message = "contact not found")})
    ResponseEntity<List<ContactCustomerLinkDto>> getContactLinks(@PathVariable("contactId") Long contactId) throws EntityNotFoundException;

    @ApiOperation(value = "Associate contact with customer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "contact is associated with customer", response = ContactLinkPreviewDto.class),
            @ApiResponse(code = 404, message = "contact or customer not found")})
    ResponseEntity<ContactCustomerLinkSaveDto> linkContactWithCustomer(Long contactId, Long customerId, @Valid ContactCustomerLinkSaveDto contactCustomerRelationDto) throws BadRequestException, EntityNotFoundException;

    @ApiOperation(value = "Delete link between contact and customer")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "link between contact or customer was deleted", response = Void.class),
            @ApiResponse(code = 404, message = "contact or customer not found")})
    ResponseEntity<Void> deleteContactCustomerLink(Long linkId) throws EntityNotFoundException;


    @ApiOperation(value = "Get customer links")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of customer links to contact", response = ContactLinkPreviewDto.class),
            @ApiResponse(code = 404, message = "customer not found")})
    ResponseEntity<List<ContactCustomerLinkDto>> getCustomerLinks(@PathVariable("customerId") Long customerId) throws EntityNotFoundException;
}
