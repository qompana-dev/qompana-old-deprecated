package com.devqube.crmbusinessservice.customer.contactcustomerlink;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactCustomerLink {
    @JsonProperty("id")
    @Id
    @SequenceGenerator(name = "contact_customer_link_seq", sequenceName = "contact_customer_link_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_customer_link_seq")
    private Long id;

    @ManyToOne
    @NotNull
    @JsonIgnore
    private Contact contact;

    @ManyToOne
    @NotNull
    @JsonIgnore
    private Customer customer;

    @Enumerated(EnumType.STRING)
    private RelationType relationType;

    private LocalDate startDate;
    private LocalDate endDate;
    private String note;
    private String position;
}
