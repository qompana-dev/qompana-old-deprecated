package com.devqube.crmbusinessservice.customer.contact.web.dto;

public enum ContactSearchType {
    NAME, SURNAME, PHONE, EMAIL, CUSTOMER_NAME
}
