package com.devqube.crmbusinessservice.customer.contact.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactLink {
    @JsonProperty("id")
    @Id
    @SequenceGenerator(name = "contact_link_seq", sequenceName = "contact_link_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_link_seq")
    private Long id;

    @ManyToOne
    @NotNull
    @JsonIgnore
    @JoinColumn(name = "base_contact_id")
    private Contact baseContact;

    @ManyToOne
    @NotNull
    @JsonIgnoreProperties({"contactLinks", "employmentHistory"})
    @JoinColumn(name = "linked_contact_id")
    private Contact linkedContact;

    private String note;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContactLink that = (ContactLink) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
