package com.devqube.crmbusinessservice.customer.contact.web;

import com.devqube.crmbusinessservice.customer.contact.ContactService;
import com.devqube.crmbusinessservice.customer.contact.ContactSpecifications;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.contact.web.dto.*;
import com.devqube.crmbusinessservice.customer.customer.web.dto.ContactDto;
import com.devqube.crmbusinessservice.customer.exception.ContactLinkExistException;
import com.devqube.crmbusinessservice.phoneNumber.web.dto.PhoneNumberDTO;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.access.annotation.AuthControllerCase;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.search.EmailSearch;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.OK;

@RestController
@Slf4j
public class ContactsController implements ContactsApi {

    private final ModelMapper modelMapper;
    private final ContactService contactService;

    public ContactsController(@Qualifier("strictModelMapper") ModelMapper modelMapper, ContactService contactService) {
        this.modelMapper = modelMapper;
        this.contactService = contactService;
    }

    @Override
    @AuthController(name = "getContact", controllerCase = {
            @AuthControllerCase(type = "newLinkPanel", name = "getContactForNewLinkPanel"),
            @AuthControllerCase(type = "contactInfo", name = "getContactInContactInfo")
    })
    public ResponseEntity<Contact> getContact(String loggedAccountEmail, Long contactId) {
        try {
            Contact contact = contactService.getContact(loggedAccountEmail, contactId);
            return ResponseEntity.ok(contact);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Override
    @AuthController(name = "createContact", actionFrontendId = "ContactAddComponent.save")
    public ResponseEntity<Contact> createContact(@Valid ContactCreateDto dto) {
        try {
            Contact contact = this.modelMapper.map(dto, Contact.class);
            Long customerId = dto.getCustomerId();
            Set<PhoneNumberDTO> phoneNumberDtoSet = dto.getAdditionalPhones();
            Contact newContact = contactService.createContactWithRelation(customerId, contact, phoneNumberDtoSet);
            return new ResponseEntity<>(newContact, HttpStatus.CREATED);
        } catch (BadRequestException e) {
            return ResponseEntity.badRequest().build();
        }
    }


    @Override
    @AuthController(name = "createContact", actionFrontendId = "ContactAddComponent.save")
    public ResponseEntity<Contact> createContactForZapier(@Valid ContactCreateDto dto) {
        try {
            Contact contact = this.modelMapper.map(dto, Contact.class);
            Long customerId = dto.getCustomerId();
            Set<PhoneNumberDTO> phoneNumberDtoSet = dto.getAdditionalPhones();
            Contact newContact = contactService.createContactWithRelation(customerId, contact, phoneNumberDtoSet);
            return new ResponseEntity<>(newContact, HttpStatus.CREATED);
        } catch (BadRequestException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Override
    @AuthController(name = "removeContact", actionFrontendId = "ContactListComponent.remove")
    public ResponseEntity<Void> deleteContacts(@Valid List<Long> ids) {
        try {
            contactService.deleteContacts(ids);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getAllContacts")
    public ResponseEntity<Page<ContactOnListDTO>> getAllContacts(String loggedAccountEmail, @Valid Pageable pageable,
                                               @Valid Boolean today, @Valid Boolean edited,
                                               @Valid Boolean viewed, @Valid Boolean archived,
                                               @Valid String search) {
        try {
            if (today != null && today) {
                return new ResponseEntity<>(contactService.findAllCreatedToday(search, pageable), OK);
            } else if (edited != null && edited) {
                return new ResponseEntity<>(contactService.findAllRecentlyEdited(search, pageable), OK);
            } else if (viewed != null && viewed) {
                return new ResponseEntity<>(contactService.findAllRecentlyViewed(search, loggedAccountEmail, pageable), OK);
            } else if (archived !=null && archived) {
                return new ResponseEntity<>(contactService.findAllArchived(search, pageable), OK);
            }  else {
                return new ResponseEntity<>(contactService.findAll(search, pageable), OK);
            }
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override // todo: permissions
    public ResponseEntity<List<ContactAssociationDto>> getContactsForMobileAssociationList() {
        return new ResponseEntity<>(contactService.getContactsForMobileAssociationList(), OK);
    }

    @Override
    @AuthController(name = "getAllContactsForMobile")
    public ResponseEntity<List<ContactForMobileListDto>> getAllContactsForMobile(String loggedAccountEmail, @Valid Boolean today, @Valid Boolean edited, @Valid Boolean viewed) {
        try {
            if (today != null && today) {
                return new ResponseEntity<>(contactService.findAllCreatedTodayMob(), OK);
            } else if (edited != null && edited) {
                return new ResponseEntity<>(contactService.findAllRecentlyEditedMob(), OK);
            } else if (viewed != null && viewed) {
                return new ResponseEntity<>(contactService.findAllRecentlyViewedMob(loggedAccountEmail), OK);
            } else {
                return new ResponseEntity<>(contactService.findAllMob(), OK);
            }
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getAllContacts")
    public ResponseEntity<List<ContactOnListDTO>> getAllContactsForZapier(String loggedAccountEmail, @Valid Pageable pageable,
                                                                 @Valid Boolean today, @Valid Boolean edited,
                                                                 @Valid Boolean viewed, @Valid Boolean archived,
                                                                 @Valid String search) {
        try {
            if (today != null && today) {
                return new ResponseEntity<>(contactService.findAllCreatedToday(search, pageable).getContent(), OK);
            } else if (edited != null && edited) {
                return new ResponseEntity<>(contactService.findAllRecentlyEdited(search, pageable).getContent(), OK);
            } else if (viewed != null && viewed) {
                return new ResponseEntity<>(contactService.findAllRecentlyViewed(loggedAccountEmail, search, pageable).getContent(), OK);
            } else if (archived != null && archived) {
                return new ResponseEntity<>(contactService.findAllArchived(search, pageable).getContent(), OK);
            } else {
                return new ResponseEntity<>(contactService.findAll(search, pageable).getContent(), OK);
            }
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getContacts")
    public ResponseEntity<List<ContactDto>> getContacts(Long customerId) {
        try {
            Set<Contact> contacts = contactService.getContacts(customerId);
            List<ContactDto> result = contacts.stream().map(ContactDto::fromModel).collect(Collectors.toList());
            return ResponseEntity.ok(result);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    @AuthController(name = "removeContact", actionFrontendId = "ContactListComponent.remove")
    public ResponseEntity<Void> removeContact(Long contactId) {
        try {
            contactService.removeContact(contactId);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @Override
    @AuthController(name = "updateContact", actionFrontendId = "ContactEditComponent.save", controllerCase = {
            @AuthControllerCase(type = "contactInfo", name = "updateContactInContactInfo", actionFrontendId = "ContactInContactInfoComponent.save")
    })
    public ResponseEntity<Contact> updateContact(Long contactId, @Valid Contact contact) {
        try {
            Contact updatedContact = contactService.updateAndVerifyContact(contactId, contact);
            return ResponseEntity.ok(updatedContact);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Override
    @AuthController(name = "archiveContact", actionFrontendId = "ContactListComponent.archive")
    public ResponseEntity<Void> archiveContact(String loggedAccountEmail, Long contactId) {
        try {
            contactService.archiveContact(contactId);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Contact with id {} not found", contactId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "archiveContacts", actionFrontendId = "ContactListComponent.archive")
    public ResponseEntity<Void> archiveContacts(String loggedAccountEmail, @Valid List<Long> ids) {
        try {
            contactService.archiveContacts(ids);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "updateContact", actionFrontendId = "ContactEditComponent.save", controllerCase = {
            @AuthControllerCase(type = "contactInfo", name = "updateContactInContactInfo", actionFrontendId = "ContactInContactInfoComponent.save")
    })
    public ResponseEntity<Contact> updateContactForZapier(Long contactId, @Valid ContactForZapierDto contact) {
        try {
            Contact updatedContact = contactService.updateContactForZapier(contactId, contact);
            return ResponseEntity.ok(updatedContact);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Override
    @AuthController(name = "createContactLink", actionFrontendId = "ContactLinkAddComponent.add")
    public ResponseEntity<ContactLinkDto> createContactLink(Long contactId, Long linkedContactId, @Valid CreateContactLinkDto createContactLinkDto) {
        try {
            ContactLinkDto result = contactService.saveLinkBetweenContacts(contactId, linkedContactId, createContactLinkDto);
            return new ResponseEntity<>(result, HttpStatus.CREATED);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (ContactLinkExistException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @Override
    @AuthController(actionFrontendId = "ContactLinkListComponent.remove")
    public ResponseEntity<Void> deleteContactLink(Long contactId, Long linkedContactId) {
        try {
            contactService.deleteContactLink(contactId, linkedContactId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public ResponseEntity<Boolean> getContactAccess(String loggedAccountEmail, Long contactId) {
        // FIXME Just for testing file service. Resolved after implementing upload files stories.
        return ResponseEntity.ok().body(true);
    }

    @Override
    public ResponseEntity<List<Long>> getAllContactsWithAccess(String loggedAccountEmail) {
        return new ResponseEntity<>(this.contactService.getAllContactIdWithAccess(loggedAccountEmail), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Contact>> getAllPossibleLinks(Long contactId) {
        try {
            return ResponseEntity.ok(contactService.getAllPossibleLinks(contactId));
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public ResponseEntity<Boolean> getContactPermissionToAttachFile(String loggedAccountEmail, Long contactId) {
        return ResponseEntity.ok(true);
    }

    @Override
    public ResponseEntity<Boolean> getContactPermissionToDetachFile(String loggedAccountEmail, Long contactId) {
        return ResponseEntity.ok(true);
    }

    @Override
    public ResponseEntity<Boolean> contactExist(String loggedAccountEmail, Long contactId) {
        return ResponseEntity.ok(true);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getContactsForSearch(@NotNull @Valid String term) {
        return new ResponseEntity<>(contactService.findAllContaining(term)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.contact,
                        Strings.nullToEmpty(e.getName()) + " " + Strings.nullToEmpty(e.getSurname()), e.getCreated()))
                .collect(Collectors.toList()),
                OK);
    }

    @Override
    @AuthController(name = "getSearchVariants")
    public ResponseEntity<List<ContactSearchVariantsDtoOut>> getSearchVariants(@NotNull @Valid String toSearch) {
        try {
            return new ResponseEntity<>(contactService.findAllSearchVariants(toSearch), HttpStatus.OK);
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<Page<ContactTaskDtoOut>> getContactsPage(@NotNull @Valid String term,
                                                                   @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable) {
        return new ResponseEntity<>(ContactDtoMapper.toContactTaskDtoOut(
                contactService.findAll(ContactSpecifications.findByNameOrSurnameOrEmail(term), pageable)),
                OK);
    }

    @Override
    @AuthController(readFrontendId = "ContactLinkAddComponent.contact")
    public ResponseEntity<List<ContactAndCustomerNameDto>> getContactsAndCustomerForSearch(@NotNull @Valid String term) {
        return new ResponseEntity<>(contactService.findAllContaining(term)
                .stream()
                .map(ContactAndCustomerNameDto::new)
                .collect(Collectors.toList()),
                OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getContactsAsCrmObject(@Valid List<Long> ids) {
        return new ResponseEntity<>(contactService.findByIds(ids)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.contact,
                        Strings.nullToEmpty(e.getName()) + " " + Strings.nullToEmpty(e.getSurname()), e.getCreated()))
                .collect(Collectors.toList()),
                OK);
    }

    @Override
    @AuthController(name = "getContactLinks", controllerCase = {
            @AuthControllerCase(type = "newLinkPanel", name = "getContactLinkForNewLinkPanel")
    })
    public ResponseEntity<List<ContactLinkDto>> getContactLinks(Long contactId) {
        try {
            return new ResponseEntity<>(contactService.getLinksForContact(contactId), OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "getContactLinks")
    public ResponseEntity<ContactLinkPreviewDto> getContactLinksPreview(Long contactId) {
        try {
            return new ResponseEntity<>(contactService.getContactLinksPreview(contactId), OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "getContactForTopBar")
    public ResponseEntity<ContactTopBarDTO> getContactForTopBar(String loggedAccountEmail, Long contactId) {
        try {
            return new ResponseEntity<>(contactService.getContactForTopBar(loggedAccountEmail, contactId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getContactOpportunities")
    public ResponseEntity<List<ContactSalesOpportunityDTO>> getContactOpportunities(String loggedAccountEmail, Long contactId) {
        try {
            return new ResponseEntity<>(contactService.getContactOpportunities(loggedAccountEmail, contactId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getContactOpportunitiesPreview")
    public ResponseEntity<ContactSalesOpportunityPreviewDTO> getContactOpportunitiesPreview(String loggedAccountEmail, Long contactId) {
        try {
            return new ResponseEntity<>(contactService.getContactOpportunitiesPreview(loggedAccountEmail, contactId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(readFrontendId = "accessDenied", controllerCase = {
            @AuthControllerCase(type = "linkEdit", name = "getFromEditContactLinkComponent", readFrontendId = "ContactLinkListComponent.edit")
    })
    public ResponseEntity<ContactLinkDto> getContactLink(Long contactId, Long linkedContactId) {
        try {
            return new ResponseEntity<>(contactService.getContactLink(contactId, linkedContactId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "updateContactLink", actionFrontendId = "ContactLinkListComponent.edit")
    public ResponseEntity<ContactLinkDto> updateContactLink(Long contactId, Long linkedContactId, @Valid CreateContactLinkDto createContactLinkDto) {
        try {
            return new ResponseEntity<>(this.contactService.updateContactLink(contactId, linkedContactId, createContactLinkDto), OK);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Override
    @GetMapping(value = "/contacts/search-emails")
    public List<EmailSearch> searchEmails(
            @RequestHeader(value = "Logged-Account-Email") String email,
            @RequestParam(value = "term") String term) {
        return contactService.findByEmailContaining(term, email);
    }

    @Override
    @GetMapping(value = "/internal/contacts/emails/without-gdpr")
    public List<String> getContactEmailWithoutGdprAgree() {
        return contactService.getContactEmailWithoutGdprAgree();
    }

    @Override
    @GetMapping(value = "/contacts/{contactId}/name")
    public String getContactName(@PathVariable("contactId") Long contactId) throws EntityNotFoundException {
        return contactService.getContactName(contactId);
    }

    @Override
    @GetMapping(value = "/contacts/{contactId}/with-relation")
    @AuthController(name = "getContactWithRelation", actionFrontendId = "CustomerContactsComponent.edit")
    public ResponseEntity<ContactWithRelationDto> getContactWithRelation(@PathVariable("contactId") Long contactId) {
        try {
            ContactWithRelationDto contact = contactService.getContactWithRelation(contactId);
            return ResponseEntity.ok(contact);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    @PutMapping(value = "/contacts/{contactId}/with-relation")
    @AuthController(name = "updateContactWithRelation", actionFrontendId = "CustomerContactsComponent.edit")
    public ResponseEntity<ContactWithRelationDto> updateContactWithRelation(
            @PathVariable("contactId") Long contactId,
            @RequestBody ContactWithRelationDto dto) {
        try {
            ContactWithRelationDto contact = contactService.updateContactWithRelation(contactId, dto);
            return ResponseEntity.ok(contact);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Override
    @PostMapping(value = "/contacts/with-relation")
    @AuthController(name = "createContactWithRelation", actionFrontendId = "RelatedContactsInCustomerDetail.addDirect")
    public ResponseEntity<ContactWithRelationDto> createContactWithRelation(ContactWithRelationDto contactWithRelationDto) {
        try {
            ContactWithRelationDto contact = contactService.createContactWithRelation(contactWithRelationDto);
            return new ResponseEntity<>(contact, HttpStatus.CREATED);
        } catch (BadRequestException e) {
            return ResponseEntity.badRequest().build();
        }
    }

}
