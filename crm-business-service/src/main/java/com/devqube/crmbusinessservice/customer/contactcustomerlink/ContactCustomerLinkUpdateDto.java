package com.devqube.crmbusinessservice.customer.contactcustomerlink;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactCustomerLinkUpdateDto {
    private Long id;
    private String position;
    private LocalDate startDate;
    private LocalDate endDate;
    private String note;
}
