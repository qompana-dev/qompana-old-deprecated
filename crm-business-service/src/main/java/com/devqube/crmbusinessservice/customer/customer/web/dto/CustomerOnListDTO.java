package com.devqube.crmbusinessservice.customer.customer.web.dto;

import com.devqube.crmbusinessservice.customer.address.Address;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class CustomerOnListDTO {
    @NotNull
    @AuthField(controller = "getCustomers")
    private Long id;
    @NotNull
    @AuthField(controller = "getCustomers")
    private String name;

    @AuthField(controller = "getCustomers")
    private String avatar;

    @AuthField(controller = "getCustomers")
    private Address address;

    @AuthField(controller = "getCustomers")
    private String phone;

    @AuthField(controller = "getCustomers")
    private Long industry;

    @AuthField(controller = "getCustomers")
    private Boolean isActiveClient;

    @AuthField(controller = "getCustomers")
    private Boolean isActiveDelivery;

    @AuthField(controller = "getCustomers")
    private Boolean archived;

    @AuthField(controller = "getCustomers")
    private Long owner;

    @AuthField(controller = "getCustomers")
    private ContactDto firstToContact;

    public CustomerOnListDTO(Customer customer, ContactDto firstToContact) {
        this.id= customer.getId();
        this.name = customer.getName();
        this.avatar = customer.getAvatar();
        this.address = customer.getAddress();
        this.phone = customer.getPhone();
        this.industry = customer.getIndustry();
        this.owner = customer.getOwner();
        this.isActiveClient = customer.getActiveClient();
        this.isActiveDelivery = customer.getActiveDelivery();
        this.archived = customer.getArchived();
        this.firstToContact = firstToContact;
    }
}
