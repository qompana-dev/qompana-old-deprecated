package com.devqube.crmbusinessservice.customer.contactcustomerlink.dto;

import com.devqube.crmbusinessservice.customer.contactcustomerlink.RelationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactCustomerLinkSaveDto {
    private String position;
    private LocalDate startDate;
    private LocalDate endDate;
    private String note;
    private RelationType relationType;
}
