package com.devqube.crmbusinessservice.customer.customer.model;

import com.devqube.crmbusinessservice.customer.address.Address;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.ContactCustomerLink;
import com.devqube.crmbusinessservice.customer.type.Type;
import com.devqube.crmbusinessservice.customer.whitelist.model.Whitelist;
import com.devqube.crmbusinessservice.phoneNumber.PhoneNumber;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import com.devqube.crmshared.association.RemoveCrmObject;
import com.devqube.crmshared.history.SaveCrmChangesListener;
import com.devqube.crmshared.search.CrmObjectType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AuthFilterEnabled
@EntityListeners(SaveCrmChangesListener.class)
public class Customer {
    @Id
    @SequenceGenerator(name = "customer_seq", sequenceName = "customer_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_seq")
    @EqualsAndHashCode.Include
    @AuthFields(list = {
            @AuthField(controller = "getCustomers"),
            @AuthField(controller = "getCustomer"),
            @AuthField(controller = "getFromGus"),
            @AuthField(controller = "updateAccount"),
            @AuthField(controller = "getFromGus"),
            @AuthField(controller = "updateContact"),
            @AuthField(controller = "updateContactInContactInfo"),
            @AuthField(controller = "getContact"),
            @AuthField(controller = "getContactInContactInfo"),
            @AuthField(controller = "createContact"),
    })
    private Long id;

    @Column(name = "nip", unique = true)
    @NotNull
    @Length(max = 10)
    @EqualsAndHashCode.Include
    @AuthFields(list = {
            @AuthField(controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.nipField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.nipField", controller = "updateAccount"),
            @AuthField(controller = "getContact"),
            @AuthField(controller = "updateContact"),
            @AuthField(controller = "updateCustomer"),
            @AuthField(controller = "getFromGus"),
    })
    private String nip;

    @Length(max = 14)
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerListComponent.regonColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.regonField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.regonField", controller = "updateAccount"),
            @AuthField(controller = "getFromGus")
    })
    private String regon;

    @Column(name = "name", unique = true)
    @Length(max = 200)
    @NotNull
    @EqualsAndHashCode.Include
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerListComponent.nameColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.nameField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.nameField", controller = "updateAccount"),
            @AuthField(frontendId = "ContactListComponent.customer.nameColumn", controller = "getAllContacts"),
            @AuthField(frontendId = "ContactAddComponent.companyField", controller = "getContact"),
            @AuthField(controller = "getContact"),
            @AuthField(frontendId = "ContactLinkAddComponent.customer", controller = "getContactForNewLinkPanel"),
            @AuthField(controller = "updateContact"),
            @AuthField(controller = "updateCustomer"),
            @AuthField(controller = "getFromGus")
    })
    private String name;

    @Length(max = 500)
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerListComponent.descriptionColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.descriptionField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.descriptionField", controller = "updateAccount"),
            @AuthField(controller = "getFromGus"),
            @AuthField(controller = "updateCustomer"),
    })
    private String description;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    @AuthFields(list = {
            @AuthField(controller = "getCustomers"),
            @AuthField(controller = "getCustomer"),
            @AuthField(controller = "getFromGus"),
            @AuthField(controller = "getContact"),
            @AuthField(controller = "getContact"),
            @AuthField(controller = "updateCustomer"),
            @AuthField(controller = "getCustomerWithContacts")
    })
    private Address address;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "whitelist_id", referencedColumnName = "id")
    @AuthFields(list = {
            @AuthField(controller = "getCustomers"),
            @AuthField(controller = "getCustomer")
    })
    private Whitelist whitelist;

    @ManyToOne
    @JoinColumn(name = "type_id")
    @OneToOne(cascade = CascadeType.MERGE)
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerListComponent.typeColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.typeField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.typeField", controller = "updateAccount"),
            @AuthField(controller = "getFromGus"),
            @AuthField(controller = "updateCustomer"),
    })
    private Type type;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerListComponent.phoneColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.phoneField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.phoneField", controller = "updateAccount"),
            @AuthField(controller = "getFromGus"),
            @AuthField(controller = "createCustomer"),
            @AuthField(controller = "getCustomerForEdit"),
            @AuthField(controller = "updateCustomer"),
    })
    @JsonIgnoreProperties({"customer", "contactLinks"})
    private Set<Contact> contacts;

    @AuthFields(list = {
            @AuthField(frontendId = "CustomerListComponent.phoneColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.phoneField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.phoneField", controller = "updateAccount"),
            @AuthField(controller = "getFromGus"),

    })
    private String phone;

    @AuthFields(list = {
            @AuthField(frontendId = "CustomerListComponent.phoneColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.phoneField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.phoneField", controller = "updateAccount"),
            @AuthField(controller = "getFromGus")
    })
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "customer",
            fetch = FetchType.EAGER)
    private Set<PhoneNumber> additionalPhones = new HashSet<>();

    @AuthFields(list = {
            @AuthField(controller = "getCustomers")
    })
    @OneToMany
    @JoinColumn(name = "customer_id")
    @Where(clause = "main_contact = true")
    @JsonIgnoreProperties({"customer", "contactLinks"})
    private List<Contact> firstToContact;

    //    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerListComponent.ownerColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.ownerField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.ownerField", controller = "createCustomer"),
            @AuthField(controller = "getFromGus")
    })
    private Long owner;

    @AuthFields(list = {
            @AuthField(frontendId = "CustomerEditComponent.websiteField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.websiteField", controller = "updateAccount"),
            @AuthField(controller = "getFromGus")
    })
    private String website;

    @AuthFields(list = {
            @AuthField(frontendId = "CustomerEditComponent.linkedInField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.linkedInField", controller = "updateAccount"),
            @AuthField(controller = "getFromGus")
    })
    private String linkedIn;

    @AuthFields(list = {
            @AuthField(controller = "getCustomer"),
            @AuthField(controller = "createCustomer")
    })
    @CreationTimestamp
    private LocalDateTime created;

    @AuthFields(list = {
            @AuthField(controller = "getCustomer"),
            @AuthField(controller = "createCustomer")
    })
    private Long creator;

    @AuthFields(list = {
            @AuthField(controller = "updateCustomer")
    })
    @UpdateTimestamp
    private LocalDateTime updated;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
    @AuthField(controller = "getCustomer")
    private Set<LastViewedCustomer> lastViewedCustomers;

    @AuthFields(list = {
            @AuthField(controller = "getCustomer")
    })
    private String contactsNotes;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<ContactCustomerLink> contactLinks;

    private Long employeesNumber;
    @AuthFields(list = {
            @AuthField(controller = "getCustomer")
    })
    private Long industry;
    private String income;

    @JsonProperty("avatar")
    @Column(name = "avatar")
    @AuthFields(list = {
            @AuthField(controller = "createCustomer"),
            @AuthField(controller = "getCustomerWithContacts"),
            @AuthField(controller = "getCustomerForEdit"),
            @AuthField(controller = "getCustomer"),
            @AuthField(controller = "updateCustomer")
    })
    private String avatar;

    @AuthFields(list = {
            @AuthField(controller = "getCustomers"),
            @AuthField(controller = "getCustomer"),
            @AuthField(controller = "updateAccount"),
            @AuthField(controller = "getFromGus"),
            @AuthField(controller = "createCustomer"),
            @AuthField(controller = "getCustomerForEdit"),
            @AuthField(controller = "updateCustomer"),
    })
    private String additionalData;

    private Boolean archived = false;

    private Boolean activeClient = false;

    private Boolean activeDelivery = false;

    private Boolean foreignCompany = false;

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", nip='" + nip + '\'' +
                ", regon='" + regon + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @PreRemove
    public void preRemove() {
        if (this.id != null) {
            RemoveCrmObject.addObjectToRemove(CrmObjectType.customer, this.id);
        }
    }
}
