package com.devqube.crmbusinessservice.customer.exception;

public class WhitelistItemNotFound extends Exception {
    public WhitelistItemNotFound() {
    }

    public WhitelistItemNotFound(String message) {
        super(message);
    }
}
