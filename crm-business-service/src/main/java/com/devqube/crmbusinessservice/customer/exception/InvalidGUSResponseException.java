package com.devqube.crmbusinessservice.customer.exception;

public class InvalidGUSResponseException extends Exception {
    public InvalidGUSResponseException(String message) {
        super(message);
    }
}
