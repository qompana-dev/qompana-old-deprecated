package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ContactAssociationDto {
    private Long id;
    private String contactName;
    private Long customerId;
    private String customerName;

    public ContactAssociationDto(Contact contact) {
        this.id = contact.getId();
        this.contactName = contact.getName() + " " + contact.getSurname();
        this.customerId = contact.getCustomerOptional().map(Customer::getId).orElse(null);
        this.customerName = contact.getCustomerOptional().map(Customer::getName).orElse("");
    }
}
