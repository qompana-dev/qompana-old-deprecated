package com.devqube.crmbusinessservice.customer.customer.web.dto;

import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CustomerAssociationDto {
    private Long id;
    private String name;

    public CustomerAssociationDto(Customer customer) {
        this.id = customer.getId();
        this.name = customer.getName();
    }
}
