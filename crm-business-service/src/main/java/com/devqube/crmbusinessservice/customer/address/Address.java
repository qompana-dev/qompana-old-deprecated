package com.devqube.crmbusinessservice.customer.address;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@AuthFilterEnabled
public class Address {
    @Id
    @SequenceGenerator(name="address_seq", sequenceName="address_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="address_seq")
    @AuthFields(list = {
            @AuthField(controller = "getCustomers"),
            @AuthField(controller = "getCustomer"),
            @AuthField(controller = "getCustomerWithContacts"),
            @AuthField(controller = "getCustomerForEdit"),
            @AuthField(controller = "getCustomerForEditInContactInfo"),
            @AuthField(controller = "getContact"),
    })
    private Long id;

    @Length(max = 20)
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.coordinatesField", controller = "createCustomer"),
            @AuthField( controller = "getCustomers"),
            @AuthField(frontendId = "CustomerListComponent.coordinatesColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.coordinatesField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.coordinatesField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.coordinatesField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.coordinatesField", controller = "updateAccount"),
            @AuthField(frontendId = "CustomerEditComponent.coordinatesField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.coordinatesField", controller = "updateCustomerInContactInfo"),
            @AuthField(frontendId = "ContactAddComponent.coordinatesField", controller = "getContact"),
            @AuthField(frontendId = "ContactEditComponent.coordinatesField", controller = "getContact"),
            @AuthField(controller = "getFromGus"),
            @AuthField(controller = "convertToOpportunity"),
            @AuthField(frontendId = "CustomerDetailsComponent.address",  controller = "getCustomerWithContacts")
    })
    private String longitude;

    @Length(max = 20)
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.coordinatesField", controller = "createCustomer"),
            @AuthField( controller = "getCustomers"),
            @AuthField(frontendId = "CustomerListComponent.coordinatesColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.coordinatesField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.coordinatesField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.coordinatesField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.coordinatesField", controller = "updateAccount"),
            @AuthField(frontendId = "CustomerEditComponent.coordinatesField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.coordinatesField", controller = "updateCustomerInContactInfo"),
            @AuthField(frontendId = "ContactAddComponent.coordinatesField", controller = "getContact"),
            @AuthField(frontendId = "ContactEditComponent.coordinatesField", controller = "getContact"),
            @AuthField(controller = "getFromGus"),
            @AuthField(controller = "convertToOpportunity"),
            @AuthField(frontendId = "CustomerDetailsComponent.address",  controller = "getCustomerWithContacts")
    })
    private String latitude;

    @Length(max = 100)
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.countryField", controller = "createCustomer"),
            @AuthField( controller = "getCustomers"),
            @AuthField(frontendId = "CustomerListComponent.countryColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.countryField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.countryField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.countryField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.countryField", controller = "updateAccount"),
            @AuthField(frontendId = "CustomerEditComponent.countryField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.countryField", controller = "updateCustomerInContactInfo"),
            @AuthField(frontendId = "ContactAddComponent.countryField", controller = "getContact"),
            @AuthField(frontendId = "ContactEditComponent.countryField", controller = "getContact"),
            @AuthField(controller = "getFromGus"),
            @AuthField(controller = "convertToOpportunity"),
            @AuthField(frontendId = "CustomerDetailsComponent.address",  controller = "getCustomerWithContacts")
    })
    private String country;

    @Length(max = 100)
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.cityField", controller = "createCustomer"),
            @AuthField( controller = "getCustomers"),
            @AuthField(frontendId = "CustomerListComponent.cityColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.cityField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.cityField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.cityField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.cityField", controller = "updateAccount"),
            @AuthField(frontendId = "CustomerEditComponent.cityField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.cityField", controller = "updateCustomerInContactInfo"),
            @AuthField(frontendId = "ContactAddComponent.cityField", controller = "getContact"),
            @AuthField(frontendId = "ContactEditComponent.cityField", controller = "getContact"),
            @AuthField(controller = "getFromGus"),
            @AuthField(controller = "convertToOpportunity"),
            @AuthField(frontendId = "CustomerDetailsComponent.address",  controller = "getCustomerWithContacts")
    })
    private String city;

    @Length(max = 20)
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.zipCodeField", controller = "createCustomer"),
            @AuthField( controller = "getCustomers"),
            @AuthField(frontendId = "CustomerListComponent.zipCodeColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.zipCodeField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.zipCodeField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.zipCodeField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.zipCodeField", controller = "updateAccount"),
            @AuthField(frontendId = "ContactAddComponent.zipField", controller = "getContact"),
            @AuthField(frontendId = "CustomerEditComponent.zipCodeField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.zipCodeField", controller = "updateCustomerInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.zipField", controller = "getContact"),
            @AuthField(controller = "getFromGus"),
            @AuthField(controller = "convertToOpportunity"),
            @AuthField(frontendId = "CustomerDetailsComponent.address",  controller = "getCustomerWithContacts")
    })
    private String zipCode;

    @Length(max = 200)
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.streetField", controller = "createCustomer"),
            @AuthField( controller = "getCustomers"),
            @AuthField(frontendId = "CustomerListComponent.streetColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.streetField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.streetField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.streetField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.streetField", controller = "updateAccount"),
            @AuthField(frontendId = "ContactAddComponent.streetField", controller = "getContact"),
            @AuthField(frontendId = "CustomerEditComponent.streetField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.streetField", controller = "updateCustomerInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.streetField", controller = "getContact"),
            @AuthField(controller = "getFromGus"),
            @AuthField(controller = "convertToOpportunity"),
            @AuthField(frontendId = "CustomerDetailsComponent.address",  controller = "getCustomerWithContacts")
    })
    private String street;

    @Transient
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.provinceField", controller = "createCustomer"),
            @AuthField( controller = "getCustomers"),
            @AuthField(frontendId = "CustomerListComponent.provinceColumn", controller = "getCustomers"),
            @AuthField(frontendId = "CustomerEditComponent.provinceField", controller = "getCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.provinceField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.provinceField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.provinceField", controller = "updateAccount"),
            @AuthField(frontendId = "ContactAddComponent.provinceField", controller = "getContact"),
            @AuthField(frontendId = "CustomerEditComponent.provinceField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.provinceField", controller = "updateCustomerInContactInfo"),
            @AuthField(frontendId = "ContactEditComponent.provinceField", controller = "getContact"),
            @AuthField(controller = "getFromGus"),
            @AuthField(frontendId = "CustomerDetailsComponent.address",  controller = "getCustomerWithContacts")
    })
    private String province;
}
