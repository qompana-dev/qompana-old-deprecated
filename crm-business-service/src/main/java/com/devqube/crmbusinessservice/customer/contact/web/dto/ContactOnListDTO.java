package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.web.dto.CustomerNameDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ContactOnListDTO {
    private Long id;
    private CustomerNameDTO customer;
    private List<CustomerNameDTO> linkedCustomers;
    private String name;
    private String surname;
    private String avatar;
    private String position;
    private String email;
    private String phone;
    private Boolean archived;
    private Long owner;

    public ContactOnListDTO(Contact contact) {
        this.id = contact.getId();
        this.name = contact.getName();
        this.surname = contact.getSurname();
        this.avatar = contact.getAvatar();
        this.position = contact.getPosition();
        this.email = contact.getEmail();
        this.phone = contact.getPhone();
        this.archived = contact.getArchived();
        this.owner = contact.getOwner();
    }
}
