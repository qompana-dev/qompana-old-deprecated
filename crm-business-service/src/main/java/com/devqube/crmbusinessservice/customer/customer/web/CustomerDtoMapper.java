package com.devqube.crmbusinessservice.customer.customer.web;

import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.customer.web.dto.CustomerTaskDtoOut;
import org.springframework.data.domain.Page;

public class CustomerDtoMapper {

    public static CustomerTaskDtoOut toCustomerTaskDtoOut(Customer customer) {
        return new CustomerTaskDtoOut()
                .setId(customer.getId())
                .setOwnerAccountId(customer.getOwner())
                .setName(customer.getName())
                .setAvatar(customer.getAvatar());

    }

    public static Page<CustomerTaskDtoOut> toCustomerTaskDtoOut(Page<Customer> customerPage) {
        return customerPage.map(CustomerDtoMapper::toCustomerTaskDtoOut);
    }

}
