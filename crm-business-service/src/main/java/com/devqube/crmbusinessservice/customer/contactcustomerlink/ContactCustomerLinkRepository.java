package com.devqube.crmbusinessservice.customer.contactcustomerlink;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContactCustomerLinkRepository extends JpaRepository<ContactCustomerLink, Long> {
    Optional<ContactCustomerLink> findFirstByContactAndCustomer(Contact contact, Customer customer);
    Optional<ContactCustomerLink> findFirstByContactIdAndCustomerId(Long contactId, Long customerId);
    List<ContactCustomerLink> findAllByContact(Contact contact);
    List<ContactCustomerLink> findAllByIdIn(List<Long> ids);

    @Query("SELECT u FROM ContactCustomerLink u " +
            "WHERE u.contact.id = :contactId " +
            "AND u.customer.id = :customerId " +
            "AND u.relationType = com.devqube.crmbusinessservice.customer.contactcustomerlink.RelationType.DIRECT " +
            "AND u.endDate = null")
    Optional<ContactCustomerLink> findMainRelation(@Param("customerId") long customerId, @Param("contactId") long contactId);
}
