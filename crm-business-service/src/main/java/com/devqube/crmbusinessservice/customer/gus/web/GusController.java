package com.devqube.crmbusinessservice.customer.gus.web;

import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.exception.InvalidGUSResponseException;
import com.devqube.crmbusinessservice.customer.gus.GusClient;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@Slf4j
public class GusController implements GusApi{
    private final GusClient client;

    public GusController(GusClient client) {
        this.client = client;
    }

    @Override
    @AuthController(name = "getFromGus", actionFrontendId = "CustomerFormComponent.getFromGus")
    public ResponseEntity<Customer> getFromGus(@NotNull @Valid String nip, Long customerId) {
        try {
            return new ResponseEntity<>(client.getCustomer(nip, customerId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info("Customer with nip {} not found in GUS.", nip);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (InvalidGUSResponseException e) {
            log.info("There was some problem with GUS response for nip {}", nip);
            return new ResponseEntity<>(HttpStatus.GONE);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
