package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@AuthFilterEnabled
public class ContactSalesOpportunityDTO {
    @AuthFields(list = {
            @AuthField(controller = "getContactOpportunities"),
            @AuthField(controller = "getContactOpportunitiesPreview")
    })
    private Long id;

    @AuthFields(list = {
            @AuthField(controller = "getContactOpportunities", frontendId = "ContactSalesOpportunityCardComponent.name"),
            @AuthField(controller = "getContactOpportunitiesPreview", frontendId = "ContactSalesOpportunityCardComponent.name")
    })
    private String name;

    @AuthFields(list = {
            @AuthField(controller = "getContactOpportunities", frontendId = "ContactSalesOpportunityCardComponent.status"),
            @AuthField(controller = "getContactOpportunitiesPreview", frontendId = "ContactSalesOpportunityCardComponent.status")
    })
    private Long status;

    @AuthFields(list = {
            @AuthField(controller = "getContactOpportunities", frontendId = "ContactSalesOpportunityCardComponent.status"),
            @AuthField(controller = "getContactOpportunitiesPreview", frontendId = "ContactSalesOpportunityCardComponent.status")
    })
    private Long maxStatus;

    @AuthFields(list = {
            @AuthField(controller = "getContactOpportunities", frontendId = "ContactSalesOpportunityCardComponent.amount"),
            @AuthField(controller = "getContactOpportunitiesPreview", frontendId = "ContactSalesOpportunityCardComponent.amount")
    })
    private Double amount;

    @AuthFields(list = {
            @AuthField(controller = "getContactOpportunities", frontendId = "ContactSalesOpportunityCardComponent.amount"),
            @AuthField(controller = "getContactOpportunitiesPreview", frontendId = "ContactSalesOpportunityCardComponent.amount")
    })
    private String currency;

    @AuthFields(list = {
            @AuthField(controller = "getContactOpportunities", frontendId = "ContactSalesOpportunityCardComponent.finishDate"),
            @AuthField(controller = "getContactOpportunitiesPreview", frontendId = "ContactSalesOpportunityCardComponent.finishDate")
    })
    private LocalDate finishDate;
}
