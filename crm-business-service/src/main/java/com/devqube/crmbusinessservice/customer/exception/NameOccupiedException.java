package com.devqube.crmbusinessservice.customer.exception;

public class NameOccupiedException extends Exception {
    public NameOccupiedException(String message) {
        super(message);
    }
}
