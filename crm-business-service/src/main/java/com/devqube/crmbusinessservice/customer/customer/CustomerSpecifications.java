package com.devqube.crmbusinessservice.customer.customer;

import com.devqube.crmbusinessservice.customer.address.Address;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.customer.model.LastViewedCustomer;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;

import java.time.LocalDateTime;

import static javax.persistence.criteria.JoinType.LEFT;

public class CustomerSpecifications {

    public static Specification<Customer> findByName(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("name")), "%" + toSearch.toLowerCase() + "%");
    }

    public static Specification<Customer> findByCity(String toSearch){
        return (root, query, cb) -> {
            Join<Customer, Address> address = root.join("address", LEFT);
            return cb.like(cb.lower(address.get("city")), "%" + toSearch.toLowerCase() + "%");
        };
    }
    public static Specification<Customer> findByPhone(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("phone")), "%" + toSearch.toLowerCase() + "%");
    }

    public static Specification<Customer> findCreatedAfterExclusive(LocalDateTime date) {
        return (root, query, cb) -> cb.greaterThan(root.get("created"), date);
    }

    public static Specification<Customer> findCreatedBeforeInclusive(LocalDateTime date) {
        return (root, query, cb) -> cb.lessThanOrEqualTo(root.get("created"), date);
    }

    public static Specification<Customer> findUpdatedAfterExclusive(LocalDateTime date) {
        return (root, query, cb) -> cb.greaterThan(root.get("updated"), date);
    }

    public static Specification<Customer> findLastViewedAfterExclusive(LocalDateTime date, Long userId) {
        return (root, query, cb) -> {
            Join<Customer, LastViewedCustomer> lastViewedCustomers = root.joinSet("lastViewedCustomers", LEFT);
            return cb.and(
                    cb.equal(lastViewedCustomers.get("userId"), userId),
                    cb.greaterThan(lastViewedCustomers.get("lastViewed"), date));
        };
    }
    public static Specification<Customer> findByArchived(boolean archived) {
        return (root, query, cb) -> cb.equal(root.get("archived"), archived);
    }

    public static Specification<Customer> searchByCustomFields(String search) {
        return findByName(search)
                .or(findByCity(search))
                .or(findByPhone(search));
    }

}
