package com.devqube.crmbusinessservice.customer.customer.web.dto;

public enum CustomerSearchType {
    COMPANY_NAME, COMPANY_CITY, PHONE
}
