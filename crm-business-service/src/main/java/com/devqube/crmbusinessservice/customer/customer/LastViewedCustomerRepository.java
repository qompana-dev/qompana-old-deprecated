package com.devqube.crmbusinessservice.customer.customer;

import com.devqube.crmbusinessservice.customer.customer.model.LastViewedCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LastViewedCustomerRepository extends JpaRepository<LastViewedCustomer, Long> {
    List<LastViewedCustomer> findAllByCustomer_Id(Long id);
}
