package com.devqube.crmbusinessservice.customer.contact.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@AuthFilterEnabled
public class ContactTopBarDTO {
    @AuthField(controller = "getContactForTopBar")
    private Long id;
    @AuthField(controller = "getContactForTopBar", frontendId = "ContactDetailsComponent.name")
    private String name;
    @AuthField(controller = "getContactForTopBar", frontendId = "ContactDetailsComponent.name")
    private String surname;
    @AuthField(controller = "getContactForTopBar", frontendId = "ContactDetailsComponent.position")
    private String position;
    @AuthField(controller = "getContactForTopBar", frontendId = "ContactDetailsComponent.customer")
    private String customerName;
    @AuthField(controller = "getContactForTopBar", frontendId = "ContactDetailsComponent.phone")
    private String phone;
    @AuthField(controller = "getContactForTopBar", frontendId = "ContactDetailsComponent.email")
    private String email;
    @AuthField(controller = "getContactForTopBar", frontendId = "ContactDetailsComponent.created")
    private String created;
    @AuthField(controller = "getContactForTopBar", frontendId = "ContactDetailsComponent.owner")
    private Long owner;
    @AuthField(controller = "getContactForTopBar", frontendId = "ContactDetailsComponent.image")
    private String avatar;
}
