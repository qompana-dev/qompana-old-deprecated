package com.devqube.crmbusinessservice.customer.contactcustomerlink.web;

import com.devqube.crmbusinessservice.customer.contactcustomerlink.ContactCustomerLink;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.ContactCustomerLinkService;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.ContactCustomerLinkUpdateDto;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.dto.ContactCustomerLinkDto;
import com.devqube.crmbusinessservice.customer.contactcustomerlink.dto.ContactCustomerLinkSaveDto;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.access.annotation.AuthControllerCase;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
public class ContactCustomerLinkController implements ContactCustomerLinkApi {

    private final ContactCustomerLinkService contactCustomerLinkService;

    public ContactCustomerLinkController(ContactCustomerLinkService contactCustomerLinkService) {
        this.contactCustomerLinkService = contactCustomerLinkService;
    }

    @Override
    @PutMapping(value = "/contact-customer-links")
    @AuthController(name = "updateContactCustomerLinks", controllerCase = {
            @AuthControllerCase(type= "customer", name="updateLinksFromCustomer", actionFrontendId = "RelatedContactsInCustomerDetail.edit"),
            @AuthControllerCase(type= "contact", name="updateLinksFromContact", actionFrontendId = "RelatedCustomerInContactDetail.edit")
    })
    public ResponseEntity<List<ContactCustomerLinkDto>> updateContactCustomerLinks(@RequestBody List<ContactCustomerLinkUpdateDto> dtos) {
        List<ContactCustomerLinkDto> updatedLinks = contactCustomerLinkService.updateContactCustomerLinks(dtos);
        return ResponseEntity.ok(updatedLinks);
    }


    @Override
    @GetMapping(value = "/contacts/{contactId}/customer-links")
    @AuthController(name = "getContactLinks", actionFrontendId = "RelatedCustomerInContactDetail.view")
    public ResponseEntity<List<ContactCustomerLinkDto>> getContactLinks(@PathVariable("contactId") Long contactId) throws EntityNotFoundException {
        List<ContactCustomerLinkDto> dtos = contactCustomerLinkService.getContactLinks(contactId);
        return ResponseEntity.ok(dtos);
    }


    @Override
    @PostMapping(value = "/contacts/{contactId}/link/customers/{customerId}")
    @AuthController(name = "linkContactWithCustomer", controllerCase = {
            @AuthControllerCase(type= "customer", name="linkFromCustomer", actionFrontendId = "RelatedContactsInCustomerDetail.addIndirect"),
            @AuthControllerCase(type= "contact", name="linkFromContact", actionFrontendId = "RelatedCustomerInContactDetail.add")
    })
    public ResponseEntity<ContactCustomerLinkSaveDto> linkContactWithCustomer(
            @PathVariable("contactId") Long contactId,
            @PathVariable("customerId") Long customerId,
            @Valid @RequestBody ContactCustomerLinkSaveDto contactCustomerLinkSaveDto) throws BadRequestException, EntityNotFoundException {

        ContactCustomerLinkSaveDto dto = contactCustomerLinkService.saveContactCustomerLink(contactId, customerId, contactCustomerLinkSaveDto);
        return ResponseEntity.ok(dto);
    }


    @Override
    @DeleteMapping(value = "/contact-customer-links/{linkId}")
    @AuthController(name = "deleteContactCustomerLink", controllerCase = {
            @AuthControllerCase(type= "customer", name="deleteLinkFromCustomer", actionFrontendId = "RelatedContactsInCustomerDetail.delete"),
            @AuthControllerCase(type= "contact", name="deleteLinkFromContact", actionFrontendId = "RelatedCustomerInContactDetail.delete")
    })
    public ResponseEntity<Void> deleteContactCustomerLink(@PathVariable("linkId") Long linkId) throws EntityNotFoundException {
        contactCustomerLinkService.deleteContactCustomerLink(linkId);
        return ResponseEntity.noContent().build();
    }

    @Override
    @GetMapping(value = "/customers/{customerId}/contact-links")
    @AuthController(name = "getContactLinks", actionFrontendId = "RelatedContactsInCustomerDetail.view")
    public ResponseEntity<List<ContactCustomerLinkDto>> getCustomerLinks(@PathVariable("customerId") Long customerId) throws EntityNotFoundException {
        List<ContactCustomerLinkDto> dtos = contactCustomerLinkService.getCustomerLinks(customerId);
        return ResponseEntity.ok(dtos);
    }

}
