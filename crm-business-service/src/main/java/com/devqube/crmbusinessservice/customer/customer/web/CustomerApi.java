package com.devqube.crmbusinessservice.customer.customer.web;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.customer.web.dto.*;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.search.CrmObject;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "customer")
public interface CustomerApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Create customer", nickname = "createCustomer", notes = "Method used to create customer", response = CustomerWithContactsDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "added customer successfully", response = CustomerWithContactsDto.class),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 404, message = "not found"),
            @ApiResponse(code = 409, message = "customer with this name or nip already exists")})
    @RequestMapping(value = "/customer",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<CustomerWithContactsDto> createCustomer(@ApiParam(value = "Created customer object", required = true) @Valid @RequestBody CustomerWithContactsDto customerWithContactsDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"owner\" : 6,  \"website\" : \"website\",  \"address\" : {    \"country\" : \"country\",    \"city\" : \"city\",    \"street\" : \"street\",    \"latitude\" : \"latitude\",    \"buildNumber\" : \"buildNumber\",    \"longitude\" : \"longitude\"  },  \"nip\" : \"nip\",  \"phone\" : \"phone\",  \"regon\" : \"regon\",  \"name\" : \"name\",  \"description\" : \"description\",  \"id\" : 0,  \"type\" : {    \"name\" : \"name\",    \"description\" : \"description\",    \"id\" : 6  },  \"contacts\" : [ {    \"phone\" : \"phone\",    \"surname\" : \"surname\",    \"name\" : \"name\",    \"id\" : 1,    \"position\" : \"position\",    \"email\" : \"email\",    \"primary\" : true  }, {    \"phone\" : \"phone\",    \"surname\" : \"surname\",    \"name\" : \"name\",    \"id\" : 1,    \"position\" : \"position\",    \"email\" : \"email\",    \"primary\" : true  } ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get customer by id", nickname = "getCustomer", notes = "Method used to get customer", response = Customer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "customer found", response = Customer.class),
            @ApiResponse(code = 400, message = "logged user not found"),
            @ApiResponse(code = 404, message = "customer not found")})
    @RequestMapping(value = "/customer/{customerId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Customer> getCustomer(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "The customer ID", required = true) @PathVariable("customerId") Long customerId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"country\" : \"country\",  \"city\" : \"city\",  \"phone\" : \"phone\",  \"street\" : \"street\",  \"latitude\" : \"latitude\",  \"name\" : \"name\",  \"description\" : \"description\",  \"id\" : 0,  \"type\" : {    \"name\" : \"name\",    \"description\" : \"description\",    \"id\" : 6  },  \"buildNumber\" : \"buildNumber\",  \"longitude\" : \"longitude\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get customer preview by id", nickname = "getCustomerPreview", notes = "Method used to get customer preview", response = Customer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "customer found", response = Customer.class),
            @ApiResponse(code = 400, message = "logged user not found"),
            @ApiResponse(code = 404, message = "customer not found")})
    @RequestMapping(value = "/customer/{customerId}/preview",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<CustomerPreviewDto> getCustomerPreview(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "The customer ID", required = true) @PathVariable("customerId") Long customerId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"country\" : \"country\",  \"city\" : \"city\",  \"phone\" : \"phone\",  \"street\" : \"street\",  \"latitude\" : \"latitude\",  \"name\" : \"name\",  \"description\" : \"description\",  \"id\" : 0,  \"type\" : {    \"name\" : \"name\",    \"description\" : \"description\",    \"id\" : 6  },  \"buildNumber\" : \"buildNumber\",  \"longitude\" : \"longitude\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "archive customer.", nickname = "archiveCustomer", notes = "Method used to archive customer.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "customer archived"),
            @ApiResponse(code = 404, message = "customer not found")})
    @RequestMapping(value = "/customer/{customerId}/archive",
            method = RequestMethod.PUT)
    default ResponseEntity<Void> archiveCustomer(@ApiParam(value = "", required = true)
                                                 @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                                 @ApiParam(value = "The customer ID", required = true) @PathVariable("customerId") Long customerId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "archive customers", nickname = "archiveCustomers", notes = "Method used to archive several customers.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "customer archived"),
            @ApiResponse(code = 404, message = "customer not found")})
    @RequestMapping(value = "/customer/archive",
            method = RequestMethod.PUT)
    default ResponseEntity<Void> archiveCustomers(@ApiParam(value = "", required = true)
                                                 @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                                  @ApiParam(value = "", defaultValue = "new ArrayList<>()")
                                                  @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get customer by id for edit", nickname = "getCustomerForEdit", notes = "Method used to get customer for edit", response = CustomerWithContactsDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "customer found", response = CustomerWithContactsDto.class),
            @ApiResponse(code = 404, message = "customer not found")})
    @RequestMapping(value = "/customer/for-edit/{customerId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<CustomerWithContactsDto> getCustomerForEdit(@ApiParam(value = "The customer ID", required = true) @PathVariable("customerId") Long customerId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"owner\" : 1,  \"website\" : \"website\",  \"address\" : {    \"country\" : \"country\",    \"city\" : \"city\",    \"street\" : \"street\",    \"latitude\" : \"latitude\",    \"buildNumber\" : \"buildNumber\",    \"longitude\" : \"longitude\"  },  \"nip\" : \"nip\",  \"phone\" : \"phone\",  \"regon\" : \"regon\",  \"name\" : \"name\",  \"description\" : \"description\",  \"id\" : 0,  \"type\" : {    \"name\" : \"name\",    \"description\" : \"description\",    \"id\" : 6  },  \"contacts\" : [ {    \"phone\" : \"phone\",    \"surname\" : \"surname\",    \"name\" : \"name\",    \"id\" : 5,    \"position\" : \"position\",    \"email\" : \"email\",    \"primary\" : true  }, {    \"phone\" : \"phone\",    \"surname\" : \"surname\",    \"name\" : \"name\",    \"id\" : 5,    \"position\" : \"position\",    \"email\" : \"email\",    \"primary\" : true  } ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get customers", nickname = "getCustomers", notes = "Method used to get customers", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "page of customers", response = org.springframework.data.domain.Page.class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/customer",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Page<CustomerOnListDTO>> getCustomers(
            @ApiParam(value = "", required = true)
            @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
            @ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable,
            @ApiParam(value = "only todays tasks") @Valid @RequestParam(value = "today", required = false) Boolean today,
            @ApiParam(value = "only last edited customers") @Valid @RequestParam(value = "edited", required = false) Boolean edited,
            @ApiParam(value = "only last viewed customers") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed,
            @ApiParam(value = "only archived customers") @Valid @RequestParam(value = "archived", required = false) Boolean archived,
            @ApiParam(value = "searching") @Valid @RequestParam(value = "search", required = false) String search) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get customer list for mobile association list", nickname = "getCustomersForMobileAssociationList", response = CustomerAssociationDto[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all customer names", response = CustomerAssociationDto[].class)})
    @RequestMapping(value = "/mobile/customers/for-association-list",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CustomerAssociationDto>> getCustomersForMobileAssociationList() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get customers for mobile application", nickname = "getCustomersForMobile", notes = "Method used to get customers for mobile application", response = CustomerForMobileListDto[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "page of customers", response = CustomerForMobileListDto[].class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/mobile/customer",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CustomerForMobileListDto>> getCustomersForMobile(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "only todays tasks") @Valid @RequestParam(value = "today", required = false) Boolean today, @ApiParam(value = "only last edited tasks") @Valid @RequestParam(value = "edited", required = false) Boolean edited, @ApiParam(value = "only last viewed tasks") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get customers", nickname = "getCustomersForZapier", notes = "Method used to get customers", response = Customer[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "page of customers", response = Customer[].class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/zapier/customer",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CustomerOnListDTO>> getCustomersForZapier(@ApiParam(value = "", required = true)
                                                                              @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                                                          @ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable,
                                                                          @ApiParam(value = "only todays tasks") @Valid @RequestParam(value = "today", required = false) Boolean today,
                                                                          @ApiParam(value = "only last edited tasks") @Valid @RequestParam(value = "edited", required = false) Boolean edited,
                                                                          @ApiParam(value = "only last viewed tasks") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed,
                                                                          @ApiParam(value = "searching") @Valid @RequestParam(value = "search", required = false) String search) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Delete customer", nickname = "removeCustomer", notes = "Method used to remove customer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "customer removed"),
            @ApiResponse(code = 400, message = "cannot delete customer cause it's used in opportunity"),
            @ApiResponse(code = 404, message = "customer not found")})
    @RequestMapping(value = "/customer/{customerId}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> removeCustomer(@ApiParam(value = "The customer ID", required = true) @PathVariable("customerId") Long customerId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update customer", nickname = "updateCustomer", notes = "Method used to modify customer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "customer update"),
            @ApiResponse(code = 400, message = "validation error", response = String.class),
            @ApiResponse(code = 404, message = "customer not found"),
            @ApiResponse(code = 409, message = "customer with this name or nip already exists")})
    @RequestMapping(value = "/customer/{customerId}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> updateCustomer(@ApiParam(value = "The customer ID", required = true) @PathVariable("customerId") Long customerId,
                                                @ApiParam(value = "Modified customer object", required = true)
                                                @Valid @RequestBody CustomerWithContactsDto customerWithContactsDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update list of customers", nickname = "updateListOfCustomers", notes = "Method used to modify list of customers")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "list of customers update"),
            @ApiResponse(code = 400, message = "validation error", response = String.class),
            @ApiResponse(code = 404, message = "one of customers not found")})
    @RequestMapping(value = "/customer",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> updateListOfCustomers(@ApiParam(value = "The List of customers", required = true) @RequestBody @Valid List<CustomerWithContactsDto> customers) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get customer names", nickname = "getCustomerNames", notes = "Method used to get list of customer names", response = CustomerNameDTO.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all customer names", response = CustomerNameDTO.class, responseContainer = "List")})
    @RequestMapping(value = "/customer/names",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CustomerNameDTO>> getCustomerNames(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "pattern", required = true) String pattern) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"name\" : \"name\",  \"id\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update customer contact notes", nickname = "updateCustomerNotes", notes = "Method used to update customer notes")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "notes update"),
            @ApiResponse(code = 404, message = "customer not found")})
    @RequestMapping(value = "/customer/{customerId}/contact-note",
            consumes = {"text/plain"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> updateCustomerNotes(@ApiParam(value = "The customer ID", required = true) @PathVariable("customerId") Long customerId, @ApiParam(value = "New value of contacts notes") @Valid @RequestBody(required = false) String body) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Mark contact as main", nickname = "markAsMainContact", notes = "Method used to mark customer's contact as main")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "main contact updated"),
            @ApiResponse(code = 404, message = "customer or contact not found")})
    @RequestMapping(value = "/customer/{customerId}/contact/{contactId}/mark-as-main",
            method = RequestMethod.POST)
    default ResponseEntity<Void> markAsMainContact(@ApiParam(value = "", required = true) @PathVariable("customerId") Long customerId, @ApiParam(value = "", required = true) @PathVariable("contactId") Long contactId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "delete customers", nickname = "deleteCustomers", notes = "Method used to delete customers")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "deleted customers successfully"),
            @ApiResponse(code = 400, message = "cannot delete customer cause it's used in opportunity"),
            @ApiResponse(code = 404, message = "one of customers not found")})
    @RequestMapping(value = "/customer",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteCustomers(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get customer by id with contacts list", nickname = "getCustomerWithContacts", notes = "Method used to get customer with contacts list", response = CustomerWithContactsDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "customer found", response = CustomerWithContactsDto.class),
            @ApiResponse(code = 404, message = "customer not found")})
    @RequestMapping(value = "/customer/with-contacts/{customerId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<CustomerWithContactsDto> getCustomerWithContacts(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "The customer ID", required = true) @PathVariable("customerId") Long customerId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"owner\" : 1,  \"website\" : \"website\",  \"address\" : {    \"country\" : \"country\",    \"city\" : \"city\",    \"street\" : \"street\",    \"latitude\" : \"latitude\",    \"buildNumber\" : \"buildNumber\",    \"longitude\" : \"longitude\"  },  \"nip\" : \"nip\",  \"phone\" : \"phone\",  \"regon\" : \"regon\",  \"name\" : \"name\",  \"description\" : \"description\",  \"id\" : 0,  \"type\" : {    \"name\" : \"name\",    \"description\" : \"description\",    \"id\" : 6  },  \"contacts\" : [ {    \"phone\" : \"phone\",    \"surname\" : \"surname\",    \"name\" : \"name\",    \"id\" : 5,    \"position\" : \"position\",    \"email\" : \"email\",    \"primary\" : true  }, {    \"phone\" : \"phone\",    \"surname\" : \"surname\",    \"name\" : \"name\",    \"id\" : 5,    \"position\" : \"position\",    \"email\" : \"email\",    \"primary\" : true  } ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get customers list for search", nickname = "getCustomersForSearch", notes = "Method used to get customer list", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all customer names", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/customers/search",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CrmObject>> getCustomersForSearch(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get list of customers for search", nickname = "getSearchVariants",
            notes = "Method used to get search variants", response = CustomerSearchVariantsDtoOut.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all search variants", response = CustomerSearchVariantsDtoOut.class, responseContainer = "List")})
    @RequestMapping(value = "/customers/search-variants",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CustomerSearchVariantsDtoOut>> getSearchVariants(
            @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "search") String toSearch) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get customers page", nickname = "getCustomerPage", notes = "Method used to get customer page", response = CustomerTaskDtoOut.class, responseContainer = "Page")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "customers page", response = CustomerTaskDtoOut.class, responseContainer = "Page")})
    @RequestMapping(value = "/customers/search/task",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Page<CustomerTaskDtoOut>> getCustomerPage(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term,
                                                                     @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get customers list by ids", nickname = "getCustomersAsCrmObject", notes = "Method used to get customers as crm objects by ids", response = CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found customer", response = CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/customers/crm-object",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CrmObject>> getCustomersAsCrmObject(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get customer by contact id", nickname = "getCustomerByContactId", notes = "Method used to get customer as crm objects by contact id", response = CrmObject.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found customer", response = CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/customers/crm-object/by-contact-id/{contactId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<CrmObject> getCustomerByContactId(@Valid @PathVariable(value = "contactId") Long contactId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get all customer with access for user", nickname = "getAllCustomerWithAccess", notes = "", response = Long.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of contacts for specific user", response = Long.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Email not correct")})
    @RequestMapping(value = "/access/customer",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<Long>> getAllCustomerWithAccess(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "0");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get customer name")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "customer name", response = String.class)
    })
    String getCustomerName(Long customerId) throws EntityNotFoundException;

    @ApiOperation(value = "Check if NIP exists", nickname = "checkNip", notes = "Method used to check if customer with given NIP already exists")
    @RequestMapping(value = "/customer/check-nip", method = RequestMethod.GET)
    Long checkNip(@ApiParam(value = "The customer NIP", required = true) @RequestParam("nip") String customerNip, @ApiParam(value = "The customer ID") @RequestParam(value = "customerId", required = false) Long customerId);

    @ApiOperation(value = "Get opportunities for customer", nickname = "getCustomerOpportunities", notes = "Method used to get opportunities for customer", response = CustomerSalesOpportunityDTO.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "customer's opportunities found successfully", response = CustomerSalesOpportunityDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "logged user not found"),
            @ApiResponse(code = 404, message = "customer not found")})
    @RequestMapping(value = "/customer/{customerId}/opportunities",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CustomerSalesOpportunityDTO>> getCustomerOpportunities(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "Customer ID", required = true) @PathVariable("customerId") Long customerId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get opportunities preview for customer", nickname = "getCustomerOpportunitiesPreview", notes = "Method used to get opportunities preview for customer", response = CustomerSalesOpportunityPreviewDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "customer's opportunities found successfully", response = CustomerSalesOpportunityPreviewDTO.class),
            @ApiResponse(code = 400, message = "logged user not found"),
            @ApiResponse(code = 404, message = "customer not found")})
    @RequestMapping(value = "/customer/{customerId}/opportunities-preview",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<CustomerSalesOpportunityPreviewDTO> getCustomerOpportunitiesPreview(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "Customer ID", required = true) @PathVariable("customerId") Long customerId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
