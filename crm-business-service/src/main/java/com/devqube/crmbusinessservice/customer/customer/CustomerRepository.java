package com.devqube.crmbusinessservice.customer.customer;

import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface CustomerRepository extends JpaRepository<Customer, Long>, JpaSpecificationExecutor<Customer> {
    Long countByName(String name);

    Long countByNip(String nip);

    Long countByNipAndIdNot(@Param("nip") String nip, @Param("customerId") Long customerId);

    Customer findByName(String name);

    Customer findByNip(String nip);

    @Query("SELECT c FROM Customer c left join c.address a WHERE " +
            "(c.phone != '' and c.phone is not null and LOWER(c.phone) = lower(:phone)) or " +
            "LOWER(c.name) = lower(:companyName) or " +
            "(c.website != '' and c.website is not null and LOWER(c.website) = lower(:wwwAddress)) or " +
            "(a.street != '' and a.street is not null and LOWER(a.street) = lower(:streetAndNumber))")
    List<Customer> searchCustomersForLeadCreation(@Param("phone") String phone,
                                                  @Param("companyName") String companyName,
                                                  @Param("wwwAddress") String wwwAddress,
                                                  @Param("streetAndNumber") String streetAndNumber);


    @Query("select count(c) from Customer c where c.created >= :from and c.created <= :to and c.creator in :userIds")
    Long findForGoal(@Param("from") LocalDateTime from, @Param("to") LocalDateTime to, @Param("userIds") Set<Long> userIds);

    @Query("select c from Customer c where c.created > :date")
    List<Customer> findAllByCreatedAfterForMobile(@Param("date") LocalDateTime date);

    @Query("select c from Customer c where c.updated > :date")
    List<Customer> findAllByUpdatedAfterForMobile(@Param("date") LocalDateTime date);

    @Query("select c from Customer c " +
            "where (select max(lvc.lastViewed) from LastViewedCustomer lvc where lvc.customer.id = c.id and lvc.userId = :userId) > :date ")
    List<Customer> findAllByLastViewedAfterForMobile(@Param("date") LocalDateTime date, @Param("userId") Long userId);

    Page<Customer> findAll(Pageable pageable);

    @Query("SELECT c FROM Customer c WHERE LOWER(c.name) LIKE LOWER(concat('%', ?1, '%'))")
    List<Customer> findByNameContaining(String pattern);

    @Query("SELECT c FROM Customer c WHERE c.id in ?1")
    List<Customer> findByIds(Set<Long> ids);

    @Query("SELECT c FROM Customer c WHERE c.nip in ?1")
    List<Customer> findByNips(List<String> nips);

    List<Customer> findByIdIn(List<Long> ids);

}
