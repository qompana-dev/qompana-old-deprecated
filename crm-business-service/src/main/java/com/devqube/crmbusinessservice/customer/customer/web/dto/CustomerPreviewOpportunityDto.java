package com.devqube.crmbusinessservice.customer.customer.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerPreviewOpportunityDto {
    private Long id;

    private String name;

    private LocalDate finishDate;

    private Double amount;

    private String currency;

    private String processInstanceId;

    private String currentTask;
}
