package com.devqube.crmbusinessservice.customer.gus;

import com.devqube.crmbusinessservice.customer.address.Address;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.exception.InvalidGUSResponseException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Slf4j
class GUSResponseParser {
    private static final String RESPONSE_PATH = "/root/dane";
    private Document doc;
    private XPath xpath;

    GUSResponseParser(String xml) throws EntityNotFoundException {
        doc = getDocument(xml);
        xpath = XPathFactory.newInstance().newXPath();
    }

    Customer extractCustomer(String type, String nip, String regon) throws InvalidGUSResponseException {
        Customer customer = new Customer();
        customer.setNip(nip);
        customer.setRegon(regon);
        if (isPresent(RESPONSE_PATH + "/" + type + "_nazwa")) {
            customer.setName(getValue(RESPONSE_PATH + "/" + type + "_nazwa"));
        }
        if (isPresent(RESPONSE_PATH + "/" + type + "_adresStronyinternetowej")) {
            customer.setWebsite(getValue(RESPONSE_PATH + "/" + type + "_adresStronyinternetowej"));
        }
        if (isPresent(RESPONSE_PATH + "/" + type + "_numerTelefonu")) {
            String phoneToAdd = getValue(RESPONSE_PATH + "/" + type + "_numerTelefonu");
            if (phoneToAdd.length() == 9) {
                phoneToAdd = "48" + phoneToAdd;
            }
            if (phoneToAdd.length() == 10 && phoneToAdd.charAt(0) == '0') {
                phoneToAdd = "48" + phoneToAdd.substring(1, phoneToAdd.length());
            }
            customer.setPhone(phoneToAdd);
        }
        customer.setAddress(parseAddress(type));
        return customer;
    }

    Pair<String, String> extractRegonAndType() throws InvalidGUSResponseException, EntityNotFoundException {
        if (!isPresent(RESPONSE_PATH)) {
            throw new EntityNotFoundException("Customer was not found in GUS");
        }
        return new ImmutablePair<>(parseRegon(), parseType());
    }

    private Address parseAddress(String type) throws InvalidGUSResponseException {
        Address address = new Address();
        if (isPresent(RESPONSE_PATH + "/" + type + "_adSiedzKraj_Nazwa")) {
            address.setCountry(getValue(RESPONSE_PATH + "/" + type + "_adSiedzKraj_Nazwa"));
        }
        if (isPresent(RESPONSE_PATH + "/" + type + "_adSiedzMiejscowosc_Nazwa")) {
            address.setCity(getValue(RESPONSE_PATH + "/" + type + "_adSiedzMiejscowosc_Nazwa"));
        }
        if (isPresent(RESPONSE_PATH + "/" + type + "_adSiedzKodPocztowy")) {
            address.setZipCode(getValue(RESPONSE_PATH + "/" + type + "_adSiedzKodPocztowy"));
        }
        if (isPresent(RESPONSE_PATH + "/" + type + "_adSiedzWojewodztwo_Nazwa")) {
            address.setProvince(getValue(RESPONSE_PATH + "/" + type + "_adSiedzWojewodztwo_Nazwa"));
        }

        String street = null, buildNumber = null, flatNumber = null;
        if (isPresent(RESPONSE_PATH + "/" + type + "_adSiedzUlica_Nazwa")) {
            street = Strings.emptyToNull(getValue(RESPONSE_PATH + "/" + type + "_adSiedzUlica_Nazwa"));
        }
        if (isPresent(RESPONSE_PATH + "/" + type + "_adSiedzNumerNieruchomosci")) {
            buildNumber = Strings.emptyToNull(getValue(RESPONSE_PATH + "/" + type + "_adSiedzNumerNieruchomosci"));
        }
        if (isPresent(RESPONSE_PATH + "/" + type + "_adSiedzNumerLokalu")) {
            flatNumber = Strings.emptyToNull(getValue(RESPONSE_PATH + "/" + type + "_adSiedzNumerLokalu"));
        }
        address.setStreet(getStreet(street, buildNumber, flatNumber));

        return address;
    }

    private String getStreet(String street, String buildNumber, String flatNumber) {
        String result = "";
        if (street != null) {
            result += (buildNumber != null || flatNumber != null) ? street + " " : street;
        }
        if (buildNumber != null) {
            result += (flatNumber != null) ? buildNumber + "/" : buildNumber;
        }
        if (flatNumber != null) {
            result += flatNumber;
        }
        return Strings.emptyToNull(result);
    }

    private String parseType() throws InvalidGUSResponseException {
        if (isPresent(RESPONSE_PATH + "/RegonLink")) {
            String regonLink = getValue(RESPONSE_PATH + "/RegonLink/text()");
            if (regonLink.contains("DaneRaportPrawna")) {
                return "PublDaneRaportPrawna";
            } else {
                return "PublDaneRaportDzialalnoscFizycznejCeidg";
            }
        }
        throw new InvalidGUSResponseException("Invalid response from GUS");
    }

    private String parseRegon() throws InvalidGUSResponseException {
        if (isPresent(RESPONSE_PATH + "/Regon")) {
            return getValue(RESPONSE_PATH + "/Regon/text()");
        }
        throw new InvalidGUSResponseException("Invalid response from GUS");
    }

    private boolean isPresent(String expression) throws InvalidGUSResponseException {
        try {
            XPathExpression expr = xpath.compile(expression);
            return ((NodeList) expr.evaluate(doc, XPathConstants.NODESET)).getLength() != 0;
        } catch (XPathExpressionException e) {
            throw new InvalidGUSResponseException("There was some problem with response from GUS");
        }
    }

    private String getValue(String expression) throws InvalidGUSResponseException {
        try {
            XPathExpression expr = xpath.compile(expression);
            return (String) expr.evaluate(doc, XPathConstants.STRING);
        } catch (XPathExpressionException e) {
            throw new InvalidGUSResponseException("There was some problem with response from GUS");
        }
    }

    private static Document getDocument(String xml) throws EntityNotFoundException {
        try {
            DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
            domFactory.setNamespaceAware(true);
            DocumentBuilder builder = domFactory.newDocumentBuilder();
            return builder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8))));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new EntityNotFoundException("There was some problem with response from GUS");
        }
    }

    private String letterSize(String text) {
        text = text.toLowerCase();
        String[] whitespaceText = text.trim().split(" ");
        text = "";
        for (String word : whitespaceText) {
            word = word.replace(".{1}", word.substring(0,1).toUpperCase());
            text += word + " ";
        }
        String[] dashText = text.trim().split("-");
        text = "";
        for (String word : dashText) {
            word = word.replace(".{1}", word.substring(0,1).toUpperCase());
            text += word + " ";
        }
        return text.trim();
    }
}
