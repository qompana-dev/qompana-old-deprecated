package com.devqube.crmbusinessservice.customer.contactcustomerlink.dto;

import com.devqube.crmbusinessservice.customer.contactcustomerlink.RelationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactCustomerLinkDto {
    private Long id;
    private String customerName;
    private String contactName;
    private Long customerId;
    private Long contactId;
    private String position;
    private LocalDate startDate;
    private LocalDate endDate;
    private String note;
    private RelationType relationType;
    private Boolean active;
    private Boolean firstToContact;
    private String avatar;
}
