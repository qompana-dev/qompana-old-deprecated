package com.devqube.crmbusinessservice.customer.customer;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.customer.address.AddressRepository;
import com.devqube.crmbusinessservice.customer.contact.ContactCustomerRelationService;
import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.customer.model.LastViewedCustomer;
import com.devqube.crmbusinessservice.customer.customer.web.dto.*;
import com.devqube.crmbusinessservice.customer.exception.NameOccupiedException;
import com.devqube.crmbusinessservice.customer.type.TypeRepository;
import com.devqube.crmbusinessservice.customer.whitelist.WhitelistQueue;
import com.devqube.crmbusinessservice.dictionary.WordRepository;
import com.devqube.crmbusinessservice.dictionary.WordSpecifications;
import com.devqube.crmbusinessservice.leadopportunity.AccountsService;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityService;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.phoneNumber.PhoneNumberService;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.web.CurrentRequestUtil;
import feign.FeignException;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.devqube.crmbusinessservice.customer.customer.CustomerSpecifications.*;
import static com.devqube.crmbusinessservice.customer.customer.web.dto.CustomerSearchType.*;


@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final AddressRepository addressRepository;
    private final TypeRepository typeRepository;
    private final AccessService accessService;
    private final UserClient userClient;
    private final LastViewedCustomerRepository lastViewedCustomerRepository;
    private final OpportunityService opportunityService;
    private final ModelMapper modelMapper;
    private final UserService userService;
    private final ContactCustomerRelationService contactCustomerRelationService;
    private final AccountsService accountsService;
    private final ContactRepository contactRepository;
    private final PhoneNumberService phoneNumberService;
    private final WordRepository wordRepository;

    public CustomerService(CustomerRepository customerRepository, AddressRepository addressRepository, TypeRepository typeRepository,
                           AccessService accessService,UserClient userClient, LastViewedCustomerRepository lastViewedCustomerRepository,
                           OpportunityService opportunityService, ModelMapper modelMapper, UserService userService,
                           ContactCustomerRelationService contactCustomerRelationService, AccountsService accountsService,
                           ContactRepository contactRepository, PhoneNumberService phoneNumberService,
                           WordRepository wordRepository) {
        this.customerRepository = customerRepository;
        this.addressRepository = addressRepository;
        this.typeRepository = typeRepository;
        this.accessService = accessService;
        this.userClient = userClient;
        this.lastViewedCustomerRepository = lastViewedCustomerRepository;
        this.opportunityService = opportunityService;
        this.modelMapper = modelMapper;
        this.userService = userService;
        this.contactCustomerRelationService = contactCustomerRelationService;
        this.accountsService = accountsService;
        this.contactRepository = contactRepository;
        this.phoneNumberService = phoneNumberService;
        this.wordRepository = wordRepository;
    }

    public Long countByNipWithCustomer(String nip, Long customerId) {
        if (customerId != null) {
            return customerRepository.countByNipAndIdNot(nip, customerId);
        } else {
            return customerRepository.countByNip(nip);
        }
    }

    @Transactional
    public CustomerWithContactsDto save(CustomerWithContactsDto customer) throws NameOccupiedException, BadRequestException {
        checkIfValidMainContactsCount(customer);
        if (customerRepository.countByName(customer.getName()) == 0 && customerRepository.countByNip(customer.getNip()) == 0) {
            Long currentUserId = userService.getUserId(CurrentRequestUtil.getLoggedInAccountEmail());
            Customer customerToSave = accessService.assignAndVerify(customer).toModel();
            customerToSave.setCreated(LocalDateTime.now());
            customerToSave.setCreator(currentUserId);
            if (customer.getAdditionalPhones() != null && customer.getAdditionalPhones().size() > 0) {
                phoneNumberService.setCustomerAdditionalPhoneNumbers(customerToSave, customer.getAdditionalPhones());
            }
            customerToSave = customerRepository.save(customerToSave);
            customerToSave.setContacts(CustomerWithContactsDto.getContacts(customer.getContacts(), customerToSave, null));
            Customer result = customerRepository.save(customerToSave);
            result.getAdditionalPhones().forEach(phone -> phone.setType(wordRepository.findOne(WordSpecifications.findByPhoneId(phone.getId())).get()));
            WhitelistQueue.getInstance().add(result.getNip());
            return CustomerWithContactsDto.fromModel(result);
        } else {
            throw new NameOccupiedException("Customer with this name or nip already exists");
        }
    }

    @Transactional(rollbackFor = FeignException.class)
    public void modify(CustomerWithContactsDto customer) throws NameOccupiedException, EntityNotFoundException, BadRequestException {
        if (customer == null || customer.getId() == null) {
            throw new BadRequestException("id is null");
        }
        checkIfValidModification(customer);
        checkIfValidMainContactsCount(customer);
        Customer fromDb = customerRepository.findById(customer.getId()).orElseThrow(EntityNotFoundException::new);
        CustomerWithContactsDto customerWithContactsDto = accessService.assignAndVerify(CustomerWithContactsDto.fromModel(fromDb), customer);
        phoneNumberService.modifyCustomerAdditionalPhoneNumbers(fromDb, customer.getAdditionalPhones());
        Customer customerToSave = customerWithContactsDto.toModel(fromDb);
        if (customer.getType() != null && customer.getType().getId() != null) {
            customer.setType(typeRepository.getOne(customer.getType().getId()));
        }
        Customer savedCustomer = customerRepository.save(customerToSave);
        saveCustomerContactLink(savedCustomer);
        WhitelistQueue.getInstance().add(customerToSave.getNip());
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void modify(List<CustomerWithContactsDto> customers) throws NameOccupiedException, EntityNotFoundException, BadRequestException {
        for (CustomerWithContactsDto customer : customers)
            modify(customer);
    }

    private void saveCustomerContactLink(Customer customer) {
        customer.getContacts().stream()
                .filter(contact -> CollectionUtils.isEmpty(contact.getCustomerLinks()))
                .forEach(contactCustomerRelationService::saveNewLink);
    }


    public Page<CustomerOnListDTO> findAllCreatedToday(String search, Pageable pageable) {
        Specification<Customer> spec =
                findCreatedAfterExclusive(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT))
                        .and(findByArchived(false))
                        .and(searchByCustomFields(search));
        return customerRepository.findAll(spec, pageable).map(this::mapToCustomerOnListDTO);
    }

    public Page<CustomerOnListDTO> findAllRecentlyEdited(String search, Pageable pageable) {
        Specification<Customer> spec =
                findUpdatedAfterExclusive(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT))
                        .and(findByArchived(false))
                        .and(searchByCustomFields(search));
        return customerRepository.findAll(spec, pageable).map(this::mapToCustomerOnListDTO);
    }

    public Page<CustomerOnListDTO> findAllRecentlyViewed(String loggedAccountEmail, String search, Pageable pageable) throws BadRequestException {
        Long userId = getUserId(loggedAccountEmail);
        Specification<Customer> spec =
                findLastViewedAfterExclusive(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3), userId)
                        .and(findByArchived(false))
                        .and(searchByCustomFields(search));
        return customerRepository.findAll(spec, pageable)
                .map(this::mapToCustomerOnListDTO);
    }

    public Page<CustomerOnListDTO> findAllArchived(String search, Pageable pageable) {
        Specification<Customer> spec = findByArchived(true).and(searchByCustomFields(search));
        return customerRepository.findAll(spec, pageable)
                .map(this::mapToCustomerOnListDTO);
    }

    public Page<CustomerOnListDTO> findAll(Pageable pageable) {
        return customerRepository.findAll(findByArchived(false), pageable)
                .map(this::mapToCustomerOnListDTO);
    }

    public Page<CustomerOnListDTO> findAll(String search, Pageable pageable) {
        Specification<Customer> spec = findByArchived(false).and(searchByCustomFields(search));
        return customerRepository.findAll(spec, pageable)
                .map(this::mapToCustomerOnListDTO);
    }

    public Page<Customer> findAll(Specification<Customer> spec, Pageable pageable) {
        return customerRepository.findAll(spec, pageable);
    }

    public List<CustomerForMobileListDto> findAllCreatedTodayMob() {
        return customerRepository.findAllByCreatedAfterForMobile(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT)).stream().map(CustomerForMobileListDto::new).collect(Collectors.toList());
    }

    public List<CustomerForMobileListDto> findAllRecentlyEditedMob() {
        return customerRepository.findAllByUpdatedAfterForMobile(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3)).stream().map(CustomerForMobileListDto::new).collect(Collectors.toList());
    }

    public List<CustomerForMobileListDto> findAllRecentlyViewedMob(String loggedAccountEmail) throws BadRequestException {
        Long userId = getUserId(loggedAccountEmail);
        return customerRepository.findAllByLastViewedAfterForMobile(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3), userId).stream().map(CustomerForMobileListDto::new).collect(Collectors.toList());
    }

    public List<CustomerForMobileListDto> findAllMob() {
        return customerRepository.findAll().stream().map(CustomerForMobileListDto::new).collect(Collectors.toList());
    }

    public List<CustomerAssociationDto> getCustomersForMobileAssociationList() {
        return customerRepository.findAll().stream()
                .map(CustomerAssociationDto::new)
                .collect(Collectors.toList());
    }

    public List<Customer> findAllContaining(String pattern) {
        return customerRepository.findByNameContaining(pattern);
    }

    public List<Customer> findByIds(String ids) {
        Set<Long> idSet = Arrays.stream(ids.split(";"))
                .map(Long::parseLong)
                .collect(Collectors.toSet());
        return customerRepository.findByIds(idSet);
    }

    public List<Customer> findAllByIds(List<Long> ids) {
        return customerRepository.findByIdIn(ids);
    }

    public CustomerWithContactsDto findCustomerById(Long id) throws EntityNotFoundException {
        return CustomerWithContactsDto.fromModel(customerRepository.findById(id).orElseThrow(EntityNotFoundException::new));
    }

    public Customer findById(Long id) throws EntityNotFoundException {
        return customerRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Customer with id " + id + " not found"));
    }

    public void saveLastViewed(Customer customer, String loggedAccountEmail) throws BadRequestException {
        Long userId = getUserId(loggedAccountEmail);
        LastViewedCustomer newLastViewed = LastViewedCustomer.builder().userId(userId).customer(customer).build();

        LastViewedCustomer lastViewedCustomer = (customer.getLastViewedCustomers() == null) ? newLastViewed : customer.getLastViewedCustomers().stream()
                .filter(lv -> lv.getUserId().equals(userId)).findFirst()
                .orElse(newLastViewed);

        lastViewedCustomer.setLastViewed(LocalDateTime.now());
        lastViewedCustomerRepository.save(lastViewedCustomer);
    }

    @Transactional(rollbackFor = FeignException.class)
    public void deleteById(Long id) throws EntityNotFoundException, BadRequestException {
        Optional<Customer> byId = customerRepository.findById(id);
        if (byId.isEmpty()) {
            throw new EntityNotFoundException("Customer with id " + id + " not found");
        }
        if (this.opportunityService.opportunityExistsByCustomerId(id)) {
            throw new BadRequestException("cannot remove contact; it's used in opportunity");
        }
        customerRepository.deleteById(id);
    }

    private void checkIfValidMainContactsCount(CustomerWithContactsDto customer) throws BadRequestException {
        if (customer != null && customer.getContacts() != null && customer.getContacts().size() > 1) {
            long mainContactsCount = customer.getContacts().stream().filter(c -> c.getMainContact() != null).filter(ContactDto::getMainContact).count();
            if (mainContactsCount > 1) {
                throw new BadRequestException();
            }
        }

    }

    private void checkIfValidModification(CustomerWithContactsDto customer) throws EntityNotFoundException, NameOccupiedException {
        Optional<Customer> byId = customerRepository.findById(customer.getId());
        if (byId.isEmpty()) {
            throw new EntityNotFoundException("Customer with id " + customer.getId() + " not found");
        }
        if (!byId.get().getName().equals(customer.getName()) && customerRepository.findByName(customer.getName()) != null) {
            throw new NameOccupiedException("Name " + customer.getName() + " is occupied.");
        }
        if (!byId.get().getNip().equals(customer.getNip()) && customerRepository.findByNip(customer.getNip()) != null) {
            throw new NameOccupiedException("NIP " + customer.getNip() + " is occupied.");
        }
    }

    public void saveNotes(Long customerId, String notes) throws EntityNotFoundException {
        Customer customer = customerRepository.findById(customerId).orElseThrow(EntityNotFoundException::new);
        customer.setContactsNotes(notes);
        customerRepository.save(customer);
    }

    public void markAsMainContact(Long customerId, Long contactId) throws EntityNotFoundException {
        Customer customer = customerRepository.findById(customerId).orElseThrow(EntityNotFoundException::new);
        Contact contactToMark = customer.getContacts().stream().filter(contact -> contact.getId().equals(contactId)).findFirst().orElseThrow(EntityNotFoundException::new);
        customer.getContacts().forEach(contact -> contact.setMainContact(false));
        contactToMark.setMainContact(true);
        customerRepository.save(customer);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteCustomers(List<Long> ids) throws EntityNotFoundException, BadRequestException {
        for (Long id : ids) {
            deleteById(id);
        }
    }

    @Transactional
    public Customer getCustomerByContactId(Long contactId) throws EntityNotFoundException {
        Contact contact = this.contactRepository.findById(contactId).orElseThrow(EntityNotFoundException::new);
        return Optional.ofNullable(contact.getCustomer()).orElseThrow(EntityNotFoundException::new);
    }

    private Long getUserId(String accountEmail) throws BadRequestException {
        try {
            return userClient.getMyAccountId(accountEmail);
        } catch (Exception e) {
            throw new BadRequestException("user not found");
        }
    }

    public List<Long> findAll() {
        return customerRepository.findAll().stream().map(Customer::getId).collect(Collectors.toList());
    }

    public CustomerPreviewDto getCustomerPreview(Long customerId, String email) throws BadRequestException, EntityNotFoundException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(email);
        Customer customer = customerRepository.findById(customerId).orElseThrow(EntityNotFoundException::new);
        List<Opportunity> allByCustomerId = opportunityService.findAllByCustomerId(customerId, userIds);
        CustomerPreviewDto customerDto = modelMapper.map(customer, CustomerPreviewDto.class);
        customerDto.setPreviewOpportunityDtoList(allByCustomerId.stream()
                .map(c -> modelMapper.map(c, CustomerPreviewOpportunityDto.class))
                .peek(c -> c.setCurrentTask(opportunityService.getCurrentTaskForOpportunityProcessId(c.getProcessInstanceId())))
                .collect(Collectors.toList()));
        return customerDto;
    }

    private CustomerOnListDTO mapToCustomerOnListDTO(Customer customer) {
        ContactDto firstContact = null;
        Set<Contact> contacts = customer.getContacts();
        if (!contacts.isEmpty()) {
            Optional<Contact> firstFound = contacts.stream()
                    .filter(contact -> Objects.nonNull(contact.getMainContact()))
                    .filter(Contact::getMainContact).findFirst();
            firstContact = firstFound.map(ContactDto::fromModel)
                    .orElseGet(() -> ContactDto.fromModel(contacts.stream().findFirst().get()));
        }
        return new CustomerOnListDTO(customer, firstContact);
    }

    public String getCustomerName(Long customerId) throws EntityNotFoundException {
        Customer customer = customerRepository.findById(customerId).orElseThrow(() -> new EntityNotFoundException("Customer with id " + customerId + " not found"));
        return customer.getName();
    }

    private <D, T> Page<T> mapPage(Page<D> inputPages, Class<T> outputClass) {
        return inputPages.map(entity -> modelMapper.map(entity, outputClass));
    }

    public List<CustomerSalesOpportunityDTO> getCustomerOpportunities(String loggedAccountEmail, Long customerId)
            throws BadRequestException, EntityNotFoundException {
        customerRepository.findById(customerId).orElseThrow(() -> new EntityNotFoundException("Customer with id " + customerId + " not found"));
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return opportunityService.findAllByCustomerId(customerId, userIds)
                .stream()
                .map(opportunity -> modelMapper.map(opportunity, CustomerSalesOpportunityDTO.class))
                .collect(Collectors.toList());
    }

    public CustomerSalesOpportunityPreviewDTO getCustomerOpportunitiesPreview(String loggedAccountEmail, Long customerId)
            throws BadRequestException, EntityNotFoundException {
        customerRepository.findById(customerId).orElseThrow(() -> new EntityNotFoundException("Customer with id " + customerId + " not found"));
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        List<CustomerSalesOpportunityDTO> all = opportunityService.findAllByCustomerId(customerId, userIds)
                .stream()
                .map(opportunity -> modelMapper.map(opportunity, CustomerSalesOpportunityDTO.class))
                .collect(Collectors.toList());
        CustomerSalesOpportunityPreviewDTO preview = new CustomerSalesOpportunityPreviewDTO();
        preview.setTotalNumber((long) all.size());
        preview.setOpportunities(all.stream().limit(4).collect(Collectors.toList()));
        return preview;
    }

    public List<CustomerSearchVariantsDtoOut> findAllSearchVariants(String toSearch) throws BadRequestException {
        List<CustomerSearchVariantsDtoOut> searchVariants = new ArrayList<>();

        searchVariants.add(new CustomerSearchVariantsDtoOut()
                .setType(COMPANY_NAME)
                .setVariants(customerRepository.findAll(findByName(toSearch)).stream()
                        .map(Customer::getName)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        searchVariants.add(new CustomerSearchVariantsDtoOut()
                .setType(COMPANY_CITY)
                .setVariants(customerRepository.findAll(findByCity(toSearch)).stream()
                        .map(customer -> customer.getAddress().getCity())
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        searchVariants.add(new CustomerSearchVariantsDtoOut()
                .setType(PHONE)
                .setVariants(customerRepository.findAll(findByPhone(toSearch)).stream()
                        .map(Customer::getPhone)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        return searchVariants;
    }

    public void archiveCustomer(Long customerId) throws EntityNotFoundException, BadRequestException {
        Optional<Customer> byId = customerRepository.findById(customerId);
        if (byId.isEmpty()) {
            throw new EntityNotFoundException("Customer with id " + customerId + " not found");
        }
        Customer customer = byId.get();
        if (customer.getArchived()!=null && customer.getArchived()) {
            throw new BadRequestException("cannot archive customer; it's already archived");
        }
        customer.setArchived(true);
        customerRepository.save(customer);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void archiveCustomers(List<Long> ids) throws EntityNotFoundException, BadRequestException {
        for (Long id : ids) {
            archiveCustomer(id);
        }
    }
}
