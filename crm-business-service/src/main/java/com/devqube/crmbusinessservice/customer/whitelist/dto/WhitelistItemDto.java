package com.devqube.crmbusinessservice.customer.whitelist.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WhitelistItemDto {
    private WhitelistItemResultDto result;
    private String code;
    private String message;
}

