package com.devqube.crmbusinessservice.customer.customer.web.dto;

import com.devqube.crmbusinessservice.customer.address.Address;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.type.Type;
import com.devqube.crmbusinessservice.phoneNumber.PhoneNumberMapper;
import com.devqube.crmbusinessservice.phoneNumber.web.dto.PhoneNumberDTO;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@AuthFilterEnabled
public class CustomerWithContactsDto {
    @EqualsAndHashCode.Include
    @AuthFields(list = {
            @AuthField(controller = "createCustomer"),
            @AuthField(controller = "getCustomerForEdit"),
            @AuthField(controller = "getCustomerForEditInContactInfo"),
            @AuthField(controller = "updateCustomer"),
            @AuthField(controller = "updateCustomerInContactInfo"),
            @AuthField(controller = "getCustomerWithContacts")
    })
    private Long id;

    @NotNull
    @Length(max = 10)
    @EqualsAndHashCode.Include
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.nipField", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.nipField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.nipField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.nipField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.nipField", controller = "updateCustomerInContactInfo"),
            @AuthField(frontendId = "CustomerDetailsComponent.nip", controller = "getCustomerWithContacts")
    })
    private String nip;

    @Length(max = 14)
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.regonField", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.regonField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.regonField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.regonField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.regonField", controller = "updateCustomerInContactInfo"),
    })
    private String regon;

    @Length(max = 200)
    @NotNull
    @EqualsAndHashCode.Include
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.nameField", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.nameField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.nameField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.nameField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.nameField", controller = "updateCustomerInContactInfo"),
            @AuthField(frontendId = "CustomerDetailsComponent.name", controller = "getCustomerWithContacts")
    })
    private String name;

    @Length(max = 500)
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.descriptionField", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.descriptionField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerEditComponent.descriptionField", controller = "updateCustomer"),
    })
    private String description;

    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.typeField", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.typeField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerEditComponent.typeField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerDetailsComponent.type", controller = "getCustomerWithContacts")
    })
    private Type type;

    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.phoneField", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.phoneField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.phoneField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.phoneField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.phoneField", controller = "updateCustomerInContactInfo"),
            @AuthField(frontendId = "CustomerDetailsComponent.phone", controller = "getCustomerWithContacts")
    })
    private String phone;

    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.phoneField", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.phoneField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.phoneField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.phoneField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.phoneField", controller = "updateCustomerInContactInfo"),
            @AuthField(frontendId = "CustomerDetailsComponent.phone", controller = "getCustomerWithContacts")
    })
    private Set<PhoneNumberDTO> additionalPhones;

    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.ownerField", controller = "createCustomer"),
            @AuthField(controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.ownerField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerEditComponent.ownerField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.ownerField", controller = "updateListOfCustomers"),
            @AuthField(frontendId = "CustomerDetailsComponent.owner", controller = "getCustomerWithContacts")
    })
    private Long owner;

    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerDetailsComponent.activeDelivery", controller = "getCustomerWithContacts"),
            @AuthField(controller = "updateCustomer"),
    })
    private Boolean activeDelivery = false;

    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "CustomerDetailsComponent.activeClient", controller = "getCustomerWithContacts"),
            @AuthField(controller = "updateCustomer"),
    })
    private Boolean activeClient = false;


    @AuthFields(list = {
            @AuthField(frontendId = "CustomerDetailsComponent.foreignCompany", controller = "getCustomerWithContacts"),
            @AuthField(controller = "updateCustomer"),
    })
    private Boolean foreignCompany = false;

    @AuthFields(list = {
            @AuthField(controller = "getCustomerForEdit"),
            @AuthField(controller = "getCustomerForEditInContactInfo"),
            @AuthField(controller = "updateCustomer"),
            @AuthField(controller = "updateCustomerInContactInfo"),
            @AuthField(controller = "createCustomer"),
            @AuthField(frontendId = "CustomerDetailsComponent.address", controller = "getCustomerWithContacts")
    })
    private Address address;

    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.websiteField", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.websiteField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.websiteField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.websiteField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.websiteField", controller = "updateCustomerInContactInfo"),
            @AuthField(frontendId = "CustomerDetailsComponent.webpage", controller = "getCustomerWithContacts")
    })
    private String website;

    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.linkedInField", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.linkedInField", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.linkedInField", controller = "getCustomerForEditInContactInfo"),
            @AuthField(frontendId = "CustomerEditComponent.linkedInField", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerInContactInfoComponent.linkedInField", controller = "updateCustomerInContactInfo"),
            @AuthField(frontendId = "CustomerDetailsComponent.linkedIn", controller = "getCustomerWithContacts")
    })
    private String linkedIn;

    @AuthFields(list = {
            @AuthField(controller = "createCustomer"),
            @AuthField(controller = "getCustomerForEdit"),
            @AuthField(controller = "getCustomerForEditInContactInfo"),
            @AuthField(controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerDetailsComponent.contacts", controller = "getCustomerWithContacts")
    })
    @Valid
    private List<ContactDto> contacts;


    @AuthFields(list = {
            @AuthField(frontendId = "CustomerContactsComponent.notes", controller = "getCustomerWithContacts")
    })
    private String contactsNotes;

    @AuthFields(list = {
            @AuthField(frontendId = "CustomerDetailsComponent.cooperationStart", controller = "getCustomerWithContacts")
    })
    private LocalDateTime created;


    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.employeesNumber", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.employeesNumber", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerEditComponent.employeesNumber", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerDetailsComponent.employeesNumber", controller = "getCustomerWithContacts")
    })
    private Long employeesNumber;

    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.industry", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.industry", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerEditComponent.industry", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerDetailsComponent.industry", controller = "getCustomerWithContacts")
    })
    private Long industry;


    @AuthFields(list = {
            @AuthField(frontendId = "CustomerAddComponent.income", controller = "createCustomer"),
            @AuthField(frontendId = "CustomerEditComponent.income", controller = "getCustomerForEdit"),
            @AuthField(frontendId = "CustomerEditComponent.income", controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerDetailsComponent.income", controller = "getCustomerWithContacts")
    })
    private String income;

    @AuthFields(list = {
            @AuthField(controller = "createCustomer"),
            @AuthField(controller = "getCustomerForEdit"),
            @AuthField(controller = "updateCustomer"),
            @AuthField(frontendId = "CustomerContactsComponent.avatar", controller = "getCustomerWithContacts")
    })
    private String avatar;

    @AuthFields(list = {
            @AuthField(controller = "createCustomer"),
            @AuthField(controller = "getCustomerForEdit"),
            @AuthField(controller = "updateCustomer"),
            @AuthField(controller = "getCustomerForEditInContactInfo"),
            @AuthField(controller = "updateCustomerInContactInfo"),
            @AuthField(controller = "getCustomerWithContacts")
    })
    private String additionalData;

    public static CustomerWithContactsDto fromModel(Customer customer) {
        CustomerWithContactsDto dto = new CustomerWithContactsDto();
        dto.setId(customer.getId());
        dto.setNip(customer.getNip());
        dto.setRegon(customer.getRegon());
        dto.setName(customer.getName());
        dto.setDescription(customer.getDescription());
        dto.setType(customer.getType());
        dto.setPhone(customer.getPhone());
        dto.setAdditionalPhones(PhoneNumberMapper.mapToPhoneNumberDtoSet(customer.getAdditionalPhones()));
        dto.setOwner(customer.getOwner());
        dto.setActiveClient(customer.getActiveClient());
        dto.setActiveDelivery(customer.getActiveDelivery());
        dto.setForeignCompany(customer.getForeignCompany());
        dto.setAddress(customer.getAddress());
        dto.setWebsite(customer.getWebsite());
        dto.setLinkedIn(customer.getLinkedIn());
        dto.setCreated(customer.getCreated());
        dto.setContactsNotes(customer.getContactsNotes());
        dto.setEmployeesNumber(customer.getEmployeesNumber());
        dto.setIndustry(customer.getIndustry());
        dto.setIncome(customer.getIncome());
        dto.setAvatar(customer.getAvatar());
        dto.setAdditionalData(customer.getAdditionalData());


        if (customer.getContacts() == null || customer.getContacts().size() == 0) {
            dto.setContacts(new ArrayList<>());
        } else {
            dto.setContacts(customer.getContacts()
                    .stream().map(ContactDto::fromModel)
                    .collect(Collectors.toList()));
        }

        return dto;
    }

    public Customer toModel() {
        Customer customer = new Customer();
        setBasicData(customer);
        return customer;
    }

    public Customer toModel(Customer fromDb) {
        Customer customer = (fromDb == null) ? new Customer() : fromDb;
        setBasicData(customer);

        if (this.getContacts() != null) {
            customer.setContacts(getContacts(this.getContacts(), customer, fromDb));
        }
        return customer;
    }

    public static Set<Contact> getContacts(List<ContactDto> contacts, Customer customer, Customer fromDb) {
        if (contacts == null) {
            return null;
        }
        return contacts.stream()
                .map(dto -> dto.toModel(getContactFromDbById(fromDb, dto.getId()), customer.getName()))
                .peek(dto -> dto.setCustomer(customer))
                .peek(dto -> dto.setOwner(customer.getOwner()))
                .peek(dto -> dto.setCreator(customer.getCreator()))
                .collect(Collectors.toSet());
    }

    private void setBasicData(Customer customer) {
        customer.setId(this.getId());
        customer.setNip(this.getNip());
        customer.setRegon(this.getRegon());
        customer.setName(this.getName());
        customer.setDescription(this.getDescription());
        customer.setType(this.getType());
        customer.setPhone(this.getPhone());
        customer.setOwner(this.getOwner());
        customer.setAddress(this.getAddress());
        customer.setActiveClient(this.getActiveClient());
        customer.setActiveDelivery(this.getActiveDelivery());
        customer.setForeignCompany(this.getForeignCompany());
        customer.setWebsite(this.getWebsite());
        customer.setLinkedIn(this.getLinkedIn());
        customer.setContactsNotes(this.getContactsNotes());
        customer.setEmployeesNumber(this.getEmployeesNumber());
        customer.setIndustry(this.getIndustry());
        customer.setIncome(this.getIncome());
        customer.setAvatar(this.getAvatar());
        customer.setAdditionalData(this.getAdditionalData());
    }

    private static Contact getContactFromDbById(Customer fromDb, Long id) {
        if (fromDb == null || fromDb.getContacts() == null) {
            return null;
        }
        return fromDb.getContacts().stream().filter(c -> c.getId().equals(id)).findFirst().orElse(null);
    }
}

