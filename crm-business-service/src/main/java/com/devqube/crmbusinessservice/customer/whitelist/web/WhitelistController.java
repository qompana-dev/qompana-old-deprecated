package com.devqube.crmbusinessservice.customer.whitelist.web;

import com.devqube.crmbusinessservice.customer.whitelist.WhitelistService;
import com.devqube.crmbusinessservice.customer.whitelist.model.Whitelist;
import com.devqube.crmshared.access.annotation.AuthController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class WhitelistController implements WhitelistApi {
    private final WhitelistService whitelistService;

    public WhitelistController(WhitelistService whitelistService) {
        this.whitelistService = whitelistService;
    }

    @Override
    @AuthController(actionFrontendId = "WhitelistStatusInCustomer.refresh")
    public ResponseEntity<Void> refreshWhitelistStatus(String nip) {
        whitelistService.refreshWhitelistStatus(nip);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @AuthController(readFrontendId = "WhitelistStatusInCustomer.view")
    public ResponseEntity<Whitelist> getWhitelistStatusByNip(String nip) {
        return new ResponseEntity<>(whitelistService.getWhitelistStatus(nip), HttpStatus.OK);
    }
}
