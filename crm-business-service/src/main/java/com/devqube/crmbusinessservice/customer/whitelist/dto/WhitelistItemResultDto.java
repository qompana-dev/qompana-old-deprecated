package com.devqube.crmbusinessservice.customer.whitelist.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WhitelistItemResultDto {
    private WhitelistItemSubjectDto subject;
    private List<WhitelistItemSubjectDto> subjects;
    private String requestId;
}
