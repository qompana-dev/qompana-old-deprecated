package com.devqube.crmbusinessservice.customer.gus;

import cis.bir.publ._2014._07.IUslugaBIRzewnPubl;
import cis.bir.publ._2014._07.datacontract.ObjectFactory;
import cis.bir.publ._2014._07.datacontract.ParametryWyszukiwania;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.exception.InvalidGUSResponseException;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.tempuri.UslugaBIRzewnPubl;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.AddressingFeature;
import java.util.Collections;

@Slf4j
@Service
public class GusClient {

    @Value("${gus.token}")
    private String TOKEN;

    private final CustomerRepository customerRepository;

    public GusClient(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer getCustomer(String nip, Long customerId) throws EntityNotFoundException, InvalidGUSResponseException, BadRequestException {
        if (countByNipWithCustomer(nip, customerId) != 0) {
            throw new BadRequestException("NIP is occupied");
        }
        IUslugaBIRzewnPubl port = new UslugaBIRzewnPubl().getE3(new AddressingFeature());
        setHeaderWithToken((BindingProvider) port, port.zaloguj(TOKEN));
        ParametryWyszukiwania parametryWyszukiwania = new ParametryWyszukiwania();
        parametryWyszukiwania.setNip(new ObjectFactory().createParametryWyszukiwaniaNip(nip));

        Pair<String, String> regonAndType = new GUSResponseParser(port.daneSzukaj(parametryWyszukiwania)).extractRegonAndType();
        String raport = port.danePobierzPelnyRaport(regonAndType.getLeft(), regonAndType.getRight());
        String type = regonAndType.getRight().equals("PublDaneRaportPrawna") ? "praw" : "fiz";
        return new GUSResponseParser(raport).extractCustomer(type, nip, regonAndType.getLeft());
    }

    private void setHeaderWithToken(BindingProvider port, String sid) {
        port.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, Collections.singletonMap("sid", Collections.singletonList(sid)));
    }

    public Long countByNipWithCustomer(String nip, Long customerId) {
        if (customerId!=null){
            return customerRepository.countByNipAndIdNot(nip, customerId);
        } else {
            return customerRepository.countByNip(nip);
        }
    }
}
