package com.devqube.crmbusinessservice.customer.customer.web.dto;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.type.Type;
import com.devqube.crmshared.access.annotation.AuthField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerForMobileListDto {

    @AuthField(controller = "getCustomersForMobile")
    private Long id;
    @AuthField(controller = "getCustomersForMobile")
    private String name;
    @AuthField(controller = "getCustomersForMobile")
    private Type type;

    @AuthField(controller = "getCustomersForMobile")
    private Long contactId;

    @AuthField(controller = "getCustomersForMobile")
    private String contactSurname;

    @AuthField(controller = "getCustomersForMobile")
    private String contactName;

    @AuthField(controller = "getCustomersForMobile")
    private String contactMail;

    @AuthField(controller = "getCustomersForMobile")
    private String contactPhone;

    public CustomerForMobileListDto(Customer customer) {
        this.id = customer.getId();
        this.name = customer.getName();
        this.type = customer.getType();
        if (customer.getFirstToContact() != null && customer.getFirstToContact().size() > 0) {
            this.contactSurname = customer.getFirstToContact().get(0).getSurname();
            this.contactName = customer.getFirstToContact().get(0).getName();
            this.contactMail = customer.getFirstToContact().get(0).getEmail();
            this.contactId = customer.getFirstToContact().get(0).getId();
            this.contactPhone = customer.getFirstToContact().get(0).getPhone();
        }
    }

}
