package com.devqube.crmbusinessservice.scheduler;

public enum NotificationJobType {
    LEAD,
    OPPORTUNITY
}
