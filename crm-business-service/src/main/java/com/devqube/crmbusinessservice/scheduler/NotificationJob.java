package com.devqube.crmbusinessservice.scheduler;

import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmshared.kafka.util.KafkaMessageDataBuilder;
import lombok.RequiredArgsConstructor;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import static com.devqube.crmshared.notification.NotificationMsgType.LEAD_TASK_EXPIRED;
import static com.devqube.crmshared.notification.NotificationMsgType.OPPORTUNITY_TASK_EXPIRED;

@Component
@RequiredArgsConstructor
public class NotificationJob extends QuartzJobBean {

    private final KafkaService kafkaService;
    private static final Logger logger = LoggerFactory.getLogger(NotificationJob.class);

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("Executing Job with key {}", jobExecutionContext.getJobDetail().getKey());

        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        NotificationJobType type = NotificationJobType.valueOf(jobDataMap.getString("type"));
        Long userId = jobDataMap.getLong("userId");
        String name = jobDataMap.getString("name");
        String taskName = jobDataMap.getString("taskName");

        sendNotification(type, userId, name, taskName);
    }

    private void sendNotification(NotificationJobType type, Long userId, String name, String taskName) {
        try {
            kafkaService.addNotification(userId, type.equals(NotificationJobType.LEAD) ? LEAD_TASK_EXPIRED : OPPORTUNITY_TASK_EXPIRED,
                    KafkaMessageDataBuilder.builder()
                            .add("name", name)
                            .add("taskName", taskName)
                            .build());
        } catch (KafkaSendMessageException e) {
            logger.error(e.getMessage(), e);
        }
    }
}
