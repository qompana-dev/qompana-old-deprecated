package com.devqube.crmbusinessservice.scheduler;

import lombok.RequiredArgsConstructor;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class NotificationSchedulerService {
    private static final Logger logger = LoggerFactory.getLogger(NotificationSchedulerService.class);

    private final Scheduler scheduler;

    public void scheduleNotificationJob(NotificationJobType type, String processInstanceId, Long userId, String name, String taskId, String taskName, Long daysToComplete) {
        try {
            LocalDateTime fireTime = LocalDateTime.now().plusSeconds(daysToComplete * 10);
            JobDetail jobDetail = buildJobDetail(type, processInstanceId, userId, name, taskId, taskName, daysToComplete);
            Trigger trigger = buildJobTrigger(jobDetail, fireTime);

            scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException ex) {
            logger.error("Error scheduling notification job", ex);
        }
    }

    public Scheduler cancelNotificationJob(String processInstanceId, String taskId) {
        JobKey key = new JobKey("Notification_" + processInstanceId + "_" + taskId);
        try {
            scheduler.deleteJob(key);
        } catch (SchedulerException e) {
        }
        return scheduler;
    }

    private JobDetail buildJobDetail(NotificationJobType type, String processInstanceId, Long userId, String name, String taskId, String taskName, Long daysToComplete) {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("type", type.name());
        jobDataMap.put("processInstanceId", processInstanceId);
        jobDataMap.put("userId", userId);
        jobDataMap.put("name", name);
        jobDataMap.put("taskId", taskId);
        jobDataMap.put("taskName", taskName);
        jobDataMap.put("daysToComplete", daysToComplete);

        return JobBuilder.newJob(NotificationJob.class)
                .withIdentity("Notification_" + processInstanceId + "_" + taskId, "notification-jobs")
                .usingJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    private Trigger buildJobTrigger(JobDetail jobDetail, LocalDateTime fireTime) {
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName(), "notification-triggers")
                .startAt(Date.from(fireTime.atZone(ZoneId.systemDefault()).toInstant()))
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
                .build();
    }
}
