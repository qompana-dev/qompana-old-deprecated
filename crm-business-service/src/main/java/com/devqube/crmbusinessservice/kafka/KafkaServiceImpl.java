package com.devqube.crmbusinessservice.kafka;

import com.devqube.crmshared.kafka.AbstractKafkaService;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaServiceImpl extends AbstractKafkaService {

    @Value("${ignore.kafka:false}")
    private boolean ignoreKafka;

    public void processMessage(String content) {
        super.processMessage(content);
    }

    @Override
    public void receiverNotFound(KafkaMessage kafkaMessage) {
        log.info("receiverNotFound" + kafkaMessage.getDestination().name());
    }

    @Override
    public boolean isKafkaIgnored() {
        return ignoreKafka;
    }
}
