package com.devqube.crmbusinessservice.access;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GlobalConfigurationDTO {
    private Long id;
    private String key;
    private String value;
}
