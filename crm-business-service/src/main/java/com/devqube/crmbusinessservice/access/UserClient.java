package com.devqube.crmbusinessservice.access;

import com.devqube.crmshared.access.model.ProfileDto;
import com.devqube.crmshared.currency.dto.CurrencyDto;
import com.devqube.crmshared.dashboard.dto.DashboardWidgetDto;
import com.devqube.crmshared.dashboard.dto.SingleDashboardWidgetDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.user.dto.AccountEmail;
import com.devqube.crmshared.user.dto.AccountInfoDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "crm-user-service", url = "${crm-user-service.url}")
public interface UserClient {
    @RequestMapping(value = "/profile/basic/mine", consumes = {"application/json"}, method = RequestMethod.GET)
    ProfileDto getBasicProfile(@RequestHeader("Logged-Account-Email") String email);

    @RequestMapping(value = "/accounts/me/id", consumes = {"application/json"}, method = RequestMethod.GET)
    Long getMyAccountId(@RequestHeader("Logged-Account-Email") String email);

    @RequestMapping(value = "/accounts/id", consumes = {"application/json"}, method = RequestMethod.GET)
    Long getAccountIdByEmail(@RequestParam("email") String email);

    @RequestMapping(value = "/role/email/{email}/supervisor/id", consumes = {"application/json"}, method = RequestMethod.GET)
    Long getSupervisorId(@PathVariable String email);

    @RequestMapping(value = "/role/email/{email}/with-children/id", method = RequestMethod.GET)
    List<Long> getRoleIdWithChildrenIdsByAssignedAccountEmail(@PathVariable String email);

    @RequestMapping(value = "/accounts/crm-object", method = RequestMethod.GET)
    List<CrmObject> getAccountsAsCrmObjects(@RequestParam("ids") List<Long> ids);

    @RequestMapping(value = "/configuration/expenseAcceptanceThreshold", method = RequestMethod.GET)
    GlobalConfigurationDTO getExpenseAcceptanceThreshold();

    @RequestMapping(value = "/internal/configuration/currencies", method = RequestMethod.GET)
    CurrencyDto getCurrencies();

    @RequestMapping(value = "/internal/accounts/emails/by-email/{email}", method = RequestMethod.GET)
    AccountEmail getAccountInfoByEmail(@PathVariable("email") String email);

    @RequestMapping(value = "/internal/accounts/{email}/role", method = RequestMethod.GET)
    String getRoleForUserWithEmail(@PathVariable("email") String email);

    @RequestMapping(value = "/role/name/{roleName}/child/accounts/id", method = RequestMethod.GET)
    List<Long> getAllEmployeesForRole(@PathVariable("roleName") String roleName);

    @PostMapping("/widget/dashboard")
    DashboardWidgetDto saveWidget(@RequestBody SingleDashboardWidgetDto singleDashboardWidgetDto,
                                  @RequestHeader("Logged-Account-Email") String email) throws BadRequestException, EntityNotFoundException;

    @RequestMapping(value = "/accounts/emails", produces = {"application/json"}, method = RequestMethod.GET)
    List<AccountEmail> getAccountEmails();

    @RequestMapping(value = "/accounts/info/{ids}", produces = {"application/json"}, method = RequestMethod.GET)
    List<AccountInfoDto> getAccountsInfoByIds(@PathVariable List<Long> ids);
}
