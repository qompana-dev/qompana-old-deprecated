package com.devqube.crmbusinessservice.access;

import com.devqube.crmshared.currency.dto.CurrencyDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.user.dto.AccountEmail;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserClient userClient;

    public UserService(UserClient userClient) {
        this.userClient = userClient;
    }

    public Long getUserId(String accountEmail) throws BadRequestException {
        try {
            return userClient.getMyAccountId(accountEmail);
        } catch (Exception e) {
            throw new BadRequestException("user not found");
        }
    }

    public String getUserNameAndSurname(Long id) throws BadRequestException {
        try {
            List<CrmObject> accounts = userClient.getAccountsAsCrmObjects(List.of(id));
            return accounts != null && accounts.size() == 1 ? accounts.get(0).getLabel() : null;
        } catch (Exception e) {
            throw new BadRequestException("user not found");
        }
    }

    public CurrencyDto getCurrencies() throws BadRequestException {
        try {
            return userClient.getCurrencies();
        } catch (Exception e) {
            throw new BadRequestException("unable to obtain currency global param");
        }
    }

    public List<AccountEmail> getAccountEmails() throws BadRequestException {
        try {
            return userClient.getAccountEmails();
        } catch (Exception e) {
            throw new BadRequestException("unable to get account emails");
        }
    }
}
