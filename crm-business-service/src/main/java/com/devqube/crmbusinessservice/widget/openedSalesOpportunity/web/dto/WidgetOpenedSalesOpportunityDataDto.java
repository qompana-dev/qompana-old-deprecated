package com.devqube.crmbusinessservice.widget.openedSalesOpportunity.web.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WidgetOpenedSalesOpportunityDataDto {
    private String currency;
    private Long totalTasks;
    private List<ProcessStageInfo> data;
}
