package com.devqube.crmbusinessservice.widget.openedSalesOpportunity;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityService;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.OpportunityDetailsDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineHelperService;
import com.devqube.crmbusinessservice.widget.openedSalesOpportunity.web.dto.ProcessStageInfo;
import com.devqube.crmbusinessservice.widget.openedSalesOpportunity.web.dto.WidgetOpenedSalesOpportunityDataDto;
import com.devqube.crmshared.currency.dto.CurrencyDto;
import com.devqube.crmshared.currency.dto.RateOfMainCurrencyDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.web.CurrentRequestUtil;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class WidgetOpenedSalesOpportunityService {
    private final UserClient userClient;
    private final OpportunityService opportunityService;
    private final ProcessEngineHelperService processEngineHelperService;
    private final ModelMapper modelMapper;

    public WidgetOpenedSalesOpportunityService(UserClient userClient, OpportunityService opportunityService,
                                               ProcessEngineHelperService processEngineHelperService,
                                               ModelMapper modelMapper) {
        this.userClient = userClient;
        this.opportunityService = opportunityService;
        this.processEngineHelperService = processEngineHelperService;
        this.modelMapper = modelMapper;
    }

    public WidgetOpenedSalesOpportunityDataDto getWidgetDataForProcess(String processDefinitionId) throws BadRequestException {
        List<String> similarProcesses =
                processEngineHelperService.findAllVersionDefinitionsOfProcessByProcessDefinitionId(processDefinitionId);
        CurrencyDto currencies = userClient.getCurrencies();
        String loggedInAccountEmail = CurrentRequestUtil.getLoggedInAccountEmail();
        List<Opportunity> allOpenedOpportunities = opportunityService.findAllOpenedAndAvailableForUser(loggedInAccountEmail);
        List<OpportunityDetailsDTO> opportunityDetailsDtoList = new ArrayList<>();
        for (Opportunity opportunity : allOpenedOpportunities) {
            ProcessInstance processInstance = processEngineHelperService.getProcessInstance(opportunity.getProcessInstanceId());
            ProcessDefinition processDefinition = processEngineHelperService.getProcessDefinition(processInstance.getProcessDefinitionId());

            if (similarProcesses.contains(processDefinition.getId())) {
                OpportunityDetailsDTO dto = modelMapper.map(opportunity, OpportunityDetailsDTO.class);
                processEngineHelperService.setTasksToDTO(dto.getProcessInstanceId(), dto, processInstance);
                opportunityDetailsDtoList.add(dto);
            }
        }
        List<ProcessStageInfo> data = new ArrayList<>();
        for (OpportunityDetailsDTO opportunityDetailsDTO : opportunityDetailsDtoList) {
            for (int i = 0; i < opportunityDetailsDTO.getTasks().size(); i++) {
                final int iCopy = i;
                Optional<ProcessStageInfo> first = data.stream().filter(processStageInfo -> processStageInfo.getStage().equals((long) iCopy)).findFirst();
                if (first.isPresent()) {
                    ProcessStageInfo processStageInfo = first.get();
                    if (opportunityDetailsDTO.getTasks().get(i).isActive()) {
                        processStageInfo.setTotalQuantity(processStageInfo.getTotalQuantity() + 1);
                        if (opportunityDetailsDTO.getAmount() != null) {
                            processStageInfo.setTotalAmount(processStageInfo.getTotalAmount() +
                                    getAmountInSystemCurrency(opportunityDetailsDTO.getAmount(), opportunityDetailsDTO.getCurrency(), currencies));
                        }
                    }
                } else {
                    ProcessStageInfo processStageInfo = new ProcessStageInfo();
                    processStageInfo.setStage((long) i);
                    if (opportunityDetailsDTO.getTasks().get(i).isActive()) {
                        processStageInfo.setTotalQuantity(1L);
                        processStageInfo.setTotalAmount(opportunityDetailsDTO.getAmount() != null ? opportunityDetailsDTO.getAmount() : 0d);
                    } else {
                        processStageInfo.setTotalQuantity(0L);
                        processStageInfo.setTotalAmount(0D);
                    }
                    data.add(processStageInfo);
                }
            }
        }
        List<ProcessStageInfo> resultData = data.stream().filter(item -> item.getTotalQuantity() != 0)
                .sorted(Comparator.comparingLong(ProcessStageInfo::getTotalQuantity)).collect(Collectors.toList());
        return WidgetOpenedSalesOpportunityDataDto.builder()
                .currency(currencies.getSystemCurrency())
                .data(resultData)
                .totalTasks((long) data.size())
                .build();
    }

    public WidgetOpenedSalesOpportunityDataDto getWidgetSeparateProcessVersionData() throws BadRequestException {
        String loggedInAccountEmail = CurrentRequestUtil.getLoggedInAccountEmail();
        opportunityService.findAllOpenedAndAvailableForUser(loggedInAccountEmail);

        return null;
    }

    private Double getAmountInSystemCurrency(@NonNull Double amount, String currency, CurrencyDto currencyDto) {
        String systemCurrency = currencyDto.getSystemCurrency();
        List<RateOfMainCurrencyDto> ratesOfMainCurrency = currencyDto.getRatesOfMainCurrency();
        Map<String, Double> map = ratesOfMainCurrency.stream()
                .collect(Collectors.toMap(RateOfMainCurrencyDto::getCurrency, RateOfMainCurrencyDto::getRate));
        map.put(systemCurrency, 1D);
        return amount * map.get(currency);
    }
}