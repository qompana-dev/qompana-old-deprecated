package com.devqube.crmbusinessservice.widget.goal;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.goal.GoalRepository;
import com.devqube.crmbusinessservice.goal.GoalService;
import com.devqube.crmbusinessservice.goal.model.Goal;
import com.devqube.crmbusinessservice.goal.web.dto.GoalEditDto;
import com.devqube.crmbusinessservice.goal.web.dto.GoalIntervalDto;
import com.devqube.crmbusinessservice.goal.web.dto.UserGoalDto;
import com.devqube.crmbusinessservice.widget.goal.model.WidgetGoal;
import com.devqube.crmbusinessservice.widget.goal.model.WidgetGoalParticipant;
import com.devqube.crmbusinessservice.widget.goal.web.dto.UserWidgetDataDto;
import com.devqube.crmbusinessservice.widget.goal.web.dto.WidgetGoalDataDto;
import com.devqube.crmbusinessservice.widget.goal.web.dto.WidgetGoalDto;
import com.devqube.crmshared.dashboard.dto.DashboardWidgetDto;
import com.devqube.crmshared.dashboard.dto.SingleDashboardWidgetDto;
import com.devqube.crmshared.dashboard.model.WidgetTypeEnum;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.web.CurrentRequestUtil;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class WidgetGoalService {

    private final WidgetGoalRepository widgetGoalRepository;
    private final GoalRepository goalRepository;
    private final GoalService goalService;
    private final UserClient userClient;
    private final ModelMapper modelMapper;

    public WidgetGoalService(WidgetGoalRepository widgetGoalRepository, GoalRepository goalRepository, GoalService goalService,
                             UserClient userClient, ModelMapper modelMapper) {
        this.widgetGoalRepository = widgetGoalRepository;
        this.goalRepository = goalRepository;
        this.goalService = goalService;
        this.userClient = userClient;
        this.modelMapper = modelMapper;
    }

    @Transactional(rollbackFor = Exception.class)
    public DashboardWidgetDto save(WidgetGoalDto widgetGoalDto) throws EntityNotFoundException, BadRequestException {
        Goal goal = goalRepository.findById(widgetGoalDto.getGoalId()).orElseThrow(EntityNotFoundException::new);
        WidgetGoal widgetGoal = mapDtoToModel(widgetGoalDto, goal);
        WidgetGoal saved = widgetGoalRepository.save(widgetGoal);
        SingleDashboardWidgetDto singleDashboardWidgetDto;
        try {
            singleDashboardWidgetDto =
                    SingleDashboardWidgetDto.builder()
                            .widgetTypeEnum(WidgetTypeEnum.GOAL)
                            .widgetId(saved.getId())
                            .cols(widgetGoalDto.getCols())
                            .rows(widgetGoalDto.getRows())
                            .x(widgetGoalDto.getX())
                            .y(widgetGoalDto.getY())
                            .build();

            userClient.saveWidget(singleDashboardWidgetDto, CurrentRequestUtil.getLoggedInAccountEmail());
        } catch (Exception e) {
            throw new BadRequestException();
        }
        return mapToDto(singleDashboardWidgetDto);
    }

    @Transactional
    public void deleteByIds(Set<Long> widgetIds) {
        widgetGoalRepository.deleteAllByIdIn(widgetIds);
    }

    public WidgetGoalDataDto getById(Long id) throws EntityNotFoundException, BadRequestException {
        WidgetGoal widgetGoal = widgetGoalRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        GoalEditDto goalEditDto = goalService.getGoalForEdit(widgetGoal.getGoal().getId());
        WidgetGoalDataDto widgetGoalDataDto = modelMapper.map(goalEditDto, WidgetGoalDataDto.class);
        widgetGoalDataDto.setChartType(widgetGoal.getChartType());
        widgetGoalDataDto.setUserWidgetDataDtoList(getUserWidgetDataDtoSet(widgetGoal, goalEditDto));
        return widgetGoalDataDto;
    }

    private List<UserWidgetDataDto> getUserWidgetDataDtoSet(WidgetGoal widgetGoal, GoalEditDto goalEditDto) {
        Map<Long, String> accountIdNameMap = getAccountIdToNameMap(widgetGoal);
        Map<Long, List<GoalIntervalDto>> accountIdToGoalIntervalsMap = getAccountIdToGoalIntervalMap(goalEditDto);
        List<UserWidgetDataDto> userWidgetDataDtoList = new ArrayList<>();
        widgetGoal.getParticipants().forEach(p -> userWidgetDataDtoList.add(
                createUserWidgetDataDto(accountIdNameMap, accountIdToGoalIntervalsMap, p))
        );
        return userWidgetDataDtoList
                .stream()
                .sorted(Comparator.comparing(UserWidgetDataDto::getLabel).thenComparing(UserWidgetDataDto::isGoal))
                .collect(Collectors.toList());
    }

    private Map<Long, String> getAccountIdToNameMap(WidgetGoal widgetGoal) {
        return userClient.getAccountsAsCrmObjects(
                new ArrayList<>(widgetGoal.getParticipants()
                        .stream()
                        .map(WidgetGoalParticipant::getUserId)
                        .collect(Collectors.toSet()))
        ).stream().collect(Collectors.toMap(CrmObject::getId, CrmObject::getLabel));
    }

    private Map<Long, List<GoalIntervalDto>> getAccountIdToGoalIntervalMap(GoalEditDto goalEditDto) {
        return goalEditDto.getUsersGoals().stream()
                .collect(Collectors.toMap(UserGoalDto::getUserId, e -> new ArrayList<>(e.getIntervals())));
    }

    private UserWidgetDataDto createUserWidgetDataDto(Map<Long, String> accountIdNameMap, Map<Long, List<GoalIntervalDto>> accountIdToGoalIntervalsMap, WidgetGoalParticipant p) {
        List<GoalIntervalDto> goalIntervalDtoList = accountIdToGoalIntervalsMap.get(p.getUserId())
                .stream()
                .sorted(Comparator.comparing(GoalIntervalDto::getIndex))
                .collect(Collectors.toList());
        return new UserWidgetDataDto(
                goalIntervalDtoList.stream().map(
                        e -> p.isGoal() ? e.getExpectedValue() : e.getCurrentValue()
                ).collect(Collectors.toList()), accountIdNameMap.get(p.getUserId()), p.isGoal());
    }

    private DashboardWidgetDto mapToDto(SingleDashboardWidgetDto singleDashboardWidgetDto) {
        return modelMapper.map(singleDashboardWidgetDto, DashboardWidgetDto.class);
    }

    private WidgetGoal mapDtoToModel(WidgetGoalDto widgetGoalDto, Goal goal) {
        WidgetGoal widgetGoal = new WidgetGoal();
        widgetGoal.setGoal(goal);
        widgetGoal.setChartType(widgetGoalDto.getChartType());
        widgetGoal.setParticipants(mapParticipantsDtoToParticipants(widgetGoalDto, widgetGoal));
        return widgetGoal;
    }

    private List<WidgetGoalParticipant> mapParticipantsDtoToParticipants(WidgetGoalDto widgetGoalDto, WidgetGoal widgetGoal) {
        return widgetGoalDto.getParticipants().stream()
                .map(e -> WidgetGoalParticipant.builder()
                        .widgetGoal(widgetGoal)
                        .userId(e.getUserId())
                        .goal(e.isGoal())
                        .build())
                .collect(Collectors.toList());
    }
}
