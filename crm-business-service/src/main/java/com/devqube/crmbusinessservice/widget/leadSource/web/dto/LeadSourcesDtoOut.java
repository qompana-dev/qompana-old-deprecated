package com.devqube.crmbusinessservice.widget.leadSource.web.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class LeadSourcesDtoOut {

    private List<LeadSourceDtoOut> sources = new ArrayList<>();
    private Long total;

}
