package com.devqube.crmbusinessservice.widget.report.collector.types;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.widget.report.collector.ReportType;
import com.devqube.crmbusinessservice.widget.report.collector.WidgetReportType;
import com.devqube.crmbusinessservice.widget.report.collector.WidgetReportTypeUtil;
import com.devqube.crmbusinessservice.widget.report.collector.types.abstracttypes.AbstractOpportunitiesAmountType;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReport;
import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDataDto;
import com.devqube.crmshared.widgetreport.exception.CollectDataException;
import com.devqube.crmshared.widgetreport.model.WidgetReportTypeEnum;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Map;

@Service
@ReportType(type = WidgetReportTypeEnum.WON_OPPORTUNITIES_AMOUNT)
public class WonOpportunitiesAmountType extends AbstractOpportunitiesAmountType implements WidgetReportType {

    public WonOpportunitiesAmountType(EntityManager em, WidgetReportTypeUtil widgetReportTypeUtil) {
        super(em, widgetReportTypeUtil);
    }

    @Override
    public WidgetReportDataDto getData(WidgetReport widgetReport) throws CollectDataException {
        if (!WidgetReportTypeEnum.WON_OPPORTUNITIES_AMOUNT.getColumns().contains(widgetReport.getWidgetColumn())) {
            throw new CollectDataException();
        }

        Map<Long, Map<String, Double>> map = getResultMap(widgetReport);
        return widgetReportTypeUtil.getWidgetReportDataDto(map, widgetReport);
    }

    public Predicate getBasicWhere(WidgetReport widgetReport, CriteriaBuilder cb, Root<Opportunity> from, List<Long> userIds) {
        return cb.and(cb.equal(from.get("finishResult"), Opportunity.FinishResult.SUCCESS), getDefaultWhere(widgetReport, cb, from, userIds));
    }
}
