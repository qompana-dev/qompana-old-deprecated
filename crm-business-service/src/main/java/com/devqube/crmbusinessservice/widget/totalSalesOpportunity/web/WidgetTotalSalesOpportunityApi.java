package com.devqube.crmbusinessservice.widget.totalSalesOpportunity.web;

import com.devqube.crmbusinessservice.widget.totalSalesOpportunity.web.dto.WidgetTotalSalesOpportunityDataDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.validation.annotation.Validated;

@Validated
public interface WidgetTotalSalesOpportunityApi {

    @ApiOperation(value = "Get data for totalSalesOpportunity widget")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "TotalSalesOpportunity Widget data retrieved successfully",
                    response = WidgetTotalSalesOpportunityDataDto.class),
            @ApiResponse(code = 404, message = "TotalSalesOpportunity Widget data not found")
    })
    WidgetTotalSalesOpportunityDataDto getWidgetTotalSalesOpportunityData() throws EntityNotFoundException, BadRequestException;
}

