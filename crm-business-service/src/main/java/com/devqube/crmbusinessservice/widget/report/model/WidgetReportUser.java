package com.devqube.crmbusinessservice.widget.report.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WidgetReportUser {
    @Id
    @SequenceGenerator(name = "widget_report_user_seq", sequenceName = "widget_report_user_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "widget_report_user_seq")
    private Long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "widget_report_id")
    @ToString.Exclude
    private WidgetReport widgetReport;

    @NotNull
    private Long accountId;
}
