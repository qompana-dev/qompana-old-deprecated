package com.devqube.crmbusinessservice.widget.report.collector.types.integrations;

import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.widgetreport.dto.ReportDto;
import com.devqube.crmshared.widgetreport.dto.UserObjMap;
import com.devqube.crmshared.widgetreport.exception.CollectDataException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(contextId = "WidgetTaskClient", name = "crm-task-service", url = "${crm-task-service.url}")
public interface WidgetTaskClient {
    @RequestMapping(value = "/internal/widget/report", consumes = { "application/json" }, method = RequestMethod.POST)
    List<UserObjMap> getReportWidgetData(@RequestBody ReportDto reportDto) throws EntityNotFoundException, CollectDataException;
}
