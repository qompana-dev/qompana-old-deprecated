package com.devqube.crmbusinessservice.widget.report.collector.types.abstracttypes;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.widget.report.collector.ReportType;
import com.devqube.crmbusinessservice.widget.report.collector.WidgetReportType;
import com.devqube.crmbusinessservice.widget.report.collector.WidgetReportTypeUtil;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReport;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReportUser;
import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDataDto;
import com.devqube.crmshared.widgetreport.exception.CollectDataException;
import com.devqube.crmshared.widgetreport.model.WidgetReportTypeEnum;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@Service
@ReportType(type = WidgetReportTypeEnum.LOSS_OPPORTUNITIES_NUMBER)
public abstract class AbstractOpportunitiesNumberType extends AbstractOpportunityType implements WidgetReportType {

    public AbstractOpportunitiesNumberType(EntityManager em, WidgetReportTypeUtil widgetReportTypeUtil) {
        super(em, widgetReportTypeUtil);
    }

    public abstract  WidgetReportDataDto getData(WidgetReport widgetReport) throws CollectDataException;
    public abstract Predicate getBasicWhere(WidgetReport widgetReport, CriteriaBuilder cb, Root<Opportunity> from, List<Long> userIds);

    public Map<Long, Map<String, Double>> getResultMap(WidgetReport widgetReport) {
        List<Object> distinctX = getDistinctX(widgetReport);
        Map<Long, Map<String, Double>> result = new HashMap<>(); // <userId, <x, result>>
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Opportunity> from = cq.from(Opportunity.class);
        CriteriaQuery<Long> select = cq.select(cb.count(from));

        for (WidgetReportUser widgetReportUser : widgetReport.getUserList()) {
            result.put(widgetReportUser.getAccountId(), new HashMap<String, Double>());

            for (Object groupBy : distinctX) {
                Predicate where = cb.and(getOwnerPredicate(cb, widgetReport, from, groupBy),
                        getBasicWhere(widgetReport, cb, from, Collections.singletonList(widgetReportUser.getAccountId())));

                Long countAll = em.createQuery(select.where(where)).getSingleResult();
                String x = Optional.ofNullable(groupBy).orElse("").toString();
                result.get(widgetReportUser.getAccountId()).put(x, countAll.doubleValue());
            }
        }
        return result;
    }

}
