package com.devqube.crmbusinessservice.widget.report.model;

import com.devqube.crmshared.widgetreport.model.WidgetColumnEnum;
import com.devqube.crmshared.widgetreport.model.WidgetReportTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WidgetReport {
    @Id
    @SequenceGenerator(name = "widget_report_seq", sequenceName = "widget_report_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "widget_report_seq")
    private Long id;
    @NotNull
    @Enumerated(EnumType.STRING)
    private WidgetReportTypeEnum reportType;
    @NotNull
    @Enumerated(EnumType.STRING)
    private WidgetColumnEnum widgetColumn;
    @NotNull
    private Boolean withTime;
    @NotNull
    private LocalDateTime start;
    @Column(name = "\"end\"")
    private LocalDateTime end;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "widgetReport")
    private List<WidgetReportUser> userList;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ChartTypeEnum chartType;

    private String additionalValue;
}
