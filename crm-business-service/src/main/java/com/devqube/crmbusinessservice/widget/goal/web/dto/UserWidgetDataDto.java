package com.devqube.crmbusinessservice.widget.goal.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserWidgetDataDto {
    private List<Double> data;
    private String label;
    private boolean goal;
}
