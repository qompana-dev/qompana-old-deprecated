package com.devqube.crmbusinessservice.widget.goal.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WidgetGoalParticipant {
    @Id
    @SequenceGenerator(name = "widget_goal_participant_seq", sequenceName = "widget_goal_participant_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "widget_goal_participant_seq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "widget_goal_id")
    @NotNull
    private WidgetGoal widgetGoal;

    @NotNull
    private Long userId;

    @NotNull
    private boolean goal;
}
