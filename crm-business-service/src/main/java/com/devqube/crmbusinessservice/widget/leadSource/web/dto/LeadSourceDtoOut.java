package com.devqube.crmbusinessservice.widget.leadSource.web.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LeadSourceDtoOut {

    private String name;
    private Long count;
    private Double percentageChange;

}