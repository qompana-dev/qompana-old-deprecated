package com.devqube.crmbusinessservice.widget.report.collector;

import com.devqube.crmbusinessservice.widget.report.WidgetReportRepository;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReport;
import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDataDto;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.widgetreport.exception.CollectDataException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class WidgetReportDataCollectorService {
    private final List<WidgetReportType> types;
    private final WidgetReportRepository widgetReportRepository;

    public WidgetReportDataCollectorService(List<WidgetReportType> types, WidgetReportRepository widgetReportRepository) {
        this.types = types;
        this.widgetReportRepository = widgetReportRepository;
    }

    @Transactional
    public WidgetReportDataDto getWidgetReportData(Long widgetId) throws EntityNotFoundException, CollectDataException {
        WidgetReport widgetReport = widgetReportRepository.findById(widgetId).orElseThrow(EntityNotFoundException::new);
        WidgetReportType widgetReportType = types.stream()
                .filter(c -> c.getClass().getAnnotation(ReportType.class) != null && c.getClass().getAnnotation(ReportType.class).type().equals(widgetReport.getReportType()))
                .findFirst().orElseThrow(EntityNotFoundException::new);
        return widgetReportType.getData(widgetReport);
    }
}
