package com.devqube.crmbusinessservice.widget.report.collector.types;

import com.devqube.crmbusinessservice.widget.report.collector.ReportType;
import com.devqube.crmbusinessservice.widget.report.collector.WidgetReportType;
import com.devqube.crmbusinessservice.widget.report.collector.WidgetReportTypeUtil;
import com.devqube.crmbusinessservice.widget.report.collector.types.integrations.WidgetTaskClient;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReport;
import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDataDto;
import com.devqube.crmshared.widgetreport.WidgetReportConverter;
import com.devqube.crmshared.widgetreport.dto.UserObjMap;
import com.devqube.crmshared.widgetreport.exception.CollectDataException;
import com.devqube.crmshared.widgetreport.model.WidgetReportTypeEnum;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@ReportType(type = WidgetReportTypeEnum.MEETING_NUMBER_IN_CALENDAR)
public class MeetingNumberInCalendarType implements WidgetReportType {
    private final WidgetTaskClient widgetTaskClient;
    private final WidgetReportTypeUtil widgetReportTypeUtil;

    public MeetingNumberInCalendarType(WidgetTaskClient widgetTaskClient, WidgetReportTypeUtil widgetReportTypeUtil) {
        this.widgetTaskClient = widgetTaskClient;
        this.widgetReportTypeUtil = widgetReportTypeUtil;
    }

    @Override
    public WidgetReportDataDto getData(WidgetReport widgetReport) throws CollectDataException {
        try {
            List<UserObjMap> reportWidgetData = widgetTaskClient.getReportWidgetData(widgetReportTypeUtil.getReportDto(widgetReport));
            Map<Long, Map<String, Double>> map = WidgetReportConverter.getMap(reportWidgetData);
            return widgetReportTypeUtil.getWidgetReportDataDto(map, widgetReport);
        } catch (Exception e) {
            e.printStackTrace();
            throw new CollectDataException("widget task client exception");
        }
    }
}
