package com.devqube.crmbusinessservice.widget.openedSalesOpportunity.web;

import com.devqube.crmbusinessservice.widget.openedSalesOpportunity.WidgetOpenedSalesOpportunityService;
import com.devqube.crmbusinessservice.widget.openedSalesOpportunity.web.dto.WidgetOpenedSalesOpportunityDataDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/widget/opened-sales-opportunity")
@Slf4j
public class WidgetOpenedSalesOpportunityController implements WidgetOpenedSalesOpportunityApi {
    private final WidgetOpenedSalesOpportunityService widgetOpenedSalesOpportunityService;

    public WidgetOpenedSalesOpportunityController(WidgetOpenedSalesOpportunityService widgetOpenedSalesOpportunityService) {
        this.widgetOpenedSalesOpportunityService = widgetOpenedSalesOpportunityService;
    }

    @Override
    @GetMapping("/data/processDefinitionId/{id}")
    public WidgetOpenedSalesOpportunityDataDto getWidgetOpenedSalesOpportunityData(@PathVariable("id") String processDefinitionId) throws EntityNotFoundException, BadRequestException {
        return widgetOpenedSalesOpportunityService.getWidgetDataForProcess(processDefinitionId);
    }

    @Override
    @GetMapping("/data/separate-process-version")
    public WidgetOpenedSalesOpportunityDataDto getWidgetOpenedSalesOpportunitySeparateProcessData() throws EntityNotFoundException, BadRequestException {
        return widgetOpenedSalesOpportunityService.getWidgetSeparateProcessVersionData();
    }
}