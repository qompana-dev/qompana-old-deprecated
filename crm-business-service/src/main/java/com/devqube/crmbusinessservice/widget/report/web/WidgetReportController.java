package com.devqube.crmbusinessservice.widget.report.web;


import com.devqube.crmbusinessservice.widget.report.WidgetReportService;
import com.devqube.crmbusinessservice.widget.report.collector.WidgetReportDataCollectorService;
import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDataDto;
import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDto;
import com.devqube.crmshared.dashboard.dto.DashboardWidgetDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.widgetreport.exception.CollectDataException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping("/widget/report")
@Slf4j
public class WidgetReportController implements WidgetReportApi {
    private final WidgetReportService widgetReportService;
    private final WidgetReportDataCollectorService widgetReportDataCollectorService;

    public WidgetReportController(WidgetReportService widgetReportService, WidgetReportDataCollectorService widgetReportDataCollectorService) {
        this.widgetReportService = widgetReportService;
        this.widgetReportDataCollectorService = widgetReportDataCollectorService;
    }

    @Override
    @PostMapping("/save")
    public ResponseEntity<DashboardWidgetDto> saveReportWidgetConfiguration(@RequestBody @Valid WidgetReportDto widgetReportDto) throws BadRequestException {
        return ResponseEntity.ok(widgetReportService.saveWidgetConfiguration(widgetReportDto));
    }

    @Override
    @GetMapping("/{widgetId}")
    public ResponseEntity<WidgetReportDto> getReportWidgetConfiguration(@PathVariable Long widgetId) throws EntityNotFoundException {
        return ResponseEntity.ok(widgetReportService.getWidgetReport(widgetId));
    }

    @Override
    @GetMapping("/data/{widgetId}")
    public ResponseEntity<WidgetReportDataDto> getReportWidgetData(@PathVariable Long widgetId) throws EntityNotFoundException, CollectDataException {
        return ResponseEntity.ok(widgetReportDataCollectorService.getWidgetReportData(widgetId));
    }

    @Override
    @DeleteMapping("/internal/{widgetIds}")
    public ResponseEntity<Void> deleteReportById(@PathVariable Set<Long> widgetIds) {
        widgetReportService.deleteWidgets(widgetIds);
        return ResponseEntity.noContent().build();
    }
}
