package com.devqube.crmbusinessservice.widget.leadSource;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

@Data
@Accessors(chain = true)
public class LeadSource {

    private Map<Long, Long> sourceMap = new HashMap<>();
    private Long noSource = 0L;
    private Long total = 0L;

}
