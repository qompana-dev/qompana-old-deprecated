package com.devqube.crmbusinessservice.widget.totalSalesOpportunity;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.expense.web.dto.TotalCostDTO;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.widget.totalSalesOpportunity.web.dto.WidgetTotalSalesOpportunityDataDto;
import com.devqube.crmshared.currency.dto.CurrencyDto;
import com.devqube.crmshared.currency.dto.RateOfMainCurrencyDto;
import com.devqube.crmshared.user.dto.AccountEmail;
import com.devqube.crmshared.web.CurrentRequestUtil;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.IsoFields;
import java.time.temporal.TemporalAdjusters;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class WidgetTotalSalesOpportunityService {
    private final UserClient userClient;
    private final OpportunityRepository opportunityRepository;

    private static final LocalTime START_TIME_OF_DAY = LocalTime.of(0, 0, 0);
    private static final LocalTime END_TIME_OF_DAY = LocalTime.of(23, 59, 59);

    public WidgetTotalSalesOpportunityService(UserClient userClient, OpportunityRepository opportunityRepository) {
        this.userClient = userClient;
        this.opportunityRepository = opportunityRepository;
    }

    public WidgetTotalSalesOpportunityDataDto getWidgetData() {
        CurrencyDto currencies = userClient.getCurrencies();
        String loggedInAccountEmail = CurrentRequestUtil.getLoggedInAccountEmail();
        AccountEmail accountEmail = userClient.getAccountInfoByEmail(loggedInAccountEmail);

        LocalDate localDate = LocalDate.now();
        LocalDate firstDayOfQuarter = localDate.with(IsoFields.DAY_OF_QUARTER, 1L);
        LocalDate lastDayOfQuarter = firstDayOfQuarter.plusMonths(2)
                .with(TemporalAdjusters.lastDayOfMonth());

        LocalDateTime dateTimeOfFirstDayOfQuarter = LocalDateTime.of(firstDayOfQuarter, START_TIME_OF_DAY);
        LocalDateTime dateTimeOfLastDayOfQuarter = LocalDateTime.of(lastDayOfQuarter, END_TIME_OF_DAY);
        List<Opportunity> createdOfCurrentPeriod = opportunityRepository
                .findAllByCreatorAndCreatedBetween((long) accountEmail.getId(),
                        dateTimeOfFirstDayOfQuarter,
                        dateTimeOfLastDayOfQuarter);

        TotalCostDTO createdOfCurrentPeriodTotalCost = calculateCost(createdOfCurrentPeriod, currencies.getSystemCurrency(), currencies.getRatesOfMainCurrency());

        List<Opportunity> successfulOfCurrentPeriod = opportunityRepository
                .findAllByCreatorAndClosingDateBetweenAndFinishResultSuccess((long) accountEmail.getId(),
                        dateTimeOfFirstDayOfQuarter,
                        dateTimeOfLastDayOfQuarter);

        TotalCostDTO successfulOfCurrentPeriodTotalCost = calculateCost(successfulOfCurrentPeriod, currencies.getSystemCurrency(), currencies.getRatesOfMainCurrency());

        List<Opportunity> lostOfCurrentPeriod = opportunityRepository
                .findAllByCreatorAndAndClosingDateBetweenAndFinishResultError((long) accountEmail.getId(),
                        dateTimeOfFirstDayOfQuarter,
                        dateTimeOfLastDayOfQuarter);

        TotalCostDTO lostOfCurrentPeriodTotalCost = calculateCost(lostOfCurrentPeriod, currencies.getSystemCurrency(), currencies.getRatesOfMainCurrency());

        LocalDate firstDayOfPreviousQuarter = firstDayOfQuarter.minusMonths(2).with(IsoFields.DAY_OF_QUARTER, 1L);
        LocalDate lastDayOfPreviousQuarter = firstDayOfPreviousQuarter.plusMonths(2)
                .with(TemporalAdjusters.lastDayOfMonth());
        LocalDateTime dateTimeOfFirstDayOfPreviousQuarter = LocalDateTime.of(firstDayOfPreviousQuarter, START_TIME_OF_DAY);
        LocalDateTime dateTimeOfLastDayOfPreviousQuarter = LocalDateTime.of(lastDayOfPreviousQuarter, END_TIME_OF_DAY);

        List<Opportunity> createdOfPreviousPeriod = opportunityRepository
                .findAllByCreatorAndCreatedBetween((long) accountEmail.getId(),
                        dateTimeOfFirstDayOfPreviousQuarter,
                        dateTimeOfLastDayOfPreviousQuarter);

        TotalCostDTO createdOfPreviousPeriodTotalCost = calculateCost(createdOfPreviousPeriod, currencies.getSystemCurrency(), currencies.getRatesOfMainCurrency());

        List<Opportunity> successfulOfPreviousPeriod = opportunityRepository
                .findAllByCreatorAndClosingDateBetweenAndFinishResultSuccess((long) accountEmail.getId(),
                        dateTimeOfFirstDayOfPreviousQuarter,
                        dateTimeOfLastDayOfPreviousQuarter);

        TotalCostDTO successfulOfPreviousPeriodTotalCost = calculateCost(successfulOfPreviousPeriod, currencies.getSystemCurrency(), currencies.getRatesOfMainCurrency());

        List<Opportunity> lostOfPreviousPeriod = opportunityRepository
                .findAllByCreatorAndAndClosingDateBetweenAndFinishResultError((long) accountEmail.getId(),
                        dateTimeOfFirstDayOfPreviousQuarter,
                        dateTimeOfLastDayOfPreviousQuarter);

        TotalCostDTO lostOfPreviousPeriodTotalCost = calculateCost(lostOfPreviousPeriod, currencies.getSystemCurrency(), currencies.getRatesOfMainCurrency());

        return WidgetTotalSalesOpportunityDataDto.builder()
                .account(accountEmail)
                .created((long) createdOfCurrentPeriod.size())
                .createdTotalAmount(createdOfCurrentPeriodTotalCost.getTotalCost())
                .createdPercentOfPast(getComparingPercent(createdOfCurrentPeriodTotalCost.getTotalCost(),
                        createdOfPreviousPeriodTotalCost.getTotalCost()))
                .lost((long) lostOfCurrentPeriod.size())
                .lostTotalAmount(lostOfCurrentPeriodTotalCost.getTotalCost())
                .lostPercentOfPast(getComparingPercent(lostOfCurrentPeriodTotalCost.getTotalCost(),
                        lostOfPreviousPeriodTotalCost.getTotalCost()))
                .successful((long) successfulOfCurrentPeriod.size())
                .successfulTotalAmount(successfulOfCurrentPeriodTotalCost.getTotalCost())
                .successfulPercentOfPast(getComparingPercent(successfulOfCurrentPeriodTotalCost.getTotalCost(),
                        successfulOfPreviousPeriodTotalCost.getTotalCost()))
                .quarter(getCurrentQuarter())
                .currency(currencies.getSystemCurrency())
                .build();
    }

    private int getCurrentQuarter() {
        return LocalDate.now().get(IsoFields.QUARTER_OF_YEAR);

    }

    private Double getComparingPercent(@NonNull Double currentAmount, @NonNull Double previousAmount) {
        if (previousAmount == 0d) {
            return 100d;
        } else {
            return ((currentAmount - previousAmount) * 100) / previousAmount;
        }
    }

    private TotalCostDTO calculateCost(List<Opportunity> opportunities, String systemCurrency, List<RateOfMainCurrencyDto> ratesOfMainCurrency) {
        Map<String, Double> map = ratesOfMainCurrency.stream()
                .collect(Collectors.toMap(RateOfMainCurrencyDto::getCurrency, RateOfMainCurrencyDto::getRate));
        map.put(systemCurrency, 1D);
        Map<String, Double> costInCurrencies = new HashMap<>();
        Double collect = opportunities.stream()
                .filter(opportunity -> opportunity.getAmount() != null)
                .map(opportunity -> opportunity.getAmount() * map.get(opportunity.getCurrency()))
                .mapToDouble(Double::doubleValue).sum();
        return new TotalCostDTO(collect, systemCurrency, costInCurrencies, ratesOfMainCurrency);
    }

}
