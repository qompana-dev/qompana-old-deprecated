package com.devqube.crmbusinessservice.widget.totalSalesOpportunity.web;

import com.devqube.crmbusinessservice.widget.totalSalesOpportunity.WidgetTotalSalesOpportunityService;
import com.devqube.crmbusinessservice.widget.totalSalesOpportunity.web.dto.WidgetTotalSalesOpportunityDataDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/widget/total-sales-opportunity")
@Slf4j
public class WidgetTotalSalesOpportunityController implements WidgetTotalSalesOpportunityApi {
    private final WidgetTotalSalesOpportunityService widgetTotalSalesOpportunityService;

    public WidgetTotalSalesOpportunityController(WidgetTotalSalesOpportunityService widgetTotalSalesOpportunityService) {
        this.widgetTotalSalesOpportunityService = widgetTotalSalesOpportunityService;
    }

    @Override
    @GetMapping("/data")
    public WidgetTotalSalesOpportunityDataDto getWidgetTotalSalesOpportunityData() throws EntityNotFoundException, BadRequestException {
        return widgetTotalSalesOpportunityService.getWidgetData();
    }

}
