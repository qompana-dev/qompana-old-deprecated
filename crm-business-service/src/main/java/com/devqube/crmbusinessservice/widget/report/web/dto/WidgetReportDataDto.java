package com.devqube.crmbusinessservice.widget.report.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WidgetReportDataDto {
    private WidgetReportDto widgetReportDto;
    private List<DataSetDto> chartData = new ArrayList<>();
    private List<String> lineChartLabels = new ArrayList<>();
}
