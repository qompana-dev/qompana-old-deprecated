package com.devqube.crmbusinessservice.widget.report.web.dto;

import com.devqube.crmbusinessservice.widget.report.model.ChartTypeEnum;
import com.devqube.crmshared.widgetreport.model.WidgetColumnEnum;
import com.devqube.crmshared.widgetreport.model.WidgetReportTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WidgetReportDto {
    @NotNull
    private WidgetReportTypeEnum reportType;
    @NotNull
    private WidgetColumnEnum widgetColumn;

    @NotNull
    private LocalDateTime start;
    private LocalDateTime end;

    @NotNull
    private Boolean withTime;

    private List<Long> users;

    @NotNull
    private ChartTypeEnum chartType;

    private String additionalValue;

    @Min(2)
    @Max(24)
    @NotNull
    private Integer rows;

    @Min(2)
    @Max(24)
    @NotNull
    private Integer cols;

    @Min(0)
    @Max(23)
    @NotNull
    private Integer x;

    @Min(0)
    @Max(23)
    @NotNull
    private Integer y;
}
