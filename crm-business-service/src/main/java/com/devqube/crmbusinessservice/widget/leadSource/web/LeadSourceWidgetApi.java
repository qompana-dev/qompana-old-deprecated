package com.devqube.crmbusinessservice.widget.leadSource.web;

import com.devqube.crmbusinessservice.widget.leadSource.Period;
import com.devqube.crmbusinessservice.widget.leadSource.web.dto.LeadSourcesDtoOut;
import com.devqube.crmbusinessservice.widget.openedSalesOpportunity.web.dto.WidgetOpenedSalesOpportunityDataDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

@Validated
public interface LeadSourceWidgetApi {
    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Get data for source of leads widget")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "SourceOfLeads Widget data retrieved successfully",
                    response = WidgetOpenedSalesOpportunityDataDto.class)})
    @RequestMapping(value = "/widget/lead-source",
            produces = {"application/json"},
            method = RequestMethod.GET)
    LeadSourcesDtoOut getWidgetOpenedSalesOpportunityData(@RequestParam("period") Period period);

}
