package com.devqube.crmbusinessservice.widget.goal.web;

import com.devqube.crmbusinessservice.widget.goal.WidgetGoalService;
import com.devqube.crmbusinessservice.widget.goal.web.dto.WidgetGoalDataDto;
import com.devqube.crmbusinessservice.widget.goal.web.dto.WidgetGoalDto;
import com.devqube.crmshared.dashboard.dto.DashboardWidgetDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RestController
public class WidgetGoalController implements WidgetGoalApi {
    private final WidgetGoalService widgetGoalService;

    public WidgetGoalController(WidgetGoalService widgetGoalService) {
        this.widgetGoalService = widgetGoalService;
    }

    @Override
    @PostMapping("/widget-goals")
    public DashboardWidgetDto save(@Valid @RequestBody WidgetGoalDto widgetGoalDto, String email)
            throws EntityNotFoundException, BadRequestException {
        return widgetGoalService.save(widgetGoalDto);
    }

    @Override
    @DeleteMapping("/internal/widget-goals/{widgetIds}")
    public void deleteGoalWidgetById(@PathVariable Set<Long> widgetIds) {
        widgetGoalService.deleteByIds(widgetIds);
    }

    @Override
    @GetMapping("/widget-goals/{id}")
    public WidgetGoalDataDto getWidgetGoalData(@PathVariable("id") Long id) throws EntityNotFoundException, BadRequestException {
        return widgetGoalService.getById(id);
    }
}
