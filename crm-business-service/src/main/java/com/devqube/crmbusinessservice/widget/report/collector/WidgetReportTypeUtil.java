package com.devqube.crmbusinessservice.widget.report.collector;

import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReport;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReportUser;
import com.devqube.crmbusinessservice.widget.report.util.WidgetReportConverterService;
import com.devqube.crmbusinessservice.widget.report.web.dto.DataSetDto;
import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDataDto;
import com.devqube.crmshared.currency.dto.CurrencyDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.widgetreport.dto.ReportDto;
import com.devqube.crmshared.widgetreport.exception.CollectDataException;
import com.devqube.crmshared.widgetreport.model.WidgetColumnEnum;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class WidgetReportTypeUtil {
    private final WidgetReportConverterService widgetReportConverterService;
    private final UserService userService;
    private final ModelMapper modelStrictMapper;

    public WidgetReportTypeUtil(WidgetReportConverterService widgetReportConverterService, UserService userService, @Qualifier("strictModelMapper") ModelMapper modelStrictMapper) {
        this.widgetReportConverterService = widgetReportConverterService;
        this.userService = userService;
        this.modelStrictMapper = modelStrictMapper;
    }

    public WidgetReportDataDto getWidgetReportDataDto(Map<Long, Map<String, Double>> map, WidgetReport widgetReport) {
        WidgetReportDataDto widgetReportDataDto = new WidgetReportDataDto();
        widgetReportDataDto.setWidgetReportDto(widgetReportConverterService.toReportDto(widgetReport));

        widgetReportDataDto.setChartData(new ArrayList<>());

        if (map.keySet().size() == 0) {
            return widgetReportDataDto;
        }
        Set<String> firstDatasetX = map.get(map.keySet().stream().findFirst().get()).keySet();
        if (firstDatasetX.size() == 0) {
            return widgetReportDataDto;
        }
        widgetReportDataDto.setLineChartLabels(new ArrayList<>(firstDatasetX));

        if (widgetReport.getWidgetColumn().equals(WidgetColumnEnum.OWNER)) {
            setDatasetForOwner(widgetReportDataDto, map);
        } else {
            setDefaultDataset(widgetReportDataDto, map);
        }

        return widgetReportDataDto;
    }

    private void setDefaultDataset(WidgetReportDataDto result, Map<Long, Map<String, Double>> map) {
        for (Long key : map.keySet()) {
            List<Double> listToAdd = new ArrayList<>();
            result.getLineChartLabels().forEach(label -> listToAdd.add(map.get(key).get(label)));
            result.getChartData().add(new DataSetDto(listToAdd, key));
        }
    }

    private void setDatasetForOwner(WidgetReportDataDto result, Map<Long, Map<String, Double>> map) {
        List<Double> listToAdd = new ArrayList<>();
        for (Long key : map.keySet()) {
            listToAdd.add(map.get(key).get(key.toString()));
        }
        result.getChartData().add(new DataSetDto(listToAdd, null));
    }


    public CurrencyDto getCurrencyDto() throws CollectDataException {
        CurrencyDto currencyDto = null;
        try {
            currencyDto = userService.getCurrencies();
        } catch (BadRequestException e) {
            throw new CollectDataException("unable to obtain currencies");
        }
        return currencyDto;
    }

    public ReportDto getReportDto(WidgetReport widgetReport) {
        ReportDto result = modelStrictMapper.map(widgetReport, ReportDto.class);
        result.setUsers(widgetReport.getUserList().stream().map(WidgetReportUser::getAccountId).collect(Collectors.toList()));
        return result;
    }
}
