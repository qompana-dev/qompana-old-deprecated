package com.devqube.crmbusinessservice.widget.report.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DataSetDto {
    private List<Double> data = new ArrayList<>();
    private Long label;
}
