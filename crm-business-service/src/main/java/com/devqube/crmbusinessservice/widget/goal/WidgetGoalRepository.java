package com.devqube.crmbusinessservice.widget.goal;

import com.devqube.crmbusinessservice.widget.goal.model.WidgetGoal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface WidgetGoalRepository extends JpaRepository<WidgetGoal, Long> {
    void deleteAllByIdIn(Set<Long> widgetIds);
}
