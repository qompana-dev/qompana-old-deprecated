package com.devqube.crmbusinessservice.widget.leadSource;

import com.devqube.crmbusinessservice.dictionary.WordRepository;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadSpecifications;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.widget.leadSource.web.dto.LeadSourceDtoOut;
import com.devqube.crmbusinessservice.widget.leadSource.web.dto.LeadSourcesDtoOut;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map.Entry;

@Service
@RequiredArgsConstructor
public class LeadSourceWidgetService {

    private final LeadRepository leadRepository;
    private final WordRepository wordRepository;

    public LeadSourcesDtoOut calculateLeadSourceChanges(Period period) {
        LeadSource currentLeadSource = calculateCurrentLeadSource();
        LeadSource previousPeriodLeadSource = calculatePreviousPeriodLeadSource(period);

        LeadSourcesDtoOut leadSourcesDtoOut = new LeadSourcesDtoOut();
        leadSourcesDtoOut.setTotal(currentLeadSource.getTotal());
        for (int i = 0; i < 6; i++) { //top 6 sources by lead number
            Entry<Long, Long> maxLeadNumber = null;
            for (Entry<Long, Long> source : currentLeadSource.getSourceMap().entrySet()) {
                if (maxLeadNumber == null) {
                    maxLeadNumber = source;
                } else {
                    if (maxLeadNumber.getValue() < source.getValue()) {
                        maxLeadNumber = source;
                    }
                }
            }
            if (maxLeadNumber != null) {
                String sourceName = wordRepository.findById(maxLeadNumber.getKey()).get().getName();

                Double currentSourcePercentage = ((double) maxLeadNumber.getValue() / (double) currentLeadSource.getTotal()) * 100;
                Long previousPeriodSourceNumber = previousPeriodLeadSource.getSourceMap().get(maxLeadNumber.getKey());
                Double previousSourcePercentage = 0.0;
                if (previousPeriodSourceNumber != null) {
                    previousSourcePercentage = ((double) previousPeriodLeadSource.getSourceMap().get(maxLeadNumber.getKey()) / (double) previousPeriodLeadSource.getTotal()) * 100;

                }
                Double percentageChange = Math.round((currentSourcePercentage - previousSourcePercentage) * 10) / 10.0;

                currentLeadSource.getSourceMap().remove(maxLeadNumber.getKey());
                previousPeriodLeadSource.getSourceMap().remove(maxLeadNumber.getKey());

                leadSourcesDtoOut.getSources().add(new LeadSourceDtoOut()
                        .setName(sourceName)
                        .setCount(maxLeadNumber.getValue())
                        .setPercentageChange(percentageChange));
            }
        }

        Long currentOtherNumber = currentLeadSource.getNoSource();
        for (Entry<Long, Long> source : currentLeadSource.getSourceMap().entrySet()) {
            currentOtherNumber = currentOtherNumber + source.getValue();
        }
        Double currentOtherPercentage = ((double) currentOtherNumber / (double) currentLeadSource.getTotal()) * 100;


        Long previousOtherNumber = previousPeriodLeadSource.getNoSource();
        for (Entry<Long, Long> source : previousPeriodLeadSource.getSourceMap().entrySet()) {
            previousOtherNumber = previousOtherNumber + source.getValue();
        }
        Double previousOtherPercentage = ((double) previousOtherNumber / (double) previousPeriodLeadSource.getTotal()) * 100;
        Double otherPercentageChange = Math.round((currentOtherPercentage - previousOtherPercentage) * 10) / 10.0;

        leadSourcesDtoOut.getSources().add(new LeadSourceDtoOut()
                .setName("OTHER")
                .setCount(currentOtherNumber)
                .setPercentageChange(otherPercentageChange));


        return leadSourcesDtoOut;
    }

    private LeadSource calculateCurrentLeadSource() {
        final LocalTime endTimeOfDay = LocalTime.of(23, 59, 59);
        LocalDate date = LocalDate.now().minusDays(1);

        LocalDateTime dateTime = LocalDateTime.of(date, endTimeOfDay);
        List<Lead> currentLeadList = leadRepository.findAll(LeadSpecifications.findCreatedBeforeInclusive(dateTime)
                .and(LeadSpecifications.findByDeleted(false)));
        return calculateLeadSource(currentLeadList);
    }


    private LeadSource calculatePreviousPeriodLeadSource(Period period) {
        final LocalTime endTimeOfDay = LocalTime.of(23, 59, 59);
        LocalDate date = LocalDate.now();
        switch (period) {
            case DAY: {
                date = date.minusDays(2);
                break;
            }
            case WEEK: {
                date = date.minusDays(1).minusWeeks(1);
                break;
            }
            case MONTH: {
                date = date.minusDays(1).minusMonths(1);
                break;
            }
        }
        LocalDateTime dateTime = LocalDateTime.of(date, endTimeOfDay);

        List<Lead> previousLeadList = leadRepository.findAll(LeadSpecifications.findCreatedBeforeInclusive(dateTime).and(
                LeadSpecifications.findByDeleted(false)));
        return calculateLeadSource(previousLeadList);
    }


    private LeadSource calculateLeadSource(List<Lead> leadList) {
        LeadSource leadSource = new LeadSource();
        leadSource.setTotal((long) leadList.size());

        for (Lead lead : leadList) {
            if (lead.getSourceOfAcquisition() == null && lead.getSubSourceOfAcquisition() == null) {
                Long noSourceLeadsNumber = leadSource.getNoSource();
                noSourceLeadsNumber = noSourceLeadsNumber + 1;
                leadSource.setNoSource(noSourceLeadsNumber);
            } else if (lead.getSubSourceOfAcquisition() != null) {
                Long leadsNumber = leadSource.getSourceMap().get(lead.getSubSourceOfAcquisition());
                if (leadsNumber != null) {
                    leadsNumber = leadsNumber + 1;
                    leadSource.getSourceMap().put(lead.getSubSourceOfAcquisition(), leadsNumber);

                } else {
                    leadSource.getSourceMap().put(lead.getSubSourceOfAcquisition(), 1L);
                }
            } else {
                Long leadsNumber = leadSource.getSourceMap().get(lead.getSourceOfAcquisition());
                if (leadsNumber != null) {
                    leadsNumber = leadsNumber + 1;
                    leadSource.getSourceMap().put(lead.getSourceOfAcquisition(), leadsNumber);

                } else {
                    leadSource.getSourceMap().put(lead.getSourceOfAcquisition(), 1L);
                }
            }
        }
        return leadSource;
    }

}
