package com.devqube.crmbusinessservice.widget.report.collector.types.abstracttypes;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.widget.report.collector.WidgetReportTypeUtil;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReport;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReportUser;
import com.devqube.crmshared.currency.dto.CurrencyDto;
import com.devqube.crmshared.currency.dto.RateOfMainCurrencyDto;
import com.devqube.crmshared.widgetreport.model.WidgetColumnEnum;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOpportunityType {
    protected final EntityManager em;
    protected final WidgetReportTypeUtil widgetReportTypeUtil;

    public AbstractOpportunityType(EntityManager em, WidgetReportTypeUtil widgetReportTypeUtil) {
        this.em = em;
        this.widgetReportTypeUtil = widgetReportTypeUtil;
    }

    protected Path<Object> getGroupBy(WidgetReport widgetReport, Root<?> from, Integer additionalNumber) {
        switch (widgetReport.getWidgetColumn()) {
            case CUSTOMER:
                return from.get("customer").get("id");
            case CONTACT:
                return from.get("contact").get("id");
            case OPPORTUNITY_PRIORITY:
                return from.get("priority");
            case CURRENCY:
                return from.get("currency");
            case FINISH_RESULT:
                return from.get("finishResult");
            case REJECTION_RESULT:
                return from.get("rejectionReason");
            case OWNER:
                String ownerField = (additionalNumber == 1) ? "ownerOneId" : ((additionalNumber == 2) ? "ownerTwoId" : "ownerThreeId");
                return from.get(ownerField);
            default:
                return null;
        }
    }


    protected Predicate getOwnerPredicate(CriteriaBuilder cb, WidgetReport widgetReport, Root<Opportunity> from, Object groupBy) {
        if (widgetReport.getWidgetColumn().equals(WidgetColumnEnum.OWNER)) {
            return cb.or(
                    cb.equal(getGroupBy(widgetReport, from, 1), groupBy),
                    cb.equal(getGroupBy(widgetReport, from, 2), groupBy),
                    cb.equal(getGroupBy(widgetReport, from, 3), groupBy)
            );
        }
        return cb.equal(getGroupBy(widgetReport, from, null), groupBy);
    }

    public List<Object> getDistinctX(WidgetReport widgetReport) {
        if (widgetReport.getWidgetColumn().equals(WidgetColumnEnum.OWNER)) {
            return widgetReport.getUserList().stream().map(WidgetReportUser::getAccountId).collect(Collectors.toList());
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Root<Opportunity> from = cq.from(Opportunity.class);

        List<Long> userList = widgetReport.getUserList().stream().map(WidgetReportUser::getAccountId).collect(Collectors.toList());

        CriteriaQuery<Object> distinct1 = cq.select(getGroupBy(widgetReport, from, null))
                .where(getBasicWhere(widgetReport, cb, from, userList)).distinct(true);
        return em.createQuery(distinct1).getResultList();
    }


    public abstract Predicate getBasicWhere(WidgetReport widgetReport, CriteriaBuilder cb, Root<Opportunity> from, List<Long> userIds);

    protected Predicate getDefaultWhere(WidgetReport widgetReport, CriteriaBuilder cb, Root<Opportunity> from, List<Long> userIds) {
        Predicate and = cb.and(cb.greaterThanOrEqualTo(from.get("created"), widgetReport.getStart()),
                cb.or(from.get("ownerOneId").in(userIds), from.get("ownerTwoId").in(userIds), from.get("ownerThreeId").in(userIds)));
        return (widgetReport.getEnd() == null) ? and : cb.and(and, cb.lessThan(from.get("created"), widgetReport.getEnd()));
    }

    protected Double getOpportunityAmount(Opportunity opportunity, Long userId, CurrencyDto currencyDto) {
        if (opportunity.getAmount() == null || opportunity.getAmount() == 0.0) {
            return 0.0;
        }
        Long userPercent = 0L;
        if (opportunity.getOwnerOneId().equals(userId)) {
            userPercent = opportunity.getOwnerOnePercentage();
        } else {
            userPercent = (opportunity.getOwnerTwoId().equals(userId)) ? opportunity.getOwnerTwoPercentage() : opportunity.getOwnerThreePercentage();
        }

        double amount = opportunity.getAmount() * (userPercent / 100.0);
        return convertAmountToSystemCurrency(opportunity, amount, currencyDto);
    }

    protected double convertAmountToSystemCurrency(Opportunity opportunity, double amount, CurrencyDto currencyDto) {
        if (opportunity.getCurrency().equals(currencyDto.getSystemCurrency())) {
            return amount;
        }
        Optional<RateOfMainCurrencyDto> opportunityCurrency = currencyDto.getRatesOfMainCurrency().stream()
                .filter(c -> c.getCurrency().equals(opportunity.getCurrency())).findFirst();

        if (opportunityCurrency.isEmpty()) {
            return 0.0;
        }
        return opportunityCurrency.get().getRate() * amount;
    }
}
