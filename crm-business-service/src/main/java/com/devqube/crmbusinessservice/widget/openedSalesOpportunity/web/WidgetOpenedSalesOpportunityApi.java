package com.devqube.crmbusinessservice.widget.openedSalesOpportunity.web;

import com.devqube.crmbusinessservice.widget.openedSalesOpportunity.web.dto.WidgetOpenedSalesOpportunityDataDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.validation.annotation.Validated;

@Validated
public interface WidgetOpenedSalesOpportunityApi {

    @ApiOperation(value = "Get data for openedSalesOpportunity widget")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "OpenedSalesOpportunity Widget data retrieved successfully",
                    response = WidgetOpenedSalesOpportunityDataDto.class),
            @ApiResponse(code = 404, message = "OpenedSalesOpportunity Widget data not found")
    })
    WidgetOpenedSalesOpportunityDataDto getWidgetOpenedSalesOpportunityData(String processDefinitionId) throws EntityNotFoundException, BadRequestException;

    @ApiOperation(value = "Get data for openedSalesOpportunity widget data with separate process versions")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "OpenedSalesOpportunity Widget data retrieved successfully",
                    response = WidgetOpenedSalesOpportunityDataDto.class),
            @ApiResponse(code = 404, message = "OpenedSalesOpportunity Widget data not found")
    })
    WidgetOpenedSalesOpportunityDataDto getWidgetOpenedSalesOpportunitySeparateProcessData() throws EntityNotFoundException, BadRequestException;
}