package com.devqube.crmbusinessservice.widget.leadSource.web;

import com.devqube.crmbusinessservice.widget.leadSource.LeadSourceWidgetService;
import com.devqube.crmbusinessservice.widget.leadSource.Period;
import com.devqube.crmbusinessservice.widget.leadSource.web.dto.LeadSourcesDtoOut;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class LeadSourceWidgetController implements LeadSourceWidgetApi {

    private final LeadSourceWidgetService leadSourceWidgetService;

    @Override
    public LeadSourcesDtoOut getWidgetOpenedSalesOpportunityData(@RequestParam("period") Period period) {
        return leadSourceWidgetService.calculateLeadSourceChanges(period);
    }

}