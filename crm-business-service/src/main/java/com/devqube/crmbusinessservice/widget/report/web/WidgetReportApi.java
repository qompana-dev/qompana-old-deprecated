package com.devqube.crmbusinessservice.widget.report.web;

import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDataDto;
import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDto;
import com.devqube.crmshared.dashboard.dto.DashboardWidgetDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.widgetreport.exception.CollectDataException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Set;

public interface WidgetReportApi {

    @ApiOperation(value = "Save report widget configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Report widget configuration saved successfully", response = DashboardWidgetDto.class)
    })
    ResponseEntity<DashboardWidgetDto> saveReportWidgetConfiguration(WidgetReportDto widgetReportDto) throws BadRequestException;

    @ApiOperation(value = "Get report widget configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Get widget configuration", response = WidgetReportDto.class)
    })
    ResponseEntity<WidgetReportDto> getReportWidgetConfiguration(Long widgetId) throws EntityNotFoundException;


    @ApiOperation(value = "Get report widget data")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Get widget data", response = WidgetReportDataDto.class)
    })
    ResponseEntity<WidgetReportDataDto> getReportWidgetData(@PathVariable Long widgetId) throws EntityNotFoundException, CollectDataException;

    @ApiOperation(value = "Deleted report widget configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Deleted widget configuration")
    })
    ResponseEntity<Void> deleteReportById(Set<Long> widgetIds);
}
