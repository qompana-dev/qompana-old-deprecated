package com.devqube.crmbusinessservice.widget.report;

import com.devqube.crmbusinessservice.widget.report.model.WidgetReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface WidgetReportRepository extends JpaRepository<WidgetReport, Long> {
    void deleteAllByIdIn(Set<Long> widgetIds);
}
