package com.devqube.crmbusinessservice.widget.openedSalesOpportunity.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProcessStageInfo {
    private Long stage;
    private Long totalQuantity;
    private Double totalAmount;
}
