package com.devqube.crmbusinessservice.widget.totalSalesOpportunity.web.dto;


import com.devqube.crmshared.user.dto.AccountEmail;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WidgetTotalSalesOpportunityDataDto {
    private AccountEmail account;
    private Integer quarter;
    private Long created;
    private Double createdPercentOfPast;
    private Double createdTotalAmount;
    private Long successful;
    private Double successfulPercentOfPast;
    private Double successfulTotalAmount;
    private Long lost;
    private Double lostPercentOfPast;
    private Double lostTotalAmount;
    private String currency;


}
