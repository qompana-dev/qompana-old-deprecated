package com.devqube.crmbusinessservice.widget.goal.web.dto;

import com.devqube.crmbusinessservice.widget.goal.model.ChartType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Valid
public class WidgetGoalDto {
    @NotNull
    private Long goalId;

    @NotNull
    private ChartType chartType;

    @NotNull
    private List<WidgetGoalParticipantDto> participants;


    @Min(2)
    @Max(24)
    @NotNull
    private Integer rows;

    @Min(2)
    @Max(24)
    @NotNull
    private Integer cols;

    @Min(0)
    @Max(23)
    @NotNull
    private Integer x;

    @Min(0)
    @Max(23)
    @NotNull
    private Integer y;
}
