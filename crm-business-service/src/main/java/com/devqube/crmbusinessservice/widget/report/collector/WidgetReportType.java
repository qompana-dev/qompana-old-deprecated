package com.devqube.crmbusinessservice.widget.report.collector;

import com.devqube.crmbusinessservice.widget.report.model.WidgetReport;
import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDataDto;
import com.devqube.crmshared.widgetreport.exception.CollectDataException;

public interface WidgetReportType {
    WidgetReportDataDto getData(WidgetReport widgetReport) throws CollectDataException;
}
