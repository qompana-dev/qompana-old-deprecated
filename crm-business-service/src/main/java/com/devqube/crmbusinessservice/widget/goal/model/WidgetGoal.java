package com.devqube.crmbusinessservice.widget.goal.model;

import com.devqube.crmbusinessservice.goal.model.Goal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WidgetGoal {
    @Id
    @SequenceGenerator(name = "widget_goal_seq", sequenceName = "widget_goal_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "widget_goal_seq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "goal_id")
    private Goal goal;

    @Enumerated(EnumType.STRING)
    private ChartType chartType;

    @OneToMany(mappedBy = "widgetGoal", cascade = CascadeType.ALL)
    private List<WidgetGoalParticipant> participants;
}
