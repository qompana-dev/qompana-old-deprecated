package com.devqube.crmbusinessservice.widget.report;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReport;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReportUser;
import com.devqube.crmbusinessservice.widget.report.util.WidgetReportConverterService;
import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDto;
import com.devqube.crmshared.dashboard.dto.DashboardWidgetDto;
import com.devqube.crmshared.dashboard.dto.SingleDashboardWidgetDto;
import com.devqube.crmshared.dashboard.model.WidgetTypeEnum;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.web.CurrentRequestUtil;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class WidgetReportService {
    private final WidgetReportRepository widgetReportRepository;
    private final ModelMapper modelStrictMapper;
    private final UserClient userClient;
    private final WidgetReportConverterService widgetReportConverterService;

    public WidgetReportService(WidgetReportRepository widgetReportRepository, @Qualifier("strictModelMapper") ModelMapper modelStrictMapper, UserClient userClient, WidgetReportConverterService widgetReportConverterService) {
        this.widgetReportRepository = widgetReportRepository;
        this.modelStrictMapper = modelStrictMapper;
        this.userClient = userClient;
        this.widgetReportConverterService = widgetReportConverterService;
    }

    @Transactional(rollbackFor = BadRequestException.class)
    public DashboardWidgetDto saveWidgetConfiguration(WidgetReportDto widgetReportDto) throws BadRequestException {
        WidgetReport toSave = modelStrictMapper.map(widgetReportDto, WidgetReport.class);
        List<WidgetReportUser> userList = widgetReportDto.getUsers().stream().map(c -> new WidgetReportUser(null, toSave, c)).collect(Collectors.toList());
        toSave.setUserList(userList);
        if (!toSave.getReportType().getColumns().contains(toSave.getWidgetColumn())) {
            throw new BadRequestException();
        }
        WidgetReport saved = widgetReportRepository.save(toSave);
        SingleDashboardWidgetDto singleDashboardWidgetDto;
        try {
            singleDashboardWidgetDto = SingleDashboardWidgetDto.builder()
                    .widgetTypeEnum(WidgetTypeEnum.REPORT)
                    .widgetId(saved.getId())
                    .cols(widgetReportDto.getCols())
                    .rows(widgetReportDto.getRows())
                    .x(widgetReportDto.getX())
                    .y(widgetReportDto.getY())
                    .build();
            return userClient.saveWidget(singleDashboardWidgetDto, CurrentRequestUtil.getLoggedInAccountEmail());
        } catch (Exception e) {
            throw new BadRequestException();
        }
    }

    public WidgetReportDto getWidgetReport(Long widgetId) throws EntityNotFoundException {
        WidgetReport widgetReport = widgetReportRepository.findById(widgetId).orElseThrow(EntityNotFoundException::new);
        return widgetReportConverterService.toReportDto(widgetReport);
    }

    @Transactional
    public void deleteWidgets(Set<Long> widgetIds) {
        widgetReportRepository.deleteAllByIdIn(widgetIds);
    }
}
