package com.devqube.crmbusinessservice.widget.goal.web.dto;

import com.devqube.crmbusinessservice.goal.model.GoalIntervalType;
import com.devqube.crmbusinessservice.goal.model.GoalType;
import com.devqube.crmbusinessservice.widget.goal.model.ChartType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WidgetGoalDataDto {
    private Long id;
    private String name;
    private GoalType type;
    private GoalIntervalType interval;
    private Integer intervalNumber;
    private Integer startIndex;
    private Integer startYear;
    private String currency;

    private ChartType chartType;
    private List<UserWidgetDataDto> userWidgetDataDtoList = new ArrayList<>();
}
