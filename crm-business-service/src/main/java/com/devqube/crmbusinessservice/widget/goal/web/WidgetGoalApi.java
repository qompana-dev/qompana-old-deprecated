package com.devqube.crmbusinessservice.widget.goal.web;

import com.devqube.crmbusinessservice.widget.goal.web.dto.WidgetGoalDataDto;
import com.devqube.crmbusinessservice.widget.goal.web.dto.WidgetGoalDto;
import com.devqube.crmshared.dashboard.dto.DashboardWidgetDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.Set;

@Validated
public interface WidgetGoalApi {
    @ApiOperation(value = "Create new goal widget")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Widget goal created successfully", response = DashboardWidgetDto.class),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 404, message = "goal not found")
    })
    DashboardWidgetDto save(@Valid WidgetGoalDto widgetGoalDto, String email) throws EntityNotFoundException, BadRequestException;

    @ApiOperation(value = "Delete goal widget configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Deleted goal widget configuration")
    })
    void deleteGoalWidgetById(Set<Long> widgetIds);

    @ApiOperation(value = "Get data for goal widget by id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Widget goal retrieved successfully", response = WidgetGoalDataDto.class),
            @ApiResponse(code = 404, message = "Widget goal not found")
    })
    WidgetGoalDataDto getWidgetGoalData(Long id) throws EntityNotFoundException, BadRequestException;
}

