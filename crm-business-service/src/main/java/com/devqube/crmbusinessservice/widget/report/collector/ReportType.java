package com.devqube.crmbusinessservice.widget.report.collector;

import com.devqube.crmshared.widgetreport.model.WidgetReportTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ReportType {
    public WidgetReportTypeEnum type();
}
