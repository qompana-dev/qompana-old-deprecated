package com.devqube.crmbusinessservice.widget.goal.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Valid
public class WidgetGoalParticipantDto {
    @NotNull
    private Long userId;

    @NotNull
    private boolean goal;
}
