package com.devqube.crmbusinessservice.widget.report.util;

import com.devqube.crmbusinessservice.widget.report.model.WidgetReport;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReportUser;
import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class WidgetReportConverterService {
    private final ModelMapper modelStrictMapper;

    public WidgetReportConverterService(@Qualifier("strictModelMapper") ModelMapper modelStrictMapper) {
        this.modelStrictMapper = modelStrictMapper;
    }

    public WidgetReportDto toReportDto(WidgetReport widgetReport) {
        WidgetReportDto result = modelStrictMapper.map(widgetReport, WidgetReportDto.class);
        result.setUsers(widgetReport.getUserList().stream().map(WidgetReportUser::getAccountId).collect(Collectors.toList()));
        return result;
    }
}
