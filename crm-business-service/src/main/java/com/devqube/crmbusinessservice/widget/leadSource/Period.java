package com.devqube.crmbusinessservice.widget.leadSource;

public enum Period {
    DAY, WEEK, MONTH
}
