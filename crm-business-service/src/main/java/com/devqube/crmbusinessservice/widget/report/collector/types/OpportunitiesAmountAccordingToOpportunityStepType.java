package com.devqube.crmbusinessservice.widget.report.collector.types;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineReadService;
import com.devqube.crmbusinessservice.widget.report.collector.ReportType;
import com.devqube.crmbusinessservice.widget.report.collector.WidgetReportType;
import com.devqube.crmbusinessservice.widget.report.collector.WidgetReportTypeUtil;
import com.devqube.crmbusinessservice.widget.report.collector.types.abstracttypes.AbstractOpportunityType;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReport;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReportUser;
import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDataDto;
import com.devqube.crmshared.currency.dto.CurrencyDto;
import com.devqube.crmshared.widgetreport.exception.CollectDataException;
import com.devqube.crmshared.widgetreport.model.WidgetReportTypeEnum;
import com.google.common.base.Strings;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.stream.Collectors;

@Service
@ReportType(type = WidgetReportTypeEnum.OPPORTUNITIES_AMOUNT_ACCORDING_TO_OPPORTUNITY_STEP)
public class OpportunitiesAmountAccordingToOpportunityStepType extends AbstractOpportunityType implements WidgetReportType {
    private final ProcessEngineReadService processEngineReadService;

    public OpportunitiesAmountAccordingToOpportunityStepType(EntityManager em, WidgetReportTypeUtil widgetReportTypeUtil, ProcessEngineReadService processEngineReadService) {
        super(em, widgetReportTypeUtil);
        this.processEngineReadService = processEngineReadService;
    }

    @Override
    public WidgetReportDataDto getData(WidgetReport widgetReport) throws CollectDataException {
        if (!WidgetReportTypeEnum.OPPORTUNITIES_AMOUNT_ACCORDING_TO_OPPORTUNITY_STEP.getColumns().contains(widgetReport.getWidgetColumn())) {
            throw new CollectDataException();
        }

        Map<Long, Map<String, Double>> map = getResultMap(widgetReport);
        return widgetReportTypeUtil.getWidgetReportDataDto(map, widgetReport);
    }


    private Map<Long, Map<String, Double>> getResultMap(WidgetReport widgetReport) throws CollectDataException {
        CurrencyDto currencyDto = widgetReportTypeUtil.getCurrencyDto();
        Map<Long, Map<String, Double>> result = new HashMap<>(); // <userId, <x, result>>
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Opportunity> cq = cb.createQuery(Opportunity.class);
        Root<Opportunity> from = cq.from(Opportunity.class);
        CriteriaQuery<Opportunity> select = cq.select(from);

        Map<String, String> allActiveTask = processEngineReadService.getAllActiveTask(widgetReport.getAdditionalValue());
        Set<String> distinctX = allActiveTask.keySet().stream().map(allActiveTask::get).filter(Objects::nonNull).collect(Collectors.toSet());
        for (WidgetReportUser widgetReportUser : widgetReport.getUserList()) {
            result.put(widgetReportUser.getAccountId(), new HashMap<String, Double>());


            for (String groupBy : distinctX) {
                Set<String> processInstanceIdByActiveTask = allActiveTask.keySet().stream().filter(c -> Strings.nullToEmpty(allActiveTask.get(c)).equals(Strings.nullToEmpty(groupBy))).collect(Collectors.toSet());
                Predicate where = cb.and(from.get("processInstanceId").in(processInstanceIdByActiveTask),
                        getBasicWhere(widgetReport, cb, from, Collections.singletonList(widgetReportUser.getAccountId())));

                List<Opportunity> opportunities = em.createQuery(select.where(where)).getResultList();
                Double amount = opportunities.stream().map(c -> getOpportunityAmount(c, widgetReportUser.getAccountId(), currencyDto)).mapToDouble(c -> c).sum();
                result.get(widgetReportUser.getAccountId()).put(Strings.nullToEmpty(groupBy), amount);
            }
        }
        return result;
    }

    public Predicate getBasicWhere(WidgetReport widgetReport, CriteriaBuilder cb, Root<Opportunity> from, List<Long> userIds) {
        return cb.and(cb.equal(from.get("processDefinitionId"), widgetReport.getAdditionalValue()),
                cb.isNull(from.get("finishResult")),
                getDefaultWhere(widgetReport, cb, from, userIds));
    }
}
