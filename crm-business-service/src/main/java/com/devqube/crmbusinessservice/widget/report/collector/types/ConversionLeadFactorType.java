package com.devqube.crmbusinessservice.widget.report.collector.types;

import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.widget.report.collector.ReportType;
import com.devqube.crmbusinessservice.widget.report.collector.WidgetReportType;
import com.devqube.crmbusinessservice.widget.report.collector.WidgetReportTypeUtil;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReport;
import com.devqube.crmbusinessservice.widget.report.model.WidgetReportUser;
import com.devqube.crmbusinessservice.widget.report.web.dto.WidgetReportDataDto;
import com.devqube.crmshared.widgetreport.exception.CollectDataException;
import com.devqube.crmshared.widgetreport.model.WidgetColumnEnum;
import com.devqube.crmshared.widgetreport.model.WidgetReportTypeEnum;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.stream.Collectors;

@Service
@ReportType(type = WidgetReportTypeEnum.CONVERSION_LEAD_FACTOR)
public class ConversionLeadFactorType implements WidgetReportType {
    private final EntityManager em;
    private final WidgetReportTypeUtil widgetReportTypeUtil;

    public ConversionLeadFactorType(EntityManager em, WidgetReportTypeUtil widgetReportTypeUtil) {
        this.em = em;
        this.widgetReportTypeUtil = widgetReportTypeUtil;
    }

    @Override
    public WidgetReportDataDto getData(WidgetReport widgetReport) throws CollectDataException {
        if (!WidgetReportTypeEnum.CONVERSION_LEAD_FACTOR.getColumns().contains(widgetReport.getWidgetColumn())) {
            throw new CollectDataException();
        }

        Map<Long, Map<String, Double>> map = getResultMap(widgetReport);
        return widgetReportTypeUtil.getWidgetReportDataDto(map, widgetReport);
    }


    private Map<Long, Map<String, Double>> getResultMap(WidgetReport widgetReport) {
        List<Object> distinctX = getDistinctX(widgetReport);
        Map<Long, Map<String, Double>> result = new HashMap<>(); // <userId, <x, result>>
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Lead> from = cq.from(Lead.class);
        CriteriaQuery<Long> select = cq.select(cb.count(from));

        for (WidgetReportUser widgetReportUser : widgetReport.getUserList()) {
            result.put(widgetReportUser.getAccountId(), new HashMap<String, Double>());

            for (Object groupBy : distinctX) {
                Predicate where = cb.and(cb.equal(getGroupBy(widgetReport, from), groupBy),
                        getBasicWhere(widgetReport, cb, from, Collections.singletonList(widgetReportUser.getAccountId())));

                Long countAll = em.createQuery(select.where(where)).getSingleResult();
                Long countConverted = em.createQuery(select.where(cb.and(where, cb.equal(from.get("convertedToOpportunity"), true)))).getSingleResult();

                String x = Optional.ofNullable(groupBy).orElse("").toString();
                result.get(widgetReportUser.getAccountId()).put(x, (countConverted == 0L) ? 0.0 : ((countConverted / countAll.doubleValue()) * 100.0));
            }
        }
        return result;
    }

    private List<Object> getDistinctX(WidgetReport widgetReport) {
        if (widgetReport.getWidgetColumn().equals(WidgetColumnEnum.OWNER)) {
            return widgetReport.getUserList().stream().map(WidgetReportUser::getAccountId).collect(Collectors.toList());
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Root<Lead> from = cq.from(Lead.class);
        List<Long> userList = widgetReport.getUserList().stream().map(WidgetReportUser::getAccountId).collect(Collectors.toList());
        CriteriaQuery<Object> distinct = cq.select(getGroupBy(widgetReport, from))
                .where(getBasicWhere(widgetReport, cb, from, userList)).distinct(true);
        return em.createQuery(distinct).getResultList();
    }

    private Path<Object> getGroupBy(WidgetReport widgetReport, Root<?> from) {
        switch (widgetReport.getWidgetColumn()) {
            case COMPANY:
                return from.get("company").get("companyName");
            case LEAD_PRIORITY:
                return from.get("priority");
            case OWNER:
                return from.get("leadKeeperId");
            default:
                return null;
        }
    }

    private Predicate getBasicWhere(WidgetReport widgetReport, CriteriaBuilder cb, Root<?> from, List<Long> userIds) {
        Predicate and = cb.and(cb.greaterThanOrEqualTo(from.get("created"), widgetReport.getStart()),
                from.get("leadKeeperId").in(userIds));
        return (widgetReport.getEnd() == null) ? and : cb.and(and, cb.lessThan(from.get("created"), widgetReport.getEnd()));
    }
}
