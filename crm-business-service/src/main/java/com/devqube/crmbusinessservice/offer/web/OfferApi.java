package com.devqube.crmbusinessservice.offer.web;

import com.devqube.crmbusinessservice.goal.web.dto.GoalDto;
import com.devqube.crmbusinessservice.offer.web.dto.OfferDto;
import com.devqube.crmshared.exception.EntityNotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;

public interface OfferApi {
    @ApiOperation(value = "New offer created")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Offer created successfully", response = GoalDto.class),
            @ApiResponse(code = 404, message = "Opportunity not found")
    })
    ResponseEntity<Void> save(OfferDto offerDto) throws EntityNotFoundException;
}
