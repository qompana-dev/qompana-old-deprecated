package com.devqube.crmbusinessservice.offer;

import com.devqube.crmbusinessservice.offer.web.dto.OfferDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OfferService {

    private final OfferRepository offerRepository;
    private final ModelMapper modelMapper;

    public OfferService(OfferRepository offerRepository, @Qualifier("strictModelMapper") ModelMapper modelMapper) {
        this.offerRepository = offerRepository;
        this.modelMapper = modelMapper;
    }

    public void save(OfferDto offerDto) {
        Optional<Offer> existingOffer = offerRepository.findByUuid(offerDto.getUuid());
        if (existingOffer.isEmpty()) {
            Offer offer = modelMapper.map(offerDto, Offer.class);
            offerRepository.save(offer);
        } else {
            Offer offer = existingOffer.get();
            offer.setGenerationTime(offerDto.getGenerationTime());
            offerRepository.save(offer);
        }
    }
}
