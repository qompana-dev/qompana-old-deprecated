package com.devqube.crmbusinessservice.offer.web;

import com.devqube.crmbusinessservice.offer.OfferService;
import com.devqube.crmbusinessservice.offer.web.dto.OfferDto;
import com.devqube.crmshared.exception.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OfferController implements OfferApi {

    private final OfferService offerService;

    public OfferController(OfferService offerService) {
        this.offerService = offerService;
    }

    @Override
    @PostMapping("/internal/offers")
    public ResponseEntity<Void> save(@RequestBody OfferDto offerDto) throws EntityNotFoundException {
        offerService.save(offerDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
