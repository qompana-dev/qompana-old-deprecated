package com.devqube.crmbusinessservice.offer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

public interface OfferRepository extends JpaRepository<Offer, Long> {
    Optional<Offer> findByUuid(String uuid);

    @Query("select count(o) from Offer o where o.generationTime >= :from and o.generationTime <= :to and o.userId in :userIds")
    Long findCreatedOffersNumberForGoal(@Param("from") LocalDateTime from, @Param("to") LocalDateTime to, @Param("userIds") Set<Long> userIds);

    @Query("select count(distinct o.customerName) from Offer o where o.generationTime >= :from and o.generationTime <= :to and o.userId in :userIds")
    Long findCustomerWhoReceivedOfferNumberForGoal(@Param("from") LocalDateTime from, @Param("to") LocalDateTime to, @Param("userIds") Set<Long> userIds);
}
