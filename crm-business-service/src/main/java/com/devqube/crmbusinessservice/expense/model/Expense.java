package com.devqube.crmbusinessservice.expense.model;

import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmshared.history.SaveCrmChangesListener;
import com.devqube.crmshared.history.annotation.SaveHistory;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@EntityListeners(SaveCrmChangesListener.class)
public class Expense {
    @Id
    @SequenceGenerator(name = "expense_seq", sequenceName = "expense_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "expense_seq")
    @EqualsAndHashCode.Include
    private Long id;

    @Column(nullable = false)
    @EqualsAndHashCode.Include
    private String name;

    private String description;

    private LocalDate date;

    @Column(nullable = false)
    private Double amount;

    @Column(nullable = false)
    private String currency;

    @Enumerated(EnumType.STRING)
    private ExpenseType type;

    @Enumerated(EnumType.STRING)
    private ExpenseState state;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "opportunity_id")
    private Opportunity opportunity;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "lead_id")
    @SaveHistory(fields = {"id"},
            message = "{{id}}")
    private Lead lead;

    private Long creatorId;

    private Long acceptorId;

    private Long responsibleId;

    private String rejectionCause;
}
