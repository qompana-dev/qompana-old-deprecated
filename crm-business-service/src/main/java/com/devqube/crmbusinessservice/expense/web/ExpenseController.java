package com.devqube.crmbusinessservice.expense.web;

import com.devqube.crmbusinessservice.expense.exception.AssociationClientException;
import com.devqube.crmbusinessservice.expense.service.ExpenseService;
import com.devqube.crmbusinessservice.expense.web.dto.ExpenseDTO;
import com.devqube.crmbusinessservice.expense.web.dto.TotalCostDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.NotificationActionDto;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.search.CrmObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Slf4j
@RequestMapping("/expense")
public class ExpenseController implements ExpenseApi {

    private final ExpenseService expenseService;

    public ExpenseController(ExpenseService expenseService) {
        this.expenseService = expenseService;
    }

    @Override
    @PostMapping
    @AuthController(name = "save", actionFrontendId = "ExpenseListComponent.add")
    public ResponseEntity<ExpenseDTO> save(@RequestBody ExpenseDTO expenseDTO) {
        try {
            return new ResponseEntity<>(expenseService.save(expenseDTO), HttpStatus.CREATED);
        } catch (BadRequestException | KafkaSendMessageException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (AssociationClientException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @PutMapping("/{id}")
    @AuthController(name = "edit", actionFrontendId = "ExpenseListComponent.edit")
    public ResponseEntity<ExpenseDTO> update(@PathVariable @Valid Long id, @RequestBody @Valid ExpenseDTO expenseDTO) {
        try {
            return new ResponseEntity<>(expenseService.updateExpense(id, expenseDTO), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException | AssociationClientException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (KafkaSendMessageException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @GetMapping("/type/{type}/id/{id}")
    @AuthController(name = "getExpenses")
    public ResponseEntity<Page<ExpenseDTO>> getExpenses(CrmObject crmObject, Pageable pageable) throws BadRequestException {
        Page<ExpenseDTO> response = expenseService.getExpensesDtoList(crmObject, pageable);
        return ResponseEntity.ok(response);
    }

    @Override
    @PostMapping("/accept")
    public ResponseEntity<Void> acceptExpense(@Valid @RequestBody NotificationActionDto notificationActionDto) {
        try {
            expenseService.acceptExpense(notificationActionDto);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (KafkaSendMessageException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @PostMapping("/decline")
    public ResponseEntity<Void> declineExpense(@Valid @RequestBody NotificationActionDto notificationActionDto) {
        try {
            expenseService.declineExpense(notificationActionDto);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (KafkaSendMessageException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @DeleteMapping("/id/{id}/activity")
    @AuthController(actionFrontendId = "ExpenseListComponent.pin")
    public ResponseEntity<ExpenseDTO> removeActivity(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<>(expenseService.removeExpenseActivityAssociation(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @GetMapping("/{id}")
    @AuthController(name = "getExpense")
    public ResponseEntity<ExpenseDTO> getExpense(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<>(expenseService.getExpense(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @GetMapping("cost/type/{type}/id/{id}")
    @AuthController(name = "getTotalCost")
    public ResponseEntity<TotalCostDTO> getTotalCost(CrmObject crmObject, String currency) {
        try {
            return new ResponseEntity<>(expenseService.getTotalCost(crmObject, currency), HttpStatus.OK);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
