package com.devqube.crmbusinessservice.expense.service;

import com.devqube.crmbusinessservice.expense.model.Expense;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmshared.kafka.util.KafkaMessageDataBuilder;
import com.devqube.crmshared.notification.NotificationMsgType;
import org.springframework.stereotype.Service;

@Service
public class ExpenseNotificationService {
    private final KafkaService kafkaService;

    public ExpenseNotificationService(KafkaService kafkaService) {
        this.kafkaService = kafkaService;
    }

    void sendAcceptanceNotification(Expense expense, String userName) throws KafkaSendMessageException {
        kafkaService.addNotification(expense.getAcceptorId(), NotificationMsgType.EXPENSE_ACCEPTANCE,
                KafkaMessageDataBuilder.builder()
                        .add("id", "" + expense.getId())
                        .add("user", userName)
                        .add("expenseName", expense.getName())
                        .build());
    }

    void sendNotification(Expense expense, NotificationMsgType type, String rejectionCause) throws KafkaSendMessageException {
        kafkaService.addNotification(expense.getCreatorId(), type,
                KafkaMessageDataBuilder.builder()
                        .add("expenseName", expense.getName())
                        .add("rejectionCause", rejectionCause)
                        .build());
    }
}
