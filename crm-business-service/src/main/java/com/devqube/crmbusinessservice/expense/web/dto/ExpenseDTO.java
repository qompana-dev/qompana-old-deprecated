package com.devqube.crmbusinessservice.expense.web.dto;

import com.devqube.crmbusinessservice.expense.model.ExpenseState;
import com.devqube.crmbusinessservice.expense.model.ExpenseType;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@AuthFilterEnabled
public class ExpenseDTO {

    @AuthFields(list = {
            @AuthField(controller = "save"),
            @AuthField(controller = "getExpenses"),
            @AuthField(controller = "getExpense"),
            @AuthField(controller = "edit"),
    })
    private Long id;
    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "ExpenseAddComponent.name", controller = "save"),
            @AuthField(frontendId = "ExpenseListComponent.name", controller = "getExpenses"),
            @AuthField(frontendId = "ExpenseEditComponent.name", controller = "getExpense"),
            @AuthField(frontendId = "ExpenseEditComponent.name", controller = "edit"),
    })
    private String name;

    @AuthFields(list = {
            @AuthField(frontendId = "ExpenseAddComponent.description", controller = "save"),
            @AuthField(frontendId = "ExpenseListComponent.description", controller = "getExpenses"),
            @AuthField(frontendId = "ExpenseEditComponent.description", controller = "getExpense"),
            @AuthField(frontendId = "ExpenseEditComponent.description", controller = "edit"),
    })
    private String description;

    @AuthFields(list = {
            @AuthField(frontendId = "ExpenseAddComponent.date", controller = "save"),
            @AuthField(frontendId = "ExpenseListComponent.date", controller = "getExpenses"),
            @AuthField(frontendId = "ExpenseEditComponent.date", controller = "getExpense"),
            @AuthField(frontendId = "ExpenseEditComponent.date", controller = "edit"),
    })
    private LocalDate date;

    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "ExpenseAddComponent.amount", controller = "save"),
            @AuthField(frontendId = "ExpenseListComponent.amount", controller = "getExpenses"),
            @AuthField(frontendId = "ExpenseEditComponent.amount", controller = "getExpense"),
            @AuthField(frontendId = "ExpenseEditComponent.amount", controller = "edit"),
    })
    private Double amount;

    @NotNull
    @AuthFields(list = {
            @AuthField(frontendId = "ExpenseAddComponent.currency", controller = "save"),
            @AuthField(frontendId = "ExpenseListComponent.amount", controller = "getExpenses"),
            @AuthField(frontendId = "ExpenseEditComponent.amount", controller = "getExpense"),
            @AuthField(frontendId = "ExpenseEditComponent.amount", controller = "edit"),
    })
    private String currency;

    @AuthFields(list = {
            @AuthField(frontendId = "ExpenseAddComponent.type", controller = "save"),
            @AuthField(frontendId = "ExpenseListComponent.type", controller = "getExpenses"),
            @AuthField(frontendId = "ExpenseEditComponent.type", controller = "getExpense"),
            @AuthField(frontendId = "ExpenseEditComponent.type", controller = "edit"),
    })
    private ExpenseType type;

    @AuthFields(list = {
            @AuthField(controller = "save"),
            @AuthField(frontendId = "ExpenseListComponent.status", controller = "getExpenses"),
            @AuthField(controller = "getExpense"),
            @AuthField(controller = "edit"),
    })
    private ExpenseState state;

    @AuthFields(list = {
            @AuthField(controller = "save"),
            @AuthField(controller = "getExpenses"),
            @AuthField(controller = "getExpense"),
    })
    private Long opportunityId;

    @AuthFields(list = {
            @AuthField(controller = "save"),
            @AuthField(controller = "getExpenses"),
            @AuthField(controller = "getExpense")
    })
    private Long leadId;

    @AuthFields(list = {
            @AuthField(controller = "save"),
            @AuthField(controller = "getExpenses"),
            @AuthField(controller = "getExpense"),
    })
    private Long creatorId;

    @AuthFields(list = {
            @AuthField(controller = "save"),
            @AuthField(controller = "getExpenses"),
            @AuthField(controller = "getExpense"),
            @AuthField(controller = "edit"),
    })
    private Long acceptorId;

    @AuthFields(list = {
            @AuthField(frontendId = "ExpenseAddComponent.responsible", controller = "save"),
            @AuthField(frontendId = "ExpenseListComponent.responsible", controller = "getExpenses"),
            @AuthField(frontendId = "ExpenseEditComponent.responsible", controller = "getExpense"),
            @AuthField(frontendId = "ExpenseEditComponent.responsible", controller = "edit"),
    })
    private Long responsibleId;

    @AuthFields(list = {
            @AuthField(frontendId = "ExpenseAddComponent.activity", controller = "save"),
            @AuthField(frontendId = "ExpenseListComponent.pin", controller = "getExpenses"),
            @AuthField(frontendId = "ExpenseEditComponent.activity", controller = "getExpense"),
            @AuthField(frontendId = "ExpenseEditComponent.activity", controller = "edit"),
    })
    private Long activityId;
}
