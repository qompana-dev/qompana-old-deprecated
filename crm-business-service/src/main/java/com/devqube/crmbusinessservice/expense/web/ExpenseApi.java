package com.devqube.crmbusinessservice.expense.web;

import com.devqube.crmbusinessservice.expense.web.dto.ExpenseDTO;
import com.devqube.crmbusinessservice.expense.web.dto.TotalCostDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.NotificationActionDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.search.CrmObject;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Api(value = "Expense")
@Validated
public interface ExpenseApi {

    @ApiOperation(value = "save new expense", nickname = "saveExpense", notes = "save new expense")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "expense saved successfully", response = ExpenseDTO.class),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 404, message = "lead or opportunity not found")
    })
    ResponseEntity<ExpenseDTO> save(@Valid @ApiParam(value = "expense to save") ExpenseDTO expenseDTO);

    @ApiOperation(value = "modify expense", nickname = "updateExpense", notes = "modify expense")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "expense saved successfully", response = ExpenseDTO.class),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 404, message = "expense, lead or opportunity not found")
    })
    ResponseEntity<ExpenseDTO> update(@Valid @ApiParam("Expense id") Long id, @Valid @ApiParam(value = "expense to save") ExpenseDTO expenseDTO);


    @ApiOperation(value = "get list of expenses for given crmObject")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "expense saved successfully", response = ExpenseDTO.class),
            @ApiResponse(code = 404, message = "crmObject of given type and id not found")
    })
    ResponseEntity<Page<ExpenseDTO>> getExpenses(CrmObject crmObject, Pageable pageable) throws BadRequestException;

    @ApiOperation(value = "accept expense")
    @ApiResponse(code = 200, message = "expense saved successfully", response = ExpenseDTO.class)
    ResponseEntity<Void> acceptExpense(@ApiParam(value = "Notification id", required = true) @Valid NotificationActionDto notificationActionDto);

    @ApiOperation(value = "decline expense")
    @ApiResponse(code = 200, message = "expense saved successfully", response = ExpenseDTO.class)
    ResponseEntity<Void> declineExpense(@ApiParam(value = "Notification id", required = true) @Valid NotificationActionDto notificationActionDto);

    @ApiOperation(value = "remove activity associations")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "associations removed successfully", response = ExpenseDTO.class),
            @ApiResponse(code = 404, message = "expense not found")
    })
    ResponseEntity<ExpenseDTO> removeActivity(Long id);

    @ApiOperation(value = "get expense by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "expense found", response = ExpenseDTO.class),
            @ApiResponse(code = 404, message = "expense not found")
    })
    ResponseEntity<ExpenseDTO> getExpense(Long id);

    @ApiOperation(value = "Calculate the amount of costs for lead or opportunity")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Costs calculated successfully", response = Double.class),
            @ApiResponse(code = 404, message = "crmObject of given type and id not found")
    })
    ResponseEntity<TotalCostDTO> getTotalCost(CrmObject crmObject,
                                              @RequestParam(value = "currency", required = false) String currency);
}
