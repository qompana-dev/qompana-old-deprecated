package com.devqube.crmbusinessservice.expense.service;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.expense.ExpenseRepository;
import com.devqube.crmbusinessservice.expense.exception.AssociationClientException;
import com.devqube.crmbusinessservice.expense.model.Expense;
import com.devqube.crmbusinessservice.expense.model.ExpenseState;
import com.devqube.crmbusinessservice.expense.web.dto.ExpenseDTO;
import com.devqube.crmbusinessservice.expense.web.dto.TotalCostDTO;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.client.NotificationClient;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.client.UserNotificationDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.NotificationActionDto;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.currency.dto.CurrencyDto;
import com.devqube.crmshared.currency.dto.RateOfMainCurrencyDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.notification.NotificationMsgType;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.web.CurrentRequestUtil;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExpenseService {

    private final ExpenseRepository expenseRepository;
    private final LeadRepository leadRepository;
    private final OpportunityRepository opportunityRepository;
    private final ModelMapper modelMapper;
    private final AccessService accessService;
    private final ExpenseNotificationService expenseNotificationService;
    private final UserClient userClient;
    private final UserService userService;
    private final AssociationClient associationClient;
    private final NotificationClient notificationClient;

    public ExpenseService(ExpenseRepository expenseRepository, LeadRepository leadRepository,
                          OpportunityRepository opportunityRepository, ModelMapper modelMapper,
                          AccessService accessService, ExpenseNotificationService expenseNotificationService,
                          UserClient userClient, UserService userService, NotificationClient notificationClient,
                          AssociationClient associationClient) {
        this.expenseRepository = expenseRepository;
        this.leadRepository = leadRepository;
        this.opportunityRepository = opportunityRepository;
        this.modelMapper = modelMapper;
        this.accessService = accessService;
        this.expenseNotificationService = expenseNotificationService;
        this.userClient = userClient;
        this.userService = userService;
        this.notificationClient = notificationClient;
        this.associationClient = associationClient;
    }

    @Transactional(rollbackFor = {BadRequestException.class, KafkaSendMessageException.class, AssociationClientException.class})
    public ExpenseDTO save(ExpenseDTO expenseDTO) throws BadRequestException, EntityNotFoundException, KafkaSendMessageException, AssociationClientException {
        if (isNotCorrectlyAttached(expenseDTO)) {
            throw new BadRequestException("Expense must be attached only to one opportunity or one lead");
        }
        if (leadOrOpportunityDoesntExists(expenseDTO)) {
            throw new EntityNotFoundException("Lead or opportunity doesn't exist");
        }

        expenseDTO = accessService.assignAndVerify(expenseDTO);
        setStateAndAcceptorIdIfAcceptanceNeeded(expenseDTO);

        Expense savedExpense = expenseRepository.save(modelMapper.map(expenseDTO, Expense.class));

        sendAcceptanceNotification(savedExpense);
        saveActivityLink(savedExpense, expenseDTO);

        return toExpenseDto(savedExpense);
    }

    @Transactional(rollbackFor = {BadRequestException.class, KafkaSendMessageException.class, AssociationClientException.class})
    public ExpenseDTO updateExpense(Long id, ExpenseDTO expenseDTO) throws EntityNotFoundException, BadRequestException, KafkaSendMessageException, AssociationClientException {
        Expense expense = expenseRepository.findById(id).orElseThrow(EntityNotFoundException::new);

        if (expense.getState().equals(ExpenseState.TO_BE_ACCEPTED)) {
            throw new BadRequestException("incorrect expense state - must be different than " + ExpenseState.TO_BE_ACCEPTED.toString());
        }

        setStateAndAcceptorIdIfAcceptanceNeeded(expenseDTO);
        ExpenseDTO toSave = accessService.assignAndVerify(toExpenseDto(expense), expenseDTO);

        updateExpenseData(expense, toSave);
        Expense savedExpense = expenseRepository.save(expense);

        sendAcceptanceNotification(savedExpense);
        saveActivityLink(savedExpense, toSave);

        return toExpenseDto(savedExpense);
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class})
    public ExpenseDTO removeExpenseActivityAssociation(Long expenseId) throws EntityNotFoundException {
        removeActivityFromExpense(expenseId);
        return getExpense(expenseId);
    }

    private void updateExpenseData(Expense fromDb, ExpenseDTO toSave) {
        fromDb.setName(toSave.getName());
        fromDb.setDescription(toSave.getDescription());
        fromDb.setDate(toSave.getDate());
        fromDb.setAmount(toSave.getAmount());
        fromDb.setCurrency(toSave.getCurrency());
        fromDb.setType(toSave.getType());
        fromDb.setState(toSave.getState());
        fromDb.setAcceptorId(toSave.getAcceptorId());
        fromDb.setResponsibleId(toSave.getResponsibleId());
    }

    private void sendAcceptanceNotification(Expense expense) throws BadRequestException, KafkaSendMessageException {
        if (expense.getAcceptorId() != null) {
            expenseNotificationService.sendAcceptanceNotification(expense,
                    userService.getUserNameAndSurname(expense.getCreatorId()));
        }
    }

    private void saveActivityLink(Expense saved, ExpenseDTO dto) throws AssociationClientException {
        if (dto != null && saved != null) {
            try {
                removeActivityFromExpense(saved.getId());
                if (dto.getActivityId() != null) {
                    associationClient.createAssociation(AssociationDetailDto.builder()
                            .sourceType(CrmObjectType.expense)
                            .sourceId(saved.getId())
                            .destinationType(CrmObjectType.activity)
                            .destinationId(dto.getActivityId())
                            .build());
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new AssociationClientException();
            }
        }
    }

    public Page<ExpenseDTO> getExpensesDtoList(CrmObject crmObject, Pageable pageable) throws BadRequestException {
        Page<Expense> expensesList = this.getExpensesList(crmObject, pageable);
        return setAssociatedActivities(mapToExpenseDto(expensesList));
    }

    public ExpenseDTO getExpense(Long id) throws EntityNotFoundException {
        return toExpenseDto(expenseRepository.findById(id).orElseThrow(EntityNotFoundException::new));
    }

    public void acceptExpense(NotificationActionDto notificationActionDto) throws EntityNotFoundException, BadRequestException, KafkaSendMessageException {
        changeStateAfterAcceptance(notificationActionDto, ExpenseState.ACCEPTED, NotificationMsgType.EXPENSE_ACCEPTED);
    }

    public void declineExpense(NotificationActionDto notificationActionDto) throws EntityNotFoundException, BadRequestException, KafkaSendMessageException {
        if (notificationActionDto.getComment() == null) {
            notificationActionDto.setComment("");
        }
        changeStateAfterAcceptance(notificationActionDto, ExpenseState.REJECTED, NotificationMsgType.EXPENSE_DECLINED);
    }

    private void removeActivityFromExpense(Long expenseId) {
        associationClient.deleteAssociationsBySourceTypeAndSourceIdAndDestinationType(CrmObjectType.expense, expenseId, CrmObjectType.activity);
    }

    private void setStateAndAcceptorIdIfAcceptanceNeeded(ExpenseDTO expenseDTO) {
        String loggedInAccountEmail = CurrentRequestUtil.getLoggedInAccountEmail();
        expenseDTO.setCreatorId(userClient.getMyAccountId(loggedInAccountEmail));
        Long supervisorId = userClient.getSupervisorId(loggedInAccountEmail);
        Long threshold = Long.valueOf(userClient.getExpenseAcceptanceThreshold().getValue());
        if (expenseDTO.getAmount() >= threshold && supervisorId != null) {
            expenseDTO.setState(ExpenseState.TO_BE_ACCEPTED);
            expenseDTO.setAcceptorId(supervisorId);
        } else {
            expenseDTO.setState(ExpenseState.ACCEPTED);
        }
    }

    private boolean leadOrOpportunityDoesntExists(ExpenseDTO expenseDTO) {
        if (expenseDTO.getLeadId() != null) {
            return leadRepository.findById(expenseDTO.getLeadId()).isEmpty();
        } else if (expenseDTO.getOpportunityId() != null) {
            return opportunityRepository.findById(expenseDTO.getOpportunityId()).isEmpty();
        } else {
            return true;
        }
    }

    private boolean isNotCorrectlyAttached(ExpenseDTO expenseDTO) {
        return (expenseDTO.getLeadId() == null && expenseDTO.getOpportunityId() == null) ||
                (expenseDTO.getLeadId() != null && expenseDTO.getOpportunityId() != null);
    }

    private Page<Expense> getExpensesList(CrmObject crmObject, Pageable pageable) throws BadRequestException {
        if (CrmObjectType.lead.equals(crmObject.getType())) {
            return this.expenseRepository.findAllByLeadId(crmObject.getId(), pageable);
        } else if (CrmObjectType.opportunity.equals(crmObject.getType())) {
            return this.expenseRepository.findAllByOpportunityId(crmObject.getId(), pageable);
        } else {
            throw new BadRequestException("Expense must be attached only to one opportunity or one lead");
        }
    }

    private Page<ExpenseDTO> setAssociatedActivities(Page<ExpenseDTO> expenses) {
        List<Long> expenseIds = expenses.getContent().stream().map(ExpenseDTO::getId).collect(Collectors.toList());
        try {
            Map<Long, AssociationDto> map = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceIds(CrmObjectType.expense, CrmObjectType.activity, expenseIds)
                    .stream().collect(Collectors.toMap(AssociationDto::getSourceId, c -> c));
            for (int i = 0; i < expenses.getContent().size(); i++) {
                if (map.get(expenses.getContent().get(i).getId()) != null) {
                    Long activityId = map.get(expenses.getContent().get(i).getId()).getDestinationId();
                    expenses.getContent().get(i).setActivityId(activityId);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return expenses;
    }

    private Page<ExpenseDTO> mapToExpenseDto(Page<Expense> expenses) {
        return expenses.map(entity -> modelMapper.map(entity, ExpenseDTO.class));
    }

    private ExpenseDTO toExpenseDto(Expense expense) {
        ExpenseDTO result = modelMapper.map(expense, ExpenseDTO.class);
        return setActivityIdAssociation(result);
    }

    private ExpenseDTO setActivityIdAssociation(ExpenseDTO dto) {
        if (dto == null) {
            return dto;
        }
        try {
            associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(CrmObjectType.expense, CrmObjectType.activity, dto.getId())
                    .stream().findFirst().ifPresent(associationDto -> dto.setActivityId(associationDto.getDestinationId()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }

    private void validateRequestAndThrowExceptionIfNotValid(Expense expense) throws BadRequestException {
        Long loggedAccountId = userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail());
        if (!loggedAccountId.equals(expense.getAcceptorId())) {
            throw new BadRequestException("User trying to accept expense is not acceptor of the expense");
        }
    }

    private String getExpenseId(Long userNotificationId) throws EntityNotFoundException {
        return notificationClient.getNotificationById(userNotificationId)
                .getMessageParams()
                .stream()
                .filter(e -> e.getKey().equals("id"))
                .map(UserNotificationDTO.NotificationMessageParam::getValue)
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    private void changeStateAfterAcceptance(NotificationActionDto notificationActionDto, ExpenseState state, NotificationMsgType msgType) throws EntityNotFoundException, BadRequestException, KafkaSendMessageException {
        String expenseId = getExpenseId(notificationActionDto.getUserNotificationId());
        Expense expense = expenseRepository.findById(Long.valueOf(expenseId)).orElseThrow(EntityNotFoundException::new);
        validateRequestAndThrowExceptionIfNotValid(expense);
        expense.setAcceptorId(null);
        expense.setState(state);
        expense.setRejectionCause(notificationActionDto.getComment());
        expenseRepository.save(expense);
        expenseNotificationService.sendNotification(expense, msgType, expense.getRejectionCause());
        notificationClient.deleteMessage(new com.devqube.crmshared.notification.NotificationActionDto(notificationActionDto.getUserNotificationId()));
    }

    public TotalCostDTO getTotalCost(CrmObject crmObject, String currency) throws BadRequestException {
        CurrencyDto currencies = userClient.getCurrencies();
        if (CrmObjectType.lead.equals(crmObject.getType())) {
            List<Expense> allByLeadId = this.expenseRepository.findAllByLeadId(crmObject.getId());
            return calculateCost(allByLeadId, currencies.getSystemCurrency(), currencies.getRatesOfMainCurrency(), currency);
        } else if (CrmObjectType.opportunity.equals(crmObject.getType())) {
            List<Expense> allByOpportunityId = this.expenseRepository.findAllByOpportunityId(crmObject.getId());
            return calculateCost(allByOpportunityId, currencies.getSystemCurrency(), currencies.getRatesOfMainCurrency(), currency);
        } else {
            throw new BadRequestException("Expense must be attached only to one opportunity or one lead");
        }
    }
    public Double getTotalCostInCurrency(TotalCostDTO totalCost, String currency) throws BadRequestException {
        Optional<RateOfMainCurrencyDto> first = totalCost.getRateOfMainCurrency().stream().filter(rate -> rate.getCurrency().equals(currency)).findFirst();
        if (first.isPresent()){
            return totalCost.getTotalCost()/first.get().getRate();
        }else {
            throw new BadRequestException("Can't convert amount to " + currency );
        }
    }
    private TotalCostDTO calculateCost(List<Expense> expenses, String systemCurrency,
                                       List<RateOfMainCurrencyDto> ratesOfMainCurrency,
                                       String currencyToConvert) {
        Map<String, Double> map = ratesOfMainCurrency.stream()
                .collect(Collectors.toMap(RateOfMainCurrencyDto::getCurrency, RateOfMainCurrencyDto::getRate));
        map.put(systemCurrency, 1D);
        Map<String, Double> costInCurrencies = new HashMap<>();
        for (Expense expense : expenses) {
            String key = expense.getCurrency();
            double price = costInCurrencies.containsKey(key) ? costInCurrencies.get(key) : 0;
            price += expense.getAmount();
            costInCurrencies.put(key, price);
        }
        Double collect = expenses.stream().map(expense -> expense.getAmount() * map.get(expense.getCurrency()))
                .mapToDouble(Double::doubleValue).sum();
        TotalCostDTO mainTotalCostDTO = new TotalCostDTO(collect, systemCurrency, costInCurrencies, ratesOfMainCurrency);
        if (currencyToConvert== null || Objects.equals(currencyToConvert, systemCurrency)){
            return mainTotalCostDTO;
        }else {
            try {
                Double totalCostInCurrency = getTotalCostInCurrency(mainTotalCostDTO, currencyToConvert);
                return new  TotalCostDTO(totalCostInCurrency, currencyToConvert, costInCurrencies, ratesOfMainCurrency);
            } catch (BadRequestException e) {
                return mainTotalCostDTO;
            }
        }
    }
}