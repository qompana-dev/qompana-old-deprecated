package com.devqube.crmbusinessservice.expense.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import com.devqube.crmshared.currency.dto.RateOfMainCurrencyDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@AuthFilterEnabled
public class TotalCostDTO {
    @AuthFields(list = {@AuthField(controller = "getTotalCost")})
    private Double totalCost;

    @AuthFields(list = {@AuthField(controller = "getTotalCost")})
    private String mainCurrency;

    @AuthFields(list = {@AuthField(controller = "getTotalCost")})
    private Map<String, Double> costInCurrencies;

    @AuthFields(list = {@AuthField(controller = "getTotalCost")})
    private List<RateOfMainCurrencyDto> rateOfMainCurrency;
}
