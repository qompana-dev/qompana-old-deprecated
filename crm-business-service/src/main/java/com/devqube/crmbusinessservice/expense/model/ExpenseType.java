package com.devqube.crmbusinessservice.expense.model;

public enum ExpenseType {
    INVOICE, RECEIPT, DELEGATION, OTHER
}
