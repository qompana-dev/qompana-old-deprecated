package com.devqube.crmbusinessservice.expense.model;

public enum ExpenseState {
    TO_BE_ACCEPTED, ACCEPTED, REJECTED
}
