package com.devqube.crmbusinessservice.expense;

import com.devqube.crmbusinessservice.expense.model.Expense;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExpenseRepository extends JpaRepository<Expense, Long> {
    Page<Expense> findAllByOpportunityId(Long id, Pageable pageable);

    List<Expense> findAllByOpportunityId(Long id);

    Page<Expense> findAllByLeadId(Long id, Pageable pageable);

    List<Expense> findAllByLeadId(Long id);
}
