package com.devqube.crmbusinessservice.leadopportunity.lead.web;

import com.devqube.crmbusinessservice.leadopportunity.lead.LeadConversionService;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadService;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.*;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.search.CustomCrmObject;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class LeadController implements LeadsApi {

    private final LeadService leadService;

    @Autowired
    private LeadConversionService leadConversionService;

    public LeadController(LeadService leadService) {
        this.leadService = leadService;
    }

    @Override
    @AuthController(name = "getLeads")
    public ResponseEntity<Page> getLeads(String loggedAccountEmail,
                                         @Valid Pageable pageable,
                                         @Valid Boolean today,
                                         @Valid Boolean edited,
                                         @Valid Boolean viewed,
                                         @Valid Boolean forApproving,
                                         @Valid Lead.CloseResult closeResult,
                                         @Valid Boolean converted,
                                         @Valid Boolean opened,
                                         @Valid String search) {
        try {
            if (today != null && today) {
                return new ResponseEntity<>(leadService.findAllCreatedToday(loggedAccountEmail, search, pageable), HttpStatus.OK);
            } else if (edited != null && edited) {
                return new ResponseEntity<>(leadService.findAllRecentlyEdited(loggedAccountEmail, search, pageable), HttpStatus.OK);
            } else if (viewed != null && viewed) {
                return new ResponseEntity<>(leadService.findAllRecentlyViewed(loggedAccountEmail, search, pageable), HttpStatus.OK);
            } else if (forApproving != null && forApproving) {
                return new ResponseEntity<>(leadService.findAllForApproving(loggedAccountEmail, search, pageable), HttpStatus.OK);
            } else if (closeResult != null) {
                return new ResponseEntity<>(leadService.findAllClosed(loggedAccountEmail, search, pageable, closeResult), HttpStatus.OK);
            } else if (converted != null && converted) {
                return new ResponseEntity<>(leadService.findAllConverted(loggedAccountEmail, search, pageable), HttpStatus.OK);
            } else if (opened != null && opened) {
                return new ResponseEntity<>(leadService.findOpened(loggedAccountEmail, search, pageable), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(leadService.findAll(loggedAccountEmail, search, pageable), HttpStatus.OK);
            }
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<LeadSearchVariantsDtoOut>> getSearchVariants(String loggedAccountEmail, @NotNull @Valid String toSearch) {
        try {
            return new ResponseEntity<>(leadService.findAllSearchVariants(loggedAccountEmail, toSearch), HttpStatus.OK);
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override // todo: permission
    public ResponseEntity<Page> getLeadsForMobile(String loggedAccountEmail, @Valid Pageable pageable, @Valid Boolean today, @Valid Boolean edited, @Valid Boolean viewed, @Valid Boolean forApproving, @Valid Lead.CloseResult closeResult, @Valid Boolean converted, @Valid String search) {
        try {
            if (today != null && today) {
                return new ResponseEntity<>(leadService.findAllCreatedTodayForMobile(loggedAccountEmail, pageable, search), HttpStatus.OK);
            } else if (edited != null && edited) {
                return new ResponseEntity<>(leadService.findAllRecentlyEditedForMobile(loggedAccountEmail, pageable, search), HttpStatus.OK);
            } else if (viewed != null && viewed) {
                return new ResponseEntity<>(leadService.findAllRecentlyViewedForMobile(loggedAccountEmail, pageable, search), HttpStatus.OK);
            } else if (forApproving != null && forApproving) {
                return new ResponseEntity<>(leadService.findAllForApprovingForMobile(loggedAccountEmail, pageable, search), HttpStatus.OK);
            } else if (closeResult != null) {
                return new ResponseEntity<>(leadService.findAllClosedForMobile(loggedAccountEmail, pageable, closeResult, search), HttpStatus.OK);
            } else if (converted != null && converted) {
                return new ResponseEntity<>(leadService.findAllConvertedForMobile(loggedAccountEmail, pageable, search), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(leadService.findAllForMobile(loggedAccountEmail, pageable, search), HttpStatus.OK);
            }
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getLeads")
    public ResponseEntity<Page> getAllConvertedLeads(String loggedAccountEmail, @Valid Pageable pageable) {
        try {
            return new ResponseEntity<>(leadConversionService.findAllConvertedLeads(loggedAccountEmail, pageable), HttpStatus.OK);
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getLead")
    public ResponseEntity<LeadDetailsDTO> getLead(Long id) {
        try {
            return new ResponseEntity<>(leadService.getLeadById(id), HttpStatus.OK);
        } catch (EntityNotFoundException | BadRequestException | PermissionDeniedException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "getLeadForEdit")
    public ResponseEntity<LeadSaveDTO> getLeadForEdit(Long id) {
        try {
            return new ResponseEntity<>(leadService.getLeadForEditById(id), HttpStatus.OK);
        } catch (EntityNotFoundException | BadRequestException | PermissionDeniedException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "createLead", actionFrontendId = "LeadCreateComponent.save")
    public ResponseEntity<LeadSaveDTO> createLead(String loggedAccountEmail, @Valid LeadSaveDTO leadSaveDTO) {
        try {
            return new ResponseEntity<>(leadService.createLead(loggedAccountEmail, leadSaveDTO), HttpStatus.CREATED);
        } catch (FlowableObjectNotFoundException | BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "modifyLead", actionFrontendId = "LeadShared.edit")
    public ResponseEntity<LeadSaveDTO> modifyLead(String loggedAccountEmail, Long id, @Valid LeadSaveDTO leadSaveDTO) {
        try {
            return new ResponseEntity<>(leadService.modifyLead(loggedAccountEmail, id, leadSaveDTO), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException | PermissionDeniedException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(actionFrontendId = "LeadShared.delete")
    public ResponseEntity<Void> deleteLead(Long id) {
        try {
            leadService.deleteLead(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException | BadRequestException | PermissionDeniedException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(actionFrontendId = "LeadShared.delete")
    public ResponseEntity<Void> deleteLeads(@Valid List<Long> ids) {
        try {
            leadService.deleteLeads(ids);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException | BadRequestException | PermissionDeniedException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<List<CrmObject>> getLeadsForSearch(@NotNull @Valid String term) {
        return new ResponseEntity<>(leadService.findAllContaining(term)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.lead,
                        String.format("%s %s", Strings.nullToEmpty(e.getFirstName()), Strings.nullToEmpty(e.getLastName())), e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CustomCrmObject>> getLeadsAsCustomCrmObject(@Valid List<Long> ids) {
        return new ResponseEntity<>(leadService.findByIds(ids)
                .stream()
                .map(e -> new CustomCrmObject(e.getId(), CrmObjectType.lead,
                        String.format("%s %s", Strings.nullToEmpty(e.getFirstName()), Strings.nullToEmpty(e.getLastName())),
                        e.getCreated(),
                        e.getCompany() != null ? e.getCompany().getCompanyName() : null))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getLeadsAsCrmObject(@Valid List<Long> ids) {
        return new ResponseEntity<>(leadService.findByIds(ids)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.lead,
                        String.format("%s %s", Strings.nullToEmpty(e.getFirstName()), Strings.nullToEmpty(e.getLastName())),
                        e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getLeadsForMainSearch(String loggedAccountEmail, @NotNull @Valid String term) {
        try {
            return new ResponseEntity<>(leadService.findAllAvailableForUserAndContaining(loggedAccountEmail, term)
                    .stream()
                    .map(e -> new CrmObject(e.getId(), CrmObjectType.lead,
                            String.format("%s %s", Strings.nullToEmpty(e.getFirstName()), Strings.nullToEmpty(e.getLastName())),
                            e.getCreated()))
                    .collect(Collectors.toList()),
                    HttpStatus.OK);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<Long>> getAllLeadWithAccess(String loggedAccountEmail) {
        try {
            return new ResponseEntity<>(this.leadService.getAllLeadIdWithAccess(loggedAccountEmail), HttpStatus.OK);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "convertToOpportunity", actionFrontendId = "LeadConversionComponent.convert")
    public ResponseEntity<ConvertedOpportunityDto> convertToOpportunity(String loggedAccountEmail, Long leadId, @Valid LeadConversionDto leadConversionDto) {
        try {
            ConvertedOpportunityDto opportunityDto = this.leadConversionService.convertToOpportunity(loggedAccountEmail, leadId, leadConversionDto);
            return new ResponseEntity<>(opportunityDto, HttpStatus.OK);
        } catch (BadRequestException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @Override
    @AuthController(name = "convertToOpportunity", actionFrontendId = "LeadConversionComponent.convert")
    public ResponseEntity<CanConvertResponseDto> canConvertToOpportunity(String loggedAccountEmail, Long id) {
        try {
            return new ResponseEntity<>(leadConversionService.canConvertToOpportunity(loggedAccountEmail, id), HttpStatus.OK);
        } catch (BadRequestException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(actionFrontendId = "LeadShared.delete")
    public ResponseEntity<Void> closeLead(String loggedAccountEmail, Long id, Lead.CloseResult closeResult, Lead.RejectionReason rejectionReason) throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        leadService.closeLead(loggedAccountEmail, id, closeResult, rejectionReason);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> searchSimilarObjects(@Valid SearchSimilarObjectsDto searchSimilarObjectsDto) {
        return new ResponseEntity<>(leadService.searchSimilarObjects(searchSimilarObjectsDto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<LeadDetailsDTO> findLeadByOpportunityId(Long id) {
        try {
            return new ResponseEntity<>(leadService.findLeadByOpportunityId(id), HttpStatus.OK);
        } catch (EntityNotFoundException | BadRequestException | PermissionDeniedException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Void> changeLeadsKeeper(String loggedAccountEmail, @Valid LeadsKeeperDTO leadsKeeper) {
        try {
            leadService.changeLeadsKeeper(loggedAccountEmail, leadsKeeper);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
