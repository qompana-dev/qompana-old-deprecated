package com.devqube.crmbusinessservice.leadopportunity.opportunity.model;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.OpportunityProduct;
import com.devqube.crmshared.history.SaveCrmChangesListener;
import com.devqube.crmshared.history.annotation.SaveHistory;
import com.devqube.crmshared.history.annotation.SaveHistoryIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@EntityListeners(SaveCrmChangesListener.class)
public class Opportunity {
    @Id
    @SequenceGenerator(name = "opportunity_seq", sequenceName = "opportunity_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "opportunity_seq")
    @EqualsAndHashCode.Include
    @SaveHistoryIgnore
    private Long id;

    @EqualsAndHashCode.Include
    @Column(nullable = false)
    private String name;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "customer_id", nullable = false)
    @SaveHistory(fields = {"name"}, message = "{{name}}")
    private Customer customer;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "contact_id", nullable = false)
    @SaveHistory(fields = {"name", "surname"}, message = "{{name}} {{surname}}")
    private Contact contact;

    private Double amount;

    private Double estimatedAmount;
    
    private Double productAmount;

    private String currency;

    private Long probability;

    @NotNull
    private LocalDate createDate;

    private LocalDate finishDate;

    @NotNull
    @SaveHistoryIgnore
    private String processInstanceId;

    @NotNull
    @SaveHistoryIgnore
    private String processDefinitionId;

    private String description;

    private Long sourceOfAcquisition;
    private Long subSourceOfAcquisition;

    @JsonProperty("subjectOfInterest")
    @Column(name = "subject_of_interest_id")
    private Long subjectOfInterest;

    @Enumerated(EnumType.STRING)
    private FinishResult finishResult;

    private LocalDateTime closingDate;

    @Enumerated(EnumType.STRING)
    private RejectionReason rejectionReason;

    @SaveHistoryIgnore
    private Long creator;

    @Column(nullable = false)
    private Long ownerOneId;

    private Long ownerOnePercentage;

    private Long ownerTwoId;

    private Long ownerTwoPercentage;

    private Long ownerThreeId;

    private Long ownerThreePercentage;

    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("priority")
    private OpportunityPriority priority;

    @SaveHistoryIgnore
    private Boolean convertedFromLead = false;

    @SaveHistoryIgnore
    private Long acceptanceUserId;

    @CreationTimestamp
    @SaveHistoryIgnore
    private LocalDateTime created;

    @UpdateTimestamp
    @SaveHistoryIgnore
    private LocalDateTime updated;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "opportunity")
    private Set<LastViewedOpportunity> lastViewedOpportunities;

    @OneToMany(mappedBy = "opportunity")
    private List<OpportunityProduct> opportunityProducts;

    @Column(name = "actual_user_task")
    private String actualUserTask;

    @Column(name = "change_user_task_date")
    @SaveHistoryIgnore
    private LocalDateTime changeUserTaskDate = LocalDateTime.now();

    public enum OpportunityType {
        NEW_BUSINESS, EXISTING_BUSINESS
    }

    public enum OpportunitySourceOfAcquisition {
        ADVERTISEMENT, EVENT, EMPLOYEE_REFERENCES, PARTNER, GOOGLE_AD_WORDS, OTHER
    }

    public enum RejectionReason {
        NONE, LOST, NO_BUDGET, NO_DECISION, OTHER
    }

    public enum FinishResult {
        SUCCESS, ERROR
    }
}
