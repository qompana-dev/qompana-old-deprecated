package com.devqube.crmbusinessservice.leadopportunity.company;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Accessors(chain = true)
public class Company {
    @Id
    @SequenceGenerator(name="company_seq", sequenceName="company_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="company_seq")
    private Long id;
    private String nip;
    private String companyName;
    private String wwwAddress;
    private String streetAndNumber;
    private String city;
    private String postalCode;
    private String province;
    private String country;
    private Long industry;
}
