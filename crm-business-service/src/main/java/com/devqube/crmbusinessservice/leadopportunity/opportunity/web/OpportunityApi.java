package com.devqube.crmbusinessservice.leadopportunity.opportunity.web;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadSearchVariantsDtoOut;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.*;
import com.devqube.crmshared.search.CrmObjectType;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "Opportunity")
public interface OpportunityApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "get opportunities list for zapier", nickname = "getOpportunitiesForZapier", notes = "get list of all opportunities", response = OpportunityListDTO[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "opportunities list retrieved successfully", response = OpportunityListDTO[].class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/zapier/opportunity",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<OpportunityListDTO>> getOpportunitiesForZapier(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable, @ApiParam(value = "only finished") @Valid @RequestParam(value = "finished", required = false) Boolean finished) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get opportunities", nickname = "getOpportunities", notes = "get list of all opportunities", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "opportunities list retrieved successfully", response = org.springframework.data.domain.Page.class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/opportunity",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<org.springframework.data.domain.Page> getOpportunities(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                                                                  @ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable,
                                                                                  @ApiParam(value = "only todays opportunities") @Valid @RequestParam(value = "today", required = false) Boolean today,
                                                                                  @ApiParam(value = "only last edited opportunities") @Valid @RequestParam(value = "edited", required = false) Boolean edited,
                                                                                  @ApiParam(value = "only last viewed opportunities") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed,
                                                                                  @ApiParam(value = "only opportunities for approving") @Valid @RequestParam(value = "for-approving", required = false) Boolean forApproving,
                                                                                  @ApiParam(value = "only closed opportunities by reason") @Valid @RequestParam(value = "finishResult", required = false) Opportunity.FinishResult finishResult,
                                                                                  @ApiParam(value = "only opened") @Valid @RequestParam(value = "opened", required = false) Boolean opened,
                                                                                  @ApiParam(value = "only opened") @Valid @RequestParam(value = "search", required = false) String search,
                                                                                  @ApiParam(value = "only opened") @Valid @RequestParam(value = "customer", required = false) String customer,
                                                                                  @ApiParam(value = "only opened") @Valid @RequestParam(value = "contact", required = false) String contact) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get list for search", nickname = "getSearchVariants", notes = "Method used to get search variants", response = LeadSearchVariantsDtoOut.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all search variants", response = OpportunitySearchVariantsDtoOut.class, responseContainer = "List")})
    @RequestMapping(value = "/opportunity/search-variants",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<OpportunitySearchVariantsDtoOut>> getSearchVariants(
            @ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
            @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "search") String toSearch) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get opportunities for mobile", nickname = "getOpportunitiesForMobile", notes = "get list of all opportunities for mobile", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "opportunities list retrieved successfully", response = org.springframework.data.domain.Page.class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/mobile/opportunity",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<org.springframework.data.domain.Page> getOpportunitiesForMobile(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable, @ApiParam(value = "only todays opportunities") @Valid @RequestParam(value = "today", required = false) Boolean today, @ApiParam(value = "only last edited opportunities") @Valid @RequestParam(value = "edited", required = false) Boolean edited, @ApiParam(value = "only last viewed opportunities") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed, @ApiParam(value = "search") @Valid @RequestParam(value = "search", required = false) String search) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get opportunities for mobile for customer or contact", nickname = "getOpportunitiesForMobileByConnectedObject", notes = "get list of all opportunities for mobile for customer or contact", response = MobileOpportunityListDto[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "opportunities list retrieved successfully", response = MobileOpportunityListDto[].class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/mobile/opportunity/{objectType}/{objectId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<MobileOpportunityListDto>> getOpportunitiesForMobileByConnectedObject(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "crm object type") @PathVariable(value = "objectType") CrmObjectType objectType, @ApiParam(value = "object id") @PathVariable(value = "objectId") Long objectId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "create opportunity", nickname = "createOpportunity", notes = "Method used to create opportunity", response = OpportunitySaveDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "added opportunity successfully", response = OpportunitySaveDTO.class),
            @ApiResponse(code = 400, message = "this process definition not exists")})
    @RequestMapping(value = "/opportunity",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<OpportunitySaveDTO> createOpportunity(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "Created opportunity object", required = true) @Valid @RequestBody OpportunitySaveDTO opportunitySaveDTO) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"processDefinitionId\" : \"processDefinitionId\",  \"amount\" : 5.962133916683182377482808078639209270477294921875,  \"contactId\" : 1,  \"ownerThreeId\" : 3,  \"probability\" : 5,  \"keeper\" : 2,  \"sourceOfAcquisition\" : \"ADVERTISEMENT\",  \"description\" : \"description\",  \"ownerTwoId\" : 9,  \"type\" : \"NEW_BUSINESS\",  \"name\" : \"name\",  \"customerId\" : 6,  \"currency\" : \"currency\",  \"finishDate\" : \"2000-01-23\",  \"id\" : 0,  \"ownerOneId\" : 7}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "delete opportunities", nickname = "deleteOpportunities", notes = "Method used to delete opportunities")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "deleted opportunities successfully"),
            @ApiResponse(code = 404, message = "one of opportunities not found")})
    @RequestMapping(value = "/opportunity",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteOpportunities(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "delete opportunity", nickname = "deleteOpportunity", notes = "Method used to delete opportunity")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "deleted opportunity successfully"),
            @ApiResponse(code = 404, message = "opportunity not found")})
    @RequestMapping(value = "/opportunity/{id}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteOpportunity(@ApiParam(value = "opportunity's id", required = true) @PathVariable("id") Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "copy opportunity", nickname = "copyOpportunity",
            notes = "Method is used to create new opportunity based on existing. " +
                    "Throws an exception if the opportunity isn't found or the business process can't be started.")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Opportunity is copied successfully."),
            @ApiResponse(code = 404, message = "Opportunity not found")})
    @RequestMapping(value = "/opportunity/copy/{id}",
            method = RequestMethod.POST)
    default ResponseEntity<OpportunitySaveDTO> copyOpportunity(@ApiParam(value = "", required = true)
                                                               @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                                               @ApiParam(value = "opportunity's id", required = true) @PathVariable("id") Long opportunityId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "get opportunity", nickname = "getOpportunity", notes = "get one opportunity", response = OpportunityDetailsDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "opportunity retrieved successfully", response = OpportunityDetailsDTO.class),
            @ApiResponse(code = 404, message = "opportunity not found")})
    @RequestMapping(value = "/opportunity/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<OpportunityDetailsDTO> getOpportunity(@ApiParam(value = "opportunity id", required = true) @PathVariable("id") Long id, @ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"processInstanceId\" : \"processInstanceId\",  \"amount\" : 5.63737665663332876420099637471139430999755859375,  \"creator\" : 2,  \"contactId\" : 1,  \"probability\" : 5,  \"created\" : \"created\",  \"sourceOfAcquisition\" : \"ADVERTISEMENT\",  \"description\" : \"description\",  \"textColor\" : \"textColor\",  \"bgColor\" : \"bgColor\",  \"name\" : \"name\",  \"customerId\" : 6,  \"acceptanceUserId\" : 7,  \"finishDate\" : \"finishDate\",  \"currency\" : \"currency\",  \"id\" : 0,  \"tasks\" : [ {    \"documentation\" : \"documentation\",    \"acceptanceGroup\" : \"acceptanceGroup\",    \"name\" : \"name\",    \"notificationUser\" : \"notificationUser\",    \"salesOpportunity\" : 0.80082819046101150206595775671303272247314453125,    \"properties\" : [ {      \"possibleValues\" : [ {        \"name\" : \"name\",        \"id\" : \"id\"      }, {        \"name\" : \"name\",        \"id\" : \"id\"      } ],      \"id\" : \"id\",      \"type\" : \"STRING\",      \"value\" : \"value\",      \"required\" : true    }, {      \"possibleValues\" : [ {        \"name\" : \"name\",        \"id\" : \"id\"      }, {        \"name\" : \"name\",        \"id\" : \"id\"      } ],      \"id\" : \"id\",      \"type\" : \"STRING\",      \"value\" : \"value\",      \"required\" : true    } ],    \"acceptanceUser\" : \"acceptanceUser\",    \"notificationGroup\" : \"notificationGroup\"  }, {    \"documentation\" : \"documentation\",    \"acceptanceGroup\" : \"acceptanceGroup\",    \"name\" : \"name\",    \"notificationUser\" : \"notificationUser\",    \"salesOpportunity\" : 0.80082819046101150206595775671303272247314453125,    \"properties\" : [ {      \"possibleValues\" : [ {        \"name\" : \"name\",        \"id\" : \"id\"      }, {        \"name\" : \"name\",        \"id\" : \"id\"      } ],      \"id\" : \"id\",      \"type\" : \"STRING\",      \"value\" : \"value\",      \"required\" : true    }, {      \"possibleValues\" : [ {        \"name\" : \"name\",        \"id\" : \"id\"      }, {        \"name\" : \"name\",        \"id\" : \"id\"      } ],      \"id\" : \"id\",      \"type\" : \"STRING\",      \"value\" : \"value\",      \"required\" : true    } ],    \"acceptanceUser\" : \"acceptanceUser\",    \"notificationGroup\" : \"notificationGroup\"  } ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get opportunity for edit", nickname = "getOpportunityForEdit", notes = "Method used to get opportunity for edit", response = OpportunitySaveDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "get opportunity (for edit view) successfully", response = OpportunitySaveDTO.class),
            @ApiResponse(code = 404, message = "this opportunity doesn't exists")})
    @RequestMapping(value = "/opportunity/for-edit/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<OpportunitySaveDTO> getOpportunityForEdit(@ApiParam(value = "opportunity id", required = true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"processDefinitionId\" : \"processDefinitionId\",  \"amount\" : 5.962133916683182377482808078639209270477294921875,  \"contactId\" : 1,  \"ownerThreeId\" : 3,  \"probability\" : 5,  \"keeper\" : 2,  \"sourceOfAcquisition\" : \"ADVERTISEMENT\",  \"description\" : \"description\",  \"ownerTwoId\" : 9,  \"type\" : \"NEW_BUSINESS\",  \"name\" : \"name\",  \"customerId\" : 6,  \"currency\" : \"currency\",  \"finishDate\" : \"2000-01-23\",  \"id\" : 0,  \"ownerOneId\" : 7}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "put opportunity", nickname = "modifyOpportunity", notes = "Method used to modify opportunity", response = OpportunitySaveDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "modified opportunity successfully", response = OpportunitySaveDTO.class),
            @ApiResponse(code = 400, message = "this process definition doesn't exists"),
            @ApiResponse(code = 404, message = "opportunity not found")})
    @RequestMapping(value = "/opportunity/{id}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<OpportunitySaveDTO> modifyOpportunity(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "opportunity's id", required = true) @PathVariable("id") Long id, @ApiParam(value = "Modified opportunity object", required = true) @Valid @RequestBody OpportunitySaveDTO opportunitySaveDTO, @ApiParam(value = "") @RequestHeader(value = "Auth-controller-type", required = false) String authControllerType) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"processDefinitionId\" : \"processDefinitionId\",  \"amount\" : 5.962133916683182377482808078639209270477294921875,  \"contactId\" : 1,  \"ownerThreeId\" : 3,  \"probability\" : 5,  \"keeper\" : 2,  \"ownerTwoPercentage\" : 4,  \"sourceOfAcquisition\" : \"ADVERTISEMENT\",  \"description\" : \"description\",  \"ownerTwoId\" : 9,  \"type\" : \"NEW_BUSINESS\",  \"ownerThreePercentage\" : 7,  \"name\" : \"name\",  \"customerId\" : 6,  \"currency\" : \"currency\",  \"finishDate\" : \"2000-01-23\",  \"ownerOnePercentage\" : 2,  \"id\" : 0,  \"ownerOneId\" : 7}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get opportunities list by ids", nickname = "getOpportunitiesAsCrmObject", notes = "Method used to get opportunities as crm objects by ids", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found opportunities", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/opportunity/crm-object",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmshared.search.CrmObject>> getOpportunitiesAsCrmObject(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get opportunities list by ids", nickname = "getOpportunitiesAsCustomCrmObject", notes = "Method used to get opportunities as crm objects by ids", response = com.devqube.crmshared.search.CustomCrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found opportunities", response = com.devqube.crmshared.search.CustomCrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/opportunity/custom-crm-object",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmshared.search.CustomCrmObject>> getOpportunitiesAsCustomCrmObject(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get opportunity list for main search", nickname = "getOpportunitiesForMainSearch", notes = "Get opportunity list for main search", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all opportunity names", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/opportunity/main-search",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmshared.search.CrmObject>> getOpportunitiesForMainSearch(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get opportunities list for search", nickname = "getOpportunitiesForSearch", notes = "Method used to get opportunity list", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all opportunity names", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/opportunity/search",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmshared.search.CrmObject>> getOpportunitiesForSearch(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get all opportunities with access for user", nickname = "getAllOpportunityWithAccess", notes = "Get all opportunities that current user has access to", response = Long.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of opportunities for specific user", response = Long.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Email not correct")})
    @RequestMapping(value = "/access/opportunity",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<Long>> getAllOpportunityWithAccess(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "0");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "finish opportunity", nickname = "finishOpportunity", notes = "Method used to finish opportunity")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "modified opportunity successfully"),
            @ApiResponse(code = 400, message = "incorrect form"),
            @ApiResponse(code = 404, message = "opportunity not found")})
    @RequestMapping(value = "/opportunity/{id}/finish",
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> finishOpportunity(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "opportunity id", required = true) @PathVariable("id") Long id, @ApiParam(value = "Modified opportunity object", required = true) @Valid @RequestBody OpportunityFinishDto opportunityFinishDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    @ApiOperation(value = "finish opportunities", nickname = "finishOpportunities", notes = "Method used to finish opportunities")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "opportunities modified successfully"),
            @ApiResponse(code = 400, message = "incorrect form")})
    @RequestMapping(value = "/opportunity/finish",
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> finishOpportunities(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                                     @ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids,
                                                     @Valid @RequestBody OpportunityFinishDto opportunityFinishDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
