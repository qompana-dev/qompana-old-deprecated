package com.devqube.crmbusinessservice.leadopportunity.processEngine.service;

import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Process;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadDetailsDTO;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.OpportunityDetailsDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.client.NotificationClient;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.client.UserNotificationDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.NotificationActionDto;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionListDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.write.ProcessEngineForLead;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.write.ProcessEngineForOpportunity;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.write.ProcessEngineService;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.write.ProcessEngineWriteService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.IncorrectNotificationTypeException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

@Service
public class ProcessEngineProxyService {

    private final ProcessEngineReadService readService;
    private final ProcessEngineWriteService writeService;
    private final NotificationClient notificationClient;
    private final ProcessEngineForLead processEngineForLead;
    private final ProcessEngineForOpportunity processEngineForOpportunity;

    public ProcessEngineProxyService(ProcessEngineReadService readService, ProcessEngineWriteService writeService, ProcessEngineForLead processEngineForLead,
                                     NotificationClient notificationClient, ProcessEngineForOpportunity processEngineForOpportunity) {
        this.readService = readService;
        this.writeService = writeService;
        this.processEngineForLead = processEngineForLead;
        this.notificationClient = notificationClient;
        this.processEngineForOpportunity = processEngineForOpportunity;
    }

    public ProcessDefinitionDTO deployProcessDefinition(InputStream inputStream) throws IOException, BadRequestException {
        return this.writeService.deployProcessDefinition(inputStream);
    }

    public Resource getBpmnFile(String processDefinitionId) throws IOException {
        return this.readService.getBpmnFile(processDefinitionId);
    }

    public List<ProcessDefinitionDTO> getProcessDefinitions(String objectType) {
        return this.readService.getProcessDefinitionsDTO(objectType);
    }

    public List<ProcessDefinitionDTO> getProcessDefinitionsWithoutRoleCheck(String objectType) {
        return this.readService.getProcessDefinitionsDTOWithoutRoleCheck(objectType);
    }

    public void goBackToPreviousTask(String processInstanceId, Integer numberOfSteps) throws BadRequestException, EntityNotFoundException {
        checkIfDuringAcceptanceAndThrowExceptionIfYes(processInstanceId);
        Process.ObjectTypeEnum objectType = readService.getObjectTypeByProcessInstanceId(processInstanceId);
        getServiceByObjectType(objectType).goBackToPreviousTask(processInstanceId, numberOfSteps);
    }

    public LeadDetailsDTO.Task getPreviousTask(String processInstanceId, String taskDefinitionKey) throws EntityNotFoundException {
        return readService.getPreviousTask(processInstanceId, taskDefinitionKey);
    }

    public LeadDetailsDTO.Task getFutureTask(String processInstanceId, String taskDefinitionKey) throws EntityNotFoundException {
        return readService.getFutureTask(processInstanceId, taskDefinitionKey);
    }

    public List<ProcessDefinitionListDTO> getProcessDefinitionsList(String type) {
        return readService.getProcessDefinitionsList(type);
    }

    public ProcessDefinitionDTO getProcessDefinition(String processDefinitionId) throws EntityNotFoundException {
        return readService.getProcessDefinitionById(processDefinitionId);
    }

    public void deleteProcessDefinition(String id) throws BadRequestException, EntityNotFoundException {
        writeService.deleteProcessDefinition(id);
    }

    public void postFormProperties(String processInstanceId, Map<String, Object> requestBody) throws EntityNotFoundException, BadRequestException {
        checkIfDuringAcceptanceAndThrowExceptionIfYes(processInstanceId);
        writeService.postFormProperties(processInstanceId, requestBody);
    }

    public boolean completeTask(String processInstanceId) throws BadRequestException, KafkaSendMessageException, EntityNotFoundException, IncorrectNotificationTypeException {
        Process.ObjectTypeEnum objectType = readService.getObjectTypeByProcessInstanceId(processInstanceId);
        return getServiceByObjectType(objectType).completeTask(processInstanceId);
    }

    public void acceptTask(NotificationActionDto notificationActionDto) throws EntityNotFoundException, KafkaSendMessageException, IncorrectNotificationTypeException, BadRequestException {
        String processInstanceId = getProcessInstanceIdFromNotification(notificationActionDto.getUserNotificationId());
        Process.ObjectTypeEnum objectType = readService.getObjectTypeByProcessInstanceId(processInstanceId);
        getServiceByObjectType(objectType).acceptTask(notificationActionDto, processInstanceId);
    }

    public void declineTask(NotificationActionDto notificationActionDto) throws EntityNotFoundException, KafkaSendMessageException, IncorrectNotificationTypeException, BadRequestException {
        String processInstanceId = getProcessInstanceIdFromNotification(notificationActionDto.getUserNotificationId());
        Process.ObjectTypeEnum objectType = readService.getObjectTypeByProcessInstanceId(processInstanceId);
        getServiceByObjectType(objectType).declineTask(notificationActionDto, processInstanceId);
    }

    private void checkIfDuringAcceptanceAndThrowExceptionIfYes(String processInstanceId) throws BadRequestException, EntityNotFoundException {
        Process.ObjectTypeEnum objectType = readService.getObjectTypeByProcessInstanceId(processInstanceId);
        getServiceByObjectType(objectType).checkIfBeingAcceptedAndThrowExceptionIfYes(processInstanceId);
    }

    private String getProcessInstanceIdFromNotification(Long userNotificationId) throws EntityNotFoundException {
        return notificationClient.getNotificationById(userNotificationId)
                .getMessageParams()
                .stream()
                .filter(e -> e.getKey().equals("processInstanceId"))
                .map(UserNotificationDTO.NotificationMessageParam::getValue)
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    private ProcessEngineService getServiceByObjectType(Process.ObjectTypeEnum objectType) {
        if (objectType.equals(Process.ObjectTypeEnum.LEAD)) {
            return this.processEngineForLead;
        } else {
            return this.processEngineForOpportunity;
        }
    }

    public List<OpportunityDetailsDTO> getProcessDefinitionOpportunities(Pageable pageable, String loggedAccountEmail, String processDefinitionId,
                                                                         String taskDefinitionKey, Boolean includeOldProcessVersionResult) throws BadRequestException {
        return processEngineForOpportunity.findAllAvailableForUserAndProcessDefinitionId(pageable, loggedAccountEmail,
                processDefinitionId,taskDefinitionKey,includeOldProcessVersionResult);
    }
}
