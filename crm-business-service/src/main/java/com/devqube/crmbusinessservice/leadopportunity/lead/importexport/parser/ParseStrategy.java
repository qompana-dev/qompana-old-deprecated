package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser;

import java.io.InputStream;
import java.util.List;

public interface ParseStrategy {

    List<ParsedLead> parseFile(InputStream file);

    ParseStrategyType getStrategyType();

}