package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.LeadPriority;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessInstanceDetailsDTO;
import com.devqube.crmbusinessservice.phoneNumber.PhoneNumberMapper;
import com.devqube.crmbusinessservice.phoneNumber.web.dto.PhoneNumberDTO;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class LeadDetailsDTO implements ProcessInstanceDetailsDTO {
    @AuthField(controller = "getLead")
    private Long id;
    @AuthField(controller = "getLead")
    private String processInstanceId;
    @AuthField(frontendId = "LeadDetailsComponent.mainInfo", controller = "getLead")
    private String firstName;
    @AuthField(frontendId = "LeadDetailsComponent.mainInfo", controller = "getLead")
    private String lastName;
    @AuthField(frontendId = "LeadDetailsComponent.mainInfo", controller = "getLead")
    private Long position;
    @AuthField(frontendId = "LeadDetailsComponent.company", controller = "getLead")
    private String companyName;
    @AuthField(frontendId = "LeadDetailsComponent.phone", controller = "getLead")
    private String phoneNumber;
    @AuthField(frontendId = "LeadDetailsComponent.phone", controller = "getLead")
    private Set<PhoneNumberDTO> additionalPhones;
    @AuthField(frontendId = "LeadDetailsComponent.email", controller = "getLead")
    private String email;
    @AuthField(controller = "getLead")
    private List<Task> tasks;
    @AuthField(controller = "getLead")
    private String bgColor;
    @AuthField(controller = "getLead")
    private String textColor;
    @AuthField(controller = "getLead")
    private String objectType;
    @AuthField(controller = "getLead")
    private Long acceptanceUserId;
    @AuthField(controller = "getLead")
    private Boolean convertedToOpportunity;
    @AuthField(controller = "getLead")
    private String avatar;
    @AuthField(controller = "getLead")
    private Lead.CloseResult closeResult;
    @AuthField(controller = "getLead")
    private Lead.RejectionReason rejectionReason;
    @AuthField(controller = "getLead") // todo: permission
    private LocalDateTime created;
    @AuthField(controller = "getLead") // todo: permission
    private Long leadKeeperId;

    @AuthField(controller = "getLead") // todo: permission
    private String companyWww;
    @AuthField(controller = "getLead") // todo: permission
    private Long industry;
    @AuthField(controller = "getLead") // todo: permission
    private Long sourceOfAcquisition;
    @AuthField(controller = "getLead") // todo: permission
    private Long subSourceOfAcquisition;
    @AuthField(controller = "getLead") // todo: permission
    private Long subjectOfInterest;
    @AuthField(controller = "getLead") // todo: permission
    private Boolean interestedInCooperation;
    @AuthField(controller = "getLead") // todo: permission
    private String companyStreetAndNumber;
    @AuthField(controller = "getLead") // todo: permission
    private String companyCity;
    @AuthField(controller = "getLead") // todo: permission
    private String companyPostalCode;
    @AuthField(controller = "getLead") // todo: permission
    private String companyProvince;
    @AuthField(controller = "getLead") // todo: permission
    private String companyCountry;
    @AuthField(controller = "getLead") // todo: permission
    private LeadPriority priority;


    public static LeadDetailsDTO fromLead(Lead lead) {
        LeadDetailsDTO dto = new LeadDetailsDTO();
        dto.setId(lead.getId());
        dto.setProcessInstanceId(lead.getProcessInstanceId());
        dto.setFirstName(lead.getFirstName());
        dto.setLastName(lead.getLastName());
        dto.setPosition(lead.getPosition());
        dto.setPhoneNumber(lead.getPhone());
        dto.setAdditionalPhones(PhoneNumberMapper.mapToPhoneNumberDtoSet(lead.getAdditionalPhones()));
        dto.setEmail(lead.getEmail());
        dto.setAcceptanceUserId(lead.getAcceptanceUserId());
        dto.setConvertedToOpportunity(lead.getConvertedToOpportunity());
        dto.setAvatar(lead.getAvatar());
        dto.setRejectionReason(lead.getRejectionReason());
        dto.setCloseResult(lead.getCloseResult());
        dto.setCreated(lead.getCreated());
        dto.setLeadKeeperId(lead.getLeadKeeperId());
        dto.setSourceOfAcquisition(lead.getSourceOfAcquisition());
        dto.setSubSourceOfAcquisition(lead.getSubSourceOfAcquisition());
        dto.setSubjectOfInterest(lead.getSubjectOfInterest());
        dto.setInterestedInCooperation(lead.getInterestedInCooperation());
        dto.setPriority(lead.getPriority());
        if (lead.getCompany() != null) {
            dto.setCompanyName(lead.getCompany().getCompanyName());
            dto.setCompanyWww(lead.getCompany().getWwwAddress());
            dto.setCompanyStreetAndNumber(lead.getCompany().getStreetAndNumber());
            dto.setCompanyCity(lead.getCompany().getCity());
            dto.setCompanyPostalCode(lead.getCompany().getPostalCode());
            dto.setCompanyProvince(lead.getCompany().getProvince());
            dto.setCompanyCountry(lead.getCompany().getCountry());
            dto.setIndustry(lead.getCompany().getIndustry());
        }
        return dto;
    }
}

