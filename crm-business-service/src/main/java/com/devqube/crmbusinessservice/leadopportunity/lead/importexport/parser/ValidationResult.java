package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser;

public enum ValidationResult {
    NOT_VALIDATED, OK, INCORRECT, DUPLICATE
}
