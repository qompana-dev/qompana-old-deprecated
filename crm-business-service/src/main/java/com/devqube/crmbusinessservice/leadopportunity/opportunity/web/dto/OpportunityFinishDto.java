package com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OpportunityFinishDto {

    @NotNull
    private Opportunity.FinishResult finishResult;
    private Opportunity.RejectionReason rejectionReason;
}
