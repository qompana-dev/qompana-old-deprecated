package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum SalesOpportunityEnum {
    NOT_SPECIFIED("NOT_SPECIFIED"),

    HIGH("HIGH"),

    MEDIUM("MEDIUM"),

    LOW("LOW");

    private String value;

    SalesOpportunityEnum(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static SalesOpportunityEnum fromValue(String text) {
        for (SalesOpportunityEnum b : SalesOpportunityEnum.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
}
