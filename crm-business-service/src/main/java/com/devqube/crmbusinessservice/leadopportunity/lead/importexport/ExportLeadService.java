package com.devqube.crmbusinessservice.leadopportunity.lead.importexport;

import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.dictionary.DictionaryService;
import com.devqube.crmbusinessservice.dictionary.Type;
import com.devqube.crmbusinessservice.dictionary.dto.DictionaryDTO;
import com.devqube.crmbusinessservice.leadopportunity.AccountsService;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto.LeadDto;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineHelperService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.importexport.ImportExportUtil;
import com.devqube.crmshared.user.dto.AccountEmail;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ExportLeadService {
    private final ModelMapper strictModelMapper;
    private final LeadRepository leadRepository;
    private final AccountsService accountsService;
    private final UserService userService;
    private final ProcessEngineHelperService processEngineHelperService;
    private final DictionaryService dictionaryService;

    public ExportLeadService(@Qualifier("strictModelMapper") ModelMapper strictModelMapper,
                             LeadRepository leadRepository,
                             AccountsService accountsService,
                             UserService userService,
                             DictionaryService dictionaryService,
                             ProcessEngineHelperService processEngineHelperService) {
        this.leadRepository = leadRepository;
        this.accountsService = accountsService;
        this.strictModelMapper = strictModelMapper;
        this.userService = userService;
        this.processEngineHelperService = processEngineHelperService;
        this.dictionaryService = dictionaryService;
    }

    public void exportLeads(HttpServletResponse response, String loggedAccountEmail) throws BadRequestException {
        Map<Long, AccountEmail> userMap = getAccountEmailsMap();
        List<DictionaryWord> sourcesOfAcquisition = getSourcesOfAcquisitionMap();

        List<LeadDto> allLeadsForExport = getAllLeadsForExport(loggedAccountEmail)
                .stream().map(c -> convert(c, userMap, sourcesOfAcquisition))
                .collect(Collectors.toList());

        try {
            ImportExportUtil.export(response, allLeadsForExport, LeadDto.class, "leads");
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException();
        }
    }

    private Map<Long, AccountEmail> getAccountEmailsMap() throws BadRequestException {
        Map<Long, AccountEmail> userMap = new HashMap<>();
        userService.getAccountEmails().forEach(email -> userMap.put(email.getId().longValue(), email));
        return userMap;
    }

    private List<Lead> getAllLeadsForExport(String loggedAccountEmail) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return leadRepository.getLeadsByDeletedFalse(userIds);
    }

    private LeadDto convert(Lead lead, Map<Long, AccountEmail> userMap, List<DictionaryWord> sourcesOfAcquisition) {
        LeadDto leadDto = strictModelMapper.map(lead, LeadDto.class);
        leadDto.setProcessDefinitionName(findProcessDefinitionName(lead.getProcessInstanceId()));
        leadDto.setLeadKeeperName(getLeadKeeperName(lead, userMap));
        leadDto.setSourceOfAcquisition(getDictionaryWord(sourcesOfAcquisition, lead.getSourceOfAcquisition()));
        leadDto.setSubSourceOfAcquisition(getDictionaryWord(sourcesOfAcquisition, lead.getSubSourceOfAcquisition()));
        setCompany(lead, leadDto);
        return leadDto;
    }

    private String findProcessDefinitionName(String processInstanceId) {
        if (processInstanceId == null) {
            return null;
        }
        return processEngineHelperService.getProcessInstance(processInstanceId).getProcessDefinitionName();
    }

    private void setCompany(Lead lead, LeadDto leadDto) {
        if (lead.getCompany() != null) {
            leadDto.setCompanyName(lead.getCompany().getCompanyName());
            leadDto.setCompanyWwwAddress(lead.getCompany().getWwwAddress());
            leadDto.setCompanyStreetAndNumber(lead.getCompany().getStreetAndNumber());
            leadDto.setCompanyCity(lead.getCompany().getCity());

            leadDto.setCompanyPostalCode(lead.getCompany().getPostalCode());
            leadDto.setCompanyProvince(lead.getCompany().getProvince());
            leadDto.setCompanyCountry(lead.getCompany().getCountry());
            //leadDto.setCompanyNotes(lead.getCompany().getNotes());
        }
    }

    private String getLeadKeeperName(Lead lead, Map<Long, AccountEmail> userMap) {
        if (lead.getLeadKeeperId() != null) {
            AccountEmail accountEmail = userMap.get(lead.getLeadKeeperId());
            if (accountEmail != null) {
                return accountEmail.getName() + " " + accountEmail.getSurname();
            }
        }
        return null;
    }

    private List<DictionaryWord> getSourcesOfAcquisitionMap() {
        DictionaryDTO dictionaryDTO = dictionaryService.findAdminDictionary(Type.SOURCE_OF_ACQUISITION.getDictionaryId());
        return dictionaryDTO.getWords().stream()
                .map(wordDTO -> new DictionaryWord(wordDTO.getId(), wordDTO.getName()))
                .collect(Collectors.toList());
    }

    private String getDictionaryWord(List<DictionaryWord> sourcesOfAcquisition, Long sourceOfAcquisition) {
        return sourcesOfAcquisition.stream()
                .filter(dictionaryWord -> dictionaryWord.getId().equals(sourceOfAcquisition))
                .findFirst()
                .map(DictionaryWord::getName)
                .orElse("");
    }
}
