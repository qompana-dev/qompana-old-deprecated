package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser;

import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Component
public class ExcelParseStrategy implements ParseStrategy {


    @Override
    public List<ParsedLead> parseFile(InputStream file) {
        List<ParsedLead> parsedLeadList = new ArrayList<>();

        try {
            Workbook workbook = WorkbookFactory.create(file);
            Sheet sheet = workbook.getSheetAt(0);
            DataFormatter dataFormatter = new DataFormatter();

            for (Row row : sheet) {
                String[] fieldValues = {"", "", "", "", "", "", ""};
                for (int i = 0; i <= 6; i++) {
                    Cell cell = row.getCell(i);
                    if (cell != null) {
                        cell.setCellType(CellType.STRING);
                        fieldValues[i] = dataFormatter.formatCellValue(cell);
                    }
                }

                ParsedLead parsedLead = new ParsedLead()
                        .setFirstName(new ValidatedValue().setValue(fieldValues[0]))
                        .setLastName(new ValidatedValue().setValue(fieldValues[1]))
                        .setEmail(new ValidatedValue().setValue(fieldValues[2]))
                        .setPhone(new ValidatedValue().setValue(fieldValues[3]))
                        .setPosition(new ValidatedValue().setValue(fieldValues[4]))
                        .setCompanyName(new ValidatedValue().setValue(fieldValues[5]))
                        .setCompanyCity(new ValidatedValue().setValue(fieldValues[6]));

                parsedLeadList.add(parsedLead);
            }
            workbook.close();
        } catch (IOException e) {
            //todo add exception
            e.printStackTrace();
        }

        return parsedLeadList;
    }

    @Override
    public ParseStrategyType getStrategyType() {
        return ParseStrategyType.EXCEL;
    }

}

