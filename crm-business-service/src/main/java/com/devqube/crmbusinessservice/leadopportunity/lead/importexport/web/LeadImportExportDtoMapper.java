package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.web;

import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.CommonLeadParams;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto.LeadDtoIn;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto.LeadImportSettingDtoIn;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto.ParsedLeadDtoOut;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto.ValidatedValueDtoIn;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ParsedLead;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ValidatedValue;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LeadImportExportDtoMapper {

    static ParsedLeadDtoOut toParsedLeadDtoOut(ParsedLead parsedLead) {
        if (parsedLead == null) {
            return null;
        }
        return new ParsedLeadDtoOut()
                .setFirstName(parsedLead.getFirstName())
                .setLastName(parsedLead.getLastName())
                .setEmail(parsedLead.getEmail())
                .setPhone(parsedLead.getPhone())
                .setPosition(parsedLead.getPosition())
                .setCompanyName(parsedLead.getCompanyName())
                .setCompanyCity(parsedLead.getCompanyCity());
    }

    static ParsedLead toParsedLead(LeadDtoIn leadDtoIn) {
        if (leadDtoIn == null) {
            return null;
        }
        return new ParsedLead()
                .setFirstName(toValidatedValue(leadDtoIn.getFirstName()))
                .setLastName(toValidatedValue(leadDtoIn.getLastName()))
                .setEmail(toValidatedValue(leadDtoIn.getEmail()))
                .setPhone(toValidatedValue(leadDtoIn.getPhone()))
                .setPosition(toValidatedValue(leadDtoIn.getPosition()))
                .setCompanyName(toValidatedValue(leadDtoIn.getCompanyName()))
                .setCompanyCity(toValidatedValue(leadDtoIn.getCompanyCity()));

    }

    public static CommonLeadParams toCommonLeadParams(LeadImportSettingDtoIn settingDtoIn, String loggedAccountEmail) {
        if (settingDtoIn == null) {
            return null;
        }
        return new CommonLeadParams()
                .setProcessDefinitionId(settingDtoIn.getProcessDefinitionId())
                .setLeadKeeperId(settingDtoIn.getLeadKeeperId())
                .setSourceOfAcquisitionId(settingDtoIn.getSourceOfAcquisitionId())
                .setSubSourceOfAcquisitionId(settingDtoIn.getSubSourceOfAcquisitionId())
                .setLoggedAccountEmail(loggedAccountEmail);
    }

    static ValidatedValue toValidatedValue(ValidatedValueDtoIn dtoIn) {
        if (dtoIn == null) {
            return null;
        }
        return new ValidatedValue()
                .setValue(dtoIn.getValue())
                .setValidationResults(dtoIn.getValidationResults());
    }

    static List<ParsedLeadDtoOut> toParsedLeadDtoOut(List<ParsedLead> parsedLeadList) {
        if(parsedLeadList == null){
            return new ArrayList<>();
        }
        return parsedLeadList.stream()
                .map(LeadImportExportDtoMapper::toParsedLeadDtoOut)
                .collect(Collectors.toList());
    }

    static List<ParsedLead> toParsedLead(List<LeadDtoIn> leadDtoInList) {
        if(leadDtoInList == null){
            return new ArrayList<>();
        }
        return leadDtoInList.stream()
                .map(LeadImportExportDtoMapper::toParsedLead)
                .collect(Collectors.toList());
    }
}
