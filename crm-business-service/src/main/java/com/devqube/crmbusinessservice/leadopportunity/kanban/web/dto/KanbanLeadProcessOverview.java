package com.devqube.crmbusinessservice.leadopportunity.kanban.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ProcessColor;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.repository.ProcessDefinition;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class KanbanLeadProcessOverview {

    @AuthField(controller = "getProcessesOverview")
    private String id;

    @AuthField(frontendId = "LeadKanbanComponent.processNameField", controller = "getProcessesOverview")
    private String name;

    @AuthField(frontendId = "LeadKanbanComponent.processNumberField", controller = "getProcessesOverview")
    private Long size;

    @AuthField(controller = "getProcessesOverview")
    private String backgroundColor;

    @AuthField(controller = "getProcessesOverview")
    private String textColor;

    @AuthField(controller = "getProcessesOverview")
    private List<KanbanLeadTaskOverview> tasks;

    public KanbanLeadProcessOverview(ProcessDefinition processDefinition, Map<UserTask, List<KanbanLead>> tasks, ProcessColor processColor) {
        this.id = processDefinition.getId();
        this.name = processDefinition.getName();
        this.size = tasks.values().stream().mapToLong(Collection::size).sum();
        this.tasks = tasks.entrySet().stream().map(entry -> new KanbanLeadTaskOverview(entry.getKey(), entry.getValue())).collect(Collectors.toList());
        this.backgroundColor = processColor.getBackground();
        this.textColor = processColor.getText();
    }

}
