package com.devqube.crmbusinessservice.leadopportunity.processEngine.client;

import lombok.Data;

import java.util.Set;

@Data
public class UserNotificationDTO {
    private Set<NotificationMessageParam> messageParams;

    @Data
    public static class NotificationMessageParam {
        private String key;
        private String value;
    }
}
