package com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.OpportunityPriority;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class OpportunitySaveDTO {

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity"),
          @AuthField(controller = "getOpportunityForEdit"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard"),
          @AuthField(controller = "modifyOpportunity"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard")
  })
  private Long id;

  @NotNull
  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.nameField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.nameField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.name"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.nameField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.name")
  })
  private String name;

  @NotNull
  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.customerField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.customerField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.customer"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.customerField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.name")
  })
  private Long customerId;

  @NotNull
  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.contactField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.contactField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.contact"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.contactField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.contact")
  })
  private Long contactId;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.priorityField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.priorityField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.priorityField"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.priorityField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.priorityField")
  })
  private OpportunityPriority priority;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.amountField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.amountField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.amount"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.amountField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.amount")
  })
  private Double amount;

  @AuthFields(list = {
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.estimatedAmount"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.estimatedAmount")
  })
  private Double estimatedAmount;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.currencyField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.currencyField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.currency"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.currencyField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.currency"),
  })
  @NotNull
  private String currency;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.probabilityField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.probabilityField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.probability"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.probabilityField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.probability"),
  })
  private Long probability;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.createDateField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.createDateField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.createDate"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.createDateField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.createDate"),
  })
  private LocalDate createDate;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.finishDateField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.finishDateField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.finishDate"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.finishDateField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.finishDate"),
  })
  private LocalDate finishDate;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.processDefinitionField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.processDefinitionField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.processDefinitionField"),
  })
  private String processDefinitionId;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.descriptionField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.descriptionField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.description"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.descriptionField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.description"),
  })
  private String description;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.sourceOfAcquisitionField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.sourceOfAcquisitionField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.sourceOfAcquisition"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.sourceOfAcquisitionField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.sourceOfAcquisition"),
  })
  private Long sourceOfAcquisition;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.sourceOfAcquisitionField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.sourceOfAcquisitionField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.sourceOfAcquisition"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.sourceOfAcquisitionField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.sourceOfAcquisition"),
  })
  private Long subSourceOfAcquisition;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.subjectOfInterestField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.subjectOfInterestField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.subjectOfInterest"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.subjectOfInterestField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.subjectOfInterest"),
  })
  private Long subjectOfInterest;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.ownerField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.ownerField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.ownerField"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.ownerField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.ownerField")
  })
  private Long ownerOneId;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.ownerField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.ownerField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.ownerField"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.ownerField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.ownerField")
  })
  private Long ownerOnePercentage;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.ownerField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.ownerField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.ownerField"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.ownerField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.ownerField")
  })
  private Long ownerTwoId;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.ownerField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.ownerField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.ownerField"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.ownerField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.ownerField")
  })
  private Long ownerTwoPercentage;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.ownerField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.ownerField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.ownerField"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.ownerField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.ownerField")
  })
  private Long ownerThreeId;

  @AuthFields(list = {
          @AuthField(controller = "createOpportunity", frontendId = "OpportunityCreateComponent.ownerField"),
          @AuthField(controller = "getOpportunityForEdit", frontendId = "OpportunityEditComponent.ownerField"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.ownerField"),
          @AuthField(controller = "modifyOpportunity", frontendId = "OpportunityEditComponent.ownerField"),
          @AuthField(controller = "modifyOpportunityFromDetailsCard", frontendId = "OpportunityDetailsCardComponent.ownerField")
  })
  private Long ownerThreePercentage;

  @AuthFields(list = {
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.createdBy")
  })
  private Long creator;

  @AuthFields(list = {
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.createdBy")
  })
  private LocalDateTime created;

  @AuthFields(list = {
          @AuthField(controller = "getOpportunityForEdit"),
          @AuthField(controller = "getOpportunityForEditForDetailsCard")
  })
  private Boolean canEditAmount;

  @AuthFields(list = {
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.productAmount")
  })
  private Double productAmount;

  @AuthFields(list = {
          @AuthField(controller = "getOpportunityForEditForDetailsCard", frontendId = "OpportunityDetailsCardComponent.profitAmount")
  })
  private Double profitAmount;
}

