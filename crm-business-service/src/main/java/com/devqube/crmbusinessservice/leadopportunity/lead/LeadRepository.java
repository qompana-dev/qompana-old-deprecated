package com.devqube.crmbusinessservice.leadopportunity.lead;

import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface LeadRepository extends JpaRepository<Lead, Long>, JpaSpecificationExecutor<Lead> {

    @Query("select l from Lead l where l.created > :date and l.deleted = false and l.closed = false and l.convertedToOpportunity = false and (l.leadKeeperId in (:userIds) or l.creator in (:userIds))")
    List<Lead> findAllByCreatedAfterAndDeletedFalse(@Param("date") LocalDateTime date, @Param("userIds") List<Long> userIds);

    @Query("select l from Lead l where l.created > :date and l.deleted = false and l.closed = false and l.convertedToOpportunity = false and (l.leadKeeperId in (:userIds) or l.creator in (:userIds)) " +
            "and (:search is null or (LOWER(l.firstName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.lastName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.phone) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(l.created as text)) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.company.companyName) LIKE LOWER(concat('%', :search, '%'))))")
    Page<Lead> findAllByCreatedAfterAndDeletedFalseForMobile(@Param("date") LocalDateTime date, Pageable pageable, @Param("userIds") List<Long> userIds, @Param("search") String search);

    @Query("select l from Lead l where l.updated > :date and l.deleted = false and l.closed = false and l.convertedToOpportunity = false and (l.leadKeeperId in (:userIds) or l.creator in (:userIds))")
    List<Lead> findAllByUpdatedAfterAndDeletedFalse(@Param("date") LocalDateTime date, @Param("userIds") List<Long> userIds);

    @Query("select l from Lead l where l.updated > :date and l.deleted = false and l.closed = false and l.convertedToOpportunity = false and (l.leadKeeperId in (:userIds) or l.creator in (:userIds)) " +
            "and (:search is null or (LOWER(l.firstName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.lastName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.phone) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(l.created as text)) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.company.companyName) LIKE LOWER(concat('%', :search, '%'))))")
    Page<Lead> findAllByUpdatedAfterAndDeletedFalseForMobile(@Param("date") LocalDateTime date, Pageable pageable, @Param("userIds") List<Long> userIds, @Param("search") String search);

    @Query("select l from Lead l where l.deleted = false and l.closed = false and l.convertedToOpportunity = false and (l.leadKeeperId in (:userIds) or l.creator in (:userIds))" +
            " and (select max(lvl.lastViewed) from LastViewedLead lvl where lvl.lead.id = l.id and lvl.userId = :userId) > :date ")
    List<Lead> findAllByLastViewedAfterAndDeletedFalse(@Param("date") LocalDateTime date, @Param("userId") Long userId, @Param("userIds") List<Long> userIds);

    @Query("select l from Lead l where l.deleted = false and l.closed = false and l.convertedToOpportunity = false and (l.leadKeeperId in (:userIds) or l.creator in (:userIds))" +
            " and (select max(lvl.lastViewed) from LastViewedLead lvl where lvl.lead.id = l.id and lvl.userId = :userId) > :date " +
            "and (:search is null or (LOWER(l.firstName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.lastName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.phone) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(l.created as text)) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.company.companyName) LIKE LOWER(concat('%', :search, '%'))))")
    Page<Lead> findAllByLastViewedAfterAndDeletedFalseForMobile(@Param("date") LocalDateTime date, Pageable pageable, @Param("userId") Long userId, @Param("userIds") List<Long> userIds, @Param("search") String search);

    @Query("select l from Lead l where l.deleted = false and l.closed = true and (l.leadKeeperId in (:userIds) or l.creator in (:userIds)) and l.closeResult = :closeResult")
    List<Lead> findAllClosed(@Param("userIds") List<Long> userIds, @Param("closeResult") Lead.CloseResult closeResult);

    @Query("select l from Lead l where l.deleted = false and l.closed = true and (l.leadKeeperId in (:userIds) or l.creator in (:userIds)) and l.closeResult = :closeResult " +
            "and (:search is null or (LOWER(l.firstName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.lastName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.phone) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(l.created as text)) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.company.companyName) LIKE LOWER(concat('%', :search, '%'))))")
    Page<Lead> findAllClosedForMobile(Pageable pageable, @Param("userIds") List<Long> userIds, @Param("closeResult") Lead.CloseResult closeResult, @Param("search") String search);

    @Query("select l from Lead l where l.deleted = false and (l.convertedToContactId is not null or l.convertedToCustomerId is not null or l.convertedToOpportunityId is not null) and (l.leadKeeperId in (:userIds) or l.creator in (:userIds))")
    List<Lead> findAllConverted(@Param("userIds") List<Long> userIds);

    @Query("select l from Lead l where l.deleted = false and (l.leadKeeperId is null and l.creator in (:userIds))")
    List<Lead> findAllWithoutKeeper(@Param("userIds") List<Long> userIds);

    @Query("select l from Lead l where l.deleted = false and (l.convertedToContactId is not null or l.convertedToCustomerId is not null or l.convertedToOpportunityId is not null) and (l.leadKeeperId in (:userIds) or l.creator in (:userIds)) " +
            "and (:search is null or (LOWER(l.firstName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.lastName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.phone) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(l.created as text)) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.company.companyName) LIKE LOWER(concat('%', :search, '%'))))")
    Page<Lead> findAllConvertedForMobile(Pageable pageable, @Param("userIds") List<Long> userIds, @Param("search") String search);

    @Query("select l from Lead l where l.deleted = false and l.closed = false and l.convertedToOpportunity = false and (l.leadKeeperId in (:usersId) or l.creator in (:usersId))")
    List<Lead> findAllByDeletedFalse(@Param("usersId") List<Long> usersId);

    @Query("select l from Lead l where l.deleted = false and (l.leadKeeperId in (:usersId) or l.creator in (:usersId))")
    List<Lead> findAll(@Param("usersId") List<Long> usersId);

    @Query("select l from Lead l " +
            "where l.deleted = false and l.closed = false and l.convertedToOpportunity = false and (l.leadKeeperId in (:usersId) or l.creator in (:usersId)) " +
            "AND (:search is null or (LOWER(l.firstName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.lastName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.phone) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(l.created as text)) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.company.companyName) LIKE LOWER(concat('%', :search, '%'))))")
    Page<Lead> findAllByDeletedFalseForMobile(Pageable pageable, @Param("usersId") List<Long> usersId, @Param("search") String search);

    @Query("select l from Lead l where l.deleted = false and l.closed = false and l.convertedToOpportunity = false and (l.leadKeeperId in (:usersId) or l.creator in (:usersId)) order by id")
    List<Lead> getLeadsByDeletedFalse(@Param("usersId") List<Long> usersId);

    @Query("select l from Lead l where l.deleted = false and l.closed = false and l.convertedToOpportunity = false and (l.acceptanceUserId in (:userIds) or (l.acceptanceUserId is not null and (l.leadKeeperId in (:userIds) or l.creator in (:userIds))))")
    List<Lead> findAllByAcceptanceUserIdAndDeletedFalse(@Param("userIds") List<Long> userIds);

    @Query("select l from Lead l where l.deleted = false and l.closed = false and l.convertedToOpportunity = false and l.acceptanceUserId = :acceptanceUserId " +
            "and (:search is null or (LOWER(l.firstName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.lastName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.phone) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(l.created as text)) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(l.company.companyName) LIKE LOWER(concat('%', :search, '%'))))")
    Page<Lead> findAllByAcceptanceUserIdAndDeletedFalseForMobile(Pageable pageable, @Param("acceptanceUserId") Long acceptanceUserId, @Param("search") String search);

    @Query("select l from Lead l where l.deleted = false and l.closed = false and l.convertedToOpportunity = false and (l.leadKeeperId in (:userIds) or l.creator in (:userIds))")
    Set<Lead> findAllByDeletedFalseAndConvertedToOpportunityFalse(@Param("userIds") List<Long> userIds);

    Set<Lead> findAllByProcessInstanceId(String processInstanceId);

    @Query("SELECT l FROM Lead l WHERE l.convertedToOpportunity = false and (LOWER(l.firstName) LIKE LOWER(concat('%', ?1, '%')) or LOWER(l.lastName) LIKE LOWER(concat('%', ?1, '%'))) and l.deleted = false and l.closed = false")
    List<Lead> findByNameOrSurnameContaining(String pattern);

    @Query("SELECT l FROM Lead l WHERE l.convertedToOpportunity = false and l.id in ?1 and l.deleted = false and l.closed = false")
    List<Lead> findByIds(List<Long> ids);

    Long countAllByDeletedFalseAndClosedFalseAndProcessInstanceIdIn(List<String> processInstanceIds);

    @Query("SELECT l FROM Lead l WHERE l.convertedToOpportunity = false and (LOWER(l.firstName) LIKE LOWER(concat('%', :term, '%')) or LOWER(l.lastName) LIKE LOWER(concat('%', :term, '%'))) and l.deleted = false and l.closed = false and (l.leadKeeperId in (:usersId) or l.creator in (:usersId))")
    List<Lead> findAllAvailableForUserAndContaining(@Param("term") String term, @Param("usersId") List<Long> usersId);

    @Query("SELECT l.id FROM Lead l WHERE l.convertedToOpportunity = false and  l.deleted = false and l.closed = false and (l.leadKeeperId = :userId or l.creator = :userId)")
    List<Long> findAllAvailableForUser(@Param("userId") Long userId);

    @Query("select count(l) from Lead l where l.created >= :from and l.created <= :to and l.creator in :userIds")
    Long findCreatedLeadsForGoal(@Param("from") LocalDateTime from, @Param("to") LocalDateTime to, @Param("userIds") Set<Long> userIds);

    @Query("select count(l) from Lead l where l.convertedDateTime >= :from and l.convertedDateTime <= :to and l.convertedBy in :userIds")
    Long findConvertedLeadsForGoal(@Param("from") LocalDateTime from, @Param("to") LocalDateTime to, @Param("userIds") Set<Long> userIds);

    @Query("SELECT l FROM Lead l WHERE l.convertedDateTime is not null and l.convertedBy in :userIds and l.deleted=false")
    Page<Lead> findAllConvertedLeads(@Param("userIds") List<Long> userIds, Pageable pageable);


    @Query("SELECT l FROM Lead l WHERE " +
            "((l.firstName != '' and l.firstName is not null and LOWER(l.firstName) = lower(:firstName)) and LOWER(l.lastName) = lower(:lastName)) or " +
            "(l.email != '' and l.email is not null and LOWER(l.email) = lower(:email))")
    List<Lead> searchLeadsForLeadCreation(@Param("firstName") String firstName,
                                          @Param("lastName") String lastName,
                                          @Param("email") String email);

    @Query("SELECT l FROM Lead l WHERE l.convertedToOpportunity = true and l.convertedToOpportunityId = :opportunityId and l.deleted = false")
    Optional<Lead> findByOpportunityId(@Param("opportunityId") Long opportunityId);
}
