package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class ParseStrategyFactory {

    private final Map<ParseStrategyType, ParseStrategy> strategyMap;

    @Autowired
    public ParseStrategyFactory(Set<ParseStrategy> parseStrategySet) {
        strategyMap = new HashMap<>();
        parseStrategySet.forEach(strategy -> strategyMap.put(strategy.getStrategyType(), strategy));
    }

    public ParseStrategy getStrategy(ParseStrategyType parseStrategyType) {
        return strategyMap.get(parseStrategyType);
    }

}
