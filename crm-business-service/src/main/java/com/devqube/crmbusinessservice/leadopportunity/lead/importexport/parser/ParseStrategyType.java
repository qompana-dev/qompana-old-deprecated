package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser;

public enum ParseStrategyType {
    CSV, EXCEL
}
