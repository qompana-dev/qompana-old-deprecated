package com.devqube.crmbusinessservice.leadopportunity.processEngine.model;

import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Property;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

public interface ProcessInstanceDetailsDTO {
    void setTasks(List<Task> tasks);
    void setBgColor(String color);
    void setTextColor(String color);
    void setObjectType(String category);
    List<Task> getTasks();
    String getObjectType();

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Task {
        private String id;
        private String name;
        private String documentation;
        private boolean active;
        private boolean past;
        private Long percentage;
        private Long acceptanceUserId;
        private String candidateUsers;
        private String candidateGroups;
        private List<Property> properties;
        private Boolean isInformationTask;
        private String statusAcceptance;
        private Long daysToComplete;
        private LocalDateTime taskChangeDate;
    }
}
