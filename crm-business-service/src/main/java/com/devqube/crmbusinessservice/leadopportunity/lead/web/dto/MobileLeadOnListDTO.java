package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class MobileLeadOnListDTO {
    @AuthFields(list = {
            @AuthField(controller = "getLeadForEdit"),
            @AuthField(controller = "getLeads")
    })
    private Long id;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.firstNameColumn", controller = "getLeads"),
    })
    private String firstName;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.firstNameColumn", controller = "getLeads"),
    })
    private String lastName;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.companyNameColumn", controller = "getLeads"),
    })
    private String company;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.phoneColumn", controller = "getLeads"),
    })
    private String phone;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.createdColumn", controller = "getLeads"),
    })
    private LocalDateTime creationDate;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.statusColumn", controller = "getLeads"),
    })
    private Long status;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.statusColumn", controller = "getLeads"),
    })
    private Long maxStatus;

    private ProcessColor processColor;

    public MobileLeadOnListDTO(Lead lead, Long status, Long maxStatus, ProcessColor processColor) {
        this.id = lead.getId();
        this.firstName = lead.getFirstName();
        this.lastName = lead.getLastName();
        this.company = lead.getCompany().getCompanyName();
        this.phone = lead.getPhone();
        this.creationDate = lead.getCreated();
        this.status = status;
        this.maxStatus = maxStatus;
        this.processColor = processColor;
    }
}
