package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.lead.model.LeadPriority;
import com.devqube.crmbusinessservice.phoneNumber.web.dto.PhoneNumberDTO;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@AuthFilterEnabled
public class LeadSaveDTO {

    @AuthFields(list = {
            @AuthField(controller = "getLeadForEdit"),
            @AuthField(controller = "modifyLead"),
            @AuthField(controller = "createLead"),
    })
    private Long id;

    @AuthFields(list = {
            @AuthField(controller = "getLeadForEdit"),
            @AuthField(controller = "modifyLead"),
            @AuthField(controller = "createLead"),
            @AuthField(frontendId = "LeadCreateComponent.processDefinitionField", controller = "createLead"),
    })
    private String processDefinitionId;

    @AuthFields(list = {
            @AuthField(controller = "getLeadForEdit"),
            @AuthField(controller = "modifyLead"),
            @AuthField(controller = "createLead"),
    })
    private Long companyId;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.companyTypeField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.companyTypeField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.companyTypeField", controller = "createLead"),
    })
    private Long industry;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.companyNipField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.companyNipField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.companyNipField", controller = "createLead"),
    })
    private String companyNip;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.companyNameField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.companyNameField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.companyNameField", controller = "createLead"),
    })
    private String companyName;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.wwwAddressField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.wwwAddressField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.wwwAddressField", controller = "createLead"),
    })
    private String wwwAddress;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.streetAndNumberField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.streetAndNumberField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.streetAndNumberField", controller = "createLead"),
    })
    private String streetAndNumber;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.cityField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.cityField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.cityField", controller = "createLead"),
    })
    private String city;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.postalCodeField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.postalCodeField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.postalCodeField", controller = "createLead"),
    })
    private String postalCode;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.provinceField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.provinceField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.provinceField", controller = "createLead"),
    })
    private String province;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.countryField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.countryField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.countryField", controller = "createLead"),
    })
    private String country;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.notesField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.notesField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.notesField", controller = "createLead"),
    })
    private String notes;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.priorityField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.priorityField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.priorityField", controller = "createLead"),
    })
    private LeadPriority priority;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.scoringField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.scoringField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.scoringField", controller = "createLead"),
    })
    private Long scoring;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.firstNameField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.firstNameField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.firstNameField", controller = "createLead"),
    })
    private String firstName;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.lastNameField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.lastNameField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.lastNameField", controller = "createLead"),
    })
    private String lastName;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.positionField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.positionField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.positionField", controller = "createLead"),
    })
    private Long position;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.emailField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.emailField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.emailField", controller = "createLead"),
    })
    private String email;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.phoneField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.phoneField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.phoneField", controller = "createLead"),
    })
    private String phone;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.phoneField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.phoneField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.phoneField", controller = "createLead"),
    })
    private Set<PhoneNumberDTO> additionalPhones = new HashSet<>();

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.leadKeeperField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.leadKeeperField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.leadKeeperField", controller = "createLead"),
    })
    private Long leadKeeperId;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.sourceOfAcquisitionField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.sourceOfAcquisitionField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.sourceOfAcquisitionField", controller = "createLead"),
    })
    private Long sourceOfAcquisition;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.sourceOfAcquisitionField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.sourceOfAcquisitionField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.sourceOfAcquisitionField", controller = "createLead"),
    })
    private Long subSourceOfAcquisition;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.subjectOfInterestField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.subjectOfInterestField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.subjectOfInterestField", controller = "createLead"),
    })
    private Long subjectOfInterest;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadEditComponent.interestedInCooperationField", controller = "getLeadForEdit"),
            @AuthField(frontendId = "LeadEditComponent.interestedInCooperationField", controller = "modifyLead"),
            @AuthField(frontendId = "LeadCreateComponent.interestedInCooperationField", controller = "createLead"),
    })
    private Boolean interestedInCooperation;

    @AuthFields(list = {
            @AuthField(controller = "getLeadForEdit"),
            @AuthField(controller = "modifyLead"),
            @AuthField(controller = "createLead"),
    })
    private Long creator;

    @AuthFields(list = {
            @AuthField(controller = "getLeadForEdit"),
            @AuthField(controller = "modifyLead"),
            @AuthField(controller = "createLead"),
    })
    private String avatar;

}

