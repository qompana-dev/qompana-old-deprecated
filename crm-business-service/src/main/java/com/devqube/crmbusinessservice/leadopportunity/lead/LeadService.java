package com.devqube.crmbusinessservice.leadopportunity.lead;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.dictionary.Word;
import com.devqube.crmbusinessservice.dictionary.WordRepository;
import com.devqube.crmbusinessservice.dictionary.WordSpecifications;
import com.devqube.crmbusinessservice.leadopportunity.AccountsService;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.LastViewedLead;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.*;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineHelperService;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineReadService;
import com.devqube.crmbusinessservice.phoneNumber.PhoneNumberService;
import com.devqube.crmbusinessservice.scheduler.NotificationSchedulerService;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.association.RemoveCrmObject;
import com.devqube.crmshared.association.dto.NoteDTO;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.google.common.base.Strings;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import org.flowable.bpmn.model.UserTask;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.devqube.crmbusinessservice.leadopportunity.lead.LeadSpecifications.*;
import static com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadSearchType.*;
import static com.devqube.crmbusinessservice.scheduler.NotificationJobType.LEAD;

@Service
@RequiredArgsConstructor
public class LeadService {

    private final LeadRepository leadRepository;
    private final AssociationClient associationClient;
    private final UserClient userClient;
    private final UserService userService;
    private final AccessService accessService;
    private final ProcessEngineHelperService processEngineHelperService;
    private final LastViewedLeadRepository lastViewedLeadRepository;
    private final AccountsService accountsService;
    private final ProcessEngineReadService processEngineReadService;
    private final ContactRepository contactRepository;
    private final CustomerRepository customerRepository;
    private final OpportunityRepository opportunityRepository;
    private final PhoneNumberService phoneNumberService;
    private final NotificationSchedulerService notificationSchedulerService;
    private final WordRepository wordRepository;

    public Page<LeadOnListDTO> findAll(String loggedAccountEmail, String search, Pageable pageable) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        Specification<Lead> spec = LeadSpecifications.findAll(userIds).and(searchByCustomFields(search));
        if (pageable.getSort().getOrderFor("process") != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, leadRepository.findAll(spec));
        } else {
            return leadRepository.findAll(spec, pageable).map(this::mapToLeadOnListDTO);
        }
    }

    public Page<LeadOnListDTO> findOpened(String loggedAccountEmail, String search, Pageable pageable) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        Specification<Lead> spec = findAllByDeletedFalse(userIds)
                .and(searchByCustomFields(search));
        if (pageable.getSort().getOrderFor("process") != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, leadRepository.findAll(spec));
        } else {
            return leadRepository.findAll(spec, pageable).map(this::mapToLeadOnListDTO);
        }
    }

    public Page<LeadOnListDTO> findAllForApproving(String loggedAccountEmail, String search, Pageable pageable) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        Specification<Lead> spec = findAllByAcceptanceUserIdAndDeletedFalse(userIds)
                .and(searchByCustomFields(search));
        if (pageable.getSort().getOrderFor("process") != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, leadRepository.findAll(spec));
        } else {
            return leadRepository.findAll(spec, pageable).map(this::mapToLeadOnListDTO);
        }
    }

    public Page<LeadOnListDTO> findAllCreatedToday(String loggedAccountEmail, String search, Pageable pageable) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        Specification<Lead> spec = findAllByCreatedAfterAndDeletedFalse(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), userIds)
                .and(searchByCustomFields(search));
        if (pageable.getSort().getOrderFor("process") != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, leadRepository.findAll(spec));
        } else {
            return leadRepository.findAll(spec, pageable).map(this::mapToLeadOnListDTO);
        }
    }

    public Page<LeadOnListDTO> findAllRecentlyEdited(String loggedAccountEmail, String search, Pageable pageable) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        Specification<Lead> spec = findAllByUpdatedAfterAndDeletedFalse(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), userIds)
                .and(searchByCustomFields(search));
        if (pageable.getSort().getOrderFor("process") != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, leadRepository.findAll(spec));
        } else {
            return leadRepository.findAll(spec, pageable).map(this::mapToLeadOnListDTO);
        }
    }

    public Page<LeadOnListDTO> findAllRecentlyViewed(String loggedAccountEmail, String search, Pageable pageable) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        Specification<Lead> spec = findAllByLastViewedAfterAndDeletedFalse(LocalDateTime.of(LocalDate.now(),
                LocalTime.MIDNIGHT).minusDays(3), getUserId(loggedAccountEmail), userIds)
                .and(searchByCustomFields(search));
        if (pageable.getSort().getOrderFor("process") != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, leadRepository.findAll(spec));
        } else {
            return leadRepository.findAll(spec, pageable).map(this::mapToLeadOnListDTO);
        }
    }

    public Page<LeadOnListDTO> findAllClosed(String loggedAccountEmail, String search, Pageable pageable, Lead.CloseResult closeResult) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        Specification<Lead> spec = LeadSpecifications.findAllClosed(userIds, closeResult)
                .and(searchByCustomFields(search));
        if (pageable.getSort().getOrderFor("process") != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, leadRepository.findAll(spec));
        } else {
            return leadRepository.findAll(spec, pageable).map(this::mapToLeadOnListDTO);
        }
    }

    public Page<LeadOnListDTO> findAllConverted(String loggedAccountEmail, String search, Pageable pageable) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        Specification<Lead> spec = LeadSpecifications.findAllConverted(userIds)
                .and(searchByCustomFields(search));
        if (pageable.getSort().getOrderFor("process") != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, leadRepository.findAll(spec));
        } else {
            return leadRepository.findAll(spec, pageable).map(this::mapToLeadOnListDTO);
        }
    }

    public Page<LeadOnListDTO> findAllWithoutKeeper(String loggedAccountEmail, Pageable pageable) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        List<Lead> leads = leadRepository.findAllWithoutKeeper(userIds);
        return convertSortAndPage(pageable, leads);
    }

    public Page<MobileLeadOnListDTO> findAllForMobile(String loggedAccountEmail, Pageable pageable, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return leadRepository.findAllByDeletedFalseForMobile(pageable, userIds, Strings.nullToEmpty(search)).map(this::mapToMobileLeadOnListDTO);
    }

    public Page<MobileLeadOnListDTO> findAllForApprovingForMobile(String loggedAccountEmail, Pageable pageable, String search) throws BadRequestException {
        Long userId = userService.getUserId(loggedAccountEmail);
        return leadRepository.findAllByAcceptanceUserIdAndDeletedFalseForMobile(pageable, userId, Strings.nullToEmpty(search)).map(this::mapToMobileLeadOnListDTO);
    }

    public Page<MobileLeadOnListDTO> findAllCreatedTodayForMobile(String loggedAccountEmail, Pageable pageable, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return leadRepository.findAllByCreatedAfterAndDeletedFalseForMobile(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), pageable, userIds, Strings.nullToEmpty(search))
                .map(this::mapToMobileLeadOnListDTO);
    }

    public Page<MobileLeadOnListDTO> findAllRecentlyEditedForMobile(String loggedAccountEmail, Pageable pageable, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return leadRepository.findAllByUpdatedAfterAndDeletedFalseForMobile(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3), pageable, userIds, Strings.nullToEmpty(search))
                .map(this::mapToMobileLeadOnListDTO);
    }

    public Page<MobileLeadOnListDTO> findAllRecentlyViewedForMobile(String loggedAccountEmail, Pageable pageable, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return leadRepository.findAllByLastViewedAfterAndDeletedFalseForMobile(LocalDateTime.of(LocalDate.now(),
                LocalTime.MIDNIGHT).minusDays(3), pageable, getUserId(loggedAccountEmail), userIds, Strings.nullToEmpty(search))
                .map(this::mapToMobileLeadOnListDTO);
    }

    public Page<MobileLeadOnListDTO> findAllClosedForMobile(String loggedAccountEmail, Pageable pageable, Lead.CloseResult closeResult, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return leadRepository.findAllClosedForMobile(pageable, userIds, closeResult, Strings.nullToEmpty(search)).map(this::mapToMobileLeadOnListDTO);
    }

    public Page<MobileLeadOnListDTO> findAllConvertedForMobile(String loggedAccountEmail, Pageable pageable, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return leadRepository.findAllConvertedForMobile(pageable, userIds, Strings.nullToEmpty(search)).map(this::mapToMobileLeadOnListDTO);
    }

    public List<LeadSearchVariantsDtoOut> findAllSearchVariants(String loggedAccountEmail, String toSearch) throws BadRequestException {
        List<LeadSearchVariantsDtoOut> searchVariants = new ArrayList<>();

        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        Specification<Lead> commonSpec = findByDeleted(false)
                .and(findByLeadKeeperIdIn(userIds)
                        .or(findCreatorIdIn(userIds)));

        searchVariants.add(new LeadSearchVariantsDtoOut()
                .setType(FIRST_NAME)
                .setVariants(leadRepository.findAll(commonSpec.and(findByFirstName(toSearch))).stream()
                        .map(Lead::getFirstName)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        searchVariants.add(new LeadSearchVariantsDtoOut()
                .setType(LAST_NAME)
                .setVariants(leadRepository.findAll(commonSpec.and(findByLastName(toSearch))).stream()
                        .map(Lead::getLastName)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        searchVariants.add(new LeadSearchVariantsDtoOut()
                .setType(COMPANY_NAME)
                .setVariants(leadRepository.findAll(commonSpec.and(findByCompanyName(toSearch))).stream()
                        .map(lead -> lead.getCompany().getCompanyName())
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        searchVariants.add(new LeadSearchVariantsDtoOut()
                .setType(COMPANY_CITY)
                .setVariants(leadRepository.findAll(commonSpec.and(findByCompanyCity(toSearch))).stream()
                        .map(lead -> lead.getCompany().getCity())
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        searchVariants.add(new LeadSearchVariantsDtoOut()
                .setType(PHONE)
                .setVariants(leadRepository.findAll(commonSpec.and(findByPhone(toSearch))).stream()
                        .map(Lead::getPhone)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        searchVariants.add(new LeadSearchVariantsDtoOut()
                .setType(EMAIL)
                .setVariants(leadRepository.findAll(commonSpec.and(findByEmail(toSearch))).stream()
                        .map(Lead::getEmail)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        List<Lead> leadWithNotEmptySourceOrSubSourceOfAcquisition = leadRepository.findAll(
                commonSpec.and(findBySourceOfAcquisitionIsNonNull())
                        .or(findBySubSourceOfAcquisitionIsNonNull()));
        List<Long> sourceAndSubSourceOfAcquisitionIdList = getSourceAndSubSourceOfAcquisitionIdList(leadWithNotEmptySourceOrSubSourceOfAcquisition);
        searchVariants.add(new LeadSearchVariantsDtoOut()
                .setType(SOURCE)
                .setVariants(wordRepository.findAll(WordSpecifications.findByIdIn(sourceAndSubSourceOfAcquisitionIdList)
                        .and(WordSpecifications.findByName(toSearch))).stream()
                        .map(Word::getName)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        return searchVariants;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {BadRequestException.class, FlowableObjectNotFoundException.class})
    public LeadSaveDTO createLead(String loggedAccountEmail, LeadSaveDTO dto) throws BadRequestException {
        LeadSaveDTO leadSaveDTO = accessService.assignAndVerify(dto);
        ProcessInstance processInstance = processEngineHelperService.startProcessInstance(dto.getProcessDefinitionId());
        Optional<UserTask> informationTask = processEngineHelperService.getFirstInformationTask(processInstance.getProcessInstanceId());
        informationTask.ifPresent(value -> processEngineHelperService.completeInformationTask(value, processInstance.getId()));
        Lead lead = LeadUtil.setDTOValuesToLead(leadSaveDTO, new Lead());
        Long creatorId = getUserId(loggedAccountEmail);
        lead.setCreator(creatorId);
        lead.setProcessInstanceId(processInstance.getProcessInstanceId());
        if (leadSaveDTO.getAdditionalPhones() != null && !leadSaveDTO.getAdditionalPhones().isEmpty()) {
            phoneNumberService.setLeadAdditionalPhoneNumbers(lead, leadSaveDTO.getAdditionalPhones());
        }
        String leadName = lead.getFirstName() != null ?
                lead.getFirstName() + " " + lead.getLastName() : lead.getLastName();
        startJobForActiveProcessTask(processInstance, lead.getLeadKeeperId(), leadName);
        Lead savedLead = leadRepository.save(lead);
        setMainNotes(savedLead, leadSaveDTO.getNotes(), loggedAccountEmail);
        savedLead.getAdditionalPhones().forEach(phone -> phone.setType(wordRepository.findOne(WordSpecifications.findByPhoneId(phone.getId())).get()));
        return LeadUtil.fromLead(savedLead, dto.getProcessDefinitionId(), dto.getNotes());
    }


    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {EntityNotFoundException.class, BadRequestException.class})
    public LeadSaveDTO modifyLead(String loggedAccountEmail, Long id, LeadSaveDTO dto) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        LeadSaveDTO leadSaveDTO = accessService.assignAndVerify(dto);
        Lead byId = leadRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        verifyIfUserHasPermissionAndThrowIfNot(loggedAccountEmail, byId);
        verifyIfLeadNotConvertedToOpportunity(byId);
        Lead lead = LeadUtil.setDTOValuesToLead(leadSaveDTO, byId);
        lead.setAcceptanceUserId(byId.getAcceptanceUserId());
        lead.setUpdated(LocalDateTime.now());
        phoneNumberService.modifyLeadAdditionalPhoneNumbers(lead, leadSaveDTO.getAdditionalPhones());
        Lead savedLead = leadRepository.save(lead);
        setMainNotes(savedLead, leadSaveDTO.getNotes(), loggedAccountEmail);
        if (savedLead.getAdditionalPhones() != null && !savedLead.getAdditionalPhones().isEmpty()) {
            savedLead.getAdditionalPhones().forEach(phone -> phone.setType(wordRepository.findOne(WordSpecifications.findByPhoneId(phone.getId())).get()));
        }
        return LeadUtil.fromLead(savedLead, dto.getProcessDefinitionId(), dto.getNotes());
    }

    public List<CrmObject> searchSimilarObjects(SearchSimilarObjectsDto searchSimilarObjectsDto) {
        // search leads
        List<CrmObject> leads = leadRepository.searchLeadsForLeadCreation(
                Strings.nullToEmpty(searchSimilarObjectsDto.getFirstName()),
                Strings.nullToEmpty(searchSimilarObjectsDto.getLastName()),
                Strings.nullToEmpty(searchSimilarObjectsDto.getEmail())).stream()
                .map(c -> new CrmObject(c.getId(), CrmObjectType.lead,
                        Strings.nullToEmpty(c.getFirstName()) + " " + Strings.nullToEmpty(c.getLastName()), c.getCreated()))
                .collect(Collectors.toList());
        // search contacts
        List<CrmObject> contacts = contactRepository.searchContactsForLeadCreation(
                Strings.nullToEmpty(searchSimilarObjectsDto.getFirstName()),
                Strings.nullToEmpty(searchSimilarObjectsDto.getLastName()),
                Strings.nullToEmpty(searchSimilarObjectsDto.getEmail()),
                Strings.nullToEmpty(searchSimilarObjectsDto.getPhone())).stream()
                .map(c -> new CrmObject(c.getId(), CrmObjectType.contact,
                        Strings.nullToEmpty(c.getName()) + " " + Strings.nullToEmpty(c.getSurname()), c.getCreated()))
                .collect(Collectors.toList());
        // search customers
        List<CrmObject> customers = customerRepository.searchCustomersForLeadCreation(
                Strings.nullToEmpty(searchSimilarObjectsDto.getPhone()),
                Strings.nullToEmpty(searchSimilarObjectsDto.getCompanyName()),
                Strings.nullToEmpty(searchSimilarObjectsDto.getWwwAddress()),
                Strings.nullToEmpty(searchSimilarObjectsDto.getStreetAndNumber())).stream()
                .map(c -> new CrmObject(c.getId(), CrmObjectType.customer, Strings.nullToEmpty(c.getName()), c.getCreated()))
                .collect(Collectors.toList());
        // search opportunity
        Set<Long> contactIds = contacts.stream().map(CrmObject::getId).collect(Collectors.toSet());
        if (contactIds.size() == 0) {
            contactIds.add(null);
        }
        Set<Long> customerIds = customers.stream().map(CrmObject::getId).collect(Collectors.toSet());
        if (customerIds.size() == 0) {
            customerIds.add(null);
        }
        List<CrmObject> opportunities = opportunityRepository.searchOpportunitiesForLeadCreation(contactIds, customerIds)
                .stream().map(c -> new CrmObject(c.getId(), CrmObjectType.opportunity, Strings.nullToEmpty(c.getName()), c.getCreated()))
                .collect(Collectors.toList());

        return Stream.concat(Stream.concat(contacts.stream(), customers.stream()),
                Stream.concat(opportunities.stream(), leads.stream()))
                .collect(Collectors.toList());
    }

    public void closeLead(String loggedAccountEmail, Long id, Lead.CloseResult closeResult, Lead.RejectionReason rejectionReason) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Lead byId = leadRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        verifyIfUserHasPermissionAndThrowIfNot(loggedAccountEmail, byId);
        byId.setClosed(true);
        byId.setCloseResult(closeResult);
        if (rejectionReason != null) {
            byId.setRejectionReason(rejectionReason);
        }
        leadRepository.save(byId);
    }

    public void deleteLead(Long id) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Lead byId = leadRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        verifyIfUserHasPermissionAndThrowIfNot(CurrentRequestUtil.getLoggedInAccountEmail(), byId);
        byId.setDeleted(true);
        byId.setMainNoteId(null);
        leadRepository.save(byId);
        RemoveCrmObject.addObjectToRemove(CrmObjectType.lead, id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteLeads(List<Long> ids) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        for (Long id : ids) {
            deleteLead(id);
        }
    }

    public LeadSaveDTO getLeadForEditById(Long id) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Lead byId = leadRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        verifyIfUserHasPermissionAndThrowIfNot(CurrentRequestUtil.getLoggedInAccountEmail(), byId);
        ProcessInstance processInstance = processEngineHelperService.getProcessInstance(byId.getProcessInstanceId());
        if (byId.getMainNoteId() != null) {
            try {
                NoteDTO noteById = associationClient.getNoteById(byId.getMainNoteId(), CurrentRequestUtil.getLoggedInAccountEmail());
                return LeadUtil.fromLead(byId, processInstance.getProcessDefinitionId(), noteById.getContent());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return LeadUtil.fromLead(byId, processInstance.getProcessDefinitionId());
    }

    public LeadDetailsDTO getLeadById(Long id) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Lead lead = leadRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return createLeadDetailsDTO(lead);
    }

    public List<Lead> findAllContaining(String pattern) {
        return leadRepository.findByNameOrSurnameContaining(pattern);
    }

    public List<Lead> findByIds(List<Long> ids) {
        return leadRepository.findByIds(ids);
    }

    public List<Lead> findAllAvailableForUserAndContaining(String loggedAccountEmail, String term) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return leadRepository.findAllAvailableForUserAndContaining(term, userIds);
    }

    public List<Long> getAllLeadIdWithAccess(String loggedAccountEmail) throws BadRequestException {
        Long userId = getUserId(loggedAccountEmail);
        return leadRepository.findAllAvailableForUser(userId);
    }

    public LeadDetailsDTO findLeadByOpportunityId(Long id) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Lead lead = leadRepository.findByOpportunityId(id).orElseThrow(EntityNotFoundException::new);
        return createLeadDetailsDTO(lead);
    }

    @Transactional(rollbackFor = FeignException.class)
    public void changeLeadsKeeper(String loggedAccountEmail, LeadsKeeperDTO leadsKeeper) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        verifyIfNewKeeperExists(leadsKeeper.getLeadKeeper());
        for (Long leadId : leadsKeeper.getLeadsIds()) {
            Lead lead = leadRepository.findById(leadId).orElseThrow(EntityNotFoundException::new);
            verifyIfUserHasPermissionAndThrowIfNot(loggedAccountEmail, lead);
            lead.setLeadKeeperId(leadsKeeper.getLeadKeeper());
            leadRepository.save(lead);
        }
    }

    private void startJobForActiveProcessTask(ProcessInstance processInstance, Long keeperId, String leadName) {
        UserTask activeTask = processEngineHelperService.getActiveUserTask(processInstance);
        String taskId = activeTask.getId();
        String taskName = activeTask.getName();
        Optional<Long> daysToCompleteOpt = getDaysToCompleteTask(activeTask);
        daysToCompleteOpt.ifPresent(daysToComplete -> notificationSchedulerService.scheduleNotificationJob(LEAD, processInstance.getProcessInstanceId(), keeperId, leadName, taskId, taskName, daysToComplete));
    }

    private LeadDetailsDTO createLeadDetailsDTO(Lead lead) throws PermissionDeniedException, BadRequestException {
        verifyIfUserHasPermissionAndThrowIfNot(CurrentRequestUtil.getLoggedInAccountEmail(), lead);
        LeadDetailsDTO dto = LeadDetailsDTO.fromLead(lead);
        ProcessInstance processInstance = processEngineHelperService.getProcessInstance(lead.getProcessInstanceId());
        processEngineHelperService.setTasksToDTO(lead.getProcessInstanceId(), dto, processInstance);
        Long userId = getUserId(CurrentRequestUtil.getLoggedInAccountEmail());
        updateLastViewedTime(lead, userId);
        return dto;
    }

    private void updateLastViewedTime(Lead lead, Long userId) {
        LastViewedLead newLastViewed = LastViewedLead.builder().userId(userId).lead(lead).build();
        LastViewedLead lastViewedLead = (lead.getLastViewedLeads() == null) ? newLastViewed : lead.getLastViewedLeads().stream()
                .filter(lv -> lv.getUserId().equals(userId)).findFirst()
                .orElse(newLastViewed);

        lastViewedLead.setLastViewed(LocalDateTime.now());
        lastViewedLeadRepository.save(lastViewedLead);
    }

    private void setMainNotes(Lead savedLead, String notes, String loggedAccountEmail) throws BadRequestException {
        if (savedLead.getMainNoteId() != null) {
            if (notes != null && !notes.trim().isEmpty()) {
                NoteDTO noteDTO = new NoteDTO();
                noteDTO.setContent(notes);
                noteDTO.setFormattedContent(notes);
                noteDTO.setId(savedLead.getMainNoteId());
                noteDTO.setObjectId(savedLead.getId());
                noteDTO.setObjectType(NoteDTO.ObjectTypeEnum.LEAD);
                try {
                    NoteDTO mainNote = associationClient.modifyNote(savedLead.getMainNoteId(), noteDTO, loggedAccountEmail);
                    savedLead.setMainNoteId(mainNote.getId());
                } catch (Exception ex) {
                    throw new BadRequestException("Error while modifying the note.");
                }
            } else {
                try {
                    associationClient.deleteById(savedLead.getMainNoteId(), loggedAccountEmail);
                    savedLead.setMainNoteId(null);
                } catch (Exception ex) {
                    throw new BadRequestException("Error while deleting the note.");
                }
            }
        } else {
            if (notes != null && !notes.trim().isEmpty()) {
                NoteDTO noteDTO = new NoteDTO();
                noteDTO.setContent(notes);
                noteDTO.setFormattedContent(notes);
                noteDTO.setObjectId(savedLead.getId());
                noteDTO.setObjectType(NoteDTO.ObjectTypeEnum.LEAD);
                try {
                    NoteDTO mainNote = associationClient.createNote(noteDTO, loggedAccountEmail);
                    savedLead.setMainNoteId(mainNote.getId());
                } catch (Exception ex) {
                    throw new BadRequestException("Error while creating the note.");
                }

            }
        }
    }

    private LeadOnListDTO mapToLeadOnListDTO(Lead lead) {
        ProcessInstance processInstance = processEngineHelperService.getProcessInstance(lead.getProcessInstanceId());
        Task task = processEngineHelperService.getActiveTask(lead.getProcessInstanceId());

        ProcessDefinition processDefinition = processEngineHelperService.getProcessDefinition(processInstance.getProcessDefinitionId());
        String position = null;
        if (lead.getPosition() != null) {
            Optional<Word> positionWord = wordRepository.findById(lead.getPosition());
            if (positionWord.isPresent()) {
                position = positionWord.get().getName();
            }
        }
        return new LeadOnListDTO(lead, processDefinition.getName(), processDefinition.getVersion(), position, getCurrentTaskNumber(task),
                (long) processEngineHelperService.getListOfTasks(processInstance).size() - 1);
    }

    private MobileLeadOnListDTO mapToMobileLeadOnListDTO(Lead lead) {
        ProcessInstance processInstance = processEngineHelperService.getProcessInstance(lead.getProcessInstanceId());
        ProcessColor processColor = processEngineReadService.getProcessColor(processEngineHelperService.getMainProcess(processInstance));
        Task task = processEngineHelperService.getActiveTask(lead.getProcessInstanceId());
        return new MobileLeadOnListDTO(lead, getCurrentTaskNumber(task),
                (long) processEngineHelperService.getListOfTasks(processInstance).size() - 1, processColor);
    }

    private long getCurrentTaskNumber(Task task) {
        return Long.parseLong(task.getTaskDefinitionKey().replaceAll("task", ""));
    }

    private Long getUserId(String accountEmail) throws BadRequestException {
        try {
            return userClient.getMyAccountId(accountEmail);
        } catch (Exception e) {
            throw new BadRequestException("user not found");
        }
    }

    private void verifyIfUserHasPermissionAndThrowIfNot(String loggedAccountEmail, Lead byId) throws PermissionDeniedException, BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        if (!userIds.contains(byId.getLeadKeeperId()) && !userIds.contains(byId.getCreator())) {
            throw new PermissionDeniedException("user trying to modify lead that he hasn't permission to");
        }
    }

    private void verifyIfLeadNotConvertedToOpportunity(Lead lead) throws BadRequestException {
        if (lead.getConvertedToOpportunity()) {
            throw new BadRequestException("Lead converted to opportunity can not be modified");
        }
    }

    private void verifyIfNewKeeperExists(Long leadKeeper) throws EntityNotFoundException, BadRequestException {
        String user = userService.getUserNameAndSurname(leadKeeper);
        if (Objects.isNull(user)) {
            throw new EntityNotFoundException("user not found");
        }
    }

    private PageImpl<LeadOnListDTO> convertSortAndPage(Pageable pageable, List<Lead> leads) {
        List<LeadOnListDTO> leadOnListDTOs = leads.stream()
                .map(this::mapToLeadOnListDTO)
                .sorted(SortStrategyFactory.create(pageable.getSort()))
                .collect(Collectors.toList());

        PagedListHolder<LeadOnListDTO> pagedListHolder = new PagedListHolder<>(leadOnListDTOs);
        pagedListHolder.setPageSize(pageable.getPageSize());
        pagedListHolder.setPage(pageable.getPageNumber());

        return new PageImpl<>(
                pagedListHolder.getPageList(),
                PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort()),
                leadOnListDTOs.size()
        );
    }

    private Optional<Long> getDaysToCompleteTask(UserTask activeTask) {
        if (activeTask.getAttributes().containsKey("daysToComplete")) {
            return Optional.ofNullable(Long.parseLong(activeTask.getAttributes().get("daysToComplete").get(0).getValue()));
        }
        return Optional.empty();
    }

    private List<Long> getSourceAndSubSourceOfAcquisitionIdList(List<Lead> leadList) {
        return Stream.concat(
                leadList.stream()
                        .filter(lead -> lead.getSourceOfAcquisition() != null)
                        .map(Lead::getSourceOfAcquisition),
                leadList.stream()
                        .filter(lead -> lead.getSubSourceOfAcquisition() != null)
                        .map(Lead::getSubSourceOfAcquisition))
                .distinct()
                .collect(Collectors.toList());
    }

}
