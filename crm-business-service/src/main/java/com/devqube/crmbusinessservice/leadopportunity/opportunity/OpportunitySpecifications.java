package com.devqube.crmbusinessservice.leadopportunity.opportunity;

import com.devqube.crmbusinessservice.dictionary.Word;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.LastViewedOpportunity;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.time.LocalDateTime;
import java.util.List;

import static javax.persistence.criteria.JoinType.LEFT;

public class OpportunitySpecifications {

    public static Specification<Opportunity> findAll(List<Long> userIds) {
        return findByOwnerOrCreatorIdIn(userIds);
    }

    public static Specification<Opportunity> findAllByCreatedAfter(LocalDateTime date, List<Long> userIds) {
        return findByOwnerOrCreatorIdIn(userIds)
                .and(findCreatedAfterExclusive(date));
    }

    public static Specification<Opportunity> findAllOpened(List<Long> userIds) {
        return findByClosingDateIsNull()
                .and(findByOwnerOrCreatorIdIn(userIds));
    }

    public static Specification<Opportunity> findAllClosedWithResult(List<Long> userIds,Opportunity.FinishResult finishResult) {
        return findByClosingDateIsNotNull()
                .and(findByFinishResult(finishResult))
                .and(findByOwnerOrCreatorIdIn(userIds));
    }


    public static Specification<Opportunity> findAllByUpdatedAfter(LocalDateTime date, List<Long> userIds) {
        return findByOwnerOrCreatorIdIn(userIds)
                .and(findUpdatedAfterExclusive(date));
    }

    public static Specification<Opportunity> findAllByLastViewedAfter(LocalDateTime date, Long userId, List<Long> userIds) {
        return findByOwnerOrCreatorIdIn(userIds)
                .and(findLastViewedAfterExclusive(date, userId));
    }

    public static Specification<Opportunity> findAllByAcceptanceUserIdAndOpened(List<Long> userIds) {
        return findByClosingDateIsNull()
                .and(findByAcceptanceUserIdIn(userIds)
                        .or(findByAcceptanceUserIdIsNotNull()
                                .and(findByOwnerOrCreatorIdIn(userIds))));
    }
    public static Specification<Opportunity> searchByCustomFields(String search) {
        return findByName(search)
                .or(findByCustomerName(search))
                .or(findByContactName(search))
                .or(findBySourceOrSubSourceOfAcquisition(search));
    }

    public static Specification<Opportunity> findByClosingDateIsNull() {
        return (root, query, cb) -> root.get("closingDate").isNull();
    }
    public static Specification<Opportunity> findByClosingDateIsNotNull() {
        return (root, query, cb) -> root.get("closingDate").isNotNull();
    }

    public static Specification<Opportunity> findByName(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("name")), "%" + toSearch.toLowerCase() + "%");
    }

    public static Specification<Opportunity> findByCustomerName(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("customer").get("name")), "%" + toSearch.toLowerCase() + "%");
    }

    public static Specification<Opportunity> findByCustomerId(Long toSearch) {
        return (root, query, cb) -> root.get("customer").get("id").in(toSearch);
    }

    public static Specification<Opportunity> findByContactId(Long toSearch) {
        return (root, query, cb) -> root.get("contact").get("id").in(toSearch);
    }

    public static Specification<Opportunity> findByContactName(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("contact").get("name")), "%" + toSearch.toLowerCase() + "%");
    }

    public static Specification<Opportunity> findByAcceptanceUserIdIn(List<Long> userIds) {
        return (root, query, cb) -> root.get("acceptanceUserId").in(userIds);
    }

    public static Specification<Opportunity> findByAcceptanceUserIdIsNotNull() {
        return (root, query, cb) -> root.get("acceptanceUserId").isNotNull();

    }public static Specification<Opportunity> findByFinishResult(Opportunity.FinishResult finishResult) {
        return (root, query, cb) -> cb.equal(root.get("finishResult"), finishResult);
    }
    public static Specification<Opportunity> findBySourceOrSubSourceOfAcquisition(String toSearch) {
        return (root, query, cb) -> {
            Subquery<Long> wordSubquery = query.subquery(Long.class);
            Root<Word> wordSubqueryRoot = wordSubquery.from(Word.class);
            wordSubquery.select(wordSubqueryRoot.get("id"))
                    .where(cb.like(cb.lower(wordSubqueryRoot.get("name")), "%" + toSearch.toLowerCase() + "%"));

            return cb.or(
                    cb.in(root.get("sourceOfAcquisition")).value(wordSubquery),
                    cb.in(root.get("subSourceOfAcquisition")).value(wordSubquery)
            );
        };
    }

    public static Specification<Opportunity> findBySourceOfAcquisitionIsNonNull() {
        return (root, query, cb) -> cb.isNotNull(root.get("sourceOfAcquisition"));
    }

    public static Specification<Opportunity> findBySubSourceOfAcquisitionIsNonNull() {
        return (root, query, cb) -> cb.isNotNull(root.get("subSourceOfAcquisition"));
    }

    public static Specification<Opportunity> findByOwnerOrCreatorIdIn(List<Long> userIds) {
        return findByOwnerIdIn(userIds)
                .or(findByCreatorIdIn(userIds));
    }

    public static Specification<Opportunity> findByOwnerIdIn(List<Long> userIds) {
        return (root, query, cb) -> cb.or(
                root.get("ownerOneId").in(userIds),
                root.get("ownerTwoId").in(userIds),
                root.get("ownerThreeId").in(userIds));
    }

    public static Specification<Opportunity> findByCreatorIdIn(List<Long> userIds) {
        return (root, query, cb) -> root.get("creator").in(userIds);
    }

    public static Specification<Opportunity> findCreatedAfterExclusive(LocalDateTime date) {
        return (root, query, cb) -> cb.greaterThan(root.get("created"), date);
    }

    public static Specification<Opportunity> findUpdatedAfterExclusive(LocalDateTime date) {
        return (root, query, cb) -> cb.greaterThan(root.get("updated"), date);
    }

    public static Specification<Opportunity> findLastViewedAfterExclusive(LocalDateTime date, Long userId) {
        return (root, query, cb) -> {
            Join<Lead, LastViewedOpportunity> lastViewedOpportunities = root.joinSet("lastViewedOpportunities", LEFT);
            return cb.and(
                    cb.equal(lastViewedOpportunities.get("userId"), userId),
                    cb.greaterThan(lastViewedOpportunities.get("lastViewed"), date));
        };
    }

}
