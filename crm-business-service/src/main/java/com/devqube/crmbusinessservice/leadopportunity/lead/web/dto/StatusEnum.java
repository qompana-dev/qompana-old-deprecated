package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum StatusEnum {
    NOT_ELIGIBLE("NOT_ELIGIBLE"),

    NEW("NEW"),

    DURING_RECOGNITION("DURING_RECOGNITION"),

    UNDER_THE_CUSTODY("UNDER_THE_CUSTODY"),

    ELIGIBLE("ELIGIBLE");

    private String value;

    StatusEnum(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String text) {
        for (StatusEnum b : StatusEnum.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
}
