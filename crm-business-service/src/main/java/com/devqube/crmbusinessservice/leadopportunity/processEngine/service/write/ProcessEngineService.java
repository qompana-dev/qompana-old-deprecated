package com.devqube.crmbusinessservice.leadopportunity.processEngine.service.write;

import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.NotificationActionDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.IncorrectNotificationTypeException;
import com.devqube.crmshared.exception.KafkaSendMessageException;

public interface ProcessEngineService {
    void checkIfBeingAcceptedAndThrowExceptionIfYes(String id) throws BadRequestException, EntityNotFoundException;

    boolean completeTask(String processInstanceId) throws BadRequestException, KafkaSendMessageException, EntityNotFoundException, IncorrectNotificationTypeException;

    void goBackToPreviousTask(String processInstanceId, Integer numberOfSteps) throws BadRequestException, EntityNotFoundException;

    void acceptTask(NotificationActionDto notificationActionDto, String processInstanceId) throws EntityNotFoundException, KafkaSendMessageException, IncorrectNotificationTypeException, BadRequestException;

    void declineTask(NotificationActionDto notificationActionDto, String processInstanceId) throws EntityNotFoundException, KafkaSendMessageException, IncorrectNotificationTypeException, BadRequestException;
}
