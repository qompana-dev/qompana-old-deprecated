package com.devqube.crmbusinessservice.leadopportunity.lead;

import com.devqube.crmbusinessservice.leadopportunity.lead.model.LastViewedLead;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LastViewedLeadRepository extends JpaRepository<LastViewedLead, Long> {
}
