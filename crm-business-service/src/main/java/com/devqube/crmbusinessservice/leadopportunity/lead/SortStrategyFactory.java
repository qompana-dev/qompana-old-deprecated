package com.devqube.crmbusinessservice.leadopportunity.lead;

import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadOnListDTO;
import org.springframework.data.domain.Sort;

import java.util.Comparator;
import java.util.Objects;

class SortStrategyFactory {

    static Comparator<LeadOnListDTO> create(Sort sort) {
        Comparator<LeadOnListDTO> comparator = null;
        for (Sort.Order order: sort) {
            comparator = addComparator(comparator, order);
        }
        return comparator;
    }

    private static Comparator<LeadOnListDTO> addComparator(Comparator<LeadOnListDTO> comparator, Sort.Order order) {
        if(Objects.isNull(comparator)) {
            return getStrategy(order);
        }

        return comparator.thenComparing(getStrategy(order));
    }

    private static Comparator<LeadOnListDTO> getStrategy(Sort.Order order) {
        SortStrategy sortStrategy;

        switch (order.getProperty()) {
            case "updated":
                sortStrategy = new SortByUpdated();
                break;
            case "firstName":
                sortStrategy = new SortByFirstName();
                break;
            case "position":
                sortStrategy = new SortByPosition();
                break;
            case "company.companyName":
                sortStrategy = new SortByCompanyName();
                break;
            case "company.city":
                sortStrategy = new SortByCity();
                break;
            case "email":
                sortStrategy = new SortByEmail();
                break;
            case "created":
                sortStrategy = new SortByCreated();
                break;
            case "process":
                sortStrategy = new SortByProcess();
                break;
            case "lastViewedDate":
                sortStrategy = new SortByLastViewedDate();
                break;

            default: sortStrategy = new SortById();
        }

        if(order.getDirection().isDescending()) {
            return sortStrategy.getStrategy().reversed();
        }
        return sortStrategy.getStrategy();
    }

    private interface SortStrategy {
        Comparator<LeadOnListDTO> getStrategy();
    }

    private static class SortById implements SortStrategy {
        @Override
        public Comparator<LeadOnListDTO> getStrategy() {
            return Comparator.comparing(LeadOnListDTO::getId);
        }
    }

    private static class SortByUpdated implements SortStrategy {
        @Override
        public Comparator<LeadOnListDTO> getStrategy() {
            return Comparator.comparing(LeadOnListDTO::getUpdateDate, Comparator.nullsFirst(Comparator.naturalOrder()))
                    .thenComparing(LeadOnListDTO::getId, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }

    private static class SortByFirstName implements SortStrategy {
        @Override
        public Comparator<LeadOnListDTO> getStrategy() {
            return Comparator.comparing(LeadOnListDTO::getFirstName, Comparator.nullsFirst(Comparator.naturalOrder()))
                    .thenComparing(LeadOnListDTO::getLastName, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }

    private static class SortByPosition implements SortStrategy {
        @Override
        public Comparator<LeadOnListDTO> getStrategy() {
            return Comparator.comparing(LeadOnListDTO::getPosition, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }

    private static class SortByCompanyName implements SortStrategy {
        @Override
        public Comparator<LeadOnListDTO> getStrategy() {
            return Comparator.comparing(LeadOnListDTO::getCompany, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }

    private static class SortByCity implements SortStrategy {
        @Override
        public Comparator<LeadOnListDTO> getStrategy() {
            return Comparator.comparing(LeadOnListDTO::getCity, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }

    private static class SortByEmail implements SortStrategy {
        @Override
        public Comparator<LeadOnListDTO> getStrategy() {
            return Comparator.comparing(LeadOnListDTO::getEmail, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }

    private static class SortByCreated implements SortStrategy {
        @Override
        public Comparator<LeadOnListDTO> getStrategy() {
            return Comparator.comparing(LeadOnListDTO::getCreationDate, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }

    private static class SortByProcess implements SortStrategy {
        @Override
        public Comparator<LeadOnListDTO> getStrategy() {
            return Comparator.comparing(LeadOnListDTO::getProcessInstanceName, Comparator.nullsFirst(Comparator.naturalOrder()))
                    .thenComparing(LeadOnListDTO::getProcessInstanceVersion, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }

    private static class SortByLastViewedDate implements SortStrategy {
        @Override
        public Comparator<LeadOnListDTO> getStrategy() {
            return Comparator.comparing(LeadOnListDTO::getLastViewedDate, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }
}
