package com.devqube.crmbusinessservice.leadopportunity;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmshared.exception.BadRequestException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountsService {
    private final UserClient userClient;

    public AccountsService(UserClient userClient) {
        this.userClient = userClient;
    }

    public List<Long> getAllEmployeesBySupervisorEmail(String loggedAccountEmail) throws BadRequestException {
        List<Long> usersIds = new ArrayList<>();
        usersIds.add(getUserId(loggedAccountEmail));
        String roleName = userClient.getRoleForUserWithEmail(loggedAccountEmail);
        usersIds.addAll(userClient.getAllEmployeesForRole(roleName));
        return usersIds;
    }

    private Long getUserId(String accountEmail) throws BadRequestException {
        try {
            return userClient.getMyAccountId(accountEmail);
        } catch (Exception e) {
            throw new BadRequestException("user not found");
        }
    }
}
