package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.util;

import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.DictionaryWord;
import com.devqube.crmshared.user.dto.AccountEmail;

public class ImportUtil {

    public static boolean isAccountEmailEqual(AccountEmail accountEmail, String value) {
        String val1 = accountEmail.getName() + " " + accountEmail.getSurname();
        String val2 = accountEmail.getSurname() + " " + accountEmail.getName();
        return value.toLowerCase().equals(val1.toLowerCase()) || value.toLowerCase().equals(val2.toLowerCase());
    }

    public static boolean isSourceOfAcquisition(DictionaryWord dictionaryWord, String value) {
        String val1 = dictionaryWord.getName();
        return value.toLowerCase().equals(val1.toLowerCase());
    }
}
