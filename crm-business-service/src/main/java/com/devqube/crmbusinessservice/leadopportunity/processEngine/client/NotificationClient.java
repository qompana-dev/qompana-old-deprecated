package com.devqube.crmbusinessservice.leadopportunity.processEngine.client;

import com.devqube.crmshared.notification.NotificationActionDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "crm-notification-service", url = "${crm-notification-service.url}")
public interface NotificationClient {
    @RequestMapping(value = "/notifications/{id}", method = RequestMethod.GET)
    UserNotificationDTO getNotificationById(@PathVariable Long id);

    @RequestMapping(value = "/notifications/action/delete", method = RequestMethod.POST)
    void deleteMessage(@RequestBody NotificationActionDto notificationActionDto);
}



