package com.devqube.crmbusinessservice.leadopportunity.processEngine.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProcessDefinitionDTO {
    private String id;
    private String name;
    private int version;
    private String firstTaskName;
}
