package com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.OpportunityPriority;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.*;

import java.time.LocalDateTime;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AuthFilterEnabled
public class OpportunityListDTO {
    @AuthField(controller = "getOpportunities")
    private Long id;
    @AuthField(frontendId = "OpportunityListComponent.nameColumn", controller = "getOpportunities")
    private String name;
    @AuthField(frontendId = "OpportunityListComponent.customerNameColumn", controller = "getOpportunities")
    private String customerName;
    @AuthField(frontendId = "OpportunityListComponent.finishDateColumn", controller = "getOpportunities")
    private LocalDate finishDate;
    @AuthField(frontendId = "OpportunityListComponent.statusColumn", controller = "getOpportunities")
    private Long status;
    @AuthField(frontendId = "OpportunityListComponent.statusColumn", controller = "getOpportunities")
    private Long maxStatus;
    @AuthField(frontendId = "OpportunityListComponent.keeperColumn", controller = "getOpportunities")
    private Long ownerOneId;
    @AuthField(frontendId = "OpportunityListComponent.keeperColumn", controller = "getOpportunities")
    private Long ownerTwoId;
    @AuthField(frontendId = "OpportunityListComponent.keeperColumn", controller = "getOpportunities")
    private Long ownerThreeId;
    @AuthField(frontendId = "OpportunityListComponent.priorityColumn", controller = "getOpportunities")
    private OpportunityPriority priority;
    @AuthField(frontendId = "OpportunityListComponent.subjectOfInterestColumn", controller = "getOpportunities")
    private Long subjectOfInterest;
    @AuthField(controller = "getOpportunities")
    private Long acceptanceUserId;
    @AuthField(controller = "getOpportunities")
    private Opportunity.FinishResult finishResult;
    @AuthField(controller = "getOpportunities")
    private Opportunity.RejectionReason rejectionReason;
    @AuthField(controller = "getOpportunities")
    private String processInstanceName;
    @AuthField(controller = "getOpportunities")
    private int processInstanceVersion;
    @AuthField(controller = "getOpportunities")
    private String processInstanceId;

    @AuthField(frontendId = "OpportunityListComponent.createDateColumn", controller = "getOpportunities")
    private LocalDate createDate;

    // value only locally for sorted policy
    private LocalDateTime updateDate;

    @AuthField(frontendId = "OpportunityListComponent.amountColumn", controller = "getOpportunities")
    private Double amount;

    @AuthField(frontendId = "OpportunityListComponent.currencyColumn", controller = "getOpportunities")
    private String currency;

    @AuthField(controller = "getOpportunities")
    private Boolean canBeMovedToFollowingStep;

    @AuthField(controller = "getOpportunities")
    private String followingStepName;

    @AuthField(controller = "getOpportunities")
    private Boolean canBeRolledBackToPreviousStep;

    @AuthField(controller = "getOpportunities")
    private String previousStepName;



    public OpportunityListDTO(Opportunity opportunity,
                              String processInstanceName,
                              int processInstanceVersion,
                              Long status,
                              Long maxStatus) {
        this.id = opportunity.getId();
        this.name = opportunity.getName();
        if (opportunity.getCustomer() != null) {
            this.customerName = opportunity.getCustomer().getName();
        }
        this.finishDate = opportunity.getFinishDate();
        this.status = status;
        this.maxStatus = maxStatus;
        this.ownerOneId = opportunity.getOwnerOneId();
        this.ownerTwoId = opportunity.getOwnerTwoId();
        this.ownerThreeId = opportunity.getOwnerThreeId();
        this.priority = opportunity.getPriority();
        this.subjectOfInterest = opportunity.getSubjectOfInterest();
        this.finishResult = opportunity.getFinishResult();
        this.rejectionReason = opportunity.getRejectionReason();
        this.processInstanceName = processInstanceName;
        this.processInstanceVersion = processInstanceVersion;
        this.createDate = opportunity.getCreateDate();
        this.updateDate = opportunity.getUpdated() != null ? opportunity.getUpdated() : LocalDateTime.of(opportunity.getCreateDate(), LocalTime.of(12, 00));
        this.amount = opportunity.getAmount();
        this.currency = opportunity.getCurrency();
        this.acceptanceUserId = opportunity.getAcceptanceUserId();
    }
}
