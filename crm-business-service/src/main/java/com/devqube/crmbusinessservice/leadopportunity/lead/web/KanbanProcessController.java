package com.devqube.crmbusinessservice.leadopportunity.lead.web;

import com.devqube.crmbusinessservice.leadopportunity.lead.KanbanLeadService;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.kanban.KanbanLead;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.kanban.KanbanProcessOverview;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.kanban.KanbanTaskDetails;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class KanbanProcessController implements KanbanProcessesApi {

    private final KanbanLeadService kanbanLeadService;

    public KanbanProcessController(KanbanLeadService kanbanLeadService) {
        this.kanbanLeadService = kanbanLeadService;
    }

    @Override
    @AuthController(name = "getProcessesOverview")
    public ResponseEntity<List<KanbanProcessOverview>> getProcessesOverview() {
        try {
            List<KanbanProcessOverview> processesOverview = kanbanLeadService.getProcessesOverview();
            return ResponseEntity.ok(processesOverview);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getProcessKanbanDetails")
    public ResponseEntity<List<KanbanTaskDetails>> getProcessKanbanDetails(String processDefinitionId) {
        try {
            List<KanbanTaskDetails> processDetails = kanbanLeadService.getProcessDetails(processDefinitionId);
            return ResponseEntity.ok(processDetails);
        } catch (EntityNotFoundException | BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "getLeadForKanban")
    public ResponseEntity<KanbanLead> getLeadForKanban(Long id) {
        try {
            KanbanLead leadOnKanbanById = kanbanLeadService.getLeadOnKanbanById(id);
            return ResponseEntity.ok(leadOnKanbanById);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }



}
