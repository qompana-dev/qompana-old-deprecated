package com.devqube.crmbusinessservice.leadopportunity.processEngine.service.write;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.kafka.KafkaServiceImpl;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.AcceptanceStatus;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.client.NotificationClient;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.NotificationActionDto;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineHelperService;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineNotificationService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.IncorrectNotificationTypeException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.history.dto.ObjectHistoryType;
import com.devqube.crmshared.notification.NotificationMsgType;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.google.common.base.Strings;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.task.api.Task;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Service
public class ProcessEngineForLead implements ProcessEngineService {

    private final String statusAcceptance = "statusAcceptance";
    private final LeadRepository leadRepository;
    private final ProcessEngineNotificationService processEngineNotificationService;
    private final TaskService taskService;
    private final ProcessEngineHelperService processEngineHelperService;
    private final NotificationClient notificationClient;
    private final UserClient userClient;
    private final RuntimeService runtimeService;
    private final KafkaServiceImpl kafkaService;

    public ProcessEngineForLead(LeadRepository leadRepository, ProcessEngineNotificationService processEngineNotificationService,
                                TaskService taskService, ProcessEngineHelperService processEngineHelperService,
                                NotificationClient notificationClient, UserClient userClient, RuntimeService runtimeService, KafkaServiceImpl kafkaService) {
        this.leadRepository = leadRepository;
        this.processEngineNotificationService = processEngineNotificationService;
        this.taskService = taskService;
        this.processEngineHelperService = processEngineHelperService;
        this.notificationClient = notificationClient;
        this.userClient = userClient;
        this.runtimeService = runtimeService;
        this.kafkaService = kafkaService;
    }

    @Override
    public void checkIfBeingAcceptedAndThrowExceptionIfYes(String id) throws BadRequestException, EntityNotFoundException {
        Lead lead = getLead(id);
        if (lead.getAcceptanceUserId() != null) {
            throw new BadRequestException("Task is being accepted");
        }
    }

    @Override
    public boolean completeTask(String processInstanceId) throws BadRequestException, KafkaSendMessageException, EntityNotFoundException, IncorrectNotificationTypeException {
        Lead lead = getLead(processInstanceId);
        verifyIfLeadNotConvertedToOpportunity(lead);
        Task task = processEngineHelperService.getTaskByProcessInstanceId(processInstanceId);
        processEngineHelperService.validateTask(lead.getAcceptanceUserId(), task);
        //TODO solve problem with notification
        Long acceptanceUserId = sendNotificationsAndReturnAcceptanceUserId(processInstanceId, lead);
        boolean isAutomaticallyAccepted = completeTaskOrMarkAsDuringAcceptanceOrCompleteAndAccept(lead, task, acceptanceUserId, processInstanceId);
        return acceptanceUserId == null || isAutomaticallyAccepted;
    }

    private Lead getLead(String processInstanceId) throws EntityNotFoundException {
        return leadRepository.findAllByProcessInstanceId(processInstanceId).stream().findFirst().orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void goBackToPreviousTask(String processInstanceId, Integer numberOfSteps) throws EntityNotFoundException, BadRequestException {
        Lead lead = getLead(processInstanceId);
        verifyIfLeadNotConvertedToOpportunity(lead);
        Task task = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult();
        String previousTaskDefinitionKey = "task" + (Integer.parseInt(task.getTaskDefinitionKey().replaceAll("task", "")) - numberOfSteps);
        runtimeService
                .createChangeActivityStateBuilder()
                .processInstanceId(task.getProcessInstanceId())
                .moveActivityIdTo(task.getTaskDefinitionKey(), previousTaskDefinitionKey)
                .changeState();
    }

    @Override
    public void acceptTask(NotificationActionDto notificationActionDto, String processInstanceId) throws EntityNotFoundException, KafkaSendMessageException, IncorrectNotificationTypeException, BadRequestException {
        TaskAccepter taskAccepter = new TaskAccepter(notificationActionDto.getUserNotificationId(), processInstanceId).markAcceptanceAsFinished();
        Task task = taskAccepter.getTask();
        processEngineNotificationService.sendAcceptanceNotification(taskAccepter.getUserIds(), taskAccepter.getLeadName(), task.getName(), notificationActionDto.getComment(),
                NotificationMsgType.LEAD_TASK_ACCEPTED);
        processEngineNotificationService.sendOnCompleteTaskNotificationIfNeeded(processInstanceId, taskAccepter.getLeadName(), NotificationMsgType.LEAD_TASK_FINISHED);
        taskService.setVariableLocal(task.getId(), statusAcceptance, AcceptanceStatus.ACCEPTED.name());
        taskService.complete(task.getId());
    }

    @Override
    public void declineTask(NotificationActionDto notificationActionDto, String processInstanceId) throws EntityNotFoundException, KafkaSendMessageException, IncorrectNotificationTypeException, BadRequestException {
        TaskAccepter taskAccepter = new TaskAccepter(notificationActionDto.getUserNotificationId(), processInstanceId).markAcceptanceAsFinished();
        processEngineNotificationService.sendAcceptanceNotification(taskAccepter.getUserIds(), taskAccepter.getLeadName(), taskAccepter.getTask().getName(), notificationActionDto.getComment(),
                NotificationMsgType.LEAD_TASK_DECLINED);
        taskService.setVariableLocal(taskAccepter.getTask().getId(), statusAcceptance, AcceptanceStatus.DECLINED.name());
    }

    private boolean completeTaskOrMarkAsDuringAcceptanceOrCompleteAndAccept(Lead lead, Task task, Long acceptanceUserId, String processInstanceId) throws KafkaSendMessageException, BadRequestException, IncorrectNotificationTypeException, EntityNotFoundException {
        boolean isAutomaticallyAccepted = false;
        Long taskAcceptorUserId = userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail());
        if (acceptanceUserId == null) {
            taskService.complete(task.getId());
        } else if (acceptanceUserId.equals(taskAcceptorUserId)) {
            lead.setAcceptanceUserId(null);
            leadRepository.saveAndFlush(lead);
            kafkaService.saveHistory(ObjectHistoryType.LEAD, lead.getId().toString(), new JSONObject().put("selfAcceptance", taskAcceptorUserId).toString(), LocalDateTime.now(), getEmailFromRequest());
            String leadName = Strings.nullToEmpty(lead.getFirstName()) + " " + Strings.nullToEmpty(lead.getLastName());
            processEngineNotificationService.sendAcceptanceNotification(new HashSet<>(Arrays.asList(taskAcceptorUserId)), leadName, task.getName(), "Akceptacja automatyczna",
                    NotificationMsgType.LEAD_TASK_ACCEPTED);
            processEngineNotificationService.sendOnCompleteTaskNotificationIfNeeded(processInstanceId, leadName, NotificationMsgType.LEAD_TASK_FINISHED);
            taskService.setVariableLocal(task.getId(), statusAcceptance, AcceptanceStatus.ACCEPTED.name());
            taskService.complete(task.getId());
            isAutomaticallyAccepted = true;
        } else {
            lead.setAcceptanceUserId(acceptanceUserId);
            taskService.setVariableLocal(task.getId(), statusAcceptance, AcceptanceStatus.NEEDS_ACCEPTATION.name());
            leadRepository.save(lead);
        }
        return isAutomaticallyAccepted;
    }

    private Long sendNotificationsAndReturnAcceptanceUserId(String processInstanceId, Lead lead) throws KafkaSendMessageException {
        Long acceptanceUserId = processEngineNotificationService.sendTaskConfirmationNotificationIfNeeded(processInstanceId,
                Strings.nullToEmpty(lead.getFirstName()) + " " + Strings.nullToEmpty(lead.getLastName()), lead.getId(),
                NotificationMsgType.LEAD_TASK_ACCEPTANCE);
        if (acceptanceUserId == null) {
            processEngineNotificationService.sendOnCompleteTaskNotificationIfNeeded(processInstanceId, Strings.nullToEmpty(
                    lead.getFirstName()) + " " + Strings.nullToEmpty(lead.getLastName()), NotificationMsgType.LEAD_TASK_FINISHED);
        }
        return acceptanceUserId;
    }

    private class TaskAccepter {
        private Long userNotificationId;
        private String processInstanceId;
        private Task task;
        private Lead lead;

        TaskAccepter(Long userNotificationId, String processInstanceId) {
            this.userNotificationId = userNotificationId;
            this.processInstanceId = processInstanceId;
        }

        public Task getTask() {
            return task;
        }

        TaskAccepter markAcceptanceAsFinished() throws EntityNotFoundException, BadRequestException {
            task = processEngineHelperService.getTaskByProcessInstanceId(processInstanceId);
            if (task == null) {
                throw new EntityNotFoundException("Task for this processInstanceId not found");
            }

            lead = getLead(processInstanceId);
            if (!userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail()).equals(lead.getAcceptanceUserId())) {
                throw new BadRequestException("You can't accept/decline lead when you are not accepter");
            }

            lead.setAcceptanceUserId(null);
            leadRepository.save(lead);
            notificationClient.deleteMessage(new com.devqube.crmshared.notification.NotificationActionDto(userNotificationId));
            return this;
        }

        Set<Long> getUserIds() {
            Set<Long> userIds = new HashSet<>();
            userIds.add(lead.getCreator());
            if (lead.getLeadKeeperId() != null) {
                userIds.add(lead.getLeadKeeperId());
            }
            return userIds;
        }

        String getLeadName() {
            return Strings.nullToEmpty(lead.getFirstName()) + " " + Strings.nullToEmpty(lead.getLastName());
        }
    }

    private void verifyIfLeadNotConvertedToOpportunity(Lead lead) throws BadRequestException {
        if (lead.getConvertedToOpportunity()) {
            throw new BadRequestException("Lead converted to opportunity can not be modified");
        }
    }

    private String getEmailFromRequest() {
        try {
            RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            return request.getHeader("Logged-Account-Email");
        } catch (Exception ignore) {
            return "";
        }
    }
}
