package com.devqube.crmbusinessservice.leadopportunity.lead.importexport;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.dictionary.DictionaryService;
import com.devqube.crmbusinessservice.dictionary.Type;
import com.devqube.crmbusinessservice.dictionary.Word;
import com.devqube.crmbusinessservice.dictionary.WordRepository;
import com.devqube.crmbusinessservice.dictionary.dto.DictionaryDTO;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Process;
import com.devqube.crmbusinessservice.leadopportunity.company.Company;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadSpecifications;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto.LeadDto;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parseexception.ParseExceptionService;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ParseStrategyFactory;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ParseStrategyType;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ParsedLead;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ParsedLeadValidator;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.util.ImportUtil;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionListDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineHelperService;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineProxyService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.user.dto.AccountEmail;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

import static com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ParseStrategyType.CSV;
import static com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ParseStrategyType.EXCEL;
import static com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ValidationResult.DUPLICATE;
import static com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ValidationResult.INCORRECT;

@Slf4j
@Service
@RequiredArgsConstructor
public class ImportLeadService {

    private final UserService userService;
    private final ProcessEngineProxyService processEngineProxyService;
    private final ProcessEngineHelperService processEngineHelperService;
    private final UserClient userClient;
    private final LeadRepository leadRepository;
    private final ParseExceptionService parseExceptionService;
    private final DictionaryService dictionaryService;
    private final WordRepository wordRepository;
    private final ParseStrategyFactory parseStrategyFactory;

    public List<ParsedLead> parseImportFile(InputStream file, String fileExtension) throws BadRequestException/*, ParseExceptio*/ {
        ParseStrategyType parseStrategyType = null;
        fileExtension = fileExtension.toUpperCase();
        //todo move to enum
        if (fileExtension.equals("XLSX") || fileExtension.equals("XLS")) {
            parseStrategyType = EXCEL;
        } else if (fileExtension.equals("TXT") || fileExtension.equals("CSV")) {
            parseStrategyType = CSV;
        }
        List<ParsedLead> parsedLeadList = parseStrategyFactory.getStrategy(parseStrategyType).parseFile(file);
        ParsedLeadValidator.validate(parsedLeadList);
        return parsedLeadList;
    }

    public void importLeads(List<ParsedLead> parsedLeadListToSave, CommonLeadParams commonParams, Boolean skipIncorrect, Boolean mergeDuplicate) throws BadRequestException {
        removeDuplicateLeads(parsedLeadListToSave);
        if (skipIncorrect) {
            removeIncorrectLeads(parsedLeadListToSave);
        }
        List<Lead> leadListToSave = new ArrayList<>();
        if (mergeDuplicate) {
            leadListToSave.addAll(mergeWithExistingLeadsByEmail(parsedLeadListToSave));
        }
        leadListToSave.addAll(toLeadList(parsedLeadListToSave));
        addCommonParams(leadListToSave, commonParams);
        leadRepository.saveAll(leadListToSave);
    }

    private void addCommonParams(List<Lead> leadListToSave, CommonLeadParams commonParams) throws BadRequestException {
        Long creatorId = getUserId(commonParams.getLoggedAccountEmail());
        leadListToSave.forEach(lead -> {
            lead.setCreator(creatorId);
            lead.setDeleted(false);
            ProcessInstance processInstance = processEngineHelperService.startProcessInstance(commonParams.getProcessDefinitionId());
            try {
                Optional<UserTask> informationTask = processEngineHelperService.getFirstInformationTask(processInstance.getProcessInstanceId());
                informationTask.ifPresent((value) -> processEngineHelperService.completeInformationTask(value, processInstance.getId()));
            } catch (Exception ex) {
                //prod process may not have information task, so suppress exception
            }
            lead.setProcessInstanceId(processInstance.getProcessInstanceId());
            lead.setLeadKeeperId(commonParams.getLeadKeeperId());
            lead.setSourceOfAcquisition(commonParams.getSourceOfAcquisitionId());
            lead.setSubSourceOfAcquisition(commonParams.getSubSourceOfAcquisitionId());
            if (lead.getInterestedInCooperation() == null) {
                lead.setInterestedInCooperation(false);
            }
        });
    }

    private Long getUserId(String email) throws BadRequestException {
        try {
            return userClient.getAccountIdByEmail(email);
        } catch (Exception e) {
            throw new BadRequestException("user not found");
        }
    }

    private List<Lead> toLeadList(List<ParsedLead> parsedLeadList) {
        return parsedLeadList.stream()
                .map(this::toLead)
                .collect(Collectors.toList());
    }

    private Lead toLead(ParsedLead parsedLead) {
        if (parsedLead == null) {
            return null;
        }
        return new Lead()
                .setFirstName(parsedLead.getFirstName().getValue())
                .setLastName(parsedLead.getLastName().getValue())
                .setEmail(parsedLead.getEmail().getValue())
                .setPhone(parsedLead.getPhone().getValue())
                .setPosition(findPositionOrCreateNew(parsedLead.getPosition().getValue()).getId())
                .setCompany(new Company()
                        .setCompanyName(parsedLead.getCompanyName().getValue())
                        .setCity(parsedLead.getCompanyCity().getValue()));
    }

    private List<Lead> mergeWithExistingLeadsByEmail(List<ParsedLead> leadListToSave) {
        List<ParsedLead> mergedParsedLead = new ArrayList<>();
        List<Lead> mergedLeadList = new ArrayList<>();
        leadListToSave.forEach(parsedLead -> {
            List<Lead> existingLeadListByEmail = leadRepository.findAll(LeadSpecifications.findByEmail(parsedLead.getEmail().getValue()));
            if (existingLeadListByEmail.size() > 0) {
                Lead existingLead = Collections.max(existingLeadListByEmail, Comparator.comparing(Lead::getCreated));
                merge(existingLead, parsedLead);
                mergedLeadList.add(existingLead);
                mergedParsedLead.add(parsedLead);
            }
        });
        leadListToSave.removeAll(mergedParsedLead);
        return mergedLeadList;
    }

    private void removeDuplicateLeads(List<ParsedLead> leads) {
        Map<String, List<ParsedLead>> duplicateParsedLeadMap = new HashMap<>();
        for (ParsedLead lead : leads) {
            if (lead.getEmail().getValidationResults().contains(DUPLICATE)) {
                List<ParsedLead> leadList = duplicateParsedLeadMap.get(lead.getEmail().getValue());
                if (leadList != null) {
                    leadList.add(lead);
                } else {
                    leadList = new ArrayList<>();
                    leadList.add(lead);
                    duplicateParsedLeadMap.put(lead.getEmail().getValue(), leadList);
                }
            }
        }

        duplicateParsedLeadMap.forEach((email, leadsToRemove) -> {
            if (leadsToRemove.size() > 1) {
                leadsToRemove.remove(0);
                leads.removeAll(leadsToRemove);
            }
        });
    }

    private void removeIncorrectLeads(List<ParsedLead> leads) {
        List<ParsedLead> toRemove = new ArrayList<>();
        for (ParsedLead lead : leads) {
            if (lead.getFirstName().getValidationResults().contains(INCORRECT) ||
                    lead.getLastName().getValidationResults().contains(INCORRECT) ||
                    lead.getEmail().getValidationResults().contains(INCORRECT) ||
                    lead.getPhone().getValidationResults().contains(INCORRECT) ||
                    lead.getPosition().getValidationResults().contains(INCORRECT) ||
                    lead.getCompanyName().getValidationResults().contains(INCORRECT) ||
                    lead.getCompanyCity().getValidationResults().contains(INCORRECT)) {
                toRemove.add(lead);
            }
        }
        leads.removeAll(toRemove);
    }

    private void merge(Lead existingLead, ParsedLead parsedLead) {
        if (parsedLead.getFirstName().getValue() != null &&
                !parsedLead.getFirstName().getValue().isBlank()) {
            existingLead.setFirstName(parsedLead.getFirstName().getValue());
        }
        if (parsedLead.getLastName().getValue() != null &&
                !parsedLead.getLastName().getValue().isBlank()) {
            existingLead.setLastName(parsedLead.getLastName().getValue());
        }
        if (parsedLead.getEmail().getValue() != null &&
                !parsedLead.getEmail().getValue().isBlank()) {
            existingLead.setEmail(parsedLead.getEmail().getValue());
        }
        if (parsedLead.getPhone().getValue() != null &&
                !parsedLead.getPhone().getValue().isBlank()) {
            existingLead.setPhone(parsedLead.getPhone().getValue());
        }
        if (parsedLead.getPosition().getValue() != null &&
                !parsedLead.getPosition().getValue().isBlank()) {
            existingLead.setPosition(findPositionOrCreateNew(parsedLead.getPosition().getValue()).getId());
        }
        Company company = existingLead.getCompany();
        if (company == null) {
            company = new Company();
        }
        if (parsedLead.getCompanyName().getValue() != null &&
                !parsedLead.getCompanyName().getValue().isBlank()) {
            company.setCompanyName(parsedLead.getCompanyName().getValue());
            existingLead.setCompany(company);
        }
        if (parsedLead.getCompanyCity().getValue() != null &&
                !parsedLead.getCompanyCity().getValue().isBlank()) {
            company.setCity(parsedLead.getCompanyCity().getValue());
            existingLead.setCompany(company);
        }
    }

    private Lead convert(LeadDto leadDto, Map<String, String> processDefinitionMap, List<AccountEmail> accountEmails, List<DictionaryWord> sourcesOfAcquisition, Long creator) throws BadRequestException {
        Company company = getCompany(leadDto);
        Lead lead = new Lead();
        lead.setFirstName(leadDto.getFirstName());
        lead.setLastName(leadDto.getLastName());
        lead.setPriority(leadDto.getPriority());
        //lead.setPosition(leadDto.getPosition());
        lead.setEmail(leadDto.getEmail());
        lead.setPhone(leadDto.getPhone());

        lead.setCompany(company);

        Integer leadKeeperId = accountEmails.stream()
                .filter(c -> ImportUtil.isAccountEmailEqual(c, leadDto.getLeadKeeperName()))
                .findFirst().map(AccountEmail::getId).orElseThrow(BadRequestException::new);
        lead.setLeadKeeperId(leadKeeperId.longValue());

        if (Objects.isNull(leadDto.getSourceOfAcquisition()) || leadDto.getSourceOfAcquisition().isEmpty()) {
            lead.setSourceOfAcquisition(null);
        } else {
            Long sourceOfAcquisitionId = sourcesOfAcquisition.stream()
                    .filter(c -> ImportUtil.isSourceOfAcquisition(c, leadDto.getSourceOfAcquisition()))
                    .findFirst().map(DictionaryWord::getId)
                    .orElseThrow(BadRequestException::new);
            lead.setSourceOfAcquisition(sourceOfAcquisitionId);
        }

        if (Objects.isNull(leadDto.getSubSourceOfAcquisition()) || leadDto.getSubSourceOfAcquisition().isEmpty()) {
            lead.setSubSourceOfAcquisition(null);
        } else {
            Long subSourceOfAcquisitionId = sourcesOfAcquisition.stream()
                    .filter(c -> ImportUtil.isSourceOfAcquisition(c, leadDto.getSubSourceOfAcquisition()))
                    .findFirst().map(DictionaryWord::getId)
                    .orElseThrow(BadRequestException::new);
            lead.setSubSourceOfAcquisition(subSourceOfAcquisitionId);
        }

        ProcessInstance processInstance = processEngineHelperService.startProcessInstance(processDefinitionMap.get(leadDto.getProcessDefinitionName()));
        lead.setProcessInstanceId(processInstance.getProcessInstanceId());
        lead.setCreator(creator);
        return lead;
    }


    private Map<String, String> getProcessDefinitionMap(List<LeadDto> leadDtoList) throws BadRequestException { // <processDefinitionName, processDefinitionId>
        Map<String, String> result = new HashMap<>();
        List<ProcessDefinitionListDTO> processDefinitionList = processEngineProxyService.getProcessDefinitionsList(Process.ObjectTypeEnum.LEAD.name());
        Set<String> getProcessDefinitionNames = leadDtoList.stream().map(LeadDto::getProcessDefinitionName).collect(Collectors.toSet());
        for (String processDefinitionName : getProcessDefinitionNames) {
            try {
                result.put(processDefinitionName, findProcessDefinitionId(processDefinitionList, processDefinitionName));
            } catch (Exception e) {
                result.put(processDefinitionName, null);
            }
        }
        return result;
    }

    private String findProcessDefinitionId(List<ProcessDefinitionListDTO> processDefinitionList, String processDefinitionName) throws BadRequestException {
        return processDefinitionList.stream()
                .filter(c -> c.getName().toLowerCase().equals(processDefinitionName.toLowerCase()))
                .findFirst()
                .map(ProcessDefinitionListDTO::getId).orElseThrow(BadRequestException::new);
    }

    private Company getCompany(LeadDto leadDto) {
        Company company = new Company();
        company.setCompanyName(leadDto.getCompanyName());
        company.setWwwAddress(leadDto.getCompanyWwwAddress());
        company.setStreetAndNumber(leadDto.getCompanyStreetAndNumber());
        company.setCity(leadDto.getCompanyCity());
        company.setPostalCode(leadDto.getCompanyPostalCode());
        company.setProvince(leadDto.getCompanyProvince());
        company.setCountry(leadDto.getCompanyCountry());
        return company;
    }

    private List<DictionaryWord> getSourcesOfAcquisitionMap() {
        DictionaryDTO dictionaryDTO = dictionaryService.findAdminDictionary(Type.SOURCE_OF_ACQUISITION.getDictionaryId());
        return dictionaryDTO.getWords().stream()
                .map(wordDTO -> new DictionaryWord(wordDTO.getId(), wordDTO.getName()))
                .collect(Collectors.toList());
    }

    private DictionaryWord findPositionOrCreateNew(String position) {
        List<Word> wordsList = wordRepository.findByDictionaryIdAndNameIgnoreCase(Type.POSITION.getDictionaryId(), position);
        if (wordsList != null) {
            Optional<Word> first = wordsList.stream().findFirst();
            if (first.isPresent()) {
                return new DictionaryWord(first.get().getId(), first.get().getName());
            } else {
                Word savedWord = wordRepository.save(new Word(null, position, null, null, false, Type.POSITION.getDictionaryId(), Collections.emptyList(),
                        null, false, "pl", null));
                return new DictionaryWord(savedWord.getId(), savedWord.getName());
            }

        } else {
            Word savedWord = wordRepository.save(new Word(null, position, null, null, false, Type.POSITION.getDictionaryId(), Collections.emptyList(),
                    null, false, "pl", null));
            return new DictionaryWord(savedWord.getId(), savedWord.getName());
        }
    }
}
