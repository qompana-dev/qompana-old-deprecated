package com.devqube.crmbusinessservice.leadopportunity.opportunity;

import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface OpportunityRepository extends JpaRepository<Opportunity, Long>, JpaSpecificationExecutor<Opportunity> {

    @Query("select o from Opportunity o where (o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids))")
    List<Opportunity> findAllByOwnerIdOrKeeperId(@Param("ids") List<Long> userIds);

    @Query("select o from Opportunity o where (o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids)) " +
            "and (" +
            "LOWER(o.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(cast(o.finishDate as text)) LIKE LOWER(concat('%', :search, '%')) or " +
            "(o.customer is not null and LOWER(o.customer.name) LIKE LOWER(concat('%', :search, '%')))" +
            ")")
    Page<Opportunity> findAllByOwnerIdOrKeeperIdForMobile(@Param("ids") List<Long> userIds, Pageable pageable, @Param("search") String search);

    @Query("select o from Opportunity o where o.created > :date and " +
            "(o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids))")
    List<Opportunity> findAllByCreatedAfter(@Param("ids") List<Long> userIds, @Param("date") LocalDateTime date);

    @Query("select o from Opportunity o where o.created > :date and " +
            "(o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids)) " +
            "and (" +
            "LOWER(o.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(cast(o.finishDate as text)) LIKE LOWER(concat('%', :search, '%')) or " +
            "(o.customer is not null and LOWER(o.customer.name) LIKE LOWER(concat('%', :search, '%')))" +
            ")")
    Page<Opportunity> findAllByCreatedAfterForMobile(@Param("ids") List<Long> userIds, @Param("date") LocalDateTime date, Pageable pageable, @Param("search") String search);

    @Query("select o from Opportunity o where o.updated > :date and " +
            "(o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids))")
    List<Opportunity> findAllByUpdatedAfter(@Param("ids") List<Long> userIds, @Param("date") LocalDateTime date);

    @Query("select o from Opportunity o where o.updated > :date and " +
            "(o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids)) " +
            "and (" +
            "LOWER(o.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(cast(o.finishDate as text)) LIKE LOWER(concat('%', :search, '%')) or " +
            "(o.customer is not null and LOWER(o.customer.name) LIKE LOWER(concat('%', :search, '%')))" +
            ")")
    Page<Opportunity> findAllByUpdatedAfterForMobile(@Param("ids") List<Long> userIds, @Param("date") LocalDateTime date, Pageable pageable, @Param("search") String search);

    @Query("select o from Opportunity o " +
            "where (select max(lvo.lastViewed) from LastViewedOpportunity lvo where lvo.opportunity.id = o.id and lvo.userId = :id) > :date " +
            "and (o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids))")
    List<Opportunity> findAllByLastViewedAfter(@Param("id") Long userId, @Param("ids") List<Long> userIds, @Param("date") LocalDateTime date);

    @Query("select o from Opportunity o " +
            "where (select max(lvo.lastViewed) from LastViewedOpportunity lvo where lvo.opportunity.id = o.id and lvo.userId = :id) > :date " +
            "and (o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids)) " +
            "and (" +
            "LOWER(o.name) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(cast(o.finishDate as text)) LIKE LOWER(concat('%', :search, '%')) or " +
            "(o.customer is not null and LOWER(o.customer.name) LIKE LOWER(concat('%', :search, '%')))" +
            ")")
    Page<Opportunity> findAllByLastViewedAfterForMobile(@Param("id") Long userId, @Param("ids") List<Long> userIds, @Param("date") LocalDateTime date, Pageable pageable, @Param("search") String search);

    @Query("SELECT o FROM Opportunity o WHERE o.id in ?1")
    List<Opportunity> findByIds(List<Long> ids);

    @Query("SELECT o FROM Opportunity o WHERE LOWER(o.name) LIKE LOWER(concat('%', :pattern, '%')) " +
            "and (o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids))")
    List<Opportunity> findByNameContaining(@Param("pattern") String pattern, @Param("ids") List<Long> userIds);

    @Query("SELECT o FROM Opportunity o WHERE LOWER(o.name) LIKE LOWER(concat('%', :term, '%')) " +
            "and (o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids))")
    List<Opportunity> findAllAvailableForUserAndContaining(@Param("term") String term, @Param("ids") List<Long> userIds);

    @Query("select o.id from Opportunity o where (o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids))")
    List<Long> findAllAvailableForUser(@Param("ids") List<Long> userIds);

    @Query("select o from Opportunity o where (o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids))")
    List<Opportunity> findOpportunitiesAvailableForUser(@Param("ids") List<Long> userIds);

    @Query("select o from Opportunity o where (o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids)) " +
            "and o.closingDate is null")
    List<Opportunity> findAllOpenedAndAvailableForUser(@Param("ids") List<Long> userIds);

    @Query("select o from Opportunity o where o.processDefinitionId =:processDefinitionId and  (o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids)) ")
    List<Opportunity> findAllAvailableForUserAndProcessDefinitionId(@Param("ids") List<Long> userIds, @Param("processDefinitionId") String processDefinitionId);

    @Query("select o from Opportunity o where o.contact.id = :contactId " +
            " and (o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids))")
    List<Opportunity> findAllByContact(@Param("contactId") Long contactId, @Param("ids") List<Long> userIds);

    @Query("select o from Opportunity o where o.customer.id = :customerId " +
            " and (o.ownerOneId in (:ids) or o.ownerTwoId in (:ids) or o.ownerThreeId in (:ids) or o.creator in (:ids))")
    List<Opportunity> findAllByCustomer(@Param("customerId") Long customerId, @Param("ids") List<Long> userIds);

    List<Opportunity> findAllByProcessInstanceId(String processInstanceId);

    boolean existsByCustomerId(Long id);

    boolean existsByContactId(Long id);


    @Query("select count(o) from Opportunity o where o.created >= :from and o.created <= :to and o.creator in :userIds and o.convertedFromLead = false")
    Long findCreatedOpportunityForGoal(@Param("from") LocalDateTime from, @Param("to") LocalDateTime to, @Param("userIds") Set<Long> userIds);

    @Query("select o from Opportunity o " +
            "where o.closingDate is not null and o.closingDate >= :from and o.closingDate <= :to " +
            "and (o.ownerOneId in :userIds or o.ownerTwoId in :userIds or o.ownerThreeId in :userIds) " +
            "and (:currency is null or o.currency = :currency) " +
            "and o.finishResult = 'SUCCESS'")
    List<Opportunity> findAllWonOpportunitiesForGoal(@Param("from") LocalDateTime from, @Param("to") LocalDateTime to, @Param("userIds") Set<Long> userIds, @Param("currency") String currency);

    @Query("select o from Opportunity o " +
            " where o.closingDate is not null " +
            " and o.finishResult is not null " +
            " and (o.ownerOneId in :ids or o.ownerTwoId in :ids or o.ownerThreeId in :ids or o.creator in (:ids))")
    List<Opportunity> findAllFinishedOpportunieties(@Param("ids") List<Long> userIds);

    Long countAllByProcessInstanceIdIn(List<String> processInstanceIds);

    @Query("select o from Opportunity o " +
            "left join o.contact co left join o.customer cu " +
            "where co.id in :contactIds or cu.id in :customerIds")
    List<Opportunity> searchOpportunitiesForLeadCreation(@Param("contactIds") Set<Long> contactIds, @Param("customerIds") Set<Long> customerIds);

    List<Opportunity> findAllByCreatorAndCreatedBetween(Long accountId, LocalDateTime dateTimeOfFirstDayOfQuarter, LocalDateTime dateTimeOfLastDayOfQuarter);

    @Query("select o from Opportunity o " +
            "where o.creator = :accountId and o.closingDate is not null " +
            "and (o.closingDate between :fromDateTime AND :toDateTime) and o.finishResult = 'SUCCESS'")
    List<Opportunity> findAllByCreatorAndClosingDateBetweenAndFinishResultSuccess(@Param("accountId") Long accountId,
                                                                                  @Param("fromDateTime") LocalDateTime dateTimeOfFirstDayOfQuarter,
                                                                                  @Param("toDateTime") LocalDateTime dateTimeOfLastDayOfQuarter);

    @Query("select o from Opportunity o " +
            "where o.creator = :accountId and o.closingDate is not null " +
            "and (o.closingDate between :fromDateTime AND :toDateTime) and o.finishResult = 'ERROR'")
    List<Opportunity> findAllByCreatorAndAndClosingDateBetweenAndFinishResultError(@Param("accountId") Long accountId,
                                                                                   @Param("fromDateTime") LocalDateTime dateTimeOfFirstDayOfQuarter,
                                                                                   @Param("toDateTime") LocalDateTime dateTimeOfLastDayOfQuarter);

    @Query("select o from Opportunity o where (o.ownerOneId in :userIds or o.ownerTwoId in :userIds or o.ownerThreeId in :userIds or o.creator in (:userIds))")
    List<Opportunity> findAllByAnyOwnerOrCreatorIn(@Param("userIds") List<Long> userIds);


}
