package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parseexception;

import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.DictionaryWord;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto.LeadDto;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parseexception.dto.ParseExceptionDetailDto;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parseexception.exception.ParseException;
import com.devqube.crmshared.importexport.ImportObjLine;
import com.devqube.crmshared.user.dto.AccountEmail;
import com.univocity.parsers.common.DataProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ParseExceptionService {
    public void throwParseExceptionIfIncorrect(List<DataProcessingException> exceptionList, Map<String, String> processDefinition, List<AccountEmail> accountEmails, List<LeadDto> leadDtos, List<DictionaryWord> sourcesOfAcquisition) throws ParseException {
        List<ParseExceptionDetailDto> result = new ArrayList<>();
        result.addAll(getParseExceptions(exceptionList));
        result.addAll(getProcessDefinitionsExceptions(processDefinition, leadDtos));
        result.addAll(getAccountsExceptions(accountEmails, leadDtos));
        result.addAll(getSourcesOfAcquisitionExceptions(sourcesOfAcquisition, leadDtos));
        result.addAll(getSubSourcesOfAcquisitionExceptions(sourcesOfAcquisition, leadDtos));

        if (result.size() > 0) {
            throw new ParseException(result);
        }
    }

    private List<ParseExceptionDetailDto> getAccountsExceptions(List<AccountEmail> accountEmails, List<LeadDto> leadDtos) {
        List<String> names = new ArrayList<>();
        accountEmails.forEach(accountEmail -> names.add((accountEmail.getName() + " " + accountEmail.getSurname()).toLowerCase()));
        accountEmails.forEach(accountEmail -> names.add((accountEmail.getSurname() + " " + accountEmail.getName()).toLowerCase()));

        return leadDtos.stream().filter(c -> !names.contains(c.getLeadKeeperName().toLowerCase()))
                .map(c -> new ParseExceptionDetailDto(c.getLine(), "leadKeeperName", c.getLeadKeeperName()))
                .collect(Collectors.toList());
    }

    private List<ParseExceptionDetailDto> getSourcesOfAcquisitionExceptions(List<DictionaryWord> sourcesOfAcquisition, List<LeadDto> leadDtos) {
        List<String> names = new ArrayList<>();
        sourcesOfAcquisition.forEach(sourceOfAcquisition -> names.add((sourceOfAcquisition.getName()).toLowerCase()));

        return leadDtos.stream()
                .filter(c -> Objects.nonNull(c.getSourceOfAcquisition())
                        && !c.getSourceOfAcquisition().isEmpty()
                        && !names.contains(c.getSourceOfAcquisition().toLowerCase()))
                .map(c -> new ParseExceptionDetailDto(c.getLine(), "sourceOfAcquisition", c.getSourceOfAcquisition()))
                .collect(Collectors.toList());
    }

    private List<ParseExceptionDetailDto> getSubSourcesOfAcquisitionExceptions(List<DictionaryWord> sourcesOfAcquisition, List<LeadDto> leadDtos) {
        List<String> names = new ArrayList<>();
        sourcesOfAcquisition.forEach(sourceOfAcquisition -> names.add((sourceOfAcquisition.getName()).toLowerCase()));

        return leadDtos.stream()
                .filter(c -> Objects.nonNull(c.getSubSourceOfAcquisition())
                        && !c.getSubSourceOfAcquisition().isEmpty()
                        && !names.contains(c.getSubSourceOfAcquisition().toLowerCase()))
                .map(c -> new ParseExceptionDetailDto(c.getLine(), "subSourceOfAcquisition", c.getSubSourceOfAcquisition()))
                .collect(Collectors.toList());
    }

    private List<ParseExceptionDetailDto> getProcessDefinitionsExceptions(Map<String, String> processDefinition, List<LeadDto> leadDtos) {
        List<ParseExceptionDetailDto> result = new ArrayList<>();
        processDefinition.keySet().stream().filter(c -> processDefinition.get(c) == null)
                .forEach(c -> result.addAll(getProcessDefinitionExceptions(c, leadDtos)));
        return result;
    }

    private List<ParseExceptionDetailDto> getProcessDefinitionExceptions(String processDefinition, List<LeadDto> leadDtos) {
        return leadDtos.stream()
                .filter(c -> c.getProcessDefinitionName().equals(processDefinition))
                .map(ImportObjLine::getLine)
                .map(c -> new ParseExceptionDetailDto(c, "processDefinitionName", processDefinition))
                .collect(Collectors.toList());
    }

    private List<ParseExceptionDetailDto> getParseExceptions(List<DataProcessingException> exceptionList) {
        List<ParseExceptionDetailDto> result = new ArrayList<>();
        if (exceptionList != null && exceptionList.size() != 0) {
            for (DataProcessingException ex : exceptionList) {
                try {
                    ParseExceptionDetailDto parseExceptionDetailDto = new ParseExceptionDetailDto();
                    parseExceptionDetailDto.setLine(ex.getLineIndex());
                    parseExceptionDetailDto.setField(ex.getHeaders()[ex.getColumnIndex()]);
                    parseExceptionDetailDto.setValue(ex.getValue().toString());
                    result.add(parseExceptionDetailDto);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
