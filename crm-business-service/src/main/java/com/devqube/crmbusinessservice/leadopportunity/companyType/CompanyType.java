package com.devqube.crmbusinessservice.leadopportunity.companyType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class CompanyType {
    @Id
    @SequenceGenerator(name="company_type_seq", sequenceName="company_type_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="company_type_seq")
    private Long id;

    @Column(unique = true, nullable = false)
    private String type;

}
