package com.devqube.crmbusinessservice.leadopportunity.lead.importexport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@AllArgsConstructor
@Getter
public class DictionaryWord {

    private Long id;
    private String name;
}
