package com.devqube.crmbusinessservice.leadopportunity.lead;

import com.devqube.crmbusinessservice.leadopportunity.AccountsService;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ProcessColor;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.kanban.KanbanLead;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.kanban.KanbanProcessOverview;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.kanban.KanbanTaskDetails;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineReadService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.web.CurrentRequestUtil;
import org.flowable.bpmn.model.ExtensionAttribute;
import org.flowable.bpmn.model.Process;
import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.FormService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.task.api.Task;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class KanbanLeadService {

    private final LeadRepository leadRepository;

    private final RepositoryService repositoryService;

    private final TaskService taskService;

    private final FormService formService;

    private final ProcessEngineReadService processEngineReadService;

    private final AccountsService accountsService;

    public KanbanLeadService(LeadRepository leadRepository, RepositoryService repositoryService, TaskService taskService,
                             FormService formService, ProcessEngineReadService processEngineReadService, AccountsService accountsService) {
        this.leadRepository = leadRepository;
        this.repositoryService = repositoryService;
        this.taskService = taskService;
        this.formService = formService;
        this.processEngineReadService = processEngineReadService;
        this.accountsService = accountsService;
    }

    public KanbanLead getLeadOnKanbanById(Long id) throws EntityNotFoundException {
        Lead lead = leadRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        Task activeTask = getActiveTask(lead.getProcessInstanceId());
        Boolean nextStepPossible = isNextStepPossible(activeTask);
        return new KanbanLead(lead, activeTask, nextStepPossible);
    }

    public List<KanbanProcessOverview> getProcessesOverview() throws BadRequestException {
        List<ProcessDefinition> list = getAllProcesses();
        Map<String, Lead> processLeadMap = getProcessIdInstanceLeadMap();
        List<Task> allTasksInstances = getAllTasksInstances();
        return list.stream()
                .map(process -> getKanbanProcessOverview(processLeadMap, allTasksInstances, process))
                .filter(overview -> overview.getSize() != 0)
                .collect(Collectors.toList());
    }

    public List<KanbanTaskDetails> getProcessDetails(String processDefinitionId) throws EntityNotFoundException, BadRequestException {
        ProcessDefinition processDefinition = Optional.ofNullable(getProcessDefinition(processDefinitionId)).orElseThrow(EntityNotFoundException::new);
        Map<String, Lead> processLeadMap = getProcessIdInstanceLeadMap();
        List<Task> allTasksInstances = getAllTasksInstances();
        Map<UserTask, List<KanbanLead>> processWithLeads = getAllLeadsGroupedByTasks(processDefinition, allTasksInstances, processLeadMap);
        return mapToTaskDetailsList(processWithLeads);
    }

    private Map<UserTask, List<KanbanLead>> getAllLeadsGroupedByTasks(ProcessDefinition processDefinition, List<Task> allTasksInstances, Map<String, Lead> processLeadMap) {
        Process process = getMainProcess(processDefinition);
        List<UserTask> tasksInProcess = getTasksOfProcess(process);
        Map<UserTask, List<KanbanLead>> tasks = createMapOfTaskAndLeads(tasksInProcess);
        for (UserTask userTask : tasksInProcess) {
            for (Task task : allTasksInstances) {
                if (isTaskInstanceOfUserTask(task, userTask, processDefinition)) {
                    Lead lead = processLeadMap.get(task.getProcessInstanceId());
                    if (lead != null) {
                        Boolean isNextStepPossible = isNextStepPossible(task);
                        KanbanLead kanbanLead = new KanbanLead(lead, task, isNextStepPossible);
                        tasks.get(userTask).add(kanbanLead);
                    }
                }
            }
        }
        return tasks;
    }

    private Boolean isNextStepPossible(Task task) {
        return !formService
                .getTaskFormData(task.getId())
                .getFormProperties()
                .stream()
                .anyMatch(formProperty -> formProperty.isRequired() && formProperty.getValue() == null);
    }

    private List<Task> getAllTasksInstances() {
        return taskService.createTaskQuery().list();
    }

    private Map<String, Lead> getProcessIdInstanceLeadMap() throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(CurrentRequestUtil.getLoggedInAccountEmail());
        return leadRepository.findAllByDeletedFalseAndConvertedToOpportunityFalse(userIds)
                .stream()
                .collect(Collectors.toMap(Lead::getProcessInstanceId, m -> m));
    }

    private List<ProcessDefinition> getAllProcesses() {
        return processEngineReadService.getProcessDefinitions("LEAD");
    }

    private List<KanbanTaskDetails> mapToTaskDetailsList(Map<UserTask, List<KanbanLead>> processWithLeads) {
        return processWithLeads.entrySet().stream().map(entry -> new KanbanTaskDetails(entry.getKey(), entry.getValue())).collect(Collectors.toList());
    }

    private boolean isTaskInstanceOfUserTask(Task task, UserTask userTask, ProcessDefinition process) {
        return userTask.getId().equals(task.getTaskDefinitionKey()) && task.getProcessDefinitionId().equals(process.getId());
    }

    private KanbanProcessOverview getKanbanProcessOverview(Map<String, Lead> processLeadMap, List<Task> allTasksInstances, ProcessDefinition process) {
        Map<UserTask, List<KanbanLead>> processWithLeads = getAllLeadsGroupedByTasks(process, allTasksInstances, processLeadMap);
        Process mainProcess = getMainProcess(process);
        ProcessColor processColor = getProcessColor(mainProcess);
        return new KanbanProcessOverview(process, processWithLeads, processColor);
    }

    private Map<UserTask, List<KanbanLead>> createMapOfTaskAndLeads(List<UserTask> tasksInProcess) {
        Map<UserTask, List<KanbanLead>> tasks = new LinkedHashMap<>();
        for (UserTask userTask : tasksInProcess) {
            tasks.put(userTask, new ArrayList<>());
        }
        return tasks;
    }

    private ProcessDefinition getProcessDefinition(String processDefinitionId) {
        return repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionId)
                .singleResult();
    }

    private List<UserTask> getTasksOfProcess(Process process) {
        return process
                .getFlowElements()
                .stream()
                .filter(UserTask.class::isInstance)
                .map(e -> (UserTask) e)
                .collect(Collectors.toList());
    }

    private Process getMainProcess(ProcessDefinition process) {
        return repositoryService
                .getBpmnModel(process.getId())
                .getMainProcess();
    }

    private Task getActiveTask(String processInstanceId) {
        return taskService
                .createTaskQuery()
                .processInstanceId(processInstanceId)
                .singleResult();
    }

    private ProcessColor getProcessColor(Process mainProcess) {
        List<ExtensionAttribute> fontColor = mainProcess.getAttributes().get("fontColor");
        if (fontColor != null && fontColor.size() > 0) {
            String[] split = fontColor.get(0).getValue().split(";");
            if (split.length == 2) {
                return new ProcessColor(split[0], split[1]);
            }
        }
        return new ProcessColor();
    }

}
