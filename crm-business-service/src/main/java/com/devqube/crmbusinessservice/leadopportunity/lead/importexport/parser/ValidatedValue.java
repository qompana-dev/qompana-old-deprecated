package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashSet;
import java.util.Set;

import static com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ValidationResult.NOT_VALIDATED;


@Data
@Accessors(chain = true)
public class ValidatedValue {

    private String value;
    private Set<ValidationResult> validationResults = new HashSet<>() {{
        add(NOT_VALIDATED);
    }};

}
