package com.devqube.crmbusinessservice.leadopportunity.primaryProcess.web;

import com.devqube.crmbusinessservice.leadopportunity.primaryProcess.PrimaryProcessService;
import com.devqube.crmbusinessservice.leadopportunity.primaryProcess.web.dto.PrimaryProcessDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class PrimaryProcessController implements PrimaryProcessApi {
    private final PrimaryProcessService primaryProcessService;

    public PrimaryProcessController(PrimaryProcessService primaryProcessService) {
        this.primaryProcessService = primaryProcessService;
    }

    @Override
    public ResponseEntity<PrimaryProcessDto> save(String loggedAccountEmail, String processId, String type) {
        try {
            return new ResponseEntity<>(primaryProcessService.savePrimaryProcess(processId,type, loggedAccountEmail), HttpStatus.CREATED);
        } catch (Exception e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<Void> delete(String processId, String type) {
        try {
            primaryProcessService.deletePrimaryProcess(processId, type);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<PrimaryProcessDto>> getPrimaryProcesses() {
        try {
            return new ResponseEntity<>(primaryProcessService.getAllPrimaryProcess(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
