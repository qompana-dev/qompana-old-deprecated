package com.devqube.crmbusinessservice.leadopportunity.companyType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyTypeRepository extends JpaRepository<CompanyType, Long> {
}
