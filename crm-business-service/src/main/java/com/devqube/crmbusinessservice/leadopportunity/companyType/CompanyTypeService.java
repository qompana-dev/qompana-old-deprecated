package com.devqube.crmbusinessservice.leadopportunity.companyType;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyTypeService {
    private final CompanyTypeRepository companyTypeRepository;

    public CompanyTypeService(CompanyTypeRepository companyTypeRepository) {
        this.companyTypeRepository = companyTypeRepository;
    }

    public List<CompanyType> findAll() {
        return this.companyTypeRepository.findAll();
    }
}
