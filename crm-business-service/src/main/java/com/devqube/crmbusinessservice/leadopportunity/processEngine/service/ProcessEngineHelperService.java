package com.devqube.crmbusinessservice.leadopportunity.processEngine.service;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.BPMNUtil;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.PossibleValue;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Property;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessInstanceDetailsDTO;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.google.common.base.Strings;
import org.apache.commons.lang.StringUtils;
import org.flowable.bpmn.model.ExtensionAttribute;
import org.flowable.bpmn.model.Process;
import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.FormService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.form.FormProperty;
import org.flowable.engine.form.TaskFormData;
import org.flowable.engine.impl.form.FormPropertyImpl;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.flowable.task.api.TaskInfo;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProcessEngineHelperService {

    private final String statusAcceptance = "statusAcceptance";
    private final UserClient userClient;
    private final FormService formService;
    private final RepositoryService repositoryService;
    private final TaskService taskService;
    private final RuntimeService runtimeService;

    public ProcessEngineHelperService(FormService formService, UserClient userClient,
                                      RepositoryService repositoryService, TaskService taskService,
                                      RuntimeService runtimeService) {
        this.formService = formService;
        this.userClient = userClient;
        this.repositoryService = repositoryService;
        this.taskService = taskService;
        this.runtimeService = runtimeService;
    }

    public ProcessInstance startProcessInstance(String processDefinitionId) {
        return runtimeService.startProcessInstanceById(processDefinitionId);
    }

    public ProcessInstance getProcessInstance(String processInstanceId) {
        return runtimeService
                .createProcessInstanceQuery()
                .processInstanceId(processInstanceId)
                .singleResult();
    }

    public UserTask getActiveUserTask(ProcessInstance processInstance) {
        Task task = getActiveTask(processInstance.getProcessInstanceId());
        return getListOfTasks(processInstance)
                .stream()
                .filter(userTask -> userTask.getName().equals(Strings.nullToEmpty(task.getName())))
                .findFirst()
                .get();
    }

    public Task getActiveTask(String processInstanceId) {
        return taskService
                .createTaskQuery()
                .processInstanceId(processInstanceId)
                .singleResult();
    }

    public Optional<UserTask> getFirstInformationTask(String processInstanceId) {
        return getListOfTasks(getProcessInstance(processInstanceId))
                .stream()
                .filter(this::isInformationTaskValueFromAttributes)
                .findFirst();

    }

    public List<UserTask> getListOfTasks(ProcessInstance processInstance) {
        return this.getListOfTasks(this.getMainProcess(processInstance));
    }

    public Process getMainProcess(ProcessInstance processInstance) {
        return repositoryService
                .getBpmnModel(processInstance.getProcessDefinitionId())
                .getMainProcess();
    }

    public void setTasksToDTO(String processInstanceId, ProcessInstanceDetailsDTO dto, ProcessInstance processInstance) {
        Process mainProcess = getMainProcess(processInstance);
        ProcessDefinition processDefinition = getProcessDefinition(processInstance.getProcessDefinitionId());
        setTaskColors(dto, mainProcess);
        setObjectType(dto, processDefinition);
        dto.setTasks(getListOfTasks(mainProcess)
                .stream()
                .map(this::mapToTask)
                .collect(Collectors.toList())
        );
        Task task = setPropertiesForActiveTask(processInstanceId, dto);
        setAdditionalFieldsToTasks(dto, task);
    }

    public void validateTask(Long acceptanceUserId, Task task) throws BadRequestException {
        if (acceptanceUserId != null) {
            throw new BadRequestException("Task is being accepted");
        }
        List<FormProperty> formProperties = formService.getTaskFormData(task.getId()).getFormProperties();
        Optional<UserTask> first = getListOfTasks(getProcessInstance(task.getProcessInstanceId()))
                .stream().filter(userTask -> userTask.getId().equals(task.getId()))
                .findFirst();
        if (first.isPresent() && !isInformationTaskValueFromAttributes(first.get())) {
            for (FormProperty formProperty : formProperties) {
                if (formProperty.isRequired() && StringUtils.isEmpty(formProperty.getValue())) {
                    throw new BadRequestException("Not all required properties has been set");
                }
            }
        }
        UserTask lastDefaultTask = findLastDefaultTask(task.getProcessInstanceId());
        if (lastDefaultTask != null && lastDefaultTask.getId().equals(task.getTaskDefinitionKey())) {
            throw new BadRequestException("You cannot complete the last task in the process.");
        }

    }

    public Task getTaskByProcessInstanceId(String processInstanceId) {
        return taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult();
    }

    public Task getTaskByProcessInstanceIdAndTaskId(String processInstanceId, String taskKey) {

        return taskService.createTaskQuery()
                .processInstanceId(processInstanceId)
                .taskDefinitionKey(taskKey)
                .singleResult();
    }

    public TaskFormData getTaskFormData(String taskId) {
        return formService.getTaskFormData(taskId);
    }

    private ProcessInstanceDetailsDTO.Task mapToTask(UserTask element) {
        ProcessInstanceDetailsDTO.Task task = new ProcessInstanceDetailsDTO.Task();
        task.setId(element.getId());
        task.setName(element.getName());
        task.setDocumentation(element.getDocumentation());
        task.setPercentage(element.getAssignee() != null ? Long.valueOf(element.getAssignee()) : null);
        task.setIsInformationTask(isInformationTaskValueFromAttributes(element));
        task.setDaysToComplete(getDaysToCompleteFromAttributes(element));
        task.setAcceptanceUserId(getAcceptanceUserId(element));
        element.getCandidateGroups().stream().findFirst().ifPresent(task::setCandidateGroups);
        element.getCandidateUsers().stream().findFirst().ifPresent(task::setCandidateUsers);
        return task;
    }

    private void setTaskColors(ProcessInstanceDetailsDTO dto, Process mainProcess) {
        List<ExtensionAttribute> fontColor = mainProcess.getAttributes().get("fontColor");
        if (fontColor != null && !fontColor.isEmpty()) {
            String[] split = fontColor.get(0).getValue().split(";");
            if (split.length == 2) {
                dto.setBgColor(split[0]);
                dto.setTextColor(split[1]);
            }
        }
    }

    private void setObjectType(ProcessInstanceDetailsDTO dto, ProcessDefinition processDefinition) {
        dto.setObjectType(processDefinition.getCategory());
    }

    private Task setPropertiesForActiveTask(String processInstanceId, ProcessInstanceDetailsDTO dto) {
        Task task = getActiveTask(processInstanceId);
        ProcessInstanceDetailsDTO.Task activeTask = dto.getTasks()
                .stream()
                .filter(e -> e.getName().equals(Strings.nullToEmpty(task.getName())))
                .findFirst()
                .orElseThrow();
        activeTask.setProperties(formService
                .getTaskFormData(task.getId())
                .getFormProperties()
                .stream()
                .map(this::mapToProperty)
                .collect(Collectors.toList()));
        return task;
    }

    private void setAdditionalFieldsToTasks(ProcessInstanceDetailsDTO dto, Task task) {
        for (ProcessInstanceDetailsDTO.Task t : dto.getTasks()) {
            if (t.getId().equals(task.getTaskDefinitionKey())) {
                t.setStatusAcceptance((String) taskService.getVariableLocal(task.getId(), statusAcceptance));
                t.setActive(true);
                break;
            } else {
                t.setPast(true);
            }
        }
    }

    private Property mapToProperty(FormProperty formProperty) {
        Property property = new Property();
        property.setId(formProperty.getId());
        property.setRequired(formProperty.isRequired());
        property.setType(Property.TypeEnum.valueOf(formProperty.getType().getName().toUpperCase()));
        property.setValue(formProperty.getValue());
        if (property.getType().equals(Property.TypeEnum.ENUM)) {
            setPossibleValues((FormPropertyImpl) formProperty, property);
        }
        return property;
    }

    private void setPossibleValues(FormPropertyImpl formProperty, Property property) {
        Object values = formProperty.getType().getInformation("values");
        property.setPossibleValues(
                ((LinkedHashMap<String, String>) values)
                        .entrySet()
                        .stream()
                        .map(this::mapToPossibleValue)
                        .collect(Collectors.toList()));
    }

    private PossibleValue mapToPossibleValue(Map.Entry<String, String> entry) {
        PossibleValue value = new PossibleValue();
        value.setId(entry.getKey());
        value.setName(entry.getValue());
        return value;
    }

    public ProcessDefinition getProcessDefinition(String processDefinitionId) {
        return repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionId)
                .singleResult();
    }

    private List<UserTask> getListOfTasks(Process process) {
        return process.getFlowElements()
                .stream()
                .filter(UserTask.class::isInstance)
                .map(e -> (UserTask) e)
                .collect(Collectors.toList());
    }

    private Long getAcceptanceUserId(UserTask userTask) {
        Long acceptanceUserId = null;
        Optional<String> candidateUsers = userTask.getCandidateUsers().stream().findFirst();
        if (candidateUsers.isPresent()) {
            String acceptanceAsString = Strings.emptyToNull(BPMNUtil.getAcceptanceId(candidateUsers.get()));
            if (acceptanceAsString != null) {
                acceptanceUserId = Long.parseLong(acceptanceAsString);
            }
        }
        Optional<String> candidateGroups = userTask.getCandidateGroups().stream().findFirst();
        if (candidateGroups.isPresent()) {
            boolean acceptanceFromSupervisor = Boolean.parseBoolean(BPMNUtil.getAcceptanceId(candidateGroups.get()));
            if (acceptanceFromSupervisor) {
                acceptanceUserId = userClient.getSupervisorId(CurrentRequestUtil.getLoggedInAccountEmail());
            }
        }
        return acceptanceUserId;
    }

    public List<String> findAllProcessInstanceIdByTaskAndProcessDefinitionId(String taskDefinitionKey, String processDefinitionId) {
        return taskService.createTaskQuery()
                .taskDefinitionKey(taskDefinitionKey)
                .processDefinitionId(processDefinitionId)
                .active()
                .list()
                .stream()
                .map(TaskInfo::getProcessInstanceId)
                .collect(Collectors.toList());

    }

    public List<String> findAllVersionDefinitionsOfProcessByProcessDefinitionId(String processDefinitionId) {
        String key = repositoryService.createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionId)
                .singleResult().getKey();
        return repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(key)
                .list()
                .stream()
                .map(ProcessDefinition::getId)
                .collect(Collectors.toList());


    }

    public UserTask findLastDefaultTask(ProcessInstance processInstance) {
        return getListOfTasks(processInstance)
                .stream()
                .reduce((a, b) -> b)
                .orElse(null);
    }

    public UserTask findLastDefaultTask(String processInstanceId) {
        return findLastDefaultTask(getProcessInstance(processInstanceId));
    }

    public Boolean isInformationTaskValueFromAttributes(UserTask userTask) {
        String value = userTask.getAttributeValue("http://flowable.org/bpmn", "informationTask");
        if (value != null) {
            return Boolean.parseBoolean(value);
        } else {
            return false;
        }
    }

    private Long getDaysToCompleteFromAttributes(UserTask userTask) {
        String daysToComplete = userTask.getAttributeValue("http://flowable.org/bpmn", "daysToComplete");
        if (daysToComplete != null) {
            return Long.parseLong(daysToComplete);
        } else {
            return null;
        }
    }

    public void completeInformationTask(UserTask userTask, String processInstanceID) {
        Task task = getTaskByProcessInstanceIdAndTaskId(processInstanceID, userTask.getId());
        taskService.complete(task.getId());
    }

    public Boolean isActiveTaskValid(Task task) {
        return formService
                .getTaskFormData(task.getId())
                .getFormProperties()
                .stream()
                .noneMatch(formProperty -> formProperty.isRequired() && formProperty.getValue() == null);
    }
}
