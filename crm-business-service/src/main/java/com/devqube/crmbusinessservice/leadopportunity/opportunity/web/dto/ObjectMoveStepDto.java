package com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ObjectMoveStepDto {

    private Boolean canMove;
    private List<String> possibleStageId;

}
