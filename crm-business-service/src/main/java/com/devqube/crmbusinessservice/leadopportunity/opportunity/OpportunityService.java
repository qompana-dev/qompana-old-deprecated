package com.devqube.crmbusinessservice.leadopportunity.opportunity;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.contact.web.dto.ContactSalesOpportunityDTO;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.dictionary.Word;
import com.devqube.crmbusinessservice.dictionary.WordRepository;
import com.devqube.crmbusinessservice.dictionary.WordSpecifications;
import com.devqube.crmbusinessservice.expense.service.ExpenseService;
import com.devqube.crmbusinessservice.expense.web.dto.TotalCostDTO;
import com.devqube.crmbusinessservice.leadopportunity.AccountsService;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ProcessColor;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.LastViewedOpportunity;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.*;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineHelperService;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineReadService;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.OpportunityProductsService;
import com.devqube.crmbusinessservice.scheduler.NotificationSchedulerService;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.association.RemoveCrmObject;
import com.devqube.crmshared.currency.dto.CurrencyDto;
import com.devqube.crmshared.currency.dto.RateOfMainCurrencyDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.google.common.base.Strings;
import lombok.RequiredArgsConstructor;
import org.flowable.bpmn.model.Process;
import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunitySpecifications.*;
import static com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.OpportunitySearchType.*;
import static com.devqube.crmbusinessservice.scheduler.NotificationJobType.OPPORTUNITY;
import static com.devqube.crmshared.search.CrmObjectType.opportunity;

@Service
@RequiredArgsConstructor
public class OpportunityService {

    private static final Logger log = LoggerFactory.getLogger(OpportunityService.class);
    private final OpportunityRepository opportunityRepository;
    private final UserClient userClient;
    private final ModelMapper modelMapper;
    private final CustomerRepository customerRepository;
    private final ExpenseService expenseService;
    private final ContactRepository contactRepository;
    private final ProcessEngineHelperService processEngineHelperService;
    private final LastViewedOpportunityRepository lastViewedOpportunityRepository;
    private final AccessService accessService;
    private final OpportunityProductsService opportunityProductsService;
    private final AccountsService accountsService;
    private final ProcessEngineReadService processEngineReadService;
    private final NotificationSchedulerService notificationSchedulerService;
    private final WordRepository wordRepository;
    private final UserService userService;
    private static final String COPY = "(kopia)";
    private static final String PROCESS = "process";

    private List<OpportunityKanbanDTO> getOpportunityBySpec(Specification<Opportunity> spec) throws BadRequestException {
        CurrencyDto currencies = userService.getCurrencies();
        return opportunityRepository.findAll(spec).stream()
                .map(element -> mapToOpportunityKanbanDTO(element, currencies))
                .collect(Collectors.toList());
    }

    private Specification<Opportunity> findAllSpecification(String loggedAccountEmail, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return OpportunitySpecifications.findAll(userIds);
    }

    public Page<OpportunityListDTO> findAll(String loggedAccountEmail, String search, Pageable pageable) throws BadRequestException {
        Specification<Opportunity> spec = findAllSpecification(loggedAccountEmail, search);
        if (search != null) {
            spec = spec.and(searchByCustomFields(search));
        }
        if (pageable.getSort().getOrderFor(PROCESS) != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, opportunityRepository.findAll(spec));
        } else {
            return opportunityRepository.findAll(spec, pageable).map(this::mapToOpportunityListDTO);
        }
    }

    public List<OpportunityKanbanDTO> findAll(String loggedAccountEmail, String search, List<String> processDefinitionIds) throws BadRequestException {
        Specification<Opportunity> spec = findAllSpecification(loggedAccountEmail, search);
        addProcessDefinitionIdsToSpecification(spec, processDefinitionIds);
        return getOpportunityBySpec(spec);
    }

    private Specification<Opportunity> findAllCreatedTodaySpecification(String loggedAccountEmail, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return OpportunitySpecifications.findAllByCreatedAfter(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), userIds)
                .and(searchByCustomFields(search));
    }

    public Page<OpportunityListDTO> findAllCreatedToday(String loggedAccountEmail, String search, Pageable pageable) throws BadRequestException {
        Specification<Opportunity> spec = findAllCreatedTodaySpecification(loggedAccountEmail, search);
        if (pageable.getSort().getOrderFor(PROCESS) != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, opportunityRepository.findAll(spec));
        } else {
            return opportunityRepository.findAll(spec, pageable).map(this::mapToOpportunityListDTO);
        }
    }

    public List<OpportunityKanbanDTO> findAllCreatedToday(String loggedAccountEmail, String search, List<String> processDefinitionIds) throws BadRequestException {
        Specification<Opportunity> spec = findAllCreatedTodaySpecification(loggedAccountEmail, search);
        addProcessDefinitionIdsToSpecification(spec, processDefinitionIds);
        return getOpportunityBySpec(spec);
    }

    private Specification<Opportunity> findOpenedSpecification(String loggedAccountEmail, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return findAllOpened(userIds)
                .and(searchByCustomFields(search));
    }

    private void addProcessDefinitionIdsToSpecification(Specification<Opportunity> specification, List<String> processDefinitionIds) {
        if (!processDefinitionIds.isEmpty()) {
            specification = specification.and((root, query, cb) -> root.get("processDefinitionId").in(processDefinitionIds));
        }
    }

    public Page<OpportunityListDTO> findOpened(String loggedAccountEmail, String search, Pageable pageable) throws BadRequestException {
        Specification<Opportunity> spec = findOpenedSpecification(loggedAccountEmail, search);
        if (pageable.getSort().getOrderFor(PROCESS) != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, opportunityRepository.findAll(spec));
        } else {
            return opportunityRepository.findAll(spec, pageable).map(this::mapToOpportunityListDTO);
        }
    }

    public List<OpportunityKanbanDTO> findOpened(String loggedAccountEmail, String search, List<String> processDefinitionIds) throws BadRequestException {
        Specification<Opportunity> spec = findOpenedSpecification(loggedAccountEmail, search);
        addProcessDefinitionIdsToSpecification(spec, processDefinitionIds);
        return getOpportunityBySpec(spec);
    }


    private Specification<Opportunity> findAllClosedSpecification(String loggedAccountEmail, String search, Opportunity.FinishResult finishResult) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return findAllClosedWithResult(userIds, finishResult)
                .and(searchByCustomFields(search));
    }

    public Page<OpportunityListDTO> findAllClosed(String loggedAccountEmail, String search, Pageable pageable, Opportunity.FinishResult finishResult) throws BadRequestException {
        Specification<Opportunity> spec = findAllClosedSpecification(loggedAccountEmail, search, finishResult);
        if (pageable.getSort().getOrderFor(PROCESS) != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, opportunityRepository.findAll(spec));
        } else {
            return opportunityRepository.findAll(spec, pageable).map(this::mapToOpportunityListDTO);
        }
    }

    public List<OpportunityKanbanDTO> findAllClosed(String loggedAccountEmail, String search, List<String> processDefinitionIds, Opportunity.FinishResult finishResult) throws BadRequestException {
        Specification<Opportunity> spec = findAllClosedSpecification(loggedAccountEmail, search, finishResult);
        addProcessDefinitionIdsToSpecification(spec, processDefinitionIds);
        return getOpportunityBySpec(spec);
    }

    public Page<OpportunityListDTO> findAllCustomer(String search, Pageable pageable, String customer) throws BadRequestException {
        Specification<Opportunity> spec = findAllCustomerSpecification(search, customer);
        return opportunityRepository.findAll(spec, pageable).map(this::mapToOpportunityListDTO);
    }

    private Specification<Opportunity> findAllCustomerSpecification(String search, String customer) throws BadRequestException {
        long customerId;
        try {
            customerId = Long.parseLong(customer);
        } catch (NumberFormatException nfe){
            throw new BadRequestException();
        }
        return findByCustomerId(customerId)
                .and(searchByCustomFields(search));
    }

    public Page<OpportunityListDTO> findAllContact(String search, Pageable pageable, String contact) throws BadRequestException {
        Specification<Opportunity> spec = findAllContactSpecification(search, contact);
        return opportunityRepository.findAll(spec, pageable).map(this::mapToOpportunityListDTO);
    }

    private Specification<Opportunity> findAllContactSpecification(String search, String contact) throws BadRequestException {
        long contactId;
        try {
            contactId = Long.parseLong(contact);
        } catch (NumberFormatException nfe){
            throw new BadRequestException();
        }
        return findByContactId(contactId)
                .and(searchByCustomFields(search));
    }

    private Specification<Opportunity> findAllRecentlyViewedSpecification(String loggedAccountEmail, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return findAllByLastViewedAfter(LocalDateTime.of(LocalDate.now(),
                LocalTime.MIDNIGHT).minusDays(3), getUserId(loggedAccountEmail), userIds)
                .and(searchByCustomFields(search));
    }

    public Page<OpportunityListDTO> findAllRecentlyViewed(String loggedAccountEmail, @Valid String search, Pageable pageable) throws BadRequestException {
        Specification<Opportunity> spec = findAllRecentlyViewedSpecification(loggedAccountEmail, search);
        if (pageable.getSort().getOrderFor(PROCESS) != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, opportunityRepository.findAll(spec));
        } else {
            return opportunityRepository.findAll(spec, pageable).map(this::mapToOpportunityListDTO);
        }
    }

    public List<OpportunityKanbanDTO> findAllRecentlyViewed(String loggedAccountEmail, @Valid String search, List<String> processDefinitionIds) throws BadRequestException {
        Specification<Opportunity> spec = findAllRecentlyViewedSpecification(loggedAccountEmail, search);
        addProcessDefinitionIdsToSpecification(spec, processDefinitionIds);
        return getOpportunityBySpec(spec);
    }

    private Specification<Opportunity> findAllRecentlyEditedSpecification(String loggedAccountEmail, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return OpportunitySpecifications.findAllByUpdatedAfter(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), userIds)
                .and(searchByCustomFields(search));
    }

    public Page<OpportunityListDTO> findAllRecentlyEdited(String loggedAccountEmail, String search, Pageable pageable) throws BadRequestException {
        Specification<Opportunity> spec = findAllRecentlyEditedSpecification(loggedAccountEmail, search);
        if (pageable.getSort().getOrderFor(PROCESS) != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, opportunityRepository.findAll(spec));
        } else {
            return opportunityRepository.findAll(spec, pageable).map(this::mapToOpportunityListDTO);
        }
    }

    public List<OpportunityKanbanDTO> findAllRecentlyEdited(String loggedAccountEmail, String search, List<String> processDefinitionIds) throws BadRequestException {
        Specification<Opportunity> spec = findAllRecentlyEditedSpecification(loggedAccountEmail, search);
        addProcessDefinitionIdsToSpecification(spec, processDefinitionIds);
        return getOpportunityBySpec(spec);
    }

    private Specification<Opportunity> findAllForApprovingSpecification(String loggedAccountEmail, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return findAllByAcceptanceUserIdAndOpened(userIds)
                .and(searchByCustomFields(search));
    }
    public Page<OpportunityListDTO> findAllForApproving(String loggedAccountEmail, String search, Pageable pageable) throws BadRequestException {
        Specification<Opportunity> spec = findAllForApprovingSpecification(loggedAccountEmail, search);
        if (pageable.getSort().getOrderFor(PROCESS) != null || pageable.getSort().getOrderFor("updated") != null) {
            return convertSortAndPage(pageable, opportunityRepository.findAll(spec));
        } else {
            return opportunityRepository.findAll(spec, pageable).map(this::mapToOpportunityListDTO);
        }
    }

    public List<OpportunityKanbanDTO> findAllForApproving(String loggedAccountEmail, String search, List<String> processDefinitionIds) throws BadRequestException {
        Specification<Opportunity> spec = findAllForApprovingSpecification(loggedAccountEmail, search);
        addProcessDefinitionIdsToSpecification(spec, processDefinitionIds);
        return getOpportunityBySpec(spec);
    }

    public Page<MobileOpportunityListDto> findAllForMobile(String loggedAccountEmail, Pageable pageable, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return opportunityRepository.findAllByOwnerIdOrKeeperIdForMobile(userIds, pageable, Strings.nullToEmpty(search))
                .map(this::mapToMobileOpportunityListDto);
    }

    public Page<MobileOpportunityListDto> findAllCreatedTodayForMobile(String loggedAccountEmail, Pageable pageable, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return opportunityRepository.findAllByCreatedAfterForMobile(userIds, LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), pageable, Strings.nullToEmpty(search))
                .map(this::mapToMobileOpportunityListDto);
    }

    public Page<MobileOpportunityListDto> findAllRecentlyEditedForMobile(String loggedAccountEmail, Pageable pageable, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return opportunityRepository.findAllByUpdatedAfterForMobile(userIds, LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3), pageable, Strings.nullToEmpty(search))
                .map(this::mapToMobileOpportunityListDto);
    }

    public Page<MobileOpportunityListDto> findAllRecentlyViewedForMobile(String loggedAccountEmail, Pageable pageable, String search) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return opportunityRepository.findAllByLastViewedAfterForMobile(getUserId(loggedAccountEmail), userIds, LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3), pageable, Strings.nullToEmpty(search))
                .map(this::mapToMobileOpportunityListDto);
    }

    public Page<OpportunityListDTO> findAllFinished(String loggedAccountEmail, Pageable pageable) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        List<Opportunity> opportunities = opportunityRepository.findAllFinishedOpportunieties(userIds);
        return this.convertSortAndPage(pageable, opportunities);
    }

    public List<OpportunitySearchVariantsDtoOut> findAllSearchVariants(String loggedAccountEmail, String toSearch) throws BadRequestException {
        List<OpportunitySearchVariantsDtoOut> searchVariants = new ArrayList<>();

        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        Specification<Opportunity> commonSpec = findByOwnerOrCreatorIdIn(userIds);

        searchVariants.add(new OpportunitySearchVariantsDtoOut()
                .setType(NAME)
                .setVariants(opportunityRepository.findAll(commonSpec.and(findByName(toSearch))).stream()
                        .map(Opportunity::getName)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        searchVariants.add(new OpportunitySearchVariantsDtoOut()
                .setType(CUSTOMER_NAME)
                .setVariants(opportunityRepository.findAll(commonSpec.and(findByCustomerName(toSearch))).stream()
                        .map(opportunity -> opportunity.getCustomer().getName())
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));


        searchVariants.add(new OpportunitySearchVariantsDtoOut()
                .setType(CONTACT_NAME)
                .setVariants(opportunityRepository.findAll(commonSpec.and(findByContactName(toSearch))).stream()
                        .map(opportunity -> opportunity.getContact().getName())
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        List<Opportunity> opportunityWithNotEmptySourceOrSubSourceOfAcquisition = opportunityRepository.findAll(
                commonSpec.and(findBySourceOfAcquisitionIsNonNull())
                        .or(findBySubSourceOfAcquisitionIsNonNull()));
        List<Long> sourceAndSubSourceOfAcquisitionIdList = getSourceAndSubSourceOfAcquisitionIdList(opportunityWithNotEmptySourceOrSubSourceOfAcquisition);
        searchVariants.add(new OpportunitySearchVariantsDtoOut()
                .setType(SOURCE)
                .setVariants(wordRepository.findAll(WordSpecifications.findByIdIn(sourceAndSubSourceOfAcquisitionIdList)
                        .and(WordSpecifications.findByName(toSearch))).stream()
                        .map(Word::getName)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())));

        return searchVariants;
    }

    public OpportunitySaveDTO createOpportunity(OpportunitySaveDTO dto, String loggedAccountEmail) throws EntityNotFoundException, BadRequestException {
        checkIfValidObjectAndThrowIfNot(dto, true);
        ProcessInstance processInstance = processEngineHelperService.startProcessInstance(dto.getProcessDefinitionId());
        Optional<UserTask> informationTask = processEngineHelperService.getFirstInformationTask(processInstance.getProcessInstanceId());
        informationTask.ifPresent(value -> processEngineHelperService.completeInformationTask(value, processInstance.getId()));
        dto = accessService.assignAndVerify(dto);
        Opportunity opportunity = modelMapper.map(dto, Opportunity.class);
        opportunity.setCreator(getUserId(loggedAccountEmail));
        opportunity.setCustomer(customerRepository.findById(opportunity.getCustomer().getId()).orElseThrow(EntityNotFoundException::new));
        opportunity.setContact(contactRepository.findById(opportunity.getContact().getId()).orElseThrow(EntityNotFoundException::new));
        opportunity.setProcessDefinitionId(dto.getProcessDefinitionId());
        opportunity.setProcessInstanceId(processInstance.getProcessInstanceId());
        startJobForActiveProcessTask(processInstance, opportunity.getOwnerOneId(), opportunity.getName());
        return modelMapper.map(opportunityRepository.save(opportunity), OpportunitySaveDTO.class);
    }

    @Transactional
    public void deleteOpportunity(Long id) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Opportunity byId = opportunityRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(CurrentRequestUtil.getLoggedInAccountEmail());
        checkIfHasPermissionAndThrowIfNot(userIds, byId);
        opportunityProductsService.removeOpportunityProductsByOpportunityId(id);
        opportunityRepository.deleteById(id);
        RemoveCrmObject.addObjectToRemove(opportunity, id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteOpportunities(List<Long> ids) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        for (Long id : ids) {
            deleteOpportunity(id);
        }
    }

    public OpportunitySaveDTO getOpportunityForEdit(Long id) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Opportunity opportunity = opportunityRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(CurrentRequestUtil.getLoggedInAccountEmail());
        checkIfHasPermissionAndThrowIfNot(userIds, opportunity);
        OpportunitySaveDTO result = modelMapper.map(opportunity, OpportunitySaveDTO.class);
        result.setCanEditAmount(opportunity.getOpportunityProducts() == null || opportunity.getOpportunityProducts().isEmpty());
        try {
            TotalCostDTO totalCost = expenseService.getTotalCost(new CrmObject(opportunity.getId(), CrmObjectType.opportunity, opportunity.getName(), opportunity.getCreated()), null);
            if (totalCost.getMainCurrency().equals(opportunity.getCurrency())) {
                result.setProfitAmount(nullToZero(result.getProductAmount()) - totalCost.getTotalCost());
            } else {
                Double totalCostInCurrency = expenseService.getTotalCostInCurrency(totalCost, opportunity.getCurrency());
                result.setProfitAmount(nullToZero(result.getProductAmount()) - totalCostInCurrency);
            }
        } catch (BadRequestException e) {
            e.printStackTrace();
        }

        return result;
    }

    public OpportunitySaveDTO modifyOpportunity(String loggedAccountEmail, Long id, OpportunitySaveDTO dto, String authControllerType)
            throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        Opportunity fromDb = opportunityRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        checkIfHasPermissionAndThrowIfNot(userIds, fromDb);
        if (!authControllerType.equals("detailsCard")) {
            checkIfValidObjectAndThrowIfNot(dto, false);
        }
        OpportunitySaveDTO fromDbDTO = modelMapper.map(fromDb, OpportunitySaveDTO.class);
        fromDbDTO = accessService.assignAndVerify(fromDbDTO, dto);
        Opportunity toSave = modelMapper.map(fromDbDTO, Opportunity.class);
        toSave.setAcceptanceUserId(fromDb.getAcceptanceUserId());
        toSave.setConvertedFromLead(fromDb.getConvertedFromLead());
        setValuesToOpportunity(toSave, fromDb.getProcessInstanceId());
        return modelMapper.map(opportunityRepository.save(toSave), OpportunitySaveDTO.class);
    }

    public OpportunityDetailsDTO getById(String loggedAccountEmail, Long id) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Opportunity byId = opportunityRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        Long userId = getUserId(CurrentRequestUtil.getLoggedInAccountEmail());
        checkIfHasPermissionAndThrowIfNot(userIds, byId);
        OpportunityDetailsDTO dto = modelMapper.map(byId, OpportunityDetailsDTO.class);
        dto.setCustomerName(byId.getCustomer().getName());
        dto.setContactName(getContactNameForOpportunityDetail(byId));
        ProcessInstance processInstance = processEngineHelperService.getProcessInstance(byId.getProcessInstanceId());
        processEngineHelperService.setTasksToDTO(byId.getProcessInstanceId(), dto, processInstance);
        updateLastViewedTime(byId, userId);
        return dto;
    }

    public boolean opportunityExistsByContactId(Long id) {
        return opportunityRepository.existsByContactId(id);
    }

    public boolean opportunityExistsByCustomerId(Long id) {
        return opportunityRepository.existsByCustomerId(id);
    }

    public List<ContactSalesOpportunityDTO> findAllByContactId(Long contactId, List<Long> userIds) {
        return opportunityRepository.findAllByContact(contactId, userIds).stream()
                .map(this::mapToContactSalesOpportunity)
                .collect(Collectors.toList());
    }

    public List<Opportunity> findAllByCustomerId(Long contactId, List<Long> userIds) {
        return opportunityRepository.findAllByCustomer(contactId, userIds);
    }

    private void updateLastViewedTime(Opportunity opportunity, Long userId) {
        LastViewedOpportunity newLastViewed = LastViewedOpportunity.builder().userId(userId).opportunity(opportunity).build();
        LastViewedOpportunity lastViewedOpportunity = (opportunity.getLastViewedOpportunities() == null) ? newLastViewed : opportunity.getLastViewedOpportunities().stream()
                .filter(lv -> lv.getUserId().equals(userId)).findFirst()
                .orElse(newLastViewed);

        lastViewedOpportunity.setLastViewed(LocalDateTime.now());
        lastViewedOpportunityRepository.save(lastViewedOpportunity);
    }

    private void startJobForActiveProcessTask(ProcessInstance processInstance, Long keeperId, String opportunityName) {
        UserTask activeTask = processEngineHelperService.getActiveUserTask(processInstance);
        String taskId = activeTask.getId();
        String taskName = activeTask.getName();
        Optional<Long> daysToCompleteOpt = getDaysToCompleteTask(activeTask);
        daysToCompleteOpt.ifPresent(daysToComplete ->
                notificationSchedulerService.scheduleNotificationJob(OPPORTUNITY, processInstance.getProcessInstanceId(),
                        keeperId, opportunityName, taskId, taskName, daysToComplete));
    }

    private void checkIfHasPermissionAndThrowIfNot(List<Long> userIds, Opportunity fromDb) throws PermissionDeniedException {
        if (!userIds.contains(fromDb.getOwnerOneId()) && !userIds.contains(fromDb.getOwnerTwoId()) &&
                !userIds.contains(fromDb.getOwnerThreeId()) && !userIds.contains(fromDb.getCreator())) {
            throw new PermissionDeniedException();
        }
    }

    private void setValuesToOpportunity(Opportunity updated, String processInstanceId) throws BadRequestException {
        updated.setCustomer(customerRepository.findById(updated.getCustomer().getId()).orElseThrow(BadRequestException::new));
        updated.setContact(contactRepository.findById(updated.getContact().getId()).orElseThrow(BadRequestException::new));
        updated.setProcessInstanceId(processInstanceId);
    }

    private OpportunityListDTO mapToOpportunityListDTO(Opportunity opportunity) {
        ProcessInstance processInstance = processEngineHelperService.getProcessInstance(opportunity.getProcessInstanceId());
        Task activeTask = processEngineHelperService.getActiveTask(opportunity.getProcessInstanceId());
        ProcessDefinition processDefinition = processEngineHelperService.getProcessDefinition(processInstance.getProcessDefinitionId());
        List<UserTask> processTasks = getListOfTasks(processInstance);
        final OpportunityListDTO dto = new OpportunityListDTO(opportunity, processDefinition.getName(), processDefinition.getVersion(),
                getCurrentTaskNumber(activeTask), (long) processTasks.size() - 1);
        dto.setProcessInstanceId(processInstance.getId());
        setAbilityToMoveToFollowingStep(dto, activeTask, processTasks, opportunity.getClosingDate());
        setAbilityToRollBackToPreviousStep(dto, activeTask, processTasks, opportunity.getClosingDate());
        return dto;
    }

    private OpportunityKanbanDTO mapToOpportunityKanbanDTO(Opportunity opportunity, CurrencyDto currency) {
        ProcessInstance processInstance = processEngineHelperService.getProcessInstance(opportunity.getProcessInstanceId());
        Task activeTask = processEngineHelperService.getActiveTask(opportunity.getProcessInstanceId());
        ProcessDefinition processDefinition = processEngineHelperService.getProcessDefinition(processInstance.getProcessDefinitionId());
        List<UserTask> processTasks = getListOfTasks(processInstance);
        Double amountInDefaultCurrency = getAmountInDefaultCurrency(opportunity, currency);
        Optional<UserTask> activeUserTask = processTasks.stream()
                .filter(userTask -> userTask.getId().equals(activeTask.getTaskDefinitionKey()))
                .findFirst();
        Long daysToTaskComplete = 1L; //default if not found
        if (activeUserTask.isPresent()) {
            Long daysToCompleteFromAttributes = processEngineReadService.getDaysToCompleteFromAttributes(activeUserTask.get());
            if (daysToCompleteFromAttributes != null) {
                daysToTaskComplete = daysToCompleteFromAttributes;
            }
        }

        final OpportunityKanbanDTO dto = new OpportunityKanbanDTO(opportunity, amountInDefaultCurrency, processDefinition.getName(), processDefinition.getVersion(),
                getCurrentTaskNumber(activeTask), (long) processTasks.size() - 1, daysToTaskComplete);
        dto.setProcessInstanceId(processInstance.getId());
        setAbilityToMoveToFollowingStep(dto, activeTask, processTasks, opportunity.getClosingDate());
        setAbilityToRollBackToPreviousStep(dto, activeTask, processTasks, opportunity.getClosingDate());
        return dto;
    }

    private Double getAmountInDefaultCurrency(Opportunity opportunity, CurrencyDto currency) {
        Map<String, Double> ratesOfMainCurrencyMap = currency.getRatesOfMainCurrency().stream()
                .collect(Collectors.toMap(RateOfMainCurrencyDto::getCurrency, RateOfMainCurrencyDto::getRate));
        ratesOfMainCurrencyMap.put(currency.getSystemCurrency(), 1.0d);
        var amount = opportunity.getAmount() != null ? opportunity.getAmount() : 0;
        return ratesOfMainCurrencyMap.get(opportunity.getCurrency()) * amount;

    }

    private void setAbilityToMoveToFollowingStep(OpportunityListDTO dto, Task activeTask,
                                                 List<UserTask> processTasks, LocalDateTime closingDate) {
        if (closingDate != null) {
            dto.setCanBeMovedToFollowingStep(false);
            dto.setFollowingStepName(null);
            return;
        }
        if (dto.getAcceptanceUserId() != null) {
            dto.setCanBeMovedToFollowingStep(false);
            dto.setFollowingStepName(null);
            return;
        }
        long currentTaskNumber = getCurrentTaskNumber(activeTask);
        Boolean isActiveTaskValid = processEngineHelperService.isActiveTaskValid(activeTask);

        if (isActiveTaskValid) {
            Optional<UserTask> found = processTasks.stream()
                    .filter(e -> e.getId().equals("task" + (currentTaskNumber + 1)))
                    .findFirst();
            if (found.isPresent()) {
                dto.setCanBeMovedToFollowingStep(true);
                dto.setFollowingStepName(found.get().getName().equals("") ? null : found.get().getName());
            } else {
                dto.setCanBeMovedToFollowingStep(false);
                dto.setFollowingStepName(null);
            }
        } else {
            dto.setCanBeMovedToFollowingStep(false);
            dto.setFollowingStepName(null);
        }
    }

    private void setAbilityToMoveToFollowingStep(OpportunityKanbanDTO dto, Task activeTask,
                                                 List<UserTask> processTasks, LocalDateTime closingDate) {
        if (closingDate != null || dto.getAcceptanceUserId() != null) {
            dto.setCanBeMoveForward(new ObjectMoveStepDto(false, Collections.emptyList()));
            return;
        }

        long currentTaskNumber = getCurrentTaskNumber(activeTask);
        Boolean isActiveTaskValid = processEngineHelperService.isActiveTaskValid(activeTask);

        if (isActiveTaskValid) {
            Optional<UserTask> found = processTasks.stream()
                    .filter(e -> e.getId().equals("task" + (currentTaskNumber + 1)))
                    .findFirst();
            if (found.isPresent()) {
                dto.setCanBeMoveForward(new ObjectMoveStepDto(true, List.of(found.get().getId())));
            } else {
                dto.setCanBeMoveForward(new ObjectMoveStepDto(false, Collections.emptyList()));
            }
        } else {
            dto.setCanBeMoveForward(new ObjectMoveStepDto(false, Collections.emptyList()));
        }
    }

    private void setAbilityToRollBackToPreviousStep(OpportunityListDTO dto, Task activeTask, List<UserTask> processTasks, LocalDateTime closingDate) {
        if (closingDate != null) {
            dto.setCanBeRolledBackToPreviousStep(false);
            dto.setPreviousStepName(null);
            return;
        }
        long currentTaskNumber = getCurrentTaskNumber(activeTask);
        Optional<UserTask> find = processTasks.stream()
                .filter(e -> e.getId().equals("task" + (currentTaskNumber - 1)))
                .findFirst();
        if (find.isPresent() && !processEngineHelperService.isInformationTaskValueFromAttributes(find.get())) {
            dto.setCanBeRolledBackToPreviousStep(true);
            dto.setPreviousStepName(find.get().getName());
        } else {
            dto.setCanBeRolledBackToPreviousStep(false);
            dto.setPreviousStepName(null);
        }
    }

    private void setAbilityToRollBackToPreviousStep(OpportunityKanbanDTO dto, Task activeTask, List<UserTask> processTasks, LocalDateTime closingDate) {
        if (closingDate != null) {
            dto.setCanBeMoveBackward(new ObjectMoveStepDto(false, Collections.emptyList()));
            return;
        }

        long currentTaskNumber = getCurrentTaskNumber(activeTask);
        Optional<UserTask> find = processTasks.stream()
                .filter(e -> e.getId().equals("task" + (currentTaskNumber - 1)))
                .findFirst();
        if (find.isPresent() && !processEngineHelperService.isInformationTaskValueFromAttributes(find.get())) {
            dto.setCanBeMoveBackward(new ObjectMoveStepDto(true, List.of(find.get().getId())));
        } else {
            dto.setCanBeMoveBackward(new ObjectMoveStepDto(false, Collections.emptyList()));
        }
    }

    private MobileOpportunityListDto mapToMobileOpportunityListDto(Opportunity opportunity) {
        ProcessInstance processInstance = processEngineHelperService.getProcessInstance(opportunity.getProcessInstanceId());
        ProcessColor processColor = processEngineReadService.getProcessColor(processEngineHelperService.getMainProcess(processInstance));
        Task task = processEngineHelperService.getActiveTask(opportunity.getProcessInstanceId());
        return new MobileOpportunityListDto(opportunity, getCurrentTaskNumber(task),
                (long) getListOfTasks(processInstance).size() - 1, processColor);
    }

    private ContactSalesOpportunityDTO mapToContactSalesOpportunity(Opportunity opportunity) {
        ContactSalesOpportunityDTO dto = modelMapper.map(opportunity, ContactSalesOpportunityDTO.class);
        ProcessInstance processInstance = processEngineHelperService.getProcessInstance(opportunity.getProcessInstanceId());
        Task task = processEngineHelperService.getActiveTask(opportunity.getProcessInstanceId());
        dto.setStatus(getCurrentTaskNumber(task));
        dto.setMaxStatus((long) getListOfTasks(processInstance).size() - 1);
        return dto;
    }

    private Long getUserId(String accountEmail) throws BadRequestException {
        try {
            return userClient.getMyAccountId(accountEmail);
        } catch (Exception e) {
            throw new BadRequestException("user not found");
        }
    }

    private long getCurrentTaskNumber(Task task) {
        return Long.parseLong(task.getTaskDefinitionKey().replace("task", ""));
    }

    private List<UserTask> getListOfTasks(ProcessInstance processInstance) {
        return this.getListOfTasks(processEngineHelperService.getMainProcess(processInstance));
    }

    private List<UserTask> getListOfTasks(Process process) {
        return process.getFlowElements()
                .stream()
                .filter(UserTask.class::isInstance)
                .map(e -> (UserTask) e)
                .collect(Collectors.toList());
    }

    private void checkIfValidObjectAndThrowIfNot(OpportunitySaveDTO dto, boolean creation) throws BadRequestException {
        if (creation && dto.getProcessDefinitionId() == null && dto.getCreateDate() == null) {
            throw new BadRequestException();
        }

        long sum = 0L;
        sum += nullToZero(dto.getOwnerOnePercentage()) + nullToZero(dto.getOwnerTwoPercentage()) +
                nullToZero(dto.getOwnerThreePercentage());
        if (sum != 100L) {
            throw new BadRequestException();
        }
    }

    public List<Opportunity> findAllAvailableForUserAndContaining(String loggedAccountEmail, String term) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return opportunityRepository.findAllAvailableForUserAndContaining(term, userIds);
    }

    public List<Opportunity> findAllContaining(String pattern) throws BadRequestException {
        return opportunityRepository.findByNameContaining(pattern, accountsService.getAllEmployeesBySupervisorEmail(CurrentRequestUtil.getLoggedInAccountEmail()));
    }

    public List<Opportunity> findByIds(List<Long> ids) {
        return opportunityRepository.findByIds(ids);
    }

    public List<Long> getAllOpportunityWithAccess(String loggedAccountEmail) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return opportunityRepository.findAllAvailableForUser(userIds);
    }

    public List<Opportunity> findAllOpenedAndAvailableForUser(String loggedAccountEmail) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        return opportunityRepository.findAllOpenedAndAvailableForUser(userIds);
    }

    public void finishOpportunity(String email, Long id, OpportunityFinishDto opportunityFinishDto) throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        Opportunity opportunity = opportunityRepository.findById(id).orElseThrow(EntityNotFoundException::new);

        Long myAccountId = userClient.getMyAccountId(email);
        List<Long> authorizedUsers = Arrays.asList(opportunity.getOwnerOneId(), opportunity.getOwnerTwoId(), opportunity.getOwnerThreeId(), opportunity.getCreator());
        if (!authorizedUsers.contains(myAccountId)) {
            throw new PermissionDeniedException();
        }

        if (opportunity.getFinishResult() != null) {
            throw new BadRequestException();
        }

        if (opportunityFinishDto.getFinishResult().equals(Opportunity.FinishResult.ERROR) &&
                opportunityFinishDto.getRejectionReason() == null) {
            throw new BadRequestException();
        }

        opportunity.setFinishResult(opportunityFinishDto.getFinishResult());
        opportunity.setClosingDate(LocalDateTime.now());
        opportunity.setRejectionReason(opportunityFinishDto.getRejectionReason());
        opportunityRepository.save(opportunity);
    }

    public void finishOpportunity(String email, List<Long> ids, OpportunityFinishDto opportunityFinishDto) {
        for (Long id : ids) {
            try {
                finishOpportunity(email, id, opportunityFinishDto);
            } catch (EntityNotFoundException e) {
                log.warn(String.format("EntityNotFoundException. Opportunity with id=%s is not found.", id));
            } catch (PermissionDeniedException e) {
                log.warn(String.format("PermissionDeniedException. User %s has no permission to finish opportunity with id=%s.", email, id));
            } catch (BadRequestException e) {
                log.warn(String.format("BadRequestException. Opportunity with id=%s is already finished or rejection reason in DTO is null", id));
            }
        }
    }

    public String getCurrentTaskForOpportunityProcessId(String processId) {
        Task activeTask = this.processEngineHelperService.getActiveTask(processId);
        return (activeTask == null) ? null : activeTask.getName();
    }

    private static long nullToZero(Long value) {
        return value == null ? 0L : value;
    }

    private static double nullToZero(Double value) {
        return value == null ? 0D : value;
    }

    private String getContactNameForOpportunityDetail(Opportunity opportunity) {
        if (opportunity.getContact() != null) {
            String contactName = Strings.nullToEmpty(opportunity.getContact().getName());
            String contactSurname = Strings.nullToEmpty(opportunity.getContact().getSurname());
            boolean addContactSeparator = opportunity.getContact().getName() != null && opportunity.getContact().getSurname() != null;
            String result = contactName + ((addContactSeparator) ? " " : "") + contactSurname;
            if (result.length() > 0) {
                return result;
            }
        }
        return null;
    }

    public List<MobileOpportunityListDto> getOpportunitiesForCrmObject(String loggedAccountEmail, Long objectId, CrmObjectType crmObjectType) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        switch (crmObjectType) {
            case contact:
                return opportunityRepository.findAllByContact(objectId, userIds).stream().map(this::mapToMobileOpportunityListDto).collect(Collectors.toList());
            case customer:
                return opportunityRepository.findAllByCustomer(objectId, userIds).stream().map(this::mapToMobileOpportunityListDto).collect(Collectors.toList());
            default:
                throw new BadRequestException();
        }
    }

    private PageImpl<OpportunityListDTO> convertSortAndPage(Pageable pageable, List<Opportunity> opportunities) {
        List<OpportunityListDTO> opportunityListDTOs = opportunities.stream()
                .map(this::mapToOpportunityListDTO)
                .sorted(SortStrategyFactory.create(pageable.getSort()))
                .collect(Collectors.toList());

        PagedListHolder<OpportunityListDTO> pagedListHolder = new PagedListHolder<>(opportunityListDTOs);
        pagedListHolder.setPageSize(pageable.getPageSize());
        pagedListHolder.setPage(pageable.getPageNumber());

        return new PageImpl<>(
                pagedListHolder.getPageList(),
                PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort()),
                opportunityListDTOs.size()
        );
    }

    private Optional<Long> getDaysToCompleteTask(UserTask activeTask) {
        if (activeTask.getAttributes().containsKey("daysToComplete")) {
            return Optional.ofNullable(Long.parseLong(activeTask.getAttributes().get("daysToComplete").get(0).getValue()));
        }
        return Optional.empty();
    }

    private List<Long> getSourceAndSubSourceOfAcquisitionIdList(List<Opportunity> opportunityList) {
        return Stream.concat(
                opportunityList.stream()
                        .filter(opportunity -> opportunity.getSourceOfAcquisition() != null)
                        .map(Opportunity::getSourceOfAcquisition),
                opportunityList.stream()
                        .filter(opportunity -> opportunity.getSubSourceOfAcquisition() != null)
                        .map(Opportunity::getSubSourceOfAcquisition))
                .distinct()
                .collect(Collectors.toList());
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class, BadRequestException.class})
    public OpportunitySaveDTO copyOpportunity(String loggedAccountEmail, Long opportunityId) throws EntityNotFoundException, BadRequestException {
        Opportunity original = opportunityRepository.findById(opportunityId).orElseThrow(EntityNotFoundException::new);

        ProcessInstance processInstance = processEngineHelperService.startProcessInstance(original.getProcessDefinitionId());
        Optional<UserTask> informationTask = processEngineHelperService.getFirstInformationTask(processInstance.getProcessInstanceId());
        informationTask.ifPresent(value -> processEngineHelperService.completeInformationTask(value, processInstance.getId()));

        Opportunity copied = new Opportunity();
        copied.setName(original.getName() + COPY);
        copied.setCustomer(original.getCustomer());
        copied.setContact(original.getContact());
        copied.setAmount(original.getAmount());
        copied.setCurrency(original.getCurrency());
        copied.setProbability(original.getProbability());
        copied.setDescription(original.getDescription());
        copied.setSourceOfAcquisition(original.getSourceOfAcquisition());
        copied.setSubSourceOfAcquisition(original.getSubSourceOfAcquisition());
        copied.setSubjectOfInterest(original.getSubjectOfInterest());
        copied.setCreator(getUserId(loggedAccountEmail));
        copied.setOwnerOneId(original.getOwnerOneId());
        copied.setOwnerOnePercentage(original.getOwnerOnePercentage());
        copied.setOwnerTwoId(original.getOwnerTwoId());
        copied.setOwnerTwoPercentage(original.getOwnerTwoPercentage());
        copied.setOwnerThreeId(original.getOwnerThreeId());
        copied.setOwnerThreePercentage(original.getOwnerThreePercentage());
        copied.setPriority(original.getPriority());

        copied.setProcessDefinitionId(original.getProcessDefinitionId());
        copied.setProcessInstanceId(processInstance.getProcessInstanceId());

        copied.setCreateDate(original.getCreateDate());
        copied.setFinishDate(original.getFinishDate());
        startJobForActiveProcessTask(processInstance, copied.getOwnerOneId(), copied.getName());
        return modelMapper.map(opportunityRepository.save(copied), OpportunitySaveDTO.class);
    }
}
