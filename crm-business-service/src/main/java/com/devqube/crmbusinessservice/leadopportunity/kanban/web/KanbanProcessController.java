package com.devqube.crmbusinessservice.leadopportunity.kanban.web;

import com.devqube.crmbusinessservice.leadopportunity.kanban.KanbanLeadService;
import com.devqube.crmbusinessservice.leadopportunity.kanban.KanbanOpportunityService;
import com.devqube.crmbusinessservice.leadopportunity.kanban.web.dto.*;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.OpportunityFilterStrategy;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class KanbanProcessController implements KanbanProcessesApi {

    private final KanbanLeadService kanbanLeadService;

    private final KanbanOpportunityService kanbanOpportunityService;

    @Autowired
    public KanbanProcessController(KanbanLeadService kanbanLeadService, KanbanOpportunityService kanbanOpportunityService) {
        this.kanbanLeadService = kanbanLeadService;
        this.kanbanOpportunityService = kanbanOpportunityService;
    }

    @Override
    @AuthController(name = "getProcessesOverview")
    public ResponseEntity<List<KanbanLeadProcessOverview>> getProcessesOverviewLead(String loggedAccountEmail, String filterStrategy) {
        OpportunityFilterStrategy opportunityFilterStrategy = OpportunityFilterStrategy.getByValue(filterStrategy);
        if (opportunityFilterStrategy == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        try {
            return ResponseEntity.ok(kanbanLeadService.getProcessesOverview());
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getProcessesOverview")
    public ResponseEntity<List<KanbanOpportunityProcessOverview>> getProcessesOverviewOpportunity(String loggedAccountEmail, String filterStrategy, String search) {
        OpportunityFilterStrategy opportunityFilterStrategy = OpportunityFilterStrategy.getByValue(filterStrategy);
        if (opportunityFilterStrategy == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        try {
            return ResponseEntity.ok(kanbanOpportunityService.getProcessesOverview(loggedAccountEmail, opportunityFilterStrategy, search));
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getProcessKanbanDetails")
    public ResponseEntity<List<KanbanLeadTaskDetails>> getLeadProcessKanbanDetails(String processDefinitionId) {
        try {
            List<KanbanLeadTaskDetails> processDetails = kanbanLeadService.getProcessDetails(processDefinitionId);
            return ResponseEntity.ok(processDetails);
        } catch (EntityNotFoundException | BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "getProcessKanbanDetails")
    public ResponseEntity<List<KanbanOpportunityTaskDetails>> getOpportunityProcessKanbanDetails(String processDefinitionId, String filterStrategy, String search) {
        OpportunityFilterStrategy opportunityFilterStrategy = OpportunityFilterStrategy.getByValue(filterStrategy);
        if (opportunityFilterStrategy == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        try {
            List<KanbanOpportunityTaskDetails> processDetails = kanbanOpportunityService.getProcessDetails(processDefinitionId, opportunityFilterStrategy, search);
            return ResponseEntity.ok(processDetails);
        } catch (EntityNotFoundException | BadRequestException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "getLeadForKanban")
    public ResponseEntity<KanbanLead> getLeadForKanban(Long id) {
        try {
            KanbanLead leadOnKanbanById = kanbanLeadService.getLeadOnKanbanById(id);
            return ResponseEntity.ok(leadOnKanbanById);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


}
