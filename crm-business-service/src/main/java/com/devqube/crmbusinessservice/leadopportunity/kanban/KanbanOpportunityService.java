package com.devqube.crmbusinessservice.leadopportunity.kanban;


import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.goal.TaskClient;
import com.devqube.crmbusinessservice.leadopportunity.kanban.web.dto.KanbanOpportunity;
import com.devqube.crmbusinessservice.leadopportunity.kanban.web.dto.KanbanOpportunityProcessOverview;
import com.devqube.crmbusinessservice.leadopportunity.kanban.web.dto.KanbanOpportunityTaskDetails;
import com.devqube.crmbusinessservice.leadopportunity.kanban.web.dto.Owner;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ProcessColor;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityService;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.OpportunityFilterStrategy;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.OpportunityKanbanDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineReadService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.task.TaskSummaryDto;
import com.devqube.crmshared.task.TaskTimeCategoryEnum;
import com.devqube.crmshared.user.dto.AccountInfoDto;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.flowable.bpmn.model.ExtensionAttribute;
import org.flowable.bpmn.model.Process;
import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.FormService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class KanbanOpportunityService {

    private final ProcessEngineReadService processEngineReadService;
    private final OpportunityService opportunityService;
    private final RepositoryService repositoryService;
    private final TaskService taskService;
    private final FormService formService;
    private final TaskClient taskClient;
    private final UserClient userClient;

    @Autowired
    public KanbanOpportunityService(ProcessEngineReadService processEngineReadService, OpportunityService opportunityService, RepositoryService repositoryService, TaskService taskService, FormService formService, TaskClient taskClient, UserClient userClient) {
        this.processEngineReadService = processEngineReadService;
        this.opportunityService = opportunityService;
        this.repositoryService = repositoryService;
        this.taskService = taskService;
        this.formService = formService;
        this.taskClient = taskClient;
        this.userClient = userClient;
    }

    public List<KanbanOpportunityProcessOverview> getProcessesOverview(String loggedAccountEmail, OpportunityFilterStrategy filterStrategy, String search) throws BadRequestException {
        List<ProcessDefinition> list = getAllProcesses();
        List<OpportunityKanbanDTO> filteredOpportunity = getFilteredOpportunity(loggedAccountEmail, filterStrategy, search);
        Map<String, OpportunityKanbanDTO> processOpportunityMap = filteredOpportunity.stream()
                .collect(Collectors.toMap(OpportunityKanbanDTO::getProcessInstanceId, m -> m));
        List<Task> allTasksInstances = getAllTasksInstances();
        return list.stream()
                .map(process -> getKanbanProcessOverview(processOpportunityMap, allTasksInstances, process))
                .filter(overview -> overview.getSize() != 0)
                .collect(Collectors.toList());
    }

    public List<KanbanOpportunityTaskDetails> getProcessDetails(String processDefinitionId, OpportunityFilterStrategy filterStrategy, String search) throws EntityNotFoundException, BadRequestException {
        ProcessDefinition processDefinition = Optional.ofNullable(getProcessDefinition(processDefinitionId)).orElseThrow(EntityNotFoundException::new);
        List<OpportunityKanbanDTO> filteredOpportunity = getFilteredOpportunity(CurrentRequestUtil.getLoggedInAccountEmail(), filterStrategy, search, processDefinitionId);
        Map<String, OpportunityKanbanDTO> processOpportunityMap = filteredOpportunity.stream()
                .collect(Collectors.toMap(OpportunityKanbanDTO::getProcessInstanceId, m -> m));
        List<Task> allTasksInstances = getAllTasksInstances();
        Map<Long, AccountInfoDto> ownersAccountMap = getOpportunityOwnersAccountMap(filteredOpportunity);
        Map<UserTask, List<KanbanOpportunity>> processWithLeads = getAllOpportunityGroupedByTasksForProces(processDefinition, allTasksInstances, processOpportunityMap, ownersAccountMap);
        List<Long> opportunityIds = processWithLeads.values().stream().flatMap(Collection::stream).map(KanbanOpportunity::getId).collect(Collectors.toList());
        Map<Long, TaskSummaryDto> taskSummary = taskClient.getAllTaskSummary(CrmObjectType.opportunity, opportunityIds, List.of(TaskTimeCategoryEnum.OLD, TaskTimeCategoryEnum.PLANNED));
        return processWithLeads.entrySet().stream()
                .map(entry -> new KanbanOpportunityTaskDetails(entry.getKey(), fillSummaryTaskData(entry.getValue(), taskSummary)))
                .collect(Collectors.toList());
    }

    private List<KanbanOpportunity> fillSummaryTaskData(List<KanbanOpportunity> opportunityList, Map<Long, TaskSummaryDto> taskSummary) {
        opportunityList.forEach(opportunity -> opportunity.setTaskSummaryDto(taskSummary.get(opportunity.getId())));
        return opportunityList;
    }

    private List<ProcessDefinition> getAllProcesses() {
        return processEngineReadService.getProcessDefinitions("OPPORTUNITY");
    }

    private List<OpportunityKanbanDTO> getFilteredOpportunity(String loggedAccountEmail, OpportunityFilterStrategy filterStrategy, String search) throws BadRequestException {
        return getFilteredOpportunity(loggedAccountEmail, filterStrategy, search, Collections.emptyList());
    }

    private List<OpportunityKanbanDTO> getFilteredOpportunity(String loggedAccountEmail, OpportunityFilterStrategy filterStrategy, String search, String processDefinitionId) throws BadRequestException {
        return getFilteredOpportunity(loggedAccountEmail, filterStrategy, search, Collections.singletonList(processDefinitionId));
    }

    private List<OpportunityKanbanDTO> getFilteredOpportunity(String loggedAccountEmail, OpportunityFilterStrategy filterStrategy, String search, List<String> processDefinitionIds) throws BadRequestException {
        switch (filterStrategy) {
            case OPENED:
                return opportunityService.findOpened(loggedAccountEmail, Strings.nullToEmpty(search), processDefinitionIds);
            case CREATED_TODAY:
                return opportunityService.findAllCreatedToday(loggedAccountEmail, Strings.nullToEmpty(search), processDefinitionIds);
            case VIEWED_LASTLY:
                return opportunityService.findAllRecentlyViewed(loggedAccountEmail, Strings.nullToEmpty(search), processDefinitionIds);
            case EDITED_LASTLY:
                return opportunityService.findAllRecentlyEdited(loggedAccountEmail, Strings.nullToEmpty(search), processDefinitionIds);
            case FOR_APPROVING:
                return opportunityService.findAllForApproving(loggedAccountEmail, Strings.nullToEmpty(search), processDefinitionIds);
            case FINISHED_SUCCESS:
                return opportunityService.findAllClosed(loggedAccountEmail, Strings.nullToEmpty(search), processDefinitionIds, Opportunity.FinishResult.SUCCESS);
            case FINISHED_ERROR:
                return opportunityService.findAllClosed(loggedAccountEmail, Strings.nullToEmpty(search), processDefinitionIds, Opportunity.FinishResult.ERROR);
            default:
                return opportunityService.findAll(loggedAccountEmail, Strings.nullToEmpty(search), processDefinitionIds);
        }
    }

    private List<Task> getAllTasksInstances() {
        return taskService.createTaskQuery().list();
    }

    private KanbanOpportunityProcessOverview getKanbanProcessOverview(Map<String, OpportunityKanbanDTO> processOpportunityMap, List<Task> allTasksInstances, ProcessDefinition process) {
        Process mainProcess = getMainProcess(process);
        Map<UserTask, List<KanbanOpportunity>> processWithOpportunity = getAllOpportunityGroupedByTasksForOverview(process, allTasksInstances, processOpportunityMap);
        ProcessColor processColor = getProcessColor(mainProcess);
        return new KanbanOpportunityProcessOverview(process, processWithOpportunity, processColor);
    }

    private Map<UserTask, List<KanbanOpportunity>> getAllOpportunityGroupedByTasksForOverview(ProcessDefinition processDefinition, List<Task> allTasksInstances, Map<String, OpportunityKanbanDTO> processOpportunityMap) {
        Process process = getMainProcess(processDefinition);
        List<UserTask> tasksInProcess = getTasksOfProcess(process);
        Map<UserTask, List<KanbanOpportunity>> tasks = createMapOfTaskAndOpportunity(tasksInProcess);
        for (UserTask userTask : tasksInProcess) {
            for (Task task : allTasksInstances) {
                if (isTaskInstanceOfUserTask(task, userTask, processDefinition)) {
                    OpportunityKanbanDTO opportunityKanbanDTO = processOpportunityMap.get(task.getProcessInstanceId());
                    if (opportunityKanbanDTO != null) {
                        Boolean isNextStepPossible = isNextStepPossible(task);
                        KanbanOpportunity kanbanOpportunity = new KanbanOpportunity(opportunityKanbanDTO, task, null);
                        tasks.get(userTask).add(kanbanOpportunity);
                    }
                }
            }
        }
        return tasks;
    }

    private Map<UserTask, List<KanbanOpportunity>> getAllOpportunityGroupedByTasksForProces(ProcessDefinition processDefinition, List<Task> allTasksInstances, Map<String, OpportunityKanbanDTO> processOpportunityMap, Map<Long, AccountInfoDto> ownersAccountMap) {
        Process process = getMainProcess(processDefinition);
        List<UserTask> tasksInProcess = getTasksOfProcess(process);
        Map<UserTask, List<KanbanOpportunity>> tasks = createMapOfTaskAndOpportunity(tasksInProcess);
        for (UserTask userTask : tasksInProcess) {
            for (Task task : allTasksInstances) {
                if (isTaskInstanceOfUserTask(task, userTask, processDefinition)) {
                    OpportunityKanbanDTO opportunityKanbanDTO = processOpportunityMap.get(task.getProcessInstanceId());
                    if (opportunityKanbanDTO != null) {
                        Boolean isNextStepPossible = isNextStepPossible(task);
                        List<Owner> ownerList = new ArrayList<>();
                        AccountInfoDto unknownAccount = new AccountInfoDto(0L, "?", "?", "?", 0L, "?", null);
                        if (opportunityKanbanDTO.getOwnerOneId() != null)
                            ownerList.add(new Owner(1L, ownersAccountMap.getOrDefault(opportunityKanbanDTO.getOwnerOneId(), unknownAccount), opportunityKanbanDTO.getOwnerOnePercentage()));
                        if (opportunityKanbanDTO.getOwnerTwoId() != null)
                            ownerList.add(new Owner(2L, ownersAccountMap.getOrDefault(opportunityKanbanDTO.getOwnerTwoId(), unknownAccount), opportunityKanbanDTO.getOwnerTwoPercentage()));
                        if (opportunityKanbanDTO.getOwnerThreeId() != null)
                            ownerList.add(new Owner(3L, ownersAccountMap.getOrDefault(opportunityKanbanDTO.getOwnerThreeId(), unknownAccount), opportunityKanbanDTO.getOwnerThreePercentage()));

                        KanbanOpportunity kanbanOpportunity = new KanbanOpportunity(opportunityKanbanDTO, task, ownerList);
                        tasks.get(userTask).add(kanbanOpportunity);
                    }
                }
            }
        }
        return tasks;
    }

    private Map<Long, AccountInfoDto> getOpportunityOwnersAccountMap(List<OpportunityKanbanDTO> opportunityList) {
        Set<Long> ownerIds = new HashSet<>();
        opportunityList.forEach(opportunity -> {
            if (opportunity.getOwnerOneId() != null) ownerIds.add(opportunity.getOwnerOneId());
            if (opportunity.getOwnerTwoId() != null) ownerIds.add(opportunity.getOwnerTwoId());
            if (opportunity.getOwnerThreeId() != null) ownerIds.add(opportunity.getOwnerThreeId());
        });
        return userClient.getAccountsInfoByIds(new ArrayList<>(ownerIds)).stream()
                .collect(Collectors.toMap(AccountInfoDto::getId,
                        account -> account));
    }

    private List<UserTask> getTasksOfProcess(Process process) {
        return process
                .getFlowElements()
                .stream()
                .filter(UserTask.class::isInstance)
                .map(e -> (UserTask) e)
                .collect(Collectors.toList());
    }

    private Process getMainProcess(ProcessDefinition process) {
        return repositoryService
                .getBpmnModel(process.getId())
                .getMainProcess();
    }

    private Task getActiveTask(String processInstanceId) {
        return taskService
                .createTaskQuery()
                .processInstanceId(processInstanceId)
                .singleResult();
    }

    private ProcessColor getProcessColor(Process mainProcess) {
        List<ExtensionAttribute> fontColor = mainProcess.getAttributes().get("fontColor");
        if (fontColor != null && fontColor.size() > 0) {
            String[] split = fontColor.get(0).getValue().split(";");
            if (split.length == 2) {
                return new ProcessColor(split[0], split[1]);
            }
        }
        return new ProcessColor();
    }

    private Map<UserTask, List<KanbanOpportunity>> createMapOfTaskAndOpportunity(List<UserTask> tasksInProcess) {
        Map<UserTask, List<KanbanOpportunity>> tasks = new LinkedHashMap<>();
        for (UserTask userTask : tasksInProcess) {
            tasks.put(userTask, new ArrayList<>());
        }
        return tasks;
    }

    private boolean isTaskInstanceOfUserTask(Task task, UserTask userTask, ProcessDefinition process) {
        return userTask.getId().equals(task.getTaskDefinitionKey()) && task.getProcessDefinitionId().equals(process.getId());
    }

    private Boolean isNextStepPossible(Task task) {
        return !formService
                .getTaskFormData(task.getId())
                .getFormProperties()
                .stream()
                .anyMatch(formProperty -> formProperty.isRequired() && formProperty.getValue() == null);
    }

    private ProcessDefinition getProcessDefinition(String processDefinitionId) {
        return repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionId)
                .singleResult();
    }

}
