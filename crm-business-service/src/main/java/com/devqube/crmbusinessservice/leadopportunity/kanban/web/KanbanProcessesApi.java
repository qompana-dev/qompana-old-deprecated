package com.devqube.crmbusinessservice.leadopportunity.kanban.web;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.leadopportunity.kanban.web.dto.*;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "kanban-processes")
public interface KanbanProcessesApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "get lead for kanban view", nickname = "getLeadForKanban", notes = "Method used to get lead for Kanban view.", response = KanbanLead.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "lead retrieved successfully", response = KanbanLead.class),
            @ApiResponse(code = 404, message = "this lead doesn't exists")})
    @RequestMapping(value = "/kanban-processes/leads/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<KanbanLead> getLeadForKanban(@ApiParam(value = "lead id", required = true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get process for kanban", nickname = "getProcessKanbanDetails", notes = "get specific process with leads to show on kanban", response = KanbanLeadTaskDetails.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "processes overview retrieved successfully", response = KanbanLeadTaskDetails.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "lead not found")})
    @RequestMapping(value = "/kanban-processes/lead/{processDefinitionId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<KanbanLeadTaskDetails>> getLeadProcessKanbanDetails(@ApiParam(value = "process definition id", required = true) @PathVariable("processDefinitionId") String processDefinitionId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get process for kanban", nickname = "getProcessKanbanDetails", notes = "get specific process with leads to show on kanban", response = KanbanLeadTaskDetails.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "processes overview retrieved successfully", response = KanbanLeadTaskDetails.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "lead not found")})
    @RequestMapping(value = "/kanban-processes/opportunity/{processDefinitionId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<KanbanOpportunityTaskDetails>> getOpportunityProcessKanbanDetails(@ApiParam(value = "process definition id", required = true) @PathVariable("processDefinitionId") String processDefinitionId,
                                                                                                  @ApiParam(value = "filter strategy", required = true) @Valid @RequestParam(value = "filterStrategy", required = true) String filterStrategy,
                                                                                                  @ApiParam(value = "search text", required = false) @Valid @RequestParam(value = "search", required = false) String search) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get processes overview", nickname = "getProcessesOverview", notes = "get processes overview to show on kanban", response = KanbanLeadProcessOverview.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "processes retrieved successfully", response = KanbanLeadProcessOverview.class, responseContainer = "List")})
    @RequestMapping(value = "/kanban-processes/lead",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<KanbanLeadProcessOverview>> getProcessesOverviewLead(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                                                                     @Valid @RequestParam(value = "filterStrategy", required = true) String filterStrategy) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get processes overview", nickname = "getProcessesOverview", notes = "get processes overview to show on kanban", response = KanbanLeadProcessOverview.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "processes retrieved successfully", response = KanbanLeadProcessOverview.class, responseContainer = "List")})
    @RequestMapping(value = "/kanban-processes/opportunity",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<KanbanOpportunityProcessOverview>> getProcessesOverviewOpportunity(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                                                                                   @ApiParam(value = "filter strategy", required = true) @Valid @RequestParam(value = "filterStrategy", required = true) String filterStrategy,
                                                                                                   @ApiParam(value = "search text", required = false) @Valid @RequestParam(value = "search", required = false) String search) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}