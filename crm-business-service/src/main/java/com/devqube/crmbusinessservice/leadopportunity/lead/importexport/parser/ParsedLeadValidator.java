package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ValidationResult.*;

@Component
public class ParsedLeadValidator {

    public static void validate(List<ParsedLead> parsedLeadList) {
        validateDuplicateEmails(parsedLeadList);
        for (ParsedLead parsedLead : parsedLeadList) {
            validateEmail(parsedLead.getEmail());
            applyNoValidation(parsedLead.getFirstName(),
                    parsedLead.getLastName(),
                    parsedLead.getPhone(),
                    parsedLead.getPosition(),
                    parsedLead.getCompanyName(),
                    parsedLead.getCompanyCity());
        }
    }

    private static void applyNoValidation(ValidatedValue... fieldsWithoutValidation) {
        for (ValidatedValue fieldWithoutValidation : fieldsWithoutValidation) {
            fieldWithoutValidation.getValidationResults().remove(NOT_VALIDATED);
            fieldWithoutValidation.getValidationResults().add(OK);
        }
    }

    private static void validateEmail(ValidatedValue email) {
        EmailValidator emailValidator = EmailValidator.getInstance();
        if (emailValidator.isValid(email.getValue())) {
            email.getValidationResults().remove(NOT_VALIDATED);
            email.getValidationResults().add(OK);
        } else {
            email.getValidationResults().remove(NOT_VALIDATED);
            email.getValidationResults().add(INCORRECT);
        }


    }

    private static void validateDuplicateEmails(List<ParsedLead> parsedLeadList) {
        Map<String, List<Integer>> duplicateLeadIndexMap = new HashMap<>();
        for (int index = 0; index < parsedLeadList.size(); index++) {
            ParsedLead parsedLead = parsedLeadList.get(index);
            List<Integer> leadIndexList = duplicateLeadIndexMap.get(parsedLead.getEmail().getValue());
            if (leadIndexList != null) {
                leadIndexList.add(index);
            } else {
                leadIndexList = new ArrayList<>();
                leadIndexList.add(index);
                duplicateLeadIndexMap.put(parsedLead.getEmail().getValue(), leadIndexList);
            }
        }

        duplicateLeadIndexMap.forEach((email, indexes) -> {
            if (indexes.size() > 1) {
                for (Integer index : indexes) {
                    parsedLeadList.get(index).getEmail().getValidationResults().remove(NOT_VALIDATED);
                    parsedLeadList.get(index).getEmail().getValidationResults().add(DUPLICATE);
                }
            }
        });
    }

}
