package com.devqube.crmbusinessservice.leadopportunity.primaryProcess;

import com.devqube.crmbusinessservice.leadopportunity.primaryProcess.model.PrimaryProcess;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PrimaryProcessRepository extends JpaRepository<PrimaryProcess, Long> {
    Optional<PrimaryProcess> findByProcessIdAndType(String processId, String type);

    Optional<PrimaryProcess> findByType(String type);
}