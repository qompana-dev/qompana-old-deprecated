package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProcessColor {
    String background;
    String text;
}
