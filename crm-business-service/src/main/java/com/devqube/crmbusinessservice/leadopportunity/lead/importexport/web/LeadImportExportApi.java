package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.web;

import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto.LeadImportDtoIn;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto.ParsedLeadDtoOut;
import io.swagger.annotations.*;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "leadsImportExport")
public interface LeadImportExportApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Import leads", nickname = "importLeads", notes = "Import leads")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Leads successfully imported")})
    @RequestMapping(value = "/lead/import",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Void> importLeads(@RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "lead dto", required = true) @Valid @RequestBody LeadImportDtoIn leadImportDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Parse leads", nickname = "parseImportFile", notes = "Parse leads from file")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Leads successfully parsed"),
            @ApiResponse(code = 400, message = "Wrong file format")})
    @RequestMapping(value = "/lead/parse",
            consumes = {"multipart/form-data"},
            produces = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<List<ParsedLeadDtoOut>> parseImportFile(@ApiParam(value = "file detail") @Valid @RequestPart("file") MultipartFile file) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Export leads", nickname = "exportLeads", notes = "Method used to export leads", response = Void.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Downloaded csv file", response = Resource.class)})
    @RequestMapping(value = "/leads/export",
            produces = {"*/*"},
            method = RequestMethod.GET)
    default ResponseEntity<Void> exportLeads(HttpServletResponse response, @ApiParam(required = false) @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
