package com.devqube.crmbusinessservice.leadopportunity.companyType;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CompanyTypeController implements CompanyTypesApi {
    private final CompanyTypeService companyTypeService;

    public CompanyTypeController(CompanyTypeService companyTypeService) {
        this.companyTypeService = companyTypeService;
    }

    @Override
    public ResponseEntity<List<CompanyType>> getCompanyTypes() {
        return new ResponseEntity<>(companyTypeService.findAll(), HttpStatus.OK);
    }
}
