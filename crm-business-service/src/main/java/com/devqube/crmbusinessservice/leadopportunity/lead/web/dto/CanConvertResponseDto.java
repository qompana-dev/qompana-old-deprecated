package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CanConvertResponseDto {
    private Boolean canConvert;
    private List<String> tasksRequiringAcceptance;
    private List<String> tasksRequiringFillingProperties;
}
