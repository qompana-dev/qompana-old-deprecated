package com.devqube.crmbusinessservice.leadopportunity.lead.importexport;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.dictionary.Word;
import com.devqube.crmbusinessservice.dictionary.WordRepository;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ParsedLead;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineProxyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class LeadImportStartupRunner implements ApplicationRunner {

    private static final String LEAD_IMPORT_ARG = "lead.import";

    private final ResourceLoader resourceLoader;
    private final ImportLeadService leadImportService;
    private final ProcessEngineProxyService processEngineProxyService;
    private final UserClient userClient;
    private final WordRepository wordRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (args.containsOption(LEAD_IMPORT_ARG)) {
            List<String> leadImportValues = args.getOptionValues(LEAD_IMPORT_ARG);
            if (leadImportValues.size() > 0) {
                boolean doImport = Boolean.parseBoolean(leadImportValues.get(0));
                if (doImport) {
                    boolean importAborted = false;
                    List<String> importAbortMessages = new ArrayList<>();
                    importAbortMessages.add("IMPORT ABORTED!");
                    Resource importFile = resourceLoader.getResource("classpath:lead/import/prod_import.xlsx");
                    String fileExtension = FilenameUtils.getExtension(importFile.getFilename());
                    List<ParsedLead> parsedLeadList = leadImportService.parseImportFile(importFile.getInputStream(), fileExtension);
                    String processDefinitionId = null;
                    Long leadKeeperId = null;
                    Long sourceOfAcquisitionId = null;
                    String loggedAccountEmail = "monika.magdziarz@engave.pl";

                    List<ProcessDefinitionDTO> processDefinitionList = processEngineProxyService.getProcessDefinitionsWithoutRoleCheck("LEAD").stream()
                            .filter(processDefinition -> processDefinition.getName().equals("Nawiązanie kontaktu"))
                            .collect(Collectors.toList());
                    if (processDefinitionList.size() > 0) {
                        processDefinitionId = processDefinitionList.get(0).getId();
                    } else {
                        importAborted = true;
                        importAbortMessages.add("Process with name \"Nawiązanie kontaktu\" is not found!");
                    }
                    try {
                        leadKeeperId = userClient.getAccountIdByEmail("monika.magdziarz@engave.pl");
                    } catch (Exception ex) {
                        importAborted = true;
                        importAbortMessages.add("Account with email \"monika.magdziarz@engave.pl\" is not found!");
                    }
                    Word word = wordRepository.findByNameIgnoreCase("BestMicro");
                    if (word != null) {
                        sourceOfAcquisitionId = word.getId();
                    } else {
                        importAborted = true;
                        importAbortMessages.add("Lead source dictionary word \"BestMicro\" is not found!");
                    }

                    if (!importAborted) {
                        CommonLeadParams importParams = new CommonLeadParams()
                                .setProcessDefinitionId(processDefinitionId)
                                .setLeadKeeperId(leadKeeperId)
                                .setSourceOfAcquisitionId(sourceOfAcquisitionId)
                                .setLoggedAccountEmail(loggedAccountEmail);
                        leadImportService.importLeads(parsedLeadList, importParams, false, false);
                        log.info("LEADS SUCCESSFULLY IMPORTED");
                    } else {
                        log.warn(importAbortMessages.toString());
                    }
                }
            }
        }
    }

}