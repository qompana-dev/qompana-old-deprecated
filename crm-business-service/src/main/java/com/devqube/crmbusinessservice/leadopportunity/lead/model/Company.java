package com.devqube.crmbusinessservice.leadopportunity.lead.model;

import com.devqube.crmbusinessservice.leadopportunity.companyType.CompanyType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Company {
    @Id
    @SequenceGenerator(name="company_seq", sequenceName="company_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="company_seq")
    private Long id;
    @ManyToOne(cascade = CascadeType.MERGE)
    private CompanyType companyType;
    private String companyName;
    private String wwwAddress;
    private String streetAndNumber;
    private String city;
    private String postalCode;
    private String province;
    private String country;
    private String notes;
}
