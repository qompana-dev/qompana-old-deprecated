package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.kanban;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.flowable.bpmn.model.UserTask;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class KanbanTaskOverview {

    @AuthField(frontendId = "LeadKanbanComponent.stepNameField", controller = "getProcessesOverview")
    private String name;

    @AuthField(frontendId = "LeadKanbanComponent.numberLeadsInTaskField", controller = "getProcessesOverview")
    private Integer includedLeadsNumber;

    public KanbanTaskOverview(UserTask task, List<KanbanLead> leads) {
        this.name = task.getName();
        this.includedLeadsNumber = leads.size();
    }
}
