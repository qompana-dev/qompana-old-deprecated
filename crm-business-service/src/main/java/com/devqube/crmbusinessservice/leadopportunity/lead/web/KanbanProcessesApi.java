package com.devqube.crmbusinessservice.leadopportunity.lead.web;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.kanban.KanbanLead;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.kanban.KanbanProcessOverview;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.kanban.KanbanTaskDetails;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "kanban-processes")
public interface KanbanProcessesApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "get lead for kanban view", nickname = "getLeadForKanban", notes = "Method used to get lead for Kanban view.", response = KanbanLead.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "lead retrieved successfully", response = KanbanLead.class),
        @ApiResponse(code = 404, message = "this lead doesn't exists") })
    @RequestMapping(value = "/kanban-processes/leads/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    default ResponseEntity<KanbanLead> getLeadForKanban(@ApiParam(value = "lead id", required = true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get process for kanban", nickname = "getProcessKanbanDetails", notes = "get specific process with leads to show on kanban", response = KanbanTaskDetails.class, responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "processes overview retrieved successfully", response = KanbanTaskDetails.class, responseContainer = "List"),
        @ApiResponse(code = 404, message = "lead not found") })
    @RequestMapping(value = "/kanban-processes/{processDefinitionId}",
        produces = { "application/json" },
        method = RequestMethod.GET)
    default ResponseEntity<List<KanbanTaskDetails>> getProcessKanbanDetails(@ApiParam(value = "process definition id", required = true) @PathVariable("processDefinitionId") String processDefinitionId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get processes overview", nickname = "getProcessesOverview", notes = "get processes overview to show on kanban", response = KanbanProcessOverview.class, responseContainer = "List")
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "processes retrieved successfully", response = KanbanProcessOverview.class, responseContainer = "List") })
    @RequestMapping(value = "/kanban-processes",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    default ResponseEntity<List<KanbanProcessOverview>> getProcessesOverview() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
