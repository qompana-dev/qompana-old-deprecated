package com.devqube.crmbusinessservice.leadopportunity.lead;

import com.devqube.crmbusinessservice.dictionary.Word;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.LastViewedLead;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead.CloseResult;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.time.LocalDateTime;
import java.util.List;

import static javax.persistence.criteria.JoinType.LEFT;

public class LeadSpecifications {

    public static Specification<Lead> findAllByCreatedAfterAndDeletedFalse(LocalDateTime date, List<Long> userIds) {
        return findCreatedAfterExclusive(date)
                .and(findByDeleted(false))
                .and(findByClosed(false))
                .and(findByConvertedToOpportunity(false))
                .and(findByLeadKeeperIdIn(userIds)
                        .or(findCreatorIdIn(userIds)));
    }

    public static Specification<Lead> findAllByUpdatedAfterAndDeletedFalse(LocalDateTime date, List<Long> userIds) {
        return findUpdatedAfterExclusive(date)
                .and(findByDeleted(false))
                .and(findByClosed(false))
                .and(findByConvertedToOpportunity(false))
                .and(findByLeadKeeperIdIn(userIds)
                        .or(findCreatorIdIn(userIds)));
    }

    public static Specification<Lead> findAllByLastViewedAfterAndDeletedFalse(LocalDateTime date, Long userId, List<Long> userIds) {
        return findByDeleted(false)
                .and(findByClosed(false))
                .and(findByConvertedToOpportunity(false))
                .and(findByLeadKeeperIdIn(userIds)
                        .or(findCreatorIdIn(userIds)))
                .and(findLastViewedAfterExclusive(date, userId));
    }

    public static Specification<Lead> findAllByAcceptanceUserIdAndDeletedFalse(List<Long> userIds) {
        return findByDeleted(false)
                .and(findByClosed(false))
                .and(findByConvertedToOpportunity(false))
                .and(findByAcceptanceUserIdIn(userIds)
                        .or(findByAcceptanceUserIdIsNotNull()
                                .and(findByLeadKeeperIdIn(userIds)
                                        .or(findCreatorIdIn(userIds)))));
    }

    public static Specification<Lead> findAllClosed(List<Long> userIds, CloseResult closeResult) {
        return findByDeleted(false)
                .and(findByClosed(true))
                .and(findByLeadKeeperIdIn(userIds)
                        .or(findCreatorIdIn(userIds)))
                .and(findByCloseResult(closeResult));
    }

    public static Specification<Lead> findAllConverted(List<Long> userIds) {
        return findByDeleted(false)
                .and(findByLeadKeeperIdIn(userIds)
                        .or(findCreatorIdIn(userIds)))
                .and(findByConvertedToContactIdIsNotNull()
                        .or(findByConvertedToCustomerIdIsNotNull())
                        .or(findByConvertedToOpportunityIdIsNotNull()));
    }

    public static Specification<Lead> findAllByDeletedFalse(List<Long> userIds) {
        return findByDeleted(false)
                .and(findByClosed(false))
                .and(findByConvertedToOpportunity(false))
                .and(findByLeadKeeperIdIn(userIds)
                        .or(findCreatorIdIn(userIds)));
    }

    public static Specification<Lead> findAll(List<Long> userIds) {
        return findByDeleted(false)
                .and(findByLeadKeeperIdIn(userIds)
                        .or(findCreatorIdIn(userIds)));
    }

    public static Specification<Lead> searchByCustomFields(String search) {
        return findByFirstName(search)
                .or(findByLastName(search))
                .or(findByCompanyName(search))
                .or(findByCompanyCity(search))
                .or(findByPhone(search))
                .or(findByEmail(search))
                .or(findBySourceOrSubSourceOfAcquisition(search));
    }

    public static Specification<Lead> findByDeleted(boolean deleted) {
        return (root, query, cb) -> cb.equal(root.get("deleted"), deleted);
    }

    public static Specification<Lead> findByClosed(boolean closed) {
        return (root, query, cb) -> cb.equal(root.get("closed"), closed);
    }

    public static Specification<Lead> findByConvertedToOpportunity(boolean convertedToOpportunity) {
        return (root, query, cb) -> cb.equal(root.get("convertedToOpportunity"), convertedToOpportunity);
    }

    public static Specification<Lead> findByLeadKeeperIdIn(List<Long> userIds) {
        return (root, query, cb) -> root.get("leadKeeperId").in(userIds);
    }

    public static Specification<Lead> findByAcceptanceUserIdIsNotNull() {
        return (root, query, cb) -> root.get("acceptanceUserId").isNotNull();
    }

    public static Specification<Lead> findByAcceptanceUserIdIn(List<Long> userIds) {
        return (root, query, cb) -> root.get("acceptanceUserId").in(userIds);
    }

    public static Specification<Lead> findCreatorIdIn(List<Long> userIds) {
        return (root, query, cb) -> root.get("creator").in(userIds);
    }

    public static Specification<Lead> findCreatedAfterExclusive(LocalDateTime date) {
        return (root, query, cb) -> cb.greaterThan(root.get("created"), date);
    }

    public static Specification<Lead> findCreatedBeforeInclusive(LocalDateTime date) {
        return (root, query, cb) -> cb.lessThanOrEqualTo(root.get("created"), date);
    }

    public static Specification<Lead> findUpdatedAfterExclusive(LocalDateTime date) {
        return (root, query, cb) -> cb.greaterThan(root.get("updated"), date);
    }

    public static Specification<Lead> findLastViewedAfterExclusive(LocalDateTime date, Long userId) {
        return (root, query, cb) -> {
            Join<Lead, LastViewedLead> lastViewedLead = root.joinSet("lastViewedLeads", LEFT);
            return cb.and(
                    cb.equal(lastViewedLead.get("userId"), userId),
                    cb.greaterThan(lastViewedLead.get("lastViewed"), date));
        };
    }

    public static Specification<Lead> findByFirstName(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("firstName")), "%" + toSearch.toLowerCase() + "%");
    }

    public static Specification<Lead> findByLastName(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("lastName")), "%" + toSearch.toLowerCase() + "%");
    }

    public static Specification<Lead> findByCompanyName(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("company").get("companyName")), "%" + toSearch.toLowerCase() + "%");
    }

    public static Specification<Lead> findByCompanyCity(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("company").get("city")), "%" + toSearch.toLowerCase() + "%");
    }

    public static Specification<Lead> findByPhone(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("phone")), "%" + toSearch.toLowerCase() + "%");
    }

    public static Specification<Lead> findByEmail(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("email")), "%" + toSearch.toLowerCase() + "%");
    }

    public static Specification<Lead> findBySourceOfAcquisitionIsNonNull() {
        return (root, query, cb) -> cb.isNotNull(root.get("sourceOfAcquisition"));
    }

    public static Specification<Lead> findBySubSourceOfAcquisitionIsNonNull() {
        return (root, query, cb) -> cb.isNotNull(root.get("subSourceOfAcquisition"));
    }

    public static Specification<Lead> findBySourceOrSubSourceOfAcquisition(String toSearch) {
        return (root, query, cb) -> {
            Subquery<Long> wordSubquery = query.subquery(Long.class);
            Root<Word> wordSubqueryRoot = wordSubquery.from(Word.class);
            wordSubquery.select(wordSubqueryRoot.get("id"))
                    .where(cb.like(cb.lower(wordSubqueryRoot.get("name")), "%" + toSearch.toLowerCase() + "%"));

            return cb.or(
                    cb.in(root.get("sourceOfAcquisition")).value(wordSubquery),
                    cb.in(root.get("subSourceOfAcquisition")).value(wordSubquery)
            );
        };
    }

    public static Specification<Lead> findByCloseResult(CloseResult closeResult) {
        return (root, query, cb) -> cb.equal(root.get("closeResult"), closeResult);
    }

    public static Specification<Lead> findByConvertedToContactIdIsNotNull() {
        return (root, query, cb) -> root.get("convertedToContactId").isNotNull();
    }

    public static Specification<Lead> findByConvertedToCustomerIdIsNotNull() {
        return (root, query, cb) -> root.get("convertedToCustomerId").isNotNull();
    }

    public static Specification<Lead> findByConvertedToOpportunityIdIsNotNull() {
        return (root, query, cb) -> root.get("convertedToOpportunityId").isNotNull();
    }

}