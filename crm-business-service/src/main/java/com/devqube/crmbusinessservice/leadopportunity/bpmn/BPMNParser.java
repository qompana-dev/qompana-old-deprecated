package com.devqube.crmbusinessservice.leadopportunity.bpmn;

import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.PossibleValue;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Process;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Property;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Task;
import com.devqube.crmshared.exception.BadRequestException;
import com.google.common.base.Strings;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

class BPMNParser {
    Process parseBPMNFile(MultipartFile file) throws BadRequestException, IOException {
        return parseBPMNFile(new String(file.getBytes(), StandardCharsets.UTF_8));
    }

    Process parseBPMNFile(String xml) throws BadRequestException {
        Document document = Jsoup.parse(xml);
        return getProcess(document);
    }

    private Process getProcess(Document document) throws BadRequestException {
        Process process = new Process();
        setProcessObjectType(document, process);
        Element processElement = document.selectFirst("process");
        if (processElement == null) {
            throw new BadRequestException("process not found");
        }
        if (!(processElement.hasAttr("id") && processElement.hasAttr("name") && processElement.hasAttr("flowable:fontColor"))) {
            throw new BadRequestException("process id, name or fontColor is null");
        }
        process.setId(Strings.emptyToNull(processElement.attr("id")));
        process.setName(Strings.emptyToNull(processElement.attr("name")));
        setAvailableTypeAndDepartmentId(process, getDocumentationElement(processElement));
        String[] fontColor = processElement.attr("flowable:fontColor").split(";");
        if (fontColor.length != 2) {
            throw new BadRequestException("invalid fontColor");
        }
        process.setBgColor(fontColor[0]);
        process.setTextColor(fontColor[1]);
        List<Task> tasks = getTasks(processElement, process.getObjectType());
        tasks = filterOutLastDefaultTask(tasks);
        process.setTasks(tasks);
        return process;
    }

    private List<Task> filterOutLastDefaultTask(List<Task> tasks) {
        return tasks.stream().filter(task -> task.getName() != null).collect(Collectors.toList());
    }

    private void setAvailableTypeAndDepartmentId(Process process, String departmentId) {
        if (!Strings.isNullOrEmpty(departmentId)) {
            process.setAvailableType("DEPARTMENT");
            process.setDepartment(Strings.emptyToNull(departmentId));
        } else {
            process.setAvailableType("ALL");
        }
    }

    private void setProcessObjectType(Document document, Process process) throws BadRequestException {
        Element definitions = document.selectFirst("definitions");
        if (!definitions.hasAttr("targetnamespace")) {
            throw new BadRequestException("process' object type is null");
        }
        process.setObjectType(Process.ObjectTypeEnum.valueOf(definitions.attr("targetnamespace")));
    }

    private List<Task> getTasks(Element processElement, Process.ObjectTypeEnum objectType) throws BadRequestException {
        List<Element> userTasks = getFirstLvlChildrenByTag(processElement, "usertask");
        if (userTasks == null || userTasks.size() == 0) {
            return new ArrayList<>();
        }
        List<Task> result = new ArrayList<>();
        for (int i = 0; i < userTasks.size(); i++) {
            result.add(getTask(userTasks.get(i), objectType));
        }
        return result;
    }

    private Task getTask(Element taskElement, Process.ObjectTypeEnum objectType) throws BadRequestException {
        if (taskElement.tagName().equals("usertask")) {
            Task task = new Task();

            if (taskElement.hasAttr("flowable:informationTask")) {
                task.setIsInformationTask(Boolean.parseBoolean(taskElement.attr("flowable:informationTask")));
            } else {
                task.setIsInformationTask(false);
            }
            if (!taskElement.hasAttr("name")) {
                throw new BadRequestException("task name is null");
            }
            task.setName(Strings.emptyToNull(taskElement.attr("name")));
            setAcceptanceAndNotification(taskElement, task);
            setSalesOpportunity(taskElement, objectType, task);
            task.setDocumentation(getDocumentationElement(taskElement));
            task.setProperties(getProperties(taskElement));
            return task;
        }
        return null;
    }

    private void setSalesOpportunity(Element taskElement, Process.ObjectTypeEnum objectType, Task task) {
        if (objectType.equals(Process.ObjectTypeEnum.OPPORTUNITY)) {
            String salesOpportunity = Strings.emptyToNull(taskElement.attr("flowable:assignee"));
            task.setSalesOpportunity(salesOpportunity == null ? null : new BigDecimal(salesOpportunity));
        }
    }

    private void setAcceptanceAndNotification(Element taskElement, Task task) {
        String candidateGroups = taskElement.attr("flowable:candidateGroups");
        String candidateUsers = taskElement.attr("flowable:candidateUsers");

        if (Strings.isNullOrEmpty(candidateGroups)) {
            task.setAcceptanceGroup(null);
            task.setNotificationGroup(null);
        } else {
            task.setAcceptanceGroup(Strings.emptyToNull(BPMNUtil.getAcceptanceId(candidateGroups)));
            task.setNotificationGroup(Strings.emptyToNull(BPMNUtil.getNotificationId(candidateGroups)));
        }

        if (Strings.isNullOrEmpty(candidateUsers)) {
            task.setAcceptanceUser(null);
            task.setNotificationUser(null);
        } else {
            task.setAcceptanceUser(Strings.emptyToNull(BPMNUtil.getAcceptanceId(candidateUsers)));
            task.setNotificationUser(Strings.emptyToNull(BPMNUtil.getNotificationId(candidateUsers)));
        }
    }

    private List<Property> getProperties(Element taskElement) throws BadRequestException {
        List<Element> extensionElements = getFirstLvlChildrenByTag(taskElement, "extensionelements");
        if (extensionElements == null || extensionElements.size() == 0) {
            return new ArrayList<>();
        }
        List<Element> formProperties = getFirstLvlChildrenByTag(extensionElements.get(0), "flowable:formproperty");
        if (formProperties == null || formProperties.size() == 0) {
            return new ArrayList<>();
        }
        List<Property> result = new ArrayList<>();
        for (int i = 0; i < formProperties.size(); i++) {
            result.add(getProperty(formProperties.get(i)));
        }
        return result;
    }

    private Property getProperty(Element propertyElement) throws BadRequestException {
        if (propertyElement.tagName().equals("flowable:formproperty")) {
            Property property = new Property();
            if (!(propertyElement.hasAttr("id") && propertyElement.hasAttr("type") && propertyElement.hasAttr("required"))) {
                throw new BadRequestException("property id, type or required is null");
            }
            property.setId(Strings.emptyToNull(propertyElement.attr("id")));
            property.setType(Property.TypeEnum.fromValue(propertyElement.attr("type").toUpperCase()));
            property.setRequired(Boolean.parseBoolean(propertyElement.attr("required")));
            if (property.getType().equals(Property.TypeEnum.ENUM)) {
                List<PossibleValue> possibleValues = getPossibleValues(propertyElement);
                if (possibleValues == null || possibleValues.size() == 0) {
                    throw new BadRequestException("property type enum not contain possible values");
                }
                property.setPossibleValues(possibleValues);
            }
            return property;
        }
        return null;
    }

    private List<PossibleValue> getPossibleValues(Element propertyElement) throws BadRequestException {
        List<Element> possibleValues = getFirstLvlChildrenByTag(propertyElement, "flowable:value");
        if (possibleValues == null || possibleValues.size() == 0) {
            return new ArrayList<>();
        }
        List<PossibleValue> result = new ArrayList<>();
        for (int i = 0; i < possibleValues.size(); i++) {
            result.add(getPossibleValue(possibleValues.get(i)));
        }
        return result;
    }

    private PossibleValue getPossibleValue(Element possibleValueElement) throws BadRequestException {
        if (possibleValueElement.tagName().equals("flowable:value")) {
            PossibleValue possibleValue = new PossibleValue();
            if (!(possibleValueElement.hasAttr("id") && possibleValueElement.hasAttr("name"))) {
                throw new BadRequestException("possible value id or name is null");
            }
            possibleValue.setId(possibleValueElement.attr("id"));
            possibleValue.setName(possibleValueElement.attr("name"));
            return possibleValue;
        }
        return null;
    }

    private String getDocumentationElement(Element element) {
        Optional<Element> documentation = getFirstLvlChildrenByTag(element, "documentation").stream().findFirst();
        if (documentation.isEmpty() || !documentation.get().hasText()) {
            return null;
        }
        return documentation.get().text();
    }

    private List<Element> getFirstLvlChildrenByTag(Element element, String key) {
        if (element == null || element.children() == null || key == null) {
            return new ArrayList<>();
        }
        return element.children().stream()
                .filter(c -> c.tagName().equals(key))
                .collect(Collectors.toList());
    }
}
