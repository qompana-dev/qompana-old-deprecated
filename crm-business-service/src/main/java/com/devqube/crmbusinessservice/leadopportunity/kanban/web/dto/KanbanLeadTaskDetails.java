package com.devqube.crmbusinessservice.leadopportunity.kanban.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.flowable.bpmn.model.UserTask;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class KanbanLeadTaskDetails {

    @AuthField(controller = "getProcessKanbanDetails")
    private String id;
    @AuthField(frontendId = "LeadKanbanComponent.stepNameField", controller = "getProcessKanbanDetails")
    private String name;
    @AuthField(controller = "getProcessKanbanDetails")
    private List<KanbanLead> leads;

    public KanbanLeadTaskDetails(UserTask task, List<KanbanLead> leads) {
        this.id = task.getId();
        this.name = task.getName();
        this.leads = leads;
    }
}
