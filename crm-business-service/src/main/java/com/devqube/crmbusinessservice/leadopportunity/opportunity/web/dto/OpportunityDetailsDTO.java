package com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.OpportunityPriority;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessInstanceDetailsDTO;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@AuthFilterEnabled
public class OpportunityDetailsDTO implements ProcessInstanceDetailsDTO {
    @AuthFields(list = {
            @AuthField(controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private Long id;

    @AuthFields(list = {
            @AuthField(controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private String processInstanceId;

    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.mainInfo", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),
    })

    private String name;

    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.customer", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),
    })
    private Long customerId;

    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.customer", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),
    })
    private String customerName;

    @AuthField(controller = "getOpportunity")
    private Long contactId;

    @AuthField(controller = "getOpportunity")
    private String contactName;

    @AuthField(frontendId = "OpportunityDetailsComponent.createDate", controller = "getOpportunity")
    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.createDate", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),
    })
    private String createDate;


    @AuthField(frontendId = "OpportunityDetailsComponent.createDate", controller = "getOpportunity")
    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.createDate", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),
    })
    private LocalDateTime changeUserTaskDate;

    @AuthField(frontendId = "OpportunityDetailsComponent.finishDate", controller = "getOpportunity")
    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.finishDate", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),
    })
    private String finishDate;

    @AuthField(controller = "getOpportunity")
    private Long probability;

    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.amount", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),
    })
    private Double amount;


    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.amount", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),
    })
    private String currency;

    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.mainInfo", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),
    })
    private String description;

    @AuthFields(list = {
            @AuthField(controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private String created;

    @AuthFields(list = {
            @AuthField(controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private Long creator;

    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.priority", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private OpportunityPriority priority;

    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.owner", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),
    })
    private Long ownerOneId;

    @AuthField(frontendId = "OpportunityDetailsComponent.owner", controller = "getOpportunity")
    private Long ownerOnePercentage;

    @AuthField(frontendId = "OpportunityDetailsComponent.owner", controller = "getOpportunity")
    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.owner", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),
    })
    private Long ownerTwoId;

    @AuthField(frontendId = "OpportunityDetailsComponent.owner", controller = "getOpportunity")
    private Long ownerTwoPercentage;

    @AuthField(frontendId = "OpportunityDetailsComponent.owner", controller = "getOpportunity")
    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.owner", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),
    })
    private Long ownerThreeId;

    @AuthField(frontendId = "OpportunityDetailsComponent.owner", controller = "getOpportunity")
    private Long ownerThreePercentage;

    @AuthFields(list = {
            @AuthField(controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private Long sourceOfAcquisition;

    @AuthFields(list = {
            @AuthField(controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private Long subSourceOfAcquisition;

    @AuthFields(list = {
            @AuthField(controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private Boolean convertedFromLead;

    @AuthFields(list = {
            @AuthField(controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private String bgColor;

    @AuthFields(list = {
            @AuthField(controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private String textColor;

    @AuthFields(list = {
            @AuthField(controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private Long acceptanceUserId;

    @AuthFields(list = {
            @AuthField(frontendId = "OpportunityDetailsComponent.subjectOfInterest", controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),
    })
    private Long subjectOfInterest;

    @AuthFields(list = {
            @AuthField(controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private Opportunity.FinishResult finishResult;

    @AuthFields(list = {
            @AuthField(controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private Opportunity.RejectionReason rejectionReason;

    private String objectType;

    @AuthFields(list = {
            @AuthField(controller = "getOpportunity"),
            @AuthField(controller = "getProcessDefinitionOpportunities"),

    })
    private List<Task> tasks = null;
}

