package com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto;

import lombok.Getter;

import java.util.*;

@Getter
public enum OpportunityFilterStrategy {
    ALL("all"),
    OPENED("opened"),
    CREATED_TODAY("createdToday"),
    VIEWED_LASTLY("viewedLastly"),
    EDITED_LASTLY("editedLastly"),
    FOR_APPROVING("forApproving"),
    CLOSED_CLOSED("closedClosed"),
    CLOSED_ARCHIVED("closedArchived"),
    CONVERTED("converted"),
    FINISHED_SUCCESS("finishedSuccess"),
    FINISHED_ERROR("finishedError");

    private static final Map<String, OpportunityFilterStrategy> valueIndex = new HashMap<>(OpportunityFilterStrategy.values().length);
    private final String value;

    static {
        List.of(OpportunityFilterStrategy.values()).forEach(ofs -> valueIndex.put(ofs.getValue(), ofs));
    }

    OpportunityFilterStrategy(String value) {
        this.value = value;
    }

    public static OpportunityFilterStrategy getByValue(String value) {
        return valueIndex.get(value);
    }
}
