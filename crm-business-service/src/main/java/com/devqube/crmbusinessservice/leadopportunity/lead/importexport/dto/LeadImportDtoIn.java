package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.Valid;
import java.util.List;

@Data
@Accessors(chain = true)
public class LeadImportDtoIn {

    @Valid
    private LeadImportSettingDtoIn settings;

    @Valid
    private List<LeadDtoIn> leads;

}
