package com.devqube.crmbusinessservice.leadopportunity.kanban.web.dto;

import com.devqube.crmshared.user.dto.AccountInfoDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Owner {

    private Long number;
    private Long id;
    private Long percentage;
    private String name;
    private String surname;
    private String avatar;
    private String email;

    public Owner(Long number, AccountInfoDto accountInfo, Long percentage) {
        this.number = number;
        this.id = accountInfo.getId();
        this.percentage = percentage;
        this.name = accountInfo.getName();
        this.surname = accountInfo.getSurname();
        this.avatar = accountInfo.getAvatar();
        this.email = accountInfo.getEmail();
    }

}
