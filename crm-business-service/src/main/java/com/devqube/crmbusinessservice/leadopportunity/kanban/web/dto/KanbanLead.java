package com.devqube.crmbusinessservice.leadopportunity.kanban.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.flowable.task.api.Task;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class KanbanLead {


    @AuthFields(list = {
            @AuthField(controller = "getProcessKanbanDetails"),
            @AuthField(controller = "getLeadForKanban"),
    })
    private Long id;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadKanbanComponent.nameField", controller = "getProcessKanbanDetails"),
            @AuthField(frontendId = "LeadKanbanComponent.nameField", controller = "getLeadForKanban"),
    })
    private String firstName;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadKanbanComponent.nameField", controller = "getProcessKanbanDetails"),
            @AuthField(frontendId = "LeadKanbanComponent.nameField", controller = "getLeadForKanban"),
    })
    private String lastName;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadKanbanComponent.positionField", controller = "getProcessKanbanDetails"),
            @AuthField(frontendId = "LeadKanbanComponent.positionField", controller = "getLeadForKanban"),
    })
    private Long position;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadKanbanComponent.companyField", controller = "getProcessKanbanDetails"),
            @AuthField(frontendId = "LeadKanbanComponent.companyField", controller = "getLeadForKanban"),
    })
    private String companyName;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadKanbanComponent.phoneField", controller = "getProcessKanbanDetails"),
            @AuthField(frontendId = "LeadKanbanComponent.phoneField", controller = "getLeadForKanban"),
    })
    private String phone;

    @AuthFields(list = {
            @AuthField(controller = "getProcessKanbanDetails"),
            @AuthField(controller = "getLeadForKanban"),
    })
    private String processInstanceId;

    @AuthFields(list = {
            @AuthField(controller = "getProcessKanbanDetails"),
            @AuthField(controller = "getLeadForKanban"),
    })
    private String activeTaskId;

    @AuthFields(list = {
            @AuthField(controller = "getProcessKanbanDetails"),
            @AuthField(controller = "getLeadForKanban"),
    })
    private Boolean canBeMovedToFurtherStep;

    @AuthFields(list = {
            @AuthField(controller = "getProcessKanbanDetails"),
            @AuthField(controller = "getLeadForKanban"),
    })
    private Boolean duringAcceptance;

    public KanbanLead(Lead lead, Task task, Boolean canBeMovedToFurtherStep) {
        this.id = lead.getId();
        this.firstName = lead.getFirstName();
        this.lastName = lead.getLastName();
        this.position = lead.getPosition();
        this.companyName = lead.getCompany() != null ? lead.getCompany().getCompanyName() : "";
        this.phone = lead.getPhone();
        this.canBeMovedToFurtherStep = canBeMovedToFurtherStep;
        this.processInstanceId = task.getProcessInstanceId();
        this.activeTaskId = task.getTaskDefinitionKey();
        this.duringAcceptance = lead.getAcceptanceUserId() != null;
    }
}
