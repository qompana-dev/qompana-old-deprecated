package com.devqube.crmbusinessservice.leadopportunity.bpmn.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Property
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-10-23T09:43:25.901763100+02:00[Europe/Belgrade]")

public class Property {
    @JsonProperty("id")
    private String id;

    /**
     * Gets or Sets type
     */
    public enum TypeEnum {
        STRING("STRING"),

        LONG("LONG"),

        DOUBLE("DOUBLE"),

        ENUM("ENUM"),

        DATE("DATE"),

        BOOLEAN("BOOLEAN");

        private String value;

        TypeEnum(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        public String toString() {
            return String.valueOf(value);
        }

        @JsonCreator
        public static TypeEnum fromValue(String text) {
            for (TypeEnum b : TypeEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + text + "'");
        }
    }

    @JsonProperty("type")
    private TypeEnum type;

    @JsonProperty("required")
    private Boolean required;

    @JsonProperty("possibleValues")
    @Valid
    private List<PossibleValue> possibleValues = null;

    @JsonProperty("value")
    private String value;

    public Property id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     */
    @ApiModelProperty(value = "")


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Property type(TypeEnum type) {
        this.type = type;
        return this;
    }

    /**
     * Get type
     *
     * @return type
     */
    @ApiModelProperty(value = "")


    public TypeEnum getType() {
        return type;
    }

    public void setType(TypeEnum type) {
        this.type = type;
    }

    public Property required(Boolean required) {
        this.required = required;
        return this;
    }

    /**
     * Get required
     *
     * @return required
     */
    @ApiModelProperty(value = "")


    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Property possibleValues(List<PossibleValue> possibleValues) {
        this.possibleValues = possibleValues;
        return this;
    }

    public Property addPossibleValuesItem(PossibleValue possibleValuesItem) {
        if (this.possibleValues == null) {
            this.possibleValues = new ArrayList<>();
        }
        this.possibleValues.add(possibleValuesItem);
        return this;
    }

    /**
     * Get possibleValues
     *
     * @return possibleValues
     */
    @ApiModelProperty(value = "")

    @Valid

    public List<PossibleValue> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(List<PossibleValue> possibleValues) {
        this.possibleValues = possibleValues;
    }

    public Property value(String value) {
        this.value = value;
        return this;
    }

    /**
     * Get value
     *
     * @return value
     */
    @ApiModelProperty(value = "")


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Property property = (Property) o;
        return Objects.equals(this.id, property.id) &&
                Objects.equals(this.type, property.type) &&
                Objects.equals(this.required, property.required) &&
                Objects.equals(this.possibleValues, property.possibleValues) &&
                Objects.equals(this.value, property.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, required, possibleValues, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Property {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    type: ").append(toIndentedString(type)).append("\n");
        sb.append("    required: ").append(toIndentedString(required)).append("\n");
        sb.append("    possibleValues: ").append(toIndentedString(possibleValues)).append("\n");
        sb.append("    value: ").append(toIndentedString(value)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

