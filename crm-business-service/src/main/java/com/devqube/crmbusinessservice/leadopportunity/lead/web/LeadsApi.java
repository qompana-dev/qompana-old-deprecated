package com.devqube.crmbusinessservice.leadopportunity.lead.web;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.*;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CustomCrmObject;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "leads")
public interface LeadsApi {
    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }


    @ApiOperation(value = "create lead", nickname = "createLead", notes = "Method used to create lead", response = LeadSaveDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "added lead successfully", response = LeadSaveDTO.class),
            @ApiResponse(code = 400, message = "this process definition doesn't exist"),
            @ApiResponse(code = 404, message = "not found")})
    @RequestMapping(value = "/leads",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<LeadSaveDTO> createLead(@ApiParam(value = "", required = true)
                                                   @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                                   @ApiParam(value = "Created lead object", required = true) @Valid @RequestBody LeadSaveDTO leadSaveDTO) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"processDefinitionId\" : \"processDefinitionId\",  \"country\" : \"country\",  \"lastName\" : \"lastName\",  \"notes\" : \"notes\",  \"companyType\" : 1,  \"city\" : \"city\",  \"companyName\" : \"companyName\",  \"postalCode\" : \"postalCode\",  \"sourceOfAcquisition\" : \"1\",  \"sourceOfAcquisition\" : \"2\",  \"firstName\" : \"firstName\",  \"companyId\" : 6,  \"wwwAddress\" : \"wwwAddress\",  \"streetAndNumber\" : \"streetAndNumber\",  \"province\" : \"province\",  \"phone\" : \"phone\",  \"leadKeeperId\" : 5,  \"salesOpportunity\" : \"NOT_SPECIFIED\",  \"id\" : 0,  \"position\" : \"position\",  \"email\" : \"email\",  \"status\" : \"NOT_ELIGIBLE\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "delete lead", nickname = "deleteLead", notes = "Method used to delete lead")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "deleted lead successfully"),
            @ApiResponse(code = 404, message = "lead not found")})
    @RequestMapping(value = "/leads/{id}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteLead(@ApiParam(value = "lead's id", required = true) @PathVariable("id") Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    @ApiOperation(value = "delete leads", nickname = "deleteLeads", notes = "Method used to delete leads")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "deleted leads successfully"),
            @ApiResponse(code = 404, message = "one of leads not found")})
    @RequestMapping(value = "/leads",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteLeads(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    @ApiOperation(value = "get lead", nickname = "getLead", notes = "get lead by id", response = LeadDetailsDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "lead retrieved successfully", response = LeadDetailsDTO.class),
            @ApiResponse(code = 404, message = "lead not found")})
    @RequestMapping(value = "/leads/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<LeadDetailsDTO> getLead(@ApiParam(value = "lead id", required = true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get lead for edit", nickname = "getLeadForEdit", notes = "Method used to get lead for edit", response = LeadSaveDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "added lead successfully", response = LeadSaveDTO.class),
            @ApiResponse(code = 404, message = "this lead doesn't exists")})
    @RequestMapping(value = "/leads/for-edit/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<LeadSaveDTO> getLeadForEdit(@ApiParam(value = "lead id", required = true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"processDefinitionId\" : \"processDefinitionId\",  \"country\" : \"country\",  \"lastName\" : \"lastName\",  \"notes\" : \"notes\",  \"companyType\" : 1,  \"city\" : \"city\",  \"companyName\" : \"companyName\",  \"postalCode\" : \"postalCode\",  \"sourceOfAcquisition\" : \"1\",  \"subSourceOfAcquisition\" : \"2\",  \"firstName\" : \"firstName\",  \"companyId\" : 6,  \"wwwAddress\" : \"wwwAddress\",  \"streetAndNumber\" : \"streetAndNumber\",  \"province\" : \"province\",  \"phone\" : \"phone\",  \"leadKeeperId\" : 5,  \"salesOpportunity\" : \"NOT_SPECIFIED\",  \"id\" : 0,  \"position\" : \"position\",  \"email\" : \"email\",  \"status\" : \"NOT_ELIGIBLE\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get leads for mobile", nickname = "getLeadsForMobile", notes = "get page of all leads ofr mobile", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "leads page retrieved successfully", response = org.springframework.data.domain.Page.class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/mobile/leads",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Page> getLeadsForMobile(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable, @ApiParam(value = "only todays tasks") @Valid @RequestParam(value = "today", required = false) Boolean today, @ApiParam(value = "only last edited tasks") @Valid @RequestParam(value = "edited", required = false) Boolean edited, @ApiParam(value = "only last viewed tasks") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed, @ApiParam(value = "only leads for approving") @Valid @RequestParam(value = "for-approving", required = false) Boolean forApproving, @ApiParam(value = "only closed leads by reason") @Valid @RequestParam(value = "closeResult", required = false) Lead.CloseResult closeResult, @ApiParam(value = "only converted leads") @Valid @RequestParam(value = "converted", required = false) Boolean converted, @ApiParam(value = "search") @Valid @RequestParam(value = "search", required = false) String search) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "get leads", nickname = "getLeads", notes = "get page of all leads", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "leads page retrieved successfully", response = org.springframework.data.domain.Page.class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/leads",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Page> getLeads(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
                                          @ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable,
                                          @ApiParam(value = "only todays tasks") @Valid @RequestParam(value = "today", required = false) Boolean today,
                                          @ApiParam(value = "only last edited tasks") @Valid @RequestParam(value = "edited", required = false) Boolean edited,
                                          @ApiParam(value = "only last viewed tasks") @Valid @RequestParam(value = "viewed", required = false) Boolean viewed,
                                          @ApiParam(value = "only leads for approving") @Valid @RequestParam(value = "for-approving", required = false) Boolean forApproving,
                                          @ApiParam(value = "only closed leads by reason") @Valid @RequestParam(value = "closeResult", required = false) Lead.CloseResult closeResult,
                                          @ApiParam(value = "only converted leads") @Valid @RequestParam(value = "converted", required = false) Boolean converted,
                                          @ApiParam(value = "only opened") @Valid @RequestParam(value = "opened", required = false) Boolean opened,
                                          @ApiParam(value = "only opened") @Valid @RequestParam(value = "search", required = false) String search) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get list for search", nickname = "getSearchVariants", notes = "Method used to get search variants", response = LeadSearchVariantsDtoOut.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all search variants", response = LeadSearchVariantsDtoOut.class, responseContainer = "List")})
    @RequestMapping(value = "/leads/search-variants",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<LeadSearchVariantsDtoOut>> getSearchVariants(
            @ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
            @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "search") String toSearch) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get converted leads", nickname = "getConvertedLeads", notes = "get page of all leads", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "leads page retrieved successfully", response = org.springframework.data.domain.Page.class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/zapier/convertedLeads",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Page> getAllConvertedLeads(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "", defaultValue = "null") @Valid Pageable pageable) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "put leads", nickname = "modifyLead", notes = "Method used to modify lead", response = LeadSaveDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "modified lead successfully", response = LeadSaveDTO.class),
            @ApiResponse(code = 400, message = "this process definition doesn't exists"),
            @ApiResponse(code = 404, message = "lead not found")})
    @RequestMapping(value = "/leads/{id}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<LeadSaveDTO> modifyLead(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "lead's id", required = true) @PathVariable("id") Long id, @ApiParam(value = "Modified lead object", required = true) @Valid @RequestBody LeadSaveDTO leadSaveDTO) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"processDefinitionId\" : \"processDefinitionId\",  \"country\" : \"country\",  \"lastName\" : \"lastName\",  \"notes\" : \"notes\",  \"companyType\" : 1,  \"city\" : \"city\",  \"companyName\" : \"companyName\",  \"postalCode\" : \"postalCode\",  \"sourceOfAcquisition\" : \"1\",  \"sourceOfAcquisition\" : \"2\",  \"firstName\" : \"firstName\",  \"companyId\" : 6,  \"wwwAddress\" : \"wwwAddress\",  \"streetAndNumber\" : \"streetAndNumber\",  \"province\" : \"province\",  \"phone\" : \"phone\",  \"leadKeeperId\" : 5,  \"salesOpportunity\" : \"NOT_SPECIFIED\",  \"id\" : 0,  \"position\" : \"position\",  \"email\" : \"email\",  \"status\" : \"NOT_ELIGIBLE\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get lead list for search", nickname = "getLeadsForSearch", notes = "Method used to get lead list for search", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all lead names", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/leads/search",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CrmObject>> getLeadsForSearch(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get leads list by ids", nickname = "getLeadsAsCrmObject", notes = "Method used to get leads as crm objects by ids", response = CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found leads", response = CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/leads/crm-object",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CrmObject>> getLeadsAsCrmObject(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get leads list by ids", nickname = "getLeadsAsCustomCrmObject", notes = "Method used to get leads as crm objects by ids", response = CustomCrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found leads", response = CustomCrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/leads/custom-crm-object",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CustomCrmObject>> getLeadsAsCustomCrmObject(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get lead list for main search", nickname = "getLeadsForMainSearch", notes = "get all lead names by text", response = CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all lead names", response = CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/leads/main-search",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CrmObject>> getLeadsForMainSearch(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get all leads with access for user", nickname = "getAllLeadWithAccess", notes = "method used to get list of leads for specific user", response = Long.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of leads for specific user", response = Long.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Email not correct")})
    @RequestMapping(value = "/access/leads",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<Long>> getAllLeadWithAccess(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "0");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "convert lead to opportunity", nickname = "convertToOpportunity", notes = "Method used to convert lead to opportunity", response = com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ConvertedOpportunityDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "converted to opportunity successfully", response = com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ConvertedOpportunityDto.class),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 403, message = "not permission to convert lead"),
            @ApiResponse(code = 404, message = "lead not found")})
    @RequestMapping(value = "/leads/{id}/convert-to-opportunity",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ConvertedOpportunityDto> convertToOpportunity(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "", required = true) @PathVariable("id") Long id, @ApiParam(value = "", required = true) @Valid @RequestBody com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadConversionDto comDevqubeCrmbusinessserviceLeadopportunityLeadWebDtoLeadConversionDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "search similar object", nickname = "searchSimilarObjects", response = CrmObject[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "similar objects", response = CrmObject[].class)})
    @RequestMapping(value = "/leads/search-similar-objects",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<List<CrmObject>> searchSimilarObjects(@ApiParam(value = "", required = true) @Valid @RequestBody SearchSimilarObjectsDto searchSimilarObjectsDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "check if the lead can be converted", nickname = "canConvertToOpportunity", response = com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.CanConvertResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "converted to opportunity successfully", response = com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.CanConvertResponseDto.class),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 403, message = "not permission to convert lead"),
            @ApiResponse(code = 404, message = "lead not found")})
    @RequestMapping(value = "/leads/{id}/can-convert",
            method = RequestMethod.GET)
    default ResponseEntity<CanConvertResponseDto> canConvertToOpportunity(@ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail, @ApiParam(value = "", required = true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "close lead", nickname = "closeLead", notes = "Method used to close lead")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "closed lead successfully"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 403, message = "no permission to close lead"),
            @ApiResponse(code = 404, message = "lead not found")})
    @RequestMapping(value = "/leads/{id}/close",
            method = RequestMethod.GET)
    default ResponseEntity<Void> closeLead(@ApiParam(required = true) @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @ApiParam(required = true) @PathVariable("id") Long id, @ApiParam(value = "Lead close result", required = true) @RequestParam("closeResult") Lead.CloseResult closeResult, @ApiParam(value = "Lead close reason on fail") @RequestParam(value = "closeReason", required = false) Lead.RejectionReason rejectionReason)
            throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "find lead by opportunity id", nickname = "findLeadByOpportunityId", notes = "find lead by opportunity id", response = LeadDetailsDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "lead retrieved successfully", response = LeadDetailsDTO.class),
            @ApiResponse(code = 404, message = "lead not found")})
    @RequestMapping(value = "/leads/opportunity/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<LeadDetailsDTO> findLeadByOpportunityId(@ApiParam(value = "opportunity id", required = true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "change leads keeper", nickname = "changeLeadsKeeper", notes = "Method used to change keeper for leads")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "list of leads update"),
            @ApiResponse(code = 400, message = "bad request"),
            @ApiResponse(code = 403, message = "no permission to edit lead"),
            @ApiResponse(code = 404, message = "keeper or one of leads not found")})
    @RequestMapping(value = "/leads/keeper",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> changeLeadsKeeper(@ApiParam(required = true) @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail,
                                                   @ApiParam(value = "New keeper for leads", required = true) @RequestBody @Valid LeadsKeeperDTO leadsKeeper) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
