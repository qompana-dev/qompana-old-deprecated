package com.devqube.crmbusinessservice.leadopportunity.processEngine.service.write;

import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineReadService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.flowable.engine.FormService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.Execution;
import org.flowable.task.api.Task;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProcessEngineWriteService {

    private final RepositoryService repositoryService;
    private final RuntimeService runtimeService;
    private final TaskService taskService;
    private final FormService formService;
    private final LeadRepository leadRepository;
    private final OpportunityRepository opportunityRepository;
    private final ProcessEngineReadService processEngineReadService;

    public ProcessEngineWriteService(RepositoryService repositoryService, RuntimeService runtimeService,
                                     TaskService taskService, FormService formService, LeadRepository leadRepository,
                                     OpportunityRepository opportunityRepository, ProcessEngineReadService processEngineReadService) {
        this.repositoryService = repositoryService;
        this.runtimeService = runtimeService;
        this.taskService = taskService;
        this.formService = formService;
        this.leadRepository = leadRepository;
        this.opportunityRepository = opportunityRepository;
        this.processEngineReadService = processEngineReadService;
    }

    public ProcessDefinitionDTO deployProcessDefinition(InputStream inputStream) throws IOException {
        File file = createFileFromInputStream(inputStream);
        Deployment deploy = deployProcessDefinition(file);
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();
        if (!file.delete()) {
            log.warn("File {} was not deleted", file.getName());
        }
        return new ProcessDefinitionDTO(processDefinition.getId(), processDefinition.getName(), processDefinition.getVersion(), processEngineReadService.getFirstTaskOfProcessDefinition(processDefinition));
    }

    public void postFormProperties(String processInstanceId, Map<String, Object> requestBody) {
        Task task = getTaskByProcessInstanceId(processInstanceId);
        Map<String, String> params = new HashMap<>();
        for (Map.Entry<String, Object> entry : requestBody.entrySet()) {
            params.put(entry.getKey(), entry.getValue() != null ? entry.getValue().toString() : null);
        }
        formService.saveFormData(task.getId(), params);
    }

    public void deleteProcessDefinition(String id) throws BadRequestException, EntityNotFoundException {
        ProcessDefinition newestVersionOfProcess = Optional.ofNullable(repositoryService.createProcessDefinitionQuery()
                .processDefinitionId(id).singleResult()).orElseThrow(EntityNotFoundException::new);
        List<ProcessDefinition> allVersionOfProcesses = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(newestVersionOfProcess.getKey()).list();
        checkIfCanDeleteProcessAndThrowIfNot(allVersionOfProcesses);
        for (ProcessDefinition process: allVersionOfProcesses) {
            repositoryService.deleteDeployment(process.getDeploymentId(), true);
        }
    }

    private void checkIfCanDeleteProcessAndThrowIfNot(List<ProcessDefinition> allVersionOfProcesses) throws BadRequestException {
        for (ProcessDefinition process : allVersionOfProcesses) {
            List<String> processInstanceIds = getProcessInstancesByDeploymentId(process.getDeploymentId());
            if (leadRepository.countAllByDeletedFalseAndClosedFalseAndProcessInstanceIdIn(processInstanceIds) != 0) {
                throw new BadRequestException("There are leads created from this process definition");
            }
            if (opportunityRepository.countAllByProcessInstanceIdIn(processInstanceIds) != 0) {
                throw new BadRequestException("There are opportunities created from this process definition");
            }
        }
    }

    private List<String> getProcessInstancesByDeploymentId(String deploymentId) {
        return runtimeService
                    .createProcessInstanceQuery()
                    .deploymentId(deploymentId)
                    .list()
                    .stream()
                    .map(Execution::getProcessInstanceId)
                    .collect(Collectors.toList());
    }

    private File createFileFromInputStream(InputStream inputStream) throws IOException {
        File file = new File(String.valueOf(UUID.randomUUID()));
        if (!file.createNewFile()) {
            throw new IOException();
        }
        try (inputStream; OutputStream outputStream = new FileOutputStream(file)) {
            IOUtils.copy(inputStream, outputStream);
        }
        return file;
    }

    private Deployment deployProcessDefinition(File file) throws IOException {
        try (InputStream fileInputStream = new FileInputStream(file)){
            return repositoryService
                    .createDeployment()
                    .name("process.bpmn20.xml")
                    .addInputStream("process.bpmn20.xml", fileInputStream)
                    .deploy();
        }
    }

    private Task getTaskByProcessInstanceId(String processInstanceId) {
        return taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult();
    }

    private String parseTaskNumberFromName(Task task) {
        return task.getTaskDefinitionKey().replaceAll("task", "");
    }
}
