package com.devqube.crmbusinessservice.leadopportunity.bpmn;

import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Process;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

@RestController
@Slf4j
public class BPMNController implements BpmnApi {
    private final BPMNParser bpmnParser = new BPMNParser();

    @Override
    @AuthController(actionFrontendId = "BpmnComponent.add")
    public ResponseEntity<Resource> generateBPMNFile(@Valid Process process) {
        try {
            return new ResponseEntity<>(new BPMNGenerator().generateBPMNFile(process), HttpStatus.OK);
        } catch (BadRequestException | ParserConfigurationException | TransformerException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(actionFrontendId = "BpmnComponent.edit")
    public ResponseEntity<Process> parseBPMNFile(@Valid MultipartFile file) {
        try {
            return new ResponseEntity<>(bpmnParser.parseBPMNFile(file), HttpStatus.OK);
        } catch (BadRequestException | IOException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
