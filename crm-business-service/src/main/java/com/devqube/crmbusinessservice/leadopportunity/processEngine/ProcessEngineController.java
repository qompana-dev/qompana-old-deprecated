package com.devqube.crmbusinessservice.leadopportunity.processEngine;

import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadDetailsDTO;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.OpportunityDetailsDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.NotificationActionDto;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionListDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineProxyService;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.access.annotation.AuthControllerCase;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.IncorrectNotificationTypeException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import lombok.extern.slf4j.Slf4j;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
public class ProcessEngineController implements ProcessEngineApi {

    private final ProcessEngineProxyService processEngineProxyService;

    public ProcessEngineController(ProcessEngineProxyService processEngineProxyService) {
        this.processEngineProxyService = processEngineProxyService;
    }

    @Override
    public ResponseEntity<Void> postFormProperties(String processInstanceId, @Valid Map<String, Object> requestBody) {
        try {
            this.processEngineProxyService.postFormProperties(processInstanceId, requestBody);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(actionFrontendId = "accessDenied", controllerCase = {
            @AuthControllerCase(type = "lead", actionFrontendId = "LeadDetailsComponent.completeTask"),
            @AuthControllerCase(type = "opportunity", actionFrontendId = "OpportunityDetailsComponent.completeTask")
    })
    public ResponseEntity<Void> completeTask(String processInstanceId) {
        try {
            boolean completed = this.processEngineProxyService.completeTask(processInstanceId);
            if (completed) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.PARTIAL_CONTENT);
            }
        } catch (FlowableObjectNotFoundException | EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (KafkaSendMessageException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IncorrectNotificationTypeException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @AuthController(actionFrontendId = "accessDenied", controllerCase = {
            @AuthControllerCase(type = "lead", actionFrontendId = "LeadDetailsComponent.goBack"),
            @AuthControllerCase(type = "opportunity", actionFrontendId = "OpportunityDetailsComponent.goBack")
    })
    public ResponseEntity<Void> goBackToPreviousTask(String processInstanceId, Integer numberOfSteps) {
        try {
            this.processEngineProxyService.goBackToPreviousTask(processInstanceId, numberOfSteps);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (FlowableObjectNotFoundException | EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @AuthController(actionFrontendId = "accessDenied", controllerCase = {
            @AuthControllerCase(type = "lead", actionFrontendId = "LeadDetailsComponent.previewTask"),
            @AuthControllerCase(type = "opportunity", actionFrontendId = "OpportunityDetailsComponent.previewTask")
    })
    public ResponseEntity<LeadDetailsDTO.Task> getTaskReadOnlyView(String processInstanceId, String taskDefinitionKey, @Valid Boolean futureTask) {
        try {
            LeadDetailsDTO.Task task;
            if (futureTask == null || !futureTask) {
                task = processEngineProxyService.getPreviousTask(processInstanceId, taskDefinitionKey);
            } else {
                task = processEngineProxyService.getFutureTask(processInstanceId, taskDefinitionKey);
            }
            return new ResponseEntity<>(task, HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Void> acceptTask(@Valid NotificationActionDto notificationActionDto) {
        try {
            processEngineProxyService.acceptTask(notificationActionDto);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (KafkaSendMessageException | IncorrectNotificationTypeException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<Void> declineTask(@Valid NotificationActionDto notificationActionDto) {
        try {
            processEngineProxyService.declineTask(notificationActionDto);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (KafkaSendMessageException | IncorrectNotificationTypeException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<ProcessDefinitionDTO>> getProcessDefinitions(String objectType) {
        return new ResponseEntity<>(processEngineProxyService.getProcessDefinitions(objectType), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ProcessDefinitionDTO> getProcessDefinition(String processDefinitionId) {
        try{
            return new ResponseEntity<>(processEngineProxyService.getProcessDefinition(processDefinitionId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<List<OpportunityDetailsDTO>> getProcessDefinitionOpportunities(@Valid Pageable pageable, String loggedAccountEmail,
                                                                                         String processDefinitionId, @Valid String taskDefinitionKey,
                                                                                         @Valid Boolean includeOldProcessVersionResult) {
        try {
            return new ResponseEntity<>(
                    processEngineProxyService.getProcessDefinitionOpportunities(pageable, loggedAccountEmail,
                            processDefinitionId, taskDefinitionKey, includeOldProcessVersionResult),
                    HttpStatus.OK);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(readFrontendId = "BpmnComponent.list")
    public ResponseEntity<List<ProcessDefinitionListDTO>> getProcessDefinitionsListDTO(String type) {
        return new ResponseEntity<>(processEngineProxyService.getProcessDefinitionsList(type), HttpStatus.OK);
    }

    @Override
    @AuthController(actionFrontendId = "BpmnComponent.delete")
    public ResponseEntity<Void> deleteProcessDefinition(String id) {
        try {
            this.processEngineProxyService.deleteProcessDefinition(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<ProcessDefinitionDTO> deployProcessDefinition(@Valid MultipartFile file) {
        try {
            return new ResponseEntity<>(processEngineProxyService.deployProcessDefinition(file.getInputStream()),
                    HttpStatus.CREATED);
        } catch (IOException | BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<Resource> getBpmnFile(String processDefinitionId) {
        try {
            return new ResponseEntity<>(processEngineProxyService.getBpmnFile(processDefinitionId), HttpStatus.OK);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
