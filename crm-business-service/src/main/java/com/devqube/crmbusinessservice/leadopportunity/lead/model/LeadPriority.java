package com.devqube.crmbusinessservice.leadopportunity.lead.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum LeadPriority {
    LOW("LOW"),

    MEDIUM("MEDIUM"),

    HIGH("HIGH");

    private String value;

    LeadPriority(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static LeadPriority fromValue(String text) {
        for (LeadPriority b : LeadPriority.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
}
