package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Component
public class CsvParseStrategy implements ParseStrategy {

    @Override
    public List<ParsedLead> parseFile(InputStream file) {
        List<ParsedLead> parsedLeadList = new ArrayList<>();

        try {
            CSVParser csvParser = new CSVParser(new InputStreamReader(file), CSVFormat.DEFAULT
                    .withHeader("firstName", "lastName", "email", "phone", "position", "companyName", "companyCity")
                    .withIgnoreHeaderCase()
                    .withTrim());

            for (CSVRecord csvRecord : csvParser) {
                ParsedLead parsedLead = new ParsedLead()
                        .setFirstName(new ValidatedValue().setValue(csvRecord.get("firstName")))
                        .setLastName(new ValidatedValue().setValue(csvRecord.get("lastName")))
                        .setEmail(new ValidatedValue().setValue(csvRecord.get("email")))
                        .setPhone(new ValidatedValue().setValue(csvRecord.get("phone")))
                        .setPosition(new ValidatedValue().setValue(csvRecord.get("position")))
                        .setCompanyName(new ValidatedValue().setValue(csvRecord.get("companyName")))
                        .setCompanyCity(new ValidatedValue().setValue(csvRecord.get("companyCity")));

                parsedLeadList.add(parsedLead);
            }
        } catch (IOException e) {
            //todo add exception
            e.printStackTrace();
        }
        return parsedLeadList;
    }

    @Override
    public ParseStrategyType getStrategyType() {
        return ParseStrategyType.CSV;
    }

}
