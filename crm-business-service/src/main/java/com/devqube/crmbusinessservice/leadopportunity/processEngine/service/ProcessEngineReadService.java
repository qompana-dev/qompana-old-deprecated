package com.devqube.crmbusinessservice.leadopportunity.processEngine.service;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.PossibleValue;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Property;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadDetailsDTO;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ProcessColor;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionListDTO;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.flowable.bpmn.model.ExtensionAttribute;
import org.flowable.bpmn.model.FormProperty;
import org.flowable.bpmn.model.Process;
import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.flowable.task.api.history.HistoricTaskInstanceQuery;
import org.flowable.variable.api.history.HistoricVariableInstance;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProcessEngineReadService {

    private final RepositoryService repositoryService;
    private final HistoryService historyService;
    private final RuntimeService runtimeService;
    private final UserClient userClient;
    private final TaskService taskService;

    public ProcessEngineReadService(RepositoryService repositoryService, HistoryService historyService,
                                    RuntimeService runtimeService, UserClient userClient, TaskService taskService) {
        this.repositoryService = repositoryService;
        this.historyService = historyService;
        this.runtimeService = runtimeService;
        this.userClient = userClient;
        this.taskService = taskService;
    }

    public Map<String, String> getAllActiveTask(String processDefinitionId) { // <processInstanceId, activeTaskName>
        Map<String, String> result = new HashMap<>();
        taskService.createTaskQuery()
                .processInstanceIdIn(runtimeService.createProcessInstanceQuery()
                        .processDefinitionId(processDefinitionId)
                        .list().stream().map(Execution::getProcessInstanceId)
                        .collect(Collectors.toList()))
                .list().forEach(c -> result.put(c.getProcessInstanceId(), c.getName()));
        return result;
    }

    public List<ProcessDefinition> getProcessDefinitions(String objectType) {
        List<Long> roleIds = this.userClient.getRoleIdWithChildrenIdsByAssignedAccountEmail(CurrentRequestUtil.getLoggedInAccountEmail());
        List<ProcessDefinition> processDefinitions = findAllProcessDefinitionsInAllVersions(objectType, null);
        for (Long roleId : roleIds) {
            processDefinitions.addAll(findAllProcessDefinitionsInAllVersions(objectType, "" + roleId));
        }
        return processDefinitions;
    }
    public ProcessDefinitionDTO getProcessDefinitionById(String processDefinitionId) throws EntityNotFoundException{
        Optional<ProcessDefinition> processDefinition = Optional.ofNullable(findProcessDefinition(processDefinitionId));
        if (processDefinition.isPresent()) {
            ProcessDefinition processDefinition1 = processDefinition.get();
            return new ProcessDefinitionDTO(processDefinition1.getId(), processDefinition1.getName(),
                    processDefinition1.getVersion(), getFirstTaskOfProcessDefinition(processDefinition1));
        }else {
            throw new EntityNotFoundException();
        }
    }

    com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Process.ObjectTypeEnum getObjectTypeByProcessInstanceId(String id) {
        String processDefinitionId = runtimeService.createProcessInstanceQuery().processInstanceId(id).singleResult().getProcessDefinitionId();
        String type = repositoryService.createProcessDefinitionQuery().processDefinitionId(processDefinitionId).singleResult().getCategory();
        return com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Process.ObjectTypeEnum.fromValue(type);
    }

    Resource getBpmnFile(String processDefinitionId) throws IOException {
        String deploymentId = repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionId)
                .singleResult()
                .getDeploymentId();
        return new ByteArrayResource(
                repositoryService.getResourceAsStream(deploymentId, "process.bpmn20.xml").readAllBytes());
    }

    List<ProcessDefinitionDTO> getProcessDefinitionsDTO(String objectType) {
        List<Long> roleIds = this.userClient.getRoleIdWithChildrenIdsByAssignedAccountEmail(CurrentRequestUtil.getLoggedInAccountEmail());
        List<ProcessDefinitionDTO> listOfProcesses = findAllProcessDefinitionsDTO(objectType, null);
        for (Long roleId : roleIds) {
            listOfProcesses.addAll(findAllProcessDefinitionsDTO(objectType, "" + roleId));
        }
        return listOfProcesses;
    }

    List<ProcessDefinitionDTO> getProcessDefinitionsDTOWithoutRoleCheck(String objectType) {
        List<ProcessDefinitionDTO> listOfProcesses = findAllProcessDefinitionsDTO(objectType, null);
        listOfProcesses.addAll(findAllProcessDefinitionsDTOWithoutRoleCheck(objectType));
        return listOfProcesses;
    }

    List<ProcessDefinitionListDTO> getProcessDefinitionsList(String type) {
        List<Long> roleIds = this.userClient.getRoleIdWithChildrenIdsByAssignedAccountEmail(CurrentRequestUtil.getLoggedInAccountEmail());
        List<ProcessDefinition> listOfProcesses = findAllProcessDefinitions(type, null);
        for (Long roleId : roleIds) {
            listOfProcesses.addAll(findAllProcessDefinitions(type, "" + roleId));
        }
        return listOfProcesses.stream().map(this::mapToProcessDefinitionListDTO).collect(Collectors.toList());
    }

    LeadDetailsDTO.Task getPreviousTask(String processInstanceId, String taskDefinitionKey) throws EntityNotFoundException {
        Process process = getProcessDefinition(processInstanceId);
        UserTask userTask = getUserTask(process, taskDefinitionKey).orElseThrow(EntityNotFoundException::new);
        List<Property> properties = getFormProperties(processInstanceId, taskDefinitionKey);
        Boolean informationTask = getInformationTaskValueFromAttributes(userTask);
        Long daysToComplete = getDaysToCompleteFromAttributes(userTask);
        return new LeadDetailsDTO.Task(userTask.getId(), userTask.getName(), userTask.getDocumentation(),
                false, true, null,null ,null, null, properties, informationTask,
                null, daysToComplete, LocalDateTime.now());
    }

    LeadDetailsDTO.Task getFutureTask(String processInstanceId, String taskDefinitionKey) throws EntityNotFoundException {
        Process process = getProcessDefinition(processInstanceId);
        return getTaskDefinition(process, taskDefinitionKey).orElseThrow(EntityNotFoundException::new);
    }

    private ProcessDefinitionListDTO mapToProcessDefinitionListDTO(ProcessDefinition process) {
        Process mainProcess = getMainProcess(process);
        ProcessColor processColor = getProcessColor(mainProcess);
        return new ProcessDefinitionListDTO(process, getTasksOfProcess(mainProcess), processColor);
    }

    private Process getMainProcess(ProcessDefinition process) {
        return repositoryService
                .getBpmnModel(process.getId())
                .getMainProcess();
    }

    public ProcessColor getProcessColor(Process mainProcess) {
        List<ExtensionAttribute> fontColor = mainProcess.getAttributes().get("fontColor");
        if (fontColor != null && fontColor.size() > 0) {
            String[] split = fontColor.get(0).getValue().split(";");
            if (split.length == 2) {
                return new ProcessColor(split[0], split[1]);
            }
        }
        return new ProcessColor();
    }

    private List<UserTask> getTasksOfProcess(Process process) {
        return process
                .getFlowElements()
                .stream()
                .filter(UserTask.class::isInstance)
                .map(e -> (UserTask) e)
                .collect(Collectors.toList());
    }

    private List<ProcessDefinitionDTO> findAllProcessDefinitionsDTO(String objectType, String tenantId) {
        return findAllProcessDefinitions(objectType, tenantId)
                .stream()
                .map(e -> new ProcessDefinitionDTO(e.getId(), e.getName(), e.getVersion(), getFirstTaskOfProcessDefinition(e)))
                .collect(Collectors.toList());
    }

    private List<ProcessDefinitionDTO> findAllProcessDefinitionsDTOWithoutRoleCheck(String objectType) {
        return findAllProcessDefinitions(objectType)
                .stream()
                .map(e -> new ProcessDefinitionDTO(e.getId(), e.getName(), e.getVersion(), getFirstTaskOfProcessDefinition(e)))
                .collect(Collectors.toList());
    }

    public String getFirstTaskOfProcessDefinition(ProcessDefinition processDefinition) {
        Optional<UserTask> first = getTasksOfProcess(getMainProcess(processDefinition)).stream().findFirst();
        return (first.isEmpty()) ? null : first.get().getName();
    }

    private List<ProcessDefinition> findAllProcessDefinitions(String objectType, String roleId) {
        return this.repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionCategory(objectType)
                .latestVersion()
                .orderByProcessDefinitionName()
                .asc()
                .list()
                .stream()
                .filter(e -> Strings.nullToEmpty(e.getDescription()).equals(Strings.nullToEmpty(roleId)))
                .collect(Collectors.toList());
    }

    private List<ProcessDefinition> findAllProcessDefinitions(String objectType) {
        return new ArrayList<>(this.repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionCategory(objectType)
                .latestVersion()
                .orderByProcessDefinitionName()
                .asc()
                .list());
    }

    private ProcessDefinition findProcessDefinition(String processDefinitionId){
       return this.repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionId)
                .singleResult();
    }

    private List<ProcessDefinition> findAllProcessDefinitionsInAllVersions(String objectType, String roleId) {
        return this.repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionCategory(objectType)
                .orderByProcessDefinitionName()
                .asc()
                .list()
                .stream()
                .filter(e -> Strings.nullToEmpty(e.getDescription()).equals(Strings.nullToEmpty(roleId)))
                .collect(Collectors.toList());
    }

    private HistoricTaskInstance getHistoricTaskInstance(String processInstanceId, String taskDefinitionKey) throws EntityNotFoundException {
        HistoricTaskInstanceQuery historicTaskInstanceQuery = createHistoricTaskInstanceQuery(processInstanceId, taskDefinitionKey);
        if (historicTaskInstanceQuery.count() == 0) {
            throw new EntityNotFoundException("Historic task instance not found");
        }
        return historicTaskInstanceQuery.list().get(0);
    }

    private HistoricTaskInstanceQuery createHistoricTaskInstanceQuery(String processInstanceId, String taskDefinitionKey) {
        return historyService.createHistoricTaskInstanceQuery()
                .processInstanceId(processInstanceId)
                .taskDefinitionKey(taskDefinitionKey)
                .finished()
                .orderByHistoricTaskInstanceEndTime()
                .desc();
    }

    private List<Property> getFormProperties(String processInstanceId, String taskDefinitionKey) {
        Map<String, FormProperty> formPropertiesDefinition = getFromPropertiesDefinition(processInstanceId, taskDefinitionKey)
                .stream()
                .collect(Collectors.toMap(FormProperty::getId, x -> x));
        List<HistoricVariableInstance> propertyList = historyService
                .createHistoricVariableInstanceQuery()
                .processInstanceId(processInstanceId)
                .list();
        return propertyList
                .stream()
                .filter(e -> formPropertiesDefinition.containsKey(e.getVariableName()))
                .map(e -> mapToProperty(e, formPropertiesDefinition.get(e.getVariableName())))
                .collect(Collectors.toList());
    }

    private Property mapToProperty(HistoricVariableInstance variable, FormProperty formProperty) {
        Property property = new Property();
        property.setId(variable.getVariableName());
        property.setValue(variable.getValue() != null ? variable.getValue().toString() : null);
        setPropertyType(formProperty, property);
        return property;
    }

    private Property mapToProperty(FormProperty formProperty) {
        Property property = new Property();
        property.setId(formProperty.getId());
        setPropertyType(formProperty, property);
        return property;
    }

    private void setPropertyType(FormProperty formProperty, Property property) {
        property.setType(Property.TypeEnum.valueOf(formProperty.getType().toUpperCase()));
        if (formProperty.getType().equals("enum")) {
            property.setPossibleValues(getPossibleValuesFromPropertyDefinition(formProperty));
        }
    }

    private List<FormProperty> getFromPropertiesDefinition(String processInstanceId, String taskDefinitionKey) {
        ProcessInstance processInstance = runtimeService
                .createProcessInstanceQuery()
                .processInstanceId(processInstanceId)
                .singleResult();
        return ((UserTask) repositoryService
                .getBpmnModel(processInstance.getProcessDefinitionId())
                .getMainProcess()
                .getFlowElement(taskDefinitionKey))
                .getFormProperties();
    }

    private List<PossibleValue> getPossibleValuesFromPropertyDefinition(FormProperty formProperty) {
        return formProperty
                .getFormValues()
                .stream().map(e -> new PossibleValue().id(e.getId()).name(e.getName()))
                .collect(Collectors.toList());
    }

    private Optional<LeadDetailsDTO.Task> getTaskDefinition(Process process, String taskDefinitionKey) {
        return process.getFlowElements()
                .stream()
                .filter(UserTask.class::isInstance)
                .filter(e -> e.getId().equals(taskDefinitionKey))
                .map(e -> (UserTask) e)
                .map(this::mapToTask)
                .findFirst();
    }

    private Optional<UserTask> getUserTask(Process process, String taskDefinitionKey) {
        return process.getFlowElements()
                .stream()
                .filter(UserTask.class::isInstance)
                .filter(e -> e.getId().equals(taskDefinitionKey))
                .map(e -> (UserTask) e)
                .findFirst();

    }

    private Process getProcessDefinition(String processInstanceId) {
        ProcessInstance processInstance = runtimeService
                .createProcessInstanceQuery()
                .processInstanceId(processInstanceId)
                .singleResult();
        return repositoryService
                .getBpmnModel(processInstance.getProcessDefinitionId())
                .getMainProcess();
    }

    private LeadDetailsDTO.Task mapToTask(UserTask userTask) {
        List<Property> properties = userTask.getFormProperties().stream().map(this::mapToProperty).collect(Collectors.toList());
        Boolean informationTask = getInformationTaskValueFromAttributes(userTask);
        Long daysToComplete = getDaysToCompleteFromAttributes(userTask);
        return new LeadDetailsDTO.Task(userTask.getId(), userTask.getName(), userTask.getDocumentation(), false, false,
                null, null,null, null, properties, informationTask, null, daysToComplete, LocalDateTime.now());
    }

    private Boolean getInformationTaskValueFromAttributes(UserTask userTask) {
        String value = userTask.getAttributeValue("http://flowable.org/bpmn", "informationTask");
        if (value != null) {
            return Boolean.parseBoolean(value);
        } else {
            return false;
        }
    }

    public Long getDaysToCompleteFromAttributes(UserTask userTask) {
        String daysToComplete = userTask.getAttributeValue("http://flowable.org/bpmn", "daysToComplete");
        if (daysToComplete != null) {
            return Long.parseLong(daysToComplete);
        } else {
            return null;
        }
    }

}
