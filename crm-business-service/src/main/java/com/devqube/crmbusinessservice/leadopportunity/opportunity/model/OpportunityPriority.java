package com.devqube.crmbusinessservice.leadopportunity.opportunity.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum OpportunityPriority {
    LOW("LOW"),

    MEDIUM("MEDIUM"),

    HIGH("HIGH");

    private String value;

    OpportunityPriority(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static OpportunityPriority fromValue(String text) {
        for (OpportunityPriority b : OpportunityPriority.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
}
