package com.devqube.crmbusinessservice.leadopportunity.bpmn.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Process
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-10-23T09:43:25.901763100+02:00[Europe/Belgrade]")

public class Process {
    @JsonProperty("name")
    private String name;

    @JsonProperty("documentation")
    private String documentation;

    @JsonProperty("id")
    private String id;

    @JsonProperty("bgColor")
    private String bgColor;

    @JsonProperty("textColor")
    private String textColor;

    /**
     * Gets or Sets objectType
     */
    public enum ObjectTypeEnum {
        LEAD("LEAD"),

        OPPORTUNITY("OPPORTUNITY");

        private String value;

        ObjectTypeEnum(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        public String toString() {
            return String.valueOf(value);
        }

        @JsonCreator
        public static ObjectTypeEnum fromValue(String text) {
            for (ObjectTypeEnum b : ObjectTypeEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + text + "'");
        }
    }

    @JsonProperty("objectType")
    private ObjectTypeEnum objectType;

    @JsonProperty("availableType")
    private String availableType;

    @JsonProperty("department")
    private String department;

    @JsonProperty("tasks")
    @Valid
    private List<Task> tasks = null;

    public Process name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     */
    @ApiModelProperty(value = "")


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Process documentation(String documentation) {
        this.documentation = documentation;
        return this;
    }

    /**
     * Get documentation
     *
     * @return documentation
     */
    @ApiModelProperty(value = "")


    public String getDocumentation() {
        return documentation;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    public Process id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     */
    @ApiModelProperty(value = "")


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Process bgColor(String bgColor) {
        this.bgColor = bgColor;
        return this;
    }

    /**
     * Get bgColor
     *
     * @return bgColor
     */
    @ApiModelProperty(value = "")


    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public Process textColor(String textColor) {
        this.textColor = textColor;
        return this;
    }

    /**
     * Get textColor
     *
     * @return textColor
     */
    @ApiModelProperty(value = "")


    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public Process objectType(ObjectTypeEnum objectType) {
        this.objectType = objectType;
        return this;
    }

    /**
     * Get objectType
     *
     * @return objectType
     */
    @ApiModelProperty(value = "")


    public ObjectTypeEnum getObjectType() {
        return objectType;
    }

    public void setObjectType(ObjectTypeEnum objectType) {
        this.objectType = objectType;
    }

    public Process availableType(String availableType) {
        this.availableType = availableType;
        return this;
    }

    /**
     * Get availableType
     *
     * @return availableType
     */
    @ApiModelProperty(value = "")


    public String getAvailableType() {
        return availableType;
    }

    public void setAvailableType(String availableType) {
        this.availableType = availableType;
    }

    public Process department(String department) {
        this.department = department;
        return this;
    }

    /**
     * Get department
     *
     * @return department
     */
    @ApiModelProperty(value = "")


    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Process tasks(List<Task> tasks) {
        this.tasks = tasks;
        return this;
    }

    public Process addTasksItem(Task tasksItem) {
        if (this.tasks == null) {
            this.tasks = new ArrayList<>();
        }
        this.tasks.add(tasksItem);
        return this;
    }

    /**
     * Get tasks
     *
     * @return tasks
     */
    @ApiModelProperty(value = "")

    @Valid

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Process process = (Process) o;
        return Objects.equals(this.name, process.name) &&
                Objects.equals(this.documentation, process.documentation) &&
                Objects.equals(this.id, process.id) &&
                Objects.equals(this.bgColor, process.bgColor) &&
                Objects.equals(this.textColor, process.textColor) &&
                Objects.equals(this.objectType, process.objectType) &&
                Objects.equals(this.availableType, process.availableType) &&
                Objects.equals(this.department, process.department) &&
                Objects.equals(this.tasks, process.tasks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, documentation, id, bgColor, textColor, objectType, availableType, department, tasks);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Process {\n");

        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    documentation: ").append(toIndentedString(documentation)).append("\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    bgColor: ").append(toIndentedString(bgColor)).append("\n");
        sb.append("    textColor: ").append(toIndentedString(textColor)).append("\n");
        sb.append("    objectType: ").append(toIndentedString(objectType)).append("\n");
        sb.append("    availableType: ").append(toIndentedString(availableType)).append("\n");
        sb.append("    department: ").append(toIndentedString(department)).append("\n");
        sb.append("    tasks: ").append(toIndentedString(tasks)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

