package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchSimilarObjectsDto {
    private String firstName; // contact
    private String lastName; // contact
    private String email; // contact
    private String phone; // contact / customer
    private String companyName; // customer
    private String wwwAddress; // customer
    private String streetAndNumber; // customer
    // all fields to opportunity
}
