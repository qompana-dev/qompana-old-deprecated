package com.devqube.crmbusinessservice.leadopportunity.processEngine.service;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.BPMNUtil;
import com.devqube.crmshared.exception.IncorrectNotificationTypeException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmshared.kafka.util.KafkaMessageDataBuilder;
import com.devqube.crmshared.notification.NotificationMsgType;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.flowable.bpmn.model.FlowElement;
import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@Service
@Slf4j
public class ProcessEngineNotificationService {

    private final KafkaService kafkaService;
    private final TaskService taskService;
    private final RepositoryService repositoryService;
    private final RuntimeService runtimeService;
    private final UserClient userClient;


    public ProcessEngineNotificationService(KafkaService kafkaService, TaskService taskService, UserClient userClient,
                                            RepositoryService repositoryService, RuntimeService runtimeService) {
        this.kafkaService = kafkaService;
        this.taskService = taskService;
        this.repositoryService = repositoryService;
        this.runtimeService = runtimeService;
        this.userClient = userClient;
    }

    public void sendOnCompleteTaskNotificationIfNeeded(String processInstanceId, String name, NotificationMsgType type) throws KafkaSendMessageException {
        Set<Long> notificationUserId = getNotificationUserId(processInstanceId);
        if (notificationUserId.size() > 0) {
            String taskName = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult().getName();
            try {
                kafkaService.addGroupNotification(notificationUserId, type,
                        KafkaMessageDataBuilder.builder().add("name", name).add("taskName", taskName).build());
            } catch (IncorrectNotificationTypeException e) {
                log.error(e.getMessage());
                throw new KafkaSendMessageException(e.getMessage());
            }
        }
    }

    public Long sendTaskConfirmationNotificationIfNeeded(String processInstanceId, String name, Long id, NotificationMsgType type) throws KafkaSendMessageException {
        Long acceptanceUserId = getAcceptanceUserId(processInstanceId);
        Long acceptorUserId = userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail());
        //Do not send notification if acceptance user from process and user who sends a request is the same
        if (acceptanceUserId != null && !acceptanceUserId.equals(acceptorUserId)) {
            String taskName = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult().getName();
            kafkaService.addNotification(acceptanceUserId, type,
                    KafkaMessageDataBuilder.builder()
                            .add("name", name)
                            .add("taskName", taskName)
                            .add("processInstanceId", processInstanceId)
                            .add("id", String.valueOf(id))
                            .build());
        }
        return acceptanceUserId;
    }

    public void sendAcceptanceNotification(Set<Long> userIds, String name, String taskName, String comment, NotificationMsgType type) throws KafkaSendMessageException, IncorrectNotificationTypeException {
        kafkaService.addGroupNotification(userIds, type,
                KafkaMessageDataBuilder.builder()
                        .add("name", name)
                        .add("taskName", taskName)
                        .add("comment", Strings.nullToEmpty(comment))
                        .build());
    }

    private Set<Long> getNotificationUserId(String processInstanceId) {
        Set<Long> notificationUserIds = new HashSet<>();
        FlowElement flowElement = getActiveFlowElementByProcessId(processInstanceId);
        if (flowElement != null && flowElement.getClass().isAssignableFrom(UserTask.class)) {
            UserTask userTask = (UserTask) flowElement;
            Optional<String> candidateUsers = userTask.getCandidateUsers().stream().reduce((e, f) -> e + "," + f);
            if (candidateUsers.isPresent()) {
                String notificationAsString = Strings.emptyToNull(BPMNUtil.getNotificationId(candidateUsers.get()));
                if (notificationAsString != null) {
                    String[] split = notificationAsString.split(",");
                    if (split.length > 0) {
                        notificationUserIds.addAll(Arrays.stream(split).map(Long::parseLong).collect(Collectors.toList()));
                    }
                }
            }
            Optional<String> candidateGroups = userTask.getCandidateGroups().stream().findFirst();
            if (candidateGroups.isPresent()) {
                boolean notificationToSupervisor = Boolean.parseBoolean(BPMNUtil.getNotificationId(candidateGroups.get()));
                if (notificationToSupervisor) {
                    notificationUserIds.add(userClient.getSupervisorId(CurrentRequestUtil.getLoggedInAccountEmail()));
                }
            }
        }
        return notificationUserIds;
    }

    private Long getAcceptanceUserId(String processInstanceId) {
        Long acceptanceId = null;
        FlowElement flowElement = getActiveFlowElementByProcessId(processInstanceId);
        if (flowElement != null && flowElement.getClass().isAssignableFrom(UserTask.class)) {
            UserTask userTask = (UserTask) flowElement;
            Optional<String> candidateUsers = userTask.getCandidateUsers().stream().findFirst();
            if (candidateUsers.isPresent()) {
                String acceptanceAsString = Strings.emptyToNull(BPMNUtil.getAcceptanceId(candidateUsers.get()));
                if (acceptanceAsString != null) {
                    acceptanceId = Long.parseLong(acceptanceAsString);
                }
            }
            Optional<String> candidateGroups = userTask.getCandidateGroups().stream().findFirst();
            if (candidateGroups.isPresent()) {
                boolean acceptanceFromSupervisor = Boolean.parseBoolean(BPMNUtil.getAcceptanceId(candidateGroups.get()));
                if (acceptanceFromSupervisor) {
                    acceptanceId = userClient.getSupervisorId(CurrentRequestUtil.getLoggedInAccountEmail());
                }
            }
        }
        return acceptanceId;
    }

    private FlowElement getActiveFlowElementByProcessId(String processInstanceId) {
        ProcessInstance processInstance = runtimeService
                .createProcessInstanceQuery()
                .processInstanceId(processInstanceId)
                .singleResult();
        String activeTaskDefinitionKey = taskService.createTaskQuery()
                .processInstanceId(processInstanceId)
                .singleResult().getTaskDefinitionKey();
        return repositoryService.getBpmnModel(processInstance.getProcessDefinitionId())
                .getMainProcess()
                .getFlowElement(activeTaskDefinitionKey);
    }
}
