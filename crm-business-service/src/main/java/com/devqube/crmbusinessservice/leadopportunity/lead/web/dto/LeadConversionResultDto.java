package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class LeadConversionResultDto {
    @AuthFields(list = {
            @AuthField(controller = "getLeadForEdit"),
            @AuthField(controller = "getLeads")
    })
    private Long id;

    @AuthFields(list = {
            @AuthField(controller = "getLeads"),
    })
    private String processInstanceId;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.leedKeeperColumn", controller = "getLeads"),
    })
    private Long leadKeeper;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.firstNameColumn", controller = "getLeads"),
    })
    private String firstName;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.firstNameColumn", controller = "getLeads"),
    })
    private String lastName;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.positionColumn", controller = "getLeads"),
    })
    private Long position;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.companyNameColumn", controller = "getLeads"),
    })
    private String company;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.cityColumn", controller = "getLeads"),
    })
    private String city;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.phoneColumn", controller = "getLeads"),
    })
    private String phone;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.emailColumn", controller = "getLeads"),
    })
    private String email;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.createdColumn", controller = "getLeads"),
    })
    private LocalDateTime creationDate;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.statusColumn", controller = "getLeads"),
    })
    private Long status;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.statusColumn", controller = "getLeads"),
    })
    private Long maxStatus;

    @AuthField(controller = "getLeads")
    private Long convertedToOpportunityId;

    @AuthField(controller = "getLeads")
    private String convertedToOpportunityName;

    @AuthField(controller = "getLeads")
    private String convertedToOpportunityDescription;

    @AuthField(controller = "getLeads")
    private String convertedToOpportunityAmount;

    @AuthField(controller = "getLeads")
    private Long convertedToContactId;

    @AuthField(controller = "getLeads")
    private String convertedToContactName;

    @AuthField(controller = "getLeads")
    private String convertedToContactPosition;

    @AuthField(controller = "getLeads")
    private String convertedToContactDepartament;

    @AuthField(controller = "getLeads")
    private String convertedToContactDescription;

    @AuthField(controller = "getLeads")
    private Long convertedToCustomerId;

    @AuthField(controller = "getLeads")
    private String convertedToCustomerName;

    @AuthField(controller = "getLeads")
    private String convertedToCustomerDescription;

    @AuthField(controller = "getLeads")
    private String convertedToCustomerNip;

    @AuthField(controller = "getLeads")
    private String convertedToCustomerRegon;

    public LeadConversionResultDto(Lead lead, Long status, Long maxStatus) {
        this.id = lead.getId();
        this.processInstanceId = lead.getProcessInstanceId();
        this.leadKeeper = lead.getLeadKeeperId();
        this.firstName = lead.getFirstName();
        this.lastName = lead.getLastName();
        this.position = lead.getPosition();
        this.company = lead.getCompany().getCompanyName();
        this.city = lead.getCompany().getCity();
        this.phone = lead.getPhone();
        this.email = lead.getEmail();
        this.creationDate = lead.getCreated();
        this.status = status;
        this.maxStatus = maxStatus;
        this.convertedToOpportunityId = lead.getConvertedToOpportunityId();
        this.convertedToContactId = lead.getConvertedToContactId();
        this.convertedToCustomerId = lead.getConvertedToCustomerId();
    }

    public void setConvertedToOpportunity(Opportunity opportunity) {
        this.convertedToOpportunityName = opportunity.getName();
        this.convertedToOpportunityAmount = opportunity.getAmount()+" "+opportunity.getCurrency();
        this.convertedToOpportunityDescription = opportunity.getDescription();
    }

    public void setConvertedToContact(Contact contact) {
        this.convertedToContactName = contact.getName() + " " + contact.getSurname();
        this.convertedToContactPosition = contact.getPosition();
        this.convertedToContactDescription = contact.getDescription();
        this.convertedToContactDepartament = contact.getDepartment();
    }

    public void setConvertedToCustomer(Customer customer) {
        this.convertedToCustomerName = customer.getName();
        this.convertedToCustomerDescription = customer.getDescription();
        this.convertedToCustomerNip = customer.getNip();
        this.convertedToCustomerRegon = customer.getRegon();
    }
}
