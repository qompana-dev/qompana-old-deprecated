package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.lead.model.LastViewedLead;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.LeadPriority;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class LeadOnListDTO {
    @AuthFields(list = {
            @AuthField(controller = "getLeadForEdit"),
            @AuthField(controller = "getLeads")
    })
    private Long id;

    @AuthFields(list = {
            @AuthField(controller = "getLeads"),
    })
    private String processInstanceId;

    @AuthFields(list = {
            @AuthField(controller = "getLeads"),
    })
    private String processInstanceName;

    @AuthFields(list = {
            @AuthField(controller = "getLeads"),
    })
    private int processInstanceVersion;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.leedKeeperColumn", controller = "getLeads"),
    })
    private Long leadKeeper;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.firstNameColumn", controller = "getLeads"),
    })
    private String firstName;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.firstNameColumn", controller = "getLeads"),
    })
    private String lastName;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.positionColumn", controller = "getLeads"),
    })
    private String position;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.companyNameColumn", controller = "getLeads"),
    })
    private String company;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.cityColumn", controller = "getLeads"),
    })
    private String city;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.phoneColumn", controller = "getLeads"),
    })
    private String phone;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.emailColumn", controller = "getLeads"),
    })
    private String email;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.createdColumn", controller = "getLeads"),
    })
    private LocalDateTime creationDate;

    private LocalDateTime updateDate;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.lastViewedColumn", controller = "getLeads"),
    })
    private LocalDateTime lastViewedDate;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.statusColumn", controller = "getLeads"),
    })
    private Long status;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.statusColumn", controller = "getLeads"),
    })
    private Long acceptanceUserId;

    @AuthFields(list = {
            @AuthField(frontendId = "LeadListComponent.statusColumn", controller = "getLeads"),
    })
    private Long maxStatus;

    @AuthFields(list = {@AuthField(frontendId = "LeadListComponent.priorityColumn", controller = "getLeads")})
    private LeadPriority priority;

    @AuthFields(list = {@AuthField(frontendId = "LeadListComponent.avatarColumn", controller = "getLeads")})
    private String avatar;

    public LeadOnListDTO(Lead lead,
                         String processInstanceName,
                         int processInstanceVersion,
                         String position,
                         Long status,
                         Long maxStatus) {
        this.id = lead.getId();
        this.processInstanceId = lead.getProcessInstanceId();
        this.processInstanceName = processInstanceName;
        this.processInstanceVersion = processInstanceVersion;
        this.leadKeeper = lead.getLeadKeeperId();
        this.firstName = lead.getFirstName();
        this.lastName = lead.getLastName();
        this.position = position;
        this.company = lead.getCompany().getCompanyName();
        this.city = lead.getCompany().getCity();
        this.phone = lead.getPhone();
        this.email = lead.getEmail();
        this.creationDate = lead.getCreated();
        this.updateDate = lead.getUpdated() != null ? lead.getUpdated() : lead.getCreated();
        this.lastViewedDate = this.findLastViewedDate(lead.getLastViewedLeads());
        this.status = status;
        this.maxStatus = maxStatus;
        this.avatar = lead.getAvatar();
        this.priority = lead.getPriority();
        this.acceptanceUserId = lead.getAcceptanceUserId();
    }

    private LocalDateTime findLastViewedDate(Set<LastViewedLead> lastViewedLeads) {
        if(Objects.isNull(lastViewedLeads) || lastViewedLeads.isEmpty()) {
            return null;
        }

        List<LastViewedLead> lastViewedList = new LinkedList<>(lastViewedLeads);

        List<LastViewedLead> lastViewedSortedList = lastViewedList.stream()
                .sorted(Comparator.comparing(LastViewedLead::getLastViewed).reversed())
                .collect(Collectors.toList());

        return lastViewedSortedList.get(0).getLastViewed();
    }
}
