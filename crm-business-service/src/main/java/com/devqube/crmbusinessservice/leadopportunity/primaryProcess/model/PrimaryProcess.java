package com.devqube.crmbusinessservice.leadopportunity.primaryProcess.model;

import com.devqube.crmshared.history.annotation.SaveHistoryIgnore;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PrimaryProcess {
    @Id
    @SequenceGenerator(name = "primary_process_seq", sequenceName = "primary_process_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "primary_process_seq")
    @EqualsAndHashCode.Include
    @SaveHistoryIgnore
    private Long id;

    @Column(nullable = false)
    private String processId;

    @Column(nullable = false)
    private String type;
}
