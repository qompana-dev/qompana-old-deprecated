package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

public enum LeadSearchType {

    FIRST_NAME, LAST_NAME, COMPANY_NAME, COMPANY_CITY, PHONE, EMAIL, SOURCE

}
