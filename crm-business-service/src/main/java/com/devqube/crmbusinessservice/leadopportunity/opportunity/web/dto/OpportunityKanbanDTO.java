package com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.OpportunityPriority;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AuthFilterEnabled
public class OpportunityKanbanDTO {
    @AuthField(controller = "getOpportunities")
    private Long id;
    @AuthField(frontendId = "OpportunityListComponent.nameColumn", controller = "getOpportunities")
    private String name;
    @AuthField(frontendId = "OpportunityListComponent.customerNameColumn", controller = "getOpportunities")
    private Long customerId;
    @AuthField(frontendId = "OpportunityListComponent.customerNameColumn", controller = "getOpportunities")
    private String customerName;
    @AuthField(frontendId = "OpportunityListComponent.customerNameColumn", controller = "getOpportunities")
    private String customerAvatar;
    @AuthField(frontendId = "OpportunityListComponent.customerNameColumn", controller = "getOpportunities")
    private Long contactId;
    @AuthField(frontendId = "OpportunityListComponent.finishDateColumn", controller = "getOpportunities")
    private LocalDate finishDate;
    @AuthField(frontendId = "OpportunityListComponent.statusColumn", controller = "getOpportunities")
    private Long status;
    @AuthField(frontendId = "OpportunityListComponent.statusColumn", controller = "getOpportunities")
    private Long maxStatus;
    @AuthField(frontendId = "OpportunityListComponent.keeperColumn", controller = "getOpportunities")
    private Long ownerOneId;
    @AuthField(frontendId = "OpportunityListComponent.keeperColumn", controller = "getOpportunities")
    private Long ownerOnePercentage;
    @AuthField(frontendId = "OpportunityListComponent.keeperColumn", controller = "getOpportunities")
    private Long ownerTwoId;
    @AuthField(frontendId = "OpportunityListComponent.keeperColumn", controller = "getOpportunities")
    private Long ownerTwoPercentage;
    @AuthField(frontendId = "OpportunityListComponent.keeperColumn", controller = "getOpportunities")
    private Long ownerThreeId;
    @AuthField(frontendId = "OpportunityListComponent.keeperColumn", controller = "getOpportunities")
    private Long ownerThreePercentage;
    @AuthField(frontendId = "OpportunityListComponent.priorityColumn", controller = "getOpportunities")
    private OpportunityPriority priority;
    @AuthField(frontendId = "OpportunityListComponent.subjectOfInterestColumn", controller = "getOpportunities")
    private Long subjectOfInterest;
    @AuthField(controller = "getOpportunities")
    private Long acceptanceUserId;
    @AuthField(controller = "getOpportunities")
    private Opportunity.FinishResult finishResult;
    @AuthField(controller = "getOpportunities")
    private Opportunity.RejectionReason rejectionReason;
    @AuthField(controller = "getOpportunities")
    private String processInstanceName;
    @AuthField(controller = "getOpportunities")
    private int processInstanceVersion;
    @AuthField(controller = "getOpportunities")
    private String processInstanceId;

    @AuthField(frontendId = "OpportunityListComponent.createDateColumn", controller = "getOpportunities")
    private LocalDate createDate;

    @AuthField(frontendId = "OpportunityListComponent.createDateColumn", controller = "getOpportunities")
    private LocalDateTime changeUserTaskDate;

    private LocalDateTime updateDate;

    @AuthField(frontendId = "OpportunityListComponent.amountColumn", controller = "getOpportunities")
    private Double amount;

    @AuthField(frontendId = "OpportunityListComponent.currencyColumn", controller = "getOpportunities")
    private String currency;

    @AuthField(frontendId = "OpportunityListComponent.currencyColumn", controller = "getOpportunities")
    private Long probability;

    @AuthField(frontendId = "OpportunityListComponent.amountColumn", controller = "getOpportunities")
    private Double amountInDefaultCurrency;

    @AuthField(controller = "getOpportunities")
    private Long daysToTaskComplete;

    @AuthField(controller = "getOpportunities")
    private ObjectMoveStepDto canBeMoveForward;

    @AuthField(controller = "getOpportunities")
    private ObjectMoveStepDto canBeMoveBackward;



    public OpportunityKanbanDTO(Opportunity opportunity,
                                Double amountInDefaultCurrency,
                                String processInstanceName,
                                int processInstanceVersion,
                                Long status,
                                Long maxStatus, Long daysToTaskComplete) {
        this.id = opportunity.getId();
        this.name = opportunity.getName();
        if (opportunity.getCustomer() != null) {
            this.customerId = opportunity.getCustomer().getId();
            this.customerName = opportunity.getCustomer().getName();
            this.customerAvatar = opportunity.getCustomer().getAvatar();
        }
        if (opportunity.getContact() != null) {
            this.contactId = opportunity.getContact().getId();
        }
        this.finishDate = opportunity.getFinishDate();
        this.status = status;
        this.maxStatus = maxStatus;
        this.ownerOneId = opportunity.getOwnerOneId();
        this.ownerOnePercentage = opportunity.getOwnerOnePercentage();
        this.ownerTwoId = opportunity.getOwnerTwoId();
        this.ownerTwoPercentage = opportunity.getOwnerTwoPercentage();
        this.ownerThreeId = opportunity.getOwnerThreeId();
        this.ownerThreePercentage = opportunity.getOwnerThreePercentage();
        this.priority = opportunity.getPriority();
        this.subjectOfInterest = opportunity.getSubjectOfInterest();
        this.finishResult = opportunity.getFinishResult();
        this.rejectionReason = opportunity.getRejectionReason();
        this.processInstanceName = processInstanceName;
        this.processInstanceVersion = processInstanceVersion;
        this.createDate = opportunity.getCreateDate();
        this.changeUserTaskDate = opportunity.getChangeUserTaskDate();
        this.updateDate = opportunity.getUpdated() != null ? opportunity.getUpdated() : LocalDateTime.of(opportunity.getCreateDate(), LocalTime.of(12, 00));
        this.amount = opportunity.getAmount();
        this.currency = opportunity.getCurrency();
        this.probability = opportunity.getProbability();
        this.amountInDefaultCurrency = amountInDefaultCurrency;
        this.acceptanceUserId = opportunity.getAcceptanceUserId();
        this.daysToTaskComplete = daysToTaskComplete;
    }
}
