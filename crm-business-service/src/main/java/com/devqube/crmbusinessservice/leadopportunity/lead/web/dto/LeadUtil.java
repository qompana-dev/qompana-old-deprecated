package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;


import com.devqube.crmbusinessservice.leadopportunity.company.Company;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.LeadPriority;
import com.devqube.crmbusinessservice.phoneNumber.PhoneNumberMapper;

public class LeadUtil {
    public static Lead setDTOValuesToLead(LeadSaveDTO dto, Lead lead) {
        Company company = lead.getCompany();
        if (company == null) {
            company = new Company();
            lead.setCompany(company);
        }
        setCompanyValues(dto, company);
        setLeadValues(dto, lead);
        return lead;
    }

    public static LeadSaveDTO fromLead(Lead lead, String processDefinitionId) {
        LeadSaveDTO dto = new LeadSaveDTO();
        dto.setId(lead.getId());
        dto.setProcessDefinitionId(processDefinitionId);
        dto.setCompanyId(lead.getCompany().getId());
        if (lead.getCompany() != null) {
            dto.setCompanyNip(lead.getCompany().getNip());
            dto.setIndustry(lead.getCompany().getIndustry());
            dto.setCompanyName(lead.getCompany().getCompanyName());
            dto.setWwwAddress(lead.getCompany().getWwwAddress());
            dto.setStreetAndNumber(lead.getCompany().getStreetAndNumber());
            dto.setCity(lead.getCompany().getCity());
            dto.setPostalCode(lead.getCompany().getPostalCode());
            dto.setProvince(lead.getCompany().getProvince());
            dto.setCountry(lead.getCompany().getCountry());
        }
        if (lead.getPriority() != null) {
            dto.setPriority(LeadPriority.fromValue(lead.getPriority().name()));
        }
        dto.setScoring(lead.getScoring());
        dto.setFirstName(lead.getFirstName());
        dto.setLastName(lead.getLastName());
        dto.setPosition(lead.getPosition());
        dto.setEmail(lead.getEmail());
        dto.setPhone(lead.getPhone());
        if (lead.getAdditionalPhones() != null && !lead.getAdditionalPhones().isEmpty()) {
            dto.setAdditionalPhones(PhoneNumberMapper.mapToPhoneNumberDtoSet(lead.getAdditionalPhones()));
        }
        dto.setCreator(lead.getCreator());
        dto.setLeadKeeperId(lead.getLeadKeeperId());
        dto.setSourceOfAcquisition(lead.getSourceOfAcquisition());
        dto.setSubSourceOfAcquisition(lead.getSubSourceOfAcquisition());
        dto.setSubjectOfInterest(lead.getSubjectOfInterest());
        dto.setInterestedInCooperation(lead.getInterestedInCooperation());
        dto.setAvatar(lead.getAvatar());
        return dto;
    }

    public static LeadSaveDTO fromLead(Lead lead, String processDefinitionId, String note) {
        LeadSaveDTO leadSaveDTO = fromLead(lead, processDefinitionId);
        leadSaveDTO.setNotes(note);
        return leadSaveDTO;
    }

    private static void setLeadValues(LeadSaveDTO dto, Lead lead) {
        if (dto.getPriority() != null) {
            lead.setPriority(LeadPriority.valueOf(dto.getPriority().toString()));
        } else {
            Long scoring = dto.getScoring();
            if (scoring == null || scoring < 3) {
                lead.setPriority(LeadPriority.LOW);
            } else if (scoring < 5) {
                lead.setPriority(LeadPriority.MEDIUM);
            } else {
                lead.setPriority(LeadPriority.HIGH);
            }
        }
        lead.setScoring(dto.getScoring());
        lead.setFirstName(dto.getFirstName());
        lead.setLastName(dto.getLastName());
        lead.setPosition(dto.getPosition());
        lead.setEmail(dto.getEmail());
        lead.setLeadKeeperId(dto.getLeadKeeperId());
        lead.setPhone(dto.getPhone());
        lead.setSourceOfAcquisition(dto.getSourceOfAcquisition());
        lead.setSubSourceOfAcquisition(dto.getSubSourceOfAcquisition());
        lead.setSubjectOfInterest(dto.getSubjectOfInterest());
        lead.setInterestedInCooperation(dto.getInterestedInCooperation());
        lead.setAvatar(dto.getAvatar());
    }

    private static void setCompanyValues(LeadSaveDTO dto, Company company) {
        company.setNip(dto.getCompanyNip());
        company.setCompanyName(dto.getCompanyName());
        company.setWwwAddress(dto.getWwwAddress());
        company.setStreetAndNumber(dto.getStreetAndNumber());
        company.setCity(dto.getCity());
        company.setPostalCode(dto.getPostalCode());
        company.setProvince(dto.getProvince());
        company.setCountry(dto.getCountry());
        company.setIndustry(dto.getIndustry());
    }
}
