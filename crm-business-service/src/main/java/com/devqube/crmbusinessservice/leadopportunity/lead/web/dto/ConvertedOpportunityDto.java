package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import com.devqube.crmbusinessservice.customer.address.Address;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConvertedOpportunityDto {
    private String customerName;
    private String customerNip;
    private Long customerOwner;
    @JsonProperty(access = Access.READ_ONLY)
    private Address customerAddress;

    private String contactName;
    private String contactPosition;
    private String contactPhone;
    private String contactEmail;
    private Long contactOwner;
    private String contactCustomerName;

    private Long opportunityId;
    private String opportunityName;
    private String opportunityProcessName;
    private String opportunitySubjectOfInterest;
    private Double opportunityAmount;
    private String opportunityCurrency;
    private Long opportunityOwner;
}
