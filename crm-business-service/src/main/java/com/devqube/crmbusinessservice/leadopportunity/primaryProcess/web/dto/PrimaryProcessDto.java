package com.devqube.crmbusinessservice.leadopportunity.primaryProcess.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PrimaryProcessDto {
    private String processId;
    private String type;
}
