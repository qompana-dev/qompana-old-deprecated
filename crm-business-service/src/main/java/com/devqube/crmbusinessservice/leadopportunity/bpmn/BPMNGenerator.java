package com.devqube.crmbusinessservice.leadopportunity.bpmn;

import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.PossibleValue;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Process;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Property;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Task;
import com.devqube.crmshared.exception.BadRequestException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ByteArrayResource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

class BPMNGenerator {

    private Document document;
    private Element processElement;

    ByteArrayResource generateBPMNFile(Process process) throws BadRequestException, ParserConfigurationException, TransformerException {
        validateAndThrowExceptionIfInvalid(process);
        addDefaultLastTask(process);
        createBPMNStructuredXML(process);
        addAllTasksToProcess(process.getTasks(), process.getObjectType());
        return new ByteArrayResource(getDocumentAsBytes());
    }

    private void validateAndThrowExceptionIfInvalid(Process process) throws BadRequestException {
        validateProcess(process);
        validateTasks(process.getTasks());
        for (Task task : process.getTasks()) {
            validateTaskProperties(task.getProperties());
        }
        validatePropertiesForUniqueNames(
                process.getTasks()
                        .stream()
                        .filter(e -> e.getProperties() != null)
                        .flatMap(e -> e.getProperties().stream())
                        .collect(Collectors.toList()));
    }

    private void validatePropertiesForUniqueNames(List<Property> properties) throws BadRequestException {
        int numberOfElements = properties.size();
        long numberOfUniqueElements = properties.stream().map(Property::getId).distinct().count();
        if (numberOfElements != numberOfUniqueElements) {
            throw new BadRequestException("Names of properties are not unique");
        }
    }

    private void validateTaskProperties(List<Property> properties) throws BadRequestException {
        if (properties != null) {
            for (Property property : properties) {
                if(property.getId() == null || property.getType() == null) {
                    throw new BadRequestException("Property doesn't have id or type set");
                }
                if (!property.getType().equals(Property.TypeEnum.ENUM) && property.getPossibleValues() != null) {
                    throw new BadRequestException("Property of type other than enum cannot have possible values");
                }
                if (property.getType().equals(Property.TypeEnum.ENUM) && property.getPossibleValues() == null) {
                    throw new BadRequestException("Property of type enum must have possible values");
                }
            }
        }
    }

    private void validateProcess(Process process) throws BadRequestException {
        if (StringUtils.isEmpty(process.getId()) || StringUtils.isEmpty(process.getName())) {
            throw new BadRequestException("Process id or name is not defined in request");
        }
        if (process.getTasks() == null) {
            throw new BadRequestException("No tasks defined in request");
        }
        if (process.getObjectType() == null) {
            throw new BadRequestException("No object type defined in request");
        }
    }

    private void validateTasks(List<Task> tasks) throws BadRequestException {
        for (Task task : tasks) {
            if (StringUtils.isEmpty(task.getName())) {
                throw new BadRequestException("Task's name or id is not defined in request: " + task);
            }
        }
    }

    private void createBPMNStructuredXML(Process process) throws ParserConfigurationException {
        document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        Element rootElement = createRootElementAndAddItToXML(process.getObjectType());
        processElement = document.createElement("process");
        processElement.setAttribute("id", process.getId());
        processElement.setAttribute("name", process.getName());
        processElement.setAttribute("flowable:fontColor", process.getBgColor() + ";" + process.getTextColor());
        if (process.getAvailableType() != null && process.getAvailableType().equals("DEPARTMENT")) {
            createElementAndAddItToParentElement("documentation", process.getDepartment(), processElement);
        }
        rootElement.appendChild(processElement);
    }

    private void addAllTasksToProcess(List<Task> tasks, Process.ObjectTypeEnum objectType) {
        Element predecessor = createElementAndAddItToProcess("startEvent", "start", "start");
        for (int i = 0; i < tasks.size(); i++) {
            String taskId = "task" + i;
            createSequenceElementAndAddItToProcess(taskId, predecessor.getAttribute("id"), taskId);
            predecessor = createElementAndAddItToProcess("userTask", taskId, tasks.get(i).getName());
            if (tasks.get(i).getDocumentation() != null && tasks.get(i).getDocumentation().length() > 0) {
                createElementAndAddItToParentElement("documentation", tasks.get(i).getDocumentation(), predecessor);
            }
            addAttributeToElement(predecessor, "flowable:candidateUsers", BPMNUtil.joinAcceptanceAndNotification(tasks.get(i).getAcceptanceUser(), tasks.get(i).getNotificationUser()));
            addAttributeToElement(predecessor, "flowable:candidateGroups", BPMNUtil.joinAcceptanceAndNotification(tasks.get(i).getAcceptanceGroup(), tasks.get(i).getNotificationGroup()));
            addAttributeToElement(predecessor, "flowable:informationTask", BPMNUtil.getInformationTaskValueOrFalse(tasks.get(i)));
            addAttributeToElement(predecessor, "flowable:daysToComplete", BPMNUtil.getDaysToCompleteOrZero(tasks.get(i)));
            if (objectType.equals(Process.ObjectTypeEnum.OPPORTUNITY)) {
                BigDecimal salesOpportunity = tasks.get(i).getSalesOpportunity();
                addAttributeToElement(predecessor, "flowable:assignee", salesOpportunity != null ? salesOpportunity.toString() : "");
            }
            addPropertiesToTask(predecessor, tasks.get(i));
        }
        createElementAndAddItToProcess("endEvent", "end", "end");
        createSequenceElementAndAddItToProcess(String.valueOf(tasks.size()), predecessor.getAttribute("id"), "end");
    }

    private void addPropertiesToTask(Element predecessor, Task task) {
        if (task.getProperties() != null) {
            Element extensionElements = document.createElement("extensionElements");
            predecessor.appendChild(extensionElements);
            for (Property property : task.getProperties()) {
                addSingleProperty(extensionElements, property);
            }
        }
    }

    private void addSingleProperty(Element extensionElements, Property property) {
        Element propertyElement = document.createElement("flowable:formProperty");
        propertyElement.setAttribute("id", property.getId());
        propertyElement.setAttribute("type", property.getType().toString().toLowerCase());
        propertyElement.setAttribute("required",
                String.valueOf(property.getRequired() != null ? property.getRequired() : true));
        if (property.getType().equals(Property.TypeEnum.DATE)) {
            propertyElement.setAttribute("datePattern", "dd-MM-yyyy");
        }
        if (property.getType().equals(Property.TypeEnum.ENUM)) {
            addPossibleValuesForProperty(property, propertyElement);
        }
        extensionElements.appendChild(propertyElement);
    }

    private void addPossibleValuesForProperty(Property property, Element propertyElement) {
        for (PossibleValue value : property.getPossibleValues()) {
            Element valueElement = document.createElement("flowable:value");
            valueElement.setAttribute("id", value.getId());
            valueElement.setAttribute("name", value.getName());
            propertyElement.appendChild(valueElement);
        }
    }

    private void createSequenceElementAndAddItToProcess(String id, String sourceRef, String targetRef) {
        Element sequenceElement = document.createElement("sequenceFlow");
        sequenceElement.setAttribute("id", "flow-" + id);
        sequenceElement.setAttribute("sourceRef", sourceRef);
        sequenceElement.setAttribute("targetRef", targetRef);
        processElement.appendChild(sequenceElement);
    }

    private void createElementAndAddItToParentElement(String tagName, String content, Element parentElement) {
        Element element = document.createElement(tagName);
        element.setTextContent(content);
        parentElement.appendChild(element);
    }

    private Element createElementAndAddItToProcess(String tagName, String id, String name) {
        Element element = document.createElement(tagName);
        element.setAttribute("id", id);
        element.setAttribute("name", name);
        processElement.appendChild(element);
        return element;
    }

    private Element createRootElementAndAddItToXML(Process.ObjectTypeEnum objectType) {
        Element root = document.createElementNS("http://www.omg.org/spec/BPMN/20100524/MODEL", "definitions");
        root.setAttribute("xmlns:flowable", "http://flowable.org/bpmn");
        root.setAttribute("targetNamespace", objectType.toString());
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation", "http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd");
        document.appendChild(root);
        return root;
    }

    private void addAttributeToElement(Element element, String key, String value) {
        if (value != null && value.length() > 0) {
            element.setAttribute(key, value);
        }
    }

    private byte[] getDocumentAsBytes() throws TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(document);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        StreamResult result = new StreamResult(stream);
        transformer.transform(source, result);
        return stream.toByteArray();
    }

    private void addDefaultLastTask(Process process) {
        process.getTasks().add(new Task());
    }
}
