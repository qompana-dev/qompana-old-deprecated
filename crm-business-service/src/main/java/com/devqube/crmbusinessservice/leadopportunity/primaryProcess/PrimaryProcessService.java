package com.devqube.crmbusinessservice.leadopportunity.primaryProcess;

import com.devqube.crmbusinessservice.leadopportunity.primaryProcess.model.PrimaryProcess;
import com.devqube.crmbusinessservice.leadopportunity.primaryProcess.web.dto.PrimaryProcessDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PrimaryProcessService {
    private final PrimaryProcessRepository primaryProcessRepository;
    private final ModelMapper modelMapper;

    public PrimaryProcessService(PrimaryProcessRepository primaryProcessRepository, ModelMapper modelMapper) {
        this.primaryProcessRepository = primaryProcessRepository;
        this.modelMapper = modelMapper;
    }

    public PrimaryProcessDto savePrimaryProcess(String processId, String type, String loggedAccountEmail) {
        Optional<PrimaryProcess> byType = primaryProcessRepository.findByType(type);
        byType.ifPresent(primaryProcessRepository::delete);
        PrimaryProcess processToSave = PrimaryProcess.builder().processId(processId).type(type).build();
        return modelMapper.map(primaryProcessRepository.save(processToSave), PrimaryProcessDto.class);
    }

    public void deletePrimaryProcess(String processId, String type) {
        Optional<PrimaryProcess> primaryProcess = primaryProcessRepository.findByProcessIdAndType(processId, type);
        primaryProcess.ifPresent(primaryProcessRepository::delete);
    }

    public List<PrimaryProcessDto> getAllPrimaryProcess() {
        return primaryProcessRepository.findAll().stream()
                .map(primaryProcess -> modelMapper.map(primaryProcess, PrimaryProcessDto.class)).collect(Collectors.toList());
    }
}
