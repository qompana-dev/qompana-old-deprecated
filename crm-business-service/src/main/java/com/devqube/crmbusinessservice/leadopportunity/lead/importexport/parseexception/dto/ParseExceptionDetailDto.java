package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parseexception.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParseExceptionDetailDto {
    private Long line;
    private String field;
    private String value;
}
