package com.devqube.crmbusinessservice.leadopportunity.processEngine.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationActionDto {
    private Long userNotificationId;
    private String comment;

    public NotificationActionDto(Long userNotificationId) {
        this.userNotificationId = userNotificationId;
    }
}

