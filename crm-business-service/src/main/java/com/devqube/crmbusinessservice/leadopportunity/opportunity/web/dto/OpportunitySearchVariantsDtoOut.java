package com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class OpportunitySearchVariantsDtoOut {

    private OpportunitySearchType type;
    private List<String> variants = new ArrayList<>();

}
