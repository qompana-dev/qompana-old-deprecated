package com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ProcessColor;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AuthFilterEnabled
public class MobileOpportunityListDto {
    @AuthField(controller = "getOpportunities")
    private Long id;
    @AuthField(frontendId = "OpportunityListComponent.nameColumn", controller = "getOpportunities")
    private String name;
    @AuthField(frontendId = "OpportunityListComponent.customerNameColumn", controller = "getOpportunities")
    private String customerName;
    @AuthField(frontendId = "OpportunityListComponent.finishDateColumn", controller = "getOpportunities")
    private LocalDate finishDate;
    @AuthField(frontendId = "OpportunityListComponent.statusColumn", controller = "getOpportunities")
    private Long status;
    @AuthField(frontendId = "OpportunityListComponent.statusColumn", controller = "getOpportunities")
    private Long maxStatus;

    private ProcessColor processColor;

    private Double amount;

    private String currency;

    public MobileOpportunityListDto(Opportunity opportunity, Long status, Long maxStatus, ProcessColor processColor) {
        this.id = opportunity.getId();
        this.name = opportunity.getName();
        if (opportunity.getCustomer() != null) {
            this.customerName = opportunity.getCustomer().getName();
        }
        this.finishDate = opportunity.getFinishDate();
        this.status = status;
        this.maxStatus = maxStatus;
        this.processColor = processColor;
        this.amount = opportunity.getAmount();
        this.currency = opportunity.getCurrency();
    }
}
