package com.devqube.crmbusinessservice.leadopportunity.company;

import org.springframework.data.jpa.domain.Specification;

public class CompanySpecifications {

    public static Specification<Company> findByName(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("companyName")), "%" + toSearch.toLowerCase() + "%");
    }

    public static Specification<Company> findByCity(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("city")), "%" + toSearch.toLowerCase() + "%");
    }

}
