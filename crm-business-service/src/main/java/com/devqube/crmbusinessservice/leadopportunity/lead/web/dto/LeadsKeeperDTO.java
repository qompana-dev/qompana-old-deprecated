package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@AuthFilterEnabled
public class LeadsKeeperDTO {

    private Long leadKeeper;
    private List<Long> leadsIds;
}
