package com.devqube.crmbusinessservice.leadopportunity.kanban.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.flowable.bpmn.model.UserTask;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class KanbanOpportunityTaskOverview {

    @AuthField(frontendId = "LeadKanbanComponent.stepNameField", controller = "getProcessesOverview")
    private String id;

    @AuthField(frontendId = "LeadKanbanComponent.stepNameField", controller = "getProcessesOverview")
    private String name;

    @AuthField(frontendId = "LeadKanbanComponent.stepNameField", controller = "getProcessesOverview")
    private String assignee;

    @AuthField(frontendId = "LeadKanbanComponent.numberLeadsInTaskField", controller = "getProcessesOverview")
    private Integer includedOpportunitiesNumber;

    @AuthField(frontendId = "LeadKanbanComponent.stepNameField", controller = "getProcessesOverview")
    private Double summaryValue;

    public KanbanOpportunityTaskOverview(UserTask task, List<KanbanOpportunity> opportunities) {
        this.id = task.getId();
        this.name = task.getName();
        this.assignee = task.getAssignee();
        this.includedOpportunitiesNumber = opportunities.size();
        this.summaryValue = opportunities.stream().mapToDouble(KanbanOpportunity::getAmountInDefaultCurrency).sum();
    }
}
