package com.devqube.crmbusinessservice.leadopportunity.lead.model;

import com.devqube.crmbusinessservice.leadopportunity.company.Company;
import com.devqube.crmbusinessservice.phoneNumber.PhoneNumber;
import com.devqube.crmshared.history.SaveCrmChangesListener;
import com.devqube.crmshared.history.annotation.SaveHistory;
import com.devqube.crmshared.history.annotation.SaveHistoryIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Accessors(chain = true)
@EntityListeners(SaveCrmChangesListener.class)
public class Lead {
    @Id
    @SequenceGenerator(name = "lead_seq", sequenceName = "lead_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lead_seq")
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @SaveHistory(fields = {"companyType", "companyName", "wwwAddress", "streetAndNumber", "city", "postalCode", "province", "country"},
            message = "{" +
                    "\"leads.form.companyType\": \"{{companyType}}\"," +
                    "\"leads.form.companyName\": \"{{companyName}}\"," +
                    "\"leads.form.webpageAddress\": \"{{wwwAddress}}\"," +
                    "\"leads.form.street\": \"{{streetAndNumber}}\"," +
                    "\"leads.form.city\": \"{{city}}\"," +
                    "\"leads.form.zipCode\": \"{{postalCode}}\"," +
                    "\"leads.form.province\": \"{{province}}\"," +
                    "\"leads.form.country\": \"{{country}}\"" +
                    "}")
    private Company company;

    private String processInstanceId;

    private String firstName;
    private String lastName;
    private Long position;
    private String email;
    private String phone;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "lead",
            fetch = FetchType.EAGER)
    @SaveHistory(fields = {"type", "number"},
            message = "{" +
                    "\"leads.form.additionalPhone.type\": \"{{type}}\"," +
                    "\"leads.form.additionalPhone.number\": \"{{number}}\"" +
                    "}", separator = ", ")
    private Set<PhoneNumber> additionalPhones = new HashSet<>();

    private Long leadKeeperId;
    private Long sourceOfAcquisition;
    private Long subSourceOfAcquisition;

    @NotNull
    private Long creator;

    @Column(name= "created", nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime created;

    @SaveHistoryIgnore
    @UpdateTimestamp
    private LocalDateTime updated;

    private Long acceptanceUserId;

    private boolean deleted;

    private boolean closed;

    private Boolean convertedToOpportunity = false;

    private Long convertedBy;

    private LocalDateTime convertedDateTime;

    private Long convertedToOpportunityId;

    private Long convertedToContactId;

    private Long convertedToCustomerId;

    @Enumerated(EnumType.STRING)
    private RejectionReason rejectionReason;

    @Enumerated(EnumType.STRING)
    private CloseResult closeResult;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lead")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<LastViewedLead> lastViewedLeads;

    @Column(name = "avatar")
    private String avatar;

    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("priority")
    private LeadPriority priority;

    private Long scoring;

    @JsonProperty("mainNoteId")
    private Long mainNoteId;

    @JsonProperty("subjectOfInterest")
    @Column(name = "subject_of_interest_id")
    private Long subjectOfInterest;

    @JsonProperty("interestedInCooperation")
    private Boolean interestedInCooperation;

    public enum RejectionReason {
        NONE, LOST, NO_BUDGET, NO_DECISION, OTHER
    }

    public enum CloseResult {
        CLOSED,
        ARCHIVED
    }
}