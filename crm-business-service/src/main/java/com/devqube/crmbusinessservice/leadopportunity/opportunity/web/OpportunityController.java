package com.devqube.crmbusinessservice.leadopportunity.opportunity.web;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityService;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.*;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.access.annotation.AuthControllerCase;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.search.CustomCrmObject;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class OpportunityController implements OpportunityApi {

    private final OpportunityService opportunityService;

    public OpportunityController(OpportunityService opportunityService) {
        this.opportunityService = opportunityService;
    }

    @Override
    @AuthController(name = "getOpportunities")
    public ResponseEntity<List<OpportunityListDTO>> getOpportunitiesForZapier(String loggedAccountEmail, @Valid Pageable pageable, @Valid Boolean finished) {
        try {
            if (finished != null && finished) {
                return new ResponseEntity<>(opportunityService.findAllFinished(loggedAccountEmail, pageable).getContent(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(opportunityService.findAll(loggedAccountEmail, null, pageable).getContent(), HttpStatus.OK);
            }
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getOpportunities")
    public ResponseEntity<Page> getOpportunities(String loggedAccountEmail,
                                                 @Valid Pageable pageable,
                                                 @Valid Boolean today,
                                                 @Valid Boolean edited,
                                                 @Valid Boolean viewed,
                                                 @Valid Boolean forApproving,
                                                 @Valid Opportunity.FinishResult finishResult,
                                                 @Valid Boolean opened,
                                                 @Valid String search,
                                                 @Valid String customer,
                                                 @Valid String contact) {
        try {
            if (opened != null && opened) {
                return new ResponseEntity<>(opportunityService.findOpened(loggedAccountEmail, search, pageable), HttpStatus.OK);
            } else if (today != null && today) {
                return new ResponseEntity<>(opportunityService.findAllCreatedToday(loggedAccountEmail, search, pageable), HttpStatus.OK);
            } else if (viewed != null && viewed) {
                return new ResponseEntity<>(opportunityService.findAllRecentlyViewed(loggedAccountEmail, search, pageable), HttpStatus.OK);
            } else if (edited != null && edited) {
                return new ResponseEntity<>(opportunityService.findAllRecentlyEdited(loggedAccountEmail, search, pageable), HttpStatus.OK);
            } else if (forApproving != null && forApproving) {
                return new ResponseEntity<>(opportunityService.findAllForApproving(loggedAccountEmail, search, pageable), HttpStatus.OK);
            } else if (finishResult != null) {
                return new ResponseEntity<>(opportunityService.findAllClosed(loggedAccountEmail, search, pageable, finishResult), HttpStatus.OK);
            } else if (StringUtils.isNotBlank(customer)){
                return new ResponseEntity<>(opportunityService.findAllCustomer(search, pageable, customer), HttpStatus.OK);
            } else if (StringUtils.isNotBlank(contact)){
                return new ResponseEntity<>(opportunityService.findAllContact(search, pageable, contact), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(opportunityService.findAll(loggedAccountEmail, search, pageable), HttpStatus.OK);
            }
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<OpportunitySearchVariantsDtoOut>> getSearchVariants(String loggedAccountEmail, @NotNull @Valid String toSearch) {
        try {
            return new ResponseEntity<>(opportunityService.findAllSearchVariants(loggedAccountEmail, toSearch), HttpStatus.OK);
        } catch (BadRequestException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<Page> getOpportunitiesForMobile(String loggedAccountEmail, @Valid Pageable pageable, @Valid Boolean today, @Valid Boolean edited, @Valid Boolean viewed, @Valid String search) {
        try {
            if (today != null && today) {
                return new ResponseEntity<>(opportunityService.findAllCreatedTodayForMobile(loggedAccountEmail, pageable, search), HttpStatus.OK);
            } else if (edited != null && edited) {
                return new ResponseEntity<>(opportunityService.findAllRecentlyEditedForMobile(loggedAccountEmail, pageable, search), HttpStatus.OK);
            } else if (viewed != null && viewed) {
                return new ResponseEntity<>(opportunityService.findAllRecentlyViewedForMobile(loggedAccountEmail, pageable, search), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(opportunityService.findAllForMobile(loggedAccountEmail, pageable, search), HttpStatus.OK);
            }
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<MobileOpportunityListDto>> getOpportunitiesForMobileByConnectedObject(String loggedAccountEmail, CrmObjectType objectType, Long objectId) {
        try {
            return new ResponseEntity<>(opportunityService.getOpportunitiesForCrmObject(loggedAccountEmail, objectId, objectType), HttpStatus.OK);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "createOpportunity", actionFrontendId = "OpportunityCreateComponent.save")
    public ResponseEntity<OpportunitySaveDTO> createOpportunity(String loggedAccountEmail, @Valid OpportunitySaveDTO opportunitySaveDTO) {
        try {
            return new ResponseEntity<>(opportunityService.createOpportunity(opportunitySaveDTO, loggedAccountEmail), HttpStatus.CREATED);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(actionFrontendId = "OpportunityShared.delete")
    public ResponseEntity<Void> deleteOpportunities(@Valid List<Long> ids) {
        try {
            opportunityService.deleteOpportunities(ids);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (BadRequestException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(actionFrontendId = "OpportunityShared.delete")
    public ResponseEntity<Void> deleteOpportunity(Long id) {
        try {
            opportunityService.deleteOpportunity(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (BadRequestException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "createOpportunity", actionFrontendId = "OpportunityShared.copy")
    public ResponseEntity<OpportunitySaveDTO> copyOpportunity(String loggedAccountEmail, Long opportunityId) {
        try {
            return new ResponseEntity<>(opportunityService.copyOpportunity(loggedAccountEmail, opportunityId), HttpStatus.CREATED);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @Override
    @AuthController(name = "accessDenied", controllerCase = {
            @AuthControllerCase(type = "slider", name = "getOpportunityForEdit"),
            @AuthControllerCase(type = "detailsCard", name = "getOpportunityForEditForDetailsCard")
    })
    public ResponseEntity<OpportunitySaveDTO> getOpportunityForEdit(Long id) {
        try {
            return new ResponseEntity<>(opportunityService.getOpportunityForEdit(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (BadRequestException e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "accessDenied", controllerCase = {
            @AuthControllerCase(type = "slider", name = "modifyOpportunity", actionFrontendId = "OpportunityShared.edit"),
            @AuthControllerCase(type = "detailsCard", name = "modifyOpportunityFromDetailsCard", actionFrontendId = "OpportunityShared.edit")
    })
    public ResponseEntity<OpportunitySaveDTO> modifyOpportunity(String loggedAccountEmail, Long id, @Valid OpportunitySaveDTO opportunitySaveDTO, String authControllerType) {
        try {
            return new ResponseEntity<>(opportunityService.modifyOpportunity(loggedAccountEmail, id, opportunitySaveDTO, authControllerType), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (PermissionDeniedException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    public ResponseEntity<List<CustomCrmObject>> getOpportunitiesAsCustomCrmObject(@Valid List<Long> ids) {
        return new ResponseEntity<>(opportunityService.findByIds(ids)
                .stream()
                .map(e -> new CustomCrmObject(e.getId(), CrmObjectType.opportunity, Strings.nullToEmpty(e.getName()), e.getCreated(),
                        e.getCustomer() != null ? e.getCustomer().getName() : null))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getOpportunitiesAsCrmObject(@Valid List<Long> ids) {
        return new ResponseEntity<>(opportunityService.findByIds(ids)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.opportunity, Strings.nullToEmpty(e.getName()), e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getOpportunitiesForMainSearch(String loggedAccountEmail, @NotNull @Valid String term) {
        try {
            return new ResponseEntity<>(opportunityService.findAllAvailableForUserAndContaining(loggedAccountEmail, term)
                    .stream()
                    .map(e -> new CrmObject(e.getId(), CrmObjectType.opportunity, Strings.nullToEmpty(e.getName()), e.getCreated()))
                    .collect(Collectors.toList()),
                    HttpStatus.OK);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<CrmObject>> getOpportunitiesForSearch(@NotNull @Valid String term) {
        try {
            return new ResponseEntity<>(opportunityService.findAllContaining(term)
                    .stream()
                    .map(e -> new CrmObject(e.getId(), CrmObjectType.opportunity, Strings.nullToEmpty(e.getName()), e.getCreated()))
                    .collect(Collectors.toList()),
                    HttpStatus.OK);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<Long>> getAllOpportunityWithAccess(String loggedAccountEmail) {
        try {
            return new ResponseEntity<>(this.opportunityService.getAllOpportunityWithAccess(loggedAccountEmail), HttpStatus.OK);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "getOpportunity")
    public ResponseEntity<OpportunityDetailsDTO> getOpportunity(Long id, String loggedAccountEmail) {
        try {
            return new ResponseEntity<>(opportunityService.getById(loggedAccountEmail, id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (PermissionDeniedException e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    public ResponseEntity<Void> finishOpportunity(String loggedAccountEmail, Long id, @Valid OpportunityFinishDto opportunityFinishDto) {
        try {
            opportunityService.finishOpportunity(loggedAccountEmail, id, opportunityFinishDto);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (BadRequestException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Override
    public ResponseEntity<Void> finishOpportunities(String loggedAccountEmail, @Valid List<Long> ids, @Valid OpportunityFinishDto opportunityFinishDto) {
        opportunityService.finishOpportunity(loggedAccountEmail, ids, opportunityFinishDto);
        return ResponseEntity.ok().build();
    }
}
