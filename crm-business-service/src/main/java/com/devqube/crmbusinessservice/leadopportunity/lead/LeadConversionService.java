package com.devqube.crmbusinessservice.leadopportunity.lead;

import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.customer.address.Address;
import com.devqube.crmbusinessservice.customer.contact.ContactCustomerRelationService;
import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.whitelist.WhitelistService;
import com.devqube.crmbusinessservice.dictionary.Word;
import com.devqube.crmbusinessservice.dictionary.WordRepository;
import com.devqube.crmbusinessservice.leadopportunity.AccountsService;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.BPMNUtil;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.CanConvertResponseDto;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ConvertedOpportunityDto;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadConversionDto;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadConversionResultDto;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineHelperService;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineProxyService;
import com.devqube.crmshared.access.model.PermissionDto;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.CrmObjectType;
import com.google.common.base.Strings;
import org.flowable.bpmn.model.FlowElement;
import org.flowable.bpmn.model.FormProperty;
import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.form.TaskFormData;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LeadConversionService {


    private final LeadRepository leadRepository;
    private final ProcessEngineHelperService processEngineHelperService;
    private final CustomerRepository customerRepository;
    private final ContactRepository contactRepository;
    private final UserService userService;
    private final OpportunityRepository opportunityRepository;
    private final AssociationClient associationClient;
    private final AccessService accessService;
    private final ContactCustomerRelationService contactCustomerRelationService;
    private final AccountsService accountsService;
    private final ProcessEngineProxyService processEngineProxyService;
    private final WordRepository wordRepository;
    private final WhitelistService whitelistService;

    public LeadConversionService(LeadRepository leadRepository,
                                 CustomerRepository customerRepository,
                                 ContactRepository contactRepository,
                                 OpportunityRepository opportunityRepository,
                                 UserService userService,
                                 ProcessEngineHelperService processEngineHelperService,
                                 AssociationClient associationClient, AccessService accessService,
                                 ContactCustomerRelationService contactCustomerRelationService,
                                 AccountsService accountsService, ProcessEngineProxyService processEngineProxyService,
                                 WordRepository wordRepository, WhitelistService whitelistService) {
        this.leadRepository = leadRepository;
        this.processEngineHelperService = processEngineHelperService;
        this.customerRepository = customerRepository;
        this.contactRepository = contactRepository;
        this.userService = userService;
        this.opportunityRepository = opportunityRepository;
        this.associationClient = associationClient;
        this.accessService = accessService;
        this.contactCustomerRelationService = contactCustomerRelationService;
        this.accountsService = accountsService;
        this.processEngineProxyService = processEngineProxyService;
        this.wordRepository = wordRepository;
        this.whitelistService = whitelistService;
    }

    @Transactional
    public ConvertedOpportunityDto convertToOpportunity(String email, Long leadId, LeadConversionDto dto) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Lead lead = leadRepository.findById(leadId).orElseThrow(EntityNotFoundException::new);
        CanConvertResponseDto canConvertResponseDto = canConvertToOpportunity(email, leadId);
        if (!canConvertResponseDto.getCanConvert()) {
            throw new BadRequestException();
        }
        finishAllTasks(lead.getProcessInstanceId());
        Long userId = userService.getUserId(email);
        Customer customer = findCustomerOrCreate(dto, lead, userId);
        Contact contact = findContactOrCreate(dto, lead, userId, customer);
        Opportunity opportunity = null;
        if (!dto.getNoOpportunity()) {
            opportunity = creatOpportunity(dto, lead, userId, customer, contact);
        }
        markLeadAsConverted(lead, userId, customer, contact, opportunity);
        copyAssociationsFromLead(lead, contact, customer, opportunity);

        // nip is already verified in findCustomerOrCreate method
        if (customer.getNip() != null) {
            if (!dto.getIsExistingCustomer() && customer.getNip() != null) {
                whitelistService.getWhitelistStatus(customer.getNip());
            } else {
                whitelistService.refreshWhitelistStatus(customer.getNip());
            }
        }

        return createOpportunityConversionResponse(contact, customer, opportunity);
    }

    public CanConvertResponseDto canConvertToOpportunity(String email, Long leadId) throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        Lead lead = leadRepository.findById(leadId).orElseThrow(EntityNotFoundException::new);
        verifyIfLeadNotConvertedToOpportunity(lead);
        verifyIfUserHasPermissionAndThrowIfNot(email, lead);
        CanConvertResponseDto result = new CanConvertResponseDto();

        ProcessInstance processInstance = processEngineHelperService.getProcessInstance(lead.getProcessInstanceId());
        Task activeTask = processEngineHelperService.getActiveTask(lead.getProcessInstanceId());
        List<UserTask> listOfTasks = processEngineHelperService.getListOfTasks(processInstance)
                .stream().filter(c -> isNextBpmTask(c, activeTask)).collect(Collectors.toList());

        List<String> tasksRequiringAcceptance = listOfTasks.stream()
                .filter(c -> Strings.emptyToNull(BPMNUtil.getAcceptanceId(c.getCandidateUsers().get(0))) != null)
                .map(FlowElement::getName).collect(Collectors.toList());
        result.setTasksRequiringAcceptance(tasksRequiringAcceptance);

        List<String> tasksRequiringFillingProperties = listOfTasks.stream()
                .filter(c -> c.getFormProperties().stream().anyMatch(FormProperty::isRequired))
                .map(FlowElement::getName).filter(c -> !c.equals(activeTask.getName()))
                .collect(Collectors.toList());
        TaskFormData taskFormData = processEngineHelperService.getTaskFormData(activeTask.getId());
        if (taskFormData.getFormProperties().stream()
                .anyMatch(c -> c.isRequired() && Strings.emptyToNull(c.getValue()) == null)) {
            tasksRequiringFillingProperties.add(activeTask.getName());
        }
        result.setTasksRequiringFillingProperties(tasksRequiringFillingProperties);

        result.setCanConvert(tasksRequiringAcceptance.size() == 0 && tasksRequiringFillingProperties.size() == 0);

        return result;
    }

    private void finishAllTasks(String processInstanceId) throws BadRequestException {
        ProcessInstance processInstance = processEngineHelperService.getProcessInstance(processInstanceId);
        Task activeTask = processEngineHelperService.getActiveTask(processInstanceId);
        long numberOfTasks = processEngineHelperService.getListOfTasks(processInstance)
                .stream().filter(c -> isNextBpmTask(c, activeTask))
                .filter(c -> Strings.emptyToNull(c.getName()) != null)
                .count();
        for (int i = 0; i < numberOfTasks; i++) {
            try {
                processEngineProxyService.completeTask(processInstanceId);
            } catch (Exception e) {
                e.printStackTrace();
                throw new BadRequestException();
            }
        }
    }

    private boolean isNextBpmTask(UserTask task, Task activeTask) {
        int taskId = getNumberFromTaskId(task.getId());
        int activeTaskId = getNumberFromTaskId(activeTask.getTaskDefinitionKey());
        return taskId >= activeTaskId;
    }

    private int getNumberFromTaskId(String taskId) {
        return Integer.parseInt(taskId.replace("task", ""));
    }

    @Transactional(rollbackFor = {BadRequestException.class})
    public Page<LeadConversionResultDto> findAllConvertedLeads(String loggedAccountEmail, Pageable pageable) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);

        Page<LeadConversionResultDto> leads = leadRepository.findAllConvertedLeads(userIds, pageable).map(this::mapToLeadConversionResultDto);
        leads.forEach(lead -> {
            if (lead.getConvertedToContactId() != null) {
                lead.setConvertedToContact(contactRepository.getOne(lead.getConvertedToContactId()));
            }
            if (lead.getConvertedToCustomerId() != null) {
                lead.setConvertedToCustomer(customerRepository.getOne(lead.getConvertedToCustomerId()));
            }
            if (lead.getConvertedToOpportunityId() != null) {
                lead.setConvertedToOpportunity(opportunityRepository.getOne(lead.getConvertedToOpportunityId()));
            }
        });
        return leads;

    }

    private void copyAssociationsFromLead(Lead lead, Contact contact, Customer customer, Opportunity opportunity) {
        AssociationDetailDto leadToContact = new AssociationDetailDto(CrmObjectType.lead, lead.getId(), CrmObjectType.contact, contact.getId());
        AssociationDetailDto leadToCustomer = new AssociationDetailDto(CrmObjectType.lead, lead.getId(), CrmObjectType.customer, customer.getId());
        List<AssociationDetailDto> associations = new ArrayList<>(Arrays.asList(leadToContact, leadToCustomer));
        if (opportunity != null) {
            AssociationDetailDto leadToOpportunity = new AssociationDetailDto(CrmObjectType.lead, lead.getId(), CrmObjectType.opportunity, opportunity.getId());
            associations.add(leadToOpportunity);
        }
        associationClient.copyAssociations(associations);
        associationClient.copyNotes(associations);
    }

    private Opportunity creatOpportunity(LeadConversionDto dto, Lead lead, Long userId, Customer customer, Contact contact) {
        Opportunity opportunity = createOpportunity(userId, contact, customer, lead, dto);
        return opportunityRepository.save(opportunity);
    }

    private Contact findContactOrCreate(LeadConversionDto dto, Lead lead, Long userId, Customer customer) throws BadRequestException, PermissionDeniedException {
        Contact contact;
        if (dto.getIsExistingContact()) {
            contact = findContact(dto.getContactId());
        } else {
            contact = createContact(userId, lead, customer, dto);
            verifyHasPermissionsToCreateContact();
            contact = contactRepository.save(contact);
            contactCustomerRelationService.saveNewLink(customer, contact);
        }
        return contact;
    }

    private Customer findCustomerOrCreate(LeadConversionDto dto, Lead lead, Long userId) throws BadRequestException, PermissionDeniedException {
        if (dto.getIsExistingCustomer()) {
            return findCustomer(dto.getCustomerId());
        } else {
            verifyIfCustomerNameAndNipIsUnique(dto);
            Customer customer = createCustomer(userId, lead, dto);
            verifyHasPermissionsToCreateCustomer();
            return customerRepository.save(customer);
        }
    }

    private void markLeadAsConverted(Lead lead, Long userId, Customer customer, Contact contact, Opportunity opportunity) {
        lead.setConvertedToOpportunity(true);
        lead.setConvertedBy(userId);
        lead.setConvertedToCustomerId(customer.getId());
        lead.setConvertedToContactId(contact.getId());
        lead.setConvertedDateTime(LocalDateTime.now());
        if (opportunity != null) {
            lead.setConvertedToOpportunityId(opportunity.getId());
        }
    }


    private Contact findContact(Long contactId) throws BadRequestException {
        return contactRepository.findById(contactId).orElseThrow(() -> new BadRequestException("contactDoesNotExistError"));
    }


    private Customer findCustomer(Long customerId) throws BadRequestException {
        return customerRepository.findById(customerId).orElseThrow(() -> new BadRequestException("customerDoesNotExistError"));
    }

    private void verifyIfLeadNotConvertedToOpportunity(Lead lead) throws BadRequestException {
        if (lead.getConvertedToOpportunity()) {
            throw new BadRequestException("Lead converted to opportunity can not be modified");
        }
    }

    private void verifyIfCustomerNameAndNipIsUnique(LeadConversionDto dto) throws BadRequestException {
        if (!(customerRepository.countByName(dto.getCustomerName()) == 0 && customerRepository.countByNip(dto.getCustomerNip()) == 0)) {
            throw new BadRequestException("nameOrNipOccupiedError");
        }
    }

    private void verifyIfUserHasPermissionAndThrowIfNot(String loggedAccountEmail, Lead byId) throws PermissionDeniedException, BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);
        if (!userIds.contains(byId.getLeadKeeperId()) && !userIds.contains(byId.getCreator())) {
            throw new PermissionDeniedException("user trying to modify lead that he hasn't permission to");
        }
    }

    private void verifyHasPermissionsToCreateContact() throws PermissionDeniedException {
        if (!accessService.hasAccess("ContactAddComponent.save", PermissionDto.State.WRITE, false)) {
            throw new PermissionDeniedException();
        }
    }

    private void verifyHasPermissionsToCreateCustomer() throws PermissionDeniedException {
        if (!accessService.hasAccess("CustomerAddComponent.save", PermissionDto.State.WRITE, false)) {
            throw new PermissionDeniedException();
        }
    }

    private Opportunity createOpportunity(Long userId, Contact contact, Customer customer, Lead lead, LeadConversionDto dto) {
        Opportunity opportunity = new Opportunity();
        ProcessInstance processInstance = processEngineHelperService.startProcessInstance(dto.getOpportunityProcessDefinitionId());
        Optional<UserTask> informationTask = processEngineHelperService.getFirstInformationTask(processInstance.getProcessInstanceId());
        informationTask.ifPresent(value -> processEngineHelperService.completeInformationTask(value, processInstance.getId()));
        opportunity.setCreator(userId);
        opportunity.setOwnerOneId(dto.getOpportunityOwner());
        opportunity.setOwnerOnePercentage(100L);
        opportunity.setProcessInstanceId(processInstance.getProcessInstanceId());
        opportunity.setProcessDefinitionId(dto.getOpportunityProcessDefinitionId());
        opportunity.setAmount(dto.getOpportunityAmount());
        opportunity.setCurrency(dto.getOpportunityCurrency());
        opportunity.setCreateDate(LocalDate.now());
        opportunity.setFinishDate(dto.getOpportunityFinishDate());
        opportunity.setName(dto.getOpportunityName());
        opportunity.setSourceOfAcquisition(lead.getSourceOfAcquisition());
        opportunity.setSubSourceOfAcquisition(lead.getSubSourceOfAcquisition());
        opportunity.setSubjectOfInterest(lead.getSubjectOfInterest());
        opportunity.setContact(contact);
        opportunity.setCustomer(customer);
        opportunity.setConvertedFromLead(true);
        return opportunity;
    }

    private Contact createContact(Long userId, Lead lead, Customer customer, LeadConversionDto dto) {
        Contact contact = new Contact();
        contact.setName(dto.getContactName());
        contact.setPosition(dto.getContactPosition());
        contact.setSurname(dto.getContactSurname());
        contact.setEmail(lead.getEmail());
        contact.setPhone(lead.getPhone());
        contact.setOwner(userId);
        contact.setCustomer(customer);
        return contact;
    }

    private Customer createCustomer(Long userId, Lead lead, LeadConversionDto leadConversionDto) {
        Customer customer = new Customer();
        customer.setNip(leadConversionDto.getCustomerNip());
        customer.setName(leadConversionDto.getCustomerName());
        customer.setOwner(userId);
        customer.setWebsite(lead.getCompany().getWwwAddress());

        if (leadConversionDto.getCustomerAddress() != null) {
            customer.setAddress(leadConversionDto.getCustomerAddress());
        } else {
            Address address = new Address();
            address.setStreet(lead.getCompany().getStreetAndNumber());
            address.setCity(lead.getCompany().getCity());
            address.setZipCode(lead.getCompany().getPostalCode());
            address.setCountry(lead.getCompany().getCountry());
            customer.setAddress(address);
        }
        return customer;
    }

    private ConvertedOpportunityDto createOpportunityConversionResponse(Contact contact, Customer customer, Opportunity opportunity) {
        ConvertedOpportunityDto dto = new ConvertedOpportunityDto();
        dto.setCustomerName(customer.getName());
        dto.setCustomerNip(customer.getNip());
        dto.setCustomerOwner(customer.getOwner());
        dto.setCustomerAddress(customer.getAddress());
        dto.setContactName(String.format("%s %s", contact.getName(), contact.getSurname()));
        dto.setContactPosition(contact.getPosition());
        dto.setContactPhone(contact.getPhone());
        dto.setContactEmail(contact.getEmail());
        if (contact.getCustomer() != null) {
            dto.setContactCustomerName(contact.getCustomer().getName());
        }
        dto.setContactOwner(contact.getOwner());
        if (opportunity != null) {
            dto.setOpportunityId(opportunity.getId());
            dto.setOpportunityName(opportunity.getName());
            dto.setOpportunityProcessName(
                    processEngineHelperService.getActiveTask(opportunity.getProcessInstanceId()).getName());
            if (opportunity.getSubjectOfInterest() != null) {
                Optional<Word> optionalWord = wordRepository.findById(opportunity.getSubjectOfInterest());
                optionalWord.ifPresent(word -> dto.setOpportunitySubjectOfInterest(word.getName()));
            }
            dto.setOpportunityAmount(opportunity.getAmount());
            dto.setOpportunityCurrency(opportunity.getCurrency());
            dto.setOpportunityOwner(opportunity.getOwnerOneId());
        }
        return dto;
    }

    private LeadConversionResultDto mapToLeadConversionResultDto(Lead lead) {
        ProcessInstance processInstance = processEngineHelperService.getProcessInstance(lead.getProcessInstanceId());
        Task task = processEngineHelperService.getActiveTask(lead.getProcessInstanceId());

        return new LeadConversionResultDto(lead, getCurrentTaskNumber(task),
                (long) processEngineHelperService.getListOfTasks(processInstance).size() - 1);
    }

    private long getCurrentTaskNumber(Task task) {
        return Long.parseLong(task.getTaskDefinitionKey().replaceAll("task", ""));
    }

}
