package com.devqube.crmbusinessservice.leadopportunity.processEngine;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.OpportunityDetailsDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.NotificationActionDto;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionDTO;
import io.swagger.annotations.*;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Validated
@Api(value = "ProcessEngine")
public interface ProcessEngineApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Accept task", nickname = "acceptTask", notes = "Method used to accept task")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "task accepted")})
    @RequestMapping(value = "/process-engine/accept-task",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Void> acceptTask(@ApiParam(value = "Notification id", required = true) @Valid @RequestBody NotificationActionDto notificationActionDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Complete task", nickname = "completeTask", notes = "Method used to go back to complete task")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "task completed"),
            @ApiResponse(code = 404, message = "no task found for this process instance"),
            @ApiResponse(code = 400, message = "not all required properties has been set")})
    @RequestMapping(value = "/process-engine/task/{processInstanceId}/complete",
            method = RequestMethod.GET)
    default ResponseEntity<Void> completeTask(@ApiParam(value = "processInstanceId", required = true) @PathVariable("processInstanceId") String processInstanceId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Accept task", nickname = "declineTask", notes = "Method used to accept task")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "task declined")})
    @RequestMapping(value = "/process-engine/decline-task",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Void> declineTask(@ApiParam(value = "Notification id", required = true) @Valid @RequestBody NotificationActionDto notificationActionDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Delete process by id", nickname = "deleteProcessDefinition", notes = "Method used to delete process definitions by id")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "deleted successfully"),
            @ApiResponse(code = 400, message = "there are leads created based on this process definition"),
            @ApiResponse(code = 404, message = "process not found")})
    @RequestMapping(value = "/process-engine/repository/process-definitions/{id}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteProcessDefinition(@ApiParam(value = "process definition id", required = true) @PathVariable("id") String id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Deploy new process definition", nickname = "deployProcessDefinition", notes = "Method used to deploy new process definition", response = com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Process definition successfully uploaded", response = com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionDTO.class),
            @ApiResponse(code = 400, message = "Process definition is not correct")})
    @RequestMapping(value = "/process-engine/repository/deployments",
            produces = {"application/json"},
            consumes = {"multipart/form-data"},
            method = RequestMethod.POST)
    default ResponseEntity<com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionDTO> deployProcessDefinition(@ApiParam(value = "file detail") @Valid @RequestPart("file") MultipartFile file) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get bpmn file by deploymentId", nickname = "getBpmnFile", notes = "Method used to get bpmn file by deploymentId", response = Resource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "bpmn file found", response = Resource.class),
            @ApiResponse(code = 404, message = "bpmn file not found")})
    @RequestMapping(value = "/process-engine/repository/process-definitions/{processDefinitionId}/bpmn",
            produces = {"*/*"},
            method = RequestMethod.GET)
    default ResponseEntity<Resource> getBpmnFile(@ApiParam(value = "processDefinitionId", required = true) @PathVariable("processDefinitionId") String processDefinitionId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get process definitions", nickname = "getProcessDefinitions", notes = "Method used to get process definitions", response = com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionDTO.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "previous task found", response = com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionDTO.class, responseContainer = "List")})
    @RequestMapping(value = "/process-engine/repository/process-definitions/{type}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<ProcessDefinitionDTO>> getProcessDefinitions(@ApiParam(value = "process definition type", required = true) @PathVariable("type") String type) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
    @ApiOperation(value = "Get process definition", nickname = "getProcessDefinition",
            notes = "Method used to get process definition by processDefinitionId", response = ProcessDefinitionDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "process definition was found", response = ProcessDefinitionDTO.class),
            @ApiResponse(code = 404, message = "bpmn file not found")})
    @RequestMapping(value = "/process-engine/repository/process-definition/{processDefinitionId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<ProcessDefinitionDTO> getProcessDefinition(
            @ApiParam(value = "processDefinitionId", required = true) @PathVariable String processDefinitionId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
    @ApiOperation(value = "Get all opportunities related with process definitions", nickname = "getProcessDefinitionOpportunities",
            notes = "Get all opportunities related with process definitions. " +
                    "Only those opportunities are shown that are available to the logged user.",
            response = OpportunityDetailsDTO.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all opportunities were successfully fetched by processDefinitionId",
                    response = OpportunityDetailsDTO.class, responseContainer = "List")})
    @RequestMapping(value = "/process-engine/process-definitions/{processDefinitionId}/opportunities",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<OpportunityDetailsDTO>> getProcessDefinitionOpportunities(
            @ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable,
            @ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
            @ApiParam(value = "processDefinitionId", required = true) @PathVariable String processDefinitionId,
            @ApiParam(value = "taskDefinitionKey", required = false) @Valid @RequestParam(value = "taskDefinitionKey", required = false) String taskDefinitionKey,
            @ApiParam(value = "includeOldProcessVersionResult", required = false) @Valid @RequestParam(value = "includeOldProcessVersionResult", required = false) Boolean includeOldProcessVersionResult) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get process definitions for list", nickname = "getProcessDefinitionsListDTO", notes = "Method used to get process definitions for list", response = com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionListDTO.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "previous task found", response = com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionListDTO.class, responseContainer = "List")})
    @RequestMapping(value = "/process-engine/repository/process-definitions/list/{type}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionListDTO>> getProcessDefinitionsListDTO(@ApiParam(value = "process definition type", required = true) @PathVariable("type") String type) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get task for readonly", nickname = "getTaskReadOnlyView", notes = "Method used to get task for readonly", response = com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadDetailsDTO.Task.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "previous task found", response = com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadDetailsDTO.Task.class),
            @ApiResponse(code = 404, message = "previous task not found")})
    @RequestMapping(value = "/process-engine/process-instance/{processInstanceId}/task/{taskDefinitionKey}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadDetailsDTO.Task> getTaskReadOnlyView(@ApiParam(value = "processInstanceId", required = true) @PathVariable("processInstanceId") String processInstanceId, @ApiParam(value = "taskDefinitionKey", required = true) @PathVariable("taskDefinitionKey") String taskDefinitionKey, @ApiParam(value = "taskDefinitionKey") @Valid @RequestParam(value = "futureTask", required = false) Boolean futureTask) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Go back to previous task", nickname = "goBackToPreviousTask", notes = "Method used to go back to previous task")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "went back to previous task"),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/process-engine/task/{processInstanceId}/go-back/{numberOfSteps}",
            method = RequestMethod.GET)
    default ResponseEntity<Void> goBackToPreviousTask(@ApiParam(value = "processInstanceId", required = true) @PathVariable("processInstanceId") String processInstanceId, @ApiParam(value = "numberOfSteps", required = true) @PathVariable("numberOfSteps") Integer numberOfSteps) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Post form properties", nickname = "postFormProperties", notes = "Method used to post form properties")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "ok")})
    @RequestMapping(value = "/process-engine/process-instance/{id}/form",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Void> postFormProperties(@ApiParam(value = "process' instance's id", required = true) @PathVariable("id") String processInstanceId, @ApiParam(value = "property map", required = true) @Valid @RequestBody Map<String, Object> requestBody) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}

