package com.devqube.crmbusinessservice.leadopportunity.kanban.web.dto;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.OpportunityPriority;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.ObjectMoveStepDto;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.OpportunityKanbanDTO;
import com.devqube.crmshared.task.TaskSummaryDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.flowable.task.api.Task;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KanbanOpportunity {

    private Long id;
    private String name;
    private Long customerId;
    private String customerName;
    private String customerAvatar;
    private Long contactId;
    private Double amount;
    private String currency;
    private Double amountInDefaultCurrency;
    private String processInstanceId;
    private LocalDate createDate;
    private LocalDate finishDate;
    private OpportunityPriority priority;
    private Opportunity.FinishResult finishResult;
    private Long duration;
    private Long planedActivity;
    private Long oldActivity;
    private Long userTaskDelayTime;
    private List<Owner> owners;
    private TaskSummaryDto taskSummaryDto;
    private ObjectMoveStepDto canBeMoveForward;
    private ObjectMoveStepDto canBeMoveBackward;


    public KanbanOpportunity(OpportunityKanbanDTO opportunityKanbanDTO, Task task, List<Owner> owners) {
        this.id = opportunityKanbanDTO.getId();
        this.name = opportunityKanbanDTO.getName();
        this.customerId = opportunityKanbanDTO.getCustomerId();
        this.customerName = opportunityKanbanDTO.getCustomerName();
        this.customerAvatar = opportunityKanbanDTO.getCustomerAvatar();
        this.contactId = opportunityKanbanDTO.getContactId();
        this.amount = opportunityKanbanDTO.getAmount();
        this.currency = opportunityKanbanDTO.getCurrency();
        this.amountInDefaultCurrency = opportunityKanbanDTO.getAmountInDefaultCurrency();
        this.processInstanceId = task.getProcessInstanceId();
        this.createDate = opportunityKanbanDTO.getCreateDate();
        this.finishDate = opportunityKanbanDTO.getFinishDate();
        this.priority = opportunityKanbanDTO.getPriority();
        this.finishResult = opportunityKanbanDTO.getFinishResult();
        this.userTaskDelayTime = Duration.between(opportunityKanbanDTO.getChangeUserTaskDate(), LocalDateTime.now()).toDays() - opportunityKanbanDTO.getDaysToTaskComplete();
        this.duration = Duration.between(createDate.atStartOfDay(), LocalDateTime.now()).toDays();
        this.owners = owners;
        this.canBeMoveForward = opportunityKanbanDTO.getCanBeMoveForward();
        this.canBeMoveBackward = opportunityKanbanDTO.getCanBeMoveBackward();
    }
}
