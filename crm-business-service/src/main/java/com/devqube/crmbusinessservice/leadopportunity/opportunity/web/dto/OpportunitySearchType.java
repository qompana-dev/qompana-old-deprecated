package com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto;

public enum OpportunitySearchType {

    NAME, CUSTOMER_NAME, CONTACT_NAME, SOURCE

}
