package com.devqube.crmbusinessservice.leadopportunity.bpmn.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Task
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-10-23T09:43:25.901763100+02:00[Europe/Belgrade]")

public class Task {
    @JsonProperty("name")
    private String name;

    @JsonProperty("documentation")
    private String documentation;

    @JsonProperty("acceptanceUser")
    private String acceptanceUser;

    @JsonProperty("acceptanceGroup")
    private String acceptanceGroup;

    @JsonProperty("notificationUser")
    private String notificationUser;

    @JsonProperty("notificationGroup")
    private String notificationGroup;

    @JsonProperty("salesOpportunity")
    private BigDecimal salesOpportunity;

    @JsonProperty("properties")
    @Valid
    private List<Property> properties = null;

    @JsonProperty("isInformationTask")
    private Boolean isInformationTask;

    @JsonProperty("daysToComplete")
    private Long daysToComplete;

    public Task name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     */
    @ApiModelProperty(value = "")


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Task documentation(String documentation) {
        this.documentation = documentation;
        return this;
    }

    /**
     * Get documentation
     *
     * @return documentation
     */
    @ApiModelProperty(value = "")


    public String getDocumentation() {
        return documentation;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    public Task acceptanceUser(String acceptanceUser) {
        this.acceptanceUser = acceptanceUser;
        return this;
    }

    /**
     * Get acceptanceUser
     *
     * @return acceptanceUser
     */
    @ApiModelProperty(value = "")


    public String getAcceptanceUser() {
        return acceptanceUser;
    }

    public void setAcceptanceUser(String acceptanceUser) {
        this.acceptanceUser = acceptanceUser;
    }

    public Task acceptanceGroup(String acceptanceGroup) {
        this.acceptanceGroup = acceptanceGroup;
        return this;
    }

    /**
     * Get acceptanceGroup
     *
     * @return acceptanceGroup
     */
    @ApiModelProperty(value = "")


    public String getAcceptanceGroup() {
        return acceptanceGroup;
    }

    public void setAcceptanceGroup(String acceptanceGroup) {
        this.acceptanceGroup = acceptanceGroup;
    }

    public Task notificationUser(String notificationUser) {
        this.notificationUser = notificationUser;
        return this;
    }

    /**
     * Get notificationUser
     *
     * @return notificationUser
     */
    @ApiModelProperty(value = "")


    public String getNotificationUser() {
        return notificationUser;
    }

    public void setNotificationUser(String notificationUser) {
        this.notificationUser = notificationUser;
    }

    public Task notificationGroup(String notificationGroup) {
        this.notificationGroup = notificationGroup;
        return this;
    }

    /**
     * Get notificationGroup
     *
     * @return notificationGroup
     */
    @ApiModelProperty(value = "")


    public String getNotificationGroup() {
        return notificationGroup;
    }

    public void setNotificationGroup(String notificationGroup) {
        this.notificationGroup = notificationGroup;
    }

    public Task salesOpportunity(BigDecimal salesOpportunity) {
        this.salesOpportunity = salesOpportunity;
        return this;
    }

    /**
     * Get salesOpportunity
     *
     * @return salesOpportunity
     */
    @ApiModelProperty(value = "")

    @Valid

    public BigDecimal getSalesOpportunity() {
        return salesOpportunity;
    }

    public void setSalesOpportunity(BigDecimal salesOpportunity) {
        this.salesOpportunity = salesOpportunity;
    }

    public Task properties(List<Property> properties) {
        this.properties = properties;
        return this;
    }

    public Task addPropertiesItem(Property propertiesItem) {
        if (this.properties == null) {
            this.properties = new ArrayList<>();
        }
        this.properties.add(propertiesItem);
        return this;
    }

    /**
     * Get properties
     *
     * @return properties
     */
    @ApiModelProperty(value = "")

    @Valid

    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }


    /**
     * Get disabled - Shows whether the template can be edited
     *
     * @return disabled
     */
    @ApiModelProperty(value = "")

    public Boolean getIsInformationTask() {
        return isInformationTask;
    }

    /**
     * Get days to complete task
     *
     * @return properties
     */
    @ApiModelProperty(value = "")

    public Long getDaysToComplete() {
        return daysToComplete;
    }

    public void setIsInformationTask(Boolean isInformationTask) {
        this.isInformationTask = isInformationTask;
    }

    public void setDaysToComplete(Long daysToComplete) {
        this.daysToComplete = daysToComplete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Task task = (Task) o;
        return Objects.equals(this.name, task.name) &&
                Objects.equals(this.documentation, task.documentation) &&
                Objects.equals(this.acceptanceUser, task.acceptanceUser) &&
                Objects.equals(this.acceptanceGroup, task.acceptanceGroup) &&
                Objects.equals(this.notificationUser, task.notificationUser) &&
                Objects.equals(this.notificationGroup, task.notificationGroup) &&
                Objects.equals(this.salesOpportunity, task.salesOpportunity) &&
                Objects.equals(this.properties, task.properties) &&
                Objects.equals(this.isInformationTask, task.isInformationTask) &&
                Objects.equals(this.daysToComplete, task.daysToComplete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, documentation, acceptanceUser, acceptanceGroup, notificationUser, notificationGroup,
                salesOpportunity, properties, isInformationTask, daysToComplete);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Task {\n");

        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    documentation: ").append(toIndentedString(documentation)).append("\n");
        sb.append("    acceptanceUser: ").append(toIndentedString(acceptanceUser)).append("\n");
        sb.append("    acceptanceGroup: ").append(toIndentedString(acceptanceGroup)).append("\n");
        sb.append("    notificationUser: ").append(toIndentedString(notificationUser)).append("\n");
        sb.append("    notificationGroup: ").append(toIndentedString(notificationGroup)).append("\n");
        sb.append("    salesOpportunity: ").append(toIndentedString(salesOpportunity)).append("\n");
        sb.append("    properties: ").append(toIndentedString(properties)).append("\n");
        sb.append("    isInformationTask: ").append(toIndentedString(isInformationTask)).append("\n");
        sb.append("    daysToComplete: ").append(toIndentedString(daysToComplete)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

