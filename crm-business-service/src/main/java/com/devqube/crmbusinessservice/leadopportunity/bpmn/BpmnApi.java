/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech) (3.3.4).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
package com.devqube.crmbusinessservice.leadopportunity.bpmn;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Process;
import io.swagger.annotations.*;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Optional;

@Validated
@Api(value = "Bpmn")
public interface BpmnApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Generate bpmn file", nickname = "generateBPMNFile", notes = "Method used to generate BPMN file", response = Resource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "BPMN file created", response = Resource.class),
            @ApiResponse(code = 400, message = "Bad request")})
    @RequestMapping(value = "/bpmn/generate",
            produces = {"*/*"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Resource> generateBPMNFile(@ApiParam(value = "process data", required = true) @Valid @RequestBody Process process) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Parse file", nickname = "parseBPMNFile", notes = "Method used to parse BPMN file", response = Process.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File successfully parsed", response = Process.class),
            @ApiResponse(code = 400, message = "Bad request")})
    @RequestMapping(value = "/bpmn/parse",
            produces = {"application/json"},
            consumes = {"multipart/form-data"},
            method = RequestMethod.POST)
    default ResponseEntity<Process> parseBPMNFile(@ApiParam(value = "file detail") @Valid @RequestPart("file") MultipartFile file) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"bgColor\" : \"bgColor\",  \"availableType\" : \"availableType\",  \"documentation\" : \"documentation\",  \"name\" : \"name\",  \"id\" : \"id\",  \"department\" : \"department\",  \"textColor\" : \"textColor\",  \"tasks\" : [ {    \"documentation\" : \"documentation\",    \"acceptanceGroup\" : \"acceptanceGroup\",    \"name\" : \"name\",    \"notificationUser\" : \"notificationUser\",    \"salesOpportunity\" : 0.80082819046101150206595775671303272247314453125,    \"properties\" : [ {      \"possibleValues\" : [ {        \"name\" : \"name\",        \"id\" : \"id\"      }, {        \"name\" : \"name\",        \"id\" : \"id\"      } ],      \"id\" : \"id\",      \"type\" : \"STRING\",      \"value\" : \"value\",      \"required\" : true    }, {      \"possibleValues\" : [ {        \"name\" : \"name\",        \"id\" : \"id\"      }, {        \"name\" : \"name\",        \"id\" : \"id\"      } ],      \"id\" : \"id\",      \"type\" : \"STRING\",      \"value\" : \"value\",      \"required\" : true    } ],    \"acceptanceUser\" : \"acceptanceUser\",    \"notificationGroup\" : \"notificationGroup\"  }, {    \"documentation\" : \"documentation\",    \"acceptanceGroup\" : \"acceptanceGroup\",    \"name\" : \"name\",    \"notificationUser\" : \"notificationUser\",    \"salesOpportunity\" : 0.80082819046101150206595775671303272247314453125,    \"properties\" : [ {      \"possibleValues\" : [ {        \"name\" : \"name\",        \"id\" : \"id\"      }, {        \"name\" : \"name\",        \"id\" : \"id\"      } ],      \"id\" : \"id\",      \"type\" : \"STRING\",      \"value\" : \"value\",      \"required\" : true    }, {      \"possibleValues\" : [ {        \"name\" : \"name\",        \"id\" : \"id\"      }, {        \"name\" : \"name\",        \"id\" : \"id\"      } ],      \"id\" : \"id\",      \"type\" : \"STRING\",      \"value\" : \"value\",      \"required\" : true    } ],    \"acceptanceUser\" : \"acceptanceUser\",    \"notificationGroup\" : \"notificationGroup\"  } ],  \"objectType\" : \"LEAD\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
