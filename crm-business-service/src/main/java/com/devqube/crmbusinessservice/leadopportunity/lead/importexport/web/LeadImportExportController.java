package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.web;

import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.ExportLeadService;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.ImportLeadService;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto.EmailValidationGroup;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto.LeadImportDtoIn;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto.ParsedLeadDtoOut;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ParsedLead;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

import static com.devqube.crmbusinessservice.leadopportunity.lead.importexport.web.LeadImportExportDtoMapper.toCommonLeadParams;
import static com.devqube.crmbusinessservice.leadopportunity.lead.importexport.web.LeadImportExportDtoMapper.toParsedLead;

@RestController
@Validated
@RequiredArgsConstructor
public class LeadImportExportController implements LeadImportExportApi {

    private final ImportLeadService importLeadService;
    private final ExportLeadService exportLeadService;

    @Override
    @Validated(EmailValidationGroup.class)
    @AuthController(actionFrontendId = "LeadShared.import")
    public ResponseEntity<Void> importLeads(String loggedAccountEmail, @Valid LeadImportDtoIn leadImportDto) {
        try {
            importLeadService.importLeads(toParsedLead(leadImportDto.getLeads()),
                    toCommonLeadParams(leadImportDto.getSettings(), loggedAccountEmail),
                    leadImportDto.getSettings().getSkipIncorrect(),
                    leadImportDto.getSettings().getMergeDuplicate());
        } catch (BadRequestException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }

    @Override
    @AuthController(actionFrontendId = "LeadShared.import")
    public ResponseEntity<List<ParsedLeadDtoOut>> parseImportFile(@Valid MultipartFile file) {
        try {
            String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
            List<ParsedLead> parsedLeadList = importLeadService.parseImportFile(file.getInputStream(), fileExtension);
            return new ResponseEntity<>(LeadImportExportDtoMapper.toParsedLeadDtoOut(parsedLeadList), HttpStatus.OK);
        } catch (BadRequestException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
//        } catch (ParseException e) {
//            return ResponseEntity.status(423).body(e.getErrorList());
//        }
        }
    }

    @Override
    @AuthController(actionFrontendId = "LeadShared.export")
    public ResponseEntity<Void> exportLeads(HttpServletResponse response, String loggedAccountEmail) {
        try {
            exportLeadService.exportLeads(response, loggedAccountEmail);
            return ResponseEntity.ok().build();
        } catch (BadRequestException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
