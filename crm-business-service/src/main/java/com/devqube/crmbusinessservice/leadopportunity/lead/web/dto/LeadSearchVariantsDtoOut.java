package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class LeadSearchVariantsDtoOut {

    private LeadSearchType type;
    private List<String> variants = new ArrayList<>();

}
