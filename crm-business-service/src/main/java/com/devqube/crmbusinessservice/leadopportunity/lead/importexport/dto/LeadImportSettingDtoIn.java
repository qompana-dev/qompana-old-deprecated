package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;

@Data
@Accessors(chain = true)
public class LeadImportSettingDtoIn {

    @NonNull
    private Boolean skipIncorrect;

    @NonNull
    private Boolean mergeDuplicate;

    @NotEmpty
    private String processDefinitionId;

    @NonNull
    private Long leadKeeperId;

    private Long sourceOfAcquisitionId;

    private Long subSourceOfAcquisitionId;

}
