package com.devqube.crmbusinessservice.leadopportunity.bpmn.model;

public enum AcceptanceStatus {
    DECLINED, ACCEPTED, NEEDS_ACCEPTATION
}
