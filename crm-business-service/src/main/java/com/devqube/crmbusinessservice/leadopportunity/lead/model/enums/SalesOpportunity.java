package com.devqube.crmbusinessservice.leadopportunity.lead.model.enums;

public enum SalesOpportunity {
    NOT_SPECIFIED, HIGH, MEDIUM, LOW
}
