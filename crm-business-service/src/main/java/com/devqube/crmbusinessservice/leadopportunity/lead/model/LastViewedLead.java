package com.devqube.crmbusinessservice.leadopportunity.lead.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class LastViewedLead {
    @Id
    @SequenceGenerator(name = "last_viewed_lead_seq", sequenceName = "last_viewed_lead_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "last_viewed_lead_seq")
    @EqualsAndHashCode.Include
    private Long id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "last_viewed_lead_id")
    @NotNull
    private Lead lead;

    @Column(name = "user_id")
    @NotNull
    private Long userId;

    @Column(name = "last_viewed")
    @NotNull
    private LocalDateTime lastViewed;
}
