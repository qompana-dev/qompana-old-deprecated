package com.devqube.crmbusinessservice.leadopportunity.lead.importexport;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CommonLeadParams {

    private String processDefinitionId;
    private Long leadKeeperId;
    private Long sourceOfAcquisitionId;
    private Long subSourceOfAcquisitionId;
    private String loggedAccountEmail;

}
