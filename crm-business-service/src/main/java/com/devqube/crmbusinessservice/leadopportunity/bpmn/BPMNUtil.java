package com.devqube.crmbusinessservice.leadopportunity.bpmn;

import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Task;
import com.google.common.base.Strings;

import java.util.List;

public class BPMNUtil {
    static String joinAcceptanceAndNotification(String acceptance, String notification) {
        return String.join(";", List.of(Strings.nullToEmpty(acceptance), Strings.nullToEmpty(notification)));
    }

    public static String getAcceptanceId(String joinedString) {
        String[] split = joinedString.split(";", -1);
        return split[0];
    }

    public static String getNotificationId(String joinedString) {
        String[] split = joinedString.split(";", -1);
        return split[1];
    }

    public static String getInformationTaskValueOrFalse(Task task) {
        if (task.getIsInformationTask() != null) {
            return task.getIsInformationTask().toString();
        } else return Boolean.FALSE.toString();
    }

    public static String getDaysToCompleteOrZero(Task task) {
        if (task.getDaysToComplete() != null) {
            return task.getDaysToComplete().toString();
        } else {
            return "0";
        }
    }

}
