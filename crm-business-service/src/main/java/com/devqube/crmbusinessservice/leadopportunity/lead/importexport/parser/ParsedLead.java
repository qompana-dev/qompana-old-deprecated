package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ParsedLead {

    private ValidatedValue firstName;
    private ValidatedValue lastName;
    private ValidatedValue email;
    private ValidatedValue phone;
    private ValidatedValue position;
    private ValidatedValue companyName;
    private ValidatedValue companyCity;

}
