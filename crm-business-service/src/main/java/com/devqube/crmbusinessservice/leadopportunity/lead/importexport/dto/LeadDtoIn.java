package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.Valid;
import javax.validation.groups.ConvertGroup;
import javax.validation.groups.Default;

@Data
@Accessors(chain = true)
public class LeadDtoIn {

    @Valid
    @ConvertGroup(from = EmailValidationGroup.class, to = Default.class)
    private ValidatedValueDtoIn firstName;

    @Valid
    @ConvertGroup(from = EmailValidationGroup.class, to = Default.class)
    private ValidatedValueDtoIn lastName;

    @Valid
    private ValidatedValueDtoIn email;

    @Valid
    @ConvertGroup(from = EmailValidationGroup.class, to = Default.class)
    private ValidatedValueDtoIn phone;

    @Valid
    @ConvertGroup(from = EmailValidationGroup.class, to = Default.class)
    private ValidatedValueDtoIn position;

    @Valid
    @ConvertGroup(from = EmailValidationGroup.class, to = Default.class)
    private ValidatedValueDtoIn companyName;

    @Valid
    @ConvertGroup(from = EmailValidationGroup.class, to = Default.class)
    private ValidatedValueDtoIn companyCity;

}
