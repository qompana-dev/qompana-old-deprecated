package com.devqube.crmbusinessservice.leadopportunity.lead.model.enums;

public enum LeadStatus {
    NOT_ELIGIBLE, NEW, DURING_RECOGNITION, UNDER_THE_CUSTODY, ELIGIBLE
}
