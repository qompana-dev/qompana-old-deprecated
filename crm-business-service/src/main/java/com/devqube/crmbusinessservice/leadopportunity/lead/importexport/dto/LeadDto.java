package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto;

import com.devqube.crmbusinessservice.leadopportunity.lead.model.LeadPriority;
import com.devqube.crmshared.importexport.ImportObjLine;
import com.univocity.parsers.annotations.Parsed;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LeadDto extends ImportObjLine {
    //lead
    @Parsed//to
    private String processDefinitionName; // find process name and convert to processInstanceId
    @Parsed
    private LeadPriority priority;
    @Parsed
    private String firstName;
    @Parsed
    private String lastName;
    @Parsed
    private String position;
    @Parsed
    private String email;
    @Parsed
    private String phone;
    @Parsed
    private String sourceOfAcquisition;
    @Parsed
    private String subSourceOfAcquisition;
    @Parsed//to
    private String leadKeeperName; // find name and convert ot leadKeeperId

    //company //to
    @Parsed
    private String companyName;
    @Parsed
    private String companyWwwAddress;
    @Parsed
    private String companyStreetAndNumber;
    @Parsed
    private String companyCity;
    @Parsed
    private String companyPostalCode;
    @Parsed
    private String companyProvince;
    @Parsed
    private String companyCountry;
    @Parsed
    private String companyNotes;
}
