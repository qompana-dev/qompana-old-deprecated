package com.devqube.crmbusinessservice.leadopportunity.opportunity;

import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.LastViewedOpportunity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LastViewedOpportunityRepository extends JpaRepository<LastViewedOpportunity, Long> {
}
