package com.devqube.crmbusinessservice.leadopportunity.processEngine.service.write;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.kafka.KafkaServiceImpl;
import com.devqube.crmbusinessservice.leadopportunity.AccountsService;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.OpportunityDetailsDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.client.NotificationClient;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.NotificationActionDto;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineHelperService;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineNotificationService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.IncorrectNotificationTypeException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.history.dto.ObjectHistoryType;
import com.devqube.crmshared.notification.NotificationMsgType;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.google.common.base.Strings;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.devqube.crmbusinessservice.leadopportunity.bpmn.model.AcceptanceStatus.*;


@Service
public class ProcessEngineForOpportunity implements ProcessEngineService {

    private final String statusAcceptance = "statusAcceptance";
    private final OpportunityRepository opportunityRepository;
    private final ProcessEngineHelperService processEngineHelperService;
    private final TaskService taskService;
    private final ProcessEngineNotificationService notificationService;
    private final UserClient userClient;
    private final NotificationClient notificationClient;
    private final RuntimeService runtimeService;
    private final AccountsService accountsService;
    private final ModelMapper modelMapper;
    private final KafkaServiceImpl kafkaService;

    public ProcessEngineForOpportunity(OpportunityRepository opportunityRepository, ProcessEngineHelperService processEngineHelperService,
                                       TaskService taskService, ProcessEngineNotificationService notificationService,
                                       UserClient userClient, NotificationClient notificationClient,
                                       RuntimeService runtimeService, AccountsService accountsService, ModelMapper modelMapper, KafkaServiceImpl kafkaService) {
        this.opportunityRepository = opportunityRepository;
        this.processEngineHelperService = processEngineHelperService;
        this.taskService = taskService;
        this.notificationService = notificationService;
        this.userClient = userClient;
        this.notificationClient = notificationClient;
        this.runtimeService = runtimeService;
        this.accountsService = accountsService;
        this.modelMapper = modelMapper;
        this.kafkaService = kafkaService;
    }

    @Override
    public void checkIfBeingAcceptedAndThrowExceptionIfYes(String id) throws EntityNotFoundException, BadRequestException {
        Opportunity lead = opportunityRepository.findAllByProcessInstanceId(id).stream().findFirst().orElseThrow(EntityNotFoundException::new);
        if (lead.getAcceptanceUserId() != null) {
            throw new BadRequestException("Task is being accepted");
        }
    }

    @Override
    public boolean completeTask(String processInstanceId) throws EntityNotFoundException, BadRequestException, KafkaSendMessageException, IncorrectNotificationTypeException {
        Opportunity opportunity = opportunityRepository.findAllByProcessInstanceId(processInstanceId).stream().findFirst().orElseThrow(EntityNotFoundException::new);
        if (opportunity.getFinishResult() != null) {
            throw new BadRequestException();
        }
        Task task = processEngineHelperService.getTaskByProcessInstanceId(processInstanceId);
        processEngineHelperService.validateTask(opportunity.getAcceptanceUserId(), task);
        Long acceptanceUserId = sendNotificationsAndReturnAcceptanceUserId(processInstanceId, opportunity);
        boolean isAutomaticallyAccepted = completeTaskOrMarkAsDuringAcceptance(opportunity, task, acceptanceUserId, processInstanceId);
        return acceptanceUserId == null || isAutomaticallyAccepted;
    }

    @Override
    public void goBackToPreviousTask(String processInstanceId, Integer numberOfSteps) throws BadRequestException {
        Opportunity opportunity = opportunityRepository.findAllByProcessInstanceId(processInstanceId).stream().findFirst().orElseThrow(BadRequestException::new);
        if (opportunity.getFinishResult() != null) {
            throw new BadRequestException();
        }
        Task task = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult();
        String previousTaskDefinitionKey = "task" + (Integer.parseInt(task.getTaskDefinitionKey().replaceAll("task", "")) - numberOfSteps);
        runtimeService
                .createChangeActivityStateBuilder()
                .processInstanceId(task.getProcessInstanceId())
                .moveActivityIdTo(task.getTaskDefinitionKey(), previousTaskDefinitionKey)
                .changeState();
        String newUserTaskName = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult().getName();
        opportunity.setActualUserTask(newUserTaskName);
        opportunity.setChangeUserTaskDate(LocalDateTime.now());
        opportunityRepository.save(opportunity);
    }

    @Override
    public void acceptTask(NotificationActionDto notificationActionDto, String processInstanceId) throws BadRequestException, EntityNotFoundException,
            KafkaSendMessageException, IncorrectNotificationTypeException {
        TaskAccepter taskAccepter = new TaskAccepter(notificationActionDto.getUserNotificationId(), processInstanceId).markAcceptanceAsFinished();
        Task task = taskAccepter.getTask();
        notificationService.sendAcceptanceNotification(taskAccepter.getUserIds(), taskAccepter.getOpportunityName(), task.getName(), notificationActionDto.getComment(),
                NotificationMsgType.OPPORTUNITY_TASK_ACCEPTED);
        notificationService.sendOnCompleteTaskNotificationIfNeeded(processInstanceId, taskAccepter.getOpportunityName(), NotificationMsgType.OPPORTUNITY_TASK_FINISHED);
        taskService.setVariableLocal(task.getId(), statusAcceptance, ACCEPTED.name());
        taskService.complete(task.getId());
        Opportunity opportunity = opportunityRepository.findAllByProcessInstanceId(processInstanceId).stream().findFirst().orElseThrow(EntityNotFoundException::new);
        String newUserTaskName = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult().getName();
        opportunity.setActualUserTask(newUserTaskName);
        opportunity.setChangeUserTaskDate(LocalDateTime.now());
        opportunityRepository.save(opportunity);
    }

    @Override
    public void declineTask(NotificationActionDto notificationActionDto, String processInstanceId) throws BadRequestException, EntityNotFoundException,
            KafkaSendMessageException, IncorrectNotificationTypeException {
        TaskAccepter taskAccepter = new TaskAccepter(notificationActionDto.getUserNotificationId(), processInstanceId).markAcceptanceAsFinished();
        notificationService.sendAcceptanceNotification(taskAccepter.getUserIds(), taskAccepter.getOpportunityName(), taskAccepter.getTask().getName(), notificationActionDto.getComment(),
                NotificationMsgType.OPPORTUNITY_TASK_DECLINED);
        taskService.setVariableLocal(taskAccepter.getTask().getId(), statusAcceptance, DECLINED.name());

    }

    private boolean completeTaskOrMarkAsDuringAcceptance(Opportunity opportunity, Task task, Long acceptanceUserId, String processInstanceId) throws KafkaSendMessageException, IncorrectNotificationTypeException {
        boolean isAutomaticallyAccepted = false;
        Long taskAcceptorUserId = userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail());
        if (acceptanceUserId == null) {
            taskService.complete(task.getId());
            String newUserTaskName = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult().getName();
            opportunity.setActualUserTask(newUserTaskName);
            opportunity.setChangeUserTaskDate(LocalDateTime.now());
            opportunityRepository.save(opportunity);
        } else if (acceptanceUserId.equals(taskAcceptorUserId)) {
            taskService.setVariableLocal(task.getId(), statusAcceptance, ACCEPTED.name());
            taskService.complete(task.getId());
            opportunity.setAcceptanceUserId(null);
            String newUserTaskName = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult().getName();
            opportunity.setActualUserTask(newUserTaskName);
            opportunity.setChangeUserTaskDate(LocalDateTime.now());
            opportunityRepository.saveAndFlush(opportunity);
            kafkaService.saveHistory(ObjectHistoryType.OPPORTUNITY, opportunity.getId().toString(), new JSONObject().put("selfAcceptance", taskAcceptorUserId).toString(), LocalDateTime.now(), getEmailFromRequest());
            String leadName = Strings.nullToEmpty(opportunity.getName());
            notificationService.sendAcceptanceNotification(new HashSet<>(Arrays.asList(taskAcceptorUserId)), leadName, task.getName(), "Akceptacja automatyczna",
                    NotificationMsgType.OPPORTUNITY_TASK_ACCEPTED);
            notificationService.sendOnCompleteTaskNotificationIfNeeded(processInstanceId, leadName, NotificationMsgType.LEAD_TASK_FINISHED);
            isAutomaticallyAccepted = true;
        } else {
            opportunity.setAcceptanceUserId(acceptanceUserId);
            taskService.setVariableLocal(task.getId(), statusAcceptance, NEEDS_ACCEPTATION.name());
            opportunityRepository.save(opportunity);
        }
        return isAutomaticallyAccepted;
    }

    private Long sendNotificationsAndReturnAcceptanceUserId(String processInstanceId, Opportunity opportunity) throws KafkaSendMessageException {
        Long acceptanceUserId = notificationService.sendTaskConfirmationNotificationIfNeeded(processInstanceId,
                opportunity.getName(), opportunity.getId(), NotificationMsgType.OPPORTUNITY_TASK_ACCEPTANCE);
        if (acceptanceUserId == null) {
            notificationService.sendOnCompleteTaskNotificationIfNeeded(processInstanceId, opportunity.getName(), NotificationMsgType.OPPORTUNITY_TASK_FINISHED);
        }
        return acceptanceUserId;
    }

    public List<OpportunityDetailsDTO> findAllAvailableForUserAndProcessDefinitionId(Pageable pageable, String loggedAccountEmail,
                                                                                     String processDefinitionId, String taskDefinitionKey, Boolean includeOldProcessVersionResult) throws BadRequestException {
        List<Long> userIds = accountsService.getAllEmployeesBySupervisorEmail(loggedAccountEmail);

        if (includeOldProcessVersionResult != null && includeOldProcessVersionResult) {
            {
                List<String> processDefinitionIds =
                        processEngineHelperService
                                .findAllVersionDefinitionsOfProcessByProcessDefinitionId(processDefinitionId);
                List<Opportunity> opportunitiesAvailableForUser = opportunityRepository.findOpportunitiesAvailableForUser(userIds);

                return opportunitiesAvailableForUser.stream()
                        .filter(opportunity -> processDefinitionIds.contains(opportunity.getProcessDefinitionId()))
                        .map(opportunity -> {
                            OpportunityDetailsDTO dto = modelMapper.map(opportunity, OpportunityDetailsDTO.class);
                            dto.setCustomerName(opportunity.getCustomer().getName());
                            ProcessInstance processInstance = processEngineHelperService.getProcessInstance(opportunity.getProcessInstanceId());
                            processEngineHelperService.setTasksToDTO(opportunity.getProcessInstanceId(), dto, processInstance);
                            return dto;
                        }).collect(Collectors.toList());
            }
        } else {
            if (taskDefinitionKey != null) {
                List<String> processInstanceIds =
                        processEngineHelperService
                                .findAllProcessInstanceIdByTaskAndProcessDefinitionId(taskDefinitionKey, processDefinitionId);

                List<Opportunity> opportunitiesAvailableForUser = opportunityRepository.findOpportunitiesAvailableForUser(userIds);
                return opportunitiesAvailableForUser.stream()
                        .filter(opportunity -> processInstanceIds.contains(opportunity.getProcessInstanceId()))
                        .map(opportunity -> {
                            OpportunityDetailsDTO dto = modelMapper.map(opportunity, OpportunityDetailsDTO.class);
                            dto.setCustomerName(opportunity.getCustomer().getName());
                            ProcessInstance processInstance = processEngineHelperService.getProcessInstance(opportunity.getProcessInstanceId());
                            processEngineHelperService.setTasksToDTO(opportunity.getProcessInstanceId(), dto, processInstance);
                            return dto;
                        }).collect(Collectors.toList());
            } else {
                List<Opportunity> processOpportunitiesMap = opportunityRepository.findAllAvailableForUserAndProcessDefinitionId(userIds, processDefinitionId);
                return processOpportunitiesMap.stream()
                        .map(opportunity -> {
                            OpportunityDetailsDTO dto = modelMapper.map(opportunity, OpportunityDetailsDTO.class);
                            dto.setCustomerName(opportunity.getCustomer().getName());
                            ProcessInstance processInstance = processEngineHelperService.getProcessInstance(opportunity.getProcessInstanceId());
                            processEngineHelperService.setTasksToDTO(opportunity.getProcessInstanceId(), dto, processInstance);
                            return dto;
                        }).collect(Collectors.toList());
            }
        }


    }

    private class TaskAccepter {
        private Long userNotificationId;
        private String processInstanceId;
        private Task task;
        private Opportunity opportunity;

        TaskAccepter(Long userNotificationId, String processInstanceId) {
            this.userNotificationId = userNotificationId;
            this.processInstanceId = processInstanceId;
        }

        public Task getTask() {
            return task;
        }

        TaskAccepter markAcceptanceAsFinished() throws EntityNotFoundException, BadRequestException {
            task = processEngineHelperService.getTaskByProcessInstanceId(processInstanceId);
            if (task == null) {
                throw new EntityNotFoundException("Task for this processInstanceId not found");
            }

            opportunity = opportunityRepository.findAllByProcessInstanceId(processInstanceId).stream().findFirst().orElseThrow(EntityNotFoundException::new);
            if (!userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail()).equals(opportunity.getAcceptanceUserId())) {
                throw new BadRequestException("You can't accept/decline lead when you are not accepter");
            }

            opportunity.setAcceptanceUserId(null);
            opportunityRepository.save(opportunity);
            notificationClient.deleteMessage(new com.devqube.crmshared.notification.NotificationActionDto(userNotificationId));
            return this;
        }

        Set<Long> getUserIds() {
            Set<Long> userIds = new HashSet<>();
            userIds.add(opportunity.getCreator());
            if (opportunity.getOwnerOneId() != null) {
                userIds.add(opportunity.getOwnerOneId());
            }
            if (opportunity.getOwnerTwoId() != null) {
                userIds.add(opportunity.getOwnerOneId());
            }
            if (opportunity.getOwnerThreeId() != null) {
                userIds.add(opportunity.getOwnerOneId());
            }
            return userIds;
        }

        String getOpportunityName() {
            return opportunity.getName();
        }

    }

    private String getEmailFromRequest() {
        try {
            RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            return request.getHeader("Logged-Account-Email");
        } catch (Exception ignore) {
            return "";
        }
    }

}
