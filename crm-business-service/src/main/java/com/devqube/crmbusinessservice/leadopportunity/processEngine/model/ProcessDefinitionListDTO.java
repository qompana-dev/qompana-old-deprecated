package com.devqube.crmbusinessservice.leadopportunity.processEngine.model;

import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ProcessColor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.flowable.bpmn.model.FlowElement;
import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.repository.ProcessDefinition;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProcessDefinitionListDTO {
    private String id;

    private String name;

    private Long size;

    private String backgroundColor;

    private String textColor;

    private List<String> tasks;


    public ProcessDefinitionListDTO(ProcessDefinition processDefinition, List<UserTask> tasks, ProcessColor processColor) {
        this.id = processDefinition.getId();
        this.name = processDefinition.getName();
        this.size = (long) tasks.size();
        this.tasks = tasks.stream().map(FlowElement::getName).collect(Collectors.toList());
        this.backgroundColor = processColor.getBackground();
        this.textColor = processColor.getText();
    }
}
