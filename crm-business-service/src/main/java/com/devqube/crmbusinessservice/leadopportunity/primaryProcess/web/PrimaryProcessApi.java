package com.devqube.crmbusinessservice.leadopportunity.primaryProcess.web;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.leadopportunity.primaryProcess.web.dto.PrimaryProcessDto;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "PrimaryProcess")
public interface PrimaryProcessApi {
    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "set primary process", nickname = "save",
            notes = "set primary process", response = PrimaryProcessDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "primary process set successfully", response = PrimaryProcessDto.class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/primary-process/id/{id}/type/{type}",
            produces = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<PrimaryProcessDto> save(
            @ApiParam(value = "", required = true) @RequestHeader(value = "Logged-Account-Email", required = true) String loggedAccountEmail,
            @ApiParam(value = "opportunity's id", required = true) @PathVariable("id") String id,
            @ApiParam(value = "opportunity's id", required = true) @PathVariable("type") String type) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "delete record of primary process", nickname = "delete", notes = "Method used to delete record of primary process")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "deleted record of primary process successfully"),
            @ApiResponse(code = 404, message = "record of primary process not found")})
    @RequestMapping(value = "/primary-process/id/{id}/type/{type}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> delete(
            @ApiParam(value = "opportunity's id", required = true) @PathVariable("id") String id,
            @ApiParam(value = "opportunity's id", required = true) @PathVariable("type") String type) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get records of primary processes", nickname = "getPrimaryProcesses",
            notes = "Method used to get all records about primary processes",
            response = PrimaryProcessDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found primary processes",
                    response = PrimaryProcessDto.class, responseContainer = "List")})
    @RequestMapping(value = "/primary-process",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<PrimaryProcessDto>> getPrimaryProcesses() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
