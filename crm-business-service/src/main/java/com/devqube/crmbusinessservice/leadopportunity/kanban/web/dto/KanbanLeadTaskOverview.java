package com.devqube.crmbusinessservice.leadopportunity.kanban.web.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.flowable.bpmn.model.UserTask;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class KanbanLeadTaskOverview {

    @AuthField(frontendId = "LeadKanbanComponent.stepNameField", controller = "getProcessesOverview")
    private String name;

    @AuthField(frontendId = "LeadKanbanComponent.numberLeadsInTaskField", controller = "getProcessesOverview")
    private Integer includedLeadsNumber;

    public KanbanLeadTaskOverview(UserTask task, List<KanbanLead> leads) {
        this.name = task.getName();
        this.includedLeadsNumber = leads.size();
    }
}
