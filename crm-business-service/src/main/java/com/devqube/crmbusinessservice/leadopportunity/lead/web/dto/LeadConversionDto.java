package com.devqube.crmbusinessservice.leadopportunity.lead.web.dto;

import com.devqube.crmbusinessservice.customer.address.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.AssertTrue;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LeadConversionDto {
    private Long contactId;
    private String contactName;
    private String contactPosition;
    private String contactSurname;
    private Boolean isExistingContact;

    private Long customerId;
    private String customerName;
    private String customerNip;
    private Boolean isExistingCustomer;
    private Address customerAddress;

    private Boolean noOpportunity;
    private Double opportunityAmount;
    private LocalDate opportunityFinishDate;
    private String opportunityCurrency;
    private String opportunityName;
    private String opportunityProcessDefinitionId;
    private Long opportunityOwner;

    @AssertTrue(message = "One or more required fields is missing")
    public boolean isValid() {
        boolean contactValid = isExistingContact != null && (isExistingContact ? contactId != null : contactName != null && contactPosition != null && contactSurname != null);
        boolean customerValid = isExistingCustomer != null && (isExistingCustomer ? customerId != null : customerName != null && customerNip != null);
        boolean opportunityValid = noOpportunity != null && (noOpportunity || opportunityProcessDefinitionId != null && opportunityName != null);
        return contactValid && customerValid && opportunityValid;
    }
}
