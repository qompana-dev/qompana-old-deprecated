package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto;

import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ValidationResult;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@Accessors(chain = true)
public class ValidatedValueDtoIn {

    @NotNull(groups = EmailValidationGroup.class)
    private String value;

    @Valid
    @NotNull(groups = EmailValidationGroup.class)
    private Set<ValidationResult> validationResults;

}
