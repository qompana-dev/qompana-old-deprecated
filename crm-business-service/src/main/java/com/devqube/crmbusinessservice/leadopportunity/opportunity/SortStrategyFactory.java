package com.devqube.crmbusinessservice.leadopportunity.opportunity;

import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadOnListDTO;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.OpportunityListDTO;
import org.springframework.data.domain.Sort;

import java.util.Comparator;
import java.util.Objects;

class SortStrategyFactory {

    static Comparator<OpportunityListDTO> create(Sort sort) {
        Comparator<OpportunityListDTO> comparator = null;
        for (Sort.Order order: sort) {
            comparator = addComparator(comparator, order);
        }
        return comparator;
    }

    private static Comparator<OpportunityListDTO> addComparator(Comparator<OpportunityListDTO> comparator, Sort.Order order) {
        if(Objects.isNull(comparator)) {
            return getStrategy(order);
        }

        return comparator.thenComparing(getStrategy(order));
    }

    private static Comparator<OpportunityListDTO> getStrategy(Sort.Order order) {
        SortStrategy sortStrategy;

        switch (order.getProperty()) {
            case "updated":
                sortStrategy = new SortByUpdated();
                break;
            case "name":
                sortStrategy = new SortByName();
                break;
            case "customer.name":
                sortStrategy = new SortByCustomerName();
                break;
            case "finishDate":
                sortStrategy = new SortByFinishDate();
                break;
            case "process":
                sortStrategy = new SortByProcess();
                break;

            default: sortStrategy = new SortById();
        }

        if(order.getDirection().isDescending()) {
            return sortStrategy.getStrategy().reversed();
        }
        return sortStrategy.getStrategy();
    }

    private interface SortStrategy {
        Comparator<OpportunityListDTO> getStrategy();
    }

    private static class SortByUpdated implements SortStrategy {
        @Override
        public Comparator<OpportunityListDTO> getStrategy() {
            return Comparator.comparing(OpportunityListDTO::getUpdateDate, Comparator.nullsFirst(Comparator.naturalOrder()))
                    .thenComparing(OpportunityListDTO::getId, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }

    private static class SortById implements SortStrategy {
        @Override
        public Comparator<OpportunityListDTO> getStrategy() {
            return Comparator.comparing(OpportunityListDTO::getId);
        }
    }

    private static class SortByName implements SortStrategy {
        @Override
        public Comparator<OpportunityListDTO> getStrategy() {
            return Comparator.comparing(OpportunityListDTO::getName, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }

    private static class SortByCustomerName implements SortStrategy {
        @Override
        public Comparator<OpportunityListDTO> getStrategy() {
            return Comparator.comparing(OpportunityListDTO::getCustomerName, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }

    private static class SortByFinishDate implements SortStrategy {
        @Override
        public Comparator<OpportunityListDTO> getStrategy() {
            return Comparator.comparing(OpportunityListDTO::getFinishDate, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }

    private static class SortByProcess implements SortStrategy {
        @Override
        public Comparator<OpportunityListDTO> getStrategy() {
            return Comparator.comparing(OpportunityListDTO::getProcessInstanceName, Comparator.nullsFirst(Comparator.naturalOrder()))
                    .thenComparing(OpportunityListDTO::getProcessInstanceVersion, Comparator.nullsFirst(Comparator.naturalOrder()));
        }
    }
}
