package com.devqube.crmbusinessservice.leadopportunity.lead.importexport.dto;

import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ValidatedValue;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ParsedLeadDtoOut {

    private ValidatedValue firstName;
    private ValidatedValue lastName;
    private ValidatedValue email;
    private ValidatedValue phone;
    private ValidatedValue position;
    private ValidatedValue companyName;
    private ValidatedValue companyCity;

}
