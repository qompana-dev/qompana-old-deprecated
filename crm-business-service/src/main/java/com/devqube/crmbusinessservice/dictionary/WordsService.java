package com.devqube.crmbusinessservice.dictionary;

import com.devqube.crmbusinessservice.dictionary.dto.SaveWordDTO;
import com.devqube.crmbusinessservice.dictionary.dto.WordDTO;
import com.devqube.crmbusinessservice.productprice.exception.ObjectUsedInGoalException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
class WordsService {

    private final WordRepository wordRepository;

    @Autowired
    public WordsService(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }

    public SaveWordDTO createWord(SaveWordDTO saveWordDTO) throws EntityNotFoundException {
        Word parent = getParent(saveWordDTO);
        Word wordToSave = WordFactory.create(parent, saveWordDTO);
        Word savedWord = wordRepository.save(wordToSave);
        return SaveWordDTOFactory.create(savedWord);
    }

    public List<SaveWordDTO> createAllWords(List<SaveWordDTO> saveWordDTOList) throws EntityNotFoundException {
        List<Word> allWordsToSave = saveWordDTOList.stream()
                .map(saveWordDTO -> {
                    Word parent = null;
                    try {
                        parent = getParent(saveWordDTO);
                    } catch (EntityNotFoundException e) {
                        e.printStackTrace();
                    }
                    return WordFactory.create(parent, saveWordDTO);
                })
                .collect(Collectors.toList());

        List<Word> savedWordList = wordRepository.saveAll(allWordsToSave);

        return savedWordList.stream()
        .map(SaveWordDTOFactory::create)
        .collect(Collectors.toList());
    }

    public SaveWordDTO modifyWord(Long wordId, SaveWordDTO saveWordDTO) throws EntityNotFoundException {
        Word word = wordRepository.findById(wordId).orElseThrow(EntityNotFoundException::new);
        modifyWord(word, saveWordDTO);
        Word updatedWord = wordRepository.save(word);
        return SaveWordDTOFactory.create(updatedWord);
    }

    @Transactional(rollbackFor = {ObjectUsedInGoalException.class})
    public void deleteWord(Long wordId) throws EntityNotFoundException {
        Word word = wordRepository.findById(wordId).orElseThrow(EntityNotFoundException::new);
        word.setUpdated(LocalDateTime.now());
        word.setDeleted(true);
        wordRepository.save(word);
    }

    private Word getParent(SaveWordDTO saveWordDTO) throws EntityNotFoundException {
        Long parentId = saveWordDTO.getParentId();
        Word parent = null;
        if (Objects.nonNull(parentId)) {
            parent = wordRepository.findById(parentId).orElseThrow(() -> new EntityNotFoundException("Parent not found"));
        }
        return parent;
    }

    private void modifyWord(Word word, SaveWordDTO saveWordDTO) {
        word.setName(saveWordDTO.getName());
        word.setGlobal(saveWordDTO.getGlobal());
        word.setUpdated(LocalDateTime.now());
    }

    public List<WordDTO> getPrivateWords(String locale, List<Long> privateWordsIds) {
        Specification<Word> spec = WordSpecifications.findByLocaleAndIdIn(privateWordsIds, locale);
        List<Word> words = wordRepository.findAll(spec);
        List<Word> filteredWords = UserWordsFilter.filterPrivate(words);
        return WordDTOFactory.create(filteredWords);
    }
}
