package com.devqube.crmbusinessservice.dictionary;

import com.devqube.crmbusinessservice.phoneNumber.PhoneNumber;
import com.devqube.crmbusinessservice.phoneNumber.PhoneNumber;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@Accessors(chain = true)
public class Word {

    @Id
    @SequenceGenerator(name = "word_seq", sequenceName = "word_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "word_seq")
    private Long id;
    private String name;

    @CreationTimestamp
    private LocalDateTime created;
    private LocalDateTime updated;

    private boolean deleted;

    private Long dictionaryId;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parent")
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private List<Word> children;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "parent_id")
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private Word parent;

    private Boolean global;
    private String locale;

    @OneToMany(mappedBy = "type")
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private Set<PhoneNumber> phoneNumberSet;

    @Override
    public String toString() {
       return name;
    }
}
