package com.devqube.crmbusinessservice.dictionary;

import com.devqube.crmbusinessservice.dictionary.dto.DictionaryDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public
class DictionaryService {

    private final DictionaryRepository dictionaryRepository;
    private final WordRepository wordRepository;

    @Autowired
    public DictionaryService(DictionaryRepository dictionaryRepository,
                             WordRepository wordRepository) {
        this.dictionaryRepository = dictionaryRepository;
        this.wordRepository = wordRepository;
    }

    public DictionaryDTO findWholeDictionary(Long dictionaryId, String locale) {
        Dictionary dictionary = dictionaryRepository.findDictionaryFor(dictionaryId);
        List<Word> words = wordRepository.findAllWords(dictionaryId, locale);
        return DictionaryDTOFactory.create(dictionary, words);
    }

    public DictionaryDTO findUserDictionary(Long dictionaryId, String locale, List<Long> privateWordsIds) {
        Dictionary dictionary = dictionaryRepository.findDictionaryFor(dictionaryId);
        List<Word> words = wordRepository.findAllWords(dictionaryId, locale);
        List<Word> filteredWords = UserWordsFilter.filter(words, privateWordsIds);
        return DictionaryDTOFactory.create(dictionary, filteredWords);
    }

    public DictionaryDTO findAdminDictionary(Long dictionaryId, String locale) {
        Dictionary dictionary = dictionaryRepository.findDictionaryFor(dictionaryId);
        List<Word> words = wordRepository.findAllWords(dictionaryId, locale);
        List<Word> filteredWords = AdminWordsFilter.filter(words);
        return DictionaryDTOFactory.create(dictionary, filteredWords);
    }

    public DictionaryDTO findAdminDictionary(Long dictionaryId) {
        Dictionary dictionary = dictionaryRepository.findDictionaryFor(dictionaryId);
        List<Word> words = wordRepository.findAllWords(dictionaryId);
        return DictionaryDTOFactory.create(dictionary, words);
    }
}
