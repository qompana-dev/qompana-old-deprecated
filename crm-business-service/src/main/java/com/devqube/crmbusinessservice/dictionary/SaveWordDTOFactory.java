package com.devqube.crmbusinessservice.dictionary;

import com.devqube.crmbusinessservice.dictionary.dto.SaveWordDTO;

import java.util.Objects;

class SaveWordDTOFactory {

    static SaveWordDTO create(Word word) {
        return new SaveWordDTO(
                word.getId(),
                word.getName(),
                getParentId(word.getParent()),
                word.getDictionaryId(),
                word.getGlobal(),
                word.getLocale()
        );
    }

    private static Long getParentId(Word parent) {
        if (Objects.nonNull(parent)) {
            return parent.getId();
        }
        return null;
    }
}
