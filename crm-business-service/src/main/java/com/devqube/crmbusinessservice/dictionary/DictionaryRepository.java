package com.devqube.crmbusinessservice.dictionary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

interface DictionaryRepository extends JpaRepository<Dictionary, Long> {

    @Query("select d from Dictionary d where d.id = :dictionaryId")
    Dictionary findDictionaryFor(@Param("dictionaryId") Long dictionaryId);
}
