package com.devqube.crmbusinessservice.dictionary;

import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public class WordSpecifications {

    public static Specification<Word> findByPhoneId(Long phoneId) {
        return (root, query, cb) -> {
            query.distinct(true);
            return cb.equal(root.joinSet("phoneNumberSet").get("id"), phoneId);
        };
    }

    public static Specification<Word> findByName(String toSearch) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("name")), "%" + toSearch.toLowerCase() + "%");
    }

    public static Specification<Word> findByLocale(String locale) {
        return (root, query, cb) -> cb.equal(root.get("locale"), locale);
    }

    public static Specification<Word> findByIdIn(List<Long> idList) {
                return idList.size() > 0 ? (root, query, cb) -> root.get("id").in(idList) :
                        (root, query, cb) -> cb.isNull(root.get("id"));
    }

    public static Specification<Word> findByLocaleAndIdIn(List<Long> idList, String locale) {
        return findByIdIn(idList)
                .and(findByLocale(locale));
    }


}
