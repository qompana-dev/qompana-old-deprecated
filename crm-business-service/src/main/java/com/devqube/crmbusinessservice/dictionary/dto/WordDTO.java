package com.devqube.crmbusinessservice.dictionary.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class WordDTO {

    @AuthFields(list = {
            @AuthField(controller = "findAdminDictionary"),
            @AuthField(controller = "findUserDictionary"),
            @AuthField(controller = "findWholeDictionary"),
            @AuthField(controller = "getPrivateWords")
    })
    private Long id;

    @AuthFields(list = {
            @AuthField(controller = "findAdminDictionary"),
            @AuthField(controller = "findUserDictionary"),
            @AuthField(controller = "findWholeDictionary"),
            @AuthField(controller = "getPrivateWords")
    })
    private Long parentId;

    @AuthFields(list = {
            @AuthField(controller = "findAdminDictionary"),
            @AuthField(controller = "findUserDictionary"),
            @AuthField(controller = "findWholeDictionary"),
            @AuthField(controller = "getPrivateWords")
    })
    private String name;

    @AuthFields(list = {
            @AuthField(controller = "findAdminDictionary"),
            @AuthField(controller = "findUserDictionary"),
            @AuthField(controller = "findWholeDictionary"),
            @AuthField(controller = "getPrivateWords")
    })
    private List<WordDTO> children;

    @AuthFields(list = {
            @AuthField(controller = "findAdminDictionary"),
            @AuthField(controller = "findUserDictionary"),
            @AuthField(controller = "findWholeDictionary"),
            @AuthField(controller = "getPrivateWords")
    })
    private Boolean global;

    @AuthFields(list = {
            @AuthField(controller = "findAdminDictionary"),
            @AuthField(controller = "findUserDictionary"),
            @AuthField(controller = "findWholeDictionary"),
            @AuthField(controller = "getPrivateWords")
    })
    private String locale;
}
