package com.devqube.crmbusinessservice.dictionary;

import com.devqube.crmbusinessservice.dictionary.dto.DictionaryDTO;
import com.devqube.crmshared.access.annotation.AuthController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class DictionaryController implements DictionaryApi {

    private final DictionaryService dictionaryService;

    @Autowired
    public DictionaryController(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    @Override
    @AuthController(name = "findUserDictionary")
    public ResponseEntity<DictionaryDTO> findUserDictionary(Long dictionaryId, String locale, List<Long> privateWordsIds) {
        try {
            return new ResponseEntity<>(dictionaryService.findUserDictionary(dictionaryId, locale, privateWordsIds), HttpStatus.OK);
        } catch (Exception e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @AuthController(name = "findAdminDictionary")
    public ResponseEntity<DictionaryDTO> findAdminDictionary(Long dictionaryId, String locale) {
        try {
            return new ResponseEntity<>(dictionaryService.findAdminDictionary(dictionaryId, locale), HttpStatus.OK);
        } catch (Exception e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @AuthController(name = "findWholeDictionary")
    public ResponseEntity<DictionaryDTO> findWholeDictionary(Long dictionaryId, String locale) {
        try {
            return new ResponseEntity<>(dictionaryService.findWholeDictionary(dictionaryId, locale), HttpStatus.OK);
        } catch (Exception e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
