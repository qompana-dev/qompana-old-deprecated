package com.devqube.crmbusinessservice.dictionary;

import com.devqube.crmbusinessservice.dictionary.dto.WordDTO;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class WordDTOFactory {

    static List<WordDTO> create(List<Word> words) {
        return words.stream()
                .map(WordDTOFactory::create)
                .collect(Collectors.toList());
    }

    private static WordDTO create(Word word) {
        return new WordDTO(
                word.getId(),
                getParentId(word.getParent()),
                word.getName(),
                create(word.getChildren()),
                word.getGlobal(),
                word.getLocale()
        );
    }

    private static Long getParentId(Word parent) {
        if (Objects.nonNull(parent)) {
            return parent.getId();
        }
        return null;
    }
}
