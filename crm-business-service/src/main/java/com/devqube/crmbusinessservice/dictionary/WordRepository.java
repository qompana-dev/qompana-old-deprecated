package com.devqube.crmbusinessservice.dictionary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WordRepository extends JpaRepository<Word, Long>, JpaSpecificationExecutor<Word> {

    @Query("select w from Word w where w.dictionaryId = :dictionaryId and w.parent is null and w.locale = :locale order by w.name")
    List<Word> findAllWords(@Param("dictionaryId") Long dictionaryId, @Param("locale") String locale);

    @Query("select w from Word w where w.dictionaryId = :dictionaryId and w.parent is null order by w.name")
    List<Word> findAllWords(@Param("dictionaryId") Long dictionaryId);

    List<Word> findByDictionaryIdAndNameIgnoreCase(Long dictionaryId, String name);

    Word findByNameIgnoreCase(String name);

}
