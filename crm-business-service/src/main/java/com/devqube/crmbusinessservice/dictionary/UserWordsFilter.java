package com.devqube.crmbusinessservice.dictionary;

import java.util.ArrayList;
import java.util.List;

class UserWordsFilter {

    static List<Word> filter(List<Word> words, List<Long> privateWordsIds) {
        List<Word> result = new ArrayList<>();

        for (Word word : words) {
            if (!isValid(word, privateWordsIds)) {
                continue;
            }

            List<Word> children = UserWordsFilter.filter(word.getChildren(), privateWordsIds);
            Word newWord = WordFactory.create(word, children);
            result.add(newWord);
        }

        return result;
    }

    private static boolean isValid(Word word, List<Long> privateWordsIds) {
        boolean containsId = privateWordsIds.contains(word.getId());
        if (containsId) {
            return true;
        }

        return !word.isDeleted() && word.getGlobal();
    }

    public static List<Word> filterPrivate(List<Word> words) {
        List<Word> result = new ArrayList<>();

        for (Word word : words) {
           List<Word> children = UserWordsFilter.filterWithoutPrivate(word.getChildren());
            Word newWord = WordFactory.create(word, children);
            result.add(newWord);
        }

        return result;
    }
    public static List<Word> filterWithoutPrivate(List<Word> words) {
        List<Word> result = new ArrayList<>();

        for (Word word : words) {
            if (word.isDeleted() || !word.getGlobal()) {
                continue;
            }
            List<Word> children = UserWordsFilter.filterWithoutPrivate(word.getChildren());
            Word newWord = WordFactory.create(word, children);
            result.add(newWord);
        }

        return result;
    }
}
