package com.devqube.crmbusinessservice.dictionary;

import com.devqube.crmbusinessservice.dictionary.dto.SaveWordDTO;
import com.devqube.crmbusinessservice.dictionary.dto.WordDTO;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
public class WordsController implements WordsApi {

    private final WordsService wordsService;

    @Autowired
    public WordsController(WordsService wordsService) {
        this.wordsService = wordsService;
    }

    @Override
    @AuthController(name = "createWord")
    public ResponseEntity<SaveWordDTO> createWord(@Valid SaveWordDTO saveWordDTO) {
        try {
            return new ResponseEntity<>(wordsService.createWord(saveWordDTO), HttpStatus.CREATED);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(name = "createWord")
    public ResponseEntity<List<SaveWordDTO>> createAllWords(@Valid List<SaveWordDTO> saveWordDTOList) {
        try {
            return new ResponseEntity<>(wordsService.createAllWords(saveWordDTOList), HttpStatus.CREATED);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "modifyWord")
    public ResponseEntity<SaveWordDTO> modifyWord(Long wordId, @Valid SaveWordDTO saveWordDTO) {
        try {
            return new ResponseEntity<>(wordsService.modifyWord(wordId, saveWordDTO), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "deleteWord")
    public ResponseEntity<Void> deleteWord(Long wordId) {
        try {
            wordsService.deleteWord(wordId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(name = "getPrivateWords")
    public ResponseEntity<List<WordDTO>> getPrivateWords(String locale, List<Long> privateWordsIds) {
        try {
            return new ResponseEntity<>(wordsService.getPrivateWords(locale, privateWordsIds), HttpStatus.OK);
        } catch (Exception e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
