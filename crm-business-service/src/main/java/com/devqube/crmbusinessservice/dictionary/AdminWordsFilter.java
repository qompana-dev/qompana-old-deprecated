package com.devqube.crmbusinessservice.dictionary;

import java.util.ArrayList;
import java.util.List;

class AdminWordsFilter {

    static List<Word> filter(List<Word> words) {
        List<Word> result = new ArrayList<>();

        for (Word word : words) {
            if (!isValid(word)) {
                continue;
            }

            List<Word> children = AdminWordsFilter.filter(word.getChildren());
            Word newWord = WordFactory.create(word, children);
            result.add(newWord);
        }

        return result;
    }

    private static boolean isValid(Word word) {
        return !word.isDeleted();
    }
}
