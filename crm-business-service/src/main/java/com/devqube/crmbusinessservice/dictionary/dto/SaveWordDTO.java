package com.devqube.crmbusinessservice.dictionary.dto;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class SaveWordDTO {

    @AuthFields(list = {
            @AuthField(controller = "createWord"),
            @AuthField(controller = "modifyWord")
    })
    private Long id;

    @AuthFields(list = {
            @AuthField(controller = "createWord"),
            @AuthField(controller = "modifyWord")
    })
    private String name;

    @AuthFields(list = {
            @AuthField(controller = "createWord"),
            @AuthField(controller = "modifyWord")
    })
    private Long parentId;

    @AuthFields(list = {
            @AuthField(controller = "createWord"),
            @AuthField(controller = "modifyWord")
    })
    private Long dictionaryId;

    @AuthFields(list = {
            @AuthField(controller = "createWord"),
            @AuthField(controller = "modifyWord")
    })
    private Boolean global;

    @AuthFields(list = {
            @AuthField(controller = "createWord"),
            @AuthField(controller = "modifyWord")
    })
    private String locale;
}
