package com.devqube.crmbusinessservice.dictionary;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.dictionary.dto.SaveWordDTO;
import com.devqube.crmbusinessservice.dictionary.dto.WordDTO;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadSearchVariantsDtoOut;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "words")
public interface WordsApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "create word", nickname = "createWord", notes = "Method used to create word", response = SaveWordDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "added word successfully", response = SaveWordDTO.class),
            @ApiResponse(code = 400, message = "this process definition doesn't exist")})
    @RequestMapping(value = "/dictionary/word",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<SaveWordDTO> createWord(@ApiParam(value = "Created word object", required = true) @Valid @RequestBody SaveWordDTO saveWordDTO) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "create word", nickname = "createWord", notes = "Method used to create many words", response = SaveWordDTO.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "added all words successfully", response = SaveWordDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "this process definition doesn't exist")})
    @RequestMapping(value = "/dictionary/allword",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<List<SaveWordDTO>> createAllWords(@ApiParam(value = "Created word object", required = true) @Valid @RequestBody List<SaveWordDTO> saveWordDTOList) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "modify word", nickname = "modifyWord", notes = "Method used to modify word", response = SaveWordDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "modified lead successfully", response = SaveWordDTO.class),
            @ApiResponse(code = 400, message = "this process definition doesn't exists"),
            @ApiResponse(code = 404, message = "word not found")})
    @RequestMapping(value = "/dictionary/word/{wordId}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<SaveWordDTO> modifyWord(@ApiParam(value = "words's id", required = true) @PathVariable("wordId") Long wordId,
                                                   @ApiParam(value = "Modified word object", required = true) @Valid @RequestBody SaveWordDTO saveWordDTO) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "delete word", nickname = "deleteWord", notes = "Method used to delete word")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "word deleted successfully"),
            @ApiResponse(code = 400, message = "bad request"),
            @ApiResponse(code = 403, message = "no permission to delete word"),
            @ApiResponse(code = 404, message = "word not found")})
    @RequestMapping(value = "/dictionary//word/{wordId}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteWord(@ApiParam(value = "words's id", required = true) @PathVariable("wordId") Long wordId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get private words", nickname = "modifyWord", notes = "Method is used to private words for global storage", response = WordDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "words fetched successfully", response = SaveWordDTO.class)})
    @RequestMapping(value = "/dictionary/private/words/{locale}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<WordDTO>> getPrivateWords(
            @ApiParam(value = "locale", required = true) @PathVariable("locale") String locale,
            @ApiParam(value = "private words ids", required = true) @RequestParam("privateWordsIds") List<Long> privateWordsIds) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
