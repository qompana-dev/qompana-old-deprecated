package com.devqube.crmbusinessservice.dictionary;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class Dictionary {

    @Id
    @SequenceGenerator(name = "dictionary_seq", sequenceName = "dictionary_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dictionary_seq")
    private Long id;

    @Enumerated(EnumType.STRING)
    private Type type;

    private Long maxLevel;

    private Boolean isWordsDeletable;

    private Boolean isWordsHideable;

}
