package com.devqube.crmbusinessservice.dictionary;

import com.devqube.crmbusinessservice.dictionary.dto.SaveWordDTO;

import java.util.Collections;
import java.util.List;

class WordFactory {

    static Word create(Word word, List<Word> children) {
        return new Word(
                word.getId(),
                word.getName(),
                word.getCreated(),
                word.getUpdated(),
                word.isDeleted(),
                word.getDictionaryId(),
                children,
                word.getParent(),
                word.getGlobal(),
                word.getLocale(),
                null
        );
    }

    static Word create(Word parent, SaveWordDTO saveWordDTO) {
        return new Word(
                null,
                saveWordDTO.getName(),
                null,
                null,
                false,
                saveWordDTO.getDictionaryId(),
                Collections.emptyList(),
                parent,
                saveWordDTO.getGlobal(),
                saveWordDTO.getLocale(),
                null
        );
    }
}
