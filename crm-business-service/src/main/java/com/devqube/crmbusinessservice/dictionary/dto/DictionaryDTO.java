package com.devqube.crmbusinessservice.dictionary.dto;

import com.devqube.crmbusinessservice.dictionary.Type;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@AuthFilterEnabled
public class DictionaryDTO {

    @AuthFields(list = {
            @AuthField(controller = "findAdminDictionary"),
            @AuthField(controller = "findUserDictionary"),
            @AuthField(controller = "findWholeDictionary")
    })
    private Long id;

    @AuthFields(list = {
            @AuthField(controller = "findAdminDictionary"),
            @AuthField(controller = "findUserDictionary"),
            @AuthField(controller = "findWholeDictionary")
    })
    private Type type;

    @AuthFields(list = {
            @AuthField(controller = "findAdminDictionary"),
            @AuthField(controller = "findUserDictionary"),
            @AuthField(controller = "findWholeDictionary")
    })
    private Long maxLevel;

    @AuthFields(list = {
            @AuthField(controller = "findAdminDictionary"),
            @AuthField(controller = "findUserDictionary"),
            @AuthField(controller = "findWholeDictionary")
    })
    private Boolean isWordsDeletable;

    @AuthFields(list = {
            @AuthField(controller = "findAdminDictionary"),
            @AuthField(controller = "findUserDictionary"),
            @AuthField(controller = "findWholeDictionary")
    })
    private Boolean isWordsHideable;

    @AuthFields(list = {
            @AuthField(controller = "findAdminDictionary"),
            @AuthField(controller = "findUserDictionary"),
            @AuthField(controller = "findWholeDictionary")
    })
    private List<WordDTO> words;
}
