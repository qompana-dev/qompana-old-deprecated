package com.devqube.crmbusinessservice.dictionary;

import lombok.Getter;

@Getter
public enum Type {
    SOURCE_OF_ACQUISITION(1L),
    UNIT(2L),
    PHONE_NUMBER_TYPE(3L),
    SUBJECT_OF_INTEREST(4L),
    POSITION(5L),
    INDUSTRY(6L),
    LOCATION(7L);

    private Long dictionaryId;

    Type(Long dictionaryId) {
        this.dictionaryId = dictionaryId;
    }
}
