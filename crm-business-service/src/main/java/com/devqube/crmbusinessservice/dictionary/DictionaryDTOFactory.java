package com.devqube.crmbusinessservice.dictionary;

import com.devqube.crmbusinessservice.dictionary.dto.DictionaryDTO;

import java.util.List;

class DictionaryDTOFactory {

    static DictionaryDTO create(Dictionary dictionary, List<Word> words) {
        return new DictionaryDTO(
                dictionary.getId(),
                dictionary.getType(),
                dictionary.getMaxLevel(),
                dictionary.getIsWordsDeletable(),
                dictionary.getIsWordsHideable(),
                WordDTOFactory.create(words)
        );
    }
}
