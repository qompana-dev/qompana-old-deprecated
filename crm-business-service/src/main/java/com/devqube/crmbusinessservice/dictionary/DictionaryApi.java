package com.devqube.crmbusinessservice.dictionary;

import com.devqube.crmbusinessservice.ApiUtil;
import com.devqube.crmbusinessservice.dictionary.dto.DictionaryDTO;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "dictionary")
public interface DictionaryApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Find user dictionary", nickname = "findUserDictionary", notes = "Method used to find dictionary with words allowed to user", response = DictionaryDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "dictionary", response = DictionaryDTO.class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/dictionary/{dictionaryId}/{locale}/user",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<DictionaryDTO> findUserDictionary(@ApiParam(value = "dictionary's id", required = true) @PathVariable("dictionaryId") Long dictionaryId,
                                                             @ApiParam(value = "locale", required = true) @PathVariable("locale") String locale,
                                                             @ApiParam(value = "private words ids", required = true) @RequestParam("privateWordsIds") List<Long> privateWordsIds) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Find admin dictionary", nickname = "findAdminDictionary", notes = "Method used to find dictionary with words allowed to admin", response = DictionaryDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "dictionary", response = DictionaryDTO.class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/dictionary/{dictionaryId}/{locale}/admin",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<DictionaryDTO> findAdminDictionary(@ApiParam(value = "dictionary's id", required = true) @PathVariable("dictionaryId") Long dictionaryId,
                                                              @ApiParam(value = "locale", required = true) @PathVariable("locale") String locale) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Find whole dictionary", nickname = "findWholeDictionary", notes = "Method used to find dictionary with whole", response = DictionaryDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "dictionary", response = DictionaryDTO.class),
            @ApiResponse(code = 400, message = "unable to obtain user id")})
    @RequestMapping(value = "/dictionary/{dictionaryId}/{locale}/whole",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<DictionaryDTO> findWholeDictionary(@ApiParam(value = "dictionary's id", required = true) @PathVariable("dictionaryId") Long dictionaryId,
                                                              @ApiParam(value = "locale", required = true) @PathVariable("locale") String locale) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
