package com.devqube.crmbusinessservice.productprice.priceBook;

import com.devqube.crmbusinessservice.productprice.exception.PriceBookNameOccupiedException;
import com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook;
import com.devqube.crmbusinessservice.productprice.productPrice.ProductPriceRepository;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PriceBookServiceTest {

    @Mock
    private PriceBookRepository priceBookRepository;
    @Mock
    private AccessService accessService;
    @Mock
    private ProductPriceRepository productPriceRepository;

    @InjectMocks
    private PriceBookService priceBookService;

    private PriceBook priceBook;

    @Before
    public void setUp() {
        this.priceBook = new PriceBook();
        this.priceBook.setName("name");
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenPriceBookNameOccupied() throws BadRequestException {
        when(priceBookRepository.findByName(priceBook.getName())).thenReturn(Optional.of(priceBook));
        this.priceBookService.save(priceBook);
    }

    @Test
    public void shouldSavePriceBookWhenEverythingOK() throws BadRequestException {
        when(priceBookRepository.findByName(priceBook.getName())).thenReturn(Optional.empty());
        when(accessService.assignAndVerify(priceBook)).thenReturn(priceBook);
        this.priceBookService.save(priceBook);
        verify(priceBookRepository, times(1)).save(priceBook);
    }

    @Test
    public void shouldDeleteExistingPriceBook() throws EntityNotFoundException, BadRequestException {
        when(priceBookRepository.findById(1L)).thenReturn(Optional.of(priceBook));
        priceBookService.deleteById(1L);
        verify(priceBookRepository, times(1)).save(any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenDeletingPriceBookNotFound() throws EntityNotFoundException, BadRequestException {
        when(priceBookRepository.findById(1L)).thenReturn(Optional.empty());
        priceBookService.deleteById(1L);
    }

    @Test
    public void shouldDeleteManyPriceBooks() throws EntityNotFoundException, BadRequestException {
        PriceBook priceBook2 = new PriceBook();
        priceBook2.setName("name");
        when(priceBookRepository.findById(1L)).thenReturn(Optional.of(priceBook));
        when(priceBookRepository.findById(2L)).thenReturn(Optional.of(priceBook2));
        priceBookService.deleteByIds(List.of(1L, 2L));
        verify(priceBookRepository, times(2)).save(any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenDeletingManyPriceBooksAndOneNotFound() throws EntityNotFoundException, BadRequestException {
        when(priceBookRepository.findById(1L)).thenReturn(Optional.of(priceBook));
        when(priceBookRepository.findById(2L)).thenReturn(Optional.empty());
        priceBookService.deleteByIds(List.of(1L, 2L, 3L));
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenIdIsNull() throws PriceBookNameOccupiedException, EntityNotFoundException, BadRequestException {
        priceBookService.modify(new PriceBook(), null);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenPriceBookIsNull() throws PriceBookNameOccupiedException, EntityNotFoundException, BadRequestException {
        priceBookService.modify(null, 1L);
    }

    @Test(expected = PriceBookNameOccupiedException.class)
    public void shouldNotModifyWhenOtherPriceBookWithSameNameExists() throws PriceBookNameOccupiedException, BadRequestException, EntityNotFoundException {
        priceBook.setId(2L);
        when(priceBookRepository.findById(1L)).thenReturn(Optional.of(priceBook));
        when(priceBookRepository.findByName("name")).thenReturn(Optional.of(priceBook));
        PriceBook priceBook = new PriceBook();
        priceBook.setName("name");
        priceBookService.modify(priceBook, 1L);
    }

    @Test
    public void shouldModifyPriceBookWhenEverythingIsOk() throws PriceBookNameOccupiedException, BadRequestException, EntityNotFoundException {
        priceBook.setPrices(Collections.emptyList());
        when(priceBookRepository.findById(1L)).thenReturn(Optional.of(priceBook));
        when(priceBookRepository.findByName("name")).thenReturn(Optional.empty());
        PriceBook priceBook = new PriceBook();
        priceBook.setName("name");
        when(accessService.assignAndVerify(any(), any())).thenReturn(priceBook);
        priceBookService.modify(priceBook, 1L);
        verify(priceBookRepository, times(1)).save(any());
    }
}
