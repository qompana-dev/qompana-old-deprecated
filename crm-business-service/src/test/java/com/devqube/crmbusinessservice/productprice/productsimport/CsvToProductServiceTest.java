package com.devqube.crmbusinessservice.productprice.productsimport;

import com.devqube.crmbusinessservice.productprice.productsimport.dto.ProductDto;
import com.univocity.parsers.common.DataProcessingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class CsvToProductServiceTest {


    @InjectMocks
    private CsvToProductService csvToProductService;
    private List<ProductDto> expectedProducts;


    @Before
    public void setUp() {
        expectedProducts = createExpectedProducts();
    }

    @Test
    public void shouldConvertFromCsvWithHeaderToProducts() throws FileNotFoundException {
        InputStream testFile = new FileInputStream(new File("src/test/resources/test_products_with_header.csv"));
        List<DataProcessingException> errors = new ArrayList<>();
        List<ProductDto> actualProducts = csvToProductService.convertToProducts(testFile, true, errors);
        assertEquals(expectedProducts, actualProducts);
    }

    @Test
    public void shouldConvertFromCsvWithoutHeaderToProducts() throws FileNotFoundException {
        InputStream testFile = new FileInputStream(new File("src/test/resources/test_products_without_header.csv"));
        List<DataProcessingException> errors = new ArrayList<>();
        List<ProductDto> actualProducts = csvToProductService.convertToProducts(testFile, false, errors);
        assertEquals(expectedProducts, actualProducts);
    }

    @Test(expected = DataProcessingException.class)
    public void shouldThrowExceptionBecauseOfBrokenFile() throws FileNotFoundException {
        InputStream testFile = new FileInputStream(new File("src/test/resources/test_products_broken.csv"));
        List<DataProcessingException> errors = new ArrayList<>();
        csvToProductService.convertToProducts(testFile, false, errors);
    }

    private List<ProductDto> createExpectedProducts() {
        ProductDto product1 = ProductDto.builder().name("Serwer").code("1").skuCode("1").brand("Dell").purchasePrice(2000.02).currency("PLN").sellPriceNet(2300.0).vat(23).active(true).description("some description").specification("some specification").unit("sztuki").build();
        ProductDto product2 = ProductDto.builder().name("Laptop").code("2").skuCode("2").brand("HP").purchasePrice(1000.0).currency("EUR").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").unit("sztuki").build();
        ProductDto product3 = ProductDto.builder().name("Wsparcie techniczne").code("3").skuCode("3").purchasePrice(1000.0).currency("USD").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").unit("sztuki").build();
        ProductDto product4 = ProductDto.builder().name("Gwrancja rozszerzona").code("4").skuCode("4").purchasePrice(1000.0).currency("GBP").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").unit("sztuki").build();
        ProductDto product5 = ProductDto.builder().name("Konfiguracja sieci").code("5").skuCode("5").purchasePrice(1000.0).currency("EUR").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").unit("sztuki").build();
        return List.of(product1, product2, product3, product4, product5);
    }

}
