package com.devqube.crmbusinessservice.productprice.productsimport.web;

import com.devqube.crmbusinessservice.AbstractIntegrationTest;
import com.devqube.crmbusinessservice.productprice.priceBook.PriceBookRepository;
import com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook;
import com.devqube.crmbusinessservice.productprice.productPrice.ProductPriceRepository;
import com.devqube.crmbusinessservice.productprice.productPrice.model.ProductPrice;
import com.devqube.crmbusinessservice.productprice.products.ProductRepository;
import com.devqube.crmbusinessservice.productprice.products.ProductService;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
public class ImportProductsControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private PriceBookRepository priceBookRepository;
    @Autowired
    private ProductPriceRepository productPriceRepository;

    private static final String[] FIELDS_TO_COMPARE = new String[] {"name", "code", "skuCode",
            "manufacturer", "supplier", "brand", "purchasePrice", "currency", "sellPriceNet", "vat", "active",
            "description", "specification", "deleted"};

    private static final List<Product> NEW_PRODUCTS = List.of(
            Product.builder().name("Serwer").code("1").skuCode("1").brand("Dell").purchasePrice(2000.02).currency("PLN").sellPriceNet(2300.0).vat(23).active(true).description("some description").specification("some specification").deleted(false).build(),
            Product.builder().name("Laptop").code("2").skuCode("2").brand("HP").purchasePrice(1000.0).currency("EUR").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").deleted(false).build(),
            Product.builder().name("Wsparcie techniczne").code("3").skuCode("3").purchasePrice(1000.0).currency("USD").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").deleted(false).build(),
            Product.builder().name("Gwrancja rozszerzona").code("4").skuCode("4").purchasePrice(1000.0).currency("GBP").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").deleted(false).build(),
            Product.builder().name("Konfiguracja sieci").code("5").skuCode("5").purchasePrice(1000.0).currency("EUR").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").deleted(false).build()
    );

    private static final List<Product> ALREADY_EXISTING_PRODUCTS = List.of(
            Product.builder().code("100").skuCode("100").name("Telefon").description("Telefon").brand("Xiaomi").vat(30).deleted(false).build(),
            Product.builder().code("200").skuCode("200").name("Monitor").description("Monitor").brand("Eizo").vat(40).deleted(false).build(),
            Product.builder().code("300").skuCode("300").name("Laptop").description("Mac Book Pro").brand("Apple").vat(50).deleted(false).build(),
            Product.builder().code("400").skuCode("400").name("Telewizor").description("Telewizor").brand("LG").vat(60).deleted(false).build()
    );

    private static final List<Product> ALREADY_EXISTING_PRODUCTS_FOR_UPDATE = List.of(
            Product.builder().code("1").skuCode("1").name("Telefon").description("Telefon").brand("Xiaomi").vat(30).deleted(false).build(),
            Product.builder().code("2").skuCode("2").name("Monitor").description("Monitor").brand("Eizo").vat(40).deleted(false).build(),
            Product.builder().code("300").skuCode("300").name("Laptop").description("Mac Book Pro").brand("Apple").vat(50).deleted(false).build(),
            Product.builder().code("400").skuCode("400").name("Telewizor").description("Telewizor").brand("LG").vat(60).deleted(false).build()
    );

    private static final List<Product> EXPECTED_RESULT_OF_ADD_ONLY_NEW = List.of(
            Product.builder().name("Serwer").code("1").skuCode("1").brand("Dell").purchasePrice(2000.02).currency("PLN").sellPriceNet(2300.0).vat(23).active(true).description("some description").specification("some specification").deleted(false).build(),
            Product.builder().name("Laptop").code("2").skuCode("2").brand("HP").purchasePrice(1000.0).currency("EUR").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").deleted(false).build(),
            Product.builder().name("Wsparcie techniczne").code("3").skuCode("3").purchasePrice(1000.0).currency("USD").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").deleted(false).build(),
            Product.builder().name("Gwrancja rozszerzona").code("4").skuCode("4").purchasePrice(1000.0).currency("GBP").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").deleted(false).build(),
            Product.builder().name("Konfiguracja sieci").code("5").skuCode("5").purchasePrice(1000.0).currency("EUR").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").deleted(false).build(),
            Product.builder().code("100").skuCode("100").name("Telefon").description("Telefon").brand("Xiaomi").vat(30).deleted(false).build(),
            Product.builder().code("200").skuCode("200").name("Monitor").description("Monitor").brand("Eizo").vat(40).deleted(false).build(),
            Product.builder().code("300").skuCode("300").name("Laptop").description("Mac Book Pro").brand("Apple").vat(50).deleted(false).build(),
            Product.builder().code("400").skuCode("400").name("Telewizor").description("Telewizor").brand("LG").vat(60).deleted(false).build()
    );

    private static final List<Product> EXPECTED_RESULT_OF_UPDATE_EXISTING = List.of(
            Product.builder().name("Serwer").code("1").skuCode("1").brand("Dell").purchasePrice(2000.02).currency("PLN").sellPriceNet(2300.0).vat(23).active(true).description("some description").specification("some specification").deleted(false).build(),
            Product.builder().name("Laptop").code("2").skuCode("2").brand("HP").purchasePrice(1000.0).currency("EUR").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").deleted(false).build(),
            Product.builder().code("300").skuCode("300").name("Laptop").description("Mac Book Pro").brand("Apple").vat(50).deleted(false).build(),
            Product.builder().code("400").skuCode("400").name("Telewizor").description("Telewizor").brand("LG").vat(60).deleted(false).build()
    );

    @Before
    public void setUp() throws IOException {
        super.setUp();
        productPriceRepository.deleteAll();
        productRepository.deleteAll();
        productRepository.saveAll(ALREADY_EXISTING_PRODUCTS);
        priceBookRepository.deleteAll();
        PriceBook defaultPriceBook = PriceBook.builder()
                .id(-1L)
                .name("Cennik cen Standardowych")
                .description("Cennik cen Standardowych")
                .currency("PLN")
                .active(true)
                .isDefault(true)
                .deleted(false)
                .created(LocalDateTime.now())
                .updated(LocalDateTime.now()).build();
        priceBookRepository.save(defaultPriceBook);
    }

    @Test
    public void shouldSaveOverrideExistingProductsWithProductsFromCsvFile() {
        HttpEntity<MultiValueMap<String, Object>> requestEntity = createImportCsvRequest("src/test/resources/test_products_with_header.csv", "OVERRIDE", true);

        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/products/import", requestEntity, Void.class);
        List<Product> actualProducts = productService.findAll();

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        assertEquals(NEW_PRODUCTS.size(), actualProducts.size());
        assertThat(actualProducts).usingElementComparatorOnFields(FIELDS_TO_COMPARE).isEqualTo(NEW_PRODUCTS);
    }

    @Test
    public void shouldSaveOverrideExistingProductsWithProductsFromCsvFileWithoutHeader() {
        HttpEntity<MultiValueMap<String, Object>> requestEntity = createImportCsvRequest("src/test/resources/test_products_without_header.csv", "OVERRIDE", false);

        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/products/import", requestEntity, Void.class);
        List<Product> actualProducts = productService.findAll();

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        assertEquals(NEW_PRODUCTS.size(), actualProducts.size());
        assertThat(actualProducts).usingElementComparatorOnFields(FIELDS_TO_COMPARE).isEqualTo(NEW_PRODUCTS);
    }

    @Test
    public void shouldAddAllProductToExistingOnesFromCsv() {
        HttpEntity<MultiValueMap<String, Object>> requestEntity = createImportCsvRequest("src/test/resources/test_products_with_header.csv", "ADD_ALL", true);

        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/products/import", requestEntity, Void.class);
        List<Product> actualProducts = productService.findAll();

        List<Product> expectedProducts = new ArrayList<>(ALREADY_EXISTING_PRODUCTS);
        expectedProducts.addAll(NEW_PRODUCTS);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        assertEquals(expectedProducts.size(), actualProducts.size());
        assertThat(actualProducts).usingElementComparatorOnFields(FIELDS_TO_COMPARE).isEqualTo(expectedProducts);
    }

    @Test
    public void shouldAddOnlyNewProductToExistingOnesFromCsv() {
        HttpEntity<MultiValueMap<String, Object>> requestEntity = createImportCsvRequest("src/test/resources/test_products_with_header.csv", "ADD_ONLY_NEW", true);

        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/products/import", requestEntity, Void.class);
        List<Product> actualProducts = productService.findAll();

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        assertEquals(9, actualProducts.size());
    }

    @Test
    public void shouldReturnBadCsvFileResponse() {
        HttpEntity<MultiValueMap<String, Object>> requestEntity = createImportCsvRequest("src/test/resources/test_products_broken.csv", "OVERRIDE", false);
        CsvParsingErrorResponse expectedResponse = new CsvParsingErrorResponse(1L, 4L, "corrupted_value");

        ResponseEntity<CsvParsingErrorResponse> response = restTemplate.postForEntity(baseUrl + "/products/import", requestEntity, CsvParsingErrorResponse.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(expectedResponse, response.getBody());
    }

    private HttpEntity<MultiValueMap<String, Object>> createImportCsvRequest(String csvFilePath, String importStrategy, boolean isHeader) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Logged-Account-Email", "jan@nowak.com");
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(new File(csvFilePath)));
        body.add("strategy", importStrategy);
        body.add("headerPresent", isHeader);
        return new HttpEntity<>(body, headers);
    }
}
