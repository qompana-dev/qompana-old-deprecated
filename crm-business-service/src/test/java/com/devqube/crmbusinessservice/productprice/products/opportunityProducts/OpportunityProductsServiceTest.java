package com.devqube.crmbusinessservice.productprice.products.opportunityProducts;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.leadopportunity.AccountsService;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.productprice.exception.ProductCodeOccupiedException;
import com.devqube.crmbusinessservice.productprice.manufacturer.ManufacturerRepository;
import com.devqube.crmbusinessservice.productprice.products.ProductRepository;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductEditDTO;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.web.dto.OpportunityProductSaveDTO;
import com.devqube.crmbusinessservice.productprice.supplier.SupplierRepository;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OpportunityProductsServiceTest {

    @Mock
    private OpportunityProductsRepository opportunityProductsRepository;

    @Mock
    private OpportunityRepository opportunityRepository;

    @Mock
    private UserClient userClient;

    @Mock
    private OpportunityProductMapper opportunityProductMapper;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ManufacturerRepository manufacturerRepository;

    @Mock
    SupplierRepository supplierRepository;

    @Mock
    private AccessService accessService;

    @Mock
    private AccountsService accountsService;

    @InjectMocks
    private OpportunityProductsService opportunityProductsService;

    private Opportunity opportunityWithoutPermissionForUserWithId1;
    private Opportunity opportunityWithPermissionForUserWithId1;
    private OpportunityProductSaveDTO dto;

    @Before
    public void setUp() {
        this.opportunityWithoutPermissionForUserWithId1 = new Opportunity();
        this.opportunityWithoutPermissionForUserWithId1.setCreator(-1L);
        this.opportunityWithoutPermissionForUserWithId1.setOwnerOneId(-2L);
        this.opportunityWithoutPermissionForUserWithId1.setOwnerTwoId(-3L);
        this.opportunityWithoutPermissionForUserWithId1.setOwnerThreeId(-4L);

        this.opportunityWithPermissionForUserWithId1 = new Opportunity();
        this.opportunityWithPermissionForUserWithId1.setId(1L);
        this.opportunityWithPermissionForUserWithId1.setCreator(1L);
        this.opportunityWithPermissionForUserWithId1.setOwnerOneId(1L);
        this.opportunityWithPermissionForUserWithId1.setOwnerTwoId(1L);
        this.opportunityWithPermissionForUserWithId1.setOwnerThreeId(1L);

        this.dto = new OpportunityProductSaveDTO(1L, "name", "code", "skudCode", List.of(1L),
                List.of(1L), null, null, "PLN", 1L, 2000.0,
                3000.0, 10L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowEntityNotFoundExceptionWhenGettingListAndOpportunityNotFound()
            throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.empty());
        opportunityProductsService.getOpportunityProducts(1L,  "test@test.com", null);
    }

    @Test(expected = PermissionDeniedException.class)
    public void shouldThrowPermissionDeniedExceptionWhenGettingListAndUserHasNoPermission()
            throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.of(opportunityWithoutPermissionForUserWithId1));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(1L));
        opportunityProductsService.getOpportunityProducts(1L, "test@test.com", null);
    }

    @Test
    public void shouldReturnEmptyPageCorrectlyWhenEverythingOK() throws EntityNotFoundException,
            PermissionDeniedException, BadRequestException {
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.of(opportunityWithPermissionForUserWithId1));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(1L));
        when(this.opportunityProductsRepository.findAllByOpportunityId(1L, null)).thenReturn(Page.empty());
        assertEquals(0, opportunityProductsService.getOpportunityProducts(1L,  "test@test.com", null).getNumberOfElements());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowEntityNotFoundExceptionWhenAddingProductManuallyAndOpportunityNotFound()
            throws EntityNotFoundException, PermissionDeniedException, BadRequestException, ProductCodeOccupiedException {
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.empty());
        opportunityProductsService.addOpportunityProductManually(1L,  "test@test.com", null);
    }

    @Test(expected = PermissionDeniedException.class)
    public void shouldThrowPermissionDeniedExceptionWhenAddingProductManuallyAndUserHasNoPermission()
            throws EntityNotFoundException, PermissionDeniedException, BadRequestException, ProductCodeOccupiedException {
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.of(opportunityWithoutPermissionForUserWithId1));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(1L));
        opportunityProductsService.addOpportunityProductManually(1L, "test@test.com", null);
    }

    @Test
    public void shouldAddOpportunityProductWhenEverythingOK() throws BadRequestException, PermissionDeniedException,
            ProductCodeOccupiedException, EntityNotFoundException {
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.of(opportunityWithPermissionForUserWithId1));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(1L));
        when(accessService.assignAndVerify(dto)).thenReturn(dto);
        when(productRepository.findByCode(any())).thenReturn(Optional.empty());
        when(productRepository.findBySkuCode(any())).thenReturn(Optional.empty());
        opportunityProductsService.addOpportunityProductManually(1L,  "test@test.com", dto);
        verify(productRepository, times(1)).save(any());
        verify(opportunityProductsRepository, times(1)).save(any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowEntityNotFoundExceptionWhenRemovingProductManuallyAndOpportunityNotFound()
            throws EntityNotFoundException, PermissionDeniedException, BadRequestException{
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.empty());
        opportunityProductsService.removeOpportunityProduct(1L,  1L, "test@test.com");
    }

    @Test(expected = PermissionDeniedException.class)
    public void shouldThrowPermissionDeniedExceptionWhenRemovingProductManuallyAndUserHasNoPermission()
            throws EntityNotFoundException, PermissionDeniedException, BadRequestException{
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.of(opportunityWithoutPermissionForUserWithId1));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(1L));
        opportunityProductsService.removeOpportunityProduct(1L, 1L, "test@test.com");
    }

    @Test
    public void shouldDeleteProductOpportunityWhenProductIsNotOnlyInOpportunity() throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        OpportunityProduct opportunityProduct = new OpportunityProduct();
        Product product = new Product();
        product.setOpportunityProduct(false);
        opportunityProduct.setProduct(product);
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.of(opportunityWithPermissionForUserWithId1));
        when(this.opportunityProductsRepository.findById(1L)).thenReturn(Optional.of(opportunityProduct));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(1L));
        opportunityProductsService.removeOpportunityProduct(1L, 1L, "test@test.com");
        verify(this.opportunityProductsRepository, times(1)).deleteById(any());
        verify(this.productRepository, times(0)).deleteById(any());
    }

    @Test
    public void shouldDeleteProductTogetherWithProductOpportunityWhenOpportunityProductIsOnlyOpportunity()
            throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        OpportunityProduct opportunityProduct = new OpportunityProduct();
        Product product = new Product();
        product.setOpportunityProduct(true);
        opportunityProduct.setProduct(product);
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.of(opportunityWithPermissionForUserWithId1));
        when(this.opportunityProductsRepository.findById(1L)).thenReturn(Optional.of(opportunityProduct));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(1L));
        opportunityProductsService.removeOpportunityProduct(1L, 1L, "test@test.com");
        verify(this.opportunityProductsRepository, times(1)).deleteById(any());
        verify(this.productRepository, times(1)).save(any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowEntityNotFoundExceptionWhenEditingProductManuallyAndOpportunityNotFound()
            throws EntityNotFoundException, PermissionDeniedException, BadRequestException{
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.empty());
        opportunityProductsService.editOpportunityProduct(1L,  1L, "test@test.com", null);
    }

    @Test(expected = PermissionDeniedException.class)
    public void shouldThrowPermissionDeniedExceptionWhenEditingProductManuallyAndUserHasNoPermission()
            throws EntityNotFoundException, PermissionDeniedException, BadRequestException{
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.of(opportunityWithoutPermissionForUserWithId1));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(1L));
        opportunityProductsService.editOpportunityProduct(1L, 1L, "test@test.com", null);
    }

    @Test
    public void shouldEditOpportunityProductWhenEverythingOK () throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        OpportunityProduct opportunityProduct = new OpportunityProduct();
        OpportunityProductEditDTO opportunityProductEditDTO = new OpportunityProductEditDTO(1L, "new", 20.9, 30L, 1L, false);
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.of(opportunityWithPermissionForUserWithId1));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(1L));
        when(this.opportunityProductsRepository.findById(1L)).thenReturn(Optional.of(opportunityProduct));
        opportunityProductsService.editOpportunityProduct(1L,  1L, "test@test.com", opportunityProductEditDTO);
        verify(this.opportunityProductsRepository, times(1)).save(any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowEntityNotFoundExceptionWhenGettingForEditProductManuallyAndOpportunityNotFound()
            throws EntityNotFoundException, PermissionDeniedException, BadRequestException{
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.empty());
        opportunityProductsService.getOpportunityProductById(1L,  1L, "test@test.com");
    }

    @Test(expected = PermissionDeniedException.class)
    public void shouldThrowPermissionDeniedExceptionWhenGettingForEditProductManuallyAndUserHasNoPermission()
            throws EntityNotFoundException, PermissionDeniedException, BadRequestException{
        when(this.opportunityRepository.findById(1L)).thenReturn(Optional.of(opportunityWithoutPermissionForUserWithId1));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(1L));
        opportunityProductsService.getOpportunityProductById(1L, 1L, "test@test.com");
    }
}
