package com.devqube.crmbusinessservice.productprice.supplier.web;

import com.devqube.crmbusinessservice.AbstractIntegrationTest;
import com.devqube.crmbusinessservice.productprice.supplier.Supplier;
import com.devqube.crmbusinessservice.productprice.supplier.SupplierRepository;
import com.devqube.crmshared.web.RestPageImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
public class SupplierControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private SupplierRepository supplierRepository;

    @Override
    @Before
    public void setUp() throws IOException {
        super.setUp();
    }

    @After
    public void cleanUp() {
        this.supplierRepository.deleteAll();
    }

    @Test
    public void shouldSaveNewSupplier() {
        Supplier supplier = new Supplier();
        supplier.setName("name");
        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/supplier", List.of(supplier), Void.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void shouldNotSaveNewSupplierWhenNameOccupied() {
        Supplier supplier = new Supplier();
        supplier.setName("name");
        this.supplierRepository.save(supplier);
        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/supplier", List.of(supplier), Void.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldGetPageOfSuppliers() {
        Supplier supplier = new Supplier();
        supplier.setName("name");
        this.supplierRepository.save(supplier);

        Supplier supplier2 = new Supplier();
        supplier2.setName("name2");
        this.supplierRepository.save(supplier2);

        ResponseEntity<RestPageImpl<Supplier>> response = restTemplate.exchange(baseUrl + "/supplier",
                HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<Supplier>>() {});
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2L, response.getBody().getNumberOfElements());
    }

    @Test
    public void shouldDeleteManufacturer() {
        Supplier supplier = new Supplier();
        supplier.setName("name");
        supplier = this.supplierRepository.save(supplier);

        ResponseEntity<RestPageImpl<Supplier>> response = restTemplate.exchange(baseUrl + "/supplier/" + supplier.getId(),
                HttpMethod.DELETE, null, new ParameterizedTypeReference<RestPageImpl<Supplier>>() {});
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }
}
