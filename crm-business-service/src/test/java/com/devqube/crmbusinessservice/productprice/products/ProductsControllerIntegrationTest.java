package com.devqube.crmbusinessservice.productprice.products;

import com.devqube.crmbusinessservice.AbstractIntegrationTest;
import com.devqube.crmbusinessservice.productprice.priceBook.PriceBookRepository;
import com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook;
import com.devqube.crmbusinessservice.productprice.productPrice.ProductPriceRepository;
import com.devqube.crmbusinessservice.productprice.productPrice.model.ProductPrice;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmbusinessservice.productprice.products.web.dto.ProductDTO;
import com.devqube.crmshared.web.RestPageImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DirtiesContext
public class ProductsControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private ProductService productService;
    @Autowired
    private ProductPriceRepository productPriceRepository;
    @Autowired
    private PriceBookRepository priceBookRepository;
    @Autowired
    private ProductRepository productRepository;

    private static final String[] FIELDS_TO_COMPARE = new String[] {"name", "code", "skuCode",
            "brand", "purchasePrice", "currency", "sellPriceNet", "vat"};

    @Override
    @Before
    public void setUp() throws IOException {
        super.setUp();
        productPriceRepository.deleteAll();
        productRepository.deleteAll();
        priceBookRepository.deleteAll();
        PriceBook defaultPriceBook = PriceBook.builder()
                .id(-1L)
                .name("Cennik cen Standardowych")
                .description("Cennik cen Standardowych")
                .currency("PLN")
                .active(true)
                .isDefault(true)
                .deleted(false)
                .created(LocalDateTime.now())
                .updated(LocalDateTime.now()).build();
        priceBookRepository.save(defaultPriceBook);
    }

    @After
    public void cleanUp() {
        productPriceRepository.deleteAll();
        priceBookRepository.deleteAll();
        productRepository.deleteAll();
    }

    @Test
    public void shouldReturnValidProductsPage() {
        List<ProductDTO> expectedProducts = createExpectedProducts();
        productService.saveAll(createProducts());
        ResponseEntity<RestPageImpl<ProductDTO>> exchange = restTemplate.exchange(String.format("%s/products?page=0&size=3", baseUrl), HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<ProductDTO>>() {
        });
        assertEquals(exchange.getStatusCode(), HttpStatus.OK);
        assertNotNull(exchange.getBody());


        exchange = restTemplate.exchange(String.format("%s/products?page=1&size=3", baseUrl), HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<ProductDTO>>() {
        });
        assertEquals(exchange.getStatusCode(), HttpStatus.OK);
        assertNotNull(exchange.getBody());
        assertThat(exchange.getBody().getContent()).usingElementComparatorOnFields(FIELDS_TO_COMPARE).isEqualTo(List.of(expectedProducts.get(3), expectedProducts.get(4)));

    }

    private List<Product> createProducts() {
        Product product1 = Product.builder().name("Serwer").code("1").skuCode("1").brand("Dell").purchasePrice(2000.02).currency("PLN").sellPriceNet(2300.0).vat(23).active(true).description("some description").specification("some specification").deleted(false).opportunityProduct(false).build();
        Product product2 = Product.builder().name("Laptop").code("2").skuCode("2").brand("HP").purchasePrice(1000.0).currency("EUR").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").opportunityProduct(false).deleted(false).build();
        Product product3 = Product.builder().name("Wsparcie techniczne").code("3").skuCode("3").purchasePrice(1000.0).currency("USD").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").opportunityProduct(false).deleted(false).build();
        Product product4 = Product.builder().name("Gwrancja rozszerzona").code("4").skuCode("4").purchasePrice(1000.0).currency("GBP").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").opportunityProduct(false).deleted(false).build();
        Product product5 = Product.builder().name("Konfiguracja sieci").code("5").skuCode("5").purchasePrice(1000.0).currency("EUR").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").opportunityProduct(false).deleted(false).build();
        return List.of(product1, product2, product3, product4, product5);
    }

    private List<ProductDTO> createExpectedProducts() {
        ProductDTO product1 = ProductDTO.builder().name("Serwer").code("1").skuCode("1").brand("Dell").purchasePrice(2000.02).currency("PLN").sellPriceNet(2300.0).vat(23).active(true).description("some description").specification("some specification").photos(new ArrayList()).categories("").subcategories("").productPrices(new ArrayList<>()).categoryIds(new ArrayList()).subcategoryIds(new ArrayList()).build();
        ProductDTO product2 = ProductDTO.builder().name("Laptop").code("2").skuCode("2").brand("HP").purchasePrice(1000.0).currency("EUR").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").photos(new ArrayList()).categories("").subcategories("").productPrices(new ArrayList<>()).categoryIds(new ArrayList()).subcategoryIds(new ArrayList()).build();
        ProductDTO product3 = ProductDTO.builder().name("Wsparcie techniczne").code("3").skuCode("3").purchasePrice(1000.0).currency("USD").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").photos(new ArrayList()).categories("").subcategories("").categoryIds(new ArrayList()).productPrices(new ArrayList<>()).subcategoryIds(new ArrayList()).build();
        ProductDTO product4 = ProductDTO.builder().name("Gwrancja rozszerzona").code("4").skuCode("4").purchasePrice(1000.0).currency("GBP").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").photos(new ArrayList()).categories("").subcategories("").categoryIds(new ArrayList()).productPrices(new ArrayList<>()).subcategoryIds(new ArrayList()).build();
        ProductDTO product5 = ProductDTO.builder().name("Konfiguracja sieci").code("5").skuCode("5").purchasePrice(1000.0).currency("EUR").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").photos(new ArrayList()).categories("").subcategories("").categoryIds(new ArrayList()).productPrices(new ArrayList<>()).subcategoryIds(new ArrayList()).build();
        return List.of(product1, product2, product3, product4, product5);
    }

    @Test
    public void shouldReturnValidProductsPageForPriceBook() {
        List<ProductDTO> expectedProducts = createExpectedProductsForPriceBook();

        PriceBook priceBook = priceBookRepository.save(createPriceBook());
        List<Product> products = productRepository.saveAll(createProductsForPriceBook());
        List<ProductPrice> productPrices = productPriceRepository.saveAll(createProductPrices(priceBook, products));


        ResponseEntity<RestPageImpl<ProductDTO>> exchange = restTemplate.exchange(baseUrl + "/products/for-price-book/" + priceBook.getId() + "?page=0&size=3", HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<ProductDTO>>() {
        });
        assertEquals(exchange.getStatusCode(), HttpStatus.OK);
        assertNotNull(exchange.getBody());
        assertThat(exchange.getBody().getContent()).usingElementComparatorIgnoringFields("id").isEqualTo(List.of(expectedProducts.get(0), expectedProducts.get(1), expectedProducts.get(2)));


        exchange = restTemplate.exchange(baseUrl + "/products/for-price-book/" + priceBook.getId() + "?page=1&size=3", HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<ProductDTO>>() {
        });
        assertEquals(exchange.getStatusCode(), HttpStatus.OK);
        assertNotNull(exchange.getBody());
        assertThat(exchange.getBody().getContent()).usingElementComparatorIgnoringFields("id").isEqualTo(List.of(expectedProducts.get(3), expectedProducts.get(4)));

    }

    private List<ProductPrice> createProductPrices(PriceBook priceBook, List<Product> products) {
        return products.stream().map(c ->
                ProductPrice.builder().product(c).priceBook(priceBook).price(Double.parseDouble("1" + c.getSellPriceNet())).active(true).build())
                .collect(Collectors.toList());
    }

    private PriceBook createPriceBook() {
        return PriceBook.builder().name("test").currency("PLN").active(true).isDefault(false).deleted(false).build();
    }

    private List<Product> createProductsForPriceBook() {
        Product product1 = Product.builder().name("Serwer").code("1").skuCode("1").brand("Dell").purchasePrice(2000.02).currency("PLN").sellPriceNet(2300.0).vat(23).active(true).description("some description").specification("some specification").deleted(false).build();
        Product product2 = Product.builder().name("Laptop").code("2").skuCode("2").brand("HP").purchasePrice(1000.0).currency("EUR").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").deleted(false).build();
        Product product3 = Product.builder().name("Wsparcie techniczne").code("3").skuCode("3").purchasePrice(1000.0).currency("USD").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").deleted(false).build();
        Product product4 = Product.builder().name("Gwrancja rozszerzona").code("4").skuCode("4").purchasePrice(1000.0).currency("GBP").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").deleted(false).build();
        Product product5 = Product.builder().name("Konfiguracja sieci").code("5").skuCode("5").purchasePrice(1000.0).currency("EUR").sellPriceNet(1200.0).vat(23).active(false).description("some description").specification("some specification").deleted(false).build();
        return List.of(product1, product2, product3, product4, product5);
    }

    private List<ProductDTO> createExpectedProductsForPriceBook() {
        ProductDTO product1 = ProductDTO.builder().name("Serwer").code("1").skuCode("1").brand("Dell").purchasePrice(2000.02).currency("PLN").sellPriceNet(12300.0).vat(23).active(true).description("some description").specification("some specification").photos(new ArrayList()).categories("").subcategories("").productPrices(new ArrayList<>()).categoryIds(new ArrayList()).subcategoryIds(new ArrayList()).build();
        ProductDTO product2 = ProductDTO.builder().name("Laptop").code("2").skuCode("2").brand("HP").purchasePrice(1000.0).currency("PLN").sellPriceNet(11200.0).vat(23).active(false).description("some description").specification("some specification").photos(new ArrayList()).categories("").subcategories("").productPrices(new ArrayList<>()).categoryIds(new ArrayList()).subcategoryIds(new ArrayList()).build();
        ProductDTO product3 = ProductDTO.builder().name("Wsparcie techniczne").code("3").skuCode("3").purchasePrice(1000.0).currency("PLN").sellPriceNet(11200.0).vat(23).active(false).description("some description").specification("some specification").photos(new ArrayList()).categories("").subcategories("").categoryIds(new ArrayList()).productPrices(new ArrayList<>()).subcategoryIds(new ArrayList()).build();
        ProductDTO product4 = ProductDTO.builder().name("Gwrancja rozszerzona").code("4").skuCode("4").purchasePrice(1000.0).currency("PLN").sellPriceNet(11200.0).vat(23).active(false).description("some description").specification("some specification").photos(new ArrayList()).categories("").subcategories("").categoryIds(new ArrayList()).productPrices(new ArrayList<>()).subcategoryIds(new ArrayList()).build();
        ProductDTO product5 = ProductDTO.builder().name("Konfiguracja sieci").code("5").skuCode("5").purchasePrice(1000.0).currency("PLN").sellPriceNet(11200.0).vat(23).active(false).description("some description").specification("some specification").photos(new ArrayList()).categories("").subcategories("").categoryIds(new ArrayList()).productPrices(new ArrayList<>()).subcategoryIds(new ArrayList()).build();
        return List.of(product1, product2, product3, product4, product5);
    }

}
