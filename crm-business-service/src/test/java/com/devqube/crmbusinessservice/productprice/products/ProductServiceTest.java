package com.devqube.crmbusinessservice.productprice.products;

import com.devqube.crmbusinessservice.goal.GoalRepository;
import com.devqube.crmbusinessservice.productprice.category.ProductSubcategoryRepository;
import com.devqube.crmbusinessservice.productprice.exception.ObjectUsedInGoalException;
import com.devqube.crmbusinessservice.productprice.exception.PhotoOrderException;
import com.devqube.crmbusinessservice.productprice.exception.ProductCodeOccupiedException;
import com.devqube.crmbusinessservice.productprice.exception.TooManyPhotosException;
import com.devqube.crmbusinessservice.productprice.manufacturer.ManufacturerRepository;
import com.devqube.crmbusinessservice.productprice.productPrice.ProductPriceService;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmbusinessservice.productprice.products.opportunityProducts.OpportunityProduct;
import com.devqube.crmbusinessservice.productprice.products.web.dto.PhotoDTO;
import com.devqube.crmbusinessservice.productprice.products.web.dto.ProductDTO;
import com.devqube.crmbusinessservice.productprice.supplier.SupplierRepository;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.association.RemoveCrmObject;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.association.dto.AssociationPropertyDto;
import com.devqube.crmshared.association.dto.AssociationPropertyTypeEnum;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.IncorrectNotificationTypeException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.file.InternalFileClient;
import com.devqube.crmshared.file.dto.FileInfoDto;
import com.devqube.crmshared.history.dto.ObjectHistoryType;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmshared.kafka.dto.KafkaMessageData;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.notification.NotificationMsgType;
import com.devqube.crmshared.search.CrmObjectType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;
    @Mock
    private AccessService accessService;
    @Mock
    private ManufacturerRepository manufacturerRepository;
    @Mock
    SupplierRepository supplierRepository;
    @Mock
    private ModelMapper modelMapper;
    @Mock
    private AssociationClient associationClient;
    @Mock
    private InternalFileClient internalFileClient;
    @Mock
    private GoalRepository goalRepository;
    @Mock
    private ProductPriceService productPriceService;
    @Mock
    private ProductSubcategoryRepository productSubcategoryRepository;


    @InjectMocks
    private ProductService productService;

    private Product product;
    private ProductDTO productDTO;

    @Before
    public void setUp() {
        this.product = new Product();
        this.product.setName("name");
        this.product.setCode("1");

        this.productDTO = new ProductDTO();
        this.productDTO.setName("name");
        this.productDTO.setCode("1");
        setDefaultKafkaServiceToRemoveCrmObject();
    }

    @Test
    public void shouldSaveProductWhenProductCodeNotOccupied() throws ProductCodeOccupiedException, PhotoOrderException, BadRequestException, EntityNotFoundException {
        ProductDTO dto = ProductDTO.builder().name("name").code("1").build();
        when(productRepository.findByCode(any())).thenReturn(Optional.empty());
        when(accessService.assignAndVerify(eq(dto))).thenReturn(productDTO);
        when(productRepository.save(any())).thenReturn(product);
        when(associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(any(), any(), any())).thenReturn(new ArrayList<>());
        when(modelMapper.map(any(), any())).thenReturn(new Product());
        Product savedProduct = productService.save(dto);
        assertEquals(product.getCode(), savedProduct.getCode());
    }

    @Test(expected = ProductCodeOccupiedException.class)
    public void shouldThrowExceptionWhenSavingAndProductCodeOccupied() throws ProductCodeOccupiedException, PhotoOrderException, BadRequestException, EntityNotFoundException {
        ProductDTO dto = ProductDTO.builder().name("name").code("1").build();
        when(productRepository.findByCode(any())).thenReturn(Optional.of(product));
        productService.save(dto);
    }

    @Test
    public void shouldDeleteExistingProduct() throws EntityNotFoundException, BadRequestException, ObjectUsedInGoalException {
        when(productRepository.findById(1L)).thenReturn(Optional.of(product));
        when(goalRepository.countByProductsId(anyLong())).thenReturn(0L);
        productService.deleteById(1L);
        verify(productRepository, times(1)).save(any());
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenProductUsedInOpportunity() throws BadRequestException, EntityNotFoundException, ObjectUsedInGoalException {
        Product product = new Product();
        product.setOpportunityProducts(List.of(new OpportunityProduct()));
        when(productRepository.findById(1L)).thenReturn(Optional.of(product));
        productService.deleteById(1L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenDeletingProductNotFound() throws EntityNotFoundException, BadRequestException, ObjectUsedInGoalException {
        when(productRepository.findById(1L)).thenReturn(Optional.empty());
        productService.deleteById(1L);
    }

    @Test
    public void shouldDeleteManyProducts() throws EntityNotFoundException, BadRequestException, ObjectUsedInGoalException {
        Product product2 = new Product();
        product2.setName("name");
        product2.setCode("2");
        when(productRepository.findById(1L)).thenReturn(Optional.of(product));
        when(productRepository.findById(2L)).thenReturn(Optional.of(product2));
        when(goalRepository.countByProductsId(anyLong())).thenReturn(0L);
        productService.deleteByIds(List.of(1L, 2L));
        verify(productRepository, times(2)).save(any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenDeletingManyProductsAndOneNotFound() throws EntityNotFoundException, BadRequestException, ObjectUsedInGoalException {
        when(productRepository.findById(1L)).thenReturn(Optional.of(product));
        when(productRepository.findById(2L)).thenReturn(Optional.empty());
        when(goalRepository.countByProductsId(anyLong())).thenReturn(0L);
        productService.deleteByIds(List.of(1L, 2L, 3L));
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenIdIsNull() throws ProductCodeOccupiedException, EntityNotFoundException, BadRequestException, PhotoOrderException, TooManyPhotosException {
        productService.modify(new ProductDTO(), null);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenProductIsNull() throws ProductCodeOccupiedException, EntityNotFoundException, BadRequestException, PhotoOrderException, TooManyPhotosException {
        productService.modify(null, 1L);
    }

    @Test(expected = ProductCodeOccupiedException.class)
    public void shouldNotModifyWhenOtherProductWithSameCodeExists() throws ProductCodeOccupiedException, BadRequestException, EntityNotFoundException, PhotoOrderException, TooManyPhotosException {
        product.setId(2L);
        when(productRepository.findById(1L)).thenReturn(Optional.of(product));
        when(productRepository.findByCode("1")).thenReturn(Optional.of(product));
        ProductDTO productDTO = new ProductDTO();
        productDTO.setCode("1");
        productService.modify(productDTO, 1L);
    }

    @Test(expected = ProductCodeOccupiedException.class)
    public void shouldNotModifyWhenOtherProductWithSameSkuCodeExists() throws ProductCodeOccupiedException, BadRequestException, EntityNotFoundException, PhotoOrderException, TooManyPhotosException {
        product.setId(2L);
        when(productRepository.findById(1L)).thenReturn(Optional.of(product));
        when(productRepository.findByCode("1")).thenReturn(Optional.empty());
        when(productRepository.findBySkuCode("1")).thenReturn(Optional.of(product));
        ProductDTO productDTO = new ProductDTO();
        productDTO.setCode("1");
        productDTO.setSkuCode("1");
        productService.modify(productDTO, 1L);
    }

    @Test
    public void shouldModifyProductWhenEverythingIsOk() throws ProductCodeOccupiedException, BadRequestException, EntityNotFoundException, PhotoOrderException, TooManyPhotosException {
        when(productRepository.findById(1L)).thenReturn(Optional.of(product));
        when(productRepository.findByCode("1")).thenReturn(Optional.empty());
        when(modelMapper.map(product, ProductDTO.class)).thenReturn(productDTO);
        when(accessService.assignAndVerify(any(), any())).thenReturn(productDTO);
        when(productRepository.findBySkuCode("1")).thenReturn(Optional.empty());
        ProductDTO productDTO = new ProductDTO();
        productDTO.setCode("1");
        productDTO.setSkuCode("1");
        productService.modify(productDTO, 1L);
        verify(productRepository, times(1)).save(any());
    }

    @Test(expected = PhotoOrderException.class)
    public void shouldThrowPhotoOrderExceptionWhenSavingPhotosWithRepeatedOrderNumber() throws BadRequestException, ProductCodeOccupiedException, PhotoOrderException, EntityNotFoundException {
        ProductDTO dto = ProductDTO.builder().name("name").code("1").photos(
                Arrays.asList(
                        PhotoDTO.builder().id("1").tempFile(true).order(1).build(),
                        PhotoDTO.builder().id("2").tempFile(true).order(1).build()
                )
        ).build();
        when(productRepository.findByCode(any())).thenReturn(Optional.empty());
        when(accessService.assignAndVerify(eq(dto))).thenReturn(dto);
        when(productRepository.save(any())).thenReturn(product);
        when(modelMapper.map(any(), any())).thenReturn(new Product());
        productService.save(dto);
    }

    @Test(expected = PhotoOrderException.class)
    public void shouldThrowPhotoOrderExceptionWhenModifyingPhotosWithRepeatedOrderNumber() throws PhotoOrderException, TooManyPhotosException, BadRequestException, EntityNotFoundException, ProductCodeOccupiedException {
        ProductDTO dto = ProductDTO.builder().name("name").skuCode("1").code("1").photos(
                Arrays.asList(
                        PhotoDTO.builder().id("1").tempFile(false).order(2).build(),
                        PhotoDTO.builder().id("2").tempFile(true).order(2).build()
                )
        ).build();
        Product product = dto.toModel(new Product());
        when(productRepository.findById(eq(1L))).thenReturn(Optional.of(product));
        when(productRepository.findByCode(any())).thenReturn(Optional.empty());
        when(productRepository.findBySkuCode(any())).thenReturn(Optional.empty());

        when(accessService.assignAndVerify(any(), any())).thenReturn(dto);

        when(associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(eq(CrmObjectType.product), eq(CrmObjectType.file), eq(1L)))
                .thenReturn(Arrays.asList(
                        AssociationDto.builder().destinationId(1L).associationProperties(Arrays.asList(
                                AssociationPropertyDto.builder().type(AssociationPropertyTypeEnum.PRODUCT_PHOTO_ORDER).value("2").build()
                        )).build()
                ));
        when(internalFileClient.getFileInfo(anyList())).thenReturn(Arrays.asList(
                FileInfoDto.builder().fileId(1L).build()
        ));

        when(modelMapper.map(any(), any())).thenReturn(ProductDTO.builder().name("name").skuCode("1").code("1").build());

        productService.modify(dto, 1L);
    }

    @Test
    public void shouldSavePhotosWhenAddingNewProduct() throws BadRequestException, ProductCodeOccupiedException, PhotoOrderException, EntityNotFoundException {
        ProductDTO dto = ProductDTO.builder().name("name").code("1").photos(
                Arrays.asList(
                        PhotoDTO.builder().id("1").tempFile(true).order(1).build(),
                        PhotoDTO.builder().id("2").tempFile(true).order(2).build()
                )
        ).build();
        when(productRepository.findByCode(any())).thenReturn(Optional.empty());
        when(accessService.assignAndVerify(eq(dto))).thenReturn(dto);
        when(productRepository.save(any())).thenReturn(product);
        when(internalFileClient.setTempFileInfo(eq(1L), any())).thenReturn(FileInfoDto.builder().fileId(1L).build());
        when(internalFileClient.setTempFileInfo(eq(2L), any())).thenReturn(FileInfoDto.builder().fileId(2L).build());
        when(associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(any(), any(), any())).thenReturn(new ArrayList<>());
        when(modelMapper.map(any(), any())).thenReturn(new Product());
        Product savedProduct = productService.save(dto);
        assertEquals(product.getCode(), savedProduct.getCode());
        verify(associationClient, times(2)).createAssociation(any());
        verify(internalFileClient, times(2)).setTempFileInfo(any(), any());
    }

    @Test
    public void shouldSavePhotosWhenModifyingProduct() throws PhotoOrderException, TooManyPhotosException, BadRequestException, EntityNotFoundException, ProductCodeOccupiedException {
        ProductDTO dto = ProductDTO.builder().name("name").skuCode("1").code("1").photos(
                Arrays.asList(
                        PhotoDTO.builder().id("1").tempFile(false).order(1).build(),
                        PhotoDTO.builder().id("2").tempFile(true).order(2).build()
                )
        ).build();
        Product product = dto.toModel(new Product());
        when(productRepository.findById(eq(1L))).thenReturn(Optional.of(product));
        when(productRepository.findByCode(any())).thenReturn(Optional.empty());
        when(productRepository.findBySkuCode(any())).thenReturn(Optional.empty());

        when(accessService.assignAndVerify(any(), any())).thenReturn(dto);
        when(internalFileClient.setTempFileInfo(eq(2L), any())).thenReturn(FileInfoDto.builder().fileId(4L).build());

        when(associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(eq(CrmObjectType.product), eq(CrmObjectType.file), eq(1L)))
                .thenReturn(Arrays.asList(
                        AssociationDto.builder().destinationId(1L).associationProperties(Arrays.asList(
                                AssociationPropertyDto.builder().type(AssociationPropertyTypeEnum.PRODUCT_PHOTO_ORDER).value("2").build()
                        )).build()
                ));
        when(internalFileClient.getFileInfo(anyList())).thenReturn(Arrays.asList(
                FileInfoDto.builder().fileId(1L).build()
        ));

        when(modelMapper.map(any(), any())).thenReturn(ProductDTO.builder().name("name").skuCode("1").code("1").build());

        productService.modify(dto, 1L);
        verify(productRepository, times(1)).save(any());
        verify(associationClient, times(1)).createAssociation(any());
        verify(internalFileClient, times(1)).deleteFile(eq(1L));
        verify(internalFileClient, times(1)).setTempFileInfo(eq(2L), any());
    }

    private void setDefaultKafkaServiceToRemoveCrmObject() {
        RemoveCrmObject.getInstance().setKafkaService(new KafkaService() {
            @Override
            public void send(@NotNull Long aLong, @NotNull KafkaMsgType kafkaMsgType, Set<KafkaMessageData> set) throws KafkaSendMessageException {}
            @Override
            public void send(KafkaMsgType kafkaMsgType, String s) throws KafkaSendMessageException {}
            @Override
            public void addNotification(@NotNull Long aLong, @NotNull NotificationMsgType notificationMsgType, Set<KafkaMessageData> set) throws KafkaSendMessageException {}
            @Override
            public void addGroupNotification(@NotNull Set<Long> set, @NotNull NotificationMsgType notificationMsgType, Set<KafkaMessageData> set1) throws KafkaSendMessageException, IncorrectNotificationTypeException {}
            @Override
            public void sendWebsocket(String s, String s1) throws KafkaSendMessageException {}
            @Override
            public void sendRemoveAssociation(CrmObjectType crmObjectType, Long aLong) throws KafkaSendMessageException {}
            @Override
            public void saveHistory(ObjectHistoryType type, String id, String json, LocalDateTime modifiedTime, String email) throws KafkaSendMessageException {}
        });
    }
}
