package com.devqube.crmbusinessservice.productprice.category;

import com.devqube.crmbusinessservice.AbstractIntegrationTest;
import com.devqube.crmbusinessservice.productprice.category.web.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DirtiesContext
public class ProductsCategoryControllerIntegrationTest extends AbstractIntegrationTest {


    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private ProductSubcategoryRepository productSubcategoryRepository;

    ProductCategory category_1;
    ProductCategory category_2;
    ProductSubcategory category_1_subcategory_1;
    ProductSubcategory category_1_subcategory_2;
    ProductSubcategory category_1_subcategory_3;

    @Override
    @Before
    public void setUp() throws IOException {
        super.setUp();
        this.saveTestCategories();
    }

    private void saveTestCategories() {
        category_1 = ProductCategory.builder().name("category_1").build();
        category_2 = ProductCategory.builder().name("category_2").build();
        category_1 = productCategoryRepository.save(category_1);
        category_2 = productCategoryRepository.save(category_2);
        category_1_subcategory_1 = ProductSubcategory.builder().name("category_1_subcategory_1").productCategory(category_1).build();
        category_1_subcategory_2 = ProductSubcategory.builder().name("category_1_subcategory_2").productCategory(category_1).build();
        category_1_subcategory_3 = ProductSubcategory.builder().name("category_1_subcategory_3").productCategory(category_1).build();

        ProductSubcategory category_2_subcategory_1 = ProductSubcategory.builder().name("category_2_subcategory_1").productCategory(category_2).build();
        ProductSubcategory category_2_subcategory_2 = ProductSubcategory.builder().name("category_2_subcategory_2").productCategory(category_2).build();
        ProductSubcategory category_2_subcategory_3 = ProductSubcategory.builder().name("category_2_subcategory_3").productCategory(category_2).build();

        Set<ProductSubcategory> subcategories_1 = Set.of(category_1_subcategory_1, category_1_subcategory_2, category_1_subcategory_3);
        Set<ProductSubcategory> subcategories_2 = Set.of(category_2_subcategory_1, category_2_subcategory_2, category_2_subcategory_3);

        category_1.setSubcategories(subcategories_1);
        category_2.setSubcategories(subcategories_2);

        productSubcategoryRepository.saveAll(subcategories_1);
        productSubcategoryRepository.saveAll(subcategories_2);

    }

    @After
    public void cleanUp() {
        productCategoryRepository.deleteAll();
        productSubcategoryRepository.deleteAll();
    }

    @Test
    public void getAllCategories() {
        ResponseEntity<List<ProductCategoryDto>> exchange = restTemplate.exchange(String.format("%s/products/categories", baseUrl), HttpMethod.GET, null, new ParameterizedTypeReference<List<ProductCategoryDto>>() {
        });
        assertEquals(exchange.getStatusCode(), HttpStatus.OK);
        assertNotNull(exchange.getBody());
        assertThat(exchange.getBody().size()).isEqualTo(2);
        List<String> categoryList = exchange.getBody().stream().map(ProductCategoryDto::getName).collect(Collectors.toList());
        assertThat(categoryList).contains("category_1");
        assertThat(categoryList).contains("category_2");
    }

    @Test
    public void getCategoriesWithSubcategories() {
        ResponseEntity<List<ProductCategoryWithSubcategoryDto>> exchange = restTemplate.exchange(String.format("%s/products/categories-with-subcategories/%s", baseUrl, category_1.getId()), HttpMethod.GET, null, new ParameterizedTypeReference<List<ProductCategoryWithSubcategoryDto>>() {
        });
        assertEquals(exchange.getStatusCode(), HttpStatus.OK);
        assertNotNull(exchange.getBody());
        assertThat(exchange.getBody().size()).isEqualTo(1);
        List<String> categoryList = exchange.getBody().stream().map(ProductCategoryWithSubcategoryDto::getName).collect(Collectors.toList());
        assertThat(categoryList).containsExactly("category_1");
        List<String> subcategoryList = exchange.getBody().get(0).getSubcategories().stream().map(ProductSubcategoryDto::getName).collect(Collectors.toList());
        assertThat(subcategoryList).containsExactlyInAnyOrder("category_1_subcategory_1", "category_1_subcategory_2", "category_1_subcategory_3");
    }

    @Test
    public void saveCategories() {
        ProductCategorySaveDto category_1 = ProductCategorySaveDto.builder().id(this.category_1.getId()).name("category_1_new_name").build();
        ProductCategorySaveDto category_2 = ProductCategorySaveDto.builder().id(this.category_2.getId()).name(this.category_2.getName()).toDelete(true).build();
        ProductCategorySaveDto category_3 = ProductCategorySaveDto.builder().name("category_3").build();
        List<ProductCategorySaveDto> requestBody = List.of(category_1, category_2, category_3);

        ResponseEntity<List<ProductCategoryDto>> exchange = restTemplate.exchange(String.format("%s/products/categories", baseUrl), HttpMethod.POST,  new HttpEntity<>(requestBody), new ParameterizedTypeReference<List<ProductCategoryDto>>() {
        });

        assertEquals(exchange.getStatusCode(), HttpStatus.OK);
        assertNotNull(exchange.getBody());
        assertThat(exchange.getBody().size()).isEqualTo(2);
        List<String> categoryList = exchange.getBody().stream().map(ProductCategoryDto::getName).collect(Collectors.toList());
        assertThat(categoryList).contains("category_1_new_name");
        assertThat(categoryList).contains("category_3");
    }

    @Test
    public void saveCategoriesWithSubcategories() {

        ProductSubcategorySaveDto subcategory_1 = ProductSubcategorySaveDto.builder().id(this.category_1_subcategory_1.getId()).name("category_1_subcategory_1_new_name").categoryId(this.category_1.getId()).build();
        ProductSubcategorySaveDto subcategory_2 = ProductSubcategorySaveDto.builder().id(this.category_1_subcategory_2.getId()).name(this.category_1_subcategory_2.getName()).categoryId(this.category_1.getId()).toDelete(true).build();
        ProductSubcategorySaveDto subcategory_3 = ProductSubcategorySaveDto.builder().name("category_1_new_subcategory").categoryId(this.category_1.getId()).build();

        List<ProductSubcategorySaveDto> requestBody = List.of(subcategory_1, subcategory_2, subcategory_3);

        ResponseEntity<List<ProductCategoryWithSubcategoryDto>> exchange = restTemplate.exchange(String.format("%s/products/subcategories", baseUrl), HttpMethod.POST, new HttpEntity<>(requestBody), new ParameterizedTypeReference<List<ProductCategoryWithSubcategoryDto>>() {
        });
        List<ProductCategoryWithSubcategoryDto> response = exchange.getBody();

        assertThat(response.size()).isEqualTo(1);
        List<String> categoryList = exchange.getBody().stream().map(ProductCategoryWithSubcategoryDto::getName).collect(Collectors.toList());
        assertThat(categoryList).containsExactly("category_1");
        List<String> subcategoryList = exchange.getBody().get(0).getSubcategories().stream().map(ProductSubcategoryDto::getName).collect(Collectors.toList());
        assertThat(subcategoryList).containsExactlyInAnyOrder("category_1_subcategory_1_new_name", "category_1_new_subcategory", "category_1_subcategory_3");
    }

    @Test
    public void getAllCategoriesWithSubcategories() {
        ResponseEntity<List<ProductCategoryWithSubcategoryDto>> exchange = restTemplate.exchange(String.format("%s/products/categories-with-subcategories", baseUrl), HttpMethod.GET, null, new ParameterizedTypeReference<List<ProductCategoryWithSubcategoryDto>>() {
        });
        List<ProductCategoryWithSubcategoryDto> response = exchange.getBody();
        assertEquals(exchange.getStatusCode(), HttpStatus.OK);
        assertNotNull(response);
        assertThat(response.size()).isEqualTo(2);
        List<String> categoryList = response.stream().map(ProductCategoryWithSubcategoryDto::getName).collect(Collectors.toList());
        assertThat(categoryList).containsExactlyInAnyOrder("category_1", "category_2");

        ProductCategoryWithSubcategoryDto category_1 = response.stream().filter(c -> c.getName().equals("category_1")).findFirst().get();
        List<String> subcategoryList = category_1.getSubcategories().stream().map(ProductSubcategoryDto::getName).collect(Collectors.toList());
        assertThat(subcategoryList).containsExactlyInAnyOrder("category_1_subcategory_1", "category_1_subcategory_2", "category_1_subcategory_3");

        ProductCategoryWithSubcategoryDto category_2 = response.stream().filter(c -> c.getName().equals("category_2")).findFirst().get();
        subcategoryList = category_2.getSubcategories().stream().map(ProductSubcategoryDto::getName).collect(Collectors.toList());
        assertThat(subcategoryList).containsExactlyInAnyOrder("category_2_subcategory_1", "category_2_subcategory_2", "category_2_subcategory_3");
    }


}
