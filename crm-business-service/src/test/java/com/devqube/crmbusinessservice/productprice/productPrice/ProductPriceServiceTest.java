package com.devqube.crmbusinessservice.productprice.productPrice;

import com.devqube.crmbusinessservice.productprice.priceBook.PriceBookRepository;
import com.devqube.crmbusinessservice.productprice.priceBook.model.PriceBook;
import com.devqube.crmbusinessservice.productprice.productPrice.model.ProductPrice;
import com.devqube.crmbusinessservice.productprice.productPrice.web.dto.ProductPriceDto;
import com.devqube.crmbusinessservice.productprice.products.ProductRepository;
import com.devqube.crmbusinessservice.productprice.products.model.Product;
import com.devqube.crmshared.exception.EntityNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductPriceServiceTest {
    @Mock
    private ProductPriceRepository productPriceRepository;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private PriceBookRepository priceBookRepository;

    @InjectMocks
    private ProductPriceService productPriceService;

    private Product product;
    private PriceBook priceBook;
    private List<ProductPrice> productPriceList = new ArrayList<>();

    @Before
    public void setUp() {
        product = Product.builder().id(-1L).build();
        productPriceList = new ArrayList<>();
        priceBook = PriceBook.builder().id(1L).build();
        productPriceList.add(ProductPrice.builder().id(1L).priceBook(priceBook).product(product).build());
        productPriceList.add(ProductPrice.builder().id(2L).priceBook(priceBook).product(product).build());
        productPriceList.add(ProductPrice.builder().id(3L).priceBook(priceBook).product(product).build());
    }

    @Test
    public void shouldGetProductsByProductId() {
        when(productPriceRepository.getAllByProductId(eq(product.getId()))).thenReturn(productPriceList);
        List<ProductPriceDto> result = productPriceService.getProductsPriceByProductId(product.getId());
        assertNotNull(result);
        assertEquals(3, result.size());
        assertTrue(result.stream().anyMatch(c -> c.getId().equals(1L)));
        assertTrue(result.stream().anyMatch(c -> c.getId().equals(2L)));
        assertTrue(result.stream().anyMatch(c -> c.getId().equals(3L)));
    }

    @Test
    public void shouldSetProductPricesForProductId() throws EntityNotFoundException {
        List<ProductPriceDto> priceDtoList = productPriceList.stream().map(ProductPriceDto::fromModel).collect(Collectors.toList());
        when(productRepository.findById(eq(product.getId()))).thenReturn(Optional.of(product));
        when(productPriceRepository.saveAll(anyList())).thenReturn(productPriceList);
        when(priceBookRepository.findAllByDeletedFalseAndIsDefaultTrue()).thenReturn(Collections.singletonList(PriceBook.builder().id(1L).build()));

        List<ProductPriceDto> result = productPriceService.setProductPricesForProductId(product.getId(), priceDtoList);

        verify(productPriceRepository, times(1)).deleteAllByProductId(eq(product.getId()));
        verify(productRepository, times(1)).findById(eq(product.getId()));
        verify(productPriceRepository, times(1)).saveAll(anyList());

        assertNotNull(result);
        assertEquals(3, result.size());
        assertTrue(result.stream().anyMatch(c -> c.getId().equals(1L)));
        assertTrue(result.stream().anyMatch(c -> c.getId().equals(2L)));
        assertTrue(result.stream().anyMatch(c -> c.getId().equals(3L)));
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowNotFoundExceptionWhenProductNotFoundWhenSetProductPricesForProductId() throws EntityNotFoundException {
        List<ProductPriceDto> priceDtoList = productPriceList.stream().map(ProductPriceDto::fromModel).collect(Collectors.toList());
        when(productRepository.findById(eq(product.getId()))).thenReturn(Optional.empty());

        List<ProductPriceDto> result = productPriceService.setProductPricesForProductId(product.getId(), priceDtoList);
    }

    @Test
    public void shouldDeleteProductPricesByProductId() {
        productPriceService.deleteProductPricesByProductId(product.getId());
        verify(productPriceRepository, times(1)).deleteAllByProductId(eq(product.getId()));
    }
}
