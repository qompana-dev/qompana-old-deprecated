package com.devqube.crmbusinessservice.productprice.manufacturer.web;

import com.devqube.crmbusinessservice.AbstractIntegrationTest;
import com.devqube.crmbusinessservice.productprice.manufacturer.Manufacturer;
import com.devqube.crmbusinessservice.productprice.manufacturer.ManufacturerRepository;
import com.devqube.crmshared.web.RestPageImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
public class ManufacturerControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private ManufacturerRepository manufacturerRepository;

    @Override
    @Before
    public void setUp() throws IOException {
        super.setUp();
    }

    @After
    public void cleanUp() {
        this.manufacturerRepository.deleteAll();
    }

    @Test
    public void shouldSaveNewManufacturer() {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName("name");
        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/manufacturer", List.of(manufacturer), Void.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void shouldNotSaveNewManufacturerWhenNameOccupied() {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName("name");
        this.manufacturerRepository.save(manufacturer);
        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/manufacturer", List.of(manufacturer),
                Void.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldGetPageOfManufacturers() {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName("name");
        this.manufacturerRepository.save(manufacturer);


        Manufacturer manufacturer2 = new Manufacturer();
        manufacturer2.setName("name2");
        this.manufacturerRepository.save(manufacturer2);

        ResponseEntity<RestPageImpl<Manufacturer>> response = restTemplate.exchange(baseUrl + "/manufacturer",
                HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<Manufacturer>>() {});
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2L, response.getBody().getNumberOfElements());
    }

    @Test
    public void shouldDeleteManufacturer() {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName("name");
        manufacturer = this.manufacturerRepository.save(manufacturer);

        ResponseEntity<RestPageImpl<Manufacturer>> response = restTemplate.exchange(baseUrl + "/manufacturer/" + manufacturer.getId(),
                HttpMethod.DELETE, null, new ParameterizedTypeReference<RestPageImpl<Manufacturer>>() {});
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }
}
