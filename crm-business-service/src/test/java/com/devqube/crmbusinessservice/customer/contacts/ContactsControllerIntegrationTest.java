package com.devqube.crmbusinessservice.customer.contacts;

import com.devqube.crmbusinessservice.AbstractIntegrationTest;
import com.devqube.crmbusinessservice.customer.contact.ContactLinkRepository;
import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.contact.model.ContactLink;
import com.devqube.crmbusinessservice.customer.contact.model.EmploymentHistory;
import com.devqube.crmbusinessservice.customer.contact.web.dto.ContactCreateDto;
import com.devqube.crmbusinessservice.customer.contact.web.dto.ContactLinkDto;
import com.devqube.crmbusinessservice.customer.contact.web.dto.CreateContactLinkDto;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.web.RestPageImpl;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.netflix.ribbon.StaticServerList;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ActiveProfiles({"test", "wiremock"})
@ContextConfiguration(classes = {ContactsControllerIntegrationTest.LocalClientConfiguration.class})
public class ContactsControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private ContactLinkRepository contactLinkRepository;

    @Autowired
    private CustomerRepository customerRepository;

    private Customer customer;

    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(WireMockConfiguration.options().dynamicPort());
    private HttpHeaders headers = null;

    @Before
    public void beforeEach() throws IOException {
        super.setUp();
        this.contactLinkRepository.deleteAll();
        this.customerRepository.deleteAll();
        this.contactRepository.deleteAll();

        stubFor(get(urlEqualTo("/accounts/me/id"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("-1")));

        headers = new HttpHeaders();
        headers.set("Logged-Account-Email", "jan.kowalski@devqube.com");

        customer = createCustomer();
    }

    @Test
    public void shouldReturn404IfContactNotExistWhenCreateContactLink() {
        Contact contact1 = contactRepository.save(createContact());

        ResponseEntity<ContactLinkDto> response = restTemplate.postForEntity(baseUrl + "/contacts/" + contact1.getId() + "/link/" + (contact1.getId() + 1), new CreateContactLinkDto("note"), ContactLinkDto.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

        ResponseEntity<ContactLinkDto> response2 = restTemplate.postForEntity(baseUrl + "/contacts/" + (contact1.getId() + 1) + "/link/" + contact1.getId(), new CreateContactLinkDto("note"), ContactLinkDto.class);
        assertEquals(HttpStatus.NOT_FOUND, response2.getStatusCode());
    }

    @Test
    public void shouldReturn404IfContactNotExistWhenDeleteContactLink() {
        Contact contact1 = contactRepository.save(createContact());

        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/contacts/" + contact1.getId() + "/link/" + (contact1.getId() + 1),
                HttpMethod.DELETE, null, Void.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());


        ResponseEntity<Void> response2 = restTemplate.exchange(baseUrl + "/contacts/" + (contact1.getId() + 1) + "/link/" + contact1.getId(),
                HttpMethod.DELETE, null, Void.class);

        assertEquals(HttpStatus.NOT_FOUND, response2.getStatusCode());
    }

    @Test
    public void shouldCreateContactLink() {

        Contact contact1 = contactRepository.save(createContact());
        Contact contact2 = contactRepository.save(createContact());

        ResponseEntity<ContactLinkDto> response = restTemplate.postForEntity(baseUrl + "/contacts/" + contact1.getId() + "/link/" + contact2.getId(), new CreateContactLinkDto("note"), ContactLinkDto.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());

        assertNotNull(response.getBody());
        assertEquals(contact1.getId(), response.getBody().getBaseContactId());
        assertEquals(contact2.getId(), response.getBody().getLinkedContactId());

        List<ContactLink> all = contactLinkRepository.findAll();
        assertEquals(2, all.size());
        assertTrue(all.stream().anyMatch(c -> c.getBaseContact().getId().equals(contact1.getId()) &&
                        c.getLinkedContact().getId().equals(contact2.getId())));
        assertTrue(all.stream().anyMatch(c -> c.getBaseContact().getId().equals(contact2.getId()) &&
                        c.getLinkedContact().getId().equals(contact1.getId())));
    }
    @Test
    public void shouldUpdateContactLinkNote() {
        Contact contact1 = contactRepository.save(createContact());
        Contact contact2 = contactRepository.save(createContact());

        ResponseEntity<ContactLinkDto> createResponse = restTemplate.postForEntity(baseUrl + "/contacts/" + contact1.getId() + "/link/" + contact2.getId(), new CreateContactLinkDto("note"), ContactLinkDto.class);
        assertEquals(HttpStatus.CREATED, createResponse.getStatusCode());
        assertTrue(contactLinkRepository.findAll().stream().allMatch(c -> c.getNote().equals("note")));

        ResponseEntity<ContactLinkDto> updateResponse = restTemplate.exchange(baseUrl + "/contacts/" + contact1.getId() + "/link/" + contact2.getId(), HttpMethod.PUT, new HttpEntity<>(new CreateContactLinkDto("updatedNote")), ContactLinkDto.class);
        assertEquals(HttpStatus.OK, updateResponse.getStatusCode());
        assertTrue(contactLinkRepository.findAll().stream().allMatch(c -> c.getNote().equals("updatedNote")));
    }

    @Test
    public void shouldDeleteContactLink() {
        Contact contact1 = contactRepository.save(createContact());
        Contact contact2 = contactRepository.save(createContact());
        Contact contact3 = contactRepository.save(createContact());

        //to delete
        contactLinkRepository.save(ContactLink.builder().baseContact(contact1).linkedContact(contact2).build());
        contactLinkRepository.save(ContactLink.builder().baseContact(contact2).linkedContact(contact1).build());

        //not delete
        contactLinkRepository.save(ContactLink.builder().baseContact(contact3).linkedContact(contact2).build());
        contactLinkRepository.save(ContactLink.builder().baseContact(contact1).linkedContact(contact3).build());

        assertEquals(4, contactLinkRepository.findAll().size());

        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/contacts/" + contact1.getId() + "/link/" + contact2.getId(),
                HttpMethod.DELETE, null, Void.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());

        assertEquals(2, contactLinkRepository.findAll().size());
        List<ContactLink> all = contactLinkRepository.findAll();

        assertTrue(all.stream().anyMatch(c -> c.getBaseContact().getId().equals(contact3.getId())
                && c.getLinkedContact().getId().equals(contact2.getId())));

        assertTrue(all.stream().anyMatch(c -> c.getBaseContact().getId().equals(contact1.getId())
                && c.getLinkedContact().getId().equals(contact3.getId())));
    }

    @Test
    public void shouldReturn200WhenFetchingAllContacts() {
        Contact contact_1 = createContact();
        contact_1.setCustomer(customer);
        Contact contact_2 = createContact();
        contact_2.setCustomer(customer);
        contactRepository.save(contact_1);
        contactRepository.save(contact_2);

        ResponseEntity<RestPageImpl<Contact>> response = restTemplate.exchange(baseUrl + "/contacts", HttpMethod.GET,
                new HttpEntity<>(headers), new ParameterizedTypeReference<RestPageImpl<Contact>>() {
                });
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getContent());
        assertEquals(2, response.getBody().getContent().size());
    }


    @Test
    public void shouldReturn201WhenContactsCreated() {
        Contact contact = createContact();
        ContactCreateDto dto = new ModelMapper().map(contact, ContactCreateDto.class);
        dto.setCustomerId(customer.getId());

        ResponseEntity<Contact> response = restTemplate.exchange(baseUrl + "/contacts", HttpMethod.POST, new HttpEntity<>(dto, headers), Contact.class);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(contact.getName(), response.getBody().getName());
    }

    @Test
    public void shouldReturn200WhenFetchingCustomerContacts() {
        Contact contact_1 = createContact();
        contact_1.setCustomer(customer);
        Contact contact_2 = createContact();
        contact_2.setCustomer(customer);
        contactRepository.save(contact_1);
        contactRepository.save(contact_2);

        ResponseEntity<Contact[]> response = restTemplate.getForEntity(baseUrl + "/customer/" + customer.getId() + "/contacts", Contact[].class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2, response.getBody().length);
    }

    @Test
    public void shouldReturn204WhenTryingToGetContactNonExistingCustomer() {
        Contact contact = createContact();
        contact.setCustomer(customer);
        contact = contactRepository.save(contact);

        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/contacts/" + contact.getId(), HttpMethod.DELETE, null, Void.class);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        Optional<Contact> byId = contactRepository.findById(contact.getId());
        assertTrue(byId.isEmpty());
    }

    @Test
    public void shouldReturn404WhenRemovingContact() {
        ResponseEntity<Contact[]> response = restTemplate.getForEntity(baseUrl + "/customer/23423423/contacts", Contact[].class);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldReturn404WhenTryingDeleteNonExistingContact() {
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/contacts/34587683", HttpMethod.DELETE, null, Void.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldThrowNotFoundWhenContactNotExistWhenGettingPossibleLinks() {
        Contact contact1 = contactRepository.save(createContact());

        ResponseEntity<Contact[]> response = restTemplate.getForEntity(baseUrl + "/contacts/" + (contact1.getId() + 1) + "/possible-links", Contact[].class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldReturnContactsWithoutLinkedContactsWhenGettingPossibleLinks() {
        Contact contact1 = contactRepository.save(createContact());
        Contact contact2 = contactRepository.save(createContact());
        Contact contact3 = contactRepository.save(createContact());

        contactLinkRepository.save(ContactLink.builder().baseContact(contact1).linkedContact(contact2).build());

        ResponseEntity<Contact[]> response = restTemplate.getForEntity(baseUrl + "/contacts/" + contact1.getId() + "/possible-links", Contact[].class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        assertEquals(1, response.getBody().length);
        assertNotNull(response.getBody()[0]);
        assertEquals(contact3.getId(), response.getBody()[0].getId());
    }

    @Test
    public void shouldReturn200WhenContactUpdated() {
        Contact contact = createContact();
        contact.setCustomer(customer);
        contact = contactRepository.save(contact);

        contact.setName("differentName");

        ResponseEntity<Contact> response = restTemplate.exchange(baseUrl + "/contacts/" + contact.getId(), HttpMethod.PUT, new HttpEntity<>(contact), Contact.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(contact.getName(), response.getBody().getName());
    }

    @Test
    public void shouldReturn404WhenUpdatingNonExistingContact() {
        Contact contact = createContact();
        ResponseEntity<Contact> response = restTemplate.exchange(baseUrl + "/contacts/34534535", HttpMethod.PUT, new HttpEntity<>(contact), Contact.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldGetContactsWithContactLinks() {
        Contact contact1 = contactRepository.save(createContact());
        Contact contact2 = contactRepository.save(createContact());
        Contact contact3 = contactRepository.save(createContact());

        contactLinkRepository.save(ContactLink.builder().baseContact(contact1).linkedContact(contact2).build());
        contactLinkRepository.save(ContactLink.builder().baseContact(contact1).linkedContact(contact3).build());

        contactLinkRepository.save(ContactLink.builder().baseContact(contact2).linkedContact(contact1).build());
        contactLinkRepository.save(ContactLink.builder().baseContact(contact2).linkedContact(contact3).build());

        contactLinkRepository.save(ContactLink.builder().baseContact(contact3).linkedContact(contact1).build());
        contactLinkRepository.save(ContactLink.builder().baseContact(contact3).linkedContact(contact2).build());


        ResponseEntity<RestPageImpl<Contact>> response = restTemplate.exchange(baseUrl + "/contacts", HttpMethod.GET,
                new HttpEntity<>(headers), new ParameterizedTypeReference<RestPageImpl<Contact>>() {
                });
        assertEquals(HttpStatus.OK, response.getStatusCode());

        assertNotNull(response.getBody());

        List<Contact> body = response.getBody().getContent();
        Optional<Contact> returnedContact1 = body.stream().filter(c -> c.getId().equals(contact1.getId())).findFirst();
        Optional<Contact> returnedContact2 = body.stream().filter(c -> c.getId().equals(contact2.getId())).findFirst();
        Optional<Contact> returnedContact3 = body.stream().filter(c -> c.getId().equals(contact3.getId())).findFirst();

        assertTrue(returnedContact1.isPresent());
        assertTrue(returnedContact2.isPresent());
        assertTrue(returnedContact3.isPresent());

        assertEquals(2, returnedContact1.get().getContactLinks().size());
        assertTrue(returnedContact1.get().getContactLinks().stream().anyMatch(c -> c.getLinkedContact().getId().equals(contact2.getId())));
        assertTrue(returnedContact1.get().getContactLinks().stream().anyMatch(c -> c.getLinkedContact().getId().equals(contact3.getId())));

        assertEquals(2, returnedContact2.get().getContactLinks().size());
        assertTrue(returnedContact2.get().getContactLinks().stream().anyMatch(c -> c.getLinkedContact().getId().equals(contact1.getId())));
        assertTrue(returnedContact2.get().getContactLinks().stream().anyMatch(c -> c.getLinkedContact().getId().equals(contact3.getId())));

        assertEquals(2, returnedContact3.get().getContactLinks().size());
        assertTrue(returnedContact3.get().getContactLinks().stream().anyMatch(c -> c.getLinkedContact().getId().equals(contact1.getId())));
        assertTrue(returnedContact3.get().getContactLinks().stream().anyMatch(c -> c.getLinkedContact().getId().equals(contact2.getId())));
    }


    @Test
    public void shouldReturn200WhenFetchingSpecificContact() {
        Contact contact_1 = createContact();
        contact_1.setCustomer(customer);
        contact_1 = contactRepository.save(contact_1);

        ResponseEntity<Contact> response = restTemplate.exchange(baseUrl + "/contacts/" + contact_1.getId(), HttpMethod.GET, new HttpEntity<>(headers), Contact.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(response.getBody().getName(), contact_1.getName());
    }

    @Test
    public void shouldReturn404WhenFetchingNonExistingContact() {
        ResponseEntity<Contact> response = restTemplate.exchange(baseUrl + "/contacts/654654656", HttpMethod.GET, new HttpEntity<>(headers), Contact.class);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }


    @Test
    public void shouldReturnListOfAccountsContainingInputInName() {
        ResponseEntity<List<CrmObject>> response = restTemplate.exchange(
                baseUrl + "/customers/search?term=nam",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<CrmObject>>(){});
        List<CrmObject> result = response.getBody();
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(result);
        assertNotNull(result.get(0));
        assertEquals(result.get(0).getType(), CrmObjectType.customer);
        assertEquals(result.get(0).getLabel(), "name");
    }

    @Test
    public void shouldReturnEmptyListOfAccountsBecauseNoneContainsInputTerm() {
        ResponseEntity<List<CrmObject>> response = restTemplate.exchange(
                baseUrl + "/customers/search?term=uyiuy8768",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<CrmObject>>(){});
        List<CrmObject> result = response.getBody();
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void shouldReturnListOfCrmObjectsById() {
        ResponseEntity<List<CrmObject>> response = restTemplate.exchange(
                String.format("%s/customers/crm-object?ids=%s", baseUrl, customer.getId()),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<CrmObject>>(){});
        List<CrmObject> result = response.getBody();
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(result);
        assertNotNull(result.get(0));
        assertEquals(result.get(0).getType(), CrmObjectType.customer);
        assertEquals(result.get(0).getLabel(), "name");
    }

    @Test
    public void shouldReturnEmptyListOfCrmObjectsBecauseWrongId() {
        ResponseEntity<List<CrmObject>> response = restTemplate.exchange(
                String.format("%s/customers/crm-object?ids=%s", baseUrl, 8768768L),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<CrmObject>>(){});
        List<CrmObject> result = response.getBody();
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }



    private Contact createContact() {
        return Contact.builder()
                .name("name")
                .surname("surname")
                .email("email")
                .phone("12345678910")
                .position("position")
                .customer(customer)
                .build();
    }

    private Customer createCustomer() {
        Customer customer = Customer.builder().name("name").nip("5961760186").owner(-1L).build();
        return customerRepository.save(customer);
    }

    @TestConfiguration
    public static class LocalClientConfiguration {
        @Bean
        public ServerList<Server> serverList() {
            return new StaticServerList<>(new Server("localhost", wireMockRule.port()));
        }
    }
}
