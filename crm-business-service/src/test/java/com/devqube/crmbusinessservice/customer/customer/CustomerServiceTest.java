package com.devqube.crmbusinessservice.customer.customer;

import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.customer.web.dto.CustomerWithContactsDto;
import com.devqube.crmbusinessservice.customer.exception.NameOccupiedException;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityService;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {
    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private AccessService accessService;
    @Mock
    private UserClient userClient;
    @Mock
    private LastViewedCustomerRepository lastViewedCustomerRepository;
    @Mock
    private OpportunityService opportunityService;
    @Mock
    private UserService userService;

    @InjectMocks
    private CustomerService customerService;


    @Before
    public void setUp() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Logged-Account-Email", "jan.kowalski@devqube.com");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    @Test(expected = NameOccupiedException.class)
    public void shouldNotSaveUserWhenNameIsOccupied() throws NameOccupiedException, BadRequestException {
        CustomerWithContactsDto customer = CustomerWithContactsDto.builder().name("name").build();
        when(customerRepository.countByName(any())).thenReturn(1L);
        customerService.save(customer);
    }

    @Test
    public void shouldSaveUserWhenNameIsNotOccupied() throws NameOccupiedException, BadRequestException {
        CustomerWithContactsDto customer = CustomerWithContactsDto.builder().name("name").build();
        when(userService.getUserId(any())).thenReturn(1L);
        when(accessService.assignAndVerify(any())).thenReturn(customer);
        when(customerRepository.countByName(any())).thenReturn(0L);
        when(customerRepository.save(any())).thenReturn(customer.toModel());
        customerService.save(customer);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldNotModifyUserWhenByIdNotFound() throws NameOccupiedException, EntityNotFoundException, BadRequestException {
        CustomerWithContactsDto customer = CustomerWithContactsDto.builder().id(1909090L).name("n/ame").build();
        when(customerRepository.findById(any())).thenReturn(Optional.empty());
        customerService.modify(customer);
    }

    @Test(expected = NameOccupiedException.class)
    public void shouldNotModifyUserWhenNewNameOccupiedByAnotherCustomer() throws NameOccupiedException, EntityNotFoundException, BadRequestException {
        Customer customer1 = Customer.builder().id(1L).name("name1").build();
        Customer customer2 = Customer.builder().id(2L).name("name2").build();
        when(customerRepository.findById(any())).thenReturn(Optional.of(customer1));
        when(customerRepository.findByName(any())).thenReturn(customer2);
        CustomerWithContactsDto customerDto = CustomerWithContactsDto.fromModel(customer2);
        customerService.modify(customerDto);
    }

    @Test
    public void shouldModifyCustomerWhenEverythingOK() throws NameOccupiedException, EntityNotFoundException, BadRequestException {
        Customer customer = Customer.builder().id(1L).name("name1").nip("123").contacts(new HashSet<>()).build();
        CustomerWithContactsDto customerDto = CustomerWithContactsDto.fromModel(customer);
        when(customerRepository.findById(any())).thenReturn(Optional.of(customer));
        when(accessService.assignAndVerify(any(), any())).thenReturn(customerDto);
        when(customerRepository.save(any())).thenReturn(customer);
        customerService.modify(customerDto);
        verify(customerRepository, times(1)).save(any());
    }

    @Test
    public void shouldReturnCustomerWhenFound() throws EntityNotFoundException {
        Customer customer = Customer.builder().name("name1").build();
        when(customerRepository.findById(any())).thenReturn(Optional.of(customer));
        assertEquals(customer.getName(), customerService.findById(1L).getName());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenTryingToDeleteNotExistingCustomer() throws EntityNotFoundException, BadRequestException {
        when(customerRepository.findById(any())).thenReturn(Optional.empty());
        customerService.deleteById(1L);
    }

    @Test
    public void shouldDeleteCustomerWhenFound() throws EntityNotFoundException, BadRequestException {
        Customer customer = Customer.builder().name("name1").build();
        when(customerRepository.findById(any())).thenReturn(Optional.of(customer));
        when(opportunityService.opportunityExistsByCustomerId(1L)).thenReturn(false);
        customerService.deleteById(1L);
        verify(customerRepository, times(1)).deleteById(any());
    }

    @Test(expected = BadRequestException.class)
    public void shouldNotDeleteCustomerWhenUsedInOpportunity() throws EntityNotFoundException, BadRequestException {
        Customer customer = Customer.builder().name("name1").build();
        when(customerRepository.findById(any())).thenReturn(Optional.of(customer));
        when(opportunityService.opportunityExistsByCustomerId(1L)).thenReturn(true);
        customerService.deleteById(1L);
    }
}
