package com.devqube.crmbusinessservice.customer.gus;

import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.exception.InvalidGUSResponseException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GUSResponseParserTest {

    private static final String RESPONSE_WHEN_NOT_FOUND = "";
    private static final String FIRST_RESPONSE_WHEN_FOUND =
            "<root>" +
            "  <dane>" +
            "    <Regon>38259935700000</Regon>" +
            "    <RegonLink>&lt;a href='javascript:danePobierzPelnyRaport(\"38259935700000\",\"DaneRaportPrawnaPubl\", 0);'&gt;382599357&lt;/a&gt;</RegonLink>" +
            "  </dane>" +
            "</root>";
    private static final String SECOND_RESPONSE_FOR_LEGAL_UNIT =
            "<root>" +
            "  <dane>" +
            "    <praw_regon14>38259935700000</praw_regon14>" +
            "    <praw_nip>9532763387</praw_nip>" +
            "    <praw_nazwa>DEVQUBE SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ</praw_nazwa>" +
            "    <praw_nazwaSkrocona />" +
            "    <praw_adSiedzKraj_Symbol>PL</praw_adSiedzKraj_Symbol>" +
            "    <praw_adSiedzWojewodztwo_Symbol>04</praw_adSiedzWojewodztwo_Symbol>" +
            "    <praw_adSiedzPowiat_Symbol>61</praw_adSiedzPowiat_Symbol>" +
            "    <praw_adSiedzGmina_Symbol>011</praw_adSiedzGmina_Symbol>" +
            "    <praw_adSiedzKodPocztowy>85862</praw_adSiedzKodPocztowy>" +
            "    <praw_adSiedzMiejscowoscPoczty_Symbol>0928363</praw_adSiedzMiejscowoscPoczty_Symbol>" +
            "    <praw_adSiedzMiejscowosc_Symbol>0928363</praw_adSiedzMiejscowosc_Symbol>" +
            "    <praw_adSiedzUlica_Symbol>44121</praw_adSiedzUlica_Symbol>" +
            "    <praw_adSiedzNumerNieruchomosci>3</praw_adSiedzNumerNieruchomosci>" +
            "    <praw_adSiedzNumerLokalu />" +
            "    <praw_adSiedzKraj_Nazwa>POLSKA</praw_adSiedzKraj_Nazwa>" +
            "    <praw_adSiedzWojewodztwo_Nazwa>KUJAWSKO-POMORSKIE</praw_adSiedzWojewodztwo_Nazwa>" +
            "    <praw_adSiedzPowiat_Nazwa>m. Bydgoszcz</praw_adSiedzPowiat_Nazwa>" +
            "    <praw_adSiedzGmina_Nazwa>M. Bydgoszcz</praw_adSiedzGmina_Nazwa>" +
            "    <praw_adSiedzMiejscowosc_Nazwa>Bydgoszcz</praw_adSiedzMiejscowosc_Nazwa>" +
            "    <praw_adSiedzMiejscowoscPoczty_Nazwa>Bydgoszcz</praw_adSiedzMiejscowoscPoczty_Nazwa>" +
            "    <praw_adSiedzUlica_Nazwa>ul. Bydgoskich Przemysłowców</praw_adSiedzUlica_Nazwa>" +
            "  </dane>" +
            "</root>";
    private static final String SECOND_RESPONSE_FOR_PHYSICAL_UNIT =
            "<root>" +
            "  <dane>" +
            "    <fiz_regon9>210146630</fiz_regon9>" +
            "    <fiz_nazwa>JERZY SZTUKIECKI</fiz_nazwa>" +
            "    <fiz_nazwaSkrocona>JERZY SZTUKIECKI</fiz_nazwaSkrocona>" +
            "    <fiz_adSiedzKraj_Symbol>PL</fiz_adSiedzKraj_Symbol>" +
            "    <fiz_adSiedzWojewodztwo_Symbol>08</fiz_adSiedzWojewodztwo_Symbol>" +
            "    <fiz_adSiedzPowiat_Symbol>03</fiz_adSiedzPowiat_Symbol>" +
            "    <fiz_adSiedzGmina_Symbol>024</fiz_adSiedzGmina_Symbol>" +
            "    <fiz_adSiedzKodPocztowy>66300</fiz_adSiedzKodPocztowy>" +
            "    <fiz_adSiedzMiejscowoscPoczty_Symbol>0935529</fiz_adSiedzMiejscowoscPoczty_Symbol>" +
            "    <fiz_adSiedzMiejscowosc_Symbol>0935529</fiz_adSiedzMiejscowosc_Symbol>" +
            "    <fiz_adSiedzUlica_Symbol>16046</fiz_adSiedzUlica_Symbol>" +
            "    <fiz_adSiedzNumerNieruchomosci>36C</fiz_adSiedzNumerNieruchomosci>" +
            "    <fiz_adSiedzNumerLokalu>4</fiz_adSiedzNumerLokalu>" +
            "    <fiz_adSiedzNietypoweMiejsceLokalizacji />" +
            "    <fiz_adSiedzKraj_Nazwa>POLSKA</fiz_adSiedzKraj_Nazwa>" +
            "    <fiz_adSiedzWojewodztwo_Nazwa>LUBUSKIE</fiz_adSiedzWojewodztwo_Nazwa>" +
            "    <fiz_adSiedzPowiat_Nazwa>międzyrzecki</fiz_adSiedzPowiat_Nazwa>" +
            "    <fiz_adSiedzGmina_Nazwa>Międzyrzecz</fiz_adSiedzGmina_Nazwa>" +
            "    <fiz_adSiedzMiejscowosc_Nazwa>Międzyrzecz</fiz_adSiedzMiejscowosc_Nazwa>" +
            "    <fiz_adSiedzMiejscowoscPoczty_Nazwa>Międzyrzecz</fiz_adSiedzMiejscowoscPoczty_Nazwa>" +
            "    <fiz_adSiedzUlica_Nazwa>ul. Piastowska</fiz_adSiedzUlica_Nazwa>" +
            "  </dane>" +
            "</root>";

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenNoCustomerInXml() throws InvalidGUSResponseException, EntityNotFoundException {
        new GUSResponseParser(RESPONSE_WHEN_NOT_FOUND).extractRegonAndType();
    }

    @Test
    public void shouldParseRegonAndType() throws InvalidGUSResponseException, EntityNotFoundException {
        Pair<String, String> regonAndType = new GUSResponseParser(FIRST_RESPONSE_WHEN_FOUND).extractRegonAndType();
        assertEquals("38259935700000", regonAndType.getLeft());
        assertEquals("PublDaneRaportPrawna", regonAndType.getRight());
    }

    @Test
    public void shouldParseNameForPhysicalUnit() throws InvalidGUSResponseException, EntityNotFoundException {
        Customer customer = new GUSResponseParser(SECOND_RESPONSE_FOR_PHYSICAL_UNIT).extractCustomer("fiz",
                "5961070530", "210146630");
        assertNotNull(customer);
        assertEquals("JERZY SZTUKIECKI", customer.getName());
    }

    @Test
    public void shouldParseNIPForPhysicalUnit() throws InvalidGUSResponseException, EntityNotFoundException {
        Customer customer = new GUSResponseParser(SECOND_RESPONSE_FOR_PHYSICAL_UNIT).extractCustomer("fiz",
                "5961070530", "210146630");
        assertNotNull(customer);
        assertEquals("5961070530", customer.getNip());
    }

    @Test
    public void shouldParseREGONForPhysicalUnit() throws InvalidGUSResponseException, EntityNotFoundException {
        Customer customer = new GUSResponseParser(SECOND_RESPONSE_FOR_PHYSICAL_UNIT).extractCustomer("fiz",
                "5961070530", "210146630");
        assertNotNull(customer);
        assertEquals("210146630", customer.getRegon());
    }

    @Test
    public void shouldParseCountryForPhysicalUnit() throws InvalidGUSResponseException, EntityNotFoundException {
        Customer customer = new GUSResponseParser(SECOND_RESPONSE_FOR_PHYSICAL_UNIT).extractCustomer("fiz",
                "5961070530", "210146630");
        assertNotNull(customer);
        assertNotNull(customer.getAddress());
        assertEquals("POLSKA", customer.getAddress().getCountry());
    }

    @Test
    public void shouldParseCityForPhysicalUnit() throws InvalidGUSResponseException, EntityNotFoundException {
        Customer customer = new GUSResponseParser(SECOND_RESPONSE_FOR_PHYSICAL_UNIT).extractCustomer("fiz",
                "5961070530", "210146630");
        assertNotNull(customer);
        assertNotNull(customer.getAddress());
        assertEquals("Międzyrzecz", customer.getAddress().getCity());
    }

    @Test
    public void shouldParseStreetForPhysicalUnit() throws InvalidGUSResponseException, EntityNotFoundException {
        Customer customer = new GUSResponseParser(SECOND_RESPONSE_FOR_PHYSICAL_UNIT).extractCustomer("fiz",
                "5961070530", "210146630");
        assertNotNull(customer);
        assertNotNull(customer.getAddress());
        assertEquals("ul. Piastowska 36C/4", customer.getAddress().getStreet());
    }

    @Test
    public void shouldParseNameForLegalUnit() throws InvalidGUSResponseException, EntityNotFoundException {
        Customer customer = new GUSResponseParser(SECOND_RESPONSE_FOR_LEGAL_UNIT).extractCustomer("praw",
                "9532763387", "38259935700000");
        assertNotNull(customer);
        assertEquals("DEVQUBE SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ", customer.getName());
    }

    @Test
    public void shouldParseNIPForLegalUnit() throws InvalidGUSResponseException, EntityNotFoundException {
        Customer customer = new GUSResponseParser(SECOND_RESPONSE_FOR_LEGAL_UNIT).extractCustomer("praw",
                "9532763387", "38259935700000");
        assertNotNull(customer);
        assertEquals("9532763387", customer.getNip());
    }

    @Test
    public void shouldParseREGONForLegalUnit() throws InvalidGUSResponseException, EntityNotFoundException {
        Customer customer = new GUSResponseParser(SECOND_RESPONSE_FOR_LEGAL_UNIT).extractCustomer("praw",
                "9532763387", "38259935700000");
        assertNotNull(customer);
        assertEquals("38259935700000", customer.getRegon());
    }

    @Test
    public void shouldParseCountryForLegalUnit() throws InvalidGUSResponseException, EntityNotFoundException {
        Customer customer = new GUSResponseParser(SECOND_RESPONSE_FOR_LEGAL_UNIT).extractCustomer("praw",
                "9532763387", "38259935700000");
        assertNotNull(customer);
        assertNotNull(customer.getAddress());
        assertEquals("POLSKA", customer.getAddress().getCountry());
    }

    @Test
    public void shouldParseCityForLegalUnit() throws InvalidGUSResponseException, EntityNotFoundException {
        Customer customer = new GUSResponseParser(SECOND_RESPONSE_FOR_LEGAL_UNIT).extractCustomer("praw",
                "9532763387", "38259935700000");
        assertNotNull(customer);
        assertNotNull(customer.getAddress());
        assertEquals("Bydgoszcz", customer.getAddress().getCity());
    }

    @Test
    public void shouldParseStreetForLegalUnit() throws InvalidGUSResponseException, EntityNotFoundException {
        Customer customer = new GUSResponseParser(SECOND_RESPONSE_FOR_LEGAL_UNIT).extractCustomer("praw",
                "9532763387", "38259935700000");
        assertNotNull(customer);
        assertNotNull(customer.getAddress());
        assertEquals("ul. Bydgoskich Przemysłowców 3", customer.getAddress().getStreet());
    }
}
