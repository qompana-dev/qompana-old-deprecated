package com.devqube.crmbusinessservice.customer.contacts;

import com.devqube.crmbusinessservice.customer.contact.ContactLinkRepository;
import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.contact.ContactService;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.contact.model.ContactLink;
import com.devqube.crmbusinessservice.customer.contact.web.dto.ContactLinkDto;
import com.devqube.crmbusinessservice.customer.contact.web.dto.CreateContactLinkDto;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.exception.ContactLinkExistException;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceTest {

    @Mock
    private ContactRepository contactRepository;

    @Mock
    private ContactLinkRepository contactLinkRepository;

    @Mock
    private AccessService accessService;

    @InjectMocks
    private ContactService contactService;

    @Before
    public void setUp() {
        when(accessService.hasAccess(anyString(), any(), anyBoolean())).thenReturn(false);
    }


    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfContactIdNotFoundWhenCreateLink() throws EntityNotFoundException, BadRequestException, PermissionDeniedException, ContactLinkExistException {
        Contact testContact = getContact();
        when(contactRepository.findContactById(eq(1L))).thenReturn(testContact);
        when(contactRepository.findContactById(eq(2L))).thenReturn(null);

        contactService.saveLinkBetweenContacts(1L, 2L, null);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfContactIdNotFoundWhenUpdateLink() throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Contact testContact = getContact();
        when(contactRepository.findContactById(eq(1L))).thenReturn(testContact);
        when(contactRepository.findContactById(eq(2L))).thenReturn(null);

        contactService.updateContactLink(1L, 2L, null);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfContactIdNotFoundWhenGetPossibleLink() throws EntityNotFoundException {
        when(contactRepository.findById(eq(1L))).thenReturn(Optional.empty());

        contactService.getAllPossibleLinks(1L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfLinkedContactIdNotFoundWhenCreateLink() throws EntityNotFoundException, BadRequestException, PermissionDeniedException, ContactLinkExistException {
        Contact testContact = getContact();
        when(contactRepository.findContactById(eq(1L))).thenReturn(null);
        when(contactRepository.findContactById(eq(2L))).thenReturn(testContact);

        contactService.saveLinkBetweenContacts(1L, 2L, null);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfLinkedContactIdNotFoundWhenUpdateLink() throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Contact testContact = getContact();
        when(contactRepository.findContactById(eq(1L))).thenReturn(null);
        when(contactRepository.findContactById(eq(2L))).thenReturn(testContact);

        contactService.updateContactLink(1L, 2L, null);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfContactIdNotFoundWhenDeleteLink() throws EntityNotFoundException {
        Contact testContact = getContact();
        when(contactRepository.findContactById(eq(1L))).thenReturn(testContact);
        when(contactRepository.findContactById(eq(2L))).thenReturn(null);

        contactService.deleteContactLink(1L, 2L);
    }


    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfLinkedContactIdNotFoundWhenDeleteLink() throws EntityNotFoundException {
        Contact testContact = getContact();
        when(contactRepository.findContactById(eq(1L))).thenReturn(null);
        when(contactRepository.findContactById(eq(2L))).thenReturn(testContact);

        contactService.deleteContactLink(1L, 2L);
    }

    @Test
    public void shouldCreateLink() throws EntityNotFoundException, BadRequestException, PermissionDeniedException, ContactLinkExistException {
        Contact testContact1 = getContact();
        testContact1.setId(1L);
        testContact1.setCustomer(Customer.builder().name("customer").build());
        Contact testContact2 = getContact();
        testContact2.setId(2L);
        testContact2.setCustomer(Customer.builder().name("customer").build());

        when(contactRepository.findContactById(eq(1L))).thenReturn(testContact1);
        when(contactRepository.findContactById(eq(2L))).thenReturn(testContact2);
        when(accessService.assignAndVerify(any())).thenReturn(new CreateContactLinkDto("note"));

        when(contactLinkRepository.save(any())).thenReturn(ContactLink.builder().baseContact(testContact1).linkedContact(testContact2).build());

        ContactLinkDto contactLink = contactService.saveLinkBetweenContacts(1L, 2L, new CreateContactLinkDto("note"));

        Mockito.verify(contactLinkRepository, times(2)).save(any());

        assertEquals(1L, contactLink.getBaseContactId().longValue());
        assertEquals(2L, contactLink.getLinkedContactId().longValue());
    }

    @Test
    public void shouldUpdateLink() throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        Contact testContact1 = getContact();
        testContact1.setId(1L);
        testContact1.setCustomer(Customer.builder().name("customer").build());
        Contact testContact2 = getContact();
        testContact2.setId(2L);
        testContact2.setCustomer(Customer.builder().name("customer").build());

        when(contactRepository.findContactById(eq(1L))).thenReturn(testContact1);
        when(contactRepository.findContactById(eq(2L))).thenReturn(testContact2);
        ContactLink link1 = ContactLink.builder().baseContact(testContact1).linkedContact(testContact2).note("note").build();
        ContactLink link2 = ContactLink.builder().baseContact(testContact2).linkedContact(testContact1).note("note").build();
        when(contactLinkRepository.findAllByBaseContactAndLinkedContact(eq(testContact2), eq(testContact1))).thenReturn(Collections.singletonList(link2));
        when(accessService.assignAndVerify(any())).thenReturn(new CreateContactLinkDto("note"));

        when(contactLinkRepository.saveAll(any())).thenReturn(Arrays.asList(link1, link2));

        ContactLinkDto contactLink = contactService.updateContactLink(1L, 2L, new CreateContactLinkDto("note"));

        Mockito.verify(contactLinkRepository, times(1)).saveAll(any());

        assertEquals(1L, contactLink.getBaseContactId().longValue());
        assertEquals(2L, contactLink.getLinkedContactId().longValue());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfContactNotFoundWhenGettingLinksForContact() throws EntityNotFoundException {
        when(contactRepository.findById(anyLong())).thenReturn(Optional.empty());
        contactService.getLinksForContact(1L);
    }

    @Test
    public void shouldGetLinksForContact() throws EntityNotFoundException {
        Contact contact = getContact();
        when(contactRepository.findById(anyLong())).thenReturn(Optional.of(contact));
        when(contactLinkRepository.findAllByBaseContact(any())).thenReturn(new ArrayList<>());

        List<ContactLinkDto> linksForContact = contactService.getLinksForContact(1L);

        verify(contactLinkRepository, times(1)).findAllByBaseContact(any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfContactNotFoundWhenGettingContactLinksPreview() throws EntityNotFoundException {
        when(contactRepository.findById(anyLong())).thenReturn(Optional.empty());
        contactService.getContactLinksPreview(1L);
    }

    @Test
    public void shouldGetContactLinksPreview() throws EntityNotFoundException {
        Contact contact = getContact();
        when(contactRepository.findById(anyLong())).thenReturn(Optional.of(contact));
        when(contactLinkRepository.findTop3ByBaseContact(any())).thenReturn(new ArrayList<>());
        when(contactLinkRepository.countAllByBaseContact(any())).thenReturn(3L);

        contactService.getContactLinksPreview(1L);

        verify(contactLinkRepository, times(1)).countAllByBaseContact(any());
        verify(contactLinkRepository, times(1)).findTop3ByBaseContact(any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfBaseContactNotFoundWhenGettingContactLink() throws EntityNotFoundException {
        when(contactRepository.findById(eq(1L))).thenReturn(Optional.empty());
        contactService.getContactLink(1L, 2L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfLinkedContactNotFoundWhenGettingContactLink() throws EntityNotFoundException {
        when(contactRepository.findById(eq(1L))).thenReturn(Optional.of(getContact()));
        when(contactRepository.findById(eq(2L))).thenReturn(Optional.empty());
        contactService.getContactLink(1L, 2L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfContactLinkNotFoundWhenGettingContactLink() throws EntityNotFoundException {
        when(contactRepository.findById(anyLong())).thenReturn(Optional.of(getContact()));
        when(contactLinkRepository.findAllByBaseContactAndLinkedContact(any(), any())).thenReturn(new ArrayList<>());
        contactService.getContactLink(1L, 2L);
    }

    @Test
    public void shouldDeleteLink() throws EntityNotFoundException {
        Contact testContact1 = getContact();
        testContact1.setId(1L);
        Contact testContact2 = getContact();
        testContact2.setId(2L);

        when(contactRepository.findContactById(eq(1L))).thenReturn(testContact1);
        when(contactRepository.findContactById(eq(2L))).thenReturn(testContact2);

        contactService.deleteContactLink(1L, 2L);

        Mockito.verify(contactLinkRepository, times(1)).deleteAllByBaseContactAndLinkedContact(eq(testContact1), eq(testContact2));
    }

    static Contact getContact() {
        return Contact.builder()
                .id(1L)
                .email("mail@mail.com").build();
    }
}
