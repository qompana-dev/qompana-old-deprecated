package com.devqube.crmbusinessservice.customer.customer.web;

import com.devqube.crmbusinessservice.AbstractIntegrationTest;
import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.contact.model.EmploymentHistory;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.customer.customer.LastViewedCustomerRepository;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.customer.customer.model.LastViewedCustomer;
import com.devqube.crmbusinessservice.customer.customer.web.dto.ContactDto;
import com.devqube.crmbusinessservice.customer.customer.web.dto.CustomerWithContactsDto;
import com.devqube.crmshared.web.RestPageImpl;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.netflix.ribbon.StaticServerList;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ActiveProfiles({"test", "wiremock"})
@ContextConfiguration(classes = {CustomerControllerIntegrationTest.LocalClientConfiguration.class})
public class CustomerControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private LastViewedCustomerRepository lastViewedCustomerRepository;


    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(WireMockConfiguration.options().dynamicPort());
    private HttpHeaders headers = null;

    @Before
    public void setUp() throws IOException {
        super.setUp();
        this.lastViewedCustomerRepository.deleteAll();
        this.contactRepository.deleteAll();
        this.customerRepository.deleteAll();

        stubFor(get(urlEqualTo("/accounts/me/id"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("-1")));
        headers = new HttpHeaders();
        headers.set("Logged-Account-Email", "jan.kowalski@devqube.com");
    }

    @Test
    public void shouldReturn201WhenCustomerCreated() {
        Customer customer = Customer.builder().name("name").owner(-1L).nip("5961760186").build();
        ResponseEntity<Customer> response = restTemplate.postForEntity(baseUrl + "/customer", new HttpEntity<>(customer, headers), Customer.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(customer.getName(), response.getBody().getName());

    }

    @Test
    public void shouldCreateCustomerWithContacts() {
        CustomerWithContactsDto customer = CustomerWithContactsDto.builder().name("name").owner(-1L).nip("5961760186")
                .contacts(Arrays.asList(
                        ContactDto.builder().name("name1").surname("surname1").position("position1").phone("11123123123").email("mail@mail1.com").mainContact(true).build(),
                        ContactDto.builder().name("name2").surname("surname2").position("position2").phone("21123123123").email("mail@mail1.com").mainContact(false).build()
                )).build();
        ResponseEntity<CustomerWithContactsDto> response = restTemplate.postForEntity(baseUrl + "/customer", new HttpEntity<>(customer, headers), CustomerWithContactsDto.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(customer.getName(), response.getBody().getName());
        assertNotNull(customer.getContacts());
        assertEquals(2, customer.getContacts().size());

        Optional<ContactDto> contact1 = customer.getContacts().stream().filter(contact -> contact.getName().equals("name1")).findFirst();
        Optional<ContactDto> contact2 = customer.getContacts().stream().filter(contact -> contact.getName().equals("name2")).findFirst();

        assertTrue(contact1.isPresent());
        assertEquals("name1", contact1.get().getName());
        assertEquals("surname1", contact1.get().getSurname());
        assertEquals("position1", contact1.get().getPosition());
        assertEquals("11123123123", contact1.get().getPhone());
        assertEquals(true, contact1.get().getMainContact());

        assertTrue(contact2.isPresent());
        assertEquals("name2", contact2.get().getName());
        assertEquals("surname2", contact2.get().getSurname());
        assertEquals("position2", contact2.get().getPosition());
        assertEquals("21123123123", contact2.get().getPhone());
    }

    @Test
    public void shouldReturnBadRequestIfContactNameIsNull() {
        CustomerWithContactsDto customer = CustomerWithContactsDto.builder().name("name").owner(-1L).nip("5961760186")
                .contacts(Arrays.asList(
                        ContactDto.builder().name(null).surname("surname1").position("position1").phone("11123123123").email("mail@mail1.com").mainContact(true).build()
                )).build();
        ResponseEntity<CustomerWithContactsDto> response = restTemplate.postForEntity(baseUrl + "/customer", customer, CustomerWithContactsDto.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldReturnBadRequestIfContactSurameIsNull() {
        CustomerWithContactsDto customer = CustomerWithContactsDto.builder().name("name").owner(-1L).nip("5961760186")
                .contacts(Arrays.asList(
                        ContactDto.builder().name("name1").surname(null).position("position1").phone("11123123123").email("mail@mail1.com").mainContact(true).build()
                )).build();
        ResponseEntity<CustomerWithContactsDto> response = restTemplate.postForEntity(baseUrl + "/customer", customer, CustomerWithContactsDto.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldReturnBadRequestIfContactPositionIsNull() {
        CustomerWithContactsDto customer = CustomerWithContactsDto.builder().name("name").owner(-1L).nip("5961760186")
                .contacts(Arrays.asList(
                        ContactDto.builder().name("name1").surname("surname1").position(null).phone("11123123123").email("mail@mail1.com").mainContact(true).build()
                )).build();
        ResponseEntity<CustomerWithContactsDto> response = restTemplate.postForEntity(baseUrl + "/customer", customer, CustomerWithContactsDto.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldReturnBadRequestIfMoreThanOneContactIsMainContact() {
        CustomerWithContactsDto customer = CustomerWithContactsDto.builder().name("name").owner(-1L).nip("5961760186")
                .contacts(Arrays.asList(
                        ContactDto.builder().name("name1").surname("surname1").position("position1").phone("11123123123").email("mail@mail1.com").mainContact(true).build(),
                        ContactDto.builder().name("name2").surname("surname2").position("position2").phone("11123123123").email("mail@mail2.com").mainContact(true).build()
                )).build();
        ResponseEntity<CustomerWithContactsDto> response = restTemplate.postForEntity(baseUrl + "/customer", customer, CustomerWithContactsDto.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldReturn400WhenTryingToSaveUserWithNoName() {
        Customer customer = new Customer();
        ResponseEntity<Customer> response = restTemplate.postForEntity(baseUrl + "/customer", customer, Customer.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldReturn409WhenCreatingCustomerAndNameIsOccupied() {
        Customer customer = Customer.builder().name("name").owner(-1L).nip("5961760186").build();
        customerRepository.save(customer);
        ResponseEntity<Customer> response = restTemplate.postForEntity(baseUrl + "/customer", customer, Customer.class);
        assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    public void shouldReturn400WhenModifyingAndIdsDontMatch() {
        Customer customer = customerRepository.save(Customer.builder().name("name").owner(-1L).nip("5961760186").build());
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/customer/-500", HttpMethod.PUT,
                new HttpEntity<>(customer), Void.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldReturn200WhenCustomerModified() {
        Customer customer = customerRepository.save(Customer.builder().name("name").owner(-1L).nip("5961760186").build());
        String oldName = customer.getName();
        customer.setName("newName");
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/customer/" + customer.getId(), HttpMethod.PUT,
                new HttpEntity<>(customer), Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(Long.valueOf(1L), customerRepository.countByName(customer.getName()));
        assertEquals(Long.valueOf(0L), customerRepository.countByName(oldName));
    }

    @Test
    @Transactional
    public void shouldReturn200WhenContactInCustomerModified() {
        CustomerWithContactsDto customer = CustomerWithContactsDto.builder().name("namename").owner(-1L).nip("5961760189")
                .contacts(Arrays.asList(
                        ContactDto.builder().name("name1").surname("surname1").position("position1").phone("11123123123").email("mail@mail1.com").mainContact(true).build(),
                        ContactDto.builder().name("name2").surname("surname2").position("position2").phone("21123123123").email("mail@mail2.com").mainContact(false).build()
                )).build();

        ResponseEntity<CustomerWithContactsDto> postResponse = restTemplate.exchange(baseUrl + "/customer", HttpMethod.POST, new HttpEntity<>(customer, headers), CustomerWithContactsDto.class);

        assertEquals(HttpStatus.CREATED, postResponse.getStatusCode());
        customer = postResponse.getBody();
        assertNotNull(customer);
        assertNotNull(customer.getContacts());
        assertEquals(2, customer.getContacts().size());

        Long contactId = customer.getContacts().get(0).getId();
        customer.getContacts().get(0).setName("name3");
        customer.getContacts().get(0).setSurname("surname3");
        customer.getContacts().get(0).setPosition("position3");
        customer.getContacts().get(0).setPhone("31123123123");
        customer.getContacts().get(0).setEmail("mail@mail3.com");
        customer.getContacts().get(0).setMainContact(false);

        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/customer/" + customer.getId(), HttpMethod.PUT,
                new HttpEntity<>(customer, headers), Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Contact contactById = contactRepository.findContactById(contactId);
        assertNotNull(contactById);
        assertEquals("name3", contactById.getName());
        assertEquals("surname3", contactById.getSurname());
        assertEquals("31123123123", contactById.getPhone());
        assertEquals("mail@mail3.com", contactById.getEmail());
        assertEquals(false, contactById.getMainContact());
        assertNotNull(contactById.getPosition());
        assertEquals("position3", contactById.getPosition());
    }

    @Test
    public void shouldReturnBadRequestIfContactNameIsNullWhenModifying() {
        CustomerWithContactsDto customer = CustomerWithContactsDto.builder().name("name").owner(-1L).nip("5961760186")
                .contacts(Arrays.asList(
                        ContactDto.builder().name("name1").surname("surname1").position("position1").phone("11123123123").email("mail@mail1.com").mainContact(true).build(),
                        ContactDto.builder().name("name2").surname("surname2").position("position2").phone("21123123123").email("mail@mail1.com").mainContact(false).build()
                )).build();

        ResponseEntity<CustomerWithContactsDto> postResponse = restTemplate.exchange(baseUrl + "/customer", HttpMethod.POST, new HttpEntity<>(customer, headers), CustomerWithContactsDto.class);

        assertEquals(HttpStatus.CREATED, postResponse.getStatusCode());
        customer = postResponse.getBody();
        assertNotNull(customer);
        assertNotNull(customer.getContacts());
        assertEquals(2, customer.getContacts().size());
        customer.getContacts().get(0).setName(null);

        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/customer/" + customer.getId(), HttpMethod.PUT,
                new HttpEntity<>(customer, headers), Void.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldReturnBadRequestIfContactSurnameIsNullWhenModifying() {
        CustomerWithContactsDto customer = CustomerWithContactsDto.builder().name("name").owner(-1L).nip("5961760186")
                .contacts(Arrays.asList(
                        ContactDto.builder().name("name1").surname("surname1").position("position1").phone("11123123123").email("mail@mail1.com").mainContact(true).build(),
                        ContactDto.builder().name("name2").surname("surname2").position("position2").phone("21123123123").email("mail@mail1.com").mainContact(false).build()
                )).build();

        ResponseEntity<CustomerWithContactsDto> postResponse = restTemplate.exchange(baseUrl + "/customer", HttpMethod.POST, new HttpEntity<>(customer, headers), CustomerWithContactsDto.class);

        assertEquals(HttpStatus.CREATED, postResponse.getStatusCode());
        customer = postResponse.getBody();
        assertNotNull(customer);
        assertNotNull(customer.getContacts());
        assertEquals(2, customer.getContacts().size());
        customer.getContacts().get(0).setSurname(null);

        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/customer/" + customer.getId(), HttpMethod.PUT,
                new HttpEntity<>(customer, headers), Void.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldReturnBadRequestIfContactPositionIsNullWhenModifying() {
        CustomerWithContactsDto customer = CustomerWithContactsDto.builder().name("name").owner(-1L).nip("5961760186")
                .contacts(Arrays.asList(
                        ContactDto.builder().name("name1").surname("surname1").position("position1").phone("11123123123").email("mail@mail1.com").mainContact(true).build(),
                        ContactDto.builder().name("name2").surname("surname2").position("position2").phone("21123123123").email("mail@mail1.com").mainContact(false).build()
                )).build();

        ResponseEntity<CustomerWithContactsDto> postResponse = restTemplate.exchange(baseUrl + "/customer", HttpMethod.POST, new HttpEntity<>(customer, headers), CustomerWithContactsDto.class);

        assertEquals(HttpStatus.CREATED, postResponse.getStatusCode());
        customer = postResponse.getBody();
        assertNotNull(customer);
        assertNotNull(customer.getContacts());
        assertEquals(2, customer.getContacts().size());
        customer.getContacts().get(0).setPosition(null);

        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/customer/" + customer.getId(), HttpMethod.PUT,
                new HttpEntity<>(customer, headers), Void.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldReturnBadRequestIfMoreThanOneContactIsMainContactWhenModifying() {
        CustomerWithContactsDto customer = CustomerWithContactsDto.builder().name("name").owner(-1L).nip("5961760186")
                .contacts(Arrays.asList(
                        ContactDto.builder().name("name1").surname("surname1").position("position1").phone("11123123123").email("mail@mail1.com").mainContact(false).build(),
                        ContactDto.builder().name("name2").surname("surname2").position("position2").phone("21123123123").email("mail@mail1.com").mainContact(false).build()
                )).build();

        ResponseEntity<CustomerWithContactsDto> postResponse = restTemplate.exchange(baseUrl + "/customer", HttpMethod.POST, new HttpEntity<>(customer, headers), CustomerWithContactsDto.class);

        assertEquals(HttpStatus.CREATED, postResponse.getStatusCode());
        customer = postResponse.getBody();
        assertNotNull(customer);
        assertNotNull(customer.getContacts());
        assertEquals(2, customer.getContacts().size());
        customer.getContacts().get(0).setMainContact(true);
        customer.getContacts().get(1).setMainContact(true);

        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/customer/" + customer.getId(), HttpMethod.PUT,
                new HttpEntity<>(customer, headers), Void.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldReturn404WhenModifyingNotExistingCustomer() {
        Customer customer = Customer.builder().name("name").nip("5961760186").owner(-1L).id(-200L).build();
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/customer/" + customer.getId(), HttpMethod.PUT,
                new HttpEntity<>(customer), Void.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldReturn409WhenModifiedNameIsOccupied() {
        Customer customer1 = customerRepository.save(Customer.builder().name("name").owner(-1L).nip("5961760186").build());
        Customer customer2 = customerRepository.save(Customer.builder().name("name2").owner(-1L).nip("5961760181").build());
        customer2.setName(customer1.getName());
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/customer/" + customer2.getId(), HttpMethod.PUT,
                new HttpEntity<>(CustomerWithContactsDto.fromModel(customer2)), Void.class);
        assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
    }

    @Test
    public void shouldReturnCustomerWhenFound() {
        Customer customer = customerRepository.save(Customer.builder().name("name").nip("5961760186").owner(-1L).build());

        List<LastViewedCustomer> allByCustomerIdBefore = lastViewedCustomerRepository.findAllByCustomer_Id(customer.getId());
        assertNotNull(allByCustomerIdBefore);
        assertEquals(0, allByCustomerIdBefore.size());

        //added 1 element
        ResponseEntity<Customer> response = restTemplate.exchange(baseUrl + "/customer/" + customer.getId(), HttpMethod.GET, new HttpEntity<>(headers), Customer.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(customer.getName(), response.getBody().getName());

        //modified element 1 element
        response = restTemplate.exchange(baseUrl + "/customer/" + customer.getId(), HttpMethod.GET, new HttpEntity<>(headers), Customer.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(customer.getName(), response.getBody().getName());

    }

    @Test
    public void shouldReturn404WhenCustomerNotFound() {
        ResponseEntity<Customer> response = restTemplate.exchange(baseUrl + "/customer/-500", HttpMethod.GET, new HttpEntity<>(headers), Customer.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    public void shouldReturnListOfCustomers() {
        IntStream.range(0, 5).forEach(i -> customerRepository.save(Customer.builder().owner(-1L).name("name" + i).nip("596176018" + i).build()));
        ResponseEntity<RestPageImpl<Customer>> response = restTemplate.exchange(baseUrl + "/customer", HttpMethod.GET,
                new HttpEntity<>(headers), new ParameterizedTypeReference<RestPageImpl<Customer>>() {
                });
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getContent());
        assertEquals(5L, response.getBody().getContent().size());
    }

    @Test
    public void shouldReturnListOfTodayCustomers() {
        IntStream.range(0, 5).forEach(i -> customerRepository.save(Customer.builder().owner(-1L).name("name" + i).nip("596176018" + i).build()));

        List<Customer> all = customerRepository.findAll();
        assertNotNull(all);
        assertEquals(5, all.size());
        Customer customer0 = all.get(0);
        customer0.setCreated(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusMinutes(1));
        customerRepository.save(customer0);
        Customer customer1 = all.get(1);
        customer1.setCreated(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusMinutes(1));
        customerRepository.save(customer1);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/customer")
                .queryParam("today", true);

        ResponseEntity<RestPageImpl<Customer>> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET,
                new HttpEntity<>(headers), new ParameterizedTypeReference<RestPageImpl<Customer>>() {
                });

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getContent());
        assertEquals(3L, response.getBody().getContent().size());
        assertTrue(response.getBody().getContent().stream().noneMatch(client -> client.getId().equals(customer0.getId())));
        assertTrue(response.getBody().getContent().stream().noneMatch(client -> client.getId().equals(customer1.getId())));
    }

    @Test
    public void shouldReturnListOfLastViewedCustomers() {
        IntStream.range(0, 5).forEach(i -> customerRepository.save(Customer.builder().owner(-1L).name("name" + i).nip("596176018" + i).build()));

        List<Customer> all = customerRepository.findAll();
        assertNotNull(all);
        assertEquals(5, all.size());

        Customer customer0 = all.get(0);
        LastViewedCustomer customer0LastViewed = LastViewedCustomer.builder().customer(customer0).userId(-1L)
                .lastViewed(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3).plusMinutes(1)).build();
        lastViewedCustomerRepository.save(customer0LastViewed);
        Customer customer1 = all.get(1);
        LastViewedCustomer customer1LastViewed = LastViewedCustomer.builder().customer(customer1).userId(-1L)
                .lastViewed(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3).plusMinutes(1)).build();
        lastViewedCustomerRepository.save(customer1LastViewed);
        Customer customer2 = all.get(2);
        LastViewedCustomer customer2LastViewed = LastViewedCustomer.builder().customer(customer2).userId(-1L)
                .lastViewed(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(3).minusMinutes(1)).build();
        lastViewedCustomerRepository.save(customer2LastViewed);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/customer")
                .queryParam("viewed", true);

        ResponseEntity<RestPageImpl<Customer>> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET,
                new HttpEntity<>(headers), new ParameterizedTypeReference<RestPageImpl<Customer>>() {
                });

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getContent());
        assertEquals(2L, response.getBody().getContent().size());
        assertTrue(response.getBody().getContent().stream().anyMatch(client -> client.getId().equals(customer0.getId())));
        assertTrue(response.getBody().getContent().stream().anyMatch(client -> client.getId().equals(customer1.getId())));
    }

    @Test
    public void shouldDeleteCustomerWhenExists() {
        Customer customer = customerRepository.save(Customer.builder().name("name").owner(-1L).nip("5961760186").build());

        stubFor(delete(urlEqualTo("/files/group-type/customer/group-id/"+customer.getId()))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(204)));


        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/customer/" + customer.getId(),
                HttpMethod.DELETE, null, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(customerRepository.findById(customer.getId()).isEmpty());
    }

    @Test
    public void shouldReturn404WhenDeletingNotExistingCustomer() {
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/customer/-500", HttpMethod.DELETE,
                null, Void.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @TestConfiguration
    public static class LocalClientConfiguration {
        @Bean
        public ServerList<Server> serverList() {
            return new StaticServerList<>(new Server("localhost", wireMockRule.port()));
        }
    }
}
