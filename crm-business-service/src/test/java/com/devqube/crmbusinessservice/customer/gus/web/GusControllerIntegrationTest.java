package com.devqube.crmbusinessservice.customer.gus.web;

import com.devqube.crmbusinessservice.AbstractIntegrationTest;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class GusControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Before
    public void setUp() throws IOException {
        super.setUp();
        customerRepository.deleteAll();
    }

    @Test
    public void shouldReturnCustomerWhenFoundInGUS() {
        ResponseEntity<Customer> response = this.restTemplate.getForEntity(this.baseUrl + "/gus?nip=5961760186", Customer.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

    }

    @Test
    public void shouldReturn404WhenCustomerNotFoundInGUS() {
        ResponseEntity<Void> response = this.restTemplate.getForEntity(this.baseUrl + "/gus?nip=fakeNip", Void.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}
