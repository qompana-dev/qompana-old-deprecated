package com.devqube.crmbusinessservice;

import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;

@SpringBootTest(classes = {CrmBusinessServiceApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
public abstract class AbstractIntegrationTest {
    @LocalServerPort
    private int port;

    protected TestRestTemplate restTemplate;
    protected String baseUrl;

    public void setUp() throws IOException {
        baseUrl = "http://localhost:" + port;
        restTemplate = new TestRestTemplate();
    }
}
