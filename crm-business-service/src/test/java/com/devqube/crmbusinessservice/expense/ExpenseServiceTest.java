package com.devqube.crmbusinessservice.expense;

import com.devqube.crmbusinessservice.access.GlobalConfigurationDTO;
import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.expense.exception.AssociationClientException;
import com.devqube.crmbusinessservice.expense.model.Expense;
import com.devqube.crmbusinessservice.expense.service.ExpenseNotificationService;
import com.devqube.crmbusinessservice.expense.service.ExpenseService;
import com.devqube.crmbusinessservice.expense.web.dto.ExpenseDTO;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ExpenseServiceTest {
    @Mock
    private ExpenseRepository expenseRepository;

    @Mock
    private LeadRepository leadRepository;

    @Mock
    private OpportunityRepository opportunityRepository;

    @Mock
    private ModelMapper modelMapper;
    @Mock
    private ExpenseNotificationService expenseNotificationService;

    @Mock
    private AssociationClient associationClient;
    @Mock
    private AccessService accessService;
    @Mock
    private UserClient userClient;
    @Mock
    private UserService userService;

    @InjectMocks
    private ExpenseService expenseService;

    @Before
    public void setUp() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Logged-Account-Email", "jan.kowalski@devqube.com");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    @Test(expected = BadRequestException.class)
    public void shouldNotSaveWhenNoOpportunityAndNoLeadIsAttached() throws BadRequestException, EntityNotFoundException, KafkaSendMessageException, AssociationClientException {
        ExpenseDTO expenseDTO = new ExpenseDTO();
        expenseService.save(expenseDTO);
    }

    @Test(expected = BadRequestException.class)
    public void shouldNotSaveWhenBothLeadAndOpportunityAreAttached() throws BadRequestException, EntityNotFoundException, KafkaSendMessageException, AssociationClientException {
        ExpenseDTO expenseDTO = new ExpenseDTO();
        expenseDTO.setOpportunityId(1L);
        expenseDTO.setLeadId(1L);
        expenseService.save(expenseDTO);
    }


    @Test(expected = EntityNotFoundException.class)
    public void shouldNotSaveExpenseWhenLeadDoesntExist() throws BadRequestException, EntityNotFoundException, KafkaSendMessageException, AssociationClientException {
        ExpenseDTO expenseDTO = new ExpenseDTO();
        expenseDTO.setLeadId(1L);
        when(leadRepository.findById(1L)).thenReturn(Optional.empty());
        expenseService.save(expenseDTO);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldNotSaveExpenseWhenOpportunityDoesntExist() throws BadRequestException, EntityNotFoundException, KafkaSendMessageException, AssociationClientException {
        ExpenseDTO expenseDTO = new ExpenseDTO();
        expenseDTO.setOpportunityId(1L);
        when(opportunityRepository.findById(1L)).thenReturn(Optional.empty());
        expenseService.save(expenseDTO);
    }

    @Test
    public void shouldSaveExpenseForLead() throws BadRequestException, EntityNotFoundException, KafkaSendMessageException, AssociationClientException {
        ExpenseDTO expenseDTO = new ExpenseDTO();
        expenseDTO.setLeadId(1L);
        expenseDTO.setAmount(1.0);

        when(leadRepository.findById(1L)).thenReturn(Optional.of(new Lead()));

        when(accessService.assignAndVerify(any())).thenReturn(expenseDTO);
        when(userClient.getMyAccountId(anyString())).thenReturn(1L);
        when(userClient.getSupervisorId(anyString())).thenReturn(2L);
        Expense saved = Expense.builder().id(1L).amount(1.0).build();
        when(modelMapper.map(any(), eq(Expense.class))).thenReturn(saved);
        when(modelMapper.map(any(), eq(ExpenseDTO.class))).thenReturn(expenseDTO);
        when(expenseRepository.save(any())).thenReturn(saved);
        when(userClient.getExpenseAcceptanceThreshold()).thenReturn(new GlobalConfigurationDTO(1L, "test", "500"));

        expenseService.save(expenseDTO);
        verify(expenseRepository, times(1)).save(any());
    }

    @Test
    public void shouldSaveExpenseForOpportunity() throws BadRequestException, EntityNotFoundException, KafkaSendMessageException, AssociationClientException {
        ExpenseDTO expenseDTO = new ExpenseDTO();
        expenseDTO.setOpportunityId(1L);
        expenseDTO.setAmount(1.0);

        when(opportunityRepository.findById(1L)).thenReturn(Optional.of(new Opportunity()));

        when(accessService.assignAndVerify(any())).thenReturn(expenseDTO);
        when(userClient.getMyAccountId(anyString())).thenReturn(1L);
        when(userClient.getSupervisorId(anyString())).thenReturn(2L);
        Expense saved = Expense.builder().id(1L).amount(1.0).build();
        when(modelMapper.map(any(), eq(Expense.class))).thenReturn(saved);
        when(modelMapper.map(any(), eq(ExpenseDTO.class))).thenReturn(expenseDTO);
        when(expenseRepository.save(any())).thenReturn(saved);
        when(userClient.getExpenseAcceptanceThreshold()).thenReturn(new GlobalConfigurationDTO(1L, "test", "500"));

        expenseService.save(expenseDTO);
        verify(expenseRepository, times(1)).save(any());
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenCrmObjectDoesNotIncludeLeadOrOpportunity() throws BadRequestException, EntityNotFoundException {
        CrmObject notLeadOrOpportunity = CrmObject.builder().type(CrmObjectType.customer).id(123L).build();
        Pageable pageable = PageRequest.of(0, 2);
        expenseService.getExpensesDtoList(notLeadOrOpportunity, pageable);
    }

    @Test
    public void shouldReturnExpensesForLead() throws BadRequestException {
        Expense expense1 = Expense.builder().id(555L).name("expense1").build();
        Expense expense2 = Expense.builder().id(999L).name("expense2").build();
        CrmObject notLeadOrOpportunity = CrmObject.builder().type(CrmObjectType.lead).id(123L).build();
        Pageable pageable = PageRequest.of(0, 2);

        Page<Expense> pagedResponse = new PageImpl(List.of(expense1, expense2));
        ExpenseDTO expectedExpense1 = ExpenseDTO.builder().id(555L).name("expense1").build();
        ExpenseDTO expectedExpense2 = ExpenseDTO.builder().id(999L).name("expense2").build();
        List<ExpenseDTO> expectedResponse = List.of(expectedExpense1, expectedExpense2);

        when(modelMapper.map(any(), any()))
                .thenReturn(expectedExpense1)
                .thenReturn(expectedExpense2);

        when(expenseRepository.findAllByLeadId(123L, pageable)).thenReturn(pagedResponse);
        Page<ExpenseDTO> responsePage = expenseService.getExpensesDtoList(notLeadOrOpportunity, pageable);

        assertThat(responsePage.getContent()).isEqualTo(expectedResponse);
        verify(expenseRepository, times(0)).findAllByOpportunityId(any(), any());
    }

    @Test
    public void shouldReturnExpensesForOpportunity() throws BadRequestException {
        Expense expense1 = Expense.builder().id(555L).name("expense1").build();
        Expense expense2 = Expense.builder().id(999L).name("expense2").build();
        CrmObject notLeadOrOpportunity = CrmObject.builder().type(CrmObjectType.opportunity).id(123L).build();
        Pageable pageable = PageRequest.of(0, 2);

        Page<Expense> pagedResponse = new PageImpl(List.of(expense1, expense2));
        ExpenseDTO expectedExpense1 = ExpenseDTO.builder().id(555L).name("expense1").build();
        ExpenseDTO expectedExpense2 = ExpenseDTO.builder().id(999L).name("expense2").build();
        List<ExpenseDTO> expectedResponse = List.of(expectedExpense1, expectedExpense2);

        when(modelMapper.map(any(), any()))
                .thenReturn(expectedExpense1)
                .thenReturn(expectedExpense2);

        when(expenseRepository.findAllByOpportunityId(123L, pageable)).thenReturn(pagedResponse);
        Page<ExpenseDTO> responsePage = expenseService.getExpensesDtoList(notLeadOrOpportunity, pageable);

        assertThat(responsePage.getContent()).isEqualTo(expectedResponse);
        verify(expenseRepository, times(0)).findAllByLeadId(any(), any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfNotFound() throws EntityNotFoundException {
        when(expenseRepository.findById(eq(-1L))).thenReturn(Optional.empty());
        expenseService.getExpense(-1L);
    }

    @Test
    public void shouldReturnExpense() throws EntityNotFoundException {
        when(expenseRepository.findById(eq(1L))).thenReturn(Optional.of(Expense.builder().id(1L).name("name").build()));
        when(modelMapper.map(any(), any())).thenReturn(ExpenseDTO.builder().id(1L).name("name").build());
        when(associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(any(), any(), anyLong()))
                .thenReturn(Collections.singletonList(AssociationDto.builder().destinationId(100L).build()));

        ExpenseDTO expense = expenseService.getExpense(1L);
        assertNotNull(expense);
        assertEquals(1L, expense.getId().longValue());
        assertEquals("name", expense.getName());
        assertEquals(100L, expense.getActivityId().longValue());
    }
    @Test
    public void shouldReturnExpenseWhenGetEmptyList() throws EntityNotFoundException {
        when(expenseRepository.findById(eq(1L))).thenReturn(Optional.of(Expense.builder().id(1L).name("name").build()));
        when(modelMapper.map(any(), any())).thenReturn(ExpenseDTO.builder().id(1L).name("name").build());
        when(associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(any(), any(), anyLong()))
                .thenReturn(new ArrayList<>());

        ExpenseDTO expense = expenseService.getExpense(1L);
        assertNotNull(expense);
        assertEquals(1L, expense.getId().longValue());
        assertEquals("name", expense.getName());
        assertNull(expense.getActivityId());
    }

    @Test
    public void shouldRemoveOldAssociationsAndSaveNewWhenSaveNewExpense() throws KafkaSendMessageException, BadRequestException, AssociationClientException, EntityNotFoundException {
        ExpenseDTO expenseDTO = new ExpenseDTO();
        expenseDTO.setLeadId(1L);
        expenseDTO.setAmount(1.0);
        expenseDTO.setActivityId(200L);

        when(leadRepository.findById(1L)).thenReturn(Optional.of(new Lead()));
        when(accessService.assignAndVerify(any())).thenReturn(expenseDTO);
        when(userClient.getMyAccountId(anyString())).thenReturn(1L);
        when(userClient.getSupervisorId(anyString())).thenReturn(2L);
        Expense saved = Expense.builder().id(1L).amount(1.0).build();
        when(modelMapper.map(any(), eq(Expense.class))).thenReturn(saved);
        when(modelMapper.map(any(), eq(ExpenseDTO.class))).thenReturn(expenseDTO);
        when(expenseRepository.save(any())).thenReturn(saved);
        when(userClient.getExpenseAcceptanceThreshold()).thenReturn(new GlobalConfigurationDTO(1L, "test", "500"));

        expenseService.save(expenseDTO);

        verify(expenseRepository, times(1)).save(any());
        verify(associationClient, times(1)).deleteAssociationsBySourceTypeAndSourceIdAndDestinationType(eq(CrmObjectType.expense), eq(1L), eq(CrmObjectType.activity));
        verify(associationClient, times(1)).createAssociation(any());
    }
}
