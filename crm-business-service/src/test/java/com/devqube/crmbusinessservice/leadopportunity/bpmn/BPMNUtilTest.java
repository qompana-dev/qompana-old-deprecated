package com.devqube.crmbusinessservice.leadopportunity.bpmn;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Objects;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class BPMNUtilTest {

    @Test
    public void shouldJoinWhenNulls() {
        assertEquals(";", BPMNUtil.joinAcceptanceAndNotification(null, null));
    }

    @Test
    public void shouldJoinWhenFirstIsEmpty() {
        assertEquals(";-5", BPMNUtil.joinAcceptanceAndNotification("", "-5"));
    }

    @Test
    public void shouldJoinWhenSecondIsEmpty() {
        assertEquals("22;", BPMNUtil.joinAcceptanceAndNotification("22", ""));
    }

    @Test
    public void shouldJoinWhenBothAreEmpty() {
        assertEquals(";", BPMNUtil.joinAcceptanceAndNotification("", ""));
    }

    @Test
    public void shouldJoinWhenBothHasValues() {
        assertEquals("343443;12213", BPMNUtil.joinAcceptanceAndNotification("343443", "12213"));
    }

    @Test
    public void shouldParseAcceptanceIdCorrectlyWhenPresent() {
        assertEquals("343443", Objects.requireNonNull(BPMNUtil.getAcceptanceId("343443;12213")));
    }

    @Test
    public void shouldParseNotificationIdCorrectlyWhenPresent() {
        assertEquals("12213", Objects.requireNonNull(BPMNUtil.getNotificationId("343443;12213")));
    }

    @Test
    public void shouldReturnNullWhenAcceptanceIdNotPresent() {
        assertEquals("", BPMNUtil.getAcceptanceId(";12213"));
    }

    @Test
    public void shouldReturnNullWhenNotificationIdNotPresent() {
        assertEquals("", BPMNUtil.getNotificationId("12213;"));
    }

    @Test
    public void shouldReturnNullWhenNotificationAndAcceptanceNotPresent() {
        assertEquals("", BPMNUtil.getNotificationId(";"));
        assertEquals("", BPMNUtil.getAcceptanceId(";"));
    }
}
