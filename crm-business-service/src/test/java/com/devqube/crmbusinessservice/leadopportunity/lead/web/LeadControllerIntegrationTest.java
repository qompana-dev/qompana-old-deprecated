package com.devqube.crmbusinessservice.leadopportunity.lead.web;

import com.devqube.crmbusinessservice.AbstractIntegrationTest;
import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadTestUtil;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ConvertedOpportunityDto;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadConversionDto;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadSaveDTO;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmshared.web.RestPageImpl;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.impl.bpmn.deployer.ResourceNameUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.netflix.ribbon.StaticServerList;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@ActiveProfiles({"test","wiremock"})
@ContextConfiguration(classes = {LeadControllerIntegrationTest.LocalClientConfiguration.class})
public class LeadControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private RepositoryService repositoryService;

    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(WireMockConfiguration.options().dynamicPort());


    @Autowired
    private LeadRepository leadRepository;
    @Autowired
    private OpportunityRepository opportunityRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ContactRepository contactRepository;

    private LeadSaveDTO leadSaveDTO;
    private String processDefinitionId;
    private String deploymentId;
    private static final String PROCESS_DEFINITION_JSON = "{\"name\":\"process\",\"id\":\"processId\",\"objectType\":\"LEAD\",\"tasks\":[{\"name\":\"task0\",\"properties\":[{\"id\":\"prop1Id\",\"type\":\"STRING\",\"required\":false},{\"id\":\"prop2Id\",\"type\":\"ENUM\",\"required\":true,\"possibleValues\":[{\"id\":\"posibbleId\",\"name\":\"Some value\"},{\"id\":\"possibleId2\",\"name\":\"Some value 2\"}]}]},{\"name\":\"task1\"},{\"name\":\"task2\"},{\"name\":\"task3\"},{\"name\":\"task4\"}]}";
    private HttpHeaders headers = null;
    private Lead lead;

    @Before
    public void setUp() throws IOException {
        super.setUp();
        opportunityRepository.deleteAll();
        leadRepository.deleteAll();
        createBusinessProcessDefinition();
        this.leadSaveDTO = new LeadSaveDTO();
        leadSaveDTO.setProcessDefinitionId(processDefinitionId);
        leadSaveDTO.setIndustry(0L);
        leadSaveDTO.setPhone("1234");
        leadSaveDTO.setLastName("aaaa");
        leadSaveDTO.setLeadKeeperId(1L);

        lead = LeadTestUtil.createLead();

        headers = new HttpHeaders();
        headers.set("Logged-Account-Email", "jan.kowalski@devqube.com");


        stubFor(post(urlEqualTo("/internal/associations/copy"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200)));

        stubFor(post(urlEqualTo("/internal/note/copy"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200)));

        stubFor(get(urlEqualTo("/internal/accounts/jan.kowalski%40devqube.com/role"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("ceo")));

        stubFor(get(urlEqualTo("/role/name/ceo/child/accounts/id"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("[-1,2,3]")));

        stubFor(get(urlEqualTo("/accounts/me/id"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("-1")));
    }

    @After
    public void cleanUp() {
        repositoryService.deleteDeployment(deploymentId, true);
        leadRepository.deleteAll();
    }

    @Test
    public void shouldSaveLeadWhenProcessDefinitionExists() {
        ResponseEntity<LeadSaveDTO> responseEntity = restTemplate.postForEntity(baseUrl + "/leads",
                new HttpEntity<>(leadSaveDTO, headers), LeadSaveDTO.class);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }

    @Test
    public void shouldReturn400WhenProcessDefinitionNotExists() {
        leadSaveDTO.setProcessDefinitionId("not existing");
        HttpEntity<LeadSaveDTO> entity = new HttpEntity<>(leadSaveDTO, headers);
        ResponseEntity<LeadSaveDTO> responseEntity = restTemplate.postForEntity(baseUrl + "/leads",
                entity, LeadSaveDTO.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    public void shouldReturn400WhenCompanyTypeNotExists() {
        leadSaveDTO.setIndustry(-100L);
        ResponseEntity<LeadSaveDTO> responseEntity = restTemplate.postForEntity(baseUrl + "/leads",
                new HttpEntity<>(leadSaveDTO, headers), LeadSaveDTO.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    public void shouldRetrieveListOfLeadsWhenSomePresent() {
        restTemplate.postForEntity(baseUrl + "/leads", new HttpEntity<>(leadSaveDTO, headers), LeadSaveDTO.class);

        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(baseUrl + "/leads")
                .queryParam("page", "0")
                .queryParam("size", "10");

        ResponseEntity<RestPageImpl> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, new HttpEntity<>(headers), RestPageImpl.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(1, responseEntity.getBody().getNumberOfElements());
    }

    @Test
    public void shouldRetrieveEmptyListWhenNoLeadsPresent() {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(baseUrl + "/leads")
                .queryParam("page", "0")
                .queryParam("size", "10");

        ResponseEntity<RestPageImpl> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, new HttpEntity<>(headers), RestPageImpl.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(0, responseEntity.getBody().getNumberOfElements());
    }

    @Test
    public void shouldRetrieveOnlyTodayLeads() {
        restTemplate.postForEntity(baseUrl + "/leads", new HttpEntity<>(leadSaveDTO, headers), LeadSaveDTO.class);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(baseUrl + "/leads")
                .queryParam("page", "0")
                .queryParam("size", "10")
                .queryParam("today", "true");

        ResponseEntity<RestPageImpl> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, new HttpEntity<>(headers), RestPageImpl.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(1, responseEntity.getBody().getNumberOfElements());
    }

    @Test
    public void shouldRetrieveLeadsOnlyForAcceptance() {
        restTemplate.postForEntity(baseUrl + "/leads", new HttpEntity<>(leadSaveDTO, headers), LeadSaveDTO.class);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(baseUrl + "/leads")
                .queryParam("page", "0")
                .queryParam("size", "10")
                .queryParam("for-approving", "true");

        ResponseEntity<RestPageImpl> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, new HttpEntity<>(headers), RestPageImpl.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(0, responseEntity.getBody().getNumberOfElements());


        List<Lead> all = leadRepository.findAll();
        assertEquals(1, all.size());
        Lead lead = all.get(0);
        lead.setAcceptanceUserId(-1L);
        leadRepository.save(lead);

        responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, new HttpEntity<>(headers), RestPageImpl.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(1, responseEntity.getBody().getNumberOfElements());
    }

    @Test
    public void shouldRetrieveOnlyOneOfTwoLeads() {
        HttpEntity<LeadSaveDTO> entity = new HttpEntity<>(leadSaveDTO, headers);
        restTemplate.postForEntity(baseUrl + "/leads", entity, LeadSaveDTO.class);
        restTemplate.postForEntity(baseUrl + "/leads", entity, LeadSaveDTO.class);

        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(baseUrl + "/leads")
                .queryParam("page", "0")
                .queryParam("size", "1")
                .queryParam("today", "true");

        ResponseEntity<RestPageImpl> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, new HttpEntity<>(headers), RestPageImpl.class, entity);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(1, responseEntity.getBody().getNumberOfElements());

    }

    @Test
    public void shouldModifyLeadWhenRequestOK() {
        LeadSaveDTO lead = restTemplate.postForEntity(baseUrl + "/leads", new HttpEntity<>(leadSaveDTO, headers), LeadSaveDTO.class).getBody();
        assertNotNull(lead);
        lead.setFirstName("test");

        ResponseEntity<LeadSaveDTO> updateResponse = restTemplate.exchange(baseUrl + "/leads/" + lead.getId(),
                HttpMethod.PUT, new HttpEntity<>(lead, headers), LeadSaveDTO.class);
        assertEquals(HttpStatus.OK, updateResponse.getStatusCode());
        assertNotNull(updateResponse.getBody());
        assertEquals("test", updateResponse.getBody().getFirstName());

    }

    @Test
    public void shouldReturn404WhenModifyingNotExistingLead() {
        ResponseEntity<LeadSaveDTO> updateResponse = restTemplate.exchange(baseUrl + "/leads/-100",
                HttpMethod.PUT, new HttpEntity<>(leadSaveDTO, headers), LeadSaveDTO.class);
        assertEquals(HttpStatus.NOT_FOUND, updateResponse.getStatusCode());
    }

    @Test
    public void shouldReturn400WhenModifyingAndCompanyTypeNotExists() {
        LeadSaveDTO lead = restTemplate.postForEntity(baseUrl + "/leads", new HttpEntity<>(leadSaveDTO, headers), LeadSaveDTO.class).getBody();
        assertNotNull(lead);
        leadSaveDTO.setIndustry(-100L);
        ResponseEntity<LeadSaveDTO> updateResponse = restTemplate.exchange(baseUrl + "/leads/" + lead.getId(),
                HttpMethod.PUT, new HttpEntity<>(leadSaveDTO, headers), LeadSaveDTO.class);
        assertEquals(HttpStatus.BAD_REQUEST, updateResponse.getStatusCode());
    }

    @Test
    public void shouldDeleteLeadWhenExists() {
        LeadSaveDTO lead = restTemplate.postForEntity(baseUrl + "/leads", new HttpEntity<>(leadSaveDTO, headers), LeadSaveDTO.class).getBody();
        assertNotNull(lead);

        ResponseEntity<Void> responseEntity = restTemplate.exchange(baseUrl + "/leads/" + lead.getId(),
                HttpMethod.DELETE, new HttpEntity<>(headers), Void.class);
        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }

    @Test
    public void shouldReturn404WhenDeletingAndLeadNotExists() {
        ResponseEntity<Void> responseEntity = restTemplate.exchange(baseUrl + "/leads/-100",
                HttpMethod.DELETE, new HttpEntity<>(headers), Void.class);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void shouldDeleteListOfLeadsWhenExist() {
        HttpEntity<LeadSaveDTO> entity = new HttpEntity<>(leadSaveDTO, headers);
        LeadSaveDTO lead1 = restTemplate.postForEntity(baseUrl + "/leads", entity, LeadSaveDTO.class).getBody();
        LeadSaveDTO lead2 = restTemplate.postForEntity(baseUrl + "/leads", entity, LeadSaveDTO.class).getBody();
        assertNotNull(lead1);
        assertNotNull(lead2);

        ResponseEntity<Void> responseEntity = restTemplate.exchange(baseUrl + "/leads?ids=" + lead1.getId() + ","
                + lead2.getId(), HttpMethod.DELETE, new HttpEntity<>(headers), Void.class);
        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }

    @Test
    public void shouldReturn404WhenDeletingListAndOneLeadNotExists() {
        LeadSaveDTO lead1 = restTemplate.postForEntity(baseUrl + "/leads", new HttpEntity<>(leadSaveDTO, headers), LeadSaveDTO.class).getBody();
        assertNotNull(lead1);

        ResponseEntity<Void> responseEntity = restTemplate.exchange(baseUrl + "/leads?ids=" + lead1.getId() + ","
                + "-100", HttpMethod.DELETE, new HttpEntity<>(headers), Void.class);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void shouldConvertLeadToOpportunity() {
        lead.setLeadKeeperId(-1L);
        lead = leadRepository.save(lead);
        LeadConversionDto leadConversionDto = LeadTestUtil.createLeadConversionDto();
        leadConversionDto.setOpportunityProcessDefinitionId(processDefinitionId);
        ResponseEntity<ConvertedOpportunityDto> response = restTemplate.postForEntity(String.format("%s/leads/%s/convert-to-opportunity", baseUrl, lead.getId()), new HttpEntity<>(leadConversionDto, headers), ConvertedOpportunityDto.class);
        ConvertedOpportunityDto body = response.getBody();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(body);
        assertNotNull(body.getOpportunityId());
        body.setOpportunityId(null);
        assertThat(body).isEqualTo(getConversionResult());
    }

    private ConvertedOpportunityDto getConversionResult() {
        ConvertedOpportunityDto expectedResult = LeadTestUtil.createExpectedResult();
        expectedResult.setOpportunityOwner(-1L);
        expectedResult.setContactOwner(-1L);
        expectedResult.setCustomerOwner(-1L);
        expectedResult.setOpportunityId(null);
        return expectedResult;
    }

    private void createBusinessProcessDefinition() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(PROCESS_DEFINITION_JSON, headers);
        ResponseEntity<Resource> resourceResponseEntity = restTemplate.postForEntity(baseUrl + "/bpmn/generate",
                entity, Resource.class);
        deploymentId = this.repositoryService.createDeployment().addString("xmlString." + ResourceNameUtil.BPMN_RESOURCE_SUFFIXES[0],
                new String(resourceResponseEntity.getBody().getInputStream().readAllBytes())).deploy().getId();
        processDefinitionId = this.repositoryService
                .createProcessDefinitionQuery()
                .deploymentId(deploymentId)
                .singleResult()
                .getId();
    }

    @TestConfiguration
    public static class LocalClientConfiguration {
        @Bean
        public ServerList<Server> serverList() {
            return new StaticServerList<>(new Server("localhost", wireMockRule.port()));
        }
    }


}
