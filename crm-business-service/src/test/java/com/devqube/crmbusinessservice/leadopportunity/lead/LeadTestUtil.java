package com.devqube.crmbusinessservice.leadopportunity.lead;

import com.devqube.crmbusinessservice.customer.address.Address;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.leadopportunity.company.Company;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ConvertedOpportunityDto;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadConversionDto;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class LeadTestUtil {

    public static Lead createLead() {
        Lead lead = new Lead();
        lead.setFirstName("Jan");
        lead.setLastName("Kowalski");
        lead.setPhone("12312312312");
        lead.setEmail("email");
        lead.setConvertedToOpportunity(false);
        lead.setLeadKeeperId(11L);
        lead.setCreator(22L);
        Company company = new Company();
        company.setStreetAndNumber("street");
        company.setCity("city");
        company.setPostalCode("code");
        company.setCountry("country");
        lead.setCompany(company);
        return lead;
    }

    public static LeadConversionDto createLeadConversionDto() {
        LeadConversionDto leadConversionDto = new LeadConversionDto();
        leadConversionDto.setContactId(99L);
        leadConversionDto.setContactName("contactName");
        leadConversionDto.setContactPosition("contactPosition");
        leadConversionDto.setContactSurname("contactSurname");
        leadConversionDto.setIsExistingContact(false);

        leadConversionDto.setCustomerId(88L);
        leadConversionDto.setCustomerName("customerName");
        leadConversionDto.setCustomerNip("nip");
        leadConversionDto.setIsExistingCustomer(false);

        leadConversionDto.setNoOpportunity(false);
        leadConversionDto.setOpportunityAmount(100.00);
        leadConversionDto.setOpportunityFinishDate(LocalDate.parse("2007-12-03"));
        leadConversionDto.setOpportunityCurrency("PLN");
        leadConversionDto.setOpportunityName("opportunityName");
        leadConversionDto.setOpportunityProcessDefinitionId("processDefinitionId");
        return leadConversionDto;
    }

    public static ConvertedOpportunityDto createExpectedResult() {
        ConvertedOpportunityDto dto = new ConvertedOpportunityDto();
        dto.setCustomerName("customerName");
        dto.setCustomerNip("nip");
        dto.setCustomerOwner(100L);
        Address address = new Address();
        address.setStreet("street");
        address.setCity("city");
        address.setZipCode("code");
        address.setCountry("country");
        dto.setCustomerAddress(address);
        dto.setContactName("contactName contactSurname");
        dto.setContactPosition("contactPosition");
        dto.setContactOwner(100L);
        dto.setOpportunityId(777L);
        dto.setOpportunityName("opportunityName");
        dto.setOpportunityProcessName("opportunityProcessName");
        dto.setOpportunitySubjectOfInterest("opportunitySubjectOfInterest");
        dto.setOpportunityAmount(100.00);
        dto.setOpportunityCurrency("PLN");
        dto.setOpportunityOwner(100L);
        return dto;
    }

    public static Contact createContact(Customer customer) {
        Contact contact = new Contact();
        contact.setName("contactName");
        contact.setSurname("contactSurname");
        contact.setPosition("contactPosition");
        contact.setEmail("email");
        contact.setPhone("12312312312");
        contact.setOwner(100L);
        contact.setCustomer(customer);
        return contact;
    }

    public static Customer createCustomer() {
        Customer customer = new Customer();
        customer.setName("customerName");
        customer.setNip("nip");
        customer.setOwner(100L);
        customer.setWebsite("website");
        Address address = new Address();
        address.setStreet("street");
        address.setCity("city");
        address.setZipCode("code");
        address.setCountry("country");
        customer.setAddress(address);
        return customer;
    }

    public static Opportunity createOpportunity(Customer customer, Contact contact) {
        Opportunity opportunity = new Opportunity();
        opportunity.setCreator(100L);
        opportunity.setOwnerOneId(100L);
        opportunity.setOwnerOnePercentage(100L);
        opportunity.setProcessInstanceId("processInstanceId");
        opportunity.setAmount(100.00);
        opportunity.setCurrency("PLN");
        opportunity.setCreateDate(LocalDate.parse("2007-11-03"));
        opportunity.setFinishDate(LocalDate.parse("2007-12-03"));
        opportunity.setName("opportunityName");
        opportunity.setCustomer(customer);
        opportunity.setContact(contact);
        return opportunity;
    }
}
