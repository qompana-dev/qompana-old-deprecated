package com.devqube.crmbusinessservice.leadopportunity.processEngine;

import com.devqube.crmbusinessservice.AbstractIntegrationTest;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Property;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadDetailsDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineProxyService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.IncorrectNotificationTypeException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.netflix.ribbon.StaticServerList;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ActiveProfiles({"test", "wiremock"})
@ContextConfiguration(classes = {ProcessEngineProxyServiceTest.LocalClientConfiguration.class})
public class ProcessEngineProxyServiceTest extends AbstractIntegrationTest {

    @Autowired
    private ProcessEngineProxyService processEngineProxyService;
    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private LeadRepository leadRepository;

    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(WireMockConfiguration.options().dynamicPort());

    private Deployment deployment;
    private ProcessInstance processInstance;
    private Lead lead;
    private String processDefinitionId;
    private HttpHeaders headers = null;

    private final HashMap<String, Object> task0Properties = new HashMap<>() {{
        put("text", "text");
        put("number", 50.0);
        put("enum", "18ee5bc9-3001-43b1-8fcf-a2a73d1af5ca");
    }};

    @Before
    public void setUp() throws IOException {
        super.setUp();
        removeAllProcessDefinitions();
        deployment = repositoryService
                .createDeployment()
                .name("test-deployment")
                .addClasspathResource("process.bpmn")
                .deploy();
        processDefinitionId = repositoryService
                .createProcessDefinitionQuery()
                .deploymentId(deployment.getId())
                .singleResult()
                .getId();
        processInstance = runtimeService.startProcessInstanceById(processDefinitionId);

        lead = new Lead();
        lead.setLastName("test");
        lead.setPhone("48666222111");
        lead.setProcessInstanceId(processInstance.getProcessInstanceId());
        lead.setCreator(-1L);
        lead = leadRepository.save(lead);

        stubFor(get(urlEqualTo("/role/email/jan.kowalski%40devqube.com/with-children/id"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("[-4]")));

        headers = new HttpHeaders();
        headers.set("Logged-Account-Email", "jan.kowalski@devqube.com");
    }

    @After
    public void cleanUp() {
        leadRepository.deleteAll();
        try {
            repositoryService.deleteDeployment(deployment.getId(), true);
        } catch (FlowableObjectNotFoundException e) {
            // everything OK, deployment was delete directly in test
        }
    }

    @Test
    public void shouldCreateProcessDefinition() {
        assertNotNull(repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getId()).singleResult());
    }

    @Test
    public void shouldCreateProcessInstance() {
        assertNotNull(runtimeService.createProcessInstanceQuery().processInstanceId(processInstance.getId()));
    }

    @Test(expected = BadRequestException.class)
    public void shouldNotCompleteTaskWhenRequiredFieldsNotPresent() throws BadRequestException, KafkaSendMessageException, EntityNotFoundException, IncorrectNotificationTypeException {
        processEngineProxyService.completeTask(processInstance.getId());
    }

    @Test
    public void shouldCompleteTaskWhenRequiredFieldsAreSet() throws BadRequestException, KafkaSendMessageException, EntityNotFoundException, IncorrectNotificationTypeException {
        assertEquals("task0", getActiveTaskDefinitionKey());
        processEngineProxyService.postFormProperties(processInstance.getId(), task0Properties);
        processEngineProxyService.completeTask(processInstance.getId());
        assertEquals("task1", getActiveTaskDefinitionKey());
    }

    @Test
    public void shouldSendTaskToAcceptanceWhenAcceptanceRequired() throws BadRequestException, KafkaSendMessageException, EntityNotFoundException, IncorrectNotificationTypeException {
        assertEquals("task0", getActiveTaskDefinitionKey());
        processEngineProxyService.postFormProperties(processInstance.getId(), task0Properties);
        processEngineProxyService.completeTask(processInstance.getId());
        assertEquals("task1", getActiveTaskDefinitionKey());
        boolean completed = processEngineProxyService.completeTask(processInstance.getId());
        assertFalse(completed);
        assertEquals("task1", getActiveTaskDefinitionKey());
        Optional<Lead> byId = leadRepository.findById(lead.getId());
        assertTrue(byId.isPresent());
        assertEquals(-2L, byId.get().getAcceptanceUserId().longValue());
    }

    @Test
    public void shouldGoBackToPreviousTask() throws BadRequestException, KafkaSendMessageException, EntityNotFoundException, IncorrectNotificationTypeException {
        processEngineProxyService.postFormProperties(processInstance.getId(), task0Properties);
        processEngineProxyService.completeTask(processInstance.getId());
        processEngineProxyService.goBackToPreviousTask(processInstance.getId(), 1);
        assertEquals("task0", getActiveTaskDefinitionKey());
    }

    @Test
    public void shouldAcceptStepAndGoTwoStepsBack() throws BadRequestException, KafkaSendMessageException, EntityNotFoundException, IOException, IncorrectNotificationTypeException {
        leadRepository.deleteAll();
        ProcessDefinitionDTO processDefinitionDTO = processEngineProxyService.deployProcessDefinition(new FileInputStream(new File("src/test/resources/processForAll.bpmn")));
        processInstance = runtimeService.startProcessInstanceById(processDefinitionDTO.getId());
        lead.setProcessInstanceId(processInstance.getProcessInstanceId());
        lead = leadRepository.save(lead);

        processEngineProxyService.postFormProperties(processInstance.getId(), task0Properties);
        processEngineProxyService.completeTask(processInstance.getId());
        processEngineProxyService.completeTask(processInstance.getId());
        processEngineProxyService.goBackToPreviousTask(processInstance.getId(), 2);
        assertEquals("task0", getActiveTaskDefinitionKey());

        repositoryService.deleteDeployment(repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionDTO.getId())
                .singleResult()
                .getDeploymentId(), true);
    }

    @Test
    public void shouldReturnPreviousStepWithAllPropertiesSet() throws BadRequestException, EntityNotFoundException, KafkaSendMessageException, IncorrectNotificationTypeException {
        processEngineProxyService.postFormProperties(processInstance.getId(), task0Properties);
        processEngineProxyService.completeTask(processInstance.getId());
        LeadDetailsDTO.Task previousTask = processEngineProxyService.getPreviousTask(processInstance.getId(), "task0");
        assertEquals("task0", previousTask.getId());
        assertEquals("Etap 1", previousTask.getName());
        assertEquals("Wskazówki lub krótki opis etapu", previousTask.getDocumentation());
        assertTrue(previousTask.isPast());

        for (Property property : previousTask.getProperties()) {
            String expectedValue = task0Properties.get(property.getId()).toString();
            assertNotNull(expectedValue);
            assertEquals(expectedValue, property.getValue());
        }
    }

    @Test
    public void shouldReturnFutureStepWithAllPropertiesSet() throws EntityNotFoundException {
        LeadDetailsDTO.Task task = processEngineProxyService.getFutureTask(processInstance.getId(), "task1");
        assertNotNull(task);
        assertEquals("task1", task.getId());
        assertEquals("Etap 2", task.getName());
        assertEquals("Wskazówki lub krótki opis etapu", task.getDocumentation());
        assertFalse(task.isPast());
        assertFalse(task.isActive());
        assertEquals(2L, task.getProperties().size());
    }

    @Test
    public void shouldDeployNewProcessDefinition() throws IOException, BadRequestException {
        assertEquals(1L, repositoryService.createProcessDefinitionQuery().list().size());
        ProcessDefinitionDTO processDefinitionDTO = processEngineProxyService.deployProcessDefinition(new FileInputStream(new File("src/test/resources/processForAll.bpmn")));
        assertEquals(2L, repositoryService.createProcessDefinitionQuery().list().size());
        repositoryService.deleteDeployment(repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionDTO.getId())
                .singleResult()
                .getDeploymentId(), true);
    }

    @Test
    public void shouldDeployNewProcessDefinitionForAll() throws IOException, BadRequestException {
        ProcessDefinitionDTO processDefinitionDTO = processEngineProxyService.deployProcessDefinition(new FileInputStream(new File("src/test/resources/processForAll.bpmn")));
        ResponseEntity<ProcessDefinitionDTO[]> response = restTemplate.exchange(baseUrl +
                        "/process-engine/repository/process-definitions/LEAD", HttpMethod.GET,
                new HttpEntity<>(headers), ProcessDefinitionDTO[].class);
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2L, response.getBody().length);

        repositoryService.deleteDeployment(repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionDTO.getId())
                .singleResult()
                .getDeploymentId(), true);
    }

    @Test
    public void shouldDeployNewProcessDefinitionForDepartment() throws IOException, BadRequestException {
        ProcessDefinitionDTO processDefinitionDTO = processEngineProxyService.deployProcessDefinition(new FileInputStream(new File("src/test/resources/processForDepartment.bpmn")));
        ResponseEntity<ProcessDefinitionDTO[]> response = restTemplate.exchange(baseUrl +
                        "/process-engine/repository/process-definitions/LEAD", HttpMethod.GET,
                new HttpEntity<>(headers), ProcessDefinitionDTO[].class);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2L, response.getBody().length);

        repositoryService.deleteDeployment(repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionDTO.getId())
                .singleResult()
                .getDeploymentId(), true);
    }

    @Test
    public void shouldGetOnlyProcessesForMyDepartmentAndForAll() throws IOException, BadRequestException {
        ProcessDefinitionDTO processDefinitionDTO = processEngineProxyService.deployProcessDefinition(new FileInputStream(new File("src/test/resources/processForAll.bpmn")));
        ProcessDefinitionDTO processDefinitionDTO2 = processEngineProxyService.deployProcessDefinition(new FileInputStream(new File("src/test/resources/processForDepartment.bpmn")));
        ProcessDefinitionDTO processDefinitionDTO3 = processEngineProxyService.deployProcessDefinition(new FileInputStream(new File("src/test/resources/processForAnotherDepartment.bpmn")));
        ResponseEntity<ProcessDefinitionDTO[]> response = restTemplate.exchange(baseUrl +
                        "/process-engine/repository/process-definitions/LEAD", HttpMethod.GET,
                new HttpEntity<>(headers), ProcessDefinitionDTO[].class);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(3L, response.getBody().length);

        repositoryService.deleteDeployment(repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionDTO.getId())
                .singleResult()
                .getDeploymentId(), true);
        repositoryService.deleteDeployment(repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionDTO2.getId())
                .singleResult()
                .getDeploymentId(), true);
        repositoryService.deleteDeployment(repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionDTO3.getId())
                .singleResult()
                .getDeploymentId(), true);
    }

    @Test
    public void shouldDeleteProcessWithOneVersionAndNoLeadAssigned() throws BadRequestException, EntityNotFoundException {
        leadRepository.deleteAll();
        processEngineProxyService.deleteProcessDefinition(processDefinitionId);
        assertTrue(repositoryService.createProcessDefinitionQuery().list().isEmpty());
    }

    @Test
    public void shouldDeleteProcessWithTwoVersionsAndNoLeadAssigned() throws IOException, BadRequestException, EntityNotFoundException {
        leadRepository.deleteAll();
        ProcessDefinitionDTO processDefinitionDTO = processEngineProxyService.deployProcessDefinition(new FileInputStream(new File("src/test/resources/process.bpmn")));
        processEngineProxyService.deleteProcessDefinition(processDefinitionDTO.getId());
        assertTrue(repositoryService.createProcessDefinitionQuery().list().isEmpty());
    }

    @Test(expected = BadRequestException.class)
    public void shouldNotDeleteProcessWithOneVersionAndOneLeadAssigned() throws BadRequestException, EntityNotFoundException {
        processEngineProxyService.deleteProcessDefinition(processDefinitionId);
    }

    @Test(expected = BadRequestException.class)
    public void shouldNotDeleteProcessWithTwoVersionsAndOneLeadAssignedToFirstVersion() throws IOException, BadRequestException, EntityNotFoundException {
        ProcessDefinitionDTO processDefinitionDTO = processEngineProxyService.deployProcessDefinition(new FileInputStream(new File("src/test/resources/process.bpmn")));
        processEngineProxyService.deleteProcessDefinition(processDefinitionDTO.getId());
    }

    @Test(expected = BadRequestException.class)
    public void shouldNotDeleteProcessWithTwoVersionsAndOneLeadAssignedToSecondVersion() throws BadRequestException, EntityNotFoundException, IOException {
        leadRepository.deleteAll();
        ProcessDefinitionDTO processDefinitionDTO = processEngineProxyService.deployProcessDefinition(new FileInputStream(new File("src/test/resources/process.bpmn")));
        processInstance = runtimeService.startProcessInstanceById(processDefinitionDTO.getId());
        lead.setProcessInstanceId(processInstance.getProcessInstanceId());
        lead = leadRepository.save(lead);
        processEngineProxyService.deleteProcessDefinition(processDefinitionDTO.getId());
    }

    private String getActiveTaskDefinitionKey() {
        return taskService
                .createTaskQuery()
                .processInstanceId(processInstance.getId())
                .singleResult()
                .getTaskDefinitionKey();
    }

    private void removeAllProcessDefinitions() {
        List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().list();
        for (ProcessDefinition pd : list) {
            List<ProcessDefinition> allVersionOfProcesses = repositoryService.createProcessDefinitionQuery()
                    .processDefinitionKey(pd.getKey()).list();
            for (ProcessDefinition process : allVersionOfProcesses) {
                repositoryService.deleteDeployment(process.getDeploymentId(), true);
            }
        }
    }

    @TestConfiguration
    public static class LocalClientConfiguration {
        @Bean
        public ServerList<Server> serverList() {
            return new StaticServerList<>(new Server("localhost", wireMockRule.port()));
        }
    }
}
