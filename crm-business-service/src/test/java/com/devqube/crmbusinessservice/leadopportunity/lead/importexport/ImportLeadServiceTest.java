package com.devqube.crmbusinessservice.leadopportunity.lead.importexport;

import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Process;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parseexception.exception.ParseException;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.LeadPriority;
import com.devqube.crmbusinessservice.leadopportunity.lead.importexport.parser.ParsedLead;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.model.ProcessDefinitionListDTO;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineHelperService;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineProxyService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.user.dto.AccountEmail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ImportLeadServiceTest {

    @Mock
    private UserService userService;

    @Mock
    private ProcessEngineProxyService processEngineProxyService;

    @Mock
    private ProcessEngineHelperService processEngineHelperService;

    @Mock
    private LeadRepository leadRepository;

    @InjectMocks
    private ImportLeadService importLeadService;

    @Test
    public void shouldImportLead() throws BadRequestException, ParseException {
        when(userService.getUserId(eq("jan.kowalski@devqube.com"))).thenReturn(-1L);
        when(processEngineProxyService.getProcessDefinitionsList(eq(Process.ObjectTypeEnum.LEAD.name()))).thenReturn(getProcessDefinitionList());

        when(processEngineHelperService.startProcessInstance(eq("processDefinitionId1")))
                .thenReturn(new ProcessInstanceImplForTests("processInstanceId1", "processDefinitionId1", "processDefinitionName1"));
        when(processEngineHelperService.startProcessInstance(eq("processDefinitionId2")))
                .thenReturn(new ProcessInstanceImplForTests("processInstanceId2", "processDefinitionId2", "processDefinitionName2"));
        when(processEngineHelperService.startProcessInstance(eq("processDefinitionId3")))
                .thenReturn(new ProcessInstanceImplForTests("processInstanceId3", "processDefinitionId3", "processDefinitionName3"));

        when(userService.getAccountEmails()).thenReturn(getAccountEmails());
        when(leadRepository.saveAll(any())).thenAnswer(i -> i.getArgument(0));

        String toTest = getHeaders() + getValues(1L) + getValues(2L) + getValues(3L);
        List<ParsedLead> leads = importLeadService.parseImportFile(new ByteArrayInputStream(toTest.getBytes()), "csv");

        assertNotNull(leads);
        assertEquals(3, leads.size());

        Mockito.verify(leadRepository, times(1)).saveAll(any());
    }

    private List<AccountEmail> getAccountEmails() {
        List<AccountEmail> result = new ArrayList<>();
        result.add(new AccountEmail(1, "jan.kowalski@devqube.com", "Jan", "Kowalski"));
        result.add(new AccountEmail(2, "marek.nowak@devqube.com", "Marek", "Nowak"));
        result.add(new AccountEmail(3, "janusz.polak@devqube.com", "Janusz", "Polak"));
        return result;
    }

    private List<ProcessDefinitionListDTO> getProcessDefinitionList() {
        List<ProcessDefinitionListDTO> result = new ArrayList<>();
        result.add(new ProcessDefinitionListDTO("processDefinitionId1", "processDefinitionName1", 20L, "$fff", "#000", new ArrayList<>()));
        result.add(new ProcessDefinitionListDTO("processDefinitionId2", "processDefinitionName2", 20L, "$fff", "#000", new ArrayList<>()));
        result.add(new ProcessDefinitionListDTO("processDefinitionId3", "processDefinitionName3", 20L, "$fff", "#000", new ArrayList<>()));
        return result;
    }

    private String getHeaders() {
        return "processDefinitionName;" +
                "salesOpportunity;" +
                "status;" +
                "firstName;" +
                "lastName;" +
                "position;" +
                "email;" +
                "phone;" +
                "sourceOfAcquisition;" +
                "leadKeeperName;" +
                "companyName;" +
                "companyWwwAddress;" +
                "companyStreetAndNumber;" +
                "companyCity;" +
                "companyPostalCode;" +
                "companyProvince;" +
                "companyCountry;" +
                "companyNotes;\n";
    }

    private String getValues(Long index) {
        return "processDefinitionName" + index + ";" +
                LeadPriority.HIGH + ";" +
                "firstName" + index + ";" +
                "lastName" + index + ";" +
                "position" + index + ";" +
                "email" + index + ";" +
                "1231231231" + index + ";" +
                "sourceOfAcquisition" + index + ";" +
                "Jan Kowalski" + ";" +
                "leads.form.companyTypes.it;" +
                "companyName" + index + ";" +
                "companyWwwAddress" + index + ";" +
                "companyStreetAndNumber" + index + ";" +
                "companyCity" + index + ";" +
                "companyPostalCode" + index + ";" +
                "companyProvince" + index + ";" +
                "companyCountry" + index + ";" +
                "companyNotes" + index + ";\n";
    }
}
