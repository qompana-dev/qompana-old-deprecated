package com.devqube.crmbusinessservice.leadopportunity.lead;


import com.devqube.crmbusinessservice.access.UserClient;
import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.leadopportunity.AccountsService;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadSaveDTO;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static org.flowable.engine.impl.test.AbstractTestCase.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LeadServiceTest {

    @Mock
    private UserClient userClient;
    @Mock
    private LeadRepository leadRepository;
    @Mock
    private AccessService accessService;
    @Mock
    private UserService userService;
    @Mock
    private AccountsService accountsService;

    @InjectMocks
    private LeadService leadService;

    @Test
    public void shouldModifyKeeperIdWhenModifyingLead() throws BadRequestException, EntityNotFoundException, PermissionDeniedException {
        final String loggedAccountEmail = "jan.kowalski@devqube.com";
        final Long leadId = -1L;

        LeadSaveDTO leadSaveDTO = new LeadSaveDTO();
        leadSaveDTO.setLeadKeeperId(-1L);

        Lead lead = new Lead();
        lead.setId(leadId);
        lead.setCreator(-1L);
        lead.setLeadKeeperId(-1L);

        when(accessService.assignAndVerify(any())).thenReturn(leadSaveDTO);
        when(leadRepository.findById(eq(leadId))).thenReturn(Optional.of(lead));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(-1L));
        when(leadRepository.save(any())).thenAnswer(i -> i.getArgument(0));

        LeadSaveDTO result1 = leadService.modifyLead(loggedAccountEmail, leadId, leadSaveDTO);
        assertEquals(-1L, result1.getLeadKeeperId().longValue());

        leadSaveDTO.setLeadKeeperId(-2L);
        LeadSaveDTO result2 = leadService.modifyLead(loggedAccountEmail, leadId, leadSaveDTO);
        assertEquals(-2L, result2.getLeadKeeperId().longValue());


        lead.setLeadKeeperId(-2L);
        leadSaveDTO.setLeadKeeperId(-1L);
        LeadSaveDTO result3 = leadService.modifyLead(loggedAccountEmail, leadId, leadSaveDTO);
        assertEquals(-1L, result3.getLeadKeeperId().longValue());
    }

    @Test(expected = PermissionDeniedException.class)
    public void shouldThrowExceptionWhenUserDoesntHavePermissionToLead() throws BadRequestException, EntityNotFoundException, PermissionDeniedException {
        final String loggedAccountEmail = "jan.kowalski@devqube.com";
        final Long leadId = -1L;

        LeadSaveDTO leadSaveDTO = new LeadSaveDTO();
        leadSaveDTO.setLeadKeeperId(-2L);

        Lead lead = new Lead();
        lead.setId(leadId);
        lead.setCreator(-1L);
        lead.setLeadKeeperId(-3L);

        when(accessService.assignAndVerify(any())).thenReturn(leadSaveDTO);
        when(leadRepository.findById(eq(leadId))).thenReturn(Optional.of(lead));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(-2L));
        leadService.modifyLead(loggedAccountEmail, leadId, leadSaveDTO);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowBadRequestExceptionWhenLeadNotFound() throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        when(leadRepository.findById(1L)).thenReturn(Optional.empty());
        leadService.closeLead("test@test.com", 1L, Lead.CloseResult.CLOSED, null);
    }

    @Test(expected = PermissionDeniedException.class)
    public void shouldThrowPermissionDeniedExceptionWhenUserIsNotKeeperOrCreatorOfLead() throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        Lead lead = new Lead();
        lead.setCreator(-2L);
        lead.setLeadKeeperId(-3L);
        when(leadRepository.findById(1L)).thenReturn(Optional.of(lead));
        leadService.closeLead("test@test.com", 1L, Lead.CloseResult.CLOSED, null);
    }

    @Test
    public void shouldCloseLeadWhenUserIsCreator() throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        Lead lead = new Lead();
        lead.setCreator(-1L);
        lead.setLeadKeeperId(-3L);
        when(leadRepository.findById(1L)).thenReturn(Optional.of(lead));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(-3L));
        leadService.closeLead("test@test.com", 1L, Lead.CloseResult.CLOSED, null);
        verify(leadRepository, times(1)).save(lead);
    }

    @Test
    public void shouldCloseLeadWhenUserIsKeeper() throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        Lead lead = new Lead();
        lead.setCreator(-2L);
        lead.setLeadKeeperId(-1L);
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(-1L));
        when(leadRepository.findById(1L)).thenReturn(Optional.of(lead));
        leadService.closeLead("test@test.com", 1L, Lead.CloseResult.CLOSED, null);
        verify(leadRepository, times(1)).save(lead);
    }
}
