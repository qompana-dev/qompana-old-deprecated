package com.devqube.crmbusinessservice.leadopportunity.bpmn;

import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.PossibleValue;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Process;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Property;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Task;
import com.devqube.crmshared.exception.BadRequestException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.ByteArrayInputStream;
import java.util.*;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class BPMNGeneratorTest {

    private BPMNGenerator bpmnGenerator;
    private Process process;

    @Before
    public void setUp() {
        bpmnGenerator = new BPMNGenerator();
        process = new Process();
        process.setId("id");
        process.setName("name");
        process.setObjectType(Process.ObjectTypeEnum.LEAD);
        process.setTasks(new ArrayList<>(Arrays.asList(
                new Task().name("task1"),
                new Task().name("task2"),
                new Task().name("task3"),
                new Task().name("task4"),
                new Task().name("task5")
        )));
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenProcessWithoutID() throws TransformerException, BadRequestException, ParserConfigurationException {
        process.setId(null);
        bpmnGenerator.generateBPMNFile(process);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenProcessWithoutName() throws TransformerException, BadRequestException, ParserConfigurationException {
        process.setName(null);
        bpmnGenerator.generateBPMNFile(process);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenProcessWithoutTasks() throws TransformerException, BadRequestException, ParserConfigurationException {
        process.setTasks(null);
        bpmnGenerator.generateBPMNFile(process);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenTaskWithoutName() throws TransformerException, BadRequestException, ParserConfigurationException {
        process.getTasks().get(0).setName(null);
        bpmnGenerator.generateBPMNFile(process);
    }

    @Test
    public void shouldGenerateXMLWithProcessElement() throws Exception {
        Document document = convertByteArrayToDocument(bpmnGenerator.generateBPMNFile(process).getByteArray());
        assertEquals(1L, document.getElementsByTagName("process").getLength());
        NamedNodeMap attributes = document.getElementsByTagName("process").item(0).getAttributes();
        assertEquals(3L, attributes.getLength());
        assertEquals(process.getId(), attributes.getNamedItem("id").getNodeValue());
        assertEquals(process.getName(), attributes.getNamedItem("name").getNodeValue());
    }

    @Test
    public void shouldGenerateXMLWithStartEvent() throws Exception {
        Document document = convertByteArrayToDocument(bpmnGenerator.generateBPMNFile(process).getByteArray());
        assertEquals(1L, document.getElementsByTagName("startEvent").getLength());
    }

    @Test
    public void shouldGenerateXMLWithEndEvent() throws Exception {
        Document document = convertByteArrayToDocument(bpmnGenerator.generateBPMNFile(process).getByteArray());
        assertEquals(1L, document.getElementsByTagName("endEvent").getLength());
    }

    @Test
    public void shouldGenerateXMLWithUserTasks() throws Exception {
        Document document = convertByteArrayToDocument(bpmnGenerator.generateBPMNFile(process).getByteArray());
        NodeList tasks = document.getElementsByTagName("userTask");
        assertEquals(6L, tasks.getLength());
        for (int i = 0; i < tasks.getLength() - 1; i++) {
            NamedNodeMap attributes = tasks.item(i).getAttributes();
            assertEquals(process.getTasks().get(i).getName(), attributes.getNamedItem("name").getNodeValue());
        }
        NamedNodeMap attributes = tasks.item(tasks.getLength() - 1).getAttributes();
        assertEquals("", attributes.getNamedItem("name").getNodeValue());
    }

    @Test
    public void shouldGenerateXMLWithSequenceFlows() throws Exception {
        Document document = convertByteArrayToDocument(bpmnGenerator.generateBPMNFile(process).getByteArray());
        NodeList sequenceFlows = document.getElementsByTagName("sequenceFlow");
        assertEquals(7L, sequenceFlows.getLength());
        Set<String> ids = new HashSet<>();
        for (int i = 0; i < sequenceFlows.getLength(); i++) {
            String id = sequenceFlows.item(i).getAttributes().getNamedItem("id").getNodeValue();
            assertFalse(ids.contains(id));
            ids.add(id);
        }
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenStringPropertyHasPossibleValues() throws TransformerException, BadRequestException, ParserConfigurationException {
        process.getTasks().get(0).setProperties(List.of(
                new Property().
                        type(Property.TypeEnum.STRING)
                        .id("id")
                        .possibleValues(List.of(
                                new PossibleValue().id("possibleId1").name("possibleName1"),
                                new PossibleValue().id("possibleId2").name("possibleName2"))
                        )
                )
        );
        bpmnGenerator.generateBPMNFile(process);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenEnumPropertyDontHavePossibleValues() throws TransformerException, BadRequestException, ParserConfigurationException {
        process.getTasks().get(0).setProperties(List.of(
                new Property().type(Property.TypeEnum.ENUM).id("propertyId1")
                )
        );
        bpmnGenerator.generateBPMNFile(process);
    }

    @Test
    public void shouldGenerateXMLWithTasksProperties() throws Exception {
        process.getTasks().get(0).setProperties(List.of(
                new Property().type(Property.TypeEnum.STRING).id("string"),
                new Property().type(Property.TypeEnum.LONG).id("long"),
                new Property().type(Property.TypeEnum.DOUBLE).id("double"),
                new Property().type(Property.TypeEnum.DATE).id("date"),
                new Property().type(Property.TypeEnum.BOOLEAN).id("boolean"),
                new Property().type(Property.TypeEnum.ENUM).id("enum").required(false).possibleValues(
                        List.of(new PossibleValue().id("pv1").name("pn1"),
                                new PossibleValue().id("pv2").name("pn2"),
                                new PossibleValue().id("pv3").name("pn3"))
                ))
        );
        Document document = convertByteArrayToDocument(bpmnGenerator.generateBPMNFile(process).getByteArray());
        assertEquals(1L, document.getElementsByTagName("extensionElements").getLength());
        assertEquals(6L, document.getElementsByTagName("flowable:formProperty").getLength());
        assertEquals(3L, document.getElementsByTagName("flowable:value").getLength());
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhenPropertyNamesNotUnique() throws Exception {
        process.getTasks().get(0).setProperties(List.of(
                new Property().type(Property.TypeEnum.STRING).id("string"),
                new Property().type(Property.TypeEnum.LONG).id("long"),
                new Property().type(Property.TypeEnum.DOUBLE).id("double"),
                new Property().type(Property.TypeEnum.DATE).id("date"),
                new Property().type(Property.TypeEnum.BOOLEAN).id("boolean"),
                new Property().type(Property.TypeEnum.ENUM).id("enum").required(false).possibleValues(
                        List.of(new PossibleValue().id("pv1").name("pn1"),
                                new PossibleValue().id("pv2").name("pn2"),
                                new PossibleValue().id("pv3").name("pn3"))
                ))
        );
        process.getTasks().get(1).setProperties(List.of(new Property().type(Property.TypeEnum.STRING).id("string")));
        bpmnGenerator.generateBPMNFile(process);
    }

    @Test
    public void shouldGenerateXMLWithoutCandidateStarterGroupsWhenAvailableTypeNull() throws Exception {
        process.setAvailableType(null);
        Document document = convertByteArrayToDocument(bpmnGenerator.generateBPMNFile(process).getByteArray());
        NamedNodeMap attributes = document.getElementsByTagName("process").item(0).getAttributes();
        assertNull(attributes.getNamedItem("flowable:candidateStarterGroups"));
    }

    @Test
    public void shouldGenerateXMLWithCandidateStarterGroupsEmptyWhenAvailableTypeAll() throws Exception {
        process.setAvailableType("ALL");
        Document document = convertByteArrayToDocument(bpmnGenerator.generateBPMNFile(process).getByteArray());
        Node node = document.getElementsByTagName("documentation").item(0);
        assertNull(node);
    }

    @Test
    public void shouldGenerateXMLWithCandidateStarterGroupsWhenAvailableTypeDepartment() throws Exception {
        process.setAvailableType("DEPARTMENT");
        process.setDepartment("-3");
        Document document = convertByteArrayToDocument(bpmnGenerator.generateBPMNFile(process).getByteArray());
        Node node = document.getElementsByTagName("documentation").item(0);
        assertEquals("-3", node.getTextContent());
    }

    private Document convertByteArrayToDocument(byte[] xmlAsBytes) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new ByteArrayInputStream(xmlAsBytes));
    }
}
