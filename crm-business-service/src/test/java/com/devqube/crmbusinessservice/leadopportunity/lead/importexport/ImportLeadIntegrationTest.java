package com.devqube.crmbusinessservice.leadopportunity.lead.importexport;

import com.devqube.crmbusinessservice.AbstractIntegrationTest;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmshared.license.LicenseCheck;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.impl.bpmn.deployer.ResourceNameUtil;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.netflix.ribbon.StaticServerList;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ActiveProfiles({"test","wiremock"})
@ContextConfiguration(classes = {ImportLeadIntegrationTest.LocalClientConfiguration.class})
public class ImportLeadIntegrationTest extends AbstractIntegrationTest {
    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private LicenseCheck licenseCheck;

    private static final String PROCESS_DEFINITION_JSON = "{\"name\":\"process\",\"id\":\"processId\",\"objectType\":\"LEAD\",\"tasks\":[{\"name\":\"task0\",\"properties\":[{\"id\":\"prop1Id\",\"type\":\"STRING\",\"required\":false},{\"id\":\"prop2Id\",\"type\":\"ENUM\",\"required\":true,\"possibleValues\":[{\"id\":\"posibbleId\",\"name\":\"Some value\"},{\"id\":\"possibleId2\",\"name\":\"Some value 2\"}]}]},{\"name\":\"task1\"},{\"name\":\"task2\"},{\"name\":\"task3\"},{\"name\":\"task4\"}]}";
    private String processDefinitionId;
    private String deploymentId;

    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(WireMockConfiguration.options().dynamicPort());

    @Before
    public void setUp() throws IOException {
        super.setUp();
        licenseCheck.refresh();
        createBusinessProcessDefinition();

        stubFor(get(urlEqualTo("/accounts/me/id"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("-1")));

        stubFor(get(urlEqualTo("/role/email/jan.kowalski%40devqube.com/with-children/id"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("[-1]")));

        stubFor(get(urlEqualTo("/accounts/emails"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("[" +
                                "{\"id\": -1, \"name\": \"Jan\", \"surname\": \"Kowalski\", \"email\": \"jan.kowalski@devqube.com\"}," +
                                "{\"id\": 1, \"name\": \"Janusz\", \"surname\": \"Polak\", \"email\": \"janusz.polak@devqube.com\"}," +
                                "{\"id\": 2, \"name\": \"Marek\", \"surname\": \"Nowak\", \"email\": \"marek.nowak@devqube.com\"}," +
                                "{\"id\": 3, \"name\": \"Jerzy\", \"surname\": \"Pociask\", \"email\": \"jerzy.pociask@devqube.com\"}" +
                                "]")));
    }

    @Test
    public void shouldImportLead() {
        HttpEntity<MultiValueMap<String, Object>> requestEntity = createImportCsvRequest("src/test/resources/lead-import-export/test_lead_import.csv");
        List<Lead> all = leadRepository.findAll();
        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/leads/import", requestEntity, Void.class);
        List<Lead> afterRequest = leadRepository.findAll();

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        assertEquals(3, afterRequest.size() - all.size());
    }

    @Test
    public void shouldNotImportLeadsWhenCsvIsBroken() {
        HttpEntity<MultiValueMap<String, Object>> requestEntity = createImportCsvRequest("src/test/resources/lead-import-export/test_lead_import_broken.csv");
        List<Lead> all = leadRepository.findAll();
        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/leads/import", requestEntity, Void.class);
        List<Lead> afterRequest = leadRepository.findAll();

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(0, afterRequest.size() - all.size());
    }


    private HttpEntity<MultiValueMap<String, Object>> createImportCsvRequest(String csvFilePath) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Logged-Account-Email", "jan.kowalski@devqube.com");
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(new File(csvFilePath)));
        return new HttpEntity<>(body, headers);
    }


    private void createBusinessProcessDefinition() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(PROCESS_DEFINITION_JSON, headers);
        ResponseEntity<Resource> resourceResponseEntity = restTemplate.postForEntity(baseUrl + "/bpmn/generate",
                entity, Resource.class);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        deploymentId = this.repositoryService.createDeployment().addString("xmlString." + ResourceNameUtil.BPMN_RESOURCE_SUFFIXES[0],
                new String(Objects.requireNonNull(resourceResponseEntity.getBody()).getInputStream().readAllBytes())).deploy().getId();
        processDefinitionId = this.repositoryService
                .createProcessDefinitionQuery()
                .deploymentId(deploymentId)
                .singleResult()
                .getId();
    }

    @TestConfiguration
    public static class LocalClientConfiguration {
        @Bean
        public ServerList<Server> serverList() {
            return new StaticServerList<>(new Server("localhost", wireMockRule.port()));
        }
    }
}
