package com.devqube.crmbusinessservice.leadopportunity.bpmn;


import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.PossibleValue;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Process;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Property;
import com.devqube.crmbusinessservice.leadopportunity.bpmn.model.Task;
import com.devqube.crmshared.exception.BadRequestException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class BPMNParserTest {
    private BPMNParser bpmnParser = new BPMNParser();

    private String getTestXml(String testXml) throws IOException {
        return Files.readString(Paths.get("src/test/resources/bpmnparser/", testXml));
    }

    @Test
    public void shouldParse() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldParse.xml");
        Process process = this.bpmnParser.parseBPMNFile(xml);
        List<Property> properties;

        assertNotNull(process);
        assertEquals("processd9341812-d88e-41d2-82e7-4a7327a7fa02", process.getId());
        assertEquals("processName", process.getName());
        assertEquals("#f00", process.getBgColor());
        assertEquals("#fff", process.getTextColor());


        List<Task> tasks = process.getTasks();
        assertNotNull(tasks);
        assertEquals(3, tasks.size());

        //task 1
        assertEquals("Etap", tasks.get(0).getName());
        assertNull(tasks.get(0).getDocumentation());
        assertEquals("-2", tasks.get(0).getAcceptanceUser());
        assertNull(tasks.get(0).getAcceptanceGroup());

        properties = tasks.get(0).getProperties();
        assertNotNull(properties);
        assertEquals(4, properties.size());

        assertEquals("a1", properties.get(0).getId());
        assertEquals(Property.TypeEnum.STRING, properties.get(0).getType());
        assertEquals(false, properties.get(0).getRequired());
        assertNull(properties.get(0).getPossibleValues());

        assertEquals("a2", properties.get(1).getId());
        assertEquals(Property.TypeEnum.DOUBLE, properties.get(1).getType());
        assertEquals(true, properties.get(1).getRequired());
        assertNull(properties.get(1).getPossibleValues());

        assertEquals("a4", properties.get(2).getId());
        assertEquals(Property.TypeEnum.ENUM, properties.get(2).getType());
        assertEquals(false, properties.get(2).getRequired());

        List<PossibleValue> possibleValues = properties.get(2).getPossibleValues();
        assertNotNull(possibleValues);
        assertEquals(3, possibleValues.size());

        assertEquals("1ef4167a-9846-4a72-88a3-d5edf8a9a120", possibleValues.get(0).getId());
        assertEquals("one", possibleValues.get(0).getName());
        assertEquals("91861e02-1f71-4621-8160-c2e5dc610cfd", possibleValues.get(1).getId());
        assertEquals("two", possibleValues.get(1).getName());
        assertEquals("befc8699-f642-41c5-9dfa-2c9aaa6af0ee", possibleValues.get(2).getId());
        assertEquals("three", possibleValues.get(2).getName());

        assertEquals("a5", properties.get(3).getId());
        assertEquals(Property.TypeEnum.BOOLEAN, properties.get(3).getType());
        assertEquals(false, properties.get(3).getRequired());
        assertNull(properties.get(3).getPossibleValues());

        //task 2
        assertEquals("Etap1", tasks.get(1).getName());
        assertEquals("test2", tasks.get(1).getDocumentation());
        assertEquals("-1", tasks.get(1).getAcceptanceUser());
        assertNull(tasks.get(1).getAcceptanceGroup());

        assertEquals("-2", tasks.get(1).getNotificationUser());
        assertNull(tasks.get(1).getNotificationGroup());

        properties = tasks.get(1).getProperties();
        assertNotNull(properties);
        assertEquals(1, properties.size());
        assertEquals("user", properties.get(0).getId());
        assertEquals(Property.TypeEnum.STRING, properties.get(0).getType());
        assertEquals(true, properties.get(0).getRequired());
        assertNull(properties.get(0).getPossibleValues());

        //task 3
        assertEquals("Etap2", tasks.get(2).getName());
        assertNull(tasks.get(2).getDocumentation());
        assertNull(tasks.get(2).getAcceptanceUser());
        assertNull(tasks.get(2).getNotificationUser());
        assertEquals("-5", tasks.get(2).getAcceptanceGroup());
        assertEquals("-3", tasks.get(2).getNotificationGroup());

        properties = tasks.get(2).getProperties();
        assertNotNull(properties);
        assertEquals(1, properties.size());
        assertEquals("asdf", properties.get(0).getId());
        assertEquals(Property.TypeEnum.STRING, properties.get(0).getType());
        assertEquals(false, properties.get(0).getRequired());
        assertNull(properties.get(0).getPossibleValues());
    }

    @Test
    public void shouldParseWithoutProcessDocumentation() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldParseWithoutProcessDocumentation.xml");
        Process process = this.bpmnParser.parseBPMNFile(xml);
        assertNotNull(process);
        assertNull(process.getDocumentation());
    }

    @Test
    public void shouldParseWithoutTasks() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldParseWithoutTasks.xml");
        Process process = this.bpmnParser.parseBPMNFile(xml);
        assertNotNull(process);
        assertEquals(0, process.getTasks().size());
    }

    @Test
    public void shouldParseWithEmptyTask() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldParseWithEmptyTask.xml");
        Process process = this.bpmnParser.parseBPMNFile(xml);
        assertNotNull(process);
        assertEquals(1, process.getTasks().size());
        assertNotNull(process.getTasks().get(0).getProperties());
        assertEquals(0, process.getTasks().get(0).getProperties().size());
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhereProcessIdIsNull() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldThrowExceptionWhereProcessIdIsNull.xml");
        this.bpmnParser.parseBPMNFile(xml);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhereProcessNameIsNull() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldThrowExceptionWhereProcessNameIsNull.xml");
        this.bpmnParser.parseBPMNFile(xml);
    }

    @Test
    public void shouldParseWithoutTaskCandidateUser() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldParseWithoutTaskCandidateUser.xml");
        Process process = this.bpmnParser.parseBPMNFile(xml);
        assertNotNull(process);
        assertEquals(1, process.getTasks().size());
        assertNull(process.getTasks().get(0).getAcceptanceUser());
    }

    @Test
    public void shouldParseWithoutTaskCandidateGroups() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldParseWithoutTaskCandidateGroups.xml");
        Process process = this.bpmnParser.parseBPMNFile(xml);
        assertNotNull(process);
        assertEquals(1, process.getTasks().size());
        assertNull(process.getTasks().get(0).getAcceptanceGroup());
    }

    @Test
    public void shouldParseWithoutTaskDocumentation() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldParseWithoutTaskDocumentation.xml");
        Process process = this.bpmnParser.parseBPMNFile(xml);
        assertNotNull(process);
        assertEquals(1, process.getTasks().size());
        assertNull(process.getTasks().get(0).getDocumentation());
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWhereTaskNameIsNull() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldThrowExceptionWhereTaskNameIsNull.xml");
        this.bpmnParser.parseBPMNFile(xml);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWherePropertyIdIsNull() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldThrowExceptionWherePropertyIdIsNull.xml");
        this.bpmnParser.parseBPMNFile(xml);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWherePropertyTypeIsNull() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldThrowExceptionWherePropertyTypeIsNull.xml");
        this.bpmnParser.parseBPMNFile(xml);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWherePropertyRequiredIsNull() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldThrowExceptionWherePropertyRequiredIsNull.xml");
        this.bpmnParser.parseBPMNFile(xml);
    }


    @Test
    public void shouldParseWithoutPossibleValuesWhereTypeOtherThanEnum() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldParseWithoutPossibleValuesWhereTypeOtherThanEnum.xml");
        Process process = this.bpmnParser.parseBPMNFile(xml);
        assertNotNull(process);
        assertEquals(1, process.getTasks().size());
        assertNotNull(process.getTasks().get(0).getProperties());
        assertEquals(1, process.getTasks().get(0).getProperties().size());
        Property property = process.getTasks().get(0).getProperties().get(0);
        assertEquals(Property.TypeEnum.STRING, property.getType());
        assertNull(property.getPossibleValues());
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWherePossibleValuesIsEmptyWhereTypeIsEnum() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldThrowExceptionWherePossibleValuesIsEmptyWhereTypeIsEnum.xml");
        this.bpmnParser.parseBPMNFile(xml);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWherePossibleValueIdIsNull() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldThrowExceptionWherePossibleValueIdIsNull.xml");
        this.bpmnParser.parseBPMNFile(xml);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionWherePossibleValueNameIsNull() throws IOException, BadRequestException {
        String xml = this.getTestXml("shouldThrowExceptionWherePossibleValueNameIsNull.xml");
        this.bpmnParser.parseBPMNFile(xml);
    }
}
