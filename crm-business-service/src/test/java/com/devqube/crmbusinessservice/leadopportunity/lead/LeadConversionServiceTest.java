package com.devqube.crmbusinessservice.leadopportunity.lead;

import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.customer.contact.ContactCustomerRelationService;
import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.leadopportunity.AccountsService;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.ConvertedOpportunityDto;
import com.devqube.crmbusinessservice.leadopportunity.lead.web.dto.LeadConversionDto;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineHelperService;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import org.flowable.engine.runtime.ProcessInstance;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LeadConversionServiceTest {

    @Mock
    private LeadRepository leadRepository;
    @Mock
    private ProcessEngineHelperService processEngineHelperService;
    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private ContactRepository contactRepository;
    @Mock
    private UserService userService;
    @Mock
    private OpportunityRepository opportunityRepository;
    @Mock
    private AssociationClient associationClient;
    @Mock
    private AccessService accessService;
    @Mock
    private ContactCustomerRelationService contactCustomerRelationService;
    @Mock
    private AccountsService accountsService;

    @InjectMocks
    private LeadConversionService leadConversionService;

    private String email = "jan.kowalski@devqube.com";
    private Lead lead;
    private LeadConversionDto leadConversionDto;
    private Customer customer;
    private Contact contact;
    private ConvertedOpportunityDto expected;
    private Opportunity opportunity;

    @Before
    public void setUp() {
        lead = LeadTestUtil.createLead();
        leadConversionDto = LeadTestUtil.createLeadConversionDto();
        customer = LeadTestUtil.createCustomer();
        customer.setId(999L);
        contact = LeadTestUtil.createContact(customer);
        contact.setId(888L);
        opportunity = LeadTestUtil.createOpportunity(customer, contact);
        opportunity.setId(777L);
        expected = LeadTestUtil.createExpectedResult();
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowExceptionIfLeadAlreadyConvertedIntoOpportunity() throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        lead.setConvertedToOpportunity(true);
        when(leadRepository.findById(1L)).thenReturn(Optional.ofNullable(lead));
        leadConversionService.convertToOpportunity(email, 1L, new LeadConversionDto());
    }

    @Test(expected = PermissionDeniedException.class)
    public void shouldThrowExceptionWhenUserNotPermittedToMakeConversion() throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        when(leadRepository.findById(1L)).thenReturn(Optional.ofNullable(lead));
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(100L));
        leadConversionService.convertToOpportunity(email, 1L, new LeadConversionDto());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenLeadNotFound() throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        leadConversionService.convertToOpportunity(email, 87687L, new LeadConversionDto());
    }

    @Test()
    public void shouldCreateNewCustomerNewContactAndNewOpportunity() throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        when(leadRepository.findById(1L)).thenReturn(Optional.ofNullable(lead));
        when(userService.getUserId(email)).thenReturn(22L);
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(11L, 22L));
        ProcessInstance mock = mock(ProcessInstance.class);
        when(mock.getProcessInstanceId()).thenReturn("processInstanceId");
        when(processEngineHelperService.startProcessInstance(any())).thenReturn(mock);
        when(contactRepository.save(any())).thenReturn(contact);
        when(customerRepository.save(any())).thenReturn(customer);
        when(opportunityRepository.save(any())).thenReturn(opportunity);
        when(accessService.hasAccess(any(), any(), anyBoolean())).thenReturn(true);

        ConvertedOpportunityDto opportunityDto = leadConversionService.convertToOpportunity(email, 1L, leadConversionDto);

        assertThat(opportunityDto).isEqualTo(expected);
    }

    @Test()
    public void shouldCreateNewCustomerNewContactButNotOpportunity() throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        when(leadRepository.findById(1L)).thenReturn(Optional.ofNullable(lead));
        when(userService.getUserId(email)).thenReturn(22L);
        when(contactRepository.save(any())).thenReturn(contact);
        when(customerRepository.save(any())).thenReturn(customer);
        when(accessService.hasAccess(any(), any(), anyBoolean())).thenReturn(true);
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(11L, 22L));

        leadConversionDto.setNoOpportunity(true);
        ConvertedOpportunityDto opportunityDto = leadConversionService.convertToOpportunity(email, 1L, leadConversionDto);
        verify(opportunityRepository, times(0)).save(any());
        nullOpportunityPartInExpected();
        assertThat(opportunityDto).isEqualTo(expected);
    }

    @Test()
    public void shouldCreateNewOpportunityButCustomerAndContactExist() throws EntityNotFoundException, PermissionDeniedException, BadRequestException {
        when(leadRepository.findById(1L)).thenReturn(Optional.ofNullable(lead));
        when(userService.getUserId(email)).thenReturn(22L);
        leadConversionDto.setIsExistingContact(true);
        ProcessInstance mock = mock(ProcessInstance.class);
        when(mock.getProcessInstanceId()).thenReturn("processInstanceId");
        when(processEngineHelperService.startProcessInstance(any())).thenReturn(mock);
        when(contactRepository.findById(99L)).thenReturn(Optional.of(contact));
        when(customerRepository.findById(88L)).thenReturn(Optional.of(customer));
        when(opportunityRepository.save(any())).thenReturn(opportunity);
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(List.of(11L, 22L));
        leadConversionDto.setIsExistingCustomer(true);

        ConvertedOpportunityDto opportunityDto = leadConversionService.convertToOpportunity(email, 1L, leadConversionDto);

        assertThat(opportunityDto).isEqualTo(expected);
    }

    private void nullOpportunityPartInExpected() {
        expected.setOpportunityId(null);
        expected.setOpportunityName(null);
        expected.setOpportunityProcessName(null);
        expected.setOpportunitySubjectOfInterest(null);
        expected.setOpportunityAmount(null);
        expected.setOpportunityCurrency(null);
        expected.setOpportunityOwner(null);
    }
}
