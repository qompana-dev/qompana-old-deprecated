package com.devqube.crmbusinessservice.leadopportunity.lead.importexport;

import org.flowable.engine.runtime.ProcessInstance;

import java.util.Date;
import java.util.Map;

class ProcessInstanceImplForTests implements ProcessInstance {
    private String processInstanceId;
    private String processDefinitionId;
    private String processDefinitionName;

    public ProcessInstanceImplForTests(String processInstanceId, String processDefinitionId, String processDefinitionName) {
        this.processInstanceId = processInstanceId;
        this.processDefinitionId = processDefinitionId;
        this.processDefinitionName = processDefinitionName;
    }

    @Override
    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    @Override
    public String getProcessDefinitionName() {
        return processDefinitionName;
    }

    @Override
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    // @formatter:off
    public String getProcessDefinitionKey() { return null; }
    public Integer getProcessDefinitionVersion() { return 1; }
    public String getDeploymentId() { return null; }
    public String getBusinessKey() { return null; }
    public String getId() { return processInstanceId; }
    public boolean isSuspended() { return false; }
    public boolean isEnded() { return false; }
    public String getActivityId() { return null; }
    public String getParentId() { return null; }
    public String getSuperExecutionId() { return null; }
    public String getRootProcessInstanceId() { return null; }
    public Map<String, Object> getProcessVariables() { return null; }
    public String getTenantId() { return null; }
    public String getName() { return null; }
    public String getDescription() { return null; }
    public String getLocalizedName() { return null; }
    public String getLocalizedDescription() { return null; }
    public Date getStartTime() { return null; }
    public String getStartUserId() { return null; }
    public String getCallbackId() { return null; }
    public String getCallbackType() { return null; }
    // @formatter:on
}
