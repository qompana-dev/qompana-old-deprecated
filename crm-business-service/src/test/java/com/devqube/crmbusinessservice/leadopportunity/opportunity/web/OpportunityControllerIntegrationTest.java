package com.devqube.crmbusinessservice.leadopportunity.opportunity.web;

import com.devqube.crmbusinessservice.AbstractIntegrationTest;
import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.contact.model.Contact;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.customer.customer.model.Customer;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.web.dto.OpportunitySaveDTO;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.impl.bpmn.deployer.ResourceNameUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.netflix.ribbon.StaticServerList;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ActiveProfiles({"test","wiremock"})
@ContextConfiguration(classes = {OpportunityControllerIntegrationTest.LocalClientConfiguration.class})
public class OpportunityControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private OpportunityRepository opportunityRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ContactRepository contactRepository;

    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(WireMockConfiguration.options().dynamicPort());

    private HttpHeaders headers;
    private String processDefinitionId;
    private String deploymentId;
    private OpportunitySaveDTO opportunitySaveDTO;
    private Customer customer;
    private Contact contact;
    private static final String PROCESS_DEFINITION_JSON = "{\"name\":\"process\",\"id\":\"processId\",\"objectType\":\"LEAD\",\"tasks\":[{\"name\":\"task0\",\"properties\":[{\"id\":\"prop1Id\",\"type\":\"STRING\",\"required\":false},{\"id\":\"prop2Id\",\"type\":\"ENUM\",\"required\":true,\"possibleValues\":[{\"id\":\"posibbleId\",\"name\":\"Some value\"},{\"id\":\"possibleId2\",\"name\":\"Some value 2\"}]}]},{\"name\":\"task1\"},{\"name\":\"task2\"},{\"name\":\"task3\"},{\"name\":\"task4\"}]}";

    @Before
    public void setUp() throws IOException {
        super.setUp();
        createBusinessProcessDefinition();
        createHeadersAndMocks();
        customer = createCustomer("customer1");
        contact = createContact("contact1");
        opportunitySaveDTO = createOpportunitySaveDTO();
    }

    @After
    public void cleanUp() {
        repositoryService.deleteDeployment(deploymentId, true);
        opportunityRepository.deleteAll();
        customerRepository.deleteAll();
        contactRepository.deleteAll();
    }

    @Test
    public void shouldNotAddNewOpportunityWithoutCustomerAndContact() {
        opportunitySaveDTO.setContactId(null);
        opportunitySaveDTO.setCustomerId(null);
        ResponseEntity<OpportunitySaveDTO> response = restTemplate.postForEntity(baseUrl + "/opportunity",
                new HttpEntity<>(opportunitySaveDTO, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldReturn404WhenCustomerNotExists() {
        opportunitySaveDTO.setCustomerId(-99L);
        ResponseEntity<OpportunitySaveDTO> response = restTemplate.postForEntity(baseUrl + "/opportunity",
                new HttpEntity<>(opportunitySaveDTO, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldReturn404WhenContactNotExists() {
        opportunitySaveDTO.setContactId(-99L);
        ResponseEntity<OpportunitySaveDTO> response = restTemplate.postForEntity(baseUrl + "/opportunity",
                new HttpEntity<>(opportunitySaveDTO, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldNotAddOpportunityWhenSumIsNot100() {
        opportunitySaveDTO.setOwnerOnePercentage(99L);
        opportunitySaveDTO.setOwnerTwoPercentage(3L);
        ResponseEntity<OpportunitySaveDTO> response = restTemplate.postForEntity(baseUrl + "/opportunity",
                new HttpEntity<>(opportunitySaveDTO, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldAddNewOpportunityWhenEverythingOK() {
        ResponseEntity<OpportunitySaveDTO> response = restTemplate.postForEntity(baseUrl + "/opportunity",
                new HttpEntity<>(opportunitySaveDTO, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(opportunitySaveDTO.getCustomerId(), response.getBody().getCustomerId());
        assertEquals(opportunitySaveDTO.getContactId(), response.getBody().getContactId());
        assertEquals(processDefinitionId, runtimeService.createProcessInstanceQuery().singleResult().getProcessDefinitionId());
    }

    @Test
    public void shouldNotModifyOpportunityWhenItDoesntExist() {
        ResponseEntity<OpportunitySaveDTO> addingResponse = restTemplate.exchange(baseUrl + "/opportunity/-99", HttpMethod.PUT,
                new HttpEntity<>(opportunitySaveDTO, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.NOT_FOUND, addingResponse.getStatusCode());
    }

    @Test
    public void shouldNotModifyOpportunityWhenCustomerNotFound() {
        ResponseEntity<OpportunitySaveDTO> addingResponse = restTemplate.postForEntity(baseUrl + "/opportunity",
                new HttpEntity<>(opportunitySaveDTO, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.CREATED, addingResponse.getStatusCode());
        assertNotNull(addingResponse.getBody());

        OpportunitySaveDTO toUpdate = addingResponse.getBody();
        toUpdate.setProcessDefinitionId(processDefinitionId);
        toUpdate.setCustomerId(-99L);
        ResponseEntity<OpportunitySaveDTO> response = restTemplate.exchange(baseUrl + "/opportunity/" + addingResponse.getBody().getId(), HttpMethod.PUT,
                new HttpEntity<>(toUpdate, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldNotModifyOpportunityWhenContactDoesntExist() {
        ResponseEntity<OpportunitySaveDTO> addingResponse = restTemplate.postForEntity(baseUrl + "/opportunity",
                new HttpEntity<>(opportunitySaveDTO, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.CREATED, addingResponse.getStatusCode());
        assertNotNull(addingResponse.getBody());

        OpportunitySaveDTO toUpdate = addingResponse.getBody();
        toUpdate.setProcessDefinitionId(processDefinitionId);
        toUpdate.setContactId(-99L);
        ResponseEntity<OpportunitySaveDTO> response = restTemplate.exchange(baseUrl + "/opportunity/" + addingResponse.getBody().getId(), HttpMethod.PUT,
                new HttpEntity<>(toUpdate, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldModifyOpportunityWhenEverythingOK() {
        ResponseEntity<OpportunitySaveDTO> addingResponse = restTemplate.postForEntity(baseUrl + "/opportunity",
                new HttpEntity<>(opportunitySaveDTO, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.CREATED, addingResponse.getStatusCode());
        assertNotNull(addingResponse.getBody());

        customer = createCustomer("customer2");
        contact = createContact("contact2");

        OpportunitySaveDTO toUpdate = addingResponse.getBody();
        toUpdate.setProcessDefinitionId(processDefinitionId);
        toUpdate.setName("new name");
        toUpdate.setCustomerId(customer.getId());
        toUpdate.setContactId(contact.getId());
        ResponseEntity<OpportunitySaveDTO> response = restTemplate.exchange(baseUrl + "/opportunity/" + addingResponse.getBody().getId(), HttpMethod.PUT,
                new HttpEntity<>(toUpdate, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("new name", response.getBody().getName());
        assertEquals(customer.getId(), response.getBody().getCustomerId());
        assertEquals(contact.getId(), response.getBody().getContactId());
    }

    @Test
    public void shouldNotGetOpportunityForEditWhenItDoesntExist() {
        ResponseEntity<OpportunitySaveDTO> getForEditResponse = restTemplate.getForEntity(baseUrl + "/opportunity/for-edit/-99",
                OpportunitySaveDTO.class);
        assertEquals(HttpStatus.NOT_FOUND, getForEditResponse.getStatusCode());
        assertNull(getForEditResponse.getBody());
    }

    @Test
    public void shouldGetOpportunityForEditWhenEverythingIsOK() {
        ResponseEntity<OpportunitySaveDTO> addingResponse = restTemplate.postForEntity(baseUrl + "/opportunity",
                new HttpEntity<>(opportunitySaveDTO, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.CREATED, addingResponse.getStatusCode());
        assertNotNull(addingResponse.getBody());
        Long opportunityId = addingResponse.getBody().getId();


        ResponseEntity<OpportunitySaveDTO> getForEditResponse = restTemplate.exchange(baseUrl + "/opportunity/for-edit/" +
                opportunityId, HttpMethod.GET, new HttpEntity<>(headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.OK, getForEditResponse.getStatusCode());
        assertNotNull(getForEditResponse.getBody());
        assertEquals(opportunitySaveDTO.getName(), getForEditResponse.getBody().getName());
        assertEquals(opportunitySaveDTO.getCustomerId(), getForEditResponse.getBody().getCustomerId());
        assertEquals(opportunitySaveDTO.getContactId(), getForEditResponse.getBody().getContactId());
    }

    @Test
    public void shouldNotDeleteOpportunityWhenItDoesntExist() {
        ResponseEntity<Void> getForEditResponse = restTemplate.exchange(baseUrl + "/opportunity/-99",
                HttpMethod.DELETE, null, Void.class);
        assertEquals(HttpStatus.NOT_FOUND, getForEditResponse.getStatusCode());
        assertNull(getForEditResponse.getBody());
    }

    @Test
    public void shouldDeleteOpportunityWhenEverythingIsOK() {
        ResponseEntity<OpportunitySaveDTO> addingResponse = restTemplate.postForEntity(baseUrl + "/opportunity",
                new HttpEntity<>(opportunitySaveDTO, headers), OpportunitySaveDTO.class);
        assertEquals(HttpStatus.CREATED, addingResponse.getStatusCode());
        assertNotNull(addingResponse.getBody());
        Long opportunityId = addingResponse.getBody().getId();

        ResponseEntity<Void> getForEditResponse = restTemplate.exchange(baseUrl + "/opportunity/" +
                opportunityId, HttpMethod.DELETE, new HttpEntity<>(headers), Void.class);
        assertEquals(HttpStatus.NO_CONTENT, getForEditResponse.getStatusCode());
        assertNull(getForEditResponse.getBody());
        assertTrue(opportunityRepository.findAll().isEmpty());
    }

    private void createHeadersAndMocks() {
        headers = new HttpHeaders();
        headers.set("Logged-Account-Email", "jan.kowalski@devqube.com");
        headers.set("Auth-controller-type", "detailsCard");
        stubFor(get(urlEqualTo("/accounts/me/id"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("-1")));

        stubFor(get(urlEqualTo("/internal/accounts/jan.kowalski%40devqube.com/role"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("ceo")));

        stubFor(get(urlEqualTo("/role/name/ceo/child/accounts/id"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("[-1,2,3]")));
    }

    private Contact createContact(String name) {
        Contact contact = new Contact();
        contact.setName(name);
        contact.setSurname(name);
        contact.setCustomer(customer);
        contact.setCustomerLinks(new ArrayList<>());
        return contactRepository.save(contact);
    }

    private Customer createCustomer(String name) {
        Customer customer = new Customer();
        customer.setName(name);
        customer.setNip(name);
        return customerRepository.save(customer);
    }

    private OpportunitySaveDTO createOpportunitySaveDTO() {
        OpportunitySaveDTO opportunitySaveDTO = new OpportunitySaveDTO();
        opportunitySaveDTO.setName("opportunity");
        opportunitySaveDTO.setCustomerId(customer.getId());
        opportunitySaveDTO.setCurrency("PLN");
        opportunitySaveDTO.setContactId(contact.getId());
        opportunitySaveDTO.setProcessDefinitionId(processDefinitionId);
        opportunitySaveDTO.setSourceOfAcquisition(1L);
        opportunitySaveDTO.setSubSourceOfAcquisition(2L);
        opportunitySaveDTO.setOwnerOneId(-1L);
        opportunitySaveDTO.setOwnerOnePercentage(100L);
        return opportunitySaveDTO;
    }

    private void createBusinessProcessDefinition() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(PROCESS_DEFINITION_JSON, headers);
        ResponseEntity<Resource> resourceResponseEntity = restTemplate.postForEntity(baseUrl + "/bpmn/generate",
                entity, Resource.class);
        deploymentId = this.repositoryService.createDeployment().addString("xmlString." + ResourceNameUtil.BPMN_RESOURCE_SUFFIXES[0],
                new String(resourceResponseEntity.getBody().getInputStream().readAllBytes())).deploy().getId();
        processDefinitionId = this.repositoryService
                .createProcessDefinitionQuery()
                .deploymentId(deploymentId)
                .singleResult()
                .getId();
    }

    @TestConfiguration
    public static class LocalClientConfiguration {
        @Bean
        public ServerList<Server> serverList() {
            return new StaticServerList<>(new Server("localhost", wireMockRule.port()));
        }
    }
}
