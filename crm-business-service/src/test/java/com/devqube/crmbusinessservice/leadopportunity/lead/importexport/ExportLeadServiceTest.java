package com.devqube.crmbusinessservice.leadopportunity.lead.importexport;

import com.devqube.crmbusinessservice.CrmBusinessServiceApplication;
import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.leadopportunity.AccountsService;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.company.Company;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead;
import com.devqube.crmbusinessservice.leadopportunity.lead.model.LeadPriority;
import com.devqube.crmbusinessservice.leadopportunity.processEngine.service.ProcessEngineHelperService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.user.dto.AccountEmail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ExportLeadServiceTest {
    @Mock
    private LeadRepository leadRepository;

    @Mock
    private AccountsService accountsService;

    @Mock
    private UserService userService;

    @Mock
    private ProcessEngineHelperService processEngineHelperService;

    @InjectMocks
    private ExportLeadService exportLeadService;

    @Test
    public void shouldExportLeads() throws BadRequestException, IOException {
        ReflectionTestUtils.setField(exportLeadService, "strictModelMapper", CrmBusinessServiceApplication.getStrictModelMapper());
        List<Long> userIds = Arrays.asList(-1L, 1L);
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(userIds);
        when(leadRepository.getLeadsByDeletedFalse(eq(userIds))).thenReturn(getLeads());
        when(userService.getAccountEmails()).thenReturn(getAccountEmails());

        when(processEngineHelperService.getProcessInstance(eq("processInstanceId1")))
                .thenReturn(new ProcessInstanceImplForTests("processInstanceId1", "processInstanceId1", "Process definition name 1"));
        when(processEngineHelperService.getProcessInstance(eq("processInstanceId2")))
                .thenReturn(new ProcessInstanceImplForTests("processInstanceId2", "processInstanceId2", "Process definition name 2"));
        when(processEngineHelperService.getProcessInstance(eq("processInstanceId3")))
                .thenReturn(new ProcessInstanceImplForTests("processInstanceId3", "processInstanceId3", "Process definition name 3"));

        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);

        exportLeadService.exportLeads(response, "jan.kowalski@devqube.com");

        String csvFile = stringWriter.getBuffer().toString();
        String expectedCsv = getFileContent("test_lead_export.csv");

        assertEquals(expectedCsv, csvFile);
    }

    @Test
    public void shouldExportLeadsWithNullValues() throws BadRequestException, IOException {
        ReflectionTestUtils.setField(exportLeadService, "strictModelMapper", CrmBusinessServiceApplication.getStrictModelMapper());
        List<Long> userIds = Arrays.asList(-1L, 1L);
        when(accountsService.getAllEmployeesBySupervisorEmail(anyString())).thenReturn(userIds);

        List<Lead> leads = new ArrayList<>();
        leads.add(getLead(1L));
        leads.get(0).setFirstName(null);
        leads.get(0).setLastName(null);
        when(leadRepository.getLeadsByDeletedFalse(eq(userIds))).thenReturn(leads);

        when(userService.getAccountEmails()).thenReturn(getAccountEmails());

        when(processEngineHelperService.getProcessInstance(eq("processInstanceId1")))
                .thenReturn(new ProcessInstanceImplForTests("processInstanceId1", "processInstanceId1", "Process definition name 1"));

        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);

        exportLeadService.exportLeads(response, "jan.kowalski@devqube.com");

        String csvFile = stringWriter.getBuffer().toString();
        String expectedCsv = getFileContent("test_lead_export_with_null_values.csv");

        assertEquals(expectedCsv, csvFile);
    }

    private String getFileContent(String csvFilePath) throws IOException {
        Path path = Paths.get("src","test","resources","lead-import-export", csvFilePath);
        return Files.readString(path);
    }

    private List<Lead> getLeads() {
        List<Lead> leads = new ArrayList<>();
        leads.add(getLead(1L));
        leads.add(getLead(2L));
        leads.add(getLead(3L));
        return leads;
    }

    private Lead getLead(Long index) {
        Lead lead = new Lead();
        lead.setProcessInstanceId("processInstanceId" + index);
        lead.setPriority(LeadPriority.HIGH);
        lead.setFirstName("firstName" + index);
        lead.setLastName("lastName" + index);
        //lead.setPosition("position" + index);
        lead.setEmail("email@email.com" + index);
        lead.setPhone("123123123" + index);
        lead.setSourceOfAcquisition(index);
        lead.setLeadKeeperId(-1L);

        Company company = new Company();
        company.setCompanyName("companyName" + index);
        company.setWwwAddress("www.address.pl" + index);
        company.setStreetAndNumber("street" + index);
        company.setCity("city" + index);
        company.setPostalCode("39-10" + index);
        company.setProvince("province" + index);
        company.setCountry("country" + index);
        lead.setCompany(company);
        return lead;
    }


    private List<AccountEmail> getAccountEmails() {
        List<AccountEmail> result = new ArrayList<>();
        result.add(new AccountEmail(-1, "jan.kowalski@devqube.com", "Jan", "Kowalski"));
        result.add(new AccountEmail(1, "jjan.kowalski@devqube.com", "Jan", "Kowalski"));
        result.add(new AccountEmail(2, "marek.nowak@devqube.com", "Marek", "Nowak"));
        result.add(new AccountEmail(3, "janusz.polak@devqube.com", "Janusz", "Polak"));
        return result;
    }
}

