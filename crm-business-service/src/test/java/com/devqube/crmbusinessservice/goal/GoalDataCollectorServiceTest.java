package com.devqube.crmbusinessservice.goal;

import com.devqube.crmbusinessservice.customer.contact.ContactRepository;
import com.devqube.crmbusinessservice.customer.customer.CustomerRepository;
import com.devqube.crmbusinessservice.goal.model.*;
import com.devqube.crmbusinessservice.goal.web.dto.GoalIntervalDto;
import com.devqube.crmbusinessservice.goal.web.dto.IntervalRangeDto;
import com.devqube.crmbusinessservice.goal.web.dto.UserGoalDto;
import com.devqube.crmbusinessservice.leadopportunity.lead.LeadRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.OpportunityRepository;
import com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GoalDataCollectorServiceTest {
    private static final double DELTA = 1e-15;

    @InjectMocks
    private GoalDataCollectorService goalDataCollectorService;
    @Mock
    private LeadRepository leadRepository;

    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private ContactRepository contactRepository;
    @Mock
    private TaskClient taskClient;
    @Mock
    private OpportunityRepository opportunityRepository;

    @Spy
    private ModelMapper modelMapper = new ModelMapper();

    private Goal goal;


    @Before
    public void setUp() throws Exception {
        goal = getGoal();
    }



    @Test
    public void shouldReturnUserGoalsForRoot() {
        when(leadRepository.findCreatedLeadsForGoal(any(), any(), anySet())).thenReturn(3L);
        Set<UserGoalDto> userGoals = goalDataCollectorService.getUserGoals(goal, 1L);
        assertNotNull(userGoals);

        assertEquals(3, userGoals.size());
        assertTrue(userGoals.stream().allMatch(c -> Arrays.asList(1L, 2L, 3L).contains(c.getUserId())));
        assertTrue(userGoals.stream().allMatch(c -> c.getIntervals().size() == 2));

        for (UserGoalDto ugDto : userGoals) {
            Optional<GoalIntervalDto> index0 = ugDto.getIntervals().stream().filter(c -> c.getIndex().equals(0L)).findFirst();
            Optional<GoalIntervalDto> index1 = ugDto.getIntervals().stream().filter(c -> c.getIndex().equals(1L)).findFirst();

            assertTrue(index0.isPresent());
            assertTrue(index1.isPresent());

            assertEquals(1.0, index0.get().getExpectedValue(), DELTA);
            assertEquals(3.0, index0.get().getCurrentValue(), DELTA);
            assertEquals(2.0, index0.get().getResult(), DELTA);

            assertEquals(2.0, index1.get().getExpectedValue(), DELTA);
            assertEquals(3.0, index1.get().getCurrentValue(), DELTA);
            assertEquals(1.0, index1.get().getResult(), DELTA);
        }
    }

    @Test
    public void shouldReturnUserGoalsForChild() {
        when(leadRepository.findCreatedLeadsForGoal(any(), any(), anySet())).thenReturn(3L);
        Set<UserGoalDto> userGoals = goalDataCollectorService.getUserGoals(goal, 2L);
        assertNotNull(userGoals);

        assertEquals(2, userGoals.size());
        assertTrue(userGoals.stream().allMatch(c -> Arrays.asList(4L, 5L).contains(c.getUserId())));
        assertTrue(userGoals.stream().allMatch(c -> c.getIntervals().size() == 2));

        for (UserGoalDto ugDto : userGoals) {
            Optional<GoalIntervalDto> index0 = ugDto.getIntervals().stream().filter(c -> c.getIndex().equals(0L)).findFirst();
            Optional<GoalIntervalDto> index1 = ugDto.getIntervals().stream().filter(c -> c.getIndex().equals(1L)).findFirst();

            assertTrue(index0.isPresent());
            assertTrue(index1.isPresent());

            assertEquals(1.0, index0.get().getExpectedValue(), DELTA);
            assertEquals(3.0, index0.get().getCurrentValue(), DELTA);
            assertEquals(2.0, index0.get().getResult(), DELTA);

            assertEquals(2.0, index1.get().getExpectedValue(), DELTA);
            assertEquals(3.0, index1.get().getCurrentValue(), DELTA);
            assertEquals(1.0, index1.get().getResult(), DELTA);
        }
    }

    @Test
    public void shouldReturnUserGoalsForUserWithoutChild() {
        Set<UserGoalDto> userGoals = goalDataCollectorService.getUserGoals(goal, 8L);
        assertNotNull(userGoals);
        assertEquals(0, userGoals.size());
    }

    @Test
    public void shouldRetrieveDataForType_ADDED_CUSTOMERS_NUMBER() {
        when(customerRepository.findForGoal(any(), any(), anySet())).thenReturn(3L);
        goal.setType(GoalType.ADDED_CUSTOMERS_NUMBER);
        goalDataCollectorService.getUserGoals(goal, 1L);
        verify(customerRepository, times(6)).findForGoal(any(), any(), anySet());
    }

    @Test
    public void shouldRetrieveDataForType_ADDED_CONTACTS_NUMBER() {
        when(contactRepository.findForGoal(any(), any(), anySet())).thenReturn(3L);
        goal.setType(GoalType.ADDED_CONTACTS_NUMBER);
        goalDataCollectorService.getUserGoals(goal, 1L);
        verify(contactRepository, times(6)).findForGoal(any(), any(), anySet());
    }

    @Test
    public void shouldRetrieveDataForType_PHONE_CALLS_NUMBER() {
        when(taskClient.getActivityCountForGoal(anySet(), eq(true), any(), any())).thenReturn(3L);
        goal.setType(GoalType.PHONE_CALLS_NUMBER);
        goalDataCollectorService.getUserGoals(goal, 1L);
        verify(taskClient, times(6)).getActivityCountForGoal(anySet(), eq(true), any(), any());
    }

    @Test
    public void shouldRetrieveDataForType_CALENDAR_TASKS_NUMBER() {
        when(taskClient.getActivityCountForGoal(anySet(), eq(false), any(), any())).thenReturn(3L);
        goal.setType(GoalType.CALENDAR_TASKS_NUMBER);
        goalDataCollectorService.getUserGoals(goal, 1L);
        verify(taskClient, times(6)).getActivityCountForGoal(anySet(), eq(false), any(), any());
    }

    @Test
    public void shouldRetrieveDataForType_CREATED_LEADS_NUMBER() {
        when(leadRepository.findCreatedLeadsForGoal(any(), any(), anySet())).thenReturn(3L);
        goal.setType(GoalType.CREATED_LEADS_NUMBER);
        goalDataCollectorService.getUserGoals(goal, 1L);
        verify(leadRepository, times(6)).findCreatedLeadsForGoal(any(), any(), anySet());
    }

    @Test
    public void shouldRetrieveDataForType_CONVERTED_LEADS_NUMBER() {
        when(leadRepository.findConvertedLeadsForGoal(any(), any(), anySet())).thenReturn(3L);
        goal.setType(GoalType.CONVERTED_LEADS_NUMBER);
        goalDataCollectorService.getUserGoals(goal, 1L);
        verify(leadRepository, times(6)).findConvertedLeadsForGoal(any(), any(), anySet());
    }

    @Test
    public void shouldRetrieveDataForType_CREATED_OPPORTUNITIES_NUMBER() {
        when(opportunityRepository.findCreatedOpportunityForGoal(any(), any(), anySet())).thenReturn(3L);
        goal.setType(GoalType.CREATED_OPPORTUNITIES_NUMBER);
        goalDataCollectorService.getUserGoals(goal, 1L);
        verify(opportunityRepository, times(6)).findCreatedOpportunityForGoal(any(), any(), anySet());
    }

    @Test
    public void shouldRetrieveDataForType_WON_OPPORTUNITIES_NUMBER() {
        when(opportunityRepository.findAllWonOpportunitiesForGoal(any(), any(), anySet(), any())).thenReturn(getOpportunityList());
        goal.setType(GoalType.WON_OPPORTUNITIES_NUMBER);
        goalDataCollectorService.getUserGoals(goal, 1L);
        verify(opportunityRepository, times(6)).findAllWonOpportunitiesForGoal(any(), any(), anySet(), any());
    }

    @Test
    public void shouldRetrieveDataForType_WON_OPPORTUNITIES_AMOUNT() {
        when(opportunityRepository.findAllWonOpportunitiesForGoal(any(), any(), anySet(), any())).thenReturn(getOpportunityList());
        goal.setType(GoalType.WON_OPPORTUNITIES_AMOUNT);
        goalDataCollectorService.getUserGoals(goal, 1L);
        verify(opportunityRepository, times(6)).findAllWonOpportunitiesForGoal(any(), any(), anySet(), any());
    }

    @Test
    public void shouldRetrieveDataForType_WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT() {
        when(opportunityRepository.findAllWonOpportunitiesForGoal(any(), any(), anySet(), any())).thenReturn(getOpportunityList());
        goal.setType(GoalType.WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT);
        goalDataCollectorService.getUserGoals(goal, 1L);
        verify(opportunityRepository, times(6)).findAllWonOpportunitiesForGoal(any(), any(), anySet(), any());
    }

    @Test
    public void shouldRetrieveDataForType_OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT() {
        when(opportunityRepository.findAllWonOpportunitiesForGoal(any(), any(), anySet(), any())).thenReturn(getOpportunityList());
        goal.setType(GoalType.OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT);
        goalDataCollectorService.getUserGoals(goal, 1L);
        verify(opportunityRepository, times(6)).findAllWonOpportunitiesForGoal(any(), any(), anySet(), any());
    }

    @Test
    public void shouldReturnCorrectMonthlyIntervals() {
        goal.setInterval(GoalIntervalType.MONTH);
        goal.setStartIndex(1);
        Map<Integer, IntervalRangeDto> intervalRange = goalDataCollectorService.getIntervalRange(goal);
        assertNotNull(intervalRange);

        assertEquals(LocalDateTime.of(1993, 1, 1, 0, 0, 0), intervalRange.get(0).getFrom());
        assertEquals(LocalDateTime.of(1993, 1, 31, 23, 59, 59), intervalRange.get(0).getTo());

        assertEquals(LocalDateTime.of(1993, 2, 1, 0, 0, 0), intervalRange.get(1).getFrom());
        assertEquals(LocalDateTime.of(1993, 2, 28, 23, 59, 59), intervalRange.get(1).getTo());

        assertEquals(LocalDateTime.of(1993, 3, 1, 0, 0, 0), intervalRange.get(2).getFrom());
        assertEquals(LocalDateTime.of(1993, 3, 31, 23, 59, 59), intervalRange.get(2).getTo());

        assertEquals(LocalDateTime.of(1993, 4, 1, 0, 0, 0), intervalRange.get(3).getFrom());
        assertEquals(LocalDateTime.of(1993, 4, 30, 23, 59, 59), intervalRange.get(3).getTo());

        assertEquals(LocalDateTime.of(1993, 5, 1, 0, 0, 0), intervalRange.get(4).getFrom());
        assertEquals(LocalDateTime.of(1993, 5, 31, 23, 59, 59), intervalRange.get(4).getTo());

        assertEquals(LocalDateTime.of(1993, 6, 1, 0, 0, 0), intervalRange.get(5).getFrom());
        assertEquals(LocalDateTime.of(1993, 6, 30, 23, 59, 59), intervalRange.get(5).getTo());

        assertEquals(LocalDateTime.of(1993, 7, 1, 0, 0, 0), intervalRange.get(6).getFrom());
        assertEquals(LocalDateTime.of(1993, 7, 31, 23, 59, 59), intervalRange.get(6).getTo());

        assertEquals(LocalDateTime.of(1993, 8, 1, 0, 0, 0), intervalRange.get(7).getFrom());
        assertEquals(LocalDateTime.of(1993, 8, 31, 23, 59, 59), intervalRange.get(7).getTo());

        assertEquals(LocalDateTime.of(1993, 9, 1, 0, 0, 0), intervalRange.get(8).getFrom());
        assertEquals(LocalDateTime.of(1993, 9, 30, 23, 59, 59), intervalRange.get(8).getTo());

        assertEquals(LocalDateTime.of(1993, 10, 1, 0, 0, 0), intervalRange.get(9).getFrom());
        assertEquals(LocalDateTime.of(1993, 10, 31, 23, 59, 59), intervalRange.get(9).getTo());

        assertEquals(LocalDateTime.of(1993, 11, 1, 0, 0, 0), intervalRange.get(10).getFrom());
        assertEquals(LocalDateTime.of(1993, 11, 30, 23, 59, 59), intervalRange.get(10).getTo());

        assertEquals(LocalDateTime.of(1993, 12, 1, 0, 0, 0), intervalRange.get(11).getFrom());
        assertEquals(LocalDateTime.of(1993, 12, 31, 23, 59, 59), intervalRange.get(11).getTo());
    }

    @Test
    public void shouldReturnCorrectQuarterlyIntervals() {
        goal.setInterval(GoalIntervalType.QUARTER);
        goal.setStartIndex(1);
        Map<Integer, IntervalRangeDto> intervalRange = goalDataCollectorService.getIntervalRange(goal);
        assertNotNull(intervalRange);

        assertEquals(LocalDateTime.of(1993, 1, 1, 0, 0, 0), intervalRange.get(0).getFrom());
        assertEquals(LocalDateTime.of(1993, 3, 31, 23, 59, 59), intervalRange.get(0).getTo());

        assertEquals(LocalDateTime.of(1993, 4, 1, 0, 0, 0), intervalRange.get(1).getFrom());
        assertEquals(LocalDateTime.of(1993, 6, 30, 23, 59, 59), intervalRange.get(1).getTo());

        assertEquals(LocalDateTime.of(1993, 7, 1, 0, 0, 0), intervalRange.get(2).getFrom());
        assertEquals(LocalDateTime.of(1993, 9, 30, 23, 59, 59), intervalRange.get(2).getTo());

        assertEquals(LocalDateTime.of(1993, 10, 1, 0, 0, 0), intervalRange.get(3).getFrom());
        assertEquals(LocalDateTime.of(1993, 12, 31, 23, 59, 59), intervalRange.get(3).getTo());
    }

    @Test
    public void shouldReturnCorrectYearlyIntervals() {
        goal.setInterval(GoalIntervalType.YEAR);
        goal.setStartIndex(1);
        Map<Integer, IntervalRangeDto> intervalRange = goalDataCollectorService.getIntervalRange(goal);
        assertNotNull(intervalRange);

        assertEquals(LocalDateTime.of(1993, 1, 1, 0, 0, 0), intervalRange.get(0).getFrom());
        assertEquals(LocalDateTime.of(1993, 12, 31, 23, 59, 59), intervalRange.get(0).getTo());
    }

    private List<Opportunity> getOpportunityList() {
        Opportunity opportunity1 = new Opportunity();
        opportunity1.setId(1L);
        opportunity1.setOwnerOneId(1L);
        opportunity1.setOwnerOnePercentage(20L);
        opportunity1.setOpportunityProducts(new ArrayList<>());
        opportunity1.setAmount(40.0);
        Opportunity opportunity2 = new Opportunity();
        opportunity2.setId(2L);
        opportunity2.setOwnerOneId(2L);
        opportunity2.setOwnerOnePercentage(80L);
        opportunity2.setOpportunityProducts(new ArrayList<>());
        opportunity2.setAmount(80.0);
        Opportunity opportunity3 = new Opportunity();
        opportunity3.setId(3L);
        opportunity3.setOwnerOneId(3L);
        opportunity3.setOwnerOnePercentage(40L);
        opportunity3.setOpportunityProducts(new ArrayList<>());
        opportunity3.setAmount(100.0);
        return Arrays.asList(opportunity1, opportunity2, opportunity3);
    }

    private Goal getGoal() {
        Goal goal = new Goal();
        goal.setId(1L);
        goal.setName("goalName");
        goal.setType(GoalType.CREATED_LEADS_NUMBER);
        goal.setInterval(GoalIntervalType.QUARTER);
        goal.setIntervalNumber(2);
        goal.setStartIndex(2);
        goal.setStartYear(1993);
        goal.setCurrency("PLN");

        goal.setUsersGoals(new HashSet<>());
        goal.getUsersGoals().add(getUserGoal(1L, 1L, 1L));
        goal.getUsersGoals().add(getUserGoal(2L, 2L, 1L));
        goal.getUsersGoals().add(getUserGoal(3L, 3L, 1L));

        goal.getUsersGoals().add(getUserGoal(4L, 4L, 2L));
        goal.getUsersGoals().add(getUserGoal(5L, 5L, 2L));

        goal.getUsersGoals().add(getUserGoal(6L, 6L, 4L));
        goal.getUsersGoals().add(getUserGoal(7L, 7L, 4L));
        goal.getUsersGoals().add(getUserGoal(8L, 8L, 4L));


        goal.setDeleted(false);
        return goal;
    }

    private UserGoal getUserGoal(Long id, Long userId, Long parentId) {
        UserGoal userGoal = new UserGoal();
        userGoal.setId(id);
        userGoal.setUserId(userId);
        userGoal.setParentUserId(parentId);

        userGoal.setIntervals(new HashSet<>());
        userGoal.getIntervals().add(getGoalInterval(100L + id, 0L, 1.0));
        userGoal.getIntervals().add(getGoalInterval(200L + id, 1L, 2.0));

        return userGoal;
    }

    private GoalInterval getGoalInterval(Long id, Long index, Double expectedValue) {
        GoalInterval goalInterval = new GoalInterval();
        goalInterval.setId(id);
        goalInterval.setIndex(index);
        goalInterval.setExpectedValue(expectedValue);
        return goalInterval;
    }
}
