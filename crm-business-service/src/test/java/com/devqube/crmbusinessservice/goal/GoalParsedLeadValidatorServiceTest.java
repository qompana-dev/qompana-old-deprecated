package com.devqube.crmbusinessservice.goal;

import com.devqube.crmbusinessservice.goal.model.*;
import com.devqube.crmshared.exception.BadRequestException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Calendar;
import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
public class GoalParsedLeadValidatorServiceTest {


    @InjectMocks
    private GoalValidatorService goalValidatorService;

    private Goal goal;
    private UserGoal userGoal;
    private Set<GoalInterval> intervals;
    private GoalInterval interval1;
    private GoalInterval interval2;

    @Before
    public void setUp() throws Exception {
        interval1 = GoalInterval.builder()
                .index(0L)
                .expectedValue(5.0)
                .build();
        interval2 = GoalInterval.builder()
                .index(1L)
                .expectedValue(10.0)
                .build();
        intervals = Set.of(interval1, interval2);

        userGoal = UserGoal.builder()
                .intervals(intervals)
                .build();

        goal = Goal.builder()
                .name("Goal")
                .interval(GoalIntervalType.MONTH)
                .ownerId(123L)
                .type(GoalType.CONVERTED_LEADS_NUMBER)
                .intervalNumber(2)
                .startIndex(1)
                .startYear(Calendar.getInstance().get(Calendar.YEAR))
                .usersGoals(Set.of(userGoal))
                .products(Set.of())
                .subcategories(Set.of())
                .build();

    }

    @Test()
    public void shouldPassValidation() throws BadRequestException {
        goalValidatorService.assertValid(this.goal);
    }

    @Test(expected = BadRequestException.class)
    public void shouldTrowExceptionBecauseOfEmptyProductList() throws BadRequestException {
        this.goal.setType(GoalType.WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT);
        goalValidatorService.assertValid(this.goal);
    }

    @Test(expected = BadRequestException.class)
    public void shouldTrowExceptionBecauseOfEmptySubcategoriesList() throws BadRequestException {
        this.goal.setType(GoalType.OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT);
        goalValidatorService.assertValid(this.goal);
    }

    @Test(expected = BadRequestException.class)
    public void shouldTrowExceptionBecauseOfWrongIntervalNumber() throws BadRequestException {
        this.goal.setIntervalNumber(20);
        goalValidatorService.assertValid(this.goal);
    }

    @Test(expected = BadRequestException.class)
    public void shouldTrowExceptionBecauseOfThereIsOnyOneGoalInterval() throws BadRequestException {
        this.userGoal.setIntervals(Set.of(interval1));
        goalValidatorService.assertValid(this.goal);
    }

    @Test(expected = BadRequestException.class)
    public void shouldTrowExceptionBecauseOfThereIsWrongIndexInInterval() throws BadRequestException {
        this.interval1.setIndex(20L);
        goalValidatorService.assertValid(this.goal);
    }

    @Test(expected = BadRequestException.class)
    public void shouldTrowExceptionBecauseOfEmptyUserGoals() throws BadRequestException {
        this.goal.setUsersGoals(Set.of());
        goalValidatorService.assertValid(this.goal);
    }

    @Test(expected = BadRequestException.class)
    public void shouldTrowExceptionBecauseYearIsInPast() throws BadRequestException {
        this.goal.setStartYear(Calendar.getInstance().get(Calendar.YEAR) - 1);
        goalValidatorService.assertValid(this.goal);
    }

    @Test(expected = BadRequestException.class)
    public void shouldTrowExceptionBecauseOfWrongIntervalNumberWhenYearly() throws BadRequestException {
        this.goal.setInterval(GoalIntervalType.YEAR);
        this.goal.setIntervalNumber(4);
        goalValidatorService.assertValid(this.goal);
    }

    @Test(expected = BadRequestException.class)
    public void shouldTrowExceptionBecauseOfWrongIntervalNumberAndStartIndexWhenQuarterly() throws BadRequestException {
        this.goal.setInterval(GoalIntervalType.QUARTER);
        this.goal.setIntervalNumber(4);
        this.goal.setStartIndex(2);
        goalValidatorService.assertValid(this.goal);
    }

    @Test(expected = BadRequestException.class)
    public void shouldTrowExceptionBecauseOfWrongIntervalNumberAndStartIndexWhenMonthly() throws BadRequestException {
        this.goal.setInterval(GoalIntervalType.MONTH);
        this.goal.setIntervalNumber(10);
        this.goal.setStartIndex(6);
        goalValidatorService.assertValid(this.goal);
    }

    @Test(expected = BadRequestException.class)
    public void shouldTrowExceptionBecauseIntervalNumberIsNegative() throws BadRequestException {
        this.goal.setIntervalNumber(-10);
        goalValidatorService.assertValid(this.goal);
    }

    @Test(expected = BadRequestException.class)
    public void shouldTrowExceptionBecauseStartIndexIsNegative() throws BadRequestException {
        this.goal.setStartIndex(-10);
        goalValidatorService.assertValid(this.goal);
    }


}
