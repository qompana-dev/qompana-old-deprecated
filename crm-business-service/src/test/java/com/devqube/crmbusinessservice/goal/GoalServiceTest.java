package com.devqube.crmbusinessservice.goal;

import com.devqube.crmbusinessservice.access.UserService;
import com.devqube.crmbusinessservice.goal.model.Goal;
import com.devqube.crmbusinessservice.goal.model.GoalIntervalType;
import com.devqube.crmbusinessservice.goal.model.GoalType;
import com.devqube.crmbusinessservice.goal.model.UserGoal;
import com.devqube.crmbusinessservice.goal.web.dto.GoalEditDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GoalServiceTest {

    @Mock
    private UserService userService;
    @Mock
    private GoalRepository goalRepository;
    @Mock
    private GoalMapperService goalMapper;
    @Mock
    private ModelMapper modelMapper;
    @Mock
    private GoalValidatorService goalValidatorService;
    @Mock
    private UserGoalRepository userGoalRepository;
    @Mock
    private GoalDataCollectorService goalDataCollectorService;

    @InjectMocks
    private GoalService goalService;

    private Goal goal;
    private GoalEditDto goalEditDto;

    @Before
    public void setUp() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Logged-Account-Email", "jan.kowalski@devqube.com");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        goal = createGoal();
        goalEditDto = new GoalEditDto();
    }

    @Test
    public void shouldDeleteUserGoalsOnAllLevelsAndAddNewOnes() throws BadRequestException, EntityNotFoundException {
        when(userService.getUserId("test@test")).thenReturn(2L);
        when(goalRepository.findById(1L)).thenReturn(Optional.of(goal));
        when(goalMapper.getUserGoals(goal, goalEditDto, 2L)).thenReturn(Set.of(
                createUserGoal(5L, 5L, 2L),
                createUserGoal(9L, 9L, 2L)
        ));
        when(userGoalRepository.findAllByGoalIdAndParentUserIdAndUserIdNot(1L, 2L, 2L)).thenReturn(Set.of(
                createUserGoal(4L, 4L, 2L),
                createUserGoal(5L, 5L, 2L)
        ));
        when(userGoalRepository.findAllByGoalIdAndParentUserIdAndUserIdNot(1L, 4L, 4L)).thenReturn(Set.of(
                createUserGoal(6L, 6L, 4L),
                createUserGoal(7L, 7L, 4L),
                createUserGoal(8L, 8L, 4L)
        ));
        when(userGoalRepository.findAllByGoalIdAndParentUserIdAndUserIdNot(1L, 6L, 6L)).thenReturn(Collections.emptySet());
        when(userGoalRepository.findAllByGoalIdAndParentUserIdAndUserIdNot(1L, 7L, 7L)).thenReturn(Collections.emptySet());
        when(goalValidatorService.isUserGoalsValid(anySet())).thenReturn(true);
        when(userGoalRepository.findAllByGoalIdAndParentUserIdAndUserIdNot(1L, 8L, 8L)).thenReturn(Collections.emptySet());
        when(modelMapper.map(goal, GoalEditDto.class)).thenReturn(new GoalEditDto());
        goalService.updateGoal(1L, goalEditDto, "test@test");
        verify(userGoalRepository, times(1)).deleteAll(Set.of(
                createUserGoal(4L, 4L, 2L),
                createUserGoal(6L, 6L, 4L),
                createUserGoal(7L, 7L, 4L),
                createUserGoal(8L, 8L, 4L)
        ));
        verify(userGoalRepository, times(1)).deleteAllByGoalIdAndParentUserId(1L, 2L);
        verify(userGoalRepository, times(1)).saveAll(Set.of(
                createUserGoal(5L, 5L, 2L),
                createUserGoal(9L, 9L, 2L)
        ));
    }

    private static Goal createGoal() {
        Goal goal = new Goal();
        goal.setId(1L);
        goal.setName("goalName");
        goal.setType(GoalType.CREATED_LEADS_NUMBER);
        goal.setInterval(GoalIntervalType.QUARTER);
        goal.setIntervalNumber(2);
        goal.setStartIndex(2);
        goal.setStartYear(1993);
        goal.setCurrency("PLN");
        goal.setDeleted(false);

        /*
            1
                2
                    4
                        6
                        7
                        8
                    5
                3
         */

        goal.setUsersGoals(Set.of(
                createUserGoal(1L, 1L, 1L),
                createUserGoal(2L, 2L, 1L),
                createUserGoal(3L, 3L, 1L),

                createUserGoal(4L, 4L, 2L),
                createUserGoal(5L, 5L, 2L),

                createUserGoal(6L, 6L, 4L),
                createUserGoal(7L, 7L, 4L),
                createUserGoal(8L, 8L, 4L)
        ));
        return goal;
    }

    private static UserGoal createUserGoal(Long id, Long userId, Long parentId) {
        UserGoal userGoal = new UserGoal();
        userGoal.setId(id);
        userGoal.setUserId(userId);
        userGoal.setParentUserId(parentId);
        return userGoal;
    }
}
