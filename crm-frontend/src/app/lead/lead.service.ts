import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {
  CanConvertResponseDto,
  ConvertedOpportunity,
  KanbanLead,
  KanbanProcessOverview,
  KanbanTaskDetails,
  LeadConversion,
  LeadOnList,
  LeadSave,
  LeadsKeeper,
  ProcessDefinition,
  SearchSimilarObjectsDto
} from './lead.model';
import {Page, PageRequest} from '../shared/pagination/page.model';
import {FilterStrategy} from '../shared/model';
import {LeadDetails, Task} from './lead-details/lead-details.model';
import {TableUtil} from '../shared/table.util';
import {CrmObject} from '../shared/model/search.model';
import {getHeaderForAuthType} from '../shared/permissions-utils';
import {UrlUtil} from '../shared/url.util';
import {PrimaryProcess} from "../bpmn/bpmn.model";

@Injectable({
  providedIn: 'root'
})
export class LeadService {

  private leadsUrl = `${UrlUtil.url}/crm-business-service/leads`;

  constructor(private http: HttpClient) {
  }

  public getLeads(filterStrategy: FilterStrategy, pageRequest: PageRequest, selectedSearch?: string): Observable<Page<LeadOnList>> {
    let search = '';
    if (selectedSearch !== null) {
      search = selectedSearch;
    }
    let params = new HttpParams({fromObject: {...pageRequest, search}});
    params = TableUtil.enrichParamsWithFilterStrategy(filterStrategy, params);
    return this.http.get<Page<LeadOnList>>(this.leadsUrl, {params});
  }

  public getLeadForEdit(leadId: number): Observable<LeadSave> {
    const url = `${this.leadsUrl}/for-edit/${leadId}`;
    return this.http.get<LeadSave>(url);
  }

  public createLead(lead: LeadSave): Observable<LeadSave> {
    return this.http.post<LeadSave>(this.leadsUrl, lead);
  }

  public updateLead(leadId: number, lead: LeadSave): Observable<LeadSave> {
    const url = `${this.leadsUrl}/${leadId}`;
    return this.http.put<LeadSave>(url, lead);
  }

  public removeLead(leadId: number): Observable<void> {
    const url = `${this.leadsUrl}/${leadId}`;
    return this.http.delete<void>(url);
  }

  public canConvert(leadId: number): Observable<CanConvertResponseDto> {
    const url = `${this.leadsUrl}/${leadId}/can-convert`;
    return this.http.get<CanConvertResponseDto>(url);
  }

  public removeLeads(leads: LeadOnList[]): Observable<void> {
    const params = new HttpParams().set('ids', leads.map(lead => lead.id).join(','));
    return this.http.delete<void>(this.leadsUrl, {params});
  }

  public getLeadById(id: any): Observable<LeadDetails> {
    return this.http.get<LeadDetails>(this.leadsUrl + `/${id}`);
  }

  public findLeadByOpportunityId(id: any): Observable<LeadDetails> {
    return this.http.get<LeadDetails>(this.leadsUrl + `/opportunity/${id}`);
  }

  public getProcessDefinitions(objectType: string): Observable<ProcessDefinition[]> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/repository/process-definitions/${objectType}`;
    return this.http.get<ProcessDefinition[]>(url);
  }

  getAllPrimaryProcesses(): Observable<PrimaryProcess[]> {
    const url = `${UrlUtil.url}/crm-business-service/primary-process`;
    return this.http.get<PrimaryProcess[]>(url);
  }

  public postFormProperties(id: string, params: { [key: string]: { value: string } }): Observable<any> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/process-instance/${id}/form`;
    return this.http.post(url, params);
  }

  public getProcessKanbanDetails(processDefinitionId: string): Observable<KanbanTaskDetails[]> {
    // const url = `/assets/mocks/lead-canban-details.json`;
    const url = `${UrlUtil.url}/crm-business-service/kanban-processes/lead/${processDefinitionId}`;
    return this.http.get<KanbanTaskDetails[]>(url);
  }

  public getProcessKanbanOverview(filterStrategy: FilterStrategy): Observable<KanbanProcessOverview[]> {
    // const url = `/assets/mocks/lead-canban-mock.json`;
    let params = new HttpParams().set('filterStrategy', filterStrategy);
    const url = `${UrlUtil.url}/crm-business-service/kanban-processes/lead`;
    return this.http.get<KanbanProcessOverview[]>(url, {params});
  }

  public completeTask(processInstanceId: string): Observable<any> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/task/${processInstanceId}/complete`;
    return this.http.get(url, {observe: 'response', headers: getHeaderForAuthType('lead')});
  }

  public accept(notificationDto: { userNotificationId: number, comment?: string }): Observable<any> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/accept-task`;
    return this.http.post(url, notificationDto);
  }

  public decline(notificationDto: { userNotificationId: number, comment?: string }): Observable<any> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/decline-task`;
    return this.http.post(url, notificationDto);
  }

  public goBack(processInstanceId: string, numberOfSteps: number): Observable<any> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/task/${processInstanceId}/go-back/${numberOfSteps}`;
    return this.http.get(url, {headers: getHeaderForAuthType('lead')});
  }

  public getKanbanLead(leadId: number): Observable<KanbanLead> {
    const url = `${UrlUtil.url}/crm-business-service/kanban-processes/leads/${leadId}`;
    return this.http.get<KanbanLead>(url);
  }

  public getTaskForReadOnly(processInstanceId: string, taskId: string, futureTask: boolean): Observable<Task> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/process-instance/${processInstanceId}/task/${taskId}?futureTask=${futureTask}`;
    return this.http.get<Task>(url, {headers: getHeaderForAuthType('lead')});
  }

  public changeLeadsKeeper(leadsKeeper: LeadsKeeper): Observable<any> {
    const url = `${this.leadsUrl}/keeper`;
    return this.http.put<LeadSave>(url, leadsKeeper);
  }

  getLeadsForSearch(input: string): Observable<CrmObject[]> {
    return this.http.get<CrmObject[]>(`${UrlUtil.url}/crm-business-service/leads/search?term=${input}`);
  }

  getLeadsByIds(leadIds: number[]): Observable<CrmObject[]> {
    if (!leadIds || leadIds.length === 0) {
      return of([]);
    }
    const params = new HttpParams().set('ids', leadIds.join(','));
    return this.http.get<CrmObject[]>(`${UrlUtil.url}/crm-business-service/leads/crm-object`, {params});
  }

  convertToOppOpportunity(leadId: number, leadConversion: LeadConversion): Observable<ConvertedOpportunity> {
    return this.http.post<ConvertedOpportunity>(`${this.leadsUrl}/${leadId}/convert-to-opportunity`, leadConversion);
  }

  searchSimilarObjects(searchSimilarObjectsDto: SearchSimilarObjectsDto): Observable<CrmObject[]> {
    return this.http.post<CrmObject[]>(`${this.leadsUrl}/search-similar-objects`, searchSimilarObjectsDto);
  }

  closeLead(leadId: number, closeResult: string, closeReason?: string): Observable<void> {
    let params = new HttpParams().set('closeResult', closeResult);
    if (closeReason) {
      params = params.set('closeReason', closeReason);
    }
    return this.http.get<void>(`${this.leadsUrl}/${leadId}/close`, {params});
  }

  exportLeads(): Observable<Blob> {
    const url = `${UrlUtil.url}/crm-business-service/leads/export`;
    return this.http.get(url, {responseType: 'blob'});
  }
}
