import { DictionaryId } from "./../../dictionary/dictionary.model";
import { DictionaryStoreService } from "src/app/dictionary/dictionary-store.service";
import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from "@angular/forms";
import { of, Subject } from "rxjs";
import { MatCheckboxChange, MatOptionSelectionChange } from "@angular/material";
import { FormUtil } from "../../shared/form.util";
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  switchMap,
  takeUntil,
  tap,
} from "rxjs/operators";
import {
  ErrorTypes,
  NotificationService,
  NotificationType,
} from "../../shared/notification.service";
import { LeadService } from "../lead.service";
import {
  ConvertedOpportunity,
  LeadConversion,
  ProcessDefinition,
} from "../lead.model";
import { PersonService } from "../../person/person.service";
import { AccountEmail } from "../../person/person.model";
import { TranslateService } from "@ngx-translate/core";
import { LoginService } from "../../login/login.service";
import { CrmObject, CrmObjectType } from "../../shared/model/search.model";
import { AssignToGroupService } from "../../shared/assign-to-group/assign-to-group.service";
import * as isNil from "lodash/isNil";
import { ContactsService } from "../../contacts/contacts.service";
import { Contact } from "../../contacts/contacts.model";
import { SliderService } from "../../shared/slider/slider.service";
import { LeadDetails } from "../lead-details/lead-details.model";
import { BAD_REQUEST, FORBIDDEN, GONE, NOT_FOUND } from "http-status-codes";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { AuthService } from "../../login/auth/auth.service";
import { GlobalConfigService } from "../../global-config/global-config.service";
import { CustomerService } from "../../customer/customer.service";
import { FormValidators } from "../../shared/form/form-validators";
import { Customer } from "../../customer/customer.model";

@Component({
  selector: "app-lead-to-opportunity",
  templateUrl: "./lead-to-opportunity.component.html",
  styleUrls: ["./lead-to-opportunity.component.scss"],
})
export class LeadToOpportunityComponent implements OnInit, OnDestroy {
  public userMap: Map<number, string>;

  @Output() leadConverted: EventEmitter<void> = new EventEmitter<void>();

  @Input() set lead(lead: LeadDetails) {
    this._lead = lead;
    if (this.conversionForm && lead) {
      this.getPositionForById(lead.position, () => {
        this.customerNameControl.patchValue(lead.companyName);
        this.contactNameControl.patchValue(lead.firstName);
        this.contactSurnameControl.patchValue(lead.lastName);
        this.contactPositionControl.patchValue(this.leadPosition);
      });
    }
  }

  currencies = [];
  processDefinitions: ProcessDefinition[];
  accountEmails: AccountEmail[] = [];
  conversionForm: FormGroup;
  foundCustomers: CrmObject[] = [];
  foundContacts: CrmObject[] = [];
  filteredContacts: CrmObject[];
  convertedOpportunity: ConvertedOpportunity;
  beforeConversion = true;
  private componentDestroyed: Subject<void> = new Subject();
  private selectedCustomerName: string;
  private selectedContactName: string;
  // tslint:disable-next-line:variable-name
  private _lead: LeadDetails;
  leadPosition: string;

  readonly conversionErrorTypes: ErrorTypes = {
    base: "leads.toOpportunity.notification.",
    defaultText: "leadConversionError",
    errors: [
      {
        code: FORBIDDEN,
        text: "forbiddenError",
      },
      {
        code: NOT_FOUND,
        text: "leadNotFoundError",
      },
      {
        code: BAD_REQUEST,
        text: "badRequestError",
      },
    ],
  };

  gusErrors: ErrorTypes = {
    base: "customer.form.notification.",
    defaultText: "unknownError",
    errors: [
      {
        code: NOT_FOUND,
        text: "customerNotFoundInGus",
      },
      {
        code: BAD_REQUEST,
        text: "badRequest",
      },
      {
        code: GONE,
        text: "errorInGus",
      },
    ],
  };

  constructor(
    private fb: FormBuilder,
    private leadService: LeadService,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private loginService: LoginService,
    private assignToGroupService: AssignToGroupService,
    private contactsService: ContactsService,
    private sliderService: SliderService,
    private personService: PersonService,
    private router: Router,
    private authService: AuthService,
    private configService: GlobalConfigService,
    private customerService: CustomerService,
    private dictionariesService: DictionaryStoreService
  ) {
    this.createConversionForm();
  }

  ngOnInit(): void {
    this.subscribeForCurrencies();
    this.getProcessDefinitions();
    this.getAccountEmails();
    this.listenForCustomerSearch();
    this.listenForCustomerSelection();
    this.listenForContactSearch();
    this.getUsers();
    this.personService.getIdNameMap();
    this.listenWhenSliderClosed();
    this.listenWhenSliderClosed();
    this.applyPermissions();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  cancel(): void {
    this.sliderService.closeSlider();
  }

  convert(): void {
    if (!this.conversionForm.valid) {
      FormUtil.setTouched(this.conversionForm);
      return;
    }
    this.convertToOpportunity();
  }

  private listenForCustomerSearch(): void {
    this.customerSearchControl.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed),
        debounceTime(250),
        filter((input: string) => !!input && input.length >= 3),
        tap(() => {
          this.foundCustomers = [];
        }),
        switchMap((input: string) =>
          this.assignToGroupService
            .searchForType(CrmObjectType.customer, input, [
              CrmObjectType.customer,
            ])
            .pipe(catchError(() => of([])))
        )
      )
      .subscribe((customers) => (this.foundCustomers = customers));
  }

  private getProcessDefinitions(): void {
    this.leadService
      .getProcessDefinitions("OPPORTUNITY")
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (definitions) => (this.processDefinitions = definitions),
        (err) =>
          this.notificationService.notify(
            "leads.toOpportunity.notification.getProcessDefinitionsError",
            NotificationType.ERROR
          )
      );
  }

  getProcessFirstTask(): string {
    if (
      this.opportunityProcessControl &&
      this.opportunityProcessControl.value
    ) {
      const id = this.opportunityProcessControl.value;
      const process = this.processDefinitions.find((p) => p.id === id);
      if (process) {
        return process.firstTaskName;
      }
    }
    return this.translateService.instant("leads.toOpportunity.emptyStep");
  }

  getDataFromGUS($event: MouseEvent): void {
    $event.stopPropagation();
    if (!this.isNewCustomerControl.value) {
      return;
    }
    if (this.customerNipControl.valid) {
      this.customerService
        .getDataFromGUS(this.customerNipControl.value, null)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (customer: Customer) => this.handleSuccessfulImport(customer),
          (error) =>
            this.notificationService.notifyError(this.gusErrors, error.status)
        );
    } else {
      this.notificationService.notify(
        "customer.form.nipMinLength",
        NotificationType.ERROR
      );
    }
  }

  private getPositionForById(id: number, func: () => void): void {
    this.dictionariesService
      .getDictionaryWordsById(DictionaryId.POSITION, [id])
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((words) => {
        const position = words.find((position) => position.id === id);
        this.leadPosition = position && position.name;
        func();
      });
  }

  private handleSuccessfulImport(customer: Customer): void {
    this.customerNameControl.patchValue(customer.name);
    this.customerAddressControl.patchValue(customer.address);
    this.notificationService.notify(
      "customer.form.notification.successImportFromGus",
      NotificationType.SUCCESS
    );
  }

  private getAccountEmails(): void {
    const accountId = +localStorage.getItem("loggedAccountId");
    this.personService
      .getTeamEmailsById(accountId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (emails) => {
          this.accountEmails = emails;
        },
        (err) =>
          this.notificationService.notify(
            "opportunity.form.notification.accountEmailsError",
            NotificationType.ERROR
          )
      );
  }

  // @formatter:off
  get isNewCustomerControl(): AbstractControl {
    return this.conversionForm.get("isNewCustomer");
  }
  get customerNameControl(): AbstractControl {
    return this.conversionForm.get("customerName");
  }
  get customerAddressControl(): AbstractControl {
    return this.conversionForm.get("customerAddress");
  }
  get customerNipControl(): AbstractControl {
    return this.conversionForm.get("customerNip");
  }
  get isExistingCustomerControl(): AbstractControl {
    return this.conversionForm.get("isExistingCustomer");
  }
  get customerSearchControl(): AbstractControl {
    return this.conversionForm.get("customerSearch");
  }
  get customerIdControl(): AbstractControl {
    return this.conversionForm.get("customerId");
  }
  get isNewContactControl(): AbstractControl {
    return this.conversionForm.get("isNewContact");
  }
  get contactNameControl(): AbstractControl {
    return this.conversionForm.get("contactName");
  }
  get contactSurnameControl(): AbstractControl {
    return this.conversionForm.get("contactSurname");
  }
  get contactPositionControl(): AbstractControl {
    return this.conversionForm.get("contactPosition");
  }
  get isExistingContactControl(): AbstractControl {
    return this.conversionForm.get("isExistingContact");
  }
  get contactSearchControl(): AbstractControl {
    return this.conversionForm.get("contactSearch");
  }
  get contactIdControl(): AbstractControl {
    return this.conversionForm.get("contactId");
  }
  get opportunityNameControl(): AbstractControl {
    return this.conversionForm.get("opportunityName");
  }
  get noOpportunityControl(): AbstractControl {
    return this.conversionForm.get("noOpportunity");
  }
  get opportunityProcessControl(): AbstractControl {
    return this.conversionForm.get("opportunityProcessDefinitionId");
  }
  get opportunityStepControl(): AbstractControl {
    return this.conversionForm.get("opportunityStep");
  }
  get opportunityOwnerControl(): AbstractControl {
    return this.conversionForm.get("opportunityOwner");
  }
  get opportunityAmountControl(): AbstractControl {
    return this.conversionForm.get("opportunityAmount");
  }
  get opportunityCurrencyControl(): AbstractControl {
    return this.conversionForm.get("opportunityCurrency");
  }
  get opportunityFinishDateControl(): AbstractControl {
    return this.conversionForm.get("opportunityFinishDate");
  }
  // @formatter:on

  private getCustomCustomerValueValidator(): (
    control: AbstractControl
  ) => ValidationErrors {
    return (control: AbstractControl): ValidationErrors => {
      return !this.selectedCustomerName ||
        control.value === this.selectedCustomerName
        ? null
        : { customCustomerValue: true };
    };
  }

  private getCustomContactValueValidator(): (
    control: AbstractControl
  ) => ValidationErrors {
    return (control: AbstractControl): ValidationErrors => {
      return !this.selectedContactName ||
        control.value === this.selectedContactName
        ? null
        : { customContactValue: true };
    };
  }

  private createConversionForm(): void {
    const loggedUserId = this.getLoggedUser();
    this.conversionForm = this.fb.group({
      isNewCustomer: [true],
      customerName: [
        null,
        [
          Validators.required,
          FormValidators.whitespace,
          Validators.maxLength(200),
        ],
      ],
      customerAddress: [null],
      customerNip: [null, [Validators.required, Validators.maxLength(10)]],
      isExistingCustomer: [false],
      customerSearch: [
        { value: null, disabled: true },
        [Validators.required, this.getCustomCustomerValueValidator()],
      ],
      customerId: [{ value: null, disabled: true }, Validators.required],
      isNewContact: [{ value: true, disabled: true }],
      contactName: [
        null,
        [
          Validators.required,
          FormValidators.whitespace,
          Validators.maxLength(200),
        ],
      ],
      contactSurname: [
        null,
        [
          Validators.required,
          FormValidators.whitespace,
          Validators.maxLength(200),
        ],
      ],
      contactPosition: [
        null,
        [
          Validators.required,
          FormValidators.whitespace,
          Validators.maxLength(100),
        ],
      ],
      isExistingContact: [{ value: false, disabled: true }],
      contactSearch: [
        { value: null, disabled: true },
        [Validators.required, this.getCustomContactValueValidator()],
      ],
      contactId: [{ value: null, disabled: true }, Validators.required],
      opportunityName: [
        null,
        [
          Validators.required,
          FormValidators.whitespace,
          Validators.maxLength(200),
        ],
      ],
      noOpportunity: [false],
      opportunityProcessDefinitionId: [null, Validators.required],
      opportunityStep: [{ value: null, disabled: true }],
      opportunityOwner: [loggedUserId, Validators.required],
      opportunityAmount: [null],
      opportunityCurrency: [null, Validators.required],
      opportunityFinishDate: [null],
    });
  }

  onNewCustomerCheckboxChange($event: MatCheckboxChange): void {
    if ($event.checked) {
      this.enableCreateNewCustomer();
      this.isExistingCustomerControl.patchValue(false);
    } else {
      this.enableSelectExistingCustomer();
      this.isExistingCustomerControl.patchValue(true);
    }
  }

  onExistingCustomerChange($event: MatCheckboxChange): void {
    if ($event.checked) {
      this.enableSelectExistingCustomer();
      this.isNewCustomerControl.patchValue(false);
    } else {
      this.enableCreateNewCustomer();
      this.isNewCustomerControl.patchValue(true);
    }
  }

  private enableSelectExistingCustomer(): void {
    this.customerSearchControl.enable();
    this.customerIdControl.enable();
    this.customerNameControl.disable();
    this.customerAddressControl.disable();
    this.customerNipControl.disable();
    this.isNewContactControl.enable();
    this.isExistingContactControl.enable();
  }

  private enableCreateNewCustomer(): void {
    this.customerSearchControl.disable();
    this.customerIdControl.disable();
    this.customerNameControl.enable();
    this.customerAddressControl.enable();
    this.customerNipControl.enable();
    this.isNewContactControl.disable();
    this.isExistingContactControl.disable();
    this.isExistingContactControl.patchValue(false);
    this.isNewContactControl.patchValue(true);
    this.enableCreateNewContact();
  }

  onNewContactCheckboxChange($event: MatCheckboxChange): void {
    if ($event.checked) {
      this.enableCreateNewContact();
      this.isExistingContactControl.patchValue(false);
    } else {
      this.enableSelectExistingContact();
      this.isExistingContactControl.patchValue(true);
    }
  }

  onExistingContactCheckboxChange($event: MatCheckboxChange): void {
    if ($event.checked) {
      this.enableSelectExistingContact();
      this.isNewContactControl.patchValue(false);
    } else {
      this.enableCreateNewContact();
      this.isNewContactControl.patchValue(true);
    }
  }

  private enableCreateNewContact(): void {
    this.contactNameControl.enable();
    this.contactSurnameControl.enable();
    this.contactPositionControl.enable();
    this.contactSearchControl.disable();
    this.contactIdControl.disable();
  }

  private enableSelectExistingContact(): void {
    this.contactNameControl.disable();
    this.contactSurnameControl.disable();
    this.contactPositionControl.disable();
    this.contactSearchControl.enable();
    this.contactIdControl.enable();
  }

  getLoggedUser(): number {
    return this.loginService.getLoggedAccountId();
  }

  onCustomerSelect(change: MatOptionSelectionChange): void {
    this.customerIdControl.patchValue(+change.source.id);
    this.selectedCustomerName = change.source.value;
  }

  private listenForCustomerSelection(): void {
    this.customerIdControl.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed),
        filter((value) => !isNil(value)),
        distinctUntilChanged(),
        switchMap((customerId: string) =>
          this.contactsService
            .getCustomerContacts(+customerId)
            .pipe(
              catchError(() =>
                of([]).pipe(
                  tap(() =>
                    this.notificationService.notify(
                      "leads.toOpportunity.notification.getContactsError",
                      NotificationType.ERROR
                    )
                  )
                )
              )
            )
        )
      )
      .subscribe((contacts: Contact[]) => {
        this.foundContacts = contacts.map((c) => this.contactToCrmObject(c));
      });
  }

  private autoSelectCustomer(contactId: number): void {
    this.customerService
      .getCustomerByContactId(contactId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((customer) => {
        this.customerIdControl.patchValue(customer.id);
        this.selectedCustomerName = customer.label;
        this.customerSearchControl.patchValue(customer.label);
      });
  }

  private listenForContactSearch(): void {
    this.contactSearchControl.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed),
        debounceTime(250),
        filter(
          () =>
            this.customerIdControl &&
            this.customerIdControl.value &&
            this.customerSearchControl.value === this.selectedCustomerName
        )
      )
      .subscribe((value: string) => {
        this.filteredContacts = this.filterContacts(value);
      });
    this.contactSearchControl.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed),
        debounceTime(250),
        filter(
          () =>
            !(
              this.customerIdControl &&
              this.customerIdControl.value &&
              this.customerSearchControl.value === this.selectedCustomerName
            )
        ),
        filter((input: string) => !!input && input.length >= 3),
        switchMap((input: string) =>
          this.assignToGroupService.searchForType(
            CrmObjectType.contact,
            input,
            [CrmObjectType.customer]
          )
        )
      )
      .subscribe((result: CrmObject[]) => {
        this.filteredContacts = result;
      });
  }

  private filterContacts(value: string): CrmObject[] {
    const filterValue = value ? value.toLowerCase() : "";
    return this.foundContacts.filter((contact) =>
      contact.label.toLowerCase().includes(filterValue)
    );
  }

  onContactSelect(change: MatOptionSelectionChange): void {
    this.contactIdControl.patchValue(+change.source.id);
    this.selectedContactName = change.source.value;
    this.autoSelectCustomer(+change.source.id);
  }

  private convertToOpportunity(): void {
    const leadConversion: LeadConversion = this.conversionForm.getRawValue();
    this.leadService
      .convertToOppOpportunity(this._lead.id, leadConversion)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (result) => {
          this.convertedOpportunity = result;
          this.beforeConversion = false;
          this.leadConverted.emit();
        },
        (err) => this.notifyConversionError(err)
      );
  }

  private notifyConversionError(httpErrorResponse: HttpErrorResponse): void {
    if (httpErrorResponse.status === BAD_REQUEST) {
      this.notificationService.notify(
        `leads.toOpportunity.notification.${httpErrorResponse.error}`,
        NotificationType.ERROR
      );
    } else {
      this.notificationService.notifyError(
        this.conversionErrorTypes,
        NotificationType.ERROR
      );
    }
  }

  private getUsers(): void {
    this.personService
      .getIdNameMap()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (result) => {
          this.userMap = result;
        },
        (err) =>
          this.notificationService.notify(
            "leads.toOpportunity.notification.getUsersError",
            NotificationType.ERROR
          )
      );
  }

  goToOpportunity(): void {
    this.router.navigate([
      `/opportunity/${this.convertedOpportunity.opportunityId}`,
    ]);
  }

  goToLeads(): void {
    this.router.navigate([`/leads`]);
  }

  private listenWhenSliderClosed(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        takeUntil(this.componentDestroyed),
        filter((change) => !change.opened),
        filter((change) => change.key === "LeadDetails.LeadToOpportunitySlider")
      )
      .subscribe(() => {
        this.beforeConversion = true;
        this.createConversionForm();
        this.lead = this._lead;
      });
  }

  private listenWhenSliderOpened(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        takeUntil(this.componentDestroyed),
        filter((change) => change.opened),
        filter((change) => change.key === "LeadDetails.LeadToOpportunitySlider")
      )
      .subscribe(() => {
        this.applyPermissions();
      });
  }

  private applyPermissions(): void {
    if (
      !this.authService.isPermissionPresent(`ContactAddComponent.save`, "w")
    ) {
      this.enableSelectExistingContact();
      this.isExistingContactControl.patchValue(true);
    }

    if (
      !this.authService.isPermissionPresent(`CustomerAddComponent.save`, "w")
    ) {
      this.enableSelectExistingCustomer();
      this.isExistingCustomerControl.patchValue(true);
    }
  }

  private subscribeForCurrencies(): void {
    this.configService
      .getGlobalParamValue("currencies")
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (param) => (this.currencies = param.value.split(";")),
        () =>
          this.notificationService.notify(
            "administration.notifications.getError",
            NotificationType.ERROR
          )
      );
  }

  checkIfNipExistsInDb(event: any): void {
    if (event !== undefined && String(event).length === 10) {
      this.customerService
        .getCustomerByNip(event, null)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((val: string) => {
          return val === "0"
            ? null
            : this.customerNipControl.setErrors({ nipExists: true });
        });
    }
  }

  public contactToCrmObject(
    contact: Contact | { id: number; name: string; surname: string }
  ): CrmObject {
    return {
      id: contact.id,
      type: CrmObjectType.contact,
      label: `${contact.name} ${contact.surname}`,
    };
  }
}
