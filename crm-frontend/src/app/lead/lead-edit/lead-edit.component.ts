import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {LeadFormComponent} from '../lead-form/lead-form.component';
import {SliderService} from '../../shared/slider/slider.service';
import {LeadSave} from '../lead.model';
import {LeadService} from '../lead.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {BAD_REQUEST, NOT_FOUND} from 'http-status-codes';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-lead-edit',
  templateUrl: './lead-edit.component.html',
  styleUrls: ['./lead-edit.component.scss']
})
export class LeadEditComponent implements OnInit {

  @Input() lead: LeadSave;
  @Output() leadEdited: EventEmitter<void> = new EventEmitter();
  readonly editLeadErrorTypes: ErrorTypes = {
    base: 'leads.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'badRequest'
      },
      {
        code: NOT_FOUND,
        text: 'leadNotFound'
      }
    ]
  };
  @ViewChild(LeadFormComponent) private leadFormComponent: LeadFormComponent;

  constructor(private sliderService: SliderService,
              private leadService: LeadService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
  }


  submit(): void {
    if (!this.leadFormComponent.isFormValid()) {
      this.leadFormComponent.markAsTouched();
      return;
    }
    const lead: LeadSave = this.leadFormComponent.getLead();
    this.leadService.updateLead(this.lead.id, lead).subscribe(
      () => {
        this.notifyAddSuccess();
        this.emitContactEdited();
      },
      (err) => this.notifyError(err)
    );
  }

  private notifyAddSuccess(): void {
    this.notificationService.notify('leads.form.notification.editedSuccessfully', NotificationType.SUCCESS);
  }

  private emitContactEdited(): void {
    this.leadEdited.emit();
  }

  private notifyError(err: HttpErrorResponse): void {
    this.notificationService.notifyError(this.editLeadErrorTypes, err.status);
  }
}
