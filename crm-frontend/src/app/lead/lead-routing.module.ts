import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../login/auth/auth-guard.service';
import {LeadListComponent} from './lead-list/lead-list.component';
import {LeadDetailsComponent} from './lead-details/lead-details.component';

const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: LeadListComponent},
  {path: ':id', canActivate: [AuthGuard], component: LeadDetailsComponent},
  {path: ':id/documents', canActivate: [AuthGuard], component: LeadDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeadRoutingModule {
}
