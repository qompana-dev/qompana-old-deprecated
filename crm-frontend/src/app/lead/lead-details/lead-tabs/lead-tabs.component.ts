import { LeadDetails } from './../lead-details.model';

import { Component, Input } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-lead-tabs',
  templateUrl: './lead-tabs.component.html',
  styleUrls: ['./lead-tabs.component.scss']
})
export class LeadTabsComponent {
  @Input() leadId: number = null
  @Input() lead: LeadDetails = null
  @Input() leadChangedSubscription: Subject<void>;
  @Input() objectType = null

  selectedTabIndex = 0;

}
