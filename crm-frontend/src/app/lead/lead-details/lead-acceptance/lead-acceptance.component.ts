import { Component, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { LeadDetails } from './../lead-details.model';
import { LoginService } from './../../../login/login.service';
import { UserNotificationService } from './../../../navigation/user-notification/user-notification.service';
import { ConfirmWithCommentService } from './../../../shared/dialog/confirm-with-comment/confirm-with-comment.service';
import { LeadService } from './../../lead.service';
import { DeleteDialogService } from './../../../shared/dialog/delete/delete-dialog.service';
import { NotificationService, NotificationType } from './../../../shared/notification.service';
import { DeleteDialogData } from './../../../shared/dialog/delete/delete-dialog.component';

@Component({
  selector: 'app-lead-acceptance',
  templateUrl: './lead-acceptance.component.html',
  styleUrls: ['./lead-acceptance.component.scss']
})
export class LeadAcceptanceComponent implements OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();

  loggedAccountId: number;
  notificationId: number;
  _lead: LeadDetails;

  @Input() set lead(lead: LeadDetails) {
    this._lead = lead;
    this.initializeView(lead);
  }
  @Input() activeTask = null;

  private readonly declineDialogData: Partial<DeleteDialogData> = {
    title: 'leads.details.dialog.decline.title',
    description: 'leads.details.dialog.decline.description',
    noteFirst: 'leads.details.dialog.decline.noteFirstPart',
    noteSecond: 'leads.details.dialog.decline.noteSecondPart',
    confirmButton: 'leads.details.dialog.decline.confirm',
    cancelButton: 'leads.details.dialog.decline.cancel',
  };

  private readonly acceptDialogData: Partial<DeleteDialogData> = {
    title: 'leads.details.dialog.accept.title',
    description: 'leads.details.dialog.accept.description',
    noteFirst: 'leads.details.dialog.accept.noteFirstPart',
    noteSecond: 'leads.details.dialog.accept.noteSecondPart',
    confirmButton: 'leads.details.dialog.accept.confirm',
    cancelButton: 'leads.details.dialog.accept.cancel',
  };

  constructor(private notificationService: NotificationService,
              private deleteDialogService: DeleteDialogService,
              private leadService: LeadService,
              private $router: Router,
              private userNotificationService: UserNotificationService,
              private loginService: LoginService,
              private confirmWithCommentService: ConfirmWithCommentService) {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  tryAccept(withComment: boolean): void {
    this.acceptDialogData.numberToDelete = this.activeTask.name;
    this.acceptDialogData.onConfirmClick = (comment) => this.accept(comment);
    if (withComment) {
      this.confirmWithCommentService.open(this.acceptDialogData);
    } else {
      this.deleteDialogService.open(this.acceptDialogData);
    }
  }

  tryDecline(withComment: boolean): void {
    this.declineDialogData.numberToDelete = this.activeTask.name;
    this.declineDialogData.onConfirmClick = (comment) => this.decline(comment);
    if (withComment) {
      this.confirmWithCommentService.open(this.declineDialogData);
    } else {
      this.deleteDialogService.open(this.declineDialogData);
    }
  }


  private accept(comment: string): void {
    this.leadService.accept({ userNotificationId: this.notificationId, comment }).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.notificationService.notify('leads.details.notifications.acceptedTask', NotificationType.SUCCESS);
        this.$router.navigate(['/leads']);
      },
      () => this.notificationService.notify('leads.details.notifications.problemAcceptingTask', NotificationType.ERROR)
    );
  }

  private decline(comment: string): void {
    this.leadService.decline({ userNotificationId: this.notificationId, comment }).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.notificationService.notify('leads.details.notifications.declinedTask', NotificationType.SUCCESS);
        this.$router.navigate(['/leads']);
      },
      () => this.notificationService.notify('leads.details.notifications.problemDecliningTask', NotificationType.ERROR)
    );
  }

  private initializeView(lead: LeadDetails): void {
    this.loggedAccountId = this.loginService.getLoggedAccountId();
    if (lead.acceptanceUserId === this.loggedAccountId) {
      this.userNotificationService.getNotificationId(this.loggedAccountId, 'LEAD_TASK_ACCEPTANCE', lead.processInstanceId)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (notificationId) => {
            this.notificationId = notificationId;
          }
        );
    }
  }
}
