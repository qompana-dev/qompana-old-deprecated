import { DictionaryStoreService } from "./../../dictionary/dictionary-store.service";
import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { LeadService } from "../lead.service";
import {
  ErrorTypes,
  NotificationService,
  NotificationType,
} from "../../shared/notification.service";
import { LeadDetails, Property, Task } from "./lead-details.model";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Subject } from "rxjs";
import { map, takeUntil } from "rxjs/operators";
import { NOT_FOUND } from "http-status-codes";
import {
  CanConvertResponseDto,
  LeadSave,
  LeadPriority,
  LeadScoring,
  PriorityRadioButtons,
} from "../lead.model";
import { SliderComponent } from "../../shared/slider/slider.component";
import { DeleteDialogData } from "../../shared/dialog/delete/delete-dialog.component";
import * as cloneDeep from "lodash/cloneDeep";
import { CrmObjectType } from "../../shared/model/search.model";
import { DeleteDialogService } from "../../shared/dialog/delete/delete-dialog.service";
import { LeadDetailsTabComponent } from "../lead-details-tab/lead-details-tab.component";
import { PersonService } from "../../person/person.service";
import { AuthService } from "src/app/login/auth/auth.service";
import { DictionaryId } from "src/app/dictionary/dictionary.model";

@Component({
  selector: "app-lead-details",
  templateUrl: "./lead-details.component.html",
  styleUrls: ["./lead-details.component.scss"],
})
export class LeadDetailsComponent implements OnInit, OnDestroy {
  @ViewChild("editSlider") editSlider: SliderComponent;
  @ViewChild("leadToOpportunitySlider")
  leadToOpportunitySlider: SliderComponent;
  @ViewChild("leadDetailsTabComponent")
  leadDetailsTabComponent: LeadDetailsTabComponent;
  propertyForm: FormGroup;
  lead: LeadDetails;
  taskOnScreen: Task;
  activeTask: Task;
  leadChangedSubscription: Subject<void> = new Subject();
  private componentDestroyed: Subject<void> = new Subject();
  leadToEdit: LeadSave;
  leadId: number;
  objectType = CrmObjectType;
  canSave: boolean;
  showActionButtons = false;

  selectedPriority: string;
  leadPriority = LeadPriority;
  leadScoring = LeadScoring;

  documentsVisible = false;
  loadingLead = false;
  userMap: Map<number, string>;
  positionMap: Map<number, string>;

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: "leads.form.dialog.delete.fromDetails.title",
    description: "leads.form.dialog.delete.fromDetails.description",
    noteFirst: "leads.form.dialog.delete.fromDetails.noteFirstPart",
    noteSecond: "leads.form.dialog.delete.fromDetails.noteSecondPart",
    confirmButton: "leads.form.dialog.delete.fromDetails.confirm",
    cancelButton: "leads.form.dialog.delete.fromDetails.cancel",
  };

  private readonly leadErrorTypes: ErrorTypes = {
    base: "leads.list.notification.",
    defaultText: "unknownError",
    errors: [
      {
        code: NOT_FOUND,
        text: "leadNotFound",
      },
    ],
  };
  private fragment: string;
  leadConversionSliderTitle: string;

  readonly priorities = PriorityRadioButtons;

  constructor(
    private notificationService: NotificationService,
    private deleteDialogService: DeleteDialogService,
    private activatedRoute: ActivatedRoute,
    private leadService: LeadService,
    private personService: PersonService,
    private fb: FormBuilder,
    private $router: Router,
    private authService: AuthService,
    private dictionariesService: DictionaryStoreService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((params) => {
        this.leadId = +params.id;
        this.documentsVisible = this.$router.url.indexOf("/documents") !== -1;
        this.getLead(this.leadId);
      });
    this.getAccountEmails();
    this.getPositions(this.lead ? [this.lead.position] : [])
    this.activatedRoute.fragment
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((fragment: string) => (this.fragment = fragment));
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }
  private getAccountEmails(): void {
    this.personService.getAndMapAccountEmails(this.componentDestroyed)
      .subscribe(
        (users) => (this.userMap = users),
        () => this.notificationService.notify('contacts.details.notifications.accountEmailsError', NotificationType.ERROR)
      );
  }

  private getPositions(privateWordsIds: number[]): void {
    this.dictionariesService
      .getDictionaryWordsById(DictionaryId.POSITION, privateWordsIds)
      .pipe(
        takeUntil(this.componentDestroyed),
        map(
          (position) =>
            new Map<number, string>(
              position.map((i) => [i.id, i.name] as [number, string])
            )
        )
      )
      .subscribe(
        (position) => {
          this.positionMap = position;
        },
        (err) =>
          this.notificationService.notify(
            "leads.form.notification.positionError",
            NotificationType.ERROR
          )
      );
  }

  getLead(leadId: number): void {
    this.loadingLead = true;
    this.leadService
      .getLeadById(leadId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (lead: LeadDetails) => {
          this.initializeView(lead);
          this.initLeadPriority(lead.priority);
          this.openToOpportunitySliderIfNeeded();
        },
        () =>
          this.notificationService.notify(
            "leads.form.notification.leadNotFound",
            NotificationType.ERROR
          ),
        () => (this.loadingLead = false)
      );
  }

  submit(): void {
    this.leadService
      .postFormProperties(this.lead.processInstanceId, this.propertyForm.value)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          Object.keys(this.propertyForm.controls).forEach((key) =>
            this.updatePropertyValues(key)
          );
        },
        () =>
          this.notificationService.notify(
            "leads.details.notifications.propertyUpdateError",
            NotificationType.ERROR
          )
      );
  }

  initLeadPriority(priority: string): void {
    this.selectedPriority = LeadPriority[priority] || LeadPriority.LOW;
  }

  onPriorityChange(priority: string) {
    this.showActionButtons = true;
    this.selectedPriority = priority;
    this.leadDetailsTabComponent.changePriority(priority);
  }

  handleDetailsChanged(lead: LeadSave): void {
    this.showActionButtons = true;
  }

  discardLeadDetailsChanges(): void {
    this.canSave = false;
    this.showActionButtons = false;
    this.initLeadPriority(this.lead.priority);
    setTimeout(() => (this.canSave = null), 500);
  }

  saveLeadDetailsChanges(): void {
    if (this.leadDetailsTabComponent.leadForm.valid) {
      this.canSave = true;
      this.showActionButtons = false;
      setTimeout(() => (this.canSave = null), 500);
    } else {
      this.leadDetailsTabComponent.leadForm.markAsTouched();
    }
  }

  private updatePropertyValues(key: string): void {
    this.taskOnScreen.properties.find(
      (e) => e.id === key
    ).value = this.propertyForm.controls[key].value;
    this.activeTask.properties.find(
      (e) => e.id === key
    ).value = this.propertyForm.controls[key].value;
  }

  isEmpty(property: Property): boolean {
    const value = this.taskOnScreen.properties.find((e) => e.id === property.id)
      .value;
    return value === null || value === "";
  }

  isFalse(property: Property): boolean {
    return (
      this.propertyForm.get(property.id) &&
      !this.propertyForm.get(property.id).value
    );
  }

  cancelChange(id: string): void {
    this.propertyForm
      .get(id)
      .patchValue(this.taskOnScreen.properties.find((e) => e.id === id).value);
  }

  public edit(leadId: number): void {
    this.leadService
      .getLeadForEdit(leadId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (lead) => {
          this.leadToEdit = lead;
          this.editSlider.open();
        },
        (err) =>
          this.notificationService.notifyError(this.leadErrorTypes, err.status)
      );
  }

  public tryDeleteLead(leadId: number): void {
    this.deleteDialogData.numberToDelete = this.lead.lastName;
    this.deleteDialogData.onConfirmClick = () => this.deleteLead(leadId);
    this.openDeleteDialog();
  }

  public handleLeadEdited(): void {
    this.getLead(this.leadId);
    this.leadDetailsTabComponent.getLead(this.leadId);
    this.editSlider.close();
    this.handleLeadChangedSubscription();
  }

  public handleLeadChangedSubscription(): void {
    this.leadChangedSubscription.next();
  }

  public refresh(): void {
    this.getLead(this.leadId);
    this.leadDetailsTabComponent.getLead(this.leadId);
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private deleteLead(leadId: number): void {
    this.leadService
      .removeLead(leadId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (ok) => {
          this.notificationService.notify(
            "leads.list.notification.deleteLeadSuccess",
            NotificationType.SUCCESS
          );
          this.$router.navigate(["/leads"]);
        },
        (err) =>
          this.notificationService.notifyError(this.leadErrorTypes, err.status)
      );
  }

  private initializeView(lead: LeadDetails): void {
    this.lead = lead;
    for (const task of lead.tasks) {
      if (task.active) {
        this.taskOnScreen = cloneDeep(task);
        this.activeTask = cloneDeep(task);
        break;
      }
    }
    if (this.taskOnScreen) {
      this.createPropertyForm();
      if (!this.lead.acceptanceUserId) {
        this.submit();
      }
    }
  }

  setActiveTask(task: Task) {
    this.activeTask = task;
  }

  private createPropertyForm(): void {
    this.propertyForm = this.fb.group({});
    this.taskOnScreen.properties.forEach((field) => this.addControl(field));
  }

  private addControl(field: Property): void {
    if (field.type !== "BOOLEAN") {
      this.propertyForm.addControl(
        field.id,
        this.fb.control(
          {
            value: field.value,
            disabled:
              this.activeTask.id !== this.taskOnScreen.id ||
              this.lead.acceptanceUserId,
          },
          field.required ? [Validators.required] : []
        )
      );
    } else {
      this.propertyForm.addControl(
        field.id,
        this.fb.control(
          {
            value: field.value === "true",
            disabled:
              this.activeTask.id !== this.taskOnScreen.id ||
              this.lead.acceptanceUserId,
          },
          field.required ? [Validators.requiredTrue] : []
        )
      );
    }
  }

  openLeadDocumentsView(): void {
    this.$router.navigate(["/leads", this.leadId, "documents"]);
  }

  isLastTaskCompleted(): boolean {
    const lastTask: Task =
      this.lead &&
      this.lead.tasks &&
      this.lead.tasks[this.lead.tasks.length - 1];
    return lastTask && lastTask.active && !lastTask.name;
  }

  private openToOpportunitySliderIfNeeded(): void {
    if (
      this.fragment === "to-opportunity" &&
      this.isLastTaskCompleted() &&
      !this.isConvertedToOpportunity()
    ) {
      this.openLeadConversionSlider();
    }
  }

  public openLeadConversionSlider(): void {
    this.leadService
      .canConvert(this.leadId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((response: CanConvertResponseDto) => {
        if (response.canConvert) {
          this.leadConversionSliderTitle = "leads.toOpportunity.title";
          this.leadToOpportunitySlider.open();
        } else {
          this.showOpenConversionPanelError(response);
        }
      });
  }

  private showOpenConversionPanelError(
    canConvertResponseDto: CanConvertResponseDto
  ): void {
    const {
      tasksRequiringAcceptance,
      tasksRequiringFillingProperties,
    } = canConvertResponseDto;
    if (tasksRequiringAcceptance && tasksRequiringAcceptance.length > 0) {
      const props = { tasks: tasksRequiringAcceptance.join(", ") };
      const plural = tasksRequiringAcceptance.length > 1 ? "s" : "";
      const key = `leads.details.notifications.task${plural}RequiringAcceptance`;
      this.notificationService.notify(key, NotificationType.ERROR, props);
    }
    if (
      tasksRequiringFillingProperties &&
      tasksRequiringFillingProperties.length > 0
    ) {
      const props = { tasks: tasksRequiringFillingProperties.join(", ") };
      const plural = tasksRequiringFillingProperties.length > 1 ? "s" : "";
      const key = `leads.details.notifications.task${plural}RequiringFillingProperties`;
      this.notificationService.notify(key, NotificationType.ERROR, props);
    }
  }

  onLeadConverted(): void {
    this.leadConversionSliderTitle = "leads.afterConversion.title";
    this.markLeadAsConverted();
    this.getLead(this.leadId);
  }

  private isConvertedToOpportunity(): boolean {
    return this.lead && this.lead.convertedToOpportunity;
  }

  private markLeadAsConverted(): void {
    if (this.lead) {
      this.lead.convertedToOpportunity = true;
    }
  }

  public taskClicked(task: Task): void {
    if (
      task.id !== this.taskOnScreen.id &&
      this.authService.isPermissionPresent(
        "LeadDetailsComponent.previewTask",
        "w"
      )
    ) {
      if (task.active) {
        this.taskOnScreen = cloneDeep(this.activeTask);
        this.createPropertyForm();
      } else {
        this.leadService
          .getTaskForReadOnly(this.lead.processInstanceId, task.id, !task.past)
          .pipe(takeUntil(this.componentDestroyed))
          .subscribe((pastTask) => {
            this.taskOnScreen = pastTask;
            this.createPropertyForm();
          });
      }
    }
  }
}
