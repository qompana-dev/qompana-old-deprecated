import { Router } from '@angular/router';
import { SelectItemDialogService } from './../../../shared/dialog/select-item-dialog/select-item-dialog.service';
import { CloseLeadResult, LeadRejectionReason } from './../../lead.model';
import { SelectItemDialogData } from './../../../shared/dialog/select-item-dialog/select-item-dialog.component';
import {
  NotificationService,
  NotificationType,
} from './../../../shared/notification.service';
import { LeadService } from './../../lead.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DeleteDialogService } from './../../../shared/dialog/delete/delete-dialog.service';
import { DeleteDialogData } from './../../../shared/dialog/delete/delete-dialog.component';
import { Task, LeadDetails } from './../lead-details.model';
import { TaskTooltipDataFactory } from './../../../shared/task-tooltip/task-tooltip.model';
import { BpmnUtil } from './../../../bpmn/bpmn.util';
import { StatusAcceptance } from './../../../bpmn/bpmn.model';
import {
  Component,
  Input,
  OnDestroy,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'app-lead-tasks',
  templateUrl: './lead-tasks.component.html',
  styleUrls: ['./lead-tasks.component.scss'],
})
export class LeadTasksComponent implements OnDestroy {
  @Input() lead: LeadDetails = null;
  @Input() leadId: number = null;
  @Input() activeTask: Task = null;
  @Input() taskOnScreen: Task = null;
  @Input() propertyForm = null;
  @Output() getLead = new EventEmitter<string | number>();
  @Output() openLeadConversionSlider = new EventEmitter<void>();
  @Output() taskClicked = new EventEmitter<Task>();

  private componentDestroyed: Subject<void> = new Subject();
  scrollAtTheEnd: boolean;
  bpmnUtil = BpmnUtil;

  private readonly goBackDialogData: Partial<DeleteDialogData> = {
    title: 'leads.form.dialog.goBack.title',
    description: 'leads.form.dialog.goBack.description',
    noteFirst: 'leads.form.dialog.goBack.noteFirstPart',
    noteSecond: 'leads.form.dialog.goBack.noteSecondPart',
    confirmButton: 'leads.form.dialog.goBack.confirm',
    cancelButton: 'leads.form.dialog.goBack.cancel',
  };

  private readonly closeLeadDialogData: Partial<SelectItemDialogData> = {
    title: 'leads.form.dialog.closeLead.title',
    items: [
      {
        title: 'leads.form.dialog.closeLead.item.closed',
        value: CloseLeadResult.CLOSED,
      },
      {
        title: 'leads.form.dialog.closeLead.item.archived',
        value: CloseLeadResult.ARCHIVED,
      },
    ],
    propertyVisibleOnList: 'title',
    confirmButton: 'leads.form.dialog.closeLead.confirm',
    placeholder: 'leads.form.dialog.closeLead.placeholder',
    cancelButton: 'leads.form.dialog.closeLead.cancel',
  };

  private rejectionReasonItems: any[] = [
    {
      title: 'leads.form.dialog.rejectionReason.item.none',
      value: LeadRejectionReason.NONE,
    },
    {
      title: 'leads.form.dialog.rejectionReason.item.lost',
      value: LeadRejectionReason.LOST,
    },
    {
      title: 'leads.form.dialog.rejectionReason.item.noBudget',
      value: LeadRejectionReason.NO_BUDGET,
    },
    {
      title: 'leads.form.dialog.rejectionReason.item.noDecision',
      value: LeadRejectionReason.NO_DECISION,
    },
    {
      title: 'leads.form.dialog.rejectionReason.item.other',
      value: LeadRejectionReason.OTHER,
    },
  ];

  private readonly rejectionReasonDialogData: Partial<SelectItemDialogData> = {
    title: 'leads.form.dialog.rejectionReason.title',
    items: this.rejectionReasonItems,
    propertyVisibleOnList: 'title',
    confirmButton: 'leads.form.dialog.rejectionReason.confirm',
    placeholder: 'leads.form.dialog.rejectionReason.placeholder',
    cancelButton: 'leads.form.dialog.rejectionReason.cancel',
  };

  private readonly closeDialogData: Partial<DeleteDialogData> = {
    title: 'leads.form.dialog.close.title',
    description: 'leads.form.dialog.close.description',
    noteFirst: 'leads.form.dialog.close.noteFirstPart',
    noteSecond: 'leads.form.dialog.close.noteSecondPart',
    confirmButton: 'leads.form.dialog.close.confirm',
    cancelButton: 'leads.form.dialog.close.cancel',
  };

  constructor(
    private deleteDialogService: DeleteDialogService,
    private leadService: LeadService,
    private notificationService: NotificationService,
    private selectItemDialogService: SelectItemDialogService,
    private $router: Router
  ) {}

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  isScrollVisible(tasksContainer: any): boolean {
    return tasksContainer.scrollWidth !== tasksContainer.clientWidth;
  }

  onScroll(tasksContainer: any): void {
    this.scrollAtTheEnd =
      tasksContainer.clientWidth + tasksContainer.scrollLeft >=
      tasksContainer.scrollWidth - 5;
  }

  isTaskError(task: Task): boolean {
    return task.statusAcceptance === StatusAcceptance.DECLINED;
  }

  isAlertTask(task: Task): boolean {
    const isNotApproved = !!this.lead.acceptanceUserId && task.active;
    return isNotApproved;
  }

  isTaskButtonDisabled(): boolean {
    if (this.lead) {
      const disabled =
        this.lead.convertedToOpportunity ||
        !this.taskOnScreen.active ||
        this.taskOnScreen.past ||
        this.lead.acceptanceUserId;
      return !!disabled;
    }
    return true;
  }

  isTasksCompleted(): boolean {
    if (this.lead) {
      const lastTaskIndex = this.lead.tasks.length - 1;
      const lastTask = this.lead.tasks[lastTaskIndex];
      const isActive = lastTask.active;
      return isActive;
    }
    return true;
  }

  isConvertedToOpportunity(): boolean {
    return this.lead && this.lead.convertedToOpportunity;
  }

  createTaskTooltipData(documentation: string) {
    return TaskTooltipDataFactory.create(documentation);
  }

  public tryGoPrevious(): void {
    this.goBackDialogData.onConfirmClick = () => this.goPrevious();
    this.deleteDialogService.open(this.goBackDialogData);
  }

  tryCloseLead(): void {
    this.closeDialogData.numberToDelete = this.lead.lastName;

    this.closeLeadDialogData.onConfirmClick = (result: any) => {
      if (result.value === CloseLeadResult.ARCHIVED) {
        this.rejectionReasonDialogData.onConfirmClick = (
          rejectionResponse: any
        ) => {
          this.closeLead(result.value, rejectionResponse.value);
        };
        this.selectItemDialogService.open(this.rejectionReasonDialogData);
      } else {
        this.closeLead(result.value, undefined);
      }
    };
    this.selectItemDialogService.open(this.closeLeadDialogData);
    this.openCloseDialog();
  }

  closeLead(closeResult: string, closeReason: string): void {
    this.leadService
      .closeLead(this.leadId, closeResult, closeReason)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (result) => {
          this.notificationService.notify(
            'leads.list.notification.closeLeadSuccess',
            NotificationType.SUCCESS
          );
          this.$router.navigate(['/leads']);
        },
        () =>
          this.notificationService.notify(
            'leads.list.notification.closeLeadError',
            NotificationType.ERROR
          )
      );
  }

  private openCloseDialog(): void {
    this.deleteDialogService.open(this.closeDialogData);
  }

  private goPrevious(): void {
    const activeTask = this.getActiveTask();
    if (activeTask) {
      this.leadService
        .goBack(this.lead.processInstanceId, 1)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => this.handleSuccessfulGoBack(),
          () =>
            this.notificationService.notify(
              'leads.details.notifications.noTaskFound',
              NotificationType.ERROR
            )
        );
    }
  }

  private getActiveTask(): Task {
    if (!this.lead || !this.lead.tasks) {
      console.error('tasks not found');
      return undefined;
    }
    return this.lead.tasks.find((task) => task.active);
  }

  private handleSuccessfulGoBack(): void {
    this.getLead.emit(this.leadId);
    this.notificationService.notify(
      'leads.details.notifications.successfulGoBack',
      NotificationType.SUCCESS
    );
  }

  completeTask(): void {
    if (this.taskOnScreen.id !== this.activeTask.id) {
      this.notificationService.notify(
        'leads.details.notifications.cannotCompleteTaskFromAnotherTask',
        NotificationType.ERROR
      );
    } else {
      if (this.propertyForm.valid || this.taskOnScreen.isInformationTask) {
        this.leadService
          .completeTask(this.lead.processInstanceId)
          .pipe(takeUntil(this.componentDestroyed))
          .subscribe(
            (response) => this.handleTaskCompletion(response.status),
            () =>
              this.notificationService.notify(
                'leads.details.notifications.noTaskFound',
                NotificationType.ERROR
              )
          );
      } else {
        for (const property of this.taskOnScreen.properties) {
          if (this.propertyForm.get(property.id).hasError('required')) {
            this.propertyForm.get(property.id).markAsTouched();
          }
        }
        this.notificationService.notify(
          'leads.details.notifications.requiredPropertiesNotFilled',
          NotificationType.ERROR
        );
      }
    }
  }

  private handleTaskCompletion(status: number): void {
    this.getLead.emit(this.leadId);
    if (status === 204) {
      this.notificationService.notify(
        'leads.details.notifications.taskCompleted',
        NotificationType.SUCCESS
      );
    } else if (status === 206) {
      this.notificationService.notify(
        'leads.details.notifications.taskWaitingForAcceptance',
        NotificationType.SUCCESS
      );
    }
  }

  openConversionSlider() {
    this.openLeadConversionSlider.emit();
  }

  taskSelect(task: Task) {
    if (!task.isInformationTask) {
      this.taskClicked.emit(task);
    }
  }
}
