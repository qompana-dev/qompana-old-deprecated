import { StatusAcceptance } from './../../bpmn/bpmn.model';
import { LeadRejectionReason } from '../lead.model';

export class LeadDetails {
  id: number;
  processInstanceId: string;
  firstName: string;
  lastName: string;
  position: number;
  companyName: string;
  phoneNumber: string;
  email: string;
  priority: string;
  scoring: string;
  created: string;
  bgColor?: string;
  textColor?: string;
  salesOpportunity: string;
  acceptanceUserId: number;
  tasks: Task[];
  convertedToOpportunity: boolean;
  avatar?: string;
  rejectionReason: LeadRejectionReason;
  closeResult: string;
  sourceOfAcquisition: number;
  subSourceOfAcquisition: number;
}

export class Task {
  id: string;
  name: string;
  documentation: string;
  active: boolean;
  past: boolean;
  percentage: number;
  isInformationTask?: boolean;
  properties: Property[];
  statusAcceptance: StatusAcceptance | null;
};

export class Property {
  id: string;
  type: string;
  required: boolean;
  value: string;
  possibleValues: PossibleValue[];
}

export class PossibleValue {
  id: string;
  name: string;
}
