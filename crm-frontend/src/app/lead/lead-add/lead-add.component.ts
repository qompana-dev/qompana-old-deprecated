import {Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {LeadFormComponent} from '../lead-form/lead-form.component';
import {LeadSave} from '../lead.model';
import {LeadService} from '../lead.service';
import {SliderService} from '../../shared/slider/slider.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {HttpErrorResponse} from '@angular/common/http';
import {BAD_REQUEST} from 'http-status-codes';
import {CrmObject} from '../../shared/model/search.model';
import {DeleteDialogData} from '../../shared/dialog/delete/delete-dialog.component';
import {TranslateService} from '@ngx-translate/core';
import {DeleteDialogService} from '../../shared/dialog/delete/delete-dialog.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-lead-add',
  templateUrl: './lead-add.component.html',
  styleUrls: ['./lead-add.component.scss']
})
export class LeadAddComponent {

  @Output() leadAdded: EventEmitter<void> = new EventEmitter();
  readonly addLeadErrorTypes: ErrorTypes = {
    base: 'leads.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'badRequest'
      }
    ]
  };
  saveInProgress = false;

  private readonly similarObjectsDialogData: Partial<DeleteDialogData> = {
    title: 'leads.form.dialog.similarObjects.title',
    description: 'leads.form.dialog.similarObjects.description',
    noteFirst: 'leads.form.dialog.similarObjects.noteFirstPart',
    noteSecond: 'leads.form.dialog.similarObjects.noteSecondPart',
    confirmButton: 'leads.form.dialog.similarObjects.confirm',
    cancelButton: 'leads.form.dialog.similarObjects.cancel',
  };

  @ViewChild(LeadFormComponent) private leadFormComponent: LeadFormComponent;

  constructor(private leadService: LeadService,
              private sliderService: SliderService,
              private translateService: TranslateService,
              private deleteDialogService: DeleteDialogService,
              private notificationService: NotificationService) {
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
  }

  submit(): void {
    if (!this.leadFormComponent.isFormValid()) {
      this.leadFormComponent.markAsTouched();
      return;
    }
    if (!this.saveInProgress) {
      const lead: LeadSave = this.leadFormComponent.getLead();
      this.leadService.searchSimilarObjects({
        firstName: lead.firstName,
        lastName: lead.lastName,
        email: lead.email,
        phone: lead.phone,
        companyName: lead.companyName,
        wwwAddress: lead.wwwAddress,
        streetAndNumber: lead.streetAndNumber,
      }).subscribe((objects: CrmObject[]) => {
        if (objects && objects.length > 0) {
          this.similarObjectsDialogData.numberToDelete =
            objects.map(c => {
              const key = 'leads.form.dialog.similarObjects.type.' + c.type;
              const type = this.translateService.instant(key);
              return `${type}: ${c.label}`;
            }).join(', ');
          this.similarObjectsDialogData.onConfirmClick = () => this.save();
          this.deleteDialogService.openWithoutHeight(this.similarObjectsDialogData);
        } else {
          this.save();
        }
      }, (err) => this.notifyError(err));
    }
  }

  private save(): void {
    this.saveInProgress = true;
    const lead: LeadSave = this.leadFormComponent.getLead();
    this.leadService.createLead(lead)
        .subscribe(
          () => {
            this.saveInProgress = false;
            this.notifyAddSuccess();
            this.emitContactAdded();
          },
          (err) => this.notifyError(err)
        );
  }

  private notifyAddSuccess(): void {
    this.notificationService.notify('leads.form.notification.addedSuccessfully', NotificationType.SUCCESS);
  }

  private emitContactAdded(): void {
    this.leadAdded.emit();
  }

  private notifyError(err: HttpErrorResponse): void {
    this.saveInProgress = false;
    this.notificationService.notifyError(this.addLeadErrorTypes, err.status);
  }
}
