import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { LeadService } from '../lead.service';
import { takeUntil, debounceTime, switchMap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { FoundPlace, LeadSave, LeadPriority, LeadScoring, PhotonResponcePlace } from '../lead.model';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  ErrorTypes,
  NotificationService,
  NotificationType,
} from '../../shared/notification.service';
import { MapService } from '../../map/map.service';
import { SliderComponent } from '../../shared/slider/slider.component';
import { Address } from '../../map/map.model';
import { MatSelect } from '@angular/material/select';
import { LngLatLike } from 'mapbox-gl';
import { MapViewMarker } from '../../map/map-view/map-view.model';
import { FormUtil } from '../../shared/form.util';
import * as cloneDeep from 'lodash/cloneDeep';
import { FormValidators } from '../../shared/form/form-validators';
import {
  Dictionary,
  DictionaryId,
  DictionaryWord,
  SaveDictionaryWord,
} from '../../dictionary/dictionary.model';
import { DictionaryAddWordComponent } from '../../dictionary/dictionary-add-word/dictionary-add-word.component';
import { AccountEmail } from 'src/app/person/person.model';
import { PersonService } from 'src/app/person/person.service';
import { DictionaryStoreService } from 'src/app/dictionary/dictionary-store.service';
import {Customer} from '../../customer/customer.model';
import {CustomerService} from '../../customer/customer.service';
import {BAD_REQUEST, GONE, NOT_FOUND} from 'http-status-codes';
import { add } from 'ngx-bootstrap/chronos/public_api';
import {PhoneService} from '../../shared/phone-fields/phone.service';

@Component({
  selector: 'app-lead-details-tab',
  templateUrl: './lead-details-tab.component.html',
  styleUrls: ['./lead-details-tab.component.scss'],
})
export class LeadDetailsTabComponent implements OnInit {
  address: Address;
  extents: {
    city: number[],
    country: number[],
  };
  foundPlaces: FoundPlace[] = [];
  leadForm: FormGroup;
  @Input() leadId: number;
  @Input() isTaskProperties = false;
  @ViewChild('placeSelect') placeSelect: MatSelect;
  @ViewChild('mapSlider') mapSlider: SliderComponent;

  center: LngLatLike = undefined;
  showMap = false;
  markers: MapViewMarker[] = [];
  lead: LeadSave;

  formControlFocusedKey: string;
  formControlFocusedValue: string;
  formChanged = false;
  baseLead: LeadSave = {} as LeadSave;
  @Output() leadChanged: EventEmitter<LeadSave> = new EventEmitter<LeadSave>();
  @Output() leadEdited: EventEmitter<void> = new EventEmitter<void>();

  readonly undefinedCategoryId = -1000;
  dictionaryId = DictionaryId;
  parentWordIdForCreation: number = null;
  sourceOfAcquisitionCreation: Subject<void> = new Subject();
  allSourceOfAcquisition: DictionaryWord[];
  allSubSourceOfAcquisition: DictionaryWord[];
  allSubjectOfInterest: DictionaryWord[];
  allIndustry: DictionaryWord[];
  allPosition: DictionaryWord[];
  accountEmails: AccountEmail[];

  gusErrors: ErrorTypes = {
    base: 'customer.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'customerNotFoundInGus',
      },
      {
        code: BAD_REQUEST,
        text: 'badRequest',
      },
      {
        code: GONE,
        text: 'errorInGus',
      },
    ],
  };

  @ViewChild('sourceOfAcquisitionSelect') sourceOfAcquisitionSelect: MatSelect;
  @ViewChild('subSourceOfAcquisitionSelect')
  subSourceOfAcquisitionSelect: MatSelect;

  @ViewChild('addSourceOfAcquisitionSlider')
  addSourceOfAcquisitionSlider: SliderComponent;
  @ViewChild('addSourceOfAcquisitionWord')
  addSourceOfAcquisitionWord: DictionaryAddWordComponent;

  @ViewChild('addSubSourceOfAcquisitionSlider')
  addSubSourceOfAcquisitionSlider: SliderComponent;
  @ViewChild('addSubSourceOfAcquisitionWord')
  addSubSourceOfAcquisitionWord: DictionaryAddWordComponent;

  @ViewChild('addSubjectOfInterestSlider')
  addSubjectOfInterestSlider: SliderComponent;
  @ViewChild('addSubjectOfInterestWord')
  addSubjectOfInterestWord: DictionaryAddWordComponent;

  @ViewChild('addPositionSlider')
  addPositionSlider: SliderComponent;
  @ViewChild('addPositionWord')
  addPositionWord: DictionaryAddWordComponent;

  @ViewChild('addIndustrySlider')
  addIndustrySlider: SliderComponent;
  @ViewChild('addIndustryWord')
  addIndustryWord: DictionaryAddWordComponent;

  private addAdditionalPhoneNumbersSubject: Subject<void> = new Subject<void>();
  public addAdditionalPhoneNumbers$: Observable<void> = this.addAdditionalPhoneNumbersSubject.asObservable();

  @Input() set canSave(canSave: boolean) {
    if (canSave === true) {
      this.saveChanges();
    } else if (canSave === false) {
      this.revertChanges();
    }
  }

  @Output() refresh: EventEmitter<void> = new EventEmitter<void>();

  private componentDestroyed: Subject<void> = new Subject();

  constructor(
    private fb: FormBuilder,
    private leadService: LeadService,
    private notificationService: NotificationService,
    private personService: PersonService,
    private mapService: MapService,
    private customerService: CustomerService,
    private dictionariesService: DictionaryStoreService,
    public phoneService: PhoneService
  ) {
    this.createContactForm();
    this.getAccountEmails();
    this.getSourcesOfAcquisitionForMe([]);
    this.getSubjectsOfInterestForMe([]);
    this.getIndustryForMe([]);
    this.getPositionForMe([]);
  }

  // @formatter:off
  get firstName(): AbstractControl {
    return this.leadForm.get('firstName');
  }

  get lastName(): AbstractControl {
    return this.leadForm.get('lastName');
  }

  get position(): AbstractControl {
    return this.leadForm.get('position');
  }

  get email(): AbstractControl {
    return this.leadForm.get('email');
  }

  get phone(): AbstractControl {
    return this.leadForm.get('phone');
  }

  get companyNip(): AbstractControl {
    return this.leadForm.get('companyNip');
  }

  get companyName(): AbstractControl {
    return this.leadForm.get('companyName');
  }

  get industry(): AbstractControl {
    return this.leadForm.get('industry');
  }

  get sourceOfAcquisition(): AbstractControl {
    return this.leadForm.get('sourceOfAcquisition');
  }

  get subSourceOfAcquisition(): AbstractControl {
    return this.leadForm.get('subSourceOfAcquisition');
  }

  get subjectOfInterest(): AbstractControl {
    return this.leadForm.get('subjectOfInterest');
  }

  get interestedInCooperation(): AbstractControl {
    return this.leadForm.get('interestedInCooperation');
  }

  get salesOpportunity(): AbstractControl {
    return this.leadForm.get('salesOpportunity');
  }

  get wwwAddress(): AbstractControl {
    return this.leadForm.get('wwwAddress');
  }

  get streetAndNumber(): AbstractControl {
    return this.leadForm.get('streetAndNumber');
  }

  get city(): AbstractControl {
    return this.leadForm.get('city');
  }

  get postalCode(): AbstractControl {
    return this.leadForm.get('postalCode');
  }

  get province(): AbstractControl {
    return this.leadForm.get('province');
  }

  get country(): AbstractControl {
    return this.leadForm.get('country');
  }

  get notes(): AbstractControl {
    return this.leadForm.get('notes');
  }

  get priority(): AbstractControl {
    return this.leadForm.get('priority');
  }

  get leadKeeperId(): AbstractControl {
    return this.leadForm.get('leadKeeperId');
  }

  get authKey(): string {
    return 'LeadEditComponent.';
  }

  // @formatter:on

  ngOnInit(): void {
    this.getLead(this.leadId);
    this.subscribeForPlaceSearch();
  }

  getLead(leadId: number): void {
    this.leadService
      .getLeadForEdit(leadId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((lead: LeadSave) => {
        this.lead = lead;
        this.leadForm.reset(lead);
        this.sanitizeFrom();
        this.baseLead = cloneDeep(lead);

        const privateWordsIds: number[] = [];
        privateWordsIds.push(this.lead.sourceOfAcquisition);
        privateWordsIds.push(this.lead.subSourceOfAcquisition);
        this.getSourcesOfAcquisitionForMe(privateWordsIds);
        const subjectOfInterestPrivateWordsIds: number[] = [];
        subjectOfInterestPrivateWordsIds.push(this.lead.subjectOfInterest);
        this.getSubjectsOfInterestForMe(subjectOfInterestPrivateWordsIds);
        const industryWordsIds: number[] = [];
        industryWordsIds.push(this.lead.industry);
        this.getIndustryForMe(industryWordsIds);

        const positionWordsIds: number[] = [];
        positionWordsIds.push(this.lead.position);
        this.getPositionForMe(positionWordsIds);

        this.initAddress();
      });
  }

  private createContactForm(): void {
    this.leadForm = this.fb.group({
      firstName: [null, Validators.maxLength(200)],
      lastName: [
        null,
        [
          Validators.required,
          FormValidators.whitespace,
          Validators.maxLength(200),
        ],
      ],
      email: [null, [Validators.email, Validators.maxLength(200)]],
      phone: [null, Validators.maxLength(128)],
      additionalPhones: [[]],
      companyNip: [null],
      companyName: [null, Validators.maxLength(300)],
      leadKeeperId: [null],
      industry: [this.undefinedCategoryId],
      position: [this.undefinedCategoryId],
      sourceOfAcquisition: [this.undefinedCategoryId],
      subSourceOfAcquisition: [this.undefinedCategoryId],
      subjectOfInterest: [this.undefinedCategoryId],
      interestedInCooperation: [false],
      salesOpportunity: [null, Validators.maxLength(200)],
      wwwAddress: [null, Validators.maxLength(200)],
      streetAndNumber: [null, Validators.maxLength(300)],
      city: [null, Validators.maxLength(200)],
      postalCode: [null, Validators.maxLength(20)],
      province: [null, Validators.maxLength(200)],
      country: [null, Validators.maxLength(200)],
      notes: [null, Validators.maxLength(1000)],
    });
  }

  private getAccountEmails(): void {
    this.personService
      .getAccountEmails()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (emails) => (this.accountEmails = emails),
        (err) =>
          this.notificationService.notify(
            'leads.form.notification.accountEmailsError',
            NotificationType.ERROR
          )
      );
  }

  public showOnMap($event): void {
    $event.stopPropagation();
    const address: Address = {
      street: this.streetAndNumber.value,
      city: this.city.value,
      state: this.province.value,
      zip: this.postalCode.value,
      country: this.country.value,
    };
    this.mapService
      .getCoordinatesByAddress(address)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (places) => {
          this.handleFoundPlaces(places);
        },
        (err) => this.mapService.notifyPlaceNotFound()
      );
  }

  private handleFoundPlaces(places: FoundPlace[]): void {
    this.foundPlaces = places;
    if (places && places.length) {
      this.showAndUpdateForm(places[0]);
    } else {
      this.mapService.notifyPlaceNotFound();
    }
  }

  showAndUpdateForm(place: FoundPlace): void {
    this.showPlace(place);
    this.updateAddressInForm(place);
  }

  private initAddress(): void {
    const address: Address = {
      street: this.streetAndNumber.value,
      city: this.city.value,
      state: this.province.value,
      zip: this.postalCode.value,
      country: this.country.value,
    };

    this.mapService.getCoordinatesByAddress(address, 'en')
      .subscribe(places => {
        if (places && places.length) {
          const foundAddress = places[0].address;
          this.address = {
            street: foundAddress.road,
            city: foundAddress.city,
            country: foundAddress.country,
            state: foundAddress.state,
            zip: foundAddress.postcode,
          };

        }
      });
  }

  private showPlace(place: FoundPlace): void {
    this.center = this.mapService.getCenter(place);
    this.markers = this.mapService.getSingleMarkerFromFoundPlace(place, this.updateAddressInForm.bind(this));
    this.mapSlider.open();
    setTimeout(() => (this.showMap = true), 500);
  }

  private updateAddressInForm(place: FoundPlace): void {
    if (['house', 'street', 'city', 'country'].includes(place.type)) {
      this.mapService.getAddressDetails(place)
      .subscribe(place => {
        this.patchAddressForm(place.address);
      });
    }

    if (place.type === 'city' || place.type === 'country') {
      this.address = {
        street: place.street,
        city: place.type === 'city' ? place.name : place.city,
        state: place.state,
        zip: place.postcode,
        country: place.type === 'country' ? place.name : place.country,
      };

      this.extents = {
        ...this.extents,
        [place.type]: place.extent,
      };
    }

    this.patchAddressForm(place.address || place);
    this.leadChanged.emit(this.lead);
  }

  patchAddressForm(address) {
    this.streetAndNumber.patchValue(this.getStreetAndNumberStringValue(address.road, address.house_number));
    this.city.patchValue(address.city || address.town || address.county);
    this.postalCode.patchValue(address.postcode);
    this.province.patchValue(address.state);
    this.country.patchValue(address.country);
  }

  emptyIfNullOrUndefined(value): any {
    if (value === null || value === undefined) {
      return '';
    } else {
      return value;
    }
  }

  private getStreetAndNumberStringValue(road: string, house_number: string): string {
    if ((road === null || road === undefined) && (house_number === null || house_number === undefined)) {
      return null;
    } else {
      return `${this.emptyIfNullOrUndefined(road)} ${this.emptyIfNullOrUndefined(house_number)}`;
    }
  }

  private subscribeForPlaceSearch(): void {
    const subscribeFor = (observable: Observable<any>, type: string) => {
      observable.pipe(
        takeUntil(this.componentDestroyed),
        debounceTime(250),
        switchMap(((input: string) => {
          let extentType = null;

          if (this.address && this.extents) {
            if (type === 'street' && this.address.city) { extentType = 'city'; }
            else if (type === 'street' && this.address.country) { extentType = 'country'; }

            if (type === 'city' && this.address.country) { extentType = 'country'; }
          }

          return this.mapService.getCoordsAutocomplete(input, type, this.address, extentType && this.extents[extentType]);
        }
        ))).subscribe(
      (places: PhotonResponcePlace[]) => {
        this.foundPlaces = places
          .map(place => {
            place.properties.display_name = this.getDisplayNameForPlace(place.properties);
            return place.properties;
          });
      });
    };

    subscribeFor(this.city.valueChanges, 'city');
    subscribeFor(this.streetAndNumber.valueChanges, 'street');
    subscribeFor(this.country.valueChanges, 'country');
  }

  getDisplayNameForPlace(properties: FoundPlace): string {
    const propertiesForDescription = ['name', 'street', 'housenumber', 'city', 'state', 'country', 'postcode'];
    let display_name = '';

    propertiesForDescription.forEach(key => {
      const value = properties[key];
      if (value) { display_name += `${display_name && ', '}${value}`; }
    });

    return display_name;
  }

  formControlFocusIn(formControlName: string): void {
    this.formControlFocusedKey = cloneDeep(formControlName);
    this.formControlFocusedValue = cloneDeep(
      this.leadForm.get(formControlName).value
    );
  }

  formControlFocusOut(newValue?: string): void {
    if (this.formControlFocusedKey) {
      const control = this.leadForm.get(this.formControlFocusedKey);
      const formValue = !newValue ? control.value : newValue;
      control.setValue(formValue);
      this.lead[this.formControlFocusedKey] = formValue;
      const formChanged = formValue !== this.formControlFocusedValue;
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
      if (formChanged) {
        this.leadChanged.emit(this.lead);
      }
    }
  }

  // TODO(ekon): may be improved or merged with formControlFocusOut
  formControlFocusOutAdditionalPhones(newValue?: string): void {
    if (this.formControlFocusedKey) {
      const formValue = this.leadForm.get(this.formControlFocusedKey).value;
      const formChanged =
        JSON.stringify(formValue) !==
        JSON.stringify(this.formControlFocusedValue);
      if (formChanged) {
        this.lead[this.formControlFocusedKey] = formValue;
        this.leadChanged.emit(this.lead);
      }
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
    }
  }

  formControlEsc(): void {
    if (this.formControlFocusedKey) {
      this.leadForm
        .get(this.formControlFocusedKey)
        .patchValue(this.formControlFocusedValue);
    }
  }

  private saveChanges(): void {
    if (this.leadForm.valid) {
      const lead: LeadSave = {
        ...this.lead,
        ...this.leadForm.getRawValue(),
        sourceOfAcquisition: this.lead.sourceOfAcquisition,
        subSourceOfAcquisition: this.lead.subSourceOfAcquisition,
        subjectOfInterest: this.lead.subjectOfInterest,
        industry: this.lead.industry,
        position: this.lead.position,
      };
      this.leadService
        .updateLead(this.leadId, lead)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => {
            this.baseLead = cloneDeep(this.lead);
            this.notificationService.notify(
              'leads.form.notification.editedSuccessfully',
              NotificationType.SUCCESS
            );

            const privateWordsIds: number[] = [];
            privateWordsIds.push(this.lead.sourceOfAcquisition);
            privateWordsIds.push(this.lead.subSourceOfAcquisition);
            this.getSourcesOfAcquisitionForMe(privateWordsIds);

            const subjectOfInterestPrivateWordsIds: number[] = [];
            subjectOfInterestPrivateWordsIds.push(this.lead.subjectOfInterest);
            this.getSubjectsOfInterestForMe(subjectOfInterestPrivateWordsIds);
            this.refresh.emit();
            this.leadEdited.emit();
          },
          (err) =>
            this.notificationService.notify(
              'leads.form.notification.errorWhileUpdate',
              NotificationType.ERROR
            )
        );
    } else {
      FormUtil.setTouched(this.leadForm);
    }
  }

  changePriority(priority: string): void {
    this.lead.priority = LeadPriority[priority] || LeadPriority.LOW;
    this.lead.scoring = LeadScoring[priority] || LeadScoring.LOW;
  }

  revertChanges(): void {
    this.lead = cloneDeep(this.baseLead);
    this.leadForm.reset(this.lead);
    this.sanitizeFrom();
    this.formChanged = false;

    const privateWordsIds: number[] = [];
    privateWordsIds.push(this.lead.sourceOfAcquisition);
    privateWordsIds.push(this.lead.subSourceOfAcquisition);
    this.getSourcesOfAcquisitionForMe(privateWordsIds);
    const subjectOfInterestPrivateWordsIds: number[] = [];
    subjectOfInterestPrivateWordsIds.push(this.lead.subjectOfInterest);
    this.getSubjectsOfInterestForMe(subjectOfInterestPrivateWordsIds);

    const industryWordsIds: number[] = [];
    industryWordsIds.push(this.lead.industry);
    this.getIndustryForMe(industryWordsIds);

    const positionWordsIds: number[] = [];
    positionWordsIds.push(this.lead.position);
    this.getPositionForMe(positionWordsIds);
  }

  private sanitizeFrom(): void {
    this.sourceOfAcquisition.setValue(
      this.setSelectedValue(this.sourceOfAcquisition.value)
    );
    this.subSourceOfAcquisition.setValue(
      this.setSelectedValue(this.subSourceOfAcquisition.value)
    );
    this.subjectOfInterest.setValue(
      this.setSelectedValue(this.subjectOfInterest.value)
    );
    this.industry.setValue(this.setSelectedValue(this.industry.value));
    this.position.setValue(this.setSelectedValue(this.position.value));
  }

  getSubSourceOfAcquisition(): void {
    const selected = this.sourceOfAcquisition.value;

    if (!selected || selected === this.undefinedCategoryId) {
      this.subSourceOfAcquisition.disable();
      this.subSourceOfAcquisition.patchValue(this.undefinedCategoryId);
      if (this.lead) {
        this.lead.subSourceOfAcquisition = null;
      }
      return;
    }

    this.subSourceOfAcquisition.enable();
    this.allSubSourceOfAcquisition = this.allSourceOfAcquisition.filter(
      (word) => word.id === selected
    );

    if (
      !this.allSubSourceOfAcquisition[0] ||
      this.allSubSourceOfAcquisition[0].children.length === 0
    ) {
      this.subSourceOfAcquisition.patchValue(this.undefinedCategoryId);
      this.lead.subSourceOfAcquisition = null;
    }
  }

  private getSourcesOfAcquisitionForMe(privateWordsIds: number[]): void {
    this.dictionariesService
      .getDictionaryWordsById(
        DictionaryId.SOURCE_OF_ACQUISITION,
        privateWordsIds
      )
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (words) => {
          this.allSourceOfAcquisition = words;
          this.getSubSourceOfAcquisition();
        },
        (err) =>
          this.notificationService.notify(
            'opportunity.form.notifications.getSourcesOfAcquisitionError',
            NotificationType.ERROR
          )
      );
  }

  private getSubjectsOfInterestForMe(privateWordsIds: number[]): void {
    this.dictionariesService
      .getDictionaryWordsById(DictionaryId.SUBJECT_OF_INTEREST, privateWordsIds)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (words) => {
          this.allSubjectOfInterest = words;
        },
        (err) =>
          this.notificationService.notify(
            'leads.form.notifications.getSubjectsOfInterestError',
            NotificationType.ERROR
          )
      );
  }

  private getIndustryForMe(privateWordsIds: number[]): void {
    this.dictionariesService
      .getDictionaryWordsById(DictionaryId.INDUSTRY, privateWordsIds)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (words) => {
          this.allIndustry = words;
        },
        (err) =>
          this.notificationService.notify(
            'leads.form.notification.companyTypeError',
            NotificationType.ERROR
          )
      );
  }

  private getPositionForMe(privateWordsIds: number[]): void {
    this.dictionariesService
      .getDictionaryWordsById(DictionaryId.POSITION, privateWordsIds)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (words) => {
          this.allPosition = words;
        },
        (err) =>
          this.notificationService.notify(
            'leads.form.notification.positionError',
            NotificationType.ERROR
          )
      );
  }

  private getWords(dictionary: Dictionary): DictionaryWord[] {
    if (dictionary.words === undefined || dictionary.words === null) {
      return [];
    }
    return dictionary.words;
  }

  private getSelectedValue(value: number): number {
    if (value !== this.undefinedCategoryId) {
      return value;
    }
    return null;
  }

  private setSelectedValue(value: number): number {
    if (value === null || value === undefined) {
      return this.undefinedCategoryId;
    }
    return value;
  }

  formControlFocusOutSourceOfAcquisition(newValue?: number): void {
    if (this.formControlFocusedKey) {
      const formValue = !newValue
        ? this.leadForm.get(this.formControlFocusedKey).value
        : newValue;
      this.leadForm.get(this.formControlFocusedKey).setValue(formValue);
      this.lead[this.formControlFocusedKey] = this.getSelectedValue(formValue);
      const formChanged = formValue !== this.formControlFocusedValue;
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
      if (formChanged) {
        this.leadChanged.emit(this.lead);
      }

      this.getSubSourceOfAcquisition();
      this.formControlFocusIn('subSourceOfAcquisition');
      this.formControlFocusOutSubSourceOfAcquisition(this.undefinedCategoryId);
    }
  }

  formControlFocusOutSubSourceOfAcquisition(newValue?: number): void {
    if (this.formControlFocusedKey) {
      const formValue = !newValue
        ? this.leadForm.get(this.formControlFocusedKey).value
        : newValue;
      this.leadForm.get(this.formControlFocusedKey).setValue(formValue);
      this.lead[this.formControlFocusedKey] = this.getSelectedValue(formValue);
      const formChanged = formValue !== this.formControlFocusedValue;
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
      if (formChanged) {
        this.leadChanged.emit(this.lead);
      }
    }
  }

  formControlFocusOutPosition(newValue?: number): void {
    if (this.formControlFocusedKey) {
      const formValue = !newValue
        ? this.leadForm.get(this.formControlFocusedKey).value
        : newValue;
      this.leadForm.get(this.formControlFocusedKey).setValue(formValue);
      this.lead[this.formControlFocusedKey] = this.getSelectedValue(formValue);
      const formChanged = formValue !== this.formControlFocusedValue;
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
      if (formChanged) {
        this.leadChanged.emit(this.lead);
      }
    }
  }

  openAddSourceOfAcquisitionSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.addSourceOfAcquisitionSlider.open();
  }

  onCancelAddSourceOfAcquisitionSlider(): void {
    this.addSourceOfAcquisitionSlider.close();
  }

  onSubmitAddSourceOfAcquisitionSlider(savedWord: SaveDictionaryWord): void {
    this.allSourceOfAcquisition.unshift({ ...savedWord, children: [] });
    this.sourceOfAcquisition.patchValue(savedWord.id);
    this.lead.sourceOfAcquisition = savedWord.id;
    this.getSubSourceOfAcquisition();
    this.leadChanged.emit(this.lead);
    this.addSourceOfAcquisitionSlider.close();
  }

  openAddSubSourceOfAcquisitionSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.parentWordIdForCreation = this.sourceOfAcquisition.value;
    this.addSubSourceOfAcquisitionSlider.open();
  }

  onCancelAddSubSourceOfAcquisitionSlider(): void {
    this.addSubSourceOfAcquisitionSlider.close();
  }

  onSubmitAddSubSourceOfAcquisitionSlider(savedWord: SaveDictionaryWord): void {
    const parentWord = this.getSelectedSourceOfAcquisition();
    parentWord.children.unshift({ ...savedWord, children: [] });
    this.getSubSourceOfAcquisition();
    this.subSourceOfAcquisition.patchValue(savedWord.id);
    this.parentWordIdForCreation = null;
    this.leadChanged.emit(this.lead);
    this.addSubSourceOfAcquisitionSlider.close();
  }

  private getSelectedSourceOfAcquisition(): DictionaryWord {
    return this.allSourceOfAcquisition.find(
      (word) => word.id === this.sourceOfAcquisition.value
    );
  }

  addAdditionalPhone(event: Event): void {
    event.preventDefault();
    event.stopPropagation();
    this.addAdditionalPhoneNumbersSubject.next();
  }

  onCancelAddSubjectOfInterestSlider(): void {
    this.addSubjectOfInterestSlider.close();
  }

  openAddSubjectOfInterestSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.addSubjectOfInterestSlider.open();
  }

  onSubmitAddSubjectOfInterestSlider(savedWord: SaveDictionaryWord): void {
    this.allSubjectOfInterest.unshift({ ...savedWord, children: [] });
    this.subjectOfInterest.patchValue(savedWord.id);
    this.addSubjectOfInterestSlider.close();
  }

  openAddPositionSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.addPositionSlider.open();
  }

  onCancelAddPositionSlider(): void {
    this.addPositionSlider.close();
  }

  onSubmitAddPositionSlider(savedWord: SaveDictionaryWord): void {
    this.allPosition.unshift({ ...savedWord, children: [] });
    this.position.patchValue(savedWord.id);
    this.lead.position = savedWord.id;
    this.leadChanged.emit(this.lead);
    this.addPositionSlider.close();
  }

  openAddIndustrySlider(matSelect: MatSelect): void {
    matSelect.close();
    this.addIndustrySlider.open();
  }

  onCancelAddIndustrySlider(): void {
    this.addIndustrySlider.close();
  }

  onSubmitAddIndustrySlider(savedWord: SaveDictionaryWord): void {
    this.allIndustry.unshift({ ...savedWord, children: [] });
    this.industry.patchValue(savedWord.id);
    this.lead.industry = savedWord.id;
    this.leadChanged.emit(this.lead);
    this.addIndustrySlider.close();
  }

  private handleSuccessfulImport(customer: Customer): void {
    this.companyName.patchValue(customer.name);
    this.streetAndNumber.patchValue(customer.address.street);
    this.city.patchValue(customer.address.city);
    this.postalCode.patchValue(customer.address.zipCode);
    this.province.patchValue(customer.address.province);
    this.country.patchValue(customer.address.country);
    this.notificationService.notify(
      'leads.form.notification.gus.successImportFromGus',
      NotificationType.SUCCESS
    );
  }

  getDataFromGUS($event: MouseEvent): void {
    $event.stopPropagation();
    if (this.companyNip.value && this.companyNip.value.length === 10) {
      this.customerService
        .getDataFromGUS(this.companyNip.value, null)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (customer: Customer) => this.handleSuccessfulImport(customer),
          (error) => this.notificationService.notifyError(this.gusErrors, error.status)
        );
    } else {
      this.notificationService.notify('leads.form.notification.nipNotValid', NotificationType.WARNING
      );
    }
  }
}
