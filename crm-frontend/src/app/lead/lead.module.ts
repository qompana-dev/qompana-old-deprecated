import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LeadRoutingModule} from './lead-routing.module';
import {LeadListComponent} from './lead-list/lead-list.component';
import {SharedModule} from '../shared/shared.module';
import {LeadAddComponent} from './lead-add/lead-add.component';
import {LeadFormComponent} from './lead-form/lead-form.component';
import {LeadEditComponent} from './lead-edit/lead-edit.component';
import {LeadDetailsComponent} from './lead-details/lead-details.component';
import {NgxMaskModule} from 'ngx-mask';
import {LeadsCanbanComponent} from './leads-canban/leads-canban.component';
import {LeadAccountsFilterPipe} from './lead-form/lead-accounts-filter.pipe';
import {LeadToOpportunityComponent} from './lead-to-opportunity/lead-to-opportunity.component';
import {WidgetModule} from '../widgets/widget.module';
import {MapViewModule} from '../map/map-view/map-view.module';
import {DocumentModule} from '../documents/document.module';
import {TabsModule} from '../shared/tabs/tabs.module';
import {ExpensesModule} from '../shared/tabs/expense-tab/expenses.module';
import {MailModule} from '../mail/mail.module';
import {AvatarModule} from '../shared/avatar-form/avatar.module';
import {TooltipModule} from 'ngx-bootstrap';
import {LeadDetailsTabComponent} from './lead-details-tab/lead-details-tab.component';
import {DictionaryModule} from '../dictionary/dictionary.module';
import {LeadChangeOwnerComponent} from './lead-change-owner/lead-change-owner.component';
import {AdditionalPhoneNumbersModule} from '../shared/additional-phone-numbers/additional-phone-numbers.module';
import {MatTooltipModule} from '@angular/material/tooltip';
import {TaskTooltipModule} from '../shared/task-tooltip/task-tooltip.module';
import {LeadAcceptanceComponent} from './lead-details/lead-acceptance/lead-acceptance.component';
import {LeadTasksComponent} from './lead-details/lead-tasks/lead-tasks.component';
import {LeadTabsComponent} from './lead-details/lead-tabs/lead-tabs.component';
import {LeadDetailsPropertiesComponent} from './lead-details-properties/lead-details-properties.component';
import {LeadImportComponent} from './lead-import/lead-import.component';
import {LeadImportDialogComponent} from './lead-import/lead-import-dialog/lead-import-dialog.component';
import { LeadSearchComponent } from './lead-list/lead-search/lead-search.component';
import {MatRadioModule} from '@angular/material/radio';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import {DateUtil} from '../shared/util/date.util';

@NgModule({

  declarations: [
    LeadListComponent,
    LeadAddComponent,
    LeadFormComponent,
    LeadEditComponent,
    LeadDetailsComponent,
    LeadsCanbanComponent,
    LeadAccountsFilterPipe,
    LeadToOpportunityComponent,
    LeadAccountsFilterPipe,
    LeadDetailsTabComponent,
    LeadChangeOwnerComponent,
    LeadAcceptanceComponent,
    LeadTasksComponent,
    LeadTabsComponent,
    LeadDetailsPropertiesComponent,
    LeadImportComponent,
    LeadImportDialogComponent,
    LeadSearchComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    TabsModule,
    LeadRoutingModule,
    MapViewModule,
    NgxMaskModule,
    WidgetModule,
    DocumentModule,
    ExpensesModule,
    MailModule,
    AvatarModule,
    TooltipModule.forRoot(),
    DictionaryModule,
    AdditionalPhoneNumbersModule,
    MatTooltipModule,
    TaskTooltipModule,
    MatRadioModule,
  ],
  entryComponents: [LeadImportDialogComponent],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: DateUtil.getInputFormat()}
  ]
})
export class LeadModule {
}
