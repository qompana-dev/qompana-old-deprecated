import {Component} from '@angular/core';
import {MatDialog} from '@angular/material';
import {LeadImportDialogComponent} from './lead-import-dialog/lead-import-dialog.component';
import {DeleteDialogData} from '../../shared/dialog/delete/delete-dialog.component';
import {LeadImportDialogData} from './lead-import-dialog/lead-import.model';

@Component({
  selector: 'app-lead-import',
  templateUrl: './lead-import.component.html',
  styleUrls: ['./lead-import.component.scss']
})
export class LeadImportComponent {

  constructor(public dialog: MatDialog) {
  }

  openDialog(data: Partial<LeadImportDialogData>): void {
    const dialogRef = this.dialog.open(LeadImportDialogComponent, {
      data,
      panelClass: 'lead-import-dialog-container'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
