import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MatSelect} from '@angular/material';
import {LeadImportService} from '../lead-import.service';
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {LeadService} from '../../lead.service';
import {Subject} from 'rxjs';
import omitBy from 'lodash/omitBy';
import {ProcessDefinition} from '../../lead.model';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {applyPermission} from '../../../shared/permissions-utils';
import {AuthService} from '../../../login/auth/auth.service';
import {AccountEmail} from '../../../person/person.model';
import {PersonService} from '../../../person/person.service';
import {
  Dictionary,
  DictionaryId,
  DictionaryWord,
  DictionaryWordFactory,
  SaveDictionaryWord,
  SaveDictionaryWordFactory
} from '../../../dictionary/dictionary.model';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {DictionaryAddWordComponent} from '../../../dictionary/dictionary-add-word/dictionary-add-word.component';
import {LeadImportDialogData, LeadImportSettings, ParsedLead} from './lead-import.model';
import {HttpErrorResponse} from '@angular/common/http';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DictionaryStoreService} from 'src/app/dictionary/dictionary-store.service';
import {MatTable} from '@angular/material/table';
import {DictionaryWordsAddComponent} from '../../../dictionary/dictionary-words-add/dictionary-words-add.component';
import {MatAutocompleteTrigger} from '@angular/material/autocomplete';

@Component({
  selector: 'app-lead-import-dialog',
  templateUrl: './lead-import-dialog.component.html',
  styleUrls: ['./lead-import-dialog.component.scss'],
  providers: [LeadImportService],
})
export class LeadImportDialogComponent implements OnInit, OnDestroy {

  leadImportForm: FormGroup;
  skipIncorrect = false;
  mergeDuplicate = false;
  accountEmails: AccountEmail[];
  processDefinitions: ProcessDefinition[];
  readonly undefinedCategoryId = -1000;
  readonly undefinedCategoryObject = {
    id: this.undefinedCategoryId,
    name: 'Bez kategorii'
  };
  parentWordIdForCreation: number = null;
  dictionaryId = DictionaryId;
  allSourceOfAcquisition: DictionaryWord[];
  allSubSourceOfAcquisition: DictionaryWord[];
  allPosition: DictionaryWord[];
  hasBaseDropZoneOver = false;
  displayedColumns: string[] = ['firstName', 'lastName', 'email', 'phone', 'position', 'companyName', 'companyCity', 'validation', 'menu'];
  editedLead: ParsedLead;
  private componentDestroyedNotifier: Subject<void> = new Subject();
  private componentDestroyed: Subject<void> = new Subject();

  emailChanged: Subject<string> = new Subject<string>();

  @ViewChild('addSourceOfAcquisitionSlider') addSourceOfAcquisitionSlider: SliderComponent;
  @ViewChild('addSourceOfAcquisitionWord') addSourceOfAcquisitionWord: DictionaryAddWordComponent;

  @ViewChild('addSubSourceOfAcquisitionSlider') addSubSourceOfAcquisitionSlider: SliderComponent;
  @ViewChild('addSubSourceOfAcquisitionWord') addSubSourceOfAcquisitionWord: DictionaryAddWordComponent;

  @ViewChild('addPositionSlider') addPositionSlider: SliderComponent;
  @ViewChild('table') table: MatTable<any>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: LeadImportDialogData,
    public importService: LeadImportService,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private authService: AuthService,
    private leadService: LeadService,
    private personService: PersonService,
    private dictionaryStoreService: DictionaryStoreService,
    private notificationService: NotificationService,
    private dialogRef: MatDialogRef<LeadImportDialogComponent>) {
    this.createLeadImportForm();
    this.subscribeForEmailChange();
  }


  ngOnInit(): void {
    this.getAccountEmails();
    this.getProcessDefinitions();
    this.getSourcesOfAcquisitionForMe([]);
    this.getPositionForMe([]);
    this.applyPermissionsToForm();
  }

  ngOnDestroy(): void {
    this.componentDestroyedNotifier.next();
    this.componentDestroyedNotifier.complete();
  }

  get processDefinitionId(): AbstractControl {
    return this.leadImportForm.get('processDefinitionId');
  }

  get leadKeeperId(): AbstractControl {
    return this.leadImportForm.get('leadKeeperId');
  }

  get sourceOfAcquisition(): AbstractControl {
    return this.leadImportForm.get('sourceOfAcquisitionId');
  }

  get subSourceOfAcquisition(): AbstractControl {
    return this.leadImportForm.get('subSourceOfAcquisitionId');
  }

  get authKey(): string {
    return 'LeadCreateComponent.';
  }

  subscribeForEmailChange(): void {
    this.emailChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(email => {
        this.importService.refreshValidationDuplicate();
      });
  }


  onEmailChange(email: string): void {
    this.emailChanged.next(email);
  }

  public submit(): void {
    if (!this.leadImportForm.valid) {
      (Object as any).values(this.leadImportForm.controls).forEach(control => {
        control.markAsTouched();
      });
      return;
    }
    if (!this.getParsedLeads()) {
      this.notificationService.notify('leads.form.notification.emptyImport', NotificationType.WARNING);
      return;
    }

    const saveDictionaryWords: SaveDictionaryWord[] = this.getParsedLeads()
      .filter(lead => lead.position.value)
      .filter(lead => this.allPosition.findIndex(value => value.name === lead.position.value) === -1)
      .map(lead => {
        const newWord = DictionaryWordFactory.createNew();
        newWord.name = lead.position.value;
        newWord.global = false;
        return SaveDictionaryWordFactory.create(DictionaryId.POSITION, newWord);
      });

    if (!(saveDictionaryWords.length > 0)) {
      this.importLeads();
    } else {
      const dialogRef = this.dialog.open(DictionaryWordsAddComponent, {
        data: {newWords: saveDictionaryWords}
      });

      dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.importLeads();
          } else {
            return;
          }
        },
        (err) => this.notifyError(err)
      );
    }

  }

  public importLeads(): void {
    const leadImportSettings: LeadImportSettings = this.leadImportForm.getRawValue();
    leadImportSettings.sourceOfAcquisitionId = this.getSelectedValue(leadImportSettings.sourceOfAcquisitionId);
    leadImportSettings.subSourceOfAcquisitionId = this.getSelectedValue(leadImportSettings.subSourceOfAcquisitionId);
    leadImportSettings.skipIncorrect = this.skipIncorrect;
    leadImportSettings.mergeDuplicate = this.mergeDuplicate;
    omitBy(leadImportSettings, value => value === null || value === undefined);

    this.importService.importLeads(leadImportSettings).subscribe(
      () => {
        this.data.reloadLeadsCallback();
        this.notifyAddSuccess();
        this.close();
      },
      (err) => this.notifyError(err)
    );
  }

  close(): void {
    this.dialogRef.close();
  }

  public positionSelect(lead: ParsedLead, position: DictionaryWord): void {
    const index: number = this.getParsedLeads().findIndex(element => element === lead);
    this.importService.setPosition(index, position);
    this.table.renderRows();
  }

  public deleteLead(lead: ParsedLead): void {
    const index: number = this.getParsedLeads().findIndex(element => element === lead);
    this.importService.deleteParsedLead(index);
    this.table.renderRows();
    const parsedLeads = this.getParsedLeads();
  }

  public getParsedLeads(): ParsedLead[] {
    return this.importService.getParsedLeads();
  }

  public isParsingFinished(): boolean {
    return this.importService.getParsingFinished();
  }

  public isDuplicate(lead: ParsedLead): boolean {
    let isDuplicate = false;
    if (lead !== undefined) {
      isDuplicate = lead.email.validationResults.includes('DUPLICATE');
    }
    return isDuplicate;
  }

  public isIncorrect(lead: ParsedLead): boolean {
    let isIncorrect = false;
    if (lead !== undefined) {
      isIncorrect = lead.firstName.validationResults.includes('INCORRECT') ||
        lead.lastName.validationResults.includes('INCORRECT') ||
        lead.email.validationResults.includes('INCORRECT') ||
        lead.phone.validationResults.includes('INCORRECT') ||
        lead.position.validationResults.includes('INCORRECT') ||
        lead.companyName.validationResults.includes('INCORRECT') ||
        lead.companyCity.validationResults.includes('INCORRECT');
    }
    return isIncorrect;
  }

  private createLeadImportForm(): void {
    this.leadImportForm = this.fb.group({
      processDefinitionId: [null, Validators.required],
      leadKeeperId: [+localStorage.getItem('loggedAccountId'), Validators.required],
      sourceOfAcquisitionId: [this.undefinedCategoryId],
      subSourceOfAcquisitionId: [this.undefinedCategoryId],
    });
    this.applyPermissionsToForm();
  }

  private applyPermissionsToForm(): void {
    if (this.leadImportForm) {
      applyPermission(this.processDefinitionId, !this.authService.isPermissionPresent(`${this.authKey}processDefinitionField`, 'r'));
    }
  }

  private getSelectedValue(value: number): number {
    if (value !== this.undefinedCategoryId) {
      return value;
    }
    return null;
  }

  private notifyAddSuccess(): void {
    // todo message
    this.notificationService.notify('leads.form.notification.editedSuccessfully', NotificationType.SUCCESS);
  }

  private notifyError(err: HttpErrorResponse): void {
    // todo message
    // this.notificationService.notifyError(this.editLeadErrorTypes, err.status);
  }

  private getAccountEmails(): void {
    this.personService.getAccountEmails()
      .pipe(
        takeUntil(this.componentDestroyedNotifier)
      ).subscribe(
      emails => this.accountEmails = emails,
      err => this.notificationService.notify('leads.form.notification.accountEmailsError', NotificationType.ERROR)
    );
  }

  private getProcessDefinitions(): void {
    this.leadService.getProcessDefinitions('LEAD')
      .pipe(
        takeUntil(this.componentDestroyedNotifier)
      ).subscribe(
      definitions => this.processDefinitions = definitions,
      err => this.notificationService.notify('leads.form.notification.processDefinitionError', NotificationType.ERROR)
    );
  }

  getSubSourceOfAcquisition(): void {
    const selected = this.sourceOfAcquisition.value;

    if (!selected || selected === this.undefinedCategoryId) {
      this.subSourceOfAcquisition.disable();
      this.subSourceOfAcquisition.patchValue(this.undefinedCategoryId);
      return;
    }

    this.subSourceOfAcquisition.enable();
    this.allSubSourceOfAcquisition = this.allSourceOfAcquisition.filter(word => word.id === selected);

    if (!this.allSubSourceOfAcquisition[0] || this.allSubSourceOfAcquisition[0].children.length === 0) {
      this.subSourceOfAcquisition.patchValue(this.undefinedCategoryId);
    }
  }

  private getSourcesOfAcquisitionForMe(privateWordsIds: number[]): void {
    this.dictionaryStoreService.getDictionaryWordsById(DictionaryId.SOURCE_OF_ACQUISITION, privateWordsIds).pipe(
      takeUntil(this.componentDestroyedNotifier)
    ).subscribe(
      words => {
        this.allSourceOfAcquisition = words;
        this.getSubSourceOfAcquisition();
      },
      err => this.notificationService.notify('leads.form.notifications.getSourcesOfAcquisitionError', NotificationType.ERROR)
    );
  }

  private getPositionForMe(privateWordsIds: number[]): void {
    this.dictionaryStoreService.getDictionaryWordsById(DictionaryId.POSITION, privateWordsIds)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(words => {
          this.allPosition = words;
        },
        err => this.notificationService.notify('leads.form.notification.positionError', NotificationType.ERROR)
      );
  }

  private getWords(dictionary: Dictionary): DictionaryWord[] {
    if (dictionary.words === undefined || dictionary.words === null) {
      return [];
    }
    return dictionary.words;
  }

  openAddSourceOfAcquisitionSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.addSourceOfAcquisitionSlider.open();
  }

  onSubmitAddSourceOfAcquisitionSlider(savedWord: SaveDictionaryWord): void {
    this.allSourceOfAcquisition.unshift({...savedWord, children: []});
    this.sourceOfAcquisition.patchValue(savedWord.id);
    this.addSourceOfAcquisitionSlider.close();
  }

  onCancelAddSourceOfAcquisitionSlider(): void {
    this.addSourceOfAcquisitionSlider.close();
  }

  openAddSubSourceOfAcquisitionSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.parentWordIdForCreation = this.sourceOfAcquisition.value;
    this.addSubSourceOfAcquisitionSlider.open();
  }

  onSubmitAddSubSourceOfAcquisitionSlider(savedWord: SaveDictionaryWord): void {
    const parentWord = this.getSelectedSourceOfAcquisition();
    parentWord.children.unshift({...savedWord, children: []});
    this.getSubSourceOfAcquisition();
    this.subSourceOfAcquisition.patchValue(savedWord.id);
    this.parentWordIdForCreation = null;
    this.addSubSourceOfAcquisitionSlider.close();
  }

  private getSelectedSourceOfAcquisition(): DictionaryWord {
    return this.allSourceOfAcquisition.find(word => word.id === this.sourceOfAcquisition.value);
  }

  onCancelAddSubSourceOfAcquisitionSlider(): void {
    this.addSubSourceOfAcquisitionSlider.close();
  }

  openAddPositionSlider(lead: ParsedLead, trigger: MatAutocompleteTrigger): void {
    trigger.closePanel();
    this.editedLead = lead;
    this.addPositionSlider.open();
  }

  onCancelAddPositionSlider(): void {
    this.editedLead = null;
    this.addPositionSlider.close();
  }

  onSubmitAddPositionSlider(savedWord: SaveDictionaryWord): void {
    this.allPosition.unshift({...savedWord, children: []});
    const dictWord: DictionaryWord = {...savedWord, children: []};
    this.positionSelect(this.editedLead, dictWord);
    this.editedLead = null;
    this.addPositionSlider.close();
  }

}
