export interface LeadImportDialogData {
  reloadLeadsCallback: () => void;
}

export interface LeadImport {
  settings: LeadImportSettings;
  leads: ParsedLead[];
}

export interface LeadImportSettings {
  skipIncorrect: boolean;
  mergeDuplicate: boolean;
  processDefinitionId: string;
  leadKeeperId: number;
  sourceOfAcquisitionId: number;
  subSourceOfAcquisitionId: number;
}

export interface ParsedLead {
  firstName: ParsedLeadValidatedValue;
  lastName: ParsedLeadValidatedValue;
  email: ParsedLeadValidatedValue;
  phone: ParsedLeadValidatedValue;
  position: ParsedLeadValidatedValue;
  companyName: ParsedLeadValidatedValue;
  companyCity: ParsedLeadValidatedValue;
}

export interface ParsedLeadValidatedValue {
  value: string;
  validationResults: string[];
}



