import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {UrlUtil} from '../../shared/url.util';
import {HttpClient} from '@angular/common/http';
import {LoginService} from '../../login/login.service';
import {Observable} from 'rxjs';
import {FileItem, FileUploader, ParsedResponseHeaders} from 'ng2-file-upload';
import {FileFormData} from '../../files/file.model';
import {LeadImport, LeadImportSettings, ParsedLead} from './lead-import-dialog/lead-import.model';
import {DictionaryWord} from '../../dictionary/dictionary.model';

@Injectable({
  providedIn: 'root'
})
export class LeadImportService extends FileUploader {

  constructor(private dialog: MatDialog,
              private http: HttpClient,
              private loginService: LoginService) {
    super({
      url: `${LeadImportService.leadUrl}/parse`,
      queueLimit: 1,
      autoUpload: true
    });
  }

  static readonly leadUrl = `${UrlUtil.url}/crm-business-service/lead`;

  private parsedLeads: ParsedLead[];
  private parsingFinished = true;

  public getParsedLeads() {
    return this.parsedLeads;
  }

  public getParsingFinished() {
    return this.parsingFinished;
  }

  public importLeads(leadImportSettings: LeadImportSettings): Observable<LeadImport> {
    const url = `${LeadImportService.leadUrl}/import`;
    const leadImportDto: LeadImport = {
      settings: leadImportSettings,
      leads: this.parsedLeads
    };
    return this.http.post<LeadImport>(url, leadImportDto);
  }

  uploadItems(items: FileItem[], form: FileFormData): void {
    items.forEach(item => {
      this.uploadItem(item);
    });
  }

  uploadItem(item: FileItem): void {
    if (!this.loginService.isLoggedIn()) {
      this.loginService.getRefreshTokenSubscription().subscribe(() => {
        this.parsingFinished = false;
        super.uploadItem(item);
      });
    } else {
      this.parsingFinished = false;
      super.uploadItem(item);
    }
  }


  onCompleteItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    this.parsedLeads = JSON.parse(response);
    this.parsingFinished = true;
    console.log('type', item.file.type);
    console.log('rawFile', item.file.rawFile);
    console.log('name', item.file.name);
    return super.clearQueue();
  }

  onBeforeUploadItem(fileItem: FileItem): any {
    fileItem.headers = [
      {name: 'Authorization', value: 'Bearer ' + this.loginService.getToken()},];
    fileItem.withCredentials = false;
  }

  public setPosition(index: number, position: DictionaryWord): void {
    this.parsedLeads[index].position.value = position.name;
  }

  public deleteParsedLead(index: number): void {
    this.parsedLeads.splice(index, 1);
    this.refreshValidationDuplicate();
  }

  public refreshValidationDuplicate(): void {
    this.parsedLeads.forEach((lead) => {
      const index = lead.email.validationResults.indexOf('DUPLICATE');
      if (index !== -1) {
        lead.email.validationResults.splice(index, 1);
      }
      this.parsedLeads
        .filter(compareLead => compareLead !== lead)
        .filter((compareLead) => compareLead.email.value === lead.email.value)
        .forEach((compareLead) => {
          if (!lead.email.validationResults.includes('DUPLICATE')) {
            lead.email.validationResults.push('DUPLICATE');
          }
        });
    });
  }

}
