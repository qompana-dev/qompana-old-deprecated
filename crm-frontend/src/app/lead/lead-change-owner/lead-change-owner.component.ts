import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {BAD_REQUEST, NOT_FOUND, FORBIDDEN} from 'http-status-codes';
import {takeUntil} from 'rxjs/operators';
import {SliderService} from '../../shared/slider/slider.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {LeadOnList, LeadsKeeper} from "../lead.model";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {applyPermission} from "../../shared/permissions-utils";
import {AuthService} from "../../login/auth/auth.service";
import {LeadService} from "../lead.service";
import {PersonService} from "../../person/person.service";
import {AccountEmail} from "../../person/person.model";


@Component({
  selector: 'app-lead-change-owner',
  templateUrl: './lead-change-owner.component.html',
  styleUrls: ['./lead-change-owner.component.scss']
})
export class LeadChangeOwnerComponent implements OnInit, OnDestroy {

  private componentDestroyed: Subject<void> = new Subject();

  ownerForm: FormGroup;

  @Input() selectedLeads: LeadOnList[];
  @Output() ownerChanged: EventEmitter<void> = new EventEmitter();

  accountEmails: AccountEmail[];

  readonly errorTypesForModification: ErrorTypes = {
    base: 'leads.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'badRequest'
      }, {
        code: NOT_FOUND,
        text: 'leadNotFound'
      }, {
        code: FORBIDDEN,
        text: 'forbidden'
      }
    ]
  };

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private personService: PersonService,
              private sliderService: SliderService,
              private leadService: LeadService,
              private notificationService: NotificationService) {
    this.createKeeperForm();
  }

  ngOnInit(): void {
    this.getAccountEmails();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  get authKey(): string {
    return 'CustomerEditComponent.';
  }

  get leadKeeper(): AbstractControl {
    return this.ownerForm.get('leadKeeper');
  }

  get leadsIds(): AbstractControl {
    return this.ownerForm.get('leadsIds');
  }

  submit(): void {
    this.leadsIds.patchValue(this.selectedLeads.map(lead => lead.id));
    const leadsKeeper: LeadsKeeper = this.ownerForm.getRawValue();
    this.leadService.changeLeadsKeeper(leadsKeeper)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => this.handleSuccess(),
        (error) => this.notificationService.notifyError(this.errorTypesForModification, error.status)
      );
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
  }

  private handleSuccess(): void {
    this.notificationService.notify('leads.form.notification.editedSuccessfully', NotificationType.SUCCESS);
    this.ownerChanged.next();
    this.closeSlider();
  }

  private createKeeperForm(): void {
    if (!this.ownerForm) {
      this.ownerForm = this.formBuilder.group({
        leadKeeper: [null, [Validators.required]],
        leadsIds: [[], [Validators.required]]
      });
      this.applyPermissionsToForm();
    }
  }

  private applyPermissionsToForm(): void {
    if (this.ownerForm) {
      applyPermission(this.leadKeeper, !this.authService.isPermissionPresent(`LeadShared.changeKeeper`, 'r'));
    }
  }

  private getAccountEmails(): void {
    this.personService.getAccountEmails()
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      emails => {
        this.accountEmails = emails;
        this.setDefaultOwnerIfNull();
      },
      err => this.notificationService.notify('leads.form.notification.accountEmailsError', NotificationType.ERROR)
    );
  }

  private setDefaultOwnerIfNull(): void {
    if (!this.leadKeeper.value && this.accountEmails) {
      const loggedAccountId = +localStorage.getItem('loggedAccountId');
      if (loggedAccountId) {
        this.leadKeeper.patchValue(loggedAccountId);
      }
    }
  }
}
