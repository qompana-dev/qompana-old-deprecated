import { Property } from './../../opportunity/opportunity.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-lead-details-properties',
  templateUrl: './lead-details-properties.component.html',
  styleUrls: ['./lead-details-properties.component.scss']
})
export class LeadDetailsPropertiesComponent {

  @Input() taskOnScreen = null;
  @Input() activeTaskId = null;
  @Input() acceptanceUserId = null;
  @Input() propertyForm: FormGroup;

  @Output() submit: EventEmitter<void> = new EventEmitter<void>();

  isEmpty(property: Property): boolean {
    const value = this.taskOnScreen.properties.find(e => e.id === property.id).value;
    return value === null || value === '';
  }

  isFalse(property: Property): boolean {
    return this.propertyForm.get(property.id) && !this.propertyForm.get(property.id).value;
  }

  cancelChange(id: string): void {
    this.propertyForm.get(id).patchValue(this.taskOnScreen.properties.find(e => e.id === id).value);
  }

  onSubmit() {
    this.submit.emit();
  }
}
