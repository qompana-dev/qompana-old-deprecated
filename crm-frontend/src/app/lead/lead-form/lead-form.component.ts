import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {FoundPlace, LeadPriority, LeadScoring, LeadSave, ProcessDefinition, PriorityRadioButtons, PhotonResponcePlace} from '../lead.model';
import {LeadService} from '../lead.service';
import {merge, Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, switchMap, takeUntil, tap} from 'rxjs/operators';
import {PersonService} from '../../person/person.service';
import {AccountEmail} from '../../person/person.model';
import {SliderService} from '../../shared/slider/slider.service';
import omitBy from 'lodash/omitBy';
import {MapService} from '../../map/map.service';
import {Address} from '../../map/map.model';
import {MapViewMarker} from '../../map/map-view/map-view.model';
import {SliderComponent} from '../../shared/slider/slider.component';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {applyPermission} from '../../shared/permissions-utils';
import {AuthService} from '../../login/auth/auth.service';
import {LngLatLike} from 'mapbox-gl';
import {MatSelect} from '@angular/material';
import {whenSliderClosed, whenSliderOpen} from '../../shared/util/slider.util';
import {FormValidators} from '../../shared/form/form-validators';
import {DictionaryId, DictionaryWord, SaveDictionaryWord} from '../../dictionary/dictionary.model';
import * as isEqual from 'lodash/isEqual';
import {DictionaryAddWordComponent} from '../../dictionary/dictionary-add-word/dictionary-add-word.component';
import {MatAutocompleteTrigger} from '@angular/material/autocomplete';
import { DictionaryStoreService } from 'src/app/dictionary/dictionary-store.service';
import {Customer} from '../../customer/customer.model';
import {CustomerService} from '../../customer/customer.service';
import {BAD_REQUEST, GONE, NOT_FOUND} from 'http-status-codes';
import {PhoneService} from '../../shared/phone-fields/phone.service';


@Component({
  selector: 'app-lead-form',
  templateUrl: './lead-form.component.html',
  styleUrls: ['./lead-form.component.scss']
})
export class LeadFormComponent implements OnInit, OnDestroy {

  address: Address;
  extents: {
    city: number[],
    country: number[],
  };

  focusedElement: string;
  leadForm: FormGroup;
  leadPriority = LeadPriority;
  accountEmails: AccountEmail[];
  processDefinitions: ProcessDefinition[];
  primaryProcessId: string = null;
  foundPlaces: FoundPlace[] = [];
  @ViewChild('mapSlider') mapSlider: SliderComponent;
  @Input() update = false;
  private componentDestroyed: Subject<void> = new Subject();
  leadSave: LeadSave;
  center: LngLatLike = undefined;
  placeSearchLoading = false;
  showMap = true;
  markers: MapViewMarker[] = [];
  @ViewChild('placeSelect') placeSelect: MatSelect;

  gusErrors: ErrorTypes = {
    base: 'leads.form.notification.gus.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'customerNotFoundInGus'
      },
      {
        code: BAD_REQUEST,
        text: 'badRequest'
      },
      {
        code: GONE,
        text: 'errorInGus'
      }
    ]
  };

  readonly undefinedCategoryId = -1000;
  readonly undefinedCategoryObject = {
    id: this.undefinedCategoryId,
    name: 'Bez kategorii'
  };
  readonly defaultCountry = {
    name: 'Polska',
    lon: '19.134422',
    lat: '52.215933',
  };

  readonly priorities = PriorityRadioButtons;

  dictionaryId = DictionaryId;
  parentWordIdForCreation: number = null;
  allSourceOfAcquisition: DictionaryWord[];
  allSubSourceOfAcquisition: DictionaryWord[];
  allSubjectOfInterest: DictionaryWord[];
  allIndustry: DictionaryWord[];
  allPosition: DictionaryWord[];

  private addAdditionalPhoneNumbersSubject: Subject<void> = new Subject<void>();
  public addAdditionalPhoneNumbers$: Observable<void> = this.addAdditionalPhoneNumbersSubject.asObservable();

  @ViewChild('sourceOfAcquisitionSelect') sourceOfAcquisitionSelect: MatSelect;
  @ViewChild('subSourceOfAcquisitionSelect') subSourceOfAcquisitionSelect: MatSelect;

  @ViewChild('addSourceOfAcquisitionSlider') addSourceOfAcquisitionSlider: SliderComponent;
  @ViewChild('addSourceOfAcquisitionWord') addSourceOfAcquisitionWord: DictionaryAddWordComponent;

  @ViewChild('addSubSourceOfAcquisitionSlider') addSubSourceOfAcquisitionSlider: SliderComponent;
  @ViewChild('addSubSourceOfAcquisitionWord') addSubSourceOfAcquisitionWord: DictionaryAddWordComponent;

  @ViewChild('addSubjectOfInterestSlider') addSubjectOfInterestSlider: SliderComponent;
  @ViewChild('addSubjectOfInterestWord') addSubjectOfInterestWord: DictionaryAddWordComponent;

  @ViewChild('addPositionSlider') addPositionSlider: SliderComponent;
  @ViewChild('addPositionWord') addPositionWord: DictionaryAddWordComponent;

  @ViewChild('addIndustrySlider') addIndustrySlider: SliderComponent;
  @ViewChild('addIndustryWord') addIndustryWord: DictionaryAddWordComponent;

  constructor(private fb: FormBuilder,
              private leadService: LeadService,
              private personService: PersonService,
              private sliderService: SliderService,
              private mapService: MapService,
              private notificationService: NotificationService,
              private authService: AuthService,
              private dictionaryStoreService: DictionaryStoreService,
              private customerService: CustomerService,
              public phoneService: PhoneService) {
    this.createLeadForm();
  }

  @Input() set lead(lead: LeadSave) {
    if (lead) {
      this.leadSave = lead;
      this.leadForm.reset(lead);

      const privateWordsIds: number[] = [];
      privateWordsIds.push(this.leadSave.sourceOfAcquisition);
      privateWordsIds.push(this.leadSave.subSourceOfAcquisition);
      this.getSourcesOfAcquisitionForMe(privateWordsIds);

      const subjectOfInterestPrivateWordsIds: number[] = [];
      subjectOfInterestPrivateWordsIds.push(this.leadSave.subjectOfInterest);
      this.getSubjectsOfInterestForMe(subjectOfInterestPrivateWordsIds);

      const industryWordsIds: number[] = [];
      industryWordsIds.push(this.leadSave.industry);
      this.getIndustryForMe(industryWordsIds);

      const positionWordsIds: number[] = [];
      positionWordsIds.push(this.leadSave.position);
      this.getPositionForMe(positionWordsIds);
    }
  }

  // @formatter:off
  get processDefinitionId(): AbstractControl {
    return this.leadForm.get('processDefinitionId');
  }

  get industrySearch(): AbstractControl {
    return this.leadForm.get('industrySearch');
  }

  get industry(): AbstractControl {
    return this.leadForm.get('industry');
  }

  get companyNip(): AbstractControl {
    return this.leadForm.get('companyNip');
  }

  get companyName(): AbstractControl {
    return this.leadForm.get('companyName');
  }

  get wwwAddress(): AbstractControl {
    return this.leadForm.get('wwwAddress');
  }

  get streetAndNumber(): AbstractControl {
    return this.leadForm.get('streetAndNumber');
  }

  get city(): AbstractControl {
    return this.leadForm.get('city');
  }

  get postalCode(): AbstractControl {
    return this.leadForm.get('postalCode');
  }

  get province(): AbstractControl {
    return this.leadForm.get('province');
  }

  get country(): AbstractControl {
    return this.leadForm.get('country');
  }

  get notes(): AbstractControl {
    return this.leadForm.get('notes');
  }

  get priority(): AbstractControl {
    return this.leadForm.get('priority');
  }

  get scoring(): AbstractControl {
    return this.leadForm.get('scoring');
  }

  get firstName(): AbstractControl {
    return this.leadForm.get('firstName');
  }

  get lastName(): AbstractControl {
    return this.leadForm.get('lastName');
  }

  get positionSearch(): AbstractControl {
    return this.leadForm.get('positionSearch');
  }

  get position(): AbstractControl {
    return this.leadForm.get('position');
  }

  get email(): AbstractControl {
    return this.leadForm.get('email');
  }

  get phone(): AbstractControl {
    return this.leadForm.get('phone');
  }

  get additionalPhones(): AbstractControl {
    return this.leadForm.get('additionalPhones');
  }

  get leadKeeperId(): AbstractControl {
    return this.leadForm.get('leadKeeperId');
  }

  get sourceOfAcquisition(): AbstractControl {
    return this.leadForm.get('sourceOfAcquisition');
  }

  get subSourceOfAcquisition(): AbstractControl {
    return this.leadForm.get('subSourceOfAcquisition');
  }

  get subjectOfInterest(): AbstractControl {
    return this.leadForm.get('subjectOfInterest');
  }

  get interestedInCooperation(): AbstractControl {
    return this.leadForm.get('interestedInCooperation');
  }

  get authKey(): string {
    return (this.update) ? 'LeadEditComponent.' : 'LeadCreateComponent.';
  }

  get avatar(): AbstractControl {
    return this.leadForm.get('avatar');
  }

  // @formatter:on
  ngOnInit(): void {
    this.subscribeForSliderClose();
    this.subscribeForSliderOpen();
    this.subscribeForPositionSearch();
    this.subscribeForIndustrySearch();
    this.subscribeForPlaceSearch();
    this.subscribeForPriority();
    this.subscribeForProvinceAndPostalCodeSearch();
    this.getAccountEmails();
    this.getProcessDefinitions();
    this.getPrimaryProcesses();
    this.getSourcesOfAcquisitionForMe([]);
    this.getSubjectsOfInterestForMe([]);
    this.getIndustryForMe([]);
    this.getPositionForMe([]);
    this.applyPermissionsToForm();
    this.showPlace(this.defaultCountry);
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  public showOnMap($event?): void {
    if ($event) {
      $event.stopPropagation();
    }
    const address: Address = {
      street: this.streetAndNumber.value,
      city: this.city.value,
      state: this.province.value,
      zip: this.postalCode.value,
      country: this.country.value || this.defaultCountry.name,
    };
    this.mapService.getCoordinatesByAddress(address)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      places => {
        this.handleFoundPlaces(places);
      },
      err => this.mapService.notifyPlaceNotFound()
    );
  }

  public getLead(): LeadSave {
    const lead: LeadSave = this.leadForm.getRawValue();
    lead.sourceOfAcquisition = this.getSelectedValue(lead.sourceOfAcquisition);
    lead.subSourceOfAcquisition = this.getSelectedValue(lead.subSourceOfAcquisition);
    lead.subjectOfInterest = this.getSelectedValue(lead.subjectOfInterest);
    lead.industry = this.getSelectedValue(lead.industry);
    lead.position = this.getSelectedValue(lead.position);
    return omitBy(lead, value => value === null || value === undefined);
  }

  private getSelectedValue(value: number): number {
    if (value !== this.undefinedCategoryId) {
      return value;
    }
    return null;
  }

  public isFormValid(): boolean {
    return this.leadForm.valid;
  }

  public markAsTouched(): void {
    (Object as any).values(this.leadForm.controls).forEach(control => {
      control.markAsTouched();
    });
  }

  getSubSourceOfAcquisition(): void {
    const selected = this.sourceOfAcquisition.value;

    if (!selected || selected === this.undefinedCategoryId) {
      this.subSourceOfAcquisition.disable();
      this.subSourceOfAcquisition.patchValue(this.undefinedCategoryId);
      return;
    }

    this.subSourceOfAcquisition.enable();
    this.allSubSourceOfAcquisition = this.allSourceOfAcquisition.filter(word => word.id === selected);

    if (!this.allSubSourceOfAcquisition[0] || this.allSubSourceOfAcquisition[0].children.length === 0) {
      this.subSourceOfAcquisition.patchValue(this.undefinedCategoryId);
    }
  }

  private getSourcesOfAcquisitionForMe(privateWordsIds: number[]): void {
    this.dictionaryStoreService
    .getDictionaryWordsById(
      DictionaryId.SOURCE_OF_ACQUISITION,
      privateWordsIds
    ).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      words => {
        this.allSourceOfAcquisition = words;
        this.getSubSourceOfAcquisition();
      },
      err => this.notificationService.notify('leads.form.notifications.getSourcesOfAcquisitionError', NotificationType.ERROR)
    );
  }

  private getSubjectsOfInterestForMe(privateWordsIds: number[]): void {
    this.dictionaryStoreService
    .getDictionaryWordsById(
      DictionaryId.SUBJECT_OF_INTEREST,
      privateWordsIds
    ).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      words => {
        this.allSubjectOfInterest = words;
      },
      err => this.notificationService.notify('leads.form.notifications.getSubjectsOfInterestError', NotificationType.ERROR)
    );
  }

  private getIndustryForMe(privateWordsIds: number[]): void {
    this.dictionaryStoreService
      .getDictionaryIndustry(this.componentDestroyed, privateWordsIds)
      .subscribe(
      words => {
        this.allIndustry = words;
        if (this.leadSave) {
          this.industrySelect(this.allIndustry.find(word => word.id === this.leadSave.industry));
        }
      },
        () => this.notificationService.notify('leads.form.notification.companyTypeError', NotificationType.ERROR)
    );
  }

  private getPositionForMe(privateWordsIds: number[]): void {
    this.dictionaryStoreService
      .getDictionaryPosition(this.componentDestroyed, privateWordsIds)
      .subscribe(
        words => {
          this.allPosition = words;
          if (this.leadSave) {
            this.positionSelect(this.allPosition.find(word => word.id === this.leadSave.position));
          }
        },
        () => this.notificationService.notify('leads.form.notification.positionError', NotificationType.ERROR)
      );
  }

  private getAccountEmails(): void {
    this.personService.getAccountEmails()
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      emails => this.accountEmails = emails,
      err => this.notificationService.notify('leads.form.notification.accountEmailsError', NotificationType.ERROR)
    );
  }

  private getProcessDefinitions(): void {
    this.leadService.getProcessDefinitions('LEAD')
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      definitions => this.processDefinitions = definitions,
      err => this.notificationService.notify('leads.form.notification.processDefinitionError', NotificationType.ERROR)
    );
  }

  private getPrimaryProcesses(): void {
    if (!this.leadSave || !this.update) {
      this.leadService.getAllPrimaryProcesses()
        .pipe(
          takeUntil(this.componentDestroyed)
        ).subscribe(
        primaryProcesses => {
          const primaryProcess = primaryProcesses.find(process => process.type === 'LEAD');
          if (primaryProcess) {
            this.primaryProcessId = primaryProcess.processId;
            this.processDefinitionId.patchValue(primaryProcess.processId);
          }
        }
      );
    }
  }

  private handleFoundPlaces(places: FoundPlace[]): void {
    this.foundPlaces = places;
    if (!places || !places.length) {
      this.mapService.notifyPlaceNotFound();
    }
  }

  private createLeadForm(): void {
    this.leadForm = this.fb.group({
      processDefinitionId: [this.primaryProcessId, Validators.required],
      companyNip: [null, Validators.maxLength(10)],
      companyName: [null, Validators.maxLength(300)],
      wwwAddress: [null, Validators.maxLength(200)],
      streetAndNumber: [null, Validators.maxLength(300)],
      city: [null, Validators.maxLength(200)],
      postalCode: [null, Validators.maxLength(20)],
      province: [null, Validators.maxLength(200)],
      country: [this.defaultCountry.name, Validators.maxLength(200)],
      notes: [null, Validators.maxLength(1000)],
      firstName: [null, Validators.maxLength(200)],
      lastName: [null, [Validators.required, FormValidators.whitespace, Validators.maxLength(200)]],
      email: [null, [Validators.email, Validators.maxLength(200)]],
      phone: [null, Validators.maxLength(128)],
      additionalPhones: this.fb.array([]),
      leadKeeperId: [null],
      sourceOfAcquisition: [this.undefinedCategoryId],
      subSourceOfAcquisition: [this.undefinedCategoryId],
      subjectOfInterest: [this.undefinedCategoryId],
      interestedInCooperation: [false, [Validators.required]],
      industrySearch: [null],
      industry: [null],
      positionSearch: [null],
      position: [null],
      avatar: [null],
      priority: [LeadPriority.LOW],
      scoring: new FormControl({ value: '', disabled: true }),
    });
    this.listenForSourceOfAcquisitionChanges();
    this.applyPermissionsToForm();
  }

  private subscribeForPriority(): void {
    this.priority.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(priority => this.scoring.patchValue(LeadScoring[priority]));
  }

  private subscribeForSliderClose(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        whenSliderClosed(this.componentDestroyed, ['LeadList.AddSlider', 'LeadList.EditSlider']),
      ).subscribe(
      () => {
        this.leadForm.reset();
        this.sourceOfAcquisition.patchValue(this.undefinedCategoryId);
        this.subSourceOfAcquisition.patchValue(this.undefinedCategoryId);
        this.subjectOfInterest.patchValue(this.undefinedCategoryId);
        this.position.patchValue(this.undefinedCategoryId);
        this.industry.patchValue(this.undefinedCategoryId);
        this.processDefinitionId.patchValue(this.primaryProcessId);
        this.country.patchValue(this.defaultCountry.name);
        this.priority.patchValue(LeadPriority.LOW);
        this.scoring.patchValue(LeadScoring.LOW);
        this.showPlace(this.defaultCountry);
        this.address = null;
        this.extents = {
          city: null,
          country: null,
        };
      }
    );
  }

  private applyPermissionsToForm(): void {
    if (this.leadForm) {
      applyPermission(this.processDefinitionId, !this.authService.isPermissionPresent(`${this.authKey}processDefinitionField`, 'r'));
      applyPermission(this.industry, !this.authService.isPermissionPresent(`${this.authKey}companyTypeField`, 'r'));
      applyPermission(this.companyNip, !this.authService.isPermissionPresent(`${this.authKey}companyNipField`, 'r'));
      applyPermission(this.companyName, !this.authService.isPermissionPresent(`${this.authKey}companyNameField`, 'r'));
      applyPermission(this.wwwAddress, !this.authService.isPermissionPresent(`${this.authKey}wwwAddressField`, 'r'));
      applyPermission(this.streetAndNumber, !this.authService.isPermissionPresent(`${this.authKey}streetAndNumberField`, 'r'));
      applyPermission(this.city, !this.authService.isPermissionPresent(`${this.authKey}cityField`, 'r'));
      applyPermission(this.postalCode, !this.authService.isPermissionPresent(`${this.authKey}postalCodeField`, 'r'));
      applyPermission(this.province, !this.authService.isPermissionPresent(`${this.authKey}provinceField`, 'r'));
      applyPermission(this.country, !this.authService.isPermissionPresent(`${this.authKey}countryField`, 'r'));
      applyPermission(this.notes, !this.authService.isPermissionPresent(`${this.authKey}notesField`, 'r'));
      applyPermission(this.priority, !this.authService.isPermissionPresent(`${this.authKey}priorityField`, 'r'));
      applyPermission(this.firstName, !this.authService.isPermissionPresent(`${this.authKey}firstNameField`, 'r'));
      applyPermission(this.lastName, !this.authService.isPermissionPresent(`${this.authKey}lastNameField`, 'r'));
      applyPermission(this.position, !this.authService.isPermissionPresent(`${this.authKey}positionField`, 'r'));
      applyPermission(this.email, !this.authService.isPermissionPresent(`${this.authKey}emailField`, 'r'));
      applyPermission(this.phone, !this.authService.isPermissionPresent(`${this.authKey}phoneField`, 'r'));
      // TODO(ekon): add permissions for additionalPhones
      applyPermission(this.leadKeeperId, !this.authService.isPermissionPresent(`${this.authKey}leadKeeperField`, 'r'));
      applyPermission(this.sourceOfAcquisition, !this.authService.isPermissionPresent(`${this.authKey}sourceOfAcquisitionField`, 'r'));
      applyPermission(this.subSourceOfAcquisition, !this.authService.isPermissionPresent(`${this.authKey}sourceOfAcquisitionField`, 'r'));
      applyPermission(this.subjectOfInterest, !this.authService.isPermissionPresent(`${this.authKey}subjectOfInterestField`, 'r'));
      applyPermission(this.interestedInCooperation,
        !this.authService.isPermissionPresent(`${this.authKey}interestedInCooperationField`, 'r'));
    }
  }

  showAndUpdateForm(place: FoundPlace): void {
    this.showPlace(place);
  }

  private showPlace(place: any): void {
    this.center = this.mapService.getCenter(place);
    this.markers = this.mapService.getSingleMarkerFromFoundPlace(place, this.updateAddressInForm.bind(this));
  }

  private updateAddressInForm(place: FoundPlace): void {
    if (['house', 'street', 'city', 'country'].includes(place.type)) {
      this.mapService.getAddressDetails(place)
      .subscribe(place => {
        this.patchAddressForm(place.address);
      });
    }

    if (place.type === 'city' || place.type === 'country') {
      this.address = {
        street: place.street,
        city: place.type === 'city' ? place.name : place.city,
        state: place.state,
        zip: place.postcode,
        country: place.type === 'country' ? place.name : place.country,
      };

      this.extents = {
        ...this.extents,
        [place.type]: place.extent,
      };
    }

    this.patchAddressForm(place.address || place);
    this.showPlace(place);
  }

  patchAddressForm(address) {
    this.streetAndNumber.patchValue(this.getStreetAndNumberStringValue(address.road, address.house_number));
    this.city.patchValue(address.city || address.town || address.county);
    this.postalCode.patchValue(address.postcode);
    this.province.patchValue(address.state);
    this.country.patchValue(address.country);
  }

  private subscribeForSliderOpen(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        whenSliderOpen(this.componentDestroyed, ['LeadList.AddSlider']),
      ).subscribe(
      () => this.leadKeeperId.patchValue(+localStorage.getItem('loggedAccountId'))
    );
  }

  private listenForSourceOfAcquisitionChanges(): void {
    this.sourceOfAcquisition.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      filter(value => !!value),
      distinctUntilChanged((x, y) => isEqual(x, y)),
    ).subscribe(
      (category: number) => {
        if (category === undefined || category === null) {
          this.sourceOfAcquisition.patchValue(this.undefinedCategoryId);
          return;
        }
        this.getSubSourceOfAcquisition();
      }
    );
  }

  getDataFromGUS($event: MouseEvent): void {
    $event.stopPropagation();
    if (this.companyNip.value && this.companyNip.value.length === 10) {
      this.customerService
        .getDataFromGUS(this.companyNip.value, null)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (customer: Customer) => this.handleSuccessfulImport(customer),
          (error) => this.notificationService.notifyError(this.gusErrors, error.status)
        );
    } else {
      this.notificationService.notify('leads.form.notification.nipNotValid', NotificationType.WARNING
      );
    }
  }

  private handleSuccessfulImport(customer: Customer): void {
    this.companyName.patchValue(customer.name);
    this.streetAndNumber.patchValue(customer.address.street);
    this.city.patchValue(customer.address.city);
    this.postalCode.patchValue(customer.address.zipCode);
    this.country.patchValue(customer.address.country);
    this.province.patchValue(customer.address.province);
    this.notificationService.notify('leads.form.notification.gus.successImportFromGus', NotificationType.SUCCESS);
  }

  openAddSourceOfAcquisitionSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.addSourceOfAcquisitionSlider.open();
  }

  onCancelAddSourceOfAcquisitionSlider(): void {
    this.addSourceOfAcquisitionSlider.close();
  }

  onSubmitAddSourceOfAcquisitionSlider(savedWord: SaveDictionaryWord): void {
    this.allSourceOfAcquisition.unshift({...savedWord, children: []});
    this.sourceOfAcquisition.patchValue(savedWord.id);
    this.addSourceOfAcquisitionSlider.close();
  }

  openAddSubSourceOfAcquisitionSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.parentWordIdForCreation = this.sourceOfAcquisition.value;
    this.addSubSourceOfAcquisitionSlider.open();
  }

  onCancelAddSubSourceOfAcquisitionSlider(): void {
    this.addSubSourceOfAcquisitionSlider.close();
  }

  onSubmitAddSubSourceOfAcquisitionSlider(savedWord: SaveDictionaryWord): void {
    const parentWord = this.getSelectedSourceOfAcquisition();
    parentWord.children.unshift({...savedWord, children: []});
    this.getSubSourceOfAcquisition();
    this.subSourceOfAcquisition.patchValue(savedWord.id);
    this.parentWordIdForCreation = null;
    this.addSubSourceOfAcquisitionSlider.close();
  }

  private getSelectedSourceOfAcquisition(): DictionaryWord {
    return this.allSourceOfAcquisition.find(word => word.id === this.sourceOfAcquisition.value);
  }

  addAdditionalPhone(event: Event): void {
    event.preventDefault();
    event.stopPropagation();
    this.addAdditionalPhoneNumbersSubject.next();
  }

  onCancelAddSubjectOfInterestSlider(): void {
    this.addSubjectOfInterestSlider.close();
  }

  openAddSubjectOfInterestSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.addSubjectOfInterestSlider.open();
  }

  onSubmitAddSubjectOfInterestSlider(savedWord: SaveDictionaryWord): void {
    this.allSubjectOfInterest.unshift({...savedWord, children: []});
    this.subjectOfInterest.patchValue(savedWord.id);
    this.addSubjectOfInterestSlider.close();
  }

  openAddPositionSlider(trigger: MatAutocompleteTrigger): void {
    trigger.closePanel();
    this.addPositionSlider.open();
  }

  onCancelAddPositionSlider(): void {
    this.addPositionSlider.close();
  }

  onSubmitAddPositionSlider(savedWord: SaveDictionaryWord): void {
    this.allPosition.unshift({...savedWord, children: []});
    const dictWord: DictionaryWord = {...savedWord, children: []};
    this.positionSelect(dictWord);
    this.addPositionSlider.close();
  }


  openAddIndustrySlider(trigger: MatAutocompleteTrigger): void {
    trigger.closePanel();
    this.addIndustrySlider.open();
  }

  onCancelAddIndustrySlider(): void {
    this.addIndustrySlider.close();
  }

  onSubmitAddIndustrySlider(savedWord: SaveDictionaryWord): void {
    this.allIndustry.unshift({...savedWord, children: []});
    const dictWord: DictionaryWord = {...savedWord, children: []};
    this.industrySelect(dictWord);
    this.addIndustrySlider.close();
  }

  private subscribeForPositionSearch(): void {
    this.positionSearch.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250)
    ).subscribe(
      (input: string) => {
        if (!input || input === '') {
          this.position.patchValue(null);
        }
      }
    );
  }

  private subscribeForIndustrySearch(): void {
    this.industrySearch.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250)
    ).subscribe(
      (input: string) => {
        if (!input || input === '') {
          this.industry.patchValue(null);
        }
      }
    );
  }

  industrySelect(industry: DictionaryWord): void {
    if (industry) {
      this.industry.patchValue(industry.id);
      this.industrySearch.patchValue(industry.name);
    }
  }

  positionSelect(position: DictionaryWord): void {
    if (position) {
      this.position.patchValue(position.id);
      this.positionSearch.patchValue(position.name);
    }
  }

  private subscribeForProvinceAndPostalCodeSearch(): void {
    merge(this.province.valueChanges, this.postalCode.valueChanges)
      .pipe(
        takeUntil(this.componentDestroyed),
        debounceTime(250),
        tap(() => this.placeSearchLoading = true),
        switchMap(((input: string) => {
            const address: Address = {
              street: this.streetAndNumber.value,
              city: this.city.value,
              state: this.province.value,
              zip: this.postalCode.value,
              country: this.country.value,
            };
            return this.mapService.getCoordinatesByAddress(address);
          }
        ))).subscribe(
      (places: FoundPlace[]) => {
        this.foundPlaces = places;
      }
    );
  }

  private subscribeForPlaceSearch(): void {
    const subscribeFor = (observable: Observable<any>, type: string) => {
      observable.pipe(
        takeUntil(this.componentDestroyed),
        debounceTime(250),
        tap(() => this.placeSearchLoading = true),
        switchMap(((input: string) => {
          if (this.focusedElement === type) {
            let extentType = null;

            if (this.address && this.extents) {
              if (type === 'street' && this.address.city) { extentType = 'city'; }
              else if (type === 'street' && this.address.country) { extentType = 'country'; }

              if (type === 'city' && this.address.country) { extentType = 'country'; }
            }

            return this.mapService.getCoordsAutocomplete(input, type, this.address, extentType && this.extents[extentType]);
          } else { return []; }
        }
        ))).subscribe(
      (places: PhotonResponcePlace[]) => {
        this.foundPlaces = places
          .map(place => {
            place.properties.display_name = this.getDisplayNameForPlace(place.properties);
            place.properties.lon = place.geometry.coordinates[0];
            place.properties.lat = place.geometry.coordinates[1];
            return place.properties;
          });
      });
    };

    subscribeFor(this.city.valueChanges, 'city');
    subscribeFor(this.streetAndNumber.valueChanges, 'street');
    subscribeFor(this.country.valueChanges, 'country');
  }

  getDisplayNameForPlace(properties: FoundPlace): string {
    const propertiesForDescription = ['name', 'street', 'housenumber', 'city', 'state', 'country', 'postcode'];
    let display_name = '';

    propertiesForDescription.forEach(key => {
      const value = properties[key];
      if (value) { display_name += `${display_name && ', '}${value}`; }
    });

    return display_name;
  }

  emptyIfNullOrUndefined(value): any {
    if (value === null || value === undefined) {
      return '';
    } else {
      return value;
    }
  }

  private getStreetAndNumberStringValue(road: string, house_number: string): string {
    if ((road === null || road === undefined) && (house_number === null || house_number === undefined)) {
      return null;
    } else {
      return `${this.emptyIfNullOrUndefined(road)} ${this.emptyIfNullOrUndefined(house_number)}`;
    }
  }

  resetFocusedElement() {
    this.focusedElement = null;
  }

  resetPlaces(event): void {
    this.focusedElement = event.target.name;
    this.foundPlaces = [];
  }
}
