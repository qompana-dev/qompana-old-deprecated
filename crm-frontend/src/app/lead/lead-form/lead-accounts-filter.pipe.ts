import { Pipe, PipeTransform } from '@angular/core';
import {AccountEmail} from '../../person/person.model';
import {LeadSave} from '../lead.model';

@Pipe({
  name: 'leadAccountsFilter'
})
export class LeadAccountsFilterPipe implements PipeTransform {
  transform(list: AccountEmail[], lead: LeadSave, update: boolean): AccountEmail[] {
    const loggedAccountId = +localStorage.getItem('loggedAccountId');
    if (!update || (lead && lead.creator === loggedAccountId)) {
      return list;
    }
    if (lead && lead.leadKeeperId === loggedAccountId) {
      return list.filter((email: AccountEmail) => {
        return email.id === lead.creator || email.id === lead.leadKeeperId;
      });
    }
    return [];
  }
}
