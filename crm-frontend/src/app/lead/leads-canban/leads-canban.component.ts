import {Component, EventEmitter, OnDestroy, OnInit, Output, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {LeadService} from '../lead.service';
import {KanbanLead, KanbanProcessOverview, KanbanTaskDetails} from '../lead.model';
import {Subject} from 'rxjs';
import {concatMap, distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';
import {
  CdkDrag,
  CdkDragDrop,
  CdkDragRelease,
  CdkDragStart,
  moveItemInArray,
  transferArrayItem
} from '@angular/cdk/drag-drop';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {NOT_FOUND} from 'http-status-codes';
import {SliderComponent} from '../../shared/slider/slider.component';
import {SliderService} from '../../shared/slider/slider.service';
import {DeleteDialogData} from '../../shared/dialog/delete/delete-dialog.component';
import {MatDialog, MatExpansionPanel} from '@angular/material';
import {Router} from '@angular/router';
import {BpmnUtil} from '../../bpmn/bpmn.util';
import {DeleteDialogService} from '../../shared/dialog/delete/delete-dialog.service';
import {FilterStrategy} from "../../shared/model";

@Component({
  selector: 'app-leads-canban',
  templateUrl: './leads-canban.component.html',
  styleUrls: ['./leads-canban.component.scss']
})
export class LeadsCanbanComponent implements OnInit, OnDestroy {

  processOverview: KanbanProcessOverview[];
  processDetails: KanbanTaskDetails[];
  @Output() onLeadEdit: EventEmitter<number> = new EventEmitter();
  @ViewChild('mapSlider') mapSlider: SliderComponent;
  @ViewChildren('placeholder') placeholders: QueryList<any>;
  @ViewChildren('expansionPanel') panels: QueryList<MatExpansionPanel>;
  moveImpossibleVisibilityMap: Map<number, boolean> = new Map();
  leadDuringAcceptanceVisibilityMap: Map<number, boolean> = new Map();
  listDisabled = false;
  bpmnUtil = BpmnUtil;
  private componentDestroyed: Subject<void> = new Subject();
  private currentlyOpenedProcessId: string;

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'leads.form.dialog.delete.title',
    description: 'leads.form.dialog.delete.description',
    noteFirst: 'leads.form.dialog.delete.noteFirstPart',
    noteSecond: 'leads.form.dialog.delete.noteSecondPart',
    confirmButton: 'leads.form.dialog.delete.confirm',
    cancelButton: 'leads.form.dialog.delete.cancel',
  };
  private readonly leadErrorTypes: ErrorTypes = {
    base: 'leads.list.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'leadNotFound'
      }
    ]
  };
  private readonly getProcessErrorTypes: ErrorTypes = {
    base: 'leads.kanban.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'processNotFound'
      }
    ]
  };
  private readonly changeTaskErrorTypes: ErrorTypes = {
    base: 'leads.kanban.notification.',
    defaultText: 'taskCompletedError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'noTaskFound'
      }
    ]
  };


  constructor(private leadService: LeadService,
              private notificationService: NotificationService,
              private deleteDialogService: DeleteDialogService,
              private sliderService: SliderService,
              private dialog: MatDialog,
              private $router: Router) {
  }

  ngOnInit(): void {
    this.getKanbanOverview();
    this.subscribeForSliderClose();
    this.subscribeForAddSliderOpen();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  getProcess(processDefinitionId: string): void {
    if (!processDefinitionId) {
      return;
    }
    this.processDetails = null;
    this.leadService.getProcessKanbanDetails(processDefinitionId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      details => this.processDetails = details,
      err => this.notificationService.notifyError(this.getProcessErrorTypes, err.status)
    );
  }

  drop(event: CdkDragDrop<KanbanTaskDetails>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data.leads, event.previousIndex, event.currentIndex);
    } else {
      this.changeStep(event);
    }
  }

  cdkDragReleased($event: CdkDragRelease<any>): void {
    this.moveImpossibleVisibilityMap.clear();
    this.leadDuringAcceptanceVisibilityMap.clear();
  }

  cdkDragStarted($event: CdkDragStart<any>): void {
    this.scrollPlaceholderToView();
    const containerIndex: number = this.getContainerIndexEventComesFrom($event);
    const kanbanLead = $event.source.data[0];
    if (!kanbanLead.canBeMovedToFurtherStep) {
      this.moveImpossibleVisibilityMap.set(containerIndex + 1, true);
    }
    if (kanbanLead.duringAcceptance) {
      this.leadDuringAcceptanceVisibilityMap.set(containerIndex + 1, true);
    }
  }

  getPredicateFunction(columnIndex: number): (item: CdkDrag<any>) => boolean {
    return (item) => {
      const index = item.data[1];
      return ((columnIndex <= index) || ((index + 1 === columnIndex) && item.data[0].canBeMovedToFurtherStep
        && !item.data[0].duringAcceptance));
    };
  }

  scrollToTop(el: HTMLElement): void {
    el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
  }

  goToDetails(leadId: number): void {
    this.$router.navigate(['/leads', leadId]);
  }

  getTaskWeight(): number {
    return !!this.processDetails ? 100 / this.processDetails.length : 0;
  }

  edit(leadId: number): void {
    this.onLeadEdit.emit(leadId);
  }

  saveCurrentlyOpened(id: string): void {
    this.currentlyOpenedProcessId = id;
  }

  tryDeleteLead(listIndex: number, lead: KanbanLead): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteLead(listIndex, lead);
    this.openDeleteDialog();
  }

  clearCurrentlyOpened(): void {
    this.currentlyOpenedProcessId = null;
  }


  private getKanbanOverview(): void {
    this.leadService.getProcessKanbanOverview(FilterStrategy.OPENED)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      overview => this.processOverview = overview,
      err => this.notificationService.notify('leads.kanban.notification.getProccessesOverviewError', NotificationType.ERROR)
    );
  }

  private deleteLead(listIndex: number, lead: KanbanLead): void {
    this.leadService.removeLead(lead.id).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      ok => {
        this.removeFromKanban(listIndex, lead);
        this.notificationService.notify('leads.list.notification.deleteLeadSuccess', NotificationType.SUCCESS);
      },
      err => this.notificationService.notifyError(this.leadErrorTypes, err.status)
    );
  }

  private changeStep(event: CdkDragDrop<KanbanTaskDetails>): void {
    this.moveLeadBetweenColumns(event);
    this.listDisabled = true;
    const kanbanLead: KanbanLead = this.getLeadFrom(event);
    const taskDetails: KanbanTaskDetails = event.container.data;
    const currentTaskNumber: number = this.getTaskNumber(kanbanLead.activeTaskId);
    const targetTaskNumber: number = this.getTaskNumber(taskDetails.id);

    if (this.isNextTask(currentTaskNumber, targetTaskNumber) && kanbanLead.canBeMovedToFurtherStep && !kanbanLead.duringAcceptance) {
      this.completeTask(event);
    } else if (this.isPreviousTask(targetTaskNumber, currentTaskNumber)) {
      this.moveTaskBack(event, currentTaskNumber - targetTaskNumber);
    }
  }

  private completeTask(event: CdkDragDrop<KanbanTaskDetails>): void {
    const kanbanLead: KanbanLead = this.getLeadFrom(event);
    this.leadService.completeTask(kanbanLead.processInstanceId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      response => this.handleTaskCompletion(response.status, kanbanLead, event),
      err => {
        this.revertLeadMoveBetweenColumns(event);
        this.notificationService.notifyError(this.changeTaskErrorTypes, err.status);
      },
      () => this.listDisabled = false
    );
  }

  private handleTaskCompletion(status: number, kanbanLead: KanbanLead, event: CdkDragDrop<KanbanTaskDetails>): void {
    if (status === 204) {
      this.notificationService.notify('leads.kanban.notification.taskCompleted', NotificationType.SUCCESS);
    } else if (status === 206) {
      this.revertLeadMoveBetweenColumns(event);
      this.notificationService.notify('leads.details.notifications.taskWaitingForAcceptance', NotificationType.SUCCESS);
    }
    this.leadService.getKanbanLead(kanbanLead.id).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (lead) => this.updateLead(kanbanLead, lead)
    );
  }

  private moveTaskBack(event: CdkDragDrop<KanbanTaskDetails>, howManyBack: number): void {
    const kanbanLead: KanbanLead = this.getLeadFrom(event);
    this.leadService.goBack(kanbanLead.processInstanceId, howManyBack)
      .pipe(
        takeUntil(this.componentDestroyed),
        concatMap(() => this.leadService.getKanbanLead(kanbanLead.id))
      ).subscribe(
      lead => {
        this.updateLead(kanbanLead, lead);
        this.notificationService.notify('leads.kanban.notification.successfulGoBack', NotificationType.SUCCESS);
      },
      err => {
        this.revertLeadMoveBetweenColumns(event);
        this.notificationService.notifyError(this.changeTaskErrorTypes, err.status);
      },
      () => this.listDisabled = false
    );
  }

  private getContainerIndexEventComesFrom($event: CdkDragStart<any>): number {
    return $event.source.data[1];
  }

  private subscribeForSliderClose(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        takeUntil(this.componentDestroyed),
        filter(change => change.key === 'LeadList.AddSlider' || change.key === 'LeadList.EditSlider'),
        map(change => change.opened),
        distinctUntilChanged(),
        filter(opened => !opened)
      ).subscribe(
      () => {
        if (this.currentlyOpenedProcessId) {
          this.getProcess(this.currentlyOpenedProcessId);
        }
        this.getKanbanOverview();
      }
    );
  }

  private subscribeForAddSliderOpen(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        takeUntil(this.componentDestroyed),
        filter(change => change.key === 'LeadList.AddSlider' || change.key === 'LeadList.EditSlider'),
        map(change => change.opened),
        distinctUntilChanged(),
        filter(opened => opened)
      ).subscribe(
      () => {
        this.closeAllPanels();
      }
    );
  }

  private closeAllPanels(): void {
    this.panels.forEach(item => item.close());
  }

  private removeFromKanban(listIndex: number, lead: KanbanLead): void {
    if (this.processDetails) {
      const leadsList = this.processDetails[listIndex].leads;
      const deletedIndex = leadsList.indexOf(lead);
      if (deletedIndex > -1) {
        leadsList.splice(deletedIndex, 1);
      }
    }
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private getLeadFrom(event: CdkDragDrop<KanbanTaskDetails>): KanbanLead {
    return event.item.data[0];
  }

  private isPreviousTask(targetTaskNumber: number, currentTaskNumber: number): boolean {
    return targetTaskNumber < currentTaskNumber;
  }

  private isNextTask(currentTaskNumber: number, targetTaskNumber: number): boolean {
    return currentTaskNumber + 1 === targetTaskNumber;
  }

  private moveLeadBetweenColumns(event: CdkDragDrop<KanbanTaskDetails>): void {
    transferArrayItem(event.previousContainer.data.leads, event.container.data.leads, event.previousIndex, event.currentIndex);
  }

  private revertLeadMoveBetweenColumns(event: CdkDragDrop<KanbanTaskDetails>): void {
    transferArrayItem(event.container.data.leads, event.previousContainer.data.leads, event.currentIndex, event.previousIndex);
  }

  private getTaskNumber(taskId: string): number {
    return +taskId.replace('task', '');
  }

  private updateLead(target: KanbanLead, source: KanbanLead): void {
    Object.assign(target, source);
  }

  private scrollPlaceholderToView(): void {
    setTimeout(
      () => this.placeholders.forEach(item => item.nativeElement.parentElement.parentElement.scrollTop = 0), 100
    );
  }

  isLastDefaultTask(task: any): boolean {
    return !task.name;
  }

  convertToOpportunity(leadId: number): void {
    this.$router.navigate([`/leads/${leadId}`], {fragment: 'to-opportunity'});
  }
}

