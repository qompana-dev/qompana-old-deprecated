export enum SearchType {
  FIRST_NAME = 'FIRST_NAME',
  LAST_NAME = 'LAST_NAME',
  COMPANY_NAME = 'COMPANY_NAME',
  COMPANY_CITY = 'COMPANY_CITY',
  PHONE = 'PHONE',
  EMAIL = 'EMAIL',
  SOURCE = 'SOURCE'
}

export interface SearchVariantGroups {
  type: SearchType;
  variants: string[];
}
