import {Component, EventEmitter, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {FormControl} from "@angular/forms";
import {debounceTime, distinctUntilChanged, filter, map, startWith, takeUntil} from "rxjs/operators";
import {Observable, of, Subject, Subscription} from "rxjs";
import {SearchVariantGroups} from "./lead-search.model";
import {LeadSearchService} from "./lead-search.service";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete/typings/autocomplete";

@Component({
  selector: 'app-lead-search',
  templateUrl: './lead-search.component.html',
  styleUrls: ['./lead-search.component.scss'],
})
export class LeadSearchComponent implements OnInit {

  @Output() searchEvent: EventEmitter<string> = new EventEmitter<string>();

  searchControl = new FormControl();
  search = '';
  isSearched = false;
  searchVariants: SearchVariantGroups[] = [];
  searchVariantsObservable: Observable<SearchVariantGroups[]> = of(this.searchVariants);

  private componentDestroyedNotifier: Subject<void> = new Subject();
  private searchSubscription: Subscription;


  constructor(private leadSearchService: LeadSearchService) {
  }

  ngOnInit(): void {
    this.initSearchField();
  }

  private initSearchField(): void {
    this.searchSubscription = this.searchControl.valueChanges
      .pipe(
        startWith(''),
        map(value => value.trim()),
        filter(value => !value || value.length > 0),
        debounceTime(500),
        distinctUntilChanged(),
      ).subscribe(value => {
        this.search = value;
        this.initMembers();
      });
  }

  private initMembers(): void {
    this.componentDestroyedNotifier.next();
    if (this.search.length > 0) {
      this.leadSearchService.getSearchVariants(this.search)
        .pipe(
          takeUntil(this.componentDestroyedNotifier)
        ).subscribe(
        response => this.handleInitSuccess(response)
      );
    }
  }

  private handleInitSuccess(response: SearchVariantGroups[]): void {
    this.searchVariants = response;
    this.searchVariantsObservable = of(this.searchVariants.filter(group => group.variants.length > 0));
  }

  selectSearchVariant({option}: MatAutocompleteSelectedEvent): void {
    this.search = option.value;
    this.emitSearchEvent();
  }

  emitSearchEvent(): void {
    if (this.search.length > 0) {
      this.isSearched = true;
      this.searchEvent.emit(this.search);
    }
  }

  clearSearch(): void {
    this.search = '';
    this.searchControl.setValue('');
    this.isSearched = false;
    this.searchVariantsObservable = of([]);
    this.searchEvent.emit(this.search);
  }


}
