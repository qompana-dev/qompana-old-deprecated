import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { LeadService } from '../lead.service';
import { FilterStrategy } from '../../shared/model';
import { LeadOnList, LeadSave } from '../lead.model';
import { Observable, Subject } from 'rxjs';
import { MatCheckboxChange, Sort } from '@angular/material';
import { finalize, map, startWith, takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { Page, PageRequest } from '../../shared/pagination/page.model';
import { SliderComponent } from '../../shared/slider/slider.component';
import { DeleteDialogData } from '../../shared/dialog/delete/delete-dialog.component';
import {
  ErrorTypes,
  NotificationService,
  NotificationType,
} from '../../shared/notification.service';
import { NOT_FOUND } from 'http-status-codes';
import { PersonService } from '../../person/person.service';
import { AuthService } from '../../login/auth/auth.service';
import { DeleteDialogService } from '../../shared/dialog/delete/delete-dialog.service';
import { saveAs } from 'file-saver';
import { ImportService } from '../../shared/import/import.service';
import { LoginService } from '../../login/login.service';
import { FileItem } from 'ng2-file-upload';
import { ParseExceptionDetailDto } from '../../shared/import/import.model';
import { TranslateService } from '@ngx-translate/core';
import { LeadImportDialogData } from '../lead-import/lead-import-dialog/lead-import.model';
import { SearchVariantGroups } from './lead-search/lead-search.model';

@Component({
  selector: 'app-lead-list',
  templateUrl: './lead-list.component.html',
  styleUrls: ['./lead-list.component.scss'],
})
export class LeadListComponent implements OnInit, OnDestroy {

  constructor(
    public leadService: LeadService,
    private deleteDialogService: DeleteDialogService,
    private notificationService: NotificationService,
    private personService: PersonService,
    private $router: Router,
    private translateService: TranslateService,
    private loginService: LoginService,
    private authService: AuthService
  ) {}
  columns = [
    'selectRow',
    'avatar',
    'firstName',
    'external',
    'position',
    'company.companyName',
    'company.city',
    'phone',
    'email',
    'created',
    'status',
    'process',
    'priority',
    'leedKeeper',
    'action',
  ];

  expanded = false;
  filterSelection = FilterStrategy;
  selectedFilter: FilterStrategy = FilterStrategy.OPENED;
  search = '';
  leadsPage: Page<LeadOnList>;
  selectedLeads: LeadOnList[] = [];
  loadingFinished = true;
  selectedLead: LeadSave;
  userMap: Map<number, string>;
  @ViewChild('editSlider') editSlider: SliderComponent;
  @ViewChild('addSlider') addSlider: SliderComponent;
  private componentDestroyed: Subject<void> = new Subject();
  private fetchLeads: Subject<void> = new Subject();

  filteredColumns$: Observable<string[]>;

  private currentSort = 'updated,desc';
  private previousSort = 'updated,desc';
  private pageOnList = 20;
  totalElements = 0;

  kanbanVisible: boolean;
  currentAccountId: number;

  importService: ImportService;

  leadId: number;
  focusOwnerForm = false;
  @ViewChild('changeOwnerSlider') changeOwnerSlider: SliderComponent;

  public readonly leadImportDialogData: Partial<LeadImportDialogData> = {};

  private readonly importErrorDialogData: Partial<DeleteDialogData> = {
    title: 'leads.list.importErrorDialog.title',
    description: 'leads.list.importErrorDialog.description',
    confirmButton: 'leads.list.importErrorDialog.tryAgain',
    cancelButton: 'leads.list.importErrorDialog.cancel',
  };

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'leads.form.dialog.delete.title',
    description: 'leads.form.dialog.delete.description',
    noteFirst: 'leads.form.dialog.delete.noteFirstPart',
    noteSecond: 'leads.form.dialog.delete.noteSecondPart',
    confirmButton: 'leads.form.dialog.delete.confirm',
    cancelButton: 'leads.form.dialog.delete.cancel',
  };

  private readonly leadErrorTypes: ErrorTypes = {
    base: 'leads.list.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'leadNotFound',
      },
    ],
  };

  private static getExportName(): string {
    const date = moment().format('YYYY-MM-DD');
    return 'leads__' + date + '.csv';
  }

  ngOnInit(): void {
    this.getAccountEmails();
    this.getLeads(FilterStrategy.OPENED);

    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
    this.currentAccountId = +localStorage.getItem('loggedAccountId');

    this.importService = new ImportService(this.loginService, 'lead');
    this.importService.onAfterAddingFile = (fileItem: FileItem) =>
      this.import(fileItem);
    this.leadImportDialogData.reloadLeadsCallback = () =>
      this.handleLeadsImported();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.fetchLeads.next();
    this.componentDestroyed.complete();
    this.fetchLeads.complete();
  }

  public tryDeleteLead(leadId: number): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteLead(leadId);
    this.openDeleteDialog();
  }

  public tryDeleteSelectedLeads(): void {
    this.deleteDialogData.numberToDelete = this.selectedLeads.length;
    this.deleteDialogData.onConfirmClick = () => this.deleteLeads();
    this.openDeleteDialog();
  }

  public deleteLeads(): void {
    if (this.selectedLeads.length === 0) {
      return;
    }
    this.leadService
      .removeLeads(this.selectedLeads)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.getLeads(this.selectedFilter);
          this.notificationService.notify(
            'leads.list.notification.deleteLeadsSuccess',
            NotificationType.SUCCESS
          );
          this.selectedLeads = [];
        },
        (err) =>
          this.notificationService.notifyError(this.leadErrorTypes, err.status)
      );
  }

  public export(): void {
    this.leadService
      .exportLeads()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((blob: Blob) =>
        saveAs(blob, LeadListComponent.getExportName())
      );
  }

  private import(fileItem: FileItem): void {
    fileItem.onSuccess = () => {
      this.notificationService.notify(
        'leads.list.notification.importLeadSuccess',
        NotificationType.SUCCESS
      );
      this.getLeads(this.selectedFilter);
      this.importService.clearQueue();
    };
    fileItem.onError = (error: string) => {
      this.importErrorDialogData.onConfirmClick = () =>
        this.importService.uploadItem(fileItem);
      this.importErrorDialogData.onCancelClick = () =>
        this.importService.clearQueue();
      this.importErrorDialogData.noteFirst = undefined;
      try {
        if (
          error &&
          error.length > 3 &&
          error[0] === '[' &&
          error[error.length - 1] === ']'
        ) {
          this.importErrorDialogData.noteFirst =
            'leads.list.importErrorDialog.incorrectCsv';
          const errorList: ParseExceptionDetailDto[] = JSON.parse(error);
          this.importErrorDialogData.errorList = errorList.map((item) => {
            const fieldName = this.translateService.instant(
              'leads.list.importErrorDialog.types.' + item.field
            );
            return this.translateService.instant(
              'leads.list.importErrorDialog.errorMsg',
              { field: fieldName, line: item.line, value: item.value }
            );
          });
        }
      } catch (e) {
        console.log('parse msg error');
      }
      this.deleteDialogService.openWithoutHeight(this.importErrorDialogData);
    };
    this.importService.uploadItem(fileItem);
  }

  public expand(): void {
    this.expanded = true;
    this.kanbanVisible = false;
  }

  public collapse(): void {
    this.expanded = false;
    this.kanbanVisible = false;
  }

  public getLeadProgress(lead: LeadOnList): number {
    return lead.maxStatus ? (lead.status / lead.maxStatus) * 100 : 0;
  }

  public isSelected(selection: FilterStrategy): boolean {
    return this.selectedFilter === selection;
  }

  public filter(selection: FilterStrategy): void {
    this.changeSortIfNecessary(selection);
    this.selectedFilter = selection;
    this.getLeads(selection);
  }

  public toggleLead($event: MatCheckboxChange, lead: LeadOnList): void {
    if ($event.checked) {
      this.selectedLeads.push(lead);
    } else {
      const index = this.selectedLeads.indexOf(lead);
      if (index !== -1) {
        this.selectedLeads.splice(index, 1);
      }
    }
  }

  public handleLeadAdded(): void {
    this.getLeads(this.selectedFilter);
    this.addSlider.close();
  }

  public edit(leadId: number): void {
    this.leadService
      .getLeadForEdit(leadId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (lead) => {
          this.selectedLead = lead;
          this.editSlider.open();
        },
        (err) =>
          this.notificationService.notifyError(this.leadErrorTypes, err.status)
      );
  }

  public handleLeadEdited(): void {
    this.getLeads(this.selectedFilter);
    this.editSlider.close();
  }

  public handleLeadsImported(): void {
    this.getLeads(this.selectedFilter);
  }

  public showCanban(): void {
    this.kanbanVisible = !this.kanbanVisible;
  }

  public goToLeadDetails(id: number): void {
    this.$router.navigate(['/leads', id]);
  }

  public goToLeadDetailsNewTab (id: number): void {
    const url = this.$router.serializeUrl(
      this.$router.createUrlTree(['leads', id])
    );
    window.open(url, '_blank');
  }

  public onSortChange($event: Sort): void {
    this.currentSort = $event.direction
      ? $event.active + ',' + $event.direction
      : 'updated,desc';
    this.getLeads(this.selectedFilter);
  }

  public transferSelectedCustomers(): void {
    this.changeOwnerSlider.toggle();
  }

  private deleteLead(leadId: number): void {
    this.leadService
      .removeLead(leadId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.getLeads(this.selectedFilter);
          this.notificationService.notify(
            'leads.list.notification.deleteLeadSuccess',
            NotificationType.SUCCESS
          );
        },
        (err) =>
          this.notificationService.notifyError(this.leadErrorTypes, err.status)
      );
  }

  changePagination(page) {
    const { pageIndex, pageSize } = page;
    this.pageOnList = pageSize;
    this.getLeads(this.selectedFilter, {
      page: pageIndex,
      size: pageSize,
      sort: this.currentSort,
    });
  }

  public setSelectedSearch(search: string): void {
    this.search = search;
    this.getLeads(this.selectedFilter);
  }

  public getLeads(
    selection: FilterStrategy,
    pageRequest: PageRequest = {
      page: '0',
      size: this.pageOnList + '',
      sort: this.currentSort,
    }
  ): void {
    this.fetchLeads.next();
    this.loadingFinished = false;
    this.leadService
      .getLeads(selection, pageRequest, this.search)
      .pipe(
        takeUntil(this.fetchLeads),
        finalize(() => (this.loadingFinished = true))
      )
      .subscribe(
        (page) => this.handleSuccess(page),
        () =>
          this.notificationService.notify(
            'leads.list.notification.listError',
            NotificationType.ERROR
          )
      );
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private getAccountEmails(): void {
    this.personService.getAndMapAccountEmails(this.componentDestroyed)
      .subscribe(
        (users) => (this.userMap = users),
        () => this.notificationService.notify('leads.form.notification.accountEmailsError', NotificationType.ERROR)
      );
  }

  private handleSuccess(page: any): void {
    this.leadsPage = page;
    this.totalElements = page.totalElements;
    this.clearSelectedLeads();
  }

  private getFilteredColumns(): string[] {
    return this.columns.filter(
      (item) =>
        !this.authService.isPermissionPresent(
          'LeadListComponent.' + item.replace('company.', '') + 'Column',
          'i'
        ) ||
        item === 'action' ||
        item === 'selectRow' ||
        item === 'external'
    );
  }

  exists(item): boolean {
    return this.selectedLeads.indexOf(item) > -1;
  }

  isIndeterminate(): boolean {
    return this.selectedLeads.length > 0 && !this.isChecked();
  }

  isChecked(): boolean {
    if (!this.leadsPage || !this.leadsPage.content) {
      return false;
    }
    return this.selectedLeads.length === this.leadsPage.content.length;
  }

  clearSelectedLeads(): void {
    this.selectedLeads.length = 0;
  }

  toggleAll(event: MatCheckboxChange) {
    if (event.checked) {
      this.clearSelectedLeads();
      this.leadsPage.content.forEach((row) => {
        this.selectedLeads.push(row);
      });
    } else {
      this.clearSelectedLeads();
    }
  }

  isAnyCustomerChecked(): boolean {
    return this.isIndeterminate() || this.isChecked();
  }

  private changeSortIfNecessary(selection: FilterStrategy) {
    if (selection === FilterStrategy.VIEWED_LASTLY) {
      this.previousSort = this.currentSort;
      this.currentSort = 'lastViewedLeads.lastViewed,desc';
    }
    if (
      selection !== FilterStrategy.VIEWED_LASTLY &&
      this.currentSort.includes('lastViewedLeads.lastViewed')
    ) {
      this.currentSort = this.previousSort;
    }
  }
}
