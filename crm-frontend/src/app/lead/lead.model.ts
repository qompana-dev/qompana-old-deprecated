import {AdditionalPhoneNumber} from '../shared/additional-phone-numbers/additional-phone-numbers.model';
import {FinishOpportunityResult} from "../opportunity/opportunity.model";

export enum LeadPriority {
  LOW = 'LOW',
  MEDIUM = 'MEDIUM',
  HIGH = 'HIGH',
}

export enum LeadScoring {
  LOW = 1,
  MEDIUM = 3,
  HIGH = 5,
}

export const PriorityRadioButtons = [
  {
    class: 'low-radio-button',
    value: LeadPriority.LOW,
    translation: 'leads.form.leadPriority.low',
  },
  {
    class: 'medium-radio-button',
    value: LeadPriority.MEDIUM,
    translation: 'leads.form.leadPriority.medium',
  },
  {
    class: 'high-radio-button',
    value: LeadPriority.HIGH,
    translation: 'leads.form.leadPriority.high',
  },
];

export interface LeadSave {
  id: number;
  processDefinitionId: string;
  companyId: number;
  industry: number;
  companyNip: string;
  companyName: string;
  wwwAddress: string;
  streetAndNumber: string;
  city: string;
  postalCode: string;
  province: string;
  country: string;
  notes: string;
  priority: LeadPriority;
  scoring: LeadScoring;
  firstName: string;
  lastName: string;
  position: number;
  email: string;
  phone: string;
  additionalPhones?: AdditionalPhoneNumber[];
  leadKeeperId: number;
  creator: number;
  sourceOfAcquisition: number;
  subSourceOfAcquisition: number;
  subjectOfInterest: number;
  interestedInCooperation: boolean;
}

export interface LeadsKeeper {
  leadKeeper: number;
  leadsIds: number[];
}

export interface LeadOnList {
  id: number;
  processInstanceId: string;
  processInstanceName: string;
  processInstanceVersion: number;
  leadKeeper: number;
  firstName: string;
  lastName: string;
  position: string;
  company: string;
  city: string;
  phone: string;
  email: string;
  creationDate: string;
  status: number;
  maxStatus: number;
  acceptanceUserId: number;
}

export interface ProcessDefinition {
  id: string;
  version: number;
  name: string;
  firstTaskName: string;
}


export interface KanbanProcessOverview {
  id: string;
  name: string;
  version: number;
  size: number;
  backgroundColor: string;
  textColor: string;
  tasks: KanbanTaskOverview[];
}

export interface KanbanTaskOverview {
  id: string;
  name: string;
  assignee: string;
  includedLeadsNumber: number;
  summaryValue: number;
}

export interface KanbanTaskDetails {
  id: string;
  name: string;
  leads: KanbanLead[];
}

export interface KanbanLead {
  id: number;
  firstName: string;
  lastName: string;
  position: string;
  companyName: string;
  phone: string;
  processInstanceId: string;
  activeTaskId: string;
  canBeMovedToFurtherStep: boolean;
  duringAcceptance: boolean;
}

export interface LeadConversion {
  contactId: number;
  contactName: string;
  contactPosition: string;
  isExistingContact: string;
  isNewContact: string;


  contactSurname: string;
  customerId: number;
  customerName: string;
  customerNip: string;
  isExistingCustomer: boolean;
  isNewCustomer: boolean;

  noOpportunity: boolean;
  opportunityAmount: string;
  opportunityClosingDate: string;
  opportunityCurrency: string;
  opportunityName: string;
  opportunityProcessDefinitionId: string;
}

export interface ConvertedOpportunity {
  customerName: string;
  customerNip: string;
  customerOwner: number;
  customerAddress: string;
  contactName: string;
  contactPhone: string;
  contactEmail: string;
  contactAddress: string;
  contactCustomerName: string;
  contactPosition: string;
  contactOwner: number;

  opportunityId: number;
  opportunityName: string;
  opportunityFinishDate: string;
  opportunityCustomerName: string;
  opportunityAmount: number;
  opportunityCurrency: string;
  opportunityOwner: number;
  opportunityProcessName: string;
  opportunitySubjectOfInterest: string;
}


export interface Address {
  road: string;
  neighbourhood: string;
  suburb: string;
  city_district: string;
  city: string;
  county: string;
  state: string;
  postcode: string;
  country: string;
  country_code: string;
  house_number: string;
}

export interface FoundPlace {
  place_id: number;
  licence: string;
  osm_type: string;
  osm_id: number;
  city: string;
  country: string;
  district: string;
  name: string;
  osm_value: string;
  state: string;
  type: string;
  postcode: string;
  street: string;
  housenumber: string;
  address: Address;
  display_name: string;
  extent: number[];
  lon: number;
  lat: number;
  importance: number;
}

export interface Geometry {
  coordinates: number[];
}

export interface PhotonResponcePlace {
  properties: FoundPlace;
  geometry: Geometry;
};

export interface PhotonResponce {
  features: PhotonResponcePlace[];
}

export enum CloseLeadResult {
  CLOSED = 'CLOSED',
  ARCHIVED = 'ARCHIVED',
}

export enum LeadRejectionReason {
  NONE = 'NONE',
  LOST = 'LOST',
  NO_BUDGET = 'NO_BUDGET',
  NO_DECISION = 'NO_DECISION',
  OTHER = 'OTHER',
}

export interface CanConvertResponseDto {
  canConvert: boolean;
  tasksRequiringAcceptance: string[];
  tasksRequiringFillingProperties: string[];
}

export interface SearchSimilarObjectsDto {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  companyName: string;
  wwwAddress: string;
  streetAndNumber: string;
}
