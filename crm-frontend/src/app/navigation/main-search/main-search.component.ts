import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
import * as groupBy from 'lodash/groupBy';
import {Subject} from 'rxjs';
import {debounceTime, filter, finalize, map, startWith, switchMap, takeUntil, tap} from 'rxjs/operators';
import {CrmObject, CrmObjectType} from '../../shared/model/search.model';
import {MainSearchService} from './main-search.service';
import {AuthService} from '../../login/auth/auth.service';

@Component({
  selector: 'app-main-search',
  templateUrl: './main-search.component.html',
  styleUrls: ['./main-search.component.scss']
})
export class MainSearchComponent implements OnInit, OnDestroy {

  searchTextForm: FormGroup = new FormGroup({
    searchType: new FormControl('all'),
    searchText: new FormControl('')
  });

  filteredList: CrmObject[] = [];
  groupedList: any = {};
  allowedSearchTypes: string[] = [CrmObjectType.lead, CrmObjectType.customer, CrmObjectType.file, CrmObjectType.opportunity];
  types: string[] = ['all'].concat(...this.allowedSearchTypes.map(value => value.toString()));
  filteredTypes: string[];
  areSearchLoading = false;

  @Input() textPlaceholder = '';
  @Input() groupList = false;
  @Output() itemSelected: EventEmitter<CrmObject> = new EventEmitter<CrmObject>();
  private componentDestroyed: Subject<void> = new Subject();


  // @formatter:off
  get searchType(): AbstractControl {
    return this.searchTextForm.get('searchType');
  }

  get searchText(): AbstractControl {
    return this.searchTextForm.get('searchText');
  }

  // @formatter:on


  constructor(
    private mainSearchService: MainSearchService,
    private authService: AuthService) {
  }

  ngOnInit(): void {
    this.searchType.valueChanges
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.searchText.patchValue(this.searchText.value));

    this.searchText.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      tap((input) => {
        if (!input || input.trim().length < 3) {
          this.groupedList = {};
        }
      }),
      filter((input: string) => !!input && input.trim().length >= 3),
      tap(() => this.areSearchLoading = true),
      switchMap(((input: string) => this.mainSearchService.searchForType(this.searchType.value, input.trim(), this.filteredTypes)
          .pipe(finalize(() => this.areSearchLoading = false))
      ))).subscribe(
      result => {
        this.filteredList = result;
        const tmpGroupedList = groupBy(result, 'type');

        // Order of groupedList properties is matter
        this.groupedList = {};

        if (tmpGroupedList[SortingTypes.LEAD.toString().toLowerCase()]) {
          this.groupedList.lead = tmpGroupedList[SortingTypes.LEAD.toString().toLowerCase()];
        }

        if (tmpGroupedList[SortingTypes.OPPORTUNITY.toString().toLowerCase()]) {
          this.groupedList.opportunity = tmpGroupedList[SortingTypes.OPPORTUNITY.toString().toLowerCase()];
        }

        if (tmpGroupedList[SortingTypes.CUSTOMER.toString().toLowerCase()]) {
          this.groupedList.customer = tmpGroupedList[SortingTypes.CUSTOMER.toString().toLowerCase()];
        }

        if (tmpGroupedList[SortingTypes.FILE.toString().toLowerCase()]) {
          this.groupedList.file = tmpGroupedList[SortingTypes.FILE.toString().toLowerCase()];
        }

      }
    );

    this.authService.onAuthChange.pipe(
      takeUntil(this.componentDestroyed),
      startWith(this.getFilteredTypes()),
      map(() => this.getFilteredTypes())
    ).subscribe(
      types => this.filteredTypes = types
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  onSelect(crmObject: CrmObject): void {
    this.itemSelected.emit(crmObject);
  }

  getKeys(object: any): string[] {
    return object ? Object.keys(object) : [];
  }

  isOneLineResult(type: string): boolean {
    return type === CrmObjectType.file.toString();
  }

  private getFilteredTypes(): string[] {
    return this.types
      .filter((item) => this.authService.isPermissionPresent('SearchComponent.searchBy.' + item, 'w') || item === 'all');

  }

}

enum SortingTypes {

  LEAD = 'lead',
  OPPORTUNITY = 'opportunity',
  CUSTOMER = 'customer',
  FILE = 'file'

}
