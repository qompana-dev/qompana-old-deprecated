import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError, flatMap, map, mergeAll, toArray} from 'rxjs/operators';
import 'rxjs-compat/add/observable/of';
import {flatten} from '@angular/compiler';
import {CrmObject} from '../../shared/model/search.model';
import {UrlUtil} from '../../shared/url.util';


@Injectable({
  providedIn: 'root'
})
export class MainSearchService {
  requestList: RequestPath[] = [
    {
      type: 'lead',
      path: 'crm-business-service/leads/main-search'
    },
    {
      type: 'customer',
      path: 'crm-business-service/customers/search'
    },
    {
      type: 'file',
      path: 'crm-file-service/files/search'
    },
    {
      type: 'opportunity',
      path: 'crm-business-service/opportunity/search'
    },
    {
      type: 'contact',
      path: 'crm-business-service/contacts/search'
    },
  ];

  constructor(private http: HttpClient) {
  }

  searchForType(type: string, pattern: string, allowedTypes: string[]): Observable<CrmObject[]> {
    const requestPath: RequestPath = this.getRequestPath(type);
    let result: Observable<CrmObject[]>;
    if (requestPath) {
      result = this.getSearchObservable(requestPath.path, pattern, false);
    } else {
      result = this.getSearchForAll(pattern, allowedTypes);
    }
    return result;
  }

  getSearchForAll(pattern: string, allowedTypes: string[]): Observable<CrmObject[]> {
    const filteredRequestList: RequestPath[] = this.requestList.filter(value => allowedTypes.includes(value.type));
    return Observable.of(filteredRequestList.map(requestItem => this.getSearchObservable(requestItem.path, pattern, true)))
      .pipe(
        mergeAll(),
        flatMap(list => list),
        toArray(),
        map(result => flatten(result))
      );
  }

  getSearchObservable(path: string, pattern: string, returnEmptyIfError: boolean): Observable<CrmObject[]> {
    let result: Observable<CrmObject[]> = this.http.get<CrmObject[]>(`${UrlUtil.url}/${path}?term=${pattern}`);
    if (returnEmptyIfError) {
      result = result.pipe(catchError(() => of([])));
    }
    return result;
  }

  private getRequestPath(type: string): RequestPath {
    return this.requestList.find((item) => item.type === type);
  }


}

export class RequestPath {
  type: string;
  path: string;
}
