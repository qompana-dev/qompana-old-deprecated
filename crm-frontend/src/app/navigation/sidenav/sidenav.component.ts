import { Component, EventEmitter, Input, OnInit, Output, AfterViewInit, OnDestroy, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Subject } from 'rxjs';
import { LocaleService } from '../../calendar/locale.service';
import { MenuElementFrontendConfig, MenuElementDTO } from './sidenav.model';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit, AfterViewInit, OnDestroy, AfterViewChecked {
  private componentDestroyed: Subject<void> = new Subject();
  @Input() sidenavHidden = false;

  @Output() sidenavChanged: EventEmitter<boolean> = new EventEmitter();

  initialOrder = [];
  reordering = false;
  elements: MenuElementFrontendConfig[] = [];

  constructor(
    private localeService: LocaleService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  get elementsToShow() {
    if (this.elements) {
      if (this.reordering) {
        return this.elements;
      }
      return this.elements.filter((element: any) => element.visible);
    }
    else {
      return [];
    }
  }

  ngAfterViewInit() {
    const menuConfig = this.getConfigFromLocalStorage();
    if (menuConfig && menuConfig.length) {
      this.elements = this.getInitialMenuConfigs();
    }
    else {
      this.localeService.localeChanged.subscribe(() => {
        this.elements = this.getInitialMenuConfigs();
      });
    }
  }

  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }

  getInitialMenuConfigs() {
    const feConfig = {
      'dashboard': {
        icon: 'Dashboard',
        showNameInHidden: false,
      },
      'leads': {
        icon: 'Leads',
        showNameInHidden: true,
      },
      'opportunity': {
        icon: 'Opportunity',
        showNameInHidden: true,
      },
      'calendar': {
        icon: 'Calendar',
        showNameInHidden: true,
      },
      'customers': {
        icon: 'Customers',
        showNameInHidden: true,
      },
      'contacts': {
        icon: 'Contacts',
        showNameInHidden: true,
      },
      'documents': {
        icon: 'Documents',
        showNameInHidden: true,
      },
      'products': {
        icon: 'Products',
        showNameInHidden: true,
      },
      'price-book': {
        icon: 'PriceBook',
        showNameInHidden: true,
      },
      'bpmn': {
        icon: 'BPMN',
        showNameInHidden: true,
      },
      'manufacturers-and-suppliers': {
        icon: 'Suppliers',
        showNameInHidden: true,
      },
      'goals': {
        icon: 'Goals',
        showNameInHidden: true,
      },
      'mail': {
        icon: 'Email',
        showNameInHidden: true,
      },
    };

    const menuConfig = this.getConfigFromLocalStorage();
    const byOrderProperty = (a, b) => {
      if (a.order < b.order) return -1;
      if (a.order > b.order) return 1;
      return 0;
    }
    menuConfig.sort(byOrderProperty);

    const configs = menuConfig.map((element) => {
      return {
        name: element.name,
        visible: element.visible,
        icon: feConfig[element.name].icon,
        showNameInHidden: feConfig[element.name].showNameInHidden,
        cssClass: `menu-${element.name}`,
      } as MenuElementFrontendConfig;
    });
    return configs;
  }

  getConfigFromLocalStorage(): Array<any> {
    return JSON.parse(localStorage.getItem('menuOrder'));
  }

  toggleVisible(event, name) {
    event.stopPropagation();
    const current: any = this.elements.filter((element: any) => element.name === name)[0];
    current.visible = !current.visible;
  }

  toggleReordering(event) {
    event.stopPropagation();
    if (this.reordering) {
      const accountId = +localStorage.getItem('loggedAccountId');
      const newConfiguration = this.elements.map((val: any, i) => {
        return {
          order: i,
          name: val.name,
          visible: val.visible,
        } as MenuElementDTO;
      });
      this.localeService.updateMenuConfigs(accountId, newConfiguration);
    }
    this.reordering = !this.reordering;
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.elements, event.previousIndex, event.currentIndex);
  }

}
