export interface MenuElementFrontendConfig {
  name: string;
  visible: boolean;
  icon: string;
  showNameInHidden: boolean;
  cssClass: string;
}

export interface MenuElementDTO {
  order: number;
  name: string;
  visible: boolean;
}