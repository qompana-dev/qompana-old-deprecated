import {LoginService} from '../login/login.service';
import {Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from '../login/auth/auth.service';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {WebsocketService} from '../shared/websocket.service';
import {NotificationService} from '../shared/notification.service';
import {UserNotificationService} from './user-notification/user-notification.service';
import {MatMenuTrigger} from '@angular/material';
import {CrmObject, CrmObjectType} from '../shared/model/search.model';
import {FilePreviewService} from '../files/file-preview.service';
import {NotificationQueueServiceService} from "../notification/queue/notification-queue.service";
import {NotificationPopupService} from "../notification/popup/notification-popup.service";
import {UserNotification} from "./user-notification/user-notification.model";
import {NotificationDtoFactory} from "../notification/notification.model";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, OnDestroy {
  navUserName = '';
  navUserRole = '';
  navUserAvatar = '';

  search = false;

  private componentDestroyed: Subject<void> = new Subject();

  private websocketNotificationChangeSubscription: Subscription;

  @ViewChild(MatMenuTrigger) searchMenuTrigger: MatMenuTrigger;

  @ViewChild('notificationSlider') notificationSlider;
  @ViewChild('notificationsPopup') notificationPopup: ElementRef;

  numberOfAllUserNotifications = 0;

  private notificationsPopupOpen: boolean = false;

  constructor(private $router: Router,
              private authService: AuthService,
              private userNotificationService: UserNotificationService,
              private notificationService: NotificationService,
              private websocketService: WebsocketService,
              private translateService: TranslateService,
              private loginService: LoginService,
              private previewService: FilePreviewService,
              private renderer: Renderer2,
              private notificationPopupService: NotificationPopupService,
              private notificationQueueServiceService: NotificationQueueServiceService) {
  }

  ngOnInit(): void {
    this.updateUser();

    this.loginService.loggedUserChange
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.updateUser());

    this.loginService.loggedUserAvatarChange
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.navUserAvatar = localStorage.getItem('navUserAvatar'));
  }

  toggleNotifications(): void {
    if(this.notificationsPopupOpen) {
      this.notificationQueueServiceService.show();
      this.notificationPopupService.hide();
      this.searchMenuTrigger.closeMenu();
    } else {
      this.notificationQueueServiceService.hide();
      this.notificationPopupService.show();
    }
  }

  closeMenu(): void {
    this.notificationQueueServiceService.show();
    this.notificationPopupService.hide();
    this.searchMenuTrigger.closeMenu();
  }

  closeSlider(): void {
    this.notificationQueueServiceService.show();
  }

  updateUser(): void {
    if (this.websocketNotificationChangeSubscription) {
      this.websocketNotificationChangeSubscription.unsubscribe();
    }

    this.navUserName = localStorage.getItem('navUserName');
    this.navUserRole = localStorage.getItem('navUserRole');
    this.navUserAvatar = localStorage.getItem('navUserAvatar');

    this.getNotifications();

    this.websocketNotificationChangeSubscription = this.websocketService
      .listenNotificationChange(localStorage.getItem('loggedAccountId'))
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.getNotifications());
  }

  ngOnDestroy(): void {
    if (this.websocketNotificationChangeSubscription) {
      this.websocketNotificationChangeSubscription.unsubscribe();
    }
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  settingItemClick(key: string): void {
    if (key === 'info') {
      this.$router.navigate(['person', 'info']);
    }
  }

  goToAdministration(): void {
    this.$router.navigate(['administration']);
  }

  logout(): void {
    this.loginService.logout();
  }

  goToMainPage(): void {
    this.$router.navigate(['/']);
  }

  showSearchForm(event: MouseEvent): void {
    this.search = !this.search;
    event.stopPropagation();
  }

  handleSearchResultSelected(crmObject: CrmObject): void {
    switch (crmObject.type) {
      case CrmObjectType.contact:
        console.log('not implemented');
        break;
      case CrmObjectType.customer:
        this.$router.navigate(['customers', crmObject.id]);
        break;
      case CrmObjectType.file:
        this.previewService.showPreviewByFileId(crmObject.id);
        break;
      case CrmObjectType.lead:
        this.$router.navigate(['leads', crmObject.id]);
        break;
      case CrmObjectType.opportunity:
        this.$router.navigate(['opportunity', crmObject.id]);
        break;
      case CrmObjectType.product:
        console.log('not implemented');
        break;
      case CrmObjectType.role:
        console.log('not implemented');
        break;
      case CrmObjectType.user:
        console.log('not implemented');
        break;
    }
  }

  private getNotifications(): void {
    this.userNotificationService.getNotifications()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((allUserNotifications: UserNotification[]) => {
        this.numberOfAllUserNotifications = allUserNotifications.filter(e => !e.read).length;
      });
  }
}
