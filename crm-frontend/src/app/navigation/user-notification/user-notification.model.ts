export interface UserNotification {
  id: number;
  accountId: number;
  created: number | string;
  read?: number | string;
  notification: Notification;
  messageParams?: NotificationMessageParam[];
}

export interface Notification {
  id: number;
  name: string;
  msgType: string;
  type: string;
  groupNotification: boolean;
  actions: NotificationAction[];
}

export interface NotificationMessageParam {
  id: number;
  key: string;
  value: string;
}

export interface NotificationAction {
  id: number;
  name: string;
  restUrl: string;
}
export interface NotificationActionDto {
  userNotificationId: number;
  comment?: string;
}
