import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {NotificationAction, UserNotification} from './user-notification.model';
import {takeUntil} from 'rxjs/operators';
import {UserNotificationService} from './user-notification.service';
import {Subject, Subscription} from 'rxjs';
import {WebsocketService} from '../../shared/websocket.service';
import {LoginService} from '../../login/login.service';
import {Router} from '@angular/router';
import {SliderComponent} from '../../shared/slider/slider.component';

@Component({
  selector: 'app-user-notification',
  templateUrl: './user-notification.component.html',
  styleUrls: ['./user-notification.component.scss']
})
export class UserNotificationComponent implements OnInit, OnDestroy {
  @Output() close: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() userNotificationCountChanged: EventEmitter<number> = new EventEmitter<number>();
  @ViewChild('expenseAcceptSlider') expenseAcceptSlider: SliderComponent;

  userNotification: UserNotification;
  allUserNotifications: UserNotification[] = [];
  numberOfAllMineUnreadUserNotifications = 0;

  onlyMineUserNotifications: UserNotification[] = [];
  numberOfAllUnreadUserNotifications = 0;

  userNotificationsToAccept: UserNotification[] = [];
  numberOfUserNotificationsToAccept = 0;

  private componentDestroyed: Subject<void> = new Subject();
  private websocketNotificationChangeSubscription: Subscription;
  expenseId: number;

  constructor(private websocketService: WebsocketService,
              private loginService: LoginService,
              private $router: Router,
              private userNotificationService: UserNotificationService) {
  }

  ngOnInit(): void {
    this.updateUser();
    this.loginService.loggedUserChange
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.updateUser());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  markUserNotificationAsRead($event: MouseEvent, userNotification: UserNotification): void {
    $event.stopPropagation();
    this.userNotificationService.markAsRead(userNotification)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((result: UserNotification) => this.getNotifications());

    switch (userNotification.notification.msgType) {
      case 'LEAD_TASK_ACCEPTANCE':
        this.$router.navigate(['/leads', userNotification.messageParams.find(e => e.key === 'id').value]);
        break;
      case 'OPPORTUNITY_TASK_ACCEPTANCE':
        this.$router.navigate(['/opportunity', userNotification.messageParams.find(e => e.key === 'id').value]);
        break;
      case 'NEW_MAIL':
        this.$router.navigate(['/mail']);
        break;
      case 'EXPENSE_ACCEPTANCE':
        this.expenseId = +userNotification.messageParams.find(e => e.key === 'id').value;
        this.expenseAcceptSlider.open();
        this.userNotification = userNotification;
        break;
    }
    this.close.emit(true);
  }

  runAction($event: MouseEvent, userNotification: UserNotification, action: NotificationAction): void {
    $event.stopPropagation();
    this.userNotificationService.runAction(userNotification, action)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.getNotifications());
  }

  getUserNotificationArgs(userNotification: UserNotification): any {
    const result = {};
    userNotification.messageParams.forEach(item => result[item.key] = item.value);
    return result;
  }

  onNotificationAccepted(): void {
    this.getNotifications();
    this.userNotification = null;
    this.expenseId = null;
  }

  private updateUser(): void {
    if (this.websocketNotificationChangeSubscription) {
      this.websocketNotificationChangeSubscription.unsubscribe();
    }

    this.getNotifications();

    this.websocketNotificationChangeSubscription = this.websocketService
      .listenNotificationChange(localStorage.getItem('loggedAccountId'))
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.getNotifications());
  }

  private getNotifications(): void {
    this.userNotificationService.getNotifications()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((allUserNotifications: UserNotification[]) => {
        this.allUserNotifications = allUserNotifications;
        this.numberOfAllUnreadUserNotifications = this.allUserNotifications.filter(e => !e.read).length;
        this.userNotificationCountChanged.next(this.numberOfAllUnreadUserNotifications);

        this.onlyMineUserNotifications = allUserNotifications.filter(e => !e.notification.groupNotification);
        this.numberOfAllMineUnreadUserNotifications = this.onlyMineUserNotifications.filter(e => !e.read).length;

        this.userNotificationsToAccept = allUserNotifications
          .filter(e => e.notification.msgType === 'LEAD_TASK_ACCEPTANCE' || e.notification.msgType === 'OPPORTUNITY_TASK_ACCEPTANCE'
          || e.notification.msgType === 'EXPENSE_ACCEPTANCE');
        this.numberOfUserNotificationsToAccept = this.userNotificationsToAccept.filter(e => !e.read).length;
      });
  }
}
