import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {NotificationAction, NotificationActionDto, UserNotification} from './user-notification.model';
import {UrlUtil} from '../../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class UserNotificationService {

  constructor(private http: HttpClient) {
  }


  getNotifications(): Observable<UserNotification[]> {
    return this.http.get<UserNotification[]>(`${UrlUtil.url}/crm-notification-service/notifications`);
  }

  markAsRead(userNotification: UserNotification): Observable<UserNotification> {
    return this.http
      .post<UserNotification>(`${UrlUtil.url}/crm-notification-service/notifications/${userNotification.id}/read`, null);
  }

  markUserAllAsRead(): Observable<void> {
    return this.http
      .post<void>(`${UrlUtil.url}/crm-notification-service/notifications/readAll`, null);
  }

  runAction(userNotification: UserNotification, action: NotificationAction): Observable<void> {
    const body: NotificationActionDto = {userNotificationId: userNotification.id};
    return this.http
      .post<void>(`${UrlUtil.url}/${action.restUrl}`, body);
  }

  runActionWithComment(userNotification: UserNotification, action: NotificationAction, comment: string): Observable<void> {
    const body: NotificationActionDto = {userNotificationId: userNotification.id, comment};
    return this.http
      .post<void>(`${UrlUtil.url}/${action.restUrl}`, body);
  }

  getNotificationId(accountId: number, msgType: string, msgParam: string): Observable<number> {
    const url = `${UrlUtil.url}/crm-notification-service/notifications/accountId/${accountId}/msgType/${msgType}/msgParam/${msgParam}`;
    return this.http.get<number>(url);
  }
}
