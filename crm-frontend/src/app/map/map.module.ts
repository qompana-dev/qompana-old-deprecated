import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MapRoutingModule} from './map-routing.module';
import {SharedModule} from '../shared/shared.module';
import {MapComponent} from './map.component';
import {MapViewModule} from './map-view/map-view.module';

@NgModule({
  declarations: [MapComponent],
  imports: [
    CommonModule,
    SharedModule,
    MapViewModule,
    MapRoutingModule
  ]
})
export class MapModule { }
