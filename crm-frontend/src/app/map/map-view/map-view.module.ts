import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgxMapboxGLModule} from 'ngx-mapbox-gl';
import {environment} from '../../../environments/environment';
import {MapViewComponent} from './map-view.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [MapViewComponent],
  imports: [
    NgxMapboxGLModule.withConfig({
      accessToken: environment.mapboxToken,
      geocoderAccessToken: environment.mapboxToken
    }),
    TranslateModule,
    CommonModule
  ],
  exports: [
    MapViewComponent
  ]
})
export class MapViewModule { }
