import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewEncapsulation} from '@angular/core';
import {MapService} from '../map.service';
import {MapViewMarker, MgLayerConfig} from './map-view.model';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {GeoJSONSourceRaw, LngLatLike, MapMouseEvent} from 'mapbox-gl';
import { FoundPlace } from 'src/app/lead/lead.model';

@Component({
  selector: 'app-map-view',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss']
})
export class MapViewComponent implements OnInit, OnChanges {
  static readonly userClickId = 'userClick';
  @Input() mapCenter: LngLatLike;
  @Input() zoom: number;
  @Input() markers: MapViewMarker[];
  @Input() showMap = true;

  @Input() selectable = false;

  @Output() locationSelected: EventEmitter<MapViewMarker> = new EventEmitter<MapViewMarker>();

  private componentDestroyed: Subject<void> = new Subject();
  geoSource: GeoJSONSourceRaw;
  mgLayerConfig = MgLayerConfig;

  constructor(private mapService: MapService) {
  }

  ngOnInit(): void {
    this.updatePath();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.updatePath();
  }

  clearUserSelectedMarker(): void {
    if (!this.markers) {
      return;
    }
    this.markers = this.markers.filter((marker) => marker.id !== MapViewComponent.userClickId);
  }

  mapClick(clickEvent: MapMouseEvent): void {
    if (!this.selectable) {
      return;
    }
    const marker: MapViewMarker = this.addValue(this.getUserClickMarker(clickEvent.lngLat.lat, clickEvent.lngLat.lng));
    this.locationSelected.emit(marker);
  }

  getUserClickMarker(latitude: number | string, longitude: number | string): MapViewMarker {
    const numberLatitude = +latitude;
    const numberLongitude = +longitude;
    return {
      id: MapViewComponent.userClickId,
      popup: '',
      latitude: numberLatitude,
      longitude: numberLongitude
    };
  }

  markerDragEnd($event): void {
    this.mapService.getPlaceByLonLat($event.getLngLat())
      .subscribe(place => this.markers[0].onDragEnd(place));
  }

  private updatePath(): void {
    if (!this.markers) {
      this.geoSource = undefined;
      return;
    }

    const filteredMarkers = this.markers
      .filter((marker) => marker.pathOrder)
      .sort((a, b) => a.pathOrder - b.pathOrder);

    if (filteredMarkers.length === 0) {
      this.geoSource = undefined;
      return;
    }

    this.mapService.getGeoSourceForMarkers(filteredMarkers)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((result) => {
        this.geoSource = result;
      });
  }

  private addValue(newMarker: MapViewMarker): MapViewMarker {
    if (!this.markers) {
      this.markers = [];
    }
    const index = this.markers.map((marker) => marker.id).indexOf(newMarker.id);
    if (index === -1) {
      this.markers.push(newMarker);
      return newMarker;
    } else {
      this.markers[index].popup = newMarker.popup;
      this.markers[index].latitude = newMarker.latitude;
      this.markers[index].longitude = newMarker.longitude;
      return this.markers[index];
    }
  }
}
