import {LineLayout, LinePaint} from 'mapbox-gl';

export interface MapViewMarker {
  id: string;
  popup?: string;
  latitude: number;
  longitude: number;
  pathOrder?: number;
  draggable?: boolean;
  onDragEnd?: Function;
}

export class MgLayerConfig {
  static mapStyle = 'mapbox://styles/mapbox/streets-v9';
  static layout: LineLayout = {'line-join': 'round', 'line-cap': 'round'};
  static paint: LinePaint = {'line-color': '#00B3FA', 'line-width': 4};
}
