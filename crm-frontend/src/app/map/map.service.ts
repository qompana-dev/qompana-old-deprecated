import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {MapViewMarker} from './map-view/map-view.model';
import {GeoJSONSourceRaw, LngLatLike} from 'mapbox-gl';
import {map} from 'rxjs/operators';
import {Address} from './map.model';
import {FoundPlace, PhotonResponce, PhotonResponcePlace} from '../lead/lead.model';
import * as isNil from 'lodash/isNil';
import * as pickBy from 'lodash/pickBy';
import * as isEmpty from 'lodash/isEmpty';
import {NotificationService, NotificationType} from '../shared/notification.service';


@Injectable({
  providedIn: 'root'
})
export class MapService {

  readonly geocodingUrl = 'https://nominatim.openstreetmap.org/search';
  readonly geocodingDetailsUrl = 'https://nominatim.openstreetmap.org/lookup';
  readonly geocodingAutoCompleteUrl = 'https://photon.komoot.io/api/';
  readonly geocodingReverseUrl = 'https://nominatim.openstreetmap.org/reverse';

  constructor(private http: HttpClient, public notificationService: NotificationService) {
  }

  getPlaceByLonLat(lonLat) {
    let params = {
      lon: '' + lonLat.lng,
      lat: '' + lonLat.lat,
    };
    params['format'] = 'json';
    params['accept-language'] = localStorage.getItem('locale') + '&en';
    params['addressdetails'] = '1';

    return this.http.get<any>(this.geocodingReverseUrl, {params});
  }

  getGeoSourceForMarkers(markers: MapViewMarker[]): Observable<GeoJSONSourceRaw> {
    return this.getRouteForMarkers(markers)
      .pipe(map(route => this.getGeoSource(route)));
  }

  getRouteForMarkers(markers: MapViewMarker[]): Observable<any> {
    const directionUrl = `${environment.mapboxUri}/directions/v5/mapbox/driving`;
    const url = `${directionUrl}/${this.getPointsQueryParam(markers)}?access_token=${environment.mapboxToken}&geometries=geojson`;
    return this.http.get<any>(url);
  }

  getGeoSource(route: any): GeoJSONSourceRaw {
    return {type: 'geojson', data: {type: 'Feature', properties: {}, geometry: route.routes[0].geometry}};
  }

  getCoordinatesByAddress(address: Address, lang?: string): Observable<FoundPlace[]> {
    let params = {
      street: address.street,
      city: address.city,
      country: address.country,
      state: address.state,
      postalcode: this.formatZipCode(address.zip)
    };
    params = pickBy(params, (value) => !isNil(value));
    if (isEmpty(params)) {
      return of([]);
    }
    params['format'] = 'json';
    params['accept-language'] = lang || localStorage.getItem('locale') + '&en';
    params['addressdetails'] = '1';
    return this.http.get<FoundPlace[]>(this.geocodingUrl, {params}).pipe(
      map(places => this.filterOutTooSimilarPlaces(places))
    );
  }

  getAddressDetails(place: FoundPlace): Observable<FoundPlace> {
    let params = {
      osm_ids: place.osm_type + place.osm_id,
    };

    params['format'] = 'json';
    params['accept-language'] = localStorage.getItem('locale') + '&en';
    params['addressdetails'] = '1';
    return this.http.get<any>(this.geocodingDetailsUrl, {params}).pipe(
      map(places => places.length ? places[0] : {})
    );
  }

  getCoordinatesByAddressString(address: string): Observable<FoundPlace[]> {
    if (!address) {
      return of([]);
    }
    const params = {
      q: address,
      format: 'json',
      addressdetails: '1'
    };
    params['accept-language'] = localStorage.getItem('locale') + '&en';

    return this.http.get<FoundPlace[]>(this.geocodingUrl, {params}).pipe(
      map(places => this.filterOutTooSimilarPlaces(places))
    );
  }

  getOsmTag(address: string, type: string) {
    switch(type) {
      case 'city':
      case 'country': return 'place';
      case 'street': {
        const splittedAddress = address.split(' ');
        const isAddressWithHouseNumber = !!splittedAddress.pop().match("^\\d+/?\\d*[a-zA-Z]?(?<!/)$");
        return isAddressWithHouseNumber ? 'building' : 'highway';
      };
    }
  }

  getCoordsAutocomplete(addressString: string, type: string, address: Address, extent: number[]): Observable<PhotonResponcePlace[]> {
    if (!addressString || typeof addressString !== 'string') {
      return of([]);
    }
    const osm_tag = this.getOsmTag(addressString, type);
    const typeForFilter = osm_tag === 'building' ? 'house' : type;
    const shouldCompareAddress = osm_tag !== 'building' && type !== 'country';
    const params = {
      q: addressString,
      osm_tag,
      limit: '100',
    };

    if (extent) {
      params['bbox'] = extent.join();
    }
    
    return this.http.get<PhotonResponce>(this.geocodingAutoCompleteUrl, {params}).pipe(
      map(({ features }) => {
        return features.filter(place => {
          let result = place.properties.type === typeForFilter;
          if (shouldCompareAddress && place.properties.name) {

            result = result && place.properties.name.toLowerCase().includes(addressString.toLowerCase());
          }
          
          if (address) {
            if (type === 'city') {
              result = address.country ? result && place.properties.country === address.country : result;
            } else if (type === 'street') {
              result = address.country ? result && place.properties.country === address.country : result;
              result = address.city ? result && place.properties.city === address.city : result;
            }
          }
          
          return result;
        })
      })
    );
  }

  formatPlaceDescription(place: Partial<FoundPlace>): string {
    if (place && place.address) {
      return `${place.address.road || place.address.neighbourhood || ''} ${place.address.house_number || ''}, ${place.address.postcode} ${place.address.city || place.address.county || ''}, ${place.address.country}`;
    } else {
      return '';
    }
  }

  getCenter(foundPlace: Partial<FoundPlace>): { lng: number; lat: number; } {
    if (!foundPlace) {
      return undefined;
    }
    return {lng: +foundPlace.lon, lat: +foundPlace.lat};
  }

  getSingleMarkerFromFoundPlace(place: Partial<FoundPlace>, onDragEnd?: Function): MapViewMarker[] {
    return place ? [{
      onDragEnd,
      draggable: !!onDragEnd,
      longitude: +place.lon,
      latitude: +place.lat,
      id: '0',
      popup: this.formatPlaceDescription(place)
    }] : [];
  }

  notifyPlaceNotFound(): void {
    this.notificationService.notify('maps.notification.placeNotFound', NotificationType.ERROR);
  }

  private getPointsQueryParam(markers: MapViewMarker[]): string {
    if (!markers) {
      return '';
    }
    return markers.map((marker) => marker.longitude + ',' + marker.latitude).join(';');
  }

  private formatZipCode(zip: string): string {
    if (zip && zip.length > 3 && !(/[0-9]{2}-[0-9]{3}/.test(zip))) {
      return `${zip.slice(0, 2)}-${zip.slice(2)}`;
    }
    return zip;
  }

  private filterOutTooSimilarPlaces(places: FoundPlace[]): FoundPlace[] {
    if (places && places.length > 1 && places.filter(place => place.importance < 0.7).length === 0) {
      return [places[0]];
    }
    return places;
  }
}

