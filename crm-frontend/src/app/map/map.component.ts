import { Component, OnInit } from '@angular/core';
import {MapViewMarker} from './map-view/map-view.model';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  mapCenter: number[] = [17.038538, 51.107883];
  zoom = 12;

  markers: MapViewMarker[] = [
    {id: 'marker1', popup: 'Mati proguramuje Git-a', latitude: 51.0905557, longitude: 17.0158732, pathOrder: 1},
    {id: 'marker2', popup: 'Test', latitude: 51.107883, longitude: 17.038538, pathOrder: 2},
    {id: 'marker3', popup: 'Test2', latitude: 51.1064638, longitude: 17.0142642, pathOrder: 5},
    {id: 'marker5', latitude: 51.1064638, longitude: 17.038538},
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
