import {Injectable} from '@angular/core';
import {MailContent, MailHeader, MailStorageDto} from './mail.model';
import {TranslateService} from '@ngx-translate/core';
import {FormatDatePipe} from '../shared/pipe/formatDate.pipe';

@Injectable({
  providedIn: 'root'
})
export class MailUtilService {

  constructor(private translateService: TranslateService,
              private formatDatePipe: FormatDatePipe) {
  }

  getFromMailTitle(header: MailHeader | MailStorageDto): string {
    if (header && header.fromAddr && header.fromAddr[0]) {
      const from = header.fromAddr[0];
      return from.name || from.email;
    }
    return '';
  }

  getContent(mail: MailContent): string {
    const info = '<div style="font-size: 14px; color: $contrast">' +
      this.translateService.instant('mail.details.replyMessage', {
        date: this.formatDatePipe.transform(mail.headerDto.receivedDate, true),
        from: this.getFrom(mail)
      }) + '</div>';
    return `<br/><br/>${info}<div style="padding-left: 5px; padding-top: 10px;
     padding-bottom: 10px; border-left: 2px solid #319DF8;">${mail.htmlContent ? mail.htmlContent : mail.plainContent}</div>`;
  }

  private getFrom(mail: MailContent): string {
    return mail.headerDto.fromAddr
      .map(from => from.name + ' <' + from.email + '>').join(', ');
  }
}
