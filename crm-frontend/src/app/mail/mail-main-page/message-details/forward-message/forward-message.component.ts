import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {MailContent, NewMailAttachment} from '../../../mail.model';
import {MailInputComponent} from '../../mail-input/mail-input.component';
import {MailService} from '../../../mail.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {TempFileUploaderService} from '../../../../shared/temp-file/temp-file-uploader.service';
import {LoginService} from '../../../../login/login.service';
import {UNAUTHORIZED} from 'http-status-codes';
import {FileInfo, uploadFileErrorDialogData} from '../../../../files/file.model';
import {DeleteDialogService} from '../../../../shared/dialog/delete/delete-dialog.service';
import {MailSelectFileDialogData} from '../../new-mail/mail-select-file-dialog/mail-select-file-dialog.component';
import {MailSelectFileDialogService} from '../../new-mail/mail-select-file-dialog/mail-select-file-dialog.service';
import {TranslateService} from '@ngx-translate/core';
import {FormatDatePipe} from '../../../../shared/pipe/formatDate.pipe';
import {MailUtilService} from '../../../mail-util.service';

@Component({
  selector: 'app-forward-message',
  templateUrl: './forward-message.component.html',
  styleUrls: ['./forward-message.component.scss']
})
export class ForwardMessageComponent implements OnInit, OnDestroy {

  tempFileUploader: TempFileUploaderService;
  private tempFileUploaderResponseMap: Map<any, any> = new Map<any, any>();
  filesFormCrm: FileInfo[] = [];

  ccInputVisible = false;
  bccInputVisible = false;
  isSending = false;
  content = '';
  @Output() forwardClosed: EventEmitter<void> = new EventEmitter<void>();
  // tslint:disable-next-line:variable-name
  _message: MailContent;
  @Input() set message(message: MailContent) {
    this._message = message;
    this.setForwardContent();
  }

  get message(): MailContent {
    return this._message;
  }

  @Input() messageId: number;
  @ViewChild('toEmails') toEmails: MailInputComponent;
  @ViewChild('ccEmails') ccEmails: MailInputComponent;
  @ViewChild('bccEmails') bccEmails: MailInputComponent;
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private mailService: MailService,
              private mailUtilService: MailUtilService,
              private loginService: LoginService,
              private deleteDialogService: DeleteDialogService,
              private notificationService: NotificationService,
              private mailSelectFileDialogService: MailSelectFileDialogService) {
    this.tempFileUploader = new TempFileUploaderService(this.loginService);
    this.tempFileUploader.onAfterAddingFile = (file) => this.uploadTemp(file);
    this.tempFileUploader.onAfterAddingAll = files => this.uploadTemps(files);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    if (this.toEmails.isValid()) {
      this.isSending = true;
      this.mailService.forwardMail({
        to: this.toEmails.getEmails(),
        cc: (this.ccEmails) ? this.ccEmails.getEmails() : [],
        bcc: (this.bccEmails) ? this.bccEmails.getEmails() : [],
        content: this.content,
        messageId: this.messageId,
        attachments: this.getAttachment(),
      }).pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => this.handleForwardSuccess(),
          (err) => this.handleForwardError(err)
        );
    } else {
      this.toEmails.markAsTouched();
    }
  }

  private getAttachment(): NewMailAttachment[] {
    const result: NewMailAttachment[] = [];
    if (this.tempFileUploader && this.tempFileUploader.queue) {
      const tempFiles = this.tempFileUploader.queue.map((c: any): NewMailAttachment => {
        return {
          fileId: JSON.parse(this.tempFileUploaderResponseMap.get(c)).id,
          tempFile: true
        };
      });
      result.push(...tempFiles);
    }

    if (this.filesFormCrm.length !== 0) {
      const crmFiles = this.filesFormCrm.map((file: FileInfo): NewMailAttachment => {
        return {
          fileId: file.id,
          tempFile: false
        };
      });
      result.push(...crmFiles);
    }
    return result;
  }

  private handleForwardSuccess(): void {
    this.isSending = false;
    this.notificationService.notify('mail.notification.forwardSuccess', NotificationType.SUCCESS);
    this.forwardClosed.emit();
  }

  private handleForwardError(err: any): void {
    this.isSending = false;
    if (err.status === 409) {
      this.notificationService.notify('mail.notification.sendEmailGdprError', NotificationType.ERROR);
    } else {
      this.notificationService.notify('mail.notification.forwardError', NotificationType.ERROR);
    }
  }

  private setForwardContent(): void {
    this.content = this.mailUtilService.getContent(this._message);
  }

  public uploadTemps(files: any[]): void {
    files.forEach(file => this.uploadTemp(file));
  }

  public uploadTemp(file: any): void {
    this.tempFileUploader.uploadItemWithForm(file, {description: 'Attachment'});
    file.onSuccess = response => {
      this.tempFileUploaderResponseMap.set(file, response);
    };
    file.onError = response => {
      if (JSON.parse(response).status === UNAUTHORIZED) {
        this.tempFileUploader.clearQueue();
        return;
      }
      uploadFileErrorDialogData.onConfirmClick = () => this.uploadTemp(file);
      uploadFileErrorDialogData.onCancelClick = () => {
        this.removeTempFile(file);
      };
      this.deleteDialogService.open(uploadFileErrorDialogData);
    };
  }

  removeCrmFile(file: FileInfo): void {
    this.filesFormCrm = this.filesFormCrm.filter(c => c.id !== file.id);
  }

  removeTempFile(file: any): void {
    this.tempFileUploader.queue = this.tempFileUploader.queue.filter(c => c !== file);
  }

  selectFilesFromCrm(): void {
    const data: MailSelectFileDialogData = {
      onConfirmClick: (selectedFiles: FileInfo[]) => {
        this.filesFormCrm = [...selectedFiles];
      },
      selectedFiles: this.filesFormCrm
    };
    this.mailSelectFileDialogService.open(data);
  }

}
