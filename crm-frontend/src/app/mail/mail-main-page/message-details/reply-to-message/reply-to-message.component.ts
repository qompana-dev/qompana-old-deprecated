import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {MailContent, NewMail, NewMailAttachment} from '../../../mail.model';
import {takeUntil} from 'rxjs/operators';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {MailInputComponent} from '../../mail-input/mail-input.component';
import {MailService} from '../../../mail.service';
import {Subject} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {FormatDatePipe} from '../../../../shared/pipe/formatDate.pipe';
import {TempFileUploaderService} from '../../../../shared/temp-file/temp-file-uploader.service';
import {FileInfo, uploadFileErrorDialogData} from '../../../../files/file.model';
import {LoginService} from '../../../../login/login.service';
import {UNAUTHORIZED} from 'http-status-codes';
import {MailSelectFileDialogData} from '../../new-mail/mail-select-file-dialog/mail-select-file-dialog.component';
import {MailSelectFileDialogService} from '../../new-mail/mail-select-file-dialog/mail-select-file-dialog.service';
import {DeleteDialogService} from '../../../../shared/dialog/delete/delete-dialog.service';
import {MailUtilService} from '../../../mail-util.service';

@Component({
  selector: 'app-reply-to-message',
  templateUrl: './reply-to-message.component.html',
  styleUrls: ['./reply-to-message.component.scss']
})
export class ReplyToMessageComponent implements OnInit, OnDestroy {

  tempFileUploader: TempFileUploaderService;
  private tempFileUploaderResponseMap: Map<any, any> = new Map<any, any>();
  filesFormCrm: FileInfo[] = [];

  ccInputVisible = false;
  bccInputVisible = false;
  content = '';
  isSending = false;
  @Output() replyToClosed: EventEmitter<void> = new EventEmitter<void>();
  // tslint:disable-next-line:variable-name
  _message: MailContent;
  @Input() set message(message: MailContent) {
    this._message = message;
    this.setReplyContent();
  }

  get message(): MailContent {
    return this._message;
  }

  @ViewChild('ccEmails') ccEmails: MailInputComponent;
  @ViewChild('bccEmails') bccEmails: MailInputComponent;
  private componentDestroyed: Subject<void> = new Subject();
  // tslint:disable-next-line:variable-name
  private _replyToAll: boolean;

  @Input() set replyToAll(replyToAll: boolean) {
    this._replyToAll = replyToAll;
    this.ccInputVisible = replyToAll;
  }

  constructor(private notificationService: NotificationService,
              private mailUtilService: MailUtilService,
              private loginService: LoginService,
              private mailService: MailService,
              private deleteDialogService: DeleteDialogService,
              private mailSelectFileDialogService: MailSelectFileDialogService) {
    this.tempFileUploader = new TempFileUploaderService(this.loginService);
    this.tempFileUploader.onAfterAddingFile = (file) => this.uploadTemp(file);
    this.tempFileUploader.onAfterAddingAll = files => this.uploadTemps(files);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  getCcAddresses(): string[] {
    let addresses = [];
    if (this.message && this._replyToAll) {
      if (this.message.headerDto.toAddr) {
        addresses = addresses.concat(this.message.headerDto.toAddr);
      }
      if (this.message.headerDto.ccAddr) {
        addresses = addresses.concat(this.message.headerDto.ccAddr);
      }
    }
    return addresses.map(e => e.email);
  }

  submit(): void {
    if ((!this.ccInputVisible || this.ccEmails.isValid()) && (!this.bccInputVisible || this.bccEmails.isValid())) {
      this.isSending = true;
      this.mailService.sendMail(this.getMail())
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => {
          this.notificationService.notify('mail.notification.sendEmailSuccess', NotificationType.SUCCESS);
          this.replyToClosed.emit();
          this.isSending = false;
        }, (err) => {
          if (err.status === 409) {
            this.notificationService.notify('mail.notification.sendEmailGdprError', NotificationType.ERROR);
          } else {
            this.notificationService.notify('mail.notification.sendEmailError', NotificationType.ERROR);
          }
          this.isSending = false;
        });
    } else {
      this.ccEmails.markAsTouched();
    }
  }

  private getMail(): NewMail {
    return {
      subject: 'RE: ' + this.message.subject,
      content: this.content,
      to: [this.message.headerDto.fromAddr[0].email],
      cc: this.ccInputVisible ? this.ccEmails.getEmails() : [],
      bcc: (this.bccEmails) ? this.bccEmails.getEmails() : [],
      attachments: this.getAttachment(),
    };
  }

  private setReplyContent(): void {
    this.content = this.mailUtilService.getContent(this._message);
  }

  public uploadTemps(files: any[]): void {
    files.forEach(file => this.uploadTemp(file));
  }

  public uploadTemp(file: any): void {
    this.tempFileUploader.uploadItemWithForm(file, {description: 'Attachment'});
    file.onSuccess = response => {
      this.tempFileUploaderResponseMap.set(file, response);
    };
    file.onError = response => {
      if (JSON.parse(response).status === UNAUTHORIZED) {
        this.tempFileUploader.clearQueue();
        return;
      }
      uploadFileErrorDialogData.onConfirmClick = () => this.uploadTemp(file);
      uploadFileErrorDialogData.onCancelClick = () => {
        this.removeTempFile(file);
      };
      this.deleteDialogService.open(uploadFileErrorDialogData);
    };
  }

  private getAttachment(): NewMailAttachment[] {
    const result: NewMailAttachment[] = [];
    if (this.tempFileUploader && this.tempFileUploader.queue) {
      const tempFiles = this.tempFileUploader.queue.map((c: any): NewMailAttachment => {
        return {
          fileId: JSON.parse(this.tempFileUploaderResponseMap.get(c)).id,
          tempFile: true
        };
      });
      result.push(...tempFiles);
    }

    if (this.filesFormCrm.length !== 0) {
      const crmFiles = this.filesFormCrm.map((file: FileInfo): NewMailAttachment => {
        return {
          fileId: file.id,
          tempFile: false
        };
      });
      result.push(...crmFiles);
    }

    return result;
  }

  removeCrmFile(file: FileInfo): void {
    this.filesFormCrm = this.filesFormCrm.filter(c => c.id !== file.id);
  }

  removeTempFile(file: any): void {
    this.tempFileUploader.queue = this.tempFileUploader.queue.filter(c => c !== file);
  }

  selectFilesFromCrm(): void {
    const data: MailSelectFileDialogData = {
      onConfirmClick: (selectedFiles: FileInfo[]) => {
        this.filesFormCrm = [...selectedFiles];
      },
      selectedFiles: this.filesFormCrm
    };
    this.mailSelectFileDialogService.open(data);
  }

}

