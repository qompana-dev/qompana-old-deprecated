import {Directive, DoCheck, ElementRef, Input, OnChanges, OnInit, Renderer2, SimpleChanges} from '@angular/core';

@Directive({
  selector: '[appHtmlInIframe]'
})
export class HtmlInIframeDirective implements OnInit {
  private height = 0;

  constructor(private renderer: Renderer2, private el: ElementRef) {
  }

  ngOnInit(): void {
    setInterval(() => {
      this.checkHeight();
    }, 500);
  }

  checkHeight(): void {
    if (this.el && this.el.nativeElement && this.el.nativeElement.contentDocument
      && this.el.nativeElement.contentDocument.documentElement &&
      this.el.nativeElement.contentDocument.documentElement.scrollHeight) {
      const newHeight = this.el.nativeElement.contentDocument.documentElement.scrollHeight;
      if (newHeight !== this.height) {
        this.height = newHeight;
        this.renderer.setStyle(this.el.nativeElement, 'height', this.height + 'px');
      }
    }
  }
}
