import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {MailContent} from '../../mail.model';

@Component({
  selector: 'app-message-details',
  templateUrl: './message-details.component.html',
  styleUrls: ['./message-details.component.scss']
})
export class MessageDetailsComponent implements OnInit {

  @ViewChild('mainContainer') mainContainer: ElementRef;
  @ViewChild('messageForm') messageForm: ElementRef;
  @Input() messageId: number;
  @Input() selectedFolder: string;
  // tslint:disable-next-line:variable-name
  _message: MailContent;
  replyOpened = false;
  forwardOpened = false;
  replyToAll = false;

  @Input() set message(message: MailContent) {
    this.replyOpened = false;
    this.forwardOpened = false;
    this._message = message;
  }

  constructor() {
  }

  ngOnInit(): void {
  }

  replyAll(): void {
    this.replyOpened = true;
    this.replyToAll = true;
    this.forwardOpened = false;
    setTimeout(() => this.scrollToBottom(), 200);
  }

  reply(): void {
    this.replyOpened = true;
    this.replyToAll = false;
    this.forwardOpened = false;
    setTimeout(() => this.scrollToBottom(), 200);
  }

  forward(): void {
    this.replyOpened = false;
    this.forwardOpened = true;
    this.replyToAll = false;
    setTimeout(() => this.scrollToBottom(), 200);
  }

  private scrollToBottom(): void {
    try {
      this.mainContainer.nativeElement.scrollTop = this.messageForm.nativeElement.offsetTop - 80;
    } catch (err) {
    }
  }

  reset(): void {
    this.replyOpened = false;
    this.forwardOpened = false;
    this.replyToAll = false;
  }
}
