import {NgModule} from '@angular/core';
import {HtmlInIframeDirective} from './html-in-iframe.directive';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    HtmlInIframeDirective
  ],
  exports: [
    HtmlInIframeDirective
  ],
  imports: [
    CommonModule
  ]
})
export class HtmlInFrameModule {
}
