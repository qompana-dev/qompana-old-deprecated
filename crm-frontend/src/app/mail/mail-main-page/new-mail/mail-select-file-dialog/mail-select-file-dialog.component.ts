import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatCheckboxChange, MatDialogRef} from '@angular/material';
import {FileInfo} from '../../../../files/file.model';
import {Observable, Subject} from 'rxjs';
import {CrmObject} from '../../../../shared/model/search.model';
import {Page, PageRequest} from '../../../../shared/pagination/page.model';
import {AuthService} from '../../../../login/auth/auth.service';
import {NotificationService} from '../../../../shared/notification.service';
import {FileService} from '../../../../files/file.service';
import {InfinityScrollUtil, InfinityScrollUtilInterface} from '../../../../shared/infinity-scroll.util';
import {FilterStrategy} from '../../../../shared/model';
import {FormControl} from '@angular/forms';
import {debounceTime, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-mail-select-file-dialog',
  templateUrl: './mail-select-file-dialog.component.html',
  styleUrls: ['./mail-select-file-dialog.component.scss']
})
export class MailSelectFileDialogComponent implements OnInit, InfinityScrollUtilInterface {

  private componentDestroyed: Subject<void> = new Subject();
  crmObject: CrmObject = {} as CrmObject;
  infinityScroll: InfinityScrollUtil;
  selectedFiles: FileInfo[] = [];


  searchForm: FormControl = new FormControl();
  search: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: MailSelectFileDialogData,
    private dialogRef: MatDialogRef<MailSelectFileDialogComponent>,
    private authService: AuthService,
    private notificationService: NotificationService,
    private fileService: FileService) {
    this.selectedFiles = data.selectedFiles;
  }

  cancel(): void {
    this.selectedFiles = [];
  }

  confirm(): void {
    this.data.onConfirmClick(this.selectedFiles);
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.setSearchFormChangeSubscription();
    this.searchForm.patchValue(undefined);
    this.initInfinityScroll();
  }


  private initInfinityScroll(): void {
    this.infinityScroll = new InfinityScrollUtil(
      this,
      this.componentDestroyed,
      this.notificationService,
      'mail.notification.getCrmFilesError',
      'originalName,asc'
    );
  }


  getItemObservable(selection: FilterStrategy, pageRequest: PageRequest): Observable<Page<FileInfo>> {
    return this.fileService.getLoggedUserFiles(pageRequest, this.search);
  }

  toggleFile($event: MatCheckboxChange, file: FileInfo): void {
    if ($event.checked) {
      this.selectedFiles.push(file);
    } else {
      const index = this.selectedFiles.indexOf(file);
      if (index !== -1) {
        this.selectedFiles.splice(index, 1);
      }
    }
  }

  isChecked(file: FileInfo): boolean {
    return this.selectedFiles && this.selectedFiles.length !== 0 && this.selectedFiles.filter(f => f.id === file.id).length === 1;
  }

  setSearchFormChangeSubscription(): void {
    this.searchForm.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250))
      .subscribe((value: string) => {
        this.search = value;
        this.infinityScroll.initData();
      });
  }
}

export interface MailSelectFileDialogData {
  onConfirmClick: (selectedFiles: FileInfo[]) => void;
  selectedFiles: FileInfo[];
}
