import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {MailSelectFileDialogComponent, MailSelectFileDialogData} from './mail-select-file-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class MailSelectFileDialogService {

  constructor(private dialog: MatDialog) {
  }


  open(data: MailSelectFileDialogData): void {
    this.dialog.open(MailSelectFileDialogComponent, {
      width: '50vw', height: '75vh', panelClass: 'mail-select-file-dialog-container', data
    });
  }
}
