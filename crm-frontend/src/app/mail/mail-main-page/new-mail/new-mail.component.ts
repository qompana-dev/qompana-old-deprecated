import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {interval, Subject} from 'rxjs';
import {FormUtil} from '../../../shared/form.util';
import {MailService} from '../../mail.service';
import {NewMail, NewMailAttachment} from '../../mail.model';
import {takeUntil} from 'rxjs/operators';
import {MailInputComponent} from '../mail-input/mail-input.component';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {TempFileUploaderService} from '../../../shared/temp-file/temp-file-uploader.service';
import {LoginService} from '../../../login/login.service';
import {FileInfo, uploadFileErrorDialogData} from '../../../files/file.model';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import {UNAUTHORIZED} from 'http-status-codes';
import {MailTextEditorComponent} from '../../mail-text-editor/mail-text-editor.component';
import {TempFileService} from '../../../shared/temp-file/temp-file.service';
import {MailSelectFileDialogService} from './mail-select-file-dialog/mail-select-file-dialog.service';
import {MailSelectFileDialogData} from './mail-select-file-dialog/mail-select-file-dialog.component';

@Component({
  selector: 'app-new-mail',
  templateUrl: './new-mail.component.html',
  styleUrls: ['./new-mail.component.scss']
})
export class NewMailComponent implements OnInit, OnDestroy {

  mailForm: FormGroup;
  private componentDestroyed: Subject<void> = new Subject();
  content = '';
  public isSending = false;
  private ignoreGdpr = false;

  @Input() fromSlider: boolean = false;
  @Output() closeNewMessage: EventEmitter<void> = new EventEmitter<void>();
  @Output() submittedSuccessfully: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild('toEmails') toEmails: MailInputComponent;
  @ViewChild('ccEmails') ccEmails: MailInputComponent;
  @ViewChild('bccEmails') bccEmails: MailInputComponent;
  @ViewChild(MailTextEditorComponent) mailTextEditorComponent: MailTextEditorComponent;


  tempFileUploader: TempFileUploaderService;
  private tempFileUploaderResponseMap: Map<any, any> = new Map<any, any>();
  filesFormCrm: FileInfo[] = [];


  constructor(private fb: FormBuilder,
              private loginService: LoginService,
              private tempFileService: TempFileService,
              private deleteDialogService: DeleteDialogService,
              private mailService: MailService,
              private notificationService: NotificationService,
              private mailSelectFileDialogService: MailSelectFileDialogService) {
    this.tempFileUploader = new TempFileUploaderService(this.loginService);
    this.tempFileUploader.onAfterAddingFile = (file) => this.uploadTemp(file);
    this.tempFileUploader.onAfterAddingAll = files => this.uploadTemps(files);
    this.createForm();
  }

  ngOnInit(): void {
    this.changeExpirationTimeForTempFiles();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  // @formatter:off
  get subject(): AbstractControl { return this.mailForm.get('subject'); }
  // @formatter:on

  private createForm(): void {
    this.mailForm = this.fb.group({
      subject: [null, Validators.required],
    });
    this.content = '';
  }

  private getMail(): NewMail {
    return {
      subject: this.subject.value,
      content: this.content,
      to: this.toEmails.getEmails(),
      cc: this.ccEmails.getEmails(),
      bcc: this.bccEmails.getEmails(),
      attachments: this.getAttachment(),
      ignoreGdpr: this.ignoreGdpr
    };
  }


  private getAttachment(): NewMailAttachment[] {
    const result: NewMailAttachment[] = [];
    if (this.tempFileUploader && this.tempFileUploader.queue) {
      const tempFiles = this.tempFileUploader.queue.map((c: any): NewMailAttachment => {
        return {
          fileId: JSON.parse(this.tempFileUploaderResponseMap.get(c)).id,
          tempFile: true
        };
      });
      result.push(...tempFiles);
    }

    if (this.filesFormCrm.length !== 0) {
      const crmFiles = this.filesFormCrm.map((file: FileInfo): NewMailAttachment => {
        return {
          fileId: file.id,
          tempFile: false
        };
      });
      result.push(...crmFiles);
    }
    return result;
  }

  private changeExpirationTimeForTempFiles(): void {
    interval(5 * 60 * 1000)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.tempFileService.setNewExpirationTimeForTemporaryFiles().pipe(takeUntil(this.componentDestroyed)).subscribe();
      });
  }

  submit(): void {
    this.markFormAsTouched();
    const allFilesUploaded = this.allFilesUploaded();
    if (this.mailForm.valid && this.areEmailFieldsValid() && allFilesUploaded) {
      this.isSending = true;
      this.mailService.sendMail(this.getMail())
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => {
          this.notificationService.notify('mail.notification.sendEmailSuccess', NotificationType.SUCCESS);
          this.cancel();
          this.submittedSuccessfully.emit();
          this.isSending = false;
        }, (err) => {
          if (err.status === 409) {
            this.notificationService.notify('mail.notification.sendEmailGdprError', NotificationType.ERROR);
          } else {
            this.notificationService.notify('mail.notification.sendEmailError', NotificationType.ERROR);
          }
          this.isSending = false;
        });
    }
    if (!allFilesUploaded) {
      this.notificationService.notify('mail.notification.filesNotUploaded', NotificationType.ERROR);
    }
  }

  private markFormAsTouched(): void {
    FormUtil.setTouched(this.mailForm);
    this.toEmails.markAsTouched();
  }

  cancel(): void {
    this.closeNewMessage.emit();
  }

  private areEmailFieldsValid(): boolean {
    return this.toEmails.isValid() && this.ccEmails.isValid() && this.bccEmails.isValid();
  }

  private allFilesUploaded(): boolean {
    return (!this.tempFileUploader || !this.tempFileUploader.queue) ||
      (this.tempFileUploader.queue.filter(item => !item.isSuccess).length === 0);
  }

  public uploadTemps(files: any[]): void {
    files.forEach(file => this.uploadTemp(file));
  }

  public uploadTemp(file: any): void {
    this.tempFileUploader.uploadItemWithForm(file, {description: 'Attachment'});
    file.onSuccess = response => {
      this.tempFileUploaderResponseMap.set(file, response);
    };
    file.onError = response => {
      if (JSON.parse(response).status === UNAUTHORIZED) {
        this.tempFileUploader.clearQueue();
        return;
      }
      uploadFileErrorDialogData.onConfirmClick = () => this.uploadTemp(file);
      uploadFileErrorDialogData.onCancelClick = () => {
        this.removeTempFile(file);
      };
      this.deleteDialogService.open(uploadFileErrorDialogData);
    };
  }

  removeCrmFile(file: FileInfo): void {
    this.filesFormCrm = this.filesFormCrm.filter(c => c.id !== file.id);
  }
  removeTempFile(file: any): void {
    this.tempFileUploader.queue = this.tempFileUploader.queue.filter(c => c !== file);
  }

  public setToAddress(email: string): void {
    this.toEmails.setAddress(email);
  }
  public setToContent(content: string): void {
    this.mailTextEditorComponent.htmlContent = content;
    this.content = content;
  }
  public setToSubject(subject: string): void {
    this.subject.patchValue(subject);
  }
  public setToIgnoreGdpr(ignoreGdpr: boolean): void {
    this.ignoreGdpr = ignoreGdpr;
  }

  public restForm(): void {
    this.mailForm.reset();
    this.toEmails.reset();
    this.ccEmails.reset();
    this.bccEmails.reset();
    this.mailTextEditorComponent.reset();
    this.tempFileUploader = new TempFileUploaderService(this.loginService);
    this.filesFormCrm = [];
  }

  selectFilesFromCrm(): void {
    const data: MailSelectFileDialogData = {
      onConfirmClick: (selectedFiles: FileInfo[]) => {
        this.filesFormCrm = [...selectedFiles];
      },
      selectedFiles: this.filesFormCrm
    };
    this.mailSelectFileDialogService.open(data);
  }
}

