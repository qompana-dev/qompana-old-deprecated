import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxFilesizeModule} from 'ngx-filesize';
import {MailAttachmentsComponent} from './mail-attachments.component';
import {SharedModule} from '../../../../shared/shared.module';

@NgModule({
  declarations: [
    MailAttachmentsComponent
  ],
  exports: [
    MailAttachmentsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxFilesizeModule,
  ]
})
export class MailAttachmentsModule {
}
