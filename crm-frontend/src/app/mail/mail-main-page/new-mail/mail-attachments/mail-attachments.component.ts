import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {TempFileUploaderService} from '../../../../shared/temp-file/temp-file-uploader.service';
import {MailContentAttachment} from '../../../mail.model';
import {takeUntil} from 'rxjs/operators';
import {MailService} from '../../../mail.service';
import {Observable, Subject} from 'rxjs';
import {FileInfo, FileSizeDto} from '../../../../files/file.model';
import {FileService} from '../../../../files/file.service';
import {saveAs} from 'file-saver';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {CrmObjectType} from '../../../../shared/model/search.model';

@Component({
  selector: 'app-mail-attachments',
  templateUrl: './mail-attachments.component.html',
  styleUrls: ['./mail-attachments.component.scss']
})
export class MailAttachmentsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() previewAttachments: PreviewAttachments;
  @Input() previewStorageAttachments: PreviewStorageAttachments;
  @Input() previewStorageActive = false;

  @Input() tempUploader: TempFileUploaderService;
  @Input() filesFromCrm: FileInfo[];
  @Output() removeTempFile: EventEmitter<any> = new EventEmitter<any>();
  @Output() removeCrmFile: EventEmitter<FileInfo> = new EventEmitter<FileInfo>();

  private componentDestroyed: Subject<void> = new Subject();

  files: MailContentAttachment[];

  systemFileSizes: Map<string, number> = new Map<string, number>();

  constructor(private mailService: MailService,
              private fileService: FileService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.previewStorageActive) {
      this.files = (this.previewStorageAttachments) ? this.previewStorageAttachments.files : undefined;
    } else {
      this.files = (this.previewAttachments) ? this.previewAttachments.files : undefined;
    }
    this.updateFileSize();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  downloadPreviewAttachment(mailContentAttachment: MailContentAttachment): void {
    let downloadMethod;
    if (this.previewStorageActive) {
      downloadMethod = this.mailService.downloadFileFromStorage(this.previewStorageAttachments.objectId, this.previewStorageAttachments.mailStorageId, this.previewStorageAttachments.objectType, mailContentAttachment.name);
    } else {
      downloadMethod = this.mailService.downloadFile(this.previewAttachments.folderName, this.previewAttachments.messageId, mailContentAttachment.name);
    }

    downloadMethod.pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        blob => saveAs(blob, mailContentAttachment.name)
      );
  }

  getSize(fileId: string): number {
    const size = this.systemFileSizes.get(fileId);
    return (size) ? size : 0;
  }

  updateFileSize(): void {
    if (this.filesFromCrm && this.filesFromCrm.length !== 0) {
      this.fileService.getFileSize(this.filesFromCrm.map(c => c.id))
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((sizes: FileSizeDto[]) => {
          sizes.forEach(size => this.systemFileSizes.set(size.fileId, size.size));
        });
    }
  }

  saveAttachment(mailContentAttachment: MailContentAttachment): void {
    let saveAttachmentMethod: Observable<void>;
    if (this.previewStorageActive) {
      saveAttachmentMethod = this.mailService.saveAttachmentInCrm(this.previewStorageAttachments.objectId, this.previewStorageAttachments.mailStorageId, this.previewStorageAttachments.objectType, mailContentAttachment.name);
    } else {
      saveAttachmentMethod = this.mailService.saveFileInCrm(this.previewAttachments.folderName, this.previewAttachments.messageId, mailContentAttachment.name);
    }
    saveAttachmentMethod.pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => this.notificationService.notify('mail.notification.crmFileSaveSuccess', NotificationType.SUCCESS),
        (err) => this.notificationService.notify('mail.notification.crmFileSaveError', NotificationType.ERROR)
      );
  }

}

export interface PreviewStorageAttachments {
  files: MailContentAttachment[];
  objectType: CrmObjectType;
  objectId: number;
  mailStorageId: number;
}

export interface PreviewAttachments {
  files: MailContentAttachment[];
  folderName: string;
  messageId: number;
}
