import {Component, Input, OnDestroy, OnInit, ViewChild, AfterViewChecked, ChangeDetectorRef} from '@angular/core';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {NewMailComponent} from '../new-mail.component';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';
import {MailService} from '../../../mail.service';
import {LoginService} from '../../../../login/login.service';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';

@Component({
  selector: 'app-new-mail-slider',
  templateUrl: './new-mail-slider.component.html',
  styleUrls: ['./new-mail-slider.component.scss']
})
export class NewMailSliderComponent implements OnInit, OnDestroy, AfterViewChecked {

  @ViewChild(NewMailComponent) newMailComponent: NewMailComponent;

  @Input() slider: SliderComponent;
  @Input() email: string = '';
  @Input() subject: string = '';
  @Input() content: string = '';
  @Input() ignoreGdpr: boolean = false;

  mailConfigurationExist = true;

  private componentDestroyed: Subject<void> = new Subject();


  constructor(private router: Router,
              private loginService: LoginService,
              private notificationService: NotificationService,
              private mailService: MailService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  ngOnInit(): void {
    if (this.slider) {
      this.slider.closeEvent.pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        () => this.resetForm()
      );

      this.slider.openEvent.pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        () => {
          this.setAddress();
          this.checkIfConfigurationExist();
        }
      );
    }
  }

  ngAfterViewChecked(){
    this.cdr.detectChanges();
  }

  submit(): void {
    this.newMailComponent.submit();
  }


  close(): void {
    this.slider.close();
  }

  openMailConfiguration(): void {
    this.router.navigate(['/person', 'info']);
  }

  private resetForm(): void {
    this.newMailComponent.restForm();
  }

  private setAddress(): void {
    if (this.email) {
      this.newMailComponent.setToAddress(this.email);
      this.newMailComponent.setToContent(this.content);
      this.newMailComponent.setToSubject(this.subject);
      this.newMailComponent.setToIgnoreGdpr(this.ignoreGdpr);
    }
  }

  private checkIfConfigurationExist(): void {
    this.mailService.configurationExist(this.loginService.getLoggedAccountId())
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((exist: boolean) => {
        this.mailConfigurationExist = exist;
      }, () => {
        this.mailConfigurationExist = false;
        this.notificationService.notify('mail.newMailSlider.configurationExistCheckError', NotificationType.ERROR);
      });
  }
}
