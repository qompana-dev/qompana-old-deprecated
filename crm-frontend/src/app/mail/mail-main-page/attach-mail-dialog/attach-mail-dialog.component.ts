import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {CrmObject, CrmObjectType} from '../../../shared/model/search.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Subject} from 'rxjs';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {MailService} from '../../mail.service';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
import {debounceTime, filter, finalize, switchMap, takeUntil, tap} from 'rxjs/operators';
import {FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import {MainSearchService} from '../../../navigation/main-search/main-search.service';

@Component({
  selector: 'app-attach-mail-dialog',
  templateUrl: './attach-mail-dialog.component.html',
  styleUrls: ['./attach-mail-dialog.component.scss']
})
export class AttachMailDialogComponent implements OnInit, OnDestroy {

  private componentDestroyed: Subject<void> = new Subject();
  crmObjectTypeEnum = CrmObjectType;

  selectedCrmObject: CrmObject;

  showError = false;

  areSearchLoading = false;
  objectList: CrmObject[] = [];
  searchForm: FormGroup = new FormGroup({
    searchText: new FormControl('')
  });


  readonly errorTypes: ErrorTypes = {
    base: 'mail.attachDialog.notification.',
    defaultText: 'attachError',
    errors: [
      {
        code: FORBIDDEN,
        text: 'permissionDenied'
      }, {
        code: NOT_FOUND,
        text: 'messageNotFound'
      }
    ]
  };


  // @formatter:off
  get searchText(): AbstractControl { return this.searchForm.get('searchText'); }
  // @formatter:on


  constructor(@Inject(MAT_DIALOG_DATA) public data: AttachMailDialogData,
              public dialogRef: MatDialogRef<AttachMailDialogComponent>,
              private mainSearchService: MainSearchService,
              private notificationService: NotificationService,
              private mailService: MailService) {
  }

  ngOnInit(): void {
    this.showError = false;
    this.searchText.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      tap((value: string) => this.clearSelectedItemIfChanged(value)),
      filter((input: string) => !!input && input.length >= 3),
      tap(() => this.areSearchLoading = true),
      tap(() => this.showError = false),
      switchMap(((input: string) => this.mainSearchService.searchForType(this.data.destinationType, input, [this.data.destinationType])
          .pipe(finalize(() => this.areSearchLoading = false))
      ))).subscribe(
      (result: CrmObject[]) => {
        this.objectList = result;
      }
    );
  }

  private clearSelectedItemIfChanged(value: string) {
    if (this.selectedCrmObject && value !== this.selectedCrmObject.label) {
      this.selectedCrmObject = undefined;
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    if (this.selectedCrmObject) {
      this.mailService.attachMessage(this.selectedCrmObject.id, this.data.messageId, this.data.destinationType)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => {
          let key = '';
          switch(this.data.destinationType) {
            case CrmObjectType.lead: key = 'mailAttachedToLead'; break;
            case CrmObjectType.opportunity: key = 'mailAttachedToOpportunity'; break;
            case CrmObjectType.contact: key = 'mailAttachedToContact'; break;
          }
          this.notificationService.notify(`mail.attachDialog.notification.${key}`, NotificationType.SUCCESS)
          this.close();
        }, (error) => this.notificationService.notifyError(this.errorTypes, error.status));
    } else {
      this.showError = true;
    }
  }

  onSelect(crmObject: CrmObject): void {
    this.selectedCrmObject = crmObject;
  }

  getSearchPlaceholder(): string {
    const prefix = 'mail.attachDialog.';
    switch (this.data.destinationType) {
      case CrmObjectType.lead:
        return prefix + 'searchLead';
      case CrmObjectType.opportunity:
        return prefix + 'searchOpportunity';
      case CrmObjectType.contact:
        return prefix + 'searchContact';
      default:
        return '';
    }
  }

  close(): void {
    this.dialogRef.close();
  }
}

export interface AttachMailDialogData {
  destinationType: CrmObjectType;
  messageId: number;
}
