import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {forkJoin, of, Subject} from 'rxjs';
import {MatChipInputEvent} from '@angular/material';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {MailService} from '../../mail.service';
import {catchError, debounceTime, filter, finalize, map, switchMap, takeUntil, tap} from 'rxjs/operators';
import {MailSearch} from '../../mail.model';
import {CrmObjectType} from '../../../shared/model/search.model';

@Component({
  selector: 'app-mail-input',
  templateUrl: './mail-input.component.html',
  styleUrls: ['./mail-input.component.scss']
})
export class MailInputComponent implements OnInit, OnDestroy {

  constructor(private fb: FormBuilder,
              private mailService: MailService) {
  }

  @Input() placeholder: string;
  @Input() required = false;
  @Input() addresses: string[];
  @ViewChild('mailInput') tagInput: ElementRef<HTMLInputElement>;
  mailForm: FormGroup;
  availableMails: { [index: string]: MailSearch[] };
  areMailsLoading: boolean;
  private componentDestroyed: Subject<void> = new Subject();
  private lastInput: string;

  ngOnInit(): void {
    this.createForm(this.required);
    this.listenForMailInput();
    if (this.addresses) {
      this.addresses.forEach(e => this.mails.patchValue([...this.mails.value, e]));
      this.mails.updateValueAndValidity();
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  get mails(): AbstractControl {
    return this.mailForm.get('mails');
  }

  get currentMail(): AbstractControl {
    return this.mailForm.get('currentMail');
  }

  public setAddress(address: string): void {
    if (address && this.mailForm) {
      this.mails.patchValue([address]);
      this.mails.updateValueAndValidity();
    }
  }

  public isValid(): boolean {
    return this.mailForm && this.mailForm.valid;
  }

  public getEmails(): string[] {
    return this.mails.value;
  }

  public markAsTouched(): void {
    this.mails.markAsTouched();
  }

  removeMail(removedTag: string): void {
    const index = this.mails.value.indexOf(removedTag);
    if (index >= 0) {
      this.mails.value.splice(index, 1);
      this.mails.updateValueAndValidity();
    }
  }


  addMail(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    const index = this.mails.value.indexOf(value);
    if (index < 0 && (value || '').trim()) {
      this.mails.setValue([...this.mails.value, value.trim()]);
      this.mails.updateValueAndValidity();
    }
    if (input) {
      input.value = '';
    }
  }

  selected(selectedTag: string): void {
    this.addToMailList(selectedTag);
    this.tagInput.nativeElement.value = '';
    if (selectedTag !== this.lastInput) {
      this.removeMail(this.lastInput);
    }
    this.lastInput = null;
  }

  private addToMailList(mail: string): void {
    if (!(mail || '').trim()) {
      return;
    }
    const index = this.mails.value.indexOf(mail);
    if (index < 0) {
      this.mails.setValue([...this.mails.value, mail.trim()]);
      this.mails.updateValueAndValidity();
    }
  }

  private createForm(required: boolean = false): void {
    const validators = required ? [this.getCorrectEmailValidator(), Validators.required] : [this.getCorrectEmailValidator()];
    this.mailForm = this.fb.group({
      mails: [[], Validators.compose(validators)],
      currentMail: []
    });
  }

  private listenForMailInput(): void {
    this.currentMail.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(750),
      filter((input: string) => !!input),
      tap((input) => {
        this.areMailsLoading = true;
        this.availableMails = {};
        this.lastInput = input;
      }),
      switchMap(((input: string) => forkJoin(
        this.mailService.searchUserMails(input),
        this.mailService.searchContactMails(input),
        this.mailService.searchSentOrReceivedMails(input),
        ).pipe(
        map(result => this.groupByType(result)),
        catchError(() => of({})),
        finalize(() => this.areMailsLoading = false)
        ))
      )).subscribe(
      result => this.availableMails = result
    );
  }

  private getCorrectEmailValidator(): (control: AbstractControl) => ValidationErrors {
    return (control: AbstractControl): ValidationErrors => {
      return (control.value.every(email => this.emailIsValid(email))) ? null : {emailInvalid: true};
    };
  }

  private emailIsValid(email: string): boolean {
    if (email && email === '') {
      return true;
    }
    return /\S+@\S+\.\S+/.test(email);
  }

  private groupByType(result: MailSearch[][]): { [index: string]: MailSearch[] } {
    const resultMap = {};
    resultMap[CrmObjectType.user] = result[0];
    resultMap[CrmObjectType.contact] = result[1];
    resultMap[CrmObjectType.mail] = result[2];
    return resultMap;
  }

  getKeys(object: any): string[] {
    return object ? Object.keys(object) : [];
  }

  reset(): void {
    this.mails.setValue([]);
    this.currentMail.setValue(null);
    this.areMailsLoading = false;
    this.availableMails = {};
    this.lastInput = null;
  }
}
