import { MailStorageDto } from "./../mail.model";
import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  MailConfigurationDto,
  MailContent,
  MailFolders,
  MailHeader,
  MailSortStrategy,
} from "../mail.model";
import { MailService } from "../mail.service";
import {
  NotificationService,
  NotificationType,
} from "../../shared/notification.service";
import {
  InfinityScrollUtil,
  InfinityScrollUtilInterface,
} from "../../shared/infinity-scroll.util";
import { Observable, of, Subject } from "rxjs";
import { FilterStrategy } from "../../shared/model";
import { Page, PageRequest } from "../../shared/pagination/page.model";
import { MailUtilService } from "../mail-util.service";
import { TranslateService } from "@ngx-translate/core";
import { Sort } from "@angular/material";
import { catchError, exhaustMap, takeUntil, tap } from "rxjs/operators";
import { WebsocketService } from "../../shared/websocket.service";
import { LoginService } from "../../login/login.service";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import {
  AttachMailDialogComponent,
  AttachMailDialogData,
} from "./attach-mail-dialog/attach-mail-dialog.component";
import { CrmObjectType } from "../../shared/model/search.model";

@Component({
  selector: "app-mail-main-page",
  templateUrl: "./mail-main-page.component.html",
  styleUrls: ["./mail-main-page.component.scss"],
})
export class MailMainPageComponent
  implements OnInit, OnDestroy, InfinityScrollUtilInterface {
  selectedSort: MailSortStrategy = MailSortStrategy.receivedDate;
  sortSelection = MailSortStrategy;
  selectedMessage: MailContent;

  openedObject = null;
  iconByObjectTypes = {
    lead: "Leads",
    opportunity: "Opportunity",
  };

  mailFolders: MailFolders;
  infinityScroll: InfinityScrollUtil;
  selectedFolder: string;
  selectedMessageId: number;
  newMessage = false;
  private selectedFromFolderList = true;
  refreshInProgress = false;

  downloadMailsError = false;

  removingInProgress = false;

  mailConfigurations: MailConfigurationDto[] = [];
  activeConfiguration: number;

  constructor(
    private mailService: MailService,
    private mailUtil: MailUtilService,
    private router: Router,
    private dialog: MatDialog,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private webSocketService: WebsocketService,
    private loginService: LoginService
  ) {
    this.openedObject = this.router.getCurrentNavigation().extras.state;
  }

  private componentDestroyed: Subject<void> = new Subject();
  messageSelectedSubject: Subject<MailHeader> = new Subject();
  messageSelected$: Observable<MailHeader> = this.messageSelectedSubject.asObservable();

  ngOnInit(): void {
    this.getAllUserConfigurations();
    this.getMailFolders();
    this.listenOnMessageSelected();
    this.listenForMailListChanged();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  sort(sortStrategy: MailSortStrategy): void {
    const sort = this.getSort(sortStrategy);
    this.infinityScroll.onSortChange(sort);
    this.selectedSort = sortStrategy;
  }

  isSelected(selection: MailSortStrategy): boolean {
    return this.selectedSort === selection;
  }

  changeActiveConfiguration(configurationId: number): void {
    this.mailService
      .changeConfiguration(configurationId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.getAllUserConfigurations();
          this.selectedFolder = undefined;
          this.selectedMessageId = undefined;
          this.selectedMessage = undefined;
          this.getMailFolders();
        },
        () =>
          this.notificationService.notify(
            "mail.notification.changeActiveConfError",
            NotificationType.ERROR
          )
      );
  }

  private getAllUserConfigurations(): void {
    this.mailService
      .getUserConfigurations(this.loginService.getLoggedAccountId())
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (configurations: MailConfigurationDto[]) => {
          this.mailConfigurations = configurations;
          const active = this.mailConfigurations.find((conf) => conf.active);
          this.activeConfiguration = active ? active.id : undefined;
        },
        () =>
          this.notificationService.notify(
            "mail.notification.getConfigurationListError",
            NotificationType.ERROR
          )
      );
  }

  private getMailFolders(): void {
    this.mailService.getMailFolders().subscribe(
      (mailFolders) => {
        this.mailFolders = mailFolders;
        this.selectedFolder =
          this.selectedFolder || this.getFirstFolder(mailFolders);
        this.initInfinityScroll();
        this.downloadMailsError = false;
      },
      () => {
        this.downloadMailsError = true;
      }
    );
  }

  private getFirstFolder(mailFolders: MailFolders): string | null {
    if (this.openedObject) {
      return "attachedObject";
    }
    if (mailFolders && mailFolders.folders && mailFolders.folders.length > 0) {
      return mailFolders.folders[0].folderName;
    }
    return null;
  }

  attachMailTo(dest: "lead" | "opportunity" | "contact"): void {
    let destinationType;

    if (dest === "lead") {
      destinationType = CrmObjectType.lead;
    } else if (dest === "contact") {
      destinationType = CrmObjectType.contact;
    } else {
      destinationType = CrmObjectType.opportunity;
    }

    const dialogData: AttachMailDialogData = {
      destinationType,
      messageId: this.selectedMessageId,
    };
    this.dialog.open(AttachMailDialogComponent, {
      width: "500px",
      height: "310px",
      data: dialogData,
      panelClass: "attach-mail-container",
    });
  }

  isFolderSelected(folder: string): boolean {
    return this.selectedFolder === folder;
  }

  selectFolder(folder: string): void {
    this.selectedFromFolderList = true;
    this.selectedFolder = folder;
    this.infinityScroll.initData();
  }

  getItemObservable(
    selection: FilterStrategy,
    pageRequest: PageRequest
  ): Observable<Page<MailHeader | MailStorageDto>> {
    if (this.openedObject && this.selectedFolder === "attachedObject") {
      return this.mailService.getAttachedMailHeaderList(
        pageRequest,
        this.openedObject.id,
        this.openedObject.type
      );
    }

    return this.mailService.getMailHeaderList(pageRequest, this.selectedFolder);
  }

  getMailHeader(header: MailHeader): string {
    return this.mailUtil.getFromMailTitle(header);
  }

  private initInfinityScroll(): void {
    this.infinityScroll = new InfinityScrollUtil(
      this,
      this.componentDestroyed,
      this.notificationService,
      "mail.notification.getMailHeaderListError",
      "receivedDate,desc"
    );
    this.infinityScroll.onScrollInit$
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.selectFirstMessage());
  }

  private getSort(sortStrategy: MailSortStrategy): Sort {
    if (sortStrategy === MailSortStrategy.unread) {
      return { active: "seen", direction: "asc" };
    }
    if (sortStrategy === MailSortStrategy.receivedDate) {
      return { active: "receivedDate", direction: "desc" };
    }
    if (sortStrategy === MailSortStrategy.subject) {
      return { active: "subject", direction: "asc" };
    }
  }

  getMessage(header: MailHeader): void {
    this.selectedMessage = null;
    this.messageSelectedSubject.next(header);
  }

  private markAsSeen(header: MailHeader): void {
    if (!header.seen) {
      const mailFolder = this.mailFolders.folders.find(
        (folder) => folder.folderName === header.folderName
      );
      if (mailFolder != null) {
        mailFolder.unreadMessages--;
      }
      header.seen = true;
    }
  }

  translateFolderName(folderName: string): string {
    const translationKey = `mail.${folderName && folderName.toLowerCase()}`;
    const translation = this.translateService.instant(translationKey);
    return translationKey === translation ? folderName : translation;
  }

  isHeaderSelected(header: MailHeader): boolean {
    return (
      header &&
      this.selectedMessage &&
      this.selectedMessage.messageId === header.messageId
    );
  }

  private listenOnMessageSelected(): void {
    this.messageSelected$
      .pipe(
        takeUntil(this.componentDestroyed),
        tap((header) => {
          this.selectedMessageId = header.id;
          this.selectedFromFolderList = false;
        }),
        exhaustMap((header) =>
          this.mailService.getMessage(header.id).pipe(
            tap(() => this.markAsSeen(header)),
            catchError(() => of(null))
          )
        )
      )
      .subscribe((message: MailContent) => {
        this.newMessage = false;
        this.selectedMessage = message;
      });
  }

  refresh(): void {
    if (this.refreshInProgress) {
      return;
    }
    this.refreshInProgress = true;
    this.mailService
      .refreshMessages()
      .pipe(
        takeUntil(this.componentDestroyed),
        tap(() => (this.refreshInProgress = true))
      )
      .subscribe(
        () => {},
        (err) =>
          this.notificationService.notify(
            "mail.notification.refreshMessagesError",
            NotificationType.ERROR
          ),
        () => (this.refreshInProgress = false)
      );
  }

  private listenForMailListChanged(): void {
    const id = this.loginService.getLoggedAccountId();
    this.webSocketService
      .listenMailListChanged(id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.getMailFolders());
  }

  private selectFirstMessage(): void {
    if (
      this.infinityScroll &&
      this.infinityScroll.itemPage &&
      this.infinityScroll.itemPage.content &&
      this.infinityScroll.itemPage.content.length !== 0 &&
      !this.newMessage &&
      this.selectedFromFolderList
    ) {
      this.getMessage(this.infinityScroll.itemPage.content[0]);
    }
  }

  openMailConfiguration(): void {
    this.router.navigate(["/person", "info"]);
  }

  removeCurrentMessage(): void {
    if (
      !this.removingInProgress &&
      this.selectedMessage &&
      this.selectedMessage.messageId
    ) {
      this.removingInProgress = true;
      this.mailService
        .deleteMessage(this.selectedFolder, +this.selectedMessage.messageId)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => {
            this.notificationService.notify(
              "mail.notification.deleteSuccess",
              NotificationType.SUCCESS
            );
            this.removingInProgress = false;
          },
          () => {
            this.notificationService.notify(
              "mail.notification.deleteError",
              NotificationType.ERROR
            );
            this.removingInProgress = false;
          }
        );
    }
  }
}
