import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'mailBoxIcon'
})
export class MailBoxIconPipe implements PipeTransform {

  transform(value: string): any {
    if (!value) {
      return '';
    }
    switch (value.toLowerCase()) {
      case 'sent':
      case 'wysłane':
      case 'outbox':
        return 'assets/icons/mail/outbox.svg';
      case 'trash':
      case 'kosz':
        return 'assets/icons/mail/trash.svg';
      case 'junk':
      case 'spam':
        return 'assets/icons/mail/spam.svg';
      default:
        return 'assets/icons/mail/inbox.svg';

    }
  }

}
