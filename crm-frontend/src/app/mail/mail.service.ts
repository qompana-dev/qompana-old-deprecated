import {Injectable} from '@angular/core';
import {
  ForwardedMail,
  MailConfigurationDto,
  MailContent,
  MailFolders,
  MailHeader,
  MailSearch,
  MailStorageDto,
  NewMail,
  SimpleMailConfigurationDto
} from './mail.model';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Page, PageRequest} from '../shared/pagination/page.model';
import {catchError} from 'rxjs/operators';
import {UrlUtil} from '../shared/url.util';
import {CrmObjectType} from '../shared/model/search.model';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  private url = `${UrlUtil.url}/crm-mail-service`;

  constructor(private http: HttpClient) {
  }

  initializeMail(mailConfigurationDto: MailConfigurationDto): Observable<MailConfigurationDto> {
    return this.http.post<MailConfigurationDto>(`${this.url}`, mailConfigurationDto);
  }

  initializeSimpleMailConfiguration(simpleMailConfigurationDto: MailConfigurationDto | SimpleMailConfigurationDto): Observable<MailConfigurationDto> {
    return this.http.post<MailConfigurationDto>(`${this.url}/simple`, simpleMailConfigurationDto);
  }

  editMail(configurationId: number, mailConfigurationDto: MailConfigurationDto): Observable<MailConfigurationDto> {
    return this.http.put<MailConfigurationDto>(`${this.url}/${configurationId}`, mailConfigurationDto);
  }

  editMailSimpleConfiguration(configurationId: number, simpleMailConfigurationDto: MailConfigurationDto | SimpleMailConfigurationDto): Observable<MailConfigurationDto> {
    return this.http.put<MailConfigurationDto>(`${this.url}/simple/${configurationId}`, simpleMailConfigurationDto);
  }

  getInitData(id: number): Observable<MailConfigurationDto> {
    return this.http.get<MailConfigurationDto>(`${this.url}/${id}`);
  }

  getMailFolders(): Observable<MailFolders> {
    return this.http.get<MailFolders>(`${this.url}/mail-folders`);
  }

  getMailHeaderList(pageRequest: PageRequest, selectedFolder: string): Observable<Page<MailHeader>> {
    const params = new HttpParams({fromObject: pageRequest as any});
    return this.http.get<Page<MailHeader>>(`${this.url}/${selectedFolder}/mail-headers`, {params});
  }

  public getAttachedMailHeaderList(pageRequest: PageRequest, id: number, type: CrmObjectType): Observable<Page<MailHeader>> {
    const params = new HttpParams({fromObject: pageRequest as any});
    return this.http.get<Page<MailHeader>>(`${this.url}/mail-headers/attached/${type}/${id}`, {params});
  }

  getMessage(messageId: number): Observable<MailContent> {
    return this.http.get<MailContent>(`${this.url}/messages/${messageId}`);
  }

  refreshMessages(): Observable<void> {
    return this.http.get<void>(`${this.url}/messages/refresh`);
  }

  sendMail(newMail: NewMail): Observable<void> {
    return this.http.post<void>(`${this.url}/mail/send`, newMail);
  }

  forwardMail(forwardedMail: ForwardedMail): Observable<void> {
    return this.http.post<void>(`${this.url}/mail/forward`, forwardedMail);
  }

  searchUserMails(input: string): Observable<MailSearch[]> {
    return this.http.get<MailSearch[]>(`${UrlUtil.url}/crm-user-service/accounts/search-emails?term=${input}`)
      .pipe(catchError(() => of([])));
  }

  searchContactMails(input: string): Observable<MailSearch[]> {
    return this.http.get<MailSearch[]>(`${UrlUtil.url}/crm-business-service/contacts/search-emails?term=${input}`)
      .pipe(catchError(() => of([])));
  }

  searchSentOrReceivedMails(input: string): Observable<MailSearch[]> {
    return this.http.get<MailSearch[]>(`${this.url}/mails/search?term=${input}`)
      .pipe(catchError(() => of([])));
  }

  configurationExist(accountId: number): Observable<boolean> {
    return this.http.get<boolean>(`${this.url}/${accountId}/exist`);
  }

  getUserConfigurations(accountId: number): Observable<MailConfigurationDto[]> {
    return this.http.get<MailConfigurationDto[]>(`${this.url}/account/${accountId}`);
  }

  changeConfiguration(configurationId: number): Observable<MailConfigurationDto> {
    return this.http.post<MailConfigurationDto>(`${this.url}/${configurationId}/active`, null);
  }

  removeConfiguration(configurationId: number): Observable<void> {
    return this.http.delete<void>(`${this.url}/${configurationId}`);
  }

  public downloadFile(folderName: string, messageId: number, fileName: string): Observable<Blob> {
    const url = `${this.url}/messages/folder/${folderName}/message-id/${messageId}/file-name/${fileName}`;
    return this.http.get(url, {responseType: 'blob'});
  }

  public downloadFileFromStorage(id: number, mailStorageId: number, type: CrmObjectType, fileName: string): Observable<Blob> {
    const url = `${this.url}/mail-storage/content/${type}/${id}/mail-storage-id/${mailStorageId}/file/${fileName}`;
    return this.http.get(url, {responseType: 'blob'});
  }

  public saveFileInCrm(folderName: string, messageId: number, fileName: string): Observable<void> {
    const url = `${this.url}/messages/save-in-crm/folder/${folderName}/message-id/${messageId}/file-name/${fileName}`;
    return this.http.get<void>(url);
  }

  public deleteMessage(folderName: string, messageId: number): Observable<void> {
    const url = `${this.url}/messages/folder/${folderName}/message-id/${messageId}`;
    return this.http.delete<void>(url);
  }

  public getMailHeaders(id: number, type: CrmObjectType): Observable<Page<MailStorageDto>> {
    return this.http.get<Page<MailStorageDto>>(`${this.url}/mail-storage/headers/${type}/${id}`);
  }

  public getMailHeadersPage(pageRequest: PageRequest, id: number, type: CrmObjectType): Observable<Page<MailStorageDto>> {
    const params = new HttpParams({fromObject: pageRequest as any});
    return this.http.get<Page<MailStorageDto>>(`${this.url}/mail-storage/headers/${type}/${id}`, {params});
  }

  public attachMessage(id: number, messageId: number, type: CrmObjectType): Observable<void> {
    return this.http.post<void>(`${this.url}/mail-storage/attach/${type}/${id}/message/${messageId}`, undefined);
  }

  public getMessageFromStorage(id: number, mailStorageId: number, type: CrmObjectType): Observable<MailContent> {
    return this.http.get<MailContent>(`${this.url}/mail-storage/content/${type}/${id}/mail-storage-id/${mailStorageId}`);
  }

  public saveAttachmentInCrm(id: number, mailStorageId: number, type: CrmObjectType, fileName: string): Observable<void> {
    return this.http.post<void>(`${this.url}/mail-storage/content/${type}/${id}/mail-storage-id/${mailStorageId}/file/${fileName}`, undefined);
  }

  public detachMessage(id: number, mailStorageId: number, type: CrmObjectType): Observable<MailContent> {
    return this.http.delete<MailContent>(`${this.url}/mail-storage/content/${type}/${id}/mail-storage-id/${mailStorageId}`);
  }
}
