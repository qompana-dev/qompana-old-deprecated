import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MailMainPageComponent} from './mail-main-page/mail-main-page.component';
import {MailRoutingModule} from './mail-routing.module';
import {SharedModule} from '../shared/shared.module';
import {MessageDetailsComponent} from './mail-main-page/message-details/message-details.component';
import {MailBoxIconPipe} from './mail-main-page/mail-box-icon.pipe';
import {NewMailComponent} from './mail-main-page/new-mail/new-mail.component';
import {MailTextEditorComponent} from './mail-text-editor/mail-text-editor.component';
import {NgxEditorModule} from 'ngx-editor';
import {MailInputComponent} from './mail-main-page/mail-input/mail-input.component';
import {ReplyToMessageComponent} from './mail-main-page/message-details/reply-to-message/reply-to-message.component';
import {ForwardMessageComponent} from './mail-main-page/message-details/forward-message/forward-message.component';
import {NewMailSliderComponent} from './mail-main-page/new-mail/new-mail-slider/new-mail-slider.component';
import {MailSelectFileDialogComponent} from './mail-main-page/new-mail/mail-select-file-dialog/mail-select-file-dialog.component';
import {AttachMailDialogComponent} from './mail-main-page/attach-mail-dialog/attach-mail-dialog.component';
import {HtmlInFrameModule} from './mail-main-page/message-details/html-in-frame/html-in-frame.module';
import {MailAttachmentsModule} from './mail-main-page/new-mail/mail-attachments/mail-attachments.module';

@NgModule({
  declarations: [
    MailTextEditorComponent,
    MailMainPageComponent,
    MessageDetailsComponent,
    MailBoxIconPipe,
    NewMailComponent,
    MailInputComponent,
    ReplyToMessageComponent,
    ForwardMessageComponent,
    NewMailSliderComponent,
    MailSelectFileDialogComponent,
    AttachMailDialogComponent,
  ],
  exports: [
    NewMailSliderComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxEditorModule,
    HtmlInFrameModule,
    MailAttachmentsModule,
    MailRoutingModule,
  ],
  entryComponents: [AttachMailDialogComponent]
})
export class MailModule {
}
