export class MailTextEditorConfig {
  static config = {
    editable: true,
    spellcheck: false,
    height: 'auto',
    minHeight: '0',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    imageEndPoint: '',
    toolbar: [
      [
        'fontSize', 'bold', 'italic',
        'underline', 'strikeThrough', 'superscript', 'subscript',
        'orderedList', 'unorderedList', 'indent', 'outdent', 'justifyLeft',
        'justifyCenter', 'justifyRight', 'justifyFull'
      ]
    ]
  };
}
