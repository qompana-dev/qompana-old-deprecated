import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {MailTextEditorConfig} from './mail-text-editor.config';

@Component({
  selector: 'app-mail-text-editor',
  templateUrl: './mail-text-editor.component.html',
  styleUrls: ['./mail-text-editor.component.scss']
})
export class MailTextEditorComponent {
  html: string;
  editorConfig = MailTextEditorConfig.config;
  @Input() textAreaPlaceholder = '';
  @Output() valueChanged: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  set htmlContent(html: string) {
    this.html = html;
  }

  @ViewChild('mailTextEditor') editor: any;

  constructor() {
  }


  onTextChange(event: any): void {
    this.htmlContent = event;
    this.valueChanged.emit(event);
  }

  public reset(): void {
    this.htmlContent = '';
  }
}
