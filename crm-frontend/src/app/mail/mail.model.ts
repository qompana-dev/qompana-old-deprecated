import {CrmObjectType} from '../shared/model/search.model';

export class MailConfigurationDto {
  imapServerAddress: string;
  imapServerPort: number;
  imapSecurityType: 'TLS' | 'SSL';
  smtpServerAddress: string;
  smtpServerPort: number;
  smtpSecurityType: 'TLS' | 'SSL';
  username: string;
  password: string;

  id?: number;
  active?: boolean;
}

export class SimpleMailConfigurationDto {
  username: string;
  password: string;

  id?: number;
  active?: boolean;
}

export enum MailSortStrategy {
  unread = 'unread',
  subject = 'subject',
  receivedDate = 'receivedDate',
}

export enum MailStorageSortStrategy {
  receivedDate = 'receivedDate',
  subject = 'subject',
  from = 'from'
}


export class MailHeader {
  id: number;
  messageId: string;
  subject: string;
  from: string;
  fromAddr: HeaderEmailAddr[];
  to: string;
  toAddr: HeaderEmailAddr[];
  cc: any;
  ccAddr: HeaderEmailAddr[];
  bcc: any;
  bccAddr: HeaderEmailAddr[];
  receivedDate: string;
  folderName: string;
  seen: boolean;
}

export interface HeaderEmailAddr {
  name: string;
  email: string;
}

export interface MailFolders {
  mailAddress: string;
  folders: MailFolder[];
}

export interface MailFolder {
  folderName: string;
  unreadMessages: number;
}

export class MailContent {
  messageId: string;
  headers: Map<string, string>;
  htmlContent: string;
  plainContent: string;
  subject: string;
  headerDto: MailHeader;
  attachmentList: MailContentAttachment[];
}

export interface MailContentAttachment {
  name: string;
  size: number;
}

export class NewMail {
  to: string[];
  cc: string[];
  bcc?: string[];
  subject: string;
  content: string;
  attachments?: NewMailAttachment[];
  ignoreGdpr?: boolean;
}

export interface ForwardedMail {
  to: string[];
  cc?: string[];
  bcc?: string[];
  content: string;
  messageId: number;
  attachments?: NewMailAttachment[];
}

export interface NewMailAttachment {
  fileId: string;
  tempFile: boolean;
}

export class MailSearch {
  crmObjectType: CrmObjectType;
  name: string;
  email: string;
}

export class MailStorageDto {
  id: number;
  mailFileName: string;
  messageId: string;
  subject: string;
  from: string;
  fromAddr: HeaderEmailAddr[];
  to: string;
  toAddr: HeaderEmailAddr[];
  cc: any;
  ccAddr: HeaderEmailAddr[];
  bcc: any;
  bccAddr: HeaderEmailAddr[];
  receivedDate: string;
  folderName: string;
  accountId: number;
  username: string;
  htmlContent: string;
  plainContent: string;
  relatedMails: MailHeader[];
}
