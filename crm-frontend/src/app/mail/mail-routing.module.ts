import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../login/auth/auth-guard.service';
import {MailMainPageComponent} from './mail-main-page/mail-main-page.component';

const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: MailMainPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MailRoutingModule {
}
