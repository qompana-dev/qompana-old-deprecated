import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {SliderComponent} from '../../shared/slider/slider.component';
import {Supplier} from '../manufacturers-and-suppliers.model';
import {AbstractControl, FormControl, ValidationErrors, Validators} from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ManufacturersAndSuppliersService} from '../manufacturers-and-suppliers.service';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {FormValidators} from '../../shared/form/form-validators';

@Component({
  selector: 'app-suppliers-add',
  templateUrl: './suppliers-add.component.html',
  styleUrls: ['./suppliers-add.component.scss']
})
export class SuppliersAddComponent implements OnInit, OnDestroy {

  @Output() added: EventEmitter<void> = new EventEmitter<void>();
  @Input() slider: SliderComponent;

  suppliers: Supplier[] = [];

  nameControl: FormControl;

  private componentDestroyed: Subject<void> = new Subject();

  constructor(private manufacturersAndSuppliersService: ManufacturersAndSuppliersService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.nameControl = new FormControl('', [Validators.required, FormValidators.whitespace, Validators.maxLength(100),
      (control: AbstractControl): ValidationErrors =>
        (this.suppliers.length !== 0 && this.suppliers.map(e => this.lowerCase(e.name))
          .indexOf(this.lowerCase(control.value)) !== -1) ? {notUnique: true} : undefined]
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  closeSlider(): void {
    this.nameControl.reset();
    this.suppliers = [];
    this.slider.close();
  }

  submit(): void {
    if (this.suppliers.length !== 0) {
      this.manufacturersAndSuppliersService.saveSuppliers(this.suppliers).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        () => this.handleAddSuccess(),
        () => this.notificationService.notify('manufacturersAndSuppliers.suppliers.addingError', NotificationType.ERROR)
      );
    }
  }

  addTempElement(): void {
    if (this.nameControl.valid) {
      this.suppliers.push({name: this.nameControl.value});
      this.nameControl.patchValue('');
      this.nameControl.markAsUntouched();
    } else {
      this.nameControl.markAsTouched();
    }
  }

  deleteTempElement(value: string): void {
    this.suppliers = this.suppliers.filter(e => e.name !== value);
  }

  private handleAddSuccess(): void {
    this.added.emit();
    this.closeSlider();
    this.notificationService.notify('manufacturersAndSuppliers.suppliers.addedSuccessfully', NotificationType.SUCCESS);
  }

  private lowerCase(text: string): string {
    return (text) ? text.toLowerCase() : text;
  }
}
