import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Manufacturer} from '../manufacturers-and-suppliers.model';
import {AbstractControl, FormControl, ValidationErrors, Validators} from '@angular/forms';
import {SliderComponent} from '../../shared/slider/slider.component';
import {ManufacturersAndSuppliersService} from '../manufacturers-and-suppliers.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {FormValidators} from '../../shared/form/form-validators';

@Component({
  selector: 'app-manufacturers-add',
  templateUrl: './manufacturers-add.component.html',
  styleUrls: ['./manufacturers-add.component.scss']
})
export class ManufacturersAddComponent implements OnInit, OnDestroy {

  @Output() added: EventEmitter<void> = new EventEmitter<void>();
  @Input() slider: SliderComponent;
  manufacturers: Manufacturer[] = [];

  nameControl: FormControl;

  private componentDestroyed: Subject<void> = new Subject();

  constructor(private manufacturersAndSuppliersService: ManufacturersAndSuppliersService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.nameControl = new FormControl('', [FormValidators.whitespace, Validators.required, Validators.maxLength(100),
      (control: AbstractControl): ValidationErrors =>
        (this.manufacturers.length !== 0 && this.manufacturers.map(e => this.lowerCase(e.name))
          .indexOf(this.lowerCase(control.value)) !== -1) ? {notUnique: true} : undefined]
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  closeSlider(): void {
    this.nameControl.reset();
    this.manufacturers = [];
    this.slider.close();
  }

  submit(): void {
    if (this.manufacturers.length !== 0) {
      this.manufacturersAndSuppliersService.saveManufacturers(this.manufacturers).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        () => this.handleAddSuccess(),
        () => this.notificationService.notify('manufacturersAndSuppliers.manufacturers.addingError', NotificationType.ERROR)
      );
    }
  }

  addTempElement(): void {
    if (this.nameControl.valid) {
      this.manufacturers.push({name: this.nameControl.value});
      this.nameControl.patchValue('');
      this.nameControl.markAsUntouched();
    } else {
      this.nameControl.markAsTouched();
    }
  }

  deleteTempElement(value: string): void {
    this.manufacturers = this.manufacturers.filter(e => e.name !== value);
  }

  private handleAddSuccess(): void {
    this.added.emit();
    this.closeSlider();
    this.notificationService.notify('manufacturersAndSuppliers.manufacturers.addedSuccessfully', NotificationType.SUCCESS);
  }

  private lowerCase(text: string): string {
    return (text) ? text.toLowerCase() : text;
  }
}
