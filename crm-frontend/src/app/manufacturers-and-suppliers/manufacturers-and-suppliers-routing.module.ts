import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '../login/auth/auth-guard.service';
import {ManufacturersAndSuppliersComponent} from './manufacturers-and-suppliers/manufacturers-and-suppliers.component';

const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: ManufacturersAndSuppliersComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManufacturersAndSuppliersRoutingModule { }
