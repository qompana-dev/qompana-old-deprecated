export interface Manufacturer {
  id?: number;
  name?: string;
}

export interface Supplier {
  id?: number;
  name?: string;
}
