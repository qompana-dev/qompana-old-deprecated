import {Injectable} from '@angular/core';
import {Page, PageRequest} from '../shared/pagination/page.model';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Manufacturer, Supplier} from './manufacturers-and-suppliers.model';
import {UrlUtil} from '../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class ManufacturersAndSuppliersService {

  private url = `${UrlUtil.url}/crm-business-service`;

  constructor(private http: HttpClient) { }

  public getManufacturers(pageRequest: PageRequest): Observable<Page<Manufacturer>> {
    const params = new HttpParams({fromObject: pageRequest as any});
    return this.http.get<Page<Manufacturer>>(this.url + '/manufacturer', {params});
  }

  public getSuppliers(pageRequest: PageRequest): Observable<Page<Supplier>> {
    const params = new HttpParams({fromObject: pageRequest as any});
    return this.http.get<Page<Supplier>>(this.url + '/supplier', {params});
  }

  public removeManufacturer(id: number): Observable<void> {
    const url = `${this.url}/manufacturer/${id}`;
    return this.http.delete<void>(url);
  }

  public removeSupplier(id: number): Observable<void> {
    const url = `${this.url}/supplier/${id}`;
    return this.http.delete<void>(url);
  }

  public saveManufacturers(manufacturers: Manufacturer[]): Observable<void> {
    const url = `${this.url}/manufacturer`;
    return this.http.post<void>(url, manufacturers);
  }

  public saveSuppliers(suppliers: Supplier[]): Observable<void> {
    const url = `${this.url}/supplier`;
    return this.http.post<void>(url, suppliers);
  }
}
