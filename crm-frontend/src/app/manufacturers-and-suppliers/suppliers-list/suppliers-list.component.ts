import {Component, OnDestroy, OnInit} from '@angular/core';
import {InfinityScrollUtil, InfinityScrollUtilInterface} from '../../shared/infinity-scroll.util';
import {Observable, Subject} from 'rxjs';
import {ManufacturersAndSuppliersService} from '../manufacturers-and-suppliers.service';
import {ErrorTypes, NotificationService} from '../../shared/notification.service';
import {FilterStrategy} from '../../shared/model';
import {PageRequest} from '../../shared/pagination/page.model';
import {DeleteDialogData} from '../../shared/dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../shared/dialog/delete/delete-dialog.service';
import {takeUntil} from 'rxjs/operators';
import {BAD_REQUEST} from 'http-status-codes';

@Component({
  selector: 'app-suppliers-list',
  templateUrl: './suppliers-list.component.html',
  styleUrls: ['./suppliers-list.component.scss']
})
export class SuppliersListComponent implements OnInit, OnDestroy, InfinityScrollUtilInterface {

  infinityScroll: InfinityScrollUtil;
  private componentDestroyed: Subject<void> = new Subject();

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'manufacturersAndSuppliers.suppliers.deleteDialog.title',
    description: 'manufacturersAndSuppliers.suppliers.deleteDialog.description',
    noteFirst: 'manufacturersAndSuppliers.suppliers.deleteDialog.noteFirstPart',
    noteSecond: 'manufacturersAndSuppliers.suppliers.deleteDialog.noteSecondPart',
    confirmButton: 'manufacturersAndSuppliers.suppliers.deleteDialog.confirm',
    cancelButton: 'manufacturersAndSuppliers.suppliers.deleteDialog.cancel',
  };

  readonly deleteErrorTypes: ErrorTypes = {
    base: 'manufacturersAndSuppliers.suppliers.notifications.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'usedInProducts'
      }
    ]
  };

  constructor(private manufacturersAndSuppliersService: ManufacturersAndSuppliersService,
              private notificationService: NotificationService,
              private deleteDialogService: DeleteDialogService) {
    this.infinityScroll = new InfinityScrollUtil(this, this.componentDestroyed, this.notificationService,
      'manufacturersAndSuppliers.list.getError');
  }

  ngOnInit() {
    this.infinityScroll.initData();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  getItemObservable(selection: FilterStrategy, pageRequest: PageRequest): Observable<any> {
    return this.manufacturersAndSuppliersService.getSuppliers(pageRequest);
  }

  tryDelete(id: number): void {
    this.deleteDialogData.onConfirmClick = () => this.delete(id);
    this.openDeleteDialog();
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private delete(id: number): void {
    this.manufacturersAndSuppliersService.removeSupplier(id).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.infinityScroll.initData(),
      (err) => this.notificationService.notifyError(this.deleteErrorTypes, err.status)
    );
  }
}
