import {Component, OnDestroy, OnInit} from '@angular/core';
import {ManufacturersAndSuppliersService} from '../manufacturers-and-suppliers.service';
import {PageRequest} from '../../shared/pagination/page.model';
import {Observable, Subject} from 'rxjs';
import {InfinityScrollUtil, InfinityScrollUtilInterface} from '../../shared/infinity-scroll.util';
import {ErrorTypes, NotificationService} from '../../shared/notification.service';
import {FilterStrategy} from '../../shared/model';
import {DeleteDialogData} from '../../shared/dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../shared/dialog/delete/delete-dialog.service';
import {takeUntil} from 'rxjs/operators';
import {BAD_REQUEST} from 'http-status-codes';

@Component({
  selector: 'app-manufacturers-list',
  templateUrl: './manufacturers-list.component.html',
  styleUrls: ['./manufacturers-list.component.scss']
})
export class ManufacturersListComponent implements OnInit, OnDestroy, InfinityScrollUtilInterface {

  infinityScroll: InfinityScrollUtil;
  private componentDestroyed: Subject<void> = new Subject();

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'manufacturersAndSuppliers.manufacturers.deleteDialog.title',
    description: 'manufacturersAndSuppliers.manufacturers.deleteDialog.description',
    noteFirst: 'manufacturersAndSuppliers.manufacturers.deleteDialog.noteFirstPart',
    noteSecond: 'manufacturersAndSuppliers.manufacturers.deleteDialog.noteSecondPart',
    confirmButton: 'manufacturersAndSuppliers.manufacturers.deleteDialog.confirm',
    cancelButton: 'manufacturersAndSuppliers.manufacturers.deleteDialog.cancel',
  };

  readonly deleteErrorTypes: ErrorTypes = {
    base: 'manufacturersAndSuppliers.manufacturers.notifications.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'usedInProducts'
      }
    ]
  };

  constructor(private manufacturersAndSuppliersService: ManufacturersAndSuppliersService,
              private notificationService: NotificationService,
              private deleteDialogService: DeleteDialogService) {
    this.infinityScroll = new InfinityScrollUtil(
      this, this.componentDestroyed, this.notificationService,
      'manufacturersAndSuppliers.list.getError'
    );
  }

  ngOnInit() {
    this.infinityScroll.initData();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  getItemObservable(selection: FilterStrategy, pageRequest: PageRequest): Observable<any> {
    return this.manufacturersAndSuppliersService.getManufacturers(pageRequest);
  }

  tryDelete(id: number): void {
    this.deleteDialogData.onConfirmClick = () => this.delete(id);
    this.openDeleteDialog();
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private delete(id: number): void {
    this.manufacturersAndSuppliersService.removeManufacturer(id).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.infinityScroll.initData(),
      (err) => this.notificationService.notifyError(this.deleteErrorTypes, err.status)
    );
  }
}
