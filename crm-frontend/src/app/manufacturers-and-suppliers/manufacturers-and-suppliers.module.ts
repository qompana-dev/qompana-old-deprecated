import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManufacturersAndSuppliersRoutingModule } from './manufacturers-and-suppliers-routing.module';
import { ManufacturersAndSuppliersComponent } from './manufacturers-and-suppliers/manufacturers-and-suppliers.component';
import {SharedModule} from '../shared/shared.module';
import { ManufacturersListComponent } from './manufacturers-list/manufacturers-list.component';
import { SuppliersListComponent } from './suppliers-list/suppliers-list.component';
import { ManufacturersAddComponent } from './manufacturers-add/manufacturers-add.component';
import { SuppliersAddComponent } from './suppliers-add/suppliers-add.component';

@NgModule({
  declarations: [ManufacturersAndSuppliersComponent, ManufacturersListComponent, SuppliersListComponent, ManufacturersAddComponent, SuppliersAddComponent],
  imports: [
    CommonModule,
    ManufacturersAndSuppliersRoutingModule,
    SharedModule
  ]
})
export class ManufacturersAndSuppliersModule { }
