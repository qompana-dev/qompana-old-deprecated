import {LoginService} from './login/login.service';
import {TranslateService} from '@ngx-translate/core';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {AuthService} from './login/auth/auth.service';
import * as moment from 'moment';
import 'moment/locale/pl';
import {LoginStatusEnum} from './login/login.model';
import {NotificationService, NotificationType} from './shared/notification.service';

moment.locale(localStorage.getItem('locale'));

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  readonly routesWithoutAuth: {checkUrl: (url: string) => boolean}[] = [
    {checkUrl: url => (url === '/login')},
    {checkUrl: url => (url === '/password-reset')},
    {checkUrl: url => (AppComponent.startsWith(url, '/gdpr-token/'))},
    {checkUrl: url => (AppComponent.startsWith(url, '/token/'))},
    {checkUrl: url => (AppComponent.startsWith(url, '/person/set-password/'))},
  ];

  loggedUserSubscription: Subscription;

  loggedIn: boolean;
  routeWithAuth: boolean;

  sidenavHidden = true;

  constructor(private translateService: TranslateService,
              private loginService: LoginService,
              private activatedRoute: ActivatedRoute,
              private authService: AuthService,
              private $router: Router,
              private notificationService: NotificationService) {

  }

  private static startsWith(url: string, substring: string): boolean {
    return url.substring(0, substring.length) === substring;
  }

  ngOnInit(): void {
    const locale = localStorage.getItem('locale');
    this.translateService.setDefaultLang(locale ? locale : 'pl');
    this.loggedIn = this.loginService.isLoggedIn();
    this.routeWithAuth = this.isRouteWithAuth();
    this.authService.refreshPermission();

    this.loggedUserSubscription = this.loginService.loggedUserStatusChangeSubject.subscribe(
      (statusChanged: LoginStatusEnum) => {
        this.loggedIn = this.loginService.isLoggedIn();
        if ((!this.loggedIn && statusChanged !== LoginStatusEnum.LOGOUT_BEFORE_TOKEN)
          || statusChanged === LoginStatusEnum.LOGOUT_SUCCESSFUL) {
          this.notificationService.notify('login.notification.logoutSuccessful', NotificationType.SUCCESS);
          this.$router.navigate(['/login']);
        }
      });

    this.$router.events.subscribe(() => {
      this.routeWithAuth = this.isRouteWithAuth();
    });

    setInterval(() => this.automaticLogin(), 5000);
  }

  ngOnDestroy(): void {
    if (this.loggedUserSubscription) {
      this.loggedUserSubscription.unsubscribe();
    }
  }

  private automaticLogin(): void {
    this.loggedIn = this.loginService.isLoggedIn();
    this.routeWithAuth = this.isRouteWithAuth();
    if (this.routeWithAuth !== this.loggedIn) {
      if (this.loggedIn) {
        this.$router.navigate(['/home']);
      } else {
        this.$router.navigate(['/login']);
      }
    }
  }

  private isRouteWithAuth(): boolean {
    return this.routesWithoutAuth
      .filter((item) => item.checkUrl(this.$router.url))
      .length === 0;
  }
}
