"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppModule = exports.HttpLoaderFactory = void 0;
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var http_loader_1 = require("@ngx-translate/http-loader");
var http_1 = require("@angular/common/http");
var navigation_component_1 = require("./navigation/navigation.component");
var auth_interceptor_1 = require("./login/auth/auth.interceptor");
var auth_guard_service_1 = require("./login/auth/auth-guard.service");
var core_2 = require("@ngx-translate/core");
var animations_1 = require("@angular/platform-browser/animations");
var sidenav_component_1 = require("./navigation/sidenav/sidenav.component");
var ngx_mask_1 = require("ngx-mask");
var ng2_stompjs_1 = require("@stomp/ng2-stompjs");
var stomp_config_1 = require("./shared/config/stomp-config");
var user_notification_component_1 = require("./navigation/user-notification/user-notification.component");
var shared_module_1 = require("./shared/shared.module");
var login_component_1 = require("./login/login.component");
var main_page_component_1 = require("./main-page/main-page.component");
var angular_calendar_1 = require("angular-calendar");
// @ts-ignore
var moment_1 = require("moment");
var main_search_component_1 = require("./navigation/main-search/main-search.component");
var files_module_1 = require("./files/files.module");
var fr_1 = require("@angular/common/locales/fr");
var common_1 = require("@angular/common");
var expense_slider_module_1 = require("./shared/tabs/expense-tab/expense-slider/expense-slider.module");
var mail_select_file_dialog_component_1 = require("./mail/mail-main-page/new-mail/mail-select-file-dialog/mail-select-file-dialog.component");
var mail_module_1 = require("./mail/mail.module");
var ng2_dragula_1 = require("ng2-dragula");
var avatar_module_1 = require("./shared/avatar-form/avatar.module");
common_1.registerLocaleData(fr_1["default"], 'fr');
function HttpLoaderFactory(http) {
    return new http_loader_1.TranslateHttpLoader(http);
}
exports.HttpLoaderFactory = HttpLoaderFactory;
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                login_component_1.LoginComponent,
                main_page_component_1.MainPageComponent,
                sidenav_component_1.SidenavComponent,
                navigation_component_1.NavigationComponent,
                user_notification_component_1.UserNotificationComponent,
                main_search_component_1.MainSearchComponent,
            ],
            imports: [
                platform_browser_1.BrowserModule,
                app_routing_module_1.AppRoutingModule,
                shared_module_1.SharedModule,
                files_module_1.FilesModule,
                mail_module_1.MailModule,
                avatar_module_1.AvatarModule,
                http_1.HttpClientModule,
                animations_1.BrowserAnimationsModule,
                core_2.TranslateModule.forRoot({
                    loader: {
                        provide: core_2.TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [http_1.HttpClient]
                    }
                }),
                ngx_mask_1.NgxMaskModule.forRoot(),
                expense_slider_module_1.ExpenseSliderModule,
                ng2_dragula_1.DragulaModule.forRoot()
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: http_1.HTTP_INTERCEPTORS,
                    useClass: auth_interceptor_1.AuthInterceptor,
                    multi: true
                },
                ng2_stompjs_1.StompService,
                {
                    provide: ng2_stompjs_1.StompConfig,
                    useValue: stomp_config_1.StompConfiguration.stompConfig
                },
                auth_guard_service_1.AuthGuard,
                {
                    provide: angular_calendar_1.MOMENT,
                    useValue: moment_1["default"]
                }
            ],
            bootstrap: [app_component_1.AppComponent],
            entryComponents: [mail_select_file_dialog_component_1.MailSelectFileDialogComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
