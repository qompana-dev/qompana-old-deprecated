import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'strReplace'
})
export class StrReplacePipe implements PipeTransform {

  transform(value: any, regex: string, regexFlag: string = 'i'): string {
    return value.replace(new RegExp(regex, regexFlag) , '');
  }

}
