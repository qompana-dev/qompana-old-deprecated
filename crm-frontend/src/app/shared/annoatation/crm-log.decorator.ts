import {environment} from '../../../environments/environment';

// set @CrmLog() above method
export function CrmLog(): MethodDecorator {
  return ((target, key: string, descriptor: any) => {
    if (!environment.production) {
      const originalMethod = descriptor.value;
      descriptor.value =  function(...args: any[]) {
        console.log(`%c ${target.constructor.name} -> ${key}, args = `, `color: #357a38; font-weight: bold`,  args);
        const result = originalMethod.apply(this, args);
        console.log(`%c \tFinish ${target.constructor.name} -> ${key}, this = `, `color: #163418; font-weight: bold`, this);
        return result;
      };
    }
    return descriptor;
  });
}
