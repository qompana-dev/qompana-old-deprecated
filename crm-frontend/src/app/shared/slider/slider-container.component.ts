import { Overlay, OverlayRef } from "@angular/cdk/overlay";
import { TemplatePortal } from "@angular/cdk/portal";
import {
  Component,
  ContentChild,
  ElementRef,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from "@angular/core";

@Component({
  selector: "app-slider-container",
  template: "",
})
export class SliderComponentContainer implements OnInit, OnDestroy {
  @ContentChild("slider") slider: TemplateRef<any>;
  private overlayRef: OverlayRef;

  constructor(
    private overlay: Overlay,
    private viewContainerRef: ViewContainerRef
  ) {}

  ngOnInit(): void {
    this.overlayRef = this.overlay.create();
    this.overlayRef.attach(
      new TemplatePortal(this.slider, this.viewContainerRef)
    );
  }

  ngOnDestroy(): void {
    if (this.overlayRef) {
      this.overlayRef.dispose();
    }
  }
}
