import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  RendererStyleFlags2,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import {
  SliderGroupInfo,
  SliderMoreInfo,
  SliderService,
  SliderWidth,
} from './slider.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {CrmObjectType} from '../model/search.model';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
})
export class SliderComponent
  implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  opened = false;
  viewContent = false;
  private componentDestroyed: Subject<void> = new Subject();
  @Input() width = '40vw';
  @Input() title = '';
  @Input() key = '';
  @Input() headerTemplate: TemplateRef<ElementRef>;
  @Input() moreInfoVisible = false;
  @Input() closeByCross = true;

  @Input() group: string;

  @Output() openEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() closeEvent: EventEmitter<void> = new EventEmitter<void>();

  @ViewChild('el') el;

  showMoreInfo = false;

  sliderGroupInfoList: SliderGroupInfo[] = [];
  currentWidth = '40vw';

  constructor(
    private renderer: Renderer2,
    private sliderService: SliderService
  ) {}

  ngOnInit(): void {
    this.sliderService.closeSliderSubject
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.close());
    this.sliderService.changeSliderWidthSubject
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((newWidth: SliderWidth) => this.changeWidth(newWidth));
    this.sliderService.changeMoreInfoVisibleSubject
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((moreInfo: SliderMoreInfo) =>
        this.changeMoreInfoVisible(moreInfo)
      );
    this.sliderService.sliderInGroupChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((sliderGroupInfo: SliderGroupInfo) =>
        this.sliderInGroupChange(sliderGroupInfo)
      );
  }

  ngAfterViewInit(): void {
    this.renderer.addClass(this.el.nativeElement, 'slider-background-default');
    // Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    // Add 'implements AfterViewInit' to the class.
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.showMoreInfo = this.moreInfoVisible;
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  onBackgroundClick() {
    if (this.opened) {
      this.close();
    }
  }

  public toggle(): void {
    if (this.opened) {
      this.close();
    } else {
      this.open();
    }
  }

  public open(): void {
    this.viewContent = true;

    this.renderer.removeClass(this.el.nativeElement.children[1], 'hide-slider');
    setTimeout(() => {
      this.setWidthAndOpen(this.width);
      this.notifySliderInGroupChange(true);
      this.openEvent.next();
    }, 10);
  }

  closeByCrossClick(): void {
    this.sliderService.closeClicked.next(this.key);
    if (this.closeByCross) {
      this.close();
    }
  }

  public close(): void {
    this.renderer.removeClass(
      this.el.nativeElement.children[1],
      'slider-animation'
    );
    this.renderer.setStyle(
      this.el.nativeElement.children[1],
      'margin-left',
      '100vw'
    );
    setTimeout(() => {
      this.opened = false;
      this.viewContent = false;
      this.showBackground();
      this.renderer.addClass(this.el.nativeElement.children[1], 'hide-slider');
    }, 500);
    this.sliderService.stateChanged(this.key, false);
    this.showMoreInfo = this.moreInfoVisible;
    this.notifySliderInGroupChange(false);
    this.closeEvent.next();
  }

  openMoreInfo(): void {
    this.sliderService.moreInfoClicked.next(this.key);
  }

  showBackground(): void {
    const open =
      this.opened &&
      (!this.group ||
        (this.sliderGroupInfoList.length > 0 &&
          this.sliderGroupInfoList[this.sliderGroupInfoList.length - 1].key ===
            this.key));

    this.addBackgroundClass(open);
  }

  addBackgroundClass(add: boolean): void {
    if (add) {
      this.renderer.addClass(this.el.nativeElement.children[0], 'show');
    } else {
      this.renderer.removeClass(this.el.nativeElement.children[0], 'show');
    }
  }

  private notifySliderInGroupChange(open: boolean): void {
    if (this.group) {
      this.sliderService.sliderInGroupChanged.next({
        key: this.key,
        group: this.group,
        open,
        width: this.width,
      });
      if (!open) {
        this.sliderGroupInfoList = [];
      }
    }
  }

  private sliderInGroupChange(sliderGroupInfo: SliderGroupInfo): void {
    if (sliderGroupInfo.group === this.group) {
      if (sliderGroupInfo.open) {
        this.sliderGroupInfoList.push(sliderGroupInfo);
      } else {
        const index = this.sliderGroupInfoList
          .map((item) => item.key)
          .indexOf(sliderGroupInfo.key);
        if (index !== -1) {
          this.sliderGroupInfoList.splice(index, 1);
        }
      }
    }
    this.updateMarginLeftInGroup();
    this.showBackground();
  }

  private updateMarginLeftInGroup(): void {
    if (this.opened) {
      let newMargin = `100vw`;
      let addElement = false;
      this.sliderGroupInfoList.forEach((value: SliderGroupInfo) => {
        if (value.key === this.key) {
          addElement = true;
        }
        if (addElement) {
          newMargin = `calc(${newMargin} - ${value.width})`;
        }
      });
      this.renderer.setStyle(
        this.el.nativeElement.children[1],
        'margin-left',
        newMargin
      );
    } else {
      this.renderer.setStyle(
        this.el.nativeElement.children[1],
        'margin-left',
        '100vw'
      );
    }
  }

  private changeMoreInfoVisible(moreInfo: SliderMoreInfo): void {
    if (moreInfo.key === this.key) {
      this.showMoreInfo = moreInfo.visible;
    }
  }

  private changeWidth(newWidth: SliderWidth): void {
    if (newWidth.key === this.key) {
      this.setWidthAndOpen(newWidth.width);
    }
  }

  private setWidthAndOpen(sliderWidth: string): void {
    this.currentWidth = sliderWidth;
    this.renderer.addClass(
      this.el.nativeElement.children[1],
      'slider-animation'
    );
    this.renderer.setStyle(
      this.el.nativeElement.children[1],
      'margin-left',
      `calc(100vw - ${sliderWidth})`
    );
    this.renderer.setStyle(
      this.el.nativeElement.children[1],
      'min-width',
      `${sliderWidth}`,
      RendererStyleFlags2.DashCase || RendererStyleFlags2.Important
    );
    this.renderer.setStyle(
      this.el.nativeElement.children[1],
      'max-width',
      `${sliderWidth}`,
      RendererStyleFlags2.DashCase || RendererStyleFlags2.Important
    );
    setTimeout(() => (this.opened = true), 500);
    this.opened = true;
    this.sliderService.stateChanged(this.key, true);

    this.sliderService.sliderWidthChanged.next({
      key: this.key,
      width: sliderWidth,
    });
    this.showBackground();

    // setTimeout(() => {
    //   this.sliderService.sliderWidthChanged.next({key: this.key, width: sliderWidth});
    // }, 500); // animation time
  }

  @HostListener('document:keyup.escape', ['$event'])
  public keyEvent(event: KeyboardEvent): void {
    this.close();
  }
}
