import { FilterStrategy } from './model';
import { HttpParams } from '@angular/common/http';

export class TableUtil {
  static enrichParamsWithFilterStrategy( selection: FilterStrategy, params: HttpParams, value?: string): HttpParams {
    switch (selection) {
      case FilterStrategy.CREATED_TODAY:
        return params.set('today', 'true');
      case FilterStrategy.EDITED_LASTLY:
        return params.append('edited', 'true');
      case FilterStrategy.VIEWED_LASTLY:
        return params.append('viewed', 'true');
      case FilterStrategy.FOR_APPROVING:
        return params.append('for-approving', 'true');
      case FilterStrategy.CLOSED_CLOSED:
        return params.append('closeResult', 'CLOSED');
      case FilterStrategy.CLOSED_ARCHIVED:
        return params.append('closeResult', 'ARCHIVED');
      case FilterStrategy.FINISHED_SUCCESS:
        return params.append('finishResult', 'SUCCESS');
      case FilterStrategy.FINISHED_ERROR:
        return params.append('finishResult', 'ERROR');
      case FilterStrategy.CONVERTED:
        return params.append('converted', 'true');
      case FilterStrategy.OPENED:
        return params.append('opened', 'true');
      case FilterStrategy.ARCHIVED:
        return params.append('archived', 'true');
      case FilterStrategy.CUSTOMER:
        return params.append('customer', value);
      case FilterStrategy.CONTACT:
        return params.append('contact', value);
    }
    return params;
  }
}
