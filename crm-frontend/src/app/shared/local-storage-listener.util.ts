import {Subject} from 'rxjs';

export class LocalStorageListenerUtils {
  static createListener(windowObj: Window, key: string, subjectToShare: Subject<any>): void {
    subjectToShare.subscribe((obj: any) => localStorage.setItem(key, JSON.stringify(obj)));
    windowObj.addEventListener('storage', ($event: StorageEvent) => {
      if ($event.key === key) {
        subjectToShare.next(JSON.parse($event.newValue));
      }
    });
  }
}
