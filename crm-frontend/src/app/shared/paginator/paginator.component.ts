import { TranslateService } from '@ngx-translate/core';
import { PageEvent } from './paginator.model';
import {
  coerceNumberProperty,
} from '@angular/cdk/coercion';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';


const DEFAULT_PAGE_SIZE = 50;

@Component({
  selector: 'app-paginator',
  templateUrl: 'paginator.component.html',
  styleUrls: ['paginator.component.scss'],
})
export class PaginatorComponent implements OnInit {

  @Input()
  get pageIndex(): number { return this._pageIndex; }
  set pageIndex(value: number) {
    this._pageIndex = Math.max(coerceNumberProperty(value), 0);
  }
  private _pageIndex = 0;

  @Input()
  get length(): number { return this._length; }
  set length(value: number) {
    this._length = coerceNumberProperty(value);
  }
  private _length = 0;

  @Input()
  get pageSize(): number { return this._pageSize; }
  set pageSize(value: number) {
    this._pageSize = Math.max(coerceNumberProperty(value), 0);
    this._updateDisplayedPageSizeOptions();
  }
  private _pageSize: number = 20;

  @Input()
  get pageSizeOptions(): number[] { return this._pageSizeOptions; }
  set pageSizeOptions(value: number[]) {
    this._pageSizeOptions = (value || []).map(p => coerceNumberProperty(p));
    this._updateDisplayedPageSizeOptions();
  }
  private _pageSizeOptions: number[] = [20, 50, 100];

  @Output() readonly page: EventEmitter<PageEvent> = new EventEmitter<PageEvent>();

  _displayedPageSizeOptions: number[];
  newPage;

  constructor(
    private translateService: TranslateService
  ) {
  }

  ngOnInit() {
    this._updateDisplayedPageSizeOptions();
  }


  nextPage(): void {
    if (!this.hasNextPage()) { return; }
    this.pageIndex++;
    this._emitPageEvent();
  }

  previousPage(): void {
    if (!this.hasPreviousPage()) { return; }

    this.pageIndex--;
    this._emitPageEvent();
  }

  hasPreviousPage(): boolean {
    return this.pageIndex >= 1 && this.pageSize != 0;
  }

  hasNextPage(): boolean {
    const maxPageIndex = this.getNumberOfPages() - 1;
    return this.pageIndex < maxPageIndex && this.pageSize != 0;
  }

  getNumberOfPages(): number {
    if (!this.pageSize) {
      return 0;
    }

    return Math.ceil(this.length / this.pageSize);
  }

  changePageSize(pageSize: number) {
    const startIndex = this.pageIndex * this.pageSize;

    this.pageIndex = Math.floor(startIndex / pageSize) || 0;
    this.pageSize = pageSize;
    this._emitPageEvent();
  }

  private _updateDisplayedPageSizeOptions() {
    if (!this.pageSize) {
      this._pageSize = this.pageSizeOptions.length != 0 ?
        this.pageSizeOptions[0] :
        DEFAULT_PAGE_SIZE;
    }

    this._displayedPageSizeOptions = this.pageSizeOptions.slice();

    if (this._displayedPageSizeOptions.indexOf(this.pageSize) === -1) {
      this._displayedPageSizeOptions.push(this.pageSize);
    }

    this._displayedPageSizeOptions.sort((a, b) => a - b);
  }

  private _emitPageEvent() {
    this.page.emit({
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      length: this.length
    });
  }

  getRangeLabel() {
    const currentIndex = this.pageIndex + 1;
    const endIndex = Math.ceil(this.length / this.pageSize) || 1;
    const of = this.translateService.instant('paginator.of');
    return `${currentIndex} ${of} ${endIndex}`;
  }

  goPage() {
    if (this.newPage === null || isNaN(this.newPage) || this.newPage - 1 === this.pageIndex) {
      return;
    }
    const maxPage = Math.ceil(this.length / this.pageSize);
    let newPage = 0;
    if (this.newPage >= maxPage) {
      newPage = maxPage - 1;
    } else if (this.newPage > 0) {
      newPage = this.newPage - 1;
    }
    this.pageIndex = newPage;
    this.newPage = newPage + 1;
    this._emitPageEvent();
  }
}
