// @ts-ignore
import moment, {Moment} from 'moment';
import {TimePickerHour} from './model';

export class TimesUtil {
  static fromUTCToCurrentTimeZone(utcTime: string): Moment {
    if (utcTime == null) {
      return undefined;
    }
    return moment.utc(utcTime).tz(moment.tz.guess());
  }

  static getHoursList(): TimePickerHour[] {
    const HOUR_STEP = 1;
    const MINUTE_STEP = 15;
    const timePickerHour: TimePickerHour[] = [];
    for (let hour = 0, index = 1; hour < 24; hour += HOUR_STEP) {
      for (let minute = 0; minute < 60; minute += MINUTE_STEP, index++) {
        const value = ((hour < 10) ? '0' + hour : hour) + ':' + ((minute < 10) ? '0' + minute : minute);
        timePickerHour.push({id: index, hour, minute, value});
      }
    }
    return timePickerHour;
  }

  static getFullHoursList(): TimePickerHour[] {
    const HOUR_STEP = 1;
    const timePickerHour: TimePickerHour[] = [];
    for (let hour = 0, index = 1; hour < 24; hour += HOUR_STEP) {
      const value = ((hour < 10) ? '0' + hour : hour) + ':' + '00';
      timePickerHour.push({id: index, hour, minute: 0, value});
    }
    return timePickerHour;
  }

  static getTaskHourIndex(date: Date): number {
    const hour = date.getHours();
    const minute = date.getMinutes();

    let foundedIndex = 61;
    let foundedDiff = 61;

    const hoursList = this.getHoursList();
    hoursList.filter(taskHour => taskHour.hour === hour)
      .forEach(taskHour => {
        const diff = Math.abs(taskHour.minute - minute);
        if (diff < foundedDiff) {
          foundedDiff = diff;
          foundedIndex = taskHour.id;
        }
      });
    return (foundedIndex === hoursList.length) ? 1 : foundedIndex + 1;
  }

  static getTaskNotificationHourIndex(date: Date): number {
    const hour = date.getHours();
    const minute = date.getMinutes();

    let foundedIndex = 61;
    let foundedDiff = 61;

    const hoursList = this.getHoursList();
    hoursList.filter(taskHour => taskHour.hour === hour)
      .forEach(taskHour => {
        const diff = Math.abs(taskHour.minute - minute);
        if (diff < foundedDiff) {
          foundedDiff = diff;
          foundedIndex = taskHour.id;
        }
      });
    return (foundedIndex === hoursList.length) ? 1 : foundedIndex - 1;
  }
}
