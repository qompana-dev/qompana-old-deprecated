import { AvatarIconComponent } from './avatar-icon/avatar-icon.component';
import { OverlayTooltipDirective } from './overlay-tooltip/overlay-tooltip.directive';
import { SliderComponentContainer } from './slider/slider-container.component';
import { CustomChartsModule } from './custom-charts/custom-charts.module';
import { NoteSearchPipe } from './pipe/note-search.pipe';
import { FormatFileSizePipe } from './pipe/formatFileSize.pipe';
import { LimitToLengthPipe } from './pipe/limitToLength.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTooltipModule } from '@angular/material';
import { AuthPipe } from '../login/auth/auth.pipe';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SliderComponent } from './slider/slider.component';
import { MaterialDesignModule } from './material-design.module';
import { PhonePipe } from './pipe/phone.pipe';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { SafePipe } from './pipe/safe.pipe';
import { SimpleDialogComponent } from './dialog/simple-dialog.component';
import { DeleteDialogComponent } from './dialog/delete/delete-dialog.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { LoadSvgOnInitComponent } from './load-svg-on-init.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FormatDatePipe } from './pipe/formatDate.pipe';
import { AutofocusDirective } from './autofocus.directive';
import { SortPropertyPipe } from './pipe/sort-property.pipe';
import { FileFormatToIconPipe } from './pipe/fileFormatToIcon.pipe';
import { LengthMoreThanPipe } from './pipe/length-more-than.pipe';
import { FileGroupToIconPipe } from './pipe/fileGroupToIcon.pipe';
import { ConsoleLogPipe } from './pipe/console-log.pipe';
import { AutocompleteValueFilterPipe } from './pipe/autocomplete-value-filter.pipe';
import { AssignToGroupComponent } from './assign-to-group/assign-to-group.component';
import { WarningDialogComponent } from './dialog/warning/warning-dialog.component';
import { FocusNextElementDirective } from './focus-next-element.directive';
import { IsNilPipe } from './pipe/is-nil.pipe';
import { FormatNumberPipe } from './pipe/format-number.pipe';
import { FormatCurrencyPipe } from './pipe/format-currency.pipe';
import { SelectItemDialogComponent } from './dialog/select-item-dialog/select-item-dialog.component';
import { EnumToListPipe } from './pipe/enum-to-list.pipe';
import { GetCurrencySymbolPipe } from './pipe/get-currency-symbol.pipe';
import { SafeHtmlPipe } from './pipe/safe-html.pipe';
import { ConfirmWithCommentComponent } from './dialog/confirm-with-comment/confirm-with-comment.component';
import { FormatTimePipe } from './pipe/format-time.pipe';
import { TooltipDirective } from './tooltip/tooltip.directive';
import { CustomFormatDatePipe } from './pipe/custom-format-date.pipe';
import { ThemeComponent } from './theme/theme.component';
import { IconComponent } from './icon/icon.component';
import { NoteFilterPipe } from './pipe/note-filter.pipe';
import { MathAbsPipe } from './pipe/abs.pipe';
import { OverlayModule } from '@angular/cdk/overlay';
import { PhoneFieldsComponent } from './phone-fields/phone-fields.component';
import { PhoneTypeMenuComponent } from './phone-fields/phone-type-menu/phone-type-menu.component';
import { FormatAddressPipe } from './pipe/format-address.pipe';
import {NgxMaskModule} from 'ngx-mask';
import {StrReplacePipe} from '../str-replace.pipe';

@NgModule({
  declarations: [
    AuthPipe,
    PhonePipe,
    SortPropertyPipe,
    FormatDatePipe,
    FormatFileSizePipe,
    FormatAddressPipe,
    LimitToLengthPipe,
    NoteFilterPipe,
    FormatTimePipe,
    FileFormatToIconPipe,
    FileGroupToIconPipe,
    SafePipe,
    EnumToListPipe,
    AutocompleteValueFilterPipe,
    LengthMoreThanPipe,
    SliderComponent,
    SliderComponentContainer,
    LoadSvgOnInitComponent,
    SimpleDialogComponent,
    DeleteDialogComponent,
    SelectItemDialogComponent,
    PhoneFieldsComponent,
    PhoneTypeMenuComponent,
    WarningDialogComponent,
    AutofocusDirective,
    AssignToGroupComponent,
    IsNilPipe,
    CustomFormatDatePipe,
    FormatNumberPipe,
    ConsoleLogPipe,
    FocusNextElementDirective,
    FormatCurrencyPipe,
    GetCurrencySymbolPipe,
    SafeHtmlPipe,
    NoteSearchPipe,
    ConfirmWithCommentComponent,
    PaginatorComponent,
    TooltipDirective,
    ThemeComponent,
    IconComponent,
    ThemeComponent,
    MathAbsPipe,
    OverlayTooltipDirective,
    AvatarIconComponent,
    StrReplacePipe
  ],
    imports: [
        CommonModule,
        AngularSvgIconModule,
        TranslateModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialDesignModule,
        NgxExtendedPdfViewerModule,
        MatTooltipModule,
        CustomChartsModule,
        OverlayModule,
        NgxMaskModule
    ],
  exports: [
    AuthPipe,
    PhonePipe,
    FormatDatePipe,
    FormatAddressPipe,
    FormatFileSizePipe,
    LimitToLengthPipe,
    FormatTimePipe,
    SafePipe,
    CustomFormatDatePipe,
    AutocompleteValueFilterPipe,
    FileFormatToIconPipe,
    FileGroupToIconPipe,
    LengthMoreThanPipe,
    NoteFilterPipe,
    AngularSvgIconModule,
    LoadSvgOnInitComponent,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    SliderComponent,
    SliderComponentContainer,
    PaginatorComponent,
    MaterialDesignModule,
    SimpleDialogComponent,
    NgxExtendedPdfViewerModule,
    DeleteDialogComponent,
    SelectItemDialogComponent,
    PhoneFieldsComponent,
    PhoneTypeMenuComponent,
    WarningDialogComponent,
    SortPropertyPipe,
    AutofocusDirective,
    AssignToGroupComponent,
    IsNilPipe,
    FormatNumberPipe,
    ConsoleLogPipe,
    FocusNextElementDirective,
    FormatCurrencyPipe,
    EnumToListPipe,
    GetCurrencySymbolPipe,
    SafeHtmlPipe,
    NoteSearchPipe,
    TooltipDirective,
    IconComponent,
    ThemeComponent,
    CustomChartsModule,
    MathAbsPipe,
    OverlayTooltipDirective,
    AvatarIconComponent ,
    StrReplacePipe
  ],
  entryComponents: [
    SelectItemDialogComponent,
    SimpleDialogComponent,
    DeleteDialogComponent,
    WarningDialogComponent,
    ConfirmWithCommentComponent,
    SliderComponent,
  ],
  providers: [
    FormatNumberPipe,
    FormatCurrencyPipe,
    FormatDatePipe,
    FormatAddressPipe,
    FormatFileSizePipe,
    LimitToLengthPipe,
    FormatTimePipe,
    GetCurrencySymbolPipe
  ],
})
export class SharedModule {}
