import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {TextEditorConfig} from './text-editor.config';
import cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.scss']
})
export class TextEditorComponent {
  html: string;
  editorConfig = TextEditorConfig.config;

  @Input() textAreaPlaceholder = '';
  @Output() valueChanged: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  set htmlContent(html: string) {
    this.html = html;
  }

  @ViewChild('textEditor') editor: any;

  @Input()
  set insert(text: string) {
    this.insertInLastPosition(text);
  }

  constructor() {
  }

  onTextChange(event: any): void {
    this.htmlContent = event;
    this.valueChanged.emit(event);
  }

  insertInLastPosition(text: string) {
    if (text) {
      const cursorPositionedField = this.editor;
      if (this.html) {
        if (cursorPositionedField._commandExecutor.savedSelection) {
          try {
            cursorPositionedField._commandExecutor.insertHtml(text);
          } catch(err) {
            this.html += text;
          }
        } else {
          this.html += text;
        }
      } else {
        this.html = text;
      }
      this.onTextChange(this.html);
    }
  }

  public reset(): void {
    this.htmlContent = '';
  }
}
