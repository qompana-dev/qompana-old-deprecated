import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {TextEditorComponent} from './text-editor.component';
import {NgxEditorModule} from 'ngx-editor';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    NgxEditorModule,
    FormsModule
  ],
  declarations: [
    TextEditorComponent
  ],
  exports: [
    TextEditorComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TextEditorModule {
}
