import {AbstractControl} from '@angular/forms';
import {HttpHeaders} from '@angular/common/http';

export function applyPermission(control: AbstractControl, enabled: boolean): void {
  if (control) {
    const enableFunctionName = enabled ? 'enable' : 'disable';
    control[enableFunctionName]();
  }
}

export function getHeaderForAuthType(name: string): HttpHeaders {
  let headers = new HttpHeaders();
  if (name) {
    headers = headers.set('Auth-controller-type', name);
  }
  return headers;
}
