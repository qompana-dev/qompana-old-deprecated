import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UrlUtil} from '../url.util';

@Injectable({
  providedIn: 'root'
})
export class TempFileService {

  public readonly fileServiceUrl = `${UrlUtil.url}/crm-file-service`;

  constructor(private http: HttpClient) { }

  public downloadTempFile(fileId: string): Observable<Blob> {
    const url = `${this.fileServiceUrl}/files/temp/${fileId}`;
    return this.http.get(url, {responseType: 'blob'});
  }

  public setNewExpirationTimeForTemporaryFiles(): Observable<void> {
    const url = `${this.fileServiceUrl}/files/temp/change-expiration-time`;
    return this.http.post<void>(url, {});
  }
}
