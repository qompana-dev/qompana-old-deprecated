import {Injectable} from '@angular/core';
import {FileItem, FileUploader} from 'ng2-file-upload';
import {LoginService} from '../../login/login.service';
import {UrlUtil} from '../url.util';

@Injectable({
  providedIn: 'root'
})
export class TempFileUploaderService extends FileUploader {

  static readonly tempFileServiceUrl = `${UrlUtil.url}/crm-file-service/files/temp`;

  constructor(private loginService: LoginService) {
    super({url: TempFileUploaderService.tempFileServiceUrl});
  }

  uploadItemWithForm(fileItem: FileItem, form: TempFileFormData): void {
    if (fileItem) {
      fileItem.onBuildForm = this.getOnBuildItemFormForNewFile(form);
      this.uploadItem(fileItem);
    }
  }

  uploadItems(items: FileItem[], form: TempFileFormData): void {
    items.forEach(item => {
      item.onBuildForm = this.getOnBuildItemFormForNewFile(form);
      this.uploadItem(item);
    });
  }

  uploadItem(item: FileItem): void {
    if (!this.loginService.isLoggedIn()) {
      this.loginService.getRefreshTokenSubscription().subscribe(() => {
        super.uploadItem(item);
      });
    } else {
      super.uploadItem(item);
    }
  }

  getOnBuildItemFormForNewFile(fileData: TempFileFormData): (form: any) => void {
    return (form) => {
      form.append('fileData', new Blob(
        [JSON.stringify(fileData)],
        {type: 'application/json'}
      ));
    };
  }

  onBeforeUploadItem(fileItem: FileItem): any {
    fileItem.headers = [{name: 'Authorization', value: 'Bearer ' + this.loginService.getToken()}];
    fileItem.withCredentials = false;
  }
}

export class TempFileFormData {
  description: string;

  constructor() {
    this.description = '';
  }
}
