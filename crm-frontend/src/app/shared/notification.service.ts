import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {NotificationQueueServiceService} from '../notification/queue/notification-queue.service';
import {NotificationDto} from '../notification/notification.model';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private translateService: TranslateService,
              private notificationQueueServiceService: NotificationQueueServiceService) {
  }

  notify(key: string, type: NotificationType, params?: any): void {
    this.showToast(key, type, params);
  }

  notifyError(errorTypes: ErrorTypes, errorCode: number): void {
    let notified = false;
    for (const errorType of errorTypes.errors) {
      if (errorType.code === errorCode) {
        this.showToast(errorTypes.base + errorType.text, NotificationType.ERROR);
        notified = true;
        break;
      }
    }
    if (!notified) {
      this.showToast(errorTypes.base + errorTypes.defaultText, NotificationType.ERROR);
    }
  }

  private showToast(text: string, type: NotificationType, params?: any): void {
    const notification = new NotificationDto();
    notification.title = text;
    notification.type = type;
    notification.args = params;
    notification.date = new Date().toISOString();
    notification.closeable = true;
    notification.groupNotification = false;
    notification.actions = [];

    this.notificationQueueServiceService.add(notification);
  }
}

export enum NotificationType {
  SUCCESS,
  ERROR,
  INFO,
  WARNING,
}

export interface ErrorTypes {
  base: string;
  errors: ErrorType[];
  defaultText: string;
}

export interface ErrorType {
  code: number;
  text: string;
}
