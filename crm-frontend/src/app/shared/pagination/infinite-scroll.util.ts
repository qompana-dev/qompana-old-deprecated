export class InfiniteScrollUtil {
  public static readonly ELEMENTS_ON_PAGE = '20';

  public static shouldGetMoreData(e: any): boolean {
    const tableViewHeight = e.target.offsetHeight;
    const tableScrollHeight = e.target.scrollHeight;
    const scrollLocation = e.target.scrollTop;
    return Math.ceil(tableViewHeight + scrollLocation) >= tableScrollHeight;
  }
}
