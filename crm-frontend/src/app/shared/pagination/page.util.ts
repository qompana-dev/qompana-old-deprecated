import {PageEvent} from '@angular/material';
import {PageRequest} from './page.model';

export function pageEventToPageRequest(page: PageEvent): PageRequest {
  return {
    page: String(page.pageIndex),
    size: String(page.pageSize)
  };
}
