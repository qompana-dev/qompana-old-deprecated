export interface Sort {
  sorted: boolean;
  unsorted: boolean;
  empty: boolean;
}

export interface Pageable {
  sort: Sort;
  offset: number;
  pageSize: number;
  pageNumber: number;
  unpaged: boolean;
  paged: boolean;
}


export interface Page<T> {
  content: T[];
  pageable: Pageable;
  last: boolean;
  totalPages: number;
  totalElements: number;
  number: number;
  size: number;
  sort: Sort;
  numberOfElements: number;
  first: boolean;
  empty: boolean;
}

export class EmptyPage<T> implements Page<T> {
  content: T[];
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  pageable: Pageable;
  size: number;
  sort: Sort;
  totalElements: number;
  totalPages: number;

  constructor() {
    this.content = [];
    this.empty = true;
    this.first = false;
    this.last = false;
    this.number = 0;
    this.numberOfElements = 0;
    this.pageable = null;
    this.size = 0;
    this.sort = null;
    this.totalElements = 0;
    this.totalPages = 0;
  }

}

export interface PageRequest {
  page: string;
  size: string;
  sort?: string;
}
