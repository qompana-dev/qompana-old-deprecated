import { Injectable } from "@angular/core";
import { StompService } from "@stomp/ng2-stompjs";
import { Observable } from "rxjs";
import { IMessage } from "@stomp/stompjs";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class WebsocketService {
  constructor(private stompService: StompService) {
    this.stompService.initAndConnect();
    if (environment.production) {
      setInterval(() => console.clear(), 10000);
    }
  }

  public listenNotificationChange(
    accountId: string | number
  ): Observable<IMessage> {
    return this.stompService.subscribe(
      `/topic/notification/change/${accountId}`
    );
  }
  public listenCustomerWhitelistStateChanged(
    customerId: number | string
  ): Observable<IMessage> {
    return this.stompService.subscribe(
      `/topic/customer/${customerId}/whitelist/change`
    );
  }

  public listenMailListChanged(
    accountId: string | number
  ): Observable<IMessage> {
    return this.stompService.subscribe(`/topic/mails/change/${accountId}`);
  }
}
