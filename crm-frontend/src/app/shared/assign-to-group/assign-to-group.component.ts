import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CrmObject, CrmObjectType} from '../model/search.model';
import {AssignToGroupService} from './assign-to-group.service';
import * as groupBy from 'lodash/groupBy';
import {Subject} from 'rxjs';
import {debounceTime, filter, switchMap, takeUntil} from 'rxjs/operators';
import {CustomerService} from '../../customer/customer.service';


@Component({
  selector: 'app-assign-to-group',
  templateUrl: './assign-to-group.component.html',
  styleUrls: ['./assign-to-group.component.scss']
})
export class AssignToGroupComponent implements OnInit, OnDestroy {
  types: string[] = ['customer', 'contact', 'lead', 'opportunity', 'product', 'file', 'task', 'activity'];

  private componentDestroyed: Subject<void> = new Subject();


  searchTextForm: FormGroup = new FormGroup({
    searchType: new FormControl('all', Validators.compose([Validators.required])),
    searchText: new FormControl('')
  });

  filteredList: CrmObject[] = [];
  groupedList: any;

  @Input() typePlaceholder = 'assignToGroup.associatedWith';
  @Input() textPlaceholder = '';
  @Output() addNewElement: EventEmitter<CrmObject> = new EventEmitter<CrmObject>();

  @Input() set filteredValues(values: string[]) {
    this.types = this.types.filter(key => values.indexOf(key) === -1);
  }

  constructor(private assignToGroupService: AssignToGroupService,
              private customerService: CustomerService) {
  }

  ngOnInit(): void {
    this.searchTextForm.get('searchText').valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      filter((input: string) => !!input),
      switchMap(((input: string) => this.assignToGroupService.searchForType(this.searchTextForm.get('searchType').value,
          this.searchTextForm.get('searchText').value, this.types)
      ))).subscribe(
      result => {
        this.filteredList = result;
        this.groupedList = groupBy(result, 'type');
      }
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  addElement(crmObject: CrmObject): void {
    this.addNewElement.emit(crmObject);
    this.searchTextForm.get('searchText').patchValue('');
    this.searchTextForm.get('searchText').markAsUntouched();
    this.tryAddCustomer(crmObject);
  }

  private tryAddCustomer(crmObject: CrmObject): void {
    if (crmObject.type === CrmObjectType.contact) {
      this.customerService.getCustomerByContactId(crmObject.id)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((customer: CrmObject) => {
          this.addNewElement.emit(customer);
        });
    }
  }
}

export interface AssignToGroupListItem {
  id: number;
  type: string;
  name: string;
}
