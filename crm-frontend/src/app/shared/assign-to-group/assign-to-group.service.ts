import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { CrmObject, CrmObjectType } from "../model/search.model";
import { environment } from "../../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { catchError, flatMap, map, mergeAll, toArray } from "rxjs/operators";
import "rxjs-compat/add/observable/of";
import { flatten } from "@angular/compiler";
import { TaskAssociation } from "../../calendar/task.model";
import { UrlUtil } from "../url.util";

@Injectable({
  providedIn: "root",
})
export class AssignToGroupService {
  requestList: RequestPath[] = [
    {
      type: "role",
      path: "crm-user-service/role/search",
      idsPath: "crm-user-service/role/crm-object",
    },
    {
      type: "lead",
      path: "crm-business-service/leads/search",
      idsPath: "crm-business-service/leads/crm-object",
    },
    {
      type: "user",
      path: "crm-user-service/accounts/search",
      idsPath: "crm-user-service/accounts/crm-object",
    },
    {
      type: "customer",
      path: "crm-business-service/customers/search",
      idsPath: "crm-business-service/customers/crm-object",
    },
    {
      type: "contact",
      path: "crm-business-service/contacts/search",
      idsPath: "crm-business-service/contacts/crm-object",
    },
    {
      type: "file",
      path: "crm-file-service/files/search",
      idsPath: "crm-file-service/files/crm-object",
    },
    {
      type: "task",
      path: "crm-task-service/tasks/search",
      idsPath: "crm-task-service/tasks/crm-object",
    },
    {
      type: "activity",
      path: "crm-task-service/tasks/activity/search",
      idsPath: "crm-task-service/tasks/activity/crm-object",
    },
    {
      type: "opportunity",
      path: "crm-business-service/opportunity/search",
      idsPath: "crm-business-service/opportunity/crm-object",
    },
    {
      type: "manufacturer",
      path: "crm-business-service/manufacturer/search",
      idsPath: "crm-business-service/manufacturer/crm-object",
    },
    {
      type: "supplier",
      path: "crm-business-service/supplier/search",
      idsPath: "crm-business-service/supplier/crm-object",
    },
  ]; // TODO: add products

  constructor(private http: HttpClient) {}

  updateLabelsForTaskAssociation(
    taskAssociations: TaskAssociation[]
  ): Observable<TaskAssociation[]> {
    const crmObjects: CrmObject[] = taskAssociations.map((obj) => {
      return { id: obj.objectId, type: obj.objectType };
    });
    return this.updateLabels(crmObjects).pipe(
      map((objects: CrmObject[]): TaskAssociation[] => {
        return objects.map((object) => {
          const taskAssociation = taskAssociations.find(
            (ta) => ta.objectType === object.type && ta.objectId === object.id
          );
          if (taskAssociation) {
            taskAssociation.label = object.label;
            return taskAssociation;
          }
          return undefined;
        });
      })
    );
  }

  getAssociationsDetails(taskId: number): Observable<TaskAssociation[]> {
    return this.http.get<TaskAssociation[]>(
      `${UrlUtil.url}/crm-task-service/tasks/${taskId}/associations-details`
    );
  }

  updateLabels(crmObjects: CrmObject[]): Observable<CrmObject[]> {
    const objMap: Map<string, CrmObjectType[]> = new Map();
    return Observable.of(
      this.requestList.map((requestItem) =>
        this.getIdsObservable(crmObjects, requestItem)
      )
    ).pipe(
      mergeAll(),
      flatMap((list) => list),
      toArray(),
      map((result) => flatten(result))
    );
  }

  getIdsObservable(
    crmObjects: CrmObject[],
    requestPath: RequestPath
  ): Observable<CrmObject[]> {
    const ids: number[] = this.getIdsByType(crmObjects, requestPath.type);
    if (ids && ids.length > 0) {
      this.requestList.find((obj) => obj.type === requestPath.type);
      return this.http
        .get<CrmObject[]>(`${UrlUtil.url}/${requestPath.idsPath}?ids=${ids}`)
        .pipe(catchError(() => of([])));
    }
    return of([]);
  }

  getIdsByType(crmObjects: CrmObject[], type: string): number[] {
    return crmObjects.filter((obj) => obj.type === type).map((obj) => obj.id);
  }

  searchForType(
    type: string,
    pattern: string,
    allTypes: string[]
  ): Observable<CrmObject[]> {
    if (!pattern) {
      pattern = '';
    }
    const requestPath: RequestPath = this.getRequestPath(type);
    let result: Observable<CrmObject[]>;
    if (requestPath) {
      result = this.getSearchObservable(requestPath.path, pattern, false);
    } else {
      result = this.getSearchForAll(pattern, allTypes);
    }
    return result;
  }

  getSearchForAll(pattern: string, allKeys: string[]): Observable<CrmObject[]> {
    return Observable.of(
      this.requestList
        .filter((req) => allKeys.indexOf(req.type) !== -1)
        .map((requestItem) =>
          this.getSearchObservable(requestItem.path, pattern, true)
        )
    ).pipe(
      mergeAll(),
      flatMap((list) => list),
      toArray(),
      map((result) => flatten(result))
    );
  }

  getSearchObservable(
    path: string,
    pattern: string,
    returnEmptyIfError: boolean
  ): Observable<CrmObject[]> {
    let result: Observable<CrmObject[]> = this.http.get<CrmObject[]>(
      `${UrlUtil.url}/${path}?term=${pattern}`
    );
    if (returnEmptyIfError) {
      result = result.pipe(catchError(() => of([])));
    }
    return result;
  }

  getRequestPath(type: string): RequestPath {
    return this.requestList.find((item) => item.type === type);
  }
}

export class RequestPath {
  type: string;
  path: string;
  idsPath: string;
}
