import {Directive, ElementRef, Input, OnChanges, OnDestroy, OnInit, Renderer2, SimpleChanges} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {TooltipType} from './tooltip-type';

@Directive({
  selector: '[appTooltip]'
})
export class TooltipDirective implements OnInit, OnChanges, OnDestroy {
  static TRANSLATE_PREFIX = 'global.tooltip.';
  private componentDestroyed: Subject<void> = new Subject();

  @Input() appTooltip: TooltipType;

  constructor(private renderer: Renderer2,
              private translateService: TranslateService,
              private el: ElementRef) {

  }

  ngOnInit(): void {
    this.translateAndSetValue();
    this.translateService.onDefaultLangChange
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.translateAndSetValue());
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.translateAndSetValue();
  }

  private translateAndSetValue(): void {
    this.translateService.get(this.getTranslateMessage())
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((translation) => this.setValue(translation));
  }

  private setValue(translation: string): void {
    this.renderer.setAttribute(this.el.nativeElement, 'title', translation);
  }

  private getTranslateMessage(): string {
    if (!this.appTooltip) {
      return '';
    }
    return TooltipDirective.TRANSLATE_PREFIX + this.appTooltip.toLowerCase();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }
}
