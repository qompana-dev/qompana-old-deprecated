import {Observable, Subject} from 'rxjs';
import {FilterStrategy} from './model';
import {Page, PageRequest} from './pagination/page.model';
import {InfiniteScrollUtil} from './pagination/infinite-scroll.util';
import {takeUntil, throttleTime} from 'rxjs/operators';
import {NotificationService, NotificationType} from './notification.service';
import {Sort} from '@angular/material/sort';

export class InfinityScrollUtil {
  public itemPage: Page<any>;

  public availableFilters = FilterStrategy;
  public selectedFilter: FilterStrategy = FilterStrategy.ALL;

  public loaded = false;

  private currentSort = 'id,asc';
  public pageRequest: PageRequest = {page: '0', size: InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: this.currentSort};
  private onScrollInitSubject: Subject<void> = new Subject();
  public onScrollInit$ = this.onScrollInitSubject.asObservable();

  private componentDestroyed: Subject<void> = new Subject();
  private currentPage: number;

  private notificationService: NotificationService;
  private readonly errorMessage: string;
  private infinityScrollUtilInterface: InfinityScrollUtilInterface;

  constructor(infinityScrollUtilInterface: InfinityScrollUtilInterface,
              componentDestroyed: Subject<void>,
              notificationService: NotificationService,
              errorMessage: string,
              sort: string = 'id,asc') {
    this.loaded = false;
    this.componentDestroyed = componentDestroyed;
    this.notificationService = notificationService;
    this.errorMessage = errorMessage;
    this.infinityScrollUtilInterface = infinityScrollUtilInterface;
    this.currentSort = sort;
    this.init(this.selectedFilter);
  }

  public filter(selection: FilterStrategy): void {
    this.selectedFilter = selection;
    this.init(selection);
  }

  public onScroll(e: any): void {
    if (InfiniteScrollUtil.shouldGetMoreData(e) && this.itemPage.numberOfElements < this.itemPage.totalElements) {
      const nextPage = this.currentPage + 1;
      this.getMore({page: '' + nextPage, size: InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: this.currentSort});
    }
  }

  public onSortChange($event: Sort): void {
    this.currentSort = $event.direction ? ($event.active + ',' + $event.direction) : 'id,asc';
    this.init(this.selectedFilter);
  }

  public isSelected(selection: FilterStrategy): boolean {
    return this.selectedFilter === selection;
  }

  public initData(): void {
    this.init(this.selectedFilter);
  }
  public init(selection: FilterStrategy, pageRequest: PageRequest =
    {page: '0', size: InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: this.currentSort}): void {
    this.loaded = false;
    this.infinityScrollUtilInterface.getItemObservable(selection, pageRequest)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
      page => this.handleInitSuccess(page),
      () => this.notifyError()
    );
  }

  private getMore(pageRequest: PageRequest): void {
    this.infinityScrollUtilInterface.getItemObservable(this.selectedFilter, pageRequest)
      .pipe(
        takeUntil(this.componentDestroyed),
        throttleTime(500)
      ).subscribe(
      page => this.handleGetMoreSuccess(page),
      () => this.notifyError()
    );
  }

  private handleInitSuccess(page: any): void {
    this.loaded = true;
    this.currentPage = 0;
    this.itemPage = page;
    this.onScrollInitSubject.next();
  }

  private notifyError(): void {
    this.loaded = true;
    this.notificationService.notify(this.errorMessage, NotificationType.ERROR);
  }
  private handleGetMoreSuccess(page: any): void {
    this.loaded = true;
    this.currentPage++;
    this.itemPage.content = this.itemPage.content.concat(page.content);
    this.itemPage.numberOfElements += page.content.length;
  }
}

export interface InfinityScrollUtilInterface {
  getItemObservable(selection: FilterStrategy, pageRequest: PageRequest): Observable<any>;
}
