import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-load-svg-on-init',
  template: `
    <span style="width: 0; height: 0; display: none;">
      <img *ngFor="let svg of svgList" alt="svg-on-init" src="{{getSvg(svg)}}"/>
    </span>
  `
})
export class LoadSvgOnInitComponent {
  @Input() svgList: string[] = [];
  constructor() {}

  getSvg(svg: string): string {
    return 'assets/icons/' + svg + '.svg';
  }
}
