import {AbstractControl, ValidationErrors} from '@angular/forms';
import {DateUtil} from './date.util';
import * as isNil from 'lodash/isNil';

export class DurationValidatorsUtil {

  public static getDateChronologyValidator(): (control: AbstractControl) => ValidationErrors {
    return (control: AbstractControl): ValidationErrors => {
      const difference = DateUtil.getDifference(
        control.get('startDateMonth').value,
        control.get('startDateYear').value,
        control.get('endDateMonth').value,
        control.get('endDateYear').value);
      return (isNil(difference) || difference >= 0) ? null : {chronologyDateError: true};
    };
  }

  public static getDependentFieldValidatorValidator(firstField: string, secondField: string): (control: AbstractControl) => ValidationErrors {
    return (control: AbstractControl): ValidationErrors => {
      return ((control.get(firstField).value && isNil(control.get(secondField).value)) ||
        (isNil(control.get(firstField).value) && control.get(secondField).value)) ? {dependentFieldError: true} : null;
    };
  }
}
