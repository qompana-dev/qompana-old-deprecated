import {Observable, Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil, tap} from 'rxjs/operators';
import {SliderStateChange} from '../slider/slider.service';


export function whenSliderOpen(componentDestroyed: Subject<void>, keys: string[]): (source: Observable<SliderStateChange>) => Observable<boolean> {
  return source => source.pipe(
    takeUntil(componentDestroyed),
    distinctUntilChanged((x, y) => x.key === y.key && x.opened === y.opened),
    filter(change => keys.includes(change.key)),
    map(change => change.opened),
    filter(opened => opened)
  );
}


export function whenSliderClosed(componentDestroyed: Subject<void>, keys: string[]): (source: Observable<SliderStateChange>) => Observable<boolean> {
  return source => source.pipe(
    takeUntil(componentDestroyed),
    distinctUntilChanged((x, y) => x.key === y.key && x.opened === y.opened),
    filter(change => keys.includes(change.key)),
    map(change => change.opened),
    filter(opened => !opened),
  );
}
