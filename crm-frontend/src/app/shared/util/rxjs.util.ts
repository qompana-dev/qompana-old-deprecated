import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

export function consoleLog(message: string = ''): (source: Observable<any>) => Observable<any> {
  return tap(value => console.log(value, message));
}
