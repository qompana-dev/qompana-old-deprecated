// @ts-ignore
import moment, {Moment} from 'moment';
import * as humanizeDuration from 'humanize-duration';
import * as isNil from 'lodash/isNil';

import {britishCrmTimeFormat, crmTimeFormat} from '../config/times.config';


export class DateUtil {

  public static getInputFormat(): any {
    return {
      parse: {
        dateInput: 'LL'
      },
      display: {
        dateInput: localStorage.getItem('dateFormat'),
        monthYearLabel: 'YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'YYYY'
      }
    };
  }

  public static convertDateToString(date: Date): string {
    const year = date.getFullYear();
    const month = (1 + date.getMonth()).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    return year + '-' + month + '-' + day;
  }

  public static getNumbersList(start: number, stop: number, step: number): number[] {
    return Array.from({length: (stop - start) / step + 1}, (_, i) => start + (i * step));
  }

  public static getYearsListFromNow(stop: number, step: number): number[] {
    const start = (new Date()).getFullYear();
    const _stop = start + stop;
    return Array.from({length: (_stop - start) / step + 1}, (_, i) => start + (i * step));
  }

  public static getMonthsList(): { number: number, name: string }[] {
    const locale = localStorage.getItem('locale');
    moment.locale(locale);
    return moment.months('MMM').map((month, index) => ({number: index + 1, name: month}));
  }

  public static getMonthNumber(date: string): number {
    return date && +moment(date, 'YYYY-MM-DD').format('M');
  }

  public static getYear(date: string): number {
    return date && +moment(date, 'YYYY-MM-DD').format('YYYY');
  }

  public static getDuration(startMonth: number, startYear: number, endMonth: number, endYear: number): string {
    const diff = DateUtil.getDifference(startMonth, startYear, endMonth, endYear);
    return humanizeDuration(diff, {
      language: localStorage.getItem('locale'),
      units: ['y', 'mo'],
      conjunction: ' i ',
      round: true
    });
  }

  public static getDifference(startMonth: number, startYear: number, endMonth: number, endYear: number): number | null {
    if (isNil(startMonth) || isNil(startYear) || isNil(endMonth) || isNil(endYear)) {
      return null;
    }
    const _start = DateUtil.getDate(startMonth, startYear);
    const _end = DateUtil.getDate(endMonth, endYear);
    return _end.diff(_start);
  }

  public static getDate(month: number, year: number): Moment {
    return moment(`${month}-${year}`, 'MM-YYYY');
  }

  public static getStringDate(month: number, year: number): string | null {
    if (isNil(month) || isNil(year)) {
      return null;
    }
    return DateUtil.getDate(month, year).format('YYYY-MM-DD');
  }

  public static formatDate(date: string, withTime: boolean = false): string {
    const timeFormat = localStorage.getItem('hourFormat') === '12' ? britishCrmTimeFormat : crmTimeFormat;
    let dateFormat = localStorage.getItem('dateFormat');
    dateFormat = withTime ? dateFormat + ', ' + timeFormat : dateFormat;
    if (date) {
      return moment.utc(date).tz(moment.tz.guess()).locale(localStorage.getItem('locale')).format(dateFormat);
    }
  }

  public static formatTime(date: string): string {
    if (date) {
      const timeFormat = localStorage.getItem('hourFormat') === '12' ? britishCrmTimeFormat : crmTimeFormat;
      return moment.utc(date).tz(moment.tz.guess()).locale(localStorage.getItem('locale')).format(timeFormat);
    }
  }

  public static customFormatDate(date: string, format: string): string {
    if (date) {
      return moment.utc(date).tz(moment.tz.guess()).locale(localStorage.getItem('locale')).format(format);
    }
  }

  public static getCurrentYear(): number {
    return new Date().getFullYear();
  }

  public static toDate(date: string): Date {
    if (date) {
      return moment.utc(date).tz(moment.tz.guess()).locale(localStorage.getItem('locale')).toDate();
    }
  }
}
