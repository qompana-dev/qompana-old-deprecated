import {Component, Inject, OnInit} from '@angular/core';
import {DeleteDialogData} from '../delete/delete-dialog.component';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-confirm-with-comment',
  templateUrl: './confirm-with-comment.component.html',
  styleUrls: ['./confirm-with-comment.component.scss']
})
export class ConfirmWithCommentComponent implements OnInit {

  commentFormControl: FormControl;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DeleteDialogData) {
  }

  ngOnInit(): void {
    this.commentFormControl = new FormControl('');
  }

  cancel(): void {
    if (this.data.onCancelClick) {
      this.data.onCancelClick();
    }
  }

  confirm(): void {
    if (this.data.onConfirmClick) {
      this.data.onConfirmClick(this.commentFormControl.value);
    }
  }
}
