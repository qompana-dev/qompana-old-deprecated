import {Injectable} from '@angular/core';
import {DeleteDialogData} from '../delete/delete-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmWithCommentComponent} from './confirm-with-comment.component';

@Injectable({
  providedIn: 'root'
})
export class ConfirmWithCommentService {

  constructor(private dialog: MatDialog) {
  }

  open(data: Partial<DeleteDialogData>): void {
    this.dialog.open(ConfirmWithCommentComponent, {
      width: '500px',
      height: '270px',
      data,
      panelClass: 'delete-dialog-container'
    });
  }
}
