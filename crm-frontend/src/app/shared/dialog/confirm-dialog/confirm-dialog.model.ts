export interface ConfirmDialogData {
  title: string;
  content?: HTMLDialogContent | SelectDialogContent;
  contentType?: ContentTypes;
  actions?: ActionDialog[];
}

export enum ContentTypes {
  HTML = "HTML",
  SELECT = "SELECT",
}

export enum Colors {
  ALERT = "alert",
  PRIMARY = "primary",
  SUCCESS = "success",
}


export interface HTMLDialogContent {
  icon?: string;
  iconColor?: Colors;
  htmlTemplate?: string;
}

export interface SelectDialogContent {
  items: any[];
  translateField?: string;
  placeholder?: string;
}

export interface ActionDialog {
  color?: Colors;
  text: string;
  func?: (e?) => void;
}
