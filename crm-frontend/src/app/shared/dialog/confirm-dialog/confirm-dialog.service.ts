import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material";
import { ConfirmDialogComponent } from "./confirm-dialog.component";
import { ConfirmDialogData } from "./confirm-dialog.model";

@Injectable({
  providedIn: "root",
})
export class ConfirmDialogService {
  constructor(private dialog: MatDialog) {}

  open(data: Partial<ConfirmDialogData>): void {
    this.dialog.open(ConfirmDialogComponent, {
      minWidth: 500,
      data,
      panelClass: "confirm-dialog-container",
    });
  }
}
