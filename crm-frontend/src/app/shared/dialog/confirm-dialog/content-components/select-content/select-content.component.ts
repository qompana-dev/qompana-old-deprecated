import { SelectDialogContent } from "./../../confirm-dialog.model";
import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";

@Component({
  selector: "app-select-content",
  templateUrl: "./select-content.component.html",
  styleUrls: ["./select-content.component.scss"],
})
export class SelectContentComponent implements OnInit {
  @Input() content: SelectDialogContent;

  @Output() changeContentData: EventEmitter<any> = new EventEmitter();

  selectItemForm: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.selectItemForm = this.fb.group({
      selectItem: [null, Validators.required],
    });
  }

  get selectItem(): AbstractControl {
    return this.selectItemForm.get("selectItem");
  }
  get isFormValid(): boolean {
    this.selectItem.markAsTouched();
    return this.selectItem.valid;
  }

  selectionChange(): void {
    this.changeContentData.emit(this.selectItem.value)
  }
}
