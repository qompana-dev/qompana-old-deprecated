import { Colors, HTMLDialogContent } from "./../../confirm-dialog.model";
import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-html-content",
  templateUrl: "./html-content.component.html",
  styleUrls: ["./html-content.component.scss"],
})
export class HTMLContentComponent implements OnInit {
  @Input() content: HTMLDialogContent;

  public colors = Colors;

  constructor() {}

  ngOnInit(): void {}
}
