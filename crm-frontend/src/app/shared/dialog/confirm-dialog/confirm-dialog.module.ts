import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "src/app/shared/shared.module";

import { ConfirmDialogComponent } from "./confirm-dialog.component";

import { HTMLContentComponent } from "./content-components/html-content/html-content.component";
import { SelectContentComponent } from "./content-components/select-content/select-content.component";

const contentComponents = [HTMLContentComponent, SelectContentComponent];

@NgModule({
  declarations: [ConfirmDialogComponent, ...contentComponents],
  imports: [CommonModule, SharedModule],
  exports: [ConfirmDialogComponent],
  entryComponents: [ConfirmDialogComponent, ...contentComponents],
})
export class ConfirmDialogModule {}
