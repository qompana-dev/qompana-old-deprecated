import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material";
import {
  Colors,
  ConfirmDialogData,
  ContentTypes,
} from "./confirm-dialog.model";

@Component({
  selector: "app-confirm-dialog",
  templateUrl: "./confirm-dialog.component.html",
  styleUrls: ["./confirm-dialog.component.scss"],
})
export class ConfirmDialogComponent {
  public contentData: any;
  public contentTypes = ContentTypes;
  public colors = Colors;

  constructor(@Inject(MAT_DIALOG_DATA) public data: ConfirmDialogData) {}

  setContentData(data) {
    this.contentData = data;
  }
}
