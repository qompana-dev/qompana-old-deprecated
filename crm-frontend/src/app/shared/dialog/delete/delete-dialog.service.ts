import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {DeleteDialogComponent, DeleteDialogData} from './delete-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DeleteDialogService {

  constructor(private dialog: MatDialog) {
  }

  openWithoutHeight(data: Partial<DeleteDialogData>): void {
    this.dialog.open(DeleteDialogComponent, {
      width: '500px',
      data,
      panelClass: 'delete-dialog-container'
    });
  }

  open(data: Partial<DeleteDialogData>): void {
    this.dialog.open(DeleteDialogComponent, {
      width: '500px',
      height: '220px',
      data,
      panelClass: 'delete-dialog-container'
    });
  }
}
