import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DeleteDialogData) {
  }

  cancel(): void {
    if (this.data.onCancelClick) {
      this.data.onCancelClick();
    }
  }

  confirm(): void {
    if (this.data.onConfirmClick) {
      this.data.onConfirmClick();
    }
  }
}

export interface DeleteDialogData {
  title: string;
  description: string;
  numberToDelete: any;
  errorList?: string[];
  noteFirst: string;
  noteSecond: string;
  confirmButton: string;
  cancelButton: string;
  onConfirmClick: (comment?: string) => void;
  onCancelClick: () => void;
}
