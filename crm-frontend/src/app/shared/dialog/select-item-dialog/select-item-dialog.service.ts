import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {SvgIconRegistryService} from 'angular-svg-icon';
import {SelectItemDialogComponent, SelectItemDialogData} from './select-item-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class SelectItemDialogService {

  constructor(private dialog: MatDialog,
              private iconReg: SvgIconRegistryService) {
    this.iconReg.loadSvg('/assets/icons/small-cross.svg', 'small-cross');
  }

  open(data: Partial<SelectItemDialogData>): void {
    this.dialog.open(SelectItemDialogComponent, {
      width: '450px', height: '180px', data,
      panelClass: 'select-item-dialog-container'
    });
  }
}
