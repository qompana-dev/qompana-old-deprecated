import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DeleteDialogData} from '../delete/delete-dialog.component';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-select-item-dialog',
  templateUrl: './select-item-dialog.component.html',
  styleUrls: ['./select-item-dialog.component.scss']
})
export class SelectItemDialogComponent {
  selectItemForm: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: SelectItemDialogData,
              private dialogRef: MatDialogRef<any>,
              private fb: FormBuilder) {
    this.selectItemForm = this.fb.group({
      selectItem: [null, Validators.required],
    });
  }

  get selectItem(): AbstractControl {
    return this.selectItemForm.get('selectItem');
  }
  get isFormValid(): boolean {
    this.selectItem.markAsTouched();
    return this.selectItem.valid;
  }

  cancel(): void {
    if (this.data.onCancelClick) {
      this.data.onCancelClick();
    }
  }
  selectionChange(): void {
    if (this.data.onSelect) {
      this.data.onSelect(this.selectItem.value);
    }
  }

  confirm(): void {
    if (this.data.onConfirmClick && this.isFormValid) {
      this.data.onConfirmClick(this.selectItem.value);
      this.dialogRef.close();
    }
  }
}
export interface SelectItemDialogData {
  title: string;
  placeholder: string;
  items: any[];
  propertyVisibleOnList: string;
  confirmButton: string;
  cancelButton: string;
  onSelect: (item: any) => void;
  onConfirmClick: (item: any) => void;
  onCancelClick: () => void;
}
