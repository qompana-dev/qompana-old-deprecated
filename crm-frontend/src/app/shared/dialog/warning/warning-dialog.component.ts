import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-warning-dialog',
  templateUrl: './warning-dialog.component.html',
  styleUrls: ['./warning-dialog.component.scss']
})
export class WarningDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: WarningDialogData) {
  }

  cancel(): void {
    if (this.data.onCancelClick) {
      this.data.onCancelClick();
    }
  }

  confirm(): void {
    if (this.data.onConfirmClick) {
      this.data.onConfirmClick();
    }
  }
}

export interface WarningDialogData {
  title: string;
  description: string;
  confirmButton: string;
  cancelButton: string;
  onConfirmClick: () => void;
  onCancelClick: () => void;
}
