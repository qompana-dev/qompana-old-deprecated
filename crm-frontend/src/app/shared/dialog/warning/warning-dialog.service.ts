import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {SvgIconRegistryService} from 'angular-svg-icon';
import {WarningDialogComponent, WarningDialogData} from './warning-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class WarningDialogService {

  constructor(private dialog: MatDialog,
              private iconReg: SvgIconRegistryService) {
    this.iconReg.loadSvg('/assets/icons/small-cross.svg', 'small-cross');
    this.iconReg.loadSvg('/assets/icons/add.svg', 'warning');
  }

  open(data: Partial<WarningDialogData>): void {
    this.dialog.open(WarningDialogComponent, {
      width: '500px',
      height: '220px',
      data: data,
      panelClass: 'delete-dialog-container'
    });
  }

  close(): void {
    this.dialog.closeAll();
  }
}
