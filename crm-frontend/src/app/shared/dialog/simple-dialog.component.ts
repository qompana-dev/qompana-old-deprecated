import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-simple-dialog',
  template: `
    <h1 mat-dialog-title>{{data.title | translate}}</h1>
    <mat-dialog-content>
      <p>{{data.description | translate}}</p>
    </mat-dialog-content>
    <mat-dialog-actions align="end">
      <button *ngFor="let button of data.buttons" mat-raised-button [mat-dialog-close]="button" [color]="button.color"
              (click)="onClick(button)">{{button.text | translate}}</button>
    </mat-dialog-actions>
  `
})
export class SimpleDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: SimpleDialogData) {
  }

  onClick(button: SimpleDialogButton): void {
    if (button.onClick) {
      button.onClick(button);
    }
  }
}

export interface SimpleDialogButton {
  text: string;
  color?: string; // primary, accent, warn
  onClick?: (dialog: SimpleDialogButton) => void;
}

export interface SimpleDialogData {
  title: string;
  description: string;
  buttons: SimpleDialogButton[];
}
