import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared.module';
import { ActivityTabComponent } from './activity-tab/activity-tab.component';
import { MapViewModule } from '../../map/map-view/map-view.module';
import { TaskSliderModule } from '../../calendar/task-slider/task-slider.module';
import { HistoryTabComponent } from './history-tab/history-tab.component';
import { AttachedEmailsComponent } from './attached-emails/attached-emails.component';
import { HtmlInFrameModule } from '../../mail/mail-main-page/message-details/html-in-frame/html-in-frame.module';
import { MailAttachmentsModule } from '../../mail/mail-main-page/new-mail/mail-attachments/mail-attachments.module';
import { DictionaryModule } from '../../dictionary/dictionary.module';
import { ScheduleTabComponent } from './activity-tab/shedule-tab/schedule-tab.component';
import { ActivityDescriptionComponent } from './activity-tab/shedule-tab/activity-description/activity-description.component';
import { EmailExpandTabComponent } from './email-expand-tab/email-expand-tab.component';
import { RelatedOpportunitiesTabComponent } from './related-opportunities-tab/related-opportunities-tab.component';
import {OpportunitySliderModule} from "../../opportunity/opportunity-slider/opportunity-slider.module";

@NgModule({
  declarations: [
    ActivityTabComponent,
    ActivityDescriptionComponent,
    ScheduleTabComponent,
    HistoryTabComponent,
    AttachedEmailsComponent,
    EmailExpandTabComponent,
    RelatedOpportunitiesTabComponent,
  ],
    imports: [
        CommonModule,
        SharedModule,
        MapViewModule,
        TaskSliderModule,
        HtmlInFrameModule,
        MailAttachmentsModule,
        DictionaryModule,
        OpportunitySliderModule,
    ],
  exports: [
    ActivityTabComponent,
    ActivityDescriptionComponent,
    ScheduleTabComponent,
    HistoryTabComponent,
    AttachedEmailsComponent,
    EmailExpandTabComponent,
    RelatedOpportunitiesTabComponent,
  ],
})
export class TabsModule {}
