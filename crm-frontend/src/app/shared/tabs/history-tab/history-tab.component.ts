import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { CrmObject } from '../../model/search.model';
import { HistoryChange, HistoryType } from './history.model';
import { HistoryService } from './history.service';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AccountEmail } from '../../../person/person.model';
import { PersonService } from '../../../person/person.service';
import { HistoryFormatUtil } from './history-format.util';
import { FormatNumberPipe } from '../../pipe/format-number.pipe';
import { TranslateService } from '@ngx-translate/core';
import { CustomerService } from 'src/app/customer/customer.service';
import { ContactsService } from 'src/app/contacts/contacts.service';
import {
  Dictionary,
  DictionaryId,
  DictionaryWord,
} from '../../../dictionary/dictionary.model';
import { DictionaryService } from '../../../dictionary/dictionary.service';
import { AdditionalPhoneNumber } from '../../additional-phone-numbers/additional-phone-numbers.model';

@Component({
  selector: 'app-history-tab',
  templateUrl: './history-tab.component.html',
  styleUrls: ['./history-tab.component.scss'],
})
export class HistoryTabComponent implements OnInit, OnDestroy, OnChanges {
  id: number;
  type: HistoryType;

  changeList: Array<any> = [];
  userMap: Map<string, string> = new Map<string, string>();
  customers: CrmObject[] = [];
  contacts: CrmObject[] = [];
  accountEmails: AccountEmail[] = [];
  sourceOfAcquisition: DictionaryWord[] = [];
  subjectsOfInterest: DictionaryWord[] = [];
  additionalPhoneNumbers: AdditionalPhoneNumber[] = [];

  @Input()
  customerFields: string[] = [];

  @Input()
  contactFields: string[] = [];

  private componentDestroyed: Subject<void> = new Subject();

  @Input()
  set historyType(type: HistoryType) {
    this.type = type;
  }

  @Input()
  set objectId(id: number) {
    this.id = id;
  }

  @Input() changedSubscription: Subject<void>;
  $changedSubscription: Subscription;

  @Input() bigSize = false;
  @Input() fullHeight = false;

  constructor(
    private historyService: HistoryService,
    private personService: PersonService,
    private customerService: CustomerService,
    private contactsService: ContactsService,
    private translateService: TranslateService,
    private formatNumberPipe: FormatNumberPipe,
    private dictionaryService: DictionaryService
  ) {}

  ngOnInit(): void {
    this.getAccountEmailList();
    this.getSourceOfAcquisition();
    this.getSubjectsOfInterest();
    this.getHistory();

    if (this.changedSubscription) {
      this.$changedSubscription = this.changedSubscription.subscribe(() => {
        this.getHistory();
      });
    }
  }

  ngOnChanges(simpleChange: SimpleChanges): void {
    if (simpleChange.customerFields) {
      this.getCustomersList();
      this.getContactsList();
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
    if (this.$changedSubscription) {
      this.$changedSubscription.unsubscribe();
    }
  }

  getHistory(): void {
    if (this.id && this.type) {
      this.historyService
        .getHistoryChanges(this.type, this.id)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((changeList: any[]) => {
          this.changeList = changeList;
          this.getCustomersList();
          this.getContactsList();
        });
    }
  }

  private getAccountEmailList(): void {
    this.personService
      .getAccountEmails()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((response: AccountEmail[]) => {
        this.accountEmails = response;
        response.forEach((account) => {
          this.userMap.set(account.email, account.name + ' ' + account.surname);
        });
      });
  }

  private getContactsList(): void {
    const contactItems = this.changeList
      .map((item) => item.changes || item)
      .reduce((acc, val) => acc.concat(val), [])
      .filter(
        (item) => this.contactFields.includes(item.fieldName) && item.newValue
      )
      .map((contact) => +contact.newValue);
    const contactIds = contactItems.map((contact) => +contact.newValue);
    const contactUniqueIds = contactIds.filter(
      (id, i) => contactIds.indexOf(id) === i
    );

    contactUniqueIds.length &&
      this.contactsService
        .getContactsByIds(contactUniqueIds)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((contacts: CrmObject[]) => {
          this.contacts = contacts;
        });
  }

  private getCustomersList(): void {
    const customerItems = this.changeList
      .map((item) => item.changes || item)
      .reduce((acc, val) => acc.concat(val), [])
      .filter(
        (item) => this.customerFields.includes(item.fieldName) && item.newValue
      )
      .map((customer) => +customer.newValue);
    const customerIds = customerItems.map((contact) => +contact.newValue);
    const customerUniqueIds = customerIds.filter(
      (id, i) => customerIds.indexOf(id) === i
    );

    customerUniqueIds.length &&
      this.customerService
        .getCustomersByIds(customerUniqueIds)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((customers: CrmObject[]) => {
          this.customers = customers;
        });
  }

  public getValue(
    change: HistoryChange,
    newValue: boolean,
    type?: HistoryType
  ): string {
    return HistoryFormatUtil.getValueFormat(
      change,
      newValue,
      type ? type : this.type,
      this.accountEmails,
      this.formatNumberPipe,
      this.translateService,
      this.customers,
      this.contacts,
      this.sourceOfAcquisition,
      this.subjectsOfInterest,
      this.additionalPhoneNumbers
    );
  }

  public isShowValue(change: HistoryChange, type?: HistoryType): boolean {
    return HistoryFormatUtil.isShowValue(change, type ? type : this.type);
  }

  private getSourceOfAcquisition(): void {
    this.dictionaryService
      .findWholeDictionary(DictionaryId.SOURCE_OF_ACQUISITION)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((dictionary) => {
        this.sourceOfAcquisition = this.getWords(dictionary);
      });
  }

  private getSubjectsOfInterest(): void {
    this.dictionaryService
      .findWholeDictionary(DictionaryId.SUBJECT_OF_INTEREST)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((dictionary) => {
        this.subjectsOfInterest = this.getWords(dictionary);
      });
  }

  private getWords(dictionary: Dictionary): DictionaryWord[] {
    if (dictionary.words === undefined || dictionary.words === null) {
      return [];
    }
    return dictionary.words;
  }
}
