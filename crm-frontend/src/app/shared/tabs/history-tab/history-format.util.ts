import {HistoryChange, HistoryType} from './history.model';
import {AccountEmail} from '../../../person/person.model';
import {FormatNumberPipe} from '../../pipe/format-number.pipe';
import {TranslateService} from '@ngx-translate/core';
import {DateUtil} from '../../util/date.util';
import {CrmObject} from '../../model/search.model';
import {DictionaryWord} from '../../../dictionary/dictionary.model';
import * as omit from 'lodash/omit';
import {AdditionalPhoneNumber} from '../../additional-phone-numbers/additional-phone-numbers.model';

export class HistoryFormatUtil {
  private static translations: TranslateValue[] = [
    {
      historyType: HistoryType.OPPORTUNITY,
      fieldName: 'type',
      translationStart: 'opportunity.form.types.',
      isLowerCase: true
    },
    {
      historyType: HistoryType.OPPORTUNITY, fieldName: 'finishResult',
      translationStart: 'opportunity.form.dialog.finishOpportunity.item.', isLowerCase: true
    },
    {
      historyType: HistoryType.OPPORTUNITY, fieldName: 'rejectionReason',
      translationStart: 'opportunity.form.dialog.rejectionReason.item.', isLowerCase: true
    },
    {
      historyType: HistoryType.OPPORTUNITY, fieldName: 'priority',
      translationStart: 'opportunity.form.priorities.', isLowerCase: true
    },
    {
      historyType: HistoryType.LEAD, fieldName: 'salesOpportunity',
      translationStart: 'leads.form.salesOpportunity.', isCamelCase: true
    },
    {
      historyType: HistoryType.LEAD, fieldName: 'priority',
      translationStart: 'leads.form.leadPriority.', isLowerCase: true
    },
    {
      historyType: HistoryType.LEAD, fieldName: 'closeResult',
      translationStart: 'leads.form.dialog.closeLead.item.', isLowerCase: true
    },
    {
      historyType: HistoryType.LEAD, fieldName: 'rejectionReason',
      translationStart: 'leads.form.dialog.rejectionReason.item.', isLowerCase: true
    },
  ];

  static isShowValue(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.LEAD:
        return !['avatar', 'mainNoteId'].includes(change.fieldName);
      case HistoryType.EXPENSE:
        return !['id', 'lead', 'creatorId', 'state'].includes(change.fieldName);
      case HistoryType.TASK:
        return !['id', 'creator', 'notified', 'created', 'taskAssociations', 'timeZone', 'unit', 'allDayEvent', 'notificationTime', 'interval', 'dateTimeTo'].includes(change.fieldName);
      default:
        return true;
    }
  }

  static getValueFormat(change: HistoryChange,
                        newValue: boolean,
                        historyType: HistoryType,
                        accountEmails: AccountEmail[],
                        formatNumberPipe: FormatNumberPipe,
                        translateService: TranslateService,
                        customers: CrmObject[],
                        contacts: CrmObject[],
                        sourceOfAcquisition: DictionaryWord[],
                        subjectsOfInterest: DictionaryWord[],
                        additionalPhoneNumbers: AdditionalPhoneNumber[],
  ): string {
    const value: string = (newValue) ? change.newValue : change.oldValue;
    if (!value || value === '') {
      return '-';
    }
    if (this.isUserIdFormat(change, historyType)) {
      return this.getUserIdFormat(value, accountEmails);
    }
    if (this.isCustomerIdFormat(change, historyType)) {
      return this.getCustomerIdFormat(value, customers);
    }
    if (this.isContactIdFormat(change, historyType)) {
      return this.getContactIdFormat(value, contacts);
    }
    if (this.isDateFormat(change, historyType)) {
      return this.getDateFormat(value, historyType);
    }
    if (this.isPercentValue(change, historyType)) {
      return this.getPercentFormat(value);
    }
    if (this.isPriceValue(change, historyType)) {
      return this.getPriceFormat(value, formatNumberPipe);
    }
    if (this.isSourceOfAcquisition(change, historyType)) {
      return this.getSourceOfAcquisitionFormat(value, sourceOfAcquisition);
    }
    if (this.isSubSourceOfAcquisition(change, historyType)) {
      return this.getSubSourceOfAcquisitionFormat(value, sourceOfAcquisition);
    }
    if (this.isSubjectsOfInterest(change, historyType)) {
      return this.getSubjectsOfInterestFormat(value, subjectsOfInterest);
    }
    if (this.isExpenseType(change, historyType)) {
      return this.getExpenseType(value);
    }
    if (this.isStatus(change, historyType)) {
      return this.getStatus(value);
    }
    if (this.isActivityType(change, historyType)) {
      return this.getActivityType(value);
    }
    if (this.isBoolean(change, historyType)) {
      return this.getBoolean(value);
    }
    if (this.isTranslateValue(change, historyType)) {
      return this.getTranslateFormat(value, change.fieldName, historyType, translateService);
    }

    if (this.isJSONValue(change, historyType)) {
      const lightWeightHistoryFromJson = HistoryFormatUtil.lightWeightHistoryFromJson(change);
      const localValue: string = (newValue) ? lightWeightHistoryFromJson.newValue : lightWeightHistoryFromJson.oldValue;
      return this.getListFormat(localValue, subjectsOfInterest, translateService);
    }
    if (this.isAdditionalPhoneNumbers(change, historyType)) {
      return this.getAdditionalPhoneNumbersFormat(value);
    }
    if (this.isParticipants(change, historyType)) {
      return this.getParticipants(value);
    }
    return value;
  }

  private static isUserIdFormat(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.OPPORTUNITY:
        return ['ownerOneId', 'ownerTwoId', 'ownerThreeId', 'acceptanceUserId', 'selfAcceptance'].indexOf(change.fieldName) !== -1;
      case HistoryType.LEAD:
        return ['leadKeeperId', 'creator', 'convertedBy', 'acceptanceUserId', 'selfAcceptance'].indexOf(change.fieldName) !== -1;
      case HistoryType.EXPENSE:
        return ['responsibleId'].indexOf(change.fieldName) !== -1;
      case HistoryType.TASK:
        return ['owner'].indexOf(change.fieldName) !== -1;
      case HistoryType.CONTACT:
        return ['owner', 'creator'].indexOf(change.fieldName) !== -1;
      case HistoryType.CUSTOMER:
        return ['owner', 'creator'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static isCustomerIdFormat(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.LEAD:
        return ['convertedToCustomerId'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static isContactIdFormat(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.LEAD:
        return ['convertedToContactId'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static getUserIdFormat(value: string, accountEmails: AccountEmail[]): string {
    const result = accountEmails.find((account: AccountEmail) => account.id === +value);
    return (result) ? result.name + ' ' + result.surname : value;
  }

  private static getCustomerIdFormat(value: string, customers: CrmObject[]): string {
    const result = customers.find((customer: CrmObject) => customer.id === +value);
    return (result) ? result.label : value;
  }

  private static getContactIdFormat(value: string, contacts: CrmObject[]): string {
    const result = contacts.find((contact: CrmObject) => contact.id === +value);
    return (result) ? result.label : value;
  }

  private static getSourceOfAcquisitionFormat(value: string, sourcesOfAcquisition: DictionaryWord[]): string {
    const result = sourcesOfAcquisition.find((word: DictionaryWord) => word.id === +value);
    return (result) ? result.name : value;
  }

  private static getSubSourceOfAcquisitionFormat(value: string, sourcesOfAcquisition: DictionaryWord[]): string {
    const mainWord = sourcesOfAcquisition.find((word: DictionaryWord) => word.children.some(child => child.id === +value));

    if (!mainWord) {
      return value;
    }

    const result = mainWord.children.find((word: DictionaryWord) => word.id === +value);
    return (result) ? result.name : value;
  }

  private static isDateFormat(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.OPPORTUNITY:
        return ['finishDate'].indexOf(change.fieldName) !== -1;
      case HistoryType.LEAD:
        return ['convertedDateTime', 'created', 'updated'].indexOf(change.fieldName) !== -1;
      case HistoryType.EXPENSE:
        return ['date'].indexOf(change.fieldName) !== -1;
      case HistoryType.TASK:
        return ['dateTimeFrom'].indexOf(change.fieldName) !== -1;
      case HistoryType.CONTACT:
        return ['created', 'updated'].indexOf(change.fieldName) !== -1;
      case HistoryType.CUSTOMER:
        return ['created', 'updated'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static getDateFormat(value: string, historyType?: HistoryType): string {
    if (historyType && historyType === 'EXPENSE') {
      return (value) ? DateUtil.formatDate(value, false) : value;
    }
    return (value) ? DateUtil.formatDate(value, true) : value;
  }

  private static isPercentValue(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.OPPORTUNITY:
        return ['ownerOnePercentage', 'ownerTwoPercentage', 'ownerThreePercentage', 'probability'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static getPercentFormat(value: string): string {
    return (value) ? value + '%' : value;
  }

  private static isPriceValue(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.OPPORTUNITY:
        return ['amount'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static isSourceOfAcquisition(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.OPPORTUNITY:
        return ['sourceOfAcquisition'].indexOf(change.fieldName) !== -1;
      case HistoryType.LEAD:
        return ['sourceOfAcquisition'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static isSubSourceOfAcquisition(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.OPPORTUNITY:
        return ['subSourceOfAcquisition'].indexOf(change.fieldName) !== -1;
      case HistoryType.LEAD:
        return ['subSourceOfAcquisition'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static isAdditionalPhoneNumbers(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.CUSTOMER:
        return ['additionalPhones'].indexOf(change.fieldName) !== -1;
      case HistoryType.LEAD:
        return ['additionalPhones'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static isParticipants(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.TASK:
        return ['participants'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static getAdditionalPhoneNumbersFormat(value: string): string {
    const phones = JSON.parse(value);
    if (!phones.length) {
      return '-';
    }
    const formattedPhones = phones.map(phone => Object.values(phone).join(': '));
    return `[${formattedPhones.join(', ')}]`;
  }

  private static getParticipants(value: string): string {
    const participants = JSON.parse(value);
    if (!participants.length) {
      return '-';
    }
    const formattedParticipants = participants.map(participants => Object.values(participants).join(': '));
    return `[${formattedParticipants.join(', ')}]`;
  }

  private static getPriceFormat(value: string, formatNumberPipe: FormatNumberPipe): string {
    return (value) ? formatNumberPipe.transform(value) : value;
  }

  private static isTranslateValue(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.OPPORTUNITY:
        return ['type', 'finishResult', 'rejectionReason', 'priority'].indexOf(change.fieldName) !== -1;
      case HistoryType.LEAD:
        return ['salesOpportunity', 'priority', 'closeResult', 'rejectionReason'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static getTranslateFormat(value: string, fieldName: string,
                                    historyType: HistoryType, translateService: TranslateService): string {
    const translation: TranslateValue = this.translations.find(t => t.historyType === historyType && t.fieldName === fieldName);
    if (translation && value) {
      const convertedValue = translation.isLowerCase ? value.toLowerCase() : translation.isCamelCase ? this.snakeToCamel(value.toLowerCase()) : value;
      return translateService.instant(translation.translationStart + convertedValue);
    }
    return value;
  }

  private static lightWeightHistoryFromJson(change: HistoryChange): HistoryChange {
    try {
      let oldObject = JSON.parse(change.oldValue);
      let newObject = JSON.parse(change.newValue);
      const unchangedFieldKeys = [];
      Object.keys(newObject).forEach(key => {
        if (oldObject[key] === newObject[key]) {
          unchangedFieldKeys.push(key);
        }
      });
      oldObject = omit(oldObject, unchangedFieldKeys);
      newObject = omit(newObject, unchangedFieldKeys);
      change.oldValue = JSON.stringify(oldObject);
      change.newValue = JSON.stringify(newObject);
      return change;
    } catch {
      return change;
    }
  }

  private static isJSONValue(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.LEAD:
        return ['company'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static getListFormat(value: string, subjectsOfInterest: DictionaryWord[], translateService: TranslateService): string {
    try {
      const object = JSON.parse(value);
      const newValue = Object.keys(object).map((key) => {
        const newKey = translateService.instant(key);
        const newValue = object[key] ? translateService.instant(object[key]) : '-';
        return `${newKey}: ${newValue}`;
      });
      return newValue.join(', ');
    } catch {
      return value;
    }
  }

  private static snakeToCamel(value: string): string {
    return value.replace(
      /([-_][a-z])/g,
      (group) => group.toUpperCase()
        .replace('-', '')
        .replace('_', '')
    );
  }

  private static isSubjectsOfInterest(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.OPPORTUNITY:
        return ['subjectOfInterest'].indexOf(change.fieldName) !== -1;
      case HistoryType.LEAD:
        return ['subjectOfInterest'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static isExpenseType(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.EXPENSE:
        return ['type'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static isStatus(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.TASK:
        return ['state'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static isActivityType(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.TASK:
        return ['activityType'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static isBoolean(change: HistoryChange, historyType: HistoryType): boolean {
    switch (historyType) {
      case HistoryType.TASK:
        return ['priority', 'emailNotification'].indexOf(change.fieldName) !== -1;
    }
    return false;
  }

  private static getSubjectsOfInterestFormat(value: string, subjectsOfInterest: DictionaryWord[]): string {
    const result = subjectsOfInterest.find((word: DictionaryWord) => word.id === +value);
    return (result) ? result.name : value;
  }

  private static getExpenseType(expenseType: string): string {
    switch (expenseType) {
      case 'INVOICE':
        return 'Fatkura';
      case 'RECEIPT':
        return 'Paragon';
      case 'DELEGATION':
        return 'Delegacja';
      case 'OTHER':
        return 'Inne';
    }
  }

  private static getStatus(status: string): string {
    switch (status) {
      case 'CREATED':
        return 'utworzone';
      case 'IN_PROGRESS':
        return 'w trakcie';
      case 'SUSPENDED':
        return 'zawieszone';
      case 'FINISHED':
        return 'zakończone';
    }
  }

  private static getActivityType(activityType: string): string {
    switch (activityType) {
      case 'PHONE':
        return 'telefon';
      case 'MEETING':
        return 'spotkanie';
      case 'MAIL':
        return 'wiadomość';
      case 'TASK':
        return 'zadanie';
      default:
        return 'wydarzenie';
    }
  }

  private static getBoolean(value: string): string {
    if (value === 'true') {
      return 'tak';
    } else {
      return 'nie';
    }
  }
}

export class TranslateValue {
  historyType: HistoryType;
  fieldName: string;
  translationStart: string;
  isLowerCase?: boolean;
  isCamelCase?: boolean;
}
