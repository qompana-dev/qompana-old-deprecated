import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HistoryChange, HistoryType} from './history.model';
import {Observable} from 'rxjs';
import {UrlUtil} from '../../url.util';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  private historyUrl = `${UrlUtil.url}/crm-history-service/history`;

  constructor(private http: HttpClient) { }

  public getHistoryChanges(type: HistoryType, id: number): Observable<HistoryChange[]> {
    const url = `${this.historyUrl}/type/${type}/id/${id}/changes`;
    return this.http.get<HistoryChange[]>(url);
  }
}
