"use strict";
exports.__esModule = true;
exports.HistoryType = void 0;
var HistoryType;
(function (HistoryType) {
    HistoryType["CUSTOMER"] = "CUSTOMER";
    HistoryType["OPPORTUNITY"] = "OPPORTUNITY";
    HistoryType["LEAD"] = "LEAD";
})(HistoryType = exports.HistoryType || (exports.HistoryType = {}));
