"use strict";
exports.__esModule = true;
exports.TranslateValue = exports.HistoryFormatUtil = void 0;
var history_model_1 = require("./history.model");
var date_util_1 = require("../../util/date.util");
var HistoryFormatUtil = /** @class */ (function () {
    function HistoryFormatUtil() {
    }
    HistoryFormatUtil.isShowValue = function (change, historyType) {
        switch (historyType) {
            case history_model_1.HistoryType.LEAD:
                return !['avatar'].includes(change.fieldName);
            default:
                return true;
        }
    };
    ;
    HistoryFormatUtil.getValueFormat = function (change, newValue, historyType, accountEmails, formatNumberPipe, translateService, customers, contacts) {
        var value = (newValue) ? change.newValue : change.oldValue;
        if (!value || value === '') {
            return '-';
        }
        if (this.isUserIdFormat(change, historyType)) {
            return this.getUserIdFormat(value, accountEmails);
        }
        if (this.isCustomerIdFormat(change, historyType)) {
            return this.getCustomerIdFormat(value, customers);
        }
        if (this.isContactIdFormat(change, historyType)) {
            return this.getContactIdFormat(value, contacts);
        }
        if (this.isDateFormat(change, historyType)) {
            return this.getDateFormat(value);
        }
        if (this.isPercentValue(change, historyType)) {
            return this.getPercentFormat(value);
        }
        if (this.isPriceValue(change, historyType)) {
            return this.getPriceFormat(value, formatNumberPipe);
        }
        if (this.isTranslateValue(change, historyType)) {
            return this.getTranslateFormat(value, change.fieldName, historyType, translateService);
        }
        return value;
    };
    HistoryFormatUtil.isUserIdFormat = function (change, historyType) {
        switch (historyType) {
            case history_model_1.HistoryType.OPPORTUNITY:
                return ['ownerOneId', 'ownerTwoId', 'ownerThreeId'].indexOf(change.fieldName) !== -1;
            case history_model_1.HistoryType.LEAD:
                return ['leadKeeperId', 'creator', 'convertedBy'].indexOf(change.fieldName) !== -1;
        }
        return false;
    };
    HistoryFormatUtil.isCustomerIdFormat = function (change, historyType) {
        switch (historyType) {
            case history_model_1.HistoryType.LEAD:
                return ['convertedToCustomerId'].indexOf(change.fieldName) !== -1;
        }
        return false;
    };
    HistoryFormatUtil.isContactIdFormat = function (change, historyType) {
        switch (historyType) {
            case history_model_1.HistoryType.LEAD:
                return ['convertedToContactId'].indexOf(change.fieldName) !== -1;
        }
        return false;
    };
    HistoryFormatUtil.getUserIdFormat = function (value, accountEmails) {
        var result = accountEmails.find(function (account) { return account.id === +value; });
        return (result) ? result.name + ' ' + result.surname : value;
    };
    HistoryFormatUtil.getCustomerIdFormat = function (value, customers) {
        var result = customers.find(function (customer) { return customer.id === +value; });
        return (result) ? result.label : value;
    };
    HistoryFormatUtil.getContactIdFormat = function (value, contacts) {
        var result = contacts.find(function (contact) { return contact.id === +value; });
        return (result) ? result.label : value;
    };
    HistoryFormatUtil.isDateFormat = function (change, historyType) {
        switch (historyType) {
            case history_model_1.HistoryType.OPPORTUNITY:
                return ['finishDate'].indexOf(change.fieldName) !== -1;
            case history_model_1.HistoryType.LEAD:
                return ['convertedDateTime', 'created', 'updated'].indexOf(change.fieldName) !== -1;
        }
        return false;
    };
    HistoryFormatUtil.getDateFormat = function (value) {
        return (value) ? date_util_1.DateUtil.formatDate(value, true) : value;
    };
    HistoryFormatUtil.isPercentValue = function (change, historyType) {
        switch (historyType) {
            case history_model_1.HistoryType.OPPORTUNITY:
                return ['ownerOnePercentage', 'ownerTwoPercentage', 'ownerThreePercentage', 'probability'].indexOf(change.fieldName) !== -1;
        }
        return false;
    };
    HistoryFormatUtil.getPercentFormat = function (value) {
        return (value) ? value + '%' : value;
    };
    HistoryFormatUtil.isPriceValue = function (change, historyType) {
        switch (historyType) {
            case history_model_1.HistoryType.OPPORTUNITY:
                return ['amount'].indexOf(change.fieldName) !== -1;
        }
        return false;
    };
    HistoryFormatUtil.getPriceFormat = function (value, formatNumberPipe) {
        return (value) ? formatNumberPipe.transform(value) : value;
    };
    HistoryFormatUtil.isTranslateValue = function (change, historyType) {
        switch (historyType) {
            case history_model_1.HistoryType.OPPORTUNITY:
                return ['type', 'sourceOfAcquisition', 'finishResult', 'rejectionReason'].indexOf(change.fieldName) !== -1;
            case history_model_1.HistoryType.LEAD:
                return ['status', 'salesOpportunity'].indexOf(change.fieldName) !== -1;
        }
        return false;
    };
    HistoryFormatUtil.getTranslateFormat = function (value, fieldName, historyType, translateService) {
        var translation = this.translations.find(function (t) { return t.historyType === historyType && t.fieldName === fieldName; });
        if (translation && value) {
            var convertedValue = translation.isLowerCase ? value.toLowerCase() : translation.isCamelCase ? this.snakeToCamel(value.toLowerCase()) : value;
            return translateService.instant(translation.translationStart + convertedValue);
        }
        return value;
    };
    HistoryFormatUtil.snakeToCamel = function (value) {
        return value.replace(/([-_][a-z])/g, function (group) { return group.toUpperCase()
            .replace('-', '')
            .replace('_', ''); });
    };
    HistoryFormatUtil.translations = [
        { historyType: history_model_1.HistoryType.OPPORTUNITY, fieldName: 'type', translationStart: 'opportunity.form.types.', isLowerCase: true },
        { historyType: history_model_1.HistoryType.OPPORTUNITY, fieldName: 'sourceOfAcquisition',
            translationStart: 'opportunity.form.sourceOfAcquisitionTypes.', isLowerCase: true },
        { historyType: history_model_1.HistoryType.OPPORTUNITY, fieldName: 'finishResult',
            translationStart: 'opportunity.form.dialog.finishOpportunity.item.', isLowerCase: true },
        { historyType: history_model_1.HistoryType.OPPORTUNITY, fieldName: 'rejectionReason',
            translationStart: 'opportunity.form.dialog.rejectionReason.item.', isLowerCase: true },
        { historyType: history_model_1.HistoryType.LEAD, fieldName: 'status',
            translationStart: 'leads.form.leadStatus.', isCamelCase: true },
        { historyType: history_model_1.HistoryType.LEAD, fieldName: 'salesOpportunity',
            translationStart: 'leads.form.salesOpportunity.', isCamelCase: true }
    ];
    return HistoryFormatUtil;
}());
exports.HistoryFormatUtil = HistoryFormatUtil;
var TranslateValue = /** @class */ (function () {
    function TranslateValue() {
    }
    return TranslateValue;
}());
exports.TranslateValue = TranslateValue;
