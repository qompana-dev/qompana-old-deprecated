"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.HistoryTabComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var history_format_util_1 = require("./history-format.util");
var HistoryTabComponent = /** @class */ (function () {
    function HistoryTabComponent(historyService, personService, customerService, contactsService, translateService, formatNumberPipe) {
        this.historyService = historyService;
        this.personService = personService;
        this.customerService = customerService;
        this.contactsService = contactsService;
        this.translateService = translateService;
        this.formatNumberPipe = formatNumberPipe;
        this.changeList = [];
        this.userMap = new Map();
        this.customers = [];
        this.contacts = [];
        this.accountEmails = [];
        this.customerFields = [];
        this.contactFields = [];
        this.componentDestroyed = new rxjs_1.Subject();
        this.bigSize = false;
    }
    Object.defineProperty(HistoryTabComponent.prototype, "historyType", {
        set: function (type) {
            this.type = type;
            this.init();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(HistoryTabComponent.prototype, "objectId", {
        set: function (id) {
            this.id = id;
            this.init();
        },
        enumerable: false,
        configurable: true
    });
    HistoryTabComponent.prototype.ngOnInit = function () {
        this.getAccountEmailList();
    };
    HistoryTabComponent.prototype.ngOnChanges = function (simpleChange) {
        if (simpleChange.customerFields) {
            this.getCustomersList();
            this.getContactsList();
        }
    };
    HistoryTabComponent.prototype.ngOnDestroy = function () {
        this.componentDestroyed.next();
        this.componentDestroyed.complete();
    };
    HistoryTabComponent.prototype.init = function () {
        var _this = this;
        if (this.id && this.type) {
            this.historyService.getHistoryChanges(this.type, this.id)
                .pipe(operators_1.takeUntil(this.componentDestroyed))
                .subscribe(function (changeList) {
                _this.changeList = changeList;
                _this.getCustomersList();
                _this.getContactsList();
            });
        }
    };
    HistoryTabComponent.prototype.getAccountEmailList = function () {
        var _this = this;
        this.personService.getAccountEmails()
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (response) {
            _this.accountEmails = response;
            response.forEach(function (account) {
                _this.userMap.set(account.email, account.name + ' ' + account.surname);
            });
        });
    };
    HistoryTabComponent.prototype.getContactsList = function () {
        var _this = this;
        var contactsIds = this.changeList
            .filter(function (item) { return _this.contactFields.includes(item.fieldName) && item.newValue; })
            .map(function (contact) { return +contact.newValue; });
        this.contactsService.getContactsByIds(contactsIds)
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (contacts) {
            _this.contacts = contacts;
        });
    };
    HistoryTabComponent.prototype.getCustomersList = function () {
        var _this = this;
        var customersIds = this.changeList
            .filter(function (item) { return _this.customerFields.includes(item.fieldName) && item.newValue; })
            .map(function (customer) { return +customer.newValue; });
        this.customerService.getCustomersByIds(customersIds)
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (customers) {
            _this.customers = customers;
        });
    };
    HistoryTabComponent.prototype.getValue = function (change, newValue) {
        return history_format_util_1.HistoryFormatUtil.getValueFormat(change, newValue, this.type, this.accountEmails, this.formatNumberPipe, this.translateService, this.customers, this.contacts);
    };
    HistoryTabComponent.prototype.isShowValue = function (change) {
        return history_format_util_1.HistoryFormatUtil.isShowValue(change, this.type);
    };
    __decorate([
        core_1.Input()
    ], HistoryTabComponent.prototype, "customerFields");
    __decorate([
        core_1.Input()
    ], HistoryTabComponent.prototype, "contactFields");
    __decorate([
        core_1.Input()
    ], HistoryTabComponent.prototype, "historyType");
    __decorate([
        core_1.Input()
    ], HistoryTabComponent.prototype, "objectId");
    __decorate([
        core_1.Input()
    ], HistoryTabComponent.prototype, "bigSize");
    HistoryTabComponent = __decorate([
        core_1.Component({
            selector: 'app-history-tab',
            templateUrl: './history-tab.component.html',
            styleUrls: ['./history-tab.component.scss']
        })
    ], HistoryTabComponent);
    return HistoryTabComponent;
}());
exports.HistoryTabComponent = HistoryTabComponent;
