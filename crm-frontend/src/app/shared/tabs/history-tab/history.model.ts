export enum HistoryType {
  CUSTOMER = 'CUSTOMER',
  OPPORTUNITY = 'OPPORTUNITY',
  LEAD = 'LEAD',
  EXPENSE = 'EXPENSE',
  CONTACT = 'CONTACT',
  TASK = 'TASK'
}

export interface History {
  id: number;
  fieldName: string;
  value: string;
  email: string;
  modifiedTime: Date;
  deleted: boolean;
}

export interface HistoryChange {
  id: number;
  fieldName: string;
  oldValue: string;
  newValue: string;
  email?: string;
  modifiedTime?: Date;
  deleted: boolean;
  version: number;
}
