import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from "@angular/core";
import { Router } from "@angular/router";
import { MailService } from "../../../mail/mail.service";
import { CrmObject, CrmObjectType } from "../../model/search.model";
import {
  MailStorageDto,
  MailStorageSortStrategy,
} from "../../../mail/mail.model";
import { Observable, Subject } from "rxjs";
import {
  InfinityScrollUtil,
  InfinityScrollUtilInterface,
} from "../../infinity-scroll.util";
import { takeUntil } from "rxjs/operators";
import { FilterStrategy } from "../../model";
import { Page, PageRequest } from "../../pagination/page.model";
import {
  NotificationService,
  NotificationType,
} from "../../notification.service";
import { Sort } from "@angular/material";
import { DeleteDialogData } from "../../dialog/delete/delete-dialog.component";
import { DeleteDialogService } from "../../dialog/delete/delete-dialog.service";

@Component({
  selector: "app-email-extend-tab",
  templateUrl: "./email-expand-tab.component.html",
  styleUrls: ["./email-expand-tab.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class EmailExpandTabComponent
  implements OnInit, OnDestroy, InfinityScrollUtilInterface {
  activeSort: MailStorageSortStrategy = MailStorageSortStrategy.receivedDate;
  sortStrategy = MailStorageSortStrategy;
  infinityScroll: InfinityScrollUtil;

  selectedMail: MailStorageDto;
  parentSelectedIndex: number;
  selectedMailIndex: number;

  highlightedMailId: number;
  highlightedChaildMailId: number;

  @Input() emailExpandLabel = "-";
  @Input() separatePreview = false;
  openRelatedMailIds: number[] = [];

  private componentDestroyed: Subject<void> = new Subject();

  private _crmObject: CrmObject;

  private detachEmailDialog: Partial<DeleteDialogData> = {
    title: "tabs.attachedEmails.detachDialog.title",
    description: "tabs.attachedEmails.detachDialog.description",
    confirmButton: "tabs.attachedEmails.detachDialog.yes",
    cancelButton: "tabs.attachedEmails.detachDialog.no",
  };

  @Input() set crmObject(crmObject: CrmObject) {
    if (crmObject) {
      this._crmObject = crmObject;
    }
  }

  constructor(
    private router: Router,
    private mailService: MailService,
    private notificationService: NotificationService,
    private deleteDialogService: DeleteDialogService
  ) {}

  ngOnInit() {
    if (this._crmObject) {
      this.initInfinityScroll();
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  getItemObservable(
    selection: FilterStrategy,
    pageRequest: PageRequest
  ): Observable<Page<MailStorageDto>> {
    return this.mailService.getMailHeadersPage(
      pageRequest,
      this._crmObject.id,
      this._crmObject.type
    );
  }

  sort(sortStrategy: MailStorageSortStrategy): void {
    const sort = this.getSort(sortStrategy);
    this.infinityScroll.onSortChange(sort);
    this.activeSort = sortStrategy;
    this.openRelatedMailIds = [];
  }

  private getSort(sortStrategy: MailStorageSortStrategy): Sort {
    if (sortStrategy === MailStorageSortStrategy.receivedDate) {
      return { active: "receivedDate", direction: "desc" };
    }
    if (sortStrategy === MailStorageSortStrategy.subject) {
      return { active: "subject", direction: "asc" };
    }
    if (sortStrategy === MailStorageSortStrategy.from) {
      return { active: "from", direction: "asc" };
    }
  }

  private initInfinityScroll(): void {
    this.selectedMail = null;
    this.selectedMailIndex = null;

    this.infinityScroll = new InfinityScrollUtil(
      this,
      this.componentDestroyed,
      this.notificationService,
      "mail.notification.getMailHeaderListError",
      "receivedDate,desc"
    );
    this.infinityScroll.onScrollInit$
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe();
  } //--------------------------

  onClickMail(
    mail: MailStorageDto,
    mailIndex: number,
    parentMail: MailStorageDto,
    parentMailIndex: number
  ) {
    if (
      mail.id !== this.highlightedMailId &&
      mail.id !== this.highlightedChaildMailId
    ) {
      if (parentMail) {
        this.highlightedMailId = parentMail.id;
        this.highlightedChaildMailId = mail.id;
      } else {
        this.highlightedMailId = mail.id;
        this.highlightedChaildMailId = undefined;
      }
    } else {
      this.selectedMail = mail;
      this.selectedMailIndex = mailIndex;
      this.parentSelectedIndex = undefined;
      if (parentMail) {
        this.parentSelectedIndex = parentMailIndex;
      }
    }
  }

  clearSelectMail() {
    this.selectedMail = undefined;
    this.selectedMailIndex = undefined;
    this.parentSelectedIndex = undefined;
  }

  changeSelectedMail(shift: number = 0) {
    const emailsList = this.infinityScroll.itemPage.content;
    const newIndex = this.selectedMailIndex + shift;

    if (this.parentSelectedIndex === undefined) {
      const index =
        newIndex < 0
          ? emailsList.length - 1
          : newIndex >= emailsList.length
          ? 0
          : newIndex;

      this.selectedMail = emailsList[index];
      this.selectedMailIndex = index;
    } else {
      const childsList = emailsList[this.parentSelectedIndex].relatedMails;
      const index =
        newIndex < 0
          ? childsList.length - 1
          : newIndex >= childsList.length
          ? 0
          : newIndex;

      this.selectedMail = childsList[index];
      this.selectedMailIndex = index;
    }
  }

  changeStateRelatedMail(id: number) {
    if (this.openRelatedMailIds.includes(id)) {
      this.openRelatedMailIds = this.openRelatedMailIds.filter(
        (_id) => _id !== id
      );
    } else {
      this.openRelatedMailIds = [...this.openRelatedMailIds, id];
    }
  }

  toMailPage() {
    this.router.navigate(["mail"], {
      state: {
        id: this._crmObject.id,
        type: this._crmObject.type,
        label: this.emailExpandLabel,
      },
    });
  }

  getDetachMessageKey(): string {
    switch (this._crmObject && this._crmObject.type) {
      case CrmObjectType.lead:
        return "LeadDetailsComponent.detachEmail";
      case CrmObjectType.opportunity:
        return "OpportunityDetailsComponent.detachEmail";
      case CrmObjectType.contact:
        return "ContactDetailsComponent.detachEmail";
      default:
        return "OpportunityDetailsComponent.detachEmail";
    }
  }

  tryDetachEmail(mailStorageId: number) {
    this.detachEmailDialog.onConfirmClick = () =>
      this.detachEmail(mailStorageId);
    this.deleteDialogService.open(this.detachEmailDialog);
  }

  private detachEmail(mailStorageId: number) {
    this.mailService
      .detachMessage(this._crmObject.id, mailStorageId, this._crmObject.type)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          // this.emailList = this.emailList.filter(header => header.id !== mailStorageId);
          this.notificationService.notify(
            "tabs.attachedEmails.notification.mailDetached",
            NotificationType.SUCCESS
          );
          this.initInfinityScroll();
        },
        () => {
          this.notificationService.notify(
            "tabs.attachedEmails.notification.detachEmailError",
            NotificationType.ERROR
          );
          this.initInfinityScroll();
        }
      );
  }
}
