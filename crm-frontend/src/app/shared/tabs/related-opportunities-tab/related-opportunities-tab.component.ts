import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {OpportunityService} from '../../../opportunity/opportunity.service';
import {OpportunityListService} from '../../../opportunity/opportunity-list/opportunity-list.service';
import {Observable, Subject} from 'rxjs';
import {OpportunityOnList, OpportunitySave} from '../../../opportunity/opportunity.model';
import {FilterStrategy} from '../../model';
import {Page, PageRequest} from '../../pagination/page.model';
import {finalize, map, startWith, takeUntil} from 'rxjs/operators';
import {ErrorTypes, NotificationService, NotificationType} from '../../notification.service';
import {Sort} from '@angular/material/sort';
import {Router} from '@angular/router';
import {AuthService} from '../../../login/auth/auth.service';
import {FormControl} from '@angular/forms';
import {SliderComponent} from '../../slider/slider.component';
import {TranslateService} from '@ngx-translate/core';
import {OpportunityDialogsConfig} from '../../../opportunity/opportunity-list/opportunity-list.dialogs';
import {FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import {ConfirmDialogService} from '../../dialog/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-related-opportunities-tab',
  templateUrl: './related-opportunities-tab.component.html',
  styleUrls: ['./related-opportunities-tab.component.scss']
})
export class RelatedOpportunitiesTabComponent implements OnInit {
  columns = [
    'name',
    'amount',
    'finishDate',
    'actions'
  ];
  private pageOnList = 10;
  pageRequest: PageRequest = {
    page: '0',
    size: this.pageOnList + '',
    sort: 'updated,desc'
  };
  loadMoreOpportunityHide = true;
  totalElements = 0;
  search = '';
  loadingFinished = true;
  selectedFilter: FilterStrategy;

  isSearched = false;
  searchControl = new FormControl();

  private componentDestroyed: Subject<void> = new Subject();
  private fetchOpportunities: Subject<void> = new Subject();
  selectedOpportunity: number;
  opportunityPage: Page<OpportunityOnList>;
  filteredColumns$: Observable<string[]>;

  initOpportunityDefaultValues: boolean;
  @ViewChild('addSlider') addSlider: SliderComponent;
  @ViewChild('editSlider') editSlider: SliderComponent;
  opportunityToEdit: OpportunitySave;

  @Input() objectId: string;
  objectType: string;

  @Input()
  set filterStrategy(type: string) {
    this.objectType = type;
    switch (type) {
      case 'CUSTOMER':
        this.selectedFilter = FilterStrategy.CUSTOMER;
        break;
      case 'CONTACT':
        this.selectedFilter = FilterStrategy.CONTACT;
        break;
    }
  }

  private readonly dialogsConfig = new OpportunityDialogsConfig(this.translateService);

  private readonly opportunityErrorTypes: ErrorTypes = {
    base: 'tabs.relatedOpportunities.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'opportunityNotFound',
      },
      {
        code: FORBIDDEN,
        text: 'noPermission',
      },
    ],
  };

  constructor(private opportunityService: OpportunityService,
              private opportunityListService: OpportunityListService,
              private notificationService: NotificationService,
              private authService: AuthService,
              private translateService: TranslateService,
              private confirmDialogService: ConfirmDialogService,
              private $router: Router) {
  }

  ngOnInit(): void {
    this.getOpportunities(this.selectedFilter);
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
  }
  // Get Opportunities
  private getOpportunities(selection: FilterStrategy): void {
    this.fetchOpportunities.next();
    this.loadingFinished = false;
    this.loadMoreOpportunityHide = true;
    this.selectedOpportunity = null;
    this.opportunityService
      .getOpportunities(selection, this.pageRequest, this.search, this.objectId)
      .pipe(
        takeUntil(this.fetchOpportunities),
        finalize(() => (this.loadingFinished = true)))
      .subscribe(
        (page) => this.handleOpportunitiesSuccess(page),
        () => this.notificationService.notify('tabs.relatedOpportunities.notification.listError', NotificationType.ERROR)
      );
  }

  private handleOpportunitiesSuccess(page: any): void {
    this.totalElements = page.totalElements;
    this.opportunityPage = page;
    if (+this.pageRequest.size < this.totalElements) {
      this.loadMoreOpportunityHide = false;
    }
  }

  public getOpportunityProgress(opportunity: OpportunityOnList): number {
    return opportunity.maxStatus ? (opportunity.status / opportunity.maxStatus) * 100 : 0;
  }

  // Opportunities list events
  public onSortChange($event: Sort): void {
    this.pageRequest.sort =  $event.direction ? $event.active + ',' + $event.direction : 'updated,desc';
    this.getOpportunities(this.selectedFilter);
  }

  public loadMoreOpportunity(): void {
    this.pageRequest.size = this.pageRequest.size + this.pageOnList + '';
    this.getOpportunities(this.selectedFilter);
  }

  // Columns auth
  private getFilteredColumns(): string[] {
    return this.columns.filter(
      (item) =>
        !this.authService.isPermissionPresent('OpportunityListComponent.' +
          item.replace('customer.name', 'customerName') +
          'Column', 'i') || item === 'actions'
    );
  }

  // Search opportunity
  searchOpportunities(): void {
    const searchValue = this.searchControl.value.trim();
    if (searchValue && searchValue.length > 0 && this.search !== searchValue) {
      this.isSearched = true;
      this.search = searchValue;
      this.getOpportunities(this.selectedFilter);
    }
  }

  clearSearch(): void {
    this.search = '';
    this.searchControl.setValue('');
    this.isSearched = false;
    this.pageRequest.size = this.pageOnList + '';
    this.getOpportunities(this.selectedFilter);
  }

  // Redirect to opportunity
  public goToOpportunityDetails(id: number): void {
    this.$router.navigate(['/opportunity', id]);
  }

  // Add opportunity slider
  openAddOpportunity(): void {
    this.initOpportunityDefaultValues = true;
    setTimeout(() => (this.initOpportunityDefaultValues = null), 500);
    this.addSlider.open();
  }

  public handleOpportunityAdded(): void {
    this.getOpportunities(this.selectedFilter);
    this.addSlider.close();
  }

  // Edit opportunity details redirect
  openEditOpportunity(event: Event, opportunityId: number): void {
    event.preventDefault();
    event.stopPropagation();
    this.opportunityService
      .getOpportunityForEditForSlider(opportunityId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (opportunity) => {
          this.opportunityToEdit = opportunity;
          this.editSlider.open();
        },
        (err) => this.notificationService.notifyError(this.opportunityErrorTypes, err.status)
      );
  }

  handleOpportunityEdited(): void {
    setTimeout(() => (this.opportunityToEdit = null), 50);
    this.editSlider.close();
  }

  // Delete opportunity slider
  openDeleteOpportunity(event: Event, opportunityId: number): void {
    event.preventDefault();
    event.stopPropagation();
    this.selectedOpportunity = opportunityId;
    const dialogData = this.dialogsConfig.deleteOpportunites(this.deleteOpportunity.bind(this));
    this.confirmDialogService.open(dialogData);
  }

  public deleteOpportunity(): void {
    if (!this.selectedOpportunity) {
      return;
    }
    this.opportunityService
      .removeOpportunity(this.selectedOpportunity)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.getOpportunities(this.selectedFilter);
          this.notificationService.notify('tabs.relatedOpportunities.notification.deleteSuccess', NotificationType.SUCCESS);
        },
        (err) => this.notificationService.notifyError(this.opportunityErrorTypes, err.status)
      );
  }
}
