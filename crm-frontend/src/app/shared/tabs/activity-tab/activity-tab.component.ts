import { Router } from '@angular/router';
import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CrmObjectType } from '../../model/search.model';
import { Subject } from 'rxjs';
import {
  EventInfo,
  TaskActivityType,
  TaskAssociation,
} from '../../../calendar/task.model';
import { SliderComponent } from '../../slider/slider.component';
import { ACTIVITIES_LIST, ACTIVITIES_TABS } from './activity-tab.config';

@Component({
  selector: 'app-activity-tab',
  templateUrl: './activity-tab.component.html',
  styleUrls: ['./activity-tab.component.scss'],
})
export class ActivityTabComponent implements OnInit, OnDestroy {
  id: number;
  type: CrmObjectType;
  updatedEventInfo: EventInfo;
  selectedActivityType: TaskActivityType;
  activitiesTypeList = ACTIVITIES_LIST;
  activitiesTabs = ACTIVITIES_TABS;
  initAssociation: TaskAssociation;
  refreshSubject: Subject<void> = new Subject<void>();
  @ViewChild('addActivity') addActivity: SliderComponent;

  @Input()
  set objectType(type: CrmObjectType) {
    this.type = type;
    this.init();
  }

  @Input()
  set objectId(id: number) {
    this.id = id;
    this.init();
  }

  @Input() readonly = false;

  selectedTabIndex = 0;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {}

  init(): void {
    if (this.id && this.type) {
      this.setInitAssociation();
    }
  }

  toCalendarPage(): void {
    this.router.navigate(['calendar'], {
      state: {
        type: this.type,
        id: this.id,
      },
    });
  }

  openAddSlider($event: any, taskActivityType?: TaskActivityType): void {
    if (this.readonly) {
      return;
    }
    if ($event.date) {
      this.updatedEventInfo = new EventInfo($event.date, false);
    } else if ($event.day) {
      this.updatedEventInfo = new EventInfo($event.day.date, true);
    }
    this.selectedActivityType = taskActivityType;
    this.addActivity.open();
  }

  setInitAssociation(): void {
    this.initAssociation = undefined;
    this.initAssociation = {
      objectType: this.type,
      objectId: this.id,
      disabled: true,
    };
  }

  refreshEvent(): void {
    this.refreshSubject.next();
  }
}
