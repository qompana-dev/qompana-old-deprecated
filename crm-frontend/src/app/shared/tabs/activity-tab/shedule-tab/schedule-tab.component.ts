import { Component, Input, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { Subject, Subscription } from "rxjs";
import { SliderComponent } from "../../../slider/slider.component";
import {
  TaskActivityType,
  TaskAssociation,
  TaskDto,
  TaskPriority,
  TaskTimeCategoryEnum,
} from "../../../../calendar/task.model";
import { MapViewMarker } from "../../../../map/map-view/map-view.model";
import { Page, PageRequest } from "../../../pagination/page.model";
import { DeleteDialogData } from "../../../dialog/delete/delete-dialog.component";
import { CalendarService } from "../../../../calendar/calendar.service";
import {
  NotificationService,
  NotificationType,
} from "../../../notification.service";
import { DeleteDialogService } from "../../../dialog/delete/delete-dialog.service";

import { CrmObjectType } from "../../../model/search.model";
import { takeUntil, throttleTime } from "rxjs/operators";
import { InfiniteScrollUtil } from "../../../pagination/infinite-scroll.util";

@Component({
  selector: "app-schedule-tab",
  templateUrl: "./schedule-tab.component.html",
  styleUrls: ["./schedule-tab.component.scss"],
})
export class ScheduleTabComponent implements OnInit, OnDestroy {
  @Input() public objectId: number;
  @Input() public objectType: CrmObjectType;
  @Input() public taskTimeCategory: TaskTimeCategoryEnum;
  @Input() public refreshSubject: Subject<void>;

  @ViewChild("editActivity") editActivity: SliderComponent;
  @ViewChild("mapSlider") mapSlider: SliderComponent;

  private componentDestroyed: Subject<void> = new Subject();

  public activities: TaskDto[] = [];
  public pagination = {
    numberOfElements: 0,
    totalElements: 0,
    page: 0,
  };
  initAssociation: TaskAssociation;
  public openedActivities = [];
  public activityPriority = TaskPriority;
  public activityTypeEnum = TaskActivityType;

  private refreshSubscription: Subscription;

  loading = false;

  markers: MapViewMarker[];
  mapCenter: number[];
  showMap = false;

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: "tabs.activity.dialog.remove.title",
    description: "tabs.activity.dialog.remove.description",
    noteFirst: "tabs.activity.dialog.remove.noteFirstPart",
    noteSecond: "tabs.activity.dialog.remove.noteSecondPart",
    confirmButton: "tabs.activity.dialog.remove.buttonYes",
    cancelButton: "tabs.activity.dialog.remove.buttonNo",
  };

  constructor(
    private calendarService: CalendarService,
    private notificationService: NotificationService,
    private deleteDialogService: DeleteDialogService
  ) {}

  ngOnInit(): void {
    this.initActivities();
    this.setInitAssociation();
    this.refreshSubscription = this.refreshSubject.subscribe(() => {
      this.refreshEvent();
    });
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
    this.refreshSubscription.unsubscribe();
  }

  openActivity(id) {
    if (!this.openedActivities.includes(id)) {
      this.openedActivities = [...this.openedActivities, id];
    }
  }

  public onScroll(e: any): void {
    if (
      InfiniteScrollUtil.shouldGetMoreData(e) &&
      this.pagination.numberOfElements < this.pagination.totalElements
    ) {
      const nextPage = this.pagination.page + 1;
      this.getMoreActivities({
        page: "" + nextPage,
        size: InfiniteScrollUtil.ELEMENTS_ON_PAGE,
        sort: "dateTimeFrom,asc",
      });
    }
  }

  private initActivities(
    pageRequest: PageRequest = {
      page: "0",
      size: InfiniteScrollUtil.ELEMENTS_ON_PAGE,
      sort: "dateTimeFrom,asc",
    }
  ): void {
    this.loading = true;
    this.activities = [];
    this.calendarService
      .getActivitiesByAssociationIdAndTypeAndTimeCategory(
        this.objectId,
        this.objectType,
        this.taskTimeCategory,
        pageRequest
      )
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (page) => this.handleInitSuccess(page),
        () => this.notifyError()
      );
  }

  public refreshEvent() {
    this.initActivities();
  }

  private getMoreActivities(pageRequest: PageRequest): void {
    this.loading = true;
    this.calendarService
      .getActivitiesByAssociationIdAndTypeAndTimeCategory(
        this.objectId,
        this.objectType,
        this.taskTimeCategory,
        pageRequest
      )
      .pipe(takeUntil(this.componentDestroyed), throttleTime(500))
      .subscribe(
        (page) => this.handleInitSuccess(page),
        () => this.notifyError()
      );
  }

  private handleInitSuccess(page: any): void {
    const { totalElements, numberOfElements, number, content } = page;
    this.loading = false;

    this.pagination.totalElements = totalElements;
    this.pagination.numberOfElements += numberOfElements;
    this.pagination.page = number;
    this.activities = this.activities.concat(content);
  }

  private notifyError(): void {
    this.loading = false;

    this.notificationService.notify(
      "tabs.activity.notification.unknownError",
      NotificationType.ERROR
    );
  }

  public tryRemoveActivity(activity: TaskDto): void {
    this.deleteDialogData.onConfirmClick = () => this.removeActivity(activity);
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private removeActivity(activity: TaskDto): void {
    this.calendarService.deleteEvent(activity.id).subscribe(
      () => this.initActivities(),
      () =>
        this.notificationService.notify(
          "tabs.activity.notification.deleteFailed",
          NotificationType.ERROR
        )
    );
  }

  public editSelectedActivity(activity: TaskDto): void {
    this.calendarService.taskEdited.emit(activity.id);
    this.editActivity.open();
  }

  public setInitAssociation(): void {
    this.initAssociation = undefined;
    this.initAssociation = {
      objectType: this.objectType,
      objectId: this.objectId,
      disabled: true,
    };
  }

  public openMapForActivity(task: TaskDto): void {
    const marker: MapViewMarker = {
      latitude: +task.latitude,
      longitude: +task.longitude,
      id: "activity",
    };
    this.markers = [marker];
    this.mapCenter = [+task.longitude, +task.latitude];
    this.mapSlider.open();
    setTimeout(() => (this.showMap = true), 500);
  }
}
