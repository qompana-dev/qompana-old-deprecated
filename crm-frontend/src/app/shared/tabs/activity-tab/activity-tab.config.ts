import { TaskActivityType } from "src/app/calendar/task.model";

export enum ACTIVITIES_ICONS {
  EVENT = "event",
  PHONE = "phone",
  TASK = "task",
  MEETING = "meeting",
  MAIL = "mail",
}

export enum TASK_TIME_CATEGORY {
  ALL = "ALL",
  LATEST = "LATEST",
  OLD = "OLD",
  PLANNED = "PLANNED",
  COMPLETED = "COMPLETED",
}

export const ACTIVITIES_LIST = [
  {
    icon: ACTIVITIES_ICONS.PHONE,
    type: TaskActivityType.PHONE,
  },
  {
    icon: ACTIVITIES_ICONS.TASK,
    type: TaskActivityType.TASK,
  },
  {
    icon: ACTIVITIES_ICONS.MEETING,
    type: TaskActivityType.MEETING,
  },
  {
    icon: ACTIVITIES_ICONS.MAIL,
    type: TaskActivityType.MAIL,
  },
  {
    icon: ACTIVITIES_ICONS.EVENT,
  },
];

export const ACTIVITIES_TABS = [
  {
    taskTimeCategory: TASK_TIME_CATEGORY.ALL,
    label: "leads.details.tabs.all",
  },
  {
    taskTimeCategory: TASK_TIME_CATEGORY.LATEST,
    label: "leads.details.tabs.today",
  },
  {
    taskTimeCategory: TASK_TIME_CATEGORY.OLD,
    label: "leads.details.tabs.expired",
  },
  {
    taskTimeCategory: TASK_TIME_CATEGORY.PLANNED,
    label: "leads.details.tabs.future",
  },
  {
    taskTimeCategory: TASK_TIME_CATEGORY.COMPLETED,
    label: "leads.details.tabs.done",
  },
];
