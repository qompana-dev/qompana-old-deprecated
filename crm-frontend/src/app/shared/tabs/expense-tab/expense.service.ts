import {Injectable} from '@angular/core';
import {CrmObject} from '../../model/search.model';
import {FilterStrategy} from '../../model';
import {EmptyPage, Page, PageRequest} from '../../pagination/page.model';
import {Observable, of} from 'rxjs';
import {Expense, TotalCost} from './expenses.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {UrlUtil} from '../../url.util';

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {

  constructor(private http: HttpClient) {
  }

  getExpenses(pageRequest: PageRequest, strategy: FilterStrategy, crmObject: CrmObject): Observable<Page<Expense>> {
    if (!crmObject) {
      return of(new EmptyPage());
    }
    const params = new HttpParams({fromObject: pageRequest as any});
    const url = `${UrlUtil.url}/crm-business-service/expense/type/${crmObject.type}/id/${crmObject.id}`;
    return this.http.get<Page<Expense>>(url, {params});
  }

  createExpense(expense: Expense): Observable<Expense> {
    const url = `${UrlUtil.url}/crm-business-service/expense`;
    return this.http.post<Expense>(url, expense);
  }

  getExpense(expenseId: number): Observable<Expense> {
    const url = `${UrlUtil.url}/crm-business-service/expense/${expenseId}`;
    return this.http.get<Expense>(url);
  }

  detachActivityFormExpense(id: number): Observable<Expense> {
    const url = `${UrlUtil.url}/crm-business-service/expense/id/${id}/activity`;
    return this.http.delete<Expense>(url);


  }

  updateExpense(expense: Expense): Observable<Expense> {
    const url = `${UrlUtil.url}/crm-business-service/expense/${expense.id}`;
    return this.http.put<Expense>(url, expense);
  }

  getAttachedToLeadActivityTypes(crmObject: CrmObject): Observable<Map<number, string>> {
    const url = `${UrlUtil.url}/crm-task-service/tasks/activity/types/associated-with/type/${crmObject.type}/id/${crmObject.id}`;
    return this.http.get<Map<number, string>>(url);

  }

  getTotalCost(crmObject: CrmObject, currency: string = "PLN"): Observable<TotalCost> {
    const url = `${UrlUtil.url}/crm-business-service/expense/cost/type/${crmObject.type}/id/${crmObject.id}?currency=${currency}`;
    return this.http.get<TotalCost>(url);

  }
}
