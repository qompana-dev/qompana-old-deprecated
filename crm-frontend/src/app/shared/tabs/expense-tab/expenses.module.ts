import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExpenseListComponent} from './expense-list/expense-list.component';
import {SharedModule} from '../../shared.module';
import {ExpenseSliderModule} from './expense-slider/expense-slider.module';
import {CalendarsModule} from '../../../calendar/calendars.module';
import {TaskSliderModule} from "../../../calendar/task-slider/task-slider.module";

@NgModule({
  declarations: [ExpenseListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ExpenseSliderModule,
    CalendarsModule,
    TaskSliderModule
  ],
  exports: [
    ExpenseListComponent,
  ],
})
export class ExpensesModule {
}
