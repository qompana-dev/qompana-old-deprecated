export class Expense {
  id: number;
  name: string;
  description: string;
  date: string;
  amount: number;
  currency: string;
  type: 'INVOICE' | 'RECEIPT' | 'DELEGATION' | 'OTHER';
  state: 'TO_BE_ACCEPTED' | 'ACCEPTED' | 'REJECTED';
  opportunityId: number;
  leadId: number;
  creatorId: number;
  responsibleId: number;
  activityId: number;
}

export class TotalCost {
  totalCost: number;
  mainCurrency: string;
  costInCurrencies: Map<string, number>;
}

