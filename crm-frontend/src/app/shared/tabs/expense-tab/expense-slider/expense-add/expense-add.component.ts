import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {SliderComponent} from '../../../../slider/slider.component';
import {CrmObject} from '../../../../model/search.model';
import {ExpenseFormComponent} from '../expense-form/expense-form.component';
import {ExpenseService} from '../../expense.service';
import {Expense} from '../../expenses.model';
import {ErrorTypes, NotificationService, NotificationType} from '../../../../notification.service';
import {BAD_REQUEST, NOT_FOUND} from 'http-status-codes';

@Component({
  selector: 'app-expense-add',
  templateUrl: './expense-add.component.html',
  styleUrls: ['./expense-add.component.scss']
})
export class ExpenseAddComponent implements OnInit {

  @Input() object: CrmObject;
  @Input() slider: SliderComponent;
  @Output() expenseAdded: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild(ExpenseFormComponent) private expenseFormComponent: ExpenseFormComponent;

  readonly addExpenseError: ErrorTypes = {
    base: 'expenses.add.notifications.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'badRequest'
      },
      {
        code: NOT_FOUND,
        text: 'entityNotFound'
      }
    ]
  };

  constructor(private expenseService: ExpenseService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
  }

  closeSlider(): void {
    this.slider.close();
  }

  submit(): void {
    if (!this.expenseFormComponent.isFormValid()) {
      this.expenseFormComponent.markAsTouched();
      return;
    }

    const expense: Expense = this.expenseFormComponent.getExpense();
    this.expenseService.createExpense(expense)
      .subscribe(
        () => this.handleAddSuccess(),
        (err) => this.notificationService.notifyError(this.addExpenseError, err.status)
      );
  }

  private handleAddSuccess(): void {
    this.notificationService.notify('expenses.add.notifications.addedSuccessfully', NotificationType.SUCCESS);
    this.expenseAdded.emit();
    this.closeSlider();
  }
}
