import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CrmObject, CrmObjectType} from '../../../../model/search.model';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {FormUtil} from '../../../../form.util';
import {Expense} from '../../expenses.model';
import {whenSliderClosed, whenSliderOpen} from '../../../../util/slider.util';
import {SliderService} from '../../../../slider/slider.service';
import {Subject} from 'rxjs';
import {applyPermission} from '../../../../permissions-utils';
import {AuthService} from '../../../../../login/auth/auth.service';
import {SliderComponent} from '../../../../slider/slider.component';
import {TaskDto} from '../../../../../calendar/task.model';
import {GlobalConfigService} from '../../../../../global-config/global-config.service';
import {GlobalConfiguration} from '../../../../../global-config/global-config.model';
import {CalendarService} from '../../../../../calendar/calendar.service';
import {NotificationService, NotificationType} from '../../../../notification.service';
import * as isNil from 'lodash/isNil';
import {takeUntil} from 'rxjs/operators';
import {FormValidators} from '../../../../form/form-validators';
import {AccountEmail} from "../../../../../person/person.model";
import {PersonService} from "../../../../../person/person.service";


@Component({
  selector: 'app-expense-form',
  templateUrl: './expense-form.component.html',
  styleUrls: ['./expense-form.component.scss']
})
export class ExpenseFormComponent implements OnInit, OnDestroy {

  @Input() object: CrmObject;
  // tslint:disable-next-line:variable-name
  _readOnly = false;
  @Input() update = false;
  expenseForm: FormGroup;
  accountEmails: AccountEmail[];
  currencies = [];
  expenseTypes = ['INVOICE', 'RECEIPT', 'DELEGATION', 'OTHER'];
  private componentDestroyed: Subject<void> = new Subject();
  public selectedActivity: TaskDto;
  @ViewChild('expenseActivityListSlider') activitySlider: SliderComponent;
  private expenseAcceptanceThreshold: GlobalConfiguration;

  @Input() set expense(expense: Expense) {
    if (expense) {
      this.expenseForm.patchValue(expense);
      this.amount.patchValue(this.getNumberInFormat(expense.amount));
      this.getActivity(expense.activityId);
    }
  }

  @Input() set readOnly(readOnly: boolean) {
    if (readOnly === true) {
      this._readOnly = true;
      this.expenseForm.disable();
    }
  }

  // @formatter:off
  get name(): AbstractControl { return this.expenseForm.get('name'); }
  get description(): AbstractControl { return this.expenseForm.get('description'); }
  get date(): AbstractControl { return this.expenseForm.get('date'); }
  get amount(): AbstractControl { return this.expenseForm.get('amount'); }
  get currency(): AbstractControl { return this.expenseForm.get('currency'); }
  get type(): AbstractControl { return this.expenseForm.get('type'); }
  get responsibleId(): AbstractControl { return this.expenseForm.get('responsibleId'); }
  // @formatter:on

  get authKey(): string {
    return this.update ? 'ExpenseEditComponent.' : 'ExpenseAddComponent.';
  }

  constructor(private fb: FormBuilder,
              private sliderService: SliderService,
              private authService: AuthService,
              private personService: PersonService,
              private configService: GlobalConfigService,
              private taskService: CalendarService,
              private notificationService: NotificationService) {
    this.createExpenseForm();

    this.sliderService.sliderStateChanged
      .pipe(
        whenSliderClosed(this.componentDestroyed, ['addExpenseSlider', 'editExpenseSlider']),
      ).subscribe(
      () => {
        this.expenseForm.reset();
        this.resetWarnings();
        this.selectedActivity = null;
      }
    );
  }

  ngOnInit(): void {
    this.subscribeForCurrencies();
    this.configService.getGlobalParamValue('expenseAcceptanceThreshold')
      .subscribe(
        (value) => this.expenseAcceptanceThreshold = value
      );
    this.getAccountEmails();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  public isFormValid(): boolean {
    return this.expenseForm.valid;
  }

  public markAsTouched(): void {
    FormUtil.setTouched(this.expenseForm);
  }

  public getExpense(): Expense {
    const expense: Expense = this.expenseForm.getRawValue();
    if (this.object.type === CrmObjectType.lead) {
      expense.leadId = this.object.id;
    } else if (this.object.type === CrmObjectType.opportunity) {
      expense.opportunityId = this.object.id;
    }
    if (this.selectedActivity) {
      expense.activityId = this.selectedActivity.id;
    }
    return expense;
  }

  private createExpenseForm(): void {
    this.expenseForm = this.fb.group({
      id: [null],
      name: [null, [Validators.required, FormValidators.whitespace, Validators.maxLength(200)]],
      description: [null, [Validators.maxLength(1000)]],
      date: [null],
      amount: [null, Validators.compose([Validators.required, Validators.min(0), this.getAcceptanceThresholdValidator()])],
      currency: [null, Validators.required],
      type: [null],
      responsibleId: [null]
    });
    this.applyPermissionsToForm();
  }

  private applyPermissionsToForm(): void {
    if (this.expenseForm) {
      applyPermission(this.name, !this.authService.isPermissionPresent(`${this.authKey}name`, 'r'));
      applyPermission(this.description, !this.authService.isPermissionPresent(`${this.authKey}description`, 'r'));
      applyPermission(this.date, !this.authService.isPermissionPresent(`${this.authKey}date`, 'r'));
      applyPermission(this.amount, !this.authService.isPermissionPresent(`${this.authKey}amount`, 'r'));
      applyPermission(this.currency, !this.authService.isPermissionPresent(`${this.authKey}currency`, 'r'));
      applyPermission(this.type, !this.authService.isPermissionPresent(`${this.authKey}type`, 'r'));
      applyPermission(this.responsibleId, !this.authService.isPermissionPresent(`${this.authKey}responsible`, 'r'));
    }
  }

  openActivitySlider(): void {
    this.activitySlider.open();
  }

  onActivitySelection($event: TaskDto): void {
    this.selectedActivity = $event;
  }

  removeSelectedActivity(): void {
    this.selectedActivity = null;
  }

  private getAcceptanceThresholdValidator(): (control: AbstractControl) => ValidationErrors {
    return (control: AbstractControl): ValidationErrors => {
      if (!control.value) {
        return null;
      }
      control['warnings'] = this.expenseAcceptanceThreshold.value && control.value >= +this.expenseAcceptanceThreshold.value
          ? {isOverThreshold: true} : null;
      return null;
    };
  }

  private getActivity(activityId: number): void {
    if (isNil(activityId)) {
      return;
    }
    this.taskService.getTaskForExpense(activityId, 'edit')
      .subscribe(
        activity => this.selectedActivity = activity,
        () => this.notificationService.notify('expenses.add.notifications.getExpenseActivityError', NotificationType.ERROR)
      );
  }

  private resetWarnings(): void {
    this.amount['warnings'] = null;
  }

  private getNumberInFormat(value: number): string {
    return String(value).replace('.', ',');
  }

  private subscribeForCurrencies(): void {
    this.configService.getGlobalParamValue('currencies').pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (param) => this.currencies = param.value.split(';'),
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }
  private getAccountEmails(): void {
    this.personService.getAccountEmails()
        .pipe(
            takeUntil(this.componentDestroyed)
        ).subscribe(
        emails => this.accountEmails = emails,
        err => this.notificationService.notify('leads.form.notification.accountEmailsError', NotificationType.ERROR)
    );
  }
}
