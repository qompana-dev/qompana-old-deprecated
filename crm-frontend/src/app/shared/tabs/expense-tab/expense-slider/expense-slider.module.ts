import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExpenseAddComponent} from './expense-add/expense-add.component';
import {ExpenseFormComponent} from './expense-form/expense-form.component';
import {SharedModule} from '../../../shared.module';
import {NgxMaskModule} from 'ngx-mask';
import { ActivityListComponent } from './activity-list/activity-list.component';
import { ExpenseAcceptComponent } from './expense-accept/expense-accept.component';
import { ExpenseEditComponent } from './expense-edit/expense-edit.component';

@NgModule({
  declarations: [
    ExpenseAddComponent,
    ExpenseFormComponent,
    ActivityListComponent,
    ExpenseAcceptComponent,
    ExpenseEditComponent
  ],
  exports: [
    ExpenseAddComponent,
    ExpenseEditComponent,
    ExpenseAcceptComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxMaskModule
  ]
})
export class ExpenseSliderModule {
}
