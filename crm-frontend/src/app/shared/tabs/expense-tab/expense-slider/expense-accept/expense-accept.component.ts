import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {SliderComponent} from '../../../../slider/slider.component';
import {Expense} from '../../expenses.model';
import {ExpenseService} from '../../expense.service';
import {filter, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {AssignToGroupService} from '../../../../assign-to-group/assign-to-group.service';
import {CrmObject, CrmObjectType} from '../../../../model/search.model';
import {UserNotification} from '../../../../../navigation/user-notification/user-notification.model';
import {UserNotificationService} from '../../../../../navigation/user-notification/user-notification.service';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-expense-accept',
  templateUrl: './expense-accept.component.html',
  styleUrls: ['./expense-accept.component.scss']
})
export class ExpenseAcceptComponent implements OnInit, OnDestroy {

  @Input() slider: SliderComponent;
  @Input() userNotification: UserNotification;
  @Output() notificationAccepted: EventEmitter<void> = new EventEmitter<void>();
  expense: Expense;
  creatorName: string;
  leadName: string;
  opportunityName: string;
  private componentDestroyed: Subject<void> = new Subject();
  rejectionCause: FormControl;

  @Input() set expenseId(expenseId: number) {
    if (expenseId) {
      this.expenseService.getExpense(expenseId).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        (expense: Expense) => this.handleGetSuccess(expense)
      );
    }
  }

  constructor(private expenseService: ExpenseService,
              private assignToGroupService: AssignToGroupService,
              private userNotificationService: UserNotificationService) { }

  ngOnInit(): void {
    this.rejectionCause = new FormControl('');
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  decline(): void {
    this.userNotificationService.runActionWithComment(this.userNotification,
      this.userNotification.notification.actions.find(e => e.name === 'notifications.action.decline'), this.rejectionCause.value)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.notificationAccepted.emit());
    this.slider.close();
    this.rejectionCause.patchValue('');
  }

  accept(): void {
    this.userNotificationService.runAction(this.userNotification,
      this.userNotification.notification.actions.find(e => e.name === 'notifications.action.accept'))
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.notificationAccepted.emit());
    this.slider.close();
    this.rejectionCause.patchValue('');
  }

  private handleGetSuccess(expense: Expense): void {
    this.expense = expense;
    this.setCreatorName(expense);
    this.setLeadName(expense);
    this.setOpportunityName(expense);
  }

  private setCreatorName(expense: Expense): void {
    if (expense.creatorId) {
      this.assignToGroupService.updateLabels([{id: expense.creatorId, type: CrmObjectType.user}])
        .pipe(
          takeUntil(this.componentDestroyed),
          filter(objects => objects.length > 0)
        ).subscribe((objects: CrmObject[]) => {
          this.creatorName = objects[0].label;
      });
    }
  }

  private setLeadName(expense: Expense): void {
    if (expense.leadId) {
      this.assignToGroupService.updateLabels([{id: expense.leadId, type: CrmObjectType.lead}])
        .pipe(
          takeUntil(this.componentDestroyed),
          filter(objects => objects.length > 0)
        ).subscribe((objects: CrmObject[]) => {
        this.leadName = objects[0].label;
      });
    }
  }

  private setOpportunityName(expense: Expense): void {
    if (expense.opportunityId) {
      this.assignToGroupService.updateLabels([{id: expense.opportunityId, type: CrmObjectType.opportunity}])
        .pipe(
          takeUntil(this.componentDestroyed),
          filter(objects => objects.length > 0)
        ).subscribe((objects: CrmObject[]) => {
        this.opportunityName = objects[0].label;
      });
    }
  }
}
