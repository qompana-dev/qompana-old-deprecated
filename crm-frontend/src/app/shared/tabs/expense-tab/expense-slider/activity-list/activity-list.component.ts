import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {TaskAssociation, TaskDto, TaskTimeCategoryEnum} from '../../../../../calendar/task.model';
import {CrmObject} from '../../../../model/search.model';
import {crmDateFormat, crmDateTimeFormat, crmTimeFormat} from '../../../../config/times.config';
import {EmptyPage, Page, PageRequest} from '../../../../pagination/page.model';
import {CalendarService} from '../../../../../calendar/calendar.service';
import {NotificationService, NotificationType} from '../../../../notification.service';
import {takeUntil, throttleTime} from 'rxjs/operators';
import {InfiniteScrollUtil} from '../../../../pagination/infinite-scroll.util';
import {TimesUtil} from '../../../../times.util';
import {SliderComponent} from '../../../../slider/slider.component';
import {whenSliderClosed, whenSliderOpen} from '../../../../util/slider.util';
import {SliderService} from '../../../../slider/slider.service';

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.scss']
})
export class ActivityListComponent implements OnInit, OnDestroy {

  @Output()
  activitySelected: EventEmitter<TaskDto> = new EventEmitter();


  @Input()
  set crmObject(object: CrmObject) {
    if (object) {
      this._crmObject = object;
      this.setInitAssociation();
    }
  }

  @Input() public slider: SliderComponent;
  initAssociation: TaskAssociation;
  taskTimeCategoryEnum = TaskTimeCategoryEnum;
  activitiesPage: Page<TaskDto>;
  _crmObject: CrmObject;
  private componentDestroyed: Subject<void> = new Subject();
  private currentPage: number;
  private momentCalendar = {
    lastWeek: crmDateTimeFormat,
    nextWeek: crmDateTimeFormat,
    sameElse: crmDateTimeFormat
  };

  constructor(private calendarService: CalendarService,
              private notificationService: NotificationService,
              private sliderService: SliderService) {
  }

  ngOnInit(): void {
    this.subscribeForSliderClose();
    this.subscribeForSliderOpen();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  setInitAssociation(): void {
    this.initAssociation = {objectType: this._crmObject.type, objectId: this._crmObject.id, disabled: true};
  }

  private initActivities(pageRequest: PageRequest = {page: '0', size: InfiniteScrollUtil.ELEMENTS_ON_PAGE}): void {
    if (!this._crmObject) {
      return;
    }
    this.calendarService.getActivitiesByAssociationIdAndType(this._crmObject.id, this._crmObject.type, pageRequest)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      page => this.handleInitSuccess(page),
      () => this.notifyError()
    );
  }

  public onScroll(e: any): void {
    if (InfiniteScrollUtil.shouldGetMoreData(e) && this.activitiesPage.numberOfElements < this.activitiesPage.totalElements) {
      const nextPage = this.currentPage + 1;
      this.getMoreActivities({page: '' + nextPage, size: InfiniteScrollUtil.ELEMENTS_ON_PAGE});
    }
  }

  private getMoreActivities(pageRequest: PageRequest): void {
    this.calendarService.getActivitiesByAssociationIdAndType(this._crmObject.id, this._crmObject.type, pageRequest)
      .pipe(
        takeUntil(this.componentDestroyed),
        throttleTime(500)
      ).subscribe(
      page => this.handleGetMoreActivitiesSuccess(page),
      () => this.notifyError()
    );
  }

  private handleGetMoreActivitiesSuccess(page: any): void {
    this.currentPage++;
    this.activitiesPage.content = this.activitiesPage.content.concat(page.content);
    this.activitiesPage.numberOfElements += page.content.length;
  }

  private handleInitSuccess(page: any): void {
    this.currentPage = 0;
    this.activitiesPage = page;
  }

  private notifyError(): void {
    this.notificationService.notify('expenses.add.notifications.getActivitiesError', NotificationType.ERROR);
  }


  getCalendarTime(time: string): string {
    return TimesUtil.fromUTCToCurrentTimeZone(time).calendar(null, this.momentCalendar);
  }

  getEndTime(startTime: string, time: string): string {
    if (this.getDate(startTime) === this.getDate(time)) {
      return this.getTime(time);
    }
    return TimesUtil.fromUTCToCurrentTimeZone(time).format(crmDateTimeFormat);
  }

  getTime(dateTime: string): string {
    return TimesUtil.fromUTCToCurrentTimeZone(dateTime).format(crmTimeFormat);
  }

  private getDate(dateTime: string): string {
    return TimesUtil.fromUTCToCurrentTimeZone(dateTime).format(crmDateFormat);
  }

  selectActivity(activity: TaskDto): void {
    this.activitySelected.emit(activity);
    this.slider.close();
  }

  private subscribeForSliderClose(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        whenSliderClosed(this.componentDestroyed, [this.slider.key]),
      ).subscribe(
      () => this.activitiesPage = new EmptyPage()
    );
  }

  private subscribeForSliderOpen(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        whenSliderOpen(this.componentDestroyed, [this.slider.key]),
      ).subscribe(
      () => this.initActivities()
    );
  }
}
