import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { CrmObject } from "../../../model/search.model";
import {
  InfinityScrollUtil,
  InfinityScrollUtilInterface,
} from "../../../infinity-scroll.util";
import { Observable, Subject } from "rxjs";
import {
  NotificationService,
  NotificationType,
} from "../../../notification.service";
import { AuthService } from "../../../../login/auth/auth.service";
import { map, startWith, takeUntil } from "rxjs/operators";
import { FilterStrategy } from "../../../model";
import { Page, PageRequest } from "../../../pagination/page.model";
import { ExpenseService } from "../expense.service";
import { Expense, TotalCost } from "../expenses.model";
import * as isNil from "lodash/isNil";
import { DeleteDialogData } from "../../../dialog/delete/delete-dialog.component";
import { DeleteDialogService } from "../../../dialog/delete/delete-dialog.service";
import { SliderComponent } from "../../../slider/slider.component";
import { PersonService } from "../../../../person/person.service";
import { MatDialog } from "@angular/material/dialog";
import { CalendarService } from "../../../../calendar/calendar.service";
import { SingleEventComponent } from "../../../../calendar/dialog/single-event/single-event.component";
import { TaskAssociation, TaskDto } from "../../../../calendar/task.model";

@Component({
  selector: "app-expense-list",
  templateUrl: "./expense-list.component.html",
  styleUrls: ["./expense-list.component.scss"],
})
export class ExpenseListComponent
  implements OnInit, OnDestroy, InfinityScrollUtilInterface {
  @ViewChild("editExpenseSlider") editSlider: SliderComponent;
  @ViewChild("editActivity") editActivity: SliderComponent;
  public selectedExpenseId: { id: number };
  infinityScroll: InfinityScrollUtil;
  columns = ["date", "name", "amount", "responsible", "edit", "pin"];
  _crmObject: CrmObject;
  _currency: string;
  filteredColumns$: Observable<string[]>;
  initAssociation: TaskAssociation;
  activityTypes: Map<number, string> = new Map<number, string>();
  userMap: Map<number, string>;
  totalCost: TotalCost;
  @Output() crmObjectEdited: EventEmitter<void> = new EventEmitter<void>();

  @Input() set currency(currency: string) {
    this._currency = currency;
    if (this.totalCost && this.totalCost.mainCurrency !== currency) {
      this.getTotalCost();
    }
  }

  @Input() set crmObject(crmObject: CrmObject) {
    if (crmObject) {
      this._crmObject = crmObject;
      this.infinityScroll.initData();
      this.getAttachedToLeadActivityTypes();
      this.getTotalCost();
    }
  }

  private componentDestroyed: Subject<void> = new Subject();

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: "expenses.list.dialog.delete.title",
    description: "expenses.list.dialog.delete.description",
    confirmButton: "expenses.list.dialog.delete.confirm",
    cancelButton: "expenses.list.dialog.delete.cancel",
  };

  constructor(
    private authService: AuthService,
    private notificationService: NotificationService,
    private expenseService: ExpenseService,
    private personService: PersonService,
    public dialog: MatDialog,
    private taskService: CalendarService,
    private calendarService: CalendarService,
    private deleteDialogService: DeleteDialogService
  ) {
    this.infinityScroll = new InfinityScrollUtil(
      this,
      this.componentDestroyed,
      this.notificationService,
      "expenses.list.notification.getExpensesListError"
    );
  }

  ngOnInit(): void {
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
    this.getAccountEmails();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  refreshData(): void {
    this.infinityScroll.initData();
    this.crmObjectEdited.emit();
  }

  private getFilteredColumns(): string[] {
    return this.columns.filter(
      (item) =>
        !this.authService.isPermissionPresent(
          "ExpenseListComponent." + item,
          "i"
        )
    );
  }

  getItemObservable(
    selection: FilterStrategy,
    pageRequest: PageRequest
  ): Observable<Page<Expense>> {
    return this.expenseService.getExpenses(
      pageRequest,
      selection,
      this._crmObject
    );
  }

  rowClick(expense: Expense): void {
    if (this.canBeEdited(expense)) {
      this.edit(expense);
    }
  }

  edit(expense: Expense): void {
    this.selectedExpenseId = { id: expense.id };
    this.editSlider.open();
  }

  unpinActivity(expense: Expense): void {
    this.expenseService
      .detachActivityFormExpense(expense.id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          expense.activityId = null;
          this.notificationService.notify(
            "expenses.list.notification.detachActivityFormExpenseSuccess",
            NotificationType.SUCCESS
          );
        },
        (err) =>
          this.notificationService.notify(
            "expenses.list.notification.detachActivityFormExpenseError",
            NotificationType.ERROR
          )
      );
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  canBeEdited(expense: Expense): boolean {
    return expense.state !== "TO_BE_ACCEPTED";
  }

  getActivityNameById(activityId: number): string {
    if (
      this.activityTypes.get(activityId) &&
      this.activityTypes.get(activityId) !== null
    ) {
      return this.activityTypes.get(activityId);
    } else {
      return "event";
    }
  }

  getAttachedToLeadActivityTypes(): void {
    if (this._crmObject) {
      this.expenseService
        .getAttachedToLeadActivityTypes(this._crmObject)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((activityTypes: Map<number, string>) => {
          Object.keys(activityTypes).forEach((key) => {
            this.activityTypes.set(+key, activityTypes[key]);
          });
        });
    }
  }

  getTotalCost(): void {
    if (this._crmObject) {
      this.expenseService
        .getTotalCost(this._crmObject, this._currency)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((totalCost: TotalCost) => {
          const temp = new Map<string, number>();
          Object.keys(totalCost.costInCurrencies).forEach((key) => {
            temp.set(key, totalCost.costInCurrencies[key]);
          });
          totalCost.costInCurrencies = temp;
          this.totalCost = totalCost;
        });
    }
  }

  private getAccountEmails(): void {
    this.personService.getAndMapAccountEmails(this.componentDestroyed)
      .subscribe(
        (users) => (this.userMap = users),
        () => this.notificationService.notify('leads.form.notification.accountEmailsError', NotificationType.ERROR)
      );
  }

  editSelectedActivity(activity: TaskDto): void {
    this.calendarService.taskEdited.emit(activity.id);
    this.editActivity.open();
  }

  eventClicked(activityId: any): void {
    this.getActivity(activityId);
  }

  private getActivity(activityId: number): void {
    if (isNil(activityId)) {
      return;
    }
    this.taskService.getTaskForExpense(activityId, "edit").subscribe(
      (activity) => {
        const dialogRef = this.dialog.open(SingleEventComponent, {
          data: activity,
        });
        dialogRef.componentInstance.eventDeleted.subscribe((id) =>
          this.refreshData()
        );
        dialogRef.componentInstance.editEvent = () => {
          this.editSelectedActivity(activity);
          dialogRef.componentInstance.close();
        };
      },
      () =>
        this.notificationService.notify(
          "expenses.add.notifications.getExpenseActivityError",
          NotificationType.ERROR
        )
    );
  }

  private isActivityAllowedToMe(activity: TaskDto): boolean {
    return activity.creator === +localStorage.getItem("loggedAccountId");
  }

  setInitAssociation(): void {
    this.initAssociation = undefined;
    if (this.crmObject) {
      this.initAssociation = {
        objectType: this.crmObject.type,
        objectId: this.crmObject.id,
        disabled: true,
      };
    }
  }
}
