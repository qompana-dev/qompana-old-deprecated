import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {CrmObject, CrmObjectType} from '../../model/search.model';
import {MailService} from '../../../mail/mail.service';
import {exhaustMap, takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {MailContent, MailSortStrategy, MailStorageDto} from '../../../mail/mail.model';
import {NotificationService, NotificationType} from '../../notification.service';
import {MailUtilService} from '../../../mail/mail-util.service';
import {DateUtil} from '../../util/date.util';
import {DeleteDialogData} from '../../dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../dialog/delete/delete-dialog.service';
import { Page } from '../../pagination/page.model';

@Component({
  selector: 'app-attached-emails',
  templateUrl: './attached-emails.component.html',
  styleUrls: ['./attached-emails.component.scss']
})
export class AttachedEmailsComponent implements OnInit, OnDestroy {
  _crmObject: CrmObject;
  private supportedTypes: CrmObjectType[] = [CrmObjectType.lead, CrmObjectType.opportunity, CrmObjectType.contact];
  private componentDestroyed: Subject<void> = new Subject();

  messageSelectedSubject: Subject<MailStorageDto> = new Subject();

  selectedSort: MailSortStrategy = MailSortStrategy.receivedDate;
  sortSelection = MailSortStrategy;
  selectedMessageStorageId: number;

  loading = false;
  headers: MailStorageDto[] = [];
  selectedMessage: MailStorageDto;
  selectedMailContent: MailContent;

  detachEmailDialog: Partial<DeleteDialogData> = {
    title: 'tabs.attachedEmails.detachDialog.title',
    description: 'tabs.attachedEmails.detachDialog.description',
    confirmButton: 'tabs.attachedEmails.detachDialog.yes',
    cancelButton: 'tabs.attachedEmails.detachDialog.no',
  };

  @Input() set crmObject(crmObject: CrmObject) {
    if (crmObject) {
      this._crmObject = crmObject;
      this.initData();
    }
  };

  @Input() fullHeight = false;

  constructor(private mailService: MailService,
              private mailUtil: MailUtilService,
              private deleteDialogService: DeleteDialogService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.listenOnMessageSelected();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  initData(): void {
    if (this.supportedTypes.indexOf(this._crmObject.type) === -1) {
      console.error('unsupported type ', this._crmObject.type);
      return;
    }
    this.getMessages();
  }

  getMessages(): void {
    if (!this.loading) {
      this.loading = true;
      this.mailService.getMailHeaders(this._crmObject.id, this._crmObject.type)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((headers: Page<MailStorageDto>) => {
          this.headers = headers.content;
          this.loading = false;
          this.sort(MailSortStrategy.receivedDate);
        }, () => {
          this.loading = false;
        });
    }
  }

  getMessage(header: MailStorageDto): void {
    this.selectedMessage = header;
    this.selectedMailContent = null;
    this.messageSelectedSubject.next(header);
  }

  sort(sortStrategy: MailSortStrategy): void {
    this.selectedSort = sortStrategy;
    this.headers = this.headers.sort((a, b) => this.mailStorageComparator(a, b))
  }

  private mailStorageComparator(a: MailStorageDto, b: MailStorageDto): number {
    if (this.selectedSort === MailSortStrategy.subject) {
      return a.subject.localeCompare(b.subject);
    } else {
      if (a.receivedDate === b.receivedDate) {
        return 0;
      }
      return DateUtil.toDate(a.receivedDate) < DateUtil.toDate(b.receivedDate) ? 1 : -1;
    }
  }

  getMailHeader(header: MailStorageDto): string {
    return this.mailUtil.getFromMailTitle(header);
  }

  isHeaderSelected(header: MailStorageDto): boolean {
    return header && this.selectedMessage && this.selectedMessage.messageId === header.messageId;
  }

  tryDetachEmail(mailStorageId: number) {
    this.detachEmailDialog.onConfirmClick = () => this.detachEmail(mailStorageId);
    this.deleteDialogService.open(this.detachEmailDialog)
  }

  private detachEmail(mailStorageId: number) {
    this.mailService.detachMessage(this._crmObject.id, mailStorageId, this._crmObject.type)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.headers = this.headers.filter(header => header.id !== mailStorageId);
        this.notificationService.notify('tabs.attachedEmails.notification.mailDetached', NotificationType.SUCCESS);
      }, () => this.notificationService.notify('tabs.attachedEmails.notification.detachEmailError', NotificationType.ERROR));
  }

  private listenOnMessageSelected(): void {
    this.messageSelectedSubject.pipe(
      takeUntil(this.componentDestroyed),
      tap((header) => this.selectedMessageStorageId = header.id),
      exhaustMap(header => this.mailService.getMessageFromStorage(this._crmObject.id, header.id, this._crmObject.type))
    ).subscribe((message: MailContent) => {
      this.selectedMailContent = message;
    });
  }

  getDetachMessageKey(): string {
    switch (this._crmObject && this._crmObject.type) {
      case CrmObjectType.lead:
        return 'LeadDetailsComponent.detachEmail';
      case CrmObjectType.opportunity:
        return 'OpportunityDetailsComponent.detachEmail';
      case CrmObjectType.contact:
        return 'ContactDetailsComponent.detachEmail';
      default:
        return 'OpportunityDetailsComponent.detachEmail';
    }
  }
}
