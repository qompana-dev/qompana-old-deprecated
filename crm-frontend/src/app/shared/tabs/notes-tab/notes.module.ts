import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NoteListComponent} from './note-list/note-list.component';
import {AddNoteSliderComponent} from './add-note-slider/add-note-slider.component';
import {NotePreviewComponent} from './note-preview/note-preview.component';
import {SharedModule} from '../../shared.module';
import {NoteAuthKeyPipe} from './note-auth-key.pipe';


@NgModule({
  declarations: [NoteListComponent, AddNoteSliderComponent, NotePreviewComponent, NoteAuthKeyPipe],
  imports: [
    SharedModule,
    CommonModule
  ],
  exports: [
    NoteListComponent, AddNoteSliderComponent, NotePreviewComponent, NoteAuthKeyPipe
  ],
})
export class NotesModule {
}
