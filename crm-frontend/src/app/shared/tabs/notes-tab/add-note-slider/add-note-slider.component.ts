import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {SliderComponent} from '../../../slider/slider.component';
import {FormControl} from '@angular/forms';
import {CrmObject} from '../../../model/search.model';
import {Subject} from 'rxjs';
import {NotificationService, NotificationType} from '../../../notification.service';
import {takeUntil} from 'rxjs/operators';
import {NotesService} from '../../../../widgets/notes/notes.service';
import {ObjectTypeAndId} from '../../../../widgets/notes/notes.model';

@Component({
  selector: 'app-add-note-slider',
  templateUrl: './add-note-slider.component.html',
  styleUrls: ['./add-note-slider.component.scss']
})
export class AddNoteSliderComponent implements OnInit, OnDestroy {

  @Input() slider: SliderComponent;
  @Input() crmObject: CrmObject;
  @Output() public noteAdded: EventEmitter<void> = new EventEmitter<void>();
  noteControl = new FormControl('');
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private noteService: NotesService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.listenForSliderEvents();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  cancel(): void {
    this.noteControl.reset();
    this.slider.close();
  }


  save(): void {
    if (!this.trim(this.noteControl.value)) {
      return;
    }
    if (this.noteControl.value.length > 3000) {
      this.notificationService.notify('notes.add.notification.maxLengthExceeded', NotificationType.ERROR);
      return;
    }
    this.noteService.saveNote(new ObjectTypeAndId(this.crmObject), this.noteControl.value, null)
      .subscribe(
        () => {
          this.notificationService.notify('notes.add.notification.addSuccess', NotificationType.SUCCESS);
          this.noteAdded.emit();
          this.slider.close();
        },
        () => this.notificationService.notify('notes.add.notification.addError', NotificationType.ERROR)
      );
  }


  private listenForSliderEvents(): void {
    this.slider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.resetForm()
    );
  }

  private resetForm(): void {
    this.noteControl.reset();
  }

  trim(text: string): string {
    return text && text.trim();
  }

}
