import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {SliderComponent} from '../../../slider/slider.component';
import {CrmObject} from '../../../model/search.model';
import {InfinityScrollUtil, InfinityScrollUtilInterface} from '../../../infinity-scroll.util';
import {Observable, of, Subject} from 'rxjs';
import {DeleteDialogData} from '../../../dialog/delete/delete-dialog.component';
import {AuthService} from '../../../../login/auth/auth.service';
import {NotificationService, NotificationType} from '../../../notification.service';
import {DeleteDialogService} from '../../../dialog/delete/delete-dialog.service';
import {takeUntil} from 'rxjs/operators';
import {FilterStrategy} from '../../../model';
import {EmptyPage, Page, PageRequest} from '../../../pagination/page.model';
import {NotesService} from '../../../../widgets/notes/notes.service';
import {Note} from '../../../../widgets/notes/notes.model';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss']
})
export class NoteListComponent implements OnInit, OnDestroy, InfinityScrollUtilInterface {


  @Input() set crmObject(crmObject: CrmObject) {
    if (crmObject) {
      this._crmObject = crmObject;
      this.getNotes();
    }
  }

  @Input() set preview(preview: boolean) {
    if (preview != null) {
      this._preview = preview;
      this.getNotes();
    }
  }


  @Input() userMap: Map<number, string> = new Map();
  @Output() noteSelected: EventEmitter<Note> = new EventEmitter<Note>();
  @Output() changeTab: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();
  selectedNote: Note;

  @ViewChild('editExpenseSlider') editSlider: SliderComponent;
  infinityScroll: InfinityScrollUtil;
  columns = ['content', 'createdBy', 'updated', 'updatedBy', 'action'];
  _crmObject: CrmObject;
  _preview;

  private componentDestroyed: Subject<void> = new Subject();

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'notes.list.dialog.delete.title',
    description: 'notes.list.dialog.delete.description',
    confirmButton: 'notes.list.dialog.delete.confirm',
    cancelButton: 'notes.list.dialog.delete.cancel',
  };

  constructor(private authService: AuthService,
              private notificationService: NotificationService,
              private notesService: NotesService,
              private deleteDialogService: DeleteDialogService) {
    this.initInfinityScroll();
  }


  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  ngOnInit(): void {
    this.infinityScroll.onScrollInit$.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.handleListLoaded()
    );
  }


  public getNotes(): void {
    if (this._crmObject != null && this._preview != null) {
      this.infinityScroll.initData();
    }
  }

  getItemObservable(selection: FilterStrategy, pageRequest: PageRequest): Observable<Page<Note>> {
    if (this._crmObject == null || this._preview == null) {
      return of(new EmptyPage<Note>());
    }
    if (this._preview) {
      pageRequest.size = '3';
    }
    return this.notesService.getNotesAsPage(pageRequest, this._crmObject);
  }


  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }


  delete(note: Note): void {
    this.notesService.deleteNote(note.id)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      () => {
        this.getNotes();
        this.notificationService.notify('notes.list.notification.deleteNoteSuccess', NotificationType.SUCCESS);
      },
      err => this.notificationService.notify('notes.list.notification.deleteNoteError', NotificationType.ERROR)
    );
  }

  tryDelete(note: Note): void {
    this.deleteDialogData.onConfirmClick = () => this.delete(note);
    this.openDeleteDialog();
  }

  selectNote(note: Note): void {
    this.selectedNote = note;
    this.noteSelected.emit(note);
  }

  getNextNote(note: Note): void {
    if (this.pageIsEmpty()) {
      return null;
    }
    let index = this.getPageContent().indexOf(note);
    if (index < 0) {
      return null;
    }
    index += 1;
    if (index >= this.getPageContent().length) {
      index = 0;
    }
    this.selectNote(this.getPageContent()[index]);
  }

  getPreviousNote(note: Note): void {
    if (this.pageIsEmpty()) {
      return null;
    }
    let index = this.getPageContent().indexOf(note);
    if (index < 0) {
      return null;
    }
    index -= 1;
    if (index < 0) {
      index = this.getPageContent().length - 1;
    }
    this.selectNote(this.getPageContent()[index]);
  }

  private getPageContent(): any[] {
    return this.infinityScroll.itemPage.content;
  }

  private pageIsEmpty(): boolean {
    const pageContent = this.infinityScroll.itemPage;
    return !(pageContent && pageContent.content && pageContent.content.length !== 0);
  }

  private selectFirstNote(): void {
    if (!this.pageIsEmpty()) {
      this.selectNote(this.getPageContent()[0]);
    } else {
      this.selectNote(null);
    }

  }

  private handleListLoaded(): void {
    if (!this._preview) {
      this.selectFirstNote();
    }
  }

  private initInfinityScroll(): void {
    this.infinityScroll = new InfinityScrollUtil(
      this,
      this.componentDestroyed,
      this.notificationService,
      'notes.list.notification.getNoteListError',
      'created,desc'
    );
  }
}
