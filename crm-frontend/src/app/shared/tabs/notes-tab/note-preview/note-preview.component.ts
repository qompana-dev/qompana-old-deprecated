import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Note, ObjectTypeAndId} from '../../../../widgets/notes/notes.model';
import {Subject} from 'rxjs';
import {ErrorTypes, NotificationService, NotificationType} from '../../../notification.service';
import {NotesService} from '../../../../widgets/notes/notes.service';
import {CrmObject} from '../../../model/search.model';
import {takeUntil} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import {applyPermission} from '../../../permissions-utils';
import {AuthService} from '../../../../login/auth/auth.service';
import {NoteAuthService} from '../note-auth.service';

@Component({
  selector: 'app-note-preview',
  templateUrl: './note-preview.component.html',
  styleUrls: ['./note-preview.component.scss']
})
export class NotePreviewComponent implements OnInit, OnDestroy {

  noteControl = new FormControl('');
  _preview = false;
  private originalNote: Note = null;

  @Input() set note(note: Note) {
    this.originalNote = note;
    applyPermission(this.noteControl, !!note && this.authService.isPermissionPresent(
      this.noteAuthService.getAuthKey('edit', this.crmObject && this.crmObject.type), 'w'));

    if (note) {
      this.noteControl.patchValue(note.content);
    }
    this.nextDisabled = false;
    this.previousDisabled = false;
  }


  @Input() set noteText(noteText: string) {
    this.noteControl.patchValue(noteText);
  }

  @Input() set preview(preview: boolean) {
    this._preview = preview;
    this.noteControl.disable();
  }


  @Input() readonly = false;
  @Input() crmObject: CrmObject;
  @Input() nextDisabled = true;
  @Input() previousDisabled = true;
  @Output() public noteUpdated: EventEmitter<Note> = new EventEmitter<Note>();
  @Output() public next: EventEmitter<void> = new EventEmitter<void>();
  @Output() public previous: EventEmitter<void> = new EventEmitter<void>();
  private componentDestroyed: Subject<void> = new Subject();

  readonly updateNoteErrors: ErrorTypes = {
    base: 'notes.preview.notification.',
    defaultText: 'updateError',
    errors: [{
      code: 404,
      text: 'updateNoteNotFound'
    },
      {
        code: 403,
        text: 'noPermission'
      }]
  };

  constructor(public notesService: NotesService,
              private notificationService: NotificationService,
              private authService: AuthService,
              private noteAuthService: NoteAuthService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  cancel(): void {
    this.noteControl.patchValue(this.originalNote.content);
  }

  updateNote(): void {
    if (!this.trim(this.noteControl.value)) {
      return;
    }
    if (this.noteControl.value.length > 3000) {
      this.notificationService.notify('widgets.notes.notification.maxLengthExceeded', NotificationType.ERROR);
      return;
    }
    this.notesService.updateNote(new ObjectTypeAndId(this.crmObject), this.originalNote.id, this.noteControl.value, null).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      (updatedNote) => {
        this.originalNote = updatedNote;
        this.noteControl.patchValue(updatedNote.content);
        this.noteUpdated.emit(updatedNote);
        this.notificationService.notify('notes.preview.notification.updateSuccess', NotificationType.SUCCESS);
      },
      err => this.notificationService.notifyError(this.updateNoteErrors, err.status)
    );
  }

  trim(text: string): string {
    return text && text.trim();
  }

  nextNote(): void {
    this.next.emit();
  }

  previousNote(): void {
    this.previous.emit();
  }


}
