import { Pipe, PipeTransform } from '@angular/core';
import {CrmObjectType} from '../../model/search.model';
import {NoteAuthService} from './note-auth.service';

@Pipe({
  name: 'noteAuthKey'
})
export class NoteAuthKeyPipe implements PipeTransform {

  constructor(private noteAuthService: NoteAuthService) {
  }

  transform(key: string, crmType: CrmObjectType): string {
      return this.noteAuthService.getAuthKey(key, crmType);
  }

}
