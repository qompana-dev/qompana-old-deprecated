import { Injectable } from '@angular/core';
import {CrmObjectType} from '../../model/search.model';

@Injectable({
  providedIn: 'root'
})
export class NoteAuthService {

  constructor() { }

  getAuthKey(key: string, type: CrmObjectType): string {
    if (type) {
      switch (type) {
        case CrmObjectType.customer:
          return 'CustomerNotes.' + key;
        case CrmObjectType.opportunity:
          return 'OpportunityNotes.' + key;
        case CrmObjectType.lead:
          return 'LeadNotes.' + key;
        case CrmObjectType.contact:
          return 'ContactNotes.' + key;
      }
    } else {
      return '';
    }
  }
}
