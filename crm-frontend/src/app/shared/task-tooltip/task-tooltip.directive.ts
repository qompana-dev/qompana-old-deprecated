import {
  Directive,
  Input,
  ElementRef,
  HostListener,
  Renderer2
} from "@angular/core";
import {
  Overlay,
  OverlayPositionBuilder,
  OverlayRef
} from "@angular/cdk/overlay";
import { take } from "rxjs/operators";
import { ComponentPortal } from "@angular/cdk/portal";
import {TaskTooltipComponent} from "./task-tooltip.component";
import {TaskTooltipData} from "./task-tooltip.model";

@Directive({
  selector: "[taskTooltip]"
})
export class TaskTooltipDirective {

  @Input(`taskTooltipShow`) showToolTip: boolean = true;

  @Input(`taskTooltipData`) taskTooltipData: TaskTooltipData;

  private _overlayRef: OverlayRef;
  private _tooltipInstance;

  constructor(
    private _overlay: Overlay,
    private _overlayPositionBuilder: OverlayPositionBuilder,
    private _elementRef: ElementRef,
    private _r2: Renderer2
  ) {

  }

  ngOnInit() {
    if (!this.showToolTip) {
      return;
    }

    const positionStrategy = this._overlayPositionBuilder
      .flexibleConnectedTo(this._elementRef)
      .withPositions([
        {
          originX: "end",
          originY: "top",
          overlayX: "start",
          overlayY: "top",
          offsetX: 10,
          offsetY: -48
        }
      ]);

    this._overlayRef = this._overlay.create({ positionStrategy });
  }

  @HostListener("click")
  show(e) {
    if (this._overlayRef && !this._overlayRef.hasAttached()) {
      this._tooltipInstance = this._overlayRef.attach(
        new ComponentPortal(TaskTooltipComponent)
      ).instance;

      this._tooltipInstance.taskTooltipData = this.taskTooltipData;
      this._tooltipInstance.onCloseClick.subscribe(() => {
        this.hide();
      });

      this._tooltipInstance!.show(0);

      this._tooltipInstance
        .afterHidden()
        .pipe(take(1))
        .subscribe(() => {
          this._overlayRef.detach();
        });
    }
  }

  hide() {
    this._tooltipInstance!._onHide.next()
  }
}
