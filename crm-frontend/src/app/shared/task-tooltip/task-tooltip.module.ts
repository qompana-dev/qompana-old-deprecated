import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TaskTooltipComponent} from './task-tooltip.component';
import {SharedModule} from '../shared.module';
import {TaskTooltipDirective} from "./task-tooltip.directive";

@NgModule({
  declarations: [
    TaskTooltipComponent,
    TaskTooltipDirective,
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    TaskTooltipComponent,
    TaskTooltipDirective
  ],
  entryComponents: [
    TaskTooltipComponent
  ]
})
export class TaskTooltipModule {
}
