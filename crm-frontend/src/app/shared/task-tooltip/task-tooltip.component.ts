import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {TaskTooltipData} from './task-tooltip.model';
import {Subject} from "rxjs";
import {TooltipComponent} from "@angular/material/tooltip";

@Component({
  selector: 'task-tooltip',
  templateUrl: './task-tooltip.component.html',
  styleUrls: ['./task-tooltip.component.scss']
})
export class TaskTooltipComponent extends TooltipComponent implements OnInit, OnDestroy {

  private componentDestroyed: Subject<void> = new Subject();

  @Input() taskTooltipData: TaskTooltipData;

  @Output() onCloseClick: EventEmitter<void> = new EventEmitter();

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.onCloseClick.next();
    this.onCloseClick.complete();
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  close() {
    this.onCloseClick.emit();
  }
}
