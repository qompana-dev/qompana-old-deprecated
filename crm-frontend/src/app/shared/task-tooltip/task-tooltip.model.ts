export class TaskTooltipData {
  content: string;
}

export class TaskTooltipDataFactory {

  static create(content: string): TaskTooltipData {
    const taskTooltipData = new TaskTooltipData();
    taskTooltipData.content = content;
    return taskTooltipData;
  }
}
