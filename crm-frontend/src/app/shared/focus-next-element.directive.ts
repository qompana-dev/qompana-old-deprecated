import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appFocusNextElement]'
})
export class FocusNextElementDirective {

  constructor(private el: ElementRef) { }

  @HostListener('keydown.enter')
  onEnterClicked(): void {
    const nextInputIndex = +this.el.nativeElement.id.replace('property-input-index-', '') + 1;
    document.getElementById('property-input-index-' + nextInputIndex).focus();
  }
}
