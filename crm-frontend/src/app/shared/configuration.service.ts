import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UrlUtil} from './url.util';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor(private http: HttpClient) { }

  getPasswordPolicyRegex(): Observable<any> {
    return this.http.get(`${UrlUtil.url}/crm-user-service/configuration/password-policy`);
  }
}
