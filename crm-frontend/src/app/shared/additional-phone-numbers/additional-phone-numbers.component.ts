import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  OnChanges,
  SimpleChanges,
  EventEmitter,
  Output,
} from '@angular/core';
import {
  AdditionalPhoneNumber,
  PhoneType,
} from './additional-phone-numbers.model';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {AbstractControl, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { FormValidators } from '../form/form-validators';
import { DictionaryStoreService } from 'src/app/dictionary/dictionary-store.service';
import {PhoneService} from '../phone-fields/phone.service';

@Component({
  selector: 'app-additional-phone-numbers',
  templateUrl: './additional-phone-numbers.component.html',
  styleUrls: ['./additional-phone-numbers.component.scss'],
})
export class AdditionalPhoneNumbersComponent
  implements OnDestroy, OnInit, OnChanges {
  public phoneTypes: PhoneType[] = [];
  private componentDestroyed: Subject<void> = new Subject();

  @Input('field-name') fieldName: string;
  @Input('form') inputForm: FormGroup;

  @Input()
  set addFieldSubject(addFieldSubject: Observable<void>) {
    if (addFieldSubject) {
      addFieldSubject
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => this.addField());
    }
  }
  @Input('initial-phones') initPhones: AdditionalPhoneNumber[];

  @Input() showPlusButton = false;

  @Output() focusOut = new EventEmitter<FocusEvent>();
  @Output() focusIn = new EventEmitter<FocusEvent>();

  constructor(
    private fb: FormBuilder,
    private dictionariesService: DictionaryStoreService,
    public phoneService: PhoneService
  ) {}

  ngOnInit() {
    this.pullPhoneTypes();
  }

  ngOnChanges(changes: SimpleChanges) {
    const { initPhones } = changes;
    if (initPhones && this.inputForm) {
      this.clear();
      initPhones.currentValue.forEach((phone) => this.addField(phone));
    }
  }

  private pullPhoneTypes() {
    const phoneTypesDictId = 3;
    this.dictionariesService
      .getDictionaryWordsById(phoneTypesDictId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((words) => {
        this.phoneTypes = words.map((word) => {
          return {
            id: word.id,
            name: word.name,
          } as PhoneType;
        });
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  phone(index: number): AbstractControl {
    return this.phones.at(index);
  }

  get phones(): FormArray {
    if (this.inputForm) {
      return this.inputForm.get(this.fieldName) as FormArray;
    }
    return this.fb.array([]);
  }

  removeField($event: any, fieldId: number): void {
    this.isFocusIn();
    $event.stopPropagation();
    this.phones.removeAt(fieldId);
    this.isFocusOut();
  }

  public addField(
    item: AdditionalPhoneNumber = {
      type: { id: undefined, name: undefined },
      phone: undefined,
      number: undefined
    }
  ): void {
    const newPhone = this.createPhoneFormGroup(item);
    this.phones.push(newPhone);
  }

  clear(): void {
    if (this.phones) {
      while (0 !== this.phones.length) {
        this.phones.removeAt(0);
      }
    }
  }

  private createPhoneFormGroup(item: AdditionalPhoneNumber): FormGroup {
    const typeFormGroup = this.fb.group({
      id: [item.type.id, [Validators.required]],
      name: [item.type.name, [Validators.required, FormValidators.whitespace]],
    });
    const newFormGroup = this.fb.group({
      type: typeFormGroup,
      phone: [item.phone, [Validators.required, FormValidators.whitespace]],
    });

    typeFormGroup.get('name').valueChanges.subscribe((val) => {
      if (val) {
        const typeId = this.phoneTypes.filter(
          (type) => type.name === val
        )[0].id;
        typeFormGroup.get('id').setValue(typeId);
      }
    });
    return newFormGroup;
  }

  isFocusIn() {
    this.focusIn.emit();
  }

  isFocusOut() {
    this.focusOut.emit();
  }

  changeSelect(state) {
    state ? this.isFocusIn() : this.isFocusOut();
  }
}
