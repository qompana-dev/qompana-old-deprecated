import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdditionalPhoneNumbersComponent} from './additional-phone-numbers.component';
import {SharedModule} from '../shared.module';
import {NgxMaskModule} from 'ngx-mask';


@NgModule({
  declarations: [
    AdditionalPhoneNumbersComponent,
  ],
    imports: [
        CommonModule,
        SharedModule,
        NgxMaskModule
    ],
  exports: [
    AdditionalPhoneNumbersComponent
  ]
})
export class AdditionalPhoneNumbersModule {
}
