export interface PhoneType {
  id: number;
  name: string;
}

export interface AdditionalPhoneNumber {
  type: PhoneType;
  phone: string;
  number: string;
}
