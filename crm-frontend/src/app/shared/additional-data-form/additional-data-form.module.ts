import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdditionalDataFormComponent} from './additional-data-form.component';
import {SharedModule} from '../shared.module';

@NgModule({
  declarations: [
    AdditionalDataFormComponent,
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    AdditionalDataFormComponent
  ]
})
export class AdditionalDataFormModule {
}
