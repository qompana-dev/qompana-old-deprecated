import {Component, Input, OnDestroy} from '@angular/core';
import {AdditionalData} from './additional-data-form.model';
import {Observable, Subject, Subscription} from 'rxjs';
import {debounceTime, takeUntil} from 'rxjs/operators';
import {AbstractControl, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PersonAdditionalData} from '../../person/person.model';
import {FormValidators} from '../form/form-validators';

@Component({
  selector: 'app-additional-data-form',
  templateUrl: './additional-data-form.component.html',
  styleUrls: ['./additional-data-form.component.scss']
})
export class AdditionalDataFormComponent implements OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();
  // tslint:disable-next-line:variable-name
  private _additionalData: AbstractControl;

  formGroup: FormGroup;
  additionalItems: FormArray;

  private formChangeSubscription: Subscription;

  @Input()
  set additionalDataString(additionalData: AbstractControl) {
    this._additionalData = additionalData;
    this.updateValue();
    this._additionalData.valueChanges
      .pipe(
        debounceTime(1000),
        takeUntil(this.componentDestroyed))
      .subscribe(() => this.updateValue());
  }

  @Input()
  set addFieldSubject(addFieldSubject: Observable<void>) {
    if (addFieldSubject) {
      addFieldSubject.pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => this.addAdditionalField());
    }
  }

  @Input() showPlusButton = false;

  constructor(private fb: FormBuilder) {
    this.reset();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  reset(): void {
    this.formGroup = this.fb.group({
      additionalData: this.fb.array([])
    });
    this.additionalItems = this.additionalData as FormArray;
    this.subscribeFormChange();
  }

  private updateValue(): void {
    this.unsubscribeFormChange();
    this.updateAdditionalData();
    this.subscribeFormChange();
  }

  private subscribeFormChange(): void {
    this.formChangeSubscription = this.formGroup.valueChanges
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this._additionalData.patchValue(this.getAdditionalData()));
  }

  private unsubscribeFormChange(): void {
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }

  get additionalData(): AbstractControl {
    return this.formGroup.get('additionalData');
  }

  removeAdditionalField($event: any, fieldId: number): void {
    $event.stopPropagation();
    this.additionalItems.removeAt(fieldId);
  }

  private getAdditionalData(): string {
    const additionalDataArray: PersonAdditionalData[] = [];
    for (const control of this.additionalData['controls']) {
      additionalDataArray.push(control.value);
    }
    return this.additionalDataToString(additionalDataArray);
  }

  private updateAdditionalData(): void {
    this.clearFormArray(this.additionalItems);
    if (this._additionalData && this._additionalData.value) {
      const additionalDataObj = this.additionalDataToObj(this._additionalData.value);
      for (const item of additionalDataObj) {
        this.addAdditionalField(item);
      }
    }
  }

  public addAdditionalField(item: PersonAdditionalData = {title: undefined, value: undefined}): void {
    this.additionalItems.push(this.createAdditionalDataItem(item));
  }

  private createAdditionalDataItem(item: PersonAdditionalData): FormGroup {
    return this.fb.group({
      title: [item.title, [Validators.required, FormValidators.whitespace]],
      value: [item.value, [Validators.required, FormValidators.whitespace]]
    });
  }

  private clearFormArray(form: FormArray): void {
    if (form) {
      while (0 !== form.length) {
        form.removeAt(0);
      }
    }
  }

  private additionalDataToObj(additionalData: string): AdditionalData[] {
    return JSON.parse(additionalData);
  }


  private additionalDataToString(additionalData: AdditionalData[]): string {
    return JSON.stringify(additionalData);
  }
}
