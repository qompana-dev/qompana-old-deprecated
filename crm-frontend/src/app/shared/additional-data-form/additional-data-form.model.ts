export interface AdditionalData {
  title: string;
  value: string;
}
