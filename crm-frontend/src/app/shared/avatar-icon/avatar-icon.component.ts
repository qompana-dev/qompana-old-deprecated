import { Component, Input } from "@angular/core";

@Component({
  selector: "app-avatar-icon",
  templateUrl: "./avatar-icon.component.html",
  styleUrls: ["./avatar-icon.component.scss"],
})
export class AvatarIconComponent {
  @Input() imageSrc: string;
  @Input() widthPx: number = 42;
  @Input() fontSizePx: number;
  @Input() text: string;
  @Input() color: string;
  @Input() randomColor: boolean = false;

  defaultImg = "assets/icons/user-logo.svg";

  constructor() {}

  textToColor(text: string) {
    let hash = 0;
    for (let i = 0; i < text.length; i++) {
      hash = text.charCodeAt(i) + ((hash << 5) - hash);
    }
    let color = "#";
    for (let i = 0; i < 3; i++) {
      const value = (hash >> (i * 8)) & 0xff;
      color += ("00" + value.toString(16)).substr(-2);
    }
    return color;
  }

  parseTextToAvatar(text: string) {
    const texts = text.split(" ").filter((text) => !!text);
    if (texts.length > 1) {
      return `${texts[0].charAt(0)}${texts[1].charAt(0)}`;
    }
    return text.substr(0, 1);
  }
}
