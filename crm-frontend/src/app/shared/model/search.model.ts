export interface CrmObject {
  id: number;
  type: CrmObjectType;
  label?: string;
  created?: string;
}

export enum CrmObjectType {
  user = 'user',
  customer = 'customer',
  lead = 'lead',
  opportunity = 'opportunity',
  role = 'role',
  contact = 'contact',
  product = 'product',
  file = 'file',
  task = 'task',
  activity = 'activity',
  manufacturer = 'manufacturer',
  supplier = 'supplier',
  mail = 'mail'
}

export function crmObjectComparator(a: CrmObject, b: CrmObject): boolean {
  return a && b && a.type === b.type && a.id === b.id;
}
