export interface CrmFile {
  id: number;
  name: string;
  date: string;
  fileFormat: string;
  addedBy: number;
  contract: string;
}
