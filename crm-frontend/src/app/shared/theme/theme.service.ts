import { OVERRIDES_CHART_THEME } from './theme.config';
import { ChartOptions } from "chart.js";
import { ThemeService as ChartThemeService } from "ng2-charts";
import { LocaleService } from "./../../calendar/locale.service";
import { HttpClient } from "@angular/common/http";
import { UrlUtil } from "./../url.util";
import { Themes } from "./theme.model";
import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ThemeService {
  selectedTheme = null;

  public themeSubscription = new Subject<string>();

  constructor(
    private http: HttpClient,
    private localeService: LocaleService,
    private chartThemeService: ChartThemeService
  ) {
    const isTheme = !!this.getTheme();
    if (isTheme) {
      this.initTheme();
    } else {
      const localeSubscription = this.localeService.localeChanged.subscribe(
        () => {
          this.initTheme();
          localeSubscription.unsubscribe();
        }
      );
    }
  }

  initTheme() {
    const theme = this.getTheme();
    theme && this.changeTheme(theme);
  }

  changeTheme(themeName: string) {
    this.selectedTheme = themeName;
    this.themeSubscription.next(themeName);
    this.changeChartsTheme(themeName);
    const typ = document.createAttribute("theme");
    typ.value = Themes[themeName];
    document.body.attributes.setNamedItem(typ);
  }

  changeChartsTheme(themeName) {
    let overrides: ChartOptions;
    if (Themes[themeName] === Themes.DARK) {
      overrides = OVERRIDES_CHART_THEME;
    } else {
      overrides = {};
    }
    this.chartThemeService.setColorschemesOptions(overrides);
  }

  saveTheme(themeName: string) {
    this.changeTheme(themeName);
    this.updateThemeConfigs(themeName);
  }

  updateThemeConfigs(theme: string): void {
    localStorage.setItem("theme", theme);
    const accountId = +localStorage.getItem("loggedAccountId");
    this.http
      .put(
        `${UrlUtil.url}/crm-user-service/accounts/${accountId}/configuration/theme`,
        { theme }
      )
      .subscribe();
  }

  getTheme(): string {
    return localStorage.getItem("theme");
  }
}
