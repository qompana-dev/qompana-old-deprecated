const FONT_DARK_COLOR = "#a4a4a4";
const GRID_DARK_COLOR = "#505050";

export const OVERRIDES_CHART_THEME = {
  legend: {
    labels: { fontColor: FONT_DARK_COLOR },
  },
  scales: {
    xAxes: [
      {
        ticks: { fontColor: FONT_DARK_COLOR },
        gridLines: { color: GRID_DARK_COLOR },
      },
    ],
    yAxes: [
      {
        ticks: { fontColor: FONT_DARK_COLOR },
        gridLines: { color: GRID_DARK_COLOR },
      },
    ],
  },
};
