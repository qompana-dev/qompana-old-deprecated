import { Themes } from "./theme.model";
import { Component } from "@angular/core";
import { ThemeService } from "./theme.service";

@Component({
  selector: "app-theme",
  templateUrl: "theme.component.html",
  styleUrls: ["theme.component.scss"],
})
export class ThemeComponent {
  themeNames = this.getThemesList();

  constructor(private themeService: ThemeService) {}

  changeTheme(themeName) {
    this.themeService.saveTheme(themeName);
  }

  getThemesList() {
    return Object.keys(Themes);
  }
}
