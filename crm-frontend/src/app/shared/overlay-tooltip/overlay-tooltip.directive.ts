import {
  Directive,
  Input,
  ElementRef,
  HostListener,
  TemplateRef,
  ViewContainerRef,
  OnInit,
  OnDestroy,
} from "@angular/core";

import {
  Overlay,
  OverlayPositionBuilder,
  OverlayRef,
} from "@angular/cdk/overlay";

import { TemplatePortal } from "@angular/cdk/portal";

@Directive({
  selector: "[overlayTooltip]",
})
export class OverlayTooltipDirective implements OnInit, OnDestroy {
  @Input() tooltipContent: TemplateRef<any>;
  @Input() tooltipPosition = {};
  @Input() tooltipDisabled = false;
  @Input() tooltipPayload: any;

  private overlayRef: OverlayRef;

  constructor(
    private overlay: Overlay,
    private overlayPositionBuilder: OverlayPositionBuilder,
    private elementRef: ElementRef,
    private viewContainerRef: ViewContainerRef
  ) {}

  ngOnInit() {
    const positionStrategy = this.overlayPositionBuilder
      .flexibleConnectedTo(this.elementRef)
      .withPositions([
        {
          originX: "center",
          originY: "top",
          overlayX: "center",
          overlayY: "bottom",
          ...this.tooltipPosition,
        },
      ]);
    this.overlayRef = this.overlay.create({ positionStrategy });
  }

  ngOnDestroy() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
    }
  }

  @HostListener("mouseenter")
  show(e) {
    if (this.tooltipContent && !this.tooltipDisabled) {
      this.overlayRef.attach(
        new TemplatePortal(
          this.tooltipContent,
          this.viewContainerRef,
          this.tooltipPayload
        )
      );
    }
  }

  @HostListener("mouseleave")
  hide() {
    if (this.overlayRef) {
      this.overlayRef.detach();
    }
  }
}
