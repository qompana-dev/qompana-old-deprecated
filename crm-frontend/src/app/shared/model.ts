export enum FilterStrategy {
  ALL = 'all',
  OPENED = 'opened',
  CREATED_TODAY = 'createdToday',
  VIEWED_LASTLY = 'viewedLastly',
  EDITED_LASTLY = 'editedLastly',
  FOR_APPROVING = 'forApproving',
  CLOSED_CLOSED = 'closedClosed',
  CLOSED_ARCHIVED = 'closedArchived',
  CONVERTED = 'converted',
  FINISHED_SUCCESS = 'finishedSuccess',
  FINISHED_ERROR = 'finishedError',
  ARCHIVED = 'archived',
  CUSTOMER = 'customer',
  CONTACT = 'contact'
}

export class TimePickerHour {
  id: number;
  hour: number;
  minute: number;
  value: string;
}
