import { takeUntil } from "rxjs/operators";
import {
  Component,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  EventEmitter,
} from "@angular/core";
import { Subject } from "rxjs";
import { DictionaryStoreService } from "src/app/dictionary/dictionary-store.service";

export interface PhoneType {
  id: number;
  name: string;
}

@Component({
  selector: "app-phone-type-menu",
  templateUrl: "./phone-type-menu.component.html",
  styleUrls: ["./phone-type-menu.component.scss"],
})
export class PhoneTypeMenuComponent implements OnInit, OnDestroy {
  PHONE_TYPE_DICT_ID = 3;

  @ViewChild("menu") menu;
  @Output() selectType = new EventEmitter<PhoneType>();
  private componentDestroyed: Subject<void> = new Subject();

  phoneTypes: PhoneType[] = [];

  constructor(
    private dictionariesService: DictionaryStoreService
  ) {}

  ngOnInit() {
    this.getPhoneTypes();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  hendleClick(type) {
    this.selectType.emit(type);
  }

  private getPhoneTypes() {
    this.dictionariesService
      .getDictionaryWordsById(this.PHONE_TYPE_DICT_ID)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((words) => {
        this.phoneTypes = words.map((word) => {
          return {
            id: word["id"],
            name: word["name"],
          };
        });
      });
  }
}
