import { Component, EventEmitter, Output, forwardRef } from '@angular/core';
import {
  FormArray,
  FormControl,
  FormGroup,
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  Validators,
  NG_VALIDATORS,
  Validator,
  AbstractControl,
  ValidationErrors,
} from '@angular/forms';
import {PhoneService} from './phone.service';

@Component({
  selector: 'app-phone-fields',
  templateUrl: './phone-fields.component.html',
  styleUrls: ['./phone-fields.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PhoneFieldsComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => PhoneFieldsComponent),
      multi: true,
    },
  ],
})
export class PhoneFieldsComponent implements ControlValueAccessor, Validator {
  @Output() focusOut = new EventEmitter<FocusEvent>();
  @Output() focusIn = new EventEmitter<FocusEvent>();

  phones = new FormArray([]);
  types = [];

  constructor(public phoneService: PhoneService) {}

  onChange = (_: any) => {};
  onValidatorChange = (_: any) => {};

  validate(control: AbstractControl): ValidationErrors {
    const { invalid } = this.phones;
    return invalid ? { invalid } : null;
  }

  registerOnValidatorChange?(fn: () => void): void {
    this.onValidatorChange = fn;
  }

  writeValue(value = []) {
    this.types = value.map(({ type }) => type);
    const newPhonesValue = value.map(({ phone }) =>
      this.getPhoneControl(phone)
    );
    this.phones = new FormArray(newPhonesValue);
  }

  registerOnChange(fn: (_: any) => void) {
    this.onChange = fn;
  }

  registerOnTouched() {}

  addPhone(type) {
    this.focusIn.emit();
    this.types.push(type);
    this.phones.push(this.getPhoneControl());
    this.onChangeValue();
  }

  getPhoneControl(phone?: string) {
    const control = new FormControl(phone || '', [Validators.required]);
    control.markAsTouched();
    return control;
  }

  removePhone(event, index) {
    event.stopPropagation();
    this.focusIn.emit();

    this.phones.removeAt(index);
    this.types.splice(index, 1);
    this.onChangeValue();
  }

  onBlur() {
    this.onChangeValue();
  }

  onFocus() {
    this.focusIn.emit();
  }

  onChangeValue() {
    const newValue = this.phones.value.map((phone, i) => ({
      phone,
      type: this.types[i],
    }));
    this.onChange(newValue);
    this.focusOut.emit();
  }
}
