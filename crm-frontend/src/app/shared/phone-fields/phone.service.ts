import {Injectable} from '@angular/core';
import {AdditionalPhoneNumber} from '../additional-phone-numbers/additional-phone-numbers.model';

@Injectable({
  providedIn: 'root'
})
export class PhoneService {

  constructor() {
  }

  getPhoneInputMask(phone: string): string {
    const phoneRegex = /^48/;
    if (phone !== null && phone !== undefined) {
      if (phone.match(phoneRegex)) {
        return '+00 0*';
      } else {
        return '0*';
      }
    }
  }

  mapAdditionalPhones(additionalPhones: AdditionalPhoneNumber[]): AdditionalPhoneNumber[] {
    return additionalPhones.map((rawData) => {
      const phoneOrNumber = rawData.phone ? rawData.phone : rawData.number;
      return {
        type: {
          id: rawData.type.id,
          name: rawData.type.name,
        },
        phone: phoneOrNumber,
        number: phoneOrNumber,
      };
    });
  }
}
