import {FileItem, FileUploader} from 'ng2-file-upload';
import {LoginService} from '../../login/login.service';
import {UrlUtil} from '../url.util';

export class ImportService extends FileUploader {

  static readonly types: { type: ImportUrlType, url: string }[] = [
    {type: 'lead', url: `${UrlUtil.url}/crm-business-service/leads/import`}
  ]

  constructor(private loginService: LoginService, private type: ImportUrlType) {
    super({url: ImportService.getUrl(type)})
  }

  uploadItem(item: FileItem): void {
    if (!this.loginService.isLoggedIn()) {
      this.loginService.getRefreshTokenSubscription().subscribe(() => {
        super.uploadItem(item);
      });
    } else {
      super.uploadItem(item);
    }
  }

  onBeforeUploadItem(fileItem: FileItem): any {
    fileItem.headers = [{name: 'Authorization', value: 'Bearer ' + this.loginService.getToken()}];
    fileItem.withCredentials = false;
  }

  private static getUrl(type: ImportUrlType): string {
    const result = ImportService.types.find(item => item.type === type);
    if (result == null) {
      console.error(`url for type ${type} not found`);
      return undefined;
    }
    return result.url;
  }
}

export type ImportUrlType = 'lead';
//ex. export type ImportUrlType = 'lead' | 'opportunity' | 'contact';
//next add new types in ImportService.types

