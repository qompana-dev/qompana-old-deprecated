export class ParseExceptionDetailDto {
  line: number;
  field: string;
  value: string;
}
