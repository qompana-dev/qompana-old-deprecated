import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'consoleLog'
})
export class ConsoleLogPipe implements PipeTransform {

  transform(value: any, msg?: string): any {
    if (msg) {
      console.log(msg, value);
    }
    else {
      console.log(value);
    }
    return value;
  }

}
