import { Pipe, PipeTransform } from '@angular/core';
import {CurrencyService} from '../currency.service';

@Pipe({
  name: 'getCurrencySymbol'
})
export class GetCurrencySymbolPipe implements PipeTransform {

  constructor(public currencyService: CurrencyService) {
  }


  transform(currency: string): any {
    return this.currencyService.getCurrencySymbol(currency);
  }

}
