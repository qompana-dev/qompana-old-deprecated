import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'autocompleteValueFilter'
})
export class AutocompleteValueFilterPipe implements PipeTransform {

  transform(value: any[], searchValue: string, property?: string): any[] {
    if (!value) { return []; }
    if (!searchValue) {
      searchValue = '';
    }
    searchValue = searchValue.toLowerCase();
    return value.filter((item) => {
      const toFilter = (property) ? item[property] : item;
      return toFilter.toLowerCase().includes(searchValue);
    });
  }

}
