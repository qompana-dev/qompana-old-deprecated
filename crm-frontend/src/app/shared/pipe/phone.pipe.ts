import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'phone'
})
export class PhonePipe implements PipeTransform {
  constructor() {

  }
  transform(value: string, args?: any): any {
    if (value === undefined || value === null || value.length === 0) {
      return '';
    }
    return value;
  }
}
