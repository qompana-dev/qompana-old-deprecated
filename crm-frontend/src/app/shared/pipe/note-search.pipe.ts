import { Pipe, PipeTransform } from "@angular/core";
import { Note } from "../../widgets/notes/notes.model";

@Pipe({
  name: "noteSearch",
})
export class NoteSearchPipe implements PipeTransform {
  constructor() {}

  transform(notes: Note[], searchText?: string): Note[] {
    if (!notes) return null;
    if (!searchText) return notes;

    searchText = searchText.toLowerCase();

    return notes.filter((note) => {
      return note.content.toLowerCase().includes(searchText);
    });
  }
}
