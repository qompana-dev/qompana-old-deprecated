import {Pipe, PipeTransform} from '@angular/core';
import {FileGroupType} from '../../files/file.model';
import {CrmObject, CrmObjectType} from '../model/search.model';

@Pipe({
  name: 'fileGroupToIcon'
})
export class FileGroupToIconPipe implements PipeTransform {
  constructor() {

  }

  transform(fileGroup: string | FileGroupType | CrmObjectType): string {
    console.log('FileGroupToIconPipe', fileGroup);
    if (!fileGroup) {
      return 'assets/icons/question_mark.svg';
    } else if (fileGroup === 'contact') {
      return 'assets/icons/search/contact.svg';
    } else if (fileGroup === 'customer') {
      return 'assets/icons/search/customer.svg';
    } else if (fileGroup === 'docs') {
      return 'assets/icons/search/docs.svg';
    } else if (fileGroup === 'lead') {
      return 'assets/icons/search/leads.svg';
    } else if (fileGroup === 'opportunity') {
      return 'assets/icons/search/opportunity.svg';
    } else if (fileGroup === 'priceBook') {
      return 'assets/icons/search/priceBook.svg';
    } else if (fileGroup === 'product') {
      return 'assets/icons/search/product.svg';
    } else {
      return 'assets/icons/question_mark.svg';
    }
  }

}
