import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'limitToLength'
})
export class LimitToLengthPipe implements PipeTransform {

  constructor() {
  }

  transform(text: string, limit: number): string {
    if(text.length > limit)
      return text.substring(0,limit) + "...";

    return text;
  }
}
