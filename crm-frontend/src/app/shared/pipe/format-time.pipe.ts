import { Pipe, PipeTransform } from '@angular/core';
import {DateUtil} from '../util/date.util';

@Pipe({
  name: 'formatTime'
})
export class FormatTimePipe implements PipeTransform {

  transform(date: string): string {
    if (!date) {
      return '';
    }
    return DateUtil.formatTime(date);
  }

}
