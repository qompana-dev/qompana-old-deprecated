import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enumToList'
})
export class EnumToListPipe implements PipeTransform {

  transform(value: any): any[] {
    return Object.keys(value);
  }

}
