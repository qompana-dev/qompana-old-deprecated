import { Pipe, PipeTransform } from '@angular/core';
import * as isNil from 'lodash/isNil';

@Pipe({
  name: 'isNil'
})
export class IsNilPipe implements PipeTransform {

  transform(value: any): boolean {
    return isNil(value);
  }

}
