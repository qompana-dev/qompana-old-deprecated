import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'formatFileSize'
})
export class FormatFileSizePipe implements PipeTransform {

  constructor() {
  }

  transform(size: number): string {
    if(!size) {
      return this.transformSize(0, 'B');
    }
    if (size > FileSizeType.MB1) {
      return this.transformSize(size/FileSizeType.MB1, 'MB');
    }
    if (size > FileSizeType.KB1) {
      return this.transformSize(size/FileSizeType.KB1, 'KB');
    }
    return this.transformSize(size, 'B');
  }

  private transformSize(size: number, type: string) {
    return '' + Math.ceil(size) + ' ' + type;
  }
}

enum FileSizeType {
  B1 = 0,
  KB1 = 1024,
  MB1 = 1024*KB1
}
