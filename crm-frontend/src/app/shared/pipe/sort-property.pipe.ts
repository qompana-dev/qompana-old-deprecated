import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'sortProperty'
})
export class SortPropertyPipe implements PipeTransform {

  transform(value: any[], property: string, asc: boolean): any[] {
    return value.sort((a, b) => {
      return this.sort(this.getValue(a, property), this.getValue(b, property), asc);
    });
  }

  private sort(a: any, b: any, asc: boolean): number {
    if (a < b) {
      return (asc) ? -1 : 1;
    } else if (a > b) {
      return (asc) ? 1 : -1;
    }
    return 0;
  }

  private getValue(item: any, property: string): any {
    if (property.indexOf('.') !== -1) {
      const keys: string[] = property.split('.');
      return this.getValue(item[keys[0]], keys.slice(1, keys.length).join('.'));
    }
    return item[property];
  }
}
