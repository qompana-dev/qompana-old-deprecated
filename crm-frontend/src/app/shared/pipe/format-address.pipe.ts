import {Pipe, PipeTransform} from '@angular/core';
import { Address } from 'src/app/customer/customer.model';

@Pipe({
  name: 'formatAddress'
})
export class FormatAddressPipe implements PipeTransform {

  transform(address: Address): string {
    if(address) {
      const {street, zipCode, city} = address;
      return [street, zipCode, city].filter(i => i).join(", ")
    }
    return null;
  }
}
