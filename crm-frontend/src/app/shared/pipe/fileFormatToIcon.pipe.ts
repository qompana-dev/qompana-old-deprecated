import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'fileFormatToIcon'
})
export class FileFormatToIconPipe implements PipeTransform {
  constructor() {

  }

  transform(fileFormat: string): string {
    if (!fileFormat) {
      return 'assets/icons/files/unknown-file.svg';
    } else if (fileFormat.endsWith('ppt') || fileFormat.endsWith('pptx')) {
      return 'assets/icons/files/presentation-file.svg';
    } else if (fileFormat.endsWith('doc') || fileFormat.endsWith('docx')) {
      return 'assets/icons/files/word-file.svg';
    } else if (fileFormat.endsWith('xls') || fileFormat.endsWith('xlsx')) {
      return 'assets/icons/files/excel-file.svg';
    } else if (fileFormat.endsWith('jpg') || fileFormat.endsWith('jpeg') || fileFormat.endsWith('png') || fileFormat.endsWith('gif')) {
      return 'assets/icons/files/image-file.svg';
    } else if (fileFormat.endsWith('pdf')) {
      return 'assets/icons/files/pdf-file.svg';
    } else {
      return 'assets/icons/files/unknown-file.svg';
    }
  }

}
