import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lengthMoreThan'
})
export class LengthMoreThanPipe implements PipeTransform {

  transform(text: any, max: number): any {
    return text && text.length > max ? text : '';
  }
}
