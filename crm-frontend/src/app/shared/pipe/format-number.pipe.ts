import {Pipe, PipeTransform} from '@angular/core';
import {DecimalPipe} from '@angular/common';

@Pipe({
  name: 'formatNumber'
})
export class FormatNumberPipe extends DecimalPipe implements PipeTransform {

  constructor() {
    super('fr');
  }

  transform(value: any): string {
    return super.transform(value, '1.2', 'fr');
  }
}
