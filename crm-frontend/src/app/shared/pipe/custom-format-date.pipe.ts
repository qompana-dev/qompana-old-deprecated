import {Pipe, PipeTransform} from '@angular/core';
import {DateUtil} from '../util/date.util';

@Pipe({
    name: 'customFormatDate'
})
export class CustomFormatDatePipe implements PipeTransform {

    transform(date: string, format: string): string {
        if (!date) {
            return '';
        }
        return DateUtil.customFormatDate(date, format);
    }

}
