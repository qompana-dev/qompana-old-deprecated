import {Pipe, PipeTransform} from '@angular/core';
import {Note} from '../../widgets/notes/notes.model';

@Pipe({
  name: 'noteFilter'
})
export class NoteFilterPipe implements PipeTransform {
  constructor() {

  }

  getSortingTypeByDate(values: Note[]): string {
    if (values && values.length > 1) {
      const firstElDate = new Date(values[0].created);
      const lastElDate = new Date(values[values.length - 1].created);
  
      if (firstElDate.getTime() - lastElDate.getTime() > 0) return 'desc';
      else return 'asc';
    }
  }

  transform(values: Note[], sort: string, createdBy: number, searchText: string): Note[] {
    if (searchText.length !== 0) {
      const filtered = (!values) ? values : values.filter(note => note.content.indexOf(searchText) !== -1);
      if (sort === 'user') {
        return createdBy ? filtered.filter(note => +note.createdBy === createdBy) : filtered;
      } else if (sort === this.getSortingTypeByDate(values)) {
        return filtered;
      } else { return filtered.reverse(); }
    } else {
      if (sort === 'user') {
        return createdBy ? values.filter(note => +note.createdBy === createdBy) : values;
      } else if (sort === this.getSortingTypeByDate(values)) {
        return values;
      } else { return values.reverse(); }
    }
  }
}
