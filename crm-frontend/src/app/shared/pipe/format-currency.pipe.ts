import {Pipe, PipeTransform} from '@angular/core';
import {FormatNumberPipe} from './format-number.pipe';
import {CurrencyService} from '../currency.service';

@Pipe({
  name: 'formatCurrency'
})
export class FormatCurrencyPipe extends FormatNumberPipe implements PipeTransform {

  constructor(public currencyService: CurrencyService) {
    super();
  }

  transform(value: any, currency?: string): string {
    if (value === undefined || value === null) {
      return '';
    }
    const formattedValue: string = super.transform(value);
    return `${formattedValue} ${this.currencyService.getCurrencySymbol(currency)}`;
  }
}
