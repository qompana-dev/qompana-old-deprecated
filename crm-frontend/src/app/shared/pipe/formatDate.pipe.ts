import {Pipe, PipeTransform} from '@angular/core';
import {DateUtil} from '../util/date.util';

@Pipe({
  name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {
  constructor() {

  }

  transform(date: string, withTime: boolean = false): string {
    if (!date) {
      return '';
    }
    return DateUtil.formatDate(date, withTime);
  }

}
