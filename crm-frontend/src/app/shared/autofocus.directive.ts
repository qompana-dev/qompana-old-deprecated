import { AfterContentInit, Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appAutofocus]'
})
export class AutofocusDirective implements AfterContentInit {
  @Input() public autoFocus = true;
  @Input() public autoFocusTime = 500;
  @Input() public autoFocusChildSelector = undefined;

  public constructor(private el: ElementRef) {
  }

  public ngAfterContentInit(): void {
    if (this.autoFocus) {
      let elToFocus = this.el.nativeElement;
      if (this.autoFocusChildSelector) {
        elToFocus = this.el.nativeElement.querySelector(this.autoFocusChildSelector);
      }
      setTimeout(() => elToFocus.focus(), this.autoFocusTime);
    }
  }
}
