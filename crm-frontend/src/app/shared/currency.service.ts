import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  constructor() {
  }

  public getCurrencySymbol(currency: string): string {
    switch (currency) {
      case 'PLN':
        return 'PLN';
      case 'EUR':
        return 'EUR';
      case 'USD':
        return 'USD';
      case 'GBP':
        return 'GBP';
      case 'CNY':
        return 'CNY';
      default:
        return '';
    }
  }
}
