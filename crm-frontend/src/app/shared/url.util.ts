import {environment} from '../../environments/environment';

export class UrlUtil {
  static get url(): string {
    return environment.url;
  }

  static get wsUrl(): string {
    return environment.wsUrl;
  }
}
