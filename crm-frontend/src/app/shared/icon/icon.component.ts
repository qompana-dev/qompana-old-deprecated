import { Component, Input, ViewEncapsulation } from "@angular/core";
@Component({
  selector: "app-icon",
  templateUrl: "./icon.component.html",
  styleUrls: ["./icon.component.scss"]
})
export class IconComponent {
  @Input() svgIcon = null;
  @Input() size = "24px";
  @Input() hover = true;
  @Input() disabled = false;
  @Input() pointer = true;
  @Input() primary = false;
  @Input() stroke = false;
  constructor() {}
}
