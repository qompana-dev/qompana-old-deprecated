import { MatIconRegistry } from "@angular/material";
import { NgModule } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import ICONS from "./icons";

@NgModule()
export class IconModule {
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    Object.keys(ICONS).forEach((key) => {
      iconRegistry.addSvgIcon(
        key,
        sanitizer.bypassSecurityTrustResourceUrl(ICONS[key])
      );
    });
  }
}
