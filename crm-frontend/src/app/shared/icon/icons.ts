export default {
  add: "assets/icons/common/add.svg",
  archive: "assets/icons/common/archive.svg",
  "block-list": "assets/icons/common/block-list.svg",
  check: "assets/icons/common/check.svg",
  collapse: "assets/icons/common/collapse.svg",
  cross: "assets/icons/common/cross.svg",
  close: "assets/icons/common/close.svg",
  copy: "assets/icons/common/copy.svg",
  'circle-plus': 'assets/icons/products/plus-icon.svg',
  edit: "assets/icons/common/edit.svg",
  expand: "assets/icons/common/expand.svg",
  export: "assets/icons/common/export.svg",
  external: "assets/icons/common/external.svg",
  "gps-line": "assets/icons/common/gps-line.svg",
  import: "assets/icons/common/import.svg",
  info: "assets/icons/common/info.svg",
  warning: "assets/icons/common/warning.svg",
  internal: "assets/icons/common/internal.svg",
  "light-bulb": "assets/icons/common/light-bulb.svg",
  "option-dots": "assets/icons/common/option_dots.svg",
  refresh: "assets/icons/common/refresh.svg",
  trash: "assets/icons/common/trash.svg",
  transfer: "assets/icons/common/transfer.svg",
  tick: "assets/icons/common/tick.svg",
  ring: "assets/icons/common/ring.svg",
  rename: "assets/icons/common/rename.svg",
  plus: "assets/icons/common/plus.svg",
  minus: "assets/icons/common/minus.svg",
  bulb: "assets/icons/common/bulb.svg",
  "plus-circle": "assets/icons/common/plus-circle.svg",
  search: "assets/icons/common/search.svg",
  gear: "assets/icons/common/gear.svg",
  envelope: "assets/icons/common/envelope.svg",
  update: "assets/icons/common/update.svg",
  preview: "assets/icons/common/preview.svg",
  "person-active": "assets/icons/common/person-active.svg",
  "person-disabled": "assets/icons/common/person-disabled.svg",
  "delivery-active": "assets/icons/common/delivery-active.svg",
  "delivery-disabled": "assets/icons/common/delivery-disabled.svg",
  download: "assets/icons/common/download.svg",
  share: "assets/icons/common/share.svg",
  menu: "assets/icons/common/menu.svg",
  pin: "assets/icons/common/pin.svg",
  'pin-filled': 'assets/icons/common/pin-filled.svg',
  'plus-circle-b': "assets/icons/common/plus-circle-b.svg",
  'minus-circle-b': "assets/icons/common/minus-circle-b.svg",
  history: "assets/icons/common/history.svg",
  "expand-more": "assets/icons/common/arrow/expand-more.svg",
  "left-arrow": "assets/icons/common/arrow/left-arrow.svg",
  "right-arrow": "assets/icons/common/arrow/right-arrow.svg",
  "left-arrow-w": "assets/icons/common/arrow/left-arrow-w.svg",
  "right-arrow-w": "assets/icons/common/arrow/right-arrow-w.svg",
  "growth-arrow": "assets/icons/common/arrow/growth-arrow.svg",
  "right-arrow-circle": "assets/icons/common/arrow/right-arrow-circle.svg",
  'bottom-arrow-circle': "assets/icons/common/arrow/bottom-arrow-circle.svg",
  BPMN: "assets/icons/common/menu/BPMN.svg",
  Calendar: "assets/icons/common/menu/Calendar.svg",
  Contacts: "assets/icons/common/menu/Contacts.svg",
  Customers: "assets/icons/common/menu/Customers.svg",
  Dashboard: "assets/icons/common/menu/Dashboard.svg",
  Documents: "assets/icons/common/menu/Documents.svg",
  Goals: "assets/icons/common/menu/Goals.svg",
  Suppliers: "assets/icons/common/menu/Suppliers.svg",
  Email: "assets/icons/common/menu/Email.svg",
  Leads: "assets/icons/common/menu/Leads.svg",
  Opportunity: "assets/icons/common/menu/Opportunity.svg",
  PriceBook: "assets/icons/common/menu/PriceBook.svg",
  Products: "assets/icons/common/menu/Products.svg",
  logo: "assets/icons/common/system/logo.svg",
  event: "assets/icons/calendar/event-activity.svg",
  mail: "assets/icons/calendar/mail-activity.svg",
  meeting: "assets/icons/calendar/meeting-activity.svg",
  phone: "assets/icons/calendar/phone-activity.svg",
  task: "assets/icons/calendar/task-activity.svg",
  'group-icon': "assets/icons/notifications/group_icon.svg",
  'import-ok': "assets/icons/import/validation/ok.svg",
  'import-incorrect': "assets/icons/import/validation/incorrect.svg",
  'import-duplicate': "assets/icons/import/validation/duplicate.svg",
  'yellow-star': "assets/icons/yellow-star.svg",
  'grey-star': "assets/icons/grey-star.svg",
  'archive-yellow': "assets/icons/common/archive-yellow.svg",
  file: "assets/icons/add-file.svg",
  notes: "assets/icons/add-notes.svg",
  timestamp: "assets/icons/timestamp.svg",
  alertRed: "assets/icons/alert-red.svg",
  alertYellow: "assets/icons/alert-yellow.svg"
};
