import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AvatarFormComponent} from './form/avatar-form.component';
import {SharedModule} from '../shared.module';
import { AvatarPreviewComponent } from './preview/avatar-preview.component';

@NgModule({
  declarations: [
    AvatarFormComponent,
    AvatarPreviewComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    AvatarFormComponent,
    AvatarPreviewComponent
  ]
})
export class AvatarModule { }
