import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-avatar-preview',
  templateUrl: './avatar-preview.component.html',
  styleUrls: ['./avatar-preview.component.scss']
})
export class AvatarPreviewComponent implements OnInit {
  @Input() imageSrc: string;
  @Input() defaultImg = 'assets/icons/user-logo.svg';
  @Input() widthPx = 100;
  @Input() rounded: boolean = true;

  correctLogo = true;

  constructor() {
  }

  ngOnInit(): void {
  }

  logoLoaded(error: boolean): void {
    this.correctLogo = !error;
  }
}
