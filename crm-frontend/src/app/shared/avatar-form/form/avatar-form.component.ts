import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {AbstractControl} from '@angular/forms';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-avatar-form',
  templateUrl: './avatar-form.component.html',
  styleUrls: ['./avatar-form.component.scss']
})
export class AvatarFormComponent implements OnChanges, OnDestroy {
  @Input() avatar: AbstractControl;
  @Input() widthPx = 100;
  @Input() text = 'avatar.add';
  @Input() defaultImg = 'assets/icons/user-logo.svg';
  @Input() rounded: boolean = true;

  @Output() valueChanged: EventEmitter<string> = new EventEmitter<string>();

  imageSrc = '';
  correctLogo = true;

  avatarChangeSubscription: Subscription;
  private componentDestroyed: Subject<void> = new Subject();

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.subscribeAvatarChange();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  logoLoaded(error: boolean): void {
    this.correctLogo = !error;
    this.avatar.updateValueAndValidity();
  }

  handleInputChange(event: any): void {
    const file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
    const reader = new FileReader();
    if (!file.type.match(/image-*/)) {
      console.error('invalid format');
      return;
    }
    reader.onload = (e: any) => {
      console.log(e, 'reader.onload');
      this.imageSrc = e.target.result;
      this.avatar.patchValue(this.imageSrc);
      this.valueChanged.emit(this.avatar.value);
    };
    reader.readAsDataURL(file);
  }

  private subscribeAvatarChange(): void {
    if (this.avatarChangeSubscription) {
      this.avatarChangeSubscription.unsubscribe();
    }
    if (this.avatar) {
      this.avatarChangeSubscription = this.avatar.valueChanges
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => this.setValues());
    }
  }

  private setValues(): void {
    if (this.avatar && this.avatar.value) {
      this.imageSrc = this.avatar.value;
      this.correctLogo = (this.imageSrc != null);
    }
  }
}
