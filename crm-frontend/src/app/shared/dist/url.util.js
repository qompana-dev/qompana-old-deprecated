"use strict";
exports.__esModule = true;
exports.UrlUtil = void 0;
var environment_1 = require("../../environments/environment");
var UrlUtil = /** @class */ (function () {
    function UrlUtil() {
    }
    Object.defineProperty(UrlUtil, "url", {
        get: function () {
            return environment_1.environment.url;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UrlUtil, "wsUrl", {
        get: function () {
            return environment_1.environment.wsUrl;
        },
        enumerable: false,
        configurable: true
    });
    return UrlUtil;
}());
exports.UrlUtil = UrlUtil;
