import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup} from '@angular/forms';
import {DateUtil} from '../../util/date.util';
import {DurationValidatorsUtil} from '../../util/duration-validators.util';

@Component({
  selector: 'app-date-range-select',
  templateUrl: './date-range-select.component.html',
  styleUrls: ['./date-range-select.component.scss']
})
export class DateRangeSelectComponent implements OnInit {


  public dateRangeForm: FormGroup;
  dateUtil = DateUtil;
  monthList: { number: number, name: string }[] = DateUtil.getMonthsList();

  @Input() ongoing = false;

  @Input() set startDate(startDate: string) {
    if (startDate) {
      this.startDateMonthControl.patchValue(DateUtil.getMonthNumber(startDate));
      this.startDateYearControl.patchValue(DateUtil.getYear(startDate));
    }
  }

  constructor(private fb: FormBuilder) {
    this.createEmploymentForm();
  }

  ngOnInit(): void {
  }

  // @formatter:off
  get startDateMonthControl(): AbstractControl {return this.dateRangeForm.get('startDateMonth'); }
  get startDateYearControl(): AbstractControl {return this.dateRangeForm.get('startDateYear'); }
  get endDateMonthControl(): AbstractControl {return this.dateRangeForm.get('endDateMonth'); }
  get endDateYearControl(): AbstractControl { return this.dateRangeForm.get('endDateYear'); }
  // @formatter:on


  private createEmploymentForm(): void {
    this.dateRangeForm = this.fb.group({
      startDateMonth: [undefined],
      startDateYear: [undefined],
      endDateMonth: [undefined],
      endDateYear: [undefined],
    }, {
      validators: [
        DurationValidatorsUtil.getDateChronologyValidator(),
        DurationValidatorsUtil.getDependentFieldValidatorValidator('startDateMonth', 'startDateYear'),
        DurationValidatorsUtil.getDependentFieldValidatorValidator('endDateMonth', 'endDateYear'),
      ]
    });
  }

  public getStartDate(): string {
    return DateUtil.getStringDate(this.startDateMonthControl.value, this.startDateYearControl.value);
  }

  public getEndDate(): string {
    return DateUtil.getStringDate(this.endDateMonthControl.value, this.endDateYearControl.value);
  }

  public isValid(): boolean {
    return this.dateRangeForm && this.dateRangeForm.valid;
  }

  reset(): void {
    this.dateRangeForm.reset();
  }

  enable(enable: boolean): void {
    enable ? this.dateRangeForm.enable() : this.dateRangeForm.disable();
  }
}
