import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DateRangeSelectComponent} from './date-range-select/date-range-select.component';
import {SharedModule} from '../shared.module';

@NgModule({
  declarations: [
    DateRangeSelectComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    DateRangeSelectComponent
  ]
})
export class ContactCustomerRelationSharedModule {
}
