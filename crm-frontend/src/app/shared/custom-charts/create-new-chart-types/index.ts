import * as RoundedBar from "./rounded-bar";

export const initCharts = () => {
  RoundedBar.init();
};
