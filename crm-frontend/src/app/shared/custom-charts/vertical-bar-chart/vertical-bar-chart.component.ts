import {
  CHART_TYPE,
  getChartOptionsByData,
  LABEL_FONT_SIZE,
  CHART_COLORS,
} from "./vertical-bar-chart.config";
import { ChartDataSets, ChartOptions, ChartType } from "chart.js";
import { Color, Label } from "ng2-charts";
import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "app-vertical-bar-chart",
  templateUrl: "./vertical-bar-chart.component.html",
  styleUrls: ["./vertical-bar-chart.component.scss"],
})
export class VerticalBarChartComponent {
  @Input() set datasets(data: ChartDataSets[]) {
    this.chartData = this.getDatasetsByData(data);
    this.widthLeftTicks = this.getWidthLeftTicks(data);
  }

  @Input() set labels(labels: string[]) {
    this.chartLabels = labels;
  }

  @Input() legends: string[];

  public chartType: ChartType = CHART_TYPE;
  public chartLabels: Label[] = [];
  public colors = CHART_COLORS;
  public chartColors: Array<Color> = CHART_COLORS.map((color) => ({
    backgroundColor: color,
  }));

  public chartOptions: ChartOptions & any = getChartOptionsByData();

  public chartData: ChartDataSets[] = [{}];

  private canvasContext = document.createElement("canvas").getContext("2d");
  public widthLeftTicks: number = 0;

  getDatasetsByData(data) {
    return data.map((item) => ({
      ...item,
      ...{
        categoryPercentage: 0.5,
        barPercentage: 0.5,
      },
    }));
  }

  private getWidthLeftTicks(data: any[]) {
    const labelCounts = data
      .map(({ data }) => data)
      .reduce((prevArr, arr) => prevArr.concat(arr), []);
    const textWidthArr = labelCounts.map((item) =>
      this.getTextWidth(item, LABEL_FONT_SIZE)
    );
    return Math.max(...textWidthArr);
  }

  private getTextWidth(text, size, font = "Poppins", weight = "normal") {
    this.canvasContext.font = `${weight} ${size}px ${font}`;
    const { width } = this.canvasContext.measureText(text);

    return width;
  }
}
