export const CHART_TYPE = "bar";
export const CHART_COLORS = [
  "#cdecff",
  "#319df8",
  "#1DC9D7",
  "#84d0ff",
  "#ff8e00",
  "#ffad0c",
  "#ed3758",
];
export const LABEL_FONT_SIZE = 10;

export const getChartOptionsByData = (charData?) => ({
  cornerRadius: 10,
  responsive: true,
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        ticks: {
          min: 0,
          fontFamily: "Poppins",
          fontSize: LABEL_FONT_SIZE,
          precision: 0,
        },
        gridLines: {
          display: false,
          zeroLineWidth: 0,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          padding: 8,
          fontFamily: "Poppins",
          min: 0,
          fontSize: LABEL_FONT_SIZE,
        },
        gridLines: {
          drawTicks: false,
          drawBorder: false,
        },
      },
    ],
  },
});
