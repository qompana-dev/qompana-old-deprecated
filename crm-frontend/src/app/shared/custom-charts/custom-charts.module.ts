import { VerticalBarChartComponent } from './vertical-bar-chart/vertical-bar-chart.component';
import { ChartsModule } from "ng2-charts";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DoughnutChartComponent } from "./doughnut-chart/doughnut-chart.component";
import { RoundedBarChartComponent } from "./rounded-bar-chart/rounded-bar-chart.component";
import { initCharts } from './create-new-chart-types';
import { AngularResizedEventModule } from 'angular-resize-event';


initCharts();

const newCharts = [DoughnutChartComponent, RoundedBarChartComponent, VerticalBarChartComponent]

@NgModule({
  declarations: [...newCharts],
  imports: [CommonModule, ChartsModule, AngularResizedEventModule],
  exports: [...newCharts],
})
export class CustomChartsModule {}
