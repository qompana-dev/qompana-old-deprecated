import {
  CHART_TYPE,
  getChartOptionsByData,
  CHART_COLORS,
} from "./rounded-bar-chart.config";
import { RoundedBarData } from "./rounded-bar-chart.model";
import { ChartDataSets, ChartOptions, ChartType } from "chart.js";
import { Color, Label } from "ng2-charts";
import { Component, Input } from "@angular/core";

@Component({
  selector: "app-rounded-bar-chart",
  templateUrl: "./rounded-bar-chart.component.html",
  styleUrls: ["./rounded-bar-chart.component.scss"],
})
export class RoundedBarChartComponent {
  @Input() set data(data: RoundedBarData[]) {
    this.chartLabels = this.getLabelsByData(data);
    this.chartData = this.getDatasetsByData(data);
    this.chartColors = this.getColorsByData(data);
    this.chartOptions = getChartOptionsByData(data);
    this.widthLeftTicks = this.getWidthLeftTicks(data);
  }

  @Input() set length(length) {
    this.setAxesLength(length);
  }

  @Input() yLabel: string = "";
  @Input() xLabel: string = "";

  public chartOptions: ChartOptions & any = getChartOptionsByData();
  public chartLabels: Label[] = [];
  public chartType: ChartType = CHART_TYPE;
  public chartColors: Array<Color> = [{}];
  public chartData: ChartDataSets[] = [];

  private canvasContext = document.createElement("canvas").getContext("2d");
  public widthLeftTicks: number = 0;

  getDatasetsByData(data): ChartDataSets[] {
    if (!data.length) {
      return [{}];
    }
    const datasetsData = data.map((item) => item.xCount);
    return [{ data: datasetsData, maxBarThickness: 42 }];
  }

  getLabelsByData(data): Label[] {
    return data.map((item) => item.yCount);
  }

  getColorsByData(data): Color[] {
    if (!data.length) {
      return [{}];
    }
    const colorsLength = CHART_COLORS.length;
    const colors = data.map((item, i) => {
      const colorIndex = i % colorsLength;
      return CHART_COLORS[colorIndex];
    });
    return [{ backgroundColor: colors }];
  }

  private setAxesLength(length) {
    const chartOptions = { ...this.chartOptions };
    chartOptions.scales.xAxes[0].ticks.max = length;
    this.chartOptions = chartOptions;
  }

  private getWidthLeftTicks(data: RoundedBarData[]) {
    const textWidthArr = data.map((item) => this.getTextWidth(item.yCount, 13));
    return Math.max(...textWidthArr);
  }

  private getTextWidth(text, size, font = "Poppins", weight = "normal") {
    this.canvasContext.font = `${weight} ${size}px ${font}`;
    const { width } = this.canvasContext.measureText(text);
    return width;
  }
}
