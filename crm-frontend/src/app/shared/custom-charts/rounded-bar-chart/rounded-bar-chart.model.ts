export interface RoundedBarData {
    yCount: number;
    xCount: number;
    labelCount: number;
}