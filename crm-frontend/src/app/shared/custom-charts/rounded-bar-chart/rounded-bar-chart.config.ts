import { Chart } from "chart.js";
export const CHART_TYPE = "horizontalBar";
export const CHART_COLORS = ["#1dc9d7", "#4fdce8"];
export const CHART_CONTRAST_COLOR = "#fff";

export const getChartOptionsByData = (charData?) => ({
  cornerRadius: 10,
  fullCornerRadius: false,

  responsive: true,
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  tooltips: {
    enabled: false,
  },
  hover: {
    animationDuration: 0,
  },
  animation: {
    onComplete: function ({ chart }) {
      if (charData) {
        const ctx = chart.ctx;
        ctx.textAlign = "right";
        ctx.fillStyle = CHART_CONTRAST_COLOR;

        Chart.helpers.each(
          this.data.datasets.forEach((dataset, i) => {
            const meta = chart.controller.getDatasetMeta(i);
            Chart.helpers.each(
              meta.data.forEach((bar, index) => {
                const { height, x, y } = bar._model;
                const fontSize = height / 3;
                ctx.font = `${fontSize}px Poppins`;

                const { labelCount = 0 } = charData[index];
                ctx.fillText(
                  labelCount.toLocaleString("pl"),
                  x - fontSize,
                  y + fontSize / 2 - 1
                );
              })
            );
          })
        );
      }
    },
  },
  scales: {
    xAxes: [
      {
        ticks: {
          min: 0,
          max: 0,
          stepSize: 1,
          fontFamily: "Poppins",
          padding: 16,
          fontSize: 13,
        },
        gridLines: {
          drawTicks: false,
          zeroLineWidth: 0,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          fontFamily: "Poppins",
          padding: 16,
          fontSize: 13,
          reverse: true,
        },
        gridLines: {
          display: false,
          drawTicks: false,
        },
      },
    ],
  },
});
