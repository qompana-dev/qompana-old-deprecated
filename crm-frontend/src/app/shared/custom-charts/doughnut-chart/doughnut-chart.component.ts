import { ChartDataSets, ChartOptions } from "chart.js";
import { Color } from "ng2-charts";
import { Component, Input, ElementRef, ViewChild } from "@angular/core";
import { CHART_OPTION, CHART_TYPE } from "./doughnut-chart.config";

@Component({
  selector: "app-doughnut-chart",
  templateUrl: "./doughnut-chart.component.html",
  styleUrls: ["./doughnut-chart.component.scss"],
})
export class DoughnutChartComponent {
  @ViewChild("canvas") canvas: ElementRef;

  @Input() set data(data: any[]) {
    this.chartDatasets = this.getDatasetsByData(data);
  }

  @Input() set colors(data: string[]) {
    this.chartColors = this.getColorsByData(data);
  }

  @Input() chartLabels: string[] = [];

  detectResize({ newHeight }) {
    this.canvas.nativeElement.style.width = `${newHeight}px`;
  }

  public readonly chartType = CHART_TYPE;

  public chartDatasets: ChartDataSets[] = [];
  public chartColors: Array<Color>;
  public chartOptions: ChartOptions = CHART_OPTION;

  constructor() {}

  ngOnInit(): void {}

  ngOnDestroy(): void {}

  getDatasetsByData(data: number[] = []): ChartDataSets[] {
    return [{ data }];
  }

  getColorsByData(data: any[]) {
    if (!data.length) return [];
    return [
      {
        borderWidth: 0,
        backgroundColor: data,
      },
    ];
  }

  getColor({ color }: any, index: number) {
    return color;
  }
}
