export const CHART_TYPE = "doughnut";

export const CHART_OPTION = {
    responsive: true,
    maintainAspectRatio: false,
    cutoutPercentage: 60,
    legend: {
      display: false,
    },
  };