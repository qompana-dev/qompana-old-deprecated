import {FormControl} from '@angular/forms';
import {ValidationErrors} from '@angular/forms/src/directives/validators';

export class FormValidators {
  static whitespace(control: FormControl): ValidationErrors {
    if (control.value != null && control.value.length > 0) {
      if (control.value[0] === ' ' || control.value[control.value.length - 1] === ' ') {
        return {whitespace: true};
      }
    }

    return null;
  }
}
