import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class FormSaveService {

  constructor() { }

  private formId: string;

  private formChangedSource = new Subject<boolean>();
  private formSavedSource = new Subject<string>();
  private formCanceledSource = new Subject<string>();

  formChanged$ = this.formChangedSource.asObservable();
  formSavedSource$ = this.formSavedSource.asObservable();
  formCanceledSource$ = this.formCanceledSource.asObservable();

  announceFormChanged(formId: string): void {
    this.formId = formId;
    this.formChangedSource.next(true);
  }

  announceFormTheSame(formId: string): void {
    this.formId = formId;
    this.formChangedSource.next(false);
  }

  saveForm(): void {
    this.formSavedSource.next(this.formId);
  }

  cancelForm(): void {
    this.formCanceledSource.next(this.formId);
  }
}
