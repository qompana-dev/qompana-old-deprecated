import { Injectable } from "@angular/core";

import {
  BpmnProcess,
  BpmnType,
  CreateProcessFromFileResponse, PrimaryProcess,
  ProcessDefinition,
  ProcessDefinitionDTO,
} from "./bpmn.model";
import { Observable, Subject } from "rxjs";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { BpmnFilterStrategy } from "./bpmn-filter-strategy";
import { UrlUtil } from "../shared/url.util";
@Injectable({
  providedIn: "root",
})
export class BpmnService {
  addNewPropertyButtonClicked: Subject<BpmnType> = new Subject();
  saveButtonClicked: Subject<void> = new Subject<void>();

  constructor(private http: HttpClient) {}

  getProcessDefinitionList(
    objectType: BpmnFilterStrategy
  ): Observable<ProcessDefinitionDTO[]> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/repository/process-definitions/list/${objectType}`;
    return this.http.get<ProcessDefinitionDTO[]>(url);
  }

  public getProcessDefinitions(
    objectType: string
  ): Observable<ProcessDefinition[]> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/repository/process-definitions/${objectType}`;
    return this.http.get<ProcessDefinition[]>(url);
  }

  getBpmnFile(process: BpmnProcess): Observable<HttpResponse<Blob>> {
    return this.http.post(
      `${UrlUtil.url}/crm-business-service/bpmn/generate`,
      process,
      { observe: "response", responseType: "blob" }
    );
  }

  saveProcess(
    processId: string,
    blob: Blob
  ): Observable<CreateProcessFromFileResponse> {
    const formData = new FormData();
    formData.append("file", blob, `process.bpmn20.xml`);
    const url = `${UrlUtil.url}/crm-business-service/process-engine/repository/deployments`;
    return this.http.post<CreateProcessFromFileResponse>(url, formData);
  }

  getFileByProcessDefinitionId(
    processDefinitionId: string
  ): Observable<HttpResponse<Blob>> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/repository/process-definitions/${processDefinitionId}/bpmn`;
    return this.http.get(url, { observe: "response", responseType: "blob" });
  }

  parseBpmnFile(file: Blob): Observable<BpmnProcess> {
    const formData = new FormData();
    formData.append("file", file, `process.bpmn20.xml`);
    const url = `${UrlUtil.url}/crm-business-service/bpmn/parse`;
    return this.http.post<BpmnProcess>(url, formData);
  }

  deleteProcess(id: string): Observable<void> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/repository/process-definitions/${id}`;
    return this.http.delete<void>(url);
  }

  getAllPrimaryProcesses(): Observable<PrimaryProcess[]> {
    const url = `${UrlUtil.url}/crm-business-service/primary-process`;
    return this.http.get<PrimaryProcess[]>(url);
  }

  specifyPrimaryProcessForType(processDefinitionId: string, type: BpmnFilterStrategy): Observable<PrimaryProcess> {
    const url = `${UrlUtil.url}/crm-business-service/primary-process/id/${processDefinitionId}/type/${type}`;
    return this.http.post<PrimaryProcess>(url, null);
  }

  deletePrimaryProcess(processDefinitionId: string, type: BpmnFilterStrategy): Observable<void> {
    const url = `${UrlUtil.url}/crm-business-service/primary-process/id/${processDefinitionId}/type/${type}`;
    return this.http.delete<void>(url);
  }
}
