"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.TaskFormComponent = void 0;
var core_1 = require("@angular/core");
var bpmn_model_1 = require("../../bpmn.model");
var bpmn_util_1 = require("../../bpmn.util");
var drag_drop_1 = require("@angular/cdk/drag-drop");
var operators_1 = require("rxjs/operators");
var rxjs_1 = require("rxjs");
var TaskFormComponent = /** @class */ (function () {
    function TaskFormComponent(translateService, bpmnService, cdRef) {
        this.translateService = translateService;
        this.bpmnService = bpmnService;
        this.cdRef = cdRef;
        this.MAX_PROPERTIES_COUNT = 8;
        this.process = { objectType: bpmn_model_1.BpmnObjectType.LEAD };
        this.currentPanelValid = true;
        this.processChange = new core_1.EventEmitter();
        this.openPanel = new core_1.EventEmitter();
        this.bpmnTypeEnum = bpmn_model_1.BpmnType;
        this.bpmnUtil = bpmn_util_1.BpmnUtil;
        this.componentDestroyed = new rxjs_1.Subject();
    }
    TaskFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.bpmnService.addNewPropertyButtonClicked.pipe(operators_1.takeUntil(this.componentDestroyed)).subscribe(function (type) {
            _this.addProperty(_this.selectedPanel.task, type);
        });
    };
    TaskFormComponent.prototype.ngOnDestroy = function () {
        this.componentDestroyed.next();
        this.componentDestroyed.complete();
    };
    TaskFormComponent.prototype.ngOnChanges = function (changes) {
        this.addUUIDs();
        if (!this.process.tasks || this.process.tasks.length === 0) {
            this.addTask();
        }
    };
    TaskFormComponent.prototype.ngAfterViewInit = function () {
        this.cdRef.detectChanges();
    };
    TaskFormComponent.prototype.showAddPropertyTipIfNeeded = function () {
        if (this.selectedPanel.task && !this.selectedPanel.task.properties) {
            this.addPropertyTooltip.forEach(function (e) { return e.show(); });
        }
        else {
            this.addPropertyTooltip.forEach(function (e) { return e.hide(); });
        }
    };
    TaskFormComponent.prototype.drop = function (event) {
        if (!this.process.tasks[event.currentIndex].disabled) {
            drag_drop_1.moveItemInArray(this.process.tasks, event.previousIndex, event.currentIndex);
        }
    };
    TaskFormComponent.prototype.getTaskSelectedBgColor = function (task) {
        return this.isTaskSelected(task) ? this.getTaskColor(task) : undefined;
    };
    TaskFormComponent.prototype.getTaskColor = function (task) {
        return bpmn_model_1.AvailableColors.byBgColor(task.disabled ? '#EAECEF' : this.process.bgColor);
    };
    TaskFormComponent.prototype.isTaskSelected = function (task) {
        return this.selectedPanel && this.selectedPanel.type === bpmn_model_1.SelectedPanelType.TASK &&
            this.selectedPanel.task && this.selectedPanel.task.uuid === task.uuid;
    };
    TaskFormComponent.prototype.addProperty = function (task, type) {
        if (this.currentPanelValid) {
            if (type) {
                if (!task.properties) {
                    task.properties = [];
                }
                var property = {
                    id: '',
                    type: bpmn_model_1.BpmnType[type],
                    uuid: bpmn_util_1.BpmnUtil.getUUID(),
                    required: false
                };
                task.properties.push(property);
                this.openPropertyPanel(task, property);
            }
        }
    };
    TaskFormComponent.prototype.addTask = function (addFrom) {
        if (this.currentPanelValid) {
            var newElement = { name: '', documentation: '', uuid: bpmn_util_1.BpmnUtil.getUUID() };
            if (!addFrom) {
                this.process.tasks.push(newElement);
                return;
            }
            var index = this.process.tasks.indexOf(addFrom);
            this.process.tasks.splice(index + 1, 0, newElement);
            this.openPanel.emit({ type: bpmn_model_1.SelectedPanelType.TASK, task: newElement });
        }
    };
    TaskFormComponent.prototype.removeProperty = function (task, property) {
        if (this.process && this.process.tasks) {
            var taskIndex = this.process.tasks.map(function (taskObj) { return taskObj.uuid; }).indexOf(task.uuid);
            if (taskIndex !== -1 && this.process.tasks[taskIndex].properties) {
                if (this.selectedPanel.type === bpmn_model_1.SelectedPanelType.PROPERTY &&
                    this.selectedPanel.property.uuid === property.uuid &&
                    this.selectedPanel.task.uuid === task.uuid) {
                    this.openTaskPanel(task, true);
                }
                this.process.tasks[taskIndex].properties = this.process.tasks[taskIndex].properties
                    .filter(function (propertyObj) { return propertyObj.uuid !== property.uuid; });
            }
        }
    };
    TaskFormComponent.prototype.openPropertyPanel = function (task, property) {
        this.openPanel.emit({ type: bpmn_model_1.SelectedPanelType.PROPERTY, task: task, property: property });
    };
    TaskFormComponent.prototype.openTaskPanel = function (task, force) {
        if (force === void 0) { force = false; }
        this.openPanel.emit({ type: bpmn_model_1.SelectedPanelType.TASK, task: task, force: force });
    };
    TaskFormComponent.prototype.propertyOrTaskSelected = function (task) {
        return this.isTaskSelected(task) || this.propertyInTaskSelected(task);
    };
    TaskFormComponent.prototype.propertyInTaskSelected = function (task) {
        var _this = this;
        if (!task || !task.properties || this.selectedPanel.type !== bpmn_model_1.SelectedPanelType.PROPERTY) {
            return false;
        }
        return task.properties.find(function (property) { return _this.selectedPanel.property === property; }) !== undefined;
    };
    TaskFormComponent.prototype.isPropertySelected = function (property) {
        return this.selectedPanel.property === property;
    };
    TaskFormComponent.prototype.addUUIDs = function () {
        if (this.process && this.process.tasks) {
            this.process.tasks = this.process.tasks.map(function (task) {
                if (!task.uuid) {
                    task.uuid = bpmn_util_1.BpmnUtil.getUUID();
                }
                if (task.properties) {
                    task.properties = task.properties.map(function (property) {
                        if (!property.uuid) {
                            property.uuid = bpmn_util_1.BpmnUtil.getUUID();
                        }
                        return property;
                    });
                }
                return task;
            });
        }
    };
    __decorate([
        core_1.Input()
    ], TaskFormComponent.prototype, "process");
    __decorate([
        core_1.Input()
    ], TaskFormComponent.prototype, "currentPanelValid");
    __decorate([
        core_1.Input()
    ], TaskFormComponent.prototype, "selectedPanel");
    __decorate([
        core_1.Output()
    ], TaskFormComponent.prototype, "processChange");
    __decorate([
        core_1.Output()
    ], TaskFormComponent.prototype, "openPanel");
    __decorate([
        core_1.ViewChildren('propertyNameTooltip')
    ], TaskFormComponent.prototype, "propertyNameTooltip");
    __decorate([
        core_1.ViewChildren('addPropertyTooltip')
    ], TaskFormComponent.prototype, "addPropertyTooltip");
    TaskFormComponent = __decorate([
        core_1.Component({
            selector: 'app-task-form',
            templateUrl: './task-form.component.html',
            styleUrls: ['./task-form.component.scss']
        })
    ], TaskFormComponent);
    return TaskFormComponent;
}());
exports.TaskFormComponent = TaskFormComponent;
