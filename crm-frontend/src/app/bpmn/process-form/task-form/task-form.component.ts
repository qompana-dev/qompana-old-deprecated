import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  ViewChildren
} from '@angular/core';
import {
  AvailableColor,
  AvailableColors,
  BpmnObjectType,
  BpmnProcess,
  BpmnProperty,
  BpmnTask,
  BpmnType,
  SelectedPanel,
  SelectedPanelType
} from '../../bpmn.model';
import { BpmnService } from '../../bpmn.service';
import { TranslateService } from '@ngx-translate/core';
import { BpmnUtil } from '../../bpmn.util';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { TooltipDirective } from 'ngx-bootstrap';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss']
})
export class TaskFormComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  readonly MAX_PROPERTIES_COUNT = 8;
  @Input() process: BpmnProcess = { objectType: BpmnObjectType.LEAD };
  @Input() currentPanelValid = true;
  @Input() selectedPanel: SelectedPanel;

  @Output() processChange: EventEmitter<BpmnProcess> = new EventEmitter();
  @Output() openPanel: EventEmitter<SelectedPanel> = new EventEmitter();

  @ViewChildren('propertyNameTooltip') propertyNameTooltip: QueryList<TooltipDirective>;
  @ViewChildren('addPropertyTooltip') addPropertyTooltip: QueryList<TooltipDirective>;

  bpmnTypeEnum = BpmnType;
  bpmnUtil = BpmnUtil;
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private translateService: TranslateService,
    private bpmnService: BpmnService,
    private cdRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.bpmnService.addNewPropertyButtonClicked.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(type => {
      this.addProperty(this.selectedPanel.task, type);
    });
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.addUUIDs();
    if (!this.process.tasks || this.isEmptyTaskList()) {
      this.addTask();
    }
  }

  isEmptyTaskList() {
    return this.process.tasks.length === 0 || (this.process.tasks.length === 1 && this.process.tasks[0].isInformationTask)
  }

  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  public showAddPropertyTipIfNeeded(): void {
    if (this.selectedPanel.task && !this.selectedPanel.task.properties) {
      this.addPropertyTooltip.forEach(e => e.show());
    } else {
      this.addPropertyTooltip.forEach(e => e.hide());
    }
  }

  drop(event: CdkDragDrop<string[]>): void {
    if (!this.process.tasks[event.currentIndex].isInformationTask) {
      moveItemInArray(this.process.tasks, event.previousIndex, event.currentIndex);
    }
  }

  getTaskSelectedBgColor(task: BpmnTask): AvailableColor {
    return this.isTaskSelected(task) ? this.getTaskColor(task) : undefined;
  }

  getTaskColor(task: BpmnTask): AvailableColor {
    return AvailableColors.byBgColor(task.isInformationTask ? '#EAECEF' : this.process.bgColor)
  }

  isTaskSelected(task: BpmnTask): boolean {
    return this.selectedPanel && this.selectedPanel.type === SelectedPanelType.TASK &&
      this.selectedPanel.task && this.selectedPanel.task.uuid === task.uuid;
  }

  addProperty(task: BpmnTask, type: BpmnType): void {
    if (this.currentPanelValid) {
      if (type) {
        if (!task.properties) {
          task.properties = [];
        }
        const property: BpmnProperty = {
          id: '',
          type: BpmnType[type],
          uuid: BpmnUtil.getUUID(),
          required: false,
        };
        task.properties.push(property);
        this.openPropertyPanel(task, property);
      }
    }
  }

  addTask(addFrom?: BpmnTask): void {
    if (this.currentPanelValid) {
      const newElement: BpmnTask = { name: '', documentation: '', uuid: BpmnUtil.getUUID() };
      if (!addFrom) {
        this.process.tasks.push(newElement);
      } else {
        const index = this.process.tasks.indexOf(addFrom);
        this.process.tasks.splice(index + 1, 0, newElement);
      }
      this.openPanel.emit({ type: SelectedPanelType.TASK, task: newElement });
    }
  }

  removeProperty(task: BpmnTask, property: BpmnProperty): void {
    if (this.process && this.process.tasks) {
      const taskIndex = this.process.tasks.map(taskObj => taskObj.uuid).indexOf(task.uuid);
      if (taskIndex !== -1 && this.process.tasks[taskIndex].properties) {
        if (this.selectedPanel.type === SelectedPanelType.PROPERTY &&
          this.selectedPanel.property.uuid === property.uuid &&
          this.selectedPanel.task.uuid === task.uuid) {
          this.openTaskPanel(task, true);
        }

        this.process.tasks[taskIndex].properties = this.process.tasks[taskIndex].properties
          .filter(propertyObj => propertyObj.uuid !== property.uuid);
      }
    }
  }

  openPropertyPanel(task: BpmnTask, property: BpmnProperty): void {
    this.openPanel.emit({ type: SelectedPanelType.PROPERTY, task, property });
  }

  openTaskPanel(task: BpmnTask, force: boolean = false): void {
    this.openPanel.emit({ type: SelectedPanelType.TASK, task, force });
  }

  propertyOrTaskSelected(task: BpmnTask): boolean {
    return this.isTaskSelected(task) || this.propertyInTaskSelected(task);
  }
  propertyInTaskSelected(task: BpmnTask): boolean {
    if (!task || !task.properties || this.selectedPanel.type !== SelectedPanelType.PROPERTY) {
      return false;
    }
    return task.properties.find((property) => this.selectedPanel.property === property) !== undefined;
  }
  isPropertySelected(property: BpmnProperty): boolean {
    return this.selectedPanel.property === property;
  }

  private addUUIDs(): void {
    if (this.process && this.process.tasks) {
      this.process.tasks = this.process.tasks.map(task => {
        if (!task.uuid) {
          task.uuid = BpmnUtil.getUUID();
        }
        if (task.properties) {
          task.properties = task.properties.map(property => {
            if (!property.uuid) {
              property.uuid = BpmnUtil.getUUID();
            }
            return property;
          });
        }
        return task;
      });
    }
  }

}
