import { BpmnProperty, BpmnType, BpmnTask } from "../bpmn.model";
import { BpmnUtil } from "../bpmn.util";

interface PropertiesText {
  id: string;
  required?: boolean;
}

const leadPropertiesTextProps: PropertiesText[] = [
  { id: 'Priorytet Leada' },
  { id: 'Imię' },
  { id: 'Nazwisko', required: true },
  { id: 'Stanowisko' },
  { id: 'E-mail' },
  { id: 'Telefon' },
  { id: 'Proces' },
  { id: 'Nazwa firmy' },
  { id: 'Adres strony www' },
  { id: 'Branża' },
  { id: 'Źródło pozyskania Leada' },
  { id: 'Ocena szansy sprzedaży' },
  { id: 'Opiekun Leada' },
  { id: 'Pokaż na mapie' },
  { id: 'Ulica' },
  { id: 'Miasto' },
  { id: 'Kod pocztowy' },
  { id: 'Województwo' },
  { id: 'Kraj' },
  { id: 'Notatki' },
];

const opportinityPropertiesTextProps: PropertiesText[] = [
  { id: 'Nazwa szansa sprzedaży', required: true },
  { id: 'Klient', required: true },
  { id: 'Kontakt', required: true },
  { id: 'Priorytet' },
  { id: 'Kwota' },
  { id: 'Waluta', required: true },
  { id: 'Prawdopodobieństwo' },
  { id: 'Data zakończenia' },
  { id: 'Proces' },
  { id: 'Etap' },
  { id: 'Opis' },
  { id: 'Pochodzenie szansy' },
  { id: 'Podkategoria pochodzenie szansy' },
  { id: 'Właściciel szansy sprzedaży' },
  { id: 'Udział' },
];

export const LEAD_TASK_NAME = "dane podstawowe";
export const OPPORTINITY_TASK_NAME = "dane podstawowe";

const getTaskByProperties = (properties): BpmnProperty[] => {
  return properties.map((props) => ({
    type: BpmnType.STRING,
    required: false,
    uuid: BpmnUtil.getUUID(),
    ...props
  }));
}

export const leadTask: BpmnTask = {
  name: LEAD_TASK_NAME,
  properties: getTaskByProperties(leadPropertiesTextProps),
  isInformationTask: true,
  daysToComplete: 1
};

export const opportinityTask: BpmnTask = {
  name: OPPORTINITY_TASK_NAME,
  properties: getTaskByProperties(opportinityPropertiesTextProps),
  isInformationTask: true,
  daysToComplete: 1
};
