import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {BpmnPossibleValue} from '../../../../bpmn.model';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {BpmnService} from '../../../../bpmn.service';
import {BpmnUtil} from '../../../../bpmn.util';

@Component({
  selector: 'app-property-list-form',
  templateUrl: './property-list-form.component.html',
  styleUrls: ['./property-list-form.component.scss']
})
export class PropertyListFormComponent implements OnInit, OnChanges {
  @Input() possibleValues: BpmnPossibleValue[] = [];
  @Input() disabled: boolean;

  @Output() possibleValueChanged: EventEmitter<BpmnPossibleValue[]> = new EventEmitter<BpmnPossibleValue[]>();

  listFormGroup: FormGroup;
  formControlUuidList: FormControlUuid[] = [];
  @Output() private formValidChange: EventEmitter<boolean> = new EventEmitter();

  constructor(private bpmnService: BpmnService) {
  }

  ngOnInit(): void {
    if (!this.disabled && this.possibleValues && this.possibleValues.length === 0) {
      this.possibleValues.push(this.getNewPossibleValue());
      this.listFormGroup = this.getFormGroup();
      this.notifyFormValidChange();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.possibleValues) {
      this.listFormGroup = this.getFormGroup();
      this.notifyFormValidChange();
    }
  }

  removeForm($event: MouseEvent, element: FormControlUuid): void {
    $event.stopPropagation();
    if (element) {
      this.possibleValues = this.possibleValues.filter(item => item.id !== element.possibleValue.id);
      this.formControlUuidList = this.formControlUuidList.filter(item => item.possibleValue.id !== element.possibleValue.id);
      this.listFormGroup.removeControl(element.possibleValue.id);
      this.notify();
    }
  }

  addNewForm($event: MouseEvent, parent?: FormControlUuid): void {
    $event.stopPropagation();
    const newElement = this.getNewPossibleValue();
    const newFormControlUuid: FormControlUuid = {
      form: this.getForm(newElement),
      possibleValue: newElement
    };

    if (parent) {
      let index = this.getIndexByIdFromPossibleValues(parent.possibleValue.id);
      if (index !== -1) {
        this.possibleValues.splice(index + 1, 0, newElement);
      }
      index = this.getIndexByIdFromFormControlUuidList(parent.possibleValue.id);
      if (index !== -1) {
        this.formControlUuidList.splice(index + 1, 0, newFormControlUuid);
      }
    } else {
      this.possibleValues.push(newElement);
      this.formControlUuidList.push(newFormControlUuid);
    }
    this.listFormGroup.addControl(newElement.id, newFormControlUuid.form);
    this.notify();
  }

  modelChange(formControlUuid: FormControlUuid, value: string): void {
    if (this.possibleValues) {
      let index = this.getIndexByIdFromPossibleValues(formControlUuid.possibleValue.id);
      if (index !== -1) {
        this.possibleValues[index].name = value;
      }
      index = this.getIndexByIdFromFormControlUuidList(formControlUuid.possibleValue.id);
      if (index !== -1) {
        this.formControlUuidList[index].possibleValue.name = value;
      }
    }
    this.notify();
  }

  private notify(): void {
    this.possibleValueChanged.emit(this.possibleValues.filter(item => item.name && item.name.length > 0));
    this.notifyFormValidChange();
  }

  private notifyFormValidChange(): void {
    this.formValidChange.emit((this.listFormGroup.valid && this.possibleValues && this.possibleValues.length > 0) || this.disabled);
  }

  private getIndexByIdFromFormControlUuidList(id: string): number {
    return this.formControlUuidList
      .map(formControlUuidItem => formControlUuidItem.possibleValue.id)
      .indexOf(id);
  }

  private getIndexByIdFromPossibleValues(id: string): number {
    return this.possibleValues
      .map(possibleValue => possibleValue.id)
      .indexOf(id);
  }

  private convertPossibleValuesToFormControlUuid(): void {
    this.formControlUuidList = this.possibleValues
      .map((possibleValue: BpmnPossibleValue): FormControlUuid => {
        return {
          form: this.getForm(possibleValue),
          possibleValue
        };
      });
  }

  private getFormGroup(): FormGroup {
    const result: any = {};
    this.convertPossibleValuesToFormControlUuid();
    this.formControlUuidList.forEach(formControlUuid => {
      result[formControlUuid.possibleValue.id] = formControlUuid.form;
    });
    return new FormGroup(result);
  }

  private getNewPossibleValue(): BpmnPossibleValue {
    return {id: BpmnUtil.getUUID(), name: ''};
  }

  private listValueExist(value: string, id: string): boolean {
    if (!this.possibleValues || !value || value.length === 0) {
      return false;
    }
    return this.possibleValues
      .filter(item => item.id !== id)
      .filter(item => item.name === value).length > 0;
  }

  private getForm(possibleValue: BpmnPossibleValue): FormControl {
    const formControl = new FormControl(possibleValue.name, [Validators.required,
      (control: AbstractControl): ValidationErrors => (this.listValueExist(control.value, possibleValue.id)) ? {valueExist: true} : null,
      (control: AbstractControl): ValidationErrors => ((control.value || '').trim().length === 0) ? {whitespace: true} : null]);
    formControl.markAsTouched();
    return formControl;
  }

}

export interface FormControlUuid {
  possibleValue: BpmnPossibleValue;
  form: FormControl;
}
