"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.PropertyListFormComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var bpmn_util_1 = require("../../../../bpmn.util");
var PropertyListFormComponent = /** @class */ (function () {
    function PropertyListFormComponent(bpmnService) {
        this.bpmnService = bpmnService;
        this.possibleValues = [];
        this.possibleValueChanged = new core_1.EventEmitter();
        this.formControlUuidList = [];
        this.formValidChange = new core_1.EventEmitter();
    }
    PropertyListFormComponent.prototype.ngOnInit = function () {
        if (this.possibleValues && this.possibleValues.length === 0) {
            this.possibleValues.push(this.getNewPossibleValue());
            this.listFormGroup = this.getFormGroup();
            this.notifyFormValidChange();
        }
    };
    PropertyListFormComponent.prototype.ngOnChanges = function (changes) {
        if (this.possibleValues) {
            this.listFormGroup = this.getFormGroup();
            this.notifyFormValidChange();
        }
    };
    PropertyListFormComponent.prototype.removeForm = function ($event, element) {
        $event.stopPropagation();
        if (element) {
            this.possibleValues = this.possibleValues.filter(function (item) { return item.id !== element.possibleValue.id; });
            this.formControlUuidList = this.formControlUuidList.filter(function (item) { return item.possibleValue.id !== element.possibleValue.id; });
            this.listFormGroup.removeControl(element.possibleValue.id);
            this.notify();
        }
    };
    PropertyListFormComponent.prototype.addNewForm = function ($event, parent) {
        $event.stopPropagation();
        var newElement = this.getNewPossibleValue();
        var newFormControlUuid = {
            form: this.getForm(newElement),
            possibleValue: newElement
        };
        if (parent) {
            var index = this.getIndexByIdFromPossibleValues(parent.possibleValue.id);
            if (index !== -1) {
                this.possibleValues.splice(index + 1, 0, newElement);
            }
            index = this.getIndexByIdFromFormControlUuidList(parent.possibleValue.id);
            if (index !== -1) {
                this.formControlUuidList.splice(index + 1, 0, newFormControlUuid);
            }
        }
        else {
            this.possibleValues.push(newElement);
            this.formControlUuidList.push(newFormControlUuid);
        }
        this.listFormGroup.addControl(newElement.id, newFormControlUuid.form);
        this.notify();
    };
    PropertyListFormComponent.prototype.modelChange = function (formControlUuid, value) {
        if (this.possibleValues) {
            var index = this.getIndexByIdFromPossibleValues(formControlUuid.possibleValue.id);
            if (index !== -1) {
                this.possibleValues[index].name = value;
            }
            index = this.getIndexByIdFromFormControlUuidList(formControlUuid.possibleValue.id);
            if (index !== -1) {
                this.formControlUuidList[index].possibleValue.name = value;
            }
        }
        this.notify();
    };
    PropertyListFormComponent.prototype.notify = function () {
        this.possibleValueChanged.emit(this.possibleValues.filter(function (item) { return item.name && item.name.length > 0; }));
        this.notifyFormValidChange();
    };
    PropertyListFormComponent.prototype.notifyFormValidChange = function () {
        this.formValidChange.emit(this.listFormGroup.valid && (this.possibleValues && this.possibleValues.length > 0));
    };
    PropertyListFormComponent.prototype.getIndexByIdFromFormControlUuidList = function (id) {
        return this.formControlUuidList
            .map(function (formControlUuidItem) { return formControlUuidItem.possibleValue.id; })
            .indexOf(id);
    };
    PropertyListFormComponent.prototype.getIndexByIdFromPossibleValues = function (id) {
        return this.possibleValues
            .map(function (possibleValue) { return possibleValue.id; })
            .indexOf(id);
    };
    PropertyListFormComponent.prototype.convertPossibleValuesToFormControlUuid = function () {
        var _this = this;
        this.formControlUuidList = this.possibleValues
            .map(function (possibleValue) {
            return {
                form: _this.getForm(possibleValue),
                possibleValue: possibleValue
            };
        });
    };
    PropertyListFormComponent.prototype.getFormGroup = function () {
        var result = {};
        this.convertPossibleValuesToFormControlUuid();
        this.formControlUuidList.forEach(function (formControlUuid) {
            result[formControlUuid.possibleValue.id] = formControlUuid.form;
        });
        return new forms_1.FormGroup(result);
    };
    PropertyListFormComponent.prototype.getNewPossibleValue = function () {
        return { id: bpmn_util_1.BpmnUtil.getUUID(), name: '' };
    };
    PropertyListFormComponent.prototype.listValueExist = function (value, id) {
        if (!this.possibleValues || !value || value.length === 0) {
            return false;
        }
        return this.possibleValues
            .filter(function (item) { return item.id !== id; })
            .filter(function (item) { return item.name === value; }).length > 0;
    };
    PropertyListFormComponent.prototype.getForm = function (possibleValue) {
        var _this = this;
        var formControl = new forms_1.FormControl(possibleValue.name, [forms_1.Validators.required, function (control) { return (_this.listValueExist(control.value, possibleValue.id)) ? { valueExist: true } : null; },
            function (control) { return ((control.value || '').trim().length === 0) ? { whitespace: true } : null; }]);
        formControl.markAsTouched();
        return formControl;
    };
    __decorate([
        core_1.Input()
    ], PropertyListFormComponent.prototype, "possibleValues");
    __decorate([
        core_1.Input()
    ], PropertyListFormComponent.prototype, "disabled");
    __decorate([
        core_1.Output()
    ], PropertyListFormComponent.prototype, "possibleValueChanged");
    __decorate([
        core_1.Output()
    ], PropertyListFormComponent.prototype, "formValidChange");
    PropertyListFormComponent = __decorate([
        core_1.Component({
            selector: 'app-property-list-form',
            templateUrl: './property-list-form.component.html',
            styleUrls: ['./property-list-form.component.scss']
        })
    ], PropertyListFormComponent);
    return PropertyListFormComponent;
}());
exports.PropertyListFormComponent = PropertyListFormComponent;
