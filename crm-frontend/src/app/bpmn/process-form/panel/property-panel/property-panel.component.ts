import {Component, DoCheck, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {
  BpmnPossibleValue,
  BpmnProcess,
  BpmnProperty,
  BpmnTask,
  BpmnType,
  SelectedPanel,
  SelectedPanelType
} from '../../../bpmn.model';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {BpmnUtil} from '../../../bpmn.util';
import {BpmnService} from '../../../bpmn.service';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {FormValidators} from '../../../../shared/form/form-validators';

@Component({
  selector: 'app-property-panel',
  templateUrl: './property-panel.component.html',
  styleUrls: ['./property-panel.component.scss']
})
export class PropertyPanelComponent implements OnInit, DoCheck {
  public propertyTitleForm: FormGroup;
  storedProcess: BpmnProcess;
  private cachedProperty: BpmnProperty;
  selectedPropertyPanel: SelectedPanel;
  @Output() private processChanged: EventEmitter<BpmnProcess> = new EventEmitter();
  @Output() private formValidChange: EventEmitter<boolean> = new EventEmitter();

  bpmnTypeEnum = BpmnType;

  possibleValues: BpmnPossibleValue[] = [];

  listFormValid = true;
  namePlaceholder = '';

  @Input()
  set process(storedProcess: BpmnProcess) {
    this.storedProcess = storedProcess;
    this.updateForm();
  }

  @Input()
  set selectedPanel(selectedPropertyPanel: SelectedPanel) {
    this.selectedPropertyPanel = selectedPropertyPanel;
    this.initNewForm();
    this.updateForm();
  }

  constructor(private bpmnService: BpmnService,
              private notificationService: NotificationService) {
    this.initNewForm();
  }

  ngOnInit(): void {
  }

  ngDoCheck(): void {
    const id = this.getProperty().id;
    if (this.getProperty().id !== this.propertyTitleForm.get('name').value) {
      this.propertyTitleForm.get('name').patchValue(id);
      this.formValidChange.emit(this.formIsValid());
      BpmnUtil.setTouched(this.propertyTitleForm);
    }
  }

  propertyNameExist(value: string): boolean {
    if (this.storedProcess) {
      const propertyNames = [];
      for (const task of this.storedProcess.tasks) {
        if (task.properties) {
          for (const property of task.properties) {
            propertyNames.push(property.id);
          }
        }
      }
      if (propertyNames.length !== new Set(propertyNames).size) {
        this.notificationService.notify('bpmn.panel.property.nameExistError', NotificationType.ERROR);
        return true;
      }
    }
    return false;
  }

  checkPropertyType(type: BpmnType): boolean {
    return this.cachedProperty && this.cachedProperty.type === type;
  }

  nameChanged(name: string): void {
    this.propertyChanged('id', name);
  }
  possibleValuesChanged(possibleValues: BpmnPossibleValue[]): void {
    this.propertyChanged('possibleValues', possibleValues);
  }
  requiredCheckboxChanged(value: boolean): void {
    this.propertyChanged('required', value);
  }

  listFormValidChange(valid: boolean): void {
    this.listFormValid = valid;
    this.formValidChange.emit(this.formIsValid());
  }

  isNameValid(): boolean {
    return this.propertyTitleForm.get('name').valid;
  }

  addNewField(): void {
    if (this.cachedProperty) {
      this.bpmnService.addNewPropertyButtonClicked.next(this.cachedProperty.type);
    }
  }

  private propertyChanged(key: string, value: any): void {
    const property = this.getProperty();
    if (property) {
      property[key] = value;
      this.setProperty(property);
      this.processChanged.emit(this.storedProcess);
      this.formValidChange.emit(this.formIsValid());
    }
  }

  private getPropertyTypeKey(): string {
    if (!this.cachedProperty) {
      return 'bpmn.panel.property.namePlaceholder';
    }
    switch (this.cachedProperty.type) {
      case BpmnType.BOOLEAN:
        return 'bpmn.panel.property.typeKeyPlaceholder.boolean';
      case BpmnType.ENUM:
        return 'bpmn.panel.property.typeKeyPlaceholder.enum';
      case BpmnType.DOUBLE:
        return 'bpmn.panel.property.typeKeyPlaceholder.double';
      case BpmnType.STRING:
        return 'bpmn.panel.property.typeKeyPlaceholder.string';
    }
    return '';
  }

  private updateForm(): void {
    this.cachedProperty = this.getProperty();
    if (this.cachedProperty) {
      this.propertyTitleForm.get('name').setValue(this.cachedProperty.id);
      this.propertyTitleForm.get('requiredCheckbox').setValue(this.cachedProperty.required);
      this.possibleValues = this.cachedProperty.possibleValues || [];
      this.namePlaceholder = this.getPropertyTypeKey();
      BpmnUtil.setTouched(this.propertyTitleForm);
      this.formValidChange.emit(this.formIsValid());
    }
  }

  private setProperty(property: BpmnProperty): void {
    if (this.storedProcess && this.selectedPropertyPanel && this.storedProcess.tasks &&
      this.selectedPropertyPanel.task && this.selectedPropertyPanel.property &&
      this.selectedPropertyPanel.type === SelectedPanelType.PROPERTY) {

      const taskIndex = this.storedProcess.tasks
        .map(task => task.uuid)
        .indexOf(this.selectedPropertyPanel.task.uuid);

      if (taskIndex !== -1 && this.storedProcess.tasks[taskIndex].properties) {

        const propertyIndex = this.storedProcess.tasks[taskIndex].properties
          .map(prop => prop.uuid)
          .indexOf(this.selectedPropertyPanel.property.uuid);

        if (propertyIndex !== -1) {
          this.storedProcess.tasks[taskIndex].properties[propertyIndex] = property;
        }
      }
    }
  }

  private getProperty(): BpmnProperty {
    const task = this.getTask();
    if (task && task.properties) {
      return task.properties.find((property) => property.uuid === this.selectedPropertyPanel.property.uuid);
    }
    return undefined;
  }

  private getTask(): BpmnTask {
    if (this.storedProcess && this.selectedPropertyPanel && this.storedProcess.tasks &&
      this.selectedPropertyPanel.task && this.selectedPropertyPanel.property &&
      this.selectedPropertyPanel.type === SelectedPanelType.PROPERTY) {

      return this.storedProcess.tasks.find(task => task.uuid === this.selectedPropertyPanel.task.uuid);
    }
    return undefined;
  }

  private initNewForm(): void {
    this.propertyTitleForm = new FormGroup({
      requiredCheckbox: new FormControl(false),
      name: new FormControl('', [Validators.required, FormValidators.whitespace, Validators.maxLength(250),
        (control: AbstractControl): ValidationErrors => (this.propertyNameExist(control.value)) ? {propertyExist: true} : null]),
    });
    BpmnUtil.setTouched(this.propertyTitleForm);
    this.formValidChange.emit(this.formIsValid());
  }

  private formIsValid(): boolean {
    if (!this.cachedProperty || !this.propertyTitleForm) {
      return true;
    }
    return this.propertyTitleForm.valid && (this.cachedProperty.type !== BpmnType.ENUM  || this.listFormValid);
  }
}
