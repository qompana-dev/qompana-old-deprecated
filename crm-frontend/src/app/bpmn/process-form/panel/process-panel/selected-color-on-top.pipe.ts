import {Pipe, PipeTransform} from '@angular/core';
import {AvailableColor} from '../../../bpmn.model';

@Pipe({
  name: 'selectedColorOnTop'
})
export class SelectedColorOnTopPipe implements PipeTransform {

  transform(values: AvailableColor[], selected: AvailableColor): AvailableColor[] {
    if (!selected || !values) {
      return values;
    }
    const result = [this.getSelected(values, selected)].concat(this.getOtherElements(values, selected));
    return (selected.id === -1) ? result : result.filter((color) => color.id !== -1);
  }

  private getSelected(values: AvailableColor[], selected: AvailableColor): AvailableColor {
    return values.find(value => value.id === selected.id);
  }

  private getOtherElements(values: AvailableColor[], selected: AvailableColor): AvailableColor[] {
    return values.filter(value => value.id !== selected.id);
  }
}
