import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {AvailableColor, AvailableColors, BpmnObjectType, BpmnProcess, ProcessAvailableType} from '../../../bpmn.model';
import {BpmnUtil} from '../../../bpmn.util';
import {takeUntil} from 'rxjs/operators';
import {Role} from '../../../../permissions/role.model';
import {RoleService} from '../../../../permissions/roles/role.service';
import {Subject} from 'rxjs';
import {BpmnService} from '../../../bpmn.service';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {FormValidators} from '../../../../shared/form/form-validators';

@Component({
  selector: 'app-process-panel',
  templateUrl: './process-panel.component.html',
  styleUrls: ['./process-panel.component.scss']
})
export class ProcessPanelComponent implements OnInit, OnDestroy {
  @Output() private processChanged: EventEmitter<BpmnProcess> = new EventEmitter();
  @Output() private formValidChange: EventEmitter<boolean> = new EventEmitter();

  @Input()
  set process(storedProcess: BpmnProcess) {
    this.storedProcess = storedProcess;
    this.updateForm();
    if (!this.storedProcess.bgColor || !this.storedProcess.textColor) {
      this.selectColor(AvailableColors.whiteColor());
    }
  }

  public processTitleForm: FormGroup;
  public nameFormControl = new FormControl('', [Validators.required, FormValidators.whitespace, Validators.maxLength(250)]);
  storedProcess: BpmnProcess;
  documentationFormControl = new FormControl('');

  colors: AvailableColor[] = AvailableColors.colors.filter(color => color.id >= -1); // white with others
  bpmnObjectTypeEnum = BpmnObjectType;
  processAvailableTypeEnum = ProcessAvailableType;
  selectedColor: AvailableColor = AvailableColors.whiteColor();
  roles: Role[];
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private roleService: RoleService,
              private bpmnService: BpmnService,
              private notificationService: NotificationService) {
    this.subscribeForRoles();
    this.initNewForm();
  }

  ngOnInit() {
    this.bpmnService.saveButtonClicked
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.showFormNotification());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  nameChanged(name: string): void {
    this.storedProcess.name = name;
    this.notifyOnChange();
  }

  objectTypeChanged(objectType: BpmnObjectType): void {
    this.storedProcess.objectType = objectType;
    this.notifyOnChange();
  }

  availableTypeChanged(availableType: ProcessAvailableType): void {
    this.storedProcess.availableType = availableType;
    this.notifyOnChange();
  }

  departmentChanged(department: number): void {
    this.storedProcess.department = '' + department;
    this.notifyOnChange();
  }

  documentationChanged(documentation: string): void {
    this.storedProcess.documentation = documentation;
    this.notifyOnChange();
  }

  notifyOnChange(): void {
    this.processChanged.emit(this.storedProcess);
    this.formValidChange.emit(this.formIsValid());
  }

  showDepartment(): boolean {
    return this.storedProcess.availableType === ProcessAvailableType.DEPARTMENT;
  }

  private updateForm(): void {
    if (this.storedProcess) {
      this.processTitleForm.get('name').setValue(this.storedProcess.name);
      this.processTitleForm.get('documentation').setValue(this.storedProcess.documentation);
      this.processTitleForm.get('objectType').setValue(this.storedProcess.objectType);
      this.processTitleForm.get('availableType').setValue(this.storedProcess.availableType);
      if (this.storedProcess.department) {
        this.processTitleForm.get('department').setValue(+this.storedProcess.department);
      }
      this.updateColor();
      this.processTitleForm.get('color').setValue(this.selectedColor.id);
      BpmnUtil.setTouched(this.processTitleForm);
      this.formValidChange.emit(this.formIsValid());
    }
  }

  private initNewForm(): void {
    this.processTitleForm = new FormGroup({
      name: this.nameFormControl,
      documentation: this.documentationFormControl,
      objectType: new FormControl(BpmnObjectType.LEAD), // todo: save in db and add required
      availableType: new FormControl(ProcessAvailableType.ALL),
      color: new FormControl(-1, [(control: AbstractControl): ValidationErrors => ((control.value === -1)) ? {notSelected: true} : null])
    });
    this.processTitleForm.addControl('department', new FormControl(null, [(control: AbstractControl): ValidationErrors =>
      (this.processTitleForm.get('availableType').value === 'DEPARTMENT' && (control.value === null)) ? {required: true} : null]));
    BpmnUtil.setTouched(this.processTitleForm);
    this.formValidChange.emit(this.formIsValid());
  }

  private updateColor(): void {
    const colorToUpdate: AvailableColor = this.colors
      .find(color => color.hex === this.storedProcess.bgColor && color.textHex === this.storedProcess.textColor);
    if (colorToUpdate && colorToUpdate.id !== this.selectedColor.id) {
      this.selectedColor = colorToUpdate;
    }
  }

  selectColor(color: AvailableColor): void {
    if (color) {
      this.selectedColor = color;
      this.storedProcess.bgColor = color.hex;
      this.storedProcess.textColor = color.textHex;
      this.processChanged.emit(this.storedProcess);
      BpmnUtil.setTouched(this.processTitleForm);
      this.formValidChange.emit(this.formIsValid());
    }
  }

  private formIsValid(): boolean {
    if (!this.processTitleForm) {
      return false;
    }
    return this.processTitleForm.valid;
  }

  private subscribeForRoles(): void {
    this.roleService.getAllRolesAsFlatList()
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      (roles) => this.roles = roles
    );
  }

  isNameValid(): boolean {
    return this.processTitleForm.get('name').valid;
  }

  private showFormNotification(): void {
    if (!this.processTitleForm.get('name').valid) {
      this.notificationService.notify('bpmn.processForm.nameInputRequired', NotificationType.ERROR);
    } else if (!this.processTitleForm.get('color').valid) {
      this.notificationService.notify('bpmn.processForm.colorInputRequired', NotificationType.ERROR);
    }
  }
}
