export class BpmnTextEditorConfig {
  static config = {
    editable: true,
    spellcheck: false,
    height: 'auto',
    minHeight: '0',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    imageEndPoint: '',
    toolbar: [
      ['bold', 'italic', 'underline', 'orderedList', 'unorderedList']
    ]
  };
}
