import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BpmnTextEditorConfig} from './bpmn-text-editor.config';

@Component({
  selector: 'app-bpmn-text-editor',
  templateUrl: './bpmn-text-editor.component.html',
  styleUrls: ['./bpmn-text-editor.component.scss']
})
export class BpmnTextEditorComponent {
  html: string;
  editorConfig = BpmnTextEditorConfig.config;
  @Input() textAreaPlaceholder = '';
  @Output() valueChanged: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  set htmlContent(html: string) {
    this.html = html;
  }

  constructor() {
  }


  onTextChange(event: any): void {
    this.htmlContent = event;
    this.valueChanged.emit(event);
  }
}
