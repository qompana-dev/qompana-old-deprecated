import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {BpmnObjectType, BpmnProcess, BpmnTask, SelectedPanel, SelectedPanelType} from '../../../bpmn.model';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {AccountEmail} from '../../../../person/person.model';
import {DeleteDialogData} from '../../../../shared/dialog/delete/delete-dialog.component';
import {BpmnUtil} from '../../../bpmn.util';
import {BpmnTextEditorComponent} from './bpmn-text-editor/bpmn-text-editor.component';
import {DeleteDialogService} from '../../../../shared/dialog/delete/delete-dialog.service';
import {BpmnService} from '../../../bpmn.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {FormValidators} from '../../../../shared/form/form-validators';

@Component({
  selector: 'app-task-panel',
  templateUrl: './task-panel.component.html',
  styleUrls: ['./task-panel.component.scss']
})
export class TaskPanelComponent implements OnInit, OnDestroy, AfterViewInit {
  storedProcess: BpmnProcess;
  selectedTaskPanel: SelectedPanel;
  @ViewChild('documentationTextArea') documentationTextArea: BpmnTextEditorComponent;

  @Input() accountEmailList: AccountEmail[] = [];
  @Output() private processChanged: EventEmitter<BpmnProcess> = new EventEmitter();
  @Output() private formValidChange: EventEmitter<boolean> = new EventEmitter();
  @Output() openPanel: EventEmitter<SelectedPanel> = new EventEmitter();

  bpmnObjectTypeEnum = BpmnObjectType;
  public taskTitleForm: FormGroup;
  documentation = '';
  bpmnUtil = BpmnUtil;
  private componentDestroyed: Subject<void> = new Subject();

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'bpmn.panel.task.dialog.delete.title',
    description: 'bpmn.panel.task.dialog.delete.description',
    confirmButton: 'bpmn.panel.task.dialog.delete.confirm',
    cancelButton: 'bpmn.panel.task.dialog.delete.cancel',
  };

  @Input()
  set process(storedProcess: BpmnProcess) {
    this.storedProcess = storedProcess;
    this.updateForm();
  }

  @Input()
  set selectedPanel(selectedTaskPanel: SelectedPanel) {
    this.selectedTaskPanel = selectedTaskPanel;
    this.initNewForm();
    this.updateForm();
  }

  constructor(private deleteDialogService: DeleteDialogService,
              private bpmnService: BpmnService,
              private notificationService: NotificationService,
              private cdRef: ChangeDetectorRef) {
    this.initNewForm();
  }

  ngOnInit(): void {
    this.updateForm();
    this.bpmnService.saveButtonClicked
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.showFormNotification());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  nameChanged(name: string): void {
    this.propertyChanged('name', name);
  }

  daysToCompleteChanged(daysToComplete: string): void {
    this.propertyChanged('daysToComplete', daysToComplete);
  }

  updateAcceptance(): void {
    const task = this.getTask();
    if (!task) {
      return;
    }

    if (!this.taskTitleForm.get('acceptanceRequired').value) {
      task.acceptanceUser = undefined;
      task.acceptanceGroup = undefined;
    } else {
      const acceptanceType = this.taskTitleForm.get('acceptanceType').value;
      if (acceptanceType === 'users') {
        task.acceptanceGroup = undefined;
        task.acceptanceUser = this.taskTitleForm.get('acceptanceUser').value;
      } else if (acceptanceType === 'superior') {
        task.acceptanceGroup = 'true';
        task.acceptanceUser = undefined;
      }
    }
    if (task) {
      this.setTask(task);
      this.processChanged.emit(this.storedProcess);
      this.notifyFormValidChange();
    }
  }

  updateNotification(): void {
    const task = this.getTask();
    if (!task) {
      return;
    }

    if (!this.taskTitleForm.get('notificationRequired').value) {
      task.notificationUser = undefined;
      task.notificationGroup = undefined;
    } else {
      const notificationType = this.taskTitleForm.get('notificationType').value;
      if (notificationType === 'users') {
        task.notificationGroup = undefined;
        const notificationUsers = this.taskTitleForm.get('notificationUser').value;
        if (notificationUsers) {
          task.notificationUser = notificationUsers.join(',');
        }
      } else if (notificationType === 'superior') {
        task.notificationGroup = 'true';
        task.notificationUser = undefined;
      }
    }
    if (task) {
      this.setTask(task);
      this.processChanged.emit(this.storedProcess);
      this.notifyFormValidChange();
    }
  }

  documentationChanged(documentation: string): void {
    this.propertyChanged('documentation', documentation);
  }

  salesOpportunityChanged(value: string): void {
    this.propertyChanged('salesOpportunity', value);
  }

  propertyChanged(property: string, value: string): void {
    const task = this.getTask();
    if (task) {
      task[property] = value;
      this.setTask(task);
      this.processChanged.emit(this.storedProcess);
      this.notifyFormValidChange();
    }
  }

  showAcceptanceUserForm(): boolean {
    return this.taskTitleForm.get('acceptanceRequired').value
      && this.taskTitleForm.get('acceptanceType').value === 'users';
  }

  showNotificationUserForm(): boolean {
    return this.taskTitleForm.get('notificationRequired').value
      && this.taskTitleForm.get('notificationType').value === 'users';
  }

  tryRemoveTask(): void {
    this.deleteDialogData.onConfirmClick = () => this.removeTask();
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private removeTask(): void {
    if (this.storedProcess && this.storedProcess.tasks && this.selectedTaskPanel && this.selectedTaskPanel.task) {
      this.openPanel.emit({type: SelectedPanelType.PROCESS, force: true});
      this.formValidChange.emit(true);
      this.storedProcess.tasks = this.storedProcess.tasks.filter(taskObj => taskObj.uuid !== this.selectedTaskPanel.task.uuid);
      this.processChanged.emit(this.storedProcess);
    }
  }

  private updateForm(): void {
    const task: BpmnTask = this.getTask();
    if (task) {
      this.taskTitleForm.get('name').setValue(task.name);
      this.taskTitleForm.get('daysToComplete').setValue(task.daysToComplete || 1);

      this.documentation = task.documentation;
      if (this.documentationTextArea) {
        this.documentationTextArea.htmlContent = this.documentation;
      }

      this.taskTitleForm.get('acceptanceRequired').setValue((task.acceptanceGroup || task.acceptanceUser));
      if (task.acceptanceGroup) {
        this.taskTitleForm.get('acceptanceType').setValue('superior');
        this.taskTitleForm.get('acceptanceUser').setValue('');
      }
      if (task.acceptanceUser) {
        this.taskTitleForm.get('acceptanceType').setValue('users');
        this.taskTitleForm.get('acceptanceUser').setValue(+task.acceptanceUser);
      }
      if (!task.acceptanceGroup && !task.acceptanceUser) {
        this.taskTitleForm.get('acceptanceType').setValue('');
        this.taskTitleForm.get('acceptanceUser').setValue('');
      }

      this.taskTitleForm.get('notificationRequired').setValue((task.notificationGroup || task.notificationUser));
      if (task.notificationGroup) {
        this.taskTitleForm.get('notificationType').setValue('superior');
        this.taskTitleForm.get('notificationUser').setValue('');
      }
      if (task.notificationUser) {
        this.taskTitleForm.get('notificationType').setValue('users');
        this.taskTitleForm.get('notificationUser').setValue(task.notificationUser.split(',').map(e => +e));
      }
      if (!task.notificationGroup && !task.notificationUser) {
        this.taskTitleForm.get('notificationType').setValue('');
        this.taskTitleForm.get('notificationUser').setValue('');
      }

      if (this.storedProcess && this.storedProcess.objectType === BpmnObjectType.OPPORTUNITY) {
        this.taskTitleForm.get('salesOpportunity').setValue(task.salesOpportunity);
      }
    }
    BpmnUtil.setTouched(this.taskTitleForm);
    this.notifyFormValidChange();
  }

  private getTask(): BpmnTask {
    if (this.storedProcess && this.storedProcess.tasks
      && this.selectedTaskPanel && this.selectedTaskPanel.type === SelectedPanelType.TASK) {

      return this.storedProcess.tasks.find((item) => item.uuid === this.selectedTaskPanel.task.uuid);
    }
    return undefined;
  }

  private setTask(task: BpmnTask): BpmnTask {
    if (this.storedProcess && this.storedProcess.tasks
      && this.selectedTaskPanel && this.selectedTaskPanel.type === SelectedPanelType.TASK) {

      const index = this.storedProcess.tasks.map((item) => item.uuid).indexOf(task.uuid);
      if (index !== -1) {
        this.storedProcess.tasks[index] = task;
      }
    }
    return undefined;
  }

  private initNewForm(): void {
    this.taskTitleForm = new FormGroup({
      name: new FormControl('', [Validators.required, FormValidators.whitespace, Validators.maxLength(250),
        (control: AbstractControl): ValidationErrors => (this.taskNameExist(control.value)) ? {taskNameExist: true} : null]),
      acceptanceRequired: new FormControl(false),
      acceptanceType: new FormControl('superior'),
      acceptanceUser: new FormControl(),
      notificationRequired: new FormControl(false),
      notificationType: new FormControl('superior'),
      notificationUser: new FormControl(),
      daysToComplete: new FormControl('', Validators.compose([Validators.required, Validators.pattern(/^(\d*)?$/), Validators.min(1)])),
      salesOpportunity: new FormControl('', Validators.compose([Validators.max(100), Validators.min(0)]))
    });
    this.documentation = '';
    BpmnUtil.setTouched(this.taskTitleForm);
    this.notifyFormValidChange();
  }

  private taskNameExist(name: string): boolean {
    if (!this.storedProcess || !this.storedProcess.tasks || !this.selectedTaskPanel) {
      return false;
    }
    const count = this.storedProcess.tasks
      .map(task => task.name)
      .filter(taskName => taskName === name).length;
    return count !== ((name === this.selectedTaskPanel.task.name) ? 1 : 0);
  }

  private notifyFormValidChange(): void {
    this.formValidChange.emit(this.taskTitleForm.valid);
  }

  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  isNameValid(): boolean {
    return this.taskTitleForm.get('name').valid;
  }

  private showFormNotification(): void {
    if (!this.taskTitleForm.get('name').valid) {
      this.notificationService.notify('bpmn.taskForm.nameInputRequired', NotificationType.ERROR);
    }
  }
}
