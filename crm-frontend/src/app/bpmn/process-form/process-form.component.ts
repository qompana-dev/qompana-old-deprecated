import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import {
  BpmnObjectType,
  BpmnProcess,
  CreateProcessFromFileResponse,
  ProcessAvailableType,
  SelectedPanel,
  SelectedPanelType,
  BpmnTask
} from '../bpmn.model';
import { BpmnService } from '../bpmn.service';
import { interval, Subject } from 'rxjs';
import { takeUntil, takeWhile } from 'rxjs/operators';
import { AccountEmail } from '../../person/person.model';
import { PersonService } from '../../person/person.service';
import { SliderService } from '../../shared/slider/slider.service';
import { NotificationService, NotificationType } from '../../shared/notification.service';
import { HttpResponse } from '@angular/common/http';
import { BpmnUtil } from '../bpmn.util';
import { ProcessPanelComponent } from './panel/process-panel/process-panel.component';
import { TaskPanelComponent } from './panel/task-panel/task-panel.component';
import { PropertyPanelComponent } from './panel/property-panel/property-panel.component';
import { TooltipDirective } from 'ngx-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { TaskFormComponent } from './task-form/task-form.component';
import { leadTask, LEAD_TASK_NAME, opportinityTask, OPPORTINITY_TASK_NAME } from './task-data';

@Component({
  selector: 'app-process-form',
  templateUrl: './process-form.component.html',
  styleUrls: ['./process-form.component.scss']
})
export class ProcessFormComponent implements OnInit, OnDestroy {
  @Output() processSaved: EventEmitter<string> = new EventEmitter<string>();
  @Output() private processTypeChanged: EventEmitter<BpmnObjectType> = new EventEmitter();

  @Input() changeProcessId = false;
  private processType: BpmnObjectType;

  @Input() selectedType: BpmnObjectType = BpmnObjectType.LEAD;

  @Input() set currentlyOpened(change: boolean) {
    this.active = change;
    if (this.active) {
      this.initializeUserTipsMechanism();
    }
  }

  @Input() set processDefinitionId(id: string) {
    this.processId = id;
    this.getProcessFileByProcessDefinitionId(this.processId);
  }

  @ViewChild(ProcessPanelComponent) processPanel: ProcessPanelComponent;
  @ViewChild(TaskPanelComponent) taskPanel: TaskPanelComponent;
  @ViewChild(PropertyPanelComponent) propertyPanel: PropertyPanelComponent;
  @ViewChild(TaskFormComponent) taskForm: TaskFormComponent;
  @ViewChild('mainTooltip') mainTooltip: TooltipDirective;

  process: BpmnProcess = { objectType: BpmnObjectType.LEAD, tasks: [] };
  accountEmailList: AccountEmail[] = [];

  selectedPanel: SelectedPanel = { type: SelectedPanelType.PROCESS };
  selectedPanelTypeEnum = SelectedPanelType;
  currentPanelValid = true;
  tooltipText: string;

  private componentDestroyed: Subject<void> = new Subject();
  private processId: string;
  private active = false;
  private firstTimeOnCurrentPanel = true;

  constructor(private personService: PersonService,
    private sliderService: SliderService,
    private notificationService: NotificationService,
    private bpmnService: BpmnService,
    private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.getAccountEmailList();
    this.initNewProcess();
    this.subscribeForSliderClose();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  updateProcess(process: BpmnProcess): void {
    this.process = process;
    this.notifyIfProcessTypeChenged(process);
  }

  isPanelActive(type: SelectedPanelType): boolean {
    return this.selectedPanel.type === type;
  }

  openPanel(panel: SelectedPanel): void {
    if (this.currentPanelValid || panel.force) {
      this.selectedPanel = panel;
    }
  }

  saveProcess(): void {
    if (this.currentPanelValid) {
      this.bpmnService.getBpmnFile(this.getProcess())
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((result: HttpResponse<Blob>) => {
          this.uploadFile(this.process.id, result.body);
        }, (err) => {
          this.notificationService.notify('bpmn.processForm.saveErrorNotification', NotificationType.ERROR);
          if (err.status === 403) {
            this.closeSlider();
          }
        });
    } else {
      this.bpmnService.saveButtonClicked.next();
    }
  }

  uploadFile(processId: string, blob: Blob): void {
    this.bpmnService.saveProcess(processId, blob)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((response: CreateProcessFromFileResponse) => {
        this.notificationService.notify('bpmn.processForm.processSavedNotification', NotificationType.SUCCESS);
        this.processSaved.emit(response.id);
        this.closeSlider();
        if (!this.processId) {
          this.initNewProcess();
        }
      }, () => {
        this.notificationService.notify('bpmn.processForm.saveErrorNotification', NotificationType.ERROR);
      });
  }

  getProcess(): BpmnProcess {
    return BpmnUtil.removeUnusedValues(this.process);
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
  }

  checkForTipsAndShowIfNeeded(): void {
    if (this.selectedPanel.type === SelectedPanelType.PROCESS) {
      this.showTipsForProcessPanel();
    } else if (this.selectedPanel.type === SelectedPanelType.TASK) {
      this.showTipsForTaskPanel();
    } else if (this.selectedPanel.type === SelectedPanelType.PROPERTY) {
      this.showTipsForPropertyPanel();
    }
  }

  private getAccountEmailList(): void {
    this.personService.getAccountEmails()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (response: AccountEmail[]) => this.accountEmailList = response.sort(
          (a, b) => a.surname.localeCompare(b.surname) || a.name.localeCompare(b.name)
        )
      );
  }

  private getProcessFileByProcessDefinitionId(processDefinitionId: string): void {
    if (!processDefinitionId) {
      return;
    }
    this.bpmnService.getFileByProcessDefinitionId(processDefinitionId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((response: HttpResponse<Blob>) => this.parseBpmnFile(response.body),
        () => this.loadProcessError());
  }
  private parseBpmnFile(file: Blob): void {
    this.bpmnService.parseBpmnFile(file)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((response: BpmnProcess) => {
        this.handleParseSuccess(response);
      }, () => this.loadProcessError());
  }

  private handleParseSuccess(response: BpmnProcess): void {
    this.process = response;
    if (this.changeProcessId) {
      this.process.id = 'process' + BpmnUtil.getUUID();
    }
  }

  private loadProcessError(): void {
    this.notificationService.notify('bpmn.processForm.loadProcessErrorNotification', NotificationType.ERROR);
    this.initNewProcess();
  }

  private initNewProcess(): void {
    this.process = {
      objectType: this.selectedType,
      tasks: [this.getFirstTaskByType(this.selectedType)],
      availableType: ProcessAvailableType.ALL
    };
    if (!this.process.id) {
      this.process.id = 'process' + BpmnUtil.getUUID();
    }
    if (!this.process.tasks) {
      this.process.tasks = [];
    }
  }

  private initializeUserTipsMechanism(): void {
    interval(1000)
      .pipe(
        takeWhile(() => this.active),
        takeUntil(this.componentDestroyed))
      .subscribe(() => this.checkForTipsAndShowIfNeeded());
  }

  private showTipsForProcessPanel(): void {
    if (!this.processPanel.isNameValid()) {
      this.showTooltip('bpmn.tooltips.addProcessName');
    } else if (!this.processPanel.processTitleForm.valid) {
      this.mainTooltip.hide();
    } else if (this.processPanel.processTitleForm.valid) {
      this.mainTooltip.hide();
      if (this.firstTimeOnCurrentPanel && !this.processId) {
        this.firstTimeOnCurrentPanel = false;
        this.openPanel({ type: SelectedPanelType.TASK, task: this.process.tasks[0], force: true });
      }
    }
  }

  private showTipsForTaskPanel(): void {
    if (!this.taskPanel.isNameValid()) {
      this.showTooltip('bpmn.tooltips.addTaskName');
    } else {
      this.mainTooltip.hide();
      this.taskForm.showAddPropertyTipIfNeeded();
    }
  }

  private showTipsForPropertyPanel(): void {
    if (!this.propertyPanel.isNameValid()) {
      this.showTooltip('bpmn.tooltips.addPropertyName');
      this.taskForm.propertyNameTooltip.forEach(e => e.show());
    } else {
      this.mainTooltip.hide();
      this.taskForm.propertyNameTooltip.forEach(e => e.hide());
    }
  }

  private showTooltip(key: string): void {
    if (!this.mainTooltip.isOpen) {
      this.translateService.get(key).subscribe(
        translation => {
          this.tooltipText = translation;
          this.mainTooltip.show();
        });
    }
  }

  private subscribeForSliderClose(): void {
    this.sliderService.sliderStateChanged.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (change: { key: string, opened: boolean }) => this.resetAllForms(change)
    );
  }

  private resetAllForms(change: { key: string; opened: boolean }): void {
    this.initNewProcess();
    this.selectedPanel = { type: SelectedPanelType.PROCESS };
    this.firstTimeOnCurrentPanel = true;
  }

  private notifyIfProcessTypeChenged(process: BpmnProcess): void {
    if (this.processType !== process.objectType) {
      this.processTypeChanged.emit(process.objectType);
      this.processType = process.objectType;
      this.changeFirstTask(process.objectType);
    }
  }

  private changeFirstTask(processType: BpmnObjectType): void {
    this.process.tasks = this.process.tasks.filter(process => ![LEAD_TASK_NAME, OPPORTINITY_TASK_NAME].includes(process.name));
    this.process.tasks.unshift(this.getFirstTaskByType(processType));
  }

  private getFirstTaskByType(processType: BpmnObjectType): BpmnTask {
    switch (processType) {
      case BpmnObjectType.LEAD:
        return leadTask;
      case BpmnObjectType.OPPORTUNITY:
        return opportinityTask;
      default: return leadTask;
    }
  }
}
