"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ProcessFormComponent = void 0;
var core_1 = require("@angular/core");
var bpmn_model_1 = require("../bpmn.model");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var notification_service_1 = require("../../shared/notification.service");
var bpmn_util_1 = require("../bpmn.util");
var process_panel_component_1 = require("./panel/process-panel/process-panel.component");
var task_panel_component_1 = require("./panel/task-panel/task-panel.component");
var property_panel_component_1 = require("./panel/property-panel/property-panel.component");
var task_form_component_1 = require("./task-form/task-form.component");
var task_data_1 = require("./task-data");
var ProcessFormComponent = /** @class */ (function () {
    function ProcessFormComponent(personService, sliderService, notificationService, bpmnService, translateService) {
        this.personService = personService;
        this.sliderService = sliderService;
        this.notificationService = notificationService;
        this.bpmnService = bpmnService;
        this.translateService = translateService;
        this.processSaved = new core_1.EventEmitter();
        this.processTypeChanged = new core_1.EventEmitter();
        this.changeProcessId = false;
        this.process = { objectType: bpmn_model_1.BpmnObjectType.LEAD, tasks: [] };
        this.accountEmailList = [];
        this.selectedPanel = { type: bpmn_model_1.SelectedPanelType.PROCESS };
        this.selectedPanelTypeEnum = bpmn_model_1.SelectedPanelType;
        this.currentPanelValid = true;
        this.componentDestroyed = new rxjs_1.Subject();
        this.active = false;
        this.firstTimeOnCurrentPanel = true;
    }
    Object.defineProperty(ProcessFormComponent.prototype, "currentlyOpened", {
        set: function (change) {
            this.active = change;
            if (this.active) {
                this.initializeUserTipsMechanism();
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProcessFormComponent.prototype, "processDefinitionId", {
        set: function (id) {
            this.processId = id;
            this.getProcessFileByProcessDefinitionId(this.processId);
        },
        enumerable: false,
        configurable: true
    });
    ProcessFormComponent.prototype.ngOnInit = function () {
        this.getAccountEmailList();
        this.initNewProcess();
        this.subscribeForSliderClose();
    };
    ProcessFormComponent.prototype.ngOnDestroy = function () {
        this.componentDestroyed.next();
        this.componentDestroyed.complete();
    };
    ProcessFormComponent.prototype.updateProcess = function (process) {
        this.process = process;
        this.notifyIfProcessTypeChenged(process);
    };
    ProcessFormComponent.prototype.isPanelActive = function (type) {
        return this.selectedPanel.type === type;
    };
    ProcessFormComponent.prototype.openPanel = function (panel) {
        if (this.currentPanelValid || panel.force) {
            this.selectedPanel = panel;
        }
    };
    ProcessFormComponent.prototype.saveProcess = function () {
        var _this = this;
        if (this.currentPanelValid) {
            this.bpmnService.getBpmnFile(this.getProcess())
                .pipe(operators_1.takeUntil(this.componentDestroyed))
                .subscribe(function (result) {
                _this.uploadFile(_this.process.id, result.body);
            }, function (err) {
                _this.notificationService.notify('bpmn.processForm.saveErrorNotification', notification_service_1.NotificationType.ERROR);
                if (err.status === 403) {
                    _this.closeSlider();
                }
            });
        }
        else {
            this.notificationService.notify('bpmn.processForm.incorrectFormNotification', notification_service_1.NotificationType.ERROR);
        }
    };
    ProcessFormComponent.prototype.uploadFile = function (processId, blob) {
        var _this = this;
        this.bpmnService.saveProcess(processId, blob)
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (response) {
            _this.notificationService.notify('bpmn.processForm.processSavedNotification', notification_service_1.NotificationType.SUCCESS);
            _this.processSaved.emit(response.id);
            _this.closeSlider();
            if (!_this.processId) {
                _this.initNewProcess();
            }
        }, function () {
            _this.notificationService.notify('bpmn.processForm.saveErrorNotification', notification_service_1.NotificationType.ERROR);
        });
    };
    ProcessFormComponent.prototype.getProcess = function () {
        return bpmn_util_1.BpmnUtil.removeUnusedValues(this.process);
    };
    ProcessFormComponent.prototype.closeSlider = function () {
        this.sliderService.closeSlider();
    };
    ProcessFormComponent.prototype.checkForTipsAndShowIfNeeded = function () {
        if (this.selectedPanel.type === bpmn_model_1.SelectedPanelType.PROCESS) {
            this.showTipsForProcessPanel();
        }
        else if (this.selectedPanel.type === bpmn_model_1.SelectedPanelType.TASK) {
            this.showTipsForTaskPanel();
        }
        else if (this.selectedPanel.type === bpmn_model_1.SelectedPanelType.PROPERTY) {
            this.showTipsForPropertyPanel();
        }
    };
    ProcessFormComponent.prototype.getAccountEmailList = function () {
        var _this = this;
        this.personService.getAccountEmails()
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (response) { return _this.accountEmailList = response; });
    };
    ProcessFormComponent.prototype.getProcessFileByProcessDefinitionId = function (processDefinitionId) {
        var _this = this;
        if (!processDefinitionId) {
            return;
        }
        this.bpmnService.getFileByProcessDefinitionId(processDefinitionId)
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (response) { return _this.parseBpmnFile(response.body); }, function () { return _this.loadProcessError(); });
    };
    ProcessFormComponent.prototype.parseBpmnFile = function (file) {
        var _this = this;
        this.bpmnService.parseBpmnFile(file)
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (response) {
            _this.handleParseSuccess(response);
        }, function () { return _this.loadProcessError(); });
    };
    ProcessFormComponent.prototype.handleParseSuccess = function (response) {
        this.process = response;
        if (this.changeProcessId) {
            this.process.id = 'process' + bpmn_util_1.BpmnUtil.getUUID();
        }
    };
    ProcessFormComponent.prototype.loadProcessError = function () {
        this.notificationService.notify('bpmn.processForm.loadProcessErrorNotification', notification_service_1.NotificationType.ERROR);
        this.initNewProcess();
    };
    ProcessFormComponent.prototype.initNewProcess = function () {
        this.process = { objectType: bpmn_model_1.BpmnObjectType.LEAD, tasks: [task_data_1.leadTask], availableType: bpmn_model_1.ProcessAvailableType.ALL };
        if (!this.process.id) {
            this.process.id = 'process' + bpmn_util_1.BpmnUtil.getUUID();
        }
        if (!this.process.tasks) {
            this.process.tasks = [];
        }
    };
    ProcessFormComponent.prototype.initializeUserTipsMechanism = function () {
        var _this = this;
        rxjs_1.interval(1000)
            .pipe(operators_1.takeWhile(function () { return _this.active; }), operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function () { return _this.checkForTipsAndShowIfNeeded(); });
    };
    ProcessFormComponent.prototype.showTipsForProcessPanel = function () {
        if (!this.processPanel.isNameValid()) {
            this.showTooltip('bpmn.tooltips.addProcessName');
        }
        else if (!this.processPanel.processTitleForm.valid) {
            this.mainTooltip.hide();
        }
        else if (this.processPanel.processTitleForm.valid) {
            this.mainTooltip.hide();
            if (this.firstTimeOnCurrentPanel && !this.processId) {
                this.firstTimeOnCurrentPanel = false;
                this.openPanel({ type: bpmn_model_1.SelectedPanelType.TASK, task: this.process.tasks[0], force: true });
            }
        }
    };
    ProcessFormComponent.prototype.showTipsForTaskPanel = function () {
        if (!this.taskPanel.isNameValid()) {
            this.showTooltip('bpmn.tooltips.addTaskName');
        }
        else {
            this.mainTooltip.hide();
            this.taskForm.showAddPropertyTipIfNeeded();
        }
    };
    ProcessFormComponent.prototype.showTipsForPropertyPanel = function () {
        if (!this.propertyPanel.isNameValid()) {
            this.showTooltip('bpmn.tooltips.addPropertyName');
            this.taskForm.propertyNameTooltip.forEach(function (e) { return e.show(); });
        }
        else {
            this.mainTooltip.hide();
            this.taskForm.propertyNameTooltip.forEach(function (e) { return e.hide(); });
        }
    };
    ProcessFormComponent.prototype.showTooltip = function (key) {
        var _this = this;
        if (!this.mainTooltip.isOpen) {
            this.translateService.get(key).subscribe(function (translation) {
                _this.tooltipText = translation;
                _this.mainTooltip.show();
            });
        }
    };
    ProcessFormComponent.prototype.subscribeForSliderClose = function () {
        var _this = this;
        this.sliderService.sliderStateChanged.pipe(operators_1.takeUntil(this.componentDestroyed)).subscribe(function (change) { return _this.resetAllForms(change); });
    };
    ProcessFormComponent.prototype.resetAllForms = function (change) {
        this.initNewProcess();
        this.selectedPanel = { type: bpmn_model_1.SelectedPanelType.PROCESS };
        this.firstTimeOnCurrentPanel = true;
    };
    ProcessFormComponent.prototype.notifyIfProcessTypeChenged = function (process) {
        if (this.processType !== process.objectType) {
            this.processTypeChanged.emit(process.objectType);
            this.processType = process.objectType;
            this.changeFirstTask(process.objectType);
        }
    };
    ProcessFormComponent.prototype.changeFirstTask = function (processType) {
        if (processType === bpmn_model_1.BpmnObjectType.LEAD) {
            this.process.tasks.unshift(task_data_1.leadTask);
        }
        else {
            this.process.tasks = this.process.tasks.filter(function (process) { return process.name !== task_data_1.leadTaskName; });
            if (!this.process.tasks.length) {
                var newElement = { name: '', documentation: '', uuid: bpmn_util_1.BpmnUtil.getUUID() };
                this.process.tasks.push(newElement);
            }
        }
    };
    __decorate([
        core_1.Output()
    ], ProcessFormComponent.prototype, "processSaved");
    __decorate([
        core_1.Output()
    ], ProcessFormComponent.prototype, "processTypeChanged");
    __decorate([
        core_1.Input()
    ], ProcessFormComponent.prototype, "changeProcessId");
    __decorate([
        core_1.Input()
    ], ProcessFormComponent.prototype, "currentlyOpened");
    __decorate([
        core_1.Input()
    ], ProcessFormComponent.prototype, "processDefinitionId");
    __decorate([
        core_1.ViewChild(process_panel_component_1.ProcessPanelComponent)
    ], ProcessFormComponent.prototype, "processPanel");
    __decorate([
        core_1.ViewChild(task_panel_component_1.TaskPanelComponent)
    ], ProcessFormComponent.prototype, "taskPanel");
    __decorate([
        core_1.ViewChild(property_panel_component_1.PropertyPanelComponent)
    ], ProcessFormComponent.prototype, "propertyPanel");
    __decorate([
        core_1.ViewChild(task_form_component_1.TaskFormComponent)
    ], ProcessFormComponent.prototype, "taskForm");
    __decorate([
        core_1.ViewChild('mainTooltip')
    ], ProcessFormComponent.prototype, "mainTooltip");
    ProcessFormComponent = __decorate([
        core_1.Component({
            selector: 'app-process-form',
            templateUrl: './process-form.component.html',
            styleUrls: ['./process-form.component.scss']
        })
    ], ProcessFormComponent);
    return ProcessFormComponent;
}());
exports.ProcessFormComponent = ProcessFormComponent;
