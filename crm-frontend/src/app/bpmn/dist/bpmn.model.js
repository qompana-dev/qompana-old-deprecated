"use strict";
exports.__esModule = true;
exports.AvailableColors = exports.SelectedPanelType = exports.BpmnObjectType = exports.ProcessAvailableType = exports.BpmnType = void 0;
var BpmnType;
(function (BpmnType) {
    BpmnType["STRING"] = "STRING";
    BpmnType["LONG"] = "LONG";
    BpmnType["DOUBLE"] = "DOUBLE";
    BpmnType["ENUM"] = "ENUM";
    BpmnType["DATE"] = "DATE";
    BpmnType["BOOLEAN"] = "BOOLEAN";
})(BpmnType = exports.BpmnType || (exports.BpmnType = {}));
var ProcessAvailableType;
(function (ProcessAvailableType) {
    ProcessAvailableType["ALL"] = "ALL";
    ProcessAvailableType["DEPARTMENT"] = "DEPARTMENT";
})(ProcessAvailableType = exports.ProcessAvailableType || (exports.ProcessAvailableType = {}));
var BpmnObjectType;
(function (BpmnObjectType) {
    BpmnObjectType["LEAD"] = "LEAD";
    BpmnObjectType["OPPORTUNITY"] = "OPPORTUNITY";
})(BpmnObjectType = exports.BpmnObjectType || (exports.BpmnObjectType = {}));
var SelectedPanelType;
(function (SelectedPanelType) {
    SelectedPanelType["PROCESS"] = "PROCESS";
    SelectedPanelType["TASK"] = "TASK";
    SelectedPanelType["PROPERTY"] = "PROPERTY";
})(SelectedPanelType = exports.SelectedPanelType || (exports.SelectedPanelType = {}));
var AvailableColors = /** @class */ (function () {
    function AvailableColors() {
    }
    AvailableColors.byBgId = function (id) {
        return AvailableColors.colors.find(function (c) { return c.id === id; });
    };
    AvailableColors.byBgColor = function (bgColor) {
        return AvailableColors.colors.find(function (c) { return c.hex === bgColor; });
    };
    AvailableColors.byColorName = function (colorName) {
        return AvailableColors.colors.find(function (c) { return c.colorName === colorName; });
    };
    AvailableColors.whiteColor = function () {
        return this.byBgId(-1);
    };
    AvailableColors.colors = [
        { id: -3, key: 'bpmn.panel.process.color.red', hex: '#ff7777', textHex: '#8A94A6', colorName: 'red', selectedBg: '#ff7777' },
        { id: -2, key: 'bpmn.panel.process.color.lightGreen', hex: '#3ACB9C', textHex: '#fff', colorName: 'green', selectedBg: '#3ACB9C' },
        { id: -1, key: 'bpmn.panel.process.color.white', hex: '#EAECEF', textHex: '#8A94A6', colorName: 'white', selectedBg: '#F7F7F9' },
        { id: 1, key: 'bpmn.panel.process.color.blue', hex: '#319DF8', textHex: '#fff', colorName: 'blue', selectedBg: '#F1FAFF' },
        { id: 2, key: 'bpmn.panel.process.color.lightPurple', hex: '#AF7EFF', textHex: '#fff', colorName: 'purple', selectedBg: '#F9F4FF' },
        { id: 3, key: 'bpmn.panel.process.color.lightPink', hex: '#FF718C', textHex: '#fff', colorName: 'pink', selectedBg: '#FFF3F6' },
        { id: 4, key: 'bpmn.panel.process.color.turquoise', hex: '#1DC9D7', textHex: '#fff', colorName: 'turquoise', selectedBg: '#EDFBFC' },
        { id: 5, key: 'bpmn.panel.process.color.lightBrown', hex: '#837575', textHex: '#fff', colorName: 'brown', selectedBg: '#F9F5F0' },
    ];
    return AvailableColors;
}());
exports.AvailableColors = AvailableColors;
