import {AvailableColors, BpmnProcess} from './bpmn.model';
import cloneDeep from 'lodash/cloneDeep';
import * as uuid from 'uuid';
import {FormGroup} from '@angular/forms';
import {FinishOpportunityResult, OpportunityDetails} from '../opportunity/opportunity.model';
import {LeadDetails} from '../lead/lead-details/lead-details.model';
import {CloseLeadResult} from '../lead/lead.model';

export class BpmnUtil {
  static removeUnusedValues(process: BpmnProcess): BpmnProcess {
    const result: BpmnProcess = cloneDeep(process);
    if (!result || !result.tasks) {
      return result;
    }
    result.tasks = result.tasks.map(task => {
      delete task.uuid;
      if (task.properties) {
        task.properties = task.properties.map(property => {
          delete property.uuid;
          return property;
        });
      }
      return task;
    });
    return result;
  }

  static getUUID(): string {
    return uuid.v4();
  }

  static getColorNameFromLead(leadDetails: LeadDetails, isActiveTask: boolean): string {
    if (leadDetails.closeResult === CloseLeadResult.ARCHIVED && isActiveTask) {
      return this.getColorNameByBgColorHex(AvailableColors.byColorName('red').hex);
    }
    return this.getColorNameByBgColorHex(leadDetails.bgColor);
  }

  static getColorNameFromOpportunity(opportunityDetail: OpportunityDetails, isLastTask?: boolean): string {
    if (opportunityDetail.finishResult === FinishOpportunityResult.SUCCESS && isLastTask) {
      return this.getColorNameByBgColorHex(AvailableColors.byColorName('green').hex);
    }
    if (opportunityDetail.finishResult === FinishOpportunityResult.ERROR && isLastTask) {
      return this.getColorNameByBgColorHex(AvailableColors.byColorName('red').hex);
    }
    return this.getColorNameByBgColorHex(opportunityDetail.bgColor);
  }

  static getColorNameByBgColorHex(hex: string): string {
    const bgColor = AvailableColors.colors.find(color => color.hex.toLowerCase() === (hex && hex.toLowerCase()));
    if (!bgColor) {
      return 'blue';
    }
    return bgColor.colorName;
  }

  static setTouched(formGroup: FormGroup): void {
    if (formGroup) {
      formGroup.markAsTouched();
      (Object as any).values(formGroup.controls).forEach(control => control.markAsTouched());
    }
  }
}
