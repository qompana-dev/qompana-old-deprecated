import {FormGroup} from '@angular/forms';
import {BpmnFilterStrategy} from "./bpmn-filter-strategy";

export interface BpmnProcess {
  id?: string;
  name?: string;
  bgColor?: string;
  textColor?: string;
  objectType: BpmnObjectType;
  availableType?: ProcessAvailableType;
  department?: string;
  documentation?: string;
  tasks?: BpmnTask[];
}

export interface BpmnTask {
  name: string;
  documentation?: string;
  acceptanceUser?: string;
  acceptanceGroup?: string;
  properties?: BpmnProperty[];
  salesOpportunity?: number;
  uuid?: string;
  notificationUser?: string;
  notificationGroup?: string;
  isInformationTask?: boolean;
  daysToComplete?: number;
}

export interface BpmnProperty {
  id: string;
  type: BpmnType;
  required: boolean;
  possibleValues?: BpmnPossibleValue[];
  uuid?: string;
}

export interface BpmnPossibleValue {
  id: string;
  name: string;
}

export interface PrimaryProcess {
  processId: string;
  type: BpmnFilterStrategy;
}
export enum BpmnType {
  STRING = "STRING",
  LONG = "LONG",
  DOUBLE = "DOUBLE",
  ENUM = "ENUM",
  DATE = "DATE",
  BOOLEAN = "BOOLEAN",
}

export enum ProcessAvailableType {
  ALL = "ALL",
  DEPARTMENT = "DEPARTMENT",
}

export enum BpmnObjectType {
  LEAD = "LEAD",
  OPPORTUNITY = "OPPORTUNITY",
}

export interface BpmnGuiProperties {
  newPropertyName: string;
  formGroup: FormGroup;
}

export interface SelectedPanel {
  type: SelectedPanelType;
  task?: BpmnTask;
  property?: BpmnProperty;
  force?: boolean;
}

export enum SelectedPanelType {
  PROCESS = "PROCESS",
  TASK = "TASK",
  PROPERTY = "PROPERTY",
}

export enum StatusAcceptance {
  DECLINED = "DECLINED",
  ACCEPTED = "ACCEPTED",
  NEEDS_ACCEPTATION = "NEEDS_ACCEPTATION",
}

export interface CreateProcessFromFileResponse {
  id: string;
  name: string;
  version: number;
}

export interface ProcessDefinitionResponse {
  data: ProcessDefinitionDataResponse[];
  total: number;
  start: number;
  sort: string;
  order: string;
  size: number;
}

export interface ProcessDefinitionDataResponse {
  id: string;
  url: string;
  key: string;
  version: number;
  name: string;
  description: string;
  tenantId: string;
  deploymentId: string;
  deploymentUrl: string;
  resource: string;
  diagramResource: string;
  category: string;
  graphicalNotationDefined: string;
  suspended: string;
  startFormDefined: string;
}

export interface ProcessDefinitionDTO {
  id: string;
  name: string;
  size: number;
  backgroundColor: string;
  textColor: string;
  tasks: string[];
}

export interface AvailableColor {
  id: number;
  key: string;
  hex: string;
  textHex: string;
  colorName: string;
  selectedBg: string;
}

export interface ProcessDefinition {
  id: string;
  version: number;
  name: string;
  firstTaskName: string;
}

export class AvailableColors {
  static colors: AvailableColor[] = [
    {
      id: -4,
      key: "bpmn.panel.process.color.yellow",
      hex: "#ffad0c",
      textHex: "#8A94A6",
      colorName: "yellow",
      selectedBg: "#ffad0c",
    },
    {
      id: -3,
      key: "bpmn.panel.process.color.red",
      hex: "#ff7777",
      textHex: "#8A94A6",
      colorName: "red",
      selectedBg: "#ff7777",
    },
    {
      id: -2,
      key: "bpmn.panel.process.color.lightGreen",
      hex: "#3ACB9C",
      textHex: "#fff",
      colorName: "green",
      selectedBg: "#3ACB9C",
    },
    {
      id: -1,
      key: "bpmn.panel.process.color.white",
      hex: "#EAECEF",
      textHex: "#8A94A6",
      colorName: "white",
      selectedBg: "#F7F7F9",
    },
    {
      id: 1,
      key: "bpmn.panel.process.color.blue",
      hex: "#319DF8",
      textHex: "#fff",
      colorName: "blue",
      selectedBg: "#F1FAFF",
    },
    {
      id: 2,
      key: "bpmn.panel.process.color.lightPurple",
      hex: "#AF7EFF",
      textHex: "#fff",
      colorName: "purple",
      selectedBg: "#F9F4FF",
    },
    {
      id: 3,
      key: "bpmn.panel.process.color.lightPink",
      hex: "#FF718C",
      textHex: "#fff",
      colorName: "pink",
      selectedBg: "#FFF3F6",
    },
    {
      id: 4,
      key: "bpmn.panel.process.color.turquoise",
      hex: "#1DC9D7",
      textHex: "#fff",
      colorName: "turquoise",
      selectedBg: "#EDFBFC",
    },
    {
      id: 5,
      key: "bpmn.panel.process.color.lightBrown",
      hex: "#837575",
      textHex: "#fff",
      colorName: "brown",
      selectedBg: "#F9F5F0",
    },
  ];

  static byBgId(id: number): AvailableColor {
    return AvailableColors.colors.find((c) => c.id === id);
  }

  static byBgColor(bgColor: string): AvailableColor {
    return AvailableColors.colors.find((c) => c.hex === bgColor);
  }

  static byColorName(colorName: string): AvailableColor {
    return AvailableColors.colors.find((c) => c.colorName === colorName);
  }

  static whiteColor(): AvailableColor {
    return this.byBgId(-1);
  }
}
