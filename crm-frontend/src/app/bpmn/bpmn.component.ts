import {AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SliderService, SliderStateChange} from '../shared/slider/slider.service';
import {BpmnFilterStrategy} from './bpmn-filter-strategy';
import {BpmnService} from './bpmn.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {BpmnObjectType, PrimaryProcess, ProcessDefinitionDTO} from './bpmn.model';
import {ErrorTypes, NotificationService, NotificationType} from '../shared/notification.service';
import {SliderComponent} from '../shared/slider/slider.component';
import {BAD_REQUEST, NOT_FOUND} from 'http-status-codes';
import {BpmnUtil} from './bpmn.util';
import {AuthService} from '../login/auth/auth.service';

@Component({
    selector: 'app-bpmn',
    templateUrl: './bpmn.component.html',
    styleUrls: ['./bpmn.component.scss']
})
export class BpmnComponent implements OnInit, OnDestroy, AfterViewChecked {

    @ViewChild('processSlider') processSlider: SliderComponent;
    processId: string;
    changeProcessId = false;
    sliderOpened: boolean;
    sliderTitle: string;
    selectedFilter: BpmnFilterStrategy = BpmnFilterStrategy.LEAD;
    filterSelection = BpmnFilterStrategy;
    processes: ProcessDefinitionDTO[] = [];
    primaryProcesses: PrimaryProcess[] = [];
    expanded = false;
    bpmnUtil = BpmnUtil;
    private componentDestroyed: Subject<void> = new Subject();

    readonly errorTypes: ErrorTypes = {
        base: 'bpmn.list.notifications.',
        defaultText: 'problemDeleting',
        errors: [
            {
                code: BAD_REQUEST,
                text: 'leadsAreAssigned'
            }, {
                code: NOT_FOUND,
                text: 'processNotFound'
            }
        ]
    };

    constructor(private sliderService: SliderService,
                private bpmnService: BpmnService,
                private authService: AuthService,
                private notificationService: NotificationService,
                private cdr: ChangeDetectorRef) {
    }

    ngOnInit(): void {
        this.subscribeForSliderChanges();
        if (!this.authService.isPermissionPresent('BpmnComponent.list', 'i')) {
            this.initList(this.selectedFilter);
            this.getPrimaryProcesses();
        }
    }

    ngOnDestroy(): void {
        this.componentDestroyed.next();
        this.componentDestroyed.complete();
    }

    ngAfterViewChecked() {
        this.cdr.detectChanges();
    }

    public filter(selection: BpmnFilterStrategy): void {
        this.selectedFilter = selection;
        this.initList(selection);
    }

    public isSelected(selection: BpmnFilterStrategy): boolean {
        return this.selectedFilter === selection;
    }

    onProcessAdded(): void {
        this.processId = null;
        this.initList(this.selectedFilter);
    }

    editProcess(id: string): void {
        this.changeProcessId = false;
        this.processId = id;
        this.processSlider.open();
    }

    addProcess(): void {
        this.changeProcessId = false;
        this.processSlider.open();
    }

    deleteProcess(id: string): void {
        this.bpmnService.deleteProcess(id).pipe(
            takeUntil(this.componentDestroyed)
        ).subscribe(
            () => this.handleDeleteSuccess(),
            (error) => this.notificationService.notifyError(this.errorTypes, error.status)
        );
    }

    createBasedOnOld(id: string): void {
        this.changeProcessId = true;
        this.processId = id;
        this.processSlider.open();
    }

    private handleDeleteSuccess(): void {
        this.initList(this.selectedFilter);
        this.notificationService.notify('bpmn.list.notifications.deleted', NotificationType.SUCCESS);
    }

    private subscribeForSliderChanges(): void {
        this.sliderService.sliderStateChanged.subscribe(
            (change: SliderStateChange) => {
                if (change.key === 'processSlider') {
                    this.sliderOpened = change.opened;
                    if (!change.opened) {
                        this.processId = null;
                    }
                }
            }
        );
    }

    private initList(selection: BpmnFilterStrategy): void {
        this.bpmnService.getProcessDefinitionList(selection).pipe(
            takeUntil(this.componentDestroyed)
        ).subscribe(
            (processes) => this.processes = processes,
            () => this.notificationService.notify('bpmn.list.notifications.errorGettingList', NotificationType.ERROR)
        );
    }

    private getPrimaryProcesses(): void {
        this.bpmnService.getAllPrimaryProcesses().pipe(
            takeUntil(this.componentDestroyed)
        ).subscribe(
            (primaryProcesses) => this.primaryProcesses = primaryProcesses,
            // () => this.notificationService.notify('bpmn.list.notifications.errorGettingList', NotificationType.ERROR)
        );
    }

    onProcessChanged(type: BpmnObjectType): void {
        if (type === BpmnObjectType.LEAD) {
            this.sliderTitle = 'bpmn.leadSliderTitle';
        }
        if (type === BpmnObjectType.OPPORTUNITY) {
            this.sliderTitle = 'bpmn.opportunitySliderTitle';
        }

    }

    isPrimary(processId: string): boolean {
        return this.primaryProcesses.some(primaryProcess => primaryProcess.processId === processId);
    }

    specifyAsPrimaryProcess(process: ProcessDefinitionDTO, type: BpmnFilterStrategy): void {
        this.bpmnService.specifyPrimaryProcessForType(process.id, type)
            .subscribe(
                (primaryProcess) => {
                    this.getPrimaryProcesses();
                },
                // () => this.notificationService.notify('bpmn.list.notifications.errorGettingList', NotificationType.ERROR)
            );
    }

    deletePrimaryProcess(process: ProcessDefinitionDTO, type: BpmnFilterStrategy): void {
        this.bpmnService.deletePrimaryProcess(process.id, type)
            .subscribe(
                (primaryProcess) => {
                    this.getPrimaryProcesses();
                },
                // () => this.notificationService.notify('bpmn.list.notifications.errorGettingList', NotificationType.ERROR)
            );
    };
}
