import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterBpmTasks'
})
export class FilterBpmTasksPipe implements PipeTransform {

  transform(values: string[]): any {
    return (!values) ? values : values.filter(value => value && value !== '');
  }

}
