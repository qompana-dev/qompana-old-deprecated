export enum BpmnFilterStrategy {
  LEAD = 'LEAD',
  OPPORTUNITY = 'OPPORTUNITY'
}
