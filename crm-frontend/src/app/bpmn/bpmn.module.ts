import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BpmnRoutingModule} from './bpmn-routing.module';
import {SharedModule} from '../shared/shared.module';
import {ProcessFormComponent} from './process-form/process-form.component';
import {BpmnComponent} from './bpmn.component';
import {ProcessPanelComponent} from './process-form/panel/process-panel/process-panel.component';
import {TaskFormComponent} from './process-form/task-form/task-form.component';
import {TaskPanelComponent} from './process-form/panel/task-panel/task-panel.component';
import {PropertyPanelComponent} from './process-form/panel/property-panel/property-panel.component';
import {PropertyListFormComponent} from './process-form/panel/property-panel/property-list-form/property-list-form.component';
import {SelectedColorOnTopPipe} from './process-form/panel/process-panel/selected-color-on-top.pipe';
import {BpmnTextEditorComponent} from './process-form/panel/task-panel/bpmn-text-editor/bpmn-text-editor.component';
import {NgxEditorModule} from 'ngx-editor';
import {TooltipModule} from 'ngx-bootstrap';
import {FilterBpmTasksPipe} from './filter-bpm-tasks.pipe';

@NgModule({
  declarations: [
    ProcessFormComponent,
    BpmnComponent,
    ProcessPanelComponent,
    TaskFormComponent,
    TaskPanelComponent,
    PropertyPanelComponent,
    PropertyListFormComponent,
    SelectedColorOnTopPipe,
    BpmnTextEditorComponent,
    FilterBpmTasksPipe
  ],
  imports: [
    CommonModule,
    SharedModule,
    BpmnRoutingModule,
    NgxEditorModule,
    TooltipModule.forRoot()
  ]
})
export class BpmnModule {
}
