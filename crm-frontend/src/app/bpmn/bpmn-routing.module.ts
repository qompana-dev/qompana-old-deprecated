import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../login/auth/auth-guard.service';
import {BpmnComponent} from './bpmn.component';

const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: BpmnComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BpmnRoutingModule {
}
