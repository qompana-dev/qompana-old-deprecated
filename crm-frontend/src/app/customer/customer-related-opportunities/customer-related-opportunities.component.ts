import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SliderComponent} from '../../shared/slider/slider.component';
import {ContactSalesOpportunity} from '../../contacts/contacts.model';
import {Observable, Subject} from 'rxjs';
import {OpportunitySave} from '../../opportunity/opportunity.model';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import {DeleteDialogData} from '../../shared/dialog/delete/delete-dialog.component';
import {OpportunityService} from '../../opportunity/opportunity.service';
import {DeleteDialogService} from '../../shared/dialog/delete/delete-dialog.service';
import {Router} from '@angular/router';
import {AuthService} from '../../login/auth/auth.service';
import {LoginService} from '../../login/login.service';
import {map, startWith, takeUntil} from 'rxjs/operators';
import {CustomerService} from '../customer.service';
import {CustomerOpportunityDto} from '../customer.model';

@Component({
  selector: 'app-customer-related-opportunities',
  templateUrl: './customer-related-opportunities.component.html',
  styleUrls: ['./customer-related-opportunities.component.scss']
})
export class CustomerRelatedOpportunitiesComponent implements OnInit, OnDestroy {


  @ViewChild('addSlider') addSlider: SliderComponent;
  @ViewChild('editSlider') editSlider: SliderComponent;

  // tslint:disable-next-line:variable-name
  _customerId: number;
  customerOpportunities: CustomerOpportunityDto[];
  columns = ['name', 'status', 'amount', 'finishDate', 'action'];
  filteredColumns$: Observable<string[]>;
  selectedOpportunity: OpportunitySave;
  opportunityWithContact: Partial<OpportunitySave>;
  private componentDestroyed: Subject<void> = new Subject();

  @Input() set customerId(customerId: number) {
    if (customerId) {
      this._customerId = customerId;
      this.subscribeForCustomerOpportunities(customerId);
    }
  }

  private readonly opportunityErrorTypes: ErrorTypes = {
    base: 'opportunity.list.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'opportunityNotFound'
      }, {
        code: FORBIDDEN,
        text: 'noPermission'
      }
    ]
  };

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'opportunity.form.dialog.delete.title',
    description: 'opportunity.form.dialog.delete.description',
    noteFirst: 'opportunity.form.dialog.delete.noteFirstPart',
    noteSecond: 'opportunity.form.dialog.delete.noteSecondPart',
    confirmButton: 'opportunity.form.dialog.delete.confirm',
    cancelButton: 'opportunity.form.dialog.delete.cancel',
  };

  constructor(private notificationService: NotificationService,
              private customerService: CustomerService,
              private opportunityService: OpportunityService,
              private deleteDialogService: DeleteDialogService,
              private $router: Router,
              private authService: AuthService,
              private loginService: LoginService) { }

  ngOnInit(): void {
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
    this.customerService.customersChanged$
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
      () => this.subscribeForCustomerOpportunities(this._customerId)
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  edit(id: number): void {
    this.opportunityService.getOpportunityForEditForSlider(id).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      opportunity => {
        this.selectedOpportunity = opportunity;
        this.editSlider.open();
      },
      err => this.notificationService.notifyError(this.opportunityErrorTypes, err.status)
    );
  }

  tryDeleteOpportunity(opportunityId: number): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteOpportunity(opportunityId);
    this.openDeleteDialog();
  }

  addOpportunity(): void {
    this.customerService.getCustomerById(this._customerId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
      (customer) => {
        this.opportunityWithContact = {customerId: customer.id, canEditAmount: true,
          ownerOneId: this.loginService.getLoggedAccountId(), ownerOnePercentage: 100};
        this.addSlider.open();
      }
    );
  }

  getOpportunityProgress(opportunity: ContactSalesOpportunity): number {
    return opportunity.maxStatus ? (opportunity.status / opportunity.maxStatus) * 100 : 0;
  }

  handleOpportunityAdded(): void {
    this.addSlider.close();
    this.editSlider.close();
    this.customerService.updateCustomers();
  }

  goToOpportunityDetails(id: number): void {
    this.$router.navigate(['/opportunity', id]);
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private deleteOpportunity(opportunityId: number): void {
    this.opportunityService.removeOpportunity(opportunityId).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      () => {
        this.notificationService.notify('opportunity.list.notification.deleteSuccess', NotificationType.SUCCESS);
        this.customerService.updateCustomers();
      },
      err => this.notificationService.notifyError(this.opportunityErrorTypes, err.status)
    );
  }

  private subscribeForCustomerOpportunities(customerId: number): void {
    this.customerService.getCustomerOpportunities(customerId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (customerOpportunities: ContactSalesOpportunity[]) => this.customerOpportunities = customerOpportunities,
      // tslint:disable-next-line:max-line-length
      () => this.notificationService.notify('customer.dashboard.tabs.salesOpportunities.notifications.getOpportunitiesError', NotificationType.ERROR)
    );
  }

  private getFilteredColumns(): string[] {
    return this.columns
      .filter((item) => !this.authService.isPermissionPresent('CustomerOpportunity.' + item, 'i') || item === 'action');
  }
}
