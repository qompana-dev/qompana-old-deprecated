import { CustomerDialogsConfig } from './customer-list.dialogs';
import { ConfirmDialogService } from "src/app/shared/dialog/confirm-dialog/confirm-dialog.service";
import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { Observable, of, Subject } from "rxjs";
import { MatTable, Sort } from "@angular/material";
import { Router } from "@angular/router";
import { AuthService } from "../../login/auth/auth.service";
import { map, startWith, takeUntil, finalize } from "rxjs/operators";
import { Customer } from "../customer.model";
import { CustomerService } from "../customer.service";
import {
  ErrorTypes,
  NotificationService,
  NotificationType,
} from "../../shared/notification.service";
import { BAD_REQUEST, NOT_FOUND } from "http-status-codes";
import { SliderComponent } from "../../shared/slider/slider.component";
import {
  SliderService,
  SliderStateChange,
} from "../../shared/slider/slider.service";
import { PersonService } from "../../person/person.service";
import { AccountEmail } from "../../person/person.model";
import { TranslateService } from "@ngx-translate/core";
import { Page, PageRequest } from "../../shared/pagination/page.model";
import { Contact } from "../../contacts/contacts.model";
import { DeleteDialogData } from "../../shared/dialog/delete/delete-dialog.component";
import { FilterStrategy } from "../../shared/model";
import { DeleteDialogService } from "../../shared/dialog/delete/delete-dialog.service";
import { ContentTypes } from "src/app/shared/dialog/confirm-dialog/confirm-dialog.model";

@Component({
  selector: "app-customer-list",
  templateUrl: "./customer-list.component.html",
  styleUrls: ["./customer-list.component.scss"],
})
export class CustomerListComponent implements OnInit, OnDestroy {
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild("basicInfoSlider") basicInfoSlider: SliderComponent;
  @ViewChild("transferSlider") transferSlider: SliderComponent;

  filteredColumns$: Observable<string[]>;
  customersPage: Page<Customer>;
  collapsed = true;
  customerId: number;
  basicInfoSelectedCustomerId: number;
  userMap: Map<number, string> = new Map();
  focusOwnerForm = false;
  selectedCustomers: Customer[];
  search = "";

  filterSelection = FilterStrategy;
  selectedFilter: FilterStrategy = FilterStrategy.ALL;

  private readonly dialogsConfig = new CustomerDialogsConfig(
    this.translateService
  );

  private readonly columns: string[] = [
    "selectRow",
    "avatar",
    "name",
    "companyType",
    "city",
    "status",
    "firstToContact",
    "owner",
    "action",
  ];

  private readonly expandColumns: string[] = ["avatar"];
  readonly tooltipPosition = {
    originX: "center",
    overlayX: "start",
    offsetY: -4,
  };

  loadingFinished = true;
  private componentDestroyed: Subject<void> = new Subject();
  private fetchCustomers: Subject<void> = new Subject();

  private currentSort = "id,asc";
  private pageOnList = 20;
  totalElements = 0;

  readonly deleteCustomerError: ErrorTypes = {
    base: "customer.list.notification.",
    defaultText: "undefinedError",
    errors: [
      {
        code: NOT_FOUND,
        text: "customerNotFound",
      },
      {
        code: BAD_REQUEST,
        text: "usedInOpportunity",
      },
    ],
  };

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: "customer.list.dialog.remove.title",
    description: "customer.list.dialog.remove.description",
    noteFirst: "customer.list.dialog.remove.noteFirstPart",
    noteSecond: "customer.list.dialog.remove.noteSecondPart",
    confirmButton: "customer.list.dialog.remove.buttonYes",
    cancelButton: "customer.list.dialog.remove.buttonNo",
  };

  constructor(
    private $router: Router,
    private authService: AuthService,
    private personService: PersonService,
    private customerService: CustomerService,
    private translateService: TranslateService,
    private notificationService: NotificationService,
    private sliderService: SliderService,
    private confirmDialogService: ConfirmDialogService
  ) {}

  ngOnInit(): void {
    this.getCustomers(FilterStrategy.ALL);
    this.subscribeForSliderClose();
    this.subscribeForPersonList();

    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );

    this.sliderService.sliderStateChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((state: SliderStateChange) => {
        if (!state.opened) {
          this.focusOwnerForm = false;
        }
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.fetchCustomers.next();
    this.componentDestroyed.complete();
    this.fetchCustomers.complete();
  }

  public tryDeleteSelectedCustomers(): void {
    const dialogData = this.dialogsConfig.deleteCustomers(
      this.deleteSelectedCustomers.bind(this)
    );

    this.openDeleteDialog(dialogData);
  }

  public deleteSelectedCustomers(): void {
    if (this.customersPage && this.customersPage.content) {
      const toDelete: Customer[] = this.customersPage.content.filter(
        (customer) => !!customer.checked
      );

      this.customerService
        .removeCustomers(toDelete)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => {
            this.getCustomers(this.selectedFilter);
            this.notifyRemoveSuccess();
          },
          (err) =>
            this.notificationService.notifyError(
              this.deleteCustomerError,
              err.status
            )
        );
    }
  }

  public transferSelectedCustomers(): void {
    this.selectedCustomers = this.customersPage.content.filter(
      (customer) => customer.checked
    );
    this.transferSlider.toggle();
  }

  private openDeleteDialog(dialogData = {}): void {
    this.confirmDialogService.open(dialogData);
  }

  public tryDeleteCustomer(customer: Customer): void {
    this.customerId = customer.id;

    const dialogData = this.dialogsConfig.deleteCustomer(
      customer,
      this.deleteCustomer.bind(this)
    );

    this.openDeleteDialog(dialogData);
  }

  private arhiveCustomer(id: number): void {
    this.customerService
      .archiveCustomer(id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.getCustomers(this.selectedFilter);
      });
  }

  private arhiveCustomers(): void {
    if (this.customersPage && this.customersPage.content) {
      const toArchive: Customer[] = this.customersPage.content.filter(
        (customer) => !!customer.checked
      );

      this.customerService
        .archiveCustomers(toArchive)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => {
          this.getCustomers(this.selectedFilter);
        });
    }
  }

  public tryArchiveCustomer(customer: Customer): void {
    const dialogData = this.dialogsConfig.archiveCustomer(customer, () =>
      this.arhiveCustomer(customer.id)
    );

    this.openDeleteDialog(dialogData);
  }

  public tryArchiveCustomers(): void {
    const dialogData = this.dialogsConfig.archiveCustomers(
      this.arhiveCustomers.bind(this)
    );

    this.openDeleteDialog(dialogData);
  }

  
  public goContactToDetails(id: number): void {
    this.$router.navigate(['/contacts', id]);
  }

  public openBasicInfo(customerId: number): void {
    this.basicInfoSelectedCustomerId = customerId;
    this.basicInfoSlider.toggle();
  }

  public onSortChange($event: Sort): void {
    this.currentSort = $event.direction
      ? $event.active + "," + $event.direction
      : "id,asc";
    this.getCustomers(this.selectedFilter);
  }

  changeCollapse(state: boolean) {
    this.collapsed = state;
    this.filteredColumns$ = of(this.getFilteredColumns());
  }

  getCustomers(
    selection: FilterStrategy,
    pageRequest: PageRequest = {
      page: "0",
      size: this.pageOnList + "",
      sort: this.currentSort,
    }
  ): void {
    this.fetchCustomers.next();
    this.loadingFinished = false;

    this.customerService
      .getCustomers(selection, pageRequest, this.search)
      .pipe(
        takeUntil(this.fetchCustomers),
        finalize(() => (this.loadingFinished = true))
      )
      .subscribe(
        (page) => this.handleCustomersSuccess(page),
        () =>
          this.notificationService.notify(
            "customer.list.notification.listError",
            NotificationType.ERROR
          )
      );
  }

  changePagination(page) {
    const { pageIndex, pageSize } = page;
    this.pageOnList = pageSize;
    this.getCustomers(FilterStrategy.ALL, {
      page: pageIndex,
      size: pageSize,
      sort: this.currentSort,
    });
  }

  goToDetails(id: number): void {
    this.$router.navigate(["customers", id]);
  }

  getFirstToContact(customer: Customer): Contact {
    if (
      customer &&
      customer.firstToContact &&
      customer.firstToContact.length !== 0
    ) {
      return customer.firstToContact[0];
    }
    return undefined;
  }

  public isSelected(selection: FilterStrategy): boolean {
    return this.selectedFilter === selection;
  }

  public filter(selection: FilterStrategy): void {
    this.selectedFilter = selection;
    this.getCustomers(selection);
  }

  private getFilteredColumns(): string[] {
    return this.columns.filter((item) => {
      if (this.collapsed && this.expandColumns.includes(item)) {
        return false;
      }
      return (
        !this.authService.isPermissionPresent(
          "CustomerListComponent." + item + "Column",
          "i"
        ) ||
        item === "action" ||
        item === "selectRow" ||
        item === "avatar" ||
        true
      );
    });
  }

  private deleteCustomer(): void {
    this.customerService
      .removeCustomer(this.customerId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.getCustomers(this.selectedFilter);
          this.notifyRemoveSuccess();
        },
        (err) =>
          this.notificationService.notifyError(
            this.deleteCustomerError,
            err.status
          )
      );
  }

  private notifyRemoveSuccess(): void {
    this.notificationService.notify(
      "customer.list.notification.removed",
      NotificationType.SUCCESS
    );
  }

  private subscribeForSliderClose(): void {
    this.sliderService.sliderStateChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((stateChange: SliderStateChange) => {
        if (stateChange.opened === false) {
          this.customerId = null;
          this.basicInfoSelectedCustomerId = null;
        }
      });
  }

  private subscribeForPersonList(): void {
    this.personService
      .getAccountEmails()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((response: AccountEmail[]) => {
        const accountId = +localStorage.getItem("loggedAccountId");
        const me = this.translateService.instant("customer.list.i");
        response.forEach((accountEmail) => {
          const toAdd =
            accountEmail.id === accountId
              ? me
              : accountEmail.name + " " + accountEmail.surname;
          this.userMap.set(accountEmail.id, toAdd);
        });
      });
  }

  private handleCustomersSuccess(page: any): void {
    this.totalElements = page.totalElements;
    this.customersPage = page;
  }

  isAnyCustomerChecked(): boolean {
    return (
      this.customersPage &&
      this.customersPage.content &&
      this.customersPage.content.some((customer) => customer.checked)
    );
  }

  randomBool() {
    return Math.random() > 0.6;
  }

  public setSelectedSearch(search: string): void {
    this.search = search;
    this.getCustomers(this.selectedFilter);
  }
}
