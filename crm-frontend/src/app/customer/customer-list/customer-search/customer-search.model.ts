export enum SearchType {
  COMPANY_NAME = "COMPANY_NAME",
  COMPANY_CITY = "COMPANY_CITY",
  PHONE = "PHONE",
}

export interface SearchVariantGroups {
  type: SearchType;
  variants: string[];
}
