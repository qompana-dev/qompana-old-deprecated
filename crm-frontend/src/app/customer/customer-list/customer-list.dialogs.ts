import { Customer } from "./../customer.model";
import {
  Colors,
  ConfirmDialogData,
  ContentTypes,
} from "./../../shared/dialog/confirm-dialog/confirm-dialog.model";

export class CustomerDialogsConfig {
  private readonly translatePrefix = "customer.list.dialog";

  constructor(private translate) {
    this.translate = translate;
  }

  private getKey(k) {
    return `${this.translatePrefix}.${k}`;
  }

  private t(k) {
    return this.translate.instant(this.getKey(k));
  }

  public deleteCustomer(
    customer: Customer,
    func: (id) => void
  ): ConfirmDialogData {
    const { id, name } = customer;

    return {
      title: this.getKey("delete.title"),
      contentType: ContentTypes.HTML,
      content: {
        icon: "warning",
        iconColor: Colors.ALERT,
        htmlTemplate: `
          ${this.t("delete.singleText")} <b>${name}</b >?<br/>
          ${this.t("delete.description")}
        `,
      },
      actions: [
        {
          text: this.getKey("delete.confirm"),
          func: () => func(id),
        },
        {
          text: this.getKey("delete.cancel"),
          color: Colors.ALERT,
        },
      ],
    };
  }

  public deleteCustomers(func: () => void): ConfirmDialogData {
    return {
      title: this.getKey("delete.title"),
      contentType: ContentTypes.HTML,
      content: {
        icon: "warning",
        iconColor: Colors.ALERT,
        htmlTemplate: `
          ${this.t("delete.multiText")}<br/>
          ${this.t("delete.description")}
        `,
      },
      actions: [
        {
          text: this.getKey("delete.confirm"),
          func,
        },
        {
          text: this.getKey("delete.cancel"),
          color: Colors.ALERT,
        },
      ],
    };
  }

  public archiveCustomer(
    customer: Customer,
    func: (finishBody) => void
  ): ConfirmDialogData {
    const { id, name } = customer;

    return {
      title: this.getKey("archive.title"),
      contentType: ContentTypes.HTML,
      content: {
        icon: "warning",
        iconColor: Colors.ALERT,
        htmlTemplate: `
          ${this.t("archive.singleText")} <b>${name}</b >?
        `,
      },
      actions: [
        {
          text: this.getKey("archive.confirm"),
          func: () => func(id),
        },
        {
          text: this.getKey("archive.cancel"),
          color: Colors.ALERT,
        },
      ],
    };
  }

  public archiveCustomers(func: () => void): ConfirmDialogData {
    return {
      title: this.getKey("archive.title"),
      contentType: ContentTypes.HTML,
      content: {
        icon: "warning",
        iconColor: Colors.ALERT,
        htmlTemplate: `
          ${this.t("archive.multiText")}
        `,
      },
      actions: [
        {
          text: this.getKey("archive.confirm"),
          func,
        },
        {
          text: this.getKey("archive.cancel"),
          color: Colors.ALERT,
        },
      ],
    };
  }
}
