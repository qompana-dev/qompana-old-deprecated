"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CustomerListComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var material_1 = require("@angular/material");
var operators_1 = require("rxjs/operators");
var notification_service_1 = require("../../shared/notification.service");
var http_status_codes_1 = require("http-status-codes");
var infinite_scroll_util_1 = require("../../shared/pagination/infinite-scroll.util");
var model_1 = require("../../shared/model");
var CustomerListComponent = /** @class */ (function () {
    function CustomerListComponent($router, authService, personService, customerService, translateService, deleteDialogService, notificationService, sliderService) {
        this.$router = $router;
        this.authService = authService;
        this.personService = personService;
        this.customerService = customerService;
        this.translateService = translateService;
        this.deleteDialogService = deleteDialogService;
        this.notificationService = notificationService;
        this.sliderService = sliderService;
        this.collapsed = true;
        this.userMap = new Map();
        this.focusOwnerForm = false;
        this.filterSelection = model_1.FilterStrategy;
        this.selectedFilter = model_1.FilterStrategy.ALL;
        this.columns = ['selectRow', 'name', 'firstToContactName', 'firstToContactPhone',
            'firstToContactEmail', 'type', 'owner', 'action'];
        this.componentDestroyed = new rxjs_1.Subject();
        this.currentSort = 'id,asc';
        this.deleteCustomerError = {
            base: 'customer.list.notification.',
            defaultText: 'undefinedError',
            errors: [
                {
                    code: http_status_codes_1.NOT_FOUND,
                    text: 'customerNotFound'
                },
                {
                    code: http_status_codes_1.BAD_REQUEST,
                    text: 'usedInOpportunity'
                }
            ]
        };
        this.deleteDialogData = {
            title: 'customer.list.dialog.remove.title',
            description: 'customer.list.dialog.remove.description',
            noteFirst: 'customer.list.dialog.remove.noteFirstPart',
            noteSecond: 'customer.list.dialog.remove.noteSecondPart',
            confirmButton: 'customer.list.dialog.remove.buttonYes',
            cancelButton: 'customer.list.dialog.remove.buttonNo'
        };
    }
    CustomerListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initCustomers(model_1.FilterStrategy.ALL);
        this.subscribeForSliderClose();
        this.subscribeForPersonList();
        this.filteredColumns$ = this.authService.onAuthChange.pipe(operators_1.startWith(this.getFilteredColumns()), operators_1.map(function () { return _this.getFilteredColumns(); }));
        this.sliderService.sliderStateChanged
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (state) {
            if (!state.opened) {
                _this.focusOwnerForm = false;
            }
        });
    };
    CustomerListComponent.prototype.ngOnDestroy = function () {
        this.componentDestroyed.next();
        this.componentDestroyed.complete();
    };
    CustomerListComponent.prototype.tryDeleteSelectedCustomers = function () {
        var _this = this;
        this.deleteDialogData.numberToDelete = this.customersPage.content
            .filter(function (customer) { return !!customer.checked; }).length;
        this.deleteDialogData.onConfirmClick = function () { return _this.deleteSelectedCustomers(); };
        this.deleteDialogService.open(this.deleteDialogData);
    };
    CustomerListComponent.prototype.deleteSelectedCustomers = function () {
        var _this = this;
        if (this.customersPage && this.customersPage.content) {
            var toDelete = this.customersPage.content
                .filter(function (customer) { return !!customer.checked; });
            this.customerService.removeCustomers(toDelete)
                .pipe(operators_1.takeUntil(this.componentDestroyed))
                .subscribe(function () {
                _this.initCustomers(_this.selectedFilter);
                _this.notifyRemoveSuccess();
            }, function (err) { return _this.notificationService.notifyError(_this.deleteCustomerError, err.status); });
        }
    };
    CustomerListComponent.prototype.transferSelectedCustomers = function () {
        this.selectedCustomers = this.customersPage.content.filter(function (customer) { return customer.checked; });
        this.transferSlider.toggle();
    };
    CustomerListComponent.prototype.tryDeleteCustomer = function (customerId) {
        var _this = this;
        this.customerId = customerId;
        this.deleteDialogData.numberToDelete = 1;
        this.deleteDialogData.onConfirmClick = function () { return _this.deleteCustomer(); };
        this.deleteDialogService.open(this.deleteDialogData);
    };
    CustomerListComponent.prototype.transferCustomer = function (customerId) {
        this.customerId = customerId;
        this.editSlider.toggle();
        this.focusOwnerForm = true;
    };
    CustomerListComponent.prototype.editCustomer = function (customerId) {
        this.customerId = customerId;
        this.editSlider.toggle();
    };
    CustomerListComponent.prototype.openBasicInfo = function (customerId) {
        this.basicInfoSelectedCustomerId = customerId;
        this.basicInfoSlider.toggle();
    };
    CustomerListComponent.prototype.onScroll = function (e) {
        if (infinite_scroll_util_1.InfiniteScrollUtil.shouldGetMoreData(e) && this.customersPage.numberOfElements < this.customersPage.totalElements) {
            var nextPage = this.currentPage + 1;
            this.getMoreCustomers({ page: '' + nextPage, size: infinite_scroll_util_1.InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: this.currentSort });
        }
    };
    CustomerListComponent.prototype.onSortChange = function ($event) {
        this.currentSort = $event.direction ? ($event.active + ',' + $event.direction) : 'id,asc';
        this.initCustomers(this.selectedFilter);
    };
    CustomerListComponent.prototype.initCustomers = function (selection, pageRequest) {
        var _this = this;
        if (pageRequest === void 0) { pageRequest = { page: '0', size: infinite_scroll_util_1.InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: this.currentSort }; }
        this.customerService.getCustomers(selection, pageRequest).pipe(operators_1.takeUntil(this.componentDestroyed)).subscribe(function (page) { return _this.handleInitSuccess(page); }, function () { return _this.notificationService.notify('customer.list.notification.listError', notification_service_1.NotificationType.ERROR); });
    };
    CustomerListComponent.prototype.goToDetails = function (id) {
        this.$router.navigate(['customers', id]);
    };
    CustomerListComponent.prototype.getFirstToContact = function (customer) {
        if (customer && customer.firstToContact && customer.firstToContact.length !== 0) {
            return customer.firstToContact[0];
        }
        return undefined;
    };
    CustomerListComponent.prototype.isSelected = function (selection) {
        return this.selectedFilter === selection;
    };
    CustomerListComponent.prototype.filter = function (selection) {
        this.selectedFilter = selection;
        this.initCustomers(selection);
    };
    CustomerListComponent.prototype.getFilteredColumns = function () {
        var _this = this;
        return this.columns
            .filter(function (item) { return !_this.authService.isPermissionPresent('CustomerListComponent.' + item + 'Column', 'i')
            || item === 'action' || item === 'selectRow'; });
    };
    CustomerListComponent.prototype.getMoreCustomers = function (pageRequest) {
        var _this = this;
        this.customerService.getCustomers(this.selectedFilter, pageRequest)
            .pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.throttleTime(500)).subscribe(function (page) { return _this.handleGetMoreCustomersSuccess(page); }, function () { return _this.notificationService.notify('customer.list.notification.listError', notification_service_1.NotificationType.ERROR); });
    };
    CustomerListComponent.prototype.handleGetMoreCustomersSuccess = function (page) {
        this.currentPage++;
        this.customersPage.content = this.customersPage.content.concat(page.content);
        this.customersPage.numberOfElements += page.content.length;
    };
    CustomerListComponent.prototype.deleteCustomer = function () {
        var _this = this;
        this.customerService.removeCustomer(this.customerId)
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function () {
            _this.initCustomers(_this.selectedFilter);
            _this.notifyRemoveSuccess();
        }, function (err) { return _this.notificationService.notifyError(_this.deleteCustomerError, err.status); });
    };
    CustomerListComponent.prototype.notifyRemoveSuccess = function () {
        this.notificationService.notify('customer.list.notification.removed', notification_service_1.NotificationType.SUCCESS);
    };
    CustomerListComponent.prototype.subscribeForSliderClose = function () {
        var _this = this;
        this.sliderService.sliderStateChanged
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (stateChange) {
            if (stateChange.opened === false) {
                _this.customerId = null;
                _this.basicInfoSelectedCustomerId = null;
            }
        });
    };
    CustomerListComponent.prototype.subscribeForPersonList = function () {
        var _this = this;
        this.personService.getAccountEmails()
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (response) {
            var accountId = +localStorage.getItem('loggedAccountId');
            var me = _this.translateService.instant('customer.list.i');
            response.forEach(function (accountEmail) {
                var toAdd = (accountEmail.id === accountId) ? me : accountEmail.name + ' ' + accountEmail.surname;
                _this.userMap.set(accountEmail.id, toAdd);
            });
        });
    };
    CustomerListComponent.prototype.handleInitSuccess = function (page) {
        this.currentPage = 0;
        this.customersPage = page;
    };
    CustomerListComponent.prototype.isAnyCustomerChecked = function () {
        return this.customersPage && this.customersPage.content && this.customersPage.content.some(function (customer) { return customer.checked; });
    };
    __decorate([
        core_1.ViewChild(material_1.MatTable)
    ], CustomerListComponent.prototype, "table");
    __decorate([
        core_1.ViewChild('editSlider')
    ], CustomerListComponent.prototype, "editSlider");
    __decorate([
        core_1.ViewChild('basicInfoSlider')
    ], CustomerListComponent.prototype, "basicInfoSlider");
    __decorate([
        core_1.ViewChild('transferSlider')
    ], CustomerListComponent.prototype, "transferSlider");
    CustomerListComponent = __decorate([
        core_1.Component({
            selector: 'app-customer-list',
            templateUrl: './customer-list.component.html',
            styleUrls: ['./customer-list.component.scss']
        })
    ], CustomerListComponent);
    return CustomerListComponent;
}());
exports.CustomerListComponent = CustomerListComponent;
