import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {SliderService} from '../../shared/slider/slider.service';
import {filter, takeUntil} from 'rxjs/operators';
import {CustomerWithContactsDto} from '../customer.model';
import {CustomerService} from '../customer.service';
import {Subject} from 'rxjs';
import {ErrorTypes, NotificationService} from '../../shared/notification.service';
import {NOT_FOUND} from 'http-status-codes';
import {Router} from '@angular/router';
import {CrmObjectType} from '../../shared/model/search.model';

@Component({
  selector: 'app-customer-basic-info',
  templateUrl: './customer-basic-info.component.html',
  styleUrls: ['./customer-basic-info.component.scss']
})
export class CustomerBasicInfoComponent implements OnInit, OnDestroy {

  constructor(public sliderService: SliderService,
              public customerService: CustomerService,
              private notificationService: NotificationService,
              public router: Router
  ) {
  }


  @Input() set customerId(customerId: number) {
    this._customerId = customerId;
    if (customerId) {
      this.loadCustomer(customerId);
    }
  }

  objectType = CrmObjectType;
  _customerId: number;
  customer: CustomerWithContactsDto;
  mainEmail: string;
  private componentDestroyed: Subject<void> = new Subject();

  readonly getCustomerErrors: ErrorTypes = {
    base: 'customer.basicInfo.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'notFound'
      }
    ]
  };

  ngOnInit(): void {
    this.handleMoreInfoClick();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
  }

  goToDetails(id: number): void {
    if (this._customerId) {
      this.router.navigate(['customers', this._customerId]);
    }
  }

  private loadCustomer(customerId: number): void {
    this.customerService.getCustomerWithContacts(customerId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      (customer: CustomerWithContactsDto) => {
        this.customer = customer;
        this.mainEmail = this.getMainEmail(customer);
      },
      (err) => this.notificationService.notifyError(this.getCustomerErrors, err.status)
    );
  }

  private handleMoreInfoClick(): void {
    this.sliderService.moreInfoClicked
      .pipe(
        takeUntil(this.componentDestroyed),
        filter(key => key === 'CustomerList.BasicInfoSlider')
      ).subscribe((key: string) => this.goToDetails(this.customerId));
  }

  private getMainEmail(customer: CustomerWithContactsDto): string {
    if (customer && customer.contacts) {
      const mainContact = customer.contacts.filter(value => value.mainContact).pop();
      return mainContact && mainContact.email;
    } else {
      return '';
    }
  }
}
