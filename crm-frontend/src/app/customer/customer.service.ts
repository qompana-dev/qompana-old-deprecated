import {Injectable} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {
  ContactDto,
  Customer,
  CustomerOpportunityDto,
  CustomerOpportunityPreviewDto,
  CustomerOption,
  CustomerType,
  CustomerWithContactsDto,
  Whitelist
} from './customer.model';
import {Page, PageRequest} from '../shared/pagination/page.model';
import {FilterStrategy} from '../shared/model';
import {TableUtil} from '../shared/table.util';
import {CrmObject} from '../shared/model/search.model';
import {getHeaderForAuthType} from '../shared/permissions-utils';
import {AddCustomerContactRelation, ContactCustomerRelation} from '../contacts/contacts.model';
import {UrlUtil} from '../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) {
  }

  customersChanged = new Subject<void>();
  customersChanged$ = this.customersChanged.asObservable();

  updateCustomers(): void {
    this.customersChanged.next();
  }

  getCustomers(filterStrategy: FilterStrategy, pageRequest: PageRequest, selectedSearch?: string): Observable<Page<Customer>> {
    let search = '';
    if (selectedSearch !== null) {
      search = selectedSearch;
    }
    let params = new HttpParams({fromObject: {...pageRequest, search}});
    params = TableUtil.enrichParamsWithFilterStrategy(filterStrategy, params);
    return this.http.get<Page<Customer>>(`${UrlUtil.url}/crm-business-service/customer`, {params});
  }

  getCustomerForEditById(customerId: number, authKey?: string): Observable<CustomerWithContactsDto> {
    const url = `${UrlUtil.url}/crm-business-service/customer/for-edit/${customerId}`;
    return this.http.get<CustomerWithContactsDto>(url, {headers: getHeaderForAuthType(authKey)});
  }

  getCustomerById(customerId: number): Observable<Customer> {
    return this.http.get<Customer>(`${UrlUtil.url}/crm-business-service/customer/${customerId}`);
  }

  removeCustomer(customerId: number): Observable<any> {
    return this.http.delete(`${UrlUtil.url}/crm-business-service/customer/${customerId}`);
  }

  removeCustomers(customers: Customer[]): Observable<any> {
    const params = new HttpParams().set('ids', customers.map(customer => customer.id).join(','));
    return this.http.delete<void>(`${UrlUtil.url}/crm-business-service/customer`, {params});
  }

  archiveCustomer(customerId: number): Observable<any> {
    return this.http.put(`${UrlUtil.url}/crm-business-service/customer/${customerId}/archive`, {});
  }

  archiveCustomers(customers: Customer[]): Observable<any> {
    const params = new HttpParams().set('ids', customers.map(customer => customer.id).join(','));
    return this.http.put<void>(`${UrlUtil.url}/crm-business-service/customer/archive`, {}, {params});
  }

  addCustomer(customer: CustomerWithContactsDto): Observable<CustomerWithContactsDto> {
    return this.http.post<CustomerWithContactsDto>(`${UrlUtil.url}/crm-business-service/customer`, customer);
  }

  modifyCustomer(customer: CustomerWithContactsDto, authType?: string): Observable<any> {
    return this.http.put(`${UrlUtil.url}/crm-business-service/customer/${customer.id}`, customer, {headers: getHeaderForAuthType(authType)});
  }

  modifyCustomers(customers: CustomerWithContactsDto[], authType?: string): Observable<any> {
    return this.http.put(`${UrlUtil.url}/crm-business-service/customer`, customers, {headers: getHeaderForAuthType(authType)});
  }

  getCustomerWithContacts(customerId: number): Observable<CustomerWithContactsDto> {
    return this.http.get<CustomerWithContactsDto>(`${UrlUtil.url}/crm-business-service/customer/with-contacts/${customerId}`);
  }

  getCustomerTypes(): Observable<CustomerType[]> {
    return this.http.get<CustomerType[]>(`${UrlUtil.url}/crm-business-service/type`);
  }

  refreshWhiteListStatus(nip: string): Observable<void> {
    const url = `${UrlUtil.url}/crm-business-service/whitelist/nip/${nip}/refresh`;
    return this.http.post<void>(url, undefined);
  }

  getWhiteListStatus(nip: string): Observable<Whitelist> {
    const url = `${UrlUtil.url}/crm-business-service/whitelist/nip/${nip}`;
    return this.http.get<Whitelist>(url);
  }

  getDataFromGUS(nip: string, customerId: number): Observable<Customer> {
    let params = new HttpParams().set('nip', nip);
    if (customerId) {
      params = params.set('customerId', customerId.toString());
    }
    return this.http.get<Customer>(`${UrlUtil.url}/crm-business-service/gus`, {params});
  }

  getCustomersByName(input: string): Observable<CustomerOption[]> {
    return this.http.get<Customer[]>(`${UrlUtil.url}/crm-business-service/customer/names?pattern=${input}`);
  }

  getCustomersForSearch(input: string): Observable<CrmObject[]> {
    return this.http.get<CrmObject[]>(`${UrlUtil.url}/crm-business-service/customers/search?term=${input}`);
  }

  getCustomerContacts(customerId: number): Observable<ContactDto[]> {
    return this.http.get<ContactDto[]>(`${UrlUtil.url}/crm-business-service/customer/${customerId}/contacts`);
  }

  getCustomerByContactId(contactId: number): Observable<CrmObject> {
    return this.http.get<CrmObject>(`${UrlUtil.url}/crm-business-service/customers/crm-object/by-contact-id/${contactId}`);
  }

  saveContactsNotes(customerId: number, notes: string): Observable<void> {
    return this.http.put<void>(`${UrlUtil.url}/crm-business-service/customer/${customerId}/contact-note`, notes);
  }

  markCustomerContactAsMain(customerId: number, contactId: number): Observable<void> {
    return this.http.post<void>(`${UrlUtil.url}/crm-business-service/customer/${customerId}/contact/${contactId}/mark-as-main`, null);
  }

  getCustomersByIds(customerIds: number[]): Observable<CrmObject[]> {
    if (!customerIds || customerIds.length === 0) {
      return of([]);
    }
    const params = new HttpParams().set('ids', customerIds.join(','));
    return this.http.get<CrmObject[]>(`${UrlUtil.url}/crm-business-service/customers/crm-object`, {params});
  }

  getContactsRelatedWithCustomer(customerId: number): Observable<ContactCustomerRelation[]> {
    const url = `${UrlUtil.url}/crm-business-service/customers/${customerId}/contact-links`;
    return this.http.get<ContactCustomerRelation[]>(url);
  }

  getCustomerName(customerId: number): Observable<string> {
    return this.http.get(`${UrlUtil.url}/crm-business-service/customers/${customerId}/name`, {responseType: 'text'});
  }

  getCustomerByNip(nip: string, customerId?: number): Observable<string> {
    let params = new HttpParams().set('nip', nip);
    if (customerId) {
      params = params.set('customerId', customerId.toString());
    }
    return this.http.get(`${UrlUtil.url}/crm-business-service/customer/check-nip`, {responseType: 'text', params});
  }

  addDirectContactToCustomer(relation: AddCustomerContactRelation): Observable<ContactCustomerRelation> {
    const url = `${UrlUtil.url}/crm-business-service/customers/${relation.customerId}/contacts`;
    return this.http.post<ContactCustomerRelation>(url, relation);
  }

  getCustomerOpportunitiesPreview(customerId: number): Observable<CustomerOpportunityPreviewDto> {
    return this.http.get<CustomerOpportunityPreviewDto>(`${UrlUtil.url}/crm-business-service/customer/${customerId}/opportunities-preview`);
  }

  getCustomerOpportunities(customerId: number): Observable<CustomerOpportunityDto[]> {
    return this.http.get<CustomerOpportunityDto[]>(`${UrlUtil.url}/crm-business-service/customer/${customerId}/opportunities`);
  }

}
