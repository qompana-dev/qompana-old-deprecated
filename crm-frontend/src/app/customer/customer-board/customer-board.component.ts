import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {map, takeUntil} from 'rxjs/operators';
import {CustomerWithContactsDto} from '../customer.model';
import {CustomerService} from '../customer.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {MatTab, MatTabChangeEvent} from '@angular/material';
import {PersonService} from '../../person/person.service';
import {Account} from '../../person/person.model';
import {DeleteDialogService} from '../../shared/dialog/delete/delete-dialog.service';
import {DeleteDialogData} from '../../shared/dialog/delete/delete-dialog.component';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {BAD_REQUEST, NOT_FOUND} from 'http-status-codes';
import {FormSaveService} from '../../shared/form/form-save.service';
import {CrmObject, CrmObjectType} from '../../shared/model/search.model';
import {Note} from '../../widgets/notes/notes.model';
import {NoteListComponent} from '../../shared/tabs/notes-tab/note-list/note-list.component';

@Component({
  selector: 'app-customer-board',
  templateUrl: './customer-board.component.html',
  styleUrls: ['./customer-board.component.scss'],
  providers: [FormSaveService]
})
export class CustomerBoardComponent implements OnInit, OnDestroy {
  customerId: number;
  customer: CustomerWithContactsDto;
  customerOwner: Account;
  private componentDestroyed: Subject<void> = new Subject();
  widgetsVisible = true;
  selectedTabIndex: number;
  userMap: Map<number, string> = new Map();

  selectedNote: Note;
  @ViewChild(NoteListComponent) noteListComponent: NoteListComponent;
  @ViewChild('notesTab') notesTab: MatTab;


  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'customer.dashboard.removeCustomer.title',
    description: 'customer.dashboard.removeCustomer.description',
    confirmButton: 'customer.dashboard.removeCustomer.buttonYes',
    cancelButton: 'customer.dashboard.removeCustomer.buttonNo',
  };

  readonly deleteCustomerError: ErrorTypes = {
    base: 'customer.dashboard.removeCustomer.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'notFound'
      }, {
        code: BAD_REQUEST,
        text: 'usedInOpportunity'
      }
    ]
  };

  readonly getCustomerErrors: ErrorTypes = {
    base: 'customer.dashboard.contacts.notification.',
    defaultText: 'unknownError',
    errors: [{
      code: NOT_FOUND,
      text: 'customerNotFoundError'
    }]
  };

  readonly getOwnerErrors: ErrorTypes = {
    base: 'customer.dashboard.contacts.notification.',
    defaultText: 'unknownError',
    errors: [{
      code: NOT_FOUND,
      text: 'getOwnerNotFoundError'
    }]
  };
  crmObject: CrmObject;

  constructor(
    public customerService: CustomerService,
    public personService: PersonService,
    private deleteDialogService: DeleteDialogService,
    private notificationService: NotificationService,
    public route: ActivatedRoute,
    public router: Router,
    public formSaveService: FormSaveService) {
  }

  ngOnInit(): void {
    this.getAccountEmails();
    this.route.params.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(params => {
        this.customerId = +params['id'];
        this.crmObject = {id: this.customerId, type: CrmObjectType.customer};
        this.getCustomer(this.customerId);
      }
    );
  }

  public getCustomer(customerId: number): void {
    this.customerService.getCustomerWithContacts(customerId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      (customer: CustomerWithContactsDto) => {
        if (customer.owner !== (this.customerOwner && this.customerOwner.id)) {
          this.getOwner(customer.owner);
        }
        this.customer = customer;
      },
      (err) => this.notificationService.notifyError(this.getCustomerErrors, err.status)
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  public tryDeleteCustomer(): void {
    this.deleteDialogData.onConfirmClick = () => this.deleteCustomer(this.customer.id);
    this.deleteDialogService.open(this.deleteDialogData);
  }

  onTabChange($event: MatTabChangeEvent): void {
    this.widgetsVisible = [0, 3].includes($event.index);
  }

  private getOwner(ownerId: number): void {
    this.personService.getAccount(ownerId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      (account: Account) => this.customerOwner = account,
      (err) => this.notificationService.notifyError(this.getOwnerErrors, err.status)
    );
  }


  private deleteCustomer(customerId: number): void {
    this.customerService.removeCustomer(customerId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      () => {
        this.router.navigate(['customers']);
        this.notifyRemoveSuccess();
      },
      (err) => this.notificationService.notifyError(this.deleteCustomerError, err.status)
    );
  }

  private notifyRemoveSuccess(): void {
    this.notificationService.notify('customer.dashboard.removeCustomer.successfullyRemoved', NotificationType.SUCCESS);
  }

  cancelCardForm(): void {
    this.formSaveService.cancelForm();
  }

  saveCardForm(): void {
    this.formSaveService.saveForm();
  }

  private getAccountEmails(): void {
    this.personService.getAccountEmails()
      .pipe(
        takeUntil(this.componentDestroyed),
        map(emails => new Map<number, string>(emails.map(i => [i.id, `${i.name} ${i.surname}`] as [number, string]))),
      ).subscribe(
      users => this.userMap = users,
      (err) => this.notificationService.notify('customer.dashboard.contacts.notification.getAccountsError', NotificationType.ERROR)
    );
  }


  // notes

  refreshNotes(): void {
    this.noteListComponent.getNotes();
  }
  getNextNote(): void {
    this.noteListComponent.getNextNote(this.selectedNote);
  }
  getPreviousNote(): void {
    this.noteListComponent.getPreviousNote(this.selectedNote);
  }
  isNoteTabActive(): boolean {
    return this.notesTab && this.notesTab.isActive;
  }

}
