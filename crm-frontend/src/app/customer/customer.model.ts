import {Contact} from '../contacts/contacts.model';
import {AdditionalPhoneNumber} from '../shared/additional-phone-numbers/additional-phone-numbers.model';

export class CustomerType {
  id: number;
  name: string;
  description: string;
}

export class Address {
  id: number;
  country: string;
  city: string;
  province: string;
  buildNumber: string;
  flatNumber: string;
  zipCode: string;
  street: string;
  longitude: string | number;
  latitude: string | number;
}

export class Customer {
  id: number;
  nip: string;
  regon: string;
  name: string;
  description: string;
  avatar: string;
  type: CustomerType;
  phone: string;
  additionalPhones?: AdditionalPhoneNumber[];
  owner: number;
  address: Address;
  website: string;
  linkedIn: string;
  firstToContact: Contact[];
  created: string;
  contactsNotes: string;
  additionalData: string;

  checked?: boolean;
}

export class CustomerOption {
  id: number;
  name: string;
}

export class CustomerWithContactsDto {
  id: number;
  nip: string;
  regon: string;
  name: string;
  description: string;
  type: CustomerType;
  phone: string;
  additionalPhones?: AdditionalPhoneNumber[];
  owner: number;
  address: Address;
  website: string;
  linkedIn: string;
  contacts?: ContactDto[];
  contactsNotes: string;
  created: string;
  employeesNumber?: number;
  companyType?: string;
  income?: string;
  avatar?: string;
  additionalData?: string;
}

export class ContactDto {
  id: number;
  name: string;
  surname: string;
  email: string;
  phone: string;
  position?: string;
  uuid?: string;
  mainContact: boolean;
  owner: string;
  avatar?: string;

  autofocusNameOnStart?: boolean;
}

export interface Whitelist {
  id: number;
  status: 'Czynny' | 'Zwolniony' | 'Niezarejestrowany';
  registered: string;
  updated: string;
  refreshed: string;
  bankAccountNumbers: BankAccountNumbers[];
}

export interface BankAccountNumbers {
  id: number;
  number: string;
}

export interface ContactWithRelation extends Contact {
  startDate: string;
  endDate: string;
  note: string;
}

export interface CustomerOpportunityPreviewDto {
  totalNumber: number;
  opportunities: CustomerOpportunityDto[];
}

export interface CustomerOpportunityDto {
  id: number;
  name: string;
  status: number;
  maxStatus: number;
  amount: number;
  currency: string;
  finishDate: string;
}
