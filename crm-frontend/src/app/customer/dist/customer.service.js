"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CustomerService = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var http_1 = require("@angular/common/http");
var table_util_1 = require("../shared/table.util");
var permissions_utils_1 = require("../shared/permissions-utils");
var url_util_1 = require("../shared/url.util");
var CustomerService = /** @class */ (function () {
    function CustomerService(http) {
        this.http = http;
    }
    CustomerService.prototype.getCustomers = function (filterStrategy, pageRequest) {
        var params = new http_1.HttpParams({ fromObject: pageRequest });
        params = table_util_1.TableUtil.enrichParamsWithFilterStrategy(filterStrategy, params);
        return this.http.get(url_util_1.UrlUtil.url + "/crm-business-service/customer", { params: params });
    };
    CustomerService.prototype.getCustomerForEditById = function (customerId, authKey) {
        var url = url_util_1.UrlUtil.url + "/crm-business-service/customer/for-edit/" + customerId;
        return this.http.get(url, { headers: permissions_utils_1.getHeaderForAuthType(authKey) });
    };
    CustomerService.prototype.getCustomerById = function (customerId) {
        return this.http.get(url_util_1.UrlUtil.url + "/crm-business-service/customer/" + customerId);
    };
    CustomerService.prototype.removeCustomer = function (customerId) {
        return this.http["delete"](url_util_1.UrlUtil.url + "/crm-business-service/customer/" + customerId);
    };
    CustomerService.prototype.removeCustomers = function (customers) {
        var params = new http_1.HttpParams().set('ids', customers.map(function (customer) { return customer.id; }).join(','));
        return this.http["delete"](url_util_1.UrlUtil.url + "/crm-business-service/customer", { params: params });
    };
    CustomerService.prototype.addCustomer = function (customer) {
        return this.http.post(url_util_1.UrlUtil.url + "/crm-business-service/customer", customer);
    };
    CustomerService.prototype.modifyCustomer = function (customer, authType) {
        return this.http.put(url_util_1.UrlUtil.url + "/crm-business-service/customer/" + customer.id, customer, { headers: permissions_utils_1.getHeaderForAuthType(authType) });
    };
    CustomerService.prototype.modifyCustomers = function (customers, authType) {
        return this.http.put(url_util_1.UrlUtil.url + "/crm-business-service/customer", customers, { headers: permissions_utils_1.getHeaderForAuthType(authType) });
    };
    CustomerService.prototype.getCustomerWithContacts = function (customerId) {
        return this.http.get(url_util_1.UrlUtil.url + "/crm-business-service/customer/with-contacts/" + customerId);
    };
    CustomerService.prototype.getCustomerTypes = function () {
        return this.http.get(url_util_1.UrlUtil.url + "/crm-business-service/type");
    };
    CustomerService.prototype.refreshWhiteListStatus = function (nip) {
        var url = url_util_1.UrlUtil.url + "/crm-business-service/whitelist/nip/" + nip + "/refresh";
        return this.http.post(url, undefined);
    };
    CustomerService.prototype.getWhiteListStatus = function (nip) {
        var url = url_util_1.UrlUtil.url + "/crm-business-service/whitelist/nip/" + nip;
        return this.http.get(url);
    };
    CustomerService.prototype.getDataFromGUS = function (nip) {
        return this.http.get(url_util_1.UrlUtil.url + "/crm-business-service/gus?nip=" + nip);
    };
    CustomerService.prototype.getCustomersByName = function (input) {
        return this.http.get(url_util_1.UrlUtil.url + "/crm-business-service/customer/names?pattern=" + input);
    };
    CustomerService.prototype.getCustomersForSearch = function (input) {
        return this.http.get(url_util_1.UrlUtil.url + "/crm-business-service/customers/search?term=" + input);
    };
    CustomerService.prototype.getCustomerContacts = function (customerId) {
        return this.http.get(url_util_1.UrlUtil.url + "/crm-business-service/customer/" + customerId + "/contacts");
    };
    CustomerService.prototype.saveContactsNotes = function (customerId, notes) {
        return this.http.put(url_util_1.UrlUtil.url + "/crm-business-service/customer/" + customerId + "/contact-note", notes);
    };
    CustomerService.prototype.markCustomerContactAsMain = function (customerId, contactId) {
        return this.http.post(url_util_1.UrlUtil.url + "/crm-business-service/customer/" + customerId + "/contact/" + contactId + "/mark-as-main", null);
    };
    CustomerService.prototype.getCustomersByIds = function (customerIds) {
        if (!customerIds || customerIds.length === 0) {
            return rxjs_1.of([]);
        }
        var params = new http_1.HttpParams().set('ids', customerIds.join(','));
        return this.http.get(url_util_1.UrlUtil.url + "/crm-business-service/customers/crm-object", { params: params });
    };
    CustomerService.prototype.getContactsRelatedWithCustomer = function (customerId) {
        var url = url_util_1.UrlUtil.url + "/crm-business-service/customers/" + customerId + "/contact-links";
        return this.http.get(url);
    };
    CustomerService.prototype.getCustomerName = function (customerId) {
        return this.http.get(url_util_1.UrlUtil.url + "/crm-business-service/customers/" + customerId + "/name", { responseType: 'text' });
    };
    CustomerService.prototype.addDirectContactToCustomer = function (relation) {
        var url = url_util_1.UrlUtil.url + "/crm-business-service/customers/" + relation.customerId + "/contacts";
        return this.http.post(url, relation);
    };
    CustomerService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], CustomerService);
    return CustomerService;
}());
exports.CustomerService = CustomerService;
