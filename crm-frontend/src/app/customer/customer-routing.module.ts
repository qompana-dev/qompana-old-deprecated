import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../login/auth/auth-guard.service';
import {CustomerListComponent} from './customer-list/customer-list.component';

const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: CustomerListComponent},
  {path: ':id', canActivate: [AuthGuard], component: CustomerDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
