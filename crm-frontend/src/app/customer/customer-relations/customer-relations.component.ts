import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CrmObject} from '../../shared/model/search.model';

@Component({
  selector: 'app-customer-relations',
  templateUrl: './customer-relations.component.html',
  styleUrls: ['./customer-relations.component.scss']
})
export class CustomerRelationsComponent implements OnInit {

  @Input() crmObject: CrmObject;
  @Input() userMap: Map<number, string> = new Map();
  @Input() customerName: string;
  @Output() tabIndexChange: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  changeTab($event: MouseEvent, selectedTabIndex: number): void {
    if ($event) {
      $event.stopPropagation();
    }
    this.tabIndexChange.next(selectedTabIndex);
  }

}
