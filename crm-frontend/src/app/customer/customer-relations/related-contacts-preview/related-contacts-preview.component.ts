import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {ContactCustomerRelation} from '../../../contacts/contacts.model';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {ContactsService} from '../../../contacts/contacts.service';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from '../../../login/auth/auth.service';
import {Router} from '@angular/router';
import {map, startWith, takeUntil} from 'rxjs/operators';
import {CustomerService} from '../../customer.service';

@Component({
  selector: 'app-related-contacts-preview',
  templateUrl: './related-contacts-preview.component.html',
  styleUrls: ['./related-contacts-preview.component.scss']
})
export class RelatedContactsPreviewComponent implements OnDestroy {

  _customerId: number;
  expanded = false;
  columns = ['contact', 'relationType', 'position', 'action'];
  relatedCustomers: ContactCustomerRelation[];
  private componentDestroyed: Subject<void> = new Subject();

  @ViewChild('addDirectContactLinkSlider') addDirectContactLinkSlider: SliderComponent;
  @ViewChild('addIndirectContactLinkSlider') addIndirectContactLinkSlider: SliderComponent;

  @Input() set customerId(id: number) {
    this._customerId = id;
    if (id) {
      this.getRelatedContacts();
    }
  }

  @Output() changeTab: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();
  @ViewChild('addCustomerSlider') addCustomerSlider: SliderComponent;

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'customer.relatedContacts.dialog.delete.title',
    description: 'customer.relatedContacts.dialog.delete.description',
    confirmButton: 'customer.relatedContacts.dialog.delete.confirm',
    cancelButton: 'customer.relatedContacts.dialog.delete.cancel',
  };

  constructor(private contactService: ContactsService,
              private customerService: CustomerService,
              private deleteDialogService: DeleteDialogService,
              private notificationService: NotificationService,
              private translateService: TranslateService,
              private authService: AuthService,
              private router: Router) {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  public getRelatedContacts(): void {
    this.customerService.getContactsRelatedWithCustomer(this._customerId)
      .pipe(
        takeUntil(this.componentDestroyed))
      .subscribe(
        customers => {
          this.relatedCustomers = customers;
        },
        err => this.notificationService
          .notify('contacts.details.newRelatedCustomer.notification.getCustomerNameError', NotificationType.ERROR)
      );
  }

  tryDelete(linkId: number): void {
    this.deleteDialogData.onConfirmClick = () => this.deleteRelation(linkId);
    this.openDeleteDialog();
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  deleteRelation(linkId: number): void {
    this.contactService.deleteRelation(linkId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      () => {
        this.notifyDeleteRelationSuccess();
        this.getRelatedContacts();
      },
      () => this.deleteRelationError()
    );
  }

  private notifyDeleteRelationSuccess(): void {
    this.notificationService.notify('customer.relatedContacts.notification.deleteRelationSuccess', NotificationType.SUCCESS);
  }

  private deleteRelationError(): void {
    this.notificationService.notify('customer.relatedContacts.notification.deleteRelationError', NotificationType.ERROR);
  }

  private getRelationTypeTranslation(relationType: string): string {
    return relationType && this.translateService.instant(`contacts.details.relatedCustomers.relationTypes.${relationType}`);
  }

  showDetails(contact: number): void {
    this.router.navigate(['contacts', contact]);
  }

  addDirectContact(): void {
    this.addDirectContactLinkSlider.open();
  }

  addIndirectContact(): void {
    this.addIndirectContactLinkSlider.open();
  }

}
