import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {map, startWith, takeUntil} from 'rxjs/operators';
import {OpportunityService} from '../../../opportunity/opportunity.service';
import {Observable, Subject} from 'rxjs';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import {OpportunitySave} from '../../../opportunity/opportunity.model';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {CrmObject} from '../../../shared/model/search.model';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import {CustomerService} from '../../customer.service';
import {CustomerOpportunityDto, CustomerOpportunityPreviewDto} from '../../customer.model';
import {Router} from '@angular/router';
import {LoginService} from '../../../login/login.service';
import {AuthService} from '../../../login/auth/auth.service';

@Component({
  selector: 'app-related-opportunity-preview',
  templateUrl: './related-opportunity-preview.component.html',
  styleUrls: ['./related-opportunity-preview.component.scss']
})
export class RelatedOpportunityPreviewComponent implements OnInit {

  @Input() set crmObject(crmObject: CrmObject) {
    const contactId = crmObject && crmObject.id;
    this._crmObject = crmObject;
    if (contactId) {
      this.subscribeForContactOpportunities(contactId);
    }
  }

  constructor(private opportunityService: OpportunityService,
              private notificationService: NotificationService,
              private deleteDialogService: DeleteDialogService,
              private customerService: CustomerService,
              private $router: Router,
              private loginService: LoginService,
              private authService: AuthService) {
  }

  @ViewChild('addSlider') addSlider: SliderComponent;
  @ViewChild('editSlider') editSlider: SliderComponent;
  @Output() changeTab: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();

  customerOpportunityPreviewDto: CustomerOpportunityPreviewDto;
  opportunityWithContact: Partial<OpportunitySave>;
  selectedOpportunity: OpportunitySave;
  columns = ['name', 'status', 'amount', 'finishDate', 'action'];
  filteredColumns$: Observable<string[]>;

  private readonly opportunityErrorTypes: ErrorTypes = {
    base: 'opportunity.list.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'opportunityNotFound'
      }, {
        code: FORBIDDEN,
        text: 'noPermission'
      }
    ]
  };

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'opportunity.form.dialog.delete.title',
    description: 'opportunity.form.dialog.delete.description',
    noteFirst: 'opportunity.form.dialog.delete.noteFirstPart',
    noteSecond: 'opportunity.form.dialog.delete.noteSecondPart',
    confirmButton: 'opportunity.form.dialog.delete.confirm',
    cancelButton: 'opportunity.form.dialog.delete.cancel',
  };

  // tslint:disable-next-line:variable-name
  _crmObject: CrmObject;
  @Input() userMap: Map<number, string> = new Map();
  @Input() contactName: string;

  private componentDestroyed: Subject<void> = new Subject();

  ngOnInit(): void {
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
    this.customerService.customersChanged$
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => this.subscribeForContactOpportunities(this._crmObject.id)
      );
  }

  edit(id: number): void {
    this.opportunityService.getOpportunityForEditForSlider(id).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      opportunity => {
        this.selectedOpportunity = opportunity;
        this.editSlider.open();
      },
      err => this.notificationService.notifyError(this.opportunityErrorTypes, err.status)
    );
  }

  tryDeleteOpportunity(opportunityId: number): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteOpportunity(opportunityId);
    this.openDeleteDialog();
  }

  private subscribeForContactOpportunities(contactId: number): void {
    this.customerService.getCustomerOpportunitiesPreview(contactId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (customerOpportunityPreviewDto: CustomerOpportunityPreviewDto) =>
        this.customerOpportunityPreviewDto = customerOpportunityPreviewDto,
      // tslint:disable-next-line:max-line-length
      () => this.notificationService.notify('customer.dashboard.tabs.salesOpportunities.notifications.getOpportunitiesError', NotificationType.ERROR)
    );
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private deleteOpportunity(opportunityId: number): void {
    this.opportunityService.removeOpportunity(opportunityId).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      () => {
        this.notificationService.notify('opportunity.list.notification.deleteSuccess', NotificationType.SUCCESS);
        this.customerService.updateCustomers();
      },
      err => this.notificationService.notifyError(this.opportunityErrorTypes, err.status)
    );
  }

  goToOpportunityDetails(id: number): void {
    this.$router.navigate(['/opportunity', id]);
  }

  getOpportunityProgress(opportunity: CustomerOpportunityDto): number {
    return opportunity.maxStatus ? (opportunity.status / opportunity.maxStatus) * 100 : 0;
  }

  addOpportunity($event: any): void {
    $event.stopPropagation();
    this.customerService.getCustomerById(this._crmObject.id).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (customer) => {
        this.opportunityWithContact = {
          customerId: customer.id, canEditAmount: true,
          ownerOneId: this.loginService.getLoggedAccountId(), ownerOnePercentage: 100
        };
        this.addSlider.open();
      }
    );
  }

  handleOpportunityAdded(): void {
    this.addSlider.close();
    this.editSlider.close();
    this.customerService.updateCustomers();
  }

  private getFilteredColumns(): string[] {
    return this.columns
      .filter((item) => !this.authService.isPermissionPresent('CustomerOpportunity.' + item, 'i') || item === 'action');
  }

}
