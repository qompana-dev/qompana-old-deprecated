import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { CustomerFormComponent } from '../customer-form/customer-form.component';
import { BAD_REQUEST, CONFLICT, NOT_FOUND } from 'http-status-codes';
import { takeUntil } from 'rxjs/operators';
import { CustomerMiniFormComponent } from '../customer-form/customer-mini-form/customer-mini-form.component';
import * as cloneDeep from 'lodash/cloneDeep';
import { CustomerService } from '../../customer.service';
import { SliderService, SliderWidth } from '../../../shared/slider/slider.service';
import { ErrorTypes, NotificationService, NotificationType } from '../../../shared/notification.service';
import { CustomerWithContactsDto } from '../../customer.model';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.scss']
})
export class CustomerEditComponent implements OnInit, OnDestroy {

  private componentDestroyed: Subject<void> = new Subject();
  private currentWidth: string;
  customer: CustomerWithContactsDto;
  @Output() customerAdded: EventEmitter<void> = new EventEmitter();
  miniForm = true;
  addContactAndScrollToNewForm = false;
  @Input() focusOwnerForm = false;

  @Input() set customerId(customerId: number) {
    if (customerId) {
      this.loadCustomer(customerId);
    }
  }

  @ViewChild(CustomerFormComponent) private customerFormComponent: CustomerFormComponent;
  @ViewChild(CustomerMiniFormComponent) private customerMiniFormComponent: CustomerMiniFormComponent;

  readonly errorTypesForModification: ErrorTypes = {
    base: 'customer.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'validationError'
      }, {
        code: NOT_FOUND,
        text: 'notFound'
      }, {
        code: CONFLICT,
        text: 'nameOrNipOccupied'
      }
    ]
  };

  readonly errorTypesForGet: ErrorTypes = {
    base: 'customer.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'notFound'
      }
    ]
  };

  constructor(private sliderService: SliderService,
    private customerService: CustomerService,
    private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.subscribeSliderInfo();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    const isValid = (this.miniForm) ? this.customerMiniFormComponent.isValid : this.customerFormComponent.isValid;
    if (isValid) {
      const customer = (this.miniForm) ? this.customerMiniFormComponent.getCustomer() : this.customerFormComponent.getCustomer();
      this.customerService.modifyCustomer(customer)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => this.handleSuccess(),
          (error) => this.notificationService.notifyError(this.errorTypesForModification, error.status)
        );
    }
  }

  addAdditionalField(): void {
    if (this.miniForm) {
      this.customerMiniFormComponent.addAdditionalField();
    } else {
      this.customerFormComponent.addAdditionalField();
    }
  }

  addContact(): void {
    this.openFullView();
    this.addContactAndScrollToNewForm = true;
  }

  private handleSuccess(): void {
    this.notificationService.notify('customer.form.notification.edited', NotificationType.SUCCESS);
    this.customerAdded.next();
    this.closeSlider();
  }

  private subscribeSliderInfo(): void {
    this.sliderService.sliderWidthChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((sliderWidth: SliderWidth) => {
        if (this.currentWidth !== sliderWidth.width) {
          this.currentWidth = sliderWidth.width;
          this.miniForm = this.currentWidth === '40vw';
        }
      });
    this.sliderService.moreInfoClicked
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((key: string) => {
        if (key === 'CustomerList.EditSlider') {
          this.openFullView();
        }
      });
  }

  private openFullView(): void {
    const curData = this.customerMiniFormComponent.getCustomer();
    Object.assign(this.customer, cloneDeep(curData));
    this.sliderService.changeMoreInfoVisible('CustomerList.EditSlider', false);
    this.sliderService.changeWidth('CustomerList.EditSlider', '100vw');
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
    this.miniForm = true;
    this.addContactAndScrollToNewForm = false;
  }

  private loadCustomer(customerId: number): void {
    this.customerService.getCustomerForEditById(customerId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe((customer: CustomerWithContactsDto) => {
        this.customer = customer;
      },
        (err) => this.notificationService.notifyError(this.errorTypesForGet, err.status));
  }
}
