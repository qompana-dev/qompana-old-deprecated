import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {CustomerFormComponent} from '../customer-form/customer-form.component';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {BAD_REQUEST, CONFLICT} from 'http-status-codes';
import {CustomerMiniFormComponent} from '../customer-form/customer-mini-form/customer-mini-form.component';
import * as cloneDeep from 'lodash/cloneDeep';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {CustomerService} from '../../customer.service';
import {SliderService, SliderWidth} from '../../../shared/slider/slider.service';
import {CustomerWithContactsDto} from '../../customer.model';
import {SliderComponent} from '../../../shared/slider/slider.component';

@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.scss']
})
export class CustomerAddComponent implements OnInit, OnDestroy {

  private currentWidth: string;
  miniForm = true;
  private componentDestroyed: Subject<void> = new Subject();
  @Output() customerAdded: EventEmitter<{id: number, label: string}> = new EventEmitter();
  @Input() slider: SliderComponent;

  @ViewChild(CustomerFormComponent) private customerFormComponent: CustomerFormComponent;
  @ViewChild(CustomerMiniFormComponent) private customerMiniFormComponent: CustomerMiniFormComponent;

  readonly errorTypes: ErrorTypes = {
    base: 'customer.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'validationError'
      }, {
        code: CONFLICT,
        text: 'nameOrNipOccupied'
      }
    ]
  };
  saveInProgress = false;

  customer: CustomerWithContactsDto;
  addContactAndScrollToNewForm = false;

  constructor(private sliderService: SliderService,
              private customerService: CustomerService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.subscribeSliderInfo();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  addAdditionalField(): void {
    if (this.miniForm) {
      this.customerMiniFormComponent.addAdditionalField();
    } else {
      this.customerFormComponent.addAdditionalField();
    }
  }

  submit(): void {
    const isValid = (this.miniForm) ? this.customerMiniFormComponent.isValid : this.customerFormComponent.isValid;
    if (isValid && !this.saveInProgress) {
      this.saveInProgress = true;
      const customer = (this.miniForm) ? this.customerMiniFormComponent.getCustomer() : this.customerFormComponent.getCustomer();
      this.customerService.addCustomer(customer)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (customerWithContactsDto: CustomerWithContactsDto) => this.handleSuccess(customerWithContactsDto),
          (error) => {
            this.saveInProgress = false;
            this.notificationService.notifyError(this.errorTypes, error.status);
          }
        );
    }
  }

  addContact(): void {
    this.openFullView();
    this.addContactAndScrollToNewForm = true;
  }

  private subscribeSliderInfo(): void {
    this.sliderService.sliderWidthChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((sliderWidth: SliderWidth) => {
        if (this.currentWidth !== sliderWidth.width) {
          this.currentWidth = sliderWidth.width;
          this.miniForm = this.currentWidth !== '100vw';
        }
      });
    this.sliderService.moreInfoClicked
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((key: string) => {
        if (key === this.slider.key) {
          this.openFullView();
        }
      });
    this.sliderService.closeClicked
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((key: string) => {
        if (key === this.slider.key) {
          this.customer = undefined;
          this.clearForms();
        }
      });
  }
  private openFullView(): void {
    this.customer = cloneDeep(this.customerMiniFormComponent.getCustomer());
    this.sliderService.changeMoreInfoVisible(this.slider.key, false);
    this.clearForms();
    this.sliderService.changeWidth(this.slider.key, '100vw');
  }

  private handleSuccess(customerWithContactsDto: CustomerWithContactsDto): void {
    this.saveInProgress = false;
    this.notificationService.notify('customer.form.notification.added', NotificationType.SUCCESS);
    this.customerAdded.next({id: customerWithContactsDto.id, label: customerWithContactsDto.name});
    this.closeSlider();
    this.clearForms();
  }

  closeSlider(): void {
    this.slider.close();
    this.miniForm = true;
    this.addContactAndScrollToNewForm = false;
    this.customer = undefined;
    this.clearForms();
  }

  clearForms(): void {
    if (this.customerFormComponent) {
      this.customerFormComponent.clear();
    }
    if (this.customerMiniFormComponent) {
      this.customerMiniFormComponent.clear();
    }
  }
}
