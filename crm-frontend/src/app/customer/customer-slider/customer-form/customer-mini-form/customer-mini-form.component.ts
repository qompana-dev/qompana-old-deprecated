import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, AfterViewChecked} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {BAD_REQUEST, GONE, NOT_FOUND} from 'http-status-codes';
import {CustomerFormUtil} from '../customer-form.util';
import {MatSelect} from '@angular/material';
import {CustomerService} from '../../../customer.service';
import {Customer, CustomerType, CustomerWithContactsDto} from '../../../customer.model';
import {PersonService} from '../../../../person/person.service';
import {AccountEmail} from '../../../../person/person.model';
import {MapViewMarker} from '../../../../map/map-view/map-view.model';
import {ErrorTypes, NotificationService, NotificationType} from '../../../../shared/notification.service';
import {MapViewComponent} from '../../../../map/map-view/map-view.component';
import {AuthService} from '../../../../login/auth/auth.service';
import {SliderService} from '../../../../shared/slider/slider.service';
import {applyPermission} from '../../../../shared/permissions-utils';
import {FormValidators} from '../../../../shared/form/form-validators';
import {PhoneService} from '../../../../shared/phone-fields/phone.service';

@Component({
  selector: 'app-customer-mini-form',
  templateUrl: './customer-mini-form.component.html',
  styleUrls: ['./customer-mini-form.component.scss']
})
export class CustomerMiniFormComponent implements OnInit, OnDestroy {
  @Input() update: boolean;
  @Output() addNewContact: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('mapView') mapViewComponent: MapViewComponent;
  @ViewChild('ownerSelect') ownerSelect: MatSelect;

  focusOwnerForm = false;
  @Input() customersToChangeOwnerOperation: boolean;

  markers: MapViewMarker[];
  mapCenter: number[];

  customerForm: FormGroup;
  accountEmails: AccountEmail[];

  updatedCustomer: CustomerWithContactsDto;
  private componentDestroyed: Subject<void> = new Subject();

  private addAdditionalFieldSubject: Subject<void> = new Subject<void>();
  public addAdditionalField$: Observable<void> = this.addAdditionalFieldSubject.asObservable();

  private addAdditionalPhoneNumbersSubject: Subject<void> = new Subject<void>();
  public addAdditionalPhoneNumbers$: Observable<void> = this.addAdditionalPhoneNumbersSubject.asObservable();

  types$: Observable<CustomerType[]>;

  readonly errorTypes: ErrorTypes = {
    base: 'customer.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'customerNotFoundInGus'
      },
      {
        code: BAD_REQUEST,
        text: 'badRequest'
      },
      {
        code: GONE,
        text: 'errorInGus'
      }
    ]
  };

  @Input() set focusOwner(focusOwnerForm: boolean) {
    this.focusOwnerForm = focusOwnerForm;
    setTimeout(() => {
      if (this.focusOwnerForm) {
        this.ownerSelect.focus();
        this.focusOwnerForm = false;
      }
    }, 1000);
  }

  @Input() set customer(customer: CustomerWithContactsDto) {
    this.createCustomerForm();
    CustomerFormUtil.addUUIDToContacts(customer, this.customerForm);
    this.updatedCustomer = customer;
    this.setMapMarkersFromCustomer();
    this.updateCustomerForm(this.updatedCustomer);
    this.setDefaultOwnerIfNull();
  }

  constructor(private formBuilder: FormBuilder,
              private customerService: CustomerService,
              private personService: PersonService,
              private sliderService: SliderService,
              private notificationService: NotificationService,
              private authService: AuthService,
              public phoneService: PhoneService) {
    this.createCustomerForm();
  }


  ngOnInit(): void {
    this.getTypes();
    this.getAccountEmails();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  // @formatter:off
  get authKey(): string { return (this.update) ? 'CustomerEditComponent.' : 'CustomerAddComponent.'; }
  get nip(): AbstractControl { return this.customerForm.get('nip'); }
  get name(): AbstractControl { return this.customerForm.get('name'); }
  get phone(): AbstractControl { return this.customerForm.get('phone'); }
  get website(): AbstractControl { return this.customerForm.get('website'); }
  get linkedIn(): AbstractControl { return this.customerForm.get('linkedIn'); }
  get owner(): AbstractControl { return this.customerForm.get('owner'); }
  get additionalData(): AbstractControl { return this.customerForm.get('additionalData'); }
  get additionalPhones(): AbstractControl { return this.customerForm.get('additionalPhones'); }
  get firstToContact(): AbstractControl { return this.customerForm.get('firstToContact'); }
  // @formatter:on

  get isValid(): boolean {
    this.customerForm.markAsTouched();
    (Object as any).values(this.customerForm.controls).forEach(control => control.markAsTouched());
    return this.customerForm.valid;
  }

  openMoreInfo(): void {
    this.addNewContact.emit(true);
  }

  setMarkerValues(marker: MapViewMarker): void {
    if (!marker) {
      return;
    }
    this.markers = [marker];
    this.mapCenter = [marker.longitude, marker.latitude];
  }

  checkIfNipExistsInDb(event: any): void {
    if (event !== undefined && String(event).length === 10) {
      this.customerService.getCustomerByNip(event, this.getCustomer().id)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (val: string) => {
            return val === '0' ? null : this.nip.setErrors({nipExists: true});
          }
        );
    }
  }

  private setMapMarkersFromCustomer(): void {
    if (this.updatedCustomer && this.updatedCustomer.address
      && this.updatedCustomer.address.latitude && this.updatedCustomer.address.longitude) {

      const marker = this.mapViewComponent
        .getUserClickMarker(this.updatedCustomer.address.latitude, this.updatedCustomer.address.longitude);
      this.setMarkerValues(marker);
    }
  }

  getCustomer(): CustomerWithContactsDto {
    const customer = new CustomerWithContactsDto();

    CustomerFormUtil.setBasicCustomerDataFromForm(customer, this.customerForm);

    if (this.updatedCustomer) {
      customer.id = this.updatedCustomer.id;
      customer.regon = this.updatedCustomer.regon;
      customer.avatar = this.updatedCustomer.avatar;
      customer.type = this.updatedCustomer.type;
      customer.description = this.updatedCustomer.description;
      customer.address = this.updatedCustomer.address;
      customer.contacts = this.updatedCustomer.contacts;
      // customer.additionalPhones = this.updatedCustomer.additionalPhones;
    }
    CustomerFormUtil.setMainContact(customer, this.firstToContact);

    return customer;
  }

  addAdditionalField(): void {
    this.addAdditionalFieldSubject.next();
  }

  getDataFromGUS($event: MouseEvent): void {
    $event.stopPropagation();
    if (this.nip.valid) {
      this.customerService.getDataFromGUS(this.nip.value, (this.updatedCustomer !== undefined ? this.updatedCustomer.id : null))
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (customer: Customer) => this.handleSuccessfulImport(customer),
          (error) => this.notificationService.notifyError(this.errorTypes, error.status)
        );
    } else {
      this.notificationService.notify('customer.form.nipMinLength', NotificationType.ERROR);
    }
  }

  private updateCustomerForm(customer: CustomerWithContactsDto): void {
    if (this.customerForm && customer) {
      CustomerFormUtil.updateCustomerFormBasicData(this.customerForm, customer);
    }
  }

  private createCustomerForm(): void {
    if (!this.customerForm) {
      this.customerForm = this.formBuilder.group({
        nip: ['', Validators.compose([Validators.required, FormValidators.whitespace, Validators.minLength(10)])],
        name: ['', Validators.compose([Validators.required, FormValidators.whitespace, Validators.maxLength(200)])],
        phone: ['', Validators.maxLength(128)],
        additionalPhones: this.formBuilder.array([]),
        website: ['', Validators.maxLength(200)],
        linkedIn: ['', Validators.maxLength(200)],
        firstToContact: [undefined],
        owner: [undefined, [Validators.required]],
        additionalData: [null]
      });
      this.applyPermissionsToForm();
    }
  }

  private getTypes(): void {
    this.types$ = this.customerService.getCustomerTypes().pipe(takeUntil(this.componentDestroyed));
  }

  private handleSuccessfulImport(customer: Customer): void {
    this.setDataFromGUS(customer);
    this.updateCustomerForm(this.updatedCustomer);
    this.notificationService.notify('customer.form.notification.successImportFromGus', NotificationType.SUCCESS);
  }

  private setDataFromGUS(customer: Customer): void {
    if (customer) {
      if (!this.updatedCustomer) {
        this.updatedCustomer = {} as CustomerWithContactsDto;
      }
      CustomerFormUtil.setDataFromGUS(this.updatedCustomer, customer);
    }
  }

  private applyPermissionsToForm(): void {
    if (this.customerForm) {
      applyPermission(this.nip, !this.authService.isPermissionPresent(`${this.authKey}nipField`, 'r'));
      applyPermission(this.name, !this.authService.isPermissionPresent(`${this.authKey}nameField`, 'r'));
      applyPermission(this.phone, !this.authService.isPermissionPresent(`${this.authKey}phoneField`, 'r'));
      applyPermission(this.website, !this.authService.isPermissionPresent(`${this.authKey}websiteField`, 'r'));
      applyPermission(this.linkedIn, !this.authService.isPermissionPresent(`${this.authKey}linkedInField`, 'r'));
      applyPermission(this.owner, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.firstToContact, !this.authService.isPermissionPresent(`${this.authKey}firstToContactField`, 'r'));
    }
  }

  private getAccountEmails(): void {
    this.personService.getAccountEmails()
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      emails => {
        this.accountEmails = emails;
        this.setDefaultOwnerIfNull();
      },
      err => this.notificationService.notify('customer.form.notification.accountEmailsError', NotificationType.ERROR)
    );
  }

  private setDefaultOwnerIfNull(): void {
    if ((!this.update && (!this.updatedCustomer || !this.updatedCustomer.owner)) && this.accountEmails) {
      const loggedAccountId = +localStorage.getItem('loggedAccountId');
      if (loggedAccountId) {
        this.owner.patchValue(loggedAccountId);
      }
    }
  }
  public clear(): void {
    this.customer = {} as Customer;
    this.createCustomerForm();
    this.customerForm.markAsUntouched();
    (Object as any).values(this.customerForm.controls).forEach(control => control.markAsUntouched());
  }

  addAdditionalPhone(event: Event): void {
    event.preventDefault();
    event.stopPropagation();
    this.addAdditionalPhoneNumbersSubject.next();
  }
}
