"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CustomerMiniFormComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var http_status_codes_1 = require("http-status-codes");
var customer_form_util_1 = require("../customer-form.util");
var customer_model_1 = require("../../../customer.model");
var notification_service_1 = require("../../../../shared/notification.service");
var permissions_utils_1 = require("../../../../shared/permissions-utils");
var CustomerMiniFormComponent = /** @class */ (function () {
    function CustomerMiniFormComponent(formBuilder, customerService, personService, sliderService, notificationService, authService) {
        this.formBuilder = formBuilder;
        this.customerService = customerService;
        this.personService = personService;
        this.sliderService = sliderService;
        this.notificationService = notificationService;
        this.authService = authService;
        this.addNewContact = new core_1.EventEmitter();
        this.focusOwnerForm = false;
        this.componentDestroyed = new rxjs_1.Subject();
        this.errorTypes = {
            base: 'customer.form.notification.',
            defaultText: 'unknownError',
            errors: [
                {
                    code: http_status_codes_1.NOT_FOUND,
                    text: 'customerNotFoundInGus'
                },
                {
                    code: http_status_codes_1.BAD_REQUEST,
                    text: 'badRequest'
                },
                {
                    code: http_status_codes_1.GONE,
                    text: 'errorInGus'
                }
            ]
        };
        this.createCustomerForm();
    }
    Object.defineProperty(CustomerMiniFormComponent.prototype, "focusOwner", {
        set: function (focusOwnerForm) {
            var _this = this;
            this.focusOwnerForm = focusOwnerForm;
            setTimeout(function () {
                if (_this.focusOwnerForm) {
                    _this.ownerSelect.focus();
                    _this.focusOwnerForm = false;
                }
            }, 1000);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CustomerMiniFormComponent.prototype, "customer", {
        set: function (customer) {
            this.createCustomerForm();
            customer_form_util_1.CustomerFormUtil.addUUIDToContacts(customer, this.customerForm);
            this.updatedCustomer = customer;
            this.setMapMarkersFromCustomer();
            this.updateCustomerForm(this.updatedCustomer);
            this.setDefaultOwnerIfNull();
        },
        enumerable: false,
        configurable: true
    });
    CustomerMiniFormComponent.prototype.ngOnInit = function () {
        this.getTypes();
        this.getAccountEmails();
    };
    CustomerMiniFormComponent.prototype.ngOnDestroy = function () {
        this.componentDestroyed.next();
        this.componentDestroyed.complete();
    };
    Object.defineProperty(CustomerMiniFormComponent.prototype, "authKey", {
        get: function () {
            return (this.update) ? 'CustomerEditComponent.' : 'CustomerAddComponent.';
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CustomerMiniFormComponent.prototype, "nip", {
        get: function () {
            return this.customerForm.get('nip');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CustomerMiniFormComponent.prototype, "name", {
        get: function () {
            return this.customerForm.get('name');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CustomerMiniFormComponent.prototype, "phone", {
        get: function () {
            return this.customerForm.get('phone');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CustomerMiniFormComponent.prototype, "website", {
        get: function () {
            return this.customerForm.get('website');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CustomerMiniFormComponent.prototype, "owner", {
        get: function () {
            return this.customerForm.get('owner');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CustomerMiniFormComponent.prototype, "firstToContact", {
        get: function () {
            return this.customerForm.get('firstToContact');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CustomerMiniFormComponent.prototype, "isValid", {
        get: function () {
            this.customerForm.markAsTouched();
            Object.values(this.customerForm.controls).forEach(function (control) { return control.markAsTouched(); });
            return this.customerForm.valid;
        },
        enumerable: false,
        configurable: true
    });
    CustomerMiniFormComponent.prototype.openMoreInfo = function () {
        this.addNewContact.emit(true);
    };
    CustomerMiniFormComponent.prototype.setMarkerValues = function (marker) {
        if (!marker) {
            return;
        }
        this.markers = [marker];
        this.mapCenter = [marker.longitude, marker.latitude];
    };
    CustomerMiniFormComponent.prototype.setMapMarkersFromCustomer = function () {
        if (this.updatedCustomer && this.updatedCustomer.address
            && this.updatedCustomer.address.latitude && this.updatedCustomer.address.longitude) {
            var marker = this.mapViewComponent
                .getUserClickMarker(this.updatedCustomer.address.latitude, this.updatedCustomer.address.longitude);
            this.setMarkerValues(marker);
        }
    };
    CustomerMiniFormComponent.prototype.getCustomer = function () {
        var customer = new customer_model_1.CustomerWithContactsDto();
        customer_form_util_1.CustomerFormUtil.setBasicCustomerDataFromForm(customer, this.customerForm);
        if (this.updatedCustomer) {
            customer.id = this.updatedCustomer.id;
            customer.regon = this.updatedCustomer.regon;
            customer.avatar = this.updatedCustomer.avatar;
            customer.type = this.updatedCustomer.type;
            customer.description = this.updatedCustomer.description;
            customer.address = this.updatedCustomer.address;
            customer.contacts = this.updatedCustomer.contacts;
        }
        customer_form_util_1.CustomerFormUtil.setMainContact(customer, this.firstToContact);
        return customer;
    };
    CustomerMiniFormComponent.prototype.getDataFromGUS = function ($event) {
        var _this = this;
        $event.stopPropagation();
        if (this.nip.valid) {
            this.customerService.getDataFromGUS(this.nip.value)
                .pipe(operators_1.takeUntil(this.componentDestroyed))
                .subscribe(function (customer) { return _this.handleSuccessfulImport(customer); }, function (error) { return _this.notificationService.notifyError(_this.errorTypes, error.status); });
        }
        else {
            this.notificationService.notify('customer.form.nipMinLength', notification_service_1.NotificationType.ERROR);
        }
    };
    CustomerMiniFormComponent.prototype.updateCustomerForm = function (customer) {
        if (this.customerForm && customer) {
            customer_form_util_1.CustomerFormUtil.updateCustomerFormBasicData(this.customerForm, customer);
        }
    };
    CustomerMiniFormComponent.prototype.createCustomerForm = function () {
        if (!this.customerForm) {
            this.customerForm = this.formBuilder.group({
                nip: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(10)])],
                name: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(200)])],
                phone: ['', forms_1.Validators.minLength(11)],
                website: [''],
                firstToContact: [undefined],
                owner: [undefined, [forms_1.Validators.required]]
            });
            this.applyPermissionsToForm();
        }
    };
    CustomerMiniFormComponent.prototype.getTypes = function () {
        this.types$ = this.customerService.getCustomerTypes().pipe(operators_1.takeUntil(this.componentDestroyed));
    };
    CustomerMiniFormComponent.prototype.handleSuccessfulImport = function (customer) {
        this.setDataFromGUS(customer);
        this.updateCustomerForm(this.updatedCustomer);
        this.notificationService.notify('customer.form.notification.successImportFromGus', notification_service_1.NotificationType.SUCCESS);
    };
    CustomerMiniFormComponent.prototype.setDataFromGUS = function (customer) {
        if (customer) {
            if (!this.updatedCustomer) {
                this.updatedCustomer = {};
            }
            customer_form_util_1.CustomerFormUtil.setDataFromGUS(this.updatedCustomer, customer);
        }
    };
    CustomerMiniFormComponent.prototype.applyPermissionsToForm = function () {
        if (this.customerForm) {
            permissions_utils_1.applyPermission(this.nip, !this.authService.isPermissionPresent(this.authKey + "nipField", 'r'));
            permissions_utils_1.applyPermission(this.name, !this.authService.isPermissionPresent(this.authKey + "nameField", 'r'));
            permissions_utils_1.applyPermission(this.phone, !this.authService.isPermissionPresent(this.authKey + "phoneField", 'r'));
            permissions_utils_1.applyPermission(this.website, !this.authService.isPermissionPresent(this.authKey + "websiteField", 'r'));
            permissions_utils_1.applyPermission(this.owner, !this.authService.isPermissionPresent(this.authKey + "ownerField", 'r'));
            permissions_utils_1.applyPermission(this.firstToContact, !this.authService.isPermissionPresent(this.authKey + "firstToContactField", 'r'));
        }
    };
    CustomerMiniFormComponent.prototype.getAccountEmails = function () {
        var _this = this;
        this.personService.getAccountEmails()
            .pipe(operators_1.takeUntil(this.componentDestroyed)).subscribe(function (emails) {
            _this.accountEmails = emails;
            _this.setDefaultOwnerIfNull();
        }, function (err) { return _this.notificationService.notify('customer.form.notification.accountEmailsError', notification_service_1.NotificationType.ERROR); });
    };
    CustomerMiniFormComponent.prototype.setDefaultOwnerIfNull = function () {
        if ((!this.update && (!this.updatedCustomer || !this.updatedCustomer.owner)) && this.accountEmails) {
            var loggedAccountId = +localStorage.getItem('loggedAccountId');
            if (loggedAccountId) {
                this.owner.patchValue(loggedAccountId);
            }
        }
    };
    CustomerMiniFormComponent.prototype.clear = function () {
        this.customer = {};
        this.createCustomerForm();
        this.customerForm.markAsUntouched();
        Object.values(this.customerForm.controls).forEach(function (control) { return control.markAsUntouched(); });
    };
    __decorate([
        core_1.Input()
    ], CustomerMiniFormComponent.prototype, "update");
    __decorate([
        core_1.Output()
    ], CustomerMiniFormComponent.prototype, "addNewContact");
    __decorate([
        core_1.ViewChild('mapView')
    ], CustomerMiniFormComponent.prototype, "mapViewComponent");
    __decorate([
        core_1.ViewChild('ownerSelect')
    ], CustomerMiniFormComponent.prototype, "ownerSelect");
    __decorate([
        core_1.Input()
    ], CustomerMiniFormComponent.prototype, "customersToChangeOwnerOperation");
    __decorate([
        core_1.Input()
    ], CustomerMiniFormComponent.prototype, "focusOwner");
    __decorate([
        core_1.Input()
    ], CustomerMiniFormComponent.prototype, "customer");
    CustomerMiniFormComponent = __decorate([
        core_1.Component({
            selector: 'app-customer-mini-form',
            templateUrl: './customer-mini-form.component.html',
            styleUrls: ['./customer-mini-form.component.scss']
        })
    ], CustomerMiniFormComponent);
    return CustomerMiniFormComponent;
}());
exports.CustomerMiniFormComponent = CustomerMiniFormComponent;
