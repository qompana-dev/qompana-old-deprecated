import {AbstractControl, FormGroup} from '@angular/forms';
import * as uuid from 'uuid';
import {Customer, CustomerWithContactsDto} from '../../customer.model';

export class CustomerFormUtil {
  static setMainContact(customer: CustomerWithContactsDto, firstToContact: AbstractControl): void {
    const firstToContactUUID = firstToContact.value;
    if (firstToContactUUID && customer.contacts) {
      customer.contacts = customer.contacts.map(contact => {
        contact.mainContact = contact.uuid === firstToContactUUID;
        contact.uuid = undefined;
        return contact;
      });
    }
  }

  static setBasicCustomerDataFromForm(customer: CustomerWithContactsDto, customerForm: FormGroup): void {
    customer.nip = customerForm.get('nip').value;
    customer.name = customerForm.get('name').value;
    customer.phone = customerForm.get('phone').value;
    customer.website = customerForm.get('website').value;
    customer.linkedIn = customerForm.get('linkedIn').value;
    customer.owner = customerForm.get('owner').value;
    customer.additionalData = customerForm.get('additionalData').value;
    customer.additionalPhones = customerForm.get('additionalPhones').value;
    customer.avatar = (customerForm.get('avatar') ? customerForm.get('avatar').value : null);
  }

  static updateCustomerFormBasicData(customerForm: FormGroup, customer: CustomerWithContactsDto): void {
    customerForm.get('nip').patchValue(customer.nip);
    customerForm.get('name').patchValue(customer.name);
    customerForm.get('phone').patchValue(customer.phone);
    customerForm.get('website').patchValue(customer.website);
    customerForm.get('linkedIn').patchValue(customer.linkedIn);
    customerForm.get('owner').patchValue(customer.owner);
    customerForm.get('additionalData').patchValue(customer.additionalData);
    if (customerForm.get('avatar')) {
      customerForm.get('avatar').patchValue(customer.avatar);
    }
    if (customer.contacts) {
      customer.contacts.forEach(contact => {
        if (contact.mainContact) {
          customerForm.get('firstToContact').patchValue(contact.uuid);
        }
      });
    }
  }

  static setBasicCustomerDataFromFormInCustomerInfo(customer: CustomerWithContactsDto, customerForm: FormGroup): void {
    customer.nip = customerForm.get('nip').value;
    customer.name = customerForm.get('name').value;
    customer.phone = customerForm.get('phone').value;
    customer.website = customerForm.get('website').value;
    customer.avatar = customerForm.get('avatar').value;
  }

  static updateCustomerFormBasicDataInCustomerInfo(customerForm: FormGroup, customer: CustomerWithContactsDto): void {
    customerForm.get('nip').patchValue(customer.nip);
    customerForm.get('name').patchValue(customer.name);
    customerForm.get('phone').patchValue(customer.phone);
    customerForm.get('website').patchValue(customer.website);
  }

  static setDataFromGUS(updatedCustomer: CustomerWithContactsDto, customer: Customer): void {
    updatedCustomer.name = customer.name;
    updatedCustomer.nip = customer.nip;
    updatedCustomer.type = customer.type;
    updatedCustomer.description = customer.description;
    updatedCustomer.regon = customer.regon;
    updatedCustomer.phone = customer.phone;
    updatedCustomer.website = customer.website;
    updatedCustomer.address = customer.address;
  }

  static addUUIDToContacts(customer: CustomerWithContactsDto, customerForm: FormGroup): void {
    if (customer && customer.contacts) {
      customer.contacts = customer.contacts.map(contact => {
        contact.uuid = uuid.v4();
        contact.autofocusNameOnStart = false;
        if (contact.mainContact) {
          customerForm.get('firstToContact').patchValue(contact.uuid);
        }
        return contact;
      });
    }
  }
}
