import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as cloneDeep from 'lodash/cloneDeep';
import {CustomerService} from '../../../customer.service';
import {AuthService} from '../../../../login/auth/auth.service';
import {ContactDto} from '../../../customer.model';
import {applyPermission} from '../../../../shared/permissions-utils';
import {FormValidators} from '../../../../shared/form/form-validators';
import {NotificationService} from '../../../../shared/notification.service';
import {PhoneService} from '../../../../shared/phone-fields/phone.service';

@Component({
  selector: 'app-customer-new-contact-form',
  templateUrl: './customer-new-contact-form.component.html',
  styleUrls: ['./customer-new-contact-form.component.scss']
})
export class CustomerNewContactFormComponent implements OnInit {

  @Input() update: boolean;
  contactForm: FormGroup;
  selectedContact: ContactDto;
  autofocusNameOnStart = false;


  @Input() set contact(contact: ContactDto) {
    if (contact.autofocusNameOnStart) {
      this.autofocusNameOnStart = true;
      contact.autofocusNameOnStart = undefined;
    }
    this.selectedContact = contact;
    this.updateContactForm(contact);
  }

  @Output() contactNameChanged: EventEmitter<string> = new EventEmitter<string>();
  @Output() contactSurnameChanged: EventEmitter<string> = new EventEmitter<string>();
  @Output() formFocusOut: EventEmitter<boolean> = new EventEmitter<boolean>();

  formControlFocusedKey: string;
  formControlFocusedValue: string;

  constructor(private authService: AuthService,
              private customerService: CustomerService,
              private fb: FormBuilder,
              public phoneService: PhoneService) {
    this.createContactForm();
  }

  ngOnInit(): void {
  }

  avatarChanged(): void {
    this.formFocusOut.emit(true);
  }

  formControlFocusIn(formControlName: string): void {
    this.formControlFocusedKey = cloneDeep(formControlName);
    this.formControlFocusedValue = cloneDeep(this.contactForm.get(formControlName).value);
  }

  formControlFocusOut(): void {
    if (this.formControlFocusedKey) {
      const formChanged = this.contactForm.get(this.formControlFocusedKey).value !== this.formControlFocusedValue;
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
      if (formChanged) {
        this.formFocusOut.emit(true);
      }
    }
  }

  formControlEsc(): void {
    if (this.formControlFocusedKey) {
      this.contactForm.get(this.formControlFocusedKey).patchValue(this.formControlFocusedValue);
    }
  }

  get authKey(): string {
    return (this.update) ? 'ContactEditComponent.' : 'ContactAddComponent.';
  }

  get name(): AbstractControl {
    return this.contactForm.get('name');
  }

  get surname(): AbstractControl {
    return this.contactForm.get('surname');
  }

  get position(): AbstractControl {
    return this.contactForm.get('position');
  }

  get phone(): AbstractControl {
    return this.contactForm.get('phone');
  }

  get email(): AbstractControl {
    return this.contactForm.get('email');
  }

  get isValid(): boolean {
    this.contactForm.markAsTouched();
    (Object as any).values(this.contactForm.controls).forEach(control => control.markAsTouched());
    return this.contactForm.valid;
  }

  get avatar(): AbstractControl {
    return this.contactForm.get('avatar');
  }

  public getContact(): ContactDto {
    const contact: ContactDto = new ContactDto();
    contact.id = this.selectedContact.id;
    contact.name = this.name.value;
    contact.surname = this.surname.value;
    contact.position = this.position.value;
    contact.phone = !!this.phone.value ? this.phone.value : null;
    contact.email = !!this.email.value ? this.email.value : null;
    contact.uuid = this.selectedContact.uuid;
    contact.mainContact = this.selectedContact.mainContact;
    contact.avatar = !!this.avatar.value ? this.avatar.value : null;
    return contact;
  }

  private createContactForm(): void {
    this.contactForm = this.fb.group({
      name: ['', Validators.compose([
        Validators.maxLength(200),
        FormValidators.whitespace,
        Validators.required])
      ],
      surname: ['', Validators.compose([
        Validators.maxLength(200),
        FormValidators.whitespace,
        Validators.required])
      ],
      position: ['', [Validators.maxLength(100), FormValidators.whitespace]],
      phone: ['', Validators.maxLength(128)],
      email: ['', Validators.compose([
        Validators.maxLength(100),
        Validators.email
      ])],
      avatar: [null]
    });
    this.contactForm.disable();
    this.applyPermissionsToForm();
  }

  private applyPermissionsToForm(): void {
    if (this.contactForm) {
      applyPermission(this.name, !this.authService.isPermissionPresent(`${this.authKey}nameField`, 'r'));
      applyPermission(this.surname, !this.authService.isPermissionPresent(`${this.authKey}surnameField`, 'r'));
      applyPermission(this.position, !this.authService.isPermissionPresent(`${this.authKey}positionField`, 'r'));
      applyPermission(this.phone, !this.authService.isPermissionPresent(`${this.authKey}phoneField`, 'r'));
      applyPermission(this.email, !this.authService.isPermissionPresent(`${this.authKey}emailField`, 'r'));
    }
  }

  private updateContactForm(contact: ContactDto): void {
    if (this.contactForm && contact) {
      this.name.patchValue(contact.name);
      this.surname.patchValue(contact.surname);
      this.position.patchValue(contact.position);
      this.phone.patchValue(contact.phone);
      this.email.patchValue(contact.email);
      this.avatar.patchValue(contact.avatar);
    }
  }
}
