import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BAD_REQUEST, GONE, NOT_FOUND } from 'http-status-codes';
import * as uuid from 'uuid';
import { CustomerNewContactFormComponent } from './customer-new-contact-form/customer-new-contact-form.component';
import { CustomerFormUtil } from './customer-form.util';
import * as cloneDeep from 'lodash/cloneDeep';
import { SliderService, SliderStateChange } from '../../../shared/slider/slider.service';
import { CustomerService } from '../../customer.service';
import { Address, ContactDto, Customer, CustomerType, CustomerWithContactsDto } from '../../customer.model';
import { PersonService } from '../../../person/person.service';
import { AccountEmail } from '../../../person/person.model';
import { MapViewMarker } from '../../../map/map-view/map-view.model';
import { ErrorTypes, NotificationService, NotificationType } from '../../../shared/notification.service';
import { MapViewComponent } from '../../../map/map-view/map-view.component';
import { AuthService } from '../../../login/auth/auth.service';
import { applyPermission } from '../../../shared/permissions-utils';
import { FormValidators } from '../../../shared/form/form-validators';
import { AdditionalDataFormComponent } from '../../../shared/additional-data-form/additional-data-form.component';
import {PhoneService} from '../../../shared/phone-fields/phone.service';


@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.scss']
})
export class CustomerFormComponent implements OnInit, OnDestroy {

  @Input() update: boolean;
  @Input() tabView = false;
  @Input() hideMatCardStyle = false;

  @Output() formFocusOut: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('mapView') mapViewComponent: MapViewComponent;
  @ViewChild('contactDiv') contactDiv: ElementRef;
  @ViewChild('additionalDataFormComponent') additionalDataFormComponent: AdditionalDataFormComponent;
  @ViewChildren(CustomerNewContactFormComponent) contactForms: QueryList<CustomerNewContactFormComponent>;

  markers: MapViewMarker[];
  mapCenter: number[];

  customerForm: FormGroup;
  accountEmails: AccountEmail[];

  contacts: ContactDto[] = [];
  oldContacts: ContactDto[] = [];

  updatedCustomer: CustomerWithContactsDto;
  oldCustomerForm: CustomerWithContactsDto;

  private componentDestroyed: Subject<void> = new Subject();

  types$: Observable<CustomerType[]>;

  private additionalDataSubscription: Subscription;
  private addAdditionalFieldSubject: Subject<void> = new Subject<void>();
  public addAdditionalField$: Observable<void> = this.addAdditionalFieldSubject.asObservable();

  private addAdditionalPhoneNumbersSubject: Subject<void> = new Subject<void>();
  public addAdditionalPhoneNumbers$: Observable<void> = this.addAdditionalPhoneNumbersSubject.asObservable();

  readonly errorTypes: ErrorTypes = {
    base: 'customer.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'customerNotFoundInGus'
      },
      {
        code: BAD_REQUEST,
        text: 'badRequest'
      },
      {
        code: GONE,
        text: 'errorInGus'
      }
    ]
  };

  formControlFocusedKey: string;
  formControlFocusedValue: string;
  avatarWidthPx = 100;

  @Input() set customer(customer: CustomerWithContactsDto) {
    if (customer) {
      this.createCustomerForm();

      CustomerFormUtil.addUUIDToContacts(customer, this.customerForm);
      this.updatedCustomer = customer;
      this.setMapMarkersFromCustomer();
      this.updateCustomerForm(customer);

      this.setDefaultOwnerIfNull();
      this.oldCustomerForm = cloneDeep(this.updatedCustomer);
      this.oldContacts = cloneDeep(this.contacts);
    }
  }

  @Input() set addContactAndScrollToNewForm(addNewContact: boolean) {
    if (addNewContact) {
      this.addContactAndScrollToNewContactFrom();
    }
  }

  constructor(private formBuilder: FormBuilder,
    private customerService: CustomerService,
    private personService: PersonService,
    private sliderService: SliderService,
    private notificationService: NotificationService,
    private authService: AuthService,
    public phoneService: PhoneService) {
    this.createCustomerForm();
  }

  ngOnInit(): void {
    this.getTypes();
    this.getAccountEmails();
    this.subscribeForSliderClose();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  formControlFocusIn(formControlName: string): void {
    this.formControlFocusedKey = cloneDeep(formControlName);
    this.formControlFocusedValue = cloneDeep(this.customerForm.get(formControlName).value);
  }

  formControlFocusOut(newValue?: string): void {
    if (this.formControlFocusedKey) {
      const formValue = (!newValue) ? this.customerForm.get(this.formControlFocusedKey).value : newValue;
      this.customerForm.get(this.formControlFocusedKey).setValue(formValue);
      const formChanged = formValue !== this.formControlFocusedValue;
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
      if (formChanged) {
        this.emitFocusOut();
      }
    }
  }

  // TODO(ekon): may be improved or merged with formControlFocusOut
  formControlFocusOutAdditionalPhones(newValue?: string): void {
    if (this.formControlFocusedKey) {
      const formValue = this.customerForm.get(this.formControlFocusedKey).value;
      const formChanged = JSON.stringify(formValue) !== JSON.stringify(this.formControlFocusedValue);
      if (formChanged) {
        this.emitFocusOut();
      }
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
    }
  }

  emitFocusOut(): void {
    this.formFocusOut.emit(true);
  }

  formControlEsc(): void {
    if (this.formControlFocusedKey) {
      this.customerForm.get(this.formControlFocusedKey).patchValue(this.formControlFocusedValue);
    }
  }

  // @formatter:off
  get authKey(): string { return (this.update) ? 'CustomerEditComponent.' : 'CustomerAddComponent.'; }
  get nip(): AbstractControl { return this.customerForm.get('nip'); }
  get regon(): AbstractControl { return this.customerForm.get('regon'); }
  get name(): AbstractControl { return this.customerForm.get('name'); }
  get type(): AbstractControl { return this.customerForm.get('type'); }
  get street(): AbstractControl { return this.customerForm.get('street'); }
  get city(): AbstractControl { return this.customerForm.get('city'); }
  get zipCode(): AbstractControl { return this.customerForm.get('zipCode'); }
  get country(): AbstractControl { return this.customerForm.get('country'); }
  get phone(): AbstractControl { return this.customerForm.get('phone'); }
  get website(): AbstractControl { return this.customerForm.get('website'); }
  get description(): AbstractControl { return this.customerForm.get('description'); }
  get owner(): AbstractControl { return this.customerForm.get('owner'); }
  get firstToContact(): AbstractControl { return this.customerForm.get('firstToContact'); }
  get employeesNumber(): AbstractControl { return this.customerForm.get('employeesNumber'); }
  get companyType(): AbstractControl { return this.customerForm.get('companyType'); }
  get income(): AbstractControl { return this.customerForm.get('income'); }
  get additionalData(): AbstractControl { return this.customerForm.get('additionalData'); }
  get additionalPhones(): AbstractControl { return this.customerForm.get('additionalPhones'); }
  get avatar(): AbstractControl { return this.customerForm.get('avatar'); }
  // @formatter:on

  get isValid(): boolean {
    this.customerForm.markAsTouched();
    (Object as any).values(this.customerForm.controls).forEach(control => control.markAsTouched());
    const validContacts = this.contactForms.filter((contactForm: CustomerNewContactFormComponent) => !contactForm.isValid).length === 0;
    return this.customerForm.valid && validContacts;
  }

  setMarkerValues(marker: MapViewMarker): void {
    if (!marker) {
      return;
    }
    this.markers = [marker];
    this.mapCenter = [marker.longitude, marker.latitude];
  }

  private getContacts(): ContactDto[] {
    return this.contactForms.map(form => form.getContact());
  }

  private setMapMarkersFromCustomer(): void {
    if (this.updatedCustomer && this.updatedCustomer.address
      && this.updatedCustomer.address.latitude && this.updatedCustomer.address.longitude) {

      const marker = this.mapViewComponent
        .getUserClickMarker(this.updatedCustomer.address.latitude, this.updatedCustomer.address.longitude);
      this.setMarkerValues(marker);
    }
  }

  getCustomer(): CustomerWithContactsDto {
    const customer = new CustomerWithContactsDto();
    if (this.updatedCustomer && this.updatedCustomer.id) {
      customer.id = this.updatedCustomer.id;
    }

    CustomerFormUtil.setBasicCustomerDataFromForm(customer, this.customerForm);

    customer.regon = this.regon.value;
    customer.type = this.type.value && { id: this.type.value, name: null, description: null };
    customer.description = this.description.value;

    const address = new Address();
    if (this.updatedCustomer && this.updatedCustomer.address) {
      address.id = this.updatedCustomer.id;
    }
    if (this.markers && this.markers.length > 0) {
      const marker: MapViewMarker = this.markers[0];
      address.latitude = String(marker.latitude);
      address.longitude = String(marker.longitude);
    }
    address.street = this.street.value;
    address.city = this.city.value;
    address.zipCode = this.zipCode.value;
    address.country = this.country.value;
    customer.address = address;
    customer.contacts = this.getContacts();
    CustomerFormUtil.setMainContact(customer, this.firstToContact);
    customer.employeesNumber = this.employeesNumber.value;
    customer.income = this.income.value;
    customer.companyType = this.companyType.value;
    customer.avatar = this.avatar.value;
    customer.additionalPhones = this.additionalPhones.value;
    return customer;
  }

  getDataFromGUS($event: MouseEvent): void {
    $event.stopPropagation();
    if (this.nip.valid) {
      this.customerService.getDataFromGUS(this.nip.value, this.updatedCustomer.id)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (customer: Customer) => this.handleSuccessfulImport(customer),
          (error) => this.notificationService.notifyError(this.errorTypes, error.status)
        );
    } else {
      this.notificationService.notify('customer.form.nipMinLength', NotificationType.ERROR);
    }
  }

  addContactAndScrollToNewContactFrom(): void {
    const newContact = this.newContact();
    newContact.autofocusNameOnStart = true;
    this.firstToContact.patchValue(newContact.uuid);

    if (this.contactDiv && this.contactDiv.nativeElement) {
      setTimeout(() => {
        this.contactDiv.nativeElement.scrollTop = this.contactDiv.nativeElement.scrollHeight;
      }, 1000);
    }
  }

  newContact(): ContactDto {
    const contact = {} as ContactDto;
    contact.uuid = uuid.v4();
    contact.mainContact = false;
    this.contacts.push(contact);
    return contact;
  }

  addAdditionalField(): void {
    this.addAdditionalFieldSubject.next();
  }

  private updateCustomerForm(customer: CustomerWithContactsDto): void {
    if (this.customerForm && customer) {
      this.regon.patchValue(customer.regon);
      if (customer.type) {
        this.type.patchValue(customer.type.id);
      }
      this.description.patchValue(customer.description);
      this.employeesNumber.patchValue(customer.employeesNumber);
      this.companyType.patchValue(customer.companyType);
      this.income.patchValue(customer.income);
      if (customer.address) {
        this.street.patchValue(customer.address.street);
        this.city.patchValue(customer.address.city);
        this.zipCode.patchValue(customer.address.zipCode);
        this.country.patchValue(customer.address.country);
      }
      this.contacts = (!customer.contacts) ? [] : customer.contacts;
      this.unsubscribeAdditionalDataChange();
      CustomerFormUtil.updateCustomerFormBasicData(this.customerForm, customer);
      this.subscribeAdditionalDataChange();
    }
  }

  private createCustomerForm(): void {
    if (!this.customerForm) {
      this.customerForm = this.formBuilder.group({
        nip: ['', Validators.compose([Validators.required, FormValidators.whitespace, Validators.minLength(10)])],
        name: ['', Validators.compose([Validators.required, FormValidators.whitespace, Validators.maxLength(200)])],
        type: [null],
        street: ['', Validators.maxLength(200)],
        description: ['', Validators.maxLength(500)],
        city: ['', Validators.maxLength(100)],
        zipCode: ['', Validators.minLength(5)],
        country: ['', Validators.maxLength(100)],
        phone: ['', Validators.maxLength(128)],
        additionalPhones: this.formBuilder.array([]),
        website: ['', Validators.maxLength(200)],
        linkedIn: ['', Validators.maxLength(200)],
        owner: [undefined, [Validators.required]],
        firstToContact: [undefined],
        regon: ['', Validators.compose([Validators.minLength(9), Validators.maxLength(14),
        Validators.pattern('^(?=[0-9]*$)(?:.{9}|.{14})$')])],
        employeesNumber: ['', Validators.maxLength(18)],
        companyType: ['', Validators.maxLength(100)],
        income: ['', Validators.maxLength(100)],
        avatar: [null],
        additionalData: [null]
      });
      this.contacts = [({} as ContactDto)];
      this.applyPermissionsToForm();
      this.subscribeAdditionalDataChange();
    }
  }

  private unsubscribeAdditionalDataChange(): void {
    if (this.additionalDataSubscription) {
      this.additionalDataSubscription.unsubscribe();
    }
  }

  private subscribeAdditionalDataChange(): void {
    this.unsubscribeAdditionalDataChange();
    this.additionalDataSubscription = this.additionalData.valueChanges
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.emitFocusOut());
  }

  private subscribeForSliderClose(): void {
    this.sliderService.sliderStateChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (stateChange: SliderStateChange) => {
          if (stateChange.opened === false && stateChange.key !== 'CustomerForm.MapSlider') {
            this.customerForm.reset();
            this.additionalDataFormComponent.reset();
            this.mapViewComponent.clearUserSelectedMarker();
            this.markers = undefined;
          }
        }
      );
  }

  private getTypes(): void {
    this.types$ = this.customerService.getCustomerTypes().pipe(takeUntil(this.componentDestroyed));
  }

  private handleSuccessfulImport(customer: Customer): void {
    this.oldCustomerForm = cloneDeep(this.updatedCustomer);
    this.setDataFromGUS(customer);
    this.updateCustomerForm(this.updatedCustomer);
    this.notificationService.notify('customer.form.notification.successImportFromGus', NotificationType.SUCCESS);
    this.emitFocusOut();
  }

  private setDataFromGUS(customer: Customer): void {
    if (customer) {
      CustomerFormUtil.setDataFromGUS(this.updatedCustomer, customer);
    }
  }

  private applyPermissionsToForm(): void {
    if (this.customerForm) {
      applyPermission(this.nip, !this.authService.isPermissionPresent(`${this.authKey}nipField`, 'r'));
      applyPermission(this.regon, !this.authService.isPermissionPresent(`${this.authKey}regonField`, 'r'));
      applyPermission(this.name, !this.authService.isPermissionPresent(`${this.authKey}nameField`, 'r'));
      applyPermission(this.type, !this.authService.isPermissionPresent(`${this.authKey}typeField`, 'r'));
      applyPermission(this.street, !this.authService.isPermissionPresent(`${this.authKey}streetField`, 'r'));
      applyPermission(this.city, !this.authService.isPermissionPresent(`${this.authKey}cityField`, 'r'));
      applyPermission(this.zipCode, !this.authService.isPermissionPresent(`${this.authKey}zipCodeField`, 'r'));
      applyPermission(this.country, !this.authService.isPermissionPresent(`${this.authKey}countryField`, 'r'));
      applyPermission(this.phone, !this.authService.isPermissionPresent(`${this.authKey}phoneField`, 'r'));
      applyPermission(this.website, !this.authService.isPermissionPresent(`${this.authKey}websiteField`, 'r'));
      applyPermission(this.description, !this.authService.isPermissionPresent(`${this.authKey}descriptionField`, 'r'));
      applyPermission(this.owner, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.employeesNumber, !this.authService.isPermissionPresent(`${this.authKey}employeesNumber`, 'r'));
      applyPermission(this.companyType, !this.authService.isPermissionPresent(`${this.authKey}companyType`, 'r'));
      applyPermission(this.income, !this.authService.isPermissionPresent(`${this.authKey}income`, 'r'));
    }
  }

  private getAccountEmails(): void {
    this.personService.getAccountEmails()
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        emails => {
          this.accountEmails = emails;
          this.setDefaultOwnerIfNull();
        },
        err => this.notificationService.notify('customer.form.notification.accountEmailsError', NotificationType.ERROR)
      );
  }

  private setDefaultOwnerIfNull(): void {
    if ((!this.update && this.updatedCustomer && !this.updatedCustomer.owner) && this.accountEmails) {
      const loggedAccountId = +localStorage.getItem('loggedAccountId');
      if (loggedAccountId) {
        this.owner.patchValue(loggedAccountId);
      }
    }
  }

  public clear(): void {
    this.customer = {} as Customer;
    this.createCustomerForm();
    this.customerForm.markAsUntouched();
    (Object as any).values(this.customerForm.controls).forEach(control => control.markAsUntouched());
  }

  checkIfNipExistsInDb(event: any): void {
    if (event !== undefined && String(event).length === 10) {
      this.customerService.getCustomerByNip(event, this.getCustomer().id)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (val: string) => {
            return val === '0' ? null : this.nip.setErrors({ nipExists: true });
          }
        );
    }
  }

  revertChanges(): void {
    this.contacts = cloneDeep(this.oldContacts);
    this.setMapMarkersFromCustomer();
    this.updateCustomerForm(this.oldCustomerForm);
    if (this.oldCustomerForm) {
      this.oldCustomerForm.contacts = this.contacts;
    }
    this.updatedCustomer = cloneDeep(this.oldCustomerForm);
    if (this.contactForms) {
      this.getContacts();
    }
    this.setDefaultOwnerIfNull();
  }

  saved(): void {
    this.oldCustomerForm = cloneDeep(this.updatedCustomer);
    this.oldContacts = cloneDeep(this.contacts);
  }

  addAdditionalPhone(event: Event): void {
    event.preventDefault();
    event.stopPropagation();
    this.addAdditionalPhoneNumbersSubject.next();
  }

}
