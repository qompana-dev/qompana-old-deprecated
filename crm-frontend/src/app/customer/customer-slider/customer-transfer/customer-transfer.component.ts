import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {CustomerFormComponent} from '../customer-form/customer-form.component';
import {BAD_REQUEST, CONFLICT, NOT_FOUND} from 'http-status-codes';
import {takeUntil} from 'rxjs/operators';
import {CustomerMiniFormComponent} from '../customer-form/customer-mini-form/customer-mini-form.component';
import * as cloneDeep from 'lodash/cloneDeep';
import {CustomerService} from '../../customer.service';
import {SliderService, SliderWidth} from '../../../shared/slider/slider.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {CustomerWithContactsDto} from '../../customer.model';


@Component({
  selector: 'app-customer-transfer',
  templateUrl: './customer-transfer.component.html',
  styleUrls: ['./customer-transfer.component.scss']
})
export class CustomerTransferComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();
  private currentWidth: string;
  customer: CustomerWithContactsDto;
  @Input() customersSelected: CustomerWithContactsDto[];
  @Output() customerAdded: EventEmitter<void> = new EventEmitter();
  miniForm = true;
  addContactAndScrollToNewForm = false;
  @Input() focusOwnerForm = false;

  @ViewChild(CustomerFormComponent) private customerFormComponent: CustomerFormComponent;
  @ViewChild(CustomerMiniFormComponent) private customerMiniFormComponent: CustomerMiniFormComponent;

  readonly errorTypesForModification: ErrorTypes = {
    base: 'customer.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'validationError'
      }, {
        code: NOT_FOUND,
        text: 'notFound'
      }, {
        code: CONFLICT,
        text: 'nameOrNipOccupied'
      }
    ]
  };

  readonly errorTypesForGet: ErrorTypes = {
    base: 'customer.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'notFound'
      }
    ]
  };

  constructor(private sliderService: SliderService,
              private customerService: CustomerService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {

      const newOwner = this.customerMiniFormComponent.getCustomer().owner;
      this.customersSelected.forEach(customer => customer.owner = newOwner);
      this.customerService.modifyCustomers(this.customersSelected)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => this.handleSuccess(),
          (error) => this.notificationService.notifyError(this.errorTypesForModification, error.status)
        );
    
  }

  private handleSuccess(): void {
    this.notificationService.notify('customer.form.notification.edited', NotificationType.SUCCESS);
    this.customerAdded.next();
    this.closeSlider();
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
    this.addContactAndScrollToNewForm = false;
  }
}
