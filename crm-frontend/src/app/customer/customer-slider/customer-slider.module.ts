import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SharedModule} from '../../shared/shared.module';
import {CustomerAddComponent} from './customer-add/customer-add.component';
import {NgxMaskModule} from 'ngx-mask';
import {MapViewModule} from '../../map/map-view/map-view.module';
import {CustomerEditComponent} from './customer-edit/customer-edit.component';
import {CustomerFormComponent} from './customer-form/customer-form.component';
import {CustomerMiniFormComponent} from './customer-form/customer-mini-form/customer-mini-form.component';
import {CustomerNewContactFormComponent} from './customer-form/customer-new-contact-form/customer-new-contact-form.component';
import { CustomerWhitelistComponent } from './customer-whitelist/customer-whitelist.component';
import { CustomerTransferComponent } from './customer-transfer/customer-transfer.component';
import {AvatarModule} from '../../shared/avatar-form/avatar.module';
import {AdditionalDataFormModule} from '../../shared/additional-data-form/additional-data-form.module';
import {AdditionalPhoneNumbersModule} from '../../shared/additional-phone-numbers/additional-phone-numbers.module';

@NgModule({
  declarations: [
    CustomerNewContactFormComponent,
    CustomerAddComponent,
    CustomerEditComponent,
    CustomerMiniFormComponent,
    CustomerFormComponent,
    CustomerWhitelistComponent,
    CustomerTransferComponent
  ],
    imports: [
      CommonModule,
      SharedModule,
      NgxMaskModule,
      MapViewModule,
      AvatarModule,
      AdditionalDataFormModule,
      AdditionalPhoneNumbersModule,
    ],
  exports: [
    CustomerNewContactFormComponent,
    CustomerAddComponent,
    CustomerEditComponent,
    CustomerMiniFormComponent,
    CustomerFormComponent,
    CustomerTransferComponent
  ]
})
export class CustomerSliderModule { }
