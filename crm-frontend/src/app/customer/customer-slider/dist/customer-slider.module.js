"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CustomerSliderModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var shared_module_1 = require("../../shared/shared.module");
var customer_add_component_1 = require("./customer-add/customer-add.component");
var ngx_mask_1 = require("ngx-mask");
var map_view_module_1 = require("../../map/map-view/map-view.module");
var customer_edit_component_1 = require("./customer-edit/customer-edit.component");
var customer_form_component_1 = require("./customer-form/customer-form.component");
var customer_mini_form_component_1 = require("./customer-form/customer-mini-form/customer-mini-form.component");
var customer_new_contact_form_component_1 = require("./customer-form/customer-new-contact-form/customer-new-contact-form.component");
var customer_whitelist_component_1 = require("./customer-whitelist/customer-whitelist.component");
var customer_transfer_component_1 = require("./customer-transfer/customer-transfer.component");
var avatar_module_1 = require("../../shared/avatar-form/avatar.module");
var CustomerSliderModule = /** @class */ (function () {
    function CustomerSliderModule() {
    }
    CustomerSliderModule = __decorate([
        core_1.NgModule({
            declarations: [
                customer_new_contact_form_component_1.CustomerNewContactFormComponent,
                customer_add_component_1.CustomerAddComponent,
                customer_edit_component_1.CustomerEditComponent,
                customer_mini_form_component_1.CustomerMiniFormComponent,
                customer_form_component_1.CustomerFormComponent,
                customer_whitelist_component_1.CustomerWhitelistComponent,
                customer_transfer_component_1.CustomerTransferComponent
            ],
            imports: [
                common_1.CommonModule,
                shared_module_1.SharedModule,
                ngx_mask_1.NgxMaskModule,
                map_view_module_1.MapViewModule,
                avatar_module_1.AvatarModule
            ],
            exports: [
                customer_new_contact_form_component_1.CustomerNewContactFormComponent,
                customer_add_component_1.CustomerAddComponent,
                customer_edit_component_1.CustomerEditComponent,
                customer_mini_form_component_1.CustomerMiniFormComponent,
                customer_form_component_1.CustomerFormComponent,
                customer_transfer_component_1.CustomerTransferComponent
            ]
        })
    ], CustomerSliderModule);
    return CustomerSliderModule;
}());
exports.CustomerSliderModule = CustomerSliderModule;
