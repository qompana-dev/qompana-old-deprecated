import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {CustomerService} from '../../customer.service';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {WebsocketService} from '../../../shared/websocket.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Whitelist} from '../../customer.model';
import {AuthService} from '../../../login/auth/auth.service';

@Component({
  selector: 'app-customer-whitelist',
  templateUrl: './customer-whitelist.component.html',
  styleUrls: ['./customer-whitelist.component.scss']
})
export class CustomerWhitelistComponent implements OnInit, OnDestroy {
  // tslint:disable-next-line:variable-name
  __nip: string;

  private componentDestroyed: Subject<void> = new Subject();
  whitelist: Whitelist;

  @Input('nip')
  set nip(nip: string) {
    this.__nip = nip;
    this.init();
  }

  @Input() hideTitle = false;

  constructor(private customerService: CustomerService,
              private authService: AuthService,
              private notificationService: NotificationService,
              private websocketService: WebsocketService) {
  }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  refreshWhitelistStatus(): void {
    if (this.__nip) {
      this.customerService.refreshWhiteListStatus(this.__nip)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(null, () => this.notificationService
          .notify('customer.whitelist.notification.refreshWhitelistStatusError', NotificationType.ERROR));
    }
  }

  init(): void {
    if (this.authService.isPermissionPresent('WhitelistStatusInCustomer.view', 'i')) {
      return;
    }
    this.componentDestroyed.next();

    if (this.__nip) {
      this.getWhiteList();
      this.subscribeWebsocketWhitelistStatusChange();
    } else {
      this.whitelist = undefined;
    }
  }

  subscribeWebsocketWhitelistStatusChange(): void {
    this.websocketService.listenCustomerWhitelistStateChanged(this.__nip)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.getWhiteList());
  }

  getWhiteList(): void {
    this.customerService.getWhiteListStatus(this.__nip)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((whitelist: Whitelist) => this.whitelist = whitelist,
        () => this.notificationService
          .notify('customer.whitelist.notification.getWhitelistStatusError', NotificationType.ERROR));
  }
}
