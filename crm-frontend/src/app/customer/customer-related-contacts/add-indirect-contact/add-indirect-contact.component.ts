import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {AddCustomerContactRelation, ContactAndCustomerNameDto} from '../../../contacts/contacts.model';
import {DateRangeSelectComponent} from '../../../shared/contact-customer-relation-shared/date-range-select/date-range-select.component';
import {of, Subject} from 'rxjs';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {AssignToGroupService} from '../../../shared/assign-to-group/assign-to-group.service';
import {SliderService} from '../../../shared/slider/slider.service';
import {FormUtil} from '../../../shared/form.util';
import {catchError, debounceTime, filter, switchMap, takeUntil, tap} from 'rxjs/operators';
import {CustomerService} from '../../customer.service';
import {ContactsService} from '../../../contacts/contacts.service';

@Component({
  selector: 'app-add-indirect-contact',
  templateUrl: './add-indirect-contact.component.html',
  styleUrls: ['./add-indirect-contact.component.scss']
})
export class AddIndirectContactComponent implements OnInit, OnDestroy {

  @Input()public customerId: number;
  @Input() slider: SliderComponent;
  @Output() public newRelationAdded: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild(DateRangeSelectComponent) dateSelect: DateRangeSelectComponent;


  public newRelationForm: FormGroup;
  private componentDestroyed: Subject<void> = new Subject();
  filteredContacts: ContactAndCustomerNameDto[] = [];
  private selectedCustomerName: string;

  constructor(private fb: FormBuilder,
              private customerService: CustomerService,
              private contactService: ContactsService,
              private notificationService: NotificationService,
              private assignToGroupService: AssignToGroupService,
              private sliderService: SliderService) {
    this.createContactForm();
  }

  ngOnInit(): void {
    this.subscribeCustomerSearch();
    this.listenForSliderEvents();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }


  add(): void {
    if (!this.newRelationForm.valid || !this.dateSelect.isValid()) {
      FormUtil.setTouched(this.newRelationForm);
      return;
    }
    const relatedCustomer: AddCustomerContactRelation = this.getFormData();
    this.contactService.addRelatedCustomerToContact(relatedCustomer)
      .subscribe(
        ok => {
          this.newRelationAdded.emit();
          this.sliderService.closeSlider();
          this.notifySuccess();
        },
        err => this.notifyError()
      );
  }

  cancel(): void {
    this.sliderService.closeSlider();
  }

  newContactAdded(newContact: { id: number, name: string, surname: string, customerId?: number }): void {
    this.selectedCustomerName = `${newContact.name} ${newContact.surname}`;
    this.contactSearchControl.patchValue(`${newContact.name} ${newContact.surname}`);
    this.contactIdControl.patchValue(newContact.id);
    if (newContact.customerId) {
      this.customerService.getCustomerName(newContact.customerId).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(name => this.customerNameControl.patchValue(name));
    }
  }


// @formatter:off
  get contactNameControl(): AbstractControl {return this.newRelationForm.get('contactName'); }
  get contactSearchControl(): AbstractControl {return this.newRelationForm.get('contactSearch'); }
  get contactIdControl(): AbstractControl {return this.newRelationForm.get('contactId'); }
  get customerNameControl(): AbstractControl {return this.newRelationForm.get('customerName'); }
  get positionControl(): AbstractControl {return this.newRelationForm.get('position'); }
  get noteControl(): AbstractControl { return this.newRelationForm.get('note'); }
  // @formatter:on


  private createContactForm(): void {
    this.newRelationForm = this.fb.group({
      contactName: [{value: null, disabled: true}],
      contactSearch: [null, [Validators.required, this.getCustomCustomerValueValidator()]],
      contactId: [null],
      customerName: [{value: null, disabled: true}],
      position: [null, Validators.maxLength(200)],
      note: [null]
    });
  }

  contactSelect(dto: ContactAndCustomerNameDto): void {
    if (!dto) {
      return;
    }
    this.selectedCustomerName = dto.contactName;
    this.contactIdControl.patchValue(dto.id);
    this.contactSearchControl.patchValue(dto.contactName);
    this.customerNameControl.patchValue(dto.customerName);
  }

  private getCustomCustomerValueValidator(): (control: AbstractControl) => ValidationErrors {
    return (control: AbstractControl): ValidationErrors => {
      return control.touched && control.value && control.value !== this.selectedCustomerName ? {customCustomerValue: true} : null;
    };
  }

  private subscribeCustomerSearch(): void {
    this.contactSearchControl.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      filter((input: string) => !!input && input.length >= 1),
      tap(() => {
        this.filteredContacts = [];
      }),
      switchMap(((input: string) => this.contactService.searchContacts(input)
        .pipe(
          catchError(() => of([])),
        ))
      )).subscribe(
      (result: ContactAndCustomerNameDto[]) => {
        this.filteredContacts = result;
      }
    );
  }

  private updateNewRelationForm(name: string): void {
    if (this.newRelationForm) {
      this.contactNameControl.patchValue(`${name}`);
    }
  }

  private getFormData(): AddCustomerContactRelation {
    return {
      contactId: this.contactIdControl.value,
      customerId: this.customerId,
      position: this.positionControl.value,
      startDate: this.dateSelect.getStartDate(),
      endDate: this.dateSelect.getEndDate(),
      note: this.noteControl.value
    };
  }

  private notifySuccess(): void {
    this.notificationService.notify(
      'customer.relatedContacts.addIndirectContact.notification.linkContactWithCustomerSuccess', NotificationType.SUCCESS);
  }

  private notifyError(): void {
    this.notificationService.notify(
      'customer.relatedContacts.addIndirectContact.notification.linkContactWithCustomerError', NotificationType.ERROR);
  }

  private getCustomerName(customerId: number): void {
    if (customerId == null) {
      return;
    }
    this.customerService.getCustomerName(customerId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      name => this.updateNewRelationForm(name),
      err => this.notificationService.notify(
        'customer.relatedContacts.addIndirectContact.notification.getCustomerNameError', NotificationType.ERROR)
    );
  }

  private listenForSliderEvents(): void {
    this.slider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.newRelationForm.reset();
        this.dateSelect.reset();
        this.filteredContacts = [];
      }
    );

    this.slider.openEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.getCustomerName(this.customerId)
    );
  }
}
