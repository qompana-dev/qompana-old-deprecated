import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {ContactFormComponent} from '../../../contacts/contact-slider/contact-form/contact-form.component';
import {DateRangeSelectComponent} from '../../../shared/contact-customer-relation-shared/date-range-select/date-range-select.component';
import {FormControl} from '@angular/forms';
import {CrmObject, CrmObjectType} from '../../../shared/model/search.model';
import {Subject} from 'rxjs';
import {ContactsService} from '../../../contacts/contacts.service';
import {CustomerService} from '../../customer.service';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {ContactWithRelation, Customer} from '../../customer.model';
import {takeUntil} from 'rxjs/operators';
import {applyPermission} from '../../../shared/permissions-utils';
import {AuthService} from '../../../login/auth/auth.service';

@Component({
  selector: 'app-edit-direct-contact',
  templateUrl: './edit-direct-contact.component.html',
  styleUrls: ['./edit-direct-contact.component.scss']
})
export class EditDirectContactComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() slider: SliderComponent;
  @Input() customerId: number;
  @Input() contactId: number;
  @Input() edit = false;
  @Output() public newContactAdded: EventEmitter<void> = new EventEmitter<void>();


  @ViewChild(ContactFormComponent) private contactFormComponent: ContactFormComponent;
  @ViewChild(DateRangeSelectComponent) dateSelect: DateRangeSelectComponent;
  note = new FormControl('');
  startDate = '';
  customer: CrmObject;
  private componentDestroyed: Subject<void> = new Subject();
  contact: ContactWithRelation;
  relationSectionVisible = true;


  constructor(private contactService: ContactsService,
              private customerService: CustomerService,
              private notificationService: NotificationService,
              private authService: AuthService) {
  }



  ngOnInit(): void {
    this.listenForSliderEvents();
  }

  ngAfterViewInit(): void {
    applyPermission(this.note, !this.authService.isPermissionPresent(`ContactEditComponent.note`, 'r'));
    this.dateSelect.enable(!this.authService.isPermissionPresent(`ContactEditComponent.relationTime`, 'r'));
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }


  cancel(): void {
    this.slider.close();
  }

  update(): void {
    if (!this.contactFormComponent.isFormValid() || !this.dateSelect.isValid()) {
      return;
    }
    const contactWithRelation = this.getContactWithRelation();
    this.save(contactWithRelation);
  }


  private getContactWithRelation(): Partial<ContactWithRelation> {
    const contact: Partial<ContactWithRelation> = this.contactFormComponent.getContact();
    contact.startDate = this.dateSelect.getStartDate();
    contact.endDate = this.dateSelect.getEndDate();
    contact.note = this.note.value;
    return contact;
  }

  private save(contact: Partial<ContactWithRelation>): void {
    this.contactService.updateContactWithRelation(this.contactId, contact)
      .subscribe(
        () => {
          this.notificationService.notify(
            'customer.relatedContacts.updateDirectContact.notification.updateContactSuccess', NotificationType.SUCCESS);
          this.newContactAdded.emit();
          this.slider.close();
        },
        () => this.notificationService.notify(
          'customer.relatedContacts.updateDirectContact.notification.updateContactError', NotificationType.ERROR)
      );
  }

  private getCustomerName(customerId: number): void {
    if (customerId == null) {
      return;
    }
    this.customerService.getCustomerName(customerId).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      name => (this.customer = {id: customerId, label: name, type: CrmObjectType.customer}),
      err => this.notificationService.notify(
        'customer.relatedContacts.addDirectContact.notification.getCustomerNameError', NotificationType.ERROR)
    );
  }

  private listenForSliderEvents(): void {
    this.slider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.resetForm()
    );

    this.slider.openEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.getCustomerName(this.customerId);
        this.getContactForUpdate();
      }
    );
  }

  private resetForm(): void {
    this.note.reset();
    this.contactFormComponent.reset();
    this.dateSelect.reset();
    this.startDate = null;
  }

  private getContactForUpdate(): void {
    if (this.contactId == null) {
      return;
    }
    this.contactService.getContactWithRelation(this.contactId)
      .subscribe(
        (contact) => {
          contact.customer = {id: contact.customerId} as Customer;
          this.relationSectionVisible = contact.customerId !== null;
          this.contact = contact;
          this.startDate = contact.startDate;
          this.note.patchValue(contact.note);
        },
        () => this.notificationService.notify(
          'customer.relatedContacts.updateDirectContact.notification.getContactError', NotificationType.ERROR)
      );
  }

  customerIdChanged($event: number): void {
    this.relationSectionVisible = $event != null;
  }

}
