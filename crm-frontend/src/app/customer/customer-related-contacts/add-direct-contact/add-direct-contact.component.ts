import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {ContactFormComponent} from '../../../contacts/contact-slider/contact-form/contact-form.component';
import {DateRangeSelectComponent} from '../../../shared/contact-customer-relation-shared/date-range-select/date-range-select.component';
import {ContactWithRelation} from '../../customer.model';
import {ContactsService} from '../../../contacts/contacts.service';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {CustomerService} from '../../customer.service';
import {CrmObject, CrmObjectType} from '../../../shared/model/search.model';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {applyPermission} from '../../../shared/permissions-utils';
import {AuthService} from '../../../login/auth/auth.service';

@Component({
  selector: 'app-add-direct-contact',
  templateUrl: './add-direct-contact.component.html',
  styleUrls: ['./add-direct-contact.component.scss']
})
export class AddDirectContactComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() slider: SliderComponent;
  @Input() customerId: number;
  @Output() public newContactAdded: EventEmitter<void> = new EventEmitter<void>();


  @ViewChild(ContactFormComponent) private contactFormComponent: ContactFormComponent;
  @ViewChild(DateRangeSelectComponent) dateSelect: DateRangeSelectComponent;
  note = new FormControl('');
  customer: CrmObject;
  private componentDestroyed: Subject<void> = new Subject();
  relationSectionVisible = true;


  constructor(private contactService: ContactsService,
              private customerService: CustomerService,
              private notificationService: NotificationService,
              private authService: AuthService) {

  }

  ngOnInit(): void {
    this.listenForSliderEvents();
  }

  ngAfterViewInit(): void {
    applyPermission(this.note, !this.authService.isPermissionPresent(`ContactAddComponent.note`, 'r'));
    this.dateSelect.enable(!this.authService.isPermissionPresent(`ContactAddComponent.relationTime`, 'r'));
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }


  cancel(): void {
    this.slider.close();
  }

  add(): void {
    if (!this.contactFormComponent.isFormValid() || (this.dateSelect && !this.dateSelect.isValid())) {
      return;
    }
    const contactWithRelation = this.getContactWithRelation();
    this.save(contactWithRelation);
  }


  private getContactWithRelation(): Partial<ContactWithRelation> {
    const contact: Partial<ContactWithRelation> = this.contactFormComponent.getContact();
    if (this.dateSelect) {
      contact.startDate = this.dateSelect.getStartDate();
      contact.endDate = this.dateSelect.getEndDate();
      contact.note = this.note.value;
    }
    return contact;
  }

  private save(contact: Partial<ContactWithRelation>): void {
    this.contactService.createContactWithRelation(contact)
      .subscribe(
        () => {
          this.notificationService.notify(
            'customer.relatedContacts.addDirectContact.notification.contactCreateSuccess', NotificationType.SUCCESS);
          this.newContactAdded.emit();
          this.slider.close();
        },
        () => this.notificationService.notify(
          'customer.relatedContacts.addDirectContact.notification.contactCreateError', NotificationType.ERROR)
      );
  }

  private getCustomerName(customerId: number): void {
    if (customerId == null) {
      return;
    }
    this.customerService.getCustomerName(customerId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      name => (this.customer = {id: customerId, label: name, type: CrmObjectType.customer}),
      err => this.notificationService.notify(
        'customer.relatedContacts.addDirectContact.notification.getCustomerNameError', NotificationType.ERROR)
    );
  }

  private listenForSliderEvents(): void {
    this.slider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.resetForm()
    );

    this.slider.openEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.getCustomerName(this.customerId);
      }
    );
  }

  private resetForm(): void {
    this.note.reset();
    this.contactFormComponent.reset();
    if (this.dateSelect) {
      this.dateSelect.reset();
    }
  }


  customerIdChanged($event: number): void {
    this.relationSectionVisible = $event != null;
  }
}
