import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';
import {ContactCustomerRelation, ContactRelationFormModel, UpdateRelatedCustomer} from '../../contacts/contacts.model';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {TranslateService} from '@ngx-translate/core';
import {FormSaveService} from '../../shared/form/form-save.service';
import {DeleteDialogService} from '../../shared/dialog/delete/delete-dialog.service';
import {DateUtil} from '../../shared/util/date.util';
import {DeleteDialogData} from '../../shared/dialog/delete/delete-dialog.component';
import {filter, map, takeUntil} from 'rxjs/operators';
import {DurationValidatorsUtil} from '../../shared/util/duration-validators.util';
import {CustomerService} from '../customer.service';
import {ContactsService} from '../../contacts/contacts.service';
import cloneDeep from 'lodash/cloneDeep';
import * as isEqual from 'lodash/isEqual';
import {SliderComponent} from '../../shared/slider/slider.component';
import {AuthService} from '../../login/auth/auth.service';

@Component({
  selector: 'app-customer-related-contacts',
  templateUrl: './customer-related-contacts.component.html',
  styleUrls: ['./customer-related-contacts.component.scss']
})
export class CustomerRelatedContactsComponent implements OnInit, OnDestroy {

  @Input() set customerId(customerId: number) {
    this._customerId = customerId;
    if (customerId) {
      this.getRelatedContacts();
    }
  }


  public _customerId: number;

  public relatedCustomersForm: FormGroup;
  private componentDestroyed: Subject<void> = new Subject();
  public relatedCustomers: ContactCustomerRelation[];
  public originalCustomersForm: any = null;
  private originalRelatedCustomers: ContactCustomerRelation[];
  private componentId = 'app-customer-related-contacts';
  @ViewChild('addDirectContactLinkSlider') addDirectContactLinkSlider: SliderComponent;
  @ViewChild('addIndirectContactLinkSlider') addIndirectContactLinkSlider: SliderComponent;


  constructor(private fb: FormBuilder,
              private customerService: CustomerService,
              private contactService: ContactsService,
              private notificationService: NotificationService,
              private translateService: TranslateService,
              private formSaveService: FormSaveService,
              private deleteDialogService: DeleteDialogService,
              private authService: AuthService) {
    this.createEmploymentForm();
  }

  dateUtil = DateUtil;
  monthList: { number: number, name: string }[] = DateUtil.getMonthsList();

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'customer.relatedContacts.dialog.delete.title',
    description: 'customer.relatedContacts.dialog.delete.description',
    confirmButton: 'customer.relatedContacts.dialog.delete.confirm',
    cancelButton: 'customer.relatedContacts.dialog.delete.cancel',
  };

  // @formatter:off
  get customersFormArray(): FormArray { return this.relatedCustomersForm.get('customers') as FormArray; }
  // @formatter:on

  ngOnInit(): void {
    this.subscribeForFormSaved();
    this.subscribeForFormCanceled();
    this.announceFormUnchanged();
  }


  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }


  private createEmploymentForm(): void {
    this.relatedCustomersForm = new FormGroup({
      customers: this.fb.array([]),
    });
    this.subscribeForValueChangeInForm();
  }

  private subscribeForValueChangeInForm(): void {
    this.relatedCustomersForm.valueChanges.pipe(
      filter((value) => !!this.originalCustomersForm),
      map(value => isEqual(this.originalCustomersForm, value)),
    ).subscribe(
      isTheSame => isTheSame ?
        this.formSaveService.announceFormTheSame(this.componentId) :
        this.formSaveService.announceFormChanged(this.componentId)
    );
  }

  private createSingleCustomerForm(relatedCustomer: Partial<ContactCustomerRelation> = {}): FormGroup {
    const formGroup = this.fb.group({
      relationId: [relatedCustomer.id],
      relationType: [{value: this.getRelationTypeTranslation(relatedCustomer.relationType), disabled: true}],
      position: [{value: relatedCustomer.position, disabled: this.isDirectRelation(relatedCustomer)}],
      contact: [{value: relatedCustomer.contactName, disabled: true}],
      startDateMonth: [DateUtil.getMonthNumber(relatedCustomer.startDate)],
      startDateYear: [DateUtil.getYear(relatedCustomer.startDate)],
      endDateMonth: [{value: DateUtil.getMonthNumber(relatedCustomer.endDate), disabled: relatedCustomer.active}],
      endDateYear: [{value: DateUtil.getYear(relatedCustomer.endDate), disabled: relatedCustomer.active}],
      note: [relatedCustomer.note],
      isClosed: [{value: !!relatedCustomer.endDate, disabled: this.isDirectRelation(relatedCustomer)}],
    }, {
      validators: [
        DurationValidatorsUtil.getDateChronologyValidator(),
        DurationValidatorsUtil.getDependentFieldValidatorValidator('startDateMonth', 'startDateYear'),
        DurationValidatorsUtil.getDependentFieldValidatorValidator('endDateMonth', 'endDateYear'),
      ]
    });
    if (!this.authService.isPermissionPresent(`RelatedContactsInCustomerDetail.edit`, 'w')) {
      formGroup.disable();
    }
    formGroup.get('endDateYear').valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      filter(value => !value)
    ).subscribe(
      () => formGroup.get('endDateMonth').patchValue(null)
    );
    return formGroup;
  }

  private isDirectRelation(relatedCustomer: Partial<ContactCustomerRelation>): boolean {
    return relatedCustomer.relationType === 'DIRECT';
  }

  private getRelationTypeTranslation(relationType: string): string {
    return relationType && this.translateService.instant(`customer.relatedContacts.relationTypes.${relationType}`);
  }

  public getRelatedContacts(): void {
    this.originalCustomersForm = null;
    this.customerService.getContactsRelatedWithCustomer(this._customerId)
      .pipe(
        takeUntil(this.componentDestroyed),
      ).subscribe(
      customers => this.saveCustomers(customers),
      () => this.notificationService.notify('customer.relatedContacts.notification.getRelatedCustomersError', NotificationType.ERROR)
    );
  }

  private updateCustomersForm(customers: ContactCustomerRelation[]): void {
    if (!customers) {
      return;
    }
    this.clearFormArray(this.customersFormArray);
    for (const customer of customers) {
      this.customersFormArray.push(this.createSingleCustomerForm(customer));
    }
    this.originalCustomersForm = cloneDeep(this.relatedCustomersForm.value);
  }

  private clearFormArray(formArray: FormArray): void {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  cancel(): void {
    this.restoreStateOfRelationForm();
    this.formSaveService.announceFormTheSame(this.componentId);
  }

  private restoreStateOfRelationForm(): void {
    this.relatedCustomers = cloneDeep(this.originalRelatedCustomers);
    this.announceFormUnchanged();
    this.updateCustomersForm(this.relatedCustomers);
  }

  save(): void {
    if (!this.relatedCustomersForm.valid) {
      return;
    }
    const formValues: ContactRelationFormModel[] = this.customersFormArray.getRawValue();
    const update: UpdateRelatedCustomer[] = this.mapToUpdateDto(formValues);
    this.contactService.updateContactCustomerRelations(update, 'customer')
      .pipe(
        takeUntil(this.componentDestroyed),
      ).subscribe(
      () => {
        this.getRelatedContacts();
        this.notifyUpdateSuccess();
      },
      () => this.notifyUpdateError()
    );
  }

  private saveCustomers(customers: ContactCustomerRelation[]): void {
    this.relatedCustomers = customers;
    this.originalRelatedCustomers = customers;
    this.announceFormUnchanged();
    this.updateCustomersForm(customers);
  }

  private subscribeForFormSaved(): void {
    this.formSaveService.formSavedSource$.pipe(
      takeUntil(this.componentDestroyed),
      filter(id => this.componentId === id)
    ).subscribe(
      () => this.save()
    );
  }

  private subscribeForFormCanceled(): void {
    this.formSaveService.formCanceledSource$.pipe(
      takeUntil(this.componentDestroyed),
      filter(id => this.componentId === id)
    ).subscribe(
      () => this.cancel()
    );
  }

  deleteRelation(linkId: number): void {
    this.contactService.deleteRelation(linkId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      () => {
        this.notifyDeleteRelationSuccess();
        this.getRelatedContacts();
      },
      () => this.deleteRelationError()
    );
  }


  isClosedRelation(i: number): boolean {
    const control = this.customersFormArray.at(i);
    return !!control.get('isClosed').value;
  }

  tryDelete(linkId: number): void {
    this.deleteDialogData.onConfirmClick = () => this.deleteRelation(linkId);
    this.openDeleteDialog();
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private notifyDeleteRelationSuccess(): void {
    this.notificationService.notify('customer.relatedContacts.notification.deleteRelationSuccess', NotificationType.SUCCESS);
  }

  private deleteRelationError(): void {
    this.notificationService.notify('customer.relatedContacts.notification.deleteRelationError', NotificationType.ERROR);
  }

  private mapToUpdateDto(formValues: ContactRelationFormModel[]): UpdateRelatedCustomer[] {
    return formValues.map(form => ({
      id: form.relationId,
      startDate: DateUtil.getStringDate(form.startDateMonth, form.startDateYear),
      endDate: DateUtil.getStringDate(form.endDateMonth, form.endDateYear),
      note: form.note,
      position: form.position
    }));
  }

  private notifyUpdateSuccess(): void {
    this.notificationService.notify('customer.relatedContacts.notification.updateRelationsSuccess', NotificationType.SUCCESS);
  }

  private notifyUpdateError(): void {
    this.notificationService.notify('customer.relatedContacts.notification.updateRelationsError', NotificationType.ERROR);
  }

  private announceFormUnchanged(): void {
    this.formSaveService.announceFormTheSame(this.componentId);
  }

  addDirectContact(): void {
    this.addDirectContactLinkSlider.open();
  }

  addIndirectContact(): void {
    this.addIndirectContactLinkSlider.open();
  }

  isOpenCloseDisabled(i: number): boolean {
    const control = this.customersFormArray.at(i);
    return !!control.get('isClosed').disabled;
  }
}
