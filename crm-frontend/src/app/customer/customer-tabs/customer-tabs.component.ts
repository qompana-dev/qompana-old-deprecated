import {Customer} from './../customer.model';

import {Component, Input} from '@angular/core';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-customer-tabs',
  templateUrl: './customer-tabs.component.html',
  styleUrls: ['./customer-tabs.component.scss']
})
export class CustomerTabsComponent {
  @Input() customerId: number = null;
  @Input() customer: Customer = null;
  @Input() customerChangedSubscription: Subject<void>;
  @Input() objectType = null;

  selectedTabIndex = 0;

}
