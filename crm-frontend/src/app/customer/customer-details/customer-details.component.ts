import { Component, OnDestroy, OnInit } from "@angular/core";
import { takeUntil } from "rxjs/operators";
import { CustomerWithContactsDto } from "../customer.model";
import { CustomerService } from "../customer.service";
import { ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs";
import { PersonService } from "../../person/person.service";
import {
  NotificationService,
  NotificationType,
} from "../../shared/notification.service";
import { CrmObject, CrmObjectType } from "../../shared/model/search.model";

@Component({
  selector: "app-customer-details",
  templateUrl: "./customer-details.component.html",
  styleUrls: ["./customer-details.component.scss"],
})
export class CustomerDetailsComponent implements OnInit, OnDestroy {
  customerId: number;
  customer: CustomerWithContactsDto;

  private componentDestroyed: Subject<void> = new Subject();
  customerChangedSubscription: Subject<void> = new Subject();

  objectType = CrmObjectType;
  crmObject: CrmObject;

  loadingCustomer = false;
  userMap: Map<number, string>;


  constructor(
    private customerService: CustomerService,
    private personService: PersonService,
    private notificationService: NotificationService,
    public route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.getAccountEmails();
    this.route.params
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((params) => {
        this.customerId = +params["id"];
        this.crmObject = { id: this.customerId, type: CrmObjectType.customer };
        this.getCustomer(this.customerId);
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private getAccountEmails(): void {
    this.personService
      .getIdNameMap()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (users) => (this.userMap = users),
        () =>
          this.notificationService.notify(
            "contacts.details.notifications.accountEmailsError",
            NotificationType.ERROR
          )
      );
  }

  public getCustomer(customerId: number): void {
    this.loadingCustomer = true;

    this.customerService
      .getCustomerWithContacts(customerId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (customer: CustomerWithContactsDto) => {
          this.customer = customer;
        },
        () =>
          this.notificationService.notify(
            "customer.dashboard.contacts.notification.customerNotFoundError",
            NotificationType.ERROR
          ),
        () => (this.loadingCustomer = false)
      );
  }

  submit(): void {}

  public handleCustomerChangedSubscription(): void {
    this.customerChangedSubscription.next();
  }

  public refresh(): void {
    this.getCustomer(this.customerId);
  }
}
