import { CrmObject, CrmObjectType } from './../../../shared/model/search.model';
import { CustomerDialogsConfig } from './../../customer-list/customer-list.dialogs';
import { Router } from '@angular/router';
import {
  ERROR_TYPES_FOR_MODIFICATION,
  DELETE_CUSTOMER_ERROR,
  ERROR_TYPES,
} from './customer-details-panel.config';
import {debounceTime, takeUntil} from 'rxjs/operators';
import {
  NotificationService,
  NotificationType,
} from './../../../shared/notification.service';
import { CustomerService } from './../../customer.service';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Component, OnDestroy, OnInit, Input, ViewChild } from '@angular/core';
import {
  ContactDto,
  CustomerWithContactsDto,
  Customer,
  Whitelist,
} from './../../customer.model';
import { Subject } from 'rxjs';
import { FormValidators } from 'src/app/shared/form/form-validators';
import { SliderComponent } from 'src/app/shared/slider/slider.component';
import { ConfirmDialogService } from 'src/app/shared/dialog/confirm-dialog/confirm-dialog.service';
import { TranslateService } from '@ngx-translate/core';
import { PersonService } from 'src/app/person/person.service';
import { AccountEmail } from 'src/app/person/person.model';
import {PhoneService} from '../../../shared/phone-fields/phone.service';
import {DictionaryId, DictionaryWord, SaveDictionaryWord} from '../../../dictionary/dictionary.model';
import {DictionaryStoreService} from '../../../dictionary/dictionary-store.service';
import {DictionaryAddWordComponent} from '../../../dictionary/dictionary-add-word/dictionary-add-word.component';
import {MatAutocompleteTrigger} from '@angular/material/autocomplete';

@Component({
  selector: 'app-customer-details-panel',
  templateUrl: './customer-details-panel.component.html',
  styleUrls: ['./customer-details-panel.component.scss'],
})
export class CustomerDetailsPanelComponent implements OnInit, OnDestroy {
  @Input() set customer(customer: CustomerWithContactsDto) {
    this.parseCustomer(customer);
  }
  @Input() loading: boolean;

  @Input() refresh: () => void;

  @ViewChild('editSlider') editSlider: SliderComponent;

  @ViewChild('addIndustrySlider') addIndustrySlider: SliderComponent;
  @ViewChild('addIndustryWord') addIndustryWord: DictionaryAddWordComponent;

  readonly undefinedCategoryId = -1000;
  readonly undefinedCategoryObject = {
    id: this.undefinedCategoryId,
    name: 'Bez kategorii'
  };

  private readonly dialogsConfig = new CustomerDialogsConfig(
    this.translateService
  );

  baseCustomer: CustomerWithContactsDto;
  customerCrm: CrmObject;
  customerForm: FormGroup;
  whitelist: Whitelist;
  contacts: ContactDto[] = [];
  additionalData: any[] = [];
  dictionaryId = DictionaryId;
  allIndustry: DictionaryWord[];

  private changedFields = {};

  showActionButtons = false;

  private componentDestroyed: Subject<void> = new Subject();
  readonly authKey = 'CustomerEditComponent.';

  accountEmails: AccountEmail[];

  get avatar(): AbstractControl {
    return this.customerForm.get('avatar');
  }
  get nip(): AbstractControl {
    return this.customerForm.get('nip');
  }
  get phone(): AbstractControl {
    return this.customerForm.get('phone');
  }
  get owner(): AbstractControl {
    return this.customerForm.get('owner');
  }
  get street(): AbstractControl {
    return this.customerForm.get('address.street');
  }
  get city(): AbstractControl {
    return this.customerForm.get('address.city');
  }
  get zipCode(): AbstractControl {
    return this.customerForm.get('address.zipCode');
  }
  get country(): AbstractControl {
    return this.customerForm.get('address.country');
  }
  get employeesNumber(): AbstractControl {
    return this.customerForm.get('employeesNumber');
  }
  get industry(): AbstractControl {
    return this.customerForm.get('industry');
  }
  get industrySearch(): AbstractControl {
    return this.customerForm.get('industrySearch');
  }
  get income(): AbstractControl {
    return this.customerForm.get('income');
  }
  get name(): AbstractControl {
    return this.customerForm.get('name');
  }
  get website(): AbstractControl {
    return this.customerForm.get('website');
  }
  get linkedIn(): AbstractControl {
    return this.customerForm.get('linkedIn');
  }
  get address(): AbstractControl {
    return this.customerForm.get('address');
  }

  constructor(
    private fb: FormBuilder,
    private customerService: CustomerService,
    private confirmDialogService: ConfirmDialogService,
    private notificationService: NotificationService,
    private $router: Router,
    private translateService: TranslateService,
    private personService: PersonService,
    public phoneService: PhoneService,
    private dictionaryStoreService: DictionaryStoreService
  ) {
    this.createCustomerForm();
  }

  ngOnInit(): void {
    this.getAccountEmails();
    this.subscribeForIndustrySearch();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private parseCustomer(customer: CustomerWithContactsDto): void {
    this.resetChanged();
    this.baseCustomer = customer;
    this.customerCrm = this.customerToCrmObject(customer);
    customer.address = customer.address || {} as any;
    this.customerForm.reset(customer);
    this.contacts = [...(customer.contacts || [])];
    this.additionalData = customer.additionalData ? JSON.parse(customer.additionalData) : [];
    this.getIndustryForMe([]);
    this.checkIfNipExistsInDb();
  }

  public customerToCrmObject(
    customer: Customer | { id: number; name: string }
  ): CrmObject {
    if (!customer) {
      return undefined;
    }
    return {
      id: customer.id,
      type: CrmObjectType.customer,
      label: customer.name,
    };
  }

  private createCustomerForm(): void {
    this.customerForm = this.fb.group({
      avatar: [''],
      name: [
        '',
        Validators.compose([
          Validators.required,
          FormValidators.whitespace,
          Validators.maxLength(200),
        ]),
      ],
      website: ['', Validators.maxLength(200)],
      linkedIn: ['', Validators.maxLength(200)],
      foreignCompany: [false],
      activeClient: [false],
      activeDelivery: [false],
      nip: [
        '',
        Validators.compose([
          Validators.required,
          FormValidators.whitespace,
          Validators.minLength(10),
        ]),
      ],
      phone: ['', Validators.maxLength(128)],
      owner: [undefined, [Validators.required]],
      address: this.fb.group({
        street: ['', Validators.maxLength(200)],
        city: ['', Validators.maxLength(100)],
        zipCode: ['', Validators.minLength(5)],
        country: ['', Validators.maxLength(100)],
      }),
      contacts: this.fb.array([]),
      description: ['', Validators.maxLength(500)],
      firstToContact: [undefined],
      employeesNumber: ['', Validators.maxLength(18)],
      industrySearch: [null],
      industry: [null],
      income: ['', Validators.maxLength(100)],
      additionalData: [null],
    });
  }

  submit(): void {
    const saveCustomer = {
      ...this.baseCustomer,
      ...this.customerForm.value,
      address: {
        ...this.baseCustomer.address,
        ...this.customerForm.value.address,
      },
    };

    this.customerService
      .modifyCustomer(saveCustomer)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => this.handleSuccess(),
        (error) =>
          this.notificationService.notifyError(
            ERROR_TYPES_FOR_MODIFICATION,
            error.status
          )
      );
  }

  private openDeleteDialog(dialogData: object = {}): void {
    this.confirmDialogService.open(dialogData);
  }

  public tryDeleteCustomer(): void {
    const dialogData = this.dialogsConfig.deleteCustomer(
      this.baseCustomer as Customer,
      this.deleteCustomer.bind(this, [this.baseCustomer.id])
    );

    this.openDeleteDialog(dialogData);
  }

  private deleteCustomer(customerId: number): void {
    this.customerService
      .removeCustomer(customerId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.$router.navigate(['/customers']);
          this.notificationService.notify(
            'customer.list.notification.removed',
            NotificationType.SUCCESS
          );
        },
        (err) =>
          this.notificationService.notifyError(
            DELETE_CUSTOMER_ERROR,
            err.status
          )
      );
  }

  private handleSuccess(): void {
    this.notificationService.notify(
      'customer.form.notification.edited',
      NotificationType.SUCCESS
    );
    this.refresh();
  }

  handleAvatarChange(event: any): void {
    const file = event.dataTransfer
      ? event.dataTransfer.files[0]
      : event.target.files[0];
    const reader = new FileReader();
    if (!file.type.match(/image-*/)) {
      console.error('invalid format');
      return;
    }
    reader.onload = (e: any) => {
      const imageSrc = e.target.result;
      this.avatar.patchValue(imageSrc);
      this.fieldIsChanged('avatar');
    };
    reader.readAsDataURL(file);
  }

  fieldIsChanged(field: string): void {
    this.changedFields[field] =
      JSON.stringify(this.baseCustomer[field]) !==
      JSON.stringify(this.customerForm.get(field).value);
    this.checkChanged();
  }

  checkChanged(): void {
    const changed = Object.values(this.changedFields).some((v) => !!v);
    this.showActionButtons = changed;
  }

  private resetChanged(): void {
    this.changedFields = {};
    this.checkChanged();
  }

  private resetForm(): void {
    this.customerForm.reset(this.baseCustomer);
  }

  resetCustomer(): void {
    this.resetForm();
    this.contacts = [...(this.baseCustomer.contacts || [])];
    this.resetChanged();
  }

  checkIfNipExistsInDb(): void {
    if (this.nip.value && this.nip.value.length === 10) {
      this.customerService
        .getCustomerByNip(this.nip.value, this.baseCustomer.id)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((val: string) => {
          if (val === '0') {
            this.getWhiteList();
          } else {
            this.nip.setErrors({ nipExists: true });
          }
        });
    }
  }

  getDataFromGUS($event: MouseEvent): void {
    $event.stopPropagation();
    if (this.nip.valid) {
      this.customerService
        .getDataFromGUS(this.nip.value, this.baseCustomer.id)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (customer: CustomerWithContactsDto) =>
            this.handleSuccessfulImport(customer),
          (error) =>
            this.notificationService.notifyError(ERROR_TYPES, error.status)
        );
    } else {
      this.notificationService.notify(
        'customer.form.nipMinLength',
        NotificationType.ERROR
      );
    }
  }

  getWhiteList(): void {
    this.customerService
      .getWhiteListStatus(this.nip.value)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (whitelist: Whitelist) => (this.whitelist = whitelist),
        () =>
          this.notificationService.notify(
            'customer.whitelist.notification.getWhitelistStatusError',
            NotificationType.ERROR
          )
      );
  }

  private getAccountEmails(): void {
    this.personService
      .getAccountEmails()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (emails) => {
          this.accountEmails = emails;
        },
        () => this.notificationService.notify('customer.form.notification.accountEmailsError', NotificationType.ERROR)
      );
  }

  private handleSuccessfulImport(customer: CustomerWithContactsDto): void {
    this.updateCustomerForm(customer);
    this.notificationService.notify(
      'customer.form.notification.successImportFromGus',
      NotificationType.SUCCESS
    );
  }

  private updateCustomerForm(customer: CustomerWithContactsDto): void {
    const fields = ['name', 'phone', 'website', 'nip', 'address', 'linkedIn'];
    if (this.customerForm && customer) {
      fields.forEach((field) => {
        if (customer[field]) {
          this[field].patchValue(customer[field]);
          this.fieldIsChanged(field);
        }
      });
    }
  }

  private subscribeForIndustrySearch(): void {
    this.industrySearch.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250)
    ).subscribe(
      (input: string) => {
        if (!input || input === '') {
          this.industry.patchValue(null);
        }
      }
    );
  }

  industrySelect(industry: DictionaryWord): void {
    if (industry) {
      this.industry.patchValue(industry.id);
      this.industrySearch.patchValue(industry.name);
      this.fieldIsChanged('industry');
    }
  }

  private getIndustryForMe(privateWordsIds: number[]): void {
    this.dictionaryStoreService
      .getDictionaryIndustry(this.componentDestroyed, privateWordsIds)
      .subscribe(
        words => {
          this.allIndustry = words;
          if (this.industry && this.industry.value ) {
            this.industrySelect(this.allIndustry.find(word => word.id === this.industry.value));
          }
        },
        () => this.notificationService.notify('customer.form.notification.companyTypeError', NotificationType.ERROR)
      );
  }


  openAddIndustrySlider(trigger: MatAutocompleteTrigger): void {
    trigger.closePanel();
    this.addIndustrySlider.open();
  }

  onCancelAddIndustrySlider(): void {
    this.addIndustrySlider.close();
  }

  onSubmitAddIndustrySlider(savedWord: SaveDictionaryWord): void {
    this.allIndustry.unshift({...savedWord, children: []});
    const dictWord: DictionaryWord = {...savedWord, children: []};
    this.industrySelect(dictWord);
    this.addIndustrySlider.close();
  }
}
