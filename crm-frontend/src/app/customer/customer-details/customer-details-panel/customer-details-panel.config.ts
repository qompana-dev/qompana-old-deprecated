import { BAD_REQUEST, NOT_FOUND, CONFLICT, GONE } from "http-status-codes";
import { ErrorTypes } from "./../../../shared/notification.service";

export const ERROR_TYPES_FOR_MODIFICATION: ErrorTypes = {
  base: "customer.form.notification.",
  defaultText: "unknownError",
  errors: [
    {
      code: BAD_REQUEST,
      text: "validationError",
    },
    {
      code: NOT_FOUND,
      text: "notFound",
    },
    {
      code: CONFLICT,
      text: "nameOrNipOccupied",
    },
  ],
};

export const ERROR_TYPES: ErrorTypes = {
  base: "customer.form.notification.",
  defaultText: "unknownError",
  errors: [
    {
      code: NOT_FOUND,
      text: "customerNotFoundInGus",
    },
    {
      code: BAD_REQUEST,
      text: "badRequest",
    },
    {
      code: GONE,
      text: "errorInGus",
    },
  ],
};

export const DELETE_CUSTOMER_ERROR: ErrorTypes = {
  base: "customer.list.notification.",
  defaultText: "undefinedError",
  errors: [
    {
      code: NOT_FOUND,
      text: "customerNotFound",
    },
    {
      code: BAD_REQUEST,
      text: "usedInOpportunity",
    },
  ],
};
