import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CustomerService} from '../customer.service';
import {EMPTY, fromEvent, interval, merge, Observable, Subject} from 'rxjs';
import {catchError, distinctUntilChanged, map, retry, startWith, switchMap, takeUntil} from 'rxjs/operators';
import {ContactDto, CustomerWithContactsDto} from '../customer.model';
import {PersonService} from '../../person/person.service';
import {DeleteDialogData} from '../../shared/dialog/delete/delete-dialog.component';
import {ContactsService} from '../../contacts/contacts.service';
import {DeleteDialogService} from '../../shared/dialog/delete/delete-dialog.service';
import {MatTable} from '@angular/material';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {AuthService} from '../../login/auth/auth.service';
import {BAD_REQUEST, NOT_FOUND} from 'http-status-codes';
import {Router} from '@angular/router';

@Component({
  selector: 'app-customer-contacts',
  templateUrl: './customer-contacts.component.html',
  styleUrls: ['./customer-contacts.component.scss']
})
export class CustomerContactsComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('notesArea') notesArea: ElementRef;
  @ViewChild('table') table: MatTable<any>;
  columns = ['markMainContact', 'avatar', 'name', 'role', 'department', 'phone', 'email', 'owner', 'action'];
  filteredColumns$: Observable<string[]>;
  @Input() userMap: Map<number, string> = new Map();
  notes: string;
  _customerId: number;
  public selectedContact: ContactDto;
  selectedContactId: number;
  private componentDestroyed: Subject<void> = new Subject();
  collapsed = true;
  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'customer.dashboard.contacts.remove.title',
    description: 'customer.dashboard.contacts.remove.description',
    confirmButton: 'customer.dashboard.contacts.remove.buttonYes',
    cancelButton: 'customer.dashboard.contacts.remove.buttonNo',
  };

  readonly markContactErrors: ErrorTypes = {
    base: 'customer.dashboard.contacts.notification.',
    defaultText: 'unknownError',
    errors: [{
      code: 404,
      text: 'markContactCustomerNotFound'
    }]
  };

  readonly deleteContactErrors: ErrorTypes = {
    base: 'customer.dashboard.contacts.remove.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: 404,
        text: 'notFound'
      }, {
        code: BAD_REQUEST,
        text: 'usedInOpportunity'
      }
    ]
  };

  readonly getCustomerErrors: ErrorTypes = {
    base: 'customer.basicInfo.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'notFound'
      }
    ]
  };


  constructor(
    public customerService: CustomerService,
    private contactService: ContactsService,
    private personService: PersonService,
    private $router: Router,
    private deleteDialogService: DeleteDialogService,
    private notificationService: NotificationService,
    private authService: AuthService) {
  }

  public customer: CustomerWithContactsDto;

  @Input() set customerId(customerId: number) {
    if (customerId) {
      this._customerId = customerId;
      this.getCustomerWithContacts(customerId);
    }
  }

  private getCustomerWithContacts(customerId: number): void {
    this.customerService.getCustomerWithContacts(customerId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      (customer: CustomerWithContactsDto) => {
        this.customer = customer;
        this.notes = customer.contactsNotes;
        if (customer.contacts) {
          this.selectedContact = customer.contacts.filter(value => value.mainContact).pop();
        }
      },
      (err) => this.notificationService.notifyError(this.getCustomerErrors, err.status)
    );
  }

  ngOnInit(): void {
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  ngAfterViewInit(): void {
    this.startSavingNotes();
  }

  edit(contactId: number): void {
    this.selectedContactId = contactId;
  }

  tryDelete(contact: ContactDto): void {
    this.deleteDialogData.onConfirmClick = () => this.deleteContact(contact);
    this.openDeleteDialog();
  }

  goToContactDetails(id: number): void {
    this.$router.navigate(['/contacts', id]);
  }

  markContact(contact: ContactDto): void {
    const currentMainContact = this.selectedContact;
    this.selectedContact = contact;
    this.customerService.markCustomerContactAsMain(this.customer.id, contact.id)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      () => {
      },
      err => {
        this.notificationService.notifyError(this.markContactErrors, err.status);
        this.selectedContact = currentMainContact;
      }
    );
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private saveContactsNotes(notes: string): Observable<void> {
    if (!this.customer) {
      return EMPTY;
    }
    return this.customerService.saveContactsNotes(this.customer.id, notes)
      .pipe(
        retry(3),
        catchError(() => {
          return EMPTY;
        })
      );
  }

  private startSavingNotes(): void {
    if (!this.notesArea) {
      return;
    }
    merge(
      fromEvent(this.notesArea.nativeElement, 'focusout'),
      interval(10000)
    ).pipe(
      takeUntil(this.componentDestroyed),
      map(() => this.notes || ''),
      distinctUntilChanged(),
      switchMap(notes => this.saveContactsNotes(notes))
    ).subscribe();
  }


  private deleteContact(contact: ContactDto): void {
    this.contactService.deleteContact(contact.id).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      () => {
        this.notificationService.notify('customer.dashboard.contacts.remove.successfullyRemoved', NotificationType.SUCCESS);
        this.removeFromContactList(contact);
      },
      err => this.notificationService.notifyError(this.deleteContactErrors, err.status)
    );
  }

  private removeFromContactList(contact: ContactDto): void {
    const index = this.customer.contacts.indexOf(contact);
    if (index > -1) {
      this.customer.contacts.splice(index, 1);
      this.table.renderRows();
    }
  }

  private getFilteredColumns(): string[] {
    return this.columns
      .filter((item) => !this.authService.isPermissionPresent(`CustomerContactsComponent.${item}`, 'i') || item === 'action');
  }


  refreshContactList(): void {
    this.getCustomerWithContacts(this._customerId);
  }
}
