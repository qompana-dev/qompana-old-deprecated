import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {CustomerWithContactsDto} from '../customer.model';
import {CustomerService} from '../customer.service';
import {ErrorTypes, NotificationService} from '../../shared/notification.service';
import {Subject} from 'rxjs';
import {BAD_REQUEST, CONFLICT, NOT_FOUND} from 'http-status-codes';
import {CustomerFormComponent} from '../customer-slider/customer-form/customer-form.component';

@Component({
  selector: 'app-customer-edit-embedded',
  templateUrl: './customer-edit-embedded.component.html',
  styleUrls: ['./customer-edit-embedded.component.scss']
})
export class CustomerEditEmbeddedComponent implements OnInit, OnDestroy  {
  private componentDestroyed: Subject<void> = new Subject();
  customer: CustomerWithContactsDto;
  private id: number;

  @Output() customerChanged: EventEmitter<void> = new EventEmitter<void>();
  @Output() customerUpdated: EventEmitter<void> = new EventEmitter<void>();

  @Input() set canSave(canSave: boolean) {
    if (canSave) {
      this.submit();
    } else if (canSave === false) {
      this.customerFormComponent.revertChanges();
    }
  }

  @ViewChild(CustomerFormComponent) private customerFormComponent: CustomerFormComponent;
  @Input() set customerId(customerId: number) {
    this.id = customerId;
    if (customerId) {
      this.loadCustomer(customerId);
    }
  }

  readonly errorTypesForModification: ErrorTypes = {
    base: 'customer.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'validationError'
      }, {
        code: NOT_FOUND,
        text: 'notFound'
      }, {
        code: CONFLICT,
        text: 'nameOrNipOccupied'
      }
    ]
  };

  readonly errorTypesForGet: ErrorTypes = {
    base: 'customer.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'notFound'
      }
    ]
  };

  constructor(private customerService: CustomerService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }


  submit(): void {
    const isValid = this.customerFormComponent.isValid;
    if (isValid) {
      const customer = this.customerFormComponent.getCustomer();
      this.customerService.modifyCustomer(customer)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => {
            this.loadCustomer(this.id);
            this.customerFormComponent.saved();
            this.customerUpdated.emit();
          },
          (error) => this.notificationService.notifyError(this.errorTypesForModification, error.status)
        );
    }
  }

  private loadCustomer(customerId: number): void {
    this.customerService.getCustomerForEditById(customerId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe((customer: CustomerWithContactsDto) => this.customer = customer,
      (err) => this.notificationService.notifyError(this.errorTypesForGet, err.status));
  }
}
