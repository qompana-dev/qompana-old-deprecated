import { MatRadioModule } from '@angular/material/radio';
import { CustomerSearchComponent } from './customer-list/customer-search/customer-search.component';
import { TabsModule } from './../shared/tabs/tabs.module';
import { CustomerTabsComponent } from './customer-tabs/customer-tabs.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { SharedModule } from '../shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { MapViewModule } from '../map/map-view/map-view.module';
import { CustomerContactsComponent } from './customer-contacts/customer-contacts.component';
import { CustomerEditEmbeddedComponent } from './customer-edit-embedded/customer-edit-embedded.component';
import { FilesModule } from '../files/files.module';
import { WidgetModule } from '../widgets/widget.module';
import { CustomerSliderModule } from './customer-slider/customer-slider.module';
import { ContactCustomerRelationSharedModule } from '../shared/contact-customer-relation-shared/contact-customer-relation-shared.module';
import { ContactSliderModule } from '../contacts/contact-slider/contact-slider.module';
import { CustomerRelationsComponent } from './customer-relations/customer-relations.component';
import { AddDirectContactComponent } from './customer-related-contacts/add-direct-contact/add-direct-contact.component';
import { AddIndirectContactComponent } from './customer-related-contacts/add-indirect-contact/add-indirect-contact.component';
import { CustomerRelatedContactsComponent } from './customer-related-contacts/customer-related-contacts.component';
import { RelatedContactsPreviewComponent } from './customer-relations/related-contacts-preview/related-contacts-preview.component';
import { EditDirectContactComponent } from './customer-related-contacts/edit-direct-contact/edit-direct-contact.component';
import { NotesModule } from '../shared/tabs/notes-tab/notes.module';
import { AvatarModule } from '../shared/avatar-form/avatar.module';
import { DashboardModule } from '../dashboard/dashboard.module';
import { CustomerRelatedOpportunitiesComponent } from './customer-related-opportunities/customer-related-opportunities.component';
import { RelatedOpportunityPreviewComponent } from './customer-relations/related-opportunity-preview/related-opportunity-preview.component';
import { OpportunitySliderModule } from '../opportunity/opportunity-slider/opportunity-slider.module';
import { AdditionalPhoneNumbersModule } from '../shared/additional-phone-numbers/additional-phone-numbers.module';
import { CustomerDetailsPanelComponent } from './customer-details/customer-details-panel/customer-details-panel.component';
import {DictionaryModule} from '../dictionary/dictionary.module';

@NgModule({
  declarations: [
    CustomerListComponent,
    CustomerDetailsComponent,
    CustomerDetailsPanelComponent,
    CustomerTabsComponent,
    CustomerContactsComponent,
    CustomerEditEmbeddedComponent,
    AddDirectContactComponent,
    AddIndirectContactComponent,
    CustomerRelatedContactsComponent,
    CustomerRelationsComponent,
    RelatedContactsPreviewComponent,
    EditDirectContactComponent,
    CustomerRelatedOpportunitiesComponent,
    RelatedOpportunityPreviewComponent,
    CustomerRelatedOpportunitiesComponent,
    CustomerSearchComponent,
  ],
    imports: [
        MapViewModule,
        TabsModule,
        MatRadioModule,
        CommonModule,
        SharedModule,
        CustomerRoutingModule,
        NgxMaskModule,
        FilesModule,
        WidgetModule,
        CustomerSliderModule,
        ContactSliderModule,
        NotesModule,
        ContactCustomerRelationSharedModule,
        AvatarModule,
        DashboardModule,
        OpportunitySliderModule,
        AdditionalPhoneNumbersModule,
        DictionaryModule,
    ],
})
export class CustomerModule {}
