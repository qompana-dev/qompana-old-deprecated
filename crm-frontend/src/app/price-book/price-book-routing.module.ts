import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../login/auth/auth-guard.service';
import {PriceBookListComponent} from './price-book-list/price-book-list.component';

const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: PriceBookListComponent}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PriceBookRoutingModule { }
