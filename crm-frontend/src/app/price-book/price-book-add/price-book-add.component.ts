import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {PriceBookFormComponent} from '../price-book-form/price-book-form.component';
import {SliderService} from '../../shared/slider/slider.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {CONFLICT, FORBIDDEN} from 'http-status-codes';
import {PriceBookService} from '../price-book.service';
import {PriceBook} from '../price-book.model';

@Component({
  selector: 'app-price-book-add',
  templateUrl: './price-book-add.component.html',
  styleUrls: ['./price-book-add.component.scss']
})
export class PriceBookAddComponent implements OnInit {

  @Output() priceBookAdded: EventEmitter<void> = new EventEmitter();
  @ViewChild(PriceBookFormComponent) private priceBookFormComponent: PriceBookFormComponent;

  readonly addPriceBookError: ErrorTypes = {
    base: 'priceBook.form.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: CONFLICT,
        text: 'addingError'
      },
      {
        code: FORBIDDEN,
        text: 'noPermission'
      }
    ]
  };
  saveInProgress = false;

  constructor(private sliderService: SliderService,
              private notificationService: NotificationService,
              private priceBookService: PriceBookService) { }

  ngOnInit() {
  }

  submit(): void {
    if (!this.priceBookFormComponent.isFormValid()) {
      this.priceBookFormComponent.markAsTouched();
      return;
    }

    if (!this.saveInProgress) {
      this.saveInProgress = true;
      const priceBook: PriceBook = this.priceBookFormComponent.getPriceBook();
      this.priceBookService.createPriceBook(priceBook)
        .subscribe(
          () => this.handleAddSuccess(),
          (err) => {
            this.saveInProgress = false;
            this.notificationService.notifyError(this.addPriceBookError, err.status);
          }
        );
    }
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
    if (this.priceBookFormComponent) {
      this.priceBookFormComponent.clear();
    }
  }

  private handleAddSuccess(): void {
    this.saveInProgress = false;
    this.notificationService.notify('priceBook.form.notifications.addedSuccessfully', NotificationType.SUCCESS);
    this.priceBookAdded.emit();
    this.closeSlider();
  }
}
