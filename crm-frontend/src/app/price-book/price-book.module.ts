import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PriceBookRoutingModule} from './price-book-routing.module';
import {PriceBookListComponent} from './price-book-list/price-book-list.component';
import {SharedModule} from '../shared/shared.module';
import {PriceBookFormComponent} from './price-book-form/price-book-form.component';
import {PriceBookAddComponent} from './price-book-add/price-book-add.component';
import {PriceBookEditComponent} from './price-book-edit/price-book-edit.component';

@NgModule({
  declarations: [PriceBookListComponent, PriceBookFormComponent, PriceBookAddComponent, PriceBookEditComponent],
  imports: [
    CommonModule,
    PriceBookRoutingModule,
    SharedModule
  ]
})
export class PriceBookModule { }
