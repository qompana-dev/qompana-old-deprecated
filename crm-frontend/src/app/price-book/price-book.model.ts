export interface PriceBook {
  id: number;
  name: string;
  description: string;
  active: boolean;
  isDefault: boolean;
  currency: string;
  updated: string;
  checked?: boolean;
  empty?: boolean;
}
