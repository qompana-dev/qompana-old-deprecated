import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {PriceBookFormComponent} from '../price-book-form/price-book-form.component';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {CONFLICT, FORBIDDEN} from 'http-status-codes';
import {SliderService} from '../../shared/slider/slider.service';
import {PriceBookService} from '../price-book.service';
import {PriceBook} from '../price-book.model';

@Component({
  selector: 'app-price-book-edit',
  templateUrl: './price-book-edit.component.html',
  styleUrls: ['./price-book-edit.component.scss']
})
export class PriceBookEditComponent implements OnInit {

  @Input() priceBook: PriceBook;
  @Output() priceBookEdited: EventEmitter<void> = new EventEmitter();
  @ViewChild(PriceBookFormComponent) private priceBookFormComponent: PriceBookFormComponent;

  readonly editPriceBookError: ErrorTypes = {
    base: 'priceBook.form.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: CONFLICT,
        text: 'addingError'
      },
      {
        code: FORBIDDEN,
        text: 'noPermission'
      }
    ]
  };

  constructor(private sliderService: SliderService,
              private notificationService: NotificationService,
              private priceBookService: PriceBookService) { }

  ngOnInit() {
  }

  submit(): void {
    if (!this.priceBookFormComponent.isFormValid()) {
      this.priceBookFormComponent.markAsTouched();
      return;
    }

    const priceBook: PriceBook = this.priceBookFormComponent.getPriceBook();
    this.priceBookService.modifyPriceBook(this.priceBook.id, priceBook)
      .subscribe(
        () => this.handleModifySuccess(),
        (err) => this.notificationService.notifyError(this.editPriceBookError, err.status)
      );
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
    if (this.priceBookFormComponent) {
      this.priceBookFormComponent.clear();
    }
  }

  private handleModifySuccess(): void {
    this.notificationService.notify('priceBook.form.notifications.addedSuccessfully', NotificationType.SUCCESS);
    this.priceBookEdited.emit();
    this.closeSlider();
  }
}
