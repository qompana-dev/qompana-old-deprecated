import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FilterStrategy} from '../../shared/model';
import {Page, PageRequest} from '../../shared/pagination/page.model';
import {InfiniteScrollUtil} from '../../shared/pagination/infinite-scroll.util';
import {map, startWith, takeUntil, throttleTime} from 'rxjs/operators';
import {PriceBookService} from '../price-book.service';
import {Observable, Subject} from 'rxjs';
import {PriceBook} from '../price-book.model';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {Sort} from '@angular/material/sort';
import {AuthService} from '../../login/auth/auth.service';
import {DeleteDialogService} from '../../shared/dialog/delete/delete-dialog.service';
import {DeleteDialogData} from '../../shared/dialog/delete/delete-dialog.component';
import {FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import {SliderComponent} from '../../shared/slider/slider.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-price-book-list',
  templateUrl: './price-book-list.component.html',
  styleUrls: ['./price-book-list.component.scss']
})
export class PriceBookListComponent implements OnInit, OnDestroy {

  columns = ['selectRow', 'name', 'description', 'currency', 'updated', 'active', 'action'];
  priceBookForEdit: PriceBook;
  priceBookPage: Page<PriceBook>;
  filterSelection = FilterStrategy;
  selectedFilter: FilterStrategy = FilterStrategy.ALL;
  expanded = false;
  filteredColumns$: Observable<string[]>;
  private currentSort = 'id,asc';
  private currentPage: number;
  private componentDestroyed: Subject<void> = new Subject();
  @ViewChild('editSlider') editSlider: SliderComponent;

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'priceBook.list.dialog.remove.title',
    description: 'priceBook.list.dialog.remove.description',
    noteFirst: 'priceBook.list.dialog.remove.noteFirstPart',
    noteSecond: 'priceBook.list.dialog.remove.noteSecondPart',
    confirmButton: 'priceBook.list.dialog.remove.buttonYes',
    cancelButton: 'priceBook.list.dialog.remove.buttonNo',
  };

  readonly deletePriceBookError: ErrorTypes = {
    base: 'priceBook.list.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'priceBookNotFound'
      },
      {
        code: FORBIDDEN,
        text: 'noPermission'
      }
    ]
  };

  constructor(private priceBookService: PriceBookService,
              private router: Router,
              private notificationService: NotificationService,
              private deleteDialogService: DeleteDialogService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.initPriceBooks(this.selectedFilter);
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  public openProductList(id: number): void {
    this.router.navigate(['products', 'for-price-book', id]);
  }
  public tryDelete(id: number): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deletePriceBook(id);
    this.deleteDialogService.open(this.deleteDialogData);
  }

  public tryDeleteSelectedProducts(): void {
    this.deleteDialogData.numberToDelete = this.priceBookPage.content
      .filter(customer => !!customer.checked).length;
    this.deleteDialogData.onConfirmClick = () => this.deleteSelectedPriceBooks();
    this.deleteDialogService.open(this.deleteDialogData);
  }

  public onScroll(e: any): void {
    if (InfiniteScrollUtil.shouldGetMoreData(e) && this.priceBookPage.numberOfElements < this.priceBookPage.totalElements) {
      const nextPage = this.currentPage + 1;
      this.getMorePriceBooks({page: '' + nextPage, size: InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: this.currentSort});
    }
  }

  public onSortChange($event: Sort): void {
    this.currentSort = $event.direction ? ($event.active + ',' + $event.direction) : 'id,asc';
    this.initPriceBooks(this.selectedFilter);
  }

  public isSelected(selection: FilterStrategy): boolean {
    return this.selectedFilter === selection;
  }

  public filter(selection: FilterStrategy): void {
    this.selectedFilter = selection;
    this.initPriceBooks(selection);
  }

  handlePriceBookAdded(): void {
    this.initPriceBooks(this.selectedFilter);
  }

  private initPriceBooks(selection: FilterStrategy, pageRequest: PageRequest =
    {page: '0', size: InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: this.currentSort}): void {
    this.priceBookService.getPriceBooks(pageRequest, selection)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      page => this.handleInitSuccess(page),
      () => this.notifyError()
    );
  }

  private handleInitSuccess(page: Page<PriceBook>): void {
    this.currentPage = 0;
    this.priceBookPage = page;
  }

  private getMorePriceBooks(pageRequest: PageRequest): void {
    this.priceBookService.getPriceBooks(pageRequest, this.selectedFilter)
      .pipe(
        takeUntil(this.componentDestroyed),
        throttleTime(500)
      ).subscribe(
      page => this.handleGetMorePriceBooksSuccess(page),
      () => this.notifyError()
    );
  }

  private notifyError() {
    this.notificationService.notify('priceBook.list.notifications.undefinedError', NotificationType.ERROR);
  }

  private handleGetMorePriceBooksSuccess(page: any) {
    this.currentPage++;
    this.priceBookPage.content = this.priceBookPage.content.concat(page.content);
    this.priceBookPage.numberOfElements += page.content.length;
  }

  private getFilteredColumns(): string[] {
    return this.columns
      .filter((item) => !this.authService.isPermissionPresent('PriceBookListComponent.' + item, 'i') || item === 'action' || item === 'selectRow');
  }

  private deletePriceBook(id: number): void {
    this.priceBookService.deletePriceBook(id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.initPriceBooks(this.selectedFilter);
          this.notificationService.notify('priceBook.list.notifications.removed', NotificationType.SUCCESS);
        },
        (err) => this.notificationService.notifyError(this.deletePriceBookError, err.status)
      );
  }

  private deleteSelectedPriceBooks(): void {
    if (this.priceBookPage && this.priceBookPage.content) {
      const toDelete: PriceBook[] = this.priceBookPage.content.filter(priceBook => priceBook.checked);

      this.priceBookService.deletePriceBooks(toDelete)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => {
            this.initPriceBooks(this.selectedFilter);
            this.notificationService.notify('priceBook.list.notifications.removed', NotificationType.SUCCESS);
          },
          (err) => this.notificationService.notifyError(this.deletePriceBookError, err.status)
        );
    }
  }

  public edit(id: number): void {
    this.priceBookService.getPriceBook(id).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (p) => {
        this.priceBookForEdit = p;
        this.editSlider.open();
      },
      () => this.notificationService.notify('priceBook.list.notifications.priceBookNotFound', NotificationType.ERROR)
    );
  }

  isAnyChecked(): boolean {
    return this.priceBookPage && this.priceBookPage.content && this.priceBookPage.content.some(customer => customer.checked);
  }
}
