import {Injectable} from '@angular/core';
import {Page, PageRequest} from '../shared/pagination/page.model';
import {FilterStrategy} from '../shared/model';
import {Observable} from 'rxjs';
import {PriceBook} from './price-book.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {TableUtil} from '../shared/table.util';
import {UrlUtil} from '../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class PriceBookService {

  constructor(private http: HttpClient) { }

  public getPriceBooks(pageRequest: PageRequest, strategy: FilterStrategy): Observable<Page<PriceBook>> {
    let params = new HttpParams({fromObject: pageRequest as any});
    params = TableUtil.enrichParamsWithFilterStrategy(strategy, params);
    const url = `${UrlUtil.url}/crm-business-service/price-books`;
    return this.http.get<Page<PriceBook>>(url, {params});
  }

  public createPriceBook(priceBook: PriceBook): Observable<PriceBook> {
    const url = `${UrlUtil.url}/crm-business-service/price-books`;
    return this.http.post<PriceBook>(url, priceBook);
  }

  public deletePriceBook(id: number): Observable<void> {
    return this.http.delete<void>(`${UrlUtil.url}/crm-business-service/price-books/${id}`);
  }

  public deletePriceBooks(toDelete: PriceBook[]): Observable<void> {
    const params = new HttpParams().set('ids', toDelete.map(customer => customer.id).join(','));
    return this.http.delete<void>(`${UrlUtil.url}/crm-business-service/price-books`, {params});
  }

  public modifyPriceBook(id: number, priceBook: PriceBook): Observable<void> {
    const url = `${UrlUtil.url}/crm-business-service/price-books/${id}`;
    return this.http.put<void>(url, priceBook);
  }

  public getPriceBook(id: number): Observable<PriceBook> {
    const url = `${UrlUtil.url}/crm-business-service/price-books/${id}`;
    return this.http.get<PriceBook>(url);
  }

  public getPriceBookToAssign(): Observable<PriceBook[]> {
    return this.http.get<PriceBook[]>(`${UrlUtil.url}/crm-business-service/price-books-to-assign`);
  }
  public getPriceBookToAssignByCurrency(currency: string): Observable<PriceBook[]> {
    return this.http.get<PriceBook[]>(`${UrlUtil.url}/crm-business-service/price-books-to-assign/currency/${currency}`);
  }
}
