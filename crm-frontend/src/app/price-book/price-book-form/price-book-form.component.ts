import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {PriceBook} from '../price-book.model';
import omitBy from 'lodash/omitBy';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';
import {SliderService} from '../../shared/slider/slider.service';
import {Subject} from 'rxjs';
import {applyPermission} from '../../shared/permissions-utils';
import {AuthService} from '../../login/auth/auth.service';
import {GlobalConfigService} from '../../global-config/global-config.service';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {FormValidators} from '../../shared/form/form-validators';


@Component({
  selector: 'app-price-book-form',
  templateUrl: './price-book-form.component.html',
  styleUrls: ['./price-book-form.component.scss']
})
export class PriceBookFormComponent implements OnInit, OnDestroy, OnChanges {

  @Input() set priceBook(priceBook: PriceBook) {
    if (priceBook) {
      this.editedPriceBook = priceBook;
      this.priceBookForm.reset(priceBook);
      if (!priceBook.empty) {
        this.currency.disable();
      }
    }
  }

  editedPriceBook: PriceBook;
  priceBookForm: FormGroup;
  currencies = [];
  private componentDestroyed: Subject<void> = new Subject();

  // @formatter:off
  get name(): AbstractControl { return this.priceBookForm.get('name'); }
  get description(): AbstractControl { return this.priceBookForm.get('description'); }
  get currency(): AbstractControl { return this.priceBookForm.get('currency'); }
  get active(): AbstractControl { return this.priceBookForm.get('active'); }
  // @formatter:on

  get authKey(): string {
    return (this.editedPriceBook) ? 'PriceBookEditComponent.' : 'PriceBookAddComponent.';
  }

  constructor(private sliderService: SliderService,
              private authService: AuthService,
              private fb: FormBuilder,
              private configService: GlobalConfigService,
              private notificationService: NotificationService) {
    this.createPriceBookForm();
  }

  ngOnInit(): void {
    this.subscribeForCurrencies();
    this.subscribeForSliderClose();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.applyPermissionsToForm();
  }

  clear(): void {
    this.createPriceBookForm();
  }

  public markAsTouched(): void {
    (Object as any).values(this.priceBookForm.controls).forEach(control => {
      control.markAsTouched();
    });
  }

  public isFormValid(): boolean {
    return this.priceBookForm.valid;
  }

  getPriceBook(): PriceBook {
    const priceBook: PriceBook = this.priceBookForm.getRawValue();
    return omitBy(priceBook, value => value === null || value === undefined);
  }

  private createPriceBookForm(): void {
    this.priceBookForm = this.fb.group({
      name: [null, [Validators.required, FormValidators.whitespace, Validators.maxLength(200)]],
      description: [null, Validators.maxLength(1000)],
      currency: [null],
      active: [true]
    });
    this.applyPermissionsToForm();
  }

  private subscribeForSliderClose(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        takeUntil(this.componentDestroyed),
        filter(change => change.key === 'PriceBookList.AddSlider'),
        map(change => change.opened),
        distinctUntilChanged(),
        filter(opened => !opened)
      ).subscribe(
      () => this.clear()
    );
  }

  private applyPermissionsToForm(): void {
    if (this.priceBookForm) {
      this.priceBookForm.enable();
      applyPermission(this.name, !this.authService.isPermissionPresent(`${this.authKey}name`, 'r'));
      applyPermission(this.description, !this.authService.isPermissionPresent(`${this.authKey}description`, 'r'));
      applyPermission(this.currency, !this.authService.isPermissionPresent(`${this.authKey}currency`, 'r'));
      applyPermission(this.active, !this.authService.isPermissionPresent(`${this.authKey}active`, 'r'));
      if (this.editedPriceBook && !this.editedPriceBook.empty) {
        this.currency.disable();
      }
    }
  }

  private subscribeForCurrencies(): void {
    this.configService.getGlobalParamValue('currencies').pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (param) => this.currencies = param.value.split(';'),
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }
}
