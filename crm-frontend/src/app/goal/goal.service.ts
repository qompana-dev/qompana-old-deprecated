import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Page, PageRequest} from '../shared/pagination/page.model';
import {Observable} from 'rxjs';
import {GoalEditDto, GoalListDTO, GoalSaveDto, GoalType, GoalUpdateDto, GoalWidgetPreviewDto, UserGoalDto} from './goal.model';
import {UrlUtil} from '../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class GoalService {

  goalDeleted: EventEmitter<void> = new EventEmitter<void>();
  private goalUrl = `${UrlUtil.url}/crm-business-service/goals`;

  constructor(private http: HttpClient) {
  }

  getGoals(pageRequest: PageRequest, createdByMe: boolean): Observable<Page<GoalListDTO>> {
    const params = new HttpParams({fromObject: pageRequest as any});
    const url = createdByMe ? this.goalUrl + '/created-by-me' : this.goalUrl + '/assigned-to-me';
    return this.http.get<Page<GoalListDTO>>(url, {params});
  }

  getAllForMeByType(type: GoalType): Observable<GoalWidgetPreviewDto[]> {
    const url = this.goalUrl + `/type/${type}/widget-preview`;
    return this.http.get<GoalWidgetPreviewDto[]>(url);
  }

  getGoalForEdit(goalId: number): Observable<GoalEditDto> {
    const url = `${UrlUtil.url}/crm-business-service/goals/for-edit/${goalId}`;
    return this.http.get<GoalEditDto>(url);
  }

  getUserGoalsForWidgetCreator(goalId: number): Observable<UserGoalDto[]> {
    const url = `${UrlUtil.url}/crm-business-service/goals/${goalId}/user-goals`;
    return this.http.get<UserGoalDto[]>(url);
  }

  save(goalSaveDto: GoalSaveDto): Observable<GoalSaveDto> {
    const url = `${UrlUtil.url}/crm-business-service/goals`;
    return this.http.post<GoalSaveDto>(url, goalSaveDto);
  }

  delete(id: number): Observable<void> {
    const url = `${UrlUtil.url}/crm-business-service/goals/${id}`;
    return this.http.delete<void>(url);
  }

  updateGoalIntervals(id: number, goal: GoalUpdateDto): Observable<GoalUpdateDto> {
    const url = `${UrlUtil.url}/crm-business-service/goals/${id}`;
    return this.http.put<GoalUpdateDto>(url, goal);
}

  getUerGoalAssignedToMe(goalId: number): Observable<UserGoalDto> {
    const url = `${UrlUtil.url}/crm-business-service/goals/${goalId}/assigned-to-me`;
    return this.http.get<UserGoalDto>(url);
  }
}
