import {Component, Input, OnDestroy} from '@angular/core';
import {GoalIntervalDto, UserGoalDto} from '../../goal.model';
import {Subject} from 'rxjs';
import {GoalAddService} from '../../goal-add.service';
import {DateUtil} from '../../../shared/util/date.util';

@Component({
  selector: 'app-user-goal-row',
  templateUrl: './user-goal-row.component.html',
  styleUrls: ['./user-goal-row.component.scss']
})
export class UserGoalRowComponent implements OnDestroy {

  @Input() set userGoal(userGoal: UserGoalDto) {
    if (userGoal) {
      this._userGoal = userGoal;
      this.setColumns();
    }
  }
  @Input() isAmount = false;

  private componentDestroyed: Subject<void> = new Subject();

  columns: string[] = ['userName', 'sum'];
  intervalNumbers: number[] = [];
  _userGoal: UserGoalDto;

  constructor(public goalAddService: GoalAddService) {
  }

  setColumns(): void {
    this.intervalNumbers = DateUtil.getNumbersList(0, this._userGoal.intervals.length - 1, 1);
    this.columns = ['userName'].concat(this.intervalNumbers.map(index => 'interval' + index));
  }


  getInterval(index: number): GoalIntervalDto {
    return this._userGoal.intervals.find(item => item.index === index);
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }
}

