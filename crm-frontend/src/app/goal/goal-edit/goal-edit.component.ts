import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SliderComponent} from '../../shared/slider/slider.component';
import {GoalFormComponent} from '../goal-form/goal-form.component';
import {Subject} from 'rxjs';
import {FormBuilder} from '@angular/forms';
import {GoalAddService} from '../goal-add.service';
import {GoalService} from '../goal.service';
import {GoalEditDto, UserGoalDto} from '../goal.model';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {NOT_FOUND} from 'http-status-codes';
import * as isNil from 'lodash/isNil';
import {takeUntil} from 'rxjs/operators';
import {whenSliderClosed} from '../../shared/util/slider.util';
import {SliderService} from '../../shared/slider/slider.service';

@Component({
  selector: 'app-goal-edit',
  templateUrl: './goal-edit.component.html',
  styleUrls: ['./goal-edit.component.scss']
})
export class GoalEditComponent implements OnInit, OnDestroy {

  @Input() slider: SliderComponent;
  public userListIsValid: boolean;
  public userListValuesIsValid: boolean;
  public userListTouched: boolean;
  public assignedToMe: boolean;

  @Input() set goalId(goal: { id: number, assignedToMe: boolean }) {
    if (goal && !isNil(goal.id)) {
      this._goalId = goal.id;
      this.assignedToMe = goal.assignedToMe;
      this.getGoal(goal.id);
      if (this.assignedToMe) {
        this.getUserGoalAssignedToMe(goal.id);
      }
    }
  }

  goal: GoalEditDto;
  userGoal: UserGoalDto;
  _goalId: number;
  @ViewChild('goalFormComponent') goalFormComponent: GoalFormComponent;
  private componentDestroyed: Subject<void> = new Subject();
  readonly getGoalForEditErrors: ErrorTypes = {
    base: 'goal.edit.notification.',
    defaultText: 'goalForEditError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'goalForEditNotFound'
      }
    ]
  };

  readonly getUserGoalErrors: ErrorTypes = {
    base: 'goal.edit.notification.',
    defaultText: 'goalUserGoalError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'goalUserGoalNotFound'
      }
    ]
  };

  readonly updateGoalErrors: ErrorTypes = {
    base: 'goal.edit.notification.',
    defaultText: 'updateGoalError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'updateGoalNotFound'
      }
    ]
  };

  constructor(private fb: FormBuilder,
              private goalService: GoalService,
              private goalAddService: GoalAddService,
              private notificationService: NotificationService,
              private sliderService: SliderService) {
  }

  ngOnInit(): void {
    this.setValidUserList();
    this.subscribeForSliderClose();
    this.goalAddService.goalChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.setValidUserList();
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    this.userListTouched = true;
    this.setValidUserList();

    if (this.userListIsValid && this.userListValuesIsValid) {
      const goal = this.goalAddService.goal;
      this.goalService.updateGoalIntervals(goal['id'], goal)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => {
            this.slider.close();
            this.notificationService.notify('goal.edit.notification.updateGoalSuccess', NotificationType.SUCCESS);
            this.userListTouched = false;
          },
          err => this.notificationService.notifyError(this.updateGoalErrors, err.status)
        );
    }
  }

  cancel(): void {
    this.slider.close();
  }

  private getGoal(id: number): void {
    this.goalService.getGoalForEdit(id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        goal => {
          this.goal = goal;
          this.goalAddService.goal = goal;
        },
        err => this.notificationService.notifyError(this.getGoalForEditErrors, err.status)
      );
  }

  private setValidUserList(): void {
    this.userListIsValid = this.goalAddService._goal &&
      this.goalAddService._goal.usersGoals && this.goalAddService._goal.usersGoals.length > 0;
    this.userListValuesIsValid = this.goalAddService._goal &&
      this.goalAddService._goal.usersGoals &&
      this.goalAddService._goal.usersGoals
        .filter(ug => !(ug.intervals && ug.intervals.filter(i => i.expectedValue > 0).length > 0)).length === 0;
  }

  private subscribeForSliderClose(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        whenSliderClosed(this.componentDestroyed, [this.slider.key]),
      ).subscribe(
      () => this.goalAddService.resetEvent.next()
    );
  }


  private getUserGoalAssignedToMe(goalId: number): void {
    this.goalService.getUerGoalAssignedToMe(goalId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        userGoal => {
          this.userGoal = userGoal;
        },
        err => {
          if (err.status !== NOT_FOUND) {
            this.notificationService.notifyError(this.getUserGoalErrors, err.status);
          }
        }
      );
  }
}
