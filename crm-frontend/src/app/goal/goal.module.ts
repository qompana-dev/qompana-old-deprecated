import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {NgxMaskModule} from 'ngx-mask';
import {GoalRoutingModule} from './goal-routing.module';
import {GoalAddComponent} from './goal-add/goal-add.component';
import {GoalFormComponent} from './goal-form/goal-form.component';
import {GoalUserListComponent} from './goal-form/goal-user-list/goal-user-list.component';
import {AddUserToGoalComponent} from './goal-form/goal-user-list/add-user-to-goal/add-user-to-goal.component';
import {GoalListComponent} from './goal-list/goal-list.component';
import {GoalTableComponent} from './goal-list/goal-table/goal-table.component';
import {GoalInfoFormComponent} from './goal-form/goal-info-form/goal-info-form.component';
import {GoalCategoriesListComponent} from './goal-form/goal-categories-list/goal-categories-list.component';
import {ProductCategoriesListComponent} from './goal-form/goal-categories-list/product-categories-list/product-categories-list.component';
import {GoalProductsListComponent} from './goal-form/goal-products-list/goal-products-list.component';
import {GoalEditComponent} from './goal-edit/goal-edit.component';
import {GoalProductsListAddComponent} from './goal-form/goal-products-list/goal-products-list-add/goal-products-list-add.component';
import {GoalEditUserListComponent} from './goal-edit/goal-edit-user-list/goal-edit-user-list.component';
import {UserGoalRowComponent} from './goal-edit/user-goal-row/user-goal-row.component';
import {IsAmountGoalPipe} from './goal-util/is-amount-goal.pipe';
import {AvatarModule} from '../shared/avatar-form/avatar.module';

@NgModule({

  declarations: [
    GoalAddComponent,
    GoalFormComponent,
    GoalListComponent,
    GoalTableComponent,
    GoalUserListComponent,
    AddUserToGoalComponent,
    GoalInfoFormComponent,
    AddUserToGoalComponent,
    GoalCategoriesListComponent,
    ProductCategoriesListComponent,
    GoalProductsListComponent,
    GoalEditComponent,
    GoalEditUserListComponent,
    GoalProductsListAddComponent,
    UserGoalRowComponent,
    IsAmountGoalPipe
  ],
  imports: [
    CommonModule,
    SharedModule,
    GoalRoutingModule,
    NgxMaskModule,
    AvatarModule
  ]
})
export class GoalModule {
}
