import {Injectable} from '@angular/core';
import {GoalInterval, GoalIntervalIndex, GoalSaveDto, GoalType} from './goal.model';
import {Subject} from 'rxjs';
import cloneDeep from 'lodash/cloneDeep';
import {AccountInfoDto} from '../person/person.model';
import {PersonService} from '../person/person.service';
// @ts-ignore
import moment, {Moment} from 'moment';
import {TranslateService} from '@ngx-translate/core';
import {GoalWidgetData} from '../dashboard/dashboard.model';

@Injectable({
  providedIn: 'root'
})
export class GoalAddService {
  resetEvent: Subject<void> = new Subject<void>();
  resetList: Subject<void> = new Subject<void>();
  // tslint:disable-next-line:variable-name
  _goal: GoalSaveDto = {usersGoals: []} as GoalSaveDto;
  public goalChanged: Subject<GoalSaveDto> = new Subject<GoalSaveDto>();

  public accountInfoList: AccountInfoDto[] = [];
  public accountInfoMap: Map<number, AccountInfoDto> = new Map<number, AccountInfoDto>();
  public accountListChanged: Subject<void> = new Subject<void>();

  public monthlyIntervals: GoalIntervalIndex[] = [];
  public quarterIntervals: GoalIntervalIndex[] = [];

  public sumTime: string;

  constructor(private personService: PersonService,
              private translateService: TranslateService) {
    this.getUsers();

    this.setIntervalIndexLists();
    this.translateService.onDefaultLangChange
      .subscribe(() => this.setIntervalIndexLists());
  }

  set goal(goal: GoalSaveDto) {
    const oldType = cloneDeep(this._goal.type);
    this._goal = goal;
    if (oldType !== this._goal.type && !this.isAmount()) {
      this.removeDotFromIntervalExpectedValue();
    }
    this.setSumTime();
    this.sortList();
    this.goalChanged.next(this._goal);
  }

  set categories(categoriesIds: number[]) {
    this._goal.associatedObjectsIds = categoriesIds;
    this.goalChanged.next(this._goal);
  }

  set products(productsIds: number[]) {
    this._goal.associatedObjectsIds = productsIds;
    this.goalChanged.next(this._goal);
  }

  get goal(): GoalSaveDto {
    return cloneDeep(this._goal);
  }

  isAmount(): boolean {
    return [GoalType.WON_OPPORTUNITIES_AMOUNT,
      GoalType.WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT,
      GoalType.OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT]
      .indexOf(this._goal.type) !== -1;
  }

  private removeDotFromIntervalExpectedValue(): void {
    this._goal.usersGoals.forEach(ug => ug.intervals.forEach(i => i.expectedValue = +i.expectedValue.toString().replace('.', '')));
  }

  public getUsers(): void {
    const sub = this.personService.getAccountInfoChildListForLoggedUserRole()
      .subscribe((accountInfoList: AccountInfoDto[]) => {
        this.accountInfoList = accountInfoList;
        this.accountInfoList.forEach(info => this.accountInfoMap.set(info.id, info));
        this.accountListChanged.next();
      }, undefined, () => {
        if (sub) {
          sub.unsubscribe();
        }
      });
  }

  private setIntervalIndexLists(): void {
    this.monthlyIntervals = Array.apply(0, Array(12))
      .map((item, index): GoalIntervalIndex => {
        const month: Moment = moment().locale(localStorage.getItem('locale')).month(index);
        const monthString = this.getStringFromNum((index + 1));
        return {
          index: (index + 1),
          label: month.format('MMMM'),
          start: '01.' + monthString,
          end: month.daysInMonth() + '.' + monthString
        };
      });

    this.quarterIntervals = Array.apply(0, Array(4))
      .map((item, index): GoalIntervalIndex => {
        const startIndex = (index * 3) + 1;
        const endIndex = ((index + 1) * 3);
        const start = this.getStringFromNum(startIndex) + '.' + moment().locale(localStorage.getItem('locale'))
          .month(startIndex - 1).daysInMonth();
        const end = this.getStringFromNum(endIndex) + '.' + moment().locale(localStorage.getItem('locale'))
          .month(endIndex - 1).daysInMonth();
        return {
          index: (index + 1),
          label: this.translateService.instant('goal.shared.quarters.quarter' + (index + 1)),
          start,
          end
        };
      });
  }

  private getStringFromNum(num: number): string {
    return ((num < 10) ? '0' : '') + num;
  }

  public getIntervalNameByIndex(index: number): { title: string, description?: string } {
    if (this._goal.interval === GoalInterval.YEAR) {
      return {title: (this._goal.startYear + index) + ''};
    }

    const indexToSearch = this._goal.startIndex + index;
    const result = ((this._goal.interval === GoalInterval.MONTH) ? this.monthlyIntervals : this.quarterIntervals)
      .find(item => item.index === indexToSearch);

    const title = (result) ? result.label : '';
    const description = (result) ? result.start + ' - ' + result.end : undefined;
    return {title, description};
  }

  public getIntervalNameByIndexAndInterval(index: number, goalWidgetData: GoalWidgetData): { title: string, description?: string } {
    if (goalWidgetData.interval === GoalInterval.YEAR) {
      return {title: (goalWidgetData.startYear + index) + ''};
    }

    const indexToSearch = goalWidgetData.startIndex + index;
    const result = ((goalWidgetData.interval === GoalInterval.MONTH) ? this.monthlyIntervals : this.quarterIntervals)
      .find(item => item.index === indexToSearch);

    const title = (result) ? result.label : '';
    const description = (result) ? result.start + ' - ' + result.end : undefined;
    return {title, description};
  }

  private setSumTime(): void {
    if (this._goal.interval !== GoalInterval.YEAR) {
      const intervalsToSearch = ((this._goal.interval === GoalInterval.MONTH) ? this.monthlyIntervals : this.quarterIntervals);
      const start = intervalsToSearch.find(item => item.index === this._goal.startIndex).start;
      const end = intervalsToSearch.find(item => item.index === (this._goal.startIndex + this._goal.intervalNumber - 1)).end;
      this.sumTime = start + ' - ' + end;
    } else {
      this.sumTime = undefined;
    }
  }

  private sortList(): void {
    this._goal.usersGoals.sort((a, b) => (a.userId > b.userId) ? 1 : -1);
  }
}
