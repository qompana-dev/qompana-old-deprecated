import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {GoalService} from '../goal.service';
import {GoalFormComponent} from '../goal-form/goal-form.component';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {Subject} from 'rxjs';
import {GoalSaveDto} from '../goal.model';
import {takeUntil} from 'rxjs/operators';
import {GoalAddService} from '../goal-add.service';
import {SliderComponent} from '../../shared/slider/slider.component';

@Component({
  selector: 'app-goal-add',
  templateUrl: './goal-add.component.html',
  styleUrls: ['./goal-add.component.scss']
})
export class GoalAddComponent implements OnInit, OnDestroy {
  @Input() slider: SliderComponent;
  @ViewChild('goalFormComponent') goalFormComponent: GoalFormComponent;
  private componentDestroyed: Subject<void> = new Subject();

  goalFormTouched = false;
  userListIsValid = true;
  userListValuesIsValid = true;
  categoryListIsValid = true;
  productListIsValid = true;

  readonly errors: ErrorTypes = {
    base: 'goal.add.notification.',
    defaultText: 'undefinedError',
    errors: [{
      code: 403,
      text: 'noPermission'
    }, {
      code: 400,
      text: 'validationError'
    }
    ]
  };
  saveInProgress = false;

  constructor(private fb: FormBuilder,
              private goalAddService: GoalAddService,
              private notificationService: NotificationService,
              private goalService: GoalService) {
  }

  ngOnInit(): void {
    this.slider.closeEvent
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.goalAddService.resetEvent.next());
    this.setFormValidFields();
    this.goalAddService.goalChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.setFormValidFields();
      });
  }

  private setFormValidFields(): void {
    this.setValidUserList();
    this.setValidCategoryList();
    this.setValidProductsList();
  }

  private setValidUserList(): void {
    this.userListIsValid = this.goalAddService._goal &&
      this.goalAddService._goal.usersGoals && this.goalAddService._goal.usersGoals.length > 0;
    this.userListValuesIsValid = this.goalAddService._goal &&
      this.goalAddService._goal.usersGoals &&
      this.goalAddService._goal.usersGoals
        .filter(ug => !(ug.intervals && ug.intervals.filter(i => i.expectedValue > 0).length > 0)).length === 0;
  }

  private setValidCategoryList(): void {
    const goal = this.goalAddService._goal;
    this.categoryListIsValid = goal &&
      (goal.type !== 'OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT' || (goal.associatedObjectsIds && goal.associatedObjectsIds.length > 0));
  }

  private setValidProductsList(): void {
    const goal = this.goalAddService._goal;
    this.productListIsValid = goal &&
      (goal.type !== 'WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT' || (goal.associatedObjectsIds && goal.associatedObjectsIds.length > 0));
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    this.goalFormTouched = true;
    this.setFormValidFields();
    if (this.goalFormComponent && this.goalFormComponent.isValid() &&
      this.userListIsValid && this.userListValuesIsValid && this.categoryListIsValid) {

      if (!this.saveInProgress) {
        this.saveInProgress = true;
        const goal: GoalSaveDto = this.goalAddService.goal;
        this.goalService.save(goal)
          .pipe(takeUntil(this.componentDestroyed))
          .subscribe(() => {
            this.saveInProgress = false;
            this.notificationService.notify('goal.add.notification.savedSuccessful', NotificationType.SUCCESS);
            this.goalFormTouched = false;
            this.slider.close();
            this.goalAddService.resetList.next();
          }, (err) => {
            this.saveInProgress = false;
            this.notificationService.notifyError(this.errors, err.status);
          });
      }
    }
  }

  cancel(): void {
    this.goalAddService.resetEvent.next();
    this.slider.close();
  }
}
