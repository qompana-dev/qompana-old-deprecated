import {Product, ProductSubcategory} from '../product/product.model';

export enum GoalCategory {
  LEAD = 'LEAD',
  OPPORTUNITY = 'OPPORTUNITY',
  CALENDAR = 'CALENDAR',
  CUSTOMER = 'CUSTOMER',
  OFFER = 'OFFER'
}

export enum GoalInterval {
  MONTH = 'MONTH',
  QUARTER = 'QUARTER',
  YEAR = 'YEAR'
}

export interface GoalIntervalIndex {
  index: number;
  label: string;
  start?: string;
  end?: string;
}


export enum GoalType {
  CREATED_LEADS_NUMBER = 'CREATED_LEADS_NUMBER',
  CONVERTED_LEADS_NUMBER = 'CONVERTED_LEADS_NUMBER',

  CREATED_OPPORTUNITIES_NUMBER = 'CREATED_OPPORTUNITIES_NUMBER',
  WON_OPPORTUNITIES_NUMBER = 'WON_OPPORTUNITIES_NUMBER',
  WON_OPPORTUNITIES_AMOUNT = 'WON_OPPORTUNITIES_AMOUNT',
  WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT = 'WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT',
  OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT = 'OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT',

  PHONE_CALLS_NUMBER = 'PHONE_CALLS_NUMBER',
  CALENDAR_TASKS_NUMBER = 'CALENDAR_TASKS_NUMBER',

  ADDED_CUSTOMERS_NUMBER = 'ADDED_CUSTOMERS_NUMBER',
  ADDED_CONTACTS_NUMBER = 'ADDED_CONTACTS_NUMBER',

  GENERATED_OFFERS_NUMBER = 'GENERATED_OFFERS_NUMBER',
  CUSTOMERS_WHO_RECEIVED_OFFER = 'CUSTOMERS_WHO_RECEIVED_OFFER'
}

export class GoalCategoryType {
  types: {category: GoalCategory, types: GoalType[]}[] = [
    {
      category: GoalCategory.LEAD, types: [
        GoalType.CREATED_LEADS_NUMBER,
        GoalType.CONVERTED_LEADS_NUMBER]
    },
    {
      category: GoalCategory.OPPORTUNITY, types: [
        GoalType.CREATED_OPPORTUNITIES_NUMBER,
        GoalType.WON_OPPORTUNITIES_NUMBER,
        GoalType.WON_OPPORTUNITIES_AMOUNT,
        GoalType.WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT,
        GoalType.OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT]
    },
    {
      category: GoalCategory.CALENDAR, types: [
        GoalType.PHONE_CALLS_NUMBER,
        GoalType.CALENDAR_TASKS_NUMBER]
    },
    {
      category: GoalCategory.CUSTOMER, types: [
        GoalType.ADDED_CUSTOMERS_NUMBER,
        GoalType.ADDED_CONTACTS_NUMBER]
    },
    {
      category: GoalCategory.OFFER, types: [
        GoalType.GENERATED_OFFERS_NUMBER,
        GoalType.CUSTOMERS_WHO_RECEIVED_OFFER]
    },
  ];

  getTypes(category: GoalCategory): GoalType[] {
    const result: any = this.types.find(item => item.category === category);
    return (result) ? result.types : undefined;
  }

  getCategory(type: GoalType): GoalCategory {
    const result: any = this.types.filter(item => item.types.indexOf(type) !== -1);
    return (result.length === 1) ? result[0].category : undefined;
  }
}

interface BasicGoal {
  name: string;
  type: GoalType;
  interval: GoalInterval;
  intervalNumber: number;
  startIndex: number;
  startYear: number;
  currency: string;
}

export interface GoalListDTO {
  id: number;
  name: string;
  type: GoalType;
  interval: GoalInterval;
  intervalNumber: number;
  startIndex: number;
  startYear: number;
  category: GoalCategory;
  goalCompleted: number;
  deficitOrExcess: number;
}

export interface GoalSaveDto extends BasicGoal {
  usersGoals: UserGoalDto[];
  associatedObjectsIds?: number[];
}

export interface GoalEditDto extends BasicGoal {
  id: number;
  usersGoals: UserGoalDto[];
  subcategories: ProductSubcategory[];
  products: Product[];
}

export interface GoalUpdateDto {
  usersGoals: UserGoalDto[];
}

export interface UserGoalDto {
  userId: number;
  intervals: GoalIntervalDto[];
}

export interface GoalIntervalDto {
  index: number;
  expectedValue: number;
  currentValue?: number;
  result?: number;
}

export interface GoalWidgetPreviewDto {
  id: number;
  name: string;
}
