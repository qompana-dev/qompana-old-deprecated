import {Pipe, PipeTransform} from '@angular/core';
import {GoalType} from '../goal.model';

@Pipe({
  name: 'isAmountGoal'
})
export class IsAmountGoalPipe implements PipeTransform {

  transform(value: { type: GoalType }): boolean {
    if (!value) {
      return false;
    }
    return [
      GoalType.WON_OPPORTUNITIES_AMOUNT,
      GoalType.WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT,
      GoalType.OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT
    ].includes(value.type);
  }
}
