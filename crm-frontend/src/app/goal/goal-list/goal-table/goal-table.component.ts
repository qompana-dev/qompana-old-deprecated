import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {PageRequest} from '../../../shared/pagination/page.model';
import {InfinityScrollUtil, InfinityScrollUtilInterface} from '../../../shared/infinity-scroll.util';
import {FilterStrategy} from '../../../shared/model';
import {Observable, Subject} from 'rxjs';
import {GoalService} from '../../goal.service';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {map, startWith, takeUntil} from 'rxjs/operators';
import {AuthService} from '../../../login/auth/auth.service';
import {GoalAddService} from '../../goal-add.service';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import {GoalInterval} from '../../goal.model';

@Component({
  selector: 'app-goal-table',
  templateUrl: './goal-table.component.html',
  styleUrls: ['./goal-table.component.scss']
})
export class GoalTableComponent implements OnInit, OnDestroy, InfinityScrollUtilInterface {

  @Input() goalTitle: string;
  @Output() onEdit: EventEmitter<number> = new EventEmitter<number>();
  // tslint:disable-next-line:variable-name
  _createdByMe: boolean;
  columns = ['name', 'category', 'type', 'interval', 'intervalNumber', 'startIndex', 'startYear', 'goalCompleted', 'deficitOrExcess',
    'action'];
  filteredColumns$: Observable<string[]>;
  infinityScroll: InfinityScrollUtil;
  private componentDestroyed: Subject<void> = new Subject();

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'goal.list.dialog.delete.title',
    description: 'goal.list.dialog.delete.description',
    noteFirst: 'goal.list.dialog.delete.noteFirstPart',
    noteSecond: 'goal.list.dialog.delete.noteSecondPart',
    confirmButton: 'goal.list.dialog.delete.confirm',
    cancelButton: 'goal.list.dialog.delete.cancel',
  };

  @Input() set createdByMe(createdByMe: boolean) {
    if (createdByMe !== undefined) {
      this._createdByMe = createdByMe;
      this.infinityScroll = new InfinityScrollUtil(
        this,
        this.componentDestroyed,
        this.notificationService,
        'goal.list.getError'
      );
      this.infinityScroll.initData();
    }
  }

  constructor(private goalService: GoalService,
              private goalAddService: GoalAddService,
              private notificationService: NotificationService,
              private deleteDialogService: DeleteDialogService,
              private authService: AuthService) {
  }

  tryDeleteGoal(id: number): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.delete(id);
    this.deleteDialogService.open(this.deleteDialogData);
  }

  delete(id: number): void {
    this.goalService.delete(id).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.goalService.goalDeleted.emit();
        this.notificationService.notify('goal.list.deletionSuccess', NotificationType.SUCCESS);
      },
      () => this.notificationService.notify('goal.list.deletionError', NotificationType.ERROR)
    );
  }

  ngOnInit(): void {
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
    this.goalService.goalDeleted.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.infinityScroll.initData()
    );
    this.goalAddService.resetList.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.infinityScroll.initData()
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  getItemObservable(selection: FilterStrategy, pageRequest: PageRequest): Observable<any> {
    return this.goalService.getGoals(pageRequest, this._createdByMe);
  }

  edit(id: number): void {
    this.onEdit.emit(id);
  }

  getIntervalTranslation(startInterval: number, interval: GoalInterval): string {
    if (interval === GoalInterval.MONTH) {
      return this.goalAddService.monthlyIntervals[startInterval - 1].label;
    } else if (interval === GoalInterval.QUARTER) {
      return this.goalAddService.quarterIntervals[startInterval - 1].label;
    } else {
      return '';
    }
  }

  private getFilteredColumns(): string[] {
    return this.columns.filter(
      (item) => !this.authService.isPermissionPresent('GoalTableComponent.' + item, 'i') || item === 'action');
  }
}
