import {Component, OnInit, ViewChild} from '@angular/core';
import {SliderComponent} from '../../shared/slider/slider.component';

@Component({
  selector: 'app-goal-list',
  templateUrl: './goal-list.component.html',
  styleUrls: ['./goal-list.component.scss']
})
export class GoalListComponent implements OnInit {

  @ViewChild('editSlider') editSlider: SliderComponent;
  columns = ['name', 'category', 'type', 'interval', 'intervalNumber', 'startIndex', 'startYear', 'goalCompleted', 'deficitOrExcess'];
  goalSelectedToEdit: {id: number, assignedToMe: boolean};

  constructor() {
  }

  ngOnInit(): void {
  }

  editGoal(goalId: number, assignedToMe: boolean = false): void {
    this.goalSelectedToEdit = {id: goalId, assignedToMe};
    this.editSlider.open();
  }
}
