import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {GoalCategory, GoalCategoryType, GoalEditDto, GoalInterval, GoalIntervalIndex, GoalSaveDto, GoalType} from '../../goal.model';
import {DateUtil} from '../../../shared/util/date.util';
import {GoalAddService} from '../../goal-add.service';
import {FormUtil} from '../../../shared/form.util';
import {takeUntil} from 'rxjs/operators';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {GlobalConfigService} from '../../../global-config/global-config.service';

@Component({
  selector: 'app-goal-info-form',
  templateUrl: './goal-info-form.component.html',
  styleUrls: ['./goal-info-form.component.scss']
})
export class GoalInfoFormComponent implements OnInit, OnDestroy {
  goalForm: FormGroup;
  private componentDestroyed: Subject<void> = new Subject();

  goalCategoryEnum = GoalCategory;
  goalTypeEnum = GoalType;
  goalIntervalEnum = GoalInterval;
  goalCategoryTypeEnumUtil: GoalCategoryType = new GoalCategoryType();

  goalAvailableTypes: GoalType[] = [];
  goalAvailableIntervalNumbers = DateUtil.getNumbersList(1, 1, 1);

  yearList: number[] = [];

  currentAvailableIntervals: GoalIntervalIndex[] = [{index: 1, label: 'year'}];
  currencies = [];

  @Input() set goal(goal: GoalEditDto) {
    this.updateForm(goal);
  }

  @Input() set update(update: boolean) {
    this._update = update;
    if (update) {
      this.goalForm.disable();
    }
  }

  // tslint:disable-next-line:variable-name
  _update = false;

  constructor(private fb: FormBuilder,
              private goalAddService: GoalAddService,
              private configService: GlobalConfigService,
              private notificationService: NotificationService) {
    this.subscribeForCurrencies();
    this.createGoalForm();
  }

  ngOnInit(): void {
    this.setCurrentAvailableIntervals();
    this.goalAddService.resetEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.createGoalForm()
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  // @formatter:off
  get category(): AbstractControl { return this.goalForm.get('category'); }
  get type(): AbstractControl { return this.goalForm.get('type'); }
  get interval(): AbstractControl { return this.goalForm.get('interval'); }
  get intervalNumber(): AbstractControl { return this.goalForm.get('intervalNumber'); }
  get startIndex(): AbstractControl { return this.goalForm.get('startIndex'); }
  get startYear(): AbstractControl { return this.goalForm.get('startYear'); }
  get currency(): AbstractControl { return this.goalForm.get('currency'); }
  // @formatter:on

  public isValid(): boolean {
    FormUtil.setTouched(this.goalForm);
    return this.goalForm.valid;
  }

  private setGoal(): void {
    const goal = this.goalAddService.goal;
    goal.type = this.type.value;
    goal.interval = this.interval.value;
    goal.intervalNumber = this.intervalNumber.value;
    goal.startIndex = this.startIndex.value;
    goal.startYear = this.startYear.value;
    if (this.isAmountGoal()) {
      goal.currency = this.currency.value;
    } else {
      goal.currency = null;
    }
    this.goalAddService.goal = goal;
  }

  isAmountGoal(): boolean {
    return this.type.value === this.goalTypeEnum.WON_OPPORTUNITIES_AMOUNT ||
      this.type.value === this.goalTypeEnum.WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT ||
      this.type.value === this.goalTypeEnum.OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT;
  }

  private createGoalForm(): void {
    this.goalForm = this.fb.group({
      category: [this.goalCategoryEnum.LEAD, Validators.required],
      type: [this.goalTypeEnum.CREATED_LEADS_NUMBER, Validators.required],
      interval: [GoalInterval.MONTH, Validators.required],
      intervalNumber: [1, Validators.required],
      startIndex: [1, Validators.required],
      startYear: [DateUtil.getCurrentYear(), Validators.required],
      currency: [this.currencies[0]]
    });
    this.setYearList();
    this.subscribeCategoryChange();
    this.subscribeIntervalChange();
    this.subscribeStartIndexChange();
    this.subscribeFormChanged();
    this.subscribeIntervalNumberChange();
  }

  private subscribeIntervalNumberChange(): void {
    this.intervalNumber.valueChanges
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.updateUserGoals());
  }

  private updateUserGoals(): void {
    const newIntervalNumber = +this.intervalNumber.value;
    const goal: GoalSaveDto = this.goalAddService.goal;

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < goal.usersGoals.length; i++) {
      if (goal.usersGoals[i].intervals.length !== newIntervalNumber) {
        if (goal.usersGoals[i].intervals.length > newIntervalNumber) {
          goal.usersGoals[i].intervals = goal.usersGoals[i].intervals.slice(0, newIntervalNumber);
        } else {
          const start = goal.usersGoals[i].intervals.length;
          for (let j = start; j < newIntervalNumber; j++) {
            goal.usersGoals[i].intervals.push({index: j, expectedValue: 0});
          }
        }
      }
    }
    this.goalAddService.goal = goal;
  }

  private subscribeFormChanged(): void {
    this.setGoal();
    this.goalForm.valueChanges
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.setGoal());
  }

  private subscribeCategoryChange(): void {
    this.changeAvailableTypes();
    this.category.valueChanges
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.changeAvailableTypes());
  }

  private subscribeIntervalChange(): void {
    this.setCurrentAvailableIntervals();
    this.interval.valueChanges
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.setCurrentAvailableIntervals());
  }

  private subscribeStartIndexChange(): void {
    this.setAvailableIntervalNumbers();
    this.startIndex.valueChanges
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.setAvailableIntervalNumbers());
  }

  private setAvailableIntervalNumbers(): void {
    const stop = (this.currentAvailableIntervals.length + 1) - this.startIndex.value;
    if (stop < this.startIndex.value || stop < this.intervalNumber.value) {
      this.intervalNumber.setValue(stop);
    }
    if (this.intervalNumber.value === 0) {
      this.intervalNumber.setValue(1);
    }
    this.goalAvailableIntervalNumbers = DateUtil.getNumbersList(1, stop, 1);
  }

  private changeAvailableTypes(notEmit?: boolean): void {
    this.goalAvailableTypes = this.goalCategoryTypeEnumUtil.getTypes(this.category.value);
    if (this.goalAvailableTypes && this.goalAvailableTypes.indexOf(this.type.value) === -1) {
      this.type.patchValue(this.goalAvailableTypes[0], {emitEvent: !notEmit});
    }
  }

  private setYearList(): void {
    const currentYear = DateUtil.getCurrentYear();
    const start = (+this.startYear.value < currentYear) ? +this.startYear.value : currentYear;
    this.yearList = DateUtil.getNumbersList(start, DateUtil.getCurrentYear() + 50, 1);
  }

  private setCurrentAvailableIntervals(): void {
    if (!this.interval || this.interval.value === GoalInterval.YEAR) {
      this.currentAvailableIntervals = [{index: 1, label: 'year'}];
    } else {
      this.currentAvailableIntervals = (this.interval.value === GoalInterval.MONTH) ? this.goalAddService.monthlyIntervals : this.goalAddService.quarterIntervals;
    }
    if (this.currentAvailableIntervals.map(item => item.index).indexOf(this.startIndex.value) === -1) {
      this.startIndex.setValue(1);
    }
    this.setAvailableIntervalNumbers();
  }

  private updateForm(goal: GoalEditDto): void {
    if (this.goalForm && goal) {
      this.category.setValue(this.goalCategoryTypeEnumUtil.getCategory(goal.type).toString(), {emitEvent: false});
      this.changeAvailableTypes(true);
      this.type.setValue(goal.type, {emitEvent: false});
      this.interval.setValue(goal.interval, {emitEvent: false});
      this.intervalNumber.setValue(goal.intervalNumber, {emitEvent: false});
      this.startIndex.setValue(goal.startIndex, {emitEvent: false});
      this.startYear.setValue(goal.startYear, {emitEvent: false});
      this.currency.setValue(goal.currency, {emitEvent: false});
    }
  }


  private subscribeForCurrencies(): void {
    this.configService.getGlobalParamValue('currencies').pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (param) => this.currencies = param.value.split(';'),
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }
}
