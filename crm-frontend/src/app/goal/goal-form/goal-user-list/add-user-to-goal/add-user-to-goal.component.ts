import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {SliderService} from '../../../../shared/slider/slider.service';
import {takeUntil} from 'rxjs/operators';
import {AccountInfoDto} from '../../../../person/person.model';
import {GoalIntervalDto, GoalSaveDto, UserGoalDto} from '../../../goal.model';
import {GoalAddService} from '../../../goal-add.service';

@Component({
  selector: 'app-add-user-to-goal',
  templateUrl: './add-user-to-goal.component.html',
  styleUrls: ['./add-user-to-goal.component.scss']
})
export class AddUserToGoalComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();

  myAccount: AccountInfoDto;
  accountInfoList: AccountInfoDto[] = [];
  // tslint:disable-next-line:variable-name
  private _slider: SliderComponent;

  @Input() set slider(slider: SliderComponent) {
    this._slider = slider;
    this.subscribeOnSliderOpen();
  }

  constructor(private goalAddService: GoalAddService,
              private sliderService: SliderService) {
  }

  ngOnInit(): void {
    this.goalAddService.getUsers();
    this.setChecked();
    this.setAccountList();
    this.goalAddService.accountListChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.setAccountList());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private setAccountList(): void {
    if (this.goalAddService.accountInfoList) {
      const loggedAccountId = +localStorage.getItem('loggedAccountId');
      this.myAccount = this.goalAddService.accountInfoList.find(item => item.id === loggedAccountId);
      this.accountInfoList = this.goalAddService.accountInfoList.filter(item => item.id !== loggedAccountId);
      this.setChecked();
    }
  }

  closeSlider(): void {
    (this._slider) ? this._slider.close() : this.sliderService.closeSlider();
  }

  submit(): void {
    const newIds: number[] = [];
    if (this.myAccount && this.myAccount.checked) {
      newIds.push(this.myAccount.id);
    }
    this.accountInfoList.filter(item => item.checked).forEach(item => newIds.push(item.id));
    this.userListChanged(newIds);
    this.closeSlider();
  }

  setChecked(): void {
    const selectedUserIds: number[] = this.goalAddService.goal.usersGoals.map((ug) => ug.userId);
    if (selectedUserIds && selectedUserIds.length > 0
      && this.accountInfoList && this.myAccount) {
      this.myAccount.checked = (selectedUserIds.indexOf(this.myAccount.id) !== -1);
      this.accountInfoList = this.accountInfoList.map((item) => {
        item.checked = (selectedUserIds.indexOf(item.id) !== -1);
        return item;
      });
    }
  }

  subscribeOnSliderOpen(): void {
    this._slider.openEvent
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.goalAddService.getUsers();
        this.setChecked();
      });
  }


  userListChanged(ids: number[]): void {
    const goal: GoalSaveDto = this.goalAddService.goal;
    goal.usersGoals = goal.usersGoals.filter(ug => ids.indexOf(ug.userId) !== -1);
    const userGoalsIds: number[] = goal.usersGoals.map(ug => ug.userId);
    const idsToAdd = ids.filter(id => userGoalsIds.indexOf(id) === -1);
    idsToAdd.forEach(id => goal.usersGoals.push({userId: id, intervals: this.getIntervals(goal)}));
    this.goalAddService.goal = goal;
  }

  private getIntervals(goal: GoalSaveDto): GoalIntervalDto[] {
    const result: GoalIntervalDto[] = [];
    for (let i = 0; i < goal.intervalNumber; i++) {
      result.push({index: i, expectedValue: 0.0});
    }
    return result;
  }
}
