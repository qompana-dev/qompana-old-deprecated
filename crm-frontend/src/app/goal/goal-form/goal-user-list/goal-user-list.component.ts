import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {GoalSaveDto, GoalType, UserGoalDto} from '../../goal.model';
import {GoalAddService} from '../../goal-add.service';
import {DateUtil} from '../../../shared/util/date.util';

@Component({
  selector: 'app-goal-user-list',
  templateUrl: './goal-user-list.component.html',
  styleUrls: ['./goal-user-list.component.scss']
})
export class GoalUserListComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();

  public columns: string[] = ['userName', 'sum', 'equalValues'];
  intervalNumbers: number[] = [];

  focused = false;

  isAmount = false;

  // dataTable: any[] = [];
  dataTableSubject: Subject<UserGoalDto[]> = new Subject<UserGoalDto[]>();

  tableValuesList: TableValueItem[] = [];

  userSum: Map<number, number> = new Map<number, number>();
  totalSum = 0;

  equalValuesMap: Map<number, boolean> = new Map<number, boolean>();

  constructor(public goalAddService: GoalAddService) {
  }

  ngOnInit(): void {
    this.setColumns();
    this.setIsAmount();
    this.setUserSum();
    this.refreshList();
    this.goalAddService.goalChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.setIsAmount();
        this.setUserSum();
        if (!this.focused) {
          this.setColumns();
          this.setValuesToForm();
          this.refreshList();
        }
      });

    this.goalAddService.resetEvent
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.reset());
  }

  private reset(): void {
    if (this.goalAddService._goal && this.goalAddService._goal.usersGoals) {
      this.goalAddService._goal.usersGoals = [];
    }
    this.columns = ['userName', 'sum', 'equalValues'];
    this.intervalNumbers = [];
    this.focused = false;
    this.isAmount = false;
    this.tableValuesList = [];
    this.userSum = new Map<number, number>();
    this.totalSum = 0;
    this.dataTableSubject.next([]);
  }

  setColumns(): void {
    const goal: GoalSaveDto = this.goalAddService.goal;
    this.intervalNumbers = DateUtil.getNumbersList(0, goal.intervalNumber - 1, 1);
    this.columns = ['userName'].concat(this.intervalNumbers.map(index => 'interval' + index)).concat(['sum', 'equalValues']);
  }

  private updateGoal(userId: number, index: number): void {
    const goal: GoalSaveDto = this.goalAddService.goal;

    const newValue = +this.tableValuesList.find(item => item.userId === userId && item.index === index).value;

    goal.usersGoals = this.goalAddService.goal.usersGoals.map(ug => {
      if (ug.userId === userId) {
        ug.intervals = ug.intervals.map(i => {
          if (i.index === index || this.equalValuesMap.get(userId)) {
            i.expectedValue = newValue;
          }
          return i;
        });
      }
      return ug;
    });
    this.goalAddService.goal = goal;
    this.setUserSum();
  }

  private setValuesToForm(): void {
    const goal: GoalSaveDto = this.goalAddService.goal;
    if (!goal.usersGoals || goal.usersGoals.length === 0) {
      return;
    }
    const newList: TableValueItem[] = [];
    goal.usersGoals.forEach(ug => ug.intervals.forEach(i =>
      newList.push({userId: ug.userId, index: i.index, value: this.getNumberInFormat(i.expectedValue)})));

    this.getLastRow(goal.usersGoals).forEach(item => newList.push(item));
    this.updateEqualValuesMap(goal.usersGoals);

    this.tableValuesList = newList;
  }

  private updateEqualValuesMap(users: UserGoalDto[]): void {
    users.forEach(user => {
      if (this.equalValuesMap.get(user.userId) === undefined) {
        this.equalValuesMap.set(user.userId, false);
      }
    });
  }

  private getNumberInFormat(value: number): string {
    return String(value).replace('.', (this.isAmount) ? ',' : '');
  }

  private setIsAmount(): void {
    this.isAmount = [GoalType.WON_OPPORTUNITIES_AMOUNT,
      GoalType.WON_OPPORTUNITIES_FOR_PRODUCTS_AMOUNT,
      GoalType.OPPORTUNITIES_FOR_PRODUCT_GROUP_AMOUNT]
      .indexOf(this.goalAddService.goal.type) !== -1;
  }

  removeUser(userId: number): void {
    const goal: GoalSaveDto = this.goalAddService.goal;
    goal.usersGoals = goal.usersGoals.filter(item => item.userId !== userId);
    this.goalAddService.goal = goal;
    if (!goal.usersGoals || goal.usersGoals.length === 0) {
      this.tableValuesList = [];
      this.refreshList();
    }
    this.setUserSum();
  }

  getTableValueItem(userId: number, index: number): TableValueItem {
    return this.tableValuesList.find(item => item.userId === userId && item.index === index);
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private refreshList(): void {
    const users: UserGoalDto[] = this.goalAddService.goal.usersGoals;
    if (users.length > 0) {
      users.push({intervals: []} as UserGoalDto);
    }
    this.dataTableSubject.next(users);
  }

  private getLastRow(users: UserGoalDto[]): TableValueItem[] {
    const result: TableValueItem[] = [];

    for (let i = 0; i < this.goalAddService._goal.intervalNumber; i++) {
      result.push({index: i, value: this.getSumByIndex(i, users) + '', userId: undefined});
    }

    return result;
  }

  private getSumByIndex(index: number, users: UserGoalDto[]): number {
    let sum = 0;
    users.forEach(user => user.intervals
      .filter(i => i.index === index).map(i => i.expectedValue).forEach(value => sum += value));
    return sum;
  }

  private setUserSum(): void {
    this.userSum = new Map<number, number>();
    this.goalAddService._goal.usersGoals.forEach(item => {
      const sum = (item.intervals && item.intervals.length !== 0) ?
        item.intervals.map(i => i.expectedValue).reduce((acc, cur) => acc + Number(cur), 0) : 0;
      this.userSum.set(item.userId, sum);
    });

    this.totalSum = 0;
    this.userSum.forEach((value, key) => this.totalSum += value);
  }
}

export interface TableValueItem {
  userId: number;
  index: number;
  value: string;
}
