import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {InfinityScrollUtil, InfinityScrollUtilInterface} from '../../../../shared/infinity-scroll.util';
import {Product} from '../../../../product/product.model';
import {Observable, Subject} from 'rxjs';
import {FilterStrategy} from '../../../../shared/model';
import {PageRequest} from '../../../../shared/pagination/page.model';
import {ProductService} from '../../../../product/product.service';
import {NotificationService} from '../../../../shared/notification.service';
import {MatCheckboxChange} from '@angular/material/checkbox';


@Component({
  selector: 'app-goal-products-list-add',
  templateUrl: './goal-products-list-add.component.html',
  styleUrls: ['./goal-products-list-add.component.scss']
})
export class GoalProductsListAddComponent implements OnInit, OnDestroy, InfinityScrollUtilInterface {

  @Output() productsSelected: EventEmitter<any> = new EventEmitter<any>();
  @Input() slider: SliderComponent;

  selectedProducts: Product[] = [];
  infinityScroll: InfinityScrollUtil;
  columns = ['selectRow', 'name', 'category', 'code', 'sellPriceNet'];
  private componentDestroyed: Subject<void> = new Subject();

  @Input() set products(products: Product[]) {
    if (products) {
      this.selectedProducts = products;
    }
  }

  constructor(private productService: ProductService,
              private notificationService: NotificationService) {
    this.infinityScroll = new InfinityScrollUtil(
      this, this.componentDestroyed, this.notificationService,
      'goal.add.notification.errorWhileGettingProducts'
    );
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  cancel(): void {
    this.slider.close();
  }

  save(): void {
    this.productsSelected.emit(this.selectedProducts);
    this.slider.close();
  }

  getItemObservable(selection: FilterStrategy, pageRequest: PageRequest): Observable<any> {
    return this.productService.getProducts(pageRequest, FilterStrategy.ALL);
  }

  onProductCheckboxClick(product: Product, change: MatCheckboxChange): void {
    if (change.checked) {
      this.selectedProducts.push(product);
    } else {
      this.selectedProducts = this.selectedProducts.filter(e => e !== product);
    }
  }

  isProductCheckboxChecked(product: Product): boolean {
    if (this.selectedProducts) {
      return this.selectedProducts.indexOf(product) !== -1;
    } else {
      return false;
    }
  }
}
