import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Product} from '../../../product/product.model';
import {MatTable} from '@angular/material/table';
import {GoalAddService} from '../../goal-add.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {SliderComponent} from '../../../shared/slider/slider.component';

@Component({
  selector: 'app-goal-products-list',
  templateUrl: './goal-products-list.component.html',
  styleUrls: ['./goal-products-list.component.scss']
})
export class GoalProductsListComponent implements OnInit, OnDestroy {
  @ViewChild('productsForGoalSlider') productsForGoalSlider: SliderComponent;
  @Input() currency: string;
  products: Product[] = [];
  columns = ['delete', 'name', 'category', 'code', 'price'];
  private componentDestroyed: Subject<void> = new Subject();

  @Input() set updateProducts(products: Product[]) {
    if (products) {
      this.products = products;
    }
  }

  @Input() set update(update: boolean) {
    this._update = update;
    this.updateTableColumnsForUpdate(update);
  }
  _update = false;

  constructor(private goalAddService: GoalAddService) {
  }

  ngOnInit(): void {
    this.goalAddService.resetEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.reset()
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  delete(item: any, table: MatTable<any>): void {
    const index = this.products.indexOf(item);
    if (index !== -1) {
      this.products.splice(index, 1);
      table.renderRows();
      setTimeout(() => this.goalAddService.products = this.products.map(e => e.id), 500);
    }
  }

  onProductsSelected(products: Product[]): void {
    this.products = [...products];
    setTimeout(() => this.goalAddService.products = this.products.map(e => e.id), 500);
  }

  openSlider(): void {
    this.productsForGoalSlider.open();
    this.products = [...this.products];
  }

  private reset(): void {
    this.products = [];
  }

  private updateTableColumnsForUpdate(update: boolean): void {
    if (update) {
      this.columns = this.columns.filter(column => column !== 'delete');
    }
  }
}
