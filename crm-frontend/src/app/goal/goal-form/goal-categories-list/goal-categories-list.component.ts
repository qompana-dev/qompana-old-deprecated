import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ProductSubcategory} from '../../../product/product.model';
import {MatTable} from '@angular/material';
import {GoalAddService} from '../../goal-add.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-goal-categories-list',
  templateUrl: './goal-categories-list.component.html',
  styleUrls: ['./goal-categories-list.component.scss']
})
export class GoalCategoriesListComponent implements OnInit, OnDestroy {

  columns = ['delete', 'categoryName', 'subcategoryName'];

  @Input() set subcategories(subcategories: ProductSubcategory[]) {
    if (subcategories) {
      this._subcategories = subcategories;
    }
  }

  _subcategories: ProductSubcategory[] = [];
  subcategoriesIds: number[];
  private componentDestroyed: Subject<void> = new Subject();

  @Input() set update(update: boolean) {
    this._update = update;
    this.updateTableColumnsForUpdate(update);
  }
  _update = false;

  constructor(private goalAddService: GoalAddService) {
  }

  ngOnInit(): void {
    this.goalAddService.resetEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.reset()
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  onCategoriesSelected(subcategories: ProductSubcategory[]): void {
    this._subcategories = subcategories;
    setTimeout(() => this.setSubcategoriesIds(), 500);
  }

  private setSubcategoriesIds(): void {
    this.goalAddService.categories = (this.subcategoriesIds = this._subcategories.map(value => value.id));
  }

  removeCategory(item: ProductSubcategory, table: MatTable<any>): void {
    const index = this._subcategories.indexOf(item);
    if (index !== -1) {
      this._subcategories.splice(index, 1);
      this.setSubcategoriesIds();
      table.renderRows();
    }
  }

  private reset(): void {
    this._subcategories = [];
    this.subcategoriesIds = [];
  }

  private updateTableColumnsForUpdate(update: boolean): void {
    if (update) {
      this.columns = this.columns.filter(column => column !== 'delete');
    }
  }
}
