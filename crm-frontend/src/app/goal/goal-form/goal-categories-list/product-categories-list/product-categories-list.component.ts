import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {MatCheckbox, MatCheckboxChange} from '@angular/material';
import {Subject} from 'rxjs';
import {SliderService} from '../../../../shared/slider/slider.service';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {filter, takeUntil} from 'rxjs/operators';
import {ProductCategory, ProductSubcategory} from '../../../../product/product.model';
import {ProductCategoryService} from '../../../../product/product-category.service';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {whenSliderClosed} from '../../../../shared/util/slider.util';

@Component({
  selector: 'app-product-categories-list',
  templateUrl: './product-categories-list.component.html',
  styleUrls: ['./product-categories-list.component.scss']
})
export class ProductCategoriesListComponent implements OnInit, OnDestroy {


  categories: ProductCategory[];
  @Input() slider: SliderComponent;
  @Input() categoryIds: number[];
  @Input() subcategoryIds: number[] = [];
  @Output() categoriesSelected: EventEmitter<any> = new EventEmitter<any>();
  @ViewChildren(MatCheckbox) checkboxes: QueryList<MatCheckbox>;
  @Input() selectedSubcategoriesIds: number[] = [];
  private componentDestroyed: Subject<void> = new Subject();


  constructor(
    public categoryService: ProductCategoryService,
    public sliderService: SliderService,
    public notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.listenWhenSliderOpened();
    this.getCategories();
    this.listenWhenSliderClosed();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }


  cancel(): void {
    this.slider.close();
  }

  private clearData(): void {
    this.categoryIds = [];
    this.categories = [];
    this.subcategoryIds = [];
  }

  add(): void {
    const selected: ProductSubcategory[] = this.getSelectedSubcategories();
    this.categoriesSelected.emit(selected);
    this.slider.close();
    setTimeout(() => this.clearData(), 200);
  }


  isCheckboxChecked(categoryId: number, subcategoryId: number): boolean {
    if (this.subcategoryIds) {
      return this.subcategoryIds.includes(subcategoryId);
    } else {
      return false;
    }
  }

  private listenWhenSliderOpened(): void {
    this.sliderService.sliderStateChanged.pipe(
      takeUntil(this.componentDestroyed),
      filter(change => change.opened),
      filter(change => change.key === 'GoalCategoryList'),
    ).subscribe(() => {
      this.getCategories();
    });
  }

  private getSelectedSubcategories(): ProductSubcategory[] {
    return this.checkboxes.filter(item => item.checked && item.value as any).map(item => [item.value[0], item.value[1]])
      .map((item: any) => ({
        id: this.getSubcategory(item[0], item[1]).id,
        name: this.getSubcategory(item[0], item[1]).name,
        categoryId: this.categories[item[0]].id,
        categoryName: this.categories[item[0]].name,
        initial: this.getSubcategory(item[0], item[1]).initial
      }));
  }

  private getSubcategory(categoryIndex: number, subcategoryIndex: number): ProductSubcategory {
    return this.categories[categoryIndex].subcategories[subcategoryIndex];
  }

  private getCategories(): void {
    this.categoryService.getAllCategoriesWithSubcategories()
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      categories => {
        this.categories = categories;
        this.sortCategories(this.categories);
      },
      err => this.notificationService.notify('products.categories.notification.getCategoriesListError', NotificationType.ERROR)
    );
  }

  private listenWhenSliderClosed(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        whenSliderClosed(this.componentDestroyed, [this.slider.key]),
      ).subscribe(
      () => this.clearData()
    );
  }

  private sortCategories(categories: ProductCategory[]): void {
    categories.forEach(c => c.subcategories.sort((x, y) => x.initial ? -1 : y.initial ? 1 : 0));
  }

  onCategoryCheckboxClick(change: MatCheckboxChange, index: number): void {
    this.checkboxes.filter(checkbox => checkbox.value as any).forEach(checkbox => {
      if (checkbox.value[0] as any === index) {
        checkbox.checked = change.checked;
      }
    });
  }

  isCategoryCheckboxChecked(i: number): boolean {
    if (this.subcategoryIds) {
      const ids = this.categories[i].subcategories.map(c => c.id);
      return ids.every(id => this.subcategoryIds.includes(id));
    } else {
      return false;
    }
  }
}
