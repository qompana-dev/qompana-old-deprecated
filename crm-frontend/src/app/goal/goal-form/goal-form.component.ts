import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
// @ts-ignore
import {FormUtil} from '../../shared/form.util';
import {GoalAddService} from '../goal-add.service';
import {GoalInfoFormComponent} from './goal-info-form/goal-info-form.component';
import {GoalEditDto, GoalType} from '../goal.model';
import {FormValidators} from '../../shared/form/form-validators';

@Component({
  selector: 'app-goal-form',
  templateUrl: './goal-form.component.html',
  styleUrls: ['./goal-form.component.scss']
})
export class GoalFormComponent implements OnInit, OnDestroy {


  private componentDestroyed: Subject<void> = new Subject();

  goalForm: FormGroup;
  goalType = GoalType;

  @ViewChild('goalInfoFormComponent') goalInfoFormComponent: GoalInfoFormComponent;
  @Input() emptyCategoryListError = false;
  @Input() emptyProductListError = false;

  @Input() set goal(goal: GoalEditDto) {
    this.updateForm(goal);
    this._goal = goal;
  }

  _goal: GoalEditDto;

  @Input() set update(update: boolean) {
    this._update = update;
    if (update) {
      this.goalForm.disable();
    }
  }

  _update: boolean;

  constructor(private fb: FormBuilder,
              public goalAddService: GoalAddService) {
    this.createGoalForm();
  }

  ngOnInit(): void {
    this.goalAddService.resetEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        if (this.goalForm) {
          this.goalForm.reset();
        }
      }
    );

  }


  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  // @formatter:off
  get goalName(): AbstractControl { return this.goalForm.get('goalName'); }
  // @formatter:on

  isValid(): boolean {
    FormUtil.setTouched(this.goalForm);
    return this.goalForm.valid && this.goalInfoFormComponent.isValid();
  }

  reset(): void {
    this.goalForm.reset();
  }

  private createGoalForm(): void {
    this.goalForm = this.fb.group({
      goalName: [null, [Validators.required, FormValidators.whitespace, Validators.maxLength(199)]]
    });
    this.subscribeNameChanged();
    this.applyPermissionsToForm();
  }

  private subscribeNameChanged(): void {
    this.goalName.valueChanges
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        const goal = this.goalAddService.goal;
        goal.name = this.goalName.value;
        this.goalAddService.goal = goal;
      });
  }

  private applyPermissionsToForm(): void {
    if (this.goalForm) { // TODO
      // applyPermission(this.processDefinitionId, !this.authService.isPermissionPresent(`${this.authKey}processDefinitionField`, 'r'));
    }
  }

  private updateForm(goal: GoalEditDto): void {
    if (this.goalForm && goal) {
      this.goalName.setValue(goal.name, {emitEvent: false});
    }
  }
}
