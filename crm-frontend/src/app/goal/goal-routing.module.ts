import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../login/auth/auth-guard.service';
import {GoalListComponent} from './goal-list/goal-list.component';

const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: GoalListComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoalRoutingModule {
}
