import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {CrmObject, CrmObjectType} from '../../shared/model/search.model';
import {FileService} from '../../files/file.service';
import {InfiniteScrollUtil} from '../../shared/pagination/infinite-scroll.util';
import {filter, takeUntil, throttleTime} from 'rxjs/operators';
import {
  deleteAssociatedFileDialogData,
  deleteFileDialogData,
  FileAuthTypeEnum,
  fileDeleteErrorTypes,
  FileInfo,
  ScanStatusEnum
} from '../../files/file.model';
import {Page, PageRequest} from '../../shared/pagination/page.model';
import {CrmFileUploaderService} from '../../files/crm-file-uploader.service';
import {FileItem} from 'ng2-file-upload';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {Router} from '@angular/router';
import {AuthService} from '../../login/auth/auth.service';
import {FilePreviewService} from '../../files/file-preview.service';
import {FileAuthUtil} from "../../files/file-auth.util";
import {DeleteDialogService} from "../../shared/dialog/delete/delete-dialog.service";
import {FilePermissionUtil} from "../../files/file-permission.util";
import {LoginService} from "../../login/login.service";

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss']
})
export class FileComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();
  crmObject: CrmObject = {} as CrmObject;
  filesPage: Page<FileInfo>;
  currentPage: number;
  hasBaseDropZoneOver = false;
  progress: number;

  private loggedUserEmail;

  scanStatusEnum = ScanStatusEnum;
  @Output() fullView: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() set objectType(type: CrmObjectType) {
    this.crmObject.type = type;
    this.getAssociatedFiles();
  }

  @Input() set objectId(id: number) {
    this.crmObject.id = id;
    this.getAssociatedFiles();
  }
  @Input() readonly = false;
  @Input('open-full-view-button') openFullViewButton = true;

  constructor(
    public uploader: CrmFileUploaderService,
    private $router: Router,
    private filePreviewService: FilePreviewService,
    private authService: AuthService,
    private notificationService: NotificationService,
    private fileService: FileService,
    private loginService: LoginService,
    private deleteDialogService: DeleteDialogService) {
  }

  ngOnInit(): void {
    this.uploader.setFileAuthTypeEnum(this.getFileAuthTypeEnum());
    this.uploader.onAfterAddingAll = (files) => this.uploadAll(files);
    this.uploader.onCompleteAll = () => this.getAssociatedFiles();
    this.uploader.onProgressAll = (percent) => this.progress = percent;

    this.fileService.crmObjectFilesChanged.pipe(
        takeUntil(this.componentDestroyed),
        filter(obj => obj.type === this.crmObject.type && obj.id === this.crmObject.id)
      ).subscribe(() => this.getAssociatedFiles());

    this.loggedUserEmail = this.loginService.getLoggedAccountEmail();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  uploadAll(files: FileItem[]): void {
      this.progress = 0;
      this.uploader.uploadItems(files, { description: '', tags: [], assignedObjects: [this.crmObject] });
  }

  onScroll(e: any): void {
    if (InfiniteScrollUtil.shouldGetMoreData(e) && this.filesPage.numberOfElements < this.filesPage.totalElements &&
      this.crmObject && this.crmObject.id && this.crmObject.type) {
      const nextPage = this.currentPage + 1;
      this.getMoreFiles({ page: '' + nextPage, size: InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: 'created,desc' });
    }
  }

  removeExtension(fileName: string): string {
    const result = fileName.split('.').slice(0, -1).join('.');
    return (!result || result.length === 0) ? fileName : result;
  }

  private getAssociatedFiles(): void {
    this.progress = undefined;
    if (this.crmObject && this.crmObject.id && this.crmObject.type) {
      this.filesPage = undefined;
      this.fileService.getFilesInGroup(this.crmObject.type, this.crmObject.id,
        {page: '0', size: InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: 'created,desc'}, this.getFileAuthTypeEnum())
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((page: Page<FileInfo>) => {
            this.uploader.clearQueue();
            this.filesPage = page;
            console.log(page);
            this.currentPage = 0;
          },
          () => this.notificationService.notify('widgets.file.notification.listError', NotificationType.ERROR));
    }
  }

  private getMoreFiles(pageRequest: PageRequest): void {
    this.fileService.getFilesInGroup(this.crmObject.type, this.crmObject.id, pageRequest, this.getFileAuthTypeEnum())
      .pipe(
        takeUntil(this.componentDestroyed),
        throttleTime(500)
      ).subscribe(
        page => this.handleGetMoreFilesSuccess(page),
        () => this.notificationService.notify('widgets.file.notification.listError', NotificationType.ERROR)
      );
  }

  public preview(file: FileInfo): void {
    if (this.readonly) {
      return;
    }
    this.filePreviewService.showPreviewOf(file);
  }

  private handleGetMoreFilesSuccess(page: any): void {
    this.currentPage++;
    this.filesPage.content = this.filesPage.content.concat(page.content);
    this.filesPage.numberOfElements += page.content.length;
  }

  public openFullView(): void {
    if (this.readonly) {
      return;
    }
    this.fullView.emit(true);
  }
  private getFileAuthTypeEnum(): FileAuthTypeEnum {
    if (this.crmObject && this.crmObject.type) {
      switch (this.crmObject.type) {
        case CrmObjectType.lead:
          return FileAuthTypeEnum.leadMini;
        case CrmObjectType.opportunity:
          return FileAuthTypeEnum.opportunityMini;
        case CrmObjectType.product:
          return FileAuthTypeEnum.productMini;
      }
    }
    return undefined;
  }
  public getAuthKey(): string {
    if (this.crmObject && this.crmObject.type) {
      switch (this.crmObject.type) {
        case CrmObjectType.lead:
          return 'LeadFilesMiniComponent.';
        case CrmObjectType.opportunity:
          return 'OpportunityFilesMiniComponent.';
        case CrmObjectType.product:
          return 'ProductFilesMiniComponent.';
        default:
          return 'DocumentsListComponent.';
      }
    }
    return '';
  }

  tryDeleteSingleFile(fileId: number, dbFileId: number): void {
    deleteFileDialogData.numberToDelete = 1;
    deleteFileDialogData.onConfirmClick = () => this.checkIfAssociationsPresentAndDelete(fileId, dbFileId);
    this.openDeleteDialog();
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(deleteFileDialogData);
  }

  private checkIfAssociationsPresentAndDelete(fileId: number, dbFileId: number): void {
    this.fileService.getAssociations(dbFileId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe((associations) => {
      if (associations.length > 0) {
        this.tryDeleteAssociatedFile(fileId, associations.length);
      } else {
        this.deleteFile(fileId);
      }
    });
  }

  private tryDeleteAssociatedFile(fileId: number, numberOfAssociations: number): void {
    deleteAssociatedFileDialogData.numberToDelete = numberOfAssociations;
    deleteAssociatedFileDialogData.onConfirmClick = () => this.deleteFile(fileId);
    this.deleteDialogService.open(deleteAssociatedFileDialogData);
  }

  private deleteFile(fileId: number): void {
    this.fileService.deleteFile(fileId, FileAuthUtil.getFileAuthType(this.crmObject.type)).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      () => {
        this.getAssociatedFiles();
        this.notificationService.notify('files.notification.deleteFileSuccess', NotificationType.SUCCESS);
      },
      err => this.notificationService.notifyError(fileDeleteErrorTypes, err.status)
    );
  }

  isFileOwner(file: FileInfo) {
    return ((file && file.author) === this.loggedUserEmail);
  }
}
