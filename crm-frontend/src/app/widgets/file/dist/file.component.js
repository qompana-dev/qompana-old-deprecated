"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.FileComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var search_model_1 = require("../../shared/model/search.model");
var infinite_scroll_util_1 = require("../../shared/pagination/infinite-scroll.util");
var operators_1 = require("rxjs/operators");
var file_model_1 = require("../../files/file.model");
var notification_service_1 = require("../../shared/notification.service");
var FileComponent = /** @class */ (function () {
    function FileComponent(uploader, $router, filePreviewService, authService, notificationService, fileService) {
        this.importService = uploader;
        this.$router = $router;
        this.filePreviewService = filePreviewService;
        this.authService = authService;
        this.notificationService = notificationService;
        this.fileService = fileService;
        this.componentDestroyed = new rxjs_1.Subject();
        this.crmObject = {};
        this.hasBaseDropZoneOver = false;
        this.fullView = new core_1.EventEmitter();
        this.readonly = false;
        this.openFullViewButton = true;
    }
    Object.defineProperty(FileComponent.prototype, "objectType", {
        set: function (type) {
            this.crmObject.type = type;
            this.getAssociatedFiles();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(FileComponent.prototype, "objectId", {
        set: function (id) {
            this.crmObject.id = id;
            this.getAssociatedFiles();
        },
        enumerable: false,
        configurable: true
    });
    FileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.uploader.setFileAuthTypeEnum(this.getFileAuthTypeEnum());
        this.uploader.onAfterAddingAll = function (files) { return _this.uploadAll(files); };
        this.uploader.onCompleteAll = function () { return _this.getAssociatedFiles(); };
        this.uploader.onProgressAll = function (percent) { return _this.progress = percent; };
    };
    FileComponent.prototype.ngOnDestroy = function () {
        this.componentDestroyed.next();
        this.componentDestroyed.complete();
    };
    FileComponent.prototype.uploadAll = function (files) {
        if (this.authService.isPermissionPresent(this.getAuthKey() + 'showMore', 'w')) {
            this.progress = 0;
            this.uploader.uploadItems(files, { description: '', tags: [], assignedObjects: [this.crmObject] });
        }
    };
    FileComponent.prototype.onScroll = function (e) {
        if (infinite_scroll_util_1.InfiniteScrollUtil.shouldGetMoreData(e) && this.filesPage.numberOfElements < this.filesPage.totalElements &&
            this.crmObject && this.crmObject.id && this.crmObject.type) {
            var nextPage = this.currentPage + 1;
            this.getMoreFiles({ page: '' + nextPage, size: infinite_scroll_util_1.InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: 'created,desc' });
        }
    };
    FileComponent.prototype.removeExtension = function (fileName) {
        var result = fileName.split('.').slice(0, -1).join('.');
        return (!result || result.length === 0) ? fileName : result;
    };
    FileComponent.prototype.getAssociatedFiles = function () {
        var _this = this;
        this.progress = undefined;
        if (this.crmObject && this.crmObject.id && this.crmObject.type) {
            this.filesPage = undefined;
            this.fileService.getFilesInGroup(this.crmObject.type, this.crmObject.id, { page: '0', size: infinite_scroll_util_1.InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: 'created,desc' }, this.getFileAuthTypeEnum())
                .pipe(operators_1.takeUntil(this.componentDestroyed))
                .subscribe(function (page) {
                _this.uploader.clearQueue();
                _this.filesPage = page;
                _this.currentPage = 0;
            }, function () { return _this.notificationService.notify('widgets.file.notification.listError', notification_service_1.NotificationType.ERROR); });
        }
    };
    FileComponent.prototype.getMoreFiles = function (pageRequest) {
        var _this = this;
        this.fileService.getFilesInGroup(this.crmObject.type, this.crmObject.id, pageRequest, this.getFileAuthTypeEnum())
            .pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.throttleTime(500)).subscribe(function (page) { return _this.handleGetMoreFilesSuccess(page); }, function () { return _this.notificationService.notify('widgets.file.notification.listError', notification_service_1.NotificationType.ERROR); });
    };
    FileComponent.prototype.preview = function (file) {
        if (this.readonly) {
            return;
        }
        this.filePreviewService.showPreviewOf(file);
    };
    FileComponent.prototype.handleGetMoreFilesSuccess = function (page) {
        this.currentPage++;
        this.filesPage.content = this.filesPage.content.concat(page.content);
        this.filesPage.numberOfElements += page.content.length;
    };
    FileComponent.prototype.openFullView = function () {
        if (this.readonly) {
            return;
        }
        this.fullView.emit(true);
    };
    FileComponent.prototype.getFileAuthTypeEnum = function () {
        if (this.crmObject && this.crmObject.type) {
            switch (this.crmObject.type) {
                case search_model_1.CrmObjectType.lead:
                    return file_model_1.FileAuthTypeEnum.leadMini;
                case search_model_1.CrmObjectType.opportunity:
                    return file_model_1.FileAuthTypeEnum.opportunityMini;
                case search_model_1.CrmObjectType.product:
                    return file_model_1.FileAuthTypeEnum.productMini;
            }
        }
        return undefined;
    };
    FileComponent.prototype.getAuthKey = function () {
        if (this.crmObject && this.crmObject.type) {
            switch (this.crmObject.type) {
                case search_model_1.CrmObjectType.lead:
                    return 'LeadFilesMiniComponent.';
                case search_model_1.CrmObjectType.opportunity:
                    return 'OpportunityFilesMiniComponent.';
                case search_model_1.CrmObjectType.product:
                    return 'ProductFilesMiniComponent.';
            }
        }
        return '';
    };
    __decorate([
        core_1.Output()
    ], FileComponent.prototype, "fullView");
    __decorate([
        core_1.Input()
    ], FileComponent.prototype, "objectType");
    __decorate([
        core_1.Input()
    ], FileComponent.prototype, "objectId");
    __decorate([
        core_1.Input()
    ], FileComponent.prototype, "readonly");
    __decorate([
        core_1.Input('open-full-view-button')
    ], FileComponent.prototype, "openFullViewButton");
    FileComponent = __decorate([
        core_1.Component({
            selector: 'app-file',
            templateUrl: './file.component.html',
            styleUrls: ['./file.component.scss']
        })
    ], FileComponent);
    return FileComponent;
}());
exports.FileComponent = FileComponent;
