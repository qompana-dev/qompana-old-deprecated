import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FileComponent} from './file/file.component';
import {SharedModule} from '../shared/shared.module';
import {NotesComponent} from './notes/notes.component';
import {PersistNoteDialogComponent} from './notes/persist-note-dialog/persist-note-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {QuillModule} from 'ngx-quill';

@NgModule({
  declarations: [
    FileComponent,
    NotesComponent,
    PersistNoteDialogComponent,
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    SharedModule,
    QuillModule.forRoot(),
  ],
  exports: [
    FileComponent,
    NotesComponent,
    PersistNoteDialogComponent
  ],
  entryComponents: [PersistNoteDialogComponent]
})
export class WidgetModule {
}
