import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AbstractControl, FormBuilder, FormGroup} from '@angular/forms';
import {PersistNoteDialogEditorConfig} from './persist-note-dialog-editor.config';
import {QuillEditorComponent} from 'ngx-quill';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
  selector: 'app-persist-note-dialog',
  templateUrl: './persist-note-dialog.component.html',
  styleUrls: ['./persist-note-dialog.component.scss']
})
export class PersistNoteDialogComponent implements OnInit {
  form: FormGroup;
  @ViewChild('editor') editor: QuillEditorComponent;
  editorConfig = PersistNoteDialogEditorConfig.config;
  text: string;
  maxLenght: number = 3000;

  constructor(@Inject(MAT_DIALOG_DATA) public data: PersistNoteDialogData,
              private dialogRef: MatDialogRef<any>,
              private fb: FormBuilder) {
    this.form = fb.group({
      editor: [data.formattedContent]
    });
  }

  ngOnInit(): void {
    this.form
      .controls
      .editor
      .valueChanges.pipe(
      debounceTime(400),
      distinctUntilChanged()
    )
      .subscribe((data) => {

      });

    this.editor
      .onContentChanged
      .pipe(
        debounceTime(400),
        distinctUntilChanged()
      )
      .subscribe((data) => {
        this.text = data.text;
      });
  }

  get content(): AbstractControl {
    return this.form.get('editor');
  }

  cancel(): void {
    if (this.data.onCancelClick) {
      this.data.onCancelClick();
    }

  }

  confirm(): void {
    if (this.data.onConfirmClick) {
      this.data.onConfirmClick(this.getPlainText(this.content.value), this.content.value);
      this.dialogRef.close();
    }
  }

  getPlainText(content: any): string {
    const oParser = new DOMParser();
    const oDOM = oParser.parseFromString(content, 'text/html');
    return this.trim(oDOM.body.innerText);

  }

  trim(text: string): string {
    return text && text.trim();
  }


}

export interface PersistNoteDialogData {
  title: string;
  formattedContent: string;
  placeholder: string;
  confirmButton: string;
  cancelButton: string;
  onConfirmClick: (plainText: string, htmlText: string) => void;
  onCancelClick: () => void;
}
