import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {SvgIconRegistryService} from 'angular-svg-icon';
import {PersistNoteDialogComponent, PersistNoteDialogData} from './persist-note-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class PersistNoteDialogService {

  constructor(private dialog: MatDialog,
              private iconReg: SvgIconRegistryService) {
    this.iconReg.loadSvg('/assets/icons/small-cross.svg', 'small-cross');
  }

  open(data: Partial<PersistNoteDialogData>): void {
    this.dialog.open(PersistNoteDialogComponent, {
      width: '600px', height: '450px', data,
      panelClass: 'select-item-dialog-container'
    });
  }
}
