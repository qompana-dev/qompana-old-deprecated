export class PersistNoteDialogEditorConfig {
  static config = {
    toolbar: [['bold', 'italic', 'underline', 'strike'],
        [{'list': 'ordered'}, {'list': 'bullet'}],
        [{'color': []}, {'background': []}]
      ]
  };
}
