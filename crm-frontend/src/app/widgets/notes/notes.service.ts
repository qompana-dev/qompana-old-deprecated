import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Note, ObjectTypeAndId} from './notes.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {EmptyPage, Page, PageRequest} from '../../shared/pagination/page.model';
import {CrmObject} from '../../shared/model/search.model';
import {UrlUtil} from '../../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  constructor(private http: HttpClient) {
  }

  getNotes(objectTypeAndId: ObjectTypeAndId): Observable<Note[]> {
    const url = `${UrlUtil.url}/crm-association-service/note/type/${objectTypeAndId.objectType}/id/${objectTypeAndId.objectId}`;
    return this.http.get<Note[]>(url);
  }

  getNotesAsPage(pageRequest: PageRequest, crmObject: CrmObject): Observable<Page<Note>> {
    if (!crmObject) {
      return of(new EmptyPage());
    }
    const params = new HttpParams({fromObject: pageRequest as any});
    const url = `${UrlUtil.url}/crm-association-service/note/type/${crmObject.type}/id/${crmObject.id}/page`;
    return this.http.get<Page<Note>>(url, {params});
  }

  deleteNote(noteId: number): Observable<void> {
    return this.http.delete<void>(`${UrlUtil.url}/crm-association-service/note/${noteId}`);
  }

  saveNote(objectTypeAndId: ObjectTypeAndId, plainText: string, formattedText: string): Observable<Note> {
    return this.http.post<Note>(`${UrlUtil.url}/crm-association-service/note`, {
      objectType: objectTypeAndId.objectType,
      objectId: objectTypeAndId.objectId,
      content: plainText,
      formattedContent: formattedText
    });
  }

  updateNote(objectTypeAndId: ObjectTypeAndId, noteId: number, plainText: string, formattedText: string): Observable<Note> {
    return this.http.put<Note>(`${UrlUtil.url}/crm-association-service/note/${noteId}`, {
      objectType: objectTypeAndId.objectType,
      objectId: objectTypeAndId.objectId,
      content: plainText,
      formattedContent: formattedText
    });
  }
}
