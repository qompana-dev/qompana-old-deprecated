import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { Subject } from "rxjs";
import {
  debounceTime,
  filter,
  finalize,
  switchMap,
  takeUntil,
  tap,
} from "rxjs/operators";
import {
  ErrorTypes,
  NotificationService,
  NotificationType,
} from "../../shared/notification.service";
import { DeleteDialogService } from "../../shared/dialog/delete/delete-dialog.service";
import { DeleteDialogData } from "../../shared/dialog/delete/delete-dialog.component";
import { Note, ObjectTypeAndId } from "./notes.model";
import { NotesService } from "./notes.service";
import { CrmObjectType } from "../../shared/model/search.model";
import { PersistNoteDialogService } from "./persist-note-dialog/persist-note-dialog.service";
import { PersistNoteDialogData } from "./persist-note-dialog/persist-note-dialog.component";
import { AbstractControl, FormControl, FormGroup } from "@angular/forms";
import { NotesEditorConfig } from "./notes-editor.config";

@Component({
  selector: "app-notes",
  templateUrl: "./notes.component.html",
  styleUrls: ["./notes.component.scss"],
})
export class NotesComponent implements OnInit, OnDestroy {
  searchTextForm: FormGroup = new FormGroup({
    sortBy: new FormControl("desc"),
    createdBy: new FormControl(""),
    searchText: new FormControl(""),
  });
  filteredTypes: string[] = ["desc", "asc", "user"];
  editorConfig = NotesEditorConfig.config;
  _objectTypeAndId: ObjectTypeAndId;
  searchBool = false;
  notes: Note[];
  @Input() readonly = false;
  @Input() userMap: Map<number, string> = new Map();

  @Input() set objectTypeAndId(objectTypeAndId: ObjectTypeAndId) {
    if (
      objectTypeAndId &&
      objectTypeAndId.objectType &&
      objectTypeAndId.objectId
    ) {
      this._objectTypeAndId = objectTypeAndId;
      this.loadCustomerNotes(objectTypeAndId);
    }
  }

  private componentDestroyed: Subject<void> = new Subject();

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: "widgets.notes.remove.title",
    description: "widgets.notes.remove.description",
    confirmButton: "widgets.notes.remove.yes",
    cancelButton: "widgets.notes.remove.no",
  };
  private readonly persistNoteDialogData: Partial<PersistNoteDialogData> = {
    confirmButton: "widgets.notes.save",
    cancelButton: "widgets.notes.cancel",
  };

  readonly deleteNoteErrors: ErrorTypes = {
    base: "widgets.notes.remove.",
    defaultText: "undefinedError",
    errors: [
      {
        code: 404,
        text: "notFound",
      },
      {
        code: 403,
        text: "noPermission",
      },
    ],
  };

  readonly saveNoteErrors: ErrorTypes = {
    base: "widgets.notes.notification.",
    defaultText: "unknownError",
    errors: [
      {
        code: 404,
        text: "saveCustomerNotFound",
      },
      {
        code: 403,
        text: "noPermission",
      },
    ],
  };

  readonly updateNoteErrors: ErrorTypes = {
    base: "widgets.notes.notification.",
    defaultText: "unknownError",
    errors: [
      {
        code: 404,
        text: "updateNoteNotFound",
      },
      {
        code: 403,
        text: "noPermission",
      },
    ],
  };

  // @formatter:off
  get sortBy(): AbstractControl {
    return this.searchTextForm.get("sortBy");
  }

  get createdBy(): AbstractControl {
    return this.searchTextForm.get("createdBy");
  }

  get searchText(): AbstractControl {
    return this.searchTextForm.get("searchText");
  }

  // @formatter:on

  constructor(
    public notesService: NotesService,
    private notificationService: NotificationService,
    private deleteDialogService: DeleteDialogService,
    private persistNoteDialogService: PersistNoteDialogService
  ) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  get authKey(): string {
    if (this._objectTypeAndId && this._objectTypeAndId.objectType) {
      switch (this._objectTypeAndId.objectType) {
        case CrmObjectType.customer:
          return "CustomerNotes.";
        case CrmObjectType.opportunity:
          return "OpportunityNotes.";
        case CrmObjectType.lead:
          return "LeadNotes.";
        case CrmObjectType.contact:
          return "ContactNotes.";
        case CrmObjectType.user:
          return "UserNotes.";
      }
    } else {
      return "";
    }
  }

  cancel(): void {}

  save(plainText: string, htmlText: string): void {
    if (!this.trim(plainText)) {
      return;
    }
    if (plainText.length > 3000) {
      this.notificationService.notify(
        "widgets.notes.notification.maxLengthExceeded",
        NotificationType.ERROR
      );
      return;
    }
    this.notesService
      .saveNote(this._objectTypeAndId, plainText, htmlText)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (note) => {
          this.loadCustomerNotes(this._objectTypeAndId);
          this.notificationService.notify(
            "widgets.notes.notification.saveSuccess",
            NotificationType.SUCCESS
          );
        },
        (err) =>
          this.notificationService.notifyError(
            this.saveNoteErrors,
            NotificationType.ERROR
          )
      );
  }

  updateNote(note: Note, plainText: string, htmlText: string): void {
    if (!this.trim(plainText)) {
      return;
    }
    if (plainText.length > 3000) {
      this.notificationService.notify(
        "widgets.notes.notification.maxLengthExceeded",
        NotificationType.ERROR
      );
      return;
    }
    this.notesService
      .updateNote(this._objectTypeAndId, note.id, plainText, htmlText)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (updatedNote) => {
          this.updateNoteOnNotesList(note, updatedNote);
          this.notificationService.notify(
            "widgets.notes.notification.updateSuccess",
            NotificationType.SUCCESS
          );
        },
        (err) =>
          this.notificationService.notifyError(
            this.updateNoteErrors,
            err.status
          )
      );
  }

  tryDeleteNote(note: Note): void {
    this.deleteDialogData.onConfirmClick = () => this.deleteNote(note);
    this.openDeleteDialog();
  }

  trim(text: string): string {
    return text && text.trim();
  }

  private loadCustomerNotes(objectTypeAndId: ObjectTypeAndId): void {
    this.notesService
      .getNotes(objectTypeAndId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((notes) => (this.notes = notes));
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private deleteNote(note: Note): void {
    this.notesService
      .deleteNote(note.id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.removeFromNotesList(note);
          this.notificationService.notify(
            "widgets.notes.remove.successfullyRemoved",
            NotificationType.SUCCESS
          );
        },
        (err) =>
          this.notificationService.notifyError(
            this.deleteNoteErrors,
            err.status
          )
      );
  }

  private removeFromNotesList(note: Note): void {
    const index = this.notes.indexOf(note);
    if (index > -1) {
      this.notes.splice(index, 1);
    }
  }

  private updateNoteOnNotesList(oldNote: Note, newNote: Note): void {
    const index = this.notes.indexOf(oldNote);
    if (index > -1) {
      Object.assign(this.notes[index], newNote);
    }
  }

  openAddDialog(): void {
    this.persistNoteDialogData.title = "widgets.notes.dialog.add";
    this.persistNoteDialogData.formattedContent = null;
    this.persistNoteDialogData.onConfirmClick = (plainText, htmlText) => {
      this.save(plainText, htmlText);
    };
    this.persistNoteDialogService.open(this.persistNoteDialogData);
  }

  openEditDialog(note: Note): void {
    this.persistNoteDialogData.title = "widgets.notes.dialog.edit";
    this.persistNoteDialogData.formattedContent = note.formattedContent;
    this.persistNoteDialogData.onConfirmClick = (plainText, htmlText) => {
      this.updateNote(note, plainText, htmlText);
    };
    this.persistNoteDialogService.open(this.persistNoteDialogData);
  }

  showSearchForm(event: MouseEvent): void {
    this.searchBool = !this.searchBool;
    event.stopPropagation();
  }
}
