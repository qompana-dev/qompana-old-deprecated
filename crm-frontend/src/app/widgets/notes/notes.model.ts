import {CrmObject, CrmObjectType} from '../../shared/model/search.model';

export class Note {
  id: number;
  objectType: CrmObjectType;
  objectId: number;
  created: string;
  updated: string;
  content: string;
  formattedContent: string;
  updatedBy: number;
  createdBy: number;
}

export class ObjectTypeAndId {
  objectType: CrmObjectType;
  objectId: number;

  constructor(crmObject: CrmObject) {
    if (crmObject) {
      this.objectId = crmObject.id;
      this.objectType = crmObject.type;
    }
  }
}
