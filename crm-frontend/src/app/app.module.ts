import { ConfirmDialogModule } from './shared/dialog/confirm-dialog/confirm-dialog.module';
import { IconModule } from './shared/icon/icon.module';
import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {NavigationComponent} from './navigation/navigation.component';
import {AuthInterceptor} from './login/auth/auth.interceptor';
import {AuthGuard} from './login/auth/auth-guard.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SidenavComponent} from './navigation/sidenav/sidenav.component';
import {NgxMaskModule} from 'ngx-mask';
import {StompConfig, StompService} from '@stomp/ng2-stompjs';
import {StompConfiguration} from './shared/config/stomp-config';
import {SharedModule} from './shared/shared.module';
import {LoginComponent} from './login/login.component';
import {MainPageComponent} from './main-page/main-page.component';
import {MOMENT} from 'angular-calendar';
import { DragDropModule } from '@angular/cdk/drag-drop';
// @ts-ignore
import moment from 'moment';
import {MainSearchComponent} from './navigation/main-search/main-search.component';
import {FilesModule} from './files/files.module';
import localeFr from '@angular/common/locales/fr';
import {registerLocaleData} from '@angular/common';
import {ExpenseSliderModule} from './shared/tabs/expense-tab/expense-slider/expense-slider.module';
import {MailSelectFileDialogComponent} from './mail/mail-main-page/new-mail/mail-select-file-dialog/mail-select-file-dialog.component';
import {MailModule} from './mail/mail.module';
import {DragulaModule} from 'ng2-dragula';
import {AvatarModule} from './shared/avatar-form/avatar.module';
import {WidgetModule} from './widgets/widget.module';
import {ClipboardModule} from 'ngx-clipboard';
import {NotificationModule} from './notification/notification.module';

registerLocaleData(localeFr, 'fr');

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        MainPageComponent,
        SidenavComponent,
        NavigationComponent,
        MainSearchComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        SharedModule,
        FilesModule,
        MailModule,
        NotificationModule,
        AvatarModule,
        HttpClientModule,
        BrowserAnimationsModule,
        WidgetModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        NgxMaskModule.forRoot(),
        ExpenseSliderModule,
        DragulaModule.forRoot(),
        DragDropModule,
        IconModule,
        ConfirmDialogModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
        StompService,
        {provide: StompConfig, useValue: StompConfiguration.stompConfig},
        AuthGuard,
        {provide: MOMENT, useValue: moment}
    ],
    bootstrap: [AppComponent],
    entryComponents: [MailSelectFileDialogComponent]
})
export class AppModule {
}
