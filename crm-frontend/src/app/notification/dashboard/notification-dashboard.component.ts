import {
  Component,
  ComponentFactoryResolver,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {
  NotificationDto,
  NotificationDtoFactory,
  NotificationFilter,
  NotificationTimeoutConfigurationFactory
} from "../notification.model";
import {NotificationElementComponent} from "../element/notification-element.component";
import {UserNotificationService} from "../../navigation/user-notification/user-notification.service";
import {takeUntil} from "rxjs/operators";
import {UserNotification} from "../../navigation/user-notification/user-notification.model";
import {WebsocketService} from "../../shared/websocket.service";
import {LoginService} from "../../login/login.service";
import {NotificationType} from "../../shared/notification.service";

@Component({
  selector: 'notification-dashboard',
  templateUrl: './notification-dashboard.component.html',
  styleUrls: ['./notification-dashboard.component.scss'],
  host: {'class': 'notification-dashboard'}
})
export class NotificationDashboardComponent implements OnInit, OnDestroy {

  private componentDestroyed: Subject<void> = new Subject();

  @Input() notificationDto: NotificationDto;

  @Output() onCloseClick: EventEmitter<void> = new EventEmitter();

  private componentsId: number = 0;
  private notifications = [];
  @ViewChild('notificationsContainer', {read: ViewContainerRef}) notificationsContainer;

  private allUserNotifications: UserNotification[] = [];

  private websocketNotificationChangeSubscription: Subscription;

  private notificationFilter: NotificationFilter = NotificationFilter.THE_NEWEST;

  successNotificationsCount: number = 0;
  infoNotificationsCount: number = 0;
  alertNotificationsCount: number = 0;
  warningNotificationsCount: number = 0;

  constructor(private el: ElementRef,
              private componentFactoryResolver: ComponentFactoryResolver,
              private userNotificationService: UserNotificationService,
              private websocketService: WebsocketService,
              private loginService: LoginService) {
  }

  ngOnInit(): void {
    this.updateUser();
    this.loginService.loggedUserChange
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.updateUser());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  close(): void {
    this.onCloseClick.emit();
  }

  getTheNewest(event): void {
    this.changeActiveTab(event);
    this.notificationFilter = NotificationFilter.THE_NEWEST;
    this.changeFilter();
  }

  getHighPriority(event): void {
    this.changeActiveTab(event);
    this.notificationFilter = NotificationFilter.HIGH_PRIORITY;
    this.changeFilter();
  }

  getRead(event): void {
    this.changeActiveTab(event);
    this.notificationFilter = NotificationFilter.READ;
    this.changeFilter();
  }

  private changeFilter(): void {
    this.clearNotifications();
    let notifications = [];
    switch(this.notificationFilter){
      case NotificationFilter.THE_NEWEST:
        notifications = this.allUserNotifications.filter(e => !e.read);
        break;
      case NotificationFilter.HIGH_PRIORITY:
        notifications = this.allUserNotifications.filter(e => !e.read && (e.notification.type === NotificationType[NotificationType.ERROR] || e.notification.type === NotificationType[NotificationType.WARNING]));
        break;
      case NotificationFilter.READ:
        notifications = this.allUserNotifications.filter(e => e.read);
        break;
    }
    this.changeNotifications(notifications);
  }

  private addNotification(userNotification: UserNotification): void {
    const factory = this.componentFactoryResolver.resolveComponentFactory(NotificationElementComponent);
    const componentRef = this.notificationsContainer.createComponent(factory, 0);

    const closeable = !(this.notificationFilter === NotificationFilter.READ);

    componentRef.instance.notificationDto = NotificationDtoFactory.create(userNotification, closeable);
    componentRef.instance.notificationTimeoutConfiguration = NotificationTimeoutConfigurationFactory.createNoAutoClose();
    componentRef.instance.componentId = this.componentsId;

    componentRef.instance.onCloseClick.subscribe(() => {
      this.markUserNotificationAsRead(userNotification);
      this.notifications = this.notifications.filter(word => word.componentId !== componentRef.instance.componentId);
      componentRef.destroy();
    });

    componentRef.instance.onDestroy.subscribe(() => {
      this.notifications = this.notifications.filter(word => word.componentId !== componentRef.instance.componentId);
      componentRef.destroy();
    });

    componentRef.instance.onRunAction.subscribe(action => {
      this.userNotificationService.runAction(userNotification, action)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => this.getNotifications());
    });

    this.notifications.push(componentRef.instance);
    componentRef.changeDetectorRef.detectChanges();
    this.componentsId++;
  }

  private clearNotifications() {
    this.notifications.forEach(notification => notification.destroy());
  }

  private changeNotifications(allUserNotifications: UserNotification[]) {
    this.clearNotifications();
    allUserNotifications.forEach(notification => {
      this.addNotification(notification);
    });
  }

  private getNotifications(): void {
    this.userNotificationService.getNotifications()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((allUserNotifications: UserNotification[]) => {
        this.allUserNotifications = allUserNotifications;

        this.successNotificationsCount = allUserNotifications.filter(e => !e.read && e.notification.type === NotificationType[NotificationType.SUCCESS]).length;
        this.infoNotificationsCount = allUserNotifications.filter(e => !e.read && e.notification.type === NotificationType[NotificationType.INFO]).length;
        this.warningNotificationsCount = allUserNotifications.filter(e => !e.read && e.notification.type === NotificationType[NotificationType.WARNING]).length;
        this.alertNotificationsCount = allUserNotifications.filter(e => !e.read && e.notification.type === NotificationType[NotificationType.ERROR]).length;

        this.changeFilter();
      });
  }

  private updateUser(): void {
    if (this.websocketNotificationChangeSubscription) {
      this.websocketNotificationChangeSubscription.unsubscribe();
    }

    this.getNotifications();

    this.websocketNotificationChangeSubscription = this.websocketService
      .listenNotificationChange(localStorage.getItem('loggedAccountId'))
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.getNotifications());
  }

  private markUserNotificationAsRead(userNotification: UserNotification): void {
    this.userNotificationService.markAsRead(userNotification)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((result: UserNotification) => {
        this.getNotifications();
      });
  }

  private changeActiveTab(event) {
    this.el.nativeElement.querySelectorAll(".tab").forEach(
      question => {
        question.classList.remove('active');
        event.srcElement.classList.add("active");
      }
    );
  }

  private markAllUserNotificationAsRead() {
    this.userNotificationService.markUserAllAsRead()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        result => this.getNotifications()
      );
  }
}
