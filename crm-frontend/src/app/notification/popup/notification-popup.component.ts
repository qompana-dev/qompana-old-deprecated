import {
  Component,
  ComponentFactoryResolver,
  EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import {Subject} from 'rxjs';
import {NotificationDto, NotificationTimeoutConfiguration} from "../notification.model";
import {NotificationType} from "../../shared/notification.service";
import {NotificationPopupService} from "./notification-popup.service";

@Component({
  selector: 'notification-popup',
  templateUrl: './notification-popup.component.html',
  styleUrls: ['./notification-popup.component.scss'],
  host: {'class': 'notification-popup'}
})
export class NotificationPopupComponent implements OnInit, OnDestroy {

  private componentDestroyed: Subject<void> = new Subject();

  @Output() onCloseClick: EventEmitter<void> = new EventEmitter();

  private isHideSelected = true;
  @HostBinding('class.hide') get hideClass() {
    return this.isHideSelected;
  }

  constructor(private notificationPopupService: NotificationPopupService) {
    this.notificationPopupService.showListen().subscribe(() => {
      this.show();
    });
    this.notificationPopupService.hideListen().subscribe(() => {
      this.hide();
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.notificationPopupService.complete();
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  close(): void {
    this.hide();
    this.onCloseClick.emit();
  }

  private show() {
    this.isHideSelected = false;
  }

  private hide() {
    this.isHideSelected = true;
  }
}
