import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class NotificationPopupService {

  private _showListeners = new Subject<any>();
  private _hideListeners = new Subject<any>();


  showListen(): Observable<any> {
    return this._showListeners.asObservable();
  }

  hideListen(): Observable<any> {
    return this._hideListeners.asObservable();
  }

  show() {
    this._showListeners.next();
  }

  hide() {
    this._hideListeners.next();
  }

  complete() {
    this._showListeners.complete();
    this._hideListeners.complete();
  }
}
