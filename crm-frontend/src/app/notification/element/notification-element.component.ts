import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {NotificationDto, NotificationTimeoutConfiguration} from "../notification.model";
import {NotificationType} from "../../shared/notification.service";

@Component({
  selector: 'notification-element',
  templateUrl: './notification-element.component.html',
  styleUrls: ['./notification-element.component.scss'],
  host: {'class': 'notification-element'}
})
export class NotificationElementComponent implements OnInit, OnDestroy {

  private componentDestroyed: Subject<void> = new Subject();

  @Input() notificationDto: NotificationDto;

  @Input() set notificationTimeoutConfiguration(notificationTimeoutConfiguration: NotificationTimeoutConfiguration) {
      this.setNotificationTimeout(notificationTimeoutConfiguration);
  }

  @Output() onCloseClick: EventEmitter<void> = new EventEmitter();
  @Output() onCloseAllClick: EventEmitter<void> = new EventEmitter();
  @Output() onDestroy: EventEmitter<void> = new EventEmitter();
  @Output() onRunAction: EventEmitter<NotificationAction> = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  close(): void {
    this.onCloseClick.emit();
  }

  closeAll(): void {
    this.onCloseAllClick.emit();
  }

  destroy(): void {
    this.onDestroy.emit();
  }

  getNotificationType(type: NotificationType) {
    return NotificationType[type].toLowerCase();
  }

  runAction(action: NotificationAction): void {
    this.onRunAction.emit(action);
  }

  private setNotificationTimeout(notificationTimeoutConfiguration: NotificationTimeoutConfiguration) {
    if(notificationTimeoutConfiguration.autoClose) {
      setTimeout(() => {
        this.close();
      }, notificationTimeoutConfiguration.toastTime);
    }
  }
}
