import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {NgxMaskModule} from 'ngx-mask';
import {NotificationQueueComponent} from "./queue/notification-queue.component";
import {NotificationElementComponent} from "./element/notification-element.component";
import {NotificationDashboardComponent} from "./dashboard/notification-dashboard.component";
import {NotificationPopupComponent} from "./popup/notification-popup.component";

@NgModule({
  declarations: [
    NotificationQueueComponent,
    NotificationDashboardComponent,
    NotificationPopupComponent,
    NotificationElementComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxMaskModule
  ],
  exports: [
    NotificationQueueComponent,
    NotificationDashboardComponent,
    NotificationPopupComponent,
    NotificationElementComponent
  ],
  entryComponents: [
    NotificationElementComponent
  ]
})
export class NotificationModule {
}
