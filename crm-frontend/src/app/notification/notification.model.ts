import {NotificationType} from '../shared/notification.service';
import {environment} from '../../environments/environment';
import {
  NotificationAction,
  NotificationMessageParam,
  UserNotification
} from '../navigation/user-notification/user-notification.model';


export class NotificationDto {
  title: string;
  content: string;
  date: number | string;
  type: NotificationType;
  position: NotificationPosition = NotificationPosition.POPUP;
  args?: NotificationMessageParam[];
  closeable: boolean;
  groupNotification: boolean;
  actions: NotificationAction[];
}

export class NotificationDtoFactory {
  static create(userNotification: UserNotification, closeable: boolean) {
    const notificationDto = new NotificationDto();

    notificationDto.title = userNotification.notification.name + 'Title';
    notificationDto.content = userNotification.notification.name;
    notificationDto.args = this.getArgs(userNotification);
    notificationDto.date = userNotification.created;
    notificationDto.type = this.getNotificationType(userNotification);
    notificationDto.position = NotificationPosition.USER;
    notificationDto.closeable = closeable;
    notificationDto.groupNotification = userNotification.notification.groupNotification;
    notificationDto.actions = userNotification.notification.actions;

    return notificationDto;
  }

  private static getArgs(userNotification: UserNotification): any {
    const result = {};
    userNotification.messageParams.forEach(item => result[item.key] = item.value);
    return result;
  }

  private static getNotificationType(userNotification: UserNotification): NotificationType {
    switch (userNotification.notification.type) {
      case 'SUCCESS':
        return NotificationType.SUCCESS;
      case 'INFO':
        return NotificationType.INFO;
      case 'ERROR':
        return NotificationType.ERROR;
      case 'WARNING':
        return NotificationType.WARNING;
    }
  }
}

export class NotificationTimeoutConfiguration {
  autoClose = false;
  toastTime = 0;
}

export class NotificationTimeoutConfigurationFactory {
  static create(type: NotificationType) {
    let configuration = new NotificationTimeoutConfiguration();

    if (type === NotificationType.SUCCESS) {
      configuration.autoClose = true;
      configuration.toastTime = environment.toastTime;
      return configuration;
    }

    return configuration;
  }

  static createNoAutoClose() {
    return new NotificationTimeoutConfiguration();
  }
}

export enum NotificationFilter {
  THE_NEWEST,
  HIGH_PRIORITY,
  READ
}

export enum NotificationPosition {
  POPUP,
  USER
}
