import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import {NotificationDto} from "../notification.model";

@Injectable({
  providedIn: 'root'
})
export class NotificationQueueServiceService {

  private _addListeners = new Subject<any>();
  private _showListeners = new Subject<any>();
  private _hideListeners = new Subject<any>();

  addListen(): Observable<any> {
    return this._addListeners.asObservable();
  }

  showListen(): Observable<any> {
    return this._showListeners.asObservable();
  }

  hideListen(): Observable<any> {
    return this._hideListeners.asObservable();
  }

  add(notification: NotificationDto) {
    this._addListeners.next(notification);
  }

  show() {
    this._showListeners.next();
  }

  hide() {
    this._hideListeners.next();
  }

  complete() {
    this._addListeners.complete();
    this._showListeners.complete();
    this._hideListeners.complete();
  }
}
