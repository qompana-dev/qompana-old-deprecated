import {
  Component,
  ComponentFactoryResolver,
  HostBinding,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {Subject} from 'rxjs';
import {NotificationDto, NotificationTimeoutConfigurationFactory} from "../notification.model";
import {NotificationElementComponent} from "../element/notification-element.component";
import {NotificationQueueServiceService} from "./notification-queue.service";

@Component({
  selector: 'notification-queue',
  templateUrl: './notification-queue.component.html',
  styleUrls: ['./notification-queue.component.scss'],
  host: {
    'class': 'notification-queue'
  }
})
export class NotificationQueueComponent implements OnInit, OnDestroy {

  private componentDestroyed: Subject<void> = new Subject();

  private isHideSelected: boolean;
  @HostBinding('class.hide') get hideClass() {
    return this.isHideSelected;
  }

  private componentsId = 0;
  private words = [];

  @ViewChild('notificationsContainer', {read: ViewContainerRef}) notificationsContainer;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private notificationQueueServiceService: NotificationQueueServiceService) {
    this.notificationQueueServiceService.addListen().subscribe((m:any) => {
      this.addNotification(m);
    });
    this.notificationQueueServiceService.showListen().subscribe(() => {
      this.show();
    });
    this.notificationQueueServiceService.hideListen().subscribe(() => {
      this.hide();
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.notificationQueueServiceService.complete();
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  addNotification(notificationDto: NotificationDto): void {
    const factory = this.componentFactoryResolver.resolveComponentFactory(NotificationElementComponent);
    const componentRef = this.notificationsContainer.createComponent(factory, 0);

    componentRef.instance.notificationDto = notificationDto;
    componentRef.instance.notificationTimeoutConfiguration = NotificationTimeoutConfigurationFactory.create(notificationDto.type);
    componentRef.instance.componentId = this.componentsId;

    componentRef.instance.onCloseClick.subscribe(() => {
      this.words = this.words.filter(word => word.componentId !== componentRef.instance.componentId);
      componentRef.destroy();
    });

    componentRef.instance.onCloseAllClick.subscribe(() => {
      this.words = [];
      this.notificationsContainer.clear();
    });

    this.words.push(componentRef.instance);
    componentRef.changeDetectorRef.detectChanges();
    this.componentsId++;
  }

  private show() {
    this.isHideSelected = false;
  }

  private hide() {
    this.isHideSelected = true;
  }
}
