import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DocumentRoutingModule} from './document-routing.module';
import {HighlightPipe} from '../shared/pipe/highlight.pipe';
import {SharedModule} from '../shared/shared.module';
import {FilesModule} from '../files/files.module';
import { DocumentListComponent } from './document-list/document-list.component';
import {CrmFileUploaderService} from '../files/crm-file-uploader.service';

@NgModule({
  declarations: [
    HighlightPipe,
    DocumentListComponent,
  ],
  exports: [
    DocumentListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DocumentRoutingModule,
    FilesModule
  ],
  providers: [CrmFileUploaderService]
})
export class DocumentModule {
}
