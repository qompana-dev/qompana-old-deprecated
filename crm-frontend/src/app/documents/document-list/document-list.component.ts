import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {Sort} from '@angular/material';
import {Observable, Subject} from 'rxjs';
import {Page, PageRequest} from '../../shared/pagination/page.model';
import {FileAuthTypeEnum, FileInfo} from '../../files/file.model';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {NOT_FOUND} from 'http-status-codes';
import {PersonService} from '../../person/person.service';
import {LoginService} from '../../login/login.service';
import {FileService} from '../../files/file.service';
import {map, takeUntil, throttleTime} from 'rxjs/operators';
import {InfiniteScrollUtil} from '../../shared/pagination/infinite-scroll.util';
import {HttpErrorResponse} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {CrmObject, CrmObjectType} from '../../shared/model/search.model';
import {AssignToGroupService} from '../../shared/assign-to-group/assign-to-group.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit, OnDestroy, OnChanges {

  userMap: Map<string, string>;
  filesPage: Page<FileInfo>;
  loadingFinished = true;
  loggedAccountEmail: string;
  readonly getCustomerFilesErrors: ErrorTypes = {
    base: 'customer.dashboard.files.notification.',
    defaultText: 'unknownError',
    errors: [{
      code: NOT_FOUND,
      text: 'customerNotFoundError'
    }]
  };
  private currentPage: number;
  private currentSort = 'id,desc';
  private search: string;
  private componentDestroyed: Subject<void> = new Subject();

  @Input() forObject = false;
  @Input() objectType: CrmObjectType;
  @Input() objectLabel: string;
  @Input() objectId: number;
  @Input() withTopBar = false;

  title: string;

  constructor(
    private personService: PersonService,
    private router: Router,
    private translateService: TranslateService,
    private assignToGroupService: AssignToGroupService,
    private activatedRoute: ActivatedRoute,
    private notificationService: NotificationService,
    private loginService: LoginService,
    private fileService: FileService) {
  }

  ngOnInit(): void {
    this.getAccountEmails();
    this.init();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.init();
  }

  init(): void {
    if (this.forObject) {
      this.getTitle();
    }
    this.initFiles();
  }

  getTitle(): void {
    this.assignToGroupService
      .getIdsObservable([{id: this.objectId, type: this.objectType}],
        this.assignToGroupService.getRequestPath(this.objectType))
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((objects: CrmObject[]) => {
        this.title = this.translateService.instant('files.type.' + this.objectType.toLowerCase()) + ' ' + objects[0].label;
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  onScroll(e: any): void {
    if (InfiniteScrollUtil.shouldGetMoreData(e) && this.filesPage.numberOfElements < this.filesPage.totalElements) {
      const nextPage = this.currentPage + 1;
      this.getMoreFiles({page: '' + nextPage, size: InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: this.currentSort});
    }
  }

  onSortChange($event: Sort): void {
    this.currentSort = $event.direction ? (`${$event.active},${$event.direction}`) : 'created,desc';
    this.initFiles();
  }

  initFiles(pageRequest: PageRequest = {
    page: '0',
    size: InfiniteScrollUtil.ELEMENTS_ON_PAGE,
    sort: this.currentSort
  }): void {
    this.loadingFinished = false;
    this.getFiles(pageRequest)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      files => {
        this.handleFilesInitSuccess(files);
        this.loggedAccountEmail = this.loginService.getLoggedAccountEmail();
      },
      err => this.handleGetFilesError(err),
      () => this.loadingFinished = true
    );
  }

  private getMoreFiles(pageRequest: PageRequest): void {
    this.loadingFinished = false;
    this.getFiles(pageRequest)
      .pipe(
        takeUntil(this.componentDestroyed),
        throttleTime(500)
      ).subscribe(
      page => {
        this.handleGetMoreFilesSuccess(page),
          this.loggedAccountEmail = this.loginService.getLoggedAccountEmail();
      }, err => this.handleGetFilesError(err),
      () => this.loadingFinished = true
    );
  }

  private handleGetMoreFilesSuccess(page: Page<FileInfo>): void {
    this.currentPage++;
    this.filesPage.content = this.filesPage.content.concat(page.content);
    this.filesPage.numberOfElements += page.content.length;
  }

  private handleFilesInitSuccess(files: Page<FileInfo>): void {
    this.currentPage = 0;
    this.filesPage = files;
  }

  private handleGetFilesError(err: HttpErrorResponse): void {
    if (err.status !== NOT_FOUND) {
      this.notificationService.notifyError(this.getCustomerFilesErrors, err.status);
    }
  }

  private getAccountEmails(): void {
    this.personService.getAccountEmails()
      .pipe(
        takeUntil(this.componentDestroyed),
        map(emails => new Map<string, string>(emails.map(i => [i.email, `${i.name} ${i.surname}`] as [string, string]))),
      ).subscribe(
      users => this.userMap = users,
      (err) => this.notificationService.notify('customer.dashboard.contacts.notification.getAccountsError', NotificationType.ERROR)
    );
  }

  getPageContent(): FileInfo[] {
    return this.filesPage ? this.filesPage.content : [];
  }

  getFiles(pageRequest: PageRequest): Observable<Page<FileInfo>> {
    if (this.forObject && this.objectType && this.objectId) {
      return this.fileService.getFilesInGroup(this.objectType, this.objectId, pageRequest, FileAuthTypeEnum.lead, this.search);
    } else {
      return this.fileService.getLoggedUserFiles(pageRequest, this.search);
    }
  }

  getInitObjectType(): CrmObject {
    if (this.objectId && this.objectType) {
      return {id: this.objectId, type: this.objectType, label: this.objectLabel};
    }
    return undefined;
  }

  searchChange(search: string): void {
    this.search = search;
    this.initFiles();
  }
}
