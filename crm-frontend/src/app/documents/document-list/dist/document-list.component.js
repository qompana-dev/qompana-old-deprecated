"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.DocumentListComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var file_model_1 = require("../../files/file.model");
var notification_service_1 = require("../../shared/notification.service");
var http_status_codes_1 = require("http-status-codes");
var operators_1 = require("rxjs/operators");
var infinite_scroll_util_1 = require("../../shared/pagination/infinite-scroll.util");
var DocumentListComponent = /** @class */ (function () {
    function DocumentListComponent(personService, router, translateService, assignToGroupService, activatedRoute, notificationService, loginService, fileService) {
        this.personService = personService;
        this.router = router;
        this.translateService = translateService;
        this.assignToGroupService = assignToGroupService;
        this.activatedRoute = activatedRoute;
        this.notificationService = notificationService;
        this.loginService = loginService;
        this.fileService = fileService;
        this.loadingFinished = true;
        this.getCustomerFilesErrors = {
            base: 'customer.dashboard.files.notification.',
            defaultText: 'unknownError',
            errors: [{
                    code: http_status_codes_1.NOT_FOUND,
                    text: 'customerNotFoundError'
                }]
        };
        this.currentSort = 'id,desc';
        this.componentDestroyed = new rxjs_1.Subject();
        this.forObject = false;
        this.withTopBar = false;
    }
    DocumentListComponent.prototype.ngOnInit = function () {
        this.getAccountEmails();
        this.init();
    };
    DocumentListComponent.prototype.ngOnChanges = function (changes) {
        this.init();
    };
    DocumentListComponent.prototype.init = function () {
        if (this.forObject) {
            this.getTitle();
        }
        this.initFiles();
    };
    DocumentListComponent.prototype.getTitle = function () {
        var _this = this;
        this.assignToGroupService
            .getIdsObservable([{ id: this.objectId, type: this.objectType }], this.assignToGroupService.getRequestPath(this.objectType))
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (objects) {
            _this.title = _this.translateService.instant('files.type.' + _this.objectType.toLowerCase()) + ' ' + objects[0].label;
        });
    };
    DocumentListComponent.prototype.ngOnDestroy = function () {
        this.componentDestroyed.next();
        this.componentDestroyed.complete();
    };
    DocumentListComponent.prototype.onScroll = function (e) {
        if (infinite_scroll_util_1.InfiniteScrollUtil.shouldGetMoreData(e) && this.filesPage.numberOfElements < this.filesPage.totalElements) {
            var nextPage = this.currentPage + 1;
            this.getMoreFiles({ page: '' + nextPage, size: infinite_scroll_util_1.InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: this.currentSort });
        }
    };
    DocumentListComponent.prototype.onSortChange = function ($event) {
        this.currentSort = $event.direction ? ($event.active + "," + $event.direction) : 'created,desc';
        this.initFiles();
    };
    DocumentListComponent.prototype.initFiles = function (pageRequest) {
        var _this = this;
        if (pageRequest === void 0) { pageRequest = {
            page: '0',
            size: infinite_scroll_util_1.InfiniteScrollUtil.ELEMENTS_ON_PAGE,
            sort: this.currentSort
        }; }
        this.loadingFinished = false;
        this.getFiles(pageRequest)
            .pipe(operators_1.takeUntil(this.componentDestroyed)).subscribe(function (files) {
            _this.handleFilesInitSuccess(files);
            _this.loggedAccountEmail = _this.loginService.getLoggedAccountEmail();
        }, function (err) { return _this.handleGetFilesError(err); }, function () { return _this.loadingFinished = true; });
    };
    DocumentListComponent.prototype.getMoreFiles = function (pageRequest) {
        var _this = this;
        this.loadingFinished = false;
        this.getFiles(pageRequest)
            .pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.throttleTime(500)).subscribe(function (page) {
            _this.handleGetMoreFilesSuccess(page),
                _this.loggedAccountEmail = _this.loginService.getLoggedAccountEmail();
        }, function (err) { return _this.handleGetFilesError(err); }, function () { return _this.loadingFinished = true; });
    };
    DocumentListComponent.prototype.handleGetMoreFilesSuccess = function (page) {
        this.currentPage++;
        this.filesPage.content = this.filesPage.content.concat(page.content);
        this.filesPage.numberOfElements += page.content.length;
    };
    DocumentListComponent.prototype.handleFilesInitSuccess = function (files) {
        this.currentPage = 0;
        this.filesPage = files;
    };
    DocumentListComponent.prototype.handleGetFilesError = function (err) {
        if (err.status !== http_status_codes_1.NOT_FOUND) {
            this.notificationService.notifyError(this.getCustomerFilesErrors, err.status);
        }
    };
    DocumentListComponent.prototype.getAccountEmails = function () {
        var _this = this;
        this.personService.getAccountEmails()
            .pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.map(function (emails) { return new Map(emails.map(function (i) { return [i.email, i.name + " " + i.surname]; })); })).subscribe(function (users) { return _this.userMap = users; }, function (err) { return _this.notificationService.notify('customer.dashboard.contacts.notification.getAccountsError', notification_service_1.NotificationType.ERROR); });
    };
    DocumentListComponent.prototype.getPageContent = function () {
        return this.filesPage ? this.filesPage.content : [];
    };
    DocumentListComponent.prototype.getFiles = function (pageRequest) {
        if (this.forObject && this.objectType && this.objectId) {
            return this.fileService.getFilesInGroup(this.objectType, this.objectId, pageRequest, file_model_1.FileAuthTypeEnum.lead);
        }
        else {
            return this.fileService.getLoggedUserFiles(pageRequest);
        }
    };
    DocumentListComponent.prototype.getInitObjectType = function () {
        if (this.objectId && this.objectType) {
            return { id: this.objectId, type: this.objectType, label: this.objectLabel };
        }
        return undefined;
    };
    __decorate([
        core_1.Input()
    ], DocumentListComponent.prototype, "forObject");
    __decorate([
        core_1.Input()
    ], DocumentListComponent.prototype, "objectType");
    __decorate([
        core_1.Input()
    ], DocumentListComponent.prototype, "objectLabel");
    __decorate([
        core_1.Input()
    ], DocumentListComponent.prototype, "objectId");
    __decorate([
        core_1.Input()
    ], DocumentListComponent.prototype, "withTopBar");
    DocumentListComponent = __decorate([
        core_1.Component({
            selector: 'app-document-list',
            templateUrl: './document-list.component.html',
            styleUrls: ['./document-list.component.scss']
        })
    ], DocumentListComponent);
    return DocumentListComponent;
}());
exports.DocumentListComponent = DocumentListComponent;
