import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../login/auth/auth-guard.service';
import {DocumentListComponent} from './document-list/document-list.component';

const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: DocumentListComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRoutingModule {
}
