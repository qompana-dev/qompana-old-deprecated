import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {FileInfo} from './file.model';
import {FilePreviewDialogComponent} from './file-preview/file-preview-dialog.component';
import {FileService} from './file.service';

@Injectable({
  providedIn: 'root'
})
export class FilePreviewService {

  constructor(public dialog: MatDialog, public fileService: FileService) {
  }

  showPreviewOf(file: FileInfo): void {
    this.dialog.open(FilePreviewDialogComponent, {
      panelClass: 'file-preview-container',
      width: '100%',
      height: '100%',
      data: file
    });
  }

  showPreviewByFileId(fileId: number): void {
    this.fileService.getFileById(fileId)
      .subscribe(
        file => this.showPreviewOf(file)
      );
  }
}
