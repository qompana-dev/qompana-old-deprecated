"use strict";
exports.__esModule = true;
exports.FileAuthTypeEnum = exports.editFileErrors = exports.fileDeleteErrorTypes = exports.uploadFileErrorDialogData = exports.deleteFileDialogData = exports.FileGroupType = exports.FileFormData = void 0;
var http_status_codes_1 = require("http-status-codes");
var FileFormData = /** @class */ (function () {
    function FileFormData() {
        this.description = '';
        this.tags = [];
        this.assignedObjects = [];
    }
    return FileFormData;
}());
exports.FileFormData = FileFormData;
var FileGroupType;
(function (FileGroupType) {
    FileGroupType["user"] = "user";
    FileGroupType["customer"] = "customer";
})(FileGroupType = exports.FileGroupType || (exports.FileGroupType = {}));
exports.deleteFileDialogData = {
    title: 'files.removeDialog.title',
    description: 'files.removeDialog.description',
    noteFirst: 'files.removeDialog.noteFirstPart',
    noteSecond: 'files.removeDialog.noteSecondPart',
    confirmButton: 'files.removeDialog.buttonYes',
    cancelButton: 'files.removeDialog.buttonNo'
};
exports.uploadFileErrorDialogData = {
    title: 'files.uploadingErrorDialog.title',
    description: 'files.uploadingErrorDialog.description',
    confirmButton: 'files.uploadingErrorDialog.tryAgain',
    cancelButton: 'files.uploadingErrorDialog.cancel'
};
exports.fileDeleteErrorTypes = {
    base: 'files.notification.',
    defaultText: 'unknownError',
    errors: [{
            code: http_status_codes_1.NOT_FOUND,
            text: 'fileToDeleteNotFound'
        }, {
            code: http_status_codes_1.FORBIDDEN,
            text: 'deleteFileForbidden'
        }]
};
exports.editFileErrors = {
    base: 'documents.fileDetails.notification.',
    defaultText: 'unknownError',
    errors: [{
            code: http_status_codes_1.NOT_FOUND,
            text: 'fileNotFound'
        }, {
            code: http_status_codes_1.FORBIDDEN,
            text: 'editFileForbidden'
        }]
};
var FileAuthTypeEnum;
(function (FileAuthTypeEnum) {
    FileAuthTypeEnum["leadMini"] = "leadMini";
    FileAuthTypeEnum["lead"] = "lead";
    FileAuthTypeEnum["opportunityMini"] = "opportunityMini";
    FileAuthTypeEnum["opportunity"] = "opportunity";
    FileAuthTypeEnum["productMini"] = "productMini";
    FileAuthTypeEnum["product"] = "product";
})(FileAuthTypeEnum = exports.FileAuthTypeEnum || (exports.FileAuthTypeEnum = {}));
