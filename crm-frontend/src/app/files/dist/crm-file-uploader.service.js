"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CrmFileUploaderService = void 0;
var core_1 = require("@angular/core");
var ng2_file_upload_1 = require("ng2-file-upload");
var url_util_1 = require("../shared/url.util");
var CrmFileUploaderService = /** @class */ (function (_super) {
    __extends(CrmFileUploaderService, _super);
    function CrmFileUploaderService(loginService) {
        var _this = _super.call(this, { url: CrmFileUploaderService_1.fileServiceUrl }) || this;
        _this.loginService = loginService;
        return _this;
    }
    CrmFileUploaderService_1 = CrmFileUploaderService;
    CrmFileUploaderService.prototype.setFileAuthTypeEnum = function (fileAuthTypeEnum) {
        this.fileAuthTypeEnum = fileAuthTypeEnum;
    };
    CrmFileUploaderService.prototype.uploadItemWithForm = function (fileItem, form) {
        fileItem['submittedAsSingle'] = true;
        fileItem.onBuildForm = this.getOnBuildItemFormForNewFile(form);
        this.uploadItem(fileItem);
    };
    CrmFileUploaderService.prototype.uploadItems = function (items, form) {
        var _this = this;
        items.forEach(function (item) {
            item.onBuildForm = _this.getOnBuildItemFormForNewFile(form);
            _this.uploadItem(item);
        });
    };
    CrmFileUploaderService.prototype.uploadItem = function (item) {
        var _this = this;
        if (!this.loginService.isLoggedIn()) {
            this.loginService.getRefreshTokenSubscription().subscribe(function () {
                _super.prototype.uploadItem.call(_this, item);
            });
        }
        else {
            _super.prototype.uploadItem.call(this, item);
        }
    };
    CrmFileUploaderService.prototype.getOnBuildItemFormForNewFile = function (fileData) {
        return function (form) {
            form.append('fileData', new Blob([JSON.stringify(fileData)], { type: 'application/json' }));
        };
    };
    CrmFileUploaderService.prototype.onBeforeUploadItem = function (fileItem) {
        fileItem.headers = [
            { name: 'Authorization', value: 'Bearer ' + this.loginService.getToken() },
            { name: 'Auth-controller-type', value: this.fileAuthTypeEnum }
        ];
        fileItem.withCredentials = false;
    };
    CrmFileUploaderService.prototype.getItemsWithError = function () {
        return this.queue.filter(function (item) { return item.isError; });
    };
    CrmFileUploaderService.prototype.getItemsSubmittedAsGroup = function () {
        return this.queue.filter(function (item) { return !item['submittedAsSingle']; });
    };
    var CrmFileUploaderService_1;
    CrmFileUploaderService.fileServiceUrl = url_util_1.UrlUtil.url + "/crm-file-service/files";
    CrmFileUploaderService = CrmFileUploaderService_1 = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], CrmFileUploaderService);
    return CrmFileUploaderService;
}(ng2_file_upload_1.FileUploader));
exports.CrmFileUploaderService = CrmFileUploaderService;
