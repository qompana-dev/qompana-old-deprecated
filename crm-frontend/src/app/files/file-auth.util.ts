import {CrmObjectType} from '../shared/model/search.model';
import {FileAuthTypeEnum} from './file.model';

export class FileAuthUtil {
  static getFileAuthType(type: CrmObjectType): FileAuthTypeEnum {
    return (type === CrmObjectType.lead) ? FileAuthTypeEnum.lead : undefined;
  }
}
