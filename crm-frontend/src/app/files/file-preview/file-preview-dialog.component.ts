import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {editFileErrors, FileInfo} from '../file.model';
import {filter, takeUntil} from 'rxjs/operators';
import {FileService} from '../file.service';
import {Observable, Subject} from 'rxjs';
import {PagesLoadedEvent} from 'ngx-extended-pdf-viewer/lib/pages-loaded-event';
import {SliderComponent} from '../../shared/slider/slider.component';
import {SliderService} from '../../shared/slider/slider.service';
import {NotificationService} from '../../shared/notification.service';

@Component({
  selector: 'app-file-preview-dialog',
  templateUrl: './file-preview-dialog.component.html',
  styleUrls: ['./file-preview-dialog.component.scss'],
})
export class FilePreviewDialogComponent implements OnInit, OnDestroy {

  blob: Blob;
  fileUrl: string;
  inProgress = false;
  isError = false;
  page = 1;
  zoom = 100;
  numberOfPages = 0;
  bottomToolbarVisible = false;
  private componentDestroyed: Subject<void> = new Subject();
  detailsOpened = false;
  selectedFileForDetails: FileInfo;

  @ViewChild('previewFileDetails') editFileDetailsSlider: SliderComponent;


  constructor(public dialogRef: MatDialogRef<FilePreviewDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: FileInfo,
              private fileService: FileService,
              public dialog: MatDialog,
              public sliderService: SliderService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.isError = false;
    this.inProgress = true;
    this.bottomToolbarVisible = false;
    this.downloadFilePreview(this.data).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      blob => this.handleBlobResponse(blob),
      err => this.handleError(),
      () => this.stopProgress()
    );
    this.listenWhenSliderClosed();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  download(): void {
    this.fileService.downloadFile(this.data.id).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      blob => saveAs(blob, this.data.name)
    );
  }

  isImage(type: string): boolean {
    return type && type.startsWith('image');
  }

  isPdf(type: string): boolean {
    return type === 'application/pdf';
  }

  zoomIn(): void {
    if (this.zoom < 200) {
      this.zoom += 10;
    }
  }

  zoomOut(): void {
    if (10 < this.zoom) {
      this.zoom -= 10;
    }
  }

  previousPage(): void {
    if (this.page > 1) {
      this.page -= 1;
    }
  }

  nextPage(): void {
    if (this.page < this.numberOfPages) {
      this.page += 1;
    }
  }

  onPagesLoaded($event: PagesLoadedEvent): void {
    this.numberOfPages = $event.pagesCount;
    this.bottomToolbarVisible = true;
  }

  private downloadFilePreview(file: FileInfo): Observable<Blob> {
    if (this.isPdf(this.data.contentType) || this.isImage(this.data.contentType)) {
      return this.fileService.downloadFile(this.data.id);
    } else {
      return this.fileService.previewFile(this.data.id);
    }
  }

  private handleBlobResponse(blob: Blob): void {
    this.blob = blob;
    if (this.isImage(blob.type)) {
      this.createObjectUrl(blob);
    }
  }

  private createObjectUrl(blob: Blob): void {
    this.fileUrl = URL.createObjectURL(blob);
  }

  private stopProgress(): void {
    this.inProgress = false;
  }

  private handleError(): void {
    this.stopProgress();
    this.isError = true;
  }

  showDetails(): void {
    if (!this.detailsOpened) {
      this.openDetails();
    } else {
      this.closeDetails();
    }

  }

  private closeDetails(): void {
    this.editFileDetailsSlider.close();
    this.adjustFilePreviewOnSliderClose();
  }

  private openDetails(): void {
    this.fileService.getFileInfo(this.data.id).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(freshFile => {
        this.selectedFileForDetails = freshFile;
      },
      err => this.notificationService.notifyError(editFileErrors, err.status)
    );
    this.editFileDetailsSlider.open();
    this.adjustFilePreviewOnSliderOpen();
  }

  private adjustFilePreviewOnSliderClose(): void {
    this.dialogRef.removePanelClass('file-preview-container-with-slider');
    this.dialogRef.addPanelClass('file-preview-container-without-slider');
    this.detailsOpened = false;
  }

  private adjustFilePreviewOnSliderOpen(): void {
    this.dialogRef.removePanelClass('file-preview-container-without-slider');
    this.dialogRef.addPanelClass('file-preview-container-with-slider');
    this.detailsOpened = true;
  }

  private listenWhenSliderClosed(): void {
    this.sliderService.sliderStateChanged.pipe(
      takeUntil(this.componentDestroyed),
      filter(change => !change.opened),
      filter(change => change.key === 'FilePreview.EditDetailsSlider'),
    ).subscribe(() => {
      this.selectedFileForDetails = null;
      this.adjustFilePreviewOnSliderClose();
    });
  }

}
