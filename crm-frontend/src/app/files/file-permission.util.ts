import {CrmObjectType} from '../shared/model/search.model';

export class FilePermissionUtil {
  public static getListKey(type: CrmObjectType) {
    if (type) {
      switch (type) {
        case CrmObjectType.lead:
          return 'DocumentsListInLeadComponent.';
        case CrmObjectType.opportunity:
          return 'DocumentsListInOpportunityComponent.';
      }
    }
    return 'DocumentsListComponent.';
  }

  public static getHistoryKey(type: CrmObjectType) {
    if (type) {
      switch (type) {
        case CrmObjectType.lead:
          return 'DocumentsHistoryInLeadComponent.';
        case CrmObjectType.opportunity:
          return 'DocumentsHistoryInOpportunityComponent.';
      }
    }
    return 'DocumentsHistoryComponent.';
  }

  public static getAddKey(type: CrmObjectType) {
    if (type) {
      switch (type) {
        case CrmObjectType.lead:
          return 'DocumentsAddInLeadComponent.';
        case CrmObjectType.opportunity:
          return 'DocumentsAddInOpportunityComponent.';
      }
    }
    return 'DocumentsAddComponent.';
  }

  public static getEditKey(type: CrmObjectType) {
    if (type) {
      switch (type) {
        case CrmObjectType.lead:
          return 'DocumentsEditInLeadComponent.';
        case CrmObjectType.opportunity:
          return 'DocumentsEditInOpportunityComponent.';
      }
    }
    return 'DocumentsEditComponent.';
  }

  public static getNormalKey(type: CrmObjectType) {
    if (type) {
      switch (type) {
        case CrmObjectType.lead:
          return 'DocumentsInLeadComponent.';
        case CrmObjectType.opportunity:
          return 'DocumentsInOpportunityComponent.';
      }
    }
    return 'DocumentsComponent.';
  }
}
