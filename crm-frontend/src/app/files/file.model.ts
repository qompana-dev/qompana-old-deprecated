import {CrmObject, CrmObjectType} from '../shared/model/search.model';
import {DeleteDialogData} from '../shared/dialog/delete/delete-dialog.component';
import {ErrorTypes} from '../shared/notification.service';
import {FORBIDDEN, NOT_FOUND} from 'http-status-codes';

export interface FileInfo {
  self: string;
  id: string;
  fileId: string;
  name: string;
  path: string;
  format: string;
  contentType: string;
  description: string;
  author: string;
  authorFullName: string;
  tags: string[];
  created: string;
  size: number;
  fileGroups?: FileGroup[];
  scanStatus?: ScanStatusEnum;
}

export class FileFormData {
  description: string;
  tags: string[];
  assignedObjects?: CrmObject[];

  constructor() {
    this.description = '';
    this.tags = [];
    this.assignedObjects = [];
  }

}

export interface FileGroup {
  id?: number;
  groupId: number;
  groupType: CrmObjectType;
  text?: string;
}

export interface FileGroupActionDto {
  groupId: number;
  groupType: string;
}

export interface FileEvent {
  id: number;
  eventType: string;
  value1: string;
  value2: string;
  accountEmail: string;
  correlationId: number;
  created: string;
}

export enum FileGroupType {
  user = 'user',
  customer = 'customer'
}

export interface FileSizeDto {
  fileId: string;
  size: number;
}


export const deleteFileDialogData: Partial<DeleteDialogData> = {
  title: 'files.removeDialog.title',
  description: 'files.removeDialog.description',
  noteFirst: 'files.removeDialog.noteFirstPart',
  noteSecond: 'files.removeDialog.noteSecondPart',
  confirmButton: 'files.removeDialog.buttonYes',
  cancelButton: 'files.removeDialog.buttonNo',
};

export const deleteAssociatedFileDialogData: Partial<DeleteDialogData> = {
  title: 'files.removeDialog.title',
  description: 'files.removeDialog.description',
  noteFirst: 'files.removeDialog.noteFirstPartAssociated',
  noteSecond: 'files.removeDialog.noteSecondPartAssociated',
  confirmButton: 'files.removeDialog.buttonYes',
  cancelButton: 'files.removeDialog.buttonNo',
};

export const uploadFileErrorDialogData: Partial<DeleteDialogData> = {
  title: 'files.uploadingErrorDialog.title',
  description: 'files.uploadingErrorDialog.description',
  confirmButton: 'files.uploadingErrorDialog.tryAgain',
  cancelButton: 'files.uploadingErrorDialog.cancel',
};

export const fileDeleteErrorTypes: ErrorTypes = {
  base: 'files.notification.',
  defaultText: 'unknownError',
  errors: [{
    code: NOT_FOUND,
    text: 'fileToDeleteNotFound'
  }, {
    code: FORBIDDEN,
    text: 'deleteFileForbidden'
  }]
};

export const editFileErrors: ErrorTypes = {
  base: 'documents.fileDetails.notification.',
  defaultText: 'unknownError',
  errors: [{
    code: NOT_FOUND,
    text: 'fileNotFound'
  }, {
    code: FORBIDDEN,
    text: 'editFileForbidden'
  }]
};


export enum FileAuthTypeEnum {
  leadMini = 'leadMini',
  lead = 'lead',
  opportunityMini = 'opportunityMini',
  opportunity = 'opportunity',
  productMini = 'productMini',
  product = 'product',
}

export enum ScanStatusEnum {
  OK = 'OK',
  VIRUS_FOUND = 'VIRUS_FOUND',
  CREATED = 'CREATED',
  CANNOT_CHECK = 'CANNOT_CHECK'
}
