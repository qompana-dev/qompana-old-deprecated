import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FileEvent, FileInfo} from '../file.model';
import {SliderService} from '../../shared/slider/slider.service';
import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';
import {FileService} from '../file.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {NOT_FOUND} from 'http-status-codes';
import * as cloneDeep from 'lodash/cloneDeep';
import * as uniqBy from 'lodash/uniqBy';
import {CrmObjectType} from '../../shared/model/search.model';
import {FilePermissionUtil} from '../file-permission.util';
import {FileAuthUtil} from '../file-auth.util';
import {FilePreviewService} from '../file-preview.service';


@Component({
  selector: 'app-file-history',
  templateUrl: './file-history.component.html',
  styleUrls: ['./file-history.component.scss']
})
export class FileHistoryComponent implements OnInit, OnDestroy {

  @Input() forType: CrmObjectType;
  @Input() userMap: Map<string, string>;
  _file: FileInfo;
  @Input() set file(fileInfo: FileInfo) {
    if (fileInfo) {
      this._file = fileInfo;
      this.getFileHistory(+fileInfo.fileId);
    }
  }

  fileHistory: FileEvent[];
  filteredHistory: FileEvent[];
  private componentDestroyed: Subject<void> = new Subject();
  private readonly historyFileErrors: ErrorTypes = {
    base: 'documents.fileDetails.notification.',
    defaultText: 'unknownError',
    errors: [{
      code: NOT_FOUND,
      text: 'fileNotFound'
    }]
  };
  permissionUtil = FilePermissionUtil;

  constructor(
    public sliderService: SliderService,
    public fileService: FileService,
    public notificationService: NotificationService,
    public filePreviewService: FilePreviewService
  ) {
  }

  get authKey(): string {
    return 'DocumentsAddComponent.';
  }

  ngOnInit(): void {
    this.listenWhenSliderClosed();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
  }

  showDetails(event: FileEvent): void {
    this.notificationService.notify('Funkcjonalność nie zaimplementowana', NotificationType.ERROR);
  }

  downloadFileVersion(index: number): void {
    for (let i = index; i < this.filteredHistory.length; i++) {
      const event: FileEvent = this.filteredHistory[i];
      if (event.eventType === 'FILE_CREATED' || event.eventType === 'NEW_VERSION_UPLOADED') {
        const fileToPreview: FileInfo = cloneDeep(this._file);
        fileToPreview.id = event.value1;
        this.filePreviewService.showPreviewOf(fileToPreview);
        return;
      }
    }
  }

  private listenWhenSliderClosed(): void {
    this.sliderService.sliderStateChanged.pipe(
      takeUntil(this.componentDestroyed),
      filter(change => !change.opened),
      filter(change => change.key === 'CustomerFilesList.FileHistorySlider')
    ).subscribe(() => {
      this._file = null;
      this.fileHistory = null;
    });
  }

  private getFileHistory(fileId: number): void {
    this.fileService.getFileWithHistory(fileId, FileAuthUtil.getFileAuthType(this.forType))
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      history => {
        this.fileHistory = history;
        this.filteredHistory = uniqBy(history, 'correlationId');
      },
      err => this.notificationService.notifyError(this.historyFileErrors, err.status)
    );
  }
}
