import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {SliderService} from '../../shared/slider/slider.service';
import {editFileErrors, FileAuthTypeEnum, FileFormData, FileGroupType, FileInfo} from '../file.model';
import {MatAutocompleteTrigger, MatCheckboxChange, MatChipInputEvent} from '@angular/material';
import {merge, Observable, of, Subject} from 'rxjs';
import {catchError, debounceTime, filter, finalize, switchMap, takeUntil, tap} from 'rxjs/operators';
import {FileService} from '../file.service';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {FileItem} from 'ng2-file-upload';
import {applyPermission} from '../../shared/permissions-utils';
import {AuthService} from '../../login/auth/auth.service';
import * as unionWith from 'lodash/unionWith';
import * as pullAllWith from 'lodash/pullAllWith';
import {CustomerService} from '../../customer/customer.service';
import {PersonService} from '../../person/person.service';
import {CrmObject, crmObjectComparator, CrmObjectType} from '../../shared/model/search.model';
import {LeadService} from '../../lead/lead.service';
import {FilePermissionUtil} from '../file-permission.util';
import {FileAuthUtil} from '../file-auth.util';
import {OpportunityService} from '../../opportunity/opportunity.service';


@Component({
  selector: 'app-file-details',
  templateUrl: './file-details.component.html',
  styleUrls: ['./file-details.component.scss']
})
export class FileDetailsComponent implements OnInit, OnDestroy {

  @Input() update: boolean;
  @Input() forType: CrmObjectType;
  @Output() onFileUpload: EventEmitter<[FileItem, FileFormData]> = new EventEmitter();
  @Input() userMap: Map<string, string>;
  @Input() assignedTo: CrmObject;
  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement>;
  @ViewChild('searchInput', {read: MatAutocompleteTrigger}) autoComplete: MatAutocompleteTrigger;
  isPrivate: boolean = undefined;
  fileDetailsForm: FormGroup;
  fileName: string;
  fileFormat: string;
  searchVisible = false;
  areItemsToAssignLoading = false;
  assignToOptions: CrmObject[];
  filteredAssignToOptions: CrmObject[];
  selectedItemsToAssign: CrmObject[] = [];
  markedItemsToAssign: CrmObject[] = [];
  public availableTags$: Observable<Array<string>>;
  private componentDestroyed: Subject<void> = new Subject();
  private areTagsLoading = false;


  constructor(
    public sliderService: SliderService,
    public fileService: FileService,
    private opportunityService: OpportunityService,
    public notificationService: NotificationService,
    private authService: AuthService,
    private customerService: CustomerService,
    private leadService: LeadService,
    private personService: PersonService) {
    this.createFileDetailsForm();
  }

  _file: FileInfo;

  @Input() set file(fileInfo: FileInfo) {
    if (fileInfo) {
      this._file = fileInfo;
      this.updateForm(fileInfo);
      this.getCrmObjects(fileInfo);
    }
  }

  _fileItem: FileItem;

  @Input() set fileItem(fileItem: FileItem) {
    if (fileItem) {
      this._fileItem = fileItem;
      this.fileName = fileItem.file.name;
      this.fileFormat = fileItem.file.type;
    }
  }


  // @formatter:off
  get description(): AbstractControl { return this.fileDetailsForm.get('description'); }
  get tags(): AbstractControl { return this.fileDetailsForm.get('tags'); }
  get currentTag(): AbstractControl { return this.fileDetailsForm.get('currentTag'); }
  get assignToItem(): AbstractControl { return this.fileDetailsForm.get('assignToItem'); }
  get authKey(): string { return (this.update) ? FilePermissionUtil.getEditKey(this.forType) : FilePermissionUtil.getAddKey(this.forType); }
  // @formatter:on

  ngOnInit(): void {
    this.listenForTagInput();
    this.listenWhenSliderClosed();
    this.listenWhenSliderCloseClicked();
    this.listenForAssignToInputChanges();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
  }

  removeTag(removedTag: string): void {
    const index = this.tags.value.indexOf(removedTag);
    if (index >= 0) {
      this.tags.value.splice(index, 1);
      this.tags.updateValueAndValidity();
    }
  }

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    const index = this.tags.value.indexOf(value);
    if (index < 0 && (value || '').trim()) {
      this.tags.setValue([...this.tags.value, value.trim()]);
      this.tags.updateValueAndValidity();
    }
    if (input) {
      input.value = '';
    }
  }

  selected(selectedTag: string): void {
    this.addToTagList(selectedTag);
    this.tagInput.nativeElement.value = '';
  }

  submit(): void {
    if (this.update) {
      this.updateFileInfo();
    } else {
      this.emitFileToUpload();
      this.sliderService.closeSlider();
    }
  }

  cancel(): void {
    if (this._fileItem) {
      this._fileItem.remove();
    }
    this.closeSlider();
  }

  toggleItemToAssign($event: MatCheckboxChange, item: CrmObject): void {
    if ($event.checked) {
      this.markedItemsToAssign.push(item);
    } else {
      const index = this.markedItemsToAssign.indexOf(item);
      if (index !== -1) {
        this.markedItemsToAssign.splice(index, 1);
      }
    }
  }

  showGroupSearch(searchInput: any): void {
    setTimeout(() => searchInput.focus(), 200);
    this.searchVisible = true;
  }

  addItemsToAssign(): void {
    this.autoComplete.closePanel();
    this.selectedItemsToAssign = unionWith(this.selectedItemsToAssign, this.markedItemsToAssign, crmObjectComparator);
    this.filteredAssignToOptions = this.getAssignOptions();
    this.markedItemsToAssign = [];
  }

  getAssignOptions(): CrmObject[] {
    const selectedItems = this.assignedTo ? [...this.selectedItemsToAssign, this.assignedTo] : [...this.selectedItemsToAssign];
    return pullAllWith([...this.assignToOptions], selectedItems, crmObjectComparator);
  }

  deleteAssignOption(option: CrmObject): void {
    const index = this.selectedItemsToAssign.indexOf(option);
    if (index >= 0) {
      this.selectedItemsToAssign.splice(index, 1);
      this.filteredAssignToOptions = this.getAssignOptions();
    }
  }

  private emitFileToUpload(): void {
    const fileFormData = this.getFileFormData();
    this.onFileUpload.emit([this._fileItem, fileFormData]);
  }

  private createFileDetailsForm(): void {
    if (!this.fileDetailsForm) {
      this.fileDetailsForm = new FormGroup({
        description: new FormControl(''),
        currentTag: new FormControl(''),
        tags: new FormControl([]),
        assignToItem: new FormControl('')
      });
      this.applyPermissionsToForm();
    }
  }

  private addToTagList(tag: string): void {
    if (!(tag || '').trim()) {
      return;
    }
    const index = this.tags.value.indexOf(tag);
    if (index < 0) {
      this.tags.setValue([...this.tags.value, tag.trim()]);
      this.tags.updateValueAndValidity();
    }
  }

  private listenForTagInput(): void {
    this.availableTags$ = this.currentTag.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      filter((input: string) => !!input),
      tap(() => this.areTagsLoading = true),
      switchMap(((input: string) => this.fileService.getTags(input, FileAuthUtil.getFileAuthType(this.forType))
        .pipe(
          catchError(() => of([])),
          finalize(() => this.areTagsLoading = false)
        ))
      ));
  }

  private updateForm(fileInfo: FileInfo): void {
    if (this.fileDetailsForm && fileInfo) {
      this.description.patchValue(fileInfo.description);
      this.tags.patchValue(fileInfo.tags);
      this.currentTag.patchValue('');
      this.fileName = fileInfo.name;
      this.fileFormat = fileInfo.format;
    }
  }

  private updateFileInfo(): void {
    this.fileService.updateFileInfo(this._file.id, this.getFileFormData(), FileAuthUtil.getFileAuthType(this.forType))
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      () => {
        this.notificationService.notify('documents.fileDetails.notification.updateSuccess', NotificationType.SUCCESS);
        this.sliderService.closeSlider();
      },
      err => this.notificationService.notifyError(editFileErrors, err.status)
    );
  }

  private getFileFormData(): FileFormData {
    return {
      tags: this.tags.value,
      description: this.description.value,
      assignedObjects: [...this.selectedItemsToAssign, this.assignedTo].filter(x => !!x)
    };
  }

  private listenWhenSliderClosed(): void {
    this.sliderService.sliderStateChanged.pipe(
      takeUntil(this.componentDestroyed),
      filter(change => !change.opened),
      filter(change => change.key === 'CustomerFilesList.EditDetailsSlider' || change.key === 'CustomerFilesList.AddDetailsSlider'),
    ).subscribe(() => {
      this.fileDetailsForm.reset({description: '', currentTag: '', tags: []});
      this.tagInput.nativeElement.value = '';
      this.searchVisible = false;
      this.assignToOptions = [];
      this.selectedItemsToAssign = [];
    });
  }

  private listenWhenSliderCloseClicked(): void {
    this.sliderService.closeClicked.pipe(
      takeUntil(this.componentDestroyed),
      filter(key => key === 'CustomerFilesList.AddDetailsSlider'),
      filter(() => !!this._fileItem),
    ).subscribe(() => {
      if (this.authService.isPermissionPresent(`${this.authKey}save`, 'w')) {
        this.emitFileToUpload();
      } else {
        this.cancel();
      }
    });
  }

  private applyPermissionsToForm(): void {
    if (this.fileDetailsForm) {
      applyPermission(this.description, !this.authService.isPermissionPresent(`${this.authKey}description`, 'r'));
      applyPermission(this.tags, !this.authService.isPermissionPresent(`${this.authKey}tags`, 'r'));
    }
  }

  private listenForAssignToInputChanges(): void {
    this.assignToItem.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      filter((input: string) => !!input && input.length >= 3),
      tap(() => {
        this.areItemsToAssignLoading = true;
        this.assignToOptions = [];
      }),
      switchMap(((input: string) => merge(
          this.customerService.getCustomersForSearch(input).pipe(catchError(() => of([]))),
          this.leadService.getLeadsForSearch(input).pipe(catchError(() => of([]))),
          this.opportunityService.getOpportunitiesForSearch(input).pipe(catchError(() => of([]))),
        ).pipe(finalize(() => this.areItemsToAssignLoading = false))
      ))).subscribe(
      result => {
        this.assignToOptions.push(...result);
        this.filteredAssignToOptions = this.getAssignOptions();
      }
    );
  }

  private getCrmObjects(fileInfo: FileInfo): void {
    if (!fileInfo.fileGroups) {
      return;
    }
    const customerIds: number[] = fileInfo.fileGroups.filter(group => group.groupType === CrmObjectType.customer).map(group => group.groupId);
    const leadIds: number[] = fileInfo.fileGroups.filter(group => group.groupType === CrmObjectType.lead).map(group => group.groupId);
    const opportunityIds: number[] = fileInfo.fileGroups.filter(group => group.groupType === CrmObjectType.opportunity).map(group => group.groupId);
    this.selectedItemsToAssign = [];
    merge(
      this.customerService.getCustomersByIds(customerIds).pipe(catchError(() => {
        this.notificationService.notify('documents.fileDetails.notification.associatedCustomersError', NotificationType.ERROR);
        return of([]);
      })),
      this.leadService.getLeadsByIds(leadIds).pipe(catchError(() => {
        this.notificationService.notify('documents.fileDetails.notification.associatedLeadsError', NotificationType.ERROR);
        return of([]);
      })),
      this.opportunityService.getOpportunitiesByIds(opportunityIds).pipe(catchError(() => {
        this.notificationService.notify('documents.fileDetails.notification.associatedOpportunitiesError', NotificationType.ERROR);
        return of([]);
      })),
    ).subscribe(
      result => {
        this.selectedItemsToAssign.push(...pullAllWith(result, [this.assignedTo], crmObjectComparator));
      });
  }
}
