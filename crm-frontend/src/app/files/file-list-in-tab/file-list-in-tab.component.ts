import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {PersonService} from '../../person/person.service';
import {LoginService} from '../../login/login.service';
import {EmptyPage, Page, PageRequest} from '../../shared/pagination/page.model';
import {CrmObject} from '../../shared/model/search.model';
import {FileService} from '../file.service';
import {InfinityScrollUtil, InfinityScrollUtilInterface} from '../../shared/infinity-scroll.util';
import {FilterStrategy} from '../../shared/model';
import {FileListComponent} from '../file-list/file-list.component';
import {FileInfo} from '../file.model';
import {map, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-file-list-in-tab',
  templateUrl: './file-list-in-tab.component.html',
  styleUrls: ['./file-list-in-tab.component.scss']
})
export class FileListInTabComponent implements OnInit, OnDestroy, InfinityScrollUtilInterface {

  @Input() objectName: string;

  @Input() set crmObject(crmObject: CrmObject) {
    if (crmObject) {
      this._crmObject = crmObject;
      this.getFiles();
    }
  }

  _crmObject: CrmObject;


  @Input() set preview(preview: boolean) {
    if (preview != null) {
      this._preview = preview;
      this.getFiles();
    }
  }

  _preview: boolean;


  userMap: Map<string, string>;
  @Output() changeTab: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();
  @ViewChild(FileListComponent) fileListComponent: FileListComponent;

  loadingFinished = true;
  loggedAccountEmail: string;
  private componentDestroyed: Subject<void> = new Subject();
  infinityScroll: InfinityScrollUtil;


  constructor(
    private personService: PersonService,
    private notificationService: NotificationService,
    private fileService: FileService,
    private loginService: LoginService) {
    this.initInfinityScroll();
  }

  ngOnInit(): void {
    this.loggedAccountEmail = this.loginService.getLoggedAccountEmail();
    this.getAccountEmails();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }


  private initInfinityScroll(): void {
    this.infinityScroll = new InfinityScrollUtil(
      this,
      this.componentDestroyed,
      this.notificationService,
      'files.notification.getFilesError',
      'created,desc'
    );
  }

  public getFiles(): void {
    if (this._crmObject != null && this._preview != null) {
      this.fileListComponent.clearQueue();
      this.infinityScroll.initData();
    }
  }

  getItemObservable(selection: FilterStrategy, pageRequest: PageRequest): Observable<Page<FileInfo>> {
    if (this._crmObject == null || this._preview == null) {
      return of(new EmptyPage<FileInfo>());
    }
    if (this._preview) {
      pageRequest.size = '3';
    }
    return this.fileService.getFilesInGroup(this._crmObject.type, this._crmObject.id, pageRequest);
  }

  getInitFileGroup(): CrmObject {
    if (this._crmObject) {
      return {type: this._crmObject.type, id: this._crmObject.id, label: this.objectName};
    }
  }

  addFile(): void {
    this.fileListComponent.addFile();
  }

  private getAccountEmails(): void {
    this.personService.getAccountEmails()
      .pipe(
        takeUntil(this.componentDestroyed),
        map(emails => new Map<string, string>(emails.map(i => [i.email, `${i.name} ${i.surname}`] as [string, string]))),
      ).subscribe(
      users => this.userMap = users,
      (err) => this.notificationService.notify('customer.dashboard.contacts.notification.getAccountsError', NotificationType.ERROR)
    );
  }
}

