import {Injectable} from '@angular/core';
import {FileItem, FileUploader} from 'ng2-file-upload';
import {LoginService} from '../login/login.service';
import {FileAuthTypeEnum} from './file.model';
import {UrlUtil} from '../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class CrmNewVersionUploaderService extends FileUploader {

  static readonly fileServiceUrl = `${UrlUtil.url}/crm-file-service/files`;
  fileAuthTypeEnum: FileAuthTypeEnum;

  constructor(private loginService: LoginService) {
    super({url: CrmNewVersionUploaderService.fileServiceUrl, autoUpload: false, queueLimit: 1});
  }

  setFileAuthTypeEnum(fileAuthTypeEnum: FileAuthTypeEnum) {
    this.fileAuthTypeEnum = fileAuthTypeEnum;
  }

  uploadNewVersion(fileItem: FileItem, fileId: number): void {
    fileItem.url = `${CrmNewVersionUploaderService.fileServiceUrl}/${fileId}/new-version`;
    fileItem['submittedAsSingle'] = true;
    this.uploadItem(fileItem);
  }


  uploadItem(item: FileItem): void {
    if (!this.loginService.isLoggedIn()) {
      this.loginService.getRefreshTokenSubscription().subscribe(() => {
        super.uploadItem(item);
      });
    } else {
      super.uploadItem(item);
    }
  }

  onBeforeUploadItem(fileItem: FileItem): any {
    fileItem.headers = [
      {name: 'Authorization', value: 'Bearer ' + this.loginService.getToken()},
      {name: 'Auth-controller-type', value: this.fileAuthTypeEnum}];
    fileItem.withCredentials = false;
  }
}

