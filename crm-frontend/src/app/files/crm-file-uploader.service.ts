import {Injectable} from '@angular/core';
import {FileItem, FileUploader} from 'ng2-file-upload';
import {LoginService} from '../login/login.service';
import {FileAuthTypeEnum, FileFormData} from './file.model';
import {UrlUtil} from '../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class CrmFileUploaderService extends FileUploader {

  static readonly fileServiceUrl = `${UrlUtil.url}/crm-file-service/files`;
  fileAuthTypeEnum: FileAuthTypeEnum;

  constructor(private loginService: LoginService) {
    super({url: CrmFileUploaderService.fileServiceUrl});
  }

  setFileAuthTypeEnum(fileAuthTypeEnum: FileAuthTypeEnum) {
    this.fileAuthTypeEnum = fileAuthTypeEnum;
  }

  uploadItemWithForm(fileItem: FileItem, form: FileFormData): void {
    fileItem['submittedAsSingle'] = true;
    fileItem.onBuildForm = this.getOnBuildItemFormForNewFile(form);
    this.uploadItem(fileItem);
  }

  uploadItems(items: FileItem[], form: FileFormData): void {
    items.forEach(item => {
      item.onBuildForm = this.getOnBuildItemFormForNewFile(form);
      this.uploadItem(item);
    });
  }

  uploadItem(item: FileItem): void {
    if (!this.loginService.isLoggedIn()) {
      this.loginService.getRefreshTokenSubscription().subscribe(() => {
        super.uploadItem(item);
      });
    } else {
      super.uploadItem(item);
    }
  }

  getOnBuildItemFormForNewFile(fileData: FileFormData): (form: any) => void {
    return (form) => {
      form.append('fileData', new Blob(
        [JSON.stringify(fileData)],
        {type: 'application/json'}
      ));
    };
  }


  onBeforeUploadItem(fileItem: FileItem): any {
    fileItem.headers = [
      {name: 'Authorization', value: 'Bearer ' + this.loginService.getToken()},
      {name: 'Auth-controller-type', value: this.fileAuthTypeEnum}];
    fileItem.withCredentials = false;
  }

  getItemsWithError(): FileItem[] {
    return this.queue.filter(item => item.isError);
  }

  getItemsSubmittedAsGroup(): FileItem[] {
    return this.queue.filter(item => !item['submittedAsSingle']);
  }

}
