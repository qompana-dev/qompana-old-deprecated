import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {FileAuthTypeEnum, FileEvent, FileFormData, FileInfo, FileSizeDto} from './file.model';
import {Page, PageRequest} from '../shared/pagination/page.model';
import {CrmObject, CrmObjectType} from '../shared/model/search.model';
import {getHeaderForAuthType} from '../shared/permissions-utils';
import {UrlUtil} from '../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  public readonly fileServiceUrl = `${UrlUtil.url}/crm-file-service`;
  public crmObjectFilesChanged: Subject<CrmObject> = new Subject<CrmObject>();

  constructor(private http: HttpClient) {
  }

  public getLoggedUserFiles(pageRequest: PageRequest, search?: string): Observable<Page<FileInfo>> {
    const params = new HttpParams({fromObject: pageRequest as any});
    const searchArg = (search) ? `?search=${search}` : '';
    return this.http.get<Page<FileInfo>>(`${this.fileServiceUrl}/files/mine${searchArg}`, {params});
  }

  public getFilesInGroup(groupType: CrmObjectType, groupId: number, pageRequest: PageRequest, fileAuthTypeEnum?: FileAuthTypeEnum, search?: string): Observable<Page<FileInfo>> {
    const params = new HttpParams({fromObject: pageRequest as any});
    const searchArg = (search) ? `?search=${search}` : '';
    return this.http.get<Page<FileInfo>>(`${this.fileServiceUrl}/files/group-type/${groupType}/group-id/${groupId}${searchArg}`, {params, headers: getHeaderForAuthType(fileAuthTypeEnum)});
  }

  deleteFiles(selectedFiles: FileInfo[], fileAuthTypeEnum?: FileAuthTypeEnum): Observable<void> {
    const params = new HttpParams().set('ids', selectedFiles.map(file => file.id).join(','));
    return this.http.delete<void>(`${this.fileServiceUrl}/files`, {params, headers: getHeaderForAuthType(fileAuthTypeEnum)});
  }

  deleteFile(fileId: number, fileAuthTypeEnum?: FileAuthTypeEnum): Observable<void> {
    const url = `${this.fileServiceUrl}/files/${fileId}`;
    return this.http.delete<void>(url, {headers: getHeaderForAuthType(fileAuthTypeEnum)});
  }

  public downloadFile(fileId: string, fileAuthTypeEnum?: FileAuthTypeEnum): Observable<Blob> {
    const url = `${this.fileServiceUrl}/files/${fileId}`;
    return this.http.get(url, {responseType: 'blob', headers: getHeaderForAuthType(fileAuthTypeEnum)});
  }

  public previewFile(fileId: string): Observable<Blob> {
    const url = `${this.fileServiceUrl}/files/preview/${fileId}`;
    return this.http.get(url, {responseType: 'blob'});
  }

  public updateFileInfo(fileId: string, fileForm: FileFormData, fileAuthTypeEnum?: FileAuthTypeEnum): Observable<FileInfo> {
    const url = `${this.fileServiceUrl}/files/${fileId}/info`;
    return this.http.put<FileInfo>(url, fileForm, {headers: getHeaderForAuthType(fileAuthTypeEnum)});
  }

  public getTags(tagPrefix: string, fileAuthTypeEnum?: FileAuthTypeEnum): Observable<string[]> {
    const url = `${this.fileServiceUrl}/tags?starts-with=${tagPrefix}`;
    return this.http.get<string[]>(url, {headers: getHeaderForAuthType(fileAuthTypeEnum)});
  }

  public getFileSize(fileIds: string[]): Observable<FileSizeDto[]> {
    const url = `${this.fileServiceUrl}/files/size`;
    return this.http.post<FileSizeDto[]>(url, fileIds);
  }

  public getAllUserFiles(search?: string): Observable<FileInfo[]> {
    const url = `${this.fileServiceUrl}/files/info`;
    const options = (search) ? {params: {search}} : undefined;
    return this.http.get<FileInfo[]>(url, options);
  }

  public getFileWithHistory(fileId: number, fileAuthTypeEnum?: FileAuthTypeEnum): Observable<FileEvent[]> {
    const url = `${this.fileServiceUrl}/files/${fileId}/history`;
    return this.http.get<FileEvent[]>(url, {headers: getHeaderForAuthType(fileAuthTypeEnum)});
  }

  public getFileInfo(fileId: string, fileAuthTypeEnum?: FileAuthTypeEnum): Observable<FileInfo> {
    const url = `${this.fileServiceUrl}/files/${fileId}/info`;
    return this.http.get<FileInfo>(url, {headers: getHeaderForAuthType(fileAuthTypeEnum)});
  }

  public searchForCustomersToAssign(term: string): Observable<CrmObject[]> {
    const url = `assets/mocks/assign-to-customer.mock.json`;
    return this.http.get<CrmObject[]>(url);
  }

  public searchForUsersToAssign(term: string): Observable<CrmObject[]> {
    const url = `assets/mocks/assign-to-user.mock.json`;
    return this.http.get<CrmObject[]>(url);
  }

  getFileById(fileId: number): Observable<FileInfo> {
    const url = `${this.fileServiceUrl}/files/by-id/${fileId}/info`;
    return this.http.get<FileInfo>(url);
  }

  public getAssociations(fileId: number): Observable<any[]> {
    const url = `${UrlUtil.url}/crm-association-service/associations/type/file/id/${fileId}`;
    return this.http.get<any[]>(url);
  }

  public crmObjectChanged(type: CrmObjectType, id: number): void {
    this.crmObjectFilesChanged.next({type, id});
  }
}
