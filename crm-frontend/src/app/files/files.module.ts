import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FileDetailsComponent} from './file-details/file-details.component';
import {FileHistoryComponent} from './file-history/file-history.component';
import {SharedModule} from '../shared/shared.module';
import {FileListComponent} from './file-list/file-list.component';
import {MatIconModule} from '@angular/material/icon';
import {FilePreviewDialogComponent} from './file-preview/file-preview-dialog.component';
import {FileListInTabComponent} from './file-list-in-tab/file-list-in-tab.component';

@NgModule({
  declarations: [
    FileDetailsComponent,
    FileHistoryComponent,
    FileListComponent,
    FileListInTabComponent,
    FilePreviewDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MatIconModule
  ],
  exports: [
    FileDetailsComponent,
    FileHistoryComponent,
    FileListComponent,
    FileListInTabComponent,
    FilePreviewDialogComponent
  ],
  entryComponents: [
    FilePreviewDialogComponent
  ]
})
export class FilesModule {
}
