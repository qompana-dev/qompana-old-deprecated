import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {MatCheckboxChange, MatTable, Sort} from '@angular/material';
import {SliderComponent} from '../../shared/slider/slider.component';
import {Observable, Subject} from 'rxjs';
import {
  deleteAssociatedFileDialogData,
  deleteFileDialogData,
  fileDeleteErrorTypes,
  FileFormData,
  FileInfo,
  ScanStatusEnum,
  uploadFileErrorDialogData
} from '../file.model';
import {FileItem, FileUploader} from 'ng2-file-upload';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {CrmFileUploaderService} from '../crm-file-uploader.service';
import {CrmNewVersionUploaderService} from '../crm-new-version-uploader.service';
import {FilePreviewService} from '../file-preview.service';
import {PersonService} from '../../person/person.service';
import {DeleteDialogService} from '../../shared/dialog/delete/delete-dialog.service';
import {AuthService} from '../../login/auth/auth.service';
import {LoginService} from '../../login/login.service';
import {FileService} from '../file.service';
import {debounceTime, map, startWith, takeUntil} from 'rxjs/operators';
import {saveAs} from 'file-saver';
import {CrmObject, CrmObjectType} from '../../shared/model/search.model';
import {FilePermissionUtil} from '../file-permission.util';
import {FileAuthUtil} from '../file-auth.util';
import {UNAUTHORIZED} from 'http-status-codes';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.scss'],
  providers:[CrmFileUploaderService]
})
export class FileListComponent implements OnInit, OnDestroy, OnChanges {

  columns = ['selectRow', 'fileIcon', 'originalName', 'created', 'format', 'authorFullName', 'tags', 'action'];

  @Input() files: FileInfo[];
  @Input() onlyList = false;
  @Input() userMap: Map<number, string>;
  @Input() loggedUserEmail: string;
  @Input() initFileGroup: CrmObject;
  @Input() loadingFinished: boolean;
  @Input() firstHeader: string;
  @Input() secondHeader: string;
  @Input() forType: CrmObjectType;
  @Output() onFilesRefresh: EventEmitter<void> = new EventEmitter<void>();
  @Output() onScrollEnd: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSortChange: EventEmitter<Sort> = new EventEmitter<Sort>();
  @Output() onSearchChange: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('table') table: MatTable<any>;
  @ViewChild('addFileDetails') addFileDetailsSlider: SliderComponent;
  @ViewChild('editFileDetails') editFileDetailsSlider: SliderComponent;
  @ViewChild('fileHistory') fileHistorySlider: SliderComponent;
  @ViewChild('fileInput') newFileInput: ElementRef;

  filteredColumns$: Observable<string[]>;
  selectedFiles: FileInfo[] = [];
  selectedFileForDetails: FileInfo;
  selectedFileForHistory: FileInfo;
  selectedFileIdForNewVersion: number;
  fileToUpload: FileItem;
  hasBaseDropZoneOver = false;
  private componentDestroyed: Subject<void> = new Subject();
  permissionUtil = FilePermissionUtil;

  searchForm: FormControl = new FormControl();
  scanStatusEnum = ScanStatusEnum;

  constructor(
    public uploader: CrmFileUploaderService,
    public newVersionUploader: CrmNewVersionUploaderService,
    public filePreviewService: FilePreviewService,
    private personService: PersonService,
    private deleteDialogService: DeleteDialogService,
    private notificationService: NotificationService,
    private authService: AuthService,
    private loginService: LoginService,
    private fileService: FileService) {

    this.uploader.onCompleteAll = () => this.handleCompleteAllFiles();
    this.uploader.onAfterAddingAll = (fileItems) => this.handleAddingFiles(fileItems);
    this.newVersionUploader.onAfterAddingAll = (fileItems) => this.handleAddNewVersion(fileItems);

    this.setSearchFormChangeSubscription();
  }


  ngOnInit(): void {
    this.filterColumns();
    this.uploader.clearQueue();
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.uploader.setFileAuthTypeEnum(FileAuthUtil.getFileAuthType(this.forType));
    this.newVersionUploader.setFileAuthTypeEnum(FileAuthUtil.getFileAuthType(this.forType));
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  setSearchFormChangeSubscription(): void {
    this.searchForm.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250))
      .subscribe((value: string) => {
        this.onSearchChange.emit(value);
      });
  }

  tryDeleteSingleFile(fileId: number, dbFileId: number): void {
    deleteFileDialogData.numberToDelete = 1;
    deleteFileDialogData.onConfirmClick = () => this.checkIfAssociationsPresentAndDelete(fileId, dbFileId);
    this.openDeleteDialog();
  }

  tryDeleteSelectedFiles(): void {
    deleteFileDialogData.numberToDelete = this.selectedFiles.length;
    deleteFileDialogData.onConfirmClick = () => this.deleteFiles();
    this.openDeleteDialog();
  }

  toggleFile($event: MatCheckboxChange, file: FileInfo): void {
    if ($event.checked) {
      this.selectedFiles.push(file);
    } else {
      const index = this.selectedFiles.indexOf(file);
      if (index !== -1) {
        this.selectedFiles.splice(index, 1);
      }
    }
  }

  uploadFile(file: [FileItem, FileFormData]): void {
    const fileItem: FileItem = file[0];
    if (!this.checkFileSize(fileItem)) {
      this.notificationService.notify('files.notification.tooBigFile', NotificationType.ERROR);
      this.uploader.clearQueue();
      return;
    }
    this.uploader.uploadItemWithForm(file[0], file[1]);
    fileItem.onSuccess = response => {
      this.onFilesRefresh.emit();
      this.notifyFileUploadSuccess(fileItem);
      this.uploader.clearQueue();
    };
    fileItem.onError = response => {
      uploadFileErrorDialogData.onConfirmClick = () => this.uploader.uploadItemWithForm(file[0], file[1]);
      uploadFileErrorDialogData.onCancelClick = () => this.uploader.clearQueue();
      this.deleteDialogService.open(uploadFileErrorDialogData);
    };
  }

  updateVersion(fileId: number, input: HTMLInputElement): void {
    this.selectedFileIdForNewVersion = fileId;
    input.click();
  }

  onScroll(e: any): void {
    this.onScrollEnd.emit(e);
  }

  // upload

  changeSort($event: Sort): void {
    this.onSortChange.emit($event);
  }

  showDetails(file: FileInfo): void {
    this.fileService.getFileInfo(file.id, FileAuthUtil.getFileAuthType(this.forType)).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(freshFile => {
        this.selectedFileForDetails = freshFile;
        this.editFileDetailsSlider.open();
      },
      err => this.notificationService.notifyError(fileDeleteErrorTypes, err.status)
    );
  }

  download(file: FileInfo): void {
    this.fileService.downloadFile(file.id, FileAuthUtil.getFileAuthType(this.forType)).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      blob => saveAs(blob, file.name)
    );
  }

  getFilesData(): any[] {
    return [
      ...(this.getQueuedFiles(this.newVersionUploader)),
      ...(this.getQueuedFiles(this.uploader)),
      ...this.files
    ].filter(value => !!value);
  }

  preview(file: FileInfo): void {
    this.filePreviewService.showPreviewOf(file);
  }

  isFileOwner(file: FileInfo): boolean {
    return ((file && file.author) === this.loggedUserEmail);
  }

  showHistory(file: FileInfo): void {
    this.selectedFileForHistory = {...file};
    this.fileHistorySlider.open();
  }

  private deleteFiles(): void {
    if (this.selectedFiles.length === 0) {
      return;
    }
    this.fileService.deleteFiles(this.selectedFiles, FileAuthUtil.getFileAuthType(this.forType)).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(() => {
        this.onFilesRefresh.emit();
        this.notificationService.notify('files.notification.deleteFilesSuccess', NotificationType.SUCCESS);
        this.selectedFiles = [];
      },
      err => this.notificationService.notifyError(fileDeleteErrorTypes, err.status)
    );
  }

  private checkIfAssociationsPresentAndDelete(fileId: number, dbFileId: number): void {
    this.fileService.getAssociations(dbFileId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe((associations) => {
      if (associations.length > 0) {
        this.tryDeleteAssociatedFile(fileId, associations.length);
      } else {
        this.deleteFile(fileId);
      }
    });
  }

  private deleteFile(fileId: number): void {
    this.fileService.deleteFile(fileId, FileAuthUtil.getFileAuthType(this.forType)).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      () => {
        this.onFilesRefresh.emit();
        this.notificationService.notify('files.notification.deleteFileSuccess', NotificationType.SUCCESS);
      },
      err => this.notificationService.notifyError(fileDeleteErrorTypes, err.status)
    );
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(deleteFileDialogData);
  }

  private handleCompleteAllFiles(): void {
    if (this.uploader.getItemsSubmittedAsGroup().length === 0) {
      return;
    }
    const errorItems = this.uploader.getItemsWithError();
    if (errorItems.length === 0) {
      this.onFilesRefresh.emit();
      this.notifyFilesUploadSuccess();
      this.uploader.clearQueue();
    } else {
      uploadFileErrorDialogData.onConfirmClick = () => this.uploader.uploadItems(errorItems, this.getFileFormData(this.initFileGroup));
      uploadFileErrorDialogData.onCancelClick = () => this.uploader.clearQueue();
      this.deleteDialogService.open(uploadFileErrorDialogData);
    }
  }

  private handleAddingFiles(fileItems: FileItem[]): void {
    if (fileItems.length === 1) {
      this.fileToUpload = fileItems[0];
      this.addFileDetailsSlider.open();
    } else {
      this.uploader.uploadItems(fileItems, this.getFileFormData(this.initFileGroup));
    }
  }

  private handleAddNewVersion(fileItems: FileItem[]): void {
    const fileItem: FileItem = fileItems[0];
    if (!this.checkFileSize(fileItem)) {
      this.notificationService.notify('files.notification.tooBigFile', NotificationType.ERROR);
      this.uploader.clearQueue();
      return;
    }
    fileItem.onSuccess = response => {
      this.onFilesRefresh.emit();
      this.notifyNewFileVersionUploadSuccess();
      this.newVersionUploader.clearQueue();
    };
    fileItem.onError = response => {
      uploadFileErrorDialogData.onConfirmClick = () => this.newVersionUploader.uploadNewVersion(fileItems[0], this.selectedFileIdForNewVersion);
      uploadFileErrorDialogData.onCancelClick = () => this.newVersionUploader.clearQueue();
      this.deleteDialogService.open(uploadFileErrorDialogData);
    };
    this.newVersionUploader.uploadNewVersion(fileItems[0], this.selectedFileIdForNewVersion);
  }

  private getQueuedFiles(uploader: FileUploader): FileItem[] {
    return uploader.queue
      .map(file => {
        file['toUpload'] = true;
        return file;
      }).reverse();
  }

  private getFilteredColumns(): string[] {
    return this.columns
      .filter((item) => !this.authService.isPermissionPresent(FilePermissionUtil.getListKey(this.forType) + item, 'i')
        || item === 'action' || item === 'selectRow' || item === 'fileIcon');
  }

  private notifyFileUploadSuccess(fileItem: FileItem): void {
    this.notificationService.notify(
      'files.notification.fileAddedSuccess',
      NotificationType.SUCCESS,
      {filename: fileItem.file.name});
  }

  private notifyFilesUploadSuccess(): void {
    this.notificationService.notify(
      'files.notification.filesAddedSuccess',
      NotificationType.SUCCESS
    );
  }

  private notifyNewFileVersionUploadSuccess(): void {
    this.notificationService.notify(
      'files.notification.fileNewVersionAddedSuccess',
      NotificationType.SUCCESS
    );
  }

  private getFileFormData(initFileGroup: CrmObject): FileFormData {
    const fileData = new FileFormData();
    fileData.assignedObjects = !!initFileGroup ? [initFileGroup] : [];
    return fileData;
  }

  private isUnauthorized(response: string): boolean {
    return JSON.parse(response)['status'] === UNAUTHORIZED;
  }

  public addFile(): void {
    if (this.newFileInput) {
      this.newFileInput.nativeElement.click();
    }
  }

  private filterColumns(): void {
    if (this.onlyList) {
      this.columns.splice(this.columns.indexOf('selectRow'), 1);
    }
  }

  public clearQueue(): void {
    this.uploader.clearQueue();
  }

  private tryDeleteAssociatedFile(fileId: number, numberOfAssociations: number): void {
    deleteAssociatedFileDialogData.numberToDelete = numberOfAssociations;
    deleteAssociatedFileDialogData.onConfirmClick = () => this.deleteFile(fileId);
    this.deleteDialogService.open(deleteAssociatedFileDialogData);
  }

  private checkFileSize(item: FileItem): boolean {
    const fileMBSize = item._file.size / 1000000;
    const maxFileSize = 200.0;
    return fileMBSize < maxFileSize;
  }
}
