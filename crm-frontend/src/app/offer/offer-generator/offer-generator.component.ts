import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {SliderComponent} from '../../shared/slider/slider.component';
import {SliderService} from '../../shared/slider/slider.service';
import {OfferGeneratorService} from './offer-generator.service';
import {OpportunityProductsService} from '../../opportunity/opportunity-details/opportunity-products-card/opportunity-products.service';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {OpportunityProduct} from '../../opportunity/opportunity-details/opportunity-products-card/opportunity-product.model';
import {FormatCurrencyPipe} from '../../shared/pipe/format-currency.pipe';
import {Offer, OfferProductDto} from '../offer.model';
import * as uuid from 'uuid';
import {GlobalConfigService} from '../../global-config/global-config.service';
import {CompanyConfigComponent} from '../../global-config/company-config/company-config.component';
import {GlobalConfiguration} from '../../global-config/global-config.model';
import {StepPreviewComponent} from './steps/step-preview/step-preview.component';

@Component({
  selector: 'app-offer-generator',
  templateUrl: './offer-generator.component.html',
  styleUrls: ['./offer-generator.component.scss']
})
export class OfferGeneratorComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();
  private readonly uuid = uuid.v4();
  id: number;
  @ViewChild('stepPreview') stepPreviewComponent: StepPreviewComponent;

  @Input() slider: SliderComponent;

  @Input()
  set opportunityId(id: number) {
    this.id = id;
    this.offerGeneratorService.offer.opportunityId = id;
    this.offerGeneratorService.offer.uuid = this.uuid;
    this.loadProducts();
  }
  @Input() customerId: string;
  @Input() offerTitle: string;
  @Input() offerDescription: string;

  @Input() opportunityCurrency = '';

  constructor(private configService: GlobalConfigService,
              private sliderService: SliderService,
              private notificationService: NotificationService,
              public formatCurrencyPipe: FormatCurrencyPipe,
              private opportunityProductsService: OpportunityProductsService,
              private offerGeneratorService: OfferGeneratorService) {
    this.loadConfig();
  }

  ngOnInit(): void {
    if (this.slider) {
      this.slider.openEvent
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => {
          this.loadProducts();
          if (this.stepPreviewComponent.currentStepActive) {
            this.stepPreviewComponent.loadPreview();
          }
        });
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  closeSlider(): void {
    (this.slider) ? this.slider.close() : this.sliderService.closeSlider();
  }

  loadProducts(): void {
    if (this.id != null) {
      this.opportunityProductsService.getOpportunityProductsAsList(this.id, {page: '0', size: '10000'})
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(products => {
          this.offerGeneratorService.offer.products = products.map(c => this.getOfferProductDto(c));
          this.offerGeneratorService.offer.productsSummed = this.getSummedProducts(this.offerGeneratorService.offer.products);
        },
          () => this.notificationService.notify('offer.generator.productsLoadProblem', NotificationType.ERROR));
    }
  }

  getOfferProductDto(product: OpportunityProduct): OfferProductDto {
    return {
      name: product.name,
      price: this.formatCurrencyPipe.transform(product.price, product.currency),
      quantity: product.quantity,
      priceSummed: this.formatCurrencyPipe.transform(product.priceSummed, product.currency),
      unit: product.unit
    };
  }

  getSummedProducts(products: OfferProductDto[]): string {
    let summedProducts = 0;
    if (products && products.length > 0) {
      products.forEach(product => {
        const value = product.priceSummed.replace(/[^0-9,]/g, '').replace(',', '.');
        summedProducts += parseFloat(value);
      });
    }
    return this.formatCurrencyPipe.transform(summedProducts, this.opportunityCurrency);
  }

  private loadConfig(): void {
    this.configService.getGlobalParamValue(CompanyConfigComponent.DEFAULT_COMPANY_CONFIG_PARAM_KEY)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((globalConfiguration: GlobalConfiguration) => {
        this.offerGeneratorService.setOffer(JSON.parse(globalConfiguration.value));
        this.offerGeneratorService.setOfferAccountData();
      }, () => this.notificationService.notify('offer.generator.configLoadProblem', NotificationType.ERROR));
  }
}
