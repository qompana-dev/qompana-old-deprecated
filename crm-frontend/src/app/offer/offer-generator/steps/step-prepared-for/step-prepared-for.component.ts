import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {OfferGeneratorService} from '../../offer-generator.service';
import {debounceTime, filter, switchMap, takeUntil} from 'rxjs/operators';
import {AddressForPreparedForDto} from '../../../offer.model';
import {CrmObject, CrmObjectType} from '../../../../shared/model/search.model';
import {AssignToGroupService} from '../../../../shared/assign-to-group/assign-to-group.service';
import {CustomerService} from '../../../../customer/customer.service';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {Customer} from '../../../../customer/customer.model';

@Component({
  selector: 'app-step-prepared-for',
  templateUrl: './step-prepared-for.component.html',
  styleUrls: ['./step-prepared-for.component.scss']
})
export class StepPreparedForComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();
  private valueChangesSubscription: Subscription;

  preparedForGroup: FormGroup;
  filteredCustomers: CrmObject[] = [];

  // tslint:disable-next-line:variable-name
  _opportunityCustomerId: number;

  // @formatter:off
  get companyName(): AbstractControl { return this.preparedForGroup.get('companyName'); }
  get companyAddress(): AbstractControl { return this.preparedForGroup.get('companyAddress'); }
  get companyPhone(): AbstractControl { return this.preparedForGroup.get('companyPhone'); }
  get companyEmail(): AbstractControl { return this.preparedForGroup.get('companyEmail'); }
  get www(): AbstractControl { return this.preparedForGroup.get('www'); }
  get customerId(): AbstractControl { return this.preparedForGroup.get('customerId'); }
  get customerSearch(): AbstractControl { return this.preparedForGroup.get('customerSearch'); }
  // @formatter:on

  @Input()
  set opportunityCustomerId(id: number) {
    this._opportunityCustomerId = id;
    this.setValuesToForm();
  }

  constructor(private fb: FormBuilder,
              private assignToGroupService: AssignToGroupService,
              private customerService: CustomerService,
              private notificationService: NotificationService,
              private offerGeneratorService: OfferGeneratorService) {
    this.createForm();
  }

  ngOnInit(): void {
    this.subscribeValueChange();
    this.setValuesToForm();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private createForm(): void {
    if (!this.preparedForGroup) {
      this.preparedForGroup = this.fb.group({
        companyName: [null],
        companyAddress: [null],
        companyPhone: [null],
        companyEmail: [null, [Validators.email]],
        www: [null],

        customerId: [null],
        customerSearch: [null]
      });
      this.subscribeCustomerSearch();
      this.subscribeCustomerId();
      this.setValues();
    }
  }

  private subscribeCustomerSearch(): void {
    this.customerSearch.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      filter((input: string) => !!input && input.length >= 3),
      switchMap(((input: string) => this.assignToGroupService.searchForType(CrmObjectType.customer, input, [CrmObjectType.customer]))
      )).subscribe(
      (result: CrmObject[]) => {
        this.filteredCustomers = result;
      }
    );
  }

  private subscribeCustomerId(): void {
    this.customerId.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed),
        filter((value) => !!value)
      ).subscribe((value) => {
      if (value) {
        this.customerService.getCustomerById(+value).subscribe(
          customer => this.setCustomer(customer),
          (err) => this.notificationService.notify('offer.generator.stepPreparedFor.unableToLoadCustomer', NotificationType.ERROR)
        );
      }
    });
  }

  customerSelect(customer: CrmObject): void {
    if (customer && customer.type === CrmObjectType.customer) {
      this.customerId.patchValue(customer.id);
      this.customerSearch.patchValue(customer.label);
    }
  }

  private setCustomer(customer: Customer): void {
    this.companyName.patchValue(customer.name);
    this.companyAddress.patchValue(this.getCustomerAddress(customer));
    this.companyPhone.patchValue(customer.phone);
    this.www.patchValue(customer.website);
  }

  private getCustomerAddress(customer: Customer): string {
    let companyAddr = [];
    if (customer && customer.address) {
      companyAddr = [customer.address.street, customer.address.zipCode, customer.address.city];
    }
    return companyAddr.filter(c => c).join(', ');
  }

  setValues(): void {
    if (!this.offerGeneratorService.offer.preparedFor) {
      this.offerGeneratorService.offer.preparedFor = {} as AddressForPreparedForDto;
    }
    this.offerGeneratorService.offer.preparedFor.companyName = this.companyName.value;
    this.offerGeneratorService.offer.preparedFor.companyAddress = this.companyAddress.value;
    this.offerGeneratorService.offer.preparedFor.companyPhone = this.companyPhone.value;
    this.offerGeneratorService.offer.preparedFor.companyEmail = this.companyEmail.value;
    this.offerGeneratorService.offer.preparedFor.www = this.www.value;
  }

  setValuesToForm(): void {
    if (this.preparedForGroup && this.offerGeneratorService.offer && this.offerGeneratorService.offer.preparedFor) {
      this.customerId.patchValue(this._opportunityCustomerId);
    }
  }

  private subscribeValueChange(onlyStopSubscription: boolean = false): void {
    if (this.valueChangesSubscription) {
      this.valueChangesSubscription.unsubscribe();
    }
    if (!onlyStopSubscription) {
      this.valueChangesSubscription = this.preparedForGroup.valueChanges
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => this.setValues());
    }
  }
}
