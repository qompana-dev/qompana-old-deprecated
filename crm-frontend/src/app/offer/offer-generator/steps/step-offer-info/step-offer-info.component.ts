import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {OfferGeneratorService} from '../../offer-generator.service';
import {OfferType} from '../../../offer.model';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {FormatDatePipe} from '../../../../shared/pipe/formatDate.pipe';
import {TimesUtil} from '../../../../shared/times.util';
import * as cloneDeep from 'lodash/cloneDeep';
import * as moment from 'moment';
import {DateUtil} from '../../../../shared/util/date.util';

@Component({
  selector: 'app-step-offer-info',
  templateUrl: './step-offer-info.component.html',
  styleUrls: ['./step-offer-info.component.scss']
})
export class StepOfferInfoComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();
  private valueChangesSubscription: Subscription;

  offerTypeEnum = OfferType;
  allOfferTypes: string[] = Object.keys(this.offerTypeEnum);

  imageSrc = '';

  offerInfoGroup: FormGroup;
  correctLogo = true;

  expirationTypes = Array(3).fill(0).map((x, i) => (i + 1));

  // @formatter:off
  get offerType(): AbstractControl { return this.offerInfoGroup.get('offerType'); }
  get offerDate(): AbstractControl { return this.offerInfoGroup.get('offerDate'); }
  get expiration(): AbstractControl { return this.offerInfoGroup.get('expiration'); }
  get logo(): AbstractControl { return this.offerInfoGroup.get('logo'); }
  get signatureTitle(): AbstractControl { return this.offerInfoGroup.get('signatureTitle'); }
  // @formatter:on

  constructor(private fb: FormBuilder,
              private formatDatePipe: FormatDatePipe,
              private offerGeneratorService: OfferGeneratorService) {
    this.createForm();
  }

  ngOnInit(): void {
    this.subscribeValueChange();
    this.setValuesToForm();
    this.offerGeneratorService.updateFormsSubject
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.setValuesToForm());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private createForm(): void {
    if (!this.offerInfoGroup) {
      this.offerInfoGroup = this.fb.group({
        offerType: [OfferType.OFFER_1, [Validators.required]],
        logo: [null],
        offerDate: [null],
        expiration: [null],
        signatureTitle: [null]
      });
      this.setValuesToForm();
      this.setValues();
    }
  }

  setValues(): void {
    this.offerGeneratorService.offer.reportType = this.offerType.value;
    this.offerGeneratorService.offer.base64EncodedLogo = this.getBase64EncodedLogo();
    this.offerGeneratorService.offer.offerDate = this.formatDatePipe.transform(this.offerDate.value, false);
    this.offerGeneratorService.offer.expiration = this.expiration.value;
    this.offerGeneratorService.offer.signatureTitle = this.signatureTitle.value;
  }

  getBase64EncodedLogo(): string {
    if (this.logo.value) {
      return this.logo.value.replace(/^data:image\/[a-z]+;base64,/, '');
    } else {
      return undefined;
    }
  }

  getBase64DecodedLogo(logo: string): string {
    if (logo != null) {
      return 'data:image/png;base64,' + logo;
    } else {
      return undefined;
    }
  }

  logoLoaded(error: boolean): void {
    this.correctLogo = !error;
    this.logo.updateValueAndValidity();
  }

  handleInputChange(event: any): void {
    const file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
    const reader = new FileReader();
    if (!file.type.match(/image-*/)) {
      console.error('invalid format');
      return;
    }
    reader.onload = (e: any) => {
      console.log(e, 'reader.onload');
      this.imageSrc = e.target.result;
      this.logo.patchValue(this.imageSrc);
    };
    reader.readAsDataURL(file);
  }

  setValuesToForm(): void {
    this.subscribeValueChange(true);
    if (this.offerInfoGroup && this.offerGeneratorService.offer) {
      this.logo.patchValue(this.getBase64DecodedLogo(this.offerGeneratorService.offer.base64EncodedLogo), {emitEvent: false});
      this.imageSrc = this.getBase64DecodedLogo(this.offerGeneratorService.offer.base64EncodedLogo);
      this.correctLogo = (this.imageSrc != null);
    }
    this.subscribeValueChange();
  }

  private subscribeValueChange(onlyStopSubscription: boolean = false): void {
    if (this.valueChangesSubscription) {
      this.valueChangesSubscription.unsubscribe();
    }
    if (!onlyStopSubscription) {
      this.valueChangesSubscription = this.offerInfoGroup.valueChanges
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => this.setValues());
    }
  }
}
