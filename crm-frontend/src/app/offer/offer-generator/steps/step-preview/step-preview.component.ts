import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {FormBuilder} from '@angular/forms';
import {OfferGeneratorService} from '../../offer-generator.service';
import {takeUntil} from 'rxjs/operators';
import {PagesLoadedEvent} from 'ngx-extended-pdf-viewer/lib/pages-loaded-event';
import {saveAs} from 'file-saver';
import {TranslateService} from '@ngx-translate/core';
import {FormatDatePipe} from '../../../../shared/pipe/formatDate.pipe';
import {MatHorizontalStepper} from '@angular/material/stepper';
import {StepperSelectionEvent} from '@angular/cdk/stepper';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {HttpEvent, HttpEventType} from '@angular/common/http';
import {CrmObjectType} from '../../../../shared/model/search.model';
import {FileService} from '../../../../files/file.service';

@Component({
  selector: 'app-step-preview',
  templateUrl: './step-preview.component.html',
  styleUrls: ['./step-preview.component.scss']
})
export class StepPreviewComponent implements OnInit, OnDestroy {
  @Input() stepper: MatHorizontalStepper;
  @Input() opportunityId: number;

  private componentDestroyed: Subject<void> = new Subject();
  blob: Blob;
  inProgress = false;
  isError = false;
  page = 1;
  zoom = 100;
  numberOfPages = 0;
  bottomToolbarVisible = false;

  currentStepActive = false;
  uploading = false;

  constructor(private fb: FormBuilder,
              public formatDatePipe: FormatDatePipe,
              private fileService: FileService,
              private notificationService: NotificationService,
              private translateService: TranslateService,
              private offerGeneratorService: OfferGeneratorService) {
  }

  ngOnInit(): void {
    if (this.stepper) {
      this.stepper.selectionChange
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((selection: StepperSelectionEvent) => {
          if (selection.selectedIndex === this.stepper.steps.length - 1) {
            this.currentStepActive = true;
            this.loadPreview();
          } else {
            this.currentStepActive = false;
          }
        });
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  onPagesLoaded($event: PagesLoadedEvent): void {
    this.numberOfPages = $event.pagesCount;
    this.bottomToolbarVisible = true;
  }

  loadPreview(): void {
    this.isError = false;
    this.inProgress = true;
    this.bottomToolbarVisible = false;
    this.offerGeneratorService.getPreview().pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      blob => this.blob = blob,
      err => this.handleError(),
      () => this.stopProgress()
    );
  }

  private stopProgress(): void {
    this.inProgress = false;
  }

  private handleError(): void {
    this.stopProgress();
    this.isError = true;
  }

  download(): void {
    if (this.blob) {
      saveAs(this.blob, this.getOfferName());
    }
  }

  saveInCrm(): void {
    if (this.blob && !this.uploading && this.opportunityId) {
      this.uploading = true;
      this.offerGeneratorService
        .uploadFile(new File([this.blob], this.getOfferName()),
          {
            tags: ['offer'],
            assignedObjects: [{type: CrmObjectType.opportunity, id: this.opportunityId}],
            description: this.getOfferName()
          }).pipe(takeUntil(this.componentDestroyed)).subscribe(
        (event: HttpEvent<any>) => {
          if (event.type === HttpEventType.Response) {
            this.notificationService.notify('offer.generator.stepPreview.offerSaved', NotificationType.SUCCESS);
            this.uploading = false;
            this.fileService.crmObjectChanged(CrmObjectType.opportunity, this.opportunityId);
          }
        },
        () => {
          this.notificationService.notify('offer.generator.stepPreview.saveError', NotificationType.ERROR);
          this.uploading = false;
        }
      );
    }
  }

  private getOfferName(): string {
    return this.translateService
      .instant('offer.generator.stepPreview.fileName') + '-' + this.formatDatePipe.transform(new Date().toString(), true) + '.pdf';
  }
}
