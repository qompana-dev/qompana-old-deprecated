import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {NoteType} from '../../../offer.model';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {OfferGeneratorService} from '../../offer-generator.service';

@Component({
  selector: 'app-step-note',
  templateUrl: './step-note.component.html',
  styleUrls: ['./step-note.component.scss']
})
export class StepNoteComponent implements OnInit, OnDestroy {
  @Input() noteType: NoteType;
  @Input() inGlobalConfig = false;

  noteForm: FormGroup;
  private componentDestroyed: Subject<void> = new Subject();
  private valueChangesSubscription: Subscription;

  // @formatter:off
  get title1(): AbstractControl { return this.noteForm.get('title1'); }
  get description1(): AbstractControl { return this.noteForm.get('description1'); }
  get title2(): AbstractControl { return this.noteForm.get('title2'); }
  get description2(): AbstractControl { return this.noteForm.get('description2'); }
  // @formatter:on


  private opportunityTitle: string;
  private opportunityDescription: string;

  @Input()
  set offerTitle(opportunityTitle: string) {
    this.opportunityTitle = opportunityTitle;
    this.setValuesToForm();
  }

  @Input()
  set offerDescription(opportunityDescription: string) {
    this.opportunityDescription = opportunityDescription;
    this.setValuesToForm();
  }

  constructor(private fb: FormBuilder,
              private offerGeneratorService: OfferGeneratorService) {
    this.createForm();
  }

  ngOnInit(): void {
    this.subscribeValueChange();
    this.setValuesToForm();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private createForm(): void {
    if (!this.noteForm) {
      this.noteForm = this.fb.group({
        title1: [''],
        description1: [''],
        title2: [''],
        description2: ['']
      });
    }
  }

  setValues(): void {
    if (this.noteType === NoteType.TITLE) {
      this.offerGeneratorService.offer.firstPageTitle = this.title1.value;
      this.offerGeneratorService.offer.firstPageDescription = this.description1.value;
      this.offerGeneratorService.offer.secondPageTitle = this.title2.value;
      this.offerGeneratorService.offer.secondPageDescription = this.description2.value;
    } else {
      this.offerGeneratorService.offer.termsOfPurchaseTitle = this.title1.value;
      this.offerGeneratorService.offer.termsOfPurchaseDescription = this.description1.value;
      this.offerGeneratorService.offer.additionalNoteTitle = this.title2.value;
      this.offerGeneratorService.offer.additionalNoteDescription = this.description2.value;
    }
  }

  getTitle1(header: boolean): string {
    const result = (this.noteType === NoteType.TITLE) ? 'title' : 'termsOfPurchase';
    return (header) ? result + 'Header' : result;
  }

  getTitle2(header: boolean): string {
    const result = (this.noteType === NoteType.TITLE) ? 'description' : 'additionalNotes';
    return (header) ? result + 'Header' : result;
  }


  setValuesToForm(): void {
    this.subscribeValueChange(true);
    if (this.noteForm && this.offerGeneratorService.offer) {
      if (this.noteType === NoteType.TITLE) {
        this.title1.patchValue(this.opportunityTitle, {emitEvent: false});
        this.description1.patchValue(this.opportunityDescription, {emitEvent: false});
      }
    }
    this.subscribeValueChange();
  }

  private subscribeValueChange(onlyStopSubscription: boolean = false): void {
    if (this.valueChangesSubscription) {
      this.valueChangesSubscription.unsubscribe();
    }
    if (!onlyStopSubscription) {
      this.valueChangesSubscription = this.noteForm.valueChanges
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => this.setValues());
    }
  }
}
