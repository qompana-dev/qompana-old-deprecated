import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject, Subscription} from 'rxjs';
import {OfferGeneratorService} from '../../offer-generator.service';
import {AddressForPreparedByDto} from '../../../offer.model';
import {takeUntil} from 'rxjs/operators';
import {LoginService} from '../../../../login/login.service';
import {NotificationService} from '../../../../shared/notification.service';
import {PhoneService} from '../../../../shared/phone-fields/phone.service';

@Component({
  selector: 'app-step-prepared-by',
  templateUrl: './step-prepared-by.component.html',
  styleUrls: ['./step-prepared-by.component.scss']
})
export class StepPreparedByComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();
  private valueChangesSubscription: Subscription;

  preparedByGroup: FormGroup;

  // @formatter:off
  get companyName(): AbstractControl { return this.preparedByGroup.get('companyName'); }
  get companyAddress(): AbstractControl { return this.preparedByGroup.get('companyAddress'); }
  get companyPhone(): AbstractControl { return this.preparedByGroup.get('companyPhone'); }
  get companyEmail(): AbstractControl { return this.preparedByGroup.get('companyEmail'); }
  get www(): AbstractControl { return this.preparedByGroup.get('www'); }
  get name(): AbstractControl { return this.preparedByGroup.get('name'); }
  get position(): AbstractControl { return this.preparedByGroup.get('position'); }
  get phone(): AbstractControl { return this.preparedByGroup.get('phone'); }
  get email(): AbstractControl { return this.preparedByGroup.get('email'); }
  get nip(): AbstractControl { return this.preparedByGroup.get('nip'); }
  get regon(): AbstractControl { return this.preparedByGroup.get('regon'); }
  get krs(): AbstractControl { return this.preparedByGroup.get('krs'); }
  // @formatter:on

  constructor(private fb: FormBuilder,
              private loginService: LoginService,
              private offerGeneratorService: OfferGeneratorService,
              public phoneService: PhoneService) {
    this.createForm();
  }

  ngOnInit(): void {
    this.subscribeValueChange();
    this.setValuesToForm();
    this.offerGeneratorService.updateFormsSubject
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.setValuesToForm());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private subscribeValueChange(onlyStopSubscription: boolean = false): void {
    if (this.valueChangesSubscription) {
      this.valueChangesSubscription.unsubscribe();
    }
    if (!onlyStopSubscription) {
      this.valueChangesSubscription = this.preparedByGroup.valueChanges
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => this.setValues());
    }
  }

  private createForm(): void {
    if (!this.preparedByGroup) {
      this.preparedByGroup = this.fb.group({
        companyName: [null],
        companyAddress: [null],
        companyPhone: [null],
        companyEmail: [null, [Validators.email]],
        www: [null],
        name: [null],
        position: [null],
        phone: [null, [Validators.minLength(11)]],
        email: [null, [Validators.email]],
        nip: [null, [Validators.minLength(10)]],
        regon: [null, [Validators.minLength(9), Validators.maxLength(14),
          Validators.pattern('^(?=[0-9]*$)(?:.{9}|.{14})$')]],
        krs: [null]
      });
      this.setValuesToForm();
      this.setValues();
    }
  }

  setValues(): void {
    if (!this.offerGeneratorService.offer.preparedBy) {
      this.offerGeneratorService.offer.preparedBy = {} as AddressForPreparedByDto;
    }
    this.offerGeneratorService.offer.preparedBy.companyName = this.companyName.value;
    this.offerGeneratorService.offer.preparedBy.companyAddress = this.companyAddress.value;
    this.offerGeneratorService.offer.preparedBy.companyPhone = this.companyPhone.value;
    this.offerGeneratorService.offer.preparedBy.companyEmail = this.companyEmail.value;
    this.offerGeneratorService.offer.preparedBy.www = this.www.value;
    this.offerGeneratorService.offer.preparedBy.name = this.name.value;
    this.offerGeneratorService.offer.preparedBy.position = this.position.value;
    this.offerGeneratorService.offer.preparedBy.phone = this.phone.value;
    this.offerGeneratorService.offer.preparedBy.email = this.email.value;
    this.offerGeneratorService.offer.preparedBy.nip = this.nip.value;
    this.offerGeneratorService.offer.preparedBy.regon = this.regon.value;
    this.offerGeneratorService.offer.preparedBy.krs = this.krs.value;
  }

  setValuesToForm(): void {
    this.subscribeValueChange(true);
    if (this.preparedByGroup && this.offerGeneratorService.offer && this.offerGeneratorService.offer.preparedBy) {
      this.companyName.patchValue(this.offerGeneratorService.offer.preparedBy.companyName, {emitEvent: false});
      this.companyAddress.patchValue(this.offerGeneratorService.offer.preparedBy.companyAddress, {emitEvent: false});
      this.companyPhone.patchValue(this.offerGeneratorService.offer.preparedBy.companyPhone, {emitEvent: false});
      this.companyEmail.patchValue(this.offerGeneratorService.offer.preparedBy.companyEmail, {emitEvent: false});
      this.www.patchValue(this.offerGeneratorService.offer.preparedBy.www, {emitEvent: false});
      this.nip.patchValue(this.offerGeneratorService.offer.preparedBy.nip, {emitEvent: false});
      this.regon.patchValue(this.offerGeneratorService.offer.preparedBy.regon, {emitEvent: false});
      this.krs.patchValue(this.offerGeneratorService.offer.preparedBy.krs, {emitEvent: false});
      this.name.patchValue(this.offerGeneratorService.offer.preparedBy.name, {emitEvent: false});
      this.position.patchValue(this.offerGeneratorService.offer.preparedBy.position, {emitEvent: false});
      this.phone.patchValue(this.offerGeneratorService.offer.preparedBy.phone, {emitEvent: false});
      this.email.patchValue(this.offerGeneratorService.offer.preparedBy.email, {emitEvent: false});
    }
    this.subscribeValueChange();
  }
}
