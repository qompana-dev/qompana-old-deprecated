import {Injectable} from '@angular/core';
import {AddressForPreparedByDto, Offer} from '../offer.model';
import {HttpClient, HttpEvent, HttpParams, HttpRequest} from '@angular/common/http';
import {FileFormData} from '../../files/file.model';
import {Observable, Subject} from 'rxjs';
import * as cloneDeep from 'lodash/cloneDeep';
import {UrlUtil} from '../../shared/url.util';
import {LoginService} from '../../login/login.service';

@Injectable({
  providedIn: 'root'
})
export class OfferGeneratorService {
  offer: Offer = {} as Offer;
  public updateFormsSubject: Subject<void> = new Subject<void>();

  private offerUrl = `${UrlUtil.url}/crm-report-service`;

  constructor(private http: HttpClient,
              private loginService: LoginService) {
  }

  public getPreview(): Observable<Blob> {
    const url = `${this.offerUrl}/offer`;
    return this.http.post(url, this.offer, {responseType: 'blob'});
  }

  public saveOffer(): Observable<void> {
    const url = `${this.offerUrl}/offer/save`;
    return this.http.post<void>(url, this.offer);
  }

  public uploadFile(file: File, form: FileFormData): Observable<HttpEvent<any>> {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('fileData', new Blob(
      [JSON.stringify(form)],
      {type: 'application/json'}
    ));
    const params = new HttpParams();
    const options = {
      params,
      reportProgress: true,
    };
    const req = new HttpRequest('POST', `${UrlUtil.url}/crm-file-service/files`, formData, options);
    return this.http.request(req);
  }

  public setOffer(offer: Offer): void {
    if (offer) {
      this.offer.reportType = cloneDeep(offer.reportType);
      this.offer.base64EncodedLogo = cloneDeep(offer.base64EncodedLogo);
      this.offer.preparedBy = cloneDeep(offer.preparedBy);
      this.offer.offerDate = cloneDeep(offer.offerDate);
      this.offer.expiration = cloneDeep(offer.expiration);
      this.offer.signatureTitle = cloneDeep(offer.signatureTitle);
    }
    this.updateFormsSubject.next();
  }

  public setOfferAccountData(): void {
    if (this.offer) {
      if (!this.offer.preparedBy) {
        this.offer.preparedBy = {} as AddressForPreparedByDto;
      }

      this.offer.preparedBy.name = this.loginService.getLoggedAccountName();
      this.offer.preparedBy.position = this.loginService.getLoggedAccountRole();
      this.offer.preparedBy.phone = this.loginService.getLoggedAccountPhone();
      this.offer.preparedBy.email = this.loginService.getLoggedAccountEmail();
    }
    this.updateFormsSubject.next();
  }
}
