export interface Report {
  id: number;
}

export enum NoteType {
  TITLE = 'TITLE',
  TERMS = 'TERMS',
}

export interface Offer {
  reportType: OfferType;

  base64EncodedLogo: string;
  firstPageTitle: string;
  firstPageDescription: string;

  preparedFor: AddressForPreparedForDto;
  preparedBy: AddressForPreparedByDto;

  offerDate: string;
  expiration: string;

  secondPageTitle: string;
  secondPageDescription: string;

  products: OfferProductDto[];
  termsOfPurchaseTitle: string;
  termsOfPurchaseDescription: string;
  additionalNoteTitle: string;
  additionalNoteDescription: string;
  signatureTitle: string;

  productsSummed: string;

  opportunityId: number;
  uuid: string;
}

export interface AddressForPreparedByDto {
  companyName: string;
  companyAddress: string;
  companyPhone: string;
  companyEmail: string;
  www: string;
  name: string;
  position: string;
  phone: string;
  email: string;
  nip: string;
  regon: string;
  krs: string;
}

export interface AddressForPreparedForDto {
  companyName: string;
  companyAddress: string;
  companyPhone: string;
  companyEmail: string;
  www: string;
}

export interface OfferProductDto {
  name: string;
  price: string;
  quantity: number;
  priceSummed: string;
  unit: string;
}

export enum OfferType {
  OFFER_1 = 'OFFER_1',
  OFFER_2 = 'OFFER_2',
  OFFER_3 = 'OFFER_3',
}
