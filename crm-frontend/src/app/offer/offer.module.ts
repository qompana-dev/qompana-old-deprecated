import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {NgxMaskModule} from 'ngx-mask';
import {OfferGeneratorComponent} from './offer-generator/offer-generator.component';
import {StepPreparedByComponent} from './offer-generator/steps/step-prepared-by/step-prepared-by.component';
import {StepPreparedForComponent} from './offer-generator/steps/step-prepared-for/step-prepared-for.component';
import {StepNoteComponent} from './offer-generator/steps/step-note/step-note.component';
import {StepPreviewComponent} from './offer-generator/steps/step-preview/step-preview.component';
import {StepOfferInfoComponent} from './offer-generator/steps/step-offer-info/step-offer-info.component';

@NgModule({
  declarations: [
    OfferGeneratorComponent,
    StepPreparedByComponent,
    StepPreparedForComponent,
    StepNoteComponent,
    StepPreviewComponent,
    StepOfferInfoComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxMaskModule
  ],
  exports: [
    OfferGeneratorComponent
  ]
})
export class OfferModule {
}
