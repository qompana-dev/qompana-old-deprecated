import {
  Component,
  HostListener,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from "@angular/core";
import { DashboardWidget, WidgetTypeEnum } from "../dashboard.model";
import { DashboardService } from "../dashboard.service";
import { Subject } from "rxjs";
import { map, takeUntil } from "rxjs/operators";
import {
  NotificationService,
  NotificationType,
} from "../../shared/notification.service";
import { SliderComponent } from "../../shared/slider/slider.component";
import { CalendarPanelComponent } from "./panel/calendar-panel/calendar-panel.component";
import { PersonService } from "../../person/person.service";
import { DeleteDialogData } from "../../shared/dialog/delete/delete-dialog.component";
import { DeleteDialogService } from "../../shared/dialog/delete/delete-dialog.service";
import { OPTIONS } from "./dashboard.config";
import { GridsterConfig } from "angular-gridster2";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
})
export class DashboardComponent implements OnInit, OnDestroy {
  @ViewChild("widgetConfigurator") widgetConfiguratorSlider: SliderComponent;
  @ViewChildren("calendarPanelComponent")
  calendarPanelComponent: QueryList<CalendarPanelComponent>;

  options: GridsterConfig;
  edited = false;

  widgets: DashboardWidget[] = [];
  widgetTypeEnum = WidgetTypeEnum;

  private componentDestroyed: Subject<void> = new Subject();

  clickedIndex: number;
  userMap: Map<number, string> = new Map();

  fullScreenWidget: DashboardWidget;

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: "dashboard.dialog.remove.title",
    description: "dashboard.dialog.remove.description",
    confirmButton: "dashboard.dialog.remove.buttonYes",
    cancelButton: "dashboard.dialog.remove.buttonNo",
  };

  get dragEnable(): boolean {
    return (
      this.calendarPanelComponent.filter((panel) => panel.isSliderOpened)
        .length === 0
    );
  }

  constructor(
    private personService: PersonService,
    private deleteDialogService: DeleteDialogService,
    private notificationService: NotificationService,
    private dashboardService: DashboardService
  ) {}

  ngOnInit(): void {
    this.options = {
      ...OPTIONS,
      itemChangeCallback: () => this.saveDashboardSettings(),
    };
    this.getAccountEmails();
    this.loadDashboard();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  loadDashboard(): void {
    this.dashboardService
      .getDashboardSettings()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (widgetList: DashboardWidget[]) => {
          this.widgets = widgetList;
        },
        () =>
          this.notificationService.notify(
            "dashboard.loadingDashboardSettingsError",
            NotificationType.ERROR
          )
      );
  }

  saveDashboardSettings(): void {
    this.dashboardService
      .updateDashboardSettings(this.widgets)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(undefined, () =>
        this.notificationService.notify(
          "dashboard.savingDashboardSettingsError",
          NotificationType.ERROR
        )
      );
  }

  openWidgetConfigurator(): void {
    this.widgetConfiguratorSlider.open();
  }

  tryRemoveWidget(widget: DashboardWidget): void {
    this.deleteDialogData.onConfirmClick = () => this.removeWidget(widget);
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private changedOptions(): void {
    if (this.options.api && this.options.api.optionsChanged) {
      this.options.api.optionsChanged();
    }
  }

  editDashbords() {
    this.edited = !this.edited;
    this.options.draggable.enabled = this.edited;
    this.options.resizable.enabled = this.edited;
    this.changedOptions();
  }

  removeWidget(widget: DashboardWidget): void {
    if (widget) {
      this.dashboardService
        .removeDashboardById(widget.id)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => {
            this.widgets = this.widgets.filter((w) => w.id !== widget.id);
          },
          () =>
            this.notificationService.notify(
              "dashboard.removeWidgetError",
              NotificationType.ERROR
            )
        );
    }
  }

  newWidget(widget: DashboardWidget): void {
    if (widget) {
      this.widgets.push(widget);
    }
  }

  private getAccountEmails(): void {
    this.personService.getAndMapAccountEmails(this.componentDestroyed)
      .subscribe(
        (users) => (this.userMap = users),
        () => this.notificationService.notify('widgetConfigurator.goals.getAccountsError', NotificationType.ERROR)
      );
  }

  @HostListener("document:keydown.escape", ["$event"])
  onEscHandler(event: KeyboardEvent): void {
    this.fullScreenWidget = undefined;
  }
}
