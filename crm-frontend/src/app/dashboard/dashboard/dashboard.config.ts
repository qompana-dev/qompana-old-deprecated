import { WidgetTypeEnum } from "./../dashboard.model";
import { GridType } from "angular-gridster2";

export const OPTIONS = {
  gridType: GridType.Fit,
  mobileBreakpoint: 1000,
  maxItemCols: 24,
  minItemCols: 2,
  maxItemRows: 24,
  minItemRows: 2,
  minCols: 12,
  maxCols: 24,
  minRows: 12,
  maxRows: 24,
  margin: 8,
  outerMargin: false,
  draggable: {
    enabled: false,
  },
  resizable: {
    enabled: false,
  },
};

export const DASHBOARD_SETTING = {
  [WidgetTypeEnum.GOAL]: {
    rows: 4,
    cols: 4,
  },
  [WidgetTypeEnum.CALENDAR]: {
    rows: 10,
    cols: 4,
  },
  [WidgetTypeEnum.NOTIFICATIONS]: {
    rows: 5,
    cols: 4,
  },
  [WidgetTypeEnum.TOTAL_SALES_OPPORTUNITY]: {
    rows: 5,
    cols: 3,
  },
  [WidgetTypeEnum.OPENED_SALES_OPPORTUNITY]: {
    rows: 5,
    cols: 4,
  },
  [WidgetTypeEnum.LEAD_SOURCES]: {
    rows: 4,
    cols: 4,
  },
  [WidgetTypeEnum.REPORT]: {
    rows: 4,
    cols: 4,
  },
  [WidgetTypeEnum.NOTE]: {
    rows: 3,
    cols: 4,
  },
};
