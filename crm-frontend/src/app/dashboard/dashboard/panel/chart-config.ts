import {ChartOptions} from 'chart.js';
import {ChartType} from '../../dashboard.model';

const chartOptions: ChartOptions = {
  responsive: true,
  maintainAspectRatio: false,
  legend: {
    position: 'right'
  }
};

const roundChartOptions: ChartOptions = {
  ...chartOptions,
  tooltips: {
    callbacks: {
      label: function (tooltipItem, data) {
        const dataset = data.datasets[tooltipItem.datasetIndex];
        const index = tooltipItem.index;
        if (!!dataset['label']) {
          return `${dataset['label']}, ${data.labels[index]}:  ${dataset.data[index]}`;
        } else {
          return `${data.labels[index]}:  ${dataset.data[index]}`;
        }
      }
    }
  }
};


export const chartTypeOptions: { [key in keyof typeof ChartType]: ChartOptions } = {
  [ChartType.CHART1]: chartOptions,
  [ChartType.CHART2]: chartOptions,
  [ChartType.CHART3]: roundChartOptions,
  [ChartType.CHART4]: roundChartOptions,
  [ChartType.CHART5]: chartOptions
};
