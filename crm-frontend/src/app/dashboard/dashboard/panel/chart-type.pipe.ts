import {Pipe, PipeTransform} from '@angular/core';
import {ChartType} from '../../dashboard.model';
import * as chartJs from 'chart.js';

@Pipe({
  name: 'chartType'
})
export class ChartTypePipe implements PipeTransform {

  transform(type: ChartType): chartJs.ChartType {
    switch (type) {
      case ChartType.CHART1:
        return 'bar';
      case ChartType.CHART2:
        return 'horizontalBar';
      case ChartType.CHART3:
        return 'doughnut';
      case ChartType.CHART4:
        return 'pie';
      case ChartType.CHART5:
        return 'line';
      default:
        return 'bar';
    }
  }

}
