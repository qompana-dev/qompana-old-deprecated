import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from "@angular/core";
import {
  ChartType,
  DashboardWidget,
  GoalWidgetData,
} from "../../../dashboard.model";
import { DashboardService } from "../../../dashboard.service";
import { finalize, takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { TranslateService } from "@ngx-translate/core";
import { ChartOptions } from "chart.js";
import { chartTypeOptions } from "../chart-config";
import { GoalAddService } from "../../../../goal/goal-add.service";
import { GoalType } from "src/app/goal/goal.model";

@Component({
  selector: "app-goal-panel",
  templateUrl: "./goal-panel.component.html",
  styleUrls: ["./goal-panel.component.scss"],
})
export class GoalPanelComponent implements OnInit, OnDestroy {
  chartTypes = ChartType;
  goalTypes = GoalType;

  isLoading = false;
  data: GoalWidgetData;
  // tslint:disable-next-line:variable-name
  _widget: DashboardWidget;
  private componentDestroyed: Subject<void> = new Subject();
  options: { [key in keyof typeof ChartType]: ChartOptions } = chartTypeOptions;

  @Input() set widget(widget: DashboardWidget) {
    if (widget) {
      this._widget = widget;
      this.isLoading = true;
      this.getGoalWidgetData(widget);
    }
  }

  @Input() edited: boolean;

  // tslint:disable-next-line:variable-name
  _fullscreenMode = false;
  @Input()
  set fullscreenMode(mode: boolean) {
    this._fullscreenMode = mode;
    this.showCanvas = false;
    setTimeout(() => (this.showCanvas = true), 50);
  }
  get fullscreenMode(): boolean {
    return this._fullscreenMode;
  }
  @Output()
  onRemoveWidget: EventEmitter<DashboardWidget> = new EventEmitter<DashboardWidget>();
  @Output()
  openFullscreen: EventEmitter<DashboardWidget> = new EventEmitter<DashboardWidget>();

  showCanvas = true;

  constructor(
    private dashboardService: DashboardService,
    private translateService: TranslateService,
    private goalAddService: GoalAddService
  ) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  removeWidget(): void {
    this.openFullscreen.emit(undefined);
    this.onRemoveWidget.emit(this._widget);
  }

  tryRemoveWidget(widget) {
    this.onRemoveWidget.emit(widget);
  }

  private getGoalWidgetData(widget: DashboardWidget): void {
    this.dashboardService
      .getGoalWidgetData(widget.widgetId)
      .pipe(
        takeUntil(this.componentDestroyed),
        finalize(() => (this.isLoading = false))
      )
      .subscribe((data) => {
        this.data = data;
        this.setDataSetsNames();
        this.setLabels();
      });
  }

  private setDataSetsNames(): void {
    this.data.userWidgetDataDtoList.forEach((e) => {
      // @ts-ignore
      if (e.goal) {
        e.label =
          this.translateService.instant("widgetConfigurator.goal") +
          ": " +
          e.label;
      }
    });
  }

  private setLabels(): void {
    this.data.labels = [];
    for (let i = 0; i < this.data.intervalNumber; i++) {
      this.data.labels.push(
        this.goalAddService.getIntervalNameByIndexAndInterval(i, this.data)
          .title
      );
    }
  }

  openInFullscreen(): void {
    this.openFullscreen.emit(this.fullscreenMode ? undefined : this._widget);
  }

  @HostListener("window:resize", ["$event"])
  onResize() {
    this.showCanvas = false;
    setTimeout(() => (this.showCanvas = true), 25);
  }
}
