import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {CalendarService} from '../../../../calendar/calendar.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {EventInfo, Task} from '../../../../calendar/task.model';
import {FormatDatePipe} from '../../../../shared/pipe/formatDate.pipe';
import {CalendarEvent} from 'angular-calendar';
import {DateUtil} from '../../../../shared/util/date.util';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {DashboardWidget} from '../../../dashboard.model';

@Component({
  selector: 'app-calendar-panel',
  templateUrl: './calendar-panel.component.html',
  styleUrls: ['./calendar-panel.component.scss']
})
export class CalendarPanelComponent implements OnInit, OnDestroy {


  @Input() widget: DashboardWidget;
  viewDate: Date = new Date();
  locale = localStorage.getItem('locale');
  private componentDestroyed: Subject<void> = new Subject();

  tasks: Task[] = [];

  availableColors: string[] = ['#ff7777', '#3ACB9C', '#EAECEF', '#319DF8', '#AF7EFF', '#FF718C', '#1DC9D7', '#837575'];
  events: CalendarEvent[] = [];

  moreEventsForDay: string;

  weekCount = 5;

  newEvent: EventInfo = {startDate: new Date(), allDayEvent: false};

  @Output() onRemoveWidget: EventEmitter<DashboardWidget> = new EventEmitter<DashboardWidget>();

  @ViewChild('addTask') addTaskSlider: SliderComponent;

  get isSliderOpened(): boolean {
    return this.addTaskSlider.opened;
  }

  constructor(private calendarService: CalendarService,
              private notificationService: NotificationService,
              private formatDatePipe: FormatDatePipe) {
  }

  ngOnInit(): void {
    this.loadEvents();
    this.setWeekCount();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  loadEvents(): void {
    this.getEventsByOwnerId(+localStorage.getItem('loggedAccountId'));
  }

  private getEventsByOwnerId(ownerId: number): void {
    this.calendarService.getUpcomingEventsByOwnerId(ownerId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((tasks) => {
        this.tasks = this.setColors(tasks);
        this.events = this.getCalendarEvent(this.tasks);
      }, () => this.notificationService.notify('dashboard.panels.calendar.loadDataError', NotificationType.ERROR));
  }

  private setColors(tasks: Task[]): Task[] {
    let colorIndex = 0;
    return tasks.map(task => {
      task.color = this.availableColors[colorIndex];
      colorIndex++;
      if (colorIndex >= this.availableColors.length) {
        colorIndex = 0;
      }
      return task;
    });
  }

  private getCalendarEvent(tasks: Task[]): CalendarEvent[] {
    return tasks.map((task: Task): CalendarEvent => {
      return {
        start: DateUtil.toDate(task.dateTimeFrom),
        end: DateUtil.toDate(task.dateTimeTo),
        title: task.subject,
        color: {
          primary: task.color,
          secondary: task.color
        },
        allDay: false,
        resizable: {
          beforeStart: false,
          afterEnd: false
        },
        draggable: false
      };
    });
  }

  datesEqual(date1: string, date2: string): boolean {
    return this.formatDatePipe.transform(date1, false) === this.formatDatePipe.transform(date2, false);
  }

  taskClicked(task: Task): void {
    this.viewDate = DateUtil.toDate(task.dateTimeFrom);
  }

  isDaySelected(date: Date): boolean {
    return date.getFullYear() === this.viewDate.getFullYear() &&
      date.getMonth() === this.viewDate.getMonth() &&
      date.getDate() === this.viewDate.getDate();
  }

  removeWidget(): void {
    if (this.widget) {
      this.onRemoveWidget.emit(this.widget);
    }
  }

  setWeekCount(): void {
    const year = this.viewDate.getFullYear();
    const monthNumber = this.viewDate.getMonth() + 1;

    const startDayOfWeek: number = +localStorage.getItem('firstDayOfWeek');
    const firstDayOfWeek = startDayOfWeek || 0;

    const firstOfMonth = new Date(year, monthNumber - 1, 1);
    const lastOfMonth = new Date(year, monthNumber, 0);
    const numberOfDaysInMonth = lastOfMonth.getDate();
    const firstWeekDay = (firstOfMonth.getDay() - firstDayOfWeek + 7) % 7;
    const used = firstWeekDay + numberOfDaysInMonth;

    this.weekCount = Math.ceil(used / 7);
  }
}
