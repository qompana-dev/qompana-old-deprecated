import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {DashboardWidget} from "../../../dashboard.model";

@Component({
  selector: 'app-notifications-panel',
  templateUrl: './notifications-panel.component.html',
  styleUrls: ['./notifications-panel.component.scss'],
  host: {'class': 'app-notifications-panel'}
})
export class NotificationsPanelComponent implements OnInit, OnDestroy {

  @Input() widget: DashboardWidget;

  private componentDestroyed: Subject<void> = new Subject();

  constructor() {
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }
}
