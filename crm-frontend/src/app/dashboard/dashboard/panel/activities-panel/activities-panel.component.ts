import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {EventInfo, Task, TaskActivityType, TaskState} from '../../../../calendar/task.model';
import {AuthService} from '../../../../login/auth/auth.service';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {CalendarService} from '../../../../calendar/calendar.service';
import {LoginService} from '../../../../login/login.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
// @ts-ignore
import moment from 'moment';
import {TaskUtils} from '../../../../calendar/utils/task-utils';
import {TimesUtil} from '../../../../shared/times.util';
import {crmDateFormat, crmDateTimeFormat, crmTimeFormat} from '../../../../shared/config/times.config';

moment.locale(localStorage.getItem('locale'));

@Component({
  selector: 'app-activities-panel',
  templateUrl: './activities-panel.component.html',
  styleUrls: ['./activities-panel.component.scss']
})
export class ActivitiesPanelComponent implements OnInit, OnDestroy {

  @ViewChild('addActivitySlider') addSlider: SliderComponent;
  activityTypeEnum = TaskActivityType;
  selectedActivityType: TaskActivityType;
  updatedEventInfo: EventInfo;
  allActivities: Task[];
  createdTasks: Task[];
  inProgressTasks: Task[];
  suspendedTasks: Task[];
  finishedTasks: Task[];
  currentTimezone = moment.tz.guess();

  private momentCalendar = {
    lastWeek: crmDateTimeFormat,
    nextWeek: crmDateTimeFormat,
    sameElse: crmDateTimeFormat
  };

  private componentDestroyed: Subject<void> = new Subject();

  constructor(private authService: AuthService,
              private calendarService: CalendarService,
              private loginService: LoginService) {
  }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  openAddSlider($event: any, taskActivityType?: TaskActivityType): void {
    if (!this.authService.isPermissionPresent('CalendarComponent.addTask', 'w')) {
      return;
    }
    if ($event.date) {
      this.updatedEventInfo = new EventInfo($event.date, false);
    } else if ($event.day) {
      this.updatedEventInfo = new EventInfo($event.day.date, true);
    }
    this.selectedActivityType = taskActivityType;
    this.addSlider.open();
  }

  getTimezoneTooltip(task: Task, createdInTimezoneText: string): string {
    return TaskUtils.getTimezoneInfoForActivities(task.timeZone, task.dateTimeFrom, createdInTimezoneText);
  }

  getCalendarTime(time: string): string {
    return TimesUtil.fromUTCToCurrentTimeZone(time).calendar(null, this.momentCalendar);
  }

  getTime(dateTime: string): string {
    return TimesUtil.fromUTCToCurrentTimeZone(dateTime).format(crmTimeFormat);
  }

  getEndTime(startTime: string, time: string): string {
    if (this.getDate(startTime) === this.getDate(time)) {
      return this.getTime(time);
    }
    return TimesUtil.fromUTCToCurrentTimeZone(time).format(crmDateTimeFormat);
  }

  init(): void {
    this.calendarService.getEventsByOwnerId(this.loginService.getLoggedAccountId())
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(activities => this.initActivities(activities));
  }

  private getDate(dateTime: string): string {
    return TimesUtil.fromUTCToCurrentTimeZone(dateTime).format(crmDateFormat);
  }

  private initActivities(activities: Task[]): void {
    this.allActivities = activities.sort(
      (a, b) => b.dateTimeFrom < a.dateTimeFrom ? -1 : b.dateTimeFrom === a.dateTimeFrom ? 0 : 1);
    this.createdTasks = this.allActivities.filter(e => e.state === TaskState.CREATED);
    this.inProgressTasks = this.allActivities.filter(e => e.state === TaskState.IN_PROGRESS);
    this.suspendedTasks = this.allActivities.filter(e => e.state === TaskState.SUSPENDED);
    this.finishedTasks = this.allActivities.filter(e => e.state === TaskState.FINISHED);
  }
}
