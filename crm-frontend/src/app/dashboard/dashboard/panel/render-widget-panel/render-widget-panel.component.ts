import { WIDGETS } from "./widgets/render-widgets";
import {
  Component,
  ComponentFactoryResolver,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef,
} from "@angular/core";
import { DashboardWidget } from "../../../dashboard.model";

@Component({
  selector: "app-render-widget-panel",
  templateUrl: "./render-widget-panel.component.html",
  styleUrls: ["./render-widget-panel.component.scss"],
})
export class RenderWidgetPanelComponent implements OnInit, OnDestroy {
  _widget: DashboardWidget;
  _payload: any;

  @Input() set widget(widget: DashboardWidget) {
    this._widget = widget;
    this.renderWidget(this._widget);
  }

  @Input() set payload(payload: any) {
    this._payload = payload;
    this.renderWidget(this._widget);
  }

  @Input() edited: boolean = false;

  isLoading = false;

  @Output()
  onRemoveWidget: EventEmitter<DashboardWidget> = new EventEmitter<DashboardWidget>();

  @ViewChild("informationWidget", {
    read: ViewContainerRef,
  })
  viewContainerRef: ViewContainerRef;

  widgetRef = null;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.destroyWidget();
  }

  removeWidget(event: Event): void {
    event.preventDefault();
    event.stopPropagation();
    this.onRemoveWidget.emit(this._widget);
  }

  private renderWidget(widget: DashboardWidget) {
    this.destroyWidget();
    const widgetRender = WIDGETS[widget && widget.widgetTypeEnum];
    if (widgetRender) {
      const factory = this.componentFactoryResolver.resolveComponentFactory(
        widgetRender
      );
      this.widgetRef = this.viewContainerRef.createComponent(factory);
      this.widgetRef.instance.widget = widget;
      if (this._payload) {
        this.widgetRef.instance.payload = this._payload;
      }
    }
  }

  private destroyWidget() {
    if (this.widgetRef) {
      this.widgetRef.destroy();
    }
  }
}
