import { GoalWidgetComponent } from "./widgets/goal-widget/goal-widget.component";
import { TaskSliderModule } from "./../../../../calendar/task-slider/task-slider.module";
import { CalendarComponent } from "./widgets/calendar-activities/calendar/calendar.component";
import { CalendarActivitiesComponent } from "./widgets/calendar-activities/calendar-activities.component";
import { MatRadioModule } from "@angular/material/radio";
import { OpenedSalesOpportuniryComponent } from "./widgets/opened-sales-opportunity/opened-sales-opportunity.component";
import { SourceOfLeads } from "./widgets/source-of-leads/source-of-leads.component";
import { QuillModule } from "ngx-quill";
import { RenderWidgetPanelComponent } from "./render-widget-panel.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "src/app/shared/shared.module";
import { WidgetModule } from "src/app/widgets/widget.module";
import { SalesOpportunityPanelComponent } from "./widgets/sales-opportunity-panel/sales-opportunity-panel.component";
import { NotesPanelComponent } from "./widgets/notes-panel/notes-panel.component";
import { ActivityFilterComponent } from "./widgets/calendar-activities/activity-filter/activity-filter.component";
import { TitleTodayComponent } from "./widgets/calendar-activities/title-today/title-today.component";
import { ActivitiesListComponent } from "./widgets/calendar-activities/activities-list/activities-list.component";
import { ActivityDescriptionComponent } from "./widgets/calendar-activities/activities-list/activity-description/activity-description.component";
import { AddActivityComponent } from "./widgets/calendar-activities/add-activity/add-activity.component";
import { NotificationsPanelComponent } from "../notifications-panel/notifications-panel.component";
import { NotificationModule } from "../../../../notification/notification.module";

const widgetComponents = [
  SalesOpportunityPanelComponent,
  NotesPanelComponent,
  SourceOfLeads,
  OpenedSalesOpportuniryComponent,
  NotificationsPanelComponent,
  CalendarActivitiesComponent,
  GoalWidgetComponent,
];

@NgModule({
  declarations: [
    RenderWidgetPanelComponent,
    CalendarComponent,
    ...widgetComponents,
    ActivityFilterComponent,
    TitleTodayComponent,
    ActivitiesListComponent,
    ActivityDescriptionComponent,
    AddActivityComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    WidgetModule,
    MatRadioModule,
    QuillModule.forRoot(),
    NotificationModule,
    TaskSliderModule,
  ],
  exports: [RenderWidgetPanelComponent],
  entryComponents: [RenderWidgetPanelComponent, ...widgetComponents],
})
export class RenderWidgetPanelModule {}
