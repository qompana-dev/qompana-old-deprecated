export enum PERIODS {
  DAY = "DAY",
  WEEK = "WEEK",
  MONTH = "MONTH",
}

export interface SourceOfLeadsData {
  sources: SourcesData[];
  total: number;
}

export interface SourcesData {
  count: number;
  name: string;
  percentageChange: number;
}
