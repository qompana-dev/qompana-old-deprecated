import { Component, Input, OnDestroy, OnInit, Renderer2 } from "@angular/core";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { DashboardService } from "src/app/dashboard/dashboard.service";
import * as moment from "moment";
import { ACTIVITIES } from "./calendar-activities.config";

@Component({
  selector: "app-calendar-activities",
  templateUrl: "./calendar-activities.component.html",
  styleUrls: ["./calendar-activities.component.scss"],
})
export class CalendarActivitiesComponent implements OnInit, OnDestroy {
  @Input() widget;
  private componentDestroyed: Subject<void> = new Subject();

  private filters;
  public typesCount = {};
  private date;
  private timeCategory;

  public data = [];
  public filteredData = [];
  public loading = true;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  changeFliter(filters) {
    this.filters = filters;
    this.filteredData = this.getFilteredData();
  }

  changeDate(date) {
    this.date = date;
    this.getData();
  }

  changeTimeCategory(timeCategory) {
    this.timeCategory = timeCategory;
    this.getData();
  }

  getData() {
    const ownerId = +localStorage.getItem("loggedAccountId");
    const date = moment(this.date).format("DD.MM.YYYY");
    if (this.date && this.timeCategory) {
      this.componentDestroyed.next();
      this.loading = true;
      this.dashboardService
        .getOwnerTasksByDate(ownerId, date, this.timeCategory)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((data) => {
          this.parseData(data);
          this.loading = false;
        });
    }
  }

  parseData(data) {
    this.data = data;
    this.typesCount = this.getTypesCount(data);
    this.filteredData = this.getFilteredData();
  }

  getTypesCount(date: any[]) {
    const typesCount = date.reduce((typesCount, task) => {
      const { activityType } = task;
      const type = ACTIVITIES[activityType] || ACTIVITIES.EVENT;
      typesCount[type] = typesCount[type] ? typesCount[type] + 1 : 1;
      return typesCount;
    }, {});

    return typesCount;
  }

  getFilteredData() {
    return this.data.filter((task) => {
      const { activityType } = task;
      const type = ACTIVITIES[activityType] || ACTIVITIES.EVENT;
      return this.filters[type];
    });
  }
}
