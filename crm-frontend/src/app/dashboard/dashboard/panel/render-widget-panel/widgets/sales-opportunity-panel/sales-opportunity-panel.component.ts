import {
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { Subject } from "rxjs";
import { finalize, takeUntil } from "rxjs/operators";
import { DashboardService } from "src/app/dashboard/dashboard.service";
import confetti from "canvas-confetti";
import {
  CONFETTI_CONFIG,
  QUARTERS,
  SALES_CONFIGS,
} from "./sales-opportunity-panel.config";

import { SalesData, SalesTypes } from "./sales-opportunity-panel.model";

@Component({
  selector: "app-sales-opportunity-panel",
  templateUrl: "./sales-opportunity-panel.component.html",
  styleUrls: ["./sales-opportunity-panel.component.scss"],
})
export class SalesOpportunityPanelComponent implements OnInit, OnDestroy {
  @Input() widget;
  @ViewChild("confetti") confettiCanvas: ElementRef;

  salesTypes = SalesTypes;
  salesType = SalesTypes.CREATED;

  data: SalesData;
  quarterName: string;
  currency: string = "PLN";

  isLoading: boolean = true;
  isError: boolean = false;
  isProgress: boolean = true;

  salesConfig;
  confetti;

  private componentDestroyed: Subject<void> = new Subject();

  constructor(private dashboardService: DashboardService) {}

  ngOnInit(): void {
    this.getData();
    this.initConfetti();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  toggleType(type: SalesTypes) {
    this.salesType = type;
    this.salesConfig = SALES_CONFIGS[type];
    this.isProgress = this.data[this.salesConfig.percent] >= 0;
    if (type === this.salesTypes.SUCCESSFUL) {
      this.startConfetti();
    }
  }

  getData() {
    this.dashboardService
      .getTotalSalesOpportunityData()
      .pipe(
        takeUntil(this.componentDestroyed),
        finalize(() => (this.isLoading = false))
      )
      .subscribe(
        (data: SalesData) => {
          this.parseData(data);
        },
        () => {
          this.isError = true;
        }
      );
  }

  parseData(data: SalesData) {
    if (data) {
      this.data = data;
      this.currency = data.currency || this.currency;
      this.quarterName = this.getQuarterName(data.quarter);
      this.toggleType(this.salesType);
    }
  }

  getValidPercent(percent) {
    return Math.abs(percent);
  }

  getQuarterName(quarter) {
    return QUARTERS[quarter] || QUARTERS[1];
  }

  startConfetti() {
    this.confetti(CONFETTI_CONFIG);
  }

  initConfetti() {
    this.confetti =
      this.confetti ||
      confetti.create(this.confettiCanvas.nativeElement, { resize: true });
  }
}
