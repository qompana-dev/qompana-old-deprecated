import { ACTIVITIES, ACTIVITIES_LIST } from "./../calendar-activities.config";
import { Component, EventEmitter, Output, ViewChild } from "@angular/core";
import { SliderComponent } from "src/app/shared/slider/slider.component";
import { TaskActivityType } from "src/app/calendar/task.model";

@Component({
  selector: "app-add-activity",
  templateUrl: "./add-activity.component.html",
  styleUrls: ["./add-activity.component.scss"],
})
export class AddActivityComponent {
  @ViewChild("addTask") addTaskSlider: SliderComponent;

  @Output() public addActivityEvent: EventEmitter<void> = new EventEmitter();

  public newEvent = { startDate: new Date(), allDayEvent: false };
  public selectedActivityType: TaskActivityType;
  public activities = ACTIVITIES_LIST;

  constructor() {}

  openAddSlider(type) {
    const activityTypeArr = Object.entries(ACTIVITIES).find(([key, value]) => {
      return value === type;
    });
    const activityType = activityTypeArr && activityTypeArr[0];
    this.selectedActivityType = TaskActivityType[activityType];
    this.addTaskSlider.open();
  }

  addActivity(): void {
    this.addActivityEvent.emit();
  }
}
