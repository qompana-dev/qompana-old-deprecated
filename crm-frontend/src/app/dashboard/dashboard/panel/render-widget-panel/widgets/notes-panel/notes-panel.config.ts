export const ADD_TITLE = "widgets.notes.dialog.add";
export const EDIT_TITLE = "widgets.notes.dialog.edit";

export const persistNoteDialogData = {
  confirmButton: "widgets.notes.save",
  cancelButton: "widgets.notes.cancel",
};

export const deleteDialogData = {
  title: "widgets.notes.remove.title",
  description: "widgets.notes.remove.description",
  confirmButton: "widgets.notes.remove.yes",
  cancelButton: "widgets.notes.remove.no",
};

export const saveNoteErrors = {
  base: "widgets.notes.notification.",
  defaultText: "unknownError",
  errors: [
    {
      code: 404,
      text: "saveCustomerNotFound",
    },
    {
      code: 403,
      text: "noPermission",
    },
  ],
};

export const updateNoteErrors = {
  base: "widgets.notes.notification.",
  defaultText: "unknownError",
  errors: [
    {
      code: 404,
      text: "updateNoteNotFound",
    },
    {
      code: 403,
      text: "noPermission",
    },
  ],
};

export const deleteNoteErrors = {
  base: "widgets.notes.remove.",
  defaultText: "undefinedError",
  errors: [
    {
      code: 404,
      text: "notFound",
    },
    {
      code: 403,
      text: "noPermission",
    },
  ],
};
