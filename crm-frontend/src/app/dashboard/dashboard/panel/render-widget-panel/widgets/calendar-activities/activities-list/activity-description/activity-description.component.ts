import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { Subject } from "rxjs";
import { finalize, takeUntil } from "rxjs/operators";
import * as moment from "moment";

import { AssignToGroupService } from "src/app/shared/assign-to-group/assign-to-group.service";
import { CalendarService } from "src/app/calendar/calendar.service";
import { Task, TaskAssociation } from "src/app/calendar/task.model";
import {
  DESCRIPTION_FIELDS,
  ACTIVITY_FIELDS,
} from "./../activities-list.config";
import { ACTIVITIES } from "../../calendar-activities.config";

@Component({
  selector: "app-activity-description",
  templateUrl: "./activity-description.component.html",
  styleUrls: ["./activity-description.component.scss"],
})
export class ActivityDescriptionComponent implements OnInit, OnDestroy {
  @Input() activity;

  private componentDestroyed: Subject<void> = new Subject();

  public loading = true;
  public data = [];
  public associations = [];

  public isMarginTop = false;

  constructor(
    private calendarService: CalendarService,
    private assignToGroupService: AssignToGroupService
  ) {}

  ngOnInit() {
    this.parseActivityData(this.activity);
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  parseActivityData(task: Task) {
    this.isMarginTop = !!task.description;
    this.data = this.parseActivity(task);
    this.getTaskAssociation(task.id);
  }

  private getTaskAssociation(activityId: number): void {
    this.assignToGroupService
      .getAssociationsDetails(activityId)
      .pipe(
        takeUntil(this.componentDestroyed),
        finalize(() => (this.loading = false))
      )
      .subscribe((associations: TaskAssociation[]) => {
        this.associations = this.parseTaskAssociations(associations);
        this.setCompanies(associations);
      });
  }

  setCompanies(associations) {
    const companies = associations
      .map(({ firm }) => firm)
      .filter((item) => item)
      .join(", ");

    if (companies) {
      this.data.push({ field: "companies", value: companies });
    }
  }

  parseActivity(task) {
    const { activityType } = task;
    const type = ACTIVITIES[activityType] || ACTIVITIES.EVENT;
    const fields: string[] = DESCRIPTION_FIELDS[type];

    const data = fields
      .map((field) => {
        if (
          task[field] &&
          (!Array.isArray(task[field]) || task[field].length)
        ) {
          const value = this.parseViewData(field, task[field]);
          return { field, value };
        }
        return null;
      })
      .filter((item) => item);

    return data;
  }

  parseTaskAssociations(associations) {
    return associations.map((association) => ({
      field: association.type,
      value: association.label,
    }));
  }

  parseViewData(field, value) {
    switch (field) {
      case ACTIVITY_FIELDS.PARTICIPANTS:
        return this.parseToViewParicipants(value);
      case ACTIVITY_FIELDS.DATE_TIME_FROM:
      case ACTIVITY_FIELDS.DATE_TIME_TO:
        return this.parseToViewDate(value);
      default:
        return value;
    }
  }

  parseToViewDate(value) {
    const locale = localStorage.getItem("locale");
    const date = moment(value).locale(locale);
    return date.format("dddd, D MMMM, YYYY, hh:mm");
  }

  parseToViewParicipants(value) {
    const paricipantNames = value.map((participant) => participant.name);
    return paricipantNames.join(", ");
  }
}
