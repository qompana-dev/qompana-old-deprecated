export const SALES_CONFIGS = {
  CREATED: {
    count: "created",
    percent: "createdPercentOfPast",
    title: "createdTitle",
    totalAmount: "createdTotalAmount",
  },
  SUCCESSFUL: {
    count: "successful",
    percent: "successfulPercentOfPast",
    title: "succesTitle",
    totalAmount: "successfulTotalAmount",
  },
  LOST: {
    count: "lost",
    percent: "lostPercentOfPast",
    title: "lostTitle",
    totalAmount: "lostTotalAmount",
  },
};

export const QUARTERS = {
  1: "firstQuarter",
  2: "secondQuarter",
  3: "thirdQuarter",
  4: "fourthQuarter",
};

export const CONFETTI_CONFIG = {
  spread: 70,
  origin: { y: 1.2 },
};
