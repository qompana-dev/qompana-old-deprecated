import { DeleteDialogData } from "src/app/shared/dialog/delete/delete-dialog.component";
import { DeleteDialogService } from "src/app/shared/dialog/delete/delete-dialog.service";
import {
  NotificationService,
  NotificationType,
} from "src/app/shared/notification.service";
import { NotesService } from "src/app/widgets/notes/notes.service";
import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { CrmObjectType } from "src/app/shared/model/search.model";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { PersistNoteDialogService } from "src/app/widgets/notes/persist-note-dialog/persist-note-dialog.service";
import { PersistNoteDialogData } from "src/app/widgets/notes/persist-note-dialog/persist-note-dialog.component";
import { Note, ObjectTypeAndId } from "src/app/widgets/notes/notes.model";
import {
  saveNoteErrors,
  updateNoteErrors,
  deleteNoteErrors,
  deleteDialogData,
  persistNoteDialogData,
  ADD_TITLE,
  EDIT_TITLE,
} from "./notes-panel.config";

@Component({
  selector: "app-notes-panel",
  templateUrl: "./notes-panel.component.html",
  styleUrls: ["./notes-panel.component.scss"],
})
export class NotesPanelComponent implements OnInit, OnDestroy {
  @Input() widget;

  notes: Note[] = [];
  searchView: boolean = false;
  searchInput: string = "";

  panelOpenId = null;

  objectTypeAndId: ObjectTypeAndId;

  private componentDestroyed: Subject<void> = new Subject();

  constructor(
    public notesService: NotesService,
    private notificationService: NotificationService,
    private deleteDialogService: DeleteDialogService,
    private persistNoteDialogService: PersistNoteDialogService
  ) {}

  ngOnInit(): void {
    const objectId = +localStorage.getItem("loggedAccountId");
    this.objectTypeAndId = {
      objectId,
      objectType: CrmObjectType.user,
    };

    this.getNotes(this.objectTypeAndId);
  }

  private getNotes(objectTypeAndId: ObjectTypeAndId): void {
    this.notesService
      .getNotes(objectTypeAndId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((notes) => (this.notes = notes));
  }

  toggleSearchView() {
    if (this.searchView) {
      this.searchInput = "";
    }
    this.searchView = !this.searchView;
  }

  open(id) {
    this.panelOpenId = id;
  }

  close(id) {
    if (this.panelOpenId === id) {
      this.panelOpenId = null;
    }
  }

  openAddDialog($event): void {
    $event.stopPropagation();

    const dialogData = {
      ...persistNoteDialogData,
      title: ADD_TITLE,
      formattedContent: null,
      onConfirmClick: (plainText, htmlText) => {
        this.save(plainText, htmlText);
      },
    };

    this.persistNoteDialogService.open(dialogData);
  }

  openEditDialog($event, note: Note): void {
    $event.stopPropagation();
    const dialogData = {
      ...persistNoteDialogData,
      title: EDIT_TITLE,
      formattedContent: note.formattedContent,
      onConfirmClick: (plainText, htmlText) => {
        this.update(note, plainText, htmlText);
      },
    };

    this.persistNoteDialogService.open(dialogData);
  }

  tryDeleteNote($event, note: Note): void {
    $event.stopPropagation();
    this.openDeleteDialog(() => this.delete(note));
  }

  private openDeleteDialog(onConfirmClick): void {
    this.deleteDialogService.open({ ...deleteDialogData, onConfirmClick });
  }

  private save(plainText: string, htmlText: string): void {
    if (!this.trim(plainText)) {
      return;
    }
    if (plainText.length > 3000) {
      this.notificationService.notify(
        "widgets.notes.notification.maxLengthExceeded",
        NotificationType.ERROR
      );
      return;
    }
    this.notesService
      .saveNote(this.objectTypeAndId, plainText, htmlText)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (note) => {
          this.getNotes(this.objectTypeAndId);
          this.notificationService.notify(
            "widgets.notes.notification.saveSuccess",
            NotificationType.SUCCESS
          );
        },
        (err) =>
          this.notificationService.notifyError(
            saveNoteErrors,
            NotificationType.ERROR
          )
      );
  }

  private update(note: Note, plainText: string, htmlText: string): void {
    if (!this.trim(plainText)) {
      return;
    }
    if (plainText.length > 3000) {
      this.notificationService.notify(
        "widgets.notes.notification.maxLengthExceeded",
        NotificationType.ERROR
      );
      return;
    }
    this.notesService
      .updateNote(this.objectTypeAndId, note.id, plainText, htmlText)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (updatedNote) => {
          this.updateNoteOnNotesList(note, updatedNote);
          this.notificationService.notify(
            "widgets.notes.notification.updateSuccess",
            NotificationType.SUCCESS
          );
        },
        (err) =>
          this.notificationService.notifyError(updateNoteErrors, err.status)
      );
  }

  private delete(note: Note): void {
    this.notesService
      .deleteNote(note.id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.removeFromNotesList(note);
          this.notificationService.notify(
            "widgets.notes.remove.successfullyRemoved",
            NotificationType.SUCCESS
          );
        },
        (err) =>
          this.notificationService.notifyError(deleteNoteErrors, err.status)
      );
  }

  private updateNoteOnNotesList(oldNote: Note, newNote: Note): void {
    const index = this.notes.indexOf(oldNote);
    if (index > -1) {
      Object.assign(this.notes[index], newNote);
    }
  }

  private removeFromNotesList(note: Note): void {
    const index = this.notes.indexOf(note);
    if (index > -1) {
      this.notes.splice(index, 1);
    }
  }

  private trim(text: string): string {
    return text && text.trim();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }
}
