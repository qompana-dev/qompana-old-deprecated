import { GoalWidgetComponent } from "./goal-widget/goal-widget.component";
import { OpenedSalesOpportuniryComponent } from "./opened-sales-opportunity/opened-sales-opportunity.component";
import { WidgetTypeEnum } from "../../../../dashboard.model";
import { NotesPanelComponent } from "./notes-panel/notes-panel.component";
import { SalesOpportunityPanelComponent } from "./sales-opportunity-panel/sales-opportunity-panel.component";
import { SourceOfLeads } from "./source-of-leads/source-of-leads.component";
import { CalendarActivitiesComponent } from "./calendar-activities/calendar-activities.component";
import { NotificationsPanelComponent } from "../../notifications-panel/notifications-panel.component";

export const WIDGETS = {
  [WidgetTypeEnum.TOTAL_SALES_OPPORTUNITY]: SalesOpportunityPanelComponent,
  [WidgetTypeEnum.NOTE]: NotesPanelComponent,
  [WidgetTypeEnum.NOTIFICATIONS]: NotificationsPanelComponent,
  [WidgetTypeEnum.LEAD_SOURCES]: SourceOfLeads,
  [WidgetTypeEnum.OPENED_SALES_OPPORTUNITY]: OpenedSalesOpportuniryComponent,
  [WidgetTypeEnum.CALENDAR]: CalendarActivitiesComponent,
  [WidgetTypeEnum.GOAL]: GoalWidgetComponent,
};
