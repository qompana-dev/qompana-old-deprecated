import { TranslateService } from "@ngx-translate/core";
import { GoalWidgetData } from "./../../../../../dashboard.model";
import { Component, Input, OnDestroy, OnInit } from "@angular/core";

@Component({
  selector: "app-goal-widget",
  templateUrl: "./goal-widget.component.html",
  styleUrls: ["./goal-widget.component.scss"],
})
export class GoalWidgetComponent implements OnInit, OnDestroy {
  @Input() widget;

  @Input()
  set payload(payload: GoalWidgetData) {
    const data = this.filterPayloadToData(payload); //+
    this.legends = this.getLegendsByData(data); //+
    this.datasets = this.getDatasetsByData(data, this.legends); //+
    this.labels = this.getLabelsByData(data); //+
    this.title = this.getTitleByData(data); //+
    this.subTitle = this.getSubTitleByData(this.labels, payload.startYear); //+
    this.secondLabels = this.getSecondByData(data);
  }

  public title = "";
  public subTitle = "";

  public datasets = [];
  public labels = [];
  public secondLabels = [];
  public legends = [];

  constructor(private translateService: TranslateService) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {}

  filterPayloadToData(payload): GoalWidgetData {
    const { userWidgetDataDtoList } = payload;

    userWidgetDataDtoList.sort((a, b) => {
      const { goal: goalA } = a;
      const { goal: goalB } = b;
      return goalB - goalA;
    });

    return payload;
  }

  getLegendsByData(data) {
    const { type, userWidgetDataDtoList } = data;
    const legends = userWidgetDataDtoList
      .map(({ goal }) => {
        return `dashboard.widgets.goal.${type}.${goal ? "goal" : "created"}`;
      })
      .map((legend) => {
        return legend && this.translateService.instant(legend);
      });

    return legends;
  }

  getDatasetsByData(data, legends) {
    const { userWidgetDataDtoList } = data;
    return userWidgetDataDtoList.map(({ data }, i) => ({
      data,
      label: legends[i],
    }));
  }

  getSecondByData(data) {
    const labels = [];
    const datasets = data.userWidgetDataDtoList.map(({ data }) => data);
    const length = datasets[0] ? datasets[0].length : 0;
    for (let i = 0; i < length; i++) {
      const count =
        (datasets[1] ? datasets[1][i] : 0) - (datasets[0] ? datasets[0][i] : 0);
      labels.push(count);
    }
    return labels;
  }

  getLabelsByData({ labels }): string[] {
    if (labels.length) {
      return [...labels];
    } else {
      return [];
    }
  }

  getTitleByData({ type }) {
    const translateTitle = `dashboard.widgets.goal.${type}.title`;
    return translateTitle && this.translateService.instant(translateTitle);
  }

  getSubTitleByData(labels, year) {
    const labelsLength = labels.length;
    if (labelsLength <= 1) {
      return year + "";
    }
    return `${labels[0]} - ${labels[labelsLength - 1]} ${year}`;
  }
}
