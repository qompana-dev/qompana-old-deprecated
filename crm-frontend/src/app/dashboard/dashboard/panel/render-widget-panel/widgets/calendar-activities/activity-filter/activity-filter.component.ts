import { Component, OnInit, Output, EventEmitter, Inject, Input } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ACTIVITIES, ACTIVITIES_LIST } from "../calendar-activities.config";

@Component({
  selector: "app-activity-filter",
  templateUrl: "./activity-filter.component.html",
  styleUrls: ["./activity-filter.component.scss"],
})
export class ActivityFilterComponent implements OnInit {
  filterGroup: FormGroup;
  activitesList: ACTIVITIES[] = ACTIVITIES_LIST;

  @Input() numberActivities = {}
  @Output() private changedFilter: EventEmitter<any> = new EventEmitter();


  constructor(formBuilder: FormBuilder) {
    this.filterGroup = formBuilder.group(this.getFilterForm());
  }

  getFilterForm() {
    const filterForm = {};
    
    this.activitesList.forEach((activity) => {
      filterForm[activity] = true;
    });

    return filterForm;
  }

  ngOnInit() {
    this.filterGroup.valueChanges.subscribe((filter) => {
      this.changedFilter.emit(filter)
    });
    this.changedFilter.emit(this.filterGroup.value)
  }
}
