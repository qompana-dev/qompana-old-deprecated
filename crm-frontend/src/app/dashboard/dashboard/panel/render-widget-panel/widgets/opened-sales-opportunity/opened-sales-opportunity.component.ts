import { RoundedBarData } from "./../../../../../../shared/custom-charts/rounded-bar-chart/rounded-bar-chart.model";
import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ProcessDefinition } from "src/app/bpmn/bpmn.model";
import { BpmnService } from "src/app/bpmn/bpmn.service";
import { DashboardService } from "src/app/dashboard/dashboard.service";

@Component({
  selector: "app-opened-sales-opportunity",
  templateUrl: "./opened-sales-opportunity.component.html",
  styleUrls: ["./opened-sales-opportunity.component.scss"],
})
export class OpenedSalesOpportuniryComponent implements OnInit, OnDestroy {
  @Input() widget;

  private componentDestroyed: Subject<void> = new Subject();

  loading = true;
  showCanvas = true;

  processDefinitions: ProcessDefinition[] = [];

  chartData: RoundedBarData[] = [];
  chartLenght: number = 0;

  selectedProcessDefinitionId: string;

  constructor(
    private bpmnService: BpmnService,
    private dashboardService: DashboardService
  ) {}

  ngOnInit(): void {
    this.getProcessDefinitions();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  changeProcess(): void {
    this.getOpportunityProcessData(this.selectedProcessDefinitionId);
  }

  private setProcess(): void {
    if (!this.selectedProcessDefinitionId && this.processDefinitions.length) {
      this.selectedProcessDefinitionId = this.processDefinitions[0].id;
      this.changeProcess();
    } else {
      this.loading = false;
    }
  }

  private getProcessDefinitions(): void {
    this.bpmnService
      .getProcessDefinitions("OPPORTUNITY")
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((definitions) => {
        this.processDefinitions = definitions;
        this.setProcess();
      });
  }

  private getOpportunityProcessData(id): void {
    this.componentDestroyed.next();
    this.loading = true;
    this.showCanvas = false;
    this.dashboardService
      .getOpenedSalesOpportunityData(id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((processOpportunityData) => {
        this.setChartData(processOpportunityData);
        this.setChartLength(processOpportunityData);
        this.setChartData(processOpportunityData);
        this.showCanvas = true;
        this.loading = false;
      });
  }

  setChartData(response) {
    const data = response && response.data;
    if (data) {
      this.chartData = data.map((item) => ({
        yCount: item.totalQuantity,
        xCount: item.stage,
        labelCount: item.totalAmount,
      }));
    } else {
      this.chartData = [];
    }
  }

  setChartLength(response) {
    this.chartLenght = (response && response.totalTasks - 1) || 0;
  }
}
