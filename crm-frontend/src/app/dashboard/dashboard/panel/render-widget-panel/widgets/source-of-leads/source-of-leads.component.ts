import { Subject } from "rxjs";
import {
  PERIODS,
  SourceOfLeadsData,
  SourcesData,
} from "./source-of-leads.model";
import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { takeUntil } from "rxjs/operators";
import { DashboardService } from "src/app/dashboard/dashboard.service";
import { COLORS } from "./source-of-leads.config";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-source-of-leads",
  templateUrl: "./source-of-leads.component.html",
  styleUrls: ["./source-of-leads.component.scss"],
})
export class SourceOfLeads implements OnInit, OnDestroy {
  @Input() widget;

  private componentDestroyed: Subject<void> = new Subject();
  public loading = true;

  public periods = PERIODS;
  public selectedPeriod: PERIODS;

  public sources: SourcesData[] = [];
  public maxCountIndex = 0;
  public chartData: number[] = [];
  public labels: string[] = [];
  public chartColors: string[] = COLORS;
  public totalLeads: number = 0;

  constructor(
    private dashboardService: DashboardService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.changePeriod(this.periods.DAY);
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  changePeriod(period: PERIODS) {
    this.selectedPeriod = period;
    this.getSourceOfLeadsData(PERIODS[period]);
  }

  private getSourceOfLeadsData(period): void {
    this.componentDestroyed.next();
    this.loading = true;
    this.dashboardService
      .getSourceOfLeadsData(period)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((data: SourceOfLeadsData) => {
        this.parseData(data);
        this.loading = false;
      });
  }

  private parseData({ sources, total }: SourceOfLeadsData) {
    this.sources = sources;
    this.maxCountIndex = this.getMaxCountIndex();
    this.chartColors = this.getColors();
    this.chartData = sources.map(({ count }) => count);
    this.labels = sources.map(({ name }) => {
      if (name === "OTHER") {
        return this.translateService.instant(
          "dashboard.widgets.sourceOfLeads.other"
        );
      }
      return name;
    });
    this.totalLeads = total;
  }

  private getMaxCountIndex() {
    let index = 0;
    let maxCount = 0;
    this.sources.forEach((source, i) => {
      if (source.count > maxCount) {
        maxCount = source.count;
        index = i;
      }
    });
    return index;
  }

  private getColors() {
    const colors = [...COLORS];
    for (let i = 0; i < this.maxCountIndex; i++) {
      colors.unshift(colors.pop());
    }
    return colors;
  }
}
