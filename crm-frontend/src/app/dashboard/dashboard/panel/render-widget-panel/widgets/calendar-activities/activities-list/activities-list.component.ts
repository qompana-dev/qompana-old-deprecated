import { TaskPriority, Task } from "./../../../../../../../calendar/task.model";
import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  TIME_CATEGORIES,
  TIME_CATEGORIES_LIST,
} from "../calendar-activities.config";

@Component({
  selector: "app-activities-list",
  templateUrl: "./activities-list.component.html",
  styleUrls: ["./activities-list.component.scss"],
})
export class ActivitiesListComponent implements OnInit {
  @Output()
  private changedTimeCategory: EventEmitter<TIME_CATEGORIES> = new EventEmitter();

  @Input() public data: Task[] = [];
  @Input() public loading = false;

  public activityPriority = TaskPriority;
  public categoriesTabs = TIME_CATEGORIES_LIST;
  public selectedTab = TIME_CATEGORIES_LIST[0];

  public openedActivities = [];

  constructor() {}

  ngOnInit() {
    this.changedTimeCategory.emit(this.selectedTab);
  }

  selectTab(tab) {
    this.selectedTab = tab;
    this.changedTimeCategory.emit(tab);
  }

  openActivity(id) {
    if (!this.openedActivities.includes(id)) {
      this.openedActivities = [...this.openedActivities, id];
    }
  }

  clearOpenedActivities(id) {
    this.openedActivities = this.openedActivities.filter(
      (activityId) => activityId !== id
    );
  }
}
