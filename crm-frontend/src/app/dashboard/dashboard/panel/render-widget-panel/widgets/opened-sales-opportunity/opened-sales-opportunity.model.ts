export interface ProcessDefinition {
  id: string;
  version: number;
  name: string;
  firstTaskName: string;
}

export interface ProcessOpportunityData {
  currency: string;
  totalTasks: number;
  data: TaskDataInfo[];
}

interface TaskDataInfo {
  stage: number;
  totalQuantity: number;
  totalAmount: number;
}
