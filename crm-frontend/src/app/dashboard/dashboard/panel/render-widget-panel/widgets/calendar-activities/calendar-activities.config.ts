export const MAX_YEAR = 2050;
export const MIN_YEAR = 1950;

export const YEARS = (() => {
  const numbers = [];
  for (let i = MIN_YEAR; i <= MAX_YEAR; i++) {
    numbers.push(i);
  }
  return numbers;
})();

export enum ACTIVITIES {
  EVENT = "event",
  PHONE = "phone",
  TASK = "task",
  MEETING = "meeting",
  MAIL = "mail",
}

export const ACTIVITIES_COLORS = {
  [ACTIVITIES.EVENT]: "#21c9d7",
  [ACTIVITIES.PHONE]: "#4e5d77",
  [ACTIVITIES.TASK]: "#319df8",
  [ACTIVITIES.MEETING]: "#fd718c",
  [ACTIVITIES.MAIL]: "#ffad0c",
};

export const ACTIVITIES_LIST = [
  ACTIVITIES.TASK,
  ACTIVITIES.MEETING,
  ACTIVITIES.EVENT,
  ACTIVITIES.PHONE,
  ACTIVITIES.MAIL,
];

export enum TIME_CATEGORIES {
  ALL = "ALL",
  OLD = "OLD",
  PLANNED = "PLANNED",
  FINISHED = "FINISHED",
}

export const TIME_CATEGORIES_LIST = [
  TIME_CATEGORIES.ALL,
  TIME_CATEGORIES.OLD,
  TIME_CATEGORIES.PLANNED,
  TIME_CATEGORIES.FINISHED,
];
