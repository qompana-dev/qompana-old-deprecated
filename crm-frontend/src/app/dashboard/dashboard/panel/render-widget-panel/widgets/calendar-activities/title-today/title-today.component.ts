import { Component, OnInit } from "@angular/core";
import * as moment from "moment";

@Component({
  selector: "app-title-today",
  templateUrl: "./title-today.component.html",
  styleUrls: ["./title-today.component.scss"],
})
export class TitleTodayComponent implements OnInit {
  viewData: string;

  constructor() {}

  ngOnInit() {
    const locale = localStorage.getItem("locale");
    const todayDate = moment().locale(locale);
    this.viewData = todayDate.format("D MMMM");
  }
}
