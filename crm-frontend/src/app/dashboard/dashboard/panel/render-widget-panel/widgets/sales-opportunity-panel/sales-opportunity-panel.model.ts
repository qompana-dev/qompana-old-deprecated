export interface SalesData {
  account: {
    email: string;
    id: number;
    name: string;
    surname: string;
  };
  quarter: number;
  created: number;
  createdPercentOfPast: number;
  createdTotalAmount: number;
  successful: number;
  successfulPercentOfPast: number;
  successfulTotalAmount: number;
  lost: number;
  lostPercentOfPast: number;
  lostTotalAmount: number;
  currency: string;
}

export enum SalesTypes {
  CREATED = "CREATED",
  SUCCESSFUL = "SUCCESSFUL",
  LOST = "LOST",
}
