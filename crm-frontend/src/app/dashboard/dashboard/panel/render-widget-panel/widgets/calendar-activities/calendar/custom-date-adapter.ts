import { NativeDateAdapter } from "@angular/material";
import * as moment from 'moment';

export class CustomDateAdapter extends NativeDateAdapter {
  private localeData = moment.localeData("pl");

  getFirstDayOfWeek(): number {
    return 1;
  }

  getDayOfWeekNames(style: "long" | "short" | "narrow"): string[] {
    switch (style) {
      case "long":
        return this.localeData.weekdays();
      case "short":
        return this.localeData.weekdaysShort();
      case "narrow":
        return this.localeData.weekdaysMin();
    }
  }
}
