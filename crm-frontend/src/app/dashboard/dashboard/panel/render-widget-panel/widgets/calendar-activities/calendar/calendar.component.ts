import { YEARS, MIN_YEAR, MAX_YEAR } from "./../calendar-activities.config";
import {
  ChangeDetectionStrategy,
  Component,
  Output,
  ViewChild,
  EventEmitter,
  OnInit,
} from "@angular/core";

import * as moment from "moment";

import { DateAdapter } from "@angular/material/core";
import { CustomDateAdapter } from "./custom-date-adapter";
import { MatCalendar } from "@angular/material";

@Component({
  selector: "app-calendar",
  templateUrl: "./calendar.component.html",
  styleUrls: ["./calendar.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: DateAdapter, useClass: CustomDateAdapter }],
})
export class CalendarComponent implements OnInit {
  @ViewChild("calendar")
  private calendar: MatCalendar<Date>;

  @Output() private changedDate: EventEmitter<Date> = new EventEmitter();

  public selectedDate = new Date();
  public viewData = new Date();
  public viewMonth: string;
  public viewYear: number;

  public years = YEARS;

  constructor() {}

  ngOnInit() {
    const locale = localStorage.getItem("locale");
    const momentDate = moment(this.viewData).locale(locale);
    this.viewMonth = momentDate.format("MMMM");

    this.viewYear = momentDate.year();
    this.dateChanged(this.selectedDate);
  }

  changeMonth(shift: number) {
    const locale = localStorage.getItem("locale");
    const momentDate = moment(this.viewData).add(shift, "month").locale(locale);
    if(momentDate.year() > MAX_YEAR || momentDate.year() < MIN_YEAR) {
      return;
    }
    this.viewData = momentDate.toDate();
    this.viewMonth = momentDate.format("MMMM");
    this.viewYear = momentDate.year();
    this.calendar._goToDateInView(this.viewData, "month");
  }

  changeYear({ value: year }) {
    this.viewData = moment(this.viewData).year(year).toDate();
    this.viewYear = year;
    this.calendar._goToDateInView(this.viewData, "month");
  }

  dateChanged(date) {
    this.changedDate.emit(date);
  }
}
