import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {ChartType, DashboardWidget, DataSet, ReportWidgetData, XDataType} from '../../../dashboard.model';
import {Subject} from 'rxjs';
import {DashboardService} from '../../../dashboard.service';
import {finalize, takeUntil} from 'rxjs/operators';
import {ChartOptions} from 'chart.js';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {chartTypeOptions} from '../chart-config';
import {ChartTranslationService} from '../../../chart-translation.service';

@Component({
  selector: 'app-report-panel',
  templateUrl: './report-panel.component.html',
  styleUrls: ['./report-panel.component.scss']
})
export class ReportPanelComponent implements OnInit, OnDestroy {

  @Input() set widget(widget: DashboardWidget) {
    this._widget = widget;
    this.getWidgetData(widget.widgetId);
  }

  // tslint:disable-next-line:variable-name
  _widget: DashboardWidget;
  isLoading = false;

  @Input() set userMap(userMap: Map<number, string>) {
    this.chartTranslationService.userMap = userMap;
  }

  // tslint:disable-next-line:variable-name
  _fullscreenMode = false;
  @Input()
  set fullscreenMode(mode: boolean) {
    this._fullscreenMode = mode;
    this.showCanvas = false;
    setTimeout(() => this.showCanvas = true, 50);
  }
  get fullscreenMode(): boolean {
    return this._fullscreenMode;
  }
  @Output() onRemoveWidget: EventEmitter<DashboardWidget> = new EventEmitter<DashboardWidget>();
  @Output() openFullscreen: EventEmitter<DashboardWidget> = new EventEmitter<DashboardWidget>();

  showCanvas = true;

  data: ReportWidgetData = null;
  options: { [key in keyof typeof ChartType]: ChartOptions } = chartTypeOptions;

  private componentDestroyed: Subject<void> = new Subject();

  constructor(private dashboardService: DashboardService,
              private notificationService: NotificationService,
              private chartTranslationService: ChartTranslationService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  removeWidget(): void {
    this.openFullscreen.emit(undefined);
    this.onRemoveWidget.emit(this._widget);
  }

  private getWidgetData(widgetId: number): void {
    this.isLoading = true;
    this.dashboardService.getReportWidgetData(widgetId)
      .pipe(
        takeUntil(this.componentDestroyed),
        finalize(() => this.isLoading = false)
      ).subscribe(
      data => this.data = this.translate(data),
      err => this.notificationService.notify('widgetConfigurator.notification.getReportWidgetData', NotificationType.ERROR),
    );
  }

  private translate(data: ReportWidgetData): ReportWidgetData {
    if (!data || !data.widgetReportDto) {
      return data;
    }
    if (data.widgetReportDto.widgetColumn !== XDataType.OWNER) {
      this.chartTranslationService.translateLegend(data);
    }
    this.chartTranslationService.translateXLabels(data);

    return data;
  }


  getData(data: ReportWidgetData): DataSet[] {
    if (!data || !data.chartData || data.chartData.length === 0) {
      return [{data: [], label: ''}];
    }
    return data.chartData;
  }

  openInFullscreen(): void {
    this.openFullscreen.emit((this.fullscreenMode) ? undefined : this._widget);
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.showCanvas = false;
    setTimeout(() => this.showCanvas = true, 25);
  }
}
