import {Pipe, PipeTransform} from '@angular/core';
import {ChartType, ReportWidgetData, XDataType} from '../../../dashboard.model';

@Pipe({
  name: 'legendVisible'
})
export class LegendVisiblePipe implements PipeTransform {

  transform(data: ReportWidgetData): boolean {
    if (data && data.chartData && data.chartData.length === 0) {
      return false;
    } else if (data && data.widgetReportDto && data.widgetReportDto.widgetColumn === XDataType.OWNER &&
      data.widgetReportDto.chartType !== ChartType.CHART3 && data.widgetReportDto.chartType !== ChartType.CHART4) {
      return false;
    } else {
      return true;
    }
  }

}
