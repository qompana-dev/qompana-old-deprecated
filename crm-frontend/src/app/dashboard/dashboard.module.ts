import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GridsterModule } from "angular-gridster2";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { SharedModule } from "../shared/shared.module";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DragulaModule } from "ng2-dragula";
import { CalendarPanelComponent } from "./dashboard/panel/calendar-panel/calendar-panel.component";
import {
  CalendarDateFormatter,
  CalendarModule,
  CalendarMomentDateFormatter,
  DateAdapter,
} from "angular-calendar";
import { momentAdapterFactory } from "../calendar/calendars.module";
import { ReportWidgetConfiguratorComponent } from "./widget-configurator/report-widget-configurator/report-widget-configurator.component";
import { GoalWidgetConfiguratorComponent } from "./widget-configurator/goal-widget-configurator/goal-widget-configurator.component";
import { WidgetConfiguratorComponent } from "./widget-configurator/widget-configurator.component";
import { TooltipModule } from "ngx-bootstrap";
import { TaskSliderModule } from "../calendar/task-slider/task-slider.module";
import { ChartTypeSelectComponent } from "./widget-configurator/chart-type-select/chart-type-select.component";
import { ReportPanelComponent } from "./dashboard/panel/report-panel/report-panel.component";
import { GoalPanelComponent } from "./dashboard/panel/goal-panel/goal-panel.component";
import { ChartsModule } from "ng2-charts";
import { ChartTypePipe } from "./dashboard/panel/chart-type.pipe";
import { LegendVisiblePipe } from "./dashboard/panel/report-panel/legend-visible.pipe";
import { ActivitiesPanelComponent } from "./dashboard/panel/activities-panel/activities-panel.component";
import { RenderWidgetPanelModule } from './dashboard/panel/render-widget-panel/render-widget-panel.module';
import {NotificationModule} from "../notification/notification.module";

@NgModule({
  declarations: [
    DashboardComponent,
    CalendarPanelComponent,
    WidgetConfiguratorComponent,
    GoalWidgetConfiguratorComponent,
    ReportWidgetConfiguratorComponent,
    ChartTypeSelectComponent,
    ReportPanelComponent,
    GoalPanelComponent,
    ChartTypePipe,
    LegendVisiblePipe,
    ActivitiesPanelComponent
  ],
  exports: [CalendarPanelComponent],
  imports: [
    SharedModule,
    CommonModule,
    DashboardRoutingModule,
    DragulaModule,
    TaskSliderModule,
    ChartsModule,
    RenderWidgetPanelModule,
    CalendarModule.forRoot(
      {
        provide: DateAdapter,
        useFactory: momentAdapterFactory,
      },
      {
        dateFormatter: {
          provide: CalendarDateFormatter,
          useClass: CalendarMomentDateFormatter,
        },
      }
    ),
    TooltipModule.forRoot(),
    GridsterModule,
    NotificationModule,
  ],
})
export class DashboardModule {}
