import {Injectable} from '@angular/core';
import {CustomerService} from '../customer/customer.service';
import {ReportWidgetData, XDataType} from './dashboard.model';
import {map} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {ContactsService} from '../contacts/contacts.service';
import {CrmObject} from '../shared/model/search.model';

@Injectable({
  providedIn: 'root'
})
export class ChartTranslationService {

  private _userMap: Map<number, string> = new Map();

  public set userMap(value: Map<number, string>) {
    this._userMap = value;
  }

  constructor(private customerService: CustomerService,
              private contactService: ContactsService,
              private translateService: TranslateService) {
  }

  public translateLegend(data: ReportWidgetData): void {
    data.chartData = data.chartData.map(d => ({...d, label: this._userMap.get(+d.label)}));
  }

  public translateXLabels(data: ReportWidgetData): void {
    if (!data || !data.widgetReportDto) {
      return;
    }

    switch (data.widgetReportDto.widgetColumn) {
      case XDataType.OWNER: {
        data.lineChartLabels = data.lineChartLabels.map(label => (this._userMap.get(+label)));
        break;
      }
      case XDataType.CUSTOMER: {
        this.translateCustomerLabels(data);
        break;
      }
      case XDataType.CONTACT: {
        this.translateContactLabels(data);
        break;
      }
      case XDataType.OPPORTUNITY_PRIORITY: {
        this.translateLabels(data, 'opportunity.form.priorities.');
        break;
      }
      case XDataType.LEAD_PRIORITY: {
        this.translateLabels(data, 'leads.form.leadPriority.');
        break;
      }
      case XDataType.REJECTION_RESULT: {
        this.translateLabels(data, 'opportunity.form.dialog.rejectionReason.item.');
        break;
      }
      case XDataType.PRIORITY: {
        this.translateLabels(data, 'calendar.event.priorities.');
        break;
      }
      case XDataType.TASK_STATE: {
        this.translateLabels(data, 'calendar.event.states.');
        break;
      }
    }
  }

  private translateCustomerLabels(data: ReportWidgetData): void {
    const customerIds: number[] = this.getIds(data);
    this.customerService.getCustomersByIds(customerIds).pipe(
      map(crmObjects => this.crmObjectsToMap(crmObjects))
    ).subscribe(
      idLabelMap => this.assignTranslations(data, idLabelMap)
    );
  }

  private translateContactLabels(data: ReportWidgetData): void {
    const contactIds: number[] = this.getIds(data);
    this.contactService.getContactsByIds(contactIds).pipe(
      map(crmObjects => this.crmObjectsToMap(crmObjects))
    ).subscribe(
      idLabelMap => this.assignTranslations(data, idLabelMap)
    );
  }

  private assignTranslations(data: ReportWidgetData, idLabelMap: Map<string, string>): void {
    data.lineChartLabels = data.lineChartLabels.map(label => (!!(idLabelMap.get(label.toString())) ? (idLabelMap.get(label.toString())) : '-'));
  }

  private crmObjectsToMap(crmObjects: CrmObject[]): Map<string, string> {
    return new Map<string, string>(crmObjects.map(i => [String(i.id), i.label] as [string, string]));
  }

  private getIds(data: ReportWidgetData): number[] {
    return data.lineChartLabels.map(label => +label);
  }

  private translateLabels(data: ReportWidgetData, keyPrefix: string, camelCase: boolean = false): void {
    const keys: string[] = this.mapToTranslationKeys(data, keyPrefix, camelCase);
    if (!keys || keys.length === 0) {
      return;
    }
    this.translateService.get(keys).pipe(
      map(translations => this.translationsToMap(translations, keyPrefix, camelCase))
    ).subscribe(
      idLabelMap => this.assignTranslations(data, idLabelMap)
    );
  }

  private mapToTranslationKeys(data: ReportWidgetData, keyPrefix: string, camelCase: boolean): string[] {
    const labels = data.lineChartLabels.filter(label => !!label);
    if (camelCase) {
      return labels.map(label => `${keyPrefix}${this.snakeToCamel(String(label).toLowerCase())}`);
    }
    return labels.map(label => `${keyPrefix}${String(label).toLowerCase()}`);
  }

  private translationsToMap(translations: any, keyPrefix: string, camelCase: boolean = false): Map<string, string> {
    if (camelCase) {
      return new Map(Object.entries(translations).map(
        entry => [this.camelToSnake(entry[0].slice(keyPrefix.length)).toUpperCase(), entry[1]] as [string, string]));
    }
    return new Map(Object.entries(translations).map(
      entry => [entry[0].slice(keyPrefix.length).toUpperCase(), entry[1]] as [string, string]));
  }

  private snakeToCamel(value: string): string {
    return value.replace(
      /([-_][a-z])/g,
      (group) => group.toUpperCase()
        .replace('-', '')
        .replace('_', '')
    );
  }

  private camelToSnake(value: string): string {
    return value.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`);
  }

}
