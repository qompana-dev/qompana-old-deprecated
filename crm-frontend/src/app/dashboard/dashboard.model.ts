import { ChartDataSets } from "chart.js";
import { Label } from "ng2-charts";
import { GoalInterval, GoalType } from "../goal/goal.model";

export interface DashboardWidget extends WidgetPositionItem {
  id: number;
  widgetTypeEnum: WidgetTypeEnum;
  widgetId?: number;
}

export interface SingleDashboardWidget extends WidgetPositionItem {
  widgetTypeEnum: WidgetTypeEnum;
  widgetId?: number;
}

export enum WidgetTypeEnum {
  GOAL = "GOAL",
  REPORT = "REPORT",
  CALENDAR = "CALENDAR",
  TOTAL_SALES_OPPORTUNITY = "TOTAL_SALES_OPPORTUNITY",
  NOTE = "NOTE",
  ACTIVITY = "ACTIVITY",
  FILE = "FILE",
  LEAD_SOURCES = "LEAD_SOURCES",
  OPENED_SALES_OPPORTUNITY = "OPENED_SALES_OPPORTUNITY",
  NOTIFICATIONS = "NOTIFICATIONS",
}

export interface WidgetPositionItem {
  rows: number;
  cols: number;
  x: number;
  y: number;
}

export enum ReportWidgetType {
  CONVERSION_LEAD_FACTOR = "CONVERSION_LEAD_FACTOR",
  WON_OPPORTUNITIES_NUMBER = "WON_OPPORTUNITIES_NUMBER",
  WON_OPPORTUNITIES_AMOUNT = "WON_OPPORTUNITIES_AMOUNT",
  WON_OPPORTUNITIES_NUMBER_CONVERTED_FROM_LEAD = "WON_OPPORTUNITIES_NUMBER_CONVERTED_FROM_LEAD",
  LOSS_OPPORTUNITIES_NUMBER = "LOSS_OPPORTUNITIES_NUMBER",
  LOSS_OPPORTUNITIES_AMOUNT = "LOSS_OPPORTUNITIES_AMOUNT",
  OPPORTUNITIES_AMOUNT_ACCORDING_TO_OPPORTUNITY_STEP = "OPPORTUNITIES_AMOUNT_ACCORDING_TO_OPPORTUNITY_STEP",
  MEETING_NUMBER_IN_CALENDAR = "MEETING_NUMBER_IN_CALENDAR",
}

export class WidgetGoal {
  goalId: number;
  chartType: string;
  participants: WidgetGoalParticipant[];
}

export interface WidgetGoalParticipant {
  userId: number;
  goal: boolean;
}

export enum ChartType {
  CHART1 = "CHART1",
  CHART2 = "CHART2",
  CHART3 = "CHART3",
  CHART4 = "CHART4",
  CHART5 = "CHART5",
}

export enum XDataType {
  CUSTOMER = "CUSTOMER",
  CONTACT = "CONTACT",
  OPPORTUNITY_PRIORITY = "OPPORTUNITY_PRIORITY",
  CURRENCY = "CURRENCY",
  REJECTION_RESULT = "REJECTION_RESULT",
  OWNER = "OWNER",
  COMPANY = "COMPANY",
  LEAD_PRIORITY = "LEAD_PRIORITY",
  PRIORITY = "PRIORITY",
  TASK_STATE = "TASK_STATE",
}

export const reportTypeXData: {
  [key in keyof typeof ReportWidgetType]: XDataType[];
} = {
  [ReportWidgetType.CONVERSION_LEAD_FACTOR]: [
    XDataType.COMPANY,
    XDataType.LEAD_PRIORITY,
    XDataType.OWNER,
  ],
  [ReportWidgetType.OPPORTUNITIES_AMOUNT_ACCORDING_TO_OPPORTUNITY_STEP]: [],
  [ReportWidgetType.WON_OPPORTUNITIES_NUMBER]: [
    XDataType.CUSTOMER,
    XDataType.CONTACT,
    XDataType.OPPORTUNITY_PRIORITY,
    XDataType.CURRENCY,
    XDataType.OWNER,
  ],
  [ReportWidgetType.WON_OPPORTUNITIES_AMOUNT]: [
    XDataType.CUSTOMER,
    XDataType.CONTACT,
    XDataType.OPPORTUNITY_PRIORITY,
    XDataType.CURRENCY,
    XDataType.OWNER,
  ],
  [ReportWidgetType.WON_OPPORTUNITIES_NUMBER_CONVERTED_FROM_LEAD]: [
    XDataType.CUSTOMER,
    XDataType.CONTACT,
    XDataType.OPPORTUNITY_PRIORITY,
    XDataType.CURRENCY,
    XDataType.OWNER,
  ],
  [ReportWidgetType.LOSS_OPPORTUNITIES_NUMBER]: [
    XDataType.CUSTOMER,
    XDataType.CONTACT,
    XDataType.OPPORTUNITY_PRIORITY,
    XDataType.CURRENCY,
    XDataType.REJECTION_RESULT,
    XDataType.OWNER,
  ],
  [ReportWidgetType.LOSS_OPPORTUNITIES_AMOUNT]: [
    XDataType.CUSTOMER,
    XDataType.CONTACT,
    XDataType.OPPORTUNITY_PRIORITY,
    XDataType.CURRENCY,
    XDataType.REJECTION_RESULT,
    XDataType.OWNER,
  ],
  [ReportWidgetType.MEETING_NUMBER_IN_CALENDAR]: [
    XDataType.PRIORITY,
    XDataType.TASK_STATE,
    XDataType.OWNER,
  ],
};

export class ReportWidget {
  reportType: ReportWidgetType;
  start: string;
  end: string;
  withTime: boolean;
  chartType: ChartType;
  widgetColumn: XDataType;
  additionalValue: string;
  users: number[];
}

export class DataSet {
  data: number[];
  label: string;
}

export class ReportWidgetData {
  widgetReportDto: ReportWidget;
  chartData: DataSet[];
  lineChartLabels: Label[];
}

export class GoalWidgetData {
  name: string;
  type: GoalType;
  interval: GoalInterval;
  intervalNumber: number;
  startIndex: number;
  startYear: number;
  currency: string;
  chartType: ChartType;
  userWidgetDataDtoList: ChartDataSets[];
  labels: Label[];
}
