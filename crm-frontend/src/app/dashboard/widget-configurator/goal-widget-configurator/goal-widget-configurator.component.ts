import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GoalCategory, GoalCategoryType, GoalType, GoalWidgetPreviewDto, UserGoalDto} from '../../../goal/goal.model';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {GoalService} from '../../../goal/goal.service';
import {ChartType, ReportWidget, WidgetGoal, WidgetGoalParticipant} from '../../dashboard.model';
import {FormUtil} from '../../../shared/form.util';
import {MatCheckboxChange} from '@angular/material/checkbox';

@Component({
  selector: 'app-goal-widget-configurator',
  templateUrl: './goal-widget-configurator.component.html',
  styleUrls: ['./goal-widget-configurator.component.scss']
})
export class GoalWidgetConfiguratorComponent implements OnInit, OnDestroy {
  goalWidgetForm: FormGroup;
  goalCategoryEnum = GoalCategory;
  goalCategoryTypeEnumUtil = new GoalCategoryType();
  goalAvailableTypes: GoalType[] = [];
  goalsByType: GoalWidgetPreviewDto[] = [];
  userGoals: UserGoalDto[] = [];
  participants: WidgetGoalParticipant[] = [];
  @Input() userMap;
  private componentDestroyed: Subject<void> = new Subject();

  // @formatter:off
  get category(): AbstractControl { return this.goalWidgetForm.get('category'); }
  get type(): AbstractControl { return this.goalWidgetForm.get('type'); }
  get goal(): AbstractControl { return this.goalWidgetForm.get('goal'); }
  get chartType(): AbstractControl { return this.goalWidgetForm.get('chartType'); }
  // @formatter:on

  constructor(private fb: FormBuilder,
              private goalService: GoalService) {
  }

  ngOnInit(): void {
    this.createGoalWidgetForm();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  public clear(): void {
    this.createGoalWidgetForm();
    this.userGoals = [];
    this.participants = [];
  }

  public getGoalWidget(): WidgetGoal {
    return {
      goalId: +this.goal.value,
      chartType: '' + this.chartType.value,
      participants: this.participants
    };
  }

  public setTouched(): void {
    FormUtil.setTouched(this.goalWidgetForm);
  }

  public isValid(): boolean {
    return this.goalWidgetForm.valid && this.participants.length !== 0;
  }

  changeChartType(type: ChartType): void {
    this.chartType.patchValue(type);
  }

  changeParticipants(userId: number, goal: boolean, checked: MatCheckboxChange): void {
    if (checked.checked) {
      this.participants.push({userId, goal});
    } else {
      this.participants = this.participants.filter(e => !(e.userId === userId && e.goal === goal));
    }
  }

  private createGoalWidgetForm(): void {
    this.goalWidgetForm = this.fb.group({
      category: [this.goalCategoryEnum.LEAD, Validators.required],
      type: [GoalType.CREATED_LEADS_NUMBER, Validators.required],
      goal: ['', Validators.required],
      chartType: [ChartType.CHART1, Validators.required]
    });
    this.subscribeForCategoryChange();
    this.subscribeForTypeChange();
    this.subscribeForGoalChange();
  }

  private subscribeForCategoryChange(): void {
    this.changeAvailableTypes();
    this.category.valueChanges.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(() => this.changeAvailableTypes());
  }

  private changeAvailableTypes(notEmit?: boolean): void {
    this.goalAvailableTypes = this.goalCategoryTypeEnumUtil.getTypes(this.category.value);
    if (this.goalAvailableTypes && this.goalAvailableTypes.indexOf(this.type.value) === -1) {
      this.type.patchValue(this.goalAvailableTypes[0], {emitEvent: !notEmit});
    }
  }

  private subscribeForTypeChange(): void {
    this.changeGoals();
    this.type.valueChanges.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(() => this.changeGoals());
  }

  private changeGoals(): void {
    this.goal.patchValue(null);
    this.goalService.getAllForMeByType(this.type.value).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (goals) => {
        this.goalsByType = goals;
        this.showErrorIfNeeded();
      }
    );
  }

  private showErrorIfNeeded(): void {
    if (this.goalsByType.length === 0) {
      this.type.setErrors({noGoals: true});
      this.type.markAsTouched();
    } else {
      this.type.setErrors(null);
    }
  }

  private subscribeForGoalChange(): void {
    this.changeUserGoals();
    this.goal.valueChanges.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(() => this.changeUserGoals());
  }

  private changeUserGoals(): void {
    if (this.goal.value) {
      this.goalService.getUserGoalsForWidgetCreator(this.goal.value).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe((userGoals) => this.userGoals = userGoals);
    } else {
      this.userGoals = [];
    }
    this.participants = [];
  }


}
