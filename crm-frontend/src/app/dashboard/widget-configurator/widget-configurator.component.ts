import { WidgetPositionItem } from "./../dashboard.model";
import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { SliderComponent } from "../../shared/slider/slider.component";
import { map, takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { GoalWidgetConfiguratorComponent } from "./goal-widget-configurator/goal-widget-configurator.component";
import { ReportWidgetConfiguratorComponent } from "./report-widget-configurator/report-widget-configurator.component";
import {
  NotificationService,
  NotificationType,
} from "../../shared/notification.service";
import { DashboardService } from "../dashboard.service";
import { DashboardWidget, WidgetTypeEnum } from "../dashboard.model";
import { GridsterConfig } from "angular-gridster2";
import { DASHBOARD_SETTING } from "../dashboard/dashboard.config";

@Component({
  selector: "app-widget-configurator",
  templateUrl: "./widget-configurator.component.html",
  styleUrls: ["./widget-configurator.component.scss"],
})
export class WidgetConfiguratorComponent implements OnInit, OnDestroy {
  @Input() slider: SliderComponent;
  @Input() options: GridsterConfig;

  widgetType = WidgetTypeEnum;
  selectedWidgetType = WidgetTypeEnum.GOAL;
  @ViewChild("goalWidgetConfigurator")
  goalWidgetConfigurator: GoalWidgetConfiguratorComponent;
  @ViewChild("reportWidgetConfigurator")
  reportWidgetConfigurator: ReportWidgetConfiguratorComponent;

  @Input() userMap: Map<number, string> = new Map();
  private componentDestroyed: Subject<void> = new Subject();

  @Output() newElementAdded: EventEmitter<DashboardWidget> = new EventEmitter<
    DashboardWidget
  >();

  constructor(
    private dashboardService: DashboardService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.slider.closeEvent
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => setTimeout(() => this.reset(), 500));
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  closeSlider(): void {
    this.slider.close();
  }

  submit(): void {
    const position = this.getPositionWidget(this.selectedWidgetType);
    if (position) {
      switch (this.selectedWidgetType) {
        case WidgetTypeEnum.GOAL:
          this.submitGoalWidget(position);
          break;
        case WidgetTypeEnum.REPORT:
          this.submitReportWidget(position);
          break;
        default:
          this.submitWidget(this.selectedWidgetType, position);
          break;
      }
    }
  }

  getPositionWidget(widgetType: WidgetTypeEnum): WidgetPositionItem | null {
    const { rows = 2, cols = 2 } = DASHBOARD_SETTING[widgetType] || {};
    const position = { rows, cols, x: 0, y: 0 };
    const canAddWidget =
      this.options.api && this.options.api.getNextPossiblePosition(position);
    if (canAddWidget) {
      const firstPosition = this.options.api.getFirstPossiblePosition(position);
      return firstPosition;
    }
    this.notificationService.notify(
      "dashboard.noPlaceWidgetError",
      NotificationType.ERROR
    );
    return null;
  }

  private submitWidget(
    widgetType: WidgetTypeEnum,
    position: WidgetPositionItem
  ) {
    this.dashboardService
      .setDashboardWidget({
        widgetTypeEnum: widgetType,
        ...position,
      })
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (newWidget: DashboardWidget) => {
          this.newElementAdded.emit(newWidget);
          this.closeSlider();
        },
        () =>
          this.notificationService.notify(
            "widgetConfigurator.saveWidgetError",
            NotificationType.ERROR
          )
      );
  }

  private submitGoalWidget(position: WidgetPositionItem): void {
    if (this.goalWidgetConfigurator.isValid()) {
      this.dashboardService
        .saveGoalWidget({
          ...this.goalWidgetConfigurator.getGoalWidget(),
          ...position,
        })
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (newWidget: DashboardWidget) => {
            this.newElementAdded.emit(newWidget);
            this.notificationService.notify(
              "widgetConfigurator.goalWidgetSaved",
              NotificationType.SUCCESS
            );
            this.closeSlider();
          },
          () =>
            this.notificationService.notify(
              "widgetConfigurator.goalWidgetSaveError",
              NotificationType.ERROR
            )
        );
    } else {
      this.goalWidgetConfigurator.setTouched();
    }
  }

  private reset(): void {
    this.selectedWidgetType = WidgetTypeEnum.GOAL;
    if (this.goalWidgetConfigurator) {
      this.goalWidgetConfigurator.clear();
    }
  }

  private submitReportWidget(position: WidgetPositionItem): void {
    if (this.reportWidgetConfigurator.isValid()) {
      this.dashboardService
        .saveReportWidget({
          ...this.reportWidgetConfigurator.getReportWidget(),
          ...position,
        })
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (widget) => {
            this.notificationService.notify(
              "widgetConfigurator.notification.reportWidgetSaved",
              NotificationType.SUCCESS
            );
            this.newElementAdded.emit(widget);
            this.closeSlider();
          },
          () =>
            this.notificationService.notify(
              "widgetConfigurator.notification.reportWidgetSaveError",
              NotificationType.ERROR
            )
        );
    } else {
      this.reportWidgetConfigurator.setTouched();
    }
  }
}
