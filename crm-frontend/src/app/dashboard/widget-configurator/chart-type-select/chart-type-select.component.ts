import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ChartType} from '../../dashboard.model';

@Component({
  selector: 'app-chart-type-select',
  templateUrl: './chart-type-select.component.html',
  styleUrls: ['./chart-type-select.component.scss']
})
export class ChartTypeSelectComponent {

  @Input() value: ChartType;
  @Output() selectionChanged: EventEmitter<ChartType> = new EventEmitter<ChartType>();
  chartType = ChartType;

  constructor() {
  }

  changeChartType(chartType: ChartType): void {
    this.selectionChanged.emit(chartType);
  }
}
