import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {ChartType, reportTypeXData, ReportWidget, ReportWidgetType, XDataType} from '../../dashboard.model';
import {TimesUtil} from '../../../shared/times.util';
import {TimePickerHour} from '../../../shared/model';
import * as moment from 'moment';
import {PersonService} from '../../../person/person.service';
import {filter, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {AccountInfoDto} from '../../../person/person.model';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {MatCheckboxChange} from '@angular/material';
import {LeadService} from '../../../lead/lead.service';
import {ProcessDefinition} from '../../../lead/lead.model';
import {FormUtil} from '../../../shared/form.util';

@Component({
  selector: 'app-report-widget-configurator',
  templateUrl: './report-widget-configurator.component.html',
  styleUrls: ['./report-widget-configurator.component.scss']
})
export class ReportWidgetConfiguratorComponent implements OnInit, OnDestroy {

  reportWidgetForm: FormGroup;
  reportWidgetType = ReportWidgetType;
  taskHourList: TimePickerHour[] = [];
  moment = moment;
  britishTimeFormat = false;
  processDefinitions: ProcessDefinition[];
  private componentDestroyed: Subject<void> = new Subject();
  users: AccountInfoDto[];
  private selectedUsers: number[] = [];

  // @formatter:off
  get typeControl(): AbstractControl { return this.reportWidgetForm.get('type'); }
  get startDateControl(): AbstractControl { return this.reportWidgetForm.get('startDate'); }
  get startTimeControl(): AbstractControl { return this.reportWidgetForm.get('startTime'); }
  get endDateControl(): AbstractControl { return this.reportWidgetForm.get('endDate'); }
  get endTimeControl(): AbstractControl { return this.reportWidgetForm.get('endTime'); }
  get withTimeControl(): AbstractControl { return this.reportWidgetForm.get('withTime'); }
  get chartTypeControl(): AbstractControl { return this.reportWidgetForm.get('chartType'); }
  get xDataControl(): AbstractControl { return this.reportWidgetForm.get('xData'); }
  get usersControl(): AbstractControl { return this.reportWidgetForm.get('users'); }
  get additionalValueControl(): AbstractControl { return this.reportWidgetForm.get('additionalValue'); }
  // @formatter:on

  constructor(private fb: FormBuilder,
              private personService: PersonService,
              private notificationService: NotificationService,
              private leadService: LeadService) {
    this.createWidgetForm();
    this.taskHourList = TimesUtil.getHoursList();
    this.subscribeTypeChange();
  }

  ngOnInit(): void {
    this.getUserList();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private createWidgetForm(): void {
    this.reportWidgetForm = this.fb.group({
      type: [null, Validators.required],
      startDate: [null, Validators.required],
      startTime: [null],
      endDate: [null],
      endTime: [null],
      chartType: [ChartType.CHART1, Validators.required],
      xData: [null],
      additionalValue: [null],
      withTime: [false, Validators.required],
      users: [null, Validators.required],
    }, {validators: [this.getHronologyDateValidator(), this.getAdditionalValueOrXDataRequiredValidator()]});
  }

  changeChartType($event: ChartType): void {
    this.chartTypeControl.patchValue($event);
  }

  getXDataValues(): string[] {
    if (this.reportWidgetForm && this.typeControl) {
      return reportTypeXData[this.typeControl.value];
    } else {
      return [];
    }
  }

  changeParticipants(userId: number, checked: MatCheckboxChange): void {
    if (checked.checked) {
      this.selectedUsers.push(userId);
    } else {
      this.selectedUsers = this.selectedUsers.filter(id => id !== userId);
    }
    this.usersControl.patchValue(this.selectedUsers.length === 0 ? null : this.selectedUsers);
  }

  isValid(): boolean {
    return this.reportWidgetForm.valid;
  }

  setTouched(): void {
    FormUtil.setTouched(this.reportWidgetForm);
  }

  private getUserList(): void {
    this.personService.getAccountInfoChildListForLoggedUserRole()
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(users => this.users = users,
      () => this.notificationService.notify('widgetConfigurator.notification.getUsersListError', NotificationType.ERROR)
    );
  }


  isOpportunityProcessSelected(): boolean {
    if (this.reportWidgetForm && this.typeControl) {
      return this.typeControl.value === ReportWidgetType.OPPORTUNITIES_AMOUNT_ACCORDING_TO_OPPORTUNITY_STEP;
    } else {
      return false;
    }
  }

  private subscribeTypeChange(): void {
    this.typeControl.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      filter(type => type === ReportWidgetType.OPPORTUNITIES_AMOUNT_ACCORDING_TO_OPPORTUNITY_STEP)
    ).subscribe(
      () => this.getProcessDefinitions()
    );
  }

  private getProcessDefinitions(): void {
    this.leadService.getProcessDefinitions('OPPORTUNITY')
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      definitions => this.processDefinitions = definitions,
      err => this.notificationService.notify('widgetConfigurator.notification.getProcessDefinitionsError', NotificationType.ERROR)
    );
  }

  private getAdditionalValueOrXDataRequiredValidator(): (control: AbstractControl) => ValidationErrors {
    return (control: FormGroup): ValidationErrors | null => {
      if (this.isOpportunityProcessSelected() && !control.get('additionalValue').value) {
        control.get('additionalValue').setErrors({required: true});
      } else {
        control.get('additionalValue').setErrors(null);
      }
      if ((this.getXDataValues() && this.getXDataValues().length !== 0) && !control.get('xData').value) {
        control.get('xData').setErrors({required: true});
      } else {
        control.get('xData').setErrors(null);
      }
      return null;
    };
  }

  private getHronologyDateValidator(): (control: AbstractControl) => ValidationErrors {
    return (control: FormGroup): ValidationErrors | null => {
      if (this.reportWidgetForm && this.startDateControl && this.startDateControl.value && this.startTimeControl
        && this.endDateControl && this.endDateControl.value && this.endTimeControl) {
        let startDate = new Date(this.startDateControl.value);
        let endDate = new Date(this.endDateControl.value);
        startDate = this.setTime(this.startTimeControl.value, startDate);
        endDate = this.setTime(this.endTimeControl.value, endDate);
        return startDate > endDate ? {startDayNotBefore: true} : null;
      }
    };
  }

  private setTime(time: number, date: Date): Date {
    if (date == null) {
      return date;
    }
    if (time == null) {
      date.setHours(0);
      date.setMinutes(0);
    }
    const taskHour = this.taskHourList.find(hour => hour.id === time);
    if (taskHour) {
      date.setHours(taskHour.hour);
      date.setMinutes(taskHour.minute);
    }
    return date;
  }

  getReportWidget(): ReportWidget {
    const reportWidget: ReportWidget = {
      reportType: this.typeControl.value,
      start: this.getDateAndTime(this.startDateControl.value, this.startTimeControl.value),
      end: this.getDateAndTime(this.endDateControl.value, this.endTimeControl.value),
      chartType: this.chartTypeControl.value,
      widgetColumn: this.getWidgetColumn(),
      withTime: this.withTimeControl.value,
      additionalValue: this.additionalValueControl.value,
      users: this.usersControl.value
    };
    return reportWidget;
  }

  private getWidgetColumn(): XDataType {
    if (this.typeControl.value === ReportWidgetType.OPPORTUNITIES_AMOUNT_ACCORDING_TO_OPPORTUNITY_STEP) {
      return 'TASK' as XDataType;
    }
    return this.xDataControl.value;
  }

  getDateAndTime(date: any, time: number): string {
    if (!date) {
      return null;
    }
    return moment.utc(this.setTime(time, new Date(date))).toISOString();
    const result = new Date(date);
    this.setTime(time, result);
    return result.toISOString();
  }

}
