import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import {
  DashboardWidget,
  GoalWidgetData,
  ReportWidget,
  ReportWidgetData,
  SingleDashboardWidget,
  WidgetGoal,
} from "./dashboard.model";
import { UrlUtil } from "../shared/url.util";
import { SalesData } from "./dashboard/panel/render-widget-panel/widgets/sales-opportunity-panel/sales-opportunity-panel.model";
import { ProcessOpportunityData } from "./dashboard/panel/render-widget-panel/widgets/opened-sales-opportunity/opened-sales-opportunity.model";
import { SourceOfLeadsData } from "./dashboard/panel/render-widget-panel/widgets/source-of-leads/source-of-leads.model";
import { AnySourceData } from "mapbox-gl";

@Injectable({
  providedIn: "root",
})
export class DashboardService {
  public readonly userServiceUrl = `${UrlUtil.url}/crm-user-service`;
  public readonly businessServiceUrl = `${UrlUtil.url}/crm-business-service`;
  public readonly taskServiceUrl = `${UrlUtil.url}/crm-task-service`;

  constructor(private http: HttpClient) {}

  public updateDashboardSettings(
    widgetList: DashboardWidget[]
  ): Observable<DashboardWidget[]> {
    const url = `${this.userServiceUrl}/widget/dashboard/swap`;
    return this.http.put<DashboardWidget[]>(url, widgetList);
  }

  public setDashboardWidget(
    widget: SingleDashboardWidget
  ): Observable<DashboardWidget> {
    const url = `${this.userServiceUrl}/widget/dashboard`;
    return this.http.post<DashboardWidget>(url, widget);
  }

  public getDashboardSettings(): Observable<DashboardWidget[]> {
    const url = `${this.userServiceUrl}/widget/dashboard`;
    return this.http.get<DashboardWidget[]>(url);
  }

  public removeDashboardById(id: number): Observable<void> {
    const url = `${this.userServiceUrl}/widget/dashboard/id/${id}`;
    return this.http.delete<void>(url);
  }

  public saveGoalWidget(widgetGoal: WidgetGoal): Observable<DashboardWidget> {
    const url = `${this.businessServiceUrl}/widget-goals`;
    return this.http.post<DashboardWidget>(url, widgetGoal);
  }

  public saveReportWidget(
    reportWidget: ReportWidget
  ): Observable<DashboardWidget> {
    const url = `${this.businessServiceUrl}/widget/report/save`;
    return this.http.post<DashboardWidget>(url, reportWidget);
  }

  public getReportWidgetData(widgetId: number): Observable<ReportWidgetData> {
    const url = `${this.businessServiceUrl}/widget/report/data/${widgetId}`;
    return this.http.get<ReportWidgetData>(url);
  }

  public getInformationWidgetData(widgetId: number): Observable<any> {
    const url = `${this.businessServiceUrl}/widget/information/data/${widgetId}`;
    return this.http.get<any>(url);
  }

  public getGoalWidgetData(widgetGoalId: number): Observable<GoalWidgetData> {
    const url = `${this.businessServiceUrl}/widget-goals/${widgetGoalId}`;
    return this.http.get<GoalWidgetData>(url);
  }

  public getTotalSalesOpportunityData(): Observable<SalesData> {
    const url = `${this.businessServiceUrl}/widget/total-sales-opportunity/data`;
    return this.http.get<SalesData>(url);
  }

  public getOpenedSalesOpportunityData(
    id: string
  ): Observable<ProcessOpportunityData> {
    const url = `${this.businessServiceUrl}/widget/opened-sales-opportunity/data/processDefinitionId/${id}`;
    return this.http.get<ProcessOpportunityData>(url);
  }

  public getSourceOfLeadsData(period: string): Observable<SourceOfLeadsData> {
    const url = `${this.businessServiceUrl}/widget/lead-source?period=${period}`;
    return this.http.get<SourceOfLeadsData>(url);
  }

  public getOwnerTasksByDate(
    owner: number,
    date: string,
    timeCategory: string
  ): Observable<any> {
    const url = `${this.taskServiceUrl}/tasks?owner=${owner}&date=${date}&timeCategory=${timeCategory}`;
    return this.http.get<any>(url);
  }
}
