import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {LoginService} from '../login.service';
import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';
import {NotificationService} from '../../shared/notification.service';
import {LoginStatusEnum} from '../login.model';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private $router: Router,
              private loginService: LoginService,
              private authService: AuthService,
              private notificationService: NotificationService) {
  }

  private readonly authorization = {
    'person/info': () => this.isAuthorized('NavigationComponent.settingItem.info'),
    'person/info/password': () => this.isAuthorized('PersonInfoComponent.changePasswordButton'),
    'role/add': () => this.isAuthorized('NavigationComponent.permissions'),
    'role/edit': () => this.isAuthorized('NavigationComponent.permissions'),
    'role/list': () => this.isAuthorized('NavigationComponent.permissions'),
    'profile/list': () => this.isAuthorized('NavigationComponent.permissions'),
    'profile/add': () => this.isAuthorized('NavigationComponent.permissions'),
    'profile/edit/:profileId': () => this.isAuthorized('NavigationComponent.permissions'),

    'person/list': () => this.isAuthorized('NavigationMenu.users'),
    'person/detail/:accountId': () => this.isAuthorized('NavigationMenu.users'),
    'person/add': () => this.isAuthorized('NavigationMenu.users') && this.isAuthorized('PersonListComponent.addUser'),
    'leads': () => this.isAuthorized('NavigationMenu.leads'),
    'calendar': () => this.isAuthorized('NavigationMenu.calendar'),
    'customers': () => this.isAuthorized('NavigationMenu.customers'),
    'contacts/list': () => this.isAuthorized('NavigationMenu.contacts'),
    'documents': () => this.isAuthorized('NavigationMenu.documents'),
    'products': () => this.isAuthorized('NavigationMenu.products'),
    'bpmn': () => this.isAuthorized('NavigationMenu.bpmn'),
    'manufacturers-and-suppliers': () => this.isAuthorized('NavigationMenu.manufacturersAndSuppliers'),
    'administration': () => this.isAuthorized('NavigationComponent.settingItem.administration'),
    'mail': () => this.isAuthorized('NavigationMenu.mail')
  };

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.loginService.isLoggedIn()) {
      return this.checkIfUserIsAuthorized(route.routeConfig.path);
    } else {
      this.handleNotLoggedInUser();
      return false;
    }
  }

  private checkIfUserIsAuthorized(path: string): boolean {
    if (!this.authorization[path] || this.authorization[path]()) {
      return true;
    } else {
      this.authService.refreshPermission();
      this.$router.navigate(['/']);
      return false;
    }
  }

  private isAuthorized(permission: string): boolean {
    return this.authService.isPermissionPresent(permission, 'w');
  }

  private handleNotLoggedInUser(): void {
    this.loginService.loggedUserStatusChangeSubject.next(LoginStatusEnum.NONE);
    this.$router.navigate(['/login']);
  }
}
