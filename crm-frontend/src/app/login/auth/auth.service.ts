import {Injectable} from '@angular/core';
import {Observable, Subject, Subscription} from 'rxjs';
import {tap} from 'rxjs/operators';
import {ModulePermission, Permission, PermissionStateEnum, Profile, WebControlDependsOnEnum} from './auth.model';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {UrlUtil} from '../../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private static readonly WRITE_KEY = 'write';
  private static readonly INVISIBLE_KEY = 'invisible';
  private static readonly READONLY_KEY = 'readonly';

  private static dependsOnTypes: DependsOnType[] = [
    {dependsOn: WebControlDependsOnEnum.CREATE, check:
        (modulePermission: ModulePermission) => modulePermission.view && modulePermission.create},
    {dependsOn: WebControlDependsOnEnum.DELETE, check:
        (modulePermission: ModulePermission) => modulePermission.view && modulePermission.delete},
    {dependsOn: WebControlDependsOnEnum.VIEW, check: (modulePermission: ModulePermission) => modulePermission.view},
    {dependsOn: WebControlDependsOnEnum.EDIT, check: (modulePermission: ModulePermission) => modulePermission.view && modulePermission.edit}
  ];

  private static defaultAccessRight: AccessRights = {canWrite: false, isReadOnly: false, isInvisible: true};

  private permissions: Map<string, PermissionListItem> = new Map<string, PermissionListItem>();

  private types: PermissionType[] = [
    {normalKey: AuthService.WRITE_KEY, shortKey: 'w', accessObjectValue: 'canWrite'},
    {normalKey: AuthService.INVISIBLE_KEY, shortKey: 'i', accessObjectValue: 'isInvisible'},
    {normalKey: AuthService.READONLY_KEY, shortKey: 'r', accessObjectValue: 'isReadOnly'}
  ];

  onAuthChange: Subject<void> = new Subject<void>();

  constructor(private http: HttpClient) {
  }

  private static getRights(permission: Permission, modulePermission: ModulePermission): AccessRights {
    if (!permission || permission.webControl === undefined || permission.state === undefined || permission.webControl.type === undefined) {
      return AuthService.defaultAccessRight;
    }
    const dependsOnItem: DependsOnType = AuthService.dependsOnTypes.find((dependsOnType) => {
      return WebControlDependsOnEnum[dependsOnType.dependsOn] === permission.webControl.dependsOn;
    });

    if (!dependsOnItem || !dependsOnItem.check(modulePermission)) {
      return AuthService.defaultAccessRight;
    }

    return {
      isInvisible: permission.state === PermissionStateEnum[PermissionStateEnum.INVISIBLE],
      isReadOnly: permission.state === PermissionStateEnum[PermissionStateEnum.READ_ONLY],
      canWrite: permission.state === PermissionStateEnum[PermissionStateEnum.WRITE]
    };
  }

  public isPermissionPresent(id: string, type: string): boolean {
    if (this.permissions.size === 0) {
      this.getProfileFromLocalStorage();
    }
    if (!type) {
      return false;
    }
    const permission: PermissionListItem = this.getPermission(id);
    const typeLowerCase = type.toString().toLowerCase();
    const result = this.types.find((t) => typeLowerCase === t.normalKey || typeLowerCase === t.shortKey);

    if (result && permission) {
      return permission.access[result.accessObjectValue];
    }
    return false;
  }

  public refreshPermission(): void {
    const sub: Subscription = this.getPermissions()
      .subscribe(undefined, undefined, () => {
        if (sub) {
          sub.unsubscribe();
        }
      });
  }

  public getPermissions(): Observable<any> {
    return this.http.get(`${UrlUtil.url}/crm-user-service/profile/mine`).pipe(
      tap((profile: Profile) => this.savePermissionMapFromProfile(profile)),
      tap(() => this.onAuthChange.next())
    );
  }

  private getPermission(id: string): PermissionListItem {
    let listItem: PermissionListItem = this.permissions.get(id);
    if (!listItem) {
      listItem = {id, access: AuthService.defaultAccessRight};
    }
    return listItem;
  }

  private savePermissionMapFromProfile(profile: Profile): void {
    if (profile) {
      profile.modulePermissions.forEach((modulePermission: ModulePermission) => {
        if (modulePermission) {
          modulePermission.permissions.forEach((permission: Permission) => {
            const permissionListItem: PermissionListItem = {
              id: permission.webControl.frontendId,
              access: AuthService.getRights(permission, modulePermission)
            };
            this.permissions.set(permissionListItem.id, permissionListItem);
          });
        }
      });
    }
    this.saveToLocalStorage();
  }

  private saveToLocalStorage(): void {
    localStorage.setItem('profile', JSON.stringify(Array.from(this.permissions.values())));
  }

  private getProfileFromLocalStorage(): void {
    this.permissions = new Map<string, PermissionListItem>();
    const json = localStorage.getItem('profile');
    if (json) {
      const fromLS: PermissionListItem[] = JSON.parse(json);
      fromLS.forEach(item => this.permissions.set(item.id, item));
    }
  }
}

export interface AccessRights {
  isInvisible: boolean;
  isReadOnly: boolean;
  canWrite: boolean;
}

export interface PermissionListItem {
  id: string;
  access: AccessRights;
}

export interface PermissionType {
  normalKey: string;
  shortKey: string;
  accessObjectValue: string;
}

export interface DependsOnType {
  dependsOn: WebControlDependsOnEnum | string;
  check: (modulePermission: ModulePermission) => boolean;
}
