export interface Profile {
  id: number;
  name: string;
  modulePermissions: ModulePermission[];
}

export interface ModulePermission {
  id: number;
  view: boolean;
  create: boolean;
  edit: boolean;
  delete: boolean;

  module: Module;
  permissions: Permission[];
}

export interface Module {
  id: number;
  name: string;
  category: ModuleCategory;
}

export interface ModuleCategory {
  id: number;
  name: string;
}

export interface Permission {
  id: number;
  state: PermissionStateEnum | string;
  webControl: WebControl;
}

export interface WebControl {
  id: number;
  frontendId: string;
  name: string;
  type: PermissionTypeEnum;
  defaultState: PermissionStateEnum;
  dependsOn: WebControlDependsOnEnum;
  required: boolean;
}

export enum WebControlDependsOnEnum {
  VIEW,
  CREATE,
  EDIT,
  DELETE
}

export enum PermissionStateEnum {
  INVISIBLE,
  READ_ONLY,
  WRITE
}

export enum PermissionTypeEnum {
  FIELD,
  TOOL
}
