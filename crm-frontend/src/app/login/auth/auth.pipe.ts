import {OnDestroy, Pipe, PipeTransform} from '@angular/core';
import {AuthService} from './auth.service';
import {Subscription} from 'rxjs';

@Pipe({
  name: 'auth',
  pure: false
})
export class AuthPipe implements PipeTransform, OnDestroy {
  private readonly onAuthChangeSubscription: Subscription;

  private value: string;
  private type: string;
  private inverted: boolean;
  private returnValue: boolean;

  private result: boolean;

  constructor(private authService: AuthService) {
    if (!this.onAuthChangeSubscription) {
      this.onAuthChangeSubscription = this.authService.onAuthChange
        .subscribe(() => {
          if (this.value) {
            this.getResult();
          }
        });
    }
  }

  ngOnDestroy(): void {
    if (this.onAuthChangeSubscription) {
      this.onAuthChangeSubscription.unsubscribe();
    }
  }

  /**
   *
   * @param value web control key
   * @param type  type of access: write/invisible/readonly or shortcut w/i/r
   * @param inverted if true pipeline return !result
   * @param returnValue arg used to test pipeline. default is undefined
   *        if value is different than undefined then method return 'returnValue'
   */
  transform(value: string, type: string, inverted?: boolean, returnValue?: boolean): boolean {
    this.value = value;
    this.type = type;
    this.inverted = inverted;
    this.returnValue = returnValue;

    this.getResult();

    return this.result;
  }

  private getResult(): void {
    if (this.returnValue !== undefined) {
      console.error('do not forget delete returnValue arg for key ' + this.value);
      this.result = this.returnValue;
      return;
    }
    const access = this.authService.isPermissionPresent(this.value, this.type);
    this.result = (this.inverted) ? !access : access;
  }
}
