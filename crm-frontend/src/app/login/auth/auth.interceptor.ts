import {Injectable} from '@angular/core';

import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

import {Observable, throwError} from 'rxjs';
import {catchError, mergeMap} from 'rxjs/operators';
import {LoginService} from '../login.service';
import {environment} from '../../../environments/environment';
import {AuthService} from './auth.service';
import {UrlUtil} from '../../shared/url.util';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  private lastRefreshTimestamp = 0;

  constructor(private loginService: LoginService,
              private authService: AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.isOutsideRequest(req) && this.loginService.getToken() !== null && req.url !== `${UrlUtil.url}/auth`) {
      const handle = this.addAuthorizationHeader(req, next);
      return this.handleForbiddenError(handle);
    }
    return next.handle(req);
  }

  private addAuthorizationHeader(req: HttpRequest<any>, next: HttpHandler): any {
    if (this.shouldRefresh()) {
      this.loginService.refreshToken();
      this.lastRefreshTimestamp = Date.now() / 1000;
    }
    return next.handle(this.addToHeaders(req, this.loginService.getToken()));
  }

  private shouldRefresh() {
    return (Date.now() / 1000) - this.lastRefreshTimestamp > 60;
  }

  private addToHeaders(req: HttpRequest<any>, token: string): any {
    const request = req.clone({
      headers: req.headers.set('Authorization',
        'Bearer ' + token)
    });
    return request;
  }

  private isOutsideRequest(req: HttpRequest<any>): boolean {
    return req.url.startsWith(`${UrlUtil.url}`);
  }

  private handleForbiddenError(handle: Observable<HttpEvent<any>>): Observable<HttpEvent<any>> {
    return handle.pipe(catchError((err) => {
      if (err.status === 403) {
        this.authService.refreshPermission();
      }
      if (err.status === 402) {
        this.loginService.logout();
      }
      return throwError(err);
    }));
  }
}
