"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AuthService = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var auth_model_1 = require("./auth.model");
var url_util_1 = require("../../shared/url.util");
var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.permissions = new Map();
        this.types = [
            { normalKey: AuthService_1.WRITE_KEY, shortKey: 'w', accessObjectValue: 'canWrite' },
            { normalKey: AuthService_1.INVISIBLE_KEY, shortKey: 'i', accessObjectValue: 'isInvisible' },
            { normalKey: AuthService_1.READONLY_KEY, shortKey: 'r', accessObjectValue: 'isReadOnly' }
        ];
        this.onAuthChange = new rxjs_1.Subject();
    }
    AuthService_1 = AuthService;
    AuthService.getRights = function (permission, modulePermission) {
        if (!permission || permission.webControl === undefined || permission.state === undefined || permission.webControl.type === undefined) {
            return AuthService_1.defaultAccessRight;
        }
        var dependsOnItem = AuthService_1.dependsOnTypes.find(function (dependsOnType) {
            return auth_model_1.WebControlDependsOnEnum[dependsOnType.dependsOn] === permission.webControl.dependsOn;
        });
        if (!dependsOnItem || !dependsOnItem.check(modulePermission)) {
            return AuthService_1.defaultAccessRight;
        }
        return {
            isInvisible: permission.state === auth_model_1.PermissionStateEnum[auth_model_1.PermissionStateEnum.INVISIBLE],
            isReadOnly: permission.state === auth_model_1.PermissionStateEnum[auth_model_1.PermissionStateEnum.READ_ONLY],
            canWrite: permission.state === auth_model_1.PermissionStateEnum[auth_model_1.PermissionStateEnum.WRITE]
        };
    };
    AuthService.prototype.isPermissionPresent = function (id, type) {
        if (this.permissions.size === 0) {
            this.getProfileFromLocalStorage();
        }
        if (!type) {
            return false;
        }
        var permission = this.getPermission(id);
        var typeLowerCase = type.toLowerCase();
        var result = this.types.find(function (t) { return typeLowerCase === t.normalKey || typeLowerCase === t.shortKey; });
        if (result && permission) {
            return permission.access[result.accessObjectValue];
        }
        return false;
    };
    AuthService.prototype.refreshPermission = function () {
        var sub = this.getPermissions()
            .subscribe(undefined, undefined, function () {
            if (sub) {
                sub.unsubscribe();
            }
        });
    };
    AuthService.prototype.getPermissions = function () {
        var _this = this;
        return this.http.get(url_util_1.UrlUtil.url + "/crm-user-service/profile/mine").pipe(operators_1.tap(function (profile) { return _this.savePermissionMapFromProfile(profile); }), operators_1.tap(function () { return _this.onAuthChange.next(); }));
    };
    AuthService.prototype.getPermission = function (id) {
        var listItem = this.permissions.get(id);
        if (!listItem) {
            listItem = { id: id, access: AuthService_1.defaultAccessRight };
        }
        return listItem;
    };
    AuthService.prototype.savePermissionMapFromProfile = function (profile) {
        var _this = this;
        if (profile) {
            profile.modulePermissions.forEach(function (modulePermission) {
                if (modulePermission) {
                    modulePermission.permissions.forEach(function (permission) {
                        var permissionListItem = {
                            id: permission.webControl.frontendId,
                            access: AuthService_1.getRights(permission, modulePermission)
                        };
                        _this.permissions.set(permissionListItem.id, permissionListItem);
                    });
                }
            });
        }
        this.saveToLocalStorage();
    };
    AuthService.prototype.saveToLocalStorage = function () {
        localStorage.setItem('profile', JSON.stringify(Array.from(this.permissions.values())));
    };
    AuthService.prototype.getProfileFromLocalStorage = function () {
        var _this = this;
        this.permissions = new Map();
        var json = localStorage.getItem('profile');
        if (json) {
            var fromLS = JSON.parse(json);
            fromLS.forEach(function (item) { return _this.permissions.set(item.id, item); });
        }
    };
    var AuthService_1;
    AuthService.WRITE_KEY = 'write';
    AuthService.INVISIBLE_KEY = 'invisible';
    AuthService.READONLY_KEY = 'readonly';
    AuthService.dependsOnTypes = [
        { dependsOn: auth_model_1.WebControlDependsOnEnum.CREATE, check: function (modulePermission) { return modulePermission.view && modulePermission.create; } },
        { dependsOn: auth_model_1.WebControlDependsOnEnum.DELETE, check: function (modulePermission) { return modulePermission.view && modulePermission["delete"]; } },
        { dependsOn: auth_model_1.WebControlDependsOnEnum.VIEW, check: function (modulePermission) { return modulePermission.view; } },
        { dependsOn: auth_model_1.WebControlDependsOnEnum.EDIT, check: function (modulePermission) { return modulePermission.view && modulePermission.edit; } }
    ];
    AuthService.defaultAccessRight = { canWrite: false, isReadOnly: false, isInvisible: true };
    AuthService = AuthService_1 = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;
