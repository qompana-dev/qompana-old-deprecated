import * as jwtDecode from 'jwt-decode';
import * as moment from 'moment';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenDecoderService {

  constructor() { }

  getExpirationTimeAsTimestamp(token: string): number {
    return jwtDecode<DecodedToken>(token).exp;
  }

  getExpirationTimeAsMoment(token: string): moment.Moment {
    return moment.unix(jwtDecode<DecodedToken>(token).exp);
  }

  getAccountId(token: string): number {
    return jwtDecode<DecodedToken>(token).accountId;
  }
}

export class DecodedToken {
  sub: string;
  exp: number;
  accountId: number;
}
