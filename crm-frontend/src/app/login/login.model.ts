export enum LoginStatusEnum {
  LOGIN_SUCCESSFUL ,
  BAD_CREDENTIALS,
  ACCOUNT_LOCKED,
  LOGOUT_SUCCESSFUL,
  NONE,
  LOGOUT_BEFORE_TOKEN,
  PAYMENT_REQUIRED,
  TOO_MANY_USER_FOR_LICENSE,
  LICENSE_EXPIRED_DATE,
  LICENSE_NUMBER_OF_USERS
}

export class Credentials {
  username: string;
  password: string;
}

export class Token {
  value: string;
  expiredDate: string;
  numberOfUsers: string;
}
