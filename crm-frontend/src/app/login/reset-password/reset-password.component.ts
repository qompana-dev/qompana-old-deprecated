import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ResetPasswordService} from './reset-password.service';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {Person} from '../../person/person.model';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  constructor(private $router: Router,
              private resetPasswordService: ResetPasswordService,
              private notificationService: NotificationService,
              private translateService: TranslateService) {
  }

  person: Person;
  buttonText = '';

  emailForm = new FormGroup({
    email: new FormControl('', [Validators.email]),
  });

  ngOnInit(): void {
    this.person = Person.createEmptyPerson();
    this.translateService.get('passwordReset.confirm').subscribe(
      (translation) => this.buttonText = translation
    );
  }

  backToLogin(): void {
    this.$router.navigate(['/login']);
  }

  resetPassword(): void {
    if (this.emailForm.valid) {
      this.translateService.get('passwordReset.sending').subscribe(
        (translation) => {
          this.buttonText = translation;
        }
      );

      const sub: Subscription = this.resetPasswordService.resetPassword(this.person.email)
        .subscribe(
          () => this.notificationService.notify('passwordReset.notifications.sendForgetPasswordEmail', NotificationType.SUCCESS),
          () => this.handleError(),
          () => {
            this.$router.navigate(['/login']);
            sub.unsubscribe();
          });
    }
  }

  private handleError(): void {
    this.translateService.get('passwordReset.confirm').subscribe(
      (translation) => {
        this.buttonText = translation;
      }
    );
    this.notificationService.notify('passwordReset.notifications.failedSendingForgetPasswordEmail', NotificationType.ERROR);
  }
}
