import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import {UrlUtil} from '../../shared/url.util';


@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService {

  constructor(private http: HttpClient) {
  }

  resetPassword(email: string): Observable<any> {
    return this.http.get(`${UrlUtil.url}/crm-user-service/accounts/reset-password?email=${email}`);
  }
}
