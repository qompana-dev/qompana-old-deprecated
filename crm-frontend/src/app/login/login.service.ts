import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import * as moment from 'moment/moment.js';
import {of, Subject, Subscription} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {TokenDecoderService} from './auth/token-decoder.service';
import {AuthService} from './auth/auth.service';
import {PersonService} from '../person/person.service';
import {Account} from '../person/person.model';
import {LocaleService} from '../calendar/locale.service';
import {LOCKED} from 'http-status-codes';
import {UrlUtil} from '../shared/url.util';
import {LoginStatusEnum, Credentials, Token} from './login.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  loggedUserStatusChangeSubject: Subject<LoginStatusEnum> = new Subject();
  loggedUserChange: Subject<void> = new Subject();
  loggedUserAvatarChange: Subject<void> = new Subject();

  constructor(private http: HttpClient,
              private authService: AuthService,
              private personService: PersonService,
              private tokenDecoderService: TokenDecoderService,
              private $router: Router,
              private localeService: LocaleService) {
  }

  public getLoggedAccountEmail(): string {
    return localStorage.getItem('loggedAccountEmail');
  }

  public getLoggedAccountId(): number {
    return +localStorage.getItem('loggedAccountId');
  }

  public getLoggedAccountName(): string {
    return localStorage.getItem('navUserName');
  }

  public getLoggedAccountRole(): string {
    return localStorage.getItem('navUserRole');
  }

  public getLoggedAccountPhone(): string {
    return localStorage.getItem('navUserPhone');
  }

  public getLoggedAccountAvatar(): string {
    return localStorage.getItem('navUserAvatar');
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  public getExpiredDate(): string {
    return localStorage.getItem('expiredDate');
  }

  public getNumberOfUsers(): string {
    return localStorage.getItem('numberOfUsers');
  }

  login(credentials: Credentials): void {
    const subscription: Subscription = this.http.post(`${UrlUtil.url}/auth`, credentials).pipe()
      .subscribe(
      (token: Token) => this.handleLoginSuccess(token, subscription, true),
      (error: any) => this.handleLoginFailed(subscription, error)
    );
  }

  logout(): void {
    localStorage.removeItem('token');
    this.loggedUserStatusChangeSubject.next(LoginStatusEnum.LOGOUT_SUCCESSFUL);
  }

  logoutWithoutRedirect(): void {
    localStorage.removeItem('token');
    this.loggedUserStatusChangeSubject.next(LoginStatusEnum.LOGOUT_BEFORE_TOKEN);
  }

  getRefreshTokenSubscription(): any {
    const httpHeaders = new HttpHeaders({Authorization: 'Bearer ' + this.getToken()});
    return this.http.get(`${UrlUtil.url}/auth`, {
      headers: httpHeaders
    }).pipe(
      tap((token: Token) => this.handleLoginSuccess(token)),
      catchError(() => of(this.handleRefreshFailed()))
    );
  }

  refreshToken(): void {
    const httpHeaders = new HttpHeaders({Authorization: 'Bearer ' + this.getToken()});
    this.http.get(`${UrlUtil.url}/auth`, {
      headers: httpHeaders
    }).pipe(
      tap((token: Token) => this.handleLoginSuccess(token)),
      catchError(() => of(this.handleRefreshFailed()))
    ).subscribe(() => {
    });
  }

  private handleRefreshFailed(subscription?: Subscription): void {
    this.logout();
    if (subscription) {
      subscription.unsubscribe();
    }
  }

  isLoggedIn(): boolean {
    return localStorage.getItem('token-expires-at') !== null &&
      this.getToken() !== null && this.getToken() !== undefined && this.getToken() !== 'undefined' &&
      moment().isBefore(this.tokenDecoderService.getExpirationTimeAsMoment(this.getToken()).subtract(5, 'seconds'));
  }

  private handleLoginFailed(subscription: Subscription, error: any): void {
    if (error.status === 402) {
      this.loggedUserStatusChangeSubject.next(LoginStatusEnum.PAYMENT_REQUIRED);
    } else if (error.status === LOCKED) {
      this.loggedUserStatusChangeSubject.next(LoginStatusEnum.ACCOUNT_LOCKED);
    } else if (error.status === 480) {
      this.loggedUserStatusChangeSubject.next(LoginStatusEnum.TOO_MANY_USER_FOR_LICENSE);
    } else {
      this.loggedUserStatusChangeSubject.next(LoginStatusEnum.BAD_CREDENTIALS);
    }
    subscription.unsubscribe();
  }

  public handleLoginSuccess(token: Token, subscription?: Subscription, sendNotification?: boolean): void {
    localStorage.setItem('token', token.value);
    localStorage.setItem('expiredDate', token.expiredDate);
    localStorage.setItem('numberOfUsers', token.numberOfUsers);
    localStorage.setItem('token-expires-at', JSON.stringify(this.tokenDecoderService.getExpirationTimeAsTimestamp(token.value)));

    const accountId = this.tokenDecoderService.getAccountId(token.value);
    localStorage.setItem('loggedAccountId', JSON.stringify(accountId));

    const sub = this.personService.getMyAccount()
      .subscribe((account: Account) => {
        localStorage.setItem('navUserName', `${account.person.name} ${account.person.surname}`);
        localStorage.setItem('navUserPhone', `${account.person.phone}`);
        localStorage.setItem('navUserAvatar', `${account.person.avatar}`);
        localStorage.setItem('navUserRole', account.role.name);
        localStorage.setItem('loggedAccountEmail', account.person.email);
      }, () => {
        localStorage.setItem('navUserName', '');
        localStorage.setItem('navUserPhone', '');
        localStorage.setItem('navUserRole', '');
        localStorage.setItem('loggedAccountEmail', '');
        localStorage.setItem('navUserAvatar', '');
      }, () => {
        this.loggedUserChange.next();
        sub.unsubscribe();
      });

    const loginStatusSet = Array.from(new Set()
      .add(sendNotification ? LoginStatusEnum.LOGIN_SUCCESSFUL : LoginStatusEnum.NONE)
      .add(sendNotification && token.expiredDate != null ? LoginStatusEnum.LICENSE_EXPIRED_DATE : LoginStatusEnum.NONE)
      .add(sendNotification && token.numberOfUsers != null ? LoginStatusEnum.LICENSE_NUMBER_OF_USERS : LoginStatusEnum.NONE));

    for (const loginStatus of loginStatusSet) {
      this.loggedUserStatusChangeSubject.next(loginStatus);
    }

    if (subscription) {
      subscription.unsubscribe();
    }
    this.authService.refreshPermission();

    this.updateLocale();
  }

  updateLocale(): void {
    const accountId = +localStorage.getItem('loggedAccountId');

    if (accountId) {
      this.localeService.initAccountConfiguration(accountId);
    }
  }
}
