import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {LoginService} from './login.service';
import {NotificationService, NotificationType} from '../shared/notification.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginStatusEnum, Credentials} from './login.model';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  credentials: Credentials;
  subscription: Subscription;

  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(private loginService: LoginService,
              private translateService: TranslateService,
              private $router: Router,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.credentials = new Credentials();
    if (this.loginService.isLoggedIn()) {
      this.$router.navigate(['']);
    }
    this.subscription = this.loginService.loggedUserStatusChangeSubject.subscribe(
      (statusChanged: LoginStatusEnum) => this.sendNotificationBasedOnStatus(statusChanged)
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  login(): void {
    if (this.loginForm.valid) {
      this.loginService.login(this.credentials);
    }
  }

  private sendNotificationBasedOnStatus(statusChanged: LoginStatusEnum): void {
    switch (+statusChanged) {
      case LoginStatusEnum.LOGIN_SUCCESSFUL:
        // this.notificationService.notify('login.notification.loginSuccessful', NotificationType.SUCCESS);
        this.$router.navigate(['/home']);
        break;
      case LoginStatusEnum.BAD_CREDENTIALS:
        this.notificationService.notify('login.notification.loginFailed', NotificationType.ERROR);
        break;
      case LoginStatusEnum.PAYMENT_REQUIRED:
        this.notificationService.notify('login.notification.paymentRequired', NotificationType.ERROR);
        break;
      case LoginStatusEnum.ACCOUNT_LOCKED:
        this.notificationService.notify('login.notification.accountLocked', NotificationType.ERROR);
        break;
      case LoginStatusEnum.TOO_MANY_USER_FOR_LICENSE:
        this.notificationService.notify('login.notification.tooManyUserForLicense', NotificationType.ERROR);
        break;
      case LoginStatusEnum.LICENSE_EXPIRED_DATE:
        const expiredDate: number = +this.loginService.getExpiredDate();
        if (expiredDate === 0) {
          this.notificationService.notify('login.notification.licenseExpiredDateToday', NotificationType.ERROR);
        } else  if (expiredDate === 1) {
          this.notificationService.notify('login.notification.licenseExpiredDateTomorrow', NotificationType.WARNING);
        } else if (expiredDate > 0) {
          this.notificationService.notify('login.notification.licenseExpiredDate', NotificationType.WARNING, {count: expiredDate});
        } else {
          this.notificationService.notify('login.notification.licenseExpiredDateAfter', NotificationType.ERROR,
            {count: (expiredDate * -1)});
        }
        break;
      case LoginStatusEnum.LICENSE_NUMBER_OF_USERS:
        const numberOfUsers = this.loginService.getNumberOfUsers();
        if (numberOfUsers) {
          this.notificationService.notify('login.notification.licenseNumberOfUsers', NotificationType.WARNING, {count: numberOfUsers});
        }
        break;
    }
  }

  moveToPasswordReset(): void {
    this.$router.navigate(['/password-reset']);
  }
}
