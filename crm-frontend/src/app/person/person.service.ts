import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {
  Account,
  AccountConfiguration,
  AccountEmail,
  AccountInfoDto,
  AccountReportData,
  Passwords,
  PersonAvatar, PersonListDto
} from './person.model';
import {Observable, of, Subject} from 'rxjs';
import {Page, PageRequest} from '../shared/pagination/page.model';
import {CrmObject} from '../shared/model/search.model';
import {map, takeUntil} from 'rxjs/operators';
import {UrlUtil} from '../shared/url.util';
import {NotificationService, NotificationType} from "../shared/notification.service";

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor(private http: HttpClient) {
  }

  getAccount(accountId: number): Observable<Account> {
    return this.http.get<Account>(`${UrlUtil.url}/crm-user-service/accounts/${accountId}`);
  }

  getAccountsForSearch(input: string): Observable<CrmObject[]> {
    return this.http.get<CrmObject[]>(`${UrlUtil.url}/crm-user-service/accounts/search?term=${input}`);
  }

  getMyAccount(): Observable<Account> {
    return this.http.get<Account>(`${UrlUtil.url}/crm-user-service/accounts/me`);
  }

  addAccount(newAccount: Account): Observable<any> {
    return this.http.post(`${UrlUtil.url}/crm-user-service/accounts`, newAccount);
  }

  updateAccount(accountId: number, newAccount: Account): Observable<any> {
    return this.http.put(`${UrlUtil.url}/crm-user-service/accounts/${accountId}`, newAccount);
  }

  changeActivate(accountId: number, activated: boolean): Observable<any> {
    return this.http.put(`${UrlUtil.url}/crm-user-service/accounts/${accountId}/activated/${activated}`, {});
  }

  removeAccount(accountId: number): Observable<any> {
    return this.http.delete(`${UrlUtil.url}/crm-user-service/accounts/${accountId}`);
  }

  getAccounts(pageRequest: PageRequest): Observable<Page<PersonListDto>> {
    const params = new HttpParams({fromObject: pageRequest as any});
    const url = `${UrlUtil.url}/crm-user-service/accounts`;
    return this.http.get<Page<PersonListDto>>(url, {params});
  }

  getAccountEmails(): Observable<AccountEmail[]> {
    return this.http.get<AccountEmail[]>(`${UrlUtil.url}/crm-user-service/accounts/emails`);
  }
  getAccountsInfoByIds(ids: number[]): Observable<AccountInfoDto[]> {
    return this.http.get<AccountInfoDto[]>(`${UrlUtil.url}/crm-user-service/accounts/info/${ids}`);
  }
  getAccountInfoChildListForLoggedUserRole(): Observable<AccountInfoDto[]> {
    const url = `${UrlUtil.url}/crm-user-service/accounts/me/child-accounts`;
    return this.http.get<AccountInfoDto[]>(url);
  }
  getTeamEmailsById(id: number): Observable<AccountEmail[]> {
    return this.http.get<AccountEmail[]>(`${UrlUtil.url}/crm-user-service/accountEmails/${id}`);
  }

  getIdNameMap(): Observable<Map<number, string>> {
    return this.getAccountEmails().pipe(
      map(emails => new Map<number, string>(emails.map(i => [i.id, `${i.name} ${i.surname}`] as [number, string])))
    );
  }

  getAccountEmailsWithoutRoleAssigned(): Observable<AccountEmail[]> {
    return this.http.get<AccountEmail[]>(`${UrlUtil.url}/crm-user-service/accounts/emails?onlyWithoutRole=true`);
  }

  changePassword(accountId: number, passwords: Passwords): Observable<any> {
    return this.http.put(`${UrlUtil.url}/crm-user-service/accounts/${accountId}/password`, passwords);
  }

  setPassword(token: string, pass: string): Observable<any> {
    return this.http.post(`${UrlUtil.url}/crm-user-service/accounts/password/${token}`, {
      password: pass
    });
  }

  sendActivationLink(accountId: number): Observable<any> {
    return this.http.get(`${UrlUtil.url}/crm-user-service/accounts/${accountId}/resend-activation-link`);
  }

  generateReport(format: 'pdf' | 'html', accounts: Account[]): Observable<Blob> {
    const acceptHeader = format === 'html' ? 'text/fileHtml' : 'application/pdf';
    const httpHeaders: HttpHeaders = new HttpHeaders({Accept: acceptHeader});
    const usersData: AccountReportData[] = accounts.map(account => AccountReportData.fromAccount(account));
    const url = `${UrlUtil.url}/crm-report-service/users`;
    return this.http.post(url, usersData, {responseType: 'blob', headers: httpHeaders});
  }

  getAccountsByIds(usersIds: number[]): Observable<CrmObject[]> {
    if (!usersIds || usersIds.length === 0) {
      return of([]);
    }
    const params = new HttpParams().set('ids', usersIds.join(','));
    return this.http.get<CrmObject[]>(`${UrlUtil.url}/crm-user-service/accounts/crm-object`, {params});
  }

  getMyAvatar(): Observable<PersonAvatar> {
    return this.http.get<PersonAvatar>(`${UrlUtil.url}/crm-user-service/account/avatar`);
  }

  updateMyAvatar(avatar: string): Observable<void> {
    const personAvatar: PersonAvatar = {avatar};
    return this.http.put<void>(`${UrlUtil.url}/crm-user-service/account/avatar`, personAvatar);
  }

  getAccountConfiguration(accountId: number): Observable<AccountConfiguration> {
    return this.http.get<AccountConfiguration>(`${UrlUtil.url}/crm-user-service/accounts/${accountId}/configuration`);
  }

  updateAccountConfiguration(accountId: number, accountConfiguration: AccountConfiguration): Observable<void> {
    return this.http.put<void>(`${UrlUtil.url}/crm-user-service/accounts/${accountId}/configuration`, accountConfiguration);
  }

  getAndMapAccountEmails(componentDestroyed: Subject<void>): Observable<Map<number, string>> {
    return this.getAccountEmails()
      .pipe(
        takeUntil(componentDestroyed),
        map(
          (emails) =>
            new Map<number, string>(
              emails.map((i) => [i.id, `${i.name} ${i.surname}`] as [number, string])
            )
        )
      );
  }
}
