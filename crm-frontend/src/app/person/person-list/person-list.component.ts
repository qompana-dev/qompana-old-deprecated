import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Router} from '@angular/router';
import {PersonService} from '../person.service';
import {PersonListDto} from '../person.model';
import {AuthService} from '../../login/auth/auth.service';
import {map, startWith, takeUntil, throttleTime} from 'rxjs/operators';
import {MatDialog, MatTable, Sort} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {SimpleDialogComponent, SimpleDialogData} from '../../shared/dialog/simple-dialog.component';
import {HttpErrorResponse} from '@angular/common/http';
import {BAD_REQUEST, FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import {SliderComponent} from '../../shared/slider/slider.component';
import {Page, PageRequest} from '../../shared/pagination/page.model';
import {InfiniteScrollUtil} from '../../shared/pagination/infinite-scroll.util';
import {GlobalConfigService} from '../../global-config/global-config.service';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss'],
})
export class PersonListComponent implements OnInit, OnDestroy {

  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild('addAccountSlider') addAccountSlider: SliderComponent;
  @ViewChild('editAccountSlider') editAccountSlider: SliderComponent;
  readonly deleteUserError: ErrorTypes = {
    base: 'person.detail.notification.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'deleteOwnAccount'
      }, {
        code: FORBIDDEN,
        text: 'noPermission'
      }, {
        code: NOT_FOUND,
        text: 'personNotFound'
      }
    ]
  };
  filteredColumns$: Observable<string[]>;
  selectedAccountId: number;
  accountsPage: Page<PersonListDto>;
  private readonly columns: string[] = ['person.name', 'person.surname', 'role.name', 'role.profiles.name', 'person.description', 'person.phone',
    'person.email', 'activated', 'action'];
  private componentDestroyed: Subject<void> = new Subject();
  private accountId: number;
  private currentPage: number;
  private currentSort = 'id,asc';
  collapsed = true;

  constructor(private $router: Router,
              private authService: AuthService,
              private globalConfigService: GlobalConfigService,
              private personService: PersonService,
              private translateService: TranslateService,
              private notificationService: NotificationService,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.initAccounts();

    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );

    this.globalConfigService.updateAdministrationsList$
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.initAccounts());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  public editAccount(accountId: number): void {
    this.editAccountSlider.toggle();
    this.selectedAccountId = accountId;
  }

  public tryDeleteAccount(accountId: number): void {
    this.accountId = accountId;
    const configuration: SimpleDialogData = {
      title: 'person.detail.dialog.removeUser.title',
      description: 'person.detail.dialog.removeUser.description',
      buttons: [
        {text: 'person.detail.dialog.removeUser.buttonNo'},
        {
          text: 'person.detail.dialog.removeUser.buttonYes',
          color: 'primary',
          onClick: () => this.deleteAccount()
        }
      ]
    };
    this.dialog.open(SimpleDialogComponent, {data: configuration});
  }

  public handleAccountUpdate(): void {
    this.initAccounts();
    this.editAccountSlider.toggle();
  }

  public handleAddAccount(): void {
    this.initAccounts();
    this.addAccountSlider.toggle();
  }

  public onScroll(e: any): void {
    if (InfiniteScrollUtil.shouldGetMoreData(e) && this.accountsPage.numberOfElements < this.accountsPage.totalElements) {
      const nextPage = this.currentPage + 1;
      this.getMoreAccounts({page: '' + nextPage, size: InfiniteScrollUtil.ELEMENTS_ON_PAGE,  sort: this.currentSort});
    }
  }

  // public generateReport(format: 'pdf' | 'html'): void { // created for tests
  //   this.personService.generateReport(format, this.accountsPage.content).pipe(
  //     takeUntil(this.componentDestroyed)
  //   ).subscribe(
  //     blob => saveAs(blob, `users.${format}`),
  //     () => this.notifyUndefinedError()
  //   );
  // }

  public onSortChange($event: Sort): void {
    this.currentSort = $event.direction ? ($event.active + ',' + $event.direction) : 'id,asc';
    this.initAccounts();
  }

  private getMoreAccounts(pageRequest: PageRequest): void {
    this.personService.getAccounts(pageRequest)
      .pipe(
        takeUntil(this.componentDestroyed),
        throttleTime(500)
      ).subscribe(
      page => this.handleGetMoreAccountsSuccess(page),
      () => this.notificationService.notify('person.detail.notification.listError', NotificationType.ERROR)
    );
  }

  private handleGetMoreAccountsSuccess(page: any): void {
    this.currentPage++;
    this.accountsPage.content = this.accountsPage.content.concat(page.content);
    this.accountsPage.numberOfElements += page.content.length;
  }

  private initAccounts(pageRequest: PageRequest = {page: '0', size: InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: this.currentSort}): void {
    this.personService.getAccounts(pageRequest)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      page => this.handleInitSuccess(page),
      () => this.notificationService.notify('person.detail.notification.listError', NotificationType.ERROR)
    );
  }

  private deleteAccount(): void {
    this.personService.removeAccount(this.accountId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      () => {
        this.initAccounts();
        this.notifyRemoveSuccess();
      },
      (err) => this.notifyError(err)
    );
  }

  private handleInitSuccess(page: any): void {
    this.currentPage = 0;
    this.accountsPage = page;
  }

  private getFilteredColumns(): string[] {
    return this.columns
      .filter((item) => !this.authService.isPermissionPresent('PersonListComponent.' + item + 'Column', 'i') || item === 'action');
  }

  private notifyRemoveSuccess(): void {
    this.notificationService.notify('person.detail.notification.personRemoved', NotificationType.SUCCESS);
  }

  private notifyUndefinedError(): void {
    this.notificationService.notify('person.detail.notification.undefinedError', NotificationType.ERROR);
  }

  private notifyError(err: HttpErrorResponse): void {
    this.notificationService.notifyError(this.deleteUserError, err.status);
  }

}
