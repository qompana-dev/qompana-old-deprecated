import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PersonInfoComponent} from './person-detail/person-info/person-info.component';
import {SetPasswordComponent} from './set-password/set-password.component';
import {AuthGuard} from '../login/auth/auth-guard.service';

const routes: Routes = [
  {path: 'info', canActivate: [AuthGuard], component: PersonInfoComponent},
  {path: 'set-password/:token', component: SetPasswordComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonRoutingModule { }
