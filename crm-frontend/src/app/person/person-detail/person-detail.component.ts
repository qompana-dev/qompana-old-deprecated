import {Component, EventEmitter, Input, OnDestroy, Output, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as cloneDeep from 'lodash/cloneDeep';
import {Subject} from 'rxjs';
import {Account} from 'src/app/person/person.model';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {PersonService} from '../person.service';
import {PersonFormComponent} from '../person-form/person-form.component';
import {takeUntil} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {BAD_REQUEST, CONFLICT, FORBIDDEN, LOCKED, NOT_FOUND} from 'http-status-codes';
import {GlobalConfigService} from '../../global-config/global-config.service';

@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.scss']
})
export class PersonDetailComponent implements OnDestroy {

  @Output() accountUpdated: EventEmitter<void> = new EventEmitter();
  readonly errorTypes: ErrorTypes = {
    base: 'person.detail.notification.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'incorrectData'
      }, {
        code: CONFLICT,
        text: 'differentVersion'
      }, {
        code: FORBIDDEN,
        text: 'noPermission'
      }, {
        code: NOT_FOUND,
        text: 'personNotFound'
      }, {
        code: LOCKED,
        text: 'ownAccountActivationNotSupported'
      }, {
        code: 480,
        text: 'tooManyUserForLicense'
      }
    ]
  };
  account: Account;
  @ViewChild(PersonFormComponent) private personFormComponent: PersonFormComponent;
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private router: Router,
              private personService: PersonService,
              private globalConfigService: GlobalConfigService,
              private activatedRoute: ActivatedRoute,
              private notificationService: NotificationService) {
  }

  private _accountId: number;

  @Input() set accountId(accountId: number) {
    if (!!accountId) {
      this._accountId = accountId;
      this.loadAccount(accountId);
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submitForm(): void {
    if (!this.personFormComponent.isFormValid()) {
      this.personFormComponent.markAsTouched();
      return;
    }
    const updatedAccount = this.personFormComponent.getAccount();
    updatedAccount.version = this.account.version;
    this.personService.updateAccount(this._accountId, updatedAccount)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      () => {
        this.globalConfigService.updateAdministrationsList();
        this.loadAccount(this.account.id);
        this.emitAccountUpdated();
        this.notifySuccessUpdate();
      },
      (err) => this.notifyError(err)
    );
  }

  changeActivated(): void {
    this.personService.changeActivate(this._accountId, !this.account.isActivated)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(() => {
        this.globalConfigService.updateAdministrationsList();
        this.notifyChangeActivatedSuccess();
        this.loadAccount(this._accountId, false);
      },
      (err) => this.notifyError(err)
    );
  }

  sendActivationLink(): void {
    this.personService.sendActivationLink(this._accountId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      () => this.notifyActivationLinkSuccess(),
      () => this.notifyActivationLinkError(),
    );
  }

  loadAccount(accountId: number, override: boolean = true): void {
    this.personService.getAccount(accountId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe((account: Account) => {
        if (override) {
          this.account = cloneDeep(account);
        } else {
          this.account.version = account.version;
          this.account.isActivated = account.isActivated;
        }
      },
      (err) => {
        if (err.status === NOT_FOUND) {
          this.notifyAccountNotFound();
        } else {
          this.notifyError(err.status);
        }
      });
  }

  private notifyAccountNotFound(): void {
    this.notificationService.notify('person.detail.notification.personNotFound', NotificationType.ERROR);
  }

  private notifyActivationLinkError(): void {
    this.notificationService.notify('person.detail.notification.activationLinkSentError', NotificationType.ERROR);
  }

  private notifyActivationLinkSuccess(): void {
    this.notificationService.notify('person.detail.notification.activationLinkSent', NotificationType.SUCCESS);
  }

  private notifySuccessUpdate(): void {
    this.notificationService.notify('person.detail.notification.personUpdated', NotificationType.SUCCESS);
  }

  private notifyError(err: HttpErrorResponse): void {
    this.notificationService.notifyError(this.errorTypes, err.status);
  }

  private notifyChangeActivatedSuccess(): void {
    const msg = (!this.account.isActivated) ? 'personActive' : 'personInactive';
    this.notificationService.notify('person.detail.notification.' + msg, NotificationType.SUCCESS,
      {accountName: this.account.person.name + ' ' + this.account.person.surname});
  }

  private emitAccountUpdated(): void {
    this.accountUpdated.emit();
  }
}
