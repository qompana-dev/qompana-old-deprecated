import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {CalendarUtils} from '../../../../calendar/utils/calendar-utils';
import * as moment from 'moment-timezone';
import {TimesUtil} from '../../../../shared/times.util';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {PersonService} from '../../../person.service';
import {takeUntil} from 'rxjs/operators';
import {Subject, zip} from 'rxjs';
import {FormUtil} from '../../../../shared/form.util';
import {LocaleService} from '../../../../calendar/locale.service';
import {LoginService} from '../../../../login/login.service';

@Component({
  selector: 'app-person-general-info',
  templateUrl: './person-general-info.component.html',
  styleUrls: ['./person-general-info.component.scss']
})
export class PersonGeneralInfoComponent implements OnInit, OnDestroy {
  generalInfoForm: FormGroup;
  avatarForm: FormGroup;
  languages = CalendarUtils.languages;
  weekdays = moment.weekdays();
  hours = TimesUtil.getFullHoursList();
  // tslint:disable-next-line:variable-name
  private _accountId: number;
  private componentDestroyed: Subject<void> = new Subject();

  // @formatter:off
  get locale(): AbstractControl {return this.generalInfoForm.get('locale'); }
  get dateFormat(): AbstractControl {return this.generalInfoForm.get('dateFormat'); }
  get firstDayOfWeek(): AbstractControl {return this.generalInfoForm.get('firstDayOfWeek'); }
  get firstHourOfDay(): AbstractControl {return this.generalInfoForm.get('firstHourOfDay'); }
  get hourFormat(): AbstractControl {return this.generalInfoForm.get('hourFormat'); }
  get avatar(): AbstractControl {return this.avatarForm.get('avatar'); }
  // @formatter:on

  @Input() set accountId(accountId: number) {
    this._accountId = accountId;
    this.subscribeForAccountConfiguration();
    this.subscribeForAccountAvatar();
  }

  constructor(private personService: PersonService,
              private loginService: LoginService,
              private notificationService: NotificationService,
              private localeService: LocaleService) {
    moment.locale(localStorage.getItem('locale'));
    this.weekdays = moment.weekdays();
  }

  ngOnInit(): void {
    this.generalInfoForm = new FormGroup({
      locale: new FormControl('', Validators.required),
      dateFormat: new FormControl('', Validators.required),
      firstDayOfWeek: new FormControl('', Validators.required),
      firstHourOfDay: new FormControl('', Validators.required),
      hourFormat: new FormControl('', Validators.required),
    });
    this.avatarForm = new FormGroup({avatar: new FormControl('')});
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    if (this.generalInfoForm.valid) {
      const locale = this.generalInfoForm.get('locale');
      localStorage.setItem('locale', locale ? locale.value : 'pl');
      zip(
        this.personService.updateAccountConfiguration(this._accountId, this.generalInfoForm.value),
        this.personService.updateMyAvatar(this.avatar.value)
      ).pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => {
            this.localeService.initAccountConfiguration(this._accountId);
            this.notificationService.notify('person.detail.info.generalInfo.notifications.updateSuccess', NotificationType.SUCCESS);
            localStorage.setItem('navUserAvatar', this.avatar.value);
            this.loginService.loggedUserAvatarChange.next();
            window.location.reload();
          },
          () => this.notificationService.notify('person.detail.info.generalInfo.notifications.updateError', NotificationType.ERROR)
        );
    } else {
      FormUtil.setTouched(this.generalInfoForm);
    }
  }

  private subscribeForAccountAvatar(): void {
    this.personService.getMyAvatar().pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (avatar) => this.avatar.patchValue(avatar.avatar),
      () => this.notificationService.notify('person.detail.info.generalInfo.notifications.getError', NotificationType.ERROR)
    );
  }

  private subscribeForAccountConfiguration(): void {
    this.personService.getAccountConfiguration(this._accountId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (configuration) => this.generalInfoForm.patchValue(configuration),
      () => this.notificationService.notify('person.detail.info.generalInfo.notifications.getError', NotificationType.ERROR)
    );
  }
}
