import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {MailService} from '../../../../mail/mail.service';
import {FormUtil} from '../../../../shared/form.util';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {MailConfigurationDto} from '../../../../mail/mail.model';

@Component({
  selector: 'app-mail-connector',
  templateUrl: './mail-connector.component.html',
  styleUrls: ['./mail-connector.component.scss']
})
export class MailConnectorComponent implements OnInit, OnDestroy {

  @Input() slider: SliderComponent;
  // tslint:disable-next-line:variable-name
  _accountId: number;
  form: FormGroup;
  private componentDestroyed: Subject<void> = new Subject();

  @Input() set accountId(accountId: number) {
    if (accountId) {
      this._accountId = accountId;
      this.updateForm(accountId);
    }
  }

// @formatter:off
  get imapServerAddress(): AbstractControl { return this.form.get('imapServerAddress'); }
  get imapServerPort(): AbstractControl { return this.form.get('imapServerPort'); }
  get imapSecurityType(): AbstractControl { return this.form.get('imapSecurityType'); }
  get smtpServerAddress(): AbstractControl { return this.form.get('smtpServerAddress'); }
  get smtpServerPort(): AbstractControl { return this.form.get('smtpServerPort'); }
  get smtpSecurityType(): AbstractControl { return this.form.get('smtpSecurityType'); }
  get username(): AbstractControl { return this.form.get('username'); }
  get password(): AbstractControl { return this.form.get('password'); }
  // @formatter:on

  constructor(private fb: FormBuilder,
              private mailService: MailService,
              private notificationService: NotificationService) {
    this.createForm();
  }

  ngOnInit(): void {
    this.slider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.createForm()
    );
    this.slider.openEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        if (this._accountId) {
          this.updateForm(this._accountId);
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  closeSlider(): void {
    this.slider.close();
  }

  submit(): void {
    if (this.form.valid) {
      const mailConfigurationDto = this.form.value;
      this.mailService.initializeMail(mailConfigurationDto, this._accountId).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        () => this.handleInitSuccess(),
        () => this.handleInitFailure()
      );
    } else {
      FormUtil.setTouched(this.form);
    }
  }

  private createForm(): void {
    this.form = this.fb.group({
      imapServerAddress: [null, Validators.required],
      imapServerPort: [null, Validators.compose([Validators.required, Validators.min(0)])],
      imapSecurityType: ['SSL', Validators.required],
      smtpServerAddress: [null, Validators.required],
      smtpServerPort: [null, Validators.compose([Validators.required, Validators.min(0)])],
      smtpSecurityType: ['SSL', Validators.required],
      username: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  private handleInitSuccess(): void {
    this.notificationService.notify('person.detail.info.mail.form.notifications.initSuccess', NotificationType.SUCCESS);
    this.closeSlider();
  }

  private handleInitFailure(): void {
    this.notificationService.notify('person.detail.info.mail.form.notifications.initFailure', NotificationType.ERROR);
  }

  public updateForm(accountId: number): void {
    this.mailService.getInitData(accountId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (data: MailConfigurationDto) => this.form.patchValue(data)
    );
  }
}
