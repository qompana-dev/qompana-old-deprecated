import {Component, OnDestroy} from '@angular/core';
import {PersonService} from '../../../person.service';
import {TokenDecoderService} from '../../../../login/auth/token-decoder.service';
import {Passwords} from '../../../person.model';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {ConfigurationService} from '../../../../shared/configuration.service';
import {Router} from '@angular/router';
import {AuthService} from '../../../../login/auth/auth.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.scss']
})
export class PasswordChangeComponent implements OnDestroy {

  passwords = new Passwords();

  readonly errorTypes = {
    base: 'person.detail.info.password.notifications.',
    errors: [
      {
        code: 400,
        text: 'passwordPolicyNotFulfilled'
      }, {
        code: 404,
        text: 'accountNotFound'
      }, {
        code: 409,
        text: 'badOldPassword'
      }
    ],
    defaultText: 'unknownError'
  };
  private formValid = false;
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private personService: PersonService,
              private authService: AuthService,
              private tokenDecoderService: TokenDecoderService,
              private notificationService: NotificationService,
              private configurationService: ConfigurationService,
              private $router: Router) {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  savePassword(): void {
    if (this.formValid) {
      this.personService.changePassword(
        this.tokenDecoderService.getAccountId(localStorage.getItem('token')), this.passwords).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        () => this.notificationService.notify('person.detail.info.password.notifications.changedSuccessfully', NotificationType.SUCCESS),
        (err) => this.notificationService.notifyError(this.errorTypes, err.status)
      );
    }
  }

  formChanged(formValid: boolean): void {
    this.formValid = formValid;
  }
}
