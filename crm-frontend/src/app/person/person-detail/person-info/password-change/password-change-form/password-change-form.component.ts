import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Passwords} from '../../../../person.model';
import {ConfigurationService} from '../../../../../shared/configuration.service';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';

@Component({
  selector: 'app-password-change-form',
  templateUrl: './password-change-form.component.html',
  styleUrls: ['./password-change-form.component.scss']
})
export class PasswordChangeFormComponent implements OnInit, OnChanges {

  @Input() passwords: Passwords;
  @Input() oldPasswordEnabled: boolean;
  @Output() formChanged: EventEmitter<boolean> = new EventEmitter();
  passwordPolicyRegex: any = '(.*?)';
  passwordPolicyDescription = '';

  newPasswordFrom = new FormControl('', [Validators.required, Validators.pattern(this.passwordPolicyRegex)]);
  passwordForm = new FormGroup({
    oldPassword: new FormControl('', [Validators.required]),
    newPassword: this.newPasswordFrom,
    repeatNewPassword: new FormControl('', [Validators.required,
      (control: AbstractControl): ValidationErrors =>
        (control.value !== this.newPasswordFrom.value) ? {notEqual: true} : null
    ])
  });

  constructor(private configurationService: ConfigurationService) {
  }

  ngOnInit(): void {
    this.configurationService.getPasswordPolicyRegex()
      .subscribe((response) => {
        this.setPasswordPolicyRegex(response);
      });
    this.formChanged.emit(this.formValid);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.passwordForm.get('oldPassword').setValue(this.passwords.oldPassword);
    if (this.oldPasswordEnabled) {
      this.passwordForm.get('oldPassword').setValidators([Validators.required]);
    } else {
      this.passwordForm.get('oldPassword').setValidators([]);
    }
    this.passwordForm.get('newPassword').setValue(this.passwords.newPassword);
    this.passwordForm.get('repeatNewPassword').setValue(this.passwords.newPassword);
    this.newPasswordFrom.setValidators([Validators.required, Validators.pattern(this.passwordPolicyRegex)]);
  }

  updateModel(): void {
    this.passwords.oldPassword = this.passwordForm.get('oldPassword').value;
    this.passwords.newPassword = this.passwordForm.get('newPassword').value;
    this.formChanged.emit(this.formValid);
  }

  get formValid(): boolean {
    return this.passwordForm.valid;
  }

  private setPasswordPolicyRegex(response: any): void {
    if (response !== null) {
      this.passwordPolicyRegex = response.regex;
      this.passwordPolicyDescription = response.description;
    }
  }

}
