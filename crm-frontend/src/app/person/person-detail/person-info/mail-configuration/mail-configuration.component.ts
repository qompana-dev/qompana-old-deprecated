import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Account} from '../../../person.model';
import {takeUntil} from 'rxjs/operators';
import {NotificationService, NotificationType} from '../../../../shared/notification.service';
import {Subject} from 'rxjs';
import {PersonService} from '../../../person.service';
import {MailService} from '../../../../mail/mail.service';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {LoginService} from '../../../../login/login.service';
import {MailConnectorComponent} from './mail-connector/mail-connector.component';
import {MailConfigurationDto} from '../../../../mail/mail.model';

@Component({
  selector: 'app-mail-configuration',
  templateUrl: './mail-configuration.component.html',
  styleUrls: ['./mail-configuration.component.scss']
})
export class MailConfigurationComponent implements OnInit, OnDestroy {

  public account: Account;
  private componentDestroyed: Subject<void> = new Subject();

  private loggedUserAccountId: number;
  private removingMailConfiguration = false;

  editedConfigurationId: number;

  mailConfigurations: MailConfigurationDto[] = [];

  @ViewChild('connectToServerSlider') connectToServerSlider: SliderComponent;
  @ViewChild('mailConnectorComponent') mailConnectorComponent: MailConnectorComponent;

  constructor(private notificationService: NotificationService,
              private mailService: MailService,
              private loginService: LoginService,
              private personService: PersonService) {
  }

  ngOnInit() {
    this.loggedUserAccountId = this.getAccountId();
    this.getUserConfigurations();

    this.loginService.loggedUserAvatarChange
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.loggedUserAccountId = this.getAccountId();
        this.getUserConfigurations();
      });

    this.connectToServerSlider.closeEvent
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.editedConfigurationId = undefined);
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  editConfiguration(configurationId: number): void {
    this.editedConfigurationId = configurationId;
    this.connectToServerSlider.open();
  }

  removeMailConfiguration(configurationId: number): void {
    if (!this.removingMailConfiguration) {
      this.removingMailConfiguration = true;
      this.mailService.removeConfiguration(configurationId)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => {
            this.notificationService.notify('mail.configuration.notification.mailConfigurationRemovedSuccessfully', NotificationType.SUCCESS);
            this.getUserConfigurations();
          }, () => this.notificationService.notify('mail.configuration.notification.removingMailConfigurationError', NotificationType.ERROR),
          () => this.removingMailConfiguration = false);
    }
  }

  getUserConfigurations(): void {
    if (this.loggedUserAccountId != null) {
      this.mailService.getUserConfigurations(this.loggedUserAccountId)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((mailConfigurations: MailConfigurationDto[]) => {
          this.mailConfigurations = mailConfigurations;
        }, () => this.notificationService.notify('mail.configuration.notification.loadingUserConfigurationsError', NotificationType.ERROR));
    }
  }

  private getAccountId(): number {
    const loggedUserAccountIdString = localStorage.getItem('loggedAccountId');
    if (loggedUserAccountIdString) {
      return parseInt(localStorage.getItem('loggedAccountId'), 10);
    }
    this.notificationService.notify('mail.configuration.notification.loadingPersonError', NotificationType.ERROR);
    return undefined;
  }
}
