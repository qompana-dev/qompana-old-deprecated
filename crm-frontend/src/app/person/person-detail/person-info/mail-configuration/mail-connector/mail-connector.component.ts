import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SliderComponent} from '../../../../../shared/slider/slider.component';
import {takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {MailService} from '../../../../../mail/mail.service';
import {FormUtil} from '../../../../../shared/form.util';
import {ErrorTypes, NotificationService, NotificationType} from '../../../../../shared/notification.service';
import {MailConfigurationDto, SimpleMailConfigurationDto} from '../../../../../mail/mail.model';
import {CONFLICT} from 'http-status-codes';
import {HttpErrorResponse} from '@angular/common/http';
import {MatCheckboxChange} from '@angular/material';

@Component({
  selector: 'app-mail-connector',
  templateUrl: './mail-connector.component.html',
  styleUrls: ['./mail-connector.component.scss']
})
export class MailConnectorComponent implements OnInit, OnDestroy {

  @Input() slider: SliderComponent;
  @Output() updateList: EventEmitter<void> = new EventEmitter<void>();
  // tslint:disable-next-line:variable-name
  _id: number;
  form: FormGroup;
  private componentDestroyed: Subject<void> = new Subject();

  showHelp = false;
  autoConfigure = true;
  domainName = '';
  saving = false;

  @Input() set id(id: number) {
    this._id = id;
    if (id) {
      this.updateForm(id);
    } else {
      this.form.reset();
    }
  }


  readonly addConfigurationError: ErrorTypes = {
    base: 'person.detail.info.mail.form.notifications.',
    defaultText: 'initFailure',
    errors: [
      {
        code: CONFLICT,
        text: 'configurationExist'
      },
      {
        code: 423,
        text: 'tooManyConfigurationError'
      },
      {
        code: 424,
        text: 'unableToAutoconfigureEmail'
      }
    ]
  };

  readonly updateConfigurationError: ErrorTypes = {
    base: 'person.detail.info.mail.form.notifications.',
    defaultText: 'editFailure',
    errors: [
      {
        code: CONFLICT,
        text: 'configurationExist'
      }
    ]
  };

// @formatter:off
  get imapServerAddress(): AbstractControl { return this.form.get('imapServerAddress'); }
  get imapServerPort(): AbstractControl { return this.form.get('imapServerPort'); }
  get imapSecurityType(): AbstractControl { return this.form.get('imapSecurityType'); }
  get smtpServerAddress(): AbstractControl { return this.form.get('smtpServerAddress'); }
  get smtpServerPort(): AbstractControl { return this.form.get('smtpServerPort'); }
  get smtpSecurityType(): AbstractControl { return this.form.get('smtpSecurityType'); }
  get username(): AbstractControl { return this.form.get('username'); }
  get password(): AbstractControl { return this.form.get('password'); }
  // @formatter:on

  constructor(private fb: FormBuilder,
              private mailService: MailService,
              private notificationService: NotificationService) {
    this.createForm();
  }

  ngOnInit(): void {
    this.slider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.createForm()
    );
    this.slider.openEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        if (this._id) {
          this.updateForm(this._id);
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  autoConfigureCheckboxClicked(event: MatCheckboxChange) {
    this.autoConfigure = event.checked;

    if (this.autoConfigure) {
      this.disableManualConfigurationControls();
      this.showHelp = false;
    } else {
      this.enableManualConfigurationControls();
    }
  }

  closeSlider(): void {
    this.slider.close();
  }

  add(): void {
    this.submit(false);
  }

  edit(): void {
    this.submit(true);
  }

  submit(edit: boolean): void {
    if (this.saving) {
      return;
    }
    if (this.form.valid) {
      const mailConfigurationDto = this.form.value;
      const observable = (this.autoConfigure) ? this.getMailConfigurationObservable(edit, mailConfigurationDto) :
        this.getManualMailConfigurationObservable(edit, mailConfigurationDto);
      this.saving = true;
      observable.pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => this.handleInitSuccess(edit),
          (err) => this.handleInitFailure(edit, err)
        );
    } else {
      FormUtil.setTouched(this.form);
    }
  }

  updateDomain(username: string): void {
    if (!username || username.length == 0) {
      this.domainName = '';
    }
    const parts: string[] = username.split('@');
    if (parts.length !== 2) {
      this.domainName = '';
    } else {
      this.domainName = parts[parts.length - 1];
    }
  }

  private createForm(): void {
    this.form = this.fb.group({
      imapServerAddress: [null, Validators.required],
      imapServerPort: [null, Validators.compose([Validators.required, Validators.min(0)])],
      imapSecurityType: ['SSL', Validators.required],
      smtpServerAddress: [null, Validators.required],
      smtpServerPort: [null, Validators.compose([Validators.required, Validators.min(0)])],
      smtpSecurityType: ['SSL', Validators.required],
      username: [null, Validators.required],
      password: [null, Validators.required]
    });
    this.domainName = '';
    this.autoConfigure = true;
    this.showHelp = false;
    this.saving = false;
    this.disableManualConfigurationControls();
  }

  private handleInitSuccess(edit: boolean): void {
    this.saving = false;
    this.notificationService.notify('person.detail.info.mail.form.notifications.' + ((edit) ? 'editSuccess' : 'initSuccess'), NotificationType.SUCCESS);
    this.updateList.emit();
    this.closeSlider();
  }

  private handleInitFailure(edit: boolean, error: HttpErrorResponse): void {
    this.saving = false;
    if (edit) {
      this.notificationService.notifyError(this.updateConfigurationError, error.status);
    } else {
      this.notificationService.notifyError(this.addConfigurationError, error.status);
    }

    if (error.status !== 423 && error.status !== 409) {
      this.autoConfigure = false;
      this.showHelp = true;
      this.enableManualConfigurationControls();
    }
  }

  public updateForm(id: number): void {
    this.mailService.getInitData(id).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (data: MailConfigurationDto) => {
        this.form.patchValue(data);
        this.updateDomain(data.username);
      }
    );
  }

  private getMailConfigurationObservable(edit: boolean, mailConfigurationDto: MailConfigurationDto | SimpleMailConfigurationDto): Observable<MailConfigurationDto> {
    if (edit) {
      return this.mailService.editMailSimpleConfiguration(this._id, mailConfigurationDto);
    } else {
      return this.mailService.initializeSimpleMailConfiguration(mailConfigurationDto);
    }
  }

  private getManualMailConfigurationObservable(edit: boolean, mailConfigurationDto: MailConfigurationDto): Observable<MailConfigurationDto> {
    if (edit) {
      return this.mailService.editMail(this._id, mailConfigurationDto);
    } else {
      return this.mailService.initializeMail(mailConfigurationDto);
    }
  }

  private enableManualConfigurationControls() {
    this.imapServerAddress.enable();
    this.imapServerPort.enable();
    this.imapSecurityType.enable();
    this.smtpServerAddress.enable();
    this.smtpServerPort.enable();
    this.smtpSecurityType.enable();
  }

  private disableManualConfigurationControls() {
    this.imapServerAddress.disable();
    this.imapServerPort.disable();
    this.imapSecurityType.disable();
    this.smtpServerAddress.disable();
    this.smtpServerPort.disable();
    this.smtpSecurityType.disable();
  }
}
