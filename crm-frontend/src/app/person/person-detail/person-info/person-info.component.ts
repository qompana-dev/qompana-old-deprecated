import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {NotificationService, NotificationType} from 'src/app/shared/notification.service';
import {Account, Person} from '../../person.model';
import {PersonService} from '../../person.service';
import {CalendarService} from '../../../calendar/calendar.service';
import {takeUntil} from 'rxjs/operators';
import {LoginService} from '../../../login/login.service';
import {MailConnectorComponent} from './mail-configuration/mail-connector/mail-connector.component';
import {MailService} from '../../../mail/mail.service';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {MatTabGroup} from '@angular/material/tabs';
import {UrlUtil} from '../../../shared/url.util';

@Component({
  selector: 'app-person-info',
  templateUrl: './person-info.component.html',
  styleUrls: ['./person-info.component.scss']
})
export class PersonInfoComponent implements OnInit, OnDestroy, AfterViewInit {

  public account: Account;
  isConnectedToGoogleOrMicrosoft = true;
  private loggedUserAccountId: number;
  private componentDestroyed: Subject<void> = new Subject();

  @ViewChild('tabGroup') tabGroup: MatTabGroup;
  private removingCalendarConfiguration = false;
  private initTab: number;

  constructor(private personService: PersonService,
              private notificationService: NotificationService,
              private $router: Router,
              private mailService: MailService,
              private loginService: LoginService,
              private calendarService: CalendarService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.getAccountFromLocalStorage();
    this.subscribeForParams();

    this.loginService.loggedUserAvatarChange
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.getAccountFromLocalStorage());
  }

  ngAfterViewInit(): void {
    this.setInitTab();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private setInitTab(): void {
    if (this.initTab && this.tabGroup) {
      this.tabGroup.selectedIndex = this.initTab;
    }
  }

  redirectToGoogle(): void {
    document.location.href = `${UrlUtil.url}/crm-calendar-service/google/auth?accountId=${this.loggedUserAccountId}`;
  }

  redirectToMicrosoft(): void {
    document.location.href = `${UrlUtil.url}/crm-calendar-service/microsoft/auth?accountId=${this.loggedUserAccountId}`;
  }

  removeCalendarConfiguration(): void {
    if (this.isConnectedToGoogleOrMicrosoft && !this.removingCalendarConfiguration) {
      this.removingCalendarConfiguration = true;
      this.calendarService.removeConfiguration(this.loggedUserAccountId)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => {
            this.notificationService.notify('person.detail.info.mail.calendarConfigurationRemovedSuccessfully', NotificationType.SUCCESS);
            this.checkCalendarConnection();
          }, () => this.notificationService.notify('person.detail.info.mail.removingCalendarConfigurationError', NotificationType.ERROR),
          () => this.removingCalendarConfiguration = false);
    }
  }

  private getAccountFromLocalStorage(): void {
    const accountIdString = localStorage.getItem('loggedAccountId');
    if (!accountIdString) {
      this.notificationService.notify('person.detail.info.notFoundLoggedUser', NotificationType.ERROR);
    } else {
      this.loggedUserAccountId = parseInt(localStorage.getItem('loggedAccountId'), 10);
      this.checkCalendarConnection();
      this.getAccount();
    }
  }

  private checkCalendarConnection(): void {
    this.calendarService.isConnectedToGoogleOrMicrosoft(this.loggedUserAccountId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((connected) => this.isConnectedToGoogleOrMicrosoft = connected);
  }

  private getAccount(): void {
    this.personService.getMyAccount().pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe((account: Account) => {
      this.account = account;
      this.account = Person.copyAdditionalDataToObj(this.account);
    }, (err) => {
      this.notificationService.notify('person.detail.info.loadingPersonError', NotificationType.ERROR);
    });
  }

  private subscribeForParams(): void {
    this.activatedRoute.queryParams.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(params => {
        if (params.tab != null) {
          this.initTab = params.tab;
          this.setInitTab();
        }
        if (params.success != null) {
          if (params.success) {
            this.notificationService.notify('person.detail.info.mail.form.notifications.calendarSuccess', NotificationType.SUCCESS);
          } else {
            this.notificationService.notify('person.detail.info.mail.form.notifications.calendarFailure', NotificationType.ERROR);
          }
        }
      }
    );
  }
}
