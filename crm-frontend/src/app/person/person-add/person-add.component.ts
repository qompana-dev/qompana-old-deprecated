import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {PersonService} from '../person.service';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {PersonFormComponent} from '../person-form/person-form.component';
import {takeUntil} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {BAD_REQUEST} from 'http-status-codes';
import {GlobalConfigService} from '../../global-config/global-config.service';

@Component({
  selector: 'app-person-add',
  templateUrl: './person-add.component.html',
  styleUrls: ['./person-add.component.scss']
})
export class PersonAddComponent implements OnInit, OnDestroy {

  @Output()
  accountAdded: EventEmitter<void> = new EventEmitter();
  readonly errorTypes = {
    base: 'person.add.notification.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'undefinedError'
      },
      {
        code: 481,
        text: 'emailOccupied'
      }
    ]
  };
  @ViewChild(PersonFormComponent)
  private personFormComponent: PersonFormComponent;
  private componentDestroyed: Subject<void> = new Subject();


  constructor(private personService: PersonService,
              private globalConfigService: GlobalConfigService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  createAccount(): void {

    const newAccount2 = this.personFormComponent.getAccount();
    if (!this.personFormComponent.isFormValid()) {
      this.personFormComponent.markAsTouched();
      return;
    }
    const newAccount = this.personFormComponent.getAccount();
    this.personService.addAccount(newAccount)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(() => {
        this.globalConfigService.updateAdministrationsList();
        this.emitSuccess();
        this.notifyAddSuccess();
        this.resetForm();
      },
      (err) => this.notifyError(err)
    );
  }

  private notifyError(err: HttpErrorResponse): void {
    this.notificationService.notifyError(this.errorTypes, err.status);
  }

  private notifyAddSuccess(): void {
    this.notificationService.notify('person.add.notification.addedSuccessfully', NotificationType.SUCCESS);
  }

  private emitSuccess(): void {
    this.accountAdded.emit();
  }

  private resetForm(): void {
    this.personFormComponent.reset();
  }
}
