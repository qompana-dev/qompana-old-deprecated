import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PersonRoutingModule} from './person-routing.module';
import {PersonDetailComponent} from './person-detail/person-detail.component';
import {PersonFormComponent} from './person-form/person-form.component';
import {PersonInfoComponent} from './person-detail/person-info/person-info.component';
import {PersonListComponent} from './person-list/person-list.component';
import {PersonAddComponent} from './person-add/person-add.component';
import {PasswordChangeComponent} from './person-detail/person-info/password-change/password-change.component';
import {PasswordChangeFormComponent} from './person-detail/person-info/password-change/password-change-form/password-change-form.component';
import {SetPasswordComponent} from './set-password/set-password.component';
import {SharedModule} from '../shared/shared.module';
import {NgxMaskModule} from 'ngx-mask';
import {MailConnectorComponent} from './person-detail/person-info/mail-configuration/mail-connector/mail-connector.component';
import {MailModule} from '../mail/mail.module';
import {PersonGeneralInfoComponent} from './person-detail/person-info/person-general-info/person-general-info.component';
import {AvatarModule} from '../shared/avatar-form/avatar.module';
import { MailConfigurationComponent } from './person-detail/person-info/mail-configuration/mail-configuration.component';

@NgModule({
  declarations: [
    PersonDetailComponent,
    PersonFormComponent,
    PersonInfoComponent,
    PersonListComponent,
    PersonAddComponent,
    SetPasswordComponent,
    PasswordChangeFormComponent,
    PasswordChangeComponent,
    MailConnectorComponent,
    PersonGeneralInfoComponent,
    MailConfigurationComponent
  ],
  exports: [
    PersonListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxMaskModule,
    PersonRoutingModule,
    MailModule,
    AvatarModule
  ]
})
export class PersonModule { }
