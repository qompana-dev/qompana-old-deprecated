import {Person, PersonAdditionalData} from '../person.model';
import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {Account} from 'src/app/person/person.model';
import {RoleService} from '../../permissions/roles/role.service';
import {Role} from '../../permissions/role.model';
import {Observable, Subject} from 'rxjs';
import {AbstractControl, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../login/auth/auth.service';
import {applyPermission} from '../../shared/permissions-utils';
import {takeUntil} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {FormValidators} from '../../shared/form/form-validators';
import {GlobalConfigService} from '../../global-config/global-config.service';

@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.scss']
})
export class PersonFormComponent implements OnInit, OnChanges, OnDestroy {


  @Input() update: boolean;
  accountForm: FormGroup;
  additionalItems: FormArray;
  possibleRoles: Role[];
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private roleService: RoleService,
              private authService: AuthService,
              private globalConfigService: GlobalConfigService,
              private translateService: TranslateService,
              private fb: FormBuilder) {
    this.createAccountForm();
  }

  _account: Account;

  @Input() set account(account: Account) {
    this._account = account;
    this.refreshPossibleRoles();
    this.updateAccountForm(account);
  }

  get authKey(): string {
    return (this.update) ? 'PersonDetailComponent.' : 'PersonAddComponent.';
  }

  get id(): AbstractControl {
    return this.accountForm.get('id');
  }

  get roleId(): AbstractControl {
    return this.accountForm.get('roleId');
  }

  get personName(): AbstractControl {
    return this.accountForm.get('personName');
  }

  get personSurname(): AbstractControl {
    return this.accountForm.get('personSurname');
  }

  get personDescription(): AbstractControl {
    return this.accountForm.get('personDescription');
  }

  get personPhone(): AbstractControl {
    return this.accountForm.get('personPhone');
  }

  get personEmail(): AbstractControl {
    return this.accountForm.get('personEmail');
  }

  get additionalData(): AbstractControl {
    return this.accountForm.get('additionalData');
  }

  ngOnInit(): void {
    this.getRoles().subscribe(
      (roles) => this.possibleRoles = roles
    );

    this.globalConfigService.updateAdministrationsList$
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.refreshPossibleRoles());
  }

  ngOnChanges(): void {
    this.applyPermissionsToForm();
    this._account = Person.copyAdditionalDataToObj(this._account);
    if (this._account && !this._account.role) {
      this._account.role = new Role();
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  getRoles(): Observable<Role[]> {
    return this.roleService.getPossibleRolesAsFlatList().pipe(
      takeUntil(this.componentDestroyed)
    );
  }

  private createAdditionalDataItem(item: PersonAdditionalData): FormGroup {
    return this.fb.group({
      title: [item.title, [Validators.required, FormValidators.whitespace]],
      value: [item.value, [Validators.required, FormValidators.whitespace]]
    });
  }

  public addAdditionalField(item: PersonAdditionalData = {title: undefined, value: undefined}): void {
    this.additionalItems.push(this.createAdditionalDataItem(item));
  }

  public getAccount(): Account {
    this.refreshPossibleRoles();
    const newAccount = Account.createEmptyAccount();
    if (!!this.roleId.value) {
      newAccount.role = new Role();
      newAccount.role.id = +this.roleId.value;
    }
    newAccount.person.name = this.personName.value;
    newAccount.person.surname = this.personSurname.value;
    newAccount.person.description = this.personDescription.value;
    newAccount.person.phone = this.personPhone.value;
    newAccount.person.email = this.personEmail.value;
    newAccount.person.additionalData = this.getAdditionalData();
    return newAccount;
  }

  public isFormValid(): boolean {
    return this.accountForm.valid;
  }

  public reset(): void {
    this.accountForm.reset();
  }

  public markAsTouched(): void {
    (Object as any).values(this.accountForm.controls).forEach(control => {
      control.markAsTouched();
    });
  }

  removeAdditionalField($event: any, fieldId: number): void {
    $event.stopPropagation();
    this.additionalItems.removeAt(fieldId);
  }

  private getAdditionalData(): string {
    const additionalDataArray: PersonAdditionalData[] = [];
    for (const control of this.additionalData['controls']) {
      additionalDataArray.push(control.value);
    }
    return Person.additionalDataToString(additionalDataArray);
  }

  private createAccountForm(): void {
    this.accountForm = this.fb.group({
      id: [''],
      personName: ['', [Validators.required, FormValidators.whitespace, Validators.maxLength(200)]],
      personSurname: ['', [Validators.required, FormValidators.whitespace, Validators.maxLength(200)]],
      personDescription: ['', Validators.maxLength(500)],
      personPhone: ['', [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
      personEmail: ['', Validators.compose([
        Validators.required,
        Validators.email,
        Validators.maxLength(100)
      ])],
      additionalData: this.fb.array([]),
      roleId: ['', Validators.required]
    });
    this.additionalItems = this.additionalData as FormArray;
    this.accountForm.disable();
  }

  private updateAccountForm(account: Account): void {
    if (this.accountForm && account) {
      this.id.patchValue(account.id);
      this.roleId.patchValue(account.role && account.role.id);
      this.personName.patchValue(account.person && account.person.name);
      this.personSurname.patchValue(account.person && account.person.surname);
      this.personDescription.patchValue(account.person && account.person.description);
      this.personPhone.patchValue(account.person && account.person.phone);
      this.personEmail.patchValue(account.person && account.person.email);
      this.updateAdditionalData(account);
    }
  }

  private updateAdditionalData(account: Account): void {
    this.clearFormArray(this.additionalItems);
    if (account.person && account.person.additionalData) {
      const additionalDataObj = Person.additionalDataToObj(account.person.additionalData);
      for (const item of additionalDataObj) {
        this.addAdditionalField(item);
      }
    }
  }

  private applyPermissionsToForm(): void {
    if (this.accountForm) {
      this.accountForm.enable();
      applyPermission(this.id, !this.authService.isPermissionPresent(`${this.authKey}idField`, 'r'));
      applyPermission(this.personName, !this.authService.isPermissionPresent(`${this.authKey}nameField`, 'r'));
      applyPermission(this.personSurname, !this.authService.isPermissionPresent(`${this.authKey}surnameField`, 'r'));
      applyPermission(this.personDescription, !this.authService.isPermissionPresent(`${this.authKey}descriptionField`, 'r'));
      applyPermission(this.personPhone, !this.authService.isPermissionPresent(`${this.authKey}phoneField`, 'r'));
      applyPermission(this.personEmail, !(this.authService.isPermissionPresent(`${this.authKey}emailField`, 'r') || this.update));
      applyPermission(this.roleId, !this.authService.isPermissionPresent(`${this.authKey}roleField`, 'r'));
    }
  }

  private refreshPossibleRoles(): void {
    this.getRoles().subscribe(
      (roles) => {
        this.possibleRoles = roles;
        if (this._account) {
          if (this._account.role && this._account.role.id && !this.possibleRoles.find(e => e.id === this._account.role.id)) {
            this.possibleRoles = this.possibleRoles.concat(this._account.role);
          }
        }
      }
    );
  }

  private clearFormArray(form: FormArray): void {
    while (0 !== form.length) {
      form.removeAt(0);
    }
  }
}
