import { Themes } from './../shared/theme/theme.model';
import {Role} from '../permissions/role.model';

export class Person {
  id: number;
  name: string;
  surname: string;
  description: string;
  phone: string;
  email: string;
  password?: string;
  additionalData?: string;
  avatar?: string;
  additionalDataObj?: PersonAdditionalData[];

  static createEmptyPerson(): Person {
    const person = new Person();
    person.name = '';
    person.surname = '';
    person.description = '';
    person.phone = '';
    person.email = '';
    return person;
  }

  static copyAdditionalDataToString(account: Account): Account {
    if (account && account.person && account.person.additionalDataObj) {
      account.person.additionalData = JSON.stringify(account.person.additionalDataObj);
    }
    return account;
  }

  static copyAdditionalDataToObj(account: Account): Account {
    if (account && account.person && account.person.additionalData) {
      account.person.additionalDataObj = JSON.parse(account.person.additionalData);
    }
    return account;
  }

  static additionalDataToString(additionalData: PersonAdditionalData[]): string {
    return JSON.stringify(additionalData);
  }

  static additionalDataToObj(additionalData: string): PersonAdditionalData[] {
    return JSON.parse(additionalData);
  }
}

export interface PersonAdditionalData {
  title: string;
  value: string;
}

export class Account {
  id: number;
  person?: Person;
  isActivated: boolean;
  version: number;
  role?: Role;

  static createEmptyAccount(): Account {
    const account = new Account();
    account.isActivated = false;
    account.person = Person.createEmptyPerson();
    return account;
  }
}

export class AccountEmail {
  id: number;
  email: string;
  name?: string;
  surname?: string;
}
export class AccountInfoDto {
  id: number;
  email: string;
  name?: string;
  surname?: string;
  roleId: number;
  roleName: string;
  avatar?: string;

  checked?: boolean;
}

export class IdAndEmailDTO {
  id: number;
  personEmail?: string;

  public static toAccountEmail(dto: IdAndEmailDTO): AccountEmail {
    return {id: dto.id, email: dto.personEmail};
  }
}

export class Passwords {
  oldPassword: string;
  newPassword: string;
}

export class AccountReportData {
  name: string;
  surname: string;
  description: string;
  phone: string;
  email: string;
  isActivated: boolean;


  static fromAccount(account: Account): AccountReportData {
    return {
      name: account.person.name,
      surname: account.person.surname,
      description: account.person.description,
      phone: account.person.phone,
      email: account.person.email,
      isActivated: account.isActivated,
    };
  }
}

export interface AccountConfiguration {
  locale: string;
  theme: Themes;
  dateFormat: string;
  firstDayOfWeek: string;
  firstHourOfDay: string;
  hourFormat: string;
  menuOrder: string;
}

export interface PersonAvatar {
  avatar?: string;
}

export class PersonListDto {
  id: number;
  name: string;
  avatar: string;
  surname: string;
  description: string;
  phone: string;
  email: string;
  additionalData: string;
  isActivated: boolean;
  roleName: string;
  profileName: string;
}
