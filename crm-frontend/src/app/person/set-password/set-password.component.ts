import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Passwords} from '../person.model';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {PersonService} from '../person.service';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {LoginService} from '../../login/login.service';

@Component({
  selector: 'app-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.scss']
})
export class SetPasswordComponent implements OnInit, OnDestroy {
  @Input() passwords: Passwords;
  private formValid = false;
  private token;
  private paramSubscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute,
              private personService: PersonService,
              private notificationService: NotificationService,
              private loginService: LoginService,
              private $router: Router) {
    this.loginService.logoutWithoutRedirect();
  }

  ngOnInit(): void {
    this.passwords = new Passwords();
    this.paramSubscription = this.activatedRoute.params.subscribe(
      (param) => this.token = param.token
    );
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  onFormChanged(formValid: boolean): void {
    this.formValid = formValid;
  }

  savePassword(): void {
    if (this.formValid) {
      const subscription = this.personService.setPassword(this.token, this.passwords.newPassword)
        .subscribe(
          () => this.notificationService.notify('person.detail.info.password.notifications.setSuccessfully', NotificationType.SUCCESS),
          () => this.notificationService.notify('token.expiredOrDeleted', NotificationType.ERROR),
          () => subscription.unsubscribe()
        );
      this.$router.navigate(['/login']);
    }
  }
}
