import {NotificationPair, Task, TaskNotificationUnit} from '../task.model';
import {CalendarEvent, CalendarEventTimesChangedEvent} from 'angular-calendar';
import {EventColor} from 'calendar-utils';
// @ts-ignore
import moment from 'moment';

export class TaskUtils {

  static notifications: { key: string, value: NotificationPair }[] = [
    {key: 'calendar.event.notifications.zeroMinutes', value: new NotificationPair(0, TaskNotificationUnit.MINUTE)},
    {key: 'calendar.event.notifications.fiveMinutes', value: new NotificationPair(5, TaskNotificationUnit.MINUTE)},
    {key: 'calendar.event.notifications.tenMinutes', value: new NotificationPair(10, TaskNotificationUnit.MINUTE)},
    {key: 'calendar.event.notifications.fifteenMinutes', value: new NotificationPair(15, TaskNotificationUnit.MINUTE)},
    {key: 'calendar.event.notifications.thirtyMinutes', value: new NotificationPair(30, TaskNotificationUnit.MINUTE)},
    {key: 'calendar.event.notifications.oneHour', value: new NotificationPair(1, TaskNotificationUnit.HOUR)},
    {key: 'calendar.event.notifications.twoHours', value: new NotificationPair(2, TaskNotificationUnit.HOUR)},
    {key: 'calendar.event.notifications.threeHours', value: new NotificationPair(3, TaskNotificationUnit.HOUR)},
    {key: 'calendar.event.notifications.fourHours', value: new NotificationPair(4, TaskNotificationUnit.HOUR)},
    {key: 'calendar.event.notifications.eightHours', value: new NotificationPair(8, TaskNotificationUnit.HOUR)},
    {key: 'calendar.event.notifications.twelveHours', value: new NotificationPair(12, TaskNotificationUnit.HOUR)},
    {key: 'calendar.event.notifications.oneDay', value: new NotificationPair(1, TaskNotificationUnit.DAY)},
    {key: 'calendar.event.notifications.twoDays', value: new NotificationPair(2, TaskNotificationUnit.DAY)},
    {key: 'calendar.event.notifications.threeDays', value: new NotificationPair(3, TaskNotificationUnit.DAY)},
    {key: 'calendar.event.notifications.oneWeek', value: new NotificationPair(1, TaskNotificationUnit.WEEK)},
    {key: 'calendar.event.notifications.twoWeeks', value: new NotificationPair(2, TaskNotificationUnit.WEEK)},
  ];

  static getNotificationByIntervalAndUnit(interval: number, unit: TaskNotificationUnit): NotificationPair {
    for (const notification of this.notifications) {
      if (notification.value && notification.value.interval === interval && notification.value.unit === unit) {
        return notification.value;
      }
    }
    return null;
  }

  static convertTaskToEvent(task: Task, createdInTimezoneText: string): CalendarEvent {
    const isOwnerOfTask = task.owner === +localStorage.getItem('loggedAccountId');
    return {
      start: new Date(Date.parse(task.dateTimeFrom + 'Z')),
      end: new Date(Date.parse(task.dateTimeTo + 'Z')),
      title: task.subject + TaskUtils.getTimezoneInfo(task.timeZone, task.dateTimeFrom, createdInTimezoneText),
      allDay: task.allDayEvent,
      resizable: {
        beforeStart: isOwnerOfTask,
        afterEnd: isOwnerOfTask
      },
      draggable: isOwnerOfTask,
      meta: task,
      color: TaskUtils.getColor(isOwnerOfTask, task.timeZone),
      cssClass: isOwnerOfTask ? 'my-task' : 'not-my-task'
    };
  }

  public static getTimezoneInfoForActivities(timeZone: string, date: string, createdInTimezoneText: string): string {
    return `${createdInTimezoneText}:\n${timeZone} ` +
      `(${TaskUtils.getDateStringWithTimeZone(timeZone, date)})`;
  }

  private static getTimezoneInfo(timeZone: string, date: string, createdInTimezoneText: string): string {
    if (timeZone !== moment.tz.guess()) {
      return `<br/>${createdInTimezoneText}:<br/><b>${timeZone}</b> ` +
        `(${TaskUtils.getDateStringWithTimeZone(timeZone, date)})`;
    }
    return '';
  }

  private static getDateStringWithTimeZone(timeZone: string, date: string): string {
    return moment(date).locale(localStorage.getItem('locale')).tz(timeZone).format('DD MMM HH:mm');
  }

  private static getColor(isOwnerOfTask: boolean, timeZone: string): EventColor {
    if (timeZone !== moment.tz.guess()) {
      return {primary: '#FF8E00', secondary: (isOwnerOfTask) ? '#d1e8ff' : '#adffdd'};
    }
    return isOwnerOfTask ? {primary: '#1e90ff', secondary: '#d1e8ff'} : {primary: '#00a14b', secondary: '#adffdd'};
  }

  static appendChangesToTask(change: CalendarEventTimesChangedEvent): Task {
    const task = change.event.meta;

    if (change.newStart) {
      change.event.start = change.newStart;
      task.dateTimeFrom = change.newStart;
    }
    if (change.newEnd) {
      change.event.end = change.newEnd;
      task.dateTimeTo = change.newEnd;
    } else {
      const endTime = new Date(change.event.start);
      endTime.setHours(endTime.getHours() + 1);
      change.event.end = endTime;
      task.dateTimeTo = endTime;
    }

    if (change.allDay !== undefined) {
      change.event.allDay = change.allDay;
      task.allDayEvent = change.allDay;
    }
    return task;
  }
}
