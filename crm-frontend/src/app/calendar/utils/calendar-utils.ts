export class CalendarUtils {
  static languages: { key: string, value: string }[] = [
    {
      key: 'pl',
      value: 'languages.polish'
    },
    {
      key: 'en',
      value: 'languages.english'
    }
    // {
    //   key: 'de',
    //   value: 'languages.german'
    // },
    // {
    //   key: 'fr',
    //   value: 'languages.french'
    // },
    // {
    //   key: 'it',
    //   value: 'languages.italian'
    // },
    // {
    //   key: 'es',
    //   value: 'languages.spanish'
    // }
  ];

  static getTime(date: Date): string {
    const hours = date.getHours();
    const minutes = date.getMinutes();
    return this.appendLeadingZeroes(hours) + ':' + this.appendLeadingZeroes(minutes);
  }

  private static appendLeadingZeroes(n: number): string {
    if (n <= 9) {
      return '0' + n;
    } else {
      return '' + n;
    }
  }
}
