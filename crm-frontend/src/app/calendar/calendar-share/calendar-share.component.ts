import {Component, OnDestroy, OnInit} from '@angular/core';
import {map, takeUntil} from 'rxjs/operators';
import {KeyValue} from '@angular/common';
import {Subject} from 'rxjs';
import {PersonService} from '../../person/person.service';
import {CalendarShare, CalendarShareService} from './calendar-share.service';
import {zip} from 'rxjs/observable/zip';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import * as cloneDeep from 'lodash/cloneDeep';
import {SliderService} from '../../shared/slider/slider.service';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {FormUtil} from '../../shared/form.util';

@Component({
  selector: 'app-calendar-sharing',
  templateUrl: './calendar-share.component.html',
  styleUrls: ['./calendar-share.component.scss']
})
export class CalendarShareComponent implements OnInit, OnDestroy {

  private componentDestroyed: Subject<void> = new Subject();

  usersSeeingMyCalendar: KeyValue<string, number>[] = [];
  possibleNewUsers: KeyValue<string, number>[] = [];
  allUsers: KeyValue<string, number>[] = [];
  calendarShares: CalendarShare[] = [];

  /* MOCK */ shareTypes: any = [
    {id: 'user', translate: 'calendar.form.shareTypes.user'}
  ];

  userSelectForm: FormGroup = new FormGroup({
    user: new FormControl('', Validators.compose([
      Validators.required,
      (control: AbstractControl): ValidationErrors =>
        (this.allUsers.map(item => item.key).indexOf(control.value || '') === -1) ? {userNotExist: true} : null,
      (control: AbstractControl): ValidationErrors =>
        (this.usersSeeingMyCalendar.map(item => item.key).indexOf(control.value || '') !== -1) ? {currentlyShared: true} : null
    ]))
  });

  constructor(private personService: PersonService,
              private calendarShareService: CalendarShareService,
              private sliderService: SliderService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.subscribeForCalendarShares();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  saveCalendarShare(): void {
    FormUtil.setTouched(this.userSelectForm);
    FormUtil.updateValidity(this.userSelectForm);
    if (this.userSelectForm.valid) {
      const email = this.userSelectForm.get('user').value;
      const selected = this.allUsers.find((item) => item.key === email);
      if (selected) {
        this.calendarShareService.saveCalendarShare(new CalendarShare(+localStorage.getItem('loggedAccountId'),
          selected.value))
          .subscribe(
            (share) => this.handleSaveSuccess(share),
            () => this.notificationService.notify('calendar.notifications.shareFailed', NotificationType.ERROR));
      }
    }
  }

  deleteCalendarShare(sharedToUserId: number): void {
    this.calendarShareService.deleteCalendarShare(this.calendarShares.find((e) => e.sharedToUserId === sharedToUserId).id)
      .subscribe(() => this.handleDeletionSuccess(sharedToUserId));
  }

  private handleDeletionSuccess(sharedToUserId: number): void {
    this.possibleNewUsers.push({key: this.getUserEmailById(sharedToUserId), value: sharedToUserId});
    this.calendarShares = this.calendarShares.filter(e => e.sharedToUserId !== sharedToUserId);
    this.usersSeeingMyCalendar = this.usersSeeingMyCalendar.filter(e => e.value !== sharedToUserId);
    this.notificationService.notify('calendar.notifications.deleteShareSuccess', NotificationType.SUCCESS);
  }

  private handleSaveSuccess(share: CalendarShare): void {
    this.userSelectForm.get('user').patchValue('');
    this.notificationService.notify('calendar.notifications.shareSuccess', NotificationType.SUCCESS);
    this.calendarShares.push(share);
    this.possibleNewUsers = this.possibleNewUsers.filter((user) => user.value !== share.sharedToUserId);
    this.usersSeeingMyCalendar.push({key: this.getUserEmailById(share.sharedToUserId), value: share.sharedToUserId});
  }

  private subscribeForCalendarShares(): void {
    zip(
      this.calendarShareService.getCalendarSharesBySharingUserId(),
      this.personService.getAccountEmails()
        .pipe(map((accounts) => (accounts.map((account) => ({key: account.email, value: account.id})))))
    ).pipe(takeUntil(this.componentDestroyed))
      .subscribe(array => this.setAllInitValues(array));
  }

  private setAllInitValues(array: any): void {
    this.calendarShares = array[0];
    this.allUsers = array[1].filter((account: KeyValue<string, number>) => account.value !== +localStorage.getItem('loggedAccountId'));
    for (const share of this.calendarShares) {
      const userEmail = this.getUserEmailById(share.sharedToUserId);
      if (userEmail) {
        this.usersSeeingMyCalendar.push({key: userEmail, value: share.sharedToUserId});
      }
    }
    this.possibleNewUsers = cloneDeep(this.allUsers);
    this.possibleNewUsers = this.possibleNewUsers.filter(e => !this.usersSeeingMyCalendar.find(f => f.value === e.value));
  }

  private getUserEmailById(id: number): string {
    const foundUser = this.allUsers.find(user => user.value === id);
    return foundUser ? foundUser.key : undefined;
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
  }
}
