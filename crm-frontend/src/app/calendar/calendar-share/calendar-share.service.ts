import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UrlUtil} from '../../shared/url.util';

export class CalendarShare {
  id: number;
  sharingUserId: number;
  sharedToUserId: number;

  constructor(sharingUserId: number, sharedToUserId: number) {
    this.sharingUserId = sharingUserId;
    this.sharedToUserId = sharedToUserId;
  }
}

@Injectable({
  providedIn: 'root'
})
export class CalendarShareService {

  constructor(private http: HttpClient) { }

  getCalendarSharesBySharingUserId(): Observable<CalendarShare[]> {
    return this.http.get<CalendarShare[]>(`${UrlUtil.url}/crm-task-service/calendar-share` +
      `?sharingUserId=${localStorage.getItem('loggedAccountId')}`);
  }

  getCalendarSharesBySharedToUserId(sharedToUserId: number): Observable<CalendarShare[]> {
    return this.http.get<CalendarShare[]>(`${UrlUtil.url}/crm-task-service/calendar-share` +
      `?sharedToUserId=${sharedToUserId}`);
  }

  saveCalendarShare(calendarShare: CalendarShare): Observable<CalendarShare> {
    return this.http.post<CalendarShare>(`${UrlUtil.url}/crm-task-service/calendar-share`, calendarShare);
  }

  deleteCalendarShare(id: number): Observable<void> {
    return this.http.delete<void>(`${UrlUtil.url}/crm-task-service/calendar-share/${id}`);
  }
}
