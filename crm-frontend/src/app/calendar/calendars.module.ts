import {NgModule} from '@angular/core';
import {CommonModule, registerLocaleData} from '@angular/common';

import {CalendarsRoutingModule} from './calendars-routing.module';
import {SharedModule} from '../shared/shared.module';
import {CalendarMainComponent} from './calendar-main/calendar-main.component';
import {AllEventsComponent} from './dialog/all-events/all-events.component';
import {SingleEventComponent} from './dialog/single-event/single-event.component';
import localePl from '@angular/common/locales/pl';
import localeEn from '@angular/common/locales/en';
import localeDe from '@angular/common/locales/de';
import localeEs from '@angular/common/locales/es';
import localeIt from '@angular/common/locales/it';
import localeFr from '@angular/common/locales/fr';
import {CalendarDateFormatter, CalendarModule, CalendarMomentDateFormatter, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/moment';
import {AmazingTimePickerModule} from 'amazing-time-picker';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {CustomDateFormatter} from './calendar-main/custom-date-formatter';
import {CalendarMiniComponent} from './calendar-mini/calendar-mini.component';
import {CalendarShareComponent} from './calendar-share/calendar-share.component';
// @ts-ignore
import moment from 'moment';
import {MapViewModule} from '../map/map-view/map-view.module';
import {TaskSliderModule} from './task-slider/task-slider.module';

registerLocaleData(localePl);
registerLocaleData(localeEn);
registerLocaleData(localeDe);
registerLocaleData(localeEs);
registerLocaleData(localeIt);
registerLocaleData(localeFr);

moment.updateLocale('pl', {
  week: {
    dow: localStorage.getItem('firstDayOfWeek'),
    doy: 0
  }
});

moment.updateLocale('en', {
  week: {
    dow: localStorage.getItem('firstDayOfWeek'),
    doy: 0
  }
});

moment.updateLocale('de', {
  week: {
    dow: localStorage.getItem('firstDayOfWeek'),
    doy: 0
  }
});

moment.updateLocale('es', {
  week: {
    dow: localStorage.getItem('firstDayOfWeek'),
    doy: 0
  }
});

moment.updateLocale('it', {
  week: {
    dow: localStorage.getItem('firstDayOfWeek'),
    doy: 0
  }
});

moment.updateLocale('fr', {
  week: {
    dow: localStorage.getItem('firstDayOfWeek'),
    doy: 0
  }
});


export function momentAdapterFactory(): DateAdapter {
  return adapterFactory(moment);
}

@NgModule({
  declarations: [
    CalendarMainComponent,
    AllEventsComponent,
    SingleEventComponent,
    CalendarMiniComponent,
    CalendarShareComponent
  ],
  imports: [
    CommonModule,
    MapViewModule,
    CalendarsRoutingModule,
    SharedModule,
    AmazingTimePickerModule,
    CalendarModule.forRoot(
      {
        provide: DateAdapter,
        useFactory: momentAdapterFactory
      }, {
        dateFormatter: {
          provide: CalendarDateFormatter,
          useClass: CalendarMomentDateFormatter
        }
      }
    ),
    MatMomentDateModule,
    TaskSliderModule
  ],
  entryComponents: [AllEventsComponent, SingleEventComponent],
  providers: [
    {provide: CalendarDateFormatter, useClass: CustomDateFormatter}
  ]
})
export class CalendarsModule {
}
