import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {CalendarService} from '../../calendar.service';
// @ts-ignore
import moment from 'moment-timezone';
import {SingleEventComponent} from '../single-event/single-event.component';


@Component({
  selector: 'app-all-events',
  templateUrl: './all-events.component.html',
  styleUrls: ['./all-events.component.scss']
})
export class AllEventsComponent {

  constructor(public dialogRef: MatDialogRef<AllEventsComponent>,
              public dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  close(): void {
    this.dialogRef.close();
  }

  parseTime(start: Date, end: Date): string {
    return moment(start).format('LT') + ' - ' +  moment(end).format('LT');
  }

  eventClicked(event: any): void {
    const dialogRef = this.dialog.open(SingleEventComponent, {
      data: event.meta
    });
  }
}
