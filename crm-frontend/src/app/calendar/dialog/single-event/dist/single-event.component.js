"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
exports.SingleEventComponent = void 0;
var core_1 = require("@angular/core");
var material_1 = require("@angular/material");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var notification_service_1 = require("../../../shared/notification.service");
var search_model_1 = require("src/app/shared/model/search.model");
var SingleEventComponent = /** @class */ (function () {
    function SingleEventComponent($router, dialogRef, calendarService, assignToGroupService, previewService, notificationService, deleteDialogService, data, personService) {
        this.$router = $router;
        this.dialogRef = dialogRef;
        this.calendarService = calendarService;
        this.assignToGroupService = assignToGroupService;
        this.previewService = previewService;
        this.notificationService = notificationService;
        this.deleteDialogService = deleteDialogService;
        this.data = data;
        this.personService = personService;
        this.eventDeleted = new core_1.EventEmitter();
        this.componentDestroyed = new rxjs_1.Subject();
        this.associations = null;
        this.showMap = false;
        this.deleteDialogData = {
            title: 'calendar.dialog.remove.title',
            description: 'calendar.dialog.remove.description',
            noteFirst: 'calendar.dialog.remove.noteFirstPart',
            noteSecond: 'calendar.dialog.remove.noteSecondPart',
            confirmButton: 'calendar.dialog.remove.buttonYes',
            cancelButton: 'calendar.dialog.remove.buttonNo'
        };
        this.associationsFields = [
            search_model_1.CrmObjectType.contact,
            search_model_1.CrmObjectType.customer,
            search_model_1.CrmObjectType.lead,
            search_model_1.CrmObjectType.opportunity,
            search_model_1.CrmObjectType.file
        ];
    }
    SingleEventComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.calendarService.getEvent(this.data.id)
            .pipe(operators_1.takeUntil(this.componentDestroyed)).subscribe(function (task) {
            if (task.taskAssociations && task.taskAssociations.length) {
                _this.updateLabelsForTaskAssociation(task.taskAssociations);
            }
        });
    };
    SingleEventComponent.prototype.ngAfterViewInit = function () {
        this.setMarkerValues();
        this.getOwnerAndCreator();
    };
    SingleEventComponent.prototype.getOwnerAndCreator = function () {
        var _this = this;
        this.personService.getAccount(this.data.creator)
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (account) { return _this.creator = account.person; });
        this.personService.getAccount(this.data.owner)
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (account) { return _this.owner = account.person; });
    };
    SingleEventComponent.prototype.ngOnDestroy = function () {
        this.componentDestroyed.next();
        this.componentDestroyed.complete();
    };
    SingleEventComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    SingleEventComponent.prototype.openDeleteDialog = function (taskId) {
        var _this = this;
        this.deleteDialogData.numberToDelete = 1;
        this.deleteDialogData.onConfirmClick = function () { return _this.deleteEvent(taskId); };
        this.deleteDialogService.open(this.deleteDialogData);
    };
    SingleEventComponent.prototype.deleteEvent = function (taskId) {
        var _this = this;
        this.calendarService.deleteEvent(taskId).subscribe(function () { return _this.handleDeleteSuccess(taskId); }, function () { return _this.notificationService.notify('calendar.notifications.deleteFailed', notification_service_1.NotificationType.ERROR); });
    };
    SingleEventComponent.prototype.editEvent = function (taskId) {
        this.calendarService.taskEdited.emit(taskId);
        this.close();
    };
    SingleEventComponent.prototype.isItMyTask = function () {
        return this.data.owner === +localStorage.getItem('loggedAccountId');
    };
    SingleEventComponent.prototype.handleDeleteSuccess = function (taskId) {
        this.notificationService.notify('calendar.notifications.deleteSuccess', notification_service_1.NotificationType.SUCCESS);
        this.eventDeleted.emit(taskId);
        this.close();
    };
    SingleEventComponent.prototype.openMap = function () {
        var _this = this;
        this.mapSlider.open();
        setTimeout(function () { return _this.showMap = true; }, 500);
    };
    SingleEventComponent.prototype.setMarkerValues = function () {
        if (!(this.data && this.data.latitude && this.data.longitude)) {
            return;
        }
        var marker = { id: 'event', latitude: +this.data.latitude, longitude: +this.data.longitude };
        this.markers = [marker];
        this.mapCenter = [marker.longitude, marker.latitude];
    };
    SingleEventComponent.prototype.updateLabelsForTaskAssociation = function (taskAssociations) {
        var _this = this;
        this.assignToGroupService.updateLabelsForTaskAssociation(taskAssociations)
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (result) {
            _this.associations = result
                .filter(function (association) { return _this.associationsFields.includes(association.objectType); })
                .sort(function (a, b) {
                var indexA = _this.associationsFields.indexOf(a.objectType);
                var indexB = _this.associationsFields.indexOf(b.objectType);
                return indexA - indexB;
            });
        });
    };
    SingleEventComponent.prototype.handleClickRelations = function (asociation) {
        var objectType = asociation.objectType, objectId = asociation.objectId;
        switch (objectType) {
            case search_model_1.CrmObjectType.contact:
            case search_model_1.CrmObjectType.customer:
            case search_model_1.CrmObjectType.lead:
                this.$router.navigate([objectType + "s", objectId]);
                this.close();
                break;
            case search_model_1.CrmObjectType.opportunity:
                this.$router.navigate([objectType, objectId]);
                this.close();
                break;
            case search_model_1.CrmObjectType.file:
                this.previewService.showPreviewByFileId(objectId);
                break;
        }
    };
    __decorate([
        core_1.Output()
    ], SingleEventComponent.prototype, "eventDeleted");
    __decorate([
        core_1.ViewChild('mapSlider')
    ], SingleEventComponent.prototype, "mapSlider");
    SingleEventComponent = __decorate([
        core_1.Component({
            selector: 'app-single-event',
            templateUrl: './single-event.component.html',
            styleUrls: ['./single-event.component.scss']
        }),
        __param(7, core_1.Inject(material_1.MAT_DIALOG_DATA))
    ], SingleEventComponent);
    return SingleEventComponent;
}());
exports.SingleEventComponent = SingleEventComponent;
