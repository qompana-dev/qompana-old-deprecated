import {AfterViewInit, Component, EventEmitter, Inject, OnInit, OnDestroy, Output, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { Router } from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {CalendarService} from '../../calendar.service';
import {Task, TaskAssociation} from '../../task.model';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {PersonService} from '../../../person/person.service';
import {Account, Person} from '../../../person/person.model';
import {MapViewMarker} from '../../../map/map-view/map-view.model';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import { AssignToGroupService } from 'src/app/shared/assign-to-group/assign-to-group.service';
import { FilePreviewService } from 'src/app/files/file-preview.service';
import { CrmObjectType } from 'src/app/shared/model/search.model';

@Component({
  selector: 'app-single-event',
  templateUrl: './single-event.component.html',
  styleUrls: ['./single-event.component.scss']
})
export class SingleEventComponent implements OnInit, OnDestroy, AfterViewInit {

  @Output() eventDeleted = new EventEmitter<number>();
  private componentDestroyed: Subject<void> = new Subject();
  @ViewChild('mapSlider') mapSlider: SliderComponent;

  owner: Person;
  creator: Person;

  associations: TaskAssociation[] = null;

  markers: MapViewMarker[];
  mapCenter: number[];
  showMap = false;

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'calendar.dialog.remove.title',
    description: 'calendar.dialog.remove.description',
    noteFirst: 'calendar.dialog.remove.noteFirstPart',
    noteSecond: 'calendar.dialog.remove.noteSecondPart',
    confirmButton: 'calendar.dialog.remove.buttonYes',
    cancelButton: 'calendar.dialog.remove.buttonNo',
  };

  private readonly associationsFields: CrmObjectType[] = [
    CrmObjectType.contact,
    CrmObjectType.customer,
    CrmObjectType.lead,
    CrmObjectType.opportunity,
    CrmObjectType.file
  ];

  constructor(private $router: Router,
              public dialogRef: MatDialogRef<SingleEventComponent>,
              private calendarService: CalendarService,
              private assignToGroupService: AssignToGroupService,
              private previewService: FilePreviewService,
              private notificationService: NotificationService,
              private deleteDialogService: DeleteDialogService,
              @Inject(MAT_DIALOG_DATA) public data: Task,
              private personService: PersonService) {
  }

  ngOnInit(): void {
    this.calendarService.getEvent(this.data.id)
    .pipe(takeUntil(this.componentDestroyed)).subscribe((task: Task) => {
        if(task.taskAssociations && task.taskAssociations.length) {
          this.updateLabelsForTaskAssociation(task.taskAssociations)
        }
      });
  }

  ngAfterViewInit(): void {
    this.setMarkerValues();
    this.getOwnerAndCreator();
  }

  private getOwnerAndCreator(): void {
    this.personService.getAccount(this.data.creator)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (account: Account) => this.creator = account.person
      );
    this.personService.getAccount(this.data.owner)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (account: Account) => this.owner = account.person
      );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  close(): void {
    this.dialogRef.close();
  }

  openDeleteDialog(taskId: number): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteEvent(taskId);
    this.deleteDialogService.open(this.deleteDialogData);
  }

  deleteEvent(taskId: number): void {
    this.calendarService.deleteEvent(taskId).subscribe(
      () => this.handleDeleteSuccess(taskId),
      () => this.notificationService.notify('calendar.notifications.deleteFailed', NotificationType.ERROR)
    );
  }

  editEvent(taskId: number): void {
    this.calendarService.taskEdited.emit(taskId);
    this.close();
  }

  isItMyTask(): boolean {
    return this.data.owner === +localStorage.getItem('loggedAccountId');
  }

  private handleDeleteSuccess(taskId: number): void {
    this.notificationService.notify('calendar.notifications.deleteSuccess', NotificationType.SUCCESS);
    this.eventDeleted.emit(taskId);
    this.close();
  }

  openMap(): void {
    this.mapSlider.open();
    setTimeout(() => this.showMap = true, 500);
  }

  private setMarkerValues(): void {
    if (!(this.data && this.data.latitude && this.data.longitude)) {
      return;
    }
    const marker: MapViewMarker = {id: 'event', latitude: +this.data.latitude, longitude: +this.data.longitude};
    this.markers = [marker];
    this.mapCenter = [marker.longitude, marker.latitude];
  }

  private updateLabelsForTaskAssociation(taskAssociations: TaskAssociation[]): void {
    this.assignToGroupService.updateLabelsForTaskAssociation(taskAssociations)
    .pipe(takeUntil(this.componentDestroyed))
    .subscribe((result: TaskAssociation[]) => {
      this.associations = result
      .filter(association => this.associationsFields.includes(association.objectType))
      .sort((a, b) => {
        const indexA = this.associationsFields.indexOf(a.objectType)
        const indexB = this.associationsFields.indexOf(b.objectType)
        return indexA - indexB;
      })
    });
  }

  handleClickRelations(asociation: TaskAssociation): void {
    const {objectType, objectId} = asociation;
    switch (objectType) {
      case CrmObjectType.contact:
      case CrmObjectType.customer:
      case CrmObjectType.lead:
        this.$router.navigate([`${objectType}s`, objectId]);
        this.close();
        break;
      case CrmObjectType.opportunity:
        this.$router.navigate([objectType, objectId]);
        this.close();
        break;
      case CrmObjectType.file:
        this.previewService.showPreviewByFileId(objectId);
        break;
    }
  }
}
