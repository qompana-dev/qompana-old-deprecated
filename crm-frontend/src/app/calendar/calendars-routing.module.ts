import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CalendarMainComponent} from './calendar-main/calendar-main.component';
import {AuthGuard} from '../login/auth/auth-guard.service';

const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: CalendarMainComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarsRoutingModule {
}
