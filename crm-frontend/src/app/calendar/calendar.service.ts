import {EventEmitter, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {NearbyTask, NearbyTaskCheck, Task, TaskActivityType, TaskDto, TaskTimeCategoryEnum} from './task.model';
import {CrmObjectType} from '../shared/model/search.model';
import {Page, PageRequest} from '../shared/pagination/page.model';
import {getHeaderForAuthType} from '../shared/permissions-utils';
import {UrlUtil} from '../shared/url.util';


@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  taskEdited = new EventEmitter<number>();

  constructor(private http: HttpClient) {
  }

  isConnectedToGoogleOrMicrosoft(accountId: number): Observable<boolean> {
    return this.http.get<boolean>(`${UrlUtil.url}/crm-calendar-service/configuration-check/${accountId}`);
  }

  removeConfiguration(accountId: number): Observable<void> {
    return this.http.delete<void>(`${UrlUtil.url}/crm-calendar-service/configuration/${accountId}`);
  }

  getEventsByCreatorId(creatorId: number): Observable<Task[]> {
    return this.http.get<Task[]>(`${UrlUtil.url}/crm-task-service/tasks/creator/${creatorId}`);
  }

  getEventsByOwnerId(ownerId: number): Observable<Task[]> {
    return this.http.get<Task[]>(`${UrlUtil.url}/crm-task-service/tasks?owner=${ownerId}`);
  }

  getEventsByCrmObject(type: string, id: number | string): Observable<Task[]> {
    return this.http.get<Task[]>(`${UrlUtil.url}/crm-task-service/tasks/type/${type}/id/${id}`);
  }

  getUpcomingEventsByOwnerId(ownerId: number): Observable<Task[]> {
    return this.http.get<Task[]>(`${UrlUtil.url}/crm-task-service/tasks/upcoming?owner=${ownerId}`);
  }

  getEventsByRoleName(roleName: string): Observable<Task[]> {
    return this.http.get<Task[]>(`${UrlUtil.url}/crm-task-service/tasks/role/${roleName}`);
  }

  getPreviewActivitiesByAssociationIdAndType(id: number, type: CrmObjectType): Observable<TaskDto[]> {
    return this.http.get<TaskDto[]>(`${UrlUtil.url}/crm-task-service/tasks/activity/associated-with/type/${type}/id/${id}/preview`);
  }

  getActivitiesByAssociationIdAndType(id: number, type: CrmObjectType, pageRequest: PageRequest): Observable<Page<TaskDto>> {
    const params = new HttpParams({fromObject: pageRequest as any});
    const url = `${UrlUtil.url}/crm-task-service/tasks/activity/associated-with/type/${type}/id/${id}`;
    return this.http.get<Page<TaskDto>>(url, {params});
  }

  getActivitiesByAssociationIdAndTypeAndTimeCategory(id: number, type: CrmObjectType, timeCategory: TaskTimeCategoryEnum,
                                                     pageRequest: PageRequest): Observable<Page<TaskDto>> {
    const params = new HttpParams({fromObject: pageRequest as any});
    const url = `${UrlUtil.url}/crm-task-service/tasks/activity/associated-with/type/${type}/id/${id}/timeCategory/${timeCategory}`;
    return this.http.get<Page<TaskDto>>(url, {params});
  }

  getAllActivitiesByTypeAssociatedWithObject(id: number, type: CrmObjectType, activityType: TaskActivityType): Observable<TaskDto[]> {
    return this.http.get<TaskDto[]>(`${UrlUtil.url}/crm-task-service/tasks/activity/type/${activityType}/associated-with/type/${type}/id/${id}`);
  }

  getEvent(taskId: number): Observable<Task> {
    return this.http.get<Task>(`${UrlUtil.url}/crm-task-service/tasks/${taskId}`);
  }

  updateEvent(task: Task): Observable<any> {
    return this.http.put(`${UrlUtil.url}/crm-task-service/tasks/${task.id}?sync=true`, task);
  }

  deleteEvent(taskId: number): Observable<any> {
    return this.http.delete(`${UrlUtil.url}/crm-task-service/tasks/${taskId}?sync=true`);
  }

  addEvent(task: Task): Observable<Task> {
    return this.http.post<Task>(`${UrlUtil.url}/crm-task-service/tasks?sync=true`, task);
  }

  modifyEvent(task: Task): Observable<Task> {
    return this.http.put<Task>(`${UrlUtil.url}/crm-task-service/tasks/${task.id}?sync=true`, task);
  }

  getTaskForExpense(taskId: number, authType: string): Observable<TaskDto> {
    return this.http.get<TaskDto>(`${UrlUtil.url}/crm-task-service/tasks/for-expenses/${taskId}`, {headers: getHeaderForAuthType(authType)});
  }

  getNearbyEvents(checkTask: NearbyTaskCheck): Observable<NearbyTask[]> {
    return this.http.post<NearbyTask[]>(`${UrlUtil.url}/crm-task-service/tasks/nearby`, checkTask);
  }
}
