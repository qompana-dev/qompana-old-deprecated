import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {BAD_REQUEST} from 'http-status-codes';
import {SliderService} from '../../../shared/slider/slider.service';
import {CalendarService} from '../../calendar.service';
import {TaskFormComponent} from '../task-form/task-form.component';
import {takeUntil} from 'rxjs/operators';
import {EventInfo, Task, TaskActivityType, TaskAssociation} from '../../task.model';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.scss']
})
export class TaskAddComponent implements OnDestroy, OnChanges {

  @Input() slider: SliderComponent;
  @Input() activityType: TaskActivityType; // default undefined
  @ViewChild(TaskFormComponent) private taskFormComponent: TaskFormComponent;
  @Input() eventInfo: EventInfo;
  @Input() initAssociation: TaskAssociation;
  @Output() eventAdded: EventEmitter<number> = new EventEmitter();
  private componentDestroyed: Subject<void> = new Subject();

  readonly errorTypes: ErrorTypes = {
    base: 'calendar.notifications.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'validationError'
      }
    ]
  };
  saveInProgress = false;

  constructor(private sliderService: SliderService,
              private calendarService: CalendarService,
              private translateService: TranslateService,
              private notificationService: NotificationService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.slider) {
      const titleKey = (this.activityType) ? 'calendar.task.form.addActivityTitle' : 'calendar.task.form.addTitle';
      this.slider.title = this.translateService.instant(titleKey);
      this.taskFormComponent.activityType = this.activityType;
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    if (!this.taskFormComponent.isFormValid()) {
      this.taskFormComponent.markAsTouched();
      return;
    }
    if (!this.saveInProgress) {
      this.saveInProgress = true;
      const task = this.taskFormComponent.getTask();
      this.calendarService.addEvent(task)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (addedTask: Task) => this.handleSuccess(addedTask),
          (error) => {
            this.saveInProgress = false;
            this.notificationService.notifyError(this.errorTypes, error.status);
          }
        );
    }
  }

  private handleSuccess(task: Task): void {
    this.saveInProgress = false;
    this.notificationService.notify('calendar.notifications.addSuccess', NotificationType.SUCCESS);
    this.eventAdded.next(task.id);
    this.closeSlider();
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
  }
}
