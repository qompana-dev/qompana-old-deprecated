import {Component, EventEmitter, Input, OnDestroy, Output, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {SliderService} from '../../../shared/slider/slider.service';
import {CalendarService} from '../../calendar.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {takeUntil} from 'rxjs/operators';
import {Task, TaskActivityType, TaskAssociation} from '../../task.model';
import {TaskFormComponent} from '../task-form/task-form.component';
import {BAD_REQUEST, NOT_FOUND} from 'http-status-codes';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {TranslateService} from '@ngx-translate/core';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.scss']
})
export class TaskEditComponent implements OnDestroy {
  @Input() slider: SliderComponent;
  @Input() initAssociation: TaskAssociation;
  @ViewChild(TaskFormComponent) private taskFormComponent: TaskFormComponent;
  @Output() eventAdded: EventEmitter<number> = new EventEmitter();
  @Output() eventDeleted = new EventEmitter<number>();
  private componentDestroyed: Subject<void> = new Subject();
  removeTaskKey = 'calendar.form.removeTask';

  readonly errorTypes: ErrorTypes = {
    base: 'calendar.notifications.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'validationError'
      }, {
        code: NOT_FOUND,
        text: 'notFound'
      }
    ]
  };

  private readonly deleteDialogDataForTask: Partial<DeleteDialogData> = {
    title: 'calendar.dialog.remove.title',
    description: 'calendar.dialog.remove.description',
    noteFirst: 'calendar.dialog.remove.noteFirstPart',
    noteSecond: 'calendar.dialog.remove.noteSecondPart',
    confirmButton: 'calendar.dialog.remove.buttonYes',
    cancelButton: 'calendar.dialog.remove.buttonNo',
  };
  private readonly deleteDialogDataForActivity: Partial<DeleteDialogData> = {
    title: 'calendar.dialog.remove.titleForActivity',
    description: 'calendar.dialog.remove.descriptionForActivity',
    noteFirst: 'calendar.dialog.remove.noteFirstPart',
    noteSecond: 'calendar.dialog.remove.noteSecondPartForActivity',
    confirmButton: 'calendar.dialog.remove.buttonYes',
    cancelButton: 'calendar.dialog.remove.buttonNo',
  };

  deleteDialogData = this.deleteDialogDataForTask;

  constructor(private sliderService: SliderService,
              private calendarService: CalendarService,
              private deleteDialogService: DeleteDialogService,
              private translateService: TranslateService,
              private notificationService: NotificationService) {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    if (!this.taskFormComponent.isFormValid()) {
      this.taskFormComponent.markAsTouched();
      return;
    }
    const task = this.taskFormComponent.getTask();
    this.calendarService.modifyEvent(task)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => this.handleSuccess(task),
        (error) => this.notificationService.notifyError(this.errorTypes, error.status)
      );
  }

  activityTypeChanged(activityType: TaskActivityType): void {
    if (this.slider) {
      const titleKey = (activityType) ? 'calendar.task.form.editActivityTitle' : 'calendar.task.form.editTitle';
      this.slider.title = this.translateService.instant(titleKey);
    }

    this.removeTaskKey = (activityType) ? 'calendar.form.removeActivity' : 'calendar.form.removeTask';
    this.deleteDialogData = (activityType) ? this.deleteDialogDataForActivity : this.deleteDialogDataForTask;
  }

  tryRemoveTask(): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteEvent();
    this.deleteDialogService.open(this.deleteDialogData);
  }
  deleteEvent(): void {
    const task = this.taskFormComponent.getTask();
    if (task && task.id) {
      this.calendarService.deleteEvent(task.id).subscribe(
        () => this.handleDeleteSuccess(task.id),
        () => this.notificationService.notify('calendar.notifications.deleteFailed', NotificationType.ERROR)
      );
    }
  }

  private handleDeleteSuccess(taskId: number): void {
    this.notificationService.notify('calendar.notifications.deleteSuccess', NotificationType.SUCCESS);
    this.eventDeleted.emit(taskId);
    this.slider.close();
  }

  private handleSuccess(task: Task): void {
    this.notificationService.notify('calendar.notifications.editSuccess', NotificationType.SUCCESS);
    this.eventAdded.next(task.id);
    this.closeSlider();
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
  }
}
