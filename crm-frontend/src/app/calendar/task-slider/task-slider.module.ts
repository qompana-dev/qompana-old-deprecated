import {NgModule} from '@angular/core';
import {TaskAddComponent} from './task-add/task-add.component';
import {TaskEditComponent} from './task-edit/task-edit.component';
import {CommonModule} from '@angular/common';
import {OverlayModule} from '@angular/cdk/overlay';
import {MapViewModule} from '../../map/map-view/map-view.module';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {SharedModule} from '../../shared/shared.module';
import {CalendarDateFormatter} from 'angular-calendar';
import {CustomDateFormatter} from '../calendar-main/custom-date-formatter';
import {TaskFormComponent} from './task-form/task-form.component';
import {AvatarModule} from '../../shared/avatar-form/avatar.module';
import {HourFormatterComponent} from './task-form/hour-formatter/hour-formatter.component';
import {NearbyTasksComponent} from './task-form/nearby-tasks/nearby-tasks.component';
import {MenuMembersComponent} from './task-form/add-members/menu-members/menu-members.component';
import {AddMembersComponent} from './task-form/add-members/add-members.component';
import {MatListModule} from '@angular/material/list';
import {MatRadioModule} from '@angular/material/radio';
import {QuillModule} from 'ngx-quill';
import {TaskOwnerComponent} from './task-form/task-owner/task-owner.component';
import { MenuOwnersComponent } from './task-form/task-owner/menu-owners/menu-owners.component';

@NgModule({
  declarations: [
    TaskAddComponent,
    TaskFormComponent,
    TaskEditComponent,
    HourFormatterComponent,
    NearbyTasksComponent,
    AddMembersComponent,
    MenuMembersComponent,
    TaskOwnerComponent,
    MenuOwnersComponent
  ],
  imports: [
    CommonModule,
    OverlayModule,
    MapViewModule,
    AvatarModule,
    SharedModule,
    MatMomentDateModule,
    MatListModule,
    MatRadioModule,
    QuillModule
  ],
  providers: [
    {provide: CalendarDateFormatter, useClass: CustomDateFormatter}
  ],
  exports: [
    TaskAddComponent,
    TaskFormComponent,
    TaskEditComponent,
  ]
})
export class TaskSliderModule {
}
