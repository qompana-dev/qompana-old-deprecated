import {Component, Input} from '@angular/core';
// @ts-ignore
import moment from 'moment';
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-hour-formatter',
  templateUrl: './hour-formatter.component.html',
  styleUrls: ['./hour-formatter.component.scss']
})
export class HourFormatterComponent {
  hourFormatterData: HourFormatterData;
  textToShow = '';
  secondPartToShow = '';

  @Input()
  set hour(hour: HourFormatterData) {
    this.hourFormatterData = cloneDeep(hour);
    this.makeTextToShow(this.hourFormatterData);
  }

  constructor() {
  }

  private makeTextToShow(hour: HourFormatterData): void {
    if (!hour.hour || hour.hour.length !== 5) {
      console.error('incorrect hour', hour.hour.length);
      return;
    }
    let hourToShow = (hour.britishTimeFormat) ? moment(hour.hour, 'HH:mm').format('hh:mm A') : hour.hour;
    this.textToShow = this.addSpaceBetween(hourToShow.substring(0, 5));

    if (hour.britishTimeFormat) {
      this.secondPartToShow = hourToShow.substring(6, 8);
    }
  }

  private addSpaceBetween(text: string): string{
    let result = '';
    for (let i = 0; i < text.length; i++) {
      if (i !== 0) {
        result += ' ';
      }
      result += text.charAt(i);
    }
    return result;
  }

}

export interface HourFormatterData {
  britishTimeFormat: boolean;
  hour: string;
}
