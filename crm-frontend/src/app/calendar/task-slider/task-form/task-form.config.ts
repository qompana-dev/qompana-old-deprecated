import { TaskActivityType } from "../../task.model";

export enum ACTIVITIES_ICON {
  EVENT = "event",
  PHONE = "phone",
  TASK = "task",
  MEETING = "meeting",
  MAIL = "mail",
}

export const ACTIVITY_TYPES = [
  {
    icon: ACTIVITIES_ICON.TASK,
    type: TaskActivityType.TASK,
  },
  {
    icon: ACTIVITIES_ICON.MEETING,
    type: TaskActivityType.MEETING,
  },
  {
    icon: ACTIVITIES_ICON.EVENT,
    type: null,
  },
  {
    icon: ACTIVITIES_ICON.PHONE,
    type: TaskActivityType.PHONE,
  },
  {
    icon: ACTIVITIES_ICON.MAIL,
    type: TaskActivityType.MAIL,
  },
];
