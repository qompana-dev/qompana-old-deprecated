"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.TaskFormComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var material_moment_adapter_1 = require("@angular/material-moment-adapter");
var material_1 = require("@angular/material");
var task_model_1 = require("../../task.model");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var times_util_1 = require("../../../shared/times.util");
var task_utils_1 = require("../../utils/task-utils");
// @ts-ignore
var moment_1 = require("moment");
var cloneDeep = require("lodash/cloneDeep");
var times_config_1 = require("../../../shared/config/times.config");
var TaskFormComponent = /** @class */ (function () {
    function TaskFormComponent(assignToGroupService, formBuilder, adapter, configService, localeService, sliderService, notificationService, calendarService, personService, mapService) {
        var _this = this;
        this.assignToGroupService = assignToGroupService;
        this.formBuilder = formBuilder;
        this.adapter = adapter;
        this.configService = configService;
        this.localeService = localeService;
        this.sliderService = sliderService;
        this.notificationService = notificationService;
        this.calendarService = calendarService;
        this.personService = personService;
        this.mapService = mapService;
        this.componentDestroyed = new rxjs_1.Subject();
        this.taskState = task_model_1.TaskState;
        this.taskPriority = task_model_1.TaskPriority;
        this.notifications = task_utils_1.TaskUtils.notifications;
        this.allUsers = [];
        this.assignees = [];
        this.taskHourList = [];
        this.searchAssigneControl = new forms_1.FormControl();
        this.mapCenter = undefined;
        this.showMap = false;
        this.foundPlaces = [];
        this.showAppAssignToGroup = false;
        this.taskActivityType = task_model_1.TaskActivityType;
        this.moment = moment_1["default"];
        this.britishTimeFormat = false;
        this.associations = [];
        this.nearbyTasks = [];
        this.nearbyTaskLoading = false;
        this.timeDifferenceInD = 3;
        this.activityTypeChanged = new core_1.EventEmitter();
        this.validateDates = function (control) {
            if (_this.taskForm && _this.startDate && _this.endDate) {
                var startDate = new Date(_this.startDate.value);
                var endDate = new Date(_this.endDate.value);
                if (!_this.allDayEvent.value) {
                    if (_this.endTime.value && _this.startTime.value) {
                        startDate = _this.setTime(_this.startTime.value, startDate);
                        endDate = _this.setTime(_this.endTime.value, endDate);
                    }
                    else {
                        return { noTimes: true };
                    }
                }
                return startDate > endDate ? { startDayNotBefore: true } : null;
            }
        };
        this.britishTimeFormat = localStorage.getItem('hourFormat') === '12';
        this.taskHourList = times_util_1.TimesUtil.getHoursList();
        this.createCustomerForm();
        this.subscribeForSliderClose();
        this.subscribeForEventEdition();
        this.subscribeForLocaleChanges();
        this.subscribeForAssignableAccounts();
    }
    Object.defineProperty(TaskFormComponent.prototype, "initAssociation", {
        set: function (initAssociation) {
            this.storedInitAssociation = initAssociation;
            if (initAssociation) {
                this.updateLabelsForTaskAssociation([initAssociation]);
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "activityType", {
        set: function (selectedActivityType) {
            this.selectedActivityType = selectedActivityType;
            this.activityTypeChanged.next(this.selectedActivityType);
            this.setDefaultValuesForNewTask(new Date());
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "eventInfo", {
        set: function (info) {
            if (info) {
                this.startingDate = info.startDate;
                this.allDayView = info.allDayEvent;
                this.setDefaultValuesForNewTask(info.startDate);
            }
        },
        enumerable: false,
        configurable: true
    });
    TaskFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.configService.getGlobalParamValue('timeDifferenceInD')
            .pipe(operators_1.takeUntil(this.componentDestroyed)).subscribe(function (param) { return _this.timeDifferenceInD = +param.value; });
        this.filteredAssignees = this.searchAssigneControl.valueChanges
            .pipe(operators_1.startWith(''), operators_1.map(function (value) { return value ? _this.filterAssignees(value) : _this.assignees; }));
    };
    TaskFormComponent.prototype.resetSearchAssigneeControl = function (event) {
        if (event === void 0) { event = false; }
        !event && this.searchAssigneControl.reset();
    };
    TaskFormComponent.prototype.filterAssignees = function (value) {
        var filterValue = value.toLowerCase();
        return this.assignees.filter(function (assignee) { return assignee.key.toLowerCase().indexOf(filterValue) === 0; });
    };
    TaskFormComponent.prototype.ngOnDestroy = function () {
        this.componentDestroyed.next();
        this.componentDestroyed.complete();
    };
    Object.defineProperty(TaskFormComponent.prototype, "title", {
        // @formatter:off
        get: function () {
            return this.taskForm.get('title');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "allDayEvent", {
        get: function () {
            return this.taskForm.get('allDayEvent');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "startDate", {
        get: function () {
            return this.taskForm.get('startDate');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "startTime", {
        get: function () {
            return this.taskForm.get('startTime');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "endDate", {
        get: function () {
            return this.taskForm.get('endDate');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "endTime", {
        get: function () {
            return this.taskForm.get('endTime');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "assignee", {
        get: function () {
            return this.taskForm.get('assignee');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "priority", {
        get: function () {
            return this.taskForm.get('priority');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "place", {
        get: function () {
            return this.taskForm.get('place');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "state", {
        get: function () {
            return this.taskForm.get('state');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "description", {
        get: function () {
            return this.taskForm.get('description');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "notification", {
        get: function () {
            return this.taskForm.get('notification');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "emailNotification", {
        get: function () {
            return this.taskForm.get('emailNotification');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TaskFormComponent.prototype, "enableNotification", {
        get: function () {
            return this.taskForm.get('enableNotification');
        },
        enumerable: false,
        configurable: true
    });
    // @formatter:on
    TaskFormComponent.prototype.isFormValid = function () {
        return this.taskForm.valid;
    };
    TaskFormComponent.prototype.markAsTouched = function () {
        Object.values(this.taskForm.controls).forEach(function (control) {
            control.markAsTouched();
        });
    };
    TaskFormComponent.prototype.createCustomerForm = function () {
        var _this = this;
        this.taskForm = this.formBuilder.group({
            title: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(100)])],
            allDayEvent: [''],
            startDate: ['', forms_1.Validators.required],
            startTime: [''],
            endDate: ['', forms_1.Validators.required],
            endTime: [''],
            priority: [''],
            assignee: ['', [forms_1.Validators.required]],
            place: [''],
            state: [''],
            description: [''],
            notification: [''],
            emailNotification: [''],
            enableNotification: ['']
        }, { validators: this.validateDates });
        if (this.selectedActivityType) {
            this.taskForm.setControl('priority', new forms_1.FormControl('', [forms_1.Validators.required]));
            this.taskForm.setControl('state', new forms_1.FormControl('', [forms_1.Validators.required]));
            this.setDefaultValuesForNewTask(new Date());
        }
        else {
            this.setDefaultValuesForNewTask(this.startingDate);
        }
        this.taskForm.get('notification').valueChanges
            .subscribe(function (newValue) { return newValue === null ?
            _this.taskForm.get('emailNotification').disable() :
            _this.taskForm.get('emailNotification').enable(); });
        this.subscribeForNearbyTasks();
    };
    TaskFormComponent.prototype.setTime = function (time, date) {
        var taskHour = this.taskHourList.find(function (hour) { return hour.id === time; });
        if (taskHour) {
            date.setHours(taskHour.hour);
            date.setMinutes(taskHour.minute);
        }
        return date;
    };
    TaskFormComponent.prototype.setAssigness = function (data) {
        this.assignees = data.sort(function (a, b) {
            var nameA = a.key.toLowerCase(), nameB = b.key.toLowerCase();
            if (nameA < nameB) // ascending sort
                return -1;
            if (nameA > nameB)
                return 1;
            return 0;
        });
        this.resetSearchAssigneeControl();
    };
    TaskFormComponent.prototype.updateTaskForm = function (task) {
        if (this.taskForm && task) {
            this.selectedActivityType = task.activityType;
            this.createCustomerForm();
            this.activityTypeChanged.next(this.selectedActivityType);
            if (task.owner !== task.creator) {
                var assignees = this.allUsers.filter(function (e) { return e.value === task.owner || e.value === task.creator; });
                this.setAssigness(assignees);
            }
            this.allDayView = task.allDayEvent;
            this.title.patchValue(task.subject);
            this.allDayEvent.patchValue(task.allDayEvent);
            this.startDate.patchValue(times_util_1.TimesUtil.fromUTCToCurrentTimeZone(task.dateTimeFrom));
            this.endDate.patchValue(times_util_1.TimesUtil.fromUTCToCurrentTimeZone(task.dateTimeFrom));
            this.assignee.patchValue(task.owner);
            this.priority.patchValue(task.priority);
            this.state.patchValue(task.state);
            this.place.patchValue(task.place);
            this.searchedPlace = task.place;
            this.description.patchValue(task.description);
            var unit = task_utils_1.TaskUtils.getNotificationByIntervalAndUnit(task.interval, task.unit);
            if (unit) {
                this.enableNotification.patchValue(true);
                this.notification.patchValue(unit);
                this.emailNotification.patchValue(task.emailNotification);
            }
            else {
                this.enableNotification.patchValue(false);
                this.notification.patchValue(undefined);
                this.emailNotification.patchValue(false);
            }
            if (task.taskAssociations) {
                this.updateLabelsForTaskAssociation(task.taskAssociations);
            }
            if (!task.allDayEvent || this.selectedActivityType) {
                this.startTime.patchValue(times_util_1.TimesUtil.getTaskHourIndex(times_util_1.TimesUtil.fromUTCToCurrentTimeZone(task.dateTimeFrom).toDate()));
                this.endTime.patchValue(times_util_1.TimesUtil.getTaskHourIndex(times_util_1.TimesUtil.fromUTCToCurrentTimeZone(task.dateTimeTo).toDate()));
            }
            if (task.latitude && task.longitude) {
                this.location = { lat: +task.latitude, lng: +task.longitude };
                this.updateNearbyTasks();
            }
        }
    };
    TaskFormComponent.prototype.updateLabelsForTaskAssociation = function (taskAssociations) {
        var _this = this;
        this.assignToGroupService.updateLabelsForTaskAssociation(taskAssociations)
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (result) {
            _this.associations = result.map(function (asso) {
                asso.disabled = _this.storedInitAssociation && asso.objectId === _this.storedInitAssociation.objectId;
                return asso;
            });
        });
    };
    TaskFormComponent.prototype.getTask = function () {
        var task = new task_model_1.Task();
        if (this.editedEventId) {
            task.id = this.editedEventId;
        }
        task.subject = this.title.value;
        task.allDayEvent = !!this.allDayEvent.value;
        task.creator = +localStorage.getItem('loggedAccountId');
        if (this.selectedActivityType) {
            task.priority = this.priority.value;
            task.state = this.state.value;
        }
        task.place = this.place.value;
        task.description = this.description.value;
        if (this.enableNotification.value && this.notification.value) {
            task.interval = this.notification.value.interval;
            task.unit = this.notification.value.unit;
            task.emailNotification = this.emailNotification.value;
        }
        if (task.allDayEvent && !this.selectedActivityType) {
            task.dateTimeFrom = moment_1["default"].utc(this.startDate.value);
            task.dateTimeTo = moment_1["default"].utc(this.endDate.value);
        }
        else {
            task.dateTimeFrom = moment_1["default"].utc(this.setTime(this.startTime.value, new Date(this.startDate.value)));
            task.dateTimeTo = moment_1["default"].utc(this.setTime(this.endTime.value, new Date(this.endDate.value)));
        }
        if (this.searchedPlace !== this.place.value) {
            this.location = { lat: null, lng: null };
        }
        if (this.location) {
            task.longitude = this.location.lng ? String(this.location.lng) : '';
            task.latitude = this.location.lat ? String(this.location.lat) : '';
        }
        task.activityType = this.selectedActivityType;
        task.owner = this.assignee.value;
        task.taskAssociations = this.associations;
        return task;
    };
    TaskFormComponent.prototype.subscribeForSliderClose = function () {
        var _this = this;
        this.sliderService.sliderStateChanged
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (stateChange) {
            if (stateChange.opened === false &&
                (stateChange.key === 'Calendar.AddSlider' || stateChange.key === 'Calendar.EditSlider')) {
                _this.taskForm.reset();
                _this.associations = [];
                _this.storedInitAssociation = undefined;
                _this.setDefaultValuesForNewTask(_this.startingDate);
                _this.editedEventId = undefined;
                _this.location = null;
                _this.searchedPlace = null;
            }
        });
    };
    TaskFormComponent.prototype.subscribeForEventEdition = function () {
        var _this = this;
        this.calendarService.taskEdited
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (taskId) {
            _this.editedEventId = taskId;
            _this.calendarService.getEvent(taskId)
                .subscribe(function (task) {
                _this.updateTaskForm(task);
            });
        });
    };
    TaskFormComponent.prototype.subscribeForLocaleChanges = function () {
        var _this = this;
        this.adapter.setLocale(localStorage.getItem('locale'));
        this.localeService.localeChanged.subscribe(function () { return _this.adapter.setLocale(localStorage.getItem('locale')); });
    };
    TaskFormComponent.prototype.subscribeForAssignableAccounts = function () {
        var _this = this;
        this.personService.getAccountEmails()
            .pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.map(function (accounts) { return (accounts.map(function (account) { return ({ key: account.email, value: account.id }); })); }))
            .subscribe(function (accounts) {
            _this.allUsers = accounts;
            _this.setAssigness(cloneDeep(_this.allUsers));
        });
    };
    TaskFormComponent.prototype.setDefaultValuesForNewTask = function (date) {
        this.setAssigness(cloneDeep(this.allUsers));
        if (this.taskForm) {
            this.startDate.patchValue(date);
            this.endDate.patchValue(date);
            if (date instanceof Date) {
                this.startTime.patchValue(times_util_1.TimesUtil.getTaskHourIndex(date));
                this.endTime.patchValue(times_util_1.TimesUtil.getTaskHourIndex(new Date(date.getTime() + 30 * 60000)));
            }
            this.allDayView = false;
            this.allDayEvent.patchValue(this.allDayView);
            this.assignee.patchValue(+localStorage.getItem('loggedAccountId'));
            this.state.patchValue(task_model_1.TaskState.CREATED);
            this.priority.patchValue(task_model_1.TaskPriority.MEDIUM);
            this.enableNotification.patchValue(false);
            this.notification.patchValue(null);
            this.emailNotification.patchValue(false);
        }
    };
    TaskFormComponent.prototype.openMap = function (event) {
        var _this = this;
        this.mapService.getCoordinatesByAddressString(this.place.value)
            .pipe(operators_1.takeUntil(this.componentDestroyed)).subscribe(function (places) {
            _this.handleFoundPlaces(places);
        }, function (err) { return _this.mapService.notifyPlaceNotFound(); });
        event.stopPropagation();
    };
    TaskFormComponent.prototype.onAllDayCheckboxClicked = function ($event) {
        this.allDayView = $event.checked;
    };
    TaskFormComponent.prototype.addNewAssociation = function (crmObject) {
        if (!this.associations.find(function (item) { return item.objectId === crmObject.id && item.objectType === crmObject.type; })) {
            this.associations.push({ objectId: crmObject.id, objectType: crmObject.type, label: crmObject.label });
        }
        this.showAppAssignToGroup = false;
    };
    TaskFormComponent.prototype.removeAssociation = function (association) {
        var index = this.associations.indexOf(association);
        if (index !== -1) {
            this.associations.splice(index, 1);
            this.showAppAssignToGroup = false;
        }
    };
    TaskFormComponent.prototype.subscribeForNearbyTasks = function () {
        var _this = this;
        rxjs_1.merge(this.startDate.valueChanges, this.startTime.valueChanges, this.allDayEvent.valueChanges, this.place.valueChanges).pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.throttleTime(1000)).subscribe(function () { return _this.updateNearbyTasks(); });
    };
    TaskFormComponent.prototype.updateNearbyTasks = function () {
        var _this = this;
        if (this.selectedActivityType === task_model_1.TaskActivityType.MEETING &&
            this.startTime.value && this.startDate.value &&
            this.location && this.location.lng && this.location.lat) {
            var nearbyTaskCheck = {
                activityType: task_model_1.TaskActivityType.MEETING,
                dateTimeFrom: moment_1["default"].utc(this.setTime(this.startTime.value, new Date(this.startDate.value))),
                longitude: this.location.lng ? String(this.location.lng) : '',
                latitude: this.location.lat ? String(this.location.lat) : ''
            };
            this.nearbyTaskLoading = true;
            this.calendarService.getNearbyEvents(nearbyTaskCheck)
                .pipe(operators_1.takeUntil(this.componentDestroyed))
                .subscribe(function (nTasks) {
                _this.nearbyTasks = nTasks;
                _this.setUserNameOnNearbyTaskList();
            }, function () { return _this.nearbyTaskLoading = false; });
        }
    };
    TaskFormComponent.prototype.setUserNameOnNearbyTaskList = function () {
        var _this = this;
        if (this.nearbyTasks && this.nearbyTasks.length > 0) {
            var ids = this.nearbyTasks.map(function (nt) { return nt.accountId; }).filter(function (item, i, ar) { return ar.indexOf(item) === i; });
            this.personService.getAccountsInfoByIds(ids)
                .pipe(operators_1.takeUntil(this.componentDestroyed))
                .subscribe(function (accounts) {
                var label = new Map();
                accounts.forEach(function (o) { return label.set(o.id, o); });
                _this.nearbyTasks = _this.nearbyTasks.map(function (nt) {
                    nt.accountInfo = label.get(nt.accountId);
                    nt.daysDiff = Math.abs(_this.getCalendarTime(nt.taskDate));
                    if (nt.daysDiff === 0) {
                        nt.diffType = 'sameDay';
                    }
                    else {
                        nt.diffType = (_this.isDateAfterTaskDate(nt.taskDate)) ? 'before' : 'after';
                    }
                    return nt;
                });
            }, null, function () { return _this.nearbyTaskLoading = false; });
        }
        else {
            this.nearbyTaskLoading = false;
        }
    };
    TaskFormComponent.prototype.handleFoundPlaces = function (places) {
        var _this = this;
        this.foundPlaces = places;
        if (places && places.length === 1) {
            this.showPlace(places[0]);
        }
        else if (places && places.length > 1) {
            setTimeout(function () { return _this.placeSelect.open(); }, 500);
        }
        else {
            this.location = { lat: null, lng: null };
            this.mapService.notifyPlaceNotFound();
        }
    };
    TaskFormComponent.prototype.showPlace = function (place) {
        var _this = this;
        this.searchedPlace = this.place.value;
        this.mapCenter = this.mapService.getCenter(place);
        this.markers = this.mapService.getSingleMarkerFromFoundPlace(place);
        this.mapSlider.open();
        this.location = this.mapService.getCenter(place);
        setTimeout(function () { return _this.showMap = true; }, 500);
        this.updateNearbyTasks();
    };
    TaskFormComponent.prototype.getCalendarTime = function (time) {
        if (time == null) {
            return 0;
        }
        var taskDate = moment_1["default"].utc(this.setTime(this.startTime.value, new Date(this.startDate.value)))
            .tz(localStorage.getItem('timeZone')).startOf('day');
        return taskDate.diff(times_util_1.TimesUtil.fromUTCToCurrentTimeZone(time).startOf('day'), 'days');
    };
    TaskFormComponent.prototype.isDateAfterTaskDate = function (time) {
        if (time == null) {
            return false;
        }
        var taskDate = moment_1["default"].utc(this.setTime(this.startTime.value, new Date(this.startDate.value)))
            .tz(localStorage.getItem('timeZone')).startOf('day');
        return taskDate > times_util_1.TimesUtil.fromUTCToCurrentTimeZone(time).startOf('day');
    };
    TaskFormComponent.prototype.onStartTimeChanged = function (hour) {
        this.adjustEndDateTimeForTimeChange(hour.value);
    };
    TaskFormComponent.prototype.adjustEndDateTimeForTimeChange = function (hourValue) {
        var hour2330 = 94;
        var minutes30 = 2;
        if (hourValue <= hour2330) {
            this.endTime.patchValue(+hourValue + minutes30);
            if (moment_1["default"](this.endDate.value).format('MM/DD/YYYY') !== moment_1["default"](this.startDate.value).format('MM/DD/YYYY')) {
                this.endDate.patchValue(moment_1["default"](this.endDate.value).add(-1, 'days'));
            }
        }
        else {
            this.endTime.patchValue(hourValue - hour2330);
            if (moment_1["default"](this.endDate.value).format('MM/DD/YYYY') === moment_1["default"](this.startDate.value).format('MM/DD/YYYY')) {
                this.endDate.patchValue(moment_1["default"](this.endDate.value).add(1, 'days'));
            }
        }
    };
    TaskFormComponent.prototype.onStartDateChanged = function ($event) {
        this.endDate.patchValue($event.value);
        this.adjustEndDateTimeForTimeChange(this.startTime.value);
    };
    __decorate([
        core_1.ViewChild('mapSlider')
    ], TaskFormComponent.prototype, "mapSlider");
    __decorate([
        core_1.ViewChild('placeSelect')
    ], TaskFormComponent.prototype, "placeSelect");
    __decorate([
        core_1.Input()
    ], TaskFormComponent.prototype, "initAssociation");
    __decorate([
        core_1.Output()
    ], TaskFormComponent.prototype, "activityTypeChanged");
    __decorate([
        core_1.Input()
    ], TaskFormComponent.prototype, "activityType");
    __decorate([
        core_1.Input()
    ], TaskFormComponent.prototype, "eventInfo");
    TaskFormComponent = __decorate([
        core_1.Component({
            selector: 'app-task-form',
            templateUrl: './task-form.component.html',
            styleUrls: ['./task-form.component.scss'],
            providers: [
                { provide: material_1.DateAdapter, useClass: material_moment_adapter_1.MomentDateAdapter, deps: [material_1.MAT_DATE_LOCALE] },
                { provide: material_1.MAT_DATE_FORMATS, useValue: times_config_1.CRM_MOMENT_DATE_FORMATS }
            ]
        })
    ], TaskFormComponent);
    return TaskFormComponent;
}());
exports.TaskFormComponent = TaskFormComponent;
