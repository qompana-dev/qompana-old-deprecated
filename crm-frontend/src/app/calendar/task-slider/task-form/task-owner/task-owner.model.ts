export interface Owner {
  id: number;
  name: string;
  email: string;
  avatar: string;
  phone: string;
}
