import {Page, PageRequest} from '../../../../shared/pagination/page.model';
import {UrlUtil} from '../../../../shared/url.util';
import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Owner} from './task-owner.model';

@Injectable({
  providedIn: 'root'
})
export class TaskOwnerService {

  private url = `${UrlUtil.url}/crm-user-service/accounts/search/task`;

  constructor(private http: HttpClient) {
  }

  getOwners(pageRequest: PageRequest, term: string): Observable<Page<Owner>> {
    const params = new HttpParams({fromObject: {...pageRequest, term}});
    return this.http.get<Page<Owner>>(this.url, {params});
  }

}
