import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Subject, Subscription} from 'rxjs';
import {MemberService} from '../../add-members/member.service';
import {debounceTime, distinctUntilChanged, filter, map, startWith, takeUntil, throttleTime} from "rxjs/operators";
import {Page, PageRequest} from '../../../../../shared/pagination/page.model';
import {InfiniteScrollUtil} from '../../../../../shared/pagination/infinite-scroll.util';
import {Owner} from '../task-owner.model';
import {TaskOwnerService} from '../task-owner.service';
import {MatListOption, MatSelectionList} from '@angular/material/list';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-menu-owners',
  templateUrl: './menu-owners.component.html',
  styleUrls: ['./menu-owners.component.scss']
})
export class MenuOwnersComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('matSelectionList') matSelectionList: MatSelectionList;

  @Input() trigger: ElementRef;
  @Input() open: boolean;

  @Output() changeMenu: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() changeOwner: EventEmitter<Owner> = new EventEmitter<Owner>();

  @Input() set currentOwner(currentOwner: Owner) {
    this.selectedOwner = currentOwner;
  }

  searchControl = new FormControl();
  search = '';

  owners: Owner[] = [];
  selectedOwner: Owner;

  private currentPage: number;
  private totalElements: number;
  private numberOfElements: number;

  private cancelRequests: Subject<void> = new Subject();
  private searchSubscription: Subscription;

  constructor(private memberService: MemberService,
              private taskOwnerService: TaskOwnerService) {
  }

  ngOnInit(): void {
    this.initSearchField();
  }

  ngAfterViewInit(): void {
    this.matSelectionList.selectedOptions = new SelectionModel<MatListOption>(false);
  }

  ngOnDestroy(): void {
    this.cancelRequests.next();
    this.cancelRequests.complete();
    if (this.searchSubscription) {
      this.searchSubscription.unsubscribe();
    }
  }

  close(): void {
    this.changeMenu.emit(false);
  }

  selectOwner(): void {
    this.changeOwner.emit(this.selectedOwner);
    this.close();
  }

  changeSelectedOwner({option}): void {
    const {selected, value} = option;
    if (selected) {
      this.selectedOwner = value;
    } else {
      this.selectedOwner = null;
    }
  }

  isAcceptButtonDisabled(): boolean {
    if (this.selectedOwner !== undefined && this.selectedOwner !== null) {
      return this.selectedOwner.id === null;
    } else {
      return true;
    }
  }

  isSelected(owner: Owner): boolean {
    if (this.selectedOwner !== undefined && this.selectedOwner !== null) {
      return this.selectedOwner.id === owner.id;
    } else {
      return false;
    }
  }

  private initSearchField(): void {
    this.searchSubscription = this.searchControl.valueChanges
      .pipe(
        startWith(''),
        map(value => value.trim()),
        filter(value => !value || value.length >= 2),
        debounceTime(500),
        distinctUntilChanged(),
      ).subscribe(value => {
        this.search = value;
        this.initOwners();
      });
  }

  private initOwners(pageRequest: PageRequest = {page: '0', size: InfiniteScrollUtil.ELEMENTS_ON_PAGE}): void {
    this.owners = [];
    this.cancelRequests.next();
    this.taskOwnerService.getOwners(pageRequest, this.search)
      .pipe(
        takeUntil(this.cancelRequests)
      )
      .subscribe(page => this.handleInitSuccess(page));
  }

  public onScroll(e: any): void {
    if (InfiniteScrollUtil.shouldGetMoreData(e) && this.numberOfElements < this.totalElements) {
      const nextPage = this.currentPage + 1;
      this.cancelRequests.next();
      this.getMoreOwners({page: '' + nextPage, size: InfiniteScrollUtil.ELEMENTS_ON_PAGE});
    }
  }

  private getMoreOwners(pageRequest: PageRequest): void {
    this.taskOwnerService.getOwners(pageRequest, this.search)
      .pipe(
        takeUntil(this.cancelRequests),
        throttleTime(500)
      )
      .subscribe(page => this.handleGetMoreOwnersSuccess(page));
  }

  private handleInitSuccess(page: Page<Owner>): void {
    this.currentPage = 0;
    this.totalElements = page.totalElements;
    this.numberOfElements = page.numberOfElements;

    this.owners = page.content;
  }

  private handleGetMoreOwnersSuccess(page: Page<Owner>): void {
    this.currentPage++;
    this.numberOfElements += page.content.length;

    this.owners = this.owners.concat(page.content);
  }

}
