import {Component, forwardRef, Input, OnInit, ViewChild} from '@angular/core';
import {Owner} from './task-owner.model';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {MatListOption, MatSelectionList} from "@angular/material/list";
import {SelectionModel} from "@angular/cdk/collections";

@Component({
  selector: 'app-task-owner',
  templateUrl: './task-owner.component.html',
  styleUrls: ['./task-owner.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TaskOwnerComponent),
    multi: true
  }]
})
export class TaskOwnerComponent implements ControlValueAccessor {

  public isOpen = false;
  public owner: Owner = {
    id: null,
    name: '',
    email: '',
    avatar: '',
    phone: ''
  };

  @Input() label;

  constructor() {
  }

  onChange = (_: any) => {
  }
  onTouched = () => {
  }

  changeMenu(state: boolean) {
    !state && this.onTouched();
    this.isOpen = state;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: any): void {
    this.isOpen = false;
    this.owner = value;
  }

  changeOwner(owner) {
    this.owner = owner;
    this.onChange(this.owner);
  }

}
