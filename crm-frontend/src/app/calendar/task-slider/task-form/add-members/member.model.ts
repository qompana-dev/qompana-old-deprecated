
export enum MemberType {
  USER = 'ACCOUNT',
  CUSTOMER = 'CUSTOMER',
  CONTACT = 'CONTACT'
}

export interface Member {
  id: number;
  type: MemberType;
  name: string;
  email: string;
  avatar: string;
}
