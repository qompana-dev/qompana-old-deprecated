import {MemberService} from '../member.service';
import {Member, MemberType} from '../member.model';
import {Page, PageRequest} from '../../../../../shared/pagination/page.model';
import {InfiniteScrollUtil} from '../../../../../shared/pagination/infinite-scroll.util';
import {debounceTime, distinctUntilChanged, filter, map, startWith, takeUntil, throttleTime} from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';
import {FormControl} from '@angular/forms';
import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-menu-members',
  templateUrl: './menu-members.component.html',
  styleUrls: ['./menu-members.component.scss']
})
export class MenuMembersComponent implements OnInit, OnDestroy {
  @Input() trigger: ElementRef;
  @Input() open: boolean;

  @Output() changeMenu: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() changeMembers: EventEmitter<Member[]> = new EventEmitter<Member[]>();

  @Input() set selectMembers(value) {
    this.selectedMembers = [...value];
  };

  searchControl = new FormControl();
  search = '';

  members: Member[] = [];
  selectedMembers: Member[] = [];

  selectType: MemberType = MemberType.USER;
  selectButtons = [
    {
      name: 'calendar.form.memberType.user',
      type: MemberType.USER
    },
    {
      name: 'calendar.form.memberType.customer',
      type: MemberType.CUSTOMER
    },
    {
      name: 'calendar.form.memberType.contact',
      type: MemberType.CONTACT
    }
  ]

  private currentPage: number;
  private totalElements: number;
  private numberOfElements: number;

  private cancelRequests: Subject<void> = new Subject();
  private searchSubsribtion: Subscription;

  constructor(private memberService: MemberService) {
  }

  ngOnInit() {
    this.initSearchField();
  }

  ngOnDestroy(): void {
    this.cancelRequests.next();
    this.cancelRequests.complete();
    if (this.searchSubsribtion) {
      this.searchSubsribtion.unsubscribe();
    }
  }

  private initSearchField() {
    this.searchSubsribtion = this.searchControl.valueChanges
      .pipe(
        startWith(''),
        map(value => value.trim()),
        filter(value => !value || value.length > 2),
        debounceTime(500),
        distinctUntilChanged(),
      ).subscribe(value => {
        this.search = value;
        this.initMembers();
      });
  }

  private initMembers(pageRequest: PageRequest = {page: '0', size: InfiniteScrollUtil.ELEMENTS_ON_PAGE}): void {
    this.members = [];
    this.cancelRequests.next();
    this.memberService.getMember(pageRequest, this.getRouteByType(this.selectType), this.search)
      .pipe(
        takeUntil(this.cancelRequests)
      ).subscribe(
      page => this.handleInitSuccess(page)
    );
  }

  getRouteByType(type: MemberType): string {
    switch (type) {
      case MemberType.USER:
        return 'crm-user-service/accounts/search/task';
      case MemberType.CONTACT:
        return 'crm-business-service/contacts/search/task';
      case MemberType.CUSTOMER:
        return 'crm-business-service/customers/search/task';
      default:
        return 'crm-user-service/accounts/search/task';
    }
  }

  changeType(type: MemberType) {
    if (this.selectType !== type) {
      this.selectType = type;
      this.initMembers();
    }
  }

  private getMoreMembers(pageRequest: PageRequest): void {
    this.memberService.getMember(pageRequest, this.getRouteByType(this.selectType), this.search)
      .pipe(
        takeUntil(this.cancelRequests),
        throttleTime(500)
      ).subscribe(
      page => this.handleGetMoreMembersSuccess(page)
    );
  }

  public onScroll(e: any): void {
    if (InfiniteScrollUtil.shouldGetMoreData(e) && this.numberOfElements < this.totalElements) {
      const nextPage = this.currentPage + 1;
      this.cancelRequests.next();
      this.getMoreMembers({page: '' + nextPage, size: InfiniteScrollUtil.ELEMENTS_ON_PAGE});
    }
  }

  private handleInitSuccess(page: Page<Member>): void {
    this.currentPage = 0;
    this.totalElements = page.totalElements;
    this.numberOfElements = page.numberOfElements;

    this.members = page.content
  }

  private handleGetMoreMembersSuccess(page: Page<Member>): void {
    this.currentPage++;
    this.numberOfElements += page.content.length;

    this.members = this.members.concat(page.content)
  }

  close() {
    this.changeMenu.emit(false)
  }

  selectUser({option}) {
    const {selected, value} = option;
    if (selected) {
      this.selectedMembers = [...this.selectedMembers, value];
    } else {
      this.selectedMembers = this.selectedMembers.filter(item => item.type !== value.type || item.id !== value.id)
    }
  }

  isSelected(member) {
    const check = this.selectedMembers.some(item => item.type === member.type && item.id === member.id)
    return check;
  }

  addSelectedAssignees() {
    this.changeMembers.emit(this.selectedMembers);
    this.close()
  }

}
