import { PageRequest, Page } from './../../../../shared/pagination/page.model';
import { UrlUtil } from './../../../../shared/url.util';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Member } from './member.model';

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  constructor(private http: HttpClient) {
  }

  getMember(pageRequest: PageRequest, route: string, term: string): Observable<Page<Member>> {
    const params = new HttpParams({ fromObject: { ...pageRequest, term } });
    const url = `${UrlUtil.url}/${route}`;
    return this.http.get<Page<Member>>(url, { params });
  }

}
