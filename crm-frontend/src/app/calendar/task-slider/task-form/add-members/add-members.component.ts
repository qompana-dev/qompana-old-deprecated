import { Member } from './member.model';

import { Component, Input, forwardRef  } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-add-members',
  templateUrl: './add-members.component.html',
  styleUrls: ['./add-members.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => AddMembersComponent),
    multi: true
  }]
})
export class AddMembersComponent implements ControlValueAccessor {

  isOpen = false;
  participants: Member[] = [];

  @Input() label;

  constructor() {
  }

  onChange = (_: any) => {}
  onTouched = () => {}

  changeMenu(state: boolean) {
    !state && this.onTouched();
    this.isOpen = state;
  }

  changeParticipants(participants) {
    this.participants = participants;
    this.onChange(this.participants);
  }

  writeValue(value: any) {
    this.isOpen = false;
    this.participants = value;
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

}
