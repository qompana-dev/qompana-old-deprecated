export class TaskFormCommentEditorConfig {
  static config = {
    toolbar: [['bold', 'italic', 'underline'],
      [{'list': 'ordered'}, {'list': 'bullet'}],
      [{'color': []}, {'background': []}]
    ]
  };
}
