import {ACTIVITY_TYPES} from "./task-form.config";
import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from "@angular/forms";
import {MomentDateAdapter} from "@angular/material-moment-adapter";
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  MatSelect,
} from "@angular/material";
import {LocaleService} from "../../locale.service";
import {
  EventInfo,
  NearbyTask,
  NearbyTaskCheck,
  Task,
  TaskActivityType,
  TaskAssociation,
  TaskPriority,
  TaskState,
} from "../../task.model";
import {merge, Subject} from "rxjs";
import {map, takeUntil, throttleTime, debounceTime, switchMap} from "rxjs/operators";
import {
  SliderService,
  SliderStateChange,
} from "../../../shared/slider/slider.service";
import {CalendarService} from "../../calendar.service";
import {TimesUtil} from "../../../shared/times.util";
import {TaskUtils} from "../../utils/task-utils";
// @ts-ignore
import moment from "moment";
import {PersonService} from "../../../person/person.service";
import {TimePickerHour} from "../../../shared/model";
import {MapViewMarker} from "../../../map/map-view/map-view.model";
import {SliderComponent} from "../../../shared/slider/slider.component";
import {CrmObject} from "../../../shared/model/search.model";
import {AssignToGroupService} from "../../../shared/assign-to-group/assign-to-group.service";
import {MapService} from "../../../map/map.service";
import {FoundPlace} from "../../../lead/lead.model";
import {LngLatLike} from "mapbox-gl";
import {CRM_MOMENT_DATE_FORMATS} from "../../../shared/config/times.config";
import {MatSelectChange} from "@angular/material/select";
import {MatDatepickerInputEvent} from "@angular/material/datepicker";
import {AccountInfoDto} from "../../../person/person.model";
import {NotificationService} from "../../../shared/notification.service";
import {FormValidators} from "../../../shared/form/form-validators";
import {QuillEditorComponent} from "ngx-quill";
import {TaskFormCommentEditorConfig} from "./task-form-comment-editor.config";
import {TaskOwnerService} from "./task-owner/task-owner.service";
import {InfiniteScrollUtil} from "../../../shared/pagination/infinite-scroll.util";
import {Address} from 'src/app/lead/lead.model';

@Component({
  selector: "app-task-form",
  templateUrl: "./task-form.component.html",
  styleUrls: ["./task-form.component.scss"],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    {provide: MAT_DATE_FORMATS, useValue: CRM_MOMENT_DATE_FORMATS},
  ],
})
export class TaskFormComponent implements OnInit, OnDestroy {
  formType: FormType;
  private componentDestroyed: Subject<void> = new Subject();
  @ViewChild("mapSlider") mapSlider: SliderComponent;
  @ViewChild("commentEditor") commentEditor: QuillEditorComponent;
  commentEditorConfig = TaskFormCommentEditorConfig.config;
  task: Task;
  taskForm: FormGroup;

  editedEventId: number;
  startingDate: Date;
  allDayView: boolean;

  taskState = TaskState;
  taskPriority = TaskPriority;
  notifications = TaskUtils.notifications;
  ownerId: number;
  allUsers: KeyValue[] = [];
  assignees: KeyValue[] = [];
  taskHourList: TimePickerHour[] = [];

  location: { lng: number; lat: number };
  markers: MapViewMarker[];
  mapCenter: LngLatLike = undefined;
  showMap = false;
  foundPlaces: FoundPlace[] = [];
  @ViewChild("placeSelect") placeSelect: MatSelect;

  showAppAssignToGroup = false;
  taskActivityType = TaskActivityType;
  phoneActivityDefaultTitles: string[] = ["Welcome call", "Follow up"];
  currentCheckedPhoneActivityTitle = null;
  moment = moment;
  britishTimeFormat = false;
  associations: TaskAssociation[] = [];
  private storedInitAssociation: TaskAssociation;
  private searchedPlace: string;

  nearbyTasks: NearbyTask[] = [];
  nearbyTaskLoading = false;

  activityTypes = ACTIVITY_TYPES;

  @Input() set initAssociation(initAssociation: TaskAssociation) {
    this.storedInitAssociation = initAssociation;
    if (initAssociation) {
      this.updateLabelsForTaskAssociation([initAssociation]);
    }
  }

  @Output()
  activityTypeChanged: EventEmitter<TaskActivityType> = new EventEmitter<TaskActivityType>();

  public selectedActivityType: TaskActivityType; // default undefined

  @Input() set activityType(selectedActivityType: TaskActivityType) {
    this.selectedActivityType = selectedActivityType;
    this.activityTypeChanged.next(this.selectedActivityType);
    this.setDefaultValuesForNewTask(new Date());
  }

  @Input() set eventInfo(info: EventInfo) {
    if (info) {
      this.startingDate = info.startDate;
      this.allDayView = info.allDayEvent;
      this.setDefaultValuesForNewTask(info.startDate);
    }
  }

  constructor(
    private assignToGroupService: AssignToGroupService,
    private formBuilder: FormBuilder,
    private adapter: DateAdapter<any>,
    private localeService: LocaleService,
    private sliderService: SliderService,
    private notificationService: NotificationService,
    private calendarService: CalendarService,
    private personService: PersonService,
    private mapService: MapService,
    private taskOwnerService: TaskOwnerService
  ) {
    this.britishTimeFormat = localStorage.getItem("hourFormat") === "12";
    this.taskHourList = TimesUtil.getHoursList();
    this.createCustomerForm();
    this.subscribeForSliderOpen();
    this.subscribeForSliderClose();
    this.subscribeForEventEdition();
    this.subscribeForLocaleChanges();
    this.subscribeForLocation();
  }

  ngOnInit(): void {
    if (this.commentEditor !== undefined) {
      this.commentEditor.placeholder = "";
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  // @formatter:off
  get title(): AbstractControl {
    return this.taskForm.get("title");
  }

  get allDayEvent(): AbstractControl {
    return this.taskForm.get("allDayEvent");
  }

  get startDate(): AbstractControl {
    return this.taskForm.get("startDate");
  }

  get startTime(): AbstractControl {
    return this.taskForm.get("startTime");
  }

  get endDate(): AbstractControl {
    return this.taskForm.get("endDate");
  }

  get endTime(): AbstractControl {
    return this.taskForm.get("endTime");
  }

  get owner(): AbstractControl {
    return this.taskForm.get("owner");
  }

  get participants(): AbstractControl {
    return this.taskForm.get("participants");
  }

  get priority(): AbstractControl {
    return this.taskForm.get("priority");
  }

  get place(): AbstractControl {
    return this.taskForm.get("place");
  }

  get state(): AbstractControl {
    return this.taskForm.get("state");
  }

  get description(): AbstractControl {
    return this.taskForm.get("description");
  }

  get enableNotification(): AbstractControl {
    return this.taskForm.get("enableNotification");
  }

  get notificationDate(): AbstractControl {
    return this.taskForm.get("notificationDate");
  }

  get notificationTime(): AbstractControl {
    return this.taskForm.get("notificationTime");
  }

  // @formatter:on

  public isFormValid(): boolean {
    return this.taskForm.valid;
  }

  public markAsTouched(): void {
    (Object as any).values(this.taskForm.controls).forEach((control) => {
      control.markAsTouched();
    });
  }

  public changeType(activityType) {
    if (this.task) {
      this.updateTaskForm(this.task, activityType);
    } else {
      this.updateActivityType(activityType);
    }
  }

  private createCustomerForm(): void {
    this.taskForm = this.formBuilder.group(
      {
        title: [
          "",
          Validators.compose([
            Validators.required,
            FormValidators.whitespace,
            Validators.maxLength(100),
          ]),
        ],
        allDayEvent: [""],
        startDate: ["", Validators.required],
        startTime: [""],
        endDate: ["", Validators.required],
        endTime: [""],
        priority: [""],
        owner: new FormControl("", [Validators.required]),
        participants: new FormControl([]),
        place: ["", Validators.maxLength(200)],
        state: [""],
        description: ["", Validators.maxLength(1000)],
        enableNotification: [false],
        notificationDate: [""],
        notificationTime: [""],
      },
      {validators: this.validateDates}
    );

    if (this.selectedActivityType) {
      this.taskForm.setControl(
        "state",
        new FormControl("", [Validators.required])
      );
      this.taskForm.setControl(
        "priority",
        new FormControl("", [Validators.required])
      );
      this.setDefaultValuesForNewTask(new Date());
    } else {
      this.setDefaultValuesForNewTask(this.startingDate);
    }

    this.subscribeForNearbyTasks();
  }

  validateDates: ValidatorFn = (
    control: FormGroup
  ): ValidationErrors | null => {
    if (this.taskForm && this.startDate && this.endDate) {
      let startDate = new Date(this.startDate.value);
      let endDate = new Date(this.endDate.value);
      if (!this.allDayEvent.value) {
        if (this.endTime.value && this.startTime.value) {
          startDate = this.setTime(this.startTime.value, startDate);
          endDate = this.setTime(this.endTime.value, endDate);
        } else {
          return {noTimes: true};
        }
      }
      return startDate > endDate ? {startDayNotBefore: true} : null;
    }
  };

  private setTime(time: number, date: Date): Date {
    const taskHour = this.taskHourList.find((hour) => hour.id === time);
    if (taskHour) {
      date.setHours(taskHour.hour);
      date.setMinutes(taskHour.minute);
    }
    return date;
  }

  private updateActivityType(activity: TaskActivityType) {
    this.selectedActivityType = activity;
    this.createCustomerForm();
    this.activityTypeChanged.next(this.selectedActivityType);
  }

  private updateTaskForm(task: Task, activity?: TaskActivityType): void {
    if (this.taskForm && task) {
      this.task = task;

      this.updateActivityType(
        activity !== undefined ? activity : task.activityType
      );

      this.allDayView = task.allDayEvent;

      this.title.patchValue(task.subject);
      this.allDayEvent.patchValue(task.allDayEvent);
      this.startDate.patchValue(
        TimesUtil.fromUTCToCurrentTimeZone(task.dateTimeFrom)
      );
      this.endDate.patchValue(
        TimesUtil.fromUTCToCurrentTimeZone(task.dateTimeTo)
      );
      this.setOwnerValue(task.owner);
      this.participants.patchValue(task.participants);
      this.priority.patchValue(task.priority || TaskPriority.MEDIUM);

      this.state.patchValue(task.state || TaskState.CREATED);
      this.place.patchValue(task.place);
      this.searchedPlace = task.place;
      this.description.patchValue(task.description);

      if (task.notificationRequired) {
        this.enableNotification.patchValue(true);
        this.notificationDate.patchValue(
          TimesUtil.fromUTCToCurrentTimeZone(task.notificationDateTime)
        );
        this.notificationTime.patchValue(
          TimesUtil.getTaskHourIndex(
            TimesUtil.fromUTCToCurrentTimeZone(
              task.notificationDateTime
            ).toDate()
          ) - 1
        );
      } else {
        this.enableNotification.patchValue(false);
      }

      if (task.taskAssociations) {
        this.updateLabelsForTaskAssociation(task.taskAssociations);
      }

      if (!task.allDayEvent || this.selectedActivityType) {
        this.startTime.patchValue(
          TimesUtil.getTaskHourIndex(
            TimesUtil.fromUTCToCurrentTimeZone(task.dateTimeFrom).toDate()
          ) - 1
        );
        this.endTime.patchValue(
          TimesUtil.getTaskHourIndex(
            TimesUtil.fromUTCToCurrentTimeZone(task.dateTimeTo).toDate()
          ) - 1
        );
      }

      if (task.latitude && task.longitude) {
        this.location = {lat: +task.latitude, lng: +task.longitude};
        this.updateNearbyTasks();
      }
    }
  }

  private updateLabelsForTaskAssociation(
    taskAssociations: TaskAssociation[]
  ): void {
    this.assignToGroupService
      .updateLabelsForTaskAssociation(taskAssociations)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((result: TaskAssociation[]) => {
        this.associations = result.map((asso) => {
          asso.disabled =
            this.storedInitAssociation &&
            asso.objectId === this.storedInitAssociation.objectId;
          return asso;
        });
      });
  }

  getTask(): Task {
    const task = new Task();
    if (this.editedEventId) {
      task.id = this.editedEventId;
    }
    task.subject = this.title.value;
    task.allDayEvent = !!this.allDayEvent.value;
    task.creator = +localStorage.getItem("loggedAccountId");
    if (this.selectedActivityType) {
      task.priority = this.priority.value;
      task.state = this.state.value;
    }
    task.place = this.place.value;
    task.description = this.description.value;
    if (
      this.enableNotification.value &&
      this.notificationDate.value &&
      this.notificationTime.value
    ) {
      task.notificationRequired = this.enableNotification.value;
      task.notificationDateTime = moment.utc(
        this.setTime(
          this.notificationTime.value,
          new Date(this.notificationDate.value)
        )
      );
    }
    if (task.allDayEvent && !this.selectedActivityType) {
      task.dateTimeFrom = moment.utc(this.startDate.value);
      task.dateTimeTo = moment.utc(this.endDate.value);
    } else {
      task.dateTimeFrom = moment.utc(
        this.setTime(this.startTime.value, new Date(this.startDate.value))
      );
      task.dateTimeTo = moment.utc(
        this.setTime(this.endTime.value, new Date(this.endDate.value))
      );
    }
    if (this.searchedPlace !== this.place.value) {
      this.location = {lat: null, lng: null};
    }
    if (this.location) {
      task.longitude = this.location.lng ? String(this.location.lng) : "";
      task.latitude = this.location.lat ? String(this.location.lat) : "";
    }
    task.activityType = this.selectedActivityType;
    task.owner = this.owner.value.id;
    task.timeZone = moment.tz.guess();

    task.taskAssociations = this.associations;
    task.participants = this.participants.value;

    return task;
  }

  private subscribeForSliderClose(): void {
    this.sliderService.sliderStateChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((stateChange: SliderStateChange) => {
        if (
          stateChange.opened === false &&
          (stateChange.key === "Calendar.AddSlider" ||
            stateChange.key === "Calendar.EditSlider")
        ) {
          this.taskForm.reset();
          this.associations = [];
          this.storedInitAssociation = undefined;
          this.setDefaultValuesForNewTask(this.startingDate);
          this.editedEventId = undefined;
          this.location = null;
          this.searchedPlace = null;
        }
      });
  }

  private subscribeForSliderOpen(): void {
    this.sliderService.sliderStateChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((stateChange: SliderStateChange) => {
        if (stateChange.opened) {
          this.updateOwner();
        }
        if (
          stateChange.opened === true &&
          stateChange.key === "Calendar.AddSlider"
        ) {

          this.formType = FormType.ADD;
          if (this.selectedActivityType === TaskActivityType.PHONE) {
            if (stateChange.payload.isFirstPhoneActivity) {
              this.setDefaultTitle(this.phoneActivityDefaultTitles[0]);
            } else {
              this.setDefaultTitle(this.phoneActivityDefaultTitles[1]);
            }
          }

        } else if (
          stateChange.opened === true &&
          stateChange.key === "Calendar.EditSlider"
        ) {
          this.formType = FormType.EDIT;
        }
      });
  }

  private subscribeForEventEdition(): void {
    this.calendarService.taskEdited
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((taskId: number) => {
        this.editedEventId = taskId;
        this.calendarService.getEvent(taskId).subscribe((task: Task) => {
          this.updateTaskForm(task);
        });
      });
  }

  private subscribeForLocaleChanges(): void {
    this.adapter.setLocale(localStorage.getItem("locale"));
    this.localeService.localeChanged.subscribe(() =>
      this.adapter.setLocale(localStorage.getItem("locale"))
    );
  }

  // private subscribeForAssignableAccounts(): void {
  //   this.personService
  //     .getAccountEmails()
  //     .pipe(
  //       takeUntil(this.componentDestroyed),
  //       map((accounts) =>
  //         accounts.map((account) => ({ key: account.email, value: account.id }))
  //       )
  //     )
  //     .subscribe((accounts) => {
  //       this.allUsers = accounts;
  //     });
  // }

  private setDefaultValuesForNewTask(date: Date): void {
    if (this.taskForm) {
      this.startDate.patchValue(date);
      this.endDate.patchValue(date);
      if (date instanceof Date) {
        this.startTime.patchValue(TimesUtil.getTaskHourIndex(date));
        this.endTime.patchValue(
          TimesUtil.getTaskHourIndex(new Date(date.getTime() + 30 * 60000))
        );
      }
      this.allDayView = false;
      this.allDayEvent.patchValue(this.allDayView);
      this.setOwnerValue(+localStorage.getItem("loggedAccountId"));
      this.participants.patchValue([]);
      this.state.patchValue(TaskState.CREATED);
      this.priority.patchValue(TaskPriority.MEDIUM);
      this.enableNotification.patchValue(false);
      this.notificationDate.patchValue(date);
      if (date instanceof Date) {
        this.notificationTime.patchValue(
          TimesUtil.getTaskNotificationHourIndex(date)
        );
      }
    }
  }

  openMap(event: MouseEvent): void {
    this.mapService
      .getCoordinatesByAddressString(this.place.value)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (places) => {
          this.handleFoundPlaces(places);
        },
        (err) => this.mapService.notifyPlaceNotFound()
      );
    event.stopPropagation();
  }

  private subscribeForLocation(): void {
    this.place.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      switchMap(((input: string) => {
          return this.mapService.getCoordinatesByAddressString(input);
        }
      ))).subscribe(places => {
      this.foundPlaces = places;
      this.placeSelect.open();
    });
  }

  onAllDayCheckboxClicked($event: any): void {
    this.allDayView = $event.checked;
  }

  addNewAssociation(crmObject: CrmObject): void {
    if (
      !this.associations.find(
        (item) =>
          item.objectId === crmObject.id && item.objectType === crmObject.type
      )
    ) {
      this.associations.push({
        objectId: crmObject.id,
        objectType: crmObject.type,
        label: crmObject.label,
      });
    }
    this.showAppAssignToGroup = false;
  }

  removeAssociation(association: TaskAssociation): void {
    const index = this.associations.indexOf(association);
    if (index !== -1) {
      this.associations.splice(index, 1);
      this.showAppAssignToGroup = false;
    }
  }

  subscribeForNearbyTasks(): void {
    merge(
      this.startDate.valueChanges,
      this.startTime.valueChanges,
      this.allDayEvent.valueChanges,
      this.place.valueChanges
    )
      .pipe(takeUntil(this.componentDestroyed), throttleTime(1000))
      .subscribe(() => this.updateNearbyTasks());
  }

  clearDefaultTitleRadioButton(): void {
    if (this.selectedActivityType === TaskActivityType.PHONE) {
      this.currentCheckedPhoneActivityTitle = null;
    }
  }

  checkPhoneTitle(event: any, el: any): void {
    event.preventDefault();
    if (
      this.currentCheckedPhoneActivityTitle &&
      this.currentCheckedPhoneActivityTitle === el.value
    ) {
      el.checked = false;
      this.currentCheckedPhoneActivityTitle = null;
      this.title.patchValue(null);
    } else {
      this.currentCheckedPhoneActivityTitle = el.value;
      el.checked = true;
      this.title.patchValue(el.value);
    }
  }

  private updateNearbyTasks(): void {
    if (this.selectedActivityType === TaskActivityType.MEETING) {
      if (
        this.startTime.value &&
        this.startDate.value &&
        this.location &&
        this.location.lng &&
        this.location.lat &&
        this.place.value === this.searchedPlace
      ) {
        const nearbyTaskCheck: NearbyTaskCheck = {
          activityType: TaskActivityType.MEETING,
          dateTimeFrom: moment.utc(
            this.setTime(this.startTime.value, new Date(this.startDate.value))
          ),
          longitude: this.location.lng ? String(this.location.lng) : "",
          latitude: this.location.lat ? String(this.location.lat) : "",
        };
        this.nearbyTaskLoading = true;
        this.calendarService
          .getNearbyEvents(nearbyTaskCheck)
          .pipe(takeUntil(this.componentDestroyed))
          .subscribe(
            (nTasks: NearbyTask[]) => {
              this.nearbyTasks = nTasks;
              this.setUserNameOnNearbyTaskList();
            },
            () => (this.nearbyTaskLoading = false)
          );
      } else {
        this.mapCenter = undefined;
        this.markers = undefined;
        this.location = undefined;
        this.nearbyTasks = [];
      }
    }
  }

  private setUserNameOnNearbyTaskList(): void {
    if (this.nearbyTasks && this.nearbyTasks.length > 0) {
      const ids = this.nearbyTasks
        .map((nt) => nt.accountId)
        .filter((item, i, ar) => ar.indexOf(item) === i);
      this.personService
        .getAccountsInfoByIds(ids)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (accounts: AccountInfoDto[]) => {
            const label: Map<number, AccountInfoDto> = new Map<number,
              AccountInfoDto>();
            accounts.forEach((o) => label.set(o.id, o));
            this.nearbyTasks = this.nearbyTasks.map((nt) => {
              nt.accountInfo = label.get(nt.accountId);
              nt.daysDiff = Math.abs(this.getCalendarTime(nt.taskDate));
              if (nt.daysDiff === 0) {
                nt.diffType = "sameDay";
              } else {
                nt.diffType = this.isDateAfterTaskDate(nt.taskDate)
                  ? "before"
                  : "after";
              }
              return nt;
            });
          },
          null,
          () => (this.nearbyTaskLoading = false)
        );
    } else {
      this.nearbyTaskLoading = false;
    }
  }

  private handleFoundPlaces(places: FoundPlace[]): void {
    this.foundPlaces = places;
    if (places && places.length === 1) {
      this.showPlace(places[0]);
    } else if (places && places.length > 1) {
      setTimeout(() => this.placeSelect.open(), 500);
    } else {
      this.location = {lat: null, lng: null};
      this.mapService.notifyPlaceNotFound();
    }
  }

  showPlace(place: FoundPlace): void {
    this.place.patchValue(place.display_name, {emitEvent: false});
    this.searchedPlace = this.place.value;
    this.mapCenter = this.mapService.getCenter(place);
    this.markers = this.mapService.getSingleMarkerFromFoundPlace(place, this.onMarkerDragEnd.bind(this));
    this.location = this.mapService.getCenter(place);
    setTimeout(() => (this.showMap = true), 500);
    this.updateNearbyTasks();

    if (!this.showMap) {
      this.mapSlider.open();
    }
  }

  onMarkerDragEnd(place: FoundPlace): void {
    const displayName = this.mapService.formatPlaceDescription(place);
    this.place.patchValue(displayName, {emitEvent: false});
  }

  onMapClose(): void {
    this.showMap = false;
  }

  private setOwnerValue(ownerId: number): void {
    this.ownerId = ownerId;
  }

  private updateOwner(): void {
    this.personService
      .getAccountEmails()
      .pipe(
        takeUntil(this.componentDestroyed),
        map((accounts) =>
          accounts.map((account) => ({key: account.email, value: account.id}))
        )
      )
      .subscribe((accounts) => {
        this.allUsers = accounts;
        const user = this.allUsers.find((u) => u.value === this.ownerId);
        if (
          user !== undefined &&
          (this.owner.value === undefined ||
            this.owner.value === null ||
            this.owner.value.id !== user.key)
        ) {
          this.taskOwnerService
            .getOwners(
              {page: "0", size: InfiniteScrollUtil.ELEMENTS_ON_PAGE},
              user.key
            )
            .subscribe((page) => {
              if (page.content.length > 0) {
                this.owner.patchValue(page.content[0]);
              }
            });
        }
      });
  }

  getCalendarTime(time: string): number {
    if (time == null) {
      return 0;
    }

    const taskDate = moment
      .utc(this.setTime(this.startTime.value, new Date(this.startDate.value)))
      .tz(moment.tz.guess())
      .startOf("day");
    return taskDate.diff(
      TimesUtil.fromUTCToCurrentTimeZone(time).startOf("day"),
      "days"
    );
  }

  isDateAfterTaskDate(time: string): boolean {
    if (time == null) {
      return false;
    }
    const taskDate = moment
      .utc(this.setTime(this.startTime.value, new Date(this.startDate.value)))
      .tz(moment.tz.guess())
      .startOf("day");
    return taskDate > TimesUtil.fromUTCToCurrentTimeZone(time).startOf("day");
  }

  onStartTimeChanged(hour: MatSelectChange): void {
    this.adjustEndDateTimeForTimeChange(hour.value);
  }

  private adjustEndDateTimeForTimeChange(hourValue: any): void {
    const hour2330 = 94;
    const minutes30 = 2;
    if (hourValue <= hour2330) {
      this.endTime.patchValue(+hourValue + minutes30);
      if (
        moment(this.endDate.value).format("MM/DD/YYYY") !==
        moment(this.startDate.value).format("MM/DD/YYYY")
      ) {
        this.endDate.patchValue(moment(this.endDate.value).add(-1, "days"));
      }
    } else {
      this.endTime.patchValue(hourValue - hour2330);
      if (
        moment(this.endDate.value).format("MM/DD/YYYY") ===
        moment(this.startDate.value).format("MM/DD/YYYY")
      ) {
        this.endDate.patchValue(moment(this.endDate.value).add(1, "days"));
      }
    }
  }

  onStartDateChanged($event: MatDatepickerInputEvent<unknown>): void {
    this.endDate.patchValue($event.value);
    this.adjustEndDateTimeForTimeChange(this.startTime.value);
  }

  onNotificationDateChanged($event: MatDatepickerInputEvent<unknown>): void {
    this.endDate.patchValue($event.value);
  }

  private setDefaultTitle(title: string): void {
    if (this.taskForm) {
      this.title.patchValue(title);
      this.currentCheckedPhoneActivityTitle = title;
    }
  }
}

interface KeyValue {
  key: string;
  value: number;
}

enum FormType {
  ADD = "ADD",
  EDIT = "EDIT",
}
