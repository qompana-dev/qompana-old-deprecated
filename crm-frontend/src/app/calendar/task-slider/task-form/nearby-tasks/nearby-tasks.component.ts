import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {NearbyTask, TaskAssociation} from '../../../task.model';
import {takeUntil} from 'rxjs/operators';
import {GlobalConfigService} from '../../../../global-config/global-config.service';
import {Subject} from 'rxjs';
import {SliderService} from '../../../../shared/slider/slider.service';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {CrmObjectType} from '../../../../shared/model/search.model';

@Component({
  selector: 'app-nearby-tasks',
  templateUrl: './nearby-tasks.component.html',
  styleUrls: ['./nearby-tasks.component.scss']
})
export class NearbyTasksComponent implements OnInit, OnDestroy {

  private componentDestroyed: Subject<void> = new Subject();
  timeDifferenceInD = 3;

  @Input() nearbyTasks: NearbyTask[] = [];
  @Input() associations: TaskAssociation[] = [];
  @Input() location: string;
  @Input() dateFrom: string;
  @Input() slider: SliderComponent;

  private availableAssociationTypes: CrmObjectType[] = [
    CrmObjectType.customer,
    CrmObjectType.contact,
    CrmObjectType.opportunity,
    CrmObjectType.lead
  ];

  constructor(private configService: GlobalConfigService,
              private sliderService: SliderService) {
  }

  ngOnInit(): void {
    this.configService.getGlobalParamValue('timeDifferenceInD')
      .pipe(takeUntil(this.componentDestroyed)).subscribe(
      (param) => this.timeDifferenceInD = +param.value);
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  closeSlider(): void {
    (this.slider) ? this.slider.close() : this.sliderService.closeSlider();
  }

  getAssociationNames(): string {
    return this.associations
      .filter(association => this.availableAssociationTypes.indexOf(association.objectType) !== -1)
      .map(association => association.label)
      .join(', ');
  }
}
