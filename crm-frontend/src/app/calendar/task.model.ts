import {Member} from './task-slider/task-form/add-members/member.model';
import {CrmObjectType} from '../shared/model/search.model';
import {AccountInfoDto} from '../person/person.model';

export enum TaskPriority {
  MEDIUM = 'MEDIUM',
  URGENT = 'URGENT',
}

export enum TaskState {
  CREATED = 'CREATED',
  IN_PROGRESS = 'IN_PROGRESS',
  SUSPENDED = 'SUSPENDED',
  FINISHED = 'FINISHED',
}

export enum TaskNotificationUnit {
  MINUTE = 'MINUTE',
  HOUR = 'HOUR',
  DAY = 'DAY',
  WEEK = 'WEEK',
}

export enum TaskActivityType {
  PHONE = 'PHONE',
  MEETING = 'MEETING',
  MAIL = 'MAIL',
  TASK = 'TASK',
}

export enum TaskTimeCategoryEnum {
  ALL = 'ALL',
  PLANNED = 'PLANNED',
  LATEST = 'LATEST',
  OLD = 'OLD',
}

export class NotificationPair {
  interval: number;
  unit: TaskNotificationUnit;

  constructor(interval: number, unit: TaskNotificationUnit) {
    this.interval = interval;
    this.unit = unit;
  }
}

export class Task {
  id: number;
  subject: string;
  priority: TaskPriority;
  state: TaskState;
  creator: number;
  place: string;
  longitude: string;
  latitude: string;
  dateTimeFrom: string;
  dateTimeTo: string;
  allDayEvent: boolean;
  owner: number;
  activityType: TaskActivityType;
  ownerInitials: string;
  description: string;
  notificationRequired: boolean;
  notificationDateTime: string;
  taskAssociations: TaskAssociation[];
  participants: Member[];
  color?: string;
  timeZone: string;
}

export class TaskDto {
  id: number;
  subject: string;
  creator: number;
  place: string;
  longitude: string;
  latitude: string;
  priority: TaskPriority;

  activityType: TaskActivityType;
  dateTimeFrom: string;
  dateTimeTo: string;

  allDayEvent: boolean;
  owner: number;
  description: string;

  timeZone: string;

  taskTimeCategoryEnum: TaskTimeCategoryEnum;
  state: TaskState;
}

export interface TaskAssociation {
  id?: number;
  objectId: number;
  objectType: CrmObjectType;
  label?: string;
  firm?: string;
  disabled?: boolean;
}

export class EventInfo {
  startDate: Date;
  allDayEvent: boolean;

  constructor(startDate: Date, allDayEvent: boolean) {
    this.startDate = startDate;
    this.allDayEvent = allDayEvent;
  }
}

export interface NearbyTaskCheck {
  activityType: TaskActivityType;
  latitude: string;
  longitude: string;
  dateTimeFrom: string;
}

export interface NearbyTask {
  accountId: number;
  taskDate: string;
  accountInfo: AccountInfoDto;
  daysDiff?: number;
  diffType?: 'after' | 'sameDay' | 'before';
}
