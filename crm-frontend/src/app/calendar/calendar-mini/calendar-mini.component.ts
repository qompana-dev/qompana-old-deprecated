import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {CalendarView} from 'angular-calendar';
import {LocaleService} from '../locale.service';
import * as cloneDeep from 'lodash/cloneDeep';
import {Subject, zip} from 'rxjs';
import {CalendarShareService} from '../calendar-share/calendar-share.service';
import {map, takeUntil} from 'rxjs/operators';
import {PersonService} from '../../person/person.service';
import {KeyValue} from '@angular/common';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MatCheckboxChange} from '@angular/material';

export class TaskUserChange {
  id: number | string;
  added: boolean;
  creator: boolean;

  constructor(id: number | string, added: boolean, creator: boolean) {
    this.id = id;
    this.added = added;
    this.creator = creator;
  }
}

@Component({
  selector: 'app-calendar-mini',
  templateUrl: './calendar-mini.component.html',
  styleUrls: ['./calendar-mini.component.scss']
})
export class CalendarMiniComponent implements OnInit, OnDestroy {

  @Input() viewDate: Date;
  @Input() refresh: Subject<any>;
  @Output() viewDateChanged = new EventEmitter<Date>();
  @Output() tasksUsersChanged = new EventEmitter<TaskUserChange>();
  readonly CalendarType = CalendarView;
  taskTypesForm: FormGroup;
  calendarUsers: KeyValue<string, number>[] = [];
  allUsers: KeyValue<string, number>[];

  locale = localStorage.getItem('locale');
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private localeService: LocaleService, private calendarShareService: CalendarShareService,
              private personService: PersonService, private formBuilder: FormBuilder) {
    this.createTaskTypesForm();
  }

  ngOnInit(): void {
    zip(
      this.personService.getAccountEmails()
        .pipe(map((accounts) => (accounts.map((account) => ({key: account.email, value: account.id}))))),
      this.calendarShareService.getCalendarSharesBySharedToUserId(+localStorage.getItem('loggedAccountId')))
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((array) => {
        this.initializeView(array);
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  printMonthWithoutYear(date: string): string {
    return date.replace(/[0-9]/g, '');
  }

  changeYear($event: any): void {
    const date = this.viewDate;
    date.setFullYear($event);
    this.viewDate = cloneDeep(date);
    this.viewDateChanged.emit(cloneDeep(date));
  }

  years(): number[] {
    const numbers = [];
    for (let i = 1950; i <= 2050; i++) {
      numbers.push(i);
    }
    return numbers;
  }

  onViewDateChange(): void {
    this.viewDateChanged.emit(this.viewDate);
  }

  onCheckboxClicked(event: MatCheckboxChange, id: string | number): void {
    if (id === 'myTasks') {
      this.tasksUsersChanged.emit(new TaskUserChange(+localStorage.getItem('loggedAccountId'), event.checked, false));
    } else if (id === 'myTasksAssignedToOthers') {
      this.tasksUsersChanged.emit(new TaskUserChange(+localStorage.getItem('loggedAccountId'), event.checked, true));
    } else if (id === 'myEmployeesTasks') {
      this.tasksUsersChanged.emit(new TaskUserChange(localStorage.getItem('navUserRole'), event.checked, false));
      if (!event.checked) {
        this.unClickAllCheckboxesExceptMyTasks();
      }
    } else {
      this.tasksUsersChanged.emit(new TaskUserChange(+id, event.checked, false));
    }
  }

  private unClickAllCheckboxesExceptMyTasks(): void {
    this.taskTypesForm.get('myTasksAssignedToOthers').patchValue(false);
    (this.taskTypesForm.get('calendarUsers') as FormArray).controls.forEach(e => e.patchValue(false));
  }

  private getUserEmailById(id: number): string {
    const foundUser = this.allUsers.find(user => user.value === id);
    return foundUser ? foundUser.key : undefined;
  }

  private createTaskTypesForm(): void {
    this.taskTypesForm = this.formBuilder.group({
      myTasks: [true],
      myTasksAssignedToOthers: [false],
      myEmployeesTasks: [false],
      calendarUsers: this.formBuilder.array([])
    });
  }

  private initializeView(array: any): void {
    this.allUsers = array[0];
    for (const share of array[1]) {
      const userEmail = this.getUserEmailById(share.sharingUserId);
      if (userEmail) {
        this.calendarUsers.push({key: userEmail, value: share.sharingUserId});
      }
    }
    const formArray = this.taskTypesForm.get('calendarUsers') as FormArray;
    this.calendarUsers.forEach(() => formArray.push(new FormControl(false)));
  }
}
