import { Injectable } from '@angular/core';
import * as moment from 'moment-timezone';
import { HttpClient } from '@angular/common/http';
import { NotificationService, NotificationType } from '../shared/notification.service';
import { Subject } from 'rxjs';
import { AccountConfiguration } from '../person/person.model';
import { UrlUtil } from '../shared/url.util';
import { MenuElementDTO } from '../navigation/sidenav/sidenav.model';

@Injectable({
  providedIn: 'root'
})
export class LocaleService {

  localeChanged = new Subject();

  constructor(private http: HttpClient, private notificationService: NotificationService) {
  }

  private updateLocalStorage(configuration: AccountConfiguration) {
    localStorage.setItem('locale', configuration.locale);
    localStorage.setItem('theme', configuration.theme);
    localStorage.setItem('dateFormat', configuration.dateFormat);
    localStorage.setItem('firstDayOfWeek', configuration.firstDayOfWeek);
    localStorage.setItem('firstHourOfDay', configuration.firstHourOfDay);
    localStorage.setItem('hourFormat', configuration.hourFormat);
    localStorage.setItem('menuOrder', configuration.menuOrder);
    moment.tz.setDefault(moment.tz.guess());
    moment.locale(configuration.locale);
    this.localeChanged.next();
  }

  initAccountConfiguration(accountId: number): void {
    this.http.get(`${UrlUtil.url}/crm-user-service/accounts/${accountId}/configuration`)
      .subscribe((configuration: AccountConfiguration) => {
        this.updateLocalStorage(configuration);
      },
        () => this.handleGetAccountConfigurationError());
  }

  updateMenuConfigs(accountId: number, newConfiguration: MenuElementDTO[]): void {
    localStorage.setItem('menuOrder', JSON.stringify(newConfiguration));
    this.localeChanged.next();
    this.http.put(`${UrlUtil.url}/crm-user-service/accounts/${accountId}/menu/configuration`, newConfiguration)
      .subscribe(
        resp => { },
        () => this.handleGetAccountConfigurationError(),
      );
  }

  private handleGetAccountConfigurationError(): void {
    this.notificationService.notify('locale.initError', NotificationType.ERROR);
  }

  private handleUpdateError(): void {
    this.notificationService.notify('locale.timeZone.updateError', NotificationType.ERROR);
  }

  private handleUpdateLocaleError(): void {
    this.notificationService.notify('locale.updateError', NotificationType.ERROR);
  }
}
