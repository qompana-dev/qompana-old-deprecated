"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CalendarMainComponent = void 0;
var core_1 = require("@angular/core");
var angular_calendar_1 = require("angular-calendar");
var all_events_component_1 = require("../dialog/all-events/all-events.component");
var rxjs_1 = require("rxjs");
var task_model_1 = require("../task.model");
var single_event_component_1 = require("../dialog/single-event/single-event.component");
var operators_1 = require("rxjs/operators");
var calendar_utils_1 = require("../utils/calendar-utils");
var task_utils_1 = require("../utils/task-utils");
var notification_service_1 = require("../../shared/notification.service");
var custom_event_title_formatter_1 = require("./custom-event-title-formatter");
var CalendarMainComponent = /** @class */ (function () {
    function CalendarMainComponent(calendarService, dialog, authService, loginService, personService, notificationService) {
        this.calendarService = calendarService;
        this.dialog = dialog;
        this.authService = authService;
        this.loginService = loginService;
        this.personService = personService;
        this.notificationService = notificationService;
        this.componentDestroyed = new rxjs_1.Subject();
        this.CalendarType = angular_calendar_1.CalendarView;
        this.calendarType = angular_calendar_1.CalendarView.Week;
        this.viewDate = new Date();
        this.events = [];
        this.refresh = new rxjs_1.Subject();
        this.firstDayOfWeek = +localStorage.getItem('firstDayOfWeek');
        this.locale = localStorage.getItem('locale');
        this.timeZone = localStorage.getItem('timeZone');
        this.miniCalendarOpened = false;
        this.activityTypeEnum = task_model_1.TaskActivityType;
        this.HEADER_HEIGHT = 19;
        this.HOUR_SECTION_HEIGHT = 60;
        this.HOUR_SEGMENTS = 4;
        this.HOUR_SEGMENTS_HEIGHT = 20;
    }
    CalendarMainComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscribeForTaskEdition();
        this.loginService.updateLocale();
        this.getAccountEmails()
            .subscribe(function (users) { return _this.handleGetUsersSuccess(users); }, function () { return _this.notificationService.notify('calendar.notifications.getUsersError', notification_service_1.NotificationType.ERROR); });
        this.scrollToHour(+localStorage.getItem('firstHourOfDay'));
    };
    CalendarMainComponent.prototype.scrollToHour = function (hour) {
        var _this = this;
        setTimeout(function () { return _this.calendarContainer.nativeElement.scrollTop = _this.HEADER_HEIGHT + _this.HOUR_SECTION_HEIGHT * hour; }, 100);
    };
    CalendarMainComponent.prototype.ngOnDestroy = function () {
        this.componentDestroyed.next();
        this.componentDestroyed.complete();
    };
    CalendarMainComponent.prototype.openAllEventsDialog = function (events, $event) {
        $event.stopPropagation();
        this.dialog.open(all_events_component_1.AllEventsComponent, {
            data: events
        });
    };
    CalendarMainComponent.prototype.openSingleEventDialogCustom = function (event, $event) {
        $event.stopPropagation();
        this.openEventInfoDialog(event);
    };
    CalendarMainComponent.prototype.openSingleEventDialog = function (_a) {
        var event = _a.event;
        this.openEventInfoDialog(event);
    };
    CalendarMainComponent.prototype.getTime = function (date) {
        return calendar_utils_1.CalendarUtils.getTime(date);
    };
    CalendarMainComponent.prototype.setView = function (view) {
        this.calendarType = view;
        if (view === angular_calendar_1.CalendarView.Day || view === angular_calendar_1.CalendarView.Week) {
            this.scrollToHour(+localStorage.getItem('firstHourOfDay'));
        }
    };
    CalendarMainComponent.prototype.getEventsByOwnerId = function (ownerId) {
        var _this = this;
        if (!this.authService.isPermissionPresent('CalendarComponent.tasks', 'i')) {
            this.calendarService.getEventsByOwnerId(ownerId).subscribe(function (tasks) { return _this.handleGetSuccess(tasks); });
        }
    };
    CalendarMainComponent.prototype.getEventsByCreatorId = function (creatorId) {
        var _this = this;
        if (!this.authService.isPermissionPresent('CalendarComponent.tasks', 'i')) {
            this.calendarService.getEventsByCreatorId(creatorId).subscribe(function (tasks) { return _this.handleGetSuccess(tasks); });
        }
    };
    CalendarMainComponent.prototype.getEventsByRoleName = function (roleName) {
        var _this = this;
        if (!this.authService.isPermissionPresent('CalendarComponent.tasks', 'i')) {
            this.calendarService.getEventsByRoleName(roleName).subscribe(function (tasks) { return _this.handleGetSuccess(tasks); });
        }
    };
    CalendarMainComponent.prototype.onDragTimesChanged = function (change) {
        var _this = this;
        var task = task_utils_1.TaskUtils.appendChangesToTask(change);
        this.calendarService.updateEvent(task).subscribe(function () { return _this.refresh.next(); });
        this.refresh.next();
    };
    CalendarMainComponent.prototype.refreshAddedEvent = function (taskId) {
        this.getEventAndAddToCalendar(taskId);
    };
    CalendarMainComponent.prototype.refreshEditedEvent = function (taskId) {
        this.events = this.events.filter(function (element) { return element.meta.id !== taskId; });
        this.getEventAndAddToCalendar(taskId);
    };
    CalendarMainComponent.prototype.handleGetUsersSuccess = function (users) {
        this.userMap = users;
        this.getEventsByOwnerId(+localStorage.getItem('loggedAccountId'));
    };
    CalendarMainComponent.prototype.getEventAndAddToCalendar = function (taskId) {
        var _this = this;
        if (!this.authService.isPermissionPresent('CalendarComponent.tasks', 'i')) {
            this.calendarService.getEvent(taskId)
                .subscribe(function (task) {
                if (+localStorage.getItem('loggedAccountId') === task.owner) {
                    task.ownerInitials = _this.userMap.get(task.owner);
                    _this.events.push(task_utils_1.TaskUtils.convertTaskToEvent(task));
                }
                _this.refresh.next();
            });
        }
    };
    CalendarMainComponent.prototype.openAddSlider = function ($event, taskActivityType) {
        if (!this.authService.isPermissionPresent('CalendarComponent.addTask', 'w')) {
            return;
        }
        if ($event.date) {
            this.updatedEventInfo = new task_model_1.EventInfo($event.date, false);
        }
        else if ($event.day) {
            this.updatedEventInfo = new task_model_1.EventInfo($event.day.date, true);
        }
        this.selectedActivityType = taskActivityType;
        this.addSlider.open();
    };
    CalendarMainComponent.prototype.openShareSlider = function () {
        this.shareSlider.open();
    };
    CalendarMainComponent.prototype.refreshMainCalendar = function (event) {
        this.viewDate = event;
        this.refresh.next();
    };
    CalendarMainComponent.prototype.refreshTasks = function (change) {
        if (change.added) {
            this.refreshWhenAdded(change);
        }
        else {
            this.refreshWhenDeleted(change);
        }
    };
    CalendarMainComponent.prototype.addEventsToCalendar = function (tasks) {
        var events = [];
        var _loop_1 = function (task) {
            task.ownerInitials = this_1.userMap.get(task.owner);
            if (!this_1.events.find(function (e) { return e.meta.id === task.id; })) {
                events.push(task_utils_1.TaskUtils.convertTaskToEvent(task));
            }
        };
        var this_1 = this;
        for (var _i = 0, tasks_1 = tasks; _i < tasks_1.length; _i++) {
            var task = tasks_1[_i];
            _loop_1(task);
        }
        this.events = this.events.concat(events);
    };
    CalendarMainComponent.prototype.handleGetSuccess = function (tasks) {
        this.addEventsToCalendar(tasks);
        this.refresh.next();
    };
    CalendarMainComponent.prototype.openEventInfoDialog = function (event) {
        var _this = this;
        var dialogRef = this.dialog.open(single_event_component_1.SingleEventComponent, {
            data: event.meta
        });
        dialogRef.componentInstance.eventDeleted.subscribe(function (id) { return _this.deleteFromCalendar(id); });
    };
    CalendarMainComponent.prototype.deleteFromCalendar = function (id) {
        this.events = this.events.filter(function (element) { return element.meta.id !== id; });
        this.refresh.next();
    };
    CalendarMainComponent.prototype.refreshWhenDeleted = function (change) {
        if (typeof change.id === 'string') {
            this.events = this.events.filter(function (e) { return e.meta.owner === +localStorage.getItem('loggedAccountId'); });
        }
        else if (change.creator) {
            this.events = this.events.filter(function (e) { return !(e.meta.owner !== change.id && e.meta.creator === change.id); });
        }
        else {
            this.events = this.events.filter(function (e) { return e.meta.owner !== change.id; });
        }
    };
    CalendarMainComponent.prototype.refreshWhenAdded = function (change) {
        if (typeof change.id === 'string') {
            this.getEventsByRoleName(change.id);
        }
        else if (change.creator) {
            this.getEventsByCreatorId(+change.id);
        }
        else {
            this.getEventsByOwnerId(+change.id);
        }
    };
    CalendarMainComponent.prototype.subscribeForTaskEdition = function () {
        var _this = this;
        this.calendarService.taskEdited
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function () { return _this.editSlider.open(); });
    };
    CalendarMainComponent.prototype.getAccountEmails = function () {
        return this.personService.getAccountEmails()
            .pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.map(function (emails) { return new Map(emails.map(function (i) { return [i.id, "" + i.name.charAt(0) + i.surname.charAt(0)]; })); }));
    };
    __decorate([
        core_1.ViewChild('addSlider')
    ], CalendarMainComponent.prototype, "addSlider");
    __decorate([
        core_1.ViewChild('editSlider')
    ], CalendarMainComponent.prototype, "editSlider");
    __decorate([
        core_1.ViewChild('shareSlider')
    ], CalendarMainComponent.prototype, "shareSlider");
    __decorate([
        core_1.ViewChild('calendarContainer')
    ], CalendarMainComponent.prototype, "calendarContainer");
    CalendarMainComponent = __decorate([
        core_1.Component({
            selector: 'app-calendar',
            templateUrl: './calendar-main.component.html',
            styleUrls: ['./calendar-main.component.scss'],
            providers: [
                {
                    provide: angular_calendar_1.CalendarEventTitleFormatter,
                    useClass: custom_event_title_formatter_1.CustomEventTitleFormatter
                }
            ]
        })
    ], CalendarMainComponent);
    return CalendarMainComponent;
}());
exports.CalendarMainComponent = CalendarMainComponent;
