import {CalendarNativeDateFormatter, DateFormatterParams} from 'angular-calendar';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomDateFormatter extends CalendarNativeDateFormatter {

  public dayViewHour({date, locale}: DateFormatterParams): string {
    return new Intl.DateTimeFormat(locale, {hour: 'numeric'}).format(date);
  }

  public weekViewHour({date, locale}: DateFormatterParams): string {
    return new Intl.DateTimeFormat(locale, {hour: 'numeric'}).format(date);
  }
}
