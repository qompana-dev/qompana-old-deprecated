import { Router } from "@angular/router";
import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  CalendarEvent,
  CalendarEventTimesChangedEvent,
  CalendarEventTitleFormatter,
  CalendarView,
} from "angular-calendar";
import { CalendarService } from "../calendar.service";
import { MatDialog } from "@angular/material";
import { AllEventsComponent } from "../dialog/all-events/all-events.component";
import { Observable, Subject } from "rxjs";
import { EventInfo, Task, TaskActivityType } from "../task.model";
import { SingleEventComponent } from "../dialog/single-event/single-event.component";
import { SliderComponent } from "../../shared/slider/slider.component";
import { map, takeUntil } from "rxjs/operators";
import { CalendarUtils } from "../utils/calendar-utils";
import { TaskUtils } from "../utils/task-utils";
import { TaskUserChange } from "../calendar-mini/calendar-mini.component";
import {
  NotificationService,
  NotificationType,
} from "../../shared/notification.service";
import { PersonService } from "../../person/person.service";
import { CustomEventTitleFormatter } from "./custom-event-title-formatter";
import { LoginService } from "../../login/login.service";
import { AuthService } from "../../login/auth/auth.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-calendar",
  templateUrl: "./calendar-main.component.html",
  styleUrls: ["./calendar-main.component.scss"],
  providers: [
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter,
    },
  ],
})
export class CalendarMainComponent implements OnInit, OnDestroy {
  @ViewChild("addSlider") addSlider: SliderComponent;
  @ViewChild("editSlider") editSlider: SliderComponent;
  @ViewChild("shareSlider") shareSlider: SliderComponent;
  @ViewChild("calendarContainer") calendarContainer: ElementRef;
  private componentDestroyed: Subject<void> = new Subject();

  readonly CalendarType = CalendarView;
  calendarType: CalendarView = CalendarView.Week;
  viewDate: Date = new Date();
  events: CalendarEvent[] = [];
  refresh: Subject<any> = new Subject();
  updatedEventInfo: EventInfo;
  userMap: Map<number, string>;
  crmObject;

  firstDayOfWeek = +localStorage.getItem("firstDayOfWeek");
  locale = localStorage.getItem("locale");
  miniCalendarOpened = false;

  activityTypeEnum = TaskActivityType;
  selectedActivityType: TaskActivityType;

  readonly HEADER_HEIGHT = 19;
  readonly HOUR_SECTION_HEIGHT = 60;
  readonly HOUR_SEGMENTS = 4;
  readonly HOUR_SEGMENTS_HEIGHT = 20;

  constructor(
    private calendarService: CalendarService,
    public dialog: MatDialog,
    public authService: AuthService,
    private translateService: TranslateService,
    public loginService: LoginService,
    private personService: PersonService,
    private notificationService: NotificationService,
    private router: Router
  ) {
    this.crmObject = this.router.getCurrentNavigation().extras.state;
  }

  ngOnInit(): void {
    this.subscribeForTaskEdition();
    this.loginService.updateLocale();

    this.getAccountEmails().subscribe(
      (users) => this.handleGetUsersSuccess(users),
      () =>
        this.notificationService.notify(
          "calendar.notifications.getUsersError",
          NotificationType.ERROR
        )
    );

    this.scrollToHour(+localStorage.getItem("firstHourOfDay"));
  }

  private scrollToHour(hour: number): void {
    setTimeout(
      () =>
        (this.calendarContainer.nativeElement.scrollTop =
          this.HEADER_HEIGHT + this.HOUR_SECTION_HEIGHT * hour),
      100
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  openAllEventsDialog(events: CalendarEvent[], $event: MouseEvent): void {
    $event.stopPropagation();
    this.dialog.open(AllEventsComponent, {
      data: events,
    });
  }

  openSingleEventDialogCustom(event: CalendarEvent, $event: MouseEvent): void {
    $event.stopPropagation();
    this.openEventInfoDialog(event);
  }

  openSingleEventDialog({ event }: { event: CalendarEvent }): void {
    this.openEventInfoDialog(event);
  }

  getTime(date: Date): string {
    return CalendarUtils.getTime(date);
  }

  setView(view: CalendarView): void {
    this.calendarType = view;
    if (view === CalendarView.Day || view === CalendarView.Week) {
      this.scrollToHour(+localStorage.getItem("firstHourOfDay"));
    }
  }

  getEventsByOwnerId(ownerId: number): void {
    if (!this.authService.isPermissionPresent("CalendarComponent.tasks", "i")) {
      this.calendarService
        .getEventsByOwnerId(ownerId)
        .subscribe((tasks) => this.handleGetSuccess(tasks));
    }
  }

  getEventsByCreatorId(creatorId: number): void {
    if (!this.authService.isPermissionPresent("CalendarComponent.tasks", "i")) {
      this.calendarService
        .getEventsByCreatorId(creatorId)
        .subscribe((tasks) => this.handleGetSuccess(tasks));
    }
  }

  getEventsByRoleName(roleName: string): void {
    if (!this.authService.isPermissionPresent("CalendarComponent.tasks", "i")) {
      this.calendarService
        .getEventsByRoleName(roleName)
        .subscribe((tasks) => this.handleGetSuccess(tasks));
    }
  }

  onDragTimesChanged(change: CalendarEventTimesChangedEvent): void {
    const task = TaskUtils.appendChangesToTask(change);
    this.calendarService.updateEvent(task).subscribe(() => this.refresh.next());
    this.refresh.next();
  }

  refreshAddedEvent(taskId: number): void {
    this.getEventAndAddToCalendar(taskId);
  }

  refreshEditedEvent(taskId: number): void {
    this.events = this.events.filter((element) => element.meta.id !== taskId);
    this.getEventAndAddToCalendar(taskId);
  }

  private handleGetUsersSuccess(users: any): void {
    this.userMap = users;

    if (this.crmObject) {
      this.getCrmObjectEvents(this.crmObject);
    } else {
      this.getEventsByOwnerId(+localStorage.getItem("loggedAccountId"));
    }
  }

  private getEventAndAddToCalendar(taskId: number): void {
    if (!this.authService.isPermissionPresent("CalendarComponent.tasks", "i")) {
      const createdInTimezoneText = this.translateService.instant(
        "calendar.createdInTimezone"
      );
      this.calendarService.getEvent(taskId).subscribe((task: Task) => {
        if (+localStorage.getItem("loggedAccountId") === task.owner) {
          task.ownerInitials = this.userMap.get(task.owner);
          this.events.push(
            TaskUtils.convertTaskToEvent(task, createdInTimezoneText)
          );
        }
        this.refresh.next();
      });
    }
  }

  openAddSlider($event: any, taskActivityType?: TaskActivityType): void {
    if (
      !this.authService.isPermissionPresent("CalendarComponent.addTask", "w") || !!this.crmObject
    ) {
      return;
    }
    if ($event.date) {
      this.updatedEventInfo = new EventInfo($event.date, false);
    } else if ($event.day) {
      this.updatedEventInfo = new EventInfo($event.day.date, true);
    }
    this.selectedActivityType = taskActivityType;
    this.addSlider.open();
  }

  openShareSlider(): void {
    this.shareSlider.open();
  }

  refreshMainCalendar(event: Date): void {
    this.viewDate = event;
    this.refresh.next();
  }

  refreshTasks(change: TaskUserChange): void {
    if (change.added) {
      this.refreshWhenAdded(change);
    } else {
      this.refreshWhenDeleted(change);
    }
  }

  private addEventsToCalendar(tasks: Task[]): void {
    const createdInTimezoneText = this.translateService.instant(
      "calendar.createdInTimezone"
    );
    const events = [];
    for (const task of tasks) {
      task.ownerInitials = this.userMap.get(task.owner);
      if (!this.events.find((e) => e.meta.id === task.id)) {
        events.push(TaskUtils.convertTaskToEvent(task, createdInTimezoneText));
      }
    }
    this.events = this.events.concat(events);
  }

  private handleGetSuccess(tasks: Task[]): void {
    this.addEventsToCalendar(tasks);
    this.refresh.next();
  }

  private openEventInfoDialog(event: CalendarEvent): void {
    const dialogRef = this.dialog.open(SingleEventComponent, {
      data: event.meta,
    });
    dialogRef.componentInstance.eventDeleted.subscribe((id) =>
      this.deleteFromCalendar(id)
    );
  }

  deleteFromCalendar(id: number): any {
    this.events = this.events.filter((element) => element.meta.id !== id);
    this.refresh.next();
  }

  private refreshWhenDeleted(change: TaskUserChange): void {
    if (typeof change.id === "string") {
      this.events = this.events.filter(
        (e) => e.meta.owner === +localStorage.getItem("loggedAccountId")
      );
    } else if (change.creator) {
      this.events = this.events.filter(
        (e) => !(e.meta.owner !== change.id && e.meta.creator === change.id)
      );
    } else {
      this.events = this.events.filter((e) => e.meta.owner !== change.id);
    }
  }

  private refreshWhenAdded(change: TaskUserChange): void {
    if (typeof change.id === "string") {
      this.getEventsByRoleName(change.id);
    } else if (change.creator) {
      this.getEventsByCreatorId(+change.id);
    } else {
      this.getEventsByOwnerId(+change.id);
    }
  }

  private subscribeForTaskEdition(): void {
    this.calendarService.taskEdited
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.editSlider.open());
  }

  private getCrmObjectEvents(object) {
    if (!this.authService.isPermissionPresent("CalendarComponent.tasks", "i")) {
      this.calendarService
        .getEventsByCrmObject(object.type, object.id)
        .subscribe((tasks) => this.handleGetSuccess(tasks));
    }
  }

  private getAccountEmails(): Observable<any> {
    return this.personService.getAccountEmails().pipe(
      takeUntil(this.componentDestroyed),
      map(
        (emails) =>
          new Map<number, string>(
            emails.map(
              (i) =>
                [i.id, `${i.name.charAt(0)}${i.surname.charAt(0)}`] as [
                  number,
                  string
                ]
            )
          )
      )
    );
  }
}
