import {CalendarEventTitleFormatter, CalendarEvent} from 'angular-calendar';

export class CustomEventTitleFormatter extends CalendarEventTitleFormatter {

  constructor() {
    super();
  }

  week(event: CalendarEvent): string {
    return `<b class="mr-2">${event.meta.ownerInitials}</b>
              <span class="mr-2">${event.title}</span>`;
  }

  day(event: CalendarEvent): string {
    return `<b class="mr-2">${event.meta.ownerInitials}</b>
              <span class="mr-2">${event.title}</span>`;
  }
}
