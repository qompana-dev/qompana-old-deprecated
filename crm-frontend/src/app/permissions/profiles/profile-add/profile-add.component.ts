import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {ProfileSave} from '../profile.model';
import {ProfileService} from '../profile.service';
import {Router} from '@angular/router';
import {ProfileFormComponent} from '../profile-form/profile-form.component';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {takeUntil} from 'rxjs/operators';
import {GlobalConfigService} from '../../../global-config/global-config.service';

@Component({
  selector: 'app-profile-add',
  templateUrl: './profile-add.component.html',
  styleUrls: ['./profile-add.component.scss']
})
export class ProfileAddComponent implements OnInit, OnDestroy {
  profileSave: ProfileSave;

  @ViewChild('profileFormComponent') profileFormComponent: ProfileFormComponent;
  @Input() slider: SliderComponent;
  @Output() profileAdded: EventEmitter<void> = new EventEmitter<void>();

  readonly profileError: ErrorTypes = {
    base: 'permissions.profile.add.notification.',
    defaultText: 'undefinedError',
    errors: [{
      code: 403,
      text: 'noPermission'
    }, {
      code: 400,
      text: 'validationError'
    }
    ]
  };
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private $router: Router,
              private globalConfigService: GlobalConfigService,
              private notificationService: NotificationService,
              private profileService: ProfileService) {
  }

  ngOnInit(): void {
    this.refreshTemplate();
    this.slider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.profileFormComponent.clear();
        this.refreshTemplate();
      }
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  saveForm(): void {
    if (!this.profileFormComponent.isFormValid) {
      this.profileFormComponent.markAsTouched();
      return;
    }

    this.profileService.saveProfile(this.profileFormComponent.profileSave).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(() => this.handleAddSuccess(),
      (err) => this.notificationService.notifyError(this.profileError, err.status)
    );
  }

  closeSlider(): void {
    this.slider.close();
  }

  private handleAddSuccess(): void {
    this.notificationService.notify('permissions.profile.edit.notification.profileSaved', NotificationType.SUCCESS);
    this.globalConfigService.updateAdministrationsList();
    this.slider.close();
    this.profileAdded.emit();
  }

  private refreshTemplate(): void {
    this.profileService.getNewProfileTemplate().pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe((value: ProfileSave) => {
      this.profileSave = value;
    });
  }
}
