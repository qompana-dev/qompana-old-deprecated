import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {ProfileSave} from '../profile.model';
import {ActivatedRoute, Router} from '@angular/router';
import {ProfileService} from '../profile.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {TranslateService} from '@ngx-translate/core';
import {ProfileFormComponent} from '../profile-form/profile-form.component';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {takeUntil} from 'rxjs/operators';
import {GlobalConfigService} from '../../../global-config/global-config.service';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit, OnDestroy {
  _profileId: number;
  profileSave: ProfileSave;

  @ViewChild('profileFormComponent') profileFormComponent: ProfileFormComponent;
  @Input() slider: SliderComponent;
  @Output() profileAdded: EventEmitter<void> = new EventEmitter<void>();

  @Input() set profileId(profileId: number) {
    this._profileId = profileId;
    this.loadProfile(profileId);
  }

  readonly profileError: ErrorTypes = {
    base: 'permissions.profile.edit.notification.',
    defaultText: 'undefinedError',
    errors: [{
      code: 403,
      text: 'noPermission'
    }, {
      code: 400,
      text: 'validationError'
    }, {
      code: 404,
      text: 'profileNotFound'
    }
    ]
  };
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private $router: Router,
              private notificationService: NotificationService,
              private translateService: TranslateService,
              private globalConfigService: GlobalConfigService,
              private activatedRoute: ActivatedRoute,
              private profileService: ProfileService) {
  }

  ngOnInit(): void {
    this.slider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.profileFormComponent.clear();
      }
    );
  }

  private loadProfile(profileId: number): void {
    if (profileId) {
      this.profileService.getProfile(profileId).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        (value: ProfileSave) => this.profileSave = value,
        (err) => this.notificationService.notifyError(this.profileError, err.status)
      );
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  saveForm(): void {
    if (!this.profileFormComponent.isFormValid) {
      this.profileFormComponent.markAsTouched();
      return;
    }

    this.profileService.updateProfile(this._profileId, this.profileFormComponent.profileSave)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(() => {
        this.handleUpdateSuccess();
      }, (err) => this.notificationService.notifyError(this.profileError, err.status)
    );
  }

  private handleUpdateSuccess(): void {
    this.notificationService.notify('permissions.profile.edit.notification.profileSaved', NotificationType.SUCCESS);
    this.globalConfigService.updateAdministrationsList();
    this.closeSlider();
    this.profileAdded.emit();
  }

  closeSlider(): void {
    this.slider.close();
  }
}
