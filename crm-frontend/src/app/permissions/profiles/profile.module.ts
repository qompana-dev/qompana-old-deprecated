import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SharedModule} from '../../shared/shared.module';
import {ProfileListComponent} from './profile-list/profile-list.component';
import {ProfileEditComponent} from './profile-edit/profile-edit.component';
import {ProfileFormComponent} from './profile-form/profile-form.component';
import {ProfileAddComponent} from './profile-add/profile-add.component';
import {PermissionSwitchComponent} from './profile-form/permission-switch/permission-switch.component';
import {FilterPropertyPipe} from './profile-form/filter-property.pipe';

@NgModule({
  declarations: [
    ProfileAddComponent,
    ProfileListComponent,
    ProfileEditComponent,
    ProfileFormComponent,
    PermissionSwitchComponent,
    FilterPropertyPipe,
  ],
  exports: [
    ProfileListComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class ProfileModule { }
