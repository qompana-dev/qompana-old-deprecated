import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filterProperty'
})
export class FilterPropertyPipe implements PipeTransform {

  transform(values: any[], property: string, value: any, isEqual: boolean): any[] {
    return values.filter((item) => {
      const result = this.getValue(item, property) === value;
      return (isEqual) ? result : !result;
    });
  }

  private getValue(item: any, property: string): any {
    if (property.indexOf('.') !== -1) {
      const keys: string[] = property.split('.');
      return this.getValue(item[keys[0]], keys.slice(1, keys.length).join('.'))
    }
    return item[property];
  }

}
