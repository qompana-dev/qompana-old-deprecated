import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProfileSave} from '../profile.model';
import {ModuleCategory, ModulePermission, Permission, PermissionTypeEnum} from '../../../login/auth/auth.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatCheckboxChange} from '@angular/material';
import {FormValidators} from '../../../shared/form/form-validators';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent implements OnInit {
  groupedList: Map<number, ModulePermission[]> = new Map<number, ModulePermission[]>();
  categories: ModuleCategory[] = [];

  profileSaveToEmit: ProfileSave = {};

  readonly conf = {
    states: [
      {index: 1, value: 'INVISIBLE', color: '#000', bgColor: '#e8eaeb'},
      {index: 2, value: 'READ_ONLY', color: '#F0443E', bgColor: '#e8eaeb'},
      {index: 3, value: 'WRITE', color: '#2977FF', bgColor: '#e8eaeb'}
    ]
  };

  readonly actionConf = {
    states: [
      {index: 1, value: 'INVISIBLE', color: '#000', bgColor: '#e8eaeb'},
      {index: 2, value: 'WRITE', color: '#2977FF', bgColor: '#e8eaeb'}
    ]
  };

  readonly = false;

  @Output() valueChange: EventEmitter<ProfileSave> = new EventEmitter();

  @Input('profileSave') set profileSave(profileSave: ProfileSave) {
    if (!profileSave) {
      return;
    }
    this.mapProfileSave(profileSave);
  }

  profileForm = new FormGroup({
    id: new FormControl({value: '', disabled: true}),
    name: new FormControl('', [Validators.required, FormValidators.whitespace, Validators.maxLength(100)]),
    description: new FormControl('', Validators.maxLength(500))
  });

  constructor() {
  }

  ngOnInit(): void {
    this.clear();
  }

  permissionChange(permission: Permission, $event: string): void {
    permission.state = $event;
    this.emitChanges();
  }

  checkCheckboxInModule(checkboxType: string, modulePermission: ModulePermission, $event: MatCheckboxChange): void {
    const availableTypes = ['view', 'create', 'delete', 'edit', 'all'];
    if (availableTypes.indexOf(checkboxType) === -1) {
      console.error('value ' + checkboxType + ' not found in ModulePermission');
      return;
    }
    modulePermission[checkboxType] = $event.checked;
    if (checkboxType === 'all' && $event.checked) {
      availableTypes.filter(c => c !== 'all').forEach(c => this.checkCheckboxInModule(c, modulePermission, $event));
      return;
    }

    if ($event.checked && checkboxType !== 'view') {
      modulePermission.view = true;
    }
    if (!$event.checked && checkboxType === 'view') {
      availableTypes.filter(c => c !== 'view' && c !== 'all').forEach(c => this.checkCheckboxInModule(c, modulePermission, $event));
    }
    if (!modulePermission['all'] && availableTypes.filter(item => item !== 'all' && !modulePermission[item]).length === 0) {
      modulePermission['all'] = true;
    }
    if (modulePermission['all'] && availableTypes.filter(item => item !== 'all' && !modulePermission[item]).length !== 0) {
      modulePermission['all'] = false;
    }
    this.emitChanges();
  }

  modulePermissionsContain(modulePermission: any, field: boolean): boolean {
    const type = PermissionTypeEnum[((field) ? PermissionTypeEnum.FIELD : PermissionTypeEnum.TOOL)];
    if (!modulePermission || !modulePermission.permissions) {
      return false;
    }
    for (let i = 0; i < modulePermission.permissions.length; i++) {
      if (modulePermission.permissions[i].webControl.type === type) {
        return true;
      }
    }
    return false;
  }

  get profileSave(): ProfileSave {
    return this.profileSaveToEmit;
  }

  get isFormValid(): boolean {
    return this.profileForm.valid;
  }

  public markAsTouched(): void {
    (Object as any).values(this.profileForm.controls).forEach(control => {
      control.markAsTouched();
    });
  }

  public clear(): void {
    this.profileForm = new FormGroup({
      id: new FormControl({value: '', disabled: true}),
      name: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      description: new FormControl('', Validators.maxLength(500))
    });
    this.categories = [];
    this.groupedList = new Map<number, ModulePermission[]>();
  }

  private mapProfileSave(profileSave: ProfileSave): void {
    const categoryTemp: Map<number, ModuleCategory> = new Map<number, ModuleCategory>();
    profileSave.modulePermissions.map((item) => {
      let modulePermissions: ModulePermission[] = this.groupedList.get(item.module.category.id);
      if (!modulePermissions) {
        modulePermissions = [];
      }
      item['all'] = item.create && item.edit && item.delete && item.view;
      modulePermissions.push(item);
      this.groupedList.set(item.module.category.id, modulePermissions);
      categoryTemp.set(item.module.category.id, item.module.category);
    });
    this.categories = Array.from(categoryTemp.values());

    this.profileForm.get('id').setValue(profileSave.id);
    this.profileForm.get('name').setValue(profileSave.name);
    this.profileForm.get('description').setValue(profileSave.description);

    this.profileSaveToEmit = {
      id: profileSave.id,
      name: profileSave.name,
      description: profileSave.description,
      modulePermissions: []
    };
    Array.from(this.groupedList.values())
      .forEach((list: ModulePermission[]) => list.forEach((item) => this.profileSaveToEmit.modulePermissions.push(item)));
  }

  private emitChanges(): void {
    this.valueChange.emit(this.profileSaveToEmit);
  }
}
