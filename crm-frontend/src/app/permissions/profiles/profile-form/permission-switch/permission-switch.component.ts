import {Component, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-permission-switch',
  templateUrl: './permission-switch.component.html',
  styleUrls: ['./permission-switch.component.scss']
})
export class PermissionSwitchComponent implements OnInit {
  private _conf: Switch;
  private _value: string;
  width = 0;
  private readonly SELECTED_WIDTH = 14; // 2 difference

  selected: IconState;

  @Input() readonly = false;
  @Input() title = '';

  marginLeft = 0;

  @Output() valueChange: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.init();
  }

  get conf(): Switch {
    return this._conf;
  }

  @Input('conf')
  set conf(_conf: Switch) {
      this._conf = _conf;
      this.init();
  }

  @Input('value')
  set value(_value: string) {
    this._value = _value;
    this.changeSelected();
    this.calculateMarginLeft();
  }

  private init() {
    this.width = this.SELECTED_WIDTH * this._conf.states.length;
    this.fixIncorrectIndexes();
    this.changeSelected();
    this.calculateMarginLeft();
  }

  private changeSelected() {
    let newValue = this._conf.states.find(c => this._value === c.value);
    if (!newValue) { // if value not selected
      newValue = this._conf.states.find(c => 0 === c.index);
    }
    this.selected = newValue;
  }

  private calculateMarginLeft() {
    this.marginLeft = (this.selected.index * this.SELECTED_WIDTH) - 2; //circle 4px more than background (2 - top, 2 bottom)
  }

  private fixIncorrectIndexes() {
    let currentIndex = 0;
    this._conf.states = this._conf.states.sort((a, b) => a.index > b.index ? 1 : -1)
      .map((c) => {
        c.index = currentIndex;
        currentIndex+=1;
        return c;
      });
  }

  private elementChange(prev: boolean) {
    const currentIndex = this.selected.index;
    const indexToSearch = (prev) ? currentIndex - 1 : currentIndex + 1;
    const newSelected = this._conf.states.find((item) => item.index === indexToSearch);
    if (newSelected) {
      this.selected = newSelected;
      this.calculateMarginLeft();
      this.valueChange.emit(this.selected.value);
    }
  }

  @HostListener('click', ['$event'])
  public onClick($event) {
    if (this.readonly) {
      return;
    }
    if ($event.target.classList.contains('selected')) {
      this.elementChange(false);
    }
    if ($event.target.classList.contains('parent-element')) {
      if ($event.offsetX < this.marginLeft) {
        this.elementChange(true);
      }
      if ($event.offsetX > this.marginLeft + this.SELECTED_WIDTH + 4) {
        this.elementChange(false);
      }
    }
  }
}

export interface Switch {
  states: IconState[];
}

export interface IconState {
  index: number;
  value: any;

  color?: string;
  bgColor?: string;
}
