import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {ProfileService} from '../profile.service';
import {Profile} from '../profile.model';
import {MatDialog, MatTable, Sort} from '@angular/material';
import {takeUntil, throttleTime} from 'rxjs/operators';
import {
  SimpleDialogButton,
  SimpleDialogComponent,
  SimpleDialogData
} from '../../../shared/dialog/simple-dialog.component';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {Page, PageRequest} from '../../../shared/pagination/page.model';
import {InfiniteScrollUtil} from '../../../shared/pagination/infinite-scroll.util';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {GlobalConfigService} from '../../../global-config/global-config.service';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.scss']
})
export class ProfileListComponent implements OnInit, OnDestroy {

  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild('addProfileSlider') addProfileSlider: SliderComponent;
  @ViewChild('editProfileSlider') editProfileSlider: SliderComponent;
  readonly columns: string[] = ['id', 'name', 'description', 'action'];
  profilesPage: Page<Profile>;
  private componentDestroyed: Subject<void> = new Subject();
  private currentPage: number;
  private currentSort = 'id,asc';
  collapsed = true;
  profileId: number;
  profileName: string;

  readonly profileError: ErrorTypes = {
    base: 'permissions.profile.list.notification.',
    defaultText: 'undefinedError',
    errors: [ {
      code: 403,
      text: 'noPermission'
    }, {
      code: 400,
      text: 'validationError'
    }, {
      code: 404,
      text: 'profileNotFound'
    }
    ]
  };

  constructor(private $router: Router,
              private dialog: MatDialog,
              private globalConfigService: GlobalConfigService,
              private notificationService: NotificationService,
              private profileService: ProfileService) {
  }

  ngOnInit(): void {
    this.initProfiles();
    this.editProfileSlider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.profileId = null
    );

    this.globalConfigService.updateAdministrationsList$
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.initProfiles());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  public editProfile(id: number, name: string): void {
    this.profileId = id;
    this.profileName = name;
    this.editProfileSlider.open();
  }

  public tryDeleteProfile(id: number): void {
    const configuration: SimpleDialogData = {
      title: 'permissions.profile.list.dialog.removeProfile.title',
      description: 'permissions.profile.list.dialog.removeProfile.description',
      buttons: [
        {text: 'permissions.profile.list.dialog.removeProfile.buttonNo'},
        {
          text: 'permissions.profile.list.dialog.removeProfile.buttonYes',
          color: 'primary',
          onClick: (dialog: SimpleDialogButton) => this.confirmDeleteProfile(id)
        }
      ]
    };
    this.dialog.open(SimpleDialogComponent, {data: configuration});
  }

  public confirmDeleteProfile(profileId: number): void {
    const sub: Subscription = this.profileService.deleteProfile(profileId)
      .subscribe(() => {
        this.notificationService.notify('permissions.profile.list.notification.profileRemoved', NotificationType.SUCCESS);
        this.initProfiles();
      }, (err) => {
        this.notificationService.notifyError(this.profileError, err.status);
      }, () => sub.unsubscribe());
  }

  public addProfile(): void {
    this.addProfileSlider.open();
  }

  public onScroll(e: any): void {
    if (InfiniteScrollUtil.shouldGetMoreData(e) && this.profilesPage.numberOfElements < this.profilesPage.totalElements) {
      const nextPage = this.currentPage + 1;
      this.getMoreProfiles({page: '' + nextPage, size: InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: this.currentSort});
    }
  }

  public onSortChange($event: Sort): void {
    this.currentSort = $event.direction ? ($event.active + ',' + $event.direction) : 'id,asc';
    this.initProfiles();
  }

  onProfileAdded(): void {
    this.initProfiles();
  }

  private initProfiles(pageRequest: PageRequest = {page: '0', size: InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: this.currentSort}): void {
    this.profileService.getProfiles(pageRequest)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      page => this.handleInitSuccess(page)
    );
  }

  private handleInitSuccess(page: any): void {
    this.currentPage = 0;
    this.profilesPage = page;
  }

  private getMoreProfiles(pageRequest: PageRequest): void {
    this.profileService.getProfiles(pageRequest)
      .pipe(
        takeUntil(this.componentDestroyed),
        throttleTime(500)
      ).subscribe(
      page => this.handleGetMoreAccountsSuccess(page),
      () => this.notificationService.notify('person.detail.notification.listError', NotificationType.ERROR)
    );
  }

  private handleGetMoreAccountsSuccess(page: any): void {
    this.currentPage++;
    this.profilesPage.content = this.profilesPage.content.concat(page.content);
    this.profilesPage.numberOfElements += page.content.length;
  }
}
