import {ModulePermission} from '../../login/auth/auth.model';

export class Profile {
  id: number;
  name: string;
  description: string;
}

export interface ProfileSave {
  id?: number;
  name?: string;
  description?: string;
  modulePermissions?: ModulePermission[];
}
