import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Profile, ProfileSave} from './profile.model';
import {Page, PageRequest} from '../../shared/pagination/page.model';
import {UrlUtil} from '../../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient) { }

  getProfiles(pageRequest: PageRequest): Observable<Page<Profile>> {
    const params = new HttpParams({fromObject: pageRequest as any});
    return this.http.get<Page<Profile>>(`${UrlUtil.url}/crm-user-service/profile`, {params});
  }

  getAllProfiles(): Observable<Profile[]> {
    return this.http.get<Profile[]>(`${UrlUtil.url}/crm-user-service/profile/all`);
  }

  getNewProfileTemplate(): Observable<any> {
    return this.http.get(`${UrlUtil.url}/crm-user-service/profile/save`);
  }

  getProfile(profileId: number): Observable<any> {
    return this.http.get(`${UrlUtil.url}/crm-user-service/profile/${profileId}`);
  }

  deleteProfile(profileId: number): Observable<any> {
    return this.http.delete(`${UrlUtil.url}/crm-user-service/profile/${profileId}`);
  }

  updateProfile(profileId: number, profile: ProfileSave): Observable<any> {
    return this.http.put(`${UrlUtil.url}/crm-user-service/profile/${profileId}`, profile);
  }

  saveProfile(profile: ProfileSave): Observable<any> {
    return this.http.post(`${UrlUtil.url}/crm-user-service/profile/save`, profile);
  }
}
