import {Profile} from './profiles/profile.model';
import {IdAndEmailDTO} from '../person/person.model';

export class Role {
  id: number;
  name: string;
  hasChildren?: boolean;
  supervisor?: any;
  supervisorId?: number;
  profiles?: Profile[];
  accounts?: IdAndEmailDTO[];
  children?: Role[];

  public static toRoleNode(role: Role): RoleNode {
    const node = new RoleNode();
    node.item = new Role();
    node.item.id = role.id;
    node.item.name = role.name;
    node.item.profiles = role.profiles;
    node.item.accounts = role.accounts;
    node.children = [];
    return node;
  }

  public static toRole(roleNode: RoleNode): Role {
    const role = new Role();
    role.id = roleNode.item.id;
    role.name = roleNode.item.name;
    role.supervisorId = roleNode.item.supervisorId;
    role.profiles = roleNode.item.profiles;
    role.accounts = roleNode.item.accounts;
    return role;
  }
}

export class RoleNode {
  children: RoleNode[];
  item: Role;
}
