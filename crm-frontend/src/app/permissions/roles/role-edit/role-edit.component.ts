import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Role} from '../../role.model';
import {Subject} from 'rxjs';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {RoleService} from '../role.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RoleFormComponent} from '../role-form/role-form.component';
import {LoginService} from '../../../login/login.service';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {takeUntil} from 'rxjs/operators';
import {GlobalConfigService} from '../../../global-config/global-config.service';

@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.scss']
})
export class RoleEditComponent implements OnInit, OnDestroy {

  role: Role;
  loadIndicatorVisible = false;

  @ViewChild(RoleFormComponent) private roleFormComponent: RoleFormComponent;
  @Input() slider: SliderComponent;
  @Output() roleAdded: EventEmitter<void> = new EventEmitter<void>();

  @Input() set roleId(roleId: number) {
    if (roleId) {
      this.roleService.getRoleById(roleId).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        (role: Role) => this.role = role
      );
    }
  }

  readonly errorTypes = {
    base: 'permissions.role.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: 400,
        text: 'nameOccupied'
      },
      {
        code: 404,
        text: 'getFailed'
      }
    ]
  };
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private roleService: RoleService,
              private notificationService: NotificationService,
              private router: Router,
              private globalConfigService: GlobalConfigService,
              private activatedRoute: ActivatedRoute,
              private loginService: LoginService) {
  }

  ngOnInit(): void {
    this.slider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.roleFormComponent.clear();
        this.loadIndicatorVisible = false;
      }
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submitForm(): void {
    if (!this.roleFormComponent.isFormValid()) {
      this.roleFormComponent.markAsTouched();
      return;
    }
    const updatedRole = this.roleFormComponent.getRole();
    const loggedAccountId = this.loginService.getLoggedAccountId();
    const loggedAccountHadThisRole = this.role.accounts.find(e => e.id === loggedAccountId) !== undefined;
    const loggedAccountDoesntHaveThisRoleAfterUpdate = updatedRole.accounts.find(e => e.id === loggedAccountId) === undefined;
    if (loggedAccountHadThisRole && loggedAccountDoesntHaveThisRoleAfterUpdate) {
      this.notificationService.notify('permissions.role.notifications.ownAccountDeleteFromRole', NotificationType.ERROR);
      return;
    }
    this.loadIndicatorVisible = true;
    this.roleService.modifyRole(updatedRole).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.handleSaveSuccess(),
      (err) => this.handleSaveError(err)
    );
  }

  closeSlider(): void {
    this.slider.close();
  }

  private handleSaveSuccess(): void {
    this.notificationService.notify('permissions.role.notifications.savedSuccessfully', NotificationType.SUCCESS);
    this.globalConfigService.updateAdministrationsList();
    this.roleAdded.emit();
    this.slider.close();
    this.roleFormComponent.refresh();
  }

  private handleSaveError(err: any): void {
    this.notificationService.notifyError(this.errorTypes, err.status);
    this.loadIndicatorVisible = false;
  }
}
