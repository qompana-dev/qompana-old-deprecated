import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Role} from '../../role.model';
import {RoleService} from '../role.service';
import {ProfileService} from '../../profiles/profile.service';
import {Profile} from '../../profiles/profile.model';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {TranslateService} from '@ngx-translate/core';
import {PersonService} from '../../../person/person.service';
import {AccountEmail, IdAndEmailDTO} from '../../../person/person.model';
import {FormValidators} from '../../../shared/form/form-validators';
import {GlobalConfigService} from '../../../global-config/global-config.service';

@Component({
  selector: 'app-role-form',
  templateUrl: './role-form.component.html',
  styleUrls: ['./role-form.component.scss']
})
export class RoleFormComponent implements OnInit, OnDestroy {

  @Input() set updatedRole(role: Role) {
    this.role = role;
    this.accounts = this.getPossibleAccounts();
    this.updateRoleForm();
  }

  role: Role;
  roleFormGroup: FormGroup;
  roles: Role[] = [];
  profiles: Profile[] = [];
  accounts: AccountEmail[] = [];
  accountsWithoutRole: AccountEmail[] = [];
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private roleService: RoleService,
              private profileService: ProfileService,
              private personService: PersonService,
              private globalConfigService: GlobalConfigService,
              private notificationService: NotificationService,
              private translateService: TranslateService) {
    this.createRoleForm();
  }

  ngOnInit(): void {
    this.refresh();
    this.globalConfigService.updateAdministrationsList$
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.refresh());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  public isFormValid(): boolean {
    return this.roleFormGroup.valid;
  }

  public getRole(): Role {
    return {
      id: this.role ? this.role.id : null,
      name: this.roleFormGroup.get('name').value,
      supervisor: this.roleFormGroup.get('supervisor').value,
      profiles: this.roleFormGroup.get('profiles').value.map(e => ({id: e, name: ''})),
      accounts: this.role && this.role.hasChildren ?
        (this.roleFormGroup.get('account').value !== null ? [{id: this.roleFormGroup.get('account').value}] : [])
        : (this.roleFormGroup.get('accounts').value != null ? this.roleFormGroup.get('accounts').value.map(e => ({id: e})) : [])
    };
  }

  public markAsTouched(): void {
    (Object as any).values(this.roleFormGroup.controls).forEach(control => {
      control.markAsTouched();
    });
  }

  public refresh(): void {
    this.subscribeForRoles();
    this.subscribeForProfiles();
    this.subscribeForAccounts();
  }

  public clear(): void {
    this.createRoleForm();
  }

  private getPossibleAccounts(): AccountEmail[] {
    if (!this.role) {
      return this.accountsWithoutRole;
    } else {
      let possibleAccounts = [];
      if (this.role.hasChildren) {
        possibleAccounts = possibleAccounts.concat({id: null, email: this.translateService.instant('permissions.role.none')});
      }
      if (this.role.accounts && this.role.accounts.length > 0) {
        possibleAccounts = possibleAccounts.concat(this.role.accounts.map(e => IdAndEmailDTO.toAccountEmail(e)));
      }
      return possibleAccounts.concat(this.accountsWithoutRole);
    }
  }

  private createRoleForm(): void {
    this.roleFormGroup = new FormGroup({
      name: new FormControl('', [Validators.required, FormValidators.whitespace, Validators.maxLength(100)]),
      supervisor: new FormControl('', [
        (control: AbstractControl): ValidationErrors =>
          (this.role && control.value === this.role.id) ? {roleComparison: true} : null
      ]),
      profiles: new FormControl('', [Validators.required]),
      account: new FormControl(null, []),
      accounts: new FormControl(null, [])
    });
  }


  private updateRoleForm(): void {
    if (this.roleFormGroup && this.role) {
      this.roleFormGroup.get('name').patchValue(this.role.name);
      this.roleFormGroup.get('supervisor').patchValue(this.role.supervisorId);
      if (this.role.profiles) {
        this.roleFormGroup.get('profiles').setValue(this.role.profiles.map(e => e.id));
      }

      if (this.role.accounts && this.role.accounts.length > 0) {
        if (this.role.hasChildren) {
          this.roleFormGroup.get('account').patchValue(this.role.accounts[0].id);
        } else {
          this.roleFormGroup.get('accounts').patchValue(this.role.accounts.map(e => e.id));
        }
      }
    }
  }

  private subscribeForProfiles(): void {
    this.profileService.getAllProfiles()
      .pipe(
        takeUntil(this.componentDestroyed),
      ).subscribe(
      (profiles: Profile[]) => this.profiles = profiles,
      () => this.notificationService.notify('permissions.role.notifications.profileListFail', NotificationType.ERROR));
  }

  private subscribeForRoles(): void {
    this.roleService.getAllRolesAsFlatList()
      .pipe(
        takeUntil(this.componentDestroyed),
      )
      .subscribe(
        (roles: Role[]) => {
          this.roles = [{id: null, name: this.translateService.instant('permissions.role.none')}]
            .concat(roles.filter(e => e.accounts.length <= 1));
        }, () => this.notificationService.notify('permissions.role.notifications.roleListFail', NotificationType.ERROR));
  }

  private subscribeForAccounts(): void {
    this.personService.getAccountEmailsWithoutRoleAssigned()
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      (accounts: AccountEmail[]) => {
        this.accountsWithoutRole = accounts;
        this.accounts = this.getPossibleAccounts();
      },
      () => this.notificationService.notify('permissions.role.notifications.accountListFail', NotificationType.ERROR));
  }
}
