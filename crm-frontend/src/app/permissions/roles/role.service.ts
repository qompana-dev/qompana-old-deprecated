import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Role, RoleNode} from '../role.model';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {UrlUtil} from '../../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  constructor(private http: HttpClient) {
  }

  roles: RoleNode[] = [];

  getRolesAsTree(): Observable<any> {
    const subscription = this.http.get(`${UrlUtil.url}/crm-user-service/role`)
      .pipe(map((roleList: Role[]) => this.mapRolesToRoleNodeList(roleList)));
    subscription.subscribe(
      (roles: RoleNode[]) => this.roles = roles,
    );
    return subscription;
  }

  getAllRolesAsFlatList(): Observable<any> {
    const queryParams = new HttpParams()
      .set('onlyPossibleRoles', 'false');
    return this.http.get(`${UrlUtil.url}/crm-user-service/role/flat-list`, {params: queryParams});
  }

  getPossibleRolesAsFlatList(): Observable<any> {
    const queryParams = new HttpParams()
      .set('onlyPossibleRoles', 'true');
    return this.http.get(`${UrlUtil.url}/crm-user-service/role/flat-list`, {params: queryParams});
  }

  addRole(role: Role): Observable<any> {
    return this.http.post(`${UrlUtil.url}/crm-user-service/role`, role);
  }

  modifyRole(role: Role): Observable<any> {
    return this.http.put(`${UrlUtil.url}/crm-user-service/role`, role);
  }

  deleteRole(role: RoleNode): Observable<any> {
    return this.http.delete(`${UrlUtil.url}/crm-user-service/role/${role.item.id}`);
  }

  moveRole(movedNote: RoleNode, destinationNode: RoleNode): Observable<any> {
    const queryParams = new HttpParams().set('parentId', '' + destinationNode.item.id).set('childId', '' + movedNote.item.id);
    return this.http.get(`${UrlUtil.url}/crm-user-service/role/move`, { params: queryParams });
  }

  getRoleById(id: number): Observable<any> {
    return this.http.get(`${UrlUtil.url}/crm-user-service/role/${id}`)
      .pipe(
        map((role: Role) => this.setSupervisorAndReturn(role))
      );
  }

  private setSupervisorAndReturn(role: Role): Role {
    const roleNode = Role.toRoleNode(role);
    const parent = this.findNodeParent(roleNode);
    if (parent) {
      role.supervisorId = parent.item.id;
    } else {
      role.supervisorId = null;
    }
    return role;
  }

  private mapRolesToRoleNodeList(roles: Role[]): RoleNode[] {
    const roleNodeList: RoleNode[] = [];
    for (const role of roles) {
      const roleNode = Role.toRoleNode(role);
      roleNode.children = this.mapRolesToRoleNodeList(role.children);
      roleNodeList.push(roleNode);
    }
    return roleNodeList;
  }

  private findNodeParent(node: RoleNode): RoleNode {
    for (const currentRoot of this.roles) {
      const parent = this.findNodeParentStartingFrom(currentRoot, node);
      if (parent != null) {
        return parent;
      }
    }
    return null;
  }

  private findNodeParentStartingFrom(currentRoot: RoleNode, node: RoleNode): RoleNode {
    if (currentRoot.children && currentRoot.children.length > 0) {
      for (const child of currentRoot.children) {
        if (child.item.id === node.item.id) {
          return currentRoot;
        } else if (child.children && child.children.length > 0) {
          const parent = this.findNodeParentStartingFrom(child, node);
          if (parent != null) {
            return parent;
          }
        }
      }
    }
    return null;
  }
}
