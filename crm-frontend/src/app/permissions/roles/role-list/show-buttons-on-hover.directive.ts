import {Directive, ElementRef, HostListener, Renderer2} from '@angular/core';

@Directive({
  selector: '[appShowButtonsOnHover]'
})
export class ShowButtonsOnHoverDirective {

  constructor(private renderer: Renderer2, private el: ElementRef) { }



  @HostListener('mouseover')
  onMouseOver() {
    this.renderer.addClass(this.el.nativeElement.lastChild, 'show-additional-buttons');
  }

  @HostListener('mouseout')
  onMouseOut() {
    this.renderer.removeClass(this.el.nativeElement.lastChild, 'show-additional-buttons');
  }
}
