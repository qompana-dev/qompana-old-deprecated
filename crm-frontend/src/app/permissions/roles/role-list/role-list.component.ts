import {NestedTreeControl} from '@angular/cdk/tree';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import {RoleNode} from '../../role.model';
import {RoleService} from '../role.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {MatDialog} from '@angular/material';
import {
  SimpleDialogButton,
  SimpleDialogComponent,
  SimpleDialogData
} from '../../../shared/dialog/simple-dialog.component';
import {BAD_REQUEST, NOT_FOUND} from 'http-status-codes';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {GlobalConfigService} from '../../../global-config/global-config.service';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss']
})
export class RoleListComponent implements OnInit, OnDestroy {
  treeControl: NestedTreeControl<RoleNode>;
  dataSource: MatTreeNestedDataSource<RoleNode>;
  supervisorId: number;
  roleId: number;

  readonly EXPAND_NODE_AFTER_TIME = 300;

  dragNode: any;
  dragOverNode: any;
  nodeOverNodeTime: number;
  dragAndDropEnabled = true;

  @ViewChild('addRoleSlider') addRoleSlider: SliderComponent;
  @ViewChild('editRoleSlider') editRoleSlider: SliderComponent;

  private readonly deleteErrorTypes: ErrorTypes = {
    base: 'permissions.role.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'deletionFailedOccupied'
      }, {
        code: NOT_FOUND,
        text: 'deletionFailedNotExists'
      }
    ]
  };
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private roleService: RoleService,
              private router: Router,
              private globalConfigService: GlobalConfigService,
              private dialog: MatDialog,
              private notificationService: NotificationService,
              private translateService: TranslateService) {
    this.treeControl = new NestedTreeControl<RoleNode>(this.getChildren);
    this.dataSource = new MatTreeNestedDataSource();
    this.refreshRoleTree();
  }

  ngOnInit(): void {
    this.addRoleSlider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.supervisorId = null
    );

    this.editRoleSlider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.roleId = null
    );

    this.globalConfigService.updateAdministrationsList$
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.refreshRoleTree());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  getChildren = (node: RoleNode): RoleNode[] => node.children;
  hasChildren = (index: number, node: RoleNode) => node.children && node.children.length > 0;

  addNewRole(role: RoleNode): void {
    if (role != null) {
      this.supervisorId = role.item.id;
    } else {
      this.supervisorId = null;
    }
    this.addRoleSlider.open();
  }

  editRole(node: any): void {
    this.roleId = node.item.id;
    this.editRoleSlider.open();
  }

  showRemoveDialog(role: RoleNode): void {
    const configuration: SimpleDialogData = {
      title: 'permissions.role.dialog.removeRole.title',
      description: 'permissions.role.dialog.removeRole.description',
      buttons: [
        {text: 'permissions.role.dialog.removeRole.buttonNo'},
        {
          text: 'permissions.role.dialog.removeRole.buttonYes',
          color: 'primary',
          onClick: (dialog: SimpleDialogButton) => this.deleteRole(role)
        }
      ]
    };
    this.dialog.open(SimpleDialogComponent, {data: configuration});
  }

  deleteRole(role: RoleNode): void {
    const sub = this.roleService.deleteRole(role)
      .subscribe(
        () => this.handleDeleteSuccess(),
        (err) => this.notificationService.notifyError(this.deleteErrorTypes, err.status),
        () => sub.unsubscribe()
      );
  }

  handleDragStart(event: any, node: any): void {
    this.dragNode = node;
    this.treeControl.collapse(node);
  }

  handleDragOver(event: any, node: any): void {
    event.preventDefault();
    this.expandNodeIfCursorOver(node);
  }

  handleDrop(event: any, node: any): void {
    event.preventDefault();
    if (node !== this.dragNode) {
      const sub = this.roleService.moveRole(this.dragNode, node)
        .subscribe(
          () => this.refreshRoleTree(),
          () => this.notificationService.notify('permissions.role.notifications.moveError', NotificationType.ERROR),
          () => sub.unsubscribe()
        );
    }
  }

  handleDragEnd(): void {
    this.dragNode = null;
    this.dragOverNode = null;
    this.nodeOverNodeTime = 0;
  }

  onRoleAdded(): void {
    this.refreshRoleTree();
  }

  private refreshRoleTree(): void {
    const sub = this.roleService.getRolesAsTree()
      .subscribe(
        (roles: RoleNode[]) => this.createRoleTree(roles),
        () => this.notificationService.notify('permissions.role.notifications.refreshFailed', NotificationType.ERROR),
        () => sub.unsubscribe()
      );
  }

  private createRoleTree(roles: RoleNode[]): void {
    this.dataSource.data = null;
    this.dataSource.data = roles;
    this.treeControl.dataNodes = roles;
    this.treeControl.expandAll();
    this.checkIfCanDragAndDrop(roles);
  }

  private checkIfCanDragAndDrop(roles: RoleNode[]): void {
    this.dragAndDropEnabled = this.countAssignedAccount(roles) <= 1;
  }

  private countAssignedAccount(roles: RoleNode[]): number {
    let sum = 0;
    for (const role of roles) {
      sum += role.item.accounts.length;
      if (role.children) {
        sum += this.countAssignedAccount(role.children);
      }
    }
    return sum;
  }

  private expandNodeIfCursorOver(node: any): void {
    if (node === this.dragOverNode) {
      if (this.dragNode !== node && !this.treeControl.isExpanded(node)) {
        if ((new Date().getTime() - this.nodeOverNodeTime) > this.EXPAND_NODE_AFTER_TIME) {
          this.treeControl.expand(node);
        }
      }
    } else {
      this.dragOverNode = node;
      this.nodeOverNodeTime = new Date().getTime();
    }
  }

  private handleDeleteSuccess(): void {
    this.refreshRoleTree();
    this.notificationService.notify('permissions.role.notifications.deletedSuccessfully', NotificationType.SUCCESS);
  }
}
