import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {RoleService} from '../role.service';
import {Role} from '../../role.model';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {Subject, Subscription} from 'rxjs';
import {RoleFormComponent} from '../role-form/role-form.component';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {takeUntil} from 'rxjs/operators';
import {GlobalConfigService} from '../../../global-config/global-config.service';

@Component({
  selector: 'app-role-add',
  templateUrl: './role-add.component.html',
  styleUrls: ['./role-add.component.scss']
})
export class RoleAddComponent implements OnInit, OnDestroy {

  role: Role;
  loadIndicatorVisible = false;
  @ViewChild(RoleFormComponent) private roleFormComponent: RoleFormComponent;
  @Input() slider: SliderComponent;
  @Output() roleAdded: EventEmitter<void> = new EventEmitter<void>();

  @Input() set supervisorId(supervisorId: number) {
    this.role = new Role();
    if (supervisorId !== undefined) {
      this.role.supervisorId = supervisorId;
    }
  }

  readonly errorTypes = {
    base: 'permissions.role.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: 400,
        text: 'nameOccupied'
      }
    ]
  };
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private roleService: RoleService,
              private globalConfigService: GlobalConfigService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.role = new Role();
    this.role.supervisorId = null;

    this.slider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.roleFormComponent.clear();
        this.loadIndicatorVisible = false;
      }
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submitForm(): void {
    if (!this.roleFormComponent.isFormValid()) {
      this.roleFormComponent.markAsTouched();
      return;
    }
    this.loadIndicatorVisible = true;
    const sub: Subscription = this.roleService.addRole(this.roleFormComponent.getRole())
      .subscribe(() => this.handleSaveSuccess(),
        (err) => this.handleError(err),
        () => sub.unsubscribe()
      );
  }

  private handleSaveSuccess(): void {
    this.notificationService.notify('permissions.role.notifications.savedSuccessfully', NotificationType.SUCCESS);
    this.globalConfigService.updateAdministrationsList();
    this.roleAdded.emit();
    this.slider.close();
    this.roleFormComponent.refresh();
  }

  private handleError(err: any): void {
    this.notificationService.notifyError(this.errorTypes, err.status);
    this.loadIndicatorVisible = false;
  }

  closeSlider(): void {
    this.slider.close();
  }
}
