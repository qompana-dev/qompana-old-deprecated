import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {RoleAddComponent} from './role-add/role-add.component';
import {RoleFormComponent} from './role-form/role-form.component';
import {RoleListComponent} from './role-list/role-list.component';
import {RoleEditComponent} from './role-edit/role-edit.component';
import {ShowButtonsOnHoverDirective} from './role-list/show-buttons-on-hover.directive';

@NgModule({
  declarations: [
    RoleAddComponent,
    RoleFormComponent,
    RoleListComponent,
    RoleEditComponent,
    ShowButtonsOnHoverDirective
  ],
  exports: [
    RoleListComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class RoleModule { }
