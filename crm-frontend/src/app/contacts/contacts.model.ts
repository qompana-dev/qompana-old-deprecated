import {Address, Customer, CustomerOption} from '../customer/customer.model';
import {AdditionalPhoneNumber} from '../shared/additional-phone-numbers/additional-phone-numbers.model';

export class Contact {
  id: number;
  name: string;
  surname: string;
  description: string;
  email: string;
  phone: string;
  additionalPhones?: AdditionalPhoneNumber[];
  department?: string;
  customerId?: number;
  customer?: Customer;
  position?: string;
  linkedCustomers: CustomerOption[];
  checked?: boolean;
  owner: number;
  avatar?: string;

  facebookLink: string;
  twitterLink: string;
  linkedInLink: string;

  additionalData: string;
}

export class SocialMedia {
  facebookLink: string;
  twitterLink: string;
  linkedInLink: string;
}

export class ContactTopBarDTO {
  id: number;
  name: string;
  surname: string;
  position: string;
  customerName: string;
  phone: string;
  email: string;
  created: string;
  owner: number;
  avatar?: string;
}

export class ContactCustomerLink {
  id: number;
  relationType: string;
  startDate: string;
  endDate: string;
  note: string;
  position: string;
}

export class ContactOnList {
  id: number;
  name: string;
  surname: string;
  position: string;
  email: string;
  owner: string;
}

export class ContactLinkPreviewDto {
  count: number;
  links: ContactLinkDto[];
}

export class ContactLinkDto {
  id: number;
  baseContactId: number;
  baseContactName: string;
  baseContactSurname: string;
  linkedContactId: number;
  linkedContactName: string;
  linkedContactSurname: string;
  linkedContactPhone: string;
  linkedContactEmail: string;
  linkedCustomerName: string;
  linkedContactPosition: string;
  note: string;
}

export class CreateContactLink {
  note: string;
}

export class ContactAndCustomerNameDto {
  id: number;
  contactName: string;
  customerName: string;
}

export class ContactCustomerRelation {
  id: number;
  startDate: string;
  endDate: string;
  customerName: string;
  contactName: string;
  customerId: number;
  contactId: number;
  position: string;
  relationType: string;
  note: string;
  active: boolean;
  firstToContact: boolean;
}


export class ContactSalesOpportunity {
  id: number;
  name: string;
  status: number;
  maxStatus: number;
  amount: number;
  currency: string;
  finishDate: string;
}

export class ContactSalesOpportunityPreview {
  totalNumber: number;
  opportunities: ContactSalesOpportunity[];
}

export class RelatedContactChange {
  type: 'CREATE' | 'UPDATE' | 'DELETE';
  link: ContactLinkDto;
}


export interface AddCustomerContactRelation {
  contactId: number;
  customerId: number;
  position: string;
  startDate: string;
  endDate: string;
  note: string;
}

export interface UpdateRelatedCustomer {
  id: number;
  position: string;
  startDate: string;
  endDate: string;
  note: string;
}


export interface ContactRelationFormModel {
  relationId: number;
  company: string;
  position: string;
  relationType: string;
  startDateMonth: number;
  startDateYear: number;
  endDateMonth: number;
  endDateYear: number;
  note: string;
}

export interface CustomerPreview {
  id: number;
  name: string;
  phone: string;
  owner: number;
  ownerName?: string;
  address: Address;
  website: string;
  previewOpportunityDtoList: OpportunityPreviewDto[];
  firstTwoOpportunities: OpportunityPreviewDto[];
}

export interface OpportunityPreviewDto {
  id: number;
  name: string;
  finishDate: string;
  amount: number;
  currency: string;
  processInstanceId: string;
  currentTask: string;
}

export interface ContactAvatar {
  avatar?: string;
}
