import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {SliderService} from '../../../shared/slider/slider.service';
import {ContactsService} from '../../contacts.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {Subject} from 'rxjs';
import {BAD_REQUEST, NOT_FOUND} from 'http-status-codes';
import {ContactFormComponent} from '../contact-form/contact-form.component';
import {HttpErrorResponse} from '@angular/common/http';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {Contact} from '../../contacts.model';
import {CrmObject} from '../../../shared/model/search.model';


@Component({
  selector: 'app-contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.scss']
})
export class ContactAddComponent implements OnInit, OnDestroy {
  @Input() slider: SliderComponent;
  @Input() customer: CrmObject;

  @Output() contactAdded: EventEmitter<{ id: number, name: string, surname: string }> = new EventEmitter();

  @ViewChild(ContactFormComponent) private contactFormComponent: ContactFormComponent;
  private componentDestroyed: Subject<void> = new Subject();

  readonly errorTypes: ErrorTypes = {
    base: 'contacts.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'validationError'
      }, {
        code: NOT_FOUND,
        text: 'customerNotFound'
      }
    ]
  };
  saveInProgress = false;


  constructor(private sliderService: SliderService,
              private contactService: ContactsService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
  }

  addContact(): void {
    if (!this.contactFormComponent.isFormValid()) {
      return;
    }
    if (!this.saveInProgress) {
      this.saveInProgress = true;
      const contact = this.contactFormComponent.getContact();
      this.contactService.createContact(contact)
        .subscribe(
          (result: Contact) => {
            this.saveInProgress = false;
            this.notifyAddSuccess();
            this.closeSlider();
            const customerId = result && result.customer && result.customer.id;
            this.emitContactAdded({id: result.id, name: result.name, surname: result.surname, customerId});
          },
          (err) => this.notifyError(err)
        );
    }
  }


  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private notifyAddSuccess(): void {
    this.notificationService.notify('contacts.form.notification.added', NotificationType.SUCCESS);
  }

  private notifyError(err: HttpErrorResponse): void {
    this.saveInProgress = false;
    this.notificationService.notifyError(this.errorTypes, err.status);
  }

  closeSlider(): void {
    this.contactFormComponent.reset();
    (this.slider) ? this.slider.close() : this.sliderService.closeSlider();
  }

  addAdditionalField(): void {
    this.contactFormComponent.addAdditionalField();
  }

  private emitContactAdded(newContact: { id: number, name: string, surname: string, customerId?: number }): void {
    this.contactAdded.emit(newContact);
  }
}
