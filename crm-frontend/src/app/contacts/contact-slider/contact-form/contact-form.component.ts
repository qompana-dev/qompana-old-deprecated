import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Contact, SocialMedia} from '../../contacts.model';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {Observable, Subject, Subscription} from 'rxjs';
import {AuthService} from '../../../login/auth/auth.service';
import {debounceTime, filter, switchMap, takeUntil, tap} from 'rxjs/operators';
import {CustomerService} from '../../../customer/customer.service';
import {applyPermission} from '../../../shared/permissions-utils';
import {CrmObject, CrmObjectType} from '../../../shared/model/search.model';
import {AssignToGroupService} from '../../../shared/assign-to-group/assign-to-group.service';
import {FormUtil} from '../../../shared/form.util';
import {Customer} from '../../../customer/customer.model';
import {HttpErrorResponse} from '@angular/common/http';
import {ErrorTypes, NotificationService} from '../../../shared/notification.service';
import {NOT_FOUND} from 'http-status-codes';
import {FormValidators} from '../../../shared/form/form-validators';
import {AdditionalDataFormComponent} from '../../../shared/additional-data-form/additional-data-form.component';
import {AdditionalPhoneNumbersComponent} from '../../../shared/additional-phone-numbers/additional-phone-numbers.component';
import {PhoneService} from '../../../shared/phone-fields/phone.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit, OnDestroy {

  @Input() update: boolean;
  @Input() fromContactInfoTab = false;
  private selectedCustomerName: string;

  @Input() set customer(customer: CrmObject) {
    this.customerSelect(customer);
  }
  contactForm: FormGroup;
  private componentDestroyed: Subject<void> = new Subject();

  contactOwner: string;

  filteredCustomers: CrmObject[] = [];

  selectedCustomer: Customer;
  @Output() formChanged: EventEmitter<void> = new EventEmitter<void>();
  @Output() customerIdChanged: EventEmitter<number> = new EventEmitter<number>();

  private addAdditionalFieldSubject: Subject<void> = new Subject<void>();
  public addAdditionalField$: Observable<void> = this.addAdditionalFieldSubject.asObservable();

  private addAdditionalPhoneNumbersSubject: Subject<void> = new Subject<void>();
  public addAdditionalPhoneNumbers$: Observable<void> = this.addAdditionalPhoneNumbersSubject.asObservable();

  readonly getCustomerErrorTypes: ErrorTypes = {
    base: 'contacts.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'customerNotFound'
      }
    ]
  };

  formChangedSubscription: Subscription;

  @ViewChild('additionalDataFormComponent') additionalDataFormComponent: AdditionalDataFormComponent;

  constructor(private authService: AuthService,
              private customerService: CustomerService,
              private fb: FormBuilder,
              private notificationService: NotificationService,
              private assignToGroupService: AssignToGroupService,
              public phoneService: PhoneService) {
    this.createContactForm();
  }

  // tslint:disable-next-line:variable-name
  _contact: Contact;
  socialMedia: SocialMedia;

  @Input() set contact(contact: Contact) {
    if (contact && contact.additionalPhones) {
      contact.additionalPhones = this.phoneService.mapAdditionalPhones(contact.additionalPhones);
      this.contactForm.reset(contact);
      this._contact = contact;
      this.updateForm();
    }
  }

  get authKey(): string {
    if (this.fromContactInfoTab) {
      return 'ContactInContactInfoComponent.';
    }
    return (this.update) ? 'ContactEditComponent.' : 'ContactAddComponent.';
  }

  // @formatter:off
  get name(): AbstractControl {return this.contactForm.get('name'); }
  get surname(): AbstractControl {return this.contactForm.get('surname'); }
  get position(): AbstractControl {return this.contactForm.get('position'); }
  get phone(): AbstractControl {return this.contactForm.get('phone'); }
  get email(): AbstractControl {return this.contactForm.get('email'); }
  get customerId(): AbstractControl { return this.contactForm.get('customerId'); }
  get customerSearchControl(): AbstractControl { return this.contactForm.get('customerSearch'); }
  get avatar(): AbstractControl {return this.contactForm.get('avatar'); }
  get additionalData(): AbstractControl {return this.contactForm.get('additionalData'); }
  get additionalPhones(): AbstractControl { return this.contactForm.get('additionalPhones'); }
  // @formatter:on

  public updateForm(): void {
    if (this.formChangedSubscription) {
      this.formChangedSubscription.unsubscribe();
    }

    this.updateSocialMedia(this._contact, false);
    this.updateContactForm(this._contact);
    this.setOwner();
  }

  ngOnInit(): void {
    this.setOwner();
    this.subscribeFormChanged();
    this.subscribeForPermissionsChanges();

    this.customerSearchControl.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      filter(value => !value)
    ).subscribe(() => {
      this.customerId.patchValue(null);
      this.customerIdChanged.emit(null);
      this.selectedCustomer = null;
      this.selectedCustomerName = null;
    });
  }

  ngOnDestroy(): void {
    if (this.formChangedSubscription) {
      this.formChangedSubscription.unsubscribe();
    }
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  updateSocialMedia(socialMedia: SocialMedia | Contact, emitChanges: boolean): void {
    let changed = false;
    if ((this.socialMedia && socialMedia) &&
      (this.socialMedia.facebookLink !== socialMedia.facebookLink ||
        this.socialMedia.twitterLink !== socialMedia.twitterLink ||
        this.socialMedia.linkedInLink !== socialMedia.linkedInLink)) {
      changed = true;
    }
    this.socialMedia = {} as SocialMedia;

    if (this.socialMedia && socialMedia) {
      this.socialMedia.facebookLink = socialMedia.facebookLink;
      this.socialMedia.twitterLink = socialMedia.twitterLink;
      this.socialMedia.linkedInLink = socialMedia.linkedInLink;
    }

    if (changed && emitChanges) {
      this.formChanged.emit();
    }
  }

  public getContact(): Contact {
    const contact: Contact = new Contact();
    contact.name = this.name.value;
    contact.surname = this.surname.value;
    contact.customerId = this.customerId.value;
    contact.position = this.position.value;
    contact.phone = !!this.phone.value ? this.phone.value : null;
    contact.email = !!this.email.value ? this.email.value : null;
    contact.customer = this.selectedCustomer;
    contact.additionalData = !!this.additionalData.value ? this.additionalData.value : null;
    contact.additionalPhones = this.additionalPhones.value;
    contact.avatar = !!this.avatar.value ? this.avatar.value : null;
    if (this.socialMedia) {
      contact.facebookLink = this.socialMedia.facebookLink;
      contact.twitterLink = this.socialMedia.twitterLink;
      contact.linkedInLink = this.socialMedia.linkedInLink;
    }
    return contact;
  }

  customerSelect(customer: CrmObject): void {
    if (customer && customer.type === CrmObjectType.customer) {
      this.customerId.patchValue(customer.id);
      this.selectedCustomerName = customer.label;
      this.customerSearchControl.patchValue(customer.label);
    }
  }

  addAdditionalField(): void {
    this.addAdditionalFieldSubject.next();
  }

  public isFormValid(): boolean {
    FormUtil.setTouched(this.contactForm);
    return this.contactForm.valid;
  }

  public reset(): void {
    this.contactForm.reset();
    this.additionalDataFormComponent.reset();
  }

  private createContactForm(): void {
    this.contactForm = this.fb.group({
      name: ['', Validators.compose([
        Validators.maxLength(200),
        FormValidators.whitespace,
        Validators.required])
      ],
      surname: ['', Validators.compose([
        Validators.maxLength(200),
        FormValidators.whitespace,
        Validators.required])
      ],
      position: ['', [Validators.maxLength(100), FormValidators.whitespace]],
      phone: ['', Validators.maxLength(128)],
      additionalPhones: this.fb.array([]),
      email: ['', Validators.compose([
        Validators.maxLength(100),
        Validators.email
      ])],
      customerId: [null],
      customerSearch: [null, this.getCustomCustomerValueValidator()],
      avatar: [null],
      additionalData: [null]
    });
    this.applyPermissionsToForm();
    this.subscribeCustomerSearch();
    this.subscribeCustomerId();
  }

  private updateContactForm(contact: Contact): void {
    if (this.contactForm && contact) {
      this.setCustomer(contact);
      this.name.patchValue(contact.name, {emitEvent: false});
      this.surname.patchValue(contact.surname, {emitEvent: false});
      this.position.patchValue(contact.position, {emitEvent: false});
      this.phone.patchValue(contact.phone, {emitEvent: false});
      this.email.patchValue(contact.email, {emitEvent: false});
      this.avatar.patchValue(contact.avatar, {emitEvent: false});
      this.additionalData.patchValue(contact.additionalData);
      this.additionalPhones.patchValue(contact.additionalPhones);
    }
  }

  private setCustomer(contact: Contact): void {
    if (contact && contact.customer && contact.customer.id) {
      this.assignToGroupService.updateLabels([{type: CrmObjectType.customer, id: contact.customer.id}])
        .pipe(
          takeUntil(this.componentDestroyed),
          tap((response: CrmObject[]) => {
            if (response && response.length > 0) {
              this.selectedCustomerName = response[0].label;
              this.customerSearchControl.patchValue(response[0].label, {emitEvent: false});
              this.customerId.patchValue(contact.customer.id, {emitEvent: false});
            }
          })).subscribe();
    } else {
      this.selectedCustomerName = null;
      this.customerSearchControl.patchValue(null, {emitEvent: false});
      this.customerId.patchValue(null, {emitEvent: false});
    }
  }

  private applyPermissionsToForm(): void {
    if (this.contactForm) {
      applyPermission(this.name, !this.authService.isPermissionPresent(`${this.authKey}nameField`, 'r'));
      applyPermission(this.surname, !this.authService.isPermissionPresent(`${this.authKey}surnameField`, 'r'));
      applyPermission(this.position, !this.authService.isPermissionPresent(`${this.authKey}positionField`, 'r'));
      applyPermission(this.customerSearchControl, !this.authService.isPermissionPresent(`${this.authKey}companyField`, 'r'));
      applyPermission(this.phone, !this.authService.isPermissionPresent(`${this.authKey}phoneField`, 'r'));
      applyPermission(this.email, !this.authService.isPermissionPresent(`${this.authKey}emailField`, 'r'));
    }
  }

  private subscribeForPermissionsChanges(): void {
    this.authService.onAuthChange.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.applyPermissionsToForm()
    );
  }


  private subscribeCustomerSearch(): void {
    this.customerSearchControl.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      filter((input: string) => !!input && input.length >= 3),
      switchMap(((input: string) => this.assignToGroupService.searchForType(CrmObjectType.customer, input, [CrmObjectType.customer]))
      )).subscribe(
      (result: CrmObject[]) => {
        this.filteredCustomers = result;
      }
    );
  }

  private subscribeCustomerId(): void {
    this.customerId.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed),
        filter((value) => !!value),
        tap(() => this.selectedCustomer = undefined)
      ).subscribe((value) => {
      if (value) {
        this.customerIdChanged.emit(+value);
        this.customerService.getCustomerById(+value).subscribe(
          customer => this.selectedCustomer = customer,
          (err) => this.notifyClientError(err)
        );
      }
    });
  }

  private notifyClientError(err: HttpErrorResponse): void {
    this.notificationService.notifyError(this.getCustomerErrorTypes, err.status);
  }

  subscribeFormChanged(): void {
    if (this.contactForm) {

      if (this.formChangedSubscription) {
        this.formChangedSubscription.unsubscribe();
      }
      this.formChangedSubscription = this.contactForm.valueChanges
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
          this.formChanged.next()});
    }
  }

  private setOwner(): void {
    const ownerId = (this._contact && this._contact.owner) ? this._contact.owner : +localStorage.getItem('loggedAccountId');
    if (ownerId) {
      this.assignToGroupService.updateLabels([{id: ownerId, type: CrmObjectType.user}])
        .pipe(
          takeUntil(this.componentDestroyed),
          filter(objects => objects.length > 0)
        ).subscribe((objects: CrmObject[]) => {
        this.contactOwner = objects[0].label;

        this.subscribeFormChanged();
      });
    }
  }

  customerAdded(newCustomer: { id: number, label: string }): void {
    this.customerSearchControl.patchValue(newCustomer.label);
    this.customerId.patchValue(newCustomer.id);
  }

  private getCustomCustomerValueValidator(): (control: AbstractControl) => ValidationErrors {
    return (control: AbstractControl): ValidationErrors => {
      return !control.value || !this.selectedCustomerName || control.value === this.selectedCustomerName ? null : {customCustomerValue: true};
    };
  }

  addAdditionalPhone(event: Event): void {
    event.preventDefault();
    event.stopPropagation();
    this.addAdditionalPhoneNumbersSubject.next();
  }
}
