import {Component, EventEmitter, Input, Output} from '@angular/core';
import {SocialMedia} from '../../../contacts.model';
import {CrmLog} from '../../../../shared/annoatation/crm-log.decorator';

@Component({
  selector: 'app-social-media',
  templateUrl: './social-media.component.html',
  styleUrls: ['./social-media.component.scss']
})
export class SocialMediaComponent {
  private storedSocialMedia: SocialMedia = {} as SocialMedia;
  @Input() set socialMedia(storedSocialMedia: SocialMedia) {
    this.storedSocialMedia = storedSocialMedia;
  }
  get socialMedia(): SocialMedia {
    return this.storedSocialMedia;
  }
  @Input() authKey: string;
  @Output() socialMediaEdited: EventEmitter<SocialMedia> = new EventEmitter<SocialMedia>();

  public emitSocialMediaEdited(socialMedia: SocialMedia): void {
    this.storedSocialMedia = socialMedia;
    this.socialMediaEdited.emit(this.storedSocialMedia);
  }
}
