import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {SocialMedia} from '../../../contacts.model';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomerService} from '../../../../customer/customer.service';
import {NotificationService} from '../../../../shared/notification.service';
import {ContactsService} from '../../../contacts.service';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {SliderService} from '../../../../shared/slider/slider.service';
import {FormUtil} from '../../../../shared/form.util';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-edit-social-media',
  templateUrl: './edit-social-media.component.html',
  styleUrls: ['./edit-social-media.component.scss']
})
export class EditSocialMediaComponent implements OnInit, OnChanges, OnDestroy {
  storedSocialMedia: SocialMedia;

  private componentDestroyed: Subject<void> = new Subject();

  @Input() set socialMedia(storedSocialMedia: SocialMedia) {
    this.storedSocialMedia = storedSocialMedia;
    this.updateForm(storedSocialMedia);
  }

  @Input() slider: SliderComponent;

  @Output() contactEdited: EventEmitter<SocialMedia> = new EventEmitter<SocialMedia>();

  socialMediaForm: FormGroup;

  constructor(private customerService: CustomerService,
              private fb: FormBuilder,
              private sliderService: SliderService,
              private contactService: ContactsService,
              private notificationService: NotificationService) {
    this.createSocialMediaForm();
  }

  // @formatter:off
  get facebookLink(): AbstractControl {return this.socialMediaForm.get('facebookLink'); }
  get twitterLink(): AbstractControl {return this.socialMediaForm.get('twitterLink'); }
  get linkedInLink(): AbstractControl {return this.socialMediaForm.get('linkedInLink'); }
  // @formatter:on

  ngOnInit(): void {
    this.subscribeSliderClose();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.subscribeSliderClose();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private subscribeSliderClose(): void {
    if (this.slider) {
      this.slider.closeEvent
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => {
          this.updateForm(this.storedSocialMedia);
        });
    }
  }

  private createSocialMediaForm(): void {
    this.socialMediaForm = this.fb.group({
      facebookLink: ['', [Validators.maxLength(1024), (control: AbstractControl) => control.value && control.value.length > 0
      && !/^(http(|s):\/\/)/.test(control.value) ? {incorrectUrl: true} : undefined]],
      twitterLink: ['', [Validators.maxLength(1024), (control: AbstractControl) => control.value && control.value.length > 0
      && !/^(http(|s):\/\/)/.test(control.value) ? {incorrectUrl: true} : undefined]],
      linkedInLink: ['', [Validators.maxLength(1024), (control: AbstractControl) => control.value && control.value.length > 0
      && !/^(http(|s):\/\/)/.test(control.value) ? {incorrectUrl: true} : undefined]],
    });
  }

  private updateForm(socialMedia: SocialMedia): void {
    this.facebookLink.patchValue((socialMedia) ? socialMedia.facebookLink : undefined);
    this.twitterLink.patchValue((socialMedia) ? socialMedia.twitterLink : undefined);
    this.linkedInLink.patchValue((socialMedia) ? socialMedia.linkedInLink : undefined);
  }

  updateContact(mouseEvent?: MouseEvent): void {
    if (mouseEvent) {
      mouseEvent.stopPropagation();
    }
    FormUtil.setTouched(this.socialMediaForm);
    if (this.socialMediaForm.valid) {
      const updatedSocialMedia: SocialMedia = {
        facebookLink: this.facebookLink.value,
        twitterLink: this.twitterLink.value,
        linkedInLink: this.linkedInLink.value
      };
      this.contactEdited.emit(updatedSocialMedia);
      this.closeSlider();
    }
  }

  closeSlider(mouseEvent?: MouseEvent): void {
    if (mouseEvent) {
      mouseEvent.stopPropagation();
    }
    (this.slider) ? this.slider.close() : this.sliderService.closeSlider();
  }

  removeValue(type: 'twitter' | 'facebook' | 'linkedin', event: MouseEvent): void {
    event.stopPropagation();
    switch (type) {
      case 'twitter':
        this.twitterLink.patchValue(undefined);
        break;
      case 'facebook':
        this.facebookLink.patchValue(undefined);
        break;
      case 'linkedin':
        this.linkedInLink.patchValue(undefined);
        break;
    }
  }
}
