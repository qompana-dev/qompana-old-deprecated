import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {SliderService} from '../../../shared/slider/slider.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {ContactsService} from '../../contacts.service';
import {Contact} from '../../contacts.model';
import {Subject} from 'rxjs';
import {BAD_REQUEST, NOT_FOUND} from 'http-status-codes';
import {HttpErrorResponse} from '@angular/common/http';
import {ContactFormComponent} from '../contact-form/contact-form.component';
import {PhoneService} from '../../../shared/phone-fields/phone.service';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.scss']
})
export class ContactEditComponent implements OnInit, OnDestroy {


  @Input() contact: Contact;
  @Output() contactEdited: EventEmitter<void> = new EventEmitter();
  @ViewChild(ContactFormComponent) private contactFormComponent: ContactFormComponent;
  private componentDestroyed: Subject<void> = new Subject();

  readonly errorTypes: ErrorTypes = {
    base: 'contacts.form.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'validationError'
      }, {
        code: NOT_FOUND,
        text: 'notFound'
      }
    ]
  };

  constructor(private sliderService: SliderService,
              private contactService: ContactsService,
              private notificationService: NotificationService,
              private phoneService: PhoneService) {
  }

  ngOnInit(): void {
  }


  updateContact(): void {
    if (!this.contactFormComponent.isFormValid()) {
      return;
    }
    const updatedContact: any = this.contactFormComponent.getContact();
    updatedContact.additionalPhones = this.phoneService.mapAdditionalPhones(updatedContact.additionalPhones);
    this.contactService.updateContact(this.contact.id, updatedContact)
      .subscribe(
        () => {
          this.notifyUpdateSuccess();
          this.emitContactEdited();
          this.closeSlider();
        },
        (err) => this.notifyError(err)
      );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private notifyUpdateSuccess(): void {
    this.notificationService.notify('contacts.form.notification.updated', NotificationType.SUCCESS);
  }

  private notifyError(err: HttpErrorResponse): void {
    this.notificationService.notifyError(this.errorTypes, err.status);
  }

  closeSlider(): void {
    this.contactFormComponent.reset();
    this.sliderService.closeSlider();
  }

  addAdditionalField(): void {
    this.contactFormComponent.addAdditionalField();
  }

  private emitContactEdited(): void {
    this.contactEdited.emit();
  }

}
