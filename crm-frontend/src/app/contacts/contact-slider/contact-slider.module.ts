import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ContactAddComponent} from './contact-add/contact-add.component';
import {ContactEditComponent} from './contact-edit/contact-edit.component';
import {ContactFormComponent} from './contact-form/contact-form.component';
import {SocialMediaComponent} from './contact-form/social-media/social-media.component';
import {EditSocialMediaComponent} from './contact-form/edit-social-media/edit-social-media.component';
import {SharedModule} from '../../shared/shared.module';
import {CustomerSliderModule} from '../../customer/customer-slider/customer-slider.module';
import {AvatarModule} from '../../shared/avatar-form/avatar.module';
import {NgxMaskModule} from 'ngx-mask';
import {AdditionalDataFormModule} from '../../shared/additional-data-form/additional-data-form.module';
import {AdditionalPhoneNumbersModule} from '../../shared/additional-phone-numbers/additional-phone-numbers.module';

@NgModule({
  declarations: [
    ContactAddComponent,
    ContactEditComponent,
    ContactFormComponent,
    SocialMediaComponent,
    EditSocialMediaComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    NgxMaskModule,
    CustomerSliderModule,
    AvatarModule,
    AdditionalDataFormModule,
    AdditionalPhoneNumbersModule,
  ],
  exports: [
    ContactAddComponent,
    ContactEditComponent,
    ContactFormComponent,
    SocialMediaComponent,
    EditSocialMediaComponent
  ],
})
export class ContactSliderModule { }
