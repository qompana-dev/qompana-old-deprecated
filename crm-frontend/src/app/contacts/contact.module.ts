import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ContactRoutingModule} from './contact-routing.module';
import {ContactsListComponent} from './contacts-list/contacts-list.component';
import {ContactSearchComponent} from './contacts-list/contact-search/contact-search.component';
import {SharedModule} from '../shared/shared.module';
import {MapViewModule} from '../map/map-view/map-view.module';
import {NgxMaskModule} from 'ngx-mask';
import {ContactLinkComponent} from './contact-details/related-contacts/contact-link/contact-link.component';
import {CustomerSliderModule} from '../customer/customer-slider/customer-slider.module';
import {RelatedContactsComponent} from './contact-details/related-contacts/related-contacts.component';
import {ContactDetailsComponent} from './contact-details/contact-details.component';
import {ContactSalesOpportunityCardComponent} from './contact-details/contact-sales-opportunity-card/contact-sales-opportunity-card.component';
import {OpportunitySliderModule} from '../opportunity/opportunity-slider/opportunity-slider.module';
import {ContactRelationsCardComponent} from './contact-details/contact-relations-card/contact-relations-card.component';
import {AddRelatedCustomerComponent} from './contact-details/add-related-customer/add-related-customer.component';
import {ContactInfoComponent} from './contact-details/contact-info/contact-info.component';
import {CustomerInfoComponent} from './contact-details/contact-info/customer-info/customer-info.component';
import {CustomerPopoverComponent} from './customer-popover/customer-popover.component';
import {RelatedCustomersComponent} from './contact-details/related-customers/related-customers.component';
import {RelatedCustomersPreviewComponent} from './contact-details/related-customers-preview/related-customers-preview.component';
import {ContactCustomerRelationSharedModule} from '../shared/contact-customer-relation-shared/contact-customer-relation-shared.module';
import {ContactSliderModule} from './contact-slider/contact-slider.module';
import {NotesModule} from '../shared/tabs/notes-tab/notes.module';
import {FilesModule} from '../files/files.module';
import {AvatarModule} from '../shared/avatar-form/avatar.module';
import {GdprComponent} from './contact-details/gdpr/gdpr.component';
import {ClipboardModule} from 'ngx-clipboard';
import {TooltipModule} from 'ngx-bootstrap';
import {MailModule} from '../mail/mail.module';
import {DashboardModule} from '../dashboard/dashboard.module';
import {WidgetModule} from '../widgets/widget.module';
import {AdditionalDataFormModule} from '../shared/additional-data-form/additional-data-form.module';
import {TabsModule} from '../shared/tabs/tabs.module';
import { ContactTabsComponent } from './contact-tabs/contact-tabs.component';

@NgModule({
  declarations: [
    ContactsListComponent,
    ContactSearchComponent,
    ContactDetailsComponent,
    ContactSalesOpportunityCardComponent,
    ContactRelationsCardComponent,
    ContactLinkComponent,
    RelatedContactsComponent,
    RelatedCustomersPreviewComponent,
    AddRelatedCustomerComponent,
    ContactInfoComponent,
    CustomerInfoComponent,
    CustomerPopoverComponent,
    RelatedCustomersComponent,
    GdprComponent,
    ContactTabsComponent,
  ],
  imports: [
    MapViewModule,
    NgxMaskModule,
    TabsModule,
    SharedModule,
    CommonModule,
    ContactRoutingModule,
    OpportunitySliderModule,
    CustomerSliderModule,
    ContactSliderModule,
    NotesModule,
    FilesModule,
    MailModule,
    ContactCustomerRelationSharedModule,
    AvatarModule,
    ClipboardModule,
    TooltipModule.forRoot(),
    DashboardModule,
    WidgetModule,
    AdditionalDataFormModule
  ]
})
export class ContactModule {
}
