import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of, Subject} from 'rxjs';
import {
  AddCustomerContactRelation,
  Contact,
  ContactAndCustomerNameDto,
  ContactCustomerRelation,
  ContactLinkDto,
  ContactLinkPreviewDto,
  ContactSalesOpportunity,
  ContactSalesOpportunityPreview,
  ContactTopBarDTO,
  CreateContactLink,
  CustomerPreview,
  RelatedContactChange,
  UpdateRelatedCustomer,
} from './contacts.model';
import {Page, PageRequest} from '../shared/pagination/page.model';
import {FilterStrategy} from '../shared/model';
import {TableUtil} from '../shared/table.util';
import {getHeaderForAuthType} from '../shared/permissions-utils';
import {catchError, map} from 'rxjs/operators';
import {ContactWithRelation} from '../customer/customer.model';
import {CrmObject} from '../shared/model/search.model';
import {UrlUtil} from '../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  relatedContactsListChanged: Subject<RelatedContactChange> = new Subject<RelatedContactChange>();

  contactOpportunitiesChanged: EventEmitter<void> = new EventEmitter<void>();

  constructor(private http: HttpClient) {
  }

  createContact(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>(`${UrlUtil.url}/crm-business-service/contacts`, contact);
  }

  getCustomerPreview(customerId: number): Observable<CustomerPreview> {
    return this.http.get<CustomerPreview>(`${UrlUtil.url}/crm-business-service/customer/${customerId}/preview`);
  }

  updateContact(contactId: number, contact: Contact, authType?: string): Observable<Contact> {
    return this.http.put<Contact>(`${UrlUtil.url}/crm-business-service/contacts/${contactId}`, contact, {headers: getHeaderForAuthType(authType)});
  }

  deleteContact(contactId: number): Observable<void> {
    return this.http.delete<void>(`${UrlUtil.url}/crm-business-service/contacts/${contactId}`);
  }

  deleteContacts(customers: Contact[]): Observable<any> {
    const params = new HttpParams().set('ids', customers.map(customer => customer.id).join(','));
    return this.http.delete<void>(`${UrlUtil.url}/crm-business-service/contacts`, {params});
  }

  getAllContacts(filterStrategy: FilterStrategy, pageRequest: PageRequest): Observable<Page<Contact>> {
    let params = new HttpParams({fromObject: pageRequest as any});
    params = TableUtil.enrichParamsWithFilterStrategy(filterStrategy, params);
    return this.http.get<Page<Contact>>(`${UrlUtil.url}/crm-business-service/contacts`, {params});
  }

  getCustomerContacts(customerId: number): Observable<Contact[]> {
    return this.http.get<Contact[]>(`${UrlUtil.url}/crm-business-service/customer/${customerId}/contacts`);
  }

  getContact(contactId: number, authType?: string): Observable<Contact> {
    const url = `${UrlUtil.url}/crm-business-service/contacts/${contactId}`;
    return this.http.get<Contact>(url, {headers: getHeaderForAuthType(authType)});
  }

  getContactForTopBar(contactId: number): Observable<ContactTopBarDTO> {
    return this.http.get<ContactTopBarDTO>(`${UrlUtil.url}/crm-business-service/contacts/${contactId}/top-bar`);
  }

  getCustomersRelatedWithContact(contactId: number): Observable<ContactCustomerRelation[]> {
    const url = `${UrlUtil.url}/crm-business-service/contacts/${contactId}/customer-links`;
    return this.http.get<ContactCustomerRelation[]>(url);
  }

  getContactOpportunities(contactId: number): Observable<ContactSalesOpportunity[]> {
    return this.http.get<ContactSalesOpportunity[]>(`${UrlUtil.url}/crm-business-service/contacts/${contactId}/opportunities`);
  }

  getContactOpportunitiesPreview(contactId: number): Observable<ContactSalesOpportunityPreview> {
    const url = `${UrlUtil.url}/crm-business-service/contacts/${contactId}/opportunities-preview`;
    return this.http.get<ContactSalesOpportunityPreview>(url);
  }

  deleteContactLink(baseContactId: number, linkedContactId: number): Observable<void> {
    const url = `${UrlUtil.url}/crm-business-service/contacts/${baseContactId}/link/${linkedContactId}`;
    return this.http.delete<void>(url);
  }

  createContactLink(baseContactId: number, linkedContactId: number, note: string): Observable<ContactLinkDto> {
    const createContactLink: CreateContactLink = {note};
    const url = `${UrlUtil.url}/crm-business-service/contacts/${baseContactId}/link/${linkedContactId}`;
    return this.http.post<ContactLinkDto>(url, createContactLink);
  }

  updateContactLink(baseContactId: number, linkedContactId: number, note: string): Observable<ContactLinkDto> {
    const createContactLink: CreateContactLink = {note};
    const url = `${UrlUtil.url}/crm-business-service/contacts/${baseContactId}/link/${linkedContactId}`;
    return this.http.put<ContactLinkDto>(url, createContactLink);
  }

  getContactLinks(contactId: number, newLinkSlider?: boolean): Observable<ContactLinkDto[]> {
    const url = `${UrlUtil.url}/crm-business-service/contacts/${contactId}/links`;
    return this.http.get<ContactLinkDto[]>(url, {headers: getHeaderForAuthType((newLinkSlider) ? 'newLinkPanel' : undefined)});
  }

  getContactLinksPreview(contactId: number): Observable<ContactLinkPreviewDto> {
    const url = `${UrlUtil.url}/crm-business-service/contacts/${contactId}/links/preview`;
    return this.http.get<ContactLinkPreviewDto>(url);
  }

  getContactLink(contactId: number, linkedContactId: number): Observable<ContactLinkDto> {
    const url = `${UrlUtil.url}/crm-business-service/contacts/${contactId}/link/${linkedContactId}`;
    return this.http.get<ContactLinkDto>(url, {headers: getHeaderForAuthType('linkEdit')});
  }

  searchContacts(pattern: string): Observable<ContactAndCustomerNameDto[]> {
    if (!pattern || pattern.length < 3) {
      return of([]);
    }
    const path = 'crm-business-service/contacts/search/with-customer';
    const url = `${UrlUtil.url}/${path}?term=${pattern}`;
    return this.http.get<ContactAndCustomerNameDto[]>(url).pipe(catchError(() => of([])));
  }

  addRelatedCustomerToContact(relatedCustomer: AddCustomerContactRelation): Observable<AddCustomerContactRelation> {
    const url = `${UrlUtil.url}/crm-business-service/contacts/${relatedCustomer.contactId}/link/customers/${relatedCustomer.customerId}`;
    return this.http.post<AddCustomerContactRelation>(url, relatedCustomer);
  }

  deleteRelation(linkId: number): Observable<void> {
    const url = `${UrlUtil.url}/crm-business-service/contact-customer-links/${linkId}`;
    return this.http.delete<void>(url);
  }

  updateContactCustomerRelations(update: UpdateRelatedCustomer[], authType: 'customer' | 'contact'): Observable<ContactCustomerRelation[]> {
    const url = `${UrlUtil.url}/crm-business-service/contact-customer-links`;
    return this.http.put<ContactCustomerRelation[]>(url, update, {headers: getHeaderForAuthType(authType)});
  }

  getContactName(contactId: number): Observable<string> {
    return this.http.get(`${UrlUtil.url}/crm-business-service/contacts/${contactId}/name`, {responseType: 'text'});
  }

  createContactWithRelation(contact: Partial<ContactWithRelation>): Observable<ContactWithRelation> {
    return this.http.post<ContactWithRelation>(`${UrlUtil.url}/crm-business-service/contacts/with-relation`, contact);
  }

  getContactWithRelation(contactId: number): Observable<ContactWithRelation> {
    const url = `${UrlUtil.url}/crm-business-service/contacts/${contactId}/with-relation`;
    return this.http.get<ContactWithRelation>(url);
  }

  updateContactWithRelation(contactId: number, contact: Partial<ContactWithRelation>): Observable<ContactWithRelation> {
    return this.http.put<ContactWithRelation>(`${UrlUtil.url}/crm-business-service/contacts/${contactId}/with-relation`, contact);
  }

  getContactsByIds(customerIds: number[]): Observable<CrmObject[]> {
    if (!customerIds || customerIds.length === 0) {
      return of([]);
    }
    const params = new HttpParams().set('ids', customerIds.join(','));
    return this.http.get<CrmObject[]>(`${UrlUtil.url}/crm-business-service/contacts/crm-object`, {params});
  }

  archiveContact(contactId: number): Observable<any>{
    return this.http.put(`${UrlUtil.url}/crm-business-service/contact/${contactId}/archive`, {});
  }

  archiveContacts(contacts: Contact[]): Observable<any>{
    const contactIds = contacts.map(contact => contact.id).join(',');
    return this.http.put(`${UrlUtil.url}/crm-business-service/contact/archive?ids=${contactIds}`, {});
  }
}
