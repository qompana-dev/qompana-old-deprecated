import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ContactsService} from '../contacts.service';
import {Contact} from '../contacts.model';
import {MatTable, Sort} from '@angular/material';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {BAD_REQUEST, NOT_FOUND} from 'http-status-codes';
import {HttpErrorResponse} from '@angular/common/http';
import {SliderComponent} from '../../shared/slider/slider.component';
import {CustomerService} from '../../customer/customer.service';
import { map, startWith, takeUntil, throttleTime, finalize } from 'rxjs/operators';
import {AuthService} from '../../login/auth/auth.service';
import {Observable, Subject, of} from 'rxjs';
import {Page, PageRequest} from '../../shared/pagination/page.model';
import {FilterStrategy} from '../../shared/model';
import {DeleteDialogService} from '../../shared/dialog/delete/delete-dialog.service';
import {DeleteDialogData} from '../../shared/dialog/delete/delete-dialog.component';
import {Router} from '@angular/router';
import {PersonService} from '../../person/person.service';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss']
})
export class ContactsListComponent implements OnInit, OnDestroy {

  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild('editSlider') editSlider: SliderComponent;
  @ViewChild('addSlider') addSlider: SliderComponent;
  contactsPage: Page<Contact>;
  columns = ['selectRow', 'avatar', 'name', 'customer.name', 'position', 'phone', 'email', 'owner', 'action'];
  expandColumns = ['avatar'];
  userMap: Map<number, string>;
  filteredColumns$: Observable<string[]>;
  contact: Contact;
  collapsed = true;

  loadingFinished = true;
  private componentDestroyed: Subject<void> = new Subject();
  private fetchContacts: Subject<void> = new Subject();

  private currentSort = 'id,asc';

  private contactId: number;
  private pageOnList = 20;
  totalElements = 0;

  search = "";
  filterSelection = FilterStrategy;
  selectedFilter: FilterStrategy = FilterStrategy.ALL;

  readonly deleteContactErrors: ErrorTypes = {
    base: 'contacts.list.notification.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'contactNotFound'
      }, {
        code: BAD_REQUEST,
        text: 'usedInOpportunity'
      }
    ]
  };

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'contacts.list.dialog.remove.title',
    description: 'contacts.list.dialog.remove.description',
    noteFirst: 'contacts.list.dialog.remove.noteFirstPart',
    noteSecond: 'contacts.list.dialog.remove.noteSecondPart',
    confirmButton: 'contacts.list.dialog.remove.buttonYes',
    cancelButton: 'contacts.list.dialog.remove.buttonNo',
  };

  constructor(private contactService: ContactsService,
              private notificationService: NotificationService,
              private customerService: CustomerService,
              private authService: AuthService,
              private deleteDialogService: DeleteDialogService,
              private personService: PersonService,
              private $router: Router) {
  }

  ngOnInit(): void {
    this.getAccountEmails();
    this.getContacts(FilterStrategy.ALL);
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.fetchContacts.next();
    this.componentDestroyed.complete();
    this.fetchContacts.complete();
  }

  edit(contactId: number): void {
    this.loadContact(contactId);
    this.editSlider.toggle();
  }

  public filter(selection: FilterStrategy): void {
    this.selectedFilter = selection;
    this.getContacts(selection);
  }

  public isSelected(selection: FilterStrategy): boolean {
    return this.selectedFilter === selection;
  }

  public tryDelete(contactId: number): void {
    this.contactId = contactId;
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteContact();
    this.deleteDialogService.open(this.deleteDialogData);
  }

  public tryDeleteSelectedContacts(): void {
    this.deleteDialogData.numberToDelete = this.contactsPage.content
      .filter(contact => !!contact.checked).length;
    this.deleteDialogData.onConfirmClick = () => this.deleteSelectedContacts();
    this.deleteDialogService.open(this.deleteDialogData);
  }

  public handleContactEdited(): void {
    this.getContacts(this.selectedFilter);
    this.editSlider.close();
  }

  public handleContactAdded(): void {
    this.getContacts(this.selectedFilter);
    this.addSlider.close();
  }

  public onSortChange($event: Sort): void {
    this.currentSort = $event.direction ? ($event.active + ',' + $event.direction) : 'id,asc';
    this.getContacts(this.selectedFilter);
  }

  goToContactDetails(id: number): void {
    this.$router.navigate(['/contacts', id]);
  }

  private getAccountEmails(): void {
    this.personService.getIdNameMap()
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      users => this.userMap = users,
      () => this.notificationService.notify('contacts.details.notifications.accountEmailsError', NotificationType.ERROR)
    );
  }

  private getContacts(selection: FilterStrategy, pageRequest: PageRequest =
    {page: '0', size: this.pageOnList + '', sort: this.currentSort}): void {
      this.fetchContacts.next();
      this.loadingFinished = false;
    this.contactService.getAllContacts(selection, pageRequest)
      .pipe(takeUntil(this.fetchContacts),
      finalize(() => this.loadingFinished = true)
      )
      .subscribe(
        (page) => this.handleContactsSuccess(page),
        () => this.notifyLoadContactsError()
      );
  }

  changePagination(page) {
    const { pageIndex, pageSize } = page;
    this.pageOnList = pageSize;
    this.getContacts(FilterStrategy.ALL, { page: pageIndex, size: pageSize, sort: this.currentSort })
  }

  private handleContactsSuccess(page: any): void {
    this.totalElements = page.totalElements;
    this.contactsPage = page;
  }

  private deleteContact(): void {
    this.contactService.deleteContact(this.contactId)
      .subscribe(
        () => {
          this.getContacts(this.selectedFilter);
          this.notifyDeleteSuccess();
        },
        (err) => this.notifyError(this.deleteContactErrors, err)
      );
  }

  public deleteSelectedContacts(): void {
    if (this.contactsPage && this.contactsPage.content) {
      const toDelete: Contact[] = this.contactsPage.content
        .filter(contact => !!contact.checked);

      this.contactService.deleteContacts(toDelete)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => {
            this.getContacts(this.selectedFilter);
            this.notifyDeleteSuccess();
          },
          (err) => this.notifyError(this.deleteContactErrors, err)
        );
    }
  }

  private notifyError(possibleErrors: ErrorTypes, err: HttpErrorResponse): void {
    this.notificationService.notifyError(possibleErrors, err.status);
  }

  private notifyDeleteSuccess(): void {
    this.notificationService.notify('contacts.list.notification.removed', NotificationType.SUCCESS);
  }

  private notifyLoadContactsError(): void {
    this.notificationService.notify('contacts.list.notification.undefinedError', NotificationType.ERROR);
  }

  private getFilteredColumns(): string[] {
    return this.columns
      .filter((item) => {
        if (this.collapsed && this.expandColumns.includes(item)) {
          return false;
        }
        return !this.authService.isPermissionPresent('ContactListComponent.' + item + 'Column', 'i')
        || item === 'action' || item === 'selectRow' || item === 'avatar'
      });
  }

  onExpandCollapse(state) {
    this.collapsed = state;
    this.filteredColumns$ = of(this.getFilteredColumns());
  }

  private loadContact(contactId: number): void {
    this.contactService.getContact(contactId).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      (contact) => {
        this.contact = undefined;
        setTimeout(() => this.contact = contact, 100);
      },
      (err) => this.notifyError(this.deleteContactErrors, err)
    );
  }

  isAnyContactChecked(): boolean {
    return this.contactsPage && this.contactsPage.content && this.contactsPage.content.some(customer => customer.checked);
  }

  openCustomerPage(customerId: number): void {
    const url = this.$router.serializeUrl(
      this.$router.createUrlTree(['customers', customerId])
    );
    window.open(url, '_blank');
  }

  public setSelectedSearch(search: string): void {
    this.search = search;
    this.getContacts(this.selectedFilter);
  }

  archiveContact(contactId: number) {
    this.contactService
      .archiveContact(contactId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.getContacts(this.selectedFilter);
      });
  }

  archiveContacts() {
    const contactsToArchive = this.contactsPage.content.filter(customer => customer.checked);
    this.contactService
      .archiveContacts(contactsToArchive)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.getContacts(this.selectedFilter);
      });
  }
}
