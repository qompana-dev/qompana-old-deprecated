import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormControl } from "@angular/forms";
import { Observable, of, Subject, Subscription } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  startWith,
  takeUntil,
} from "rxjs/operators";
import { MatAutocompleteSelectedEvent } from "@angular/material/autocomplete/typings/autocomplete";
import { ContactSearchService } from "./contact-search.service";
import { SearchVariantGroups } from "./contact-search.model";

@Component({
  selector: "app-contact-search",
  templateUrl: "./contact-search.component.html",
  styleUrls: ["./contact-search.component.scss"],
})
export class ContactSearchComponent implements OnInit {
  @Output() searchEvent: EventEmitter<string> = new EventEmitter<string>();

  searchControl = new FormControl();
  search = "";
  isSearched = false;
  searchVariants: SearchVariantGroups[] = [];
  searchVariantsObservable: Observable<SearchVariantGroups[]> = of(
    this.searchVariants
  );

  private componentDestroyed: Subject<void> = new Subject();
  private searchSubscription: Subscription;

  constructor(private opportunitySearchService: ContactSearchService) {}

  ngOnInit(): void {
    this.initSearchField();
  }

  ngOnDestroy(): void {
    if (this.searchSubscription) {
      this.searchSubscription.unsubscribe();
    }
    this.componentDestroyed.next();
    this.componentDestroyed.unsubscribe();
  }

  private initSearchField(): void {
    this.searchSubscription = this.searchControl.valueChanges
      .pipe(
        startWith(""),
        map((value) => value.trim()),
        filter((value) => !value || value.length > 0),
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe((value) => {
        this.search = value;
        this.initMembers();
      });
  }

  private initMembers(): void {
    this.componentDestroyed.next();
    if (this.search.length > 0) {
      this.opportunitySearchService
        .getSearchVariants(this.search)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((response) => this.handleInitSuccess(response));
    }
  }

  private handleInitSuccess(response: SearchVariantGroups[]): void {
    this.searchVariants = response;
    this.searchVariantsObservable = of(
      this.searchVariants.filter((group) => group.variants.length > 0)
    );
  }

  selectSearchVariant({ option }: MatAutocompleteSelectedEvent): void {
    this.search = option.value;
    this.emitSearchEvent();
  }

  emitSearchEvent(): void {
    if (this.search.length > 0) {
      this.isSearched = true;
      this.searchEvent.emit(this.search);
    }
  }

  clearSearch(): void {
    this.search = "";
    this.searchControl.setValue("");
    this.isSearched = false;
    this.searchVariantsObservable = of([]);
    this.searchEvent.emit(this.search);
  }
}
