import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SearchVariantGroups} from "./contact-search.model";
import {UrlUtil} from "../../../shared/url.util";

@Injectable({
  providedIn: 'root'
})
export class ContactSearchService {

  private searchVariantsEndpoint = `${UrlUtil.url}/crm-business-service/contacts/search-variants`;

  constructor(private http: HttpClient) {
  }

  getSearchVariants(search: string): Observable<SearchVariantGroups[]> {
    const params = new HttpParams({fromObject: {search}});
    return this.http.get<SearchVariantGroups[]>(this.searchVariantsEndpoint, {params});
  }

}
