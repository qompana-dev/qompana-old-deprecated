export enum SearchType {
  CONTACT_NAME = "NAME",
  CONTACT_SURNAME = "SURNAME",
  CUSTOMER_NAME = "CUSTOMER_NAME",
  PHONE = "PHONE",
  EMAIL = "EMAIL",
}

export interface SearchVariantGroups {
  type: SearchType;
  variants: string[];
}
