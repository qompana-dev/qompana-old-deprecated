import {
  Component, DoCheck,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output, Renderer2,
  ViewChild
} from '@angular/core';
import {Subject} from 'rxjs';
import {ContactsService} from '../contacts.service';
import {takeUntil} from 'rxjs/operators';
import {CustomerPreview} from '../contacts.model';
import {AssignToGroupService} from '../../shared/assign-to-group/assign-to-group.service';
import {CrmObject, CrmObjectType} from '../../shared/model/search.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-customer-popover',
  templateUrl: './customer-popover.component.html',
  styleUrls: ['./customer-popover.component.scss']
})
export class CustomerPopoverComponent implements OnInit, OnDestroy {
  // tslint:disable-next-line:variable-name
  _customerId: number;
  preview: CustomerPreview;
  fullView = false;
  opened = false;
  formElement: HTMLDivElement;
  private lastTop = 0;
  @ViewChild('popoverElement') popoverElement: ElementRef;
  @ViewChild('parentContainer') parentContainer: ElementRef;

  private componentDestroyed: Subject<void> = new Subject();
  @Output() closePopover: EventEmitter<void> = new EventEmitter<void>();

  @Input('customerId')
  set customerId(id: number) {
    this._customerId = id;
    this.fullView = false;
    if (id) {
      this.getCustomerPreview();
    }
    setTimeout(() => this.opened = id !== undefined, 500);
  }

  @Input('elementRef')
  set elementRef(element: HTMLDivElement) {
    this.formElement = element;
  }

  constructor(private contactService: ContactsService,
              private eRef: ElementRef,
              private renderer: Renderer2,
              private assignToGroup: AssignToGroupService,
              private $router: Router) {
    setInterval(() => this.changePopoverPosition(), 100);
  }

  ngOnInit(): void {
    this.getCustomerPreview();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  getCustomerPreview(): void {
    if (this._customerId) {
      this.contactService.getCustomerPreview(this._customerId)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((preview: CustomerPreview) => {
          this.preview = preview;
          if (this.preview.previewOpportunityDtoList) {
            this.preview.firstTwoOpportunities = this.preview.previewOpportunityDtoList.slice(0, 2);
          }
          this.updateOwnerName();
        });
    } else {
      this.preview = undefined;
    }
  }

  updateOwnerName(): void {
    if (this.preview && this.preview.owner) {
      this.assignToGroup.updateLabels([{id: this.preview.owner, type: CrmObjectType.user}])
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((objects: CrmObject[]) => {
          if (objects && objects.length > 0) {
            this.preview.ownerName = objects[0].label;
          }
        });
    }
  }

  openOpportunity(opportunityId: number): void {
    this.$router.navigate(['opportunity', opportunityId]);
  }

  @HostListener('document:click', ['$event'])
  clickOut(event: any): void {
    if (!this.eRef.nativeElement.contains(event.target) && this.opened) {
      this.closePopover.emit();
      this.opened = false;
    }
  }

  private changePopoverPosition(): void {
    if (this.formElement && this.popoverElement && this.popoverElement.nativeElement) {
      const popoverDomRect: DOMRect = this.popoverElement.nativeElement.getBoundingClientRect();

      const formElementY = (this.formElement.getBoundingClientRect() as DOMRect).y;
      let top = 210;
      if (formElementY > top + popoverDomRect.height) {
        top = (formElementY - popoverDomRect.height);
      }
      if (this.lastTop !== top) {
        this.lastTop = top;
        this.renderer.setStyle(this.parentContainer.nativeElement, 'top', `${this.lastTop}px`);
      }
    }
  }
}
