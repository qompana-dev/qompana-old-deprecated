import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {ContactsService} from '../../contacts.service';
import {Observable, Subject} from 'rxjs';
import {map, startWith, takeUntil} from 'rxjs/operators';
import {ContactCustomerRelation} from '../../contacts.model';
import {AuthService} from '../../../login/auth/auth.service';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {SliderComponent} from '../../../shared/slider/slider.component';

@Component({
  selector: 'app-related-customers-preview',
  templateUrl: './related-customers-preview.component.html',
  styleUrls: ['./related-customers-preview.component.scss']
})
export class RelatedCustomersPreviewComponent implements OnDestroy {

  _contactId: number;
  expanded = false;
  columns = ['customer', 'relationType', 'position', 'action'];
  relatedCustomers: ContactCustomerRelation[];
  private componentDestroyed: Subject<void> = new Subject();

  @Input() set contactId(id: number) {
    this._contactId = id;
    if (id) {
      this.getRelatedCustomers();
    }
  }

  @Output() changeTabToRelatedContact: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();
  @ViewChild('addCustomerSlider') addCustomerSlider: SliderComponent;

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'contacts.details.relatedCustomers.dialog.delete.title',
    description: 'contacts.details.relatedCustomers.dialog.delete.description',
    confirmButton: 'contacts.details.relatedCustomers.dialog.delete.confirm',
    cancelButton: 'contacts.details.relatedCustomers.dialog.delete.cancel',
  };

  constructor(private contactService: ContactsService,
              private deleteDialogService: DeleteDialogService,
              private notificationService: NotificationService,
              private translateService: TranslateService,
              private authService: AuthService,
              private router: Router) {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  public getRelatedCustomers(): void {
    this.contactService.getCustomersRelatedWithContact(this._contactId)
      .pipe(
        takeUntil(this.componentDestroyed))
      .subscribe(
        customers => {
          this.relatedCustomers = customers;
        },
        err => this.notificationService
          .notify('contacts.details.newRelatedCustomer.notification.getCustomerNameError', NotificationType.ERROR)
      );
  }

  tryDelete(linkId: number): void {
    this.deleteDialogData.onConfirmClick = () => this.deleteRelation(linkId);
    this.openDeleteDialog();
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  deleteRelation(linkId: number): void {
    this.contactService.deleteRelation(linkId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      () => {
        this.notifyDeleteRelationSuccess();
        this.getRelatedCustomers();
      },
      () => this.deleteRelationError()
    );
  }

  private notifyDeleteRelationSuccess(): void {
    this.notificationService.notify('contacts.details.relatedCustomers.notification.deleteRelationSuccess', NotificationType.SUCCESS);
  }

  private deleteRelationError(): void {
    this.notificationService.notify('contacts.details.relatedCustomers.notification.deleteRelationError', NotificationType.ERROR);
  }

  private getRelationTypeTranslation(relationType: string): string {
    return relationType && this.translateService.instant(`contacts.details.relatedCustomers.relationTypes.${relationType}`);
  }

  showDetails(customerId: number): void {
    this.router.navigate(['customers', customerId]);
  }

  addRelatedCustomer(): void {
    this.addCustomerSlider.open();
  }

}
