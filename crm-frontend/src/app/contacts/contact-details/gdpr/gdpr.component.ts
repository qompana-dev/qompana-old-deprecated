import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject, zip} from 'rxjs';
import {GdprConfigService} from '../../../global-config/gdpr-config/gdpr-config.service';
import {
  EmailDataDto,
  GdprAgreeWayDto,
  GdprApproveDto,
  GdprShareDto,
  GdprStatusDto,
  GdprTokenInfoDto,
  GdprTypeDto
} from '../../../global-config/gdpr-config/gdpr.model';
import {takeUntil} from 'rxjs/operators';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {PersonService} from '../../../person/person.service';
import {AccountEmail} from '../../../person/person.model';
import {TooltipDirective} from 'ngx-bootstrap';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {AuthService} from '../../../login/auth/auth.service';

@Component({
  selector: 'app-gdpr',
  templateUrl: './gdpr.component.html',
  styleUrls: ['./gdpr.component.scss']
})
export class GdprComponent implements OnInit, OnDestroy {

  // tslint:disable-next-line:variable-name
  _contactId: number;
  private componentDestroyed: Subject<void> = new Subject();

  private userMap: Map<number, string> = new Map<number, string>();


  private deleteGdprType: Partial<DeleteDialogData> = {
    title: 'contacts.details.gdpr.deleteDialog.title',
    description: 'contacts.details.gdpr.deleteDialog.description',
    confirmButton: 'contacts.details.gdpr.deleteDialog.yes',
    cancelButton: 'contacts.details.gdpr.deleteDialog.no',
  };

  gdprStatuses: GdprStatusDto[] = [];
  gdprTypes: GdprTypeDto[] = [];
  gdprAgreeWays: GdprAgreeWayDto[] = [];
  gdprShareDtoList: GdprShareDto[] = [];
  approvals: GdprApproveDto[] = [];
  tokenInfo: GdprTokenInfoDto;

  mailConfig: EmailDataDto;

  loading: boolean = true;
  gdprFormLink: string;

  manualListVisible = false;
  shareListVisible = false;
  tokenVisible = false;

  @ViewChild('copyTooltip') copyTooltip: TooltipDirective;
  @ViewChild('newMailSlider') newMailSlider: SliderComponent;

  @Input() contactEmail: string;

  @Input() set contactId(contactId: number) {
    if (contactId) {
      this._contactId = contactId;
      this.loading = true;
      this.getData();
    }
  }

  constructor(private gdprConfigService: GdprConfigService,
              private personService: PersonService,
              private authService: AuthService,
              private deleteDialogService: DeleteDialogService,
              private notificationService: NotificationService) {
    this.manualListVisible = !this.authService.isPermissionPresent('ContactGdprComponent.manualChangeGdpr', 'i');
    this.shareListVisible = !this.authService.isPermissionPresent('ContactGdprComponent.shareGdpr', 'i');
    this.tokenVisible = !this.authService.isPermissionPresent('ContactGdprComponent.viewGdprLink', 'i');
  }

  ngOnInit() {
    this.getUserMap();
    this.getMailInfoForContactId();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }


  saveGdprTypesSentToContact(): void {
    const toSend = this.gdprShareDtoList.filter(item => item.checked).map(item => item.type.id);
    this.gdprConfigService.setGdprTypesSentToContact(this._contactId, toSend)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.notificationService.notify('contacts.details.gdpr.notification.savedGdprTypesSentToContact', NotificationType.SUCCESS);
        this.getData();
      }, () => this.notificationService.notify('contacts.details.gdpr.notification.gdprTypesSentToContactError', NotificationType.ERROR))
  }

  saveChanges(approve: GdprApproveDto): void {
    this.gdprConfigService.updateApproveForContact(this._contactId, {
      id: approve.id,
      gdprStatusId: approve.gdprStatusId,
      gdprApproveWayId: approve.gdprApproveWayId
    }).pipe(takeUntil(this.componentDestroyed))
      .subscribe((response: GdprApproveDto) => {
        approve.gdprStatus = response.gdprStatus;
        approve.gdprApproveWay = response.gdprApproveWay;
        approve.changedBy = response.changedBy;
        approve.modified = response.modified;
      }, () => {
        approve.gdprStatusId = approve.gdprStatus.id;
        approve.gdprApproveWayId = approve.gdprApproveWay.id;
        this.notificationService.notify('contacts.details.relatedCustomers.notification.savingChangesError', NotificationType.ERROR);
      })
  }

  getDataForToken(): void {
    this.gdprConfigService.getGdprTokenInfoForContactId(this._contactId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((response: GdprTokenInfoDto) => {
        if (response && response.token) {
          this.tokenInfo = response;
          this.gdprFormLink = `https://${window.location.host}/gdpr-token/${this.tokenInfo.token}`
        }
        this.loading = false;
      }, () => {
        this.notificationService.notify('contacts.details.gdpr.notification.unableLoadGdprData', NotificationType.ERROR);
        this.loading = false;
      });
  }

  getDataForShareList(): void {
    zip(
      this.gdprConfigService.getAllGdprType('contactView'),
      this.gdprConfigService.getAllApprovalsForContact(this._contactId, 'contactViewShare'))
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((data: [GdprTypeDto[], GdprApproveDto[]]) => {
        this.handleLoadDataSuccessForShareList([data[0], (this.manualListVisible) ? this.approvals : data[1]])
      }, () => {
        this.notificationService.notify('contacts.details.gdpr.notification.unableLoadGdprData', NotificationType.ERROR);
        this.loading = false;
      });
  }

  getDataForManualChangeList(): void {
    zip(
      this.gdprConfigService.getAllApprovalsForContact(this._contactId, 'contactViewManualChange'),
      this.gdprConfigService.getAllAgreeWays('contactView'),
      this.gdprConfigService.getAllGdprStatuses('contactView'))
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((data: [GdprApproveDto[], GdprAgreeWayDto[], GdprStatusDto[]]) => {
        this.setApprovals(data[0]);
        this.gdprAgreeWays = data[1];
        this.gdprStatuses = data[2];
        this.loading = false;
      }, () => {
        this.notificationService.notify('contacts.details.gdpr.notification.unableLoadGdprData', NotificationType.ERROR);
        this.loading = false;
      });
  }

  private getData(): void {
    if (this.tokenVisible) {
      this.getDataForToken();
    }
    if (this.manualListVisible) {
      this.getDataForManualChangeList();
    }
    if (this.shareListVisible) {
      this.getDataForShareList();
    }
  }

  private handleLoadDataSuccessForShareList(data: [GdprTypeDto[], GdprApproveDto[]]): void {
    this.gdprTypes = data[0];
    this.setApprovals(data[1]);
    const checkedList: number[] = this.approvals.map(item => item.gdprType.id);
    this.gdprShareDtoList = data[0].map((item: GdprTypeDto): GdprShareDto => {
      return {
        type: item,
        checked: checkedList.indexOf(item.id) !== -1,
        approve: this.approvals.find(approve => approve.gdprType.id === item.id)
      }
    });
    this.loading = false;
  }

  private setApprovals(approvals: GdprApproveDto[]) {
    this.approvals = approvals.map(item => {
      item.gdprStatusId = item.gdprStatus.id;
      item.gdprApproveWayId = item.gdprApproveWay.id;
      return item;
    });
  }

  sendByMail(): void {
    this.newMailSlider.open();
  }

  getUserFullNameById(accountId: number): string {
    return this.userMap.get(accountId) || '-';
  }

  createLink(): void {
    this.gdprConfigService.createGdprTokenInfoForContactId(this._contactId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((response: GdprTokenInfoDto) => {
        this.tokenInfo = response;
        this.notificationService.notify('contacts.details.gdpr.notification.createdLink', NotificationType.SUCCESS);
        this.getMailInfoForContactId();
      }, () => this.notificationService.notify('contacts.details.gdpr.notification.createLinkError', NotificationType.ERROR));
  }

  tryRemoveApprove(approve: GdprApproveDto): void {
    this.deleteGdprType.onConfirmClick = () => this.removeApprove(approve);
    this.deleteDialogService.open(this.deleteGdprType)
  }

  removeApprove(approve: GdprApproveDto): void {
    const toSend = this.approvals.filter(item => item.id !== approve.id).map(item => item.gdprType.id);
    this.gdprConfigService.setGdprTypesSentToContact(this._contactId, toSend)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.notificationService.notify('contacts.details.relatedCustomers.notification.approvalRemoved', NotificationType.SUCCESS);
        this.gdprShareDtoList.forEach(item => {
          if (item.approve.id === approve.id) {
            item.checked = false;
            item.approve = undefined;
          }
        });
        this.approvals = this.approvals.filter(item => item.id !== approve.id);
      }, () => this.notificationService.notify('contacts.details.relatedCustomers.notification.approvalRemoveError', NotificationType.ERROR))
  }

  private getUserMap(): void {
    this.personService.getAccountEmails()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((accountEmails: AccountEmail[]) => {
        accountEmails.forEach(accountEmail => this.userMap.set(accountEmail.id, `${accountEmail.name} ${accountEmail.surname}`))
      }, () => this.notificationService.notify('contacts.details.gdpr.notification.loadUserListError', NotificationType.ERROR));
  }

  showCopyTooltip(): void {
    this.copyTooltip.show();
    setTimeout(() => this.copyTooltip.hide(), 500);
  }


  private getMailInfoForContactId(): void {
    this.gdprConfigService.getGdprEmailByContactId(this._contactId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((mailConfig: EmailDataDto) => {
        this.mailConfig = mailConfig;
      });
  }
}
