import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Contact} from '../../contacts.model';
import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';
import {ContactsService} from '../../contacts.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {CustomerService} from '../../../customer/customer.service';
import {CustomerWithContactsDto} from '../../../customer/customer.model';
import {FormSaveService} from '../../../shared/form/form-save.service';
import {CustomerInfoComponent} from './customer-info/customer-info.component';
import {ContactFormComponent} from '../../contact-slider/contact-form/contact-form.component';
import {BAD_REQUEST, CONFLICT, NOT_FOUND} from 'http-status-codes';
import {PhoneService} from '../../../shared/phone-fields/phone.service';

@Component({
  selector: 'app-contact-info',
  templateUrl: './contact-info.component.html',
  styleUrls: ['./contact-info.component.scss']
})
export class ContactInfoComponent implements OnInit, OnDestroy {

  // tslint:disable-next-line:variable-name
  _contactId: number;
  contact: Contact = {} as Contact;
  customer: CustomerWithContactsDto;
  baseCustomer: CustomerWithContactsDto;

  contactFromChanged = false;
  customerFromChanged = false;

  readonly formId = 'ContactInfoComponent';

  private componentDestroyed: Subject<void> = new Subject();
  @ViewChild(CustomerInfoComponent) customerInfo: CustomerInfoComponent;
  @ViewChild(ContactFormComponent) contactForm: ContactFormComponent;

  @Input() set contactId(contactId: number) {
    if (contactId) {
      this._contactId = contactId;
      this.getContact();
    }
  }

  @Output() contactChanged: EventEmitter<void> = new EventEmitter<void>();

  readonly errorTypes: ErrorTypes = {
    base: 'contacts.contactInfo.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'validationError'
      }, {
        code: NOT_FOUND,
        text: 'notFound'
      }, {
        code: CONFLICT,
        text: 'nameOrNipOccupied'
      }
    ]
  };

  constructor(private contactsService: ContactsService,
              private customerService: CustomerService,
              private formSaveService: FormSaveService,
              private notificationService: NotificationService,
              private phoneService: PhoneService) {
  }

  ngOnInit(): void {
    this.formSaveService.formSavedSource$
      .pipe(
        takeUntil(this.componentDestroyed),
        filter(id => this.formId === id)
      ).subscribe(() => this.saveChanges());
    this.formSaveService.formCanceledSource$
      .pipe(
        takeUntil(this.componentDestroyed),
        filter(id => this.formId === id)
      ).subscribe(() => this.revertChanges());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private getContact(): void {
    this.contactsService.getContact(this._contactId, 'contactInfo')
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((contact) => {
          this.contact = contact;
          this.getCustomer();
        },
        () => this.notificationService.notify('contacts.contactInfo.unableDownloadingContact', NotificationType.ERROR));
  }

  private getCustomer(customerId?: number): void {
    if (!customerId && !(this.contact && this.contact.customer && this.contact.customer.id)) {
      this.customer = null;
      return;
    }
    const id = (customerId) ? customerId : this.contact.customer.id;
    this.customerService.getCustomerForEditById(id, 'contactInfo')
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((customer: CustomerWithContactsDto) => {
        this.customer = customer;
        if (!customerId) {
          this.baseCustomer = customer;
        }
      }, () => this.notificationService.notify('contacts.contactInfo.unableDownloadingCustomer', NotificationType.ERROR));
  }

  formChanged(source: 'customer' | 'contact'): void {
    if (source === 'customer') {
      this.customerFromChanged = true;
    } else {
      this.contactFromChanged = true;
    }
    this.formSaveService.announceFormChanged(this.formId);
  }

  saveChanges(): void {
    this.saveContactIfNeeded();
  }

  revertChanges(): void {
    this.contactForm.updateForm();
    this.customer = this.baseCustomer;
    if (this.customerInfo) {
      this.customerInfo.updateForm();
    }
    this.customerFromChanged = false;
    this.contactFromChanged = false;
    this.formSaveService.announceFormTheSame(this.formId);
  }

  customerIdChanged(customerId: number): void {
    if (customerId == null) {
      this.customer = null;
    } else {
      this.getCustomer(customerId);
    }
  }

  private saveCustomerIfNeeded(): void {
    if (this.customerFromChanged) {
      if (this.customerInfo.isValid) {
        this.customerService.modifyCustomer(this.customerInfo.getCustomer(), 'contactInfo')
          .pipe(takeUntil(this.componentDestroyed))
          .subscribe(
            () => {
              this.notificationService.notify('contacts.contactInfo.notification.edited', NotificationType.SUCCESS);
              this.customerFromChanged = false;
              this.updateFormChanges();
              this.getCustomer();
            }, (error) => this.notificationService.notifyError(this.errorTypes, error.status)
          );
      }
    } else {
      this.notificationService.notify('contacts.contactInfo.notification.edited', NotificationType.SUCCESS);
    }
  }

  private saveContactIfNeeded(): void {
    if (this.contactFromChanged) {
      if (!this.contactForm.isFormValid()) {
        return;
      }
      const updatedContact: any = this.contactForm.getContact();
      updatedContact.additionalPhones = this.phoneService.mapAdditionalPhones(updatedContact.additionalPhones);
      this.contactsService.updateContact(this.contact.id, updatedContact, 'contactInfo')
        .subscribe(
          (contact: Contact) => {
            this.contactFromChanged = false;
            this.contact = contact;
            this.updateFormChanges();
            this.saveCustomerIfNeeded();
          }, (err) => this.notificationService.notifyError(this.errorTypes, err.status)
        );
    } else {
      this.saveCustomerIfNeeded();
    }
  }

  private updateFormChanges(): void {
    if (!this.customerFromChanged && !this.contactFromChanged) {
      this.formSaveService.announceFormTheSame(this.formId);
      this.contactChanged.emit();
    }
  }
}
