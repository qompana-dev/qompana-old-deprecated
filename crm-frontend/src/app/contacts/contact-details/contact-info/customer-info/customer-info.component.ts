import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {Address, CustomerWithContactsDto} from '../../../../customer/customer.model';
import {Subject, Subscription} from 'rxjs';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomerService} from '../../../../customer/customer.service';
import {takeUntil} from 'rxjs/operators';
import {NotificationService} from '../../../../shared/notification.service';
import {applyPermission} from '../../../../shared/permissions-utils';
import {FormUtil} from '../../../../shared/form.util';
import {AuthService} from '../../../../login/auth/auth.service';
import {CustomerFormUtil} from '../../../../customer/customer-slider/customer-form/customer-form.util';
import {FormValidators} from '../../../../shared/form/form-validators';
import {PhoneService} from '../../../../shared/phone-fields/phone.service';

@Component({
  selector: 'app-customer-info',
  templateUrl: './customer-info.component.html',
  styleUrls: ['./customer-info.component.scss']
})
export class CustomerInfoComponent implements OnDestroy {
  // tslint:disable-next-line:variable-name
  _customer: CustomerWithContactsDto;
  customerForm: FormGroup;

  private componentDestroyed: Subject<void> = new Subject();
  @Output() formChanged: EventEmitter<void> = new EventEmitter<void>();

  @Input() set customer(customer: CustomerWithContactsDto) {
    this.updateCustomerForm(customer);
  }

  private valueChangedSubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private customerService: CustomerService,
              private notificationService: NotificationService,
              public phoneService: PhoneService) {
    this.createCustomerForm();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  // @formatter:off
  get authKey(): string {return 'CustomerInContactInfoComponent.'; }
  get nip(): AbstractControl {return this.customerForm.get('nip'); }
  get regon(): AbstractControl {return this.customerForm.get('regon'); }
  get name(): AbstractControl {return this.customerForm.get('name'); }
  get phone(): AbstractControl {return this.customerForm.get('phone'); }
  get website(): AbstractControl {return this.customerForm.get('website'); }
  get street(): AbstractControl {return this.customerForm.get('street'); }
  get city(): AbstractControl {return this.customerForm.get('city'); }
  get zipCode(): AbstractControl {return this.customerForm.get('zipCode'); }
  get country(): AbstractControl {return this.customerForm.get('country'); }
  get avatar(): AbstractControl {return this.customerForm.get('avatar'); }
  get additionalData(): AbstractControl {return this.customerForm.get('additionalData'); }
  // @formatter:on

  get isValid(): boolean {
    FormUtil.setTouched(this.customerForm);
    return this.customerForm.valid;
  }

  public updateForm(): void {
    this.updateCustomerForm(this._customer);
  }

  private updateCustomerForm(customer: CustomerWithContactsDto): void {
    if (this.valueChangedSubscription) {
      this.valueChangedSubscription.unsubscribe();
    }
    this._customer = customer;
    if (this.customerForm && customer) {
      this.regon.patchValue(customer.regon);
      const address = (customer.address) ? customer.address : {} as Address;
      this.street.patchValue(address.street);
      this.city.patchValue(address.city);
      this.zipCode.patchValue(address.zipCode);
      this.country.patchValue(address.country);
      this.additionalData.patchValue(customer.additionalData);
      CustomerFormUtil.updateCustomerFormBasicDataInCustomerInfo(this.customerForm, customer);
    }
    this.subscribeValueChanges();
  }

  getCustomer(): CustomerWithContactsDto {
    const customer = new CustomerWithContactsDto();

    CustomerFormUtil.setBasicCustomerDataFromFormInCustomerInfo(customer, this.customerForm);

    if (this._customer) {
      customer.id = this._customer.id;
      customer.owner = this._customer.owner;
      customer.regon = this._customer.regon;
      customer.type = this._customer.type;
      customer.description = this._customer.description;
      customer.address = this._customer.address;
      customer.contacts = this._customer.contacts;
    }

    const address = new Address();
    if (this._customer && this._customer.address) {
      address.id = this._customer.id;
    }
    if (customer.address) {
      address.latitude = customer.address.latitude;
      address.longitude = customer.address.longitude;
    }
    address.street = this.street.value;
    address.city = this.city.value;
    address.zipCode = this.zipCode.value;
    address.country = this.country.value;
    customer.address = address;
    customer.additionalData = !!this.additionalData.value ? this.additionalData.value : null;

    return customer;
  }


  private createCustomerForm(): void {
    if (!this.customerForm) {
      this.customerForm = this.formBuilder.group({
        nip: ['', Validators.compose([Validators.required, FormValidators.whitespace, Validators.minLength(10)])],
        name: ['', Validators.compose([Validators.required, FormValidators.whitespace, Validators.maxLength(200)])],
        phone: ['', Validators.maxLength(128)],
        website: ['', Validators.maxLength(200)],
        regon: ['', Validators.compose([Validators.minLength(9), Validators.maxLength(14),
          Validators.pattern('^(?=[0-9]*$)(?:.{9}|.{14})$')])],
        street: ['', Validators.maxLength(200)],
        city: ['', Validators.maxLength(100)],
        zipCode: ['', Validators.minLength(5)],
        country: ['', Validators.maxLength(100)],
        avatar: [null],
        additionalData: [null]
      });
      this.applyPermissionsToForm();
      this.subscribeValueChanges();
    }
  }

  private applyPermissionsToForm(): void {
    if (this.customerForm) {
      applyPermission(this.nip, !this.authService.isPermissionPresent(`${this.authKey}nipField`, 'r'));
      applyPermission(this.name, !this.authService.isPermissionPresent(`${this.authKey}nameField`, 'r'));
      applyPermission(this.phone, !this.authService.isPermissionPresent(`${this.authKey}phoneField`, 'r'));
      applyPermission(this.website, !this.authService.isPermissionPresent(`${this.authKey}websiteField`, 'r'));
      applyPermission(this.regon, !this.authService.isPermissionPresent(`${this.authKey}regonField`, 'r'));
      applyPermission(this.street, !this.authService.isPermissionPresent(`${this.authKey}streetField`, 'r'));
      applyPermission(this.city, !this.authService.isPermissionPresent(`${this.authKey}cityField`, 'r'));
      applyPermission(this.zipCode, !this.authService.isPermissionPresent(`${this.authKey}zipCodeField`, 'r'));
      applyPermission(this.country, !this.authService.isPermissionPresent(`${this.authKey}countryField`, 'r'));
    }
  }

  private subscribeValueChanges(): void {
    this.valueChangedSubscription = this.customerForm.valueChanges
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.formChanged.emit());
  }

  checkIfNipExistsInDb(event: any): void {
    if (event !== undefined && String(event).length === 10) {
      this.customerService.getCustomerByNip(event, this._customer.id)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (val: string) => {
            return val === '0' ? null : this.nip.setErrors({nipExists: true});
          }
        );
    }
  }

}
