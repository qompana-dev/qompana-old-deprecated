import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {map, startWith, takeUntil} from 'rxjs/operators';
import {ContactsService} from '../../contacts.service';
import {ContactSalesOpportunity, ContactSalesOpportunityPreview} from '../../contacts.model';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {OpportunitySave} from '../../../opportunity/opportunity.model';
import {LoginService} from '../../../login/login.service';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {Observable, Subject} from 'rxjs';
import {OpportunityService} from '../../../opportunity/opportunity.service';
import {FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import {AuthService} from '../../../login/auth/auth.service';
import {Router} from '@angular/router';
import {CrmObject} from '../../../shared/model/search.model';

@Component({
  selector: 'app-contact-relations-card',
  templateUrl: './contact-relations-card.component.html',
  styleUrls: ['./contact-relations-card.component.scss']
})
export class ContactRelationsCardComponent implements OnInit, OnDestroy {

  @ViewChild('addSlider') addSlider: SliderComponent;
  @ViewChild('editSlider') editSlider: SliderComponent;
  @Output() tabIndexChange: EventEmitter<number> = new EventEmitter<number>();

  // tslint:disable-next-line:variable-name
  contactSalesOpportunityPreview: ContactSalesOpportunityPreview;
  opportunityWithContact: Partial<OpportunitySave>;
  selectedOpportunity: OpportunitySave;
  columns = ['name', 'status', 'amount', 'finishDate', 'action'];
  filteredColumns$: Observable<string[]>;
  private componentDestroyed: Subject<void> = new Subject();

  private readonly opportunityErrorTypes: ErrorTypes = {
    base: 'opportunity.list.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'opportunityNotFound'
      }, {
        code: FORBIDDEN,
        text: 'noPermission'
      }
    ]
  };

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'opportunity.form.dialog.delete.title',
    description: 'opportunity.form.dialog.delete.description',
    noteFirst: 'opportunity.form.dialog.delete.noteFirstPart',
    noteSecond: 'opportunity.form.dialog.delete.noteSecondPart',
    confirmButton: 'opportunity.form.dialog.delete.confirm',
    cancelButton: 'opportunity.form.dialog.delete.cancel',
  };



  @Input() set crmObject(crmObject: CrmObject) {
    const contactId = crmObject && crmObject.id;
    this._crmObject = crmObject;
    if (contactId) {
      this.subscribeForContactOpportunities(contactId);
    }
  }
  _crmObject: CrmObject;
  @Input() userMap: Map<number, string> = new Map();
  @Input() contactName: string;

  constructor(private contactsService: ContactsService,
              private loginService: LoginService,
              private opportunityService: OpportunityService,
              private deleteDialogService: DeleteDialogService,
              private notificationService: NotificationService,
              private authService: AuthService,
              private $router: Router) {
  }

  ngOnInit(): void {
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
    this.contactsService.contactOpportunitiesChanged.subscribe(
      () => this.subscribeForContactOpportunities(this._crmObject.id)
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  addOpportunity($event: any): void {
    $event.stopPropagation();
    this.contactsService.getContact(this._crmObject.id).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (contact) => {
        this.opportunityWithContact = {
          contactId: contact.id, customerId: contact.customer.id, canEditAmount: true,
          ownerOneId: this.loginService.getLoggedAccountId(), ownerOnePercentage: 100
        };
        this.addSlider.open();
      }
    );
  }

  handleOpportunityAdded(): void {
    this.contactsService.contactOpportunitiesChanged.emit();
    this.addSlider.close();
    this.editSlider.close();
  }

  getOpportunityProgress(opportunity: ContactSalesOpportunity): number {
    return opportunity.maxStatus ? (opportunity.status / opportunity.maxStatus) * 100 : 0;
  }

  edit(id: number): void {
    this.opportunityService.getOpportunityForEditForSlider(id).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      opportunity => {
        this.selectedOpportunity = opportunity;
        this.editSlider.open();
      },
      err => this.notificationService.notifyError(this.opportunityErrorTypes, err.status)
    );
  }

  tryDeleteOpportunity(opportunityId: number): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteOpportunity(opportunityId);
    this.openDeleteDialog();
  }

  private subscribeForContactOpportunities(contactId: number): void {
    this.contactsService.getContactOpportunitiesPreview(contactId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (contactSalesOpportunityPreview: ContactSalesOpportunityPreview) =>
        this.contactSalesOpportunityPreview = contactSalesOpportunityPreview,
      () => this.notificationService.notify('contacts.tabs.salesOpportunities.notifications.getOpportunitiesError', NotificationType.ERROR)
    );
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private deleteOpportunity(opportunityId: number): void {
    this.opportunityService.removeOpportunity(opportunityId).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      () => {
        this.contactsService.contactOpportunitiesChanged.emit();
        this.notificationService.notify('opportunity.list.notification.deleteSuccess', NotificationType.SUCCESS);
      },
      err => this.notificationService.notifyError(this.opportunityErrorTypes, err.status)
    );
  }

  private getFilteredColumns(): string[] {
    return this.columns
      .filter((item) => !this.authService.isPermissionPresent('ContactSalesOpportunityCardComponent.' + item, 'i') || item === 'action');
  }

  goToOpportunityDetails(id: number): void {
    this.$router.navigate(['/opportunity', id]);
  }

  changeTab($event: MouseEvent, selectedTabIndex: number): void {
    if ($event) {
      $event.stopPropagation();
    }
    this.tabIndexChange.next(selectedTabIndex);
  }
}
