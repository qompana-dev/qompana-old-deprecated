import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {ContactsService} from '../../contacts.service';
import {Observable, Subject} from 'rxjs';
import {map, startWith, takeUntil} from 'rxjs/operators';
import {ContactLinkDto, ContactLinkPreviewDto, RelatedContactChange} from '../../contacts.model';
import {AuthService} from '../../../login/auth/auth.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import {NOT_FOUND} from 'http-status-codes';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {ContactLinkComponent} from './contact-link/contact-link.component';

@Component({
  selector: 'app-related-contacts',
  templateUrl: './related-contacts.component.html',
  styleUrls: ['./related-contacts.component.scss']
})
export class RelatedContactsComponent implements OnInit, OnDestroy, RelatedContactsNotes {
  id: number;
  links: ContactLinkDto[] = [];
  linksDataSource: Subject<ContactLinkDto[]> = new Subject<ContactLinkDto[]>();
  expanded = false;
  private componentDestroyed: Subject<void> = new Subject();
  linkedContactId: number;

  columns = ['contact', 'customer', 'position', 'phone', 'email', 'action'];
  filteredColumns$: Observable<string[]>;

  noteContactId: number;

  previewMode = false;
  previewTotalCount = 0;

  @Input() set contactId(id: number) {
    if (id) {
      this.id = id;
      this.getContactLinks();
    }
  }

  @Input() set preview(preview: boolean) {
    this.previewMode = preview;
  }

  @Output() changeTabToRelatedContact: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'contacts.details.relatedContactsTab.dialog.remove.title',
    description: 'contacts.details.relatedContactsTab.dialog.remove.description',
    noteFirst: 'contacts.details.relatedContactsTab.dialog.remove.noteFirstPart',
    noteSecond: 'contacts.details.relatedContactsTab.dialog.remove.noteSecondPart',
    confirmButton: 'contacts.details.relatedContactsTab.dialog.remove.buttonYes',
    cancelButton: 'contacts.details.relatedContactsTab.dialog.remove.buttonNo',
  };
  private readonly deleteLinkError: ErrorTypes = {
    base: 'contacts.details.relatedContactsTab.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'contactLinkNotFound'
      }
    ]
  };

  @ViewChild('addContactLink') addContactLinkComponent: ContactLinkComponent;
  @ViewChild('editContactLink') editContactLinkComponent: ContactLinkComponent;
  @ViewChild('addLinkSlider') addContactLinkSlider: SliderComponent;
  @ViewChild('editLinkSlider') editLinkSlider: SliderComponent;

  constructor(private contactService: ContactsService,
              private deleteDialogService: DeleteDialogService,
              private notificationService: NotificationService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.getContactLinks();
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
    this.contactService.relatedContactsListChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((change: RelatedContactChange) => this.handleListChangedEvent(change));
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private getFilteredColumns(): string[] {
    const key = 'ContactLinkListComponent';
    return this.columns
      .filter((item) => !this.authService.isPermissionPresent(key + '.' + item + 'Column', 'i') || item === 'action');
  }

  linkAdded(link: ContactLinkDto): void {
    this.contactService.relatedContactsListChanged.next({type: 'CREATE', link});
  }

  linkEdited(link: ContactLinkDto): void {
    this.contactService.relatedContactsListChanged.next({type: 'UPDATE', link});
  }

  private getContactLinks(): void {
    if (this.previewMode) {
      this.getContactLinksPreview();
    } else {
      this.getContactLinksNormalList();
    }
  }

  private getContactLinksNormalList(): void {
    if (this.id) {
      this.contactService.getContactLinks(this.id)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((links: ContactLinkDto[]) => this.handleSuccess(links),
          () => this.handleErrorCode());
    } else {
      this.linksDataSource.next(this.links);
    }
  }

  private getContactLinksPreview(): void {
    if (this.id) {
      this.contactService.getContactLinksPreview(this.id)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((preview: ContactLinkPreviewDto) => {
          this.previewTotalCount = preview.count;
          this.handleSuccess(preview.links);
        }, () => this.handleErrorCode());
    }
  }

  handleSuccess(links: ContactLinkDto[]): void {
    this.links = links;
    if (this.links && this.links.length > 0) {
      this.noteContactId = this.links[0].linkedContactId;
    }
    this.linksDataSource.next(this.links);
  }


  public tryDeleteContactLink(link: ContactLinkDto): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteContactLink(link);
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private deleteContactLink(link: ContactLinkDto): void {
    this.contactService.deleteContactLink(this.id, link.linkedContactId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.notificationService.notify('contacts.details.relatedContactsTab.notification.removedSuccessfully', NotificationType.SUCCESS);
        this.contactService.relatedContactsListChanged.next({type: 'DELETE', link});
      }, (err) => this.notificationService.notifyError(this.deleteLinkError, err.status));
  }

  handleErrorCode(): void {
    this.notificationService.notify('contacts.details.relatedContactsTab.notification.downloadLinkError',
      NotificationType.ERROR);
  }

  openAddLinkSlider($event?: MouseEvent): void {
    if ($event) {
      $event.stopPropagation();
    }
    this.addContactLinkComponent.getExistingLinks(this.id);
    this.addContactLinkSlider.open();
  }

  openEditLinkSlider(linkedContactId: number): void {
    this.linkedContactId = linkedContactId;
    this.editContactLinkComponent.linkedContactId = this.linkedContactId;
    this.editContactLinkComponent.getExistingLinks(this.id);
    this.editLinkSlider.open();
  }

  private handleListChangedEvent(change: RelatedContactChange) {
    switch (change.type) {
      case 'CREATE':
        this.links = [change.link].concat(this.links);
        this.linksDataSource.next(this.links);
        break;
      case 'UPDATE':
        for (let i = 0; i < this.links.length; i++) {
          if (this.links[i].linkedContactId === change.link.linkedContactId) {
            this.links[i] = change.link;
          }
        }
        this.linksDataSource.next(this.links);
        break;
      case 'DELETE':
        const index = this.links.map((item) => item.linkedContactId).indexOf(change.link.linkedContactId);
        if (index !== -1) {
          this.links.splice(index, 1);
          this.linksDataSource.next(this.links);
        }
        break;
    }
  }

  currentNote(): string {
    const contactIdIndex = this.getContactIdIndex();
    return (this.links[contactIdIndex]) ? this.links[contactIdIndex].note : undefined;
  }

  nextNote(): void {
    this.noteContactId = this.links[this.getContactIdIndex() + 1].linkedContactId;
  }

  prevNote(): void {
    this.noteContactId = this.links[this.getContactIdIndex() - 1].linkedContactId;
  }

  prevNoteButtonEnabled(): boolean {
    return this.getContactIdIndex() > 0;
  }

  nextNoteButtonEnabled(): boolean {
    return this.getContactIdIndex() !== this.links.length - 1;
  }

  getContactIdIndex(): number {
    return this.links.map((item) => item.linkedContactId).indexOf(this.noteContactId);
  }
}

export interface RelatedContactsNotes {
  currentNote(): string;

  nextNote(): void;

  prevNote(): void;

  prevNoteButtonEnabled(): boolean;

  nextNoteButtonEnabled(): boolean;
}
