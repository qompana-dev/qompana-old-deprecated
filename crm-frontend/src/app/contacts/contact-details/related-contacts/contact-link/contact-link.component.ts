import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CrmObject, CrmObjectType} from '../../../../shared/model/search.model';
import {Contact, ContactAndCustomerNameDto, ContactLinkDto} from '../../../contacts.model';
import {debounceTime, filter, finalize, switchMap, takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {AssignToGroupService} from '../../../../shared/assign-to-group/assign-to-group.service';
import {ContactsService} from '../../../contacts.service';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {SliderService} from '../../../../shared/slider/slider.service';
import {FormUtil} from '../../../../shared/form.util';
import {ErrorTypes, NotificationService, NotificationType} from '../../../../shared/notification.service';
import {BAD_REQUEST, CONFLICT, NOT_FOUND} from 'http-status-codes';
import {applyPermission} from '../../../../shared/permissions-utils';
import {AuthService} from '../../../../login/auth/auth.service';

@Component({
  selector: 'app-contact-link',
  templateUrl: './contact-link.component.html',
  styleUrls: ['./contact-link.component.scss']
})
export class ContactLinkComponent implements OnInit, OnDestroy {

  contactLinkForm: FormGroup;
  private componentDestroyed: Subject<void> = new Subject();

  filteredContacts: ContactAndCustomerNameDto[] = [];

  contactSearchLoading = false;
  storedBaseContactId: number;
  storedLinkedContactId: number;
  existingLinks: number[] = [];

  @Input() set baseContactId(baseContactId: number) {
    this.storedBaseContactId = baseContactId;
    this.getExistingLinks(baseContactId);
  }

  @Input() set linkedContactId(linkedContactId: number) {
    this.storedLinkedContactId = linkedContactId;
    this.getLinkedContact();
  }

  @Input() slider: SliderComponent;
  @Output() linkSaved: EventEmitter<ContactLinkDto> = new EventEmitter<ContactLinkDto>();

  linkedContactOwnerId: number;
  linkedContactOwner: string;

  readonly addLinkErrorTypes: ErrorTypes = {
    base: 'contacts.link.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'badRequest'
      },
      {
        code: NOT_FOUND,
        text: 'contactNotFound'
      },
      {
        code: CONFLICT,
        text: 'linkExist'
      }
    ]
  };

  constructor(private fb: FormBuilder,
              private sliderService: SliderService,
              private authService: AuthService,
              private notificationService: NotificationService,
              private contactService: ContactsService,
              private assignToGroupService: AssignToGroupService) {
    this.createOpportunityForm();
  }

  ngOnInit(): void {
    this.subscribeForPermissionsChanges();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  // @formatter:off
  get customer(): AbstractControl { return this.contactLinkForm.get('customer'); }
  get contactId(): AbstractControl { return this.contactLinkForm.get('contactId'); }
  get contactSearch(): AbstractControl { return this.contactLinkForm.get('contactSearch'); }
  get note(): AbstractControl { return this.contactLinkForm.get('note'); }
  // @formatter:on

  private setOwner(): void {
    if (this.linkedContactOwnerId) {
      this.assignToGroupService.updateLabels([{id: this.linkedContactOwnerId, type: CrmObjectType.user}])
        .pipe(
          takeUntil(this.componentDestroyed),
          filter(objects => objects.length > 0)
        ).subscribe((objects: CrmObject[]) => {
        this.linkedContactOwner = objects[0].label;
      });
    }
  }

  private createOpportunityForm(): void {
    this.contactLinkForm = this.fb.group({
      customer: [null],
      contactId: [null, Validators.required],
      contactSearch: [null, Validators.required],
      note: [null]
    });
    this.customer.disable();
    this.subscribeContactId();
    this.subscribeContactSearch();
    this.applyPermissionsToForm();
  }

  private subscribeContactSearch(): void {
    this.contactSearch.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      filter((input: string) => !!input && input.length >= 3),
      tap(() => this.contactSearchLoading = true),
      switchMap(((input: string) => this.contactService.searchContacts(input)
          .pipe(finalize(() => this.contactSearchLoading = false))
      ))).subscribe(
      (result: ContactAndCustomerNameDto[]) => {
        this.filteredContacts = result.filter((obj => this.existingLinks.indexOf(obj.id) === -1 && obj.id !== this.storedBaseContactId));
      }
    );
  }

  private subscribeContactId(): void {
    this.contactId.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed),
        filter((value) => !!value),
      ).subscribe((value) => {
      if (value) {
        this.contactService.getContact(this.contactId.value, 'newLinkPanel')
          .pipe(takeUntil(this.componentDestroyed))
          .subscribe((contact: Contact) => {
            this.customer.patchValue(contact.customer.name);
            this.linkedContactOwnerId = contact.owner;
            this.setOwner();
          }, () => {
            this.notificationService.notify('contacts.link.notification.contactNotFound', NotificationType.ERROR);
          });
      }
    });
  }

  public getExistingLinks(id: number): void {
    if (id == null) {
      return;
    }
    this.contactService.getContactLinks(id, true)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((links: ContactLinkDto[]) => {
        this.existingLinks = links.map(link => link.linkedContactId);
      }, () => {
        this.notificationService.notify('contacts.link.notification.contactNotFound', NotificationType.ERROR);
      });
  }

  contactSelect(contact: ContactAndCustomerNameDto): void {
    if (contact) {
      this.contactId.patchValue(contact.id);
      this.contactSearch.patchValue(contact.contactName);
    }
  }

  submit(): void {
    FormUtil.setTouched(this.contactLinkForm);
    if (this.contactLinkForm.valid) {
      const save = (this.storedLinkedContactId) ?
        this.contactService.updateContactLink(this.storedBaseContactId, this.contactId.value, this.note.value) :
        this.contactService.createContactLink(this.storedBaseContactId, this.contactId.value, this.note.value);

      save.pipe(takeUntil(this.componentDestroyed))
        .subscribe((contactLink: ContactLinkDto) => {
          this.notificationService.notify('contacts.link.notification.linkSavedSuccessfully', NotificationType.SUCCESS);
          this.existingLinks.push(this.contactId.value);
          this.linkSaved.emit(contactLink);
          this.closeSlider();
        }, (err) => this.notificationService.notifyError(this.addLinkErrorTypes, err.status));
    }
  }

  closeSlider(): void {
    this.contactLinkForm.reset();
    (this.slider) ? this.slider.close() : this.sliderService.closeSlider();
  }

  newContactAdded(newContact: { id: number, name: string, surname: string }): void {
    this.contactSearch.patchValue(`${newContact.name} ${newContact.surname}`);
    this.contactId.patchValue(newContact.id);
  }


  private subscribeForPermissionsChanges(): void {
    this.authService.onAuthChange.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.applyPermissionsToForm()
    );
  }

  private getLinkedContact(): void {
    if (this.storedLinkedContactId) {
      this.contactService.getContactLink(this.storedBaseContactId, this.storedLinkedContactId)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((contactLink: ContactLinkDto) => this.updateForm(contactLink),
          () => this.notificationService.notify('contacts.link.notification.linkNotFound', NotificationType.ERROR));
    }
  }

  private updateForm(contactLink: ContactLinkDto): void {
    this.createOpportunityForm();
    this.contactId.patchValue(contactLink.linkedContactId);
    this.contactSearch.patchValue(contactLink.linkedContactName + ' ' + contactLink.linkedContactSurname);
    this.customer.patchValue(contactLink.linkedCustomerName);
    this.note.patchValue(contactLink.note);
    this.applyPermissionsToForm();
  }

  private applyPermissionsToForm(): void {
    if (this.contactLinkForm) {
      this.customer.disable();
      applyPermission(this.contactSearch,
        !this.storedLinkedContactId && !this.authService.isPermissionPresent(`ContactLinkAddComponent.contact`, 'r'));
      applyPermission(this.contactId,
        !this.storedLinkedContactId && !this.authService.isPermissionPresent(`ContactLinkAddComponent.contact`, 'r'));
      applyPermission(this.note,
        !this.authService.isPermissionPresent(`ContactLinkAddComponent.note`, 'r'));
    }
  }

}
