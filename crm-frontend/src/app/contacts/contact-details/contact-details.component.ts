import {AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Contact, SocialMedia} from '../contacts.model';
import {takeUntil} from 'rxjs/operators';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {PersonService} from '../../person/person.service';
import {Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {ContactsService} from '../contacts.service';
import {BAD_REQUEST, NOT_FOUND} from 'http-status-codes';
import {DeleteDialogData} from '../../shared/dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../shared/dialog/delete/delete-dialog.service';
import {FormSaveService} from '../../shared/form/form-save.service';
import {CrmObject, CrmObjectType} from '../../shared/model/search.model';
import {Note} from '../../widgets/notes/notes.model';
import {NoteListComponent} from '../../shared/tabs/notes-tab/note-list/note-list.component';
import {MatTab, MatTabChangeEvent} from '@angular/material';
import {SliderComponent} from '../../shared/slider/slider.component';
import {CrmFileUploaderService} from '../../files/crm-file-uploader.service';
import {FormValidators} from '../../shared/form/form-validators';
import {AbstractControl, FormBuilder, FormGroup, Validators, } from '@angular/forms';
import * as cloneDeep from 'lodash/cloneDeep';
import {PhoneService} from '../../shared/phone-fields/phone.service';
import {AdditionalPhoneNumber} from '../../shared/additional-phone-numbers/additional-phone-numbers.model';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss'],
  providers: [FormSaveService, CrmFileUploaderService]
})
export class ContactDetailsComponent implements OnInit, OnDestroy, AfterViewChecked {
  loading = false;
  contactId: number;
  contact: Contact;
  userMap: Map<number, string>;
  selectedTabIndex = 0;
  private componentDestroyed: Subject<void> = new Subject();
  crmObjectType = CrmObjectType;
  socialMedia = {} as SocialMedia;

  contactForm: FormGroup;
  selectedNote: Note;
  @ViewChild(NoteListComponent) noteListComponent: NoteListComponent;
  @ViewChild('notesTab') notesTab: MatTab;
  @ViewChild('newMailSlider') newMailSlider: SliderComponent;
  @ViewChild('editSlider') editSlider: SliderComponent;
  contactToEdit: Contact;
  showActionButtons = false;

  contactChangedSubscription: Subject<void> = new Subject();

  readonly undefinedCategoryId = -1000;
  formControlFocusedKey: string;
  formControlFocusedValue: string;
  formChanged = false;
  baseContact: Contact = {} as Contact;

  get name(): AbstractControl {
    return this.contactForm.get('name');
  }

  get surname(): AbstractControl {
    return this.contactForm.get('surname');
  }

  get position(): AbstractControl {
    return this.contactForm.get('position');
  }

  get phone(): AbstractControl {
    return this.contactForm.get('phone');
  }

  get email(): AbstractControl {
    return this.contactForm.get('email');
  }

  get owner(): AbstractControl {
    return this.contactForm.get('owner');
  }

  get additionalPhones(): AbstractControl {
    return this.contactForm.get('additionalPhones');
  }

  readonly getContactErrorTypes: ErrorTypes = {
    base: 'contacts.details.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'notFound'
      }
    ]
  };

  readonly deleteContactErrors: ErrorTypes = {
    base: 'contacts.list.notification.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'contactNotFound'
      }, {
        code: BAD_REQUEST,
        text: 'usedInOpportunity'
      }
    ]
  };

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'contacts.list.dialog.remove.title',
    description: 'contacts.list.dialog.remove.description',
    noteFirst: 'contacts.list.dialog.remove.noteFirstPart',
    noteSecond: 'contacts.list.dialog.remove.noteSecondPart',
    confirmButton: 'contacts.list.dialog.remove.buttonYes',
    cancelButton: 'contacts.list.dialog.remove.buttonNo',
  };
  crmObject: CrmObject;
  widgetsVisible = true;

  constructor(private activatedRoute: ActivatedRoute,
              private personService: PersonService,
              private contactsService: ContactsService,
              private deleteDialogService: DeleteDialogService,
              private $router: Router,
              private fb: FormBuilder,
              private notificationService: NotificationService,
              public formSaveService: FormSaveService,
              private cdr: ChangeDetectorRef,
              public phoneService: PhoneService) {
    this.createContactForm();
  }

  ngOnInit(): void {
    this.getAccountEmails();
    this.subscribeForContact();
  }

  ngAfterViewChecked(): void {
    this.cdr.detectChanges();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private createContactForm(): void {
    this.contactForm = this.fb.group({
      name: ['', Validators.compose([
        Validators.maxLength(200),
        FormValidators.whitespace,
        Validators.required])
      ],
      surname: ['', Validators.compose([
        Validators.maxLength(200),
        FormValidators.whitespace,
        Validators.required])
      ],
      position: ['', Validators.compose([
        Validators.maxLength(100),
        FormValidators.whitespace])
      ],
      phone: ['', Validators.maxLength(128)],
      additionalPhones: [[]],
      email: ['', Validators.compose([
        Validators.maxLength(100),
        Validators.email
      ])],
      owner: [null]
    });
  }

  public tryDelete(contactId: number): void {
    this.contactId = contactId;
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteContact();
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private deleteContact(): void {
    this.contactsService.deleteContact(this.contactId)
      .subscribe(
        () => {
          this.notificationService.notify('contacts.list.notification.removed', NotificationType.SUCCESS);
          this.$router.navigate(['/contacts']);
        },
        (err) => this.notificationService.notifyError(this.deleteContactErrors, err.status)
      );
  }

  private setDefaultOwnerIfNull(): void {
    if (this.contact && this.contact.owner) {
      this.owner.patchValue(this.contact.owner);
    } else {
      const loggedAccountId = +localStorage.getItem('loggedAccountId');
      if (loggedAccountId) {
        this.owner.patchValue(loggedAccountId);
      }
    }
  }

  private getAccountEmails(): void {
    this.personService.getAndMapAccountEmails(this.componentDestroyed)
      .subscribe(
        (users) => (this.userMap = users),
        () => this.notificationService.notify('contacts.details.notifications.accountEmailsError', NotificationType.ERROR)
      );
  }

  private subscribeForContact(): void {
    this.activatedRoute.params.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(params => {
        this.contactId = +params.id;
        this.crmObject = {id: this.contactId, type: CrmObjectType.contact};
        this.getContact(this.contactId);
      }
    );
  }

  public edit(leadId: number): void {
    this.contactToEdit = cloneDeep(this.contact);
    this.editSlider.open();
  }

  public handleContactEdited(): void {
    this.editSlider.close();
    this.refreshContact();
  }

  public handleContactChangedSubscription(): void {
    this.contactChangedSubscription.next();
  }

  refreshContact(): void {
    this.getContact(this.contactId);
  }

  private getContact(contactId: number): void {
    this.loading = true;
    this.contactsService.getContact(contactId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      (contact: Contact) => {
        this.contact = contact;
        this.contact.additionalPhones = this.phoneService.mapAdditionalPhones(contact.additionalPhones);
        this.socialMedia.facebookLink = contact.facebookLink;
        this.socialMedia.twitterLink = contact.twitterLink;
        this.socialMedia.linkedInLink = contact.linkedInLink;
        this.baseContact = cloneDeep(this.contact);
        this.setDefaultOwnerIfNull();
        this.contactForm.reset(contact);
      },
      (err) => this.notificationService.notifyError(this.getContactErrorTypes, err.status),
      () => (this.loading = false)
    );
  }

  handleDetailsChanged(): void {
    this.showActionButtons = true;
  }

  discardDetailsChanges(): void {
    this.showActionButtons = false;
    this.revertChanges();
  }

  revertChanges(): void {
    this.contact = cloneDeep(this.baseContact);
    this.contactForm.reset(this.contact);
    this.formChanged = false;
  }

  saveDetailsChanges(): void {
    if (this.contactForm.valid) {
      this.showActionButtons = false;
      this.setDefaultOwnerIfNull();
      this.saveChanges();
    } else {
      this.contactForm.markAsTouched();
    }
  }

  private saveChanges(): void {
    if (this.contactForm.valid) {
      const contact: Contact = {
        ...this.contact,
        ...this.contactForm.getRawValue(),
      };
      contact.additionalPhones = this.phoneService.mapAdditionalPhones(contact.additionalPhones);
      this.contactsService
        .updateContact(this.contactId, contact)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => {
            this.baseContact = cloneDeep(this.contact);
            this.notificationService.notify('contacts.form.notification.updated', NotificationType.SUCCESS);
            this.refreshContact();
            this.handleContactChangedSubscription();
          },
          () => this.notificationService.notify('contacts.form.notification.errorWhileUpdate', NotificationType.ERROR)
        );
    }
  }

  formControlFocusIn(formControlName: string): void {
    this.formControlFocusedKey = cloneDeep(formControlName);
    this.formControlFocusedValue = cloneDeep(
      this.contactForm.get(formControlName).value
    );
  }

  formControlFocusOut(newValue?: string): void {
    if (this.formControlFocusedKey) {
      const control = this.contactForm.get(this.formControlFocusedKey);
      const formValue = !newValue ? control.value : newValue;
      control.setValue(formValue);
      this.contact[this.formControlFocusedKey] = formValue;
      const formChanged = formValue !== this.formControlFocusedValue;
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
      if (formChanged) {
        this.handleDetailsChanged();
      }
    }
  }

  formControlFocusOutAdditionalPhones(newValue?: string): void {
    if (this.formControlFocusedKey) {
      const formValue = this.contactForm.get(this.formControlFocusedKey).value;
      const formChanged =
        JSON.stringify(formValue) !==
        JSON.stringify(this.formControlFocusedValue);
      if (formChanged) {
        this.contact[this.formControlFocusedKey] = formValue;
        this.handleDetailsChanged();
      }
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
    }
  }

  formControlEsc(): void {
    if (this.formControlFocusedKey) {
      this.contactForm
        .get(this.formControlFocusedKey)
        .patchValue(this.formControlFocusedValue);
    }
  }

  formControlFocusOutPosition(newValue?: number): void {
    if (this.formControlFocusedKey) {
      const formValue = !newValue
        ? this.contactForm.get(this.formControlFocusedKey).value
        : newValue;
      this.contactForm.get(this.formControlFocusedKey).setValue(formValue);
      this.contact[this.formControlFocusedKey] = this.getSelectedValue(formValue);
      const formChanged = formValue !== this.formControlFocusedValue;
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
      if (formChanged) {
        this.handleDetailsChanged();
      }
    }
  }

  private getSelectedValue(value: number): number {
    if (value !== this.undefinedCategoryId) {
      return value;
    }
    return null;
  }

  getPreviousNote(): void {
    this.noteListComponent.getPreviousNote(this.selectedNote);
  }

  openMailSlider(): void {
    if (this.contact && this.contact.email && this.contact.email.length > 0) {
      this.newMailSlider.open();
    }
  }
}
