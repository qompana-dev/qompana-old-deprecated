import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';
import {ContactCustomerRelation, ContactRelationFormModel, UpdateRelatedCustomer} from '../../contacts.model';
import {ContactsService} from '../../contacts.service';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {TranslateService} from '@ngx-translate/core';
import {FormSaveService} from '../../../shared/form/form-save.service';
import {DateUtil} from '../../../shared/util/date.util';
import {filter, map, takeUntil} from 'rxjs/operators';
import cloneDeep from 'lodash/cloneDeep';
import * as isEqual from 'lodash/isEqual';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import {DurationValidatorsUtil} from '../../../shared/util/duration-validators.util';
import {CustomerPopoverComponent} from '../../customer-popover/customer-popover.component';
import {applyPermission} from '../../../shared/permissions-utils';
import {AuthService} from '../../../login/auth/auth.service';

@Component({
  selector: 'app-related-customers',
  templateUrl: './related-customers.component.html',
  styleUrls: ['./related-customers.component.scss']
})
export class RelatedCustomersComponent implements OnInit, OnDestroy {

  @Input()
  public contactId: number;

  public relatedCustomersForm: FormGroup;
  private componentDestroyed: Subject<void> = new Subject();
  public relatedCustomers: ContactCustomerRelation[];
  public originalCustomersForm: any = null;
  private originalRelatedCustomers: ContactCustomerRelation[];
  private componentId = 'app-related-customers';

  public selectedPopoverRef: HTMLDivElement;
  public selectedPopoverId: number;
  public selectedPopover: number;

  focusedCustomer: {customerId?: number, id?: number} = {};

  @ViewChild('addCustomerSlider') addCustomerSlider: SliderComponent;
  @ViewChild(CustomerPopoverComponent) customerPopoverComponent: CustomerPopoverComponent;

  constructor(private fb: FormBuilder,
              private contactService: ContactsService,
              private notificationService: NotificationService,
              private translateService: TranslateService,
              private formSaveService: FormSaveService,
              private deleteDialogService: DeleteDialogService,
              private authService: AuthService) {
    this.createEmploymentForm();
  }

  dateUtil = DateUtil;
  monthList: { number: number, name: string }[] = DateUtil.getMonthsList();

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'contacts.details.relatedCustomers.dialog.delete.title',
    description: 'contacts.details.relatedCustomers.dialog.delete.description',
    confirmButton: 'contacts.details.relatedCustomers.dialog.delete.confirm',
    cancelButton: 'contacts.details.relatedCustomers.dialog.delete.cancel',
  };

  // @formatter:off
  get customersFormArray(): FormArray { return this.relatedCustomersForm.get('customers') as FormArray; }
  // @formatter:on

  ngOnInit(): void {
    this.subscribeForFormSaved();
    this.subscribeForFormCanceled();
    this.announceFormUnchanged();
    this.getRelatedCustomers();
  }


  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }


  private createEmploymentForm(): void {
    this.relatedCustomersForm = new FormGroup({
      customers: this.fb.array([]),
    });
    this.subscribeForValueChangeInForm();
  }

  private subscribeForValueChangeInForm(): void {
    this.relatedCustomersForm.valueChanges.pipe(
      filter((value) => !!this.originalCustomersForm),
      map(value => isEqual(this.originalCustomersForm, value)),
    ).subscribe(
      isTheSame => isTheSame ?
        this.formSaveService.announceFormTheSame(this.componentId) :
        this.formSaveService.announceFormChanged(this.componentId)
    );
  }

  private createSingleCustomerForm(relatedCustomer: Partial<ContactCustomerRelation> = {}): FormGroup {
    const formGroup = this.fb.group({
      relationId: [relatedCustomer.id],
      company: [{value: relatedCustomer.customerName, disabled: true}],
      position: [{value: relatedCustomer.position, disabled: this.isDirectRelation(relatedCustomer)}],
      relationType: [{value: this.getRelationTypeTranslation(relatedCustomer.relationType), disabled: true}],
      startDateMonth: [DateUtil.getMonthNumber(relatedCustomer.startDate)],
      startDateYear: [DateUtil.getYear(relatedCustomer.startDate)],
      endDateMonth: [{value: DateUtil.getMonthNumber(relatedCustomer.endDate), disabled: relatedCustomer.active}],
      endDateYear: [{value: DateUtil.getYear(relatedCustomer.endDate), disabled: relatedCustomer.active}],
      note: [relatedCustomer.note],
      isClosed: [{value: !!relatedCustomer.endDate, disabled: this.isDirectRelation(relatedCustomer)}],
    }, {
      validators: [
        DurationValidatorsUtil.getDateChronologyValidator(),
        DurationValidatorsUtil.getDependentFieldValidatorValidator('startDateMonth', 'startDateYear'),
        DurationValidatorsUtil.getDependentFieldValidatorValidator('endDateMonth', 'endDateYear'),
      ]
    });
    if (!this.authService.isPermissionPresent(`RelatedCustomerInContactDetail.edit`, 'w')) {
      formGroup.disable();
    }
    formGroup.get('endDateYear').valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      filter(value => !value)
    ).subscribe(
      () => formGroup.get('endDateMonth').patchValue(null)
    );
    return formGroup;
  }

  private isDirectRelation(relatedCustomer: Partial<ContactCustomerRelation>): boolean {
    return relatedCustomer.relationType === 'DIRECT';
  }

  private getRelationTypeTranslation(relationType: string): string {
    return relationType && this.translateService.instant(`contacts.details.relatedCustomers.relationTypes.${relationType}`);
  }

  public getRelatedCustomers(): void {
    this.originalCustomersForm = null;
    this.contactService.getCustomersRelatedWithContact(this.contactId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      customers => this.saveCustomers(customers),
      () => this.notificationService.notify('contacts.details.relatedCustomers.notification.getRelatedCustomersError', NotificationType.ERROR)
    );
  }


  private updateCustomersForm(customers: ContactCustomerRelation[]): void {
    if (!customers) {
      return;
    }
    this.clearFormArray(this.customersFormArray);
    for (const customer of customers) {
      this.customersFormArray.push(this.createSingleCustomerForm(customer));
    }
    this.originalCustomersForm = cloneDeep(this.relatedCustomersForm.value);
  }

  private clearFormArray(formArray: FormArray): void {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  cancel(): void {
    this.restoreStateOfRelationForm();
    this.formSaveService.announceFormTheSame(this.componentId);
  }

  private restoreStateOfRelationForm(): void {
    this.relatedCustomers = cloneDeep(this.originalRelatedCustomers);
    this.announceFormUnchanged();
    this.updateCustomersForm(this.relatedCustomers);
  }

  save(): void {
    if (!this.relatedCustomersForm.valid) {
      return;
    }
    const formValues: ContactRelationFormModel[] = this.customersFormArray.getRawValue();
    const update: UpdateRelatedCustomer[] = this.mapToUpdateDto(formValues);
    this.contactService.updateContactCustomerRelations(update, 'contact')
      .pipe(
        takeUntil(this.componentDestroyed),
      ).subscribe(
      () => {
        this.getRelatedCustomers();
        this.notifyUpdateSuccess();
      },
      () => this.notifyUpdateError()
    );
  }

  private saveCustomers(customers: ContactCustomerRelation[]): void {
    this.relatedCustomers = customers;
    this.originalRelatedCustomers = customers;
    this.announceFormUnchanged();
    this.updateCustomersForm(customers);
  }

  private subscribeForFormSaved(): void {
    this.formSaveService.formSavedSource$.pipe(
      takeUntil(this.componentDestroyed),
      filter(id => this.componentId === id)
    ).subscribe(
      () => this.save()
    );
  }

  private subscribeForFormCanceled(): void {
    this.formSaveService.formCanceledSource$.pipe(
      takeUntil(this.componentDestroyed),
      filter(id => this.componentId === id)
    ).subscribe(
      () => this.cancel()
    );
  }

  clearPopover(): void {
    this.selectedPopover = undefined;
    this.selectedPopoverId = undefined;
    this.selectedPopoverRef = undefined;
  }

  deleteRelation(linkId: number): void {
    this.contactService.deleteRelation(linkId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      () => {
        this.notifyDeleteRelationSuccess();
        this.getRelatedCustomers();
      },
      () => this.deleteRelationError()
    );
  }


  addRelatedCustomer(): void {
    this.addCustomerSlider.open();
  }

  isClosedRelation(i: number): boolean {
    const control = this.customersFormArray.at(i);
    return !!control.get('isClosed').value;
  }

  tryDelete(linkId: number): void {
    this.deleteDialogData.onConfirmClick = () => this.deleteRelation(linkId);
    this.openDeleteDialog();
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private notifyDeleteRelationSuccess(): void {
    this.notificationService.notify('contacts.details.relatedCustomers.notification.deleteRelationSuccess', NotificationType.SUCCESS);
  }

  private deleteRelationError(): void {
    this.notificationService.notify('contacts.details.relatedCustomers.notification.deleteRelationError', NotificationType.ERROR);
  }

  private mapToUpdateDto(formValues: ContactRelationFormModel[]): UpdateRelatedCustomer[] {
    return formValues.map(form => ({
      id: form.relationId,
      startDate: DateUtil.getStringDate(form.startDateMonth, form.startDateYear),
      endDate: DateUtil.getStringDate(form.endDateMonth, form.endDateYear),
      note: form.note,
      position: form.position
    }));
  }

  private notifyUpdateSuccess(): void {
    this.notificationService.notify('contacts.details.relatedCustomers.notification.updateRelationsSuccess', NotificationType.SUCCESS);
  }

  private notifyUpdateError(): void {
    this.notificationService.notify('contacts.details.relatedCustomers.notification.updateRelationsError', NotificationType.ERROR);
  }

  private announceFormUnchanged(): void {
    this.formSaveService.announceFormTheSame(this.componentId);
  }

  isOpenCloseDisabled(i: number): boolean {
    const control = this.customersFormArray.at(i);
    return !!control.get('isClosed').disabled;
  }

  openPopover(customerId?: number, id?: number, element?: HTMLDivElement): void {
    this.focusedCustomer = {customerId, id};
    setTimeout(() => {
      if (this.focusedCustomer.customerId === customerId && this.focusedCustomer.id === id) {
        this.selectedPopover = customerId;
        this.selectedPopoverId = id;
        this.selectedPopoverRef = element;
      }
    }, 1000);
  }
}
