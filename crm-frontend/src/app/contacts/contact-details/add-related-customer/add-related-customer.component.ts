import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {ContactsService} from '../../contacts.service';
import {CrmObject, CrmObjectType} from '../../../shared/model/search.model';
import {catchError, debounceTime, filter, map, skip, switchMap, takeUntil, tap} from 'rxjs/operators';
import {of, Subject} from 'rxjs';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {AssignToGroupService} from '../../../shared/assign-to-group/assign-to-group.service';
import {AddCustomerContactRelation, ContactCustomerRelation} from '../../contacts.model';
import {SliderService} from '../../../shared/slider/slider.service';
import {FormUtil} from '../../../shared/form.util';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {DateRangeSelectComponent} from '../../../shared/contact-customer-relation-shared/date-range-select/date-range-select.component';

@Component({
  selector: 'app-add-related-customer',
  templateUrl: './add-related-customer.component.html',
  styleUrls: ['./add-related-customer.component.scss']
})
export class AddRelatedCustomerComponent implements OnInit, OnDestroy {

  @Input()
  public set contactId(contactId: number) {
    this._contactId = contactId;
    if (contactId != null) {
      this.getContactName();
    }
  }

  @Input() slider: SliderComponent;

  @Input()
  public set relationList(relationList: ContactCustomerRelation[]) {
    this.customerIds = relationList ? relationList.filter(relation => !!relation).map(relation => relation.customerId) : [];
  }

  @Output()
  public newRelationAdded: EventEmitter<void> = new EventEmitter<void>();

  @ViewChild(DateRangeSelectComponent) dateSelect: DateRangeSelectComponent;


  public newRelationForm: FormGroup;
  private componentDestroyed: Subject<void> = new Subject();
  filteredCustomers: CrmObject[] = [];
  private selectedCustomerName: string;
  private _contactId: number;
  private customerIds: number[] = [];

  constructor(private fb: FormBuilder,
              private contactService: ContactsService,
              private notificationService: NotificationService,
              private assignToGroupService: AssignToGroupService,
              private sliderService: SliderService) {
    this.createEmploymentForm();
  }

  ngOnInit(): void {
    this.subscribeCustomerSearch();
    this.listenForSliderEvents();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }


  add(): void {
    if (!this.newRelationForm.valid || !this.dateSelect.isValid()) {
      FormUtil.setTouched(this.newRelationForm);
      return;
    }
    const relatedCustomer: AddCustomerContactRelation = this.getFormData();
    this.contactService.addRelatedCustomerToContact(relatedCustomer)
      .subscribe(
        ok => {
          this.newRelationAdded.emit();
          this.sliderService.closeSlider();
          this.notifySuccess();
        },
        err => this.notifyError()
      );
  }

  cancel(): void {
    this.sliderService.closeSlider();
  }

  customerAdded(newCustomer: { id: number; label: string }): void {
    this.selectedCustomerName = newCustomer.label;
    this.customerSearchControl.patchValue(newCustomer.label);
    this.customerIdControl.patchValue(newCustomer.id);
  }

  // @formatter:off
  get contactNameControl(): AbstractControl {return this.newRelationForm.get('contactName'); }
  get customerSearchControl(): AbstractControl {return this.newRelationForm.get('customerSearch'); }
  get customerIdControl(): AbstractControl {return this.newRelationForm.get('customerId'); }
  get positionControl(): AbstractControl {return this.newRelationForm.get('position'); }
  get noteControl(): AbstractControl { return this.newRelationForm.get('note'); }
  // @formatter:on


  private createEmploymentForm(): void {
    this.newRelationForm = this.fb.group({
      contactName: [{value: null, disabled: true}],
      customerSearch: [null, [Validators.required, this.getCustomCustomerValueValidator()]],
      customerId: [null],
      position: [null, Validators.maxLength(199)],
      note: [null]
    });
  }

  customerSelect(customer: CrmObject): void {
    if (customer && customer.type === CrmObjectType.customer) {
      this.selectedCustomerName = customer.label;
      this.customerIdControl.patchValue(customer.id);
      this.customerSearchControl.patchValue(customer.label);
    }
  }

  private getCustomCustomerValueValidator(): (control: AbstractControl) => ValidationErrors {
    return (control: AbstractControl): ValidationErrors => {
      return control.touched && control.value && control.value !== this.selectedCustomerName ? {customCustomerValue: true} : null;
    };
  }

  private subscribeCustomerSearch(): void {
    this.customerSearchControl.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      filter((input: string) => !!input && input.length >= 1),
      tap(() => {
        this.filteredCustomers = [];
      }),
      switchMap(((input: string) => this.assignToGroupService.searchForType(CrmObjectType.customer, input, [CrmObjectType.customer])
        .pipe(
          catchError(() => of([])),
          map(result => result.filter(object => !this.customerIds.includes(object.id)))
        ))
      )).subscribe(
      (result: CrmObject[]) => {
        this.filteredCustomers = result;
      }
    );
  }

  private updateNewRelationForm(name: string): void {
    if (this.newRelationForm) {
      this.contactNameControl.patchValue(`${name}`);
    }
  }

  private getFormData(): AddCustomerContactRelation {
    return {
      contactId: this._contactId,
      customerId: this.customerIdControl.value,
      position: this.positionControl.value,
      startDate: this.dateSelect.getStartDate(),
      endDate: this.dateSelect.getEndDate(),
      note: this.noteControl.value
    };
  }

  private notifySuccess(): void {
    this.notificationService.notify(
      'contacts.details.newRelatedCustomer.notification.linkContactWithCustomerSuccess', NotificationType.SUCCESS);
  }

  private notifyError(): void {
    this.notificationService.notify(
      'contacts.details.newRelatedCustomer.notification.linkContactWithCustomerSuccess', NotificationType.ERROR);
  }

  private getContactName(): void {
    if (this._contactId == null) {
      return;
    }
    this.contactService.getContactName(this._contactId)
      .subscribe(
        name => {
          this.updateNewRelationForm(name);
        },
        err => this.notificationService.notify(
          'contacts.details.newRelatedCustomer.notification.linkContactWithCustomerSuccess', NotificationType.ERROR)
      );
  }

  private listenForSliderEvents(): void {
    this.slider.closeEvent.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.newRelationForm.reset();
        this.filteredCustomers = [];
      }
    );

    this.slider.openEvent.pipe(
      skip(1),
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.getContactName()
    );
  }
}
