import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ContactSalesOpportunity} from '../../contacts.model';
import {ContactsService} from '../../contacts.service';
import {map, startWith, takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {OpportunitySave} from '../../../opportunity/opportunity.model';
import {FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import {OpportunityService} from '../../../opportunity/opportunity.service';
import {LoginService} from '../../../login/login.service';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import {Router} from '@angular/router';
import {AuthService} from '../../../login/auth/auth.service';

@Component({
  selector: 'app-contact-sales-opportunity-card',
  templateUrl: './contact-sales-opportunity-card.component.html',
  styleUrls: ['./contact-sales-opportunity-card.component.scss']
})
export class ContactSalesOpportunityCardComponent implements OnInit, OnDestroy {

  @ViewChild('addSlider') addSlider: SliderComponent;
  @ViewChild('editSlider') editSlider: SliderComponent;

  // tslint:disable-next-line:variable-name
  _contactId: number;
  contactOpportunities: ContactSalesOpportunity[];
  columns = ['name', 'status', 'amount', 'finishDate', 'action'];
  filteredColumns$: Observable<string[]>;
  selectedOpportunity: OpportunitySave;
  opportunityWithContact: Partial<OpportunitySave>;
  private componentDestroyed: Subject<void> = new Subject();

  @Input() set contactId(contactId: number) {
    if (contactId) {
      this._contactId = contactId;
      this.subscribeForContactOpportunities(contactId);
    }
  }

  private readonly opportunityErrorTypes: ErrorTypes = {
    base: 'opportunity.list.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'opportunityNotFound'
      }, {
        code: FORBIDDEN,
        text: 'noPermission'
      }
    ]
  };

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'opportunity.form.dialog.delete.title',
    description: 'opportunity.form.dialog.delete.description',
    noteFirst: 'opportunity.form.dialog.delete.noteFirstPart',
    noteSecond: 'opportunity.form.dialog.delete.noteSecondPart',
    confirmButton: 'opportunity.form.dialog.delete.confirm',
    cancelButton: 'opportunity.form.dialog.delete.cancel',
  };

  constructor(private notificationService: NotificationService,
              private contactsService: ContactsService,
              private opportunityService: OpportunityService,
              private deleteDialogService: DeleteDialogService,
              private $router: Router,
              private authService: AuthService,
              private loginService: LoginService) { }

  ngOnInit() {
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
    this.contactsService.contactOpportunitiesChanged.subscribe(
      () => this.subscribeForContactOpportunities(this._contactId)
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  edit(id: number): void {
    this.opportunityService.getOpportunityForEditForSlider(id).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      opportunity => {
        this.selectedOpportunity = opportunity;
        this.editSlider.open();
      },
      err => this.notificationService.notifyError(this.opportunityErrorTypes, err.status)
    );
  }

  tryDeleteOpportunity(opportunityId: number): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteOpportunity(opportunityId);
    this.openDeleteDialog();
  }

  addOpportunity(): void {
    this.contactsService.getContact(this._contactId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (contact) => {
        this.opportunityWithContact = {contactId: contact.id, customerId: contact.customer.id, canEditAmount: true,
          ownerOneId: this.loginService.getLoggedAccountId(), ownerOnePercentage: 100};
        this.addSlider.open();
      }
    );
  }

  getOpportunityProgress(opportunity: ContactSalesOpportunity): number {
    return opportunity.maxStatus ? (opportunity.status / opportunity.maxStatus) * 100 : 0;
  }

  handleOpportunityAdded(): void {
    this.contactsService.contactOpportunitiesChanged.emit();
    this.addSlider.close();
    this.editSlider.close();
  }

  goToOpportunityDetails(id: number) {
    this.$router.navigate(['/opportunity', id]);
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(this.deleteDialogData);
  }

  private deleteOpportunity(opportunityId: number): void {
    this.opportunityService.removeOpportunity(opportunityId).pipe(
      takeUntil(this.componentDestroyed),
    ).subscribe(
      () => {
        this.contactsService.contactOpportunitiesChanged.emit();
        this.notificationService.notify('opportunity.list.notification.deleteSuccess', NotificationType.SUCCESS);
      },
      err => this.notificationService.notifyError(this.opportunityErrorTypes, err.status)
    );
  }

  private subscribeForContactOpportunities(contactId: number): void {
    this.contactsService.getContactOpportunities(contactId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (contactOpportunities: ContactSalesOpportunity[]) => this.contactOpportunities = contactOpportunities,
      () => this.notificationService.notify('contacts.tabs.salesOpportunities.notifications.getOpportunitiesError', NotificationType.ERROR)
    );
  }

  private getFilteredColumns(): string[] {
    return this.columns
      .filter((item) => !this.authService.isPermissionPresent('ContactSalesOpportunityCardComponent.' + item, 'i') || item === 'action');
  }
}
