import {Component, Input} from '@angular/core';
import {Subject} from 'rxjs';
import {Customer} from '../../customer/customer.model';
import {Contact} from '../contacts.model';

@Component({
  selector: 'app-contact-tabs',
  templateUrl: './contact-tabs.component.html',
  styleUrls: ['./contact-tabs.component.scss']
})
export class ContactTabsComponent {
  @Input() contactId: number = null;
  @Input() contact: Contact = null;
  @Input() contactChangedSubscription: Subject<void>;
  @Input() objectType = null;

  selectedTabIndex = 0;

}
