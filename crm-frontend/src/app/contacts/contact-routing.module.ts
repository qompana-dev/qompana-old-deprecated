import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../login/auth/auth-guard.service';
import {ContactsListComponent} from './contacts-list/contacts-list.component';
import {ContactDetailsComponent} from './contact-details/contact-details.component';

const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full' },
  {path: 'list', canActivate: [AuthGuard], component: ContactsListComponent},
  {path: ':id', canActivate: [AuthGuard], component: ContactDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactRoutingModule { }
