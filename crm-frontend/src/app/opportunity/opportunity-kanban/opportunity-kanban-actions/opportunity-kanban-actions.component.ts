import {FinishOpportunityResult, KanbanOpportunity,} from "./../../opportunity.model";
import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {
  ACTIVITIES,
  ACTIVITIES_LIST
} from "../../../dashboard/dashboard/panel/render-widget-panel/widgets/calendar-activities/calendar-activities.config";
import {TaskActivityType, TaskAssociation} from "../../../calendar/task.model";
import {CrmObjectType} from "../../../shared/model/search.model";
import {PersistNoteDialogService} from "../../../widgets/notes/persist-note-dialog/persist-note-dialog.service";
import {PersistNoteDialogData} from "../../../widgets/notes/persist-note-dialog/persist-note-dialog.component";
import {ErrorTypes, NotificationService, NotificationType} from "../../../shared/notification.service";
import {takeUntil} from "rxjs/operators";
import {NotesService} from "../../../widgets/notes/notes.service";
import {Subject} from "rxjs";
import {CrmFileUploaderService} from "../../../files/crm-file-uploader.service";
import {FileAuthTypeEnum} from "../../../files/file.model";
import {FileItem} from "ng2-file-upload";
import {OpportunityKanbanService} from "../opportunity-kanban.service";

@Component({
  selector: "app-opportunity-kanban-actions",
  templateUrl: "./opportunity-kanban-actions.component.html",
  styleUrls: ["./opportunity-kanban-actions.component.scss"],
})
export class OpportunityKanbanActionsComponent implements OnInit, OnDestroy {

  private componentDestroyed: Subject<void> = new Subject();
  private crmObjectType: CrmObjectType = CrmObjectType.opportunity;

  @ViewChild('fileInput') fileInput: ElementRef;

  @Input() opportunity: KanbanOpportunity;
  initAssociation: TaskAssociation;
  @Input() acceptanceUser: string;

  @Input() removeOpportunity: (o: KanbanOpportunity) => void;
  @Input() copyOpportunity: (o: KanbanOpportunity) => void;
  @Input() closeOpportunity: (o: KanbanOpportunity, t: FinishOpportunityResult) => void;

  public newEvent = {startDate: new Date(), allDayEvent: false};
  public selectedActivityType: TaskActivityType;
  finishTypes = FinishOpportunityResult;

  public activities = ACTIVITIES_LIST;

  readonly tooltipPosition = {
    originX: "end",
    overlayX: "end",
    offsetY: -8,
  };

  private readonly persistNoteDialogData: Partial<PersistNoteDialogData> = {
    confirmButton: "widgets.notes.save",
    cancelButton: "widgets.notes.cancel",
  };

  readonly saveNoteErrors: ErrorTypes = {
    base: "widgets.notes.notification.",
    defaultText: "unknownError",
    errors: [
      {
        code: 404,
        text: "saveCustomerNotFound",
      },
      {
        code: 403,
        text: "noPermission",
      },
    ],
  };

  constructor(public uploader: CrmFileUploaderService,
              private persistNoteDialogService: PersistNoteDialogService,
              private opportunityKanbanService: OpportunityKanbanService,
              private notesService: NotesService,
              private notificationService: NotificationService
  ) {
  }


  ngOnInit(): void {
    this.uploader.setFileAuthTypeEnum(FileAuthTypeEnum.opportunityMini);
    this.uploader.onCompleteAll = () => this.notificationService.notify('widgets.notes.notification.saveSuccess', NotificationType.SUCCESS);
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  addFiles() {
    console.log('new Add Files: ' + this.opportunity.id);
    this.uploader.onAfterAddingAll = files => this.uploadAll(files);
    this.fileInput.nativeElement.click();
  }

  uploadAll(files: FileItem[]): void {
    console.log('W action:files: ' + this.opportunity.id);
    this.uploader.uploadItems(files, {
      description: '',
      tags: [],
      assignedObjects: [{type: this.crmObjectType, id: this.opportunity.id}]
    });
  }


  openAddSlider(type: ACTIVITIES): void {
    const activityTypeArr = Object.entries(ACTIVITIES).find(([key, value]) => {
      return value === type;
    });
    const activityType = activityTypeArr && activityTypeArr[0];
    this.selectedActivityType = TaskActivityType[activityType];
    this.initAssociation = {
      objectType: CrmObjectType.opportunity,
      objectId: this.opportunity.id,
      disabled: true,
    };
    this.opportunityKanbanService.openActivitySlider(this.initAssociation, this.selectedActivityType);
  }

  openAddNotesDialog(): void {
    this.persistNoteDialogData.title = "widgets.notes.dialog.add";
    this.persistNoteDialogData.formattedContent = null;
    this.persistNoteDialogData.onConfirmClick = (plainText, htmlText) => {
      this.saveNotes(this.opportunity.id, plainText, htmlText);
    };
    this.persistNoteDialogService.open(this.persistNoteDialogData);
  }

  saveNotes(opportunityId: number, plainText: string, htmlText: string): void {
    if (!(plainText && plainText.trim())) {
      return;
    }
    if (plainText.length > 3000) {
      this.notificationService.notify('widgets.notes.notification.maxLengthExceeded', NotificationType.ERROR);
      return;
    }
    this.notesService.saveNote({
      objectType: CrmObjectType.opportunity,
      objectId: opportunityId
    }, plainText, htmlText)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (note) => this.notificationService.notify('widgets.notes.notification.saveSuccess', NotificationType.SUCCESS),
        (err) => this.notificationService.notifyError(this.saveNoteErrors, NotificationType.ERROR)
      );
  }


  onCopy(e: MouseEvent) {
    e.stopPropagation();
    if (this.copyOpportunity) {
      this.copyOpportunity(this.opportunity);
    }
  }

  onClose(e: MouseEvent, type: FinishOpportunityResult) {
    e.stopPropagation();
    this.closeOpportunity(this.opportunity, type);
  }

  onRemove(e: MouseEvent) {
    e.stopPropagation();
    if (this.removeOpportunity) {
      this.removeOpportunity(this.opportunity);
    }
  }
}
