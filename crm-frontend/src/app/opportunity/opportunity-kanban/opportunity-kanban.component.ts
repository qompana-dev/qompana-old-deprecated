import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BpmnUtil} from '../../bpmn/bpmn.util';
import {Subject} from 'rxjs';
import {finalize, takeUntil} from 'rxjs/operators';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {OpportunityService} from '../opportunity.service';
import {FilterStrategy} from '../../shared/model';
import {FORBIDDEN, NOT_FOUND} from "http-status-codes";
import {SliderComponent} from "../../shared/slider/slider.component";
import {TaskActivityType, TaskAssociation} from "../../calendar/task.model";
import {
  KanbanOpportunity, KanbanOpportunityProcessOverview,
  KanbanOpportunityTaskDetails,
  OpportunitySave
} from "../opportunity.model";
import {CdkDragDrop, CdkDragRelease, CdkDragStart, transferArrayItem} from "@angular/cdk/drag-drop";
import {OpportunityKanbanService, SliderType} from "./opportunity-kanban.service";
import {OpportunityListService} from "../opportunity-list/opportunity-list.service";


@Component({
  selector: 'app-opportunity-kanban',
  templateUrl: './opportunity-kanban.component.html',
  styleUrls: ['./opportunity-kanban.component.scss']
})
export class OpportunityKanbanComponent implements OnInit, OnDestroy {

  @ViewChild("addTask") addTaskSlider: SliderComponent;
  @ViewChild("editSlider") editOpportunitySlider: SliderComponent;
  public newEvent = {startDate: new Date(), allDayEvent: false};
  selectedActivityType: TaskActivityType;
  initAssociation: TaskAssociation;
  editOpportunity: OpportunitySave;

  @Input() selectedFilter: FilterStrategy = FilterStrategy.OPENED;

  processOverview: KanbanOpportunityProcessOverview[];
  processDetails: KanbanOpportunityTaskDetails[];
  extendedProcessId: string = null;
  bpmnUtil = BpmnUtil;
  private componentDestroyed: Subject<void> = new Subject();
  loadingData = false;
  search = '';
  freezeKanban = false;
  drag = false;
  dragArray: Array<string> = [];

  private readonly opportunityErrorTypes: ErrorTypes = {
    base: "opportunity.list.notification.",
    defaultText: "unknownError",
    errors: [
      {
        code: NOT_FOUND,
        text: "opportunityNotFound",
      },
      {
        code: FORBIDDEN,
        text: "noPermission",
      },
    ],
  };
  private readonly changeTaskErrorTypes: ErrorTypes = {
    base: 'leads.kanban.notification.',
    defaultText: 'taskCompletedError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'noTaskFound'
      }
    ]
  };

  readonly getOpportunityErrorTypes: ErrorTypes = {
    base: 'opportunity.details.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'notFound',
      },
      {
        code: FORBIDDEN,
        text: 'forbidden',
      },
    ],
  };

  constructor(
    private opportunityService: OpportunityService,
    private notificationService: NotificationService,
    private opportunityListService: OpportunityListService,
    private opportunityKanbanService: OpportunityKanbanService
  ) {
    this.opportunityListService.addSearchListen().subscribe((search: string) => {
      this.search = this.opportunityListService.getSearch();
      this.selectedFilter = this.opportunityListService.getFilterStrategy();
      this.getKanbanOverview();
    });
    this.opportunityKanbanService.addSliderListen().subscribe((search: string) => {
      if (this.opportunityKanbanService.getSliderType() === SliderType.ACTIVITY) {
        this.initAssociation = this.opportunityKanbanService.getInitAssociation();
        this.selectedActivityType = this.opportunityKanbanService.getSelectedActivityType();
        this.addTaskSlider.open();
      } else if(this.opportunityKanbanService.getSliderType() === SliderType.OPPORTUNITY_EDIT) {
        this.getOpportunitySaveAndOpenSlider(this.opportunityKanbanService.getOpportunity());
      }

    });
    this.opportunityKanbanService.addRefreshListen().subscribe((processDefinitionId: string) => {
      this.getProcess(this.extendedProcessId);
    });
  }

  getOpportunitySaveAndOpenSlider(opportunity: KanbanOpportunity): void {
    this.opportunityService
      .getOpportunityForEditForSlider(opportunity.id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (opportunity) => {
          this.editOpportunity = opportunity;
          this.editOpportunitySlider.open();
        },
        err => this.notificationService.notifyError(this.getOpportunityErrorTypes, err.status)
      );
  }

  ngOnInit(): void {
    this.search = this.opportunityListService.getSearch();
    this.selectedFilter = this.opportunityListService.getFilterStrategy();
    this.getKanbanOverview();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
    this.opportunityKanbanService.complete();
  }

  private getKanbanOverview(): void {
    this.loadingData = true;
    this.search = this.opportunityListService.getSearch();
    this.selectedFilter = this.opportunityListService.getFilterStrategy();
    this.opportunityService.getProcessKanbanOverview(this.selectedFilter, this.search)
      .pipe(
        takeUntil(this.componentDestroyed),
        finalize(() => (this.loadingData = false))
      ).subscribe(
      overview => {
        this.processOverview = overview;
        const existInActualFilers = this.processOverview.map(process => process.id).includes(this.extendedProcessId);
        if (this.extendedProcessId && existInActualFilers) {
          this.getProcess(this.extendedProcessId);
        }
      },
      err => this.notificationService.notify('leads.kanban.notification.getProccessesOverviewError', NotificationType.ERROR)
    );
  }

  setProcessDefinitionId(id: string): void {
    if (this.extendedProcessId === id) {
      this.extendedProcessId = null;
      return;
    }
    this.getProcess(id);
  }

  getProcess(processDefinitionId: string): void {
    if (!processDefinitionId) {
      return;
    }
    this.loadingData = true;
    this.processDetails = null;
    this.opportunityService.getProcessKanbanDetails(processDefinitionId, this.selectedFilter, this.search)
      .pipe(
        takeUntil(this.componentDestroyed),
        finalize(() => this.loadingData = false)
      ).subscribe(
      details => {
        this.processDetails = details;
        this.extendedProcessId = processDefinitionId;
      },
      err => this.notificationService.notify('leads.kanban.notification.processNotFound', NotificationType.ERROR)
    );
  }

  drop(event: CdkDragDrop<KanbanOpportunityTaskDetails>) {
    if (event.previousContainer !== event.container) {
      this.freezeKanban = true;
      const movedOpportunity: KanbanOpportunity = event.item.data[0];
      const newTaskId: string = event.container.data.id;
      if (movedOpportunity.canBeMoveForward.possibleStageId.includes(newTaskId) && movedOpportunity.canBeMoveForward.canMove) {
        this.moveLeadBetweenColumns(event);
        this.completeTask(movedOpportunity);
      } else if (movedOpportunity.canBeMoveBackward.possibleStageId.includes(newTaskId) && movedOpportunity.canBeMoveBackward.canMove) {
        this.moveLeadBetweenColumns(event);
        this.moveTaskBack(movedOpportunity, 1);
      } else {
        this.freezeKanban = false;
      }
    }
  }

  cdkDragStarted($event: CdkDragStart<any>): void {
    const movedOpportunity: KanbanOpportunity = $event.source.data[0];
    this.dragArray = this.dragArray.concat(movedOpportunity.canBeMoveForward.possibleStageId, movedOpportunity.canBeMoveBackward.possibleStageId);
    this.drag = true;
  }

  cdkDragReleased($event: CdkDragRelease<any>): void {
    this.dragArray = [];
    this.drag = false;
  }

  isShowPlaceholder(taskName: string): boolean {
    return this.dragArray.includes(taskName);
  }

  private completeTask(opportunity: KanbanOpportunity): void {
    this.opportunityService.completeTask(opportunity.processInstanceId)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      response => this.handleTaskCompletion(response.status),
      err => this.notificationService.notifyError(this.changeTaskErrorTypes, err.status),
      () => this.freezeKanban = false
    );
  }

  private moveTaskBack(opportunity: KanbanOpportunity, howManyBack: number): void {
    this.opportunityService.goBack(opportunity.processInstanceId, howManyBack)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      response => {
        this.getProcess(this.extendedProcessId);
        this.notificationService.notify('leads.kanban.notification.successfulGoBack', NotificationType.SUCCESS);
      },
      err => this.notificationService.notifyError(this.changeTaskErrorTypes, err.status),
      () => this.freezeKanban = false
    );
  }

  private handleTaskCompletion(status: number): void {
    if (status === 204) {
      this.notificationService.notify('leads.kanban.notification.taskCompleted', NotificationType.SUCCESS);
    } else if (status === 206) {
      this.notificationService.notify('leads.details.notifications.taskWaitingForAcceptance', NotificationType.SUCCESS);
    }
    this.getProcess(this.extendedProcessId);
  }

  private moveLeadBetweenColumns(event: CdkDragDrop<KanbanOpportunityTaskDetails>): void {
    transferArrayItem(event.previousContainer.data.opportunities, event.container.data.opportunities, event.previousIndex, event.currentIndex);
  }

}




