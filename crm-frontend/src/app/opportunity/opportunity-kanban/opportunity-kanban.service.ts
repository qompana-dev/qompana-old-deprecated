import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Subject} from "rxjs/Subject";
import {TaskActivityType, TaskAssociation} from "../../calendar/task.model";
import {KanbanOpportunity} from "../opportunity.model";

@Injectable({
  providedIn: 'root'
})
export class OpportunityKanbanService {


  private selectedActivityType: TaskActivityType;
  private initAssociation: TaskAssociation;

  private opportunity: KanbanOpportunity;

  private sliderType: SliderType;

  private _sliderListeners = new Subject<any>();
  private _refreshProcess = new Subject<any>();


  addSliderListen(): Observable<any> {
    return this._sliderListeners.asObservable();
  }

  addRefreshListen(): Observable<any> {
    return this._refreshProcess.asObservable();
  }


  openActivitySlider(initAssociation: TaskAssociation, selectedActivityType: TaskActivityType): void {
    this.sliderType = SliderType.ACTIVITY;
    this.initAssociation = initAssociation;
    this.selectedActivityType = selectedActivityType;
    this._sliderListeners.next();
  }

  openOpportunitySlider(opportunity: KanbanOpportunity): void {
    this.sliderType = SliderType.OPPORTUNITY_EDIT;
    this.opportunity = opportunity;
    this._sliderListeners.next();
  }

  refreshExtendedProcess() {
    this._refreshProcess.next();
  }

  complete() {
    this._sliderListeners.complete();
    this._refreshProcess.complete();
  }

  public getInitAssociation(): TaskAssociation {
    return this.initAssociation;
  }

  public getSelectedActivityType(): TaskActivityType {
    return this.selectedActivityType;
  }

  public getOpportunity(): KanbanOpportunity {
    return this.opportunity;
  }

  public getSliderType(): SliderType {
    return this.sliderType;
  }

}

export enum SliderType {
  ACTIVITY = 'ACTIVITY',
  OPPORTUNITY_EDIT = 'CUSTOMER_NAME'
}
