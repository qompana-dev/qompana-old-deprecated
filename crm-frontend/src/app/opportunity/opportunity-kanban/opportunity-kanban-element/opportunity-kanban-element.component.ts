import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {OpportunityDialogsConfig} from "../../opportunity-list/opportunity-list.dialogs";
import {TranslateService} from "@ngx-translate/core";
import {ConfirmDialogService} from "../../../shared/dialog/confirm-dialog/confirm-dialog.service";
import {FinishOpportunityResult, KanbanOpportunity, OpportunityFinishDto} from "../../opportunity.model";
import {takeUntil} from "rxjs/operators";
import {ErrorTypes, NotificationService, NotificationType} from "../../../shared/notification.service";
import {OpportunityService} from "../../opportunity.service";
import {FORBIDDEN, NOT_FOUND} from "http-status-codes";
import {OpportunityListService} from "../../opportunity-list/opportunity-list.service";
import {OpportunityKanbanService} from "../opportunity-kanban.service";


@Component({
  selector: 'app-opportunity-kanban-element',
  templateUrl: './opportunity-kanban-element.component.html',
  styleUrls: ['./opportunity-kanban-element.component.scss']
})
export class OpportunityKanbanElementComponent implements OnInit, OnDestroy {

  @Input() opportunity: KanbanOpportunity;
  @Input() processColor: string;

  private componentDestroyed: Subject<void> = new Subject();
  extended = true;

  private readonly dialogsConfig = new OpportunityDialogsConfig(this.translateService);
  private readonly opportunityErrorTypes: ErrorTypes = {
    base: "opportunity.list.notification.",
    defaultText: "unknownError",
    errors: [
      {
        code: NOT_FOUND,
        text: "opportunityNotFound",
      },
      {
        code: FORBIDDEN,
        text: "noPermission",
      },
    ],
  };

  constructor(private translateService: TranslateService,
              private opportunityService: OpportunityService,
              private opportunityKanbanService: OpportunityKanbanService,
              private notificationService: NotificationService,
              private confirmDialogService: ConfirmDialogService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  openEditSlider() {
    this.opportunityKanbanService.openOpportunitySlider(this.opportunity);
  }

  copyOpportunity(opportunityId: number): void {
    this.opportunityService
      .copyOpportunity(opportunityId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.opportunityKanbanService.refreshExtendedProcess();
          this.notificationService.notify("opportunity.list.notification.copySuccess", NotificationType.SUCCESS);
        },
        (err) =>
          this.notificationService.notifyError(this.opportunityErrorTypes, err.status)
      );
  }

  private finishOpportunityByType(opportunityId: number, finishBody: OpportunityFinishDto): void {
    if (finishBody.finishResult === FinishOpportunityResult.SUCCESS) {
      this.closeOpportunity(opportunityId, finishBody);
    } else {
      const dialogData = this.dialogsConfig.rejectionReason((rejectionReason) =>
        this.closeOpportunity(opportunityId, {...finishBody, rejectionReason,})
      );
      this.openDeleteDialog(dialogData);
    }
  }

  closeOpportunity(opportunityId: number, finishBody: OpportunityFinishDto): void {
    this.opportunityService
      .finishOpportunity(opportunityId, finishBody)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.opportunityKanbanService.refreshExtendedProcess();
          this.notificationService.notify("opportunity.list.notification.finishSuccess", NotificationType.SUCCESS);
        },
        (err) => this.notificationService.notifyError(this.opportunityErrorTypes, err.status)
      );
  }

  deleteOpportunity(opportunityId: number): void {
    this.opportunityService
      .removeOpportunity(opportunityId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.opportunityKanbanService.refreshExtendedProcess();
          this.notificationService.notify("opportunity.list.notification.deleteSuccess", NotificationType.SUCCESS);
        },
        (err) =>
          this.notificationService.notifyError(this.opportunityErrorTypes, err.status)
      );
  }

  public tryCopyOpportunity(opportunity: KanbanOpportunity): void {
    const dialogData = this.dialogsConfig.copyOpportunity(
      this.opportunity,
      this.copyOpportunity.bind(this)
    );
    this.openDeleteDialog(dialogData);
  }

  public tryCloseOpportunity(opportunity: KanbanOpportunity, finishResult: FinishOpportunityResult): void {
    const dialogData = this.dialogsConfig.closeOpportunity(
      this.opportunity,
      finishResult,
      this.finishOpportunityByType.bind(this)
    );
    this.openDeleteDialog(dialogData);
  }

  public tryDeleteOpportunity(opportunity: KanbanOpportunity): void {
    const dialogData = this.dialogsConfig.deleteOpportunity(
      this.opportunity,
      this.deleteOpportunity.bind(this)
    );
    this.openDeleteDialog(dialogData);
  }

  private openDeleteDialog(dialogData = {}): void {
    this.confirmDialogService.open(dialogData);
  }

}




