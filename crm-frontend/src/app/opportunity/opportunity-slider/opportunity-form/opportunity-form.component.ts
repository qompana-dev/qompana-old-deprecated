import {ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {OpportunityPriority, OpportunitySave} from '../../opportunity.model';
import {merge, Observable, Subject} from 'rxjs';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {FormUtil} from '../../../shared/form.util';
import {debounceTime, distinctUntilChanged, filter, finalize, map, switchMap, takeUntil, tap} from 'rxjs/operators';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {PersonService} from '../../../person/person.service';
import {AccountEmail} from '../../../person/person.model';
import {SliderService} from '../../../shared/slider/slider.service';
import {LeadService} from '../../../lead/lead.service';
import {ProcessDefinition} from '../../../lead/lead.model';
import {CrmObject, CrmObjectType} from '../../../shared/model/search.model';
import {AssignToGroupService} from '../../../shared/assign-to-group/assign-to-group.service';
import {ContactsService} from '../../../contacts/contacts.service';
import {Contact} from '../../../contacts/contacts.model';
import cloneDeep from 'lodash/cloneDeep';
import {applyPermission} from '../../../shared/permissions-utils';
import {AuthService} from '../../../login/auth/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {GlobalConfigService} from '../../../global-config/global-config.service';
import {CustomerService} from '../../../customer/customer.service';
import {FormValidators} from '../../../shared/form/form-validators';
import {OpportunityFormUtil} from '../../util/opportunity-form.util';
import {
  Dictionary,
  DictionaryId,
  DictionaryWord, SaveDictionaryWord,
} from '../../../dictionary/dictionary.model';
import {MatSelect} from '@angular/material';
import * as isEqual from 'lodash/isEqual';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {DictionaryAddWordComponent} from '../../../dictionary/dictionary-add-word/dictionary-add-word.component';
import { DictionaryStoreService } from 'src/app/dictionary/dictionary-store.service';
import {DateUtil} from '../../../shared/util/date.util';
import {Customer} from "../../../customer/customer.model";

@Component({
  selector: 'app-opportunity-form',
  templateUrl: './opportunity-form.component.html',
  styleUrls: ['./opportunity-form.component.scss']
})
export class OpportunityFormComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();
  opportunityForm: FormGroup;
  accountEmails: AccountEmail[] = [];
  processDefinitions: ProcessDefinition[];
  opportunitySave: OpportunitySave;

  currencies = [];

  contactSearchLoading = false;
  customerSearchLoading = false;
  filteredCustomers: CrmObject[] = [];
  filteredContacts: CrmObject[] = [];

  contactsByCustomerId: CrmObject[] = [];

  availableOpportunityPriorities = Object.keys(OpportunityPriority);
  objectType = CrmObjectType;
  ownerVisibleCount = 1;
  filteredAccounts: Map<number, AccountEmail[]> = new Map<number, AccountEmail[]>();
  percentageSumError = false;

  selectedCustomerForNewContact: CrmObject;

  selectedCustomer: string;
  selectedContact: string;

  readonly undefinedCategoryId = -1000;
  dictionaryId = DictionaryId;
  parentWordIdForCreation: number = null;
  allSourceOfAcquisition: DictionaryWord[];
  allSubSourceOfAcquisition: DictionaryWord[];
  allSubjectOfInterest: DictionaryWord[];

  @ViewChild('sourceOfAcquisitionSelect') sourceOfAcquisitionSelect: MatSelect;
  @ViewChild('subSourceOfAcquisitionSelect') subSourceOfAcquisitionSelect: MatSelect;

  @ViewChild('addSourceOfAcquisitionSlider') addSourceOfAcquisitionSlider: SliderComponent;
  @ViewChild('addSourceOfAcquisitionWord') addSourceOfAcquisitionWord: DictionaryAddWordComponent;

  @ViewChild('addSubSourceOfAcquisitionSlider') addSubSourceOfAcquisitionSlider: SliderComponent;
  @ViewChild('addSubSourceOfAcquisitionWord') addSubSourceOfAcquisitionWord: DictionaryAddWordComponent;

  @ViewChild('addSubjectOfInterestSlider') addSubjectOfInterestSlider: SliderComponent;
  @ViewChild('addSubjectOfInterestWord') addSubjectOfInterestWord: DictionaryAddWordComponent;

  @Input() update = false;

  @Input() set opportunity(opportunity: OpportunitySave) {
    if (opportunity) {
      this.opportunitySave = opportunity;
      this.opportunityForm.reset(opportunity);
      this.amount.patchValue(
        (opportunity.amount !== undefined && opportunity.amount !== null) ? this.getNumberInFormat(opportunity.amount) : undefined);
      (opportunity.canEditAmount) ? this.amount.enable() : this.amount.disable();
      (opportunity.canEditAmount) ? this.currency.enable() : this.currency.disable();
      this.setCustomerAndContact();
      this.setOpportunityOwners();

      const privateWordsIds: number[] = [];
      privateWordsIds.push(this.opportunitySave.sourceOfAcquisition);
      privateWordsIds.push(this.opportunitySave.subSourceOfAcquisition);
      this.getSourcesOfAcquisitionForMe(privateWordsIds);

      const subjectOfInterestPrivateWordsIds: number[] = [];
      subjectOfInterestPrivateWordsIds.push(this.opportunitySave.subjectOfInterest);
      this.getSubjectsOfInterestForMe(subjectOfInterestPrivateWordsIds);
    }
  }

  @Input() set initOpportunityDefaultValues(initOpportunityDefaultValues: boolean) {
    if (initOpportunityDefaultValues === true) {
      this.createOpportunityFrom();
    }
  }

  constructor(private personService: PersonService,
              private sliderService: SliderService,
              private customerService: CustomerService,
              private fb: FormBuilder,
              private authService: AuthService,
              private translateService: TranslateService,
              private cdRef: ChangeDetectorRef,
              private contactsService: ContactsService,
              private assignToGroupService: AssignToGroupService,
              private leadService: LeadService,
              private notificationService: NotificationService,
              private configService: GlobalConfigService,
              private dictionaryStoreService: DictionaryStoreService,
) {
    this.createOpportunityFrom();
  }

  // @formatter:off
  get name(): AbstractControl { return this.opportunityForm.get('name'); }
  get customerId(): AbstractControl { return this.opportunityForm.get('customerId'); }
  get customerSearch(): AbstractControl { return this.opportunityForm.get('customerSearch'); }
  get contactId(): AbstractControl { return this.opportunityForm.get('contactId'); }
  get contactSearch(): AbstractControl { return this.opportunityForm.get('contactSearch'); }
  get priority(): AbstractControl { return this.opportunityForm.get('priority'); }
  get amount(): AbstractControl { return this.opportunityForm.get('amount'); }
  get currency(): AbstractControl { return this.opportunityForm.get('currency'); }
  get probability(): AbstractControl { return this.opportunityForm.get('probability'); }
  get createDate(): AbstractControl { return this.opportunityForm.get('createDate'); }
  get finishDate(): AbstractControl { return this.opportunityForm.get('finishDate'); }
  get processDefinitionId(): AbstractControl { return this.opportunityForm.get('processDefinitionId'); }
  get description(): AbstractControl { return this.opportunityForm.get('description'); }
  get sourceOfAcquisition(): AbstractControl { return this.opportunityForm.get('sourceOfAcquisition'); }
  get subSourceOfAcquisition(): AbstractControl { return this.opportunityForm.get('subSourceOfAcquisition'); }
  get subjectOfInterest(): AbstractControl { return this.opportunityForm.get('subjectOfInterest'); }
  get ownerOneId(): AbstractControl { return this.opportunityForm.get('ownerOneId'); }
  get ownerOneSearch(): AbstractControl { return this.opportunityForm.get('ownerOneSearch'); }
  get ownerOnePercentage(): AbstractControl { return this.opportunityForm.get('ownerOnePercentage'); }
  get ownerTwoId(): AbstractControl { return this.opportunityForm.get('ownerTwoId'); }
  get ownerTwoSearch(): AbstractControl { return this.opportunityForm.get('ownerTwoSearch'); }
  get ownerTwoPercentage(): AbstractControl { return this.opportunityForm.get('ownerTwoPercentage'); }
  get ownerThreeId(): AbstractControl { return this.opportunityForm.get('ownerThreeId'); }
  get ownerThreeSearch(): AbstractControl { return this.opportunityForm.get('ownerThreeSearch'); }
  get ownerThreePercentage(): AbstractControl { return this.opportunityForm.get('ownerThreePercentage'); }
  get authKey(): string { return (this.update) ? 'OpportunityEditComponent.' : 'OpportunityCreateComponent.'; }
  // @formatter:on

  ngOnInit(): void {
    this.subscribeForCurrencies();
    this.subscribeForSliderClose();
    this.getAccountEmails();
    this.getProcessDefinitions();
    this.getSourcesOfAcquisitionForMe([]);
    this.getSubjectsOfInterestForMe([]);
    this.applyPermissionsToForm();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  isFormValid(): boolean {
    return this.opportunityForm.valid && !this.percentageSumError;
  }

  markAsTouched(): void {
    FormUtil.setTouched(this.opportunityForm);
    this.checkOwnerPercentageSum();
  }

  getOpportunity(): OpportunitySave {
    return {
      id: (!this.opportunitySave) ? undefined : this.opportunitySave.id,
      name: this.name.value,
      customerId: this.customerId.value,
      contactId: this.contactId.value,
      priority: this.priority.value,
      amount: this.amount.value,
      currency: this.currency.value,
      probability: this.probability.value,
      createDate: DateUtil.convertDateToString(new Date(this.createDate.value)),
      finishDate: (this.finishDate.value == null) ? null : DateUtil.convertDateToString(new Date(this.finishDate.value)),
      processDefinitionId: this.processDefinitionId.value,
      description: this.description.value,
      sourceOfAcquisition: this.getSelectedValue(this.sourceOfAcquisition.value),
      subSourceOfAcquisition: this.getSelectedValue(this.subSourceOfAcquisition.value),
      subjectOfInterest: this.getSelectedValue(this.subjectOfInterest. value),
      ownerOneId: this.ownerOneId.value,
      ownerOnePercentage: this.ownerOnePercentage.value,
      ownerTwoId: this.ownerTwoId.value,
      ownerTwoPercentage: this.ownerTwoPercentage.value,
      ownerThreeId: this.ownerThreeId.value,
      ownerThreePercentage: this.ownerThreePercentage.value
    };
  }

  private getSelectedValue(value: number): number {
    if (value !== this.undefinedCategoryId) {
      return value;
    }
    return null;
  }


  private subscribeForSliderClose(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        takeUntil(this.componentDestroyed),
        filter(change => change.key === 'OpportunityList.AddSlider' || change.key === 'OpportunityList.EditSlider'),
        map(change => change.opened),
        distinctUntilChanged(),
        filter(opened => !opened)
      ).subscribe(
      () => {
        this.opportunityForm.reset();
        this.sourceOfAcquisition.patchValue(this.undefinedCategoryId);
        this.subSourceOfAcquisition.patchValue(this.undefinedCategoryId);
        this.subjectOfInterest.patchValue(this.undefinedCategoryId);
        this.ownerVisibleCount = 1;
      }
    );
  }

  private getAccountEmails(): void {
    this.personService.getAccountEmails()
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      emails => {
        this.accountEmails = emails;
        this.filteredAccounts.set(1, emails);
        this.filteredAccounts.set(2, emails);
        this.filteredAccounts.set(3, emails);

        this.setOpportunityOwners();
      },
      err => this.notificationService.notify('opportunity.form.notification.accountEmailsError', NotificationType.ERROR)
    );
  }

  private getProcessDefinitions(): void {
    this.leadService.getProcessDefinitions('OPPORTUNITY')
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      definitions => this.processDefinitions = definitions,
      err => this.notificationService.notify('opportunity.form.notification.processDefinitionError', NotificationType.ERROR)
    );
  }


  private createOpportunityFrom(): void {
    this.opportunityForm = this.fb.group({
      name: [null, [Validators.required, FormValidators.whitespace, Validators.maxLength(200)]],
      customerId: [null, Validators.required],
      customerSearch: [null, [Validators.required,
        ((control: AbstractControl): ValidationErrors => this.searchControlModifiedAfterSelected(control, 'customer'))]],
      contactId: [null, Validators.required],
      contactSearch: [null, [Validators.required,
        ((control: AbstractControl): ValidationErrors => this.searchControlModifiedAfterSelected(control, 'contact'))]],
      priority: [null],
      amount: [null, [Validators.min(0), Validators.max(999999999999999999)]],
      currency: [null, [Validators.required]],
      probability: [null, Validators.compose([Validators.min(0), Validators.max(100)])],
      createDate: [null, [Validators.required]],
      finishDate: [null],
      processDefinitionId: [null, ((control: AbstractControl): ValidationErrors =>
        ((!this.update && (!control.value || control.value === '')) ? {required: true} : null))],
      description: [null, [Validators.maxLength(3000)]],
      sourceOfAcquisition: [null],
      subSourceOfAcquisition: [null],
      subjectOfInterest: [null],
      ownerOneId: [null, [Validators.required]],
      ownerOnePercentage: [null, [Validators.min(0), Validators.max(100), Validators.required]],
      ownerOneSearch: [null, [Validators.required,
        ((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequiredForSearch(control, 1))]],
      ownerTwoId: [null, [((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequired(control, 2))]],
      ownerTwoPercentage: [null, [Validators.min(0), Validators.max(100),
        ((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequired(control, 2))]],
      ownerTwoSearch: [null, [((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequiredForSearch(control, 2))]],
      ownerThreeId: [null, [((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequired(control, 3))]],
      ownerThreePercentage: [null, [Validators.min(0), Validators.max(100),
        ((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequired(control, 3))]],
      ownerThreeSearch: [null, [((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequiredForSearch(control, 3))]],
    });
    this.listenForSourceOfAcquisitionChanges();
    this.applyPermissionsToForm();
    this.subscribeCustomerSearch();
    this.subscribeCustomerId();
    this.subscribeContactSearch();
    this.subscribeAccountSearchChange();
    this.subscribeOwnerPercentageChange();
    this.subscribeProbabilityChange();
  }

  private searchControlModifiedAfterSelected(control: AbstractControl, type: 'customer' | 'contact'): ValidationErrors {
    if (!this.opportunityForm) {
      return null;
    }
    const idControl = (type === 'customer') ? this.customerId : this.contactId;
    const searchControl = (type === 'customer') ? this.customerSearch : this.contactSearch;
    if (control.value && control.value !== '' &&
      ((this.selectedCustomer && type === 'customer') ||
        (this.selectedContact && type === 'contact'))) {

      const storedValue = (type === 'customer') ?
        this.selectedCustomer : this.selectedContact;
      return (searchControl.value === storedValue) ? null : {valueModified: true};
    } else if (idControl.value && idControl.value !== '') {
      return {valueModified: true};
    } else {
      return {notSelected: true};
    }
  }

  private checkIfOwnerRequiredForSearch(control: AbstractControl, index: number): ValidationErrors {
    if (this.ownerVisibleCount >= index && (!control.value || control.value === '')) {
      return {required: true};
    }
    if (this.getOwnerIdByIndex(index)) {
      this.getOwnerIdByIndex(index).markAsTouched();
      return this.getOwnerIdByIndex(index).hasError('required') ? {required: true} : null;
    }
  }

  private checkIfOwnerRequired(control: AbstractControl, index: number): ValidationErrors {
    return (this.ownerVisibleCount >= index && (!control.value)) ? {required: true} : null;
  }

  private subscribeOwnerPercentageChange(): void {
    merge(
      this.getOwnerPercentageByIndex(1).valueChanges,
      this.getOwnerPercentageByIndex(2).valueChanges,
      this.getOwnerPercentageByIndex(3).valueChanges
    ).pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.checkOwnerPercentageSum());
  }

  private subscribeProbabilityChange(): void {
    this.probability.valueChanges
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((value: string) => OpportunityFormUtil.probabilityChange(this.probability, value));
  }

  private checkOwnerPercentageSum(): void {
    let sum = 0;
    for (let i = 1; i <= this.ownerVisibleCount; i++) {
      const percent = this.getOwnerPercentageByIndex(i).value;
      if (percent) {
        sum += +percent;
      }
    }
    this.percentageSumError = sum > 100;
  }

  subscribeAccountSearchChange(): void {
    this.subscribeAccountSearchChangeByIndex(1);
    this.subscribeAccountSearchChangeByIndex(2);
    this.subscribeAccountSearchChangeByIndex(3);
  }

  subscribeAccountSearchChangeByIndex(index: number): void {
    this.getOwnerSearchByIndex(index).valueChanges.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe((value: string) => {
      this.clearIdIfNotCorrect(index);
      this.filterAccountByIndex(value, index);
    });
  }

  private clearIdIfNotCorrect(index: number): void {
    const id = this.getOwnerIdByIndex(index).value;
    const account = this.accountEmails.find((accountObj) => accountObj.id === id);
    if (account) {
      if (this.getOwnerSearchByIndex(index).value !== (account.name + ' ' + account.surname)) {
        this.getOwnerIdByIndex(index).patchValue(undefined);
      }
    }
  }

  private filterAccountByIndex(value: string, index: number): void {
    let filtered: AccountEmail[] = [];
    if (value) {
      filtered = this.accountEmails
        .filter(account => (account.name + ' ' + account.surname).toLowerCase().indexOf(('' + value).toLowerCase()) !== -1);
    } else {
      filtered = this.accountEmails;
    }
    this.filteredAccounts.set(index, filtered || []);
    this.filterSelectedAccounts();
  }

  private filterSelectedAccounts(): void {
    const allIds = [this.ownerOneId.value, this.ownerTwoId.value, this.ownerThreeId.value].filter(id => !!id);
    if (this.filteredAccounts.get(1)) {
      this.filteredAccounts.set(1, this.filteredAccounts.get(1).filter(account => allIds.indexOf(account.id) === -1));
    }
    if (this.filteredAccounts.get(2)) {
      this.filteredAccounts.set(2, this.filteredAccounts.get(2).filter(account => allIds.indexOf(account.id) === -1));
    }
    if (this.filteredAccounts.get(3)) {
      this.filteredAccounts.set(3, this.filteredAccounts.get(3).filter(account => allIds.indexOf(account.id) === -1));
    }
  }

  private setOpportunityOwners(): void {
    if (this.opportunitySave) {
      this.ownerVisibleCount = 0;
      this.setOwner(1, this.opportunitySave.ownerOneId, this.opportunitySave.ownerOnePercentage);
      this.setOwner(2, this.opportunitySave.ownerTwoId, this.opportunitySave.ownerTwoPercentage);
      this.setOwner(3, this.opportunitySave.ownerThreeId, this.opportunitySave.ownerThreePercentage);
      if (this.ownerVisibleCount === 0) {
        this.ownerVisibleCount = 1;
      }
    }
  }

  private setOwner(index: number, ownerId: number, percentage: number): void {
    if (ownerId) {
      this.ownerVisibleCount++;
      const account: AccountEmail = this.accountEmails.find(obj => obj.id === ownerId);
      if (account) {
        this.getOwnerIdByIndex(index).patchValue(ownerId);
        this.getOwnerSearchByIndex(index).patchValue(account.name + ' ' + account.surname);
      }
      this.getOwnerPercentageByIndex(index).patchValue(percentage);
    }
  }

  private setCustomerAndContact(): void {
    if (this.opportunitySave && this.opportunitySave.customerId) {
      this.setLabelById(this.customerSearch, CrmObjectType.customer, this.customerId.value)
        .subscribe((response: CrmObject[]) => {
          if (response && response.length > 0) {
            this.selectedCustomerForNewContact = response[0];
          }
          if (this.opportunitySave.contactId) {
            this.contactId.patchValue(this.opportunitySave.contactId);
            this.setLabelById(this.contactSearch, CrmObjectType.contact, this.contactId.value)
              .subscribe();
          }
        });
    }
  }

  private setLabelById(controller: AbstractControl, type: CrmObjectType, id: number): Observable<any> {
    return this.assignToGroupService.updateLabels([{type, id}])
      .pipe(takeUntil(this.componentDestroyed),
        tap((response: CrmObject[]) => {
          if (response && response.length > 0) {
            controller.patchValue(response[0].label);
            if (type === CrmObjectType.customer) {
              this.selectedCustomer = response[0].label;
              this.customerSearch.updateValueAndValidity({emitEvent: false});
            } else if (type === CrmObjectType.contact) {
              this.selectedContact = response[0].label;
              this.contactSearch.updateValueAndValidity({emitEvent: false});
            }
          }
        }));
  }

  private subscribeContactSearch(): void {
    this.contactSearch.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed),
        debounceTime(250),
        filter(() => this.customerId && this.customerId.value && this.customerSearch.value === this.selectedCustomer)
      ).subscribe((value: string) => {
      if (!value || value === '') {
        this.filteredContacts = this.contactsByCustomerId;
      } else {
        this.filteredContacts = this.contactsByCustomerId
          .filter((contact: CrmObject) => {
            const fullName = contact.label;
            return fullName.toLowerCase().indexOf(value.toLowerCase()) !== -1;
          });
      }
    });
    this.contactSearch.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed),
        debounceTime(250),
        filter(() => !(this.customerId && this.customerId.value && this.customerSearch.value === this.selectedCustomer)),
        filter((input: string) => !!input && input.length >= 3),
        tap(() => this.contactSearchLoading = true),
        switchMap(((input: string) => this.assignToGroupService.searchForType(CrmObjectType.contact, input, [CrmObjectType.customer])
            .pipe(finalize(() => this.contactSearchLoading = false))
        ))).subscribe(
      (result: CrmObject[]) => {
        this.filteredContacts = result;
      }
    );
  }

  private autoSelectCustomer(contactId: number): void {
    this.customerService.getCustomerByContactId(contactId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(customer => this.customerSelect(customer));
  }

  private subscribeCustomerSearch(): void {
    this.customerSearch.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      filter((input: string) => !!input && input.length >= 3),
      tap(() => this.customerSearchLoading = true),
      switchMap(((input: string) => this.assignToGroupService.searchForType(CrmObjectType.customer, input, [CrmObjectType.customer])
          .pipe(finalize(() => this.customerSearchLoading = false))
      ))).subscribe(
      (result: CrmObject[]) => {
        this.filteredCustomers = result;
      }
    );
  }

  private subscribeCustomerId(): void {
    this.customerId.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed),
        filter((value) => !!value),
        tap(() => {
          this.contactsByCustomerId = [];
          this.filteredContacts = [];
        })
      ).subscribe((value) => this.updateCustomerContacts(value));
  }

  refreshCustomerContacts(): void {
    if (this.customerId) {
      this.updateCustomerContacts(this.customerId.value);
    }
  }

  updateCustomerContacts(customerId: number): void {
    if (customerId) {
      this.contactsService.getCustomerContacts(customerId)
        .pipe(
          takeUntil(this.componentDestroyed),
          map((contacts: Contact[]): CrmObject[] => contacts.map(c => this.contactToCrmObject(c)))
        ).subscribe((contacts: CrmObject[]) => {
        this.contactsByCustomerId = contacts;
        this.filteredContacts = contacts;
      });
    } else {
      this.contactsByCustomerId = [];
      this.filteredContacts = [];
    }
  }

  customerSelect(customer: CrmObject): void {
    if (customer && customer.type === CrmObjectType.customer) {
      this.selectedCustomer = customer.label;
      this.selectedCustomerForNewContact = customer;
      this.customerId.patchValue(customer.id);
      this.customerSearch.patchValue(customer.label);
    }
  }

  contactSelect(contact: CrmObject): void {
    if (contact) {
      this.selectedContact = contact.label;
      this.contactId.patchValue(contact.id);
      this.contactSearch.patchValue(contact.label);
      this.selectedCustomerForNewContact = cloneDeep(this.selectedCustomerForNewContact);
      this.autoSelectCustomer(contact.id);
    }
  }

  addNewOwner(): void {
    if (this.ownerVisibleCount < 3) {
      this.ownerVisibleCount++;
      this.updateAvailableOwnerList();
    }
  }

  getProcessFirstTask(): string {
    if (this.processDefinitionId && this.processDefinitionId.value) {
      const id = this.processDefinitionId.value;
      const process = this.processDefinitions.find(p => p.id === id);
      if (process) {
        return process.firstTaskName;
      }
    }
    return this.translateService.instant('opportunity.form.empty');
  }

  removeOwner(index: number): void {
    this.clearOwnerByIndex(index);
    for (let i = index + 1; i <= 3; i++) {
      this.moveOwnerToPreviousPlace(i);
    }
    this.ownerVisibleCount--;

    for (let i = this.ownerVisibleCount + 1; i <= 3; i++) {
      this.getOwnerIdByIndex(index).updateValueAndValidity();
      this.getOwnerSearchByIndex(index).updateValueAndValidity();
      this.getOwnerPercentageByIndex(index).updateValueAndValidity();
    }
    this.updateAvailableOwnerList();
  }

  moveOwnerToPreviousPlace(index: number): void {
    if (index > 1 && index <= 3 && this.ownerVisibleCount >= index) {
      this.getOwnerIdByIndex(index - 1).patchValue(cloneDeep(this.getOwnerIdByIndex(index).value));
      this.getOwnerSearchByIndex(index - 1).patchValue(cloneDeep(this.getOwnerSearchByIndex(index).value));
      this.getOwnerPercentageByIndex(index - 1).patchValue(cloneDeep(this.getOwnerPercentageByIndex(index).value));
      this.clearOwnerByIndex(index);
    }
  }

  clearOwnerByIndex(index: number): void {
    this.getOwnerIdByIndex(index).patchValue(undefined);
    this.getOwnerSearchByIndex(index).patchValue(undefined);
    this.getOwnerPercentageByIndex(index).patchValue(undefined);
  }

  private getOwnerIdByIndex(index: number): AbstractControl {
    return this.getOwnerByIndexAndName(index, 'Id');
  }

  private getOwnerSearchByIndex(index: number): AbstractControl {
    return this.getOwnerByIndexAndName(index, 'Search');
  }

  private getOwnerPercentageByIndex(index: number): AbstractControl {
    return this.getOwnerByIndexAndName(index, 'Percentage');
  }

  private getOwnerByIndexAndName(index: number, name: string): AbstractControl {
    if (this.opportunityForm) {
      return this.opportunityForm.get(this.getOwnerFormControlNameByIndexAndName(index, name));
    }
    return undefined;
  }

   getOwnerFormControlNameByIndexAndName(index: number, name: string): string {
    const indexName = [{id: 1, value: 'One'}, {id: 2, value: 'Two'}, {id: 3, value: 'Three'}]
      .find((obj) => obj.id === index);
    return 'owner' + indexName.value + name;
  }

  userSelect(account: AccountEmail, index: number): void {
    this.getOwnerIdByIndex(index).patchValue(cloneDeep(account.id));
    this.getOwnerSearchByIndex(index).patchValue(account.name + ' ' + account.surname);
    this.updateAvailableOwnerList();
  }

  private updateAvailableOwnerList(): void {
    this.filterAccountByIndex(this.getOwnerSearchByIndex(1).value, 1);
    this.filterAccountByIndex(this.getOwnerSearchByIndex(2).value, 2);
    this.filterAccountByIndex(this.getOwnerSearchByIndex(3).value, 3);
  }

  private applyPermissionsToForm(): void {
    if (this.opportunityForm) {
      applyPermission(this.name, !this.authService.isPermissionPresent(`${this.authKey}nameField`, 'r'));
      applyPermission(this.customerId, !this.authService.isPermissionPresent(`${this.authKey}customerField`, 'r'));
      applyPermission(this.customerSearch, !this.authService.isPermissionPresent(`${this.authKey}customerField`, 'r'));
      applyPermission(this.contactId, !this.authService.isPermissionPresent(`${this.authKey}contactField`, 'r'));
      applyPermission(this.contactSearch, !this.authService.isPermissionPresent(`${this.authKey}contactField`, 'r'));
      applyPermission(this.priority, !this.authService.isPermissionPresent(`${this.authKey}priorityField`, 'r'));
      applyPermission(this.amount, !this.authService.isPermissionPresent(`${this.authKey}amountField`, 'r'));
      applyPermission(this.currency, !this.authService.isPermissionPresent(`${this.authKey}currencyField`, 'r'));
      applyPermission(this.probability, !this.authService.isPermissionPresent(`${this.authKey}probabilityField`, 'r'));
      applyPermission(this.createDate, !this.authService.isPermissionPresent(`${this.authKey}createDateField`, 'r'));
      applyPermission(this.finishDate, !this.authService.isPermissionPresent(`${this.authKey}finishDateField`, 'r'));
      applyPermission(this.processDefinitionId, !this.authService.isPermissionPresent(`${this.authKey}processDefinitionField`, 'r'));
      applyPermission(this.description, !this.authService.isPermissionPresent(`${this.authKey}descriptionField`, 'r'));
      applyPermission(this.sourceOfAcquisition, !this.authService.isPermissionPresent(`${this.authKey}sourceOfAcquisitionField`, 'r'));
      applyPermission(this.subSourceOfAcquisition, !this.authService.isPermissionPresent(`${this.authKey}sourceOfAcquisitionField`, 'r'));
      applyPermission(this.subjectOfInterest, !this.authService.isPermissionPresent(`${this.authKey}subjectOfInterestField`, 'r'));
      applyPermission(this.ownerOneId, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerOnePercentage, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerOneSearch, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerTwoId, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerTwoPercentage, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerTwoSearch, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerThreeId, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerThreePercentage, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerThreeSearch, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
    }
  }

  public setFirstOwner(id: number, name: string, chance: number): void {
    if (this.opportunityForm) {
      this.ownerOneId.patchValue(id);
      this.ownerOneSearch.patchValue(name);
      this.ownerOnePercentage.patchValue(chance);
    }
  }

  public setCreateDate(date: Date): void {
    if (this.opportunityForm) {
      this.createDate.patchValue(date);
    }
  }

  public setCustomer(objectId: string): void {
    if (this.opportunityForm) {
      this.customerService
        .getCustomersByIds([+objectId])
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((customers: CrmObject[]) => {
          if (customers && customers.length) {
            this.customerSelect(customers[0]);
          }
        });
    }
  }

  public setContact(objectId: string): void {
    if (this.opportunityForm) {
      this.contactsService
        .getContactsByIds([+objectId])
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((contacts: CrmObject[]) => {
          if (contacts && contacts.length) {
            this.contactSelect(contacts[0]);
          }
        });
    }
  }

  public contactToCrmObject(contact: Contact | { id: number, name: string, surname: string }): CrmObject {
    return {
      id: contact.id,
      type: CrmObjectType.contact,
      label: `${contact.name} ${contact.surname}`
    };
  }

  private getNumberInFormat(value: number): string {
    return String(value).replace('.', ',');
  }

  private subscribeForCurrencies(): void {
    this.configService.getGlobalParamValue('currencies').pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (param) => this.currencies = param.value.split(';'),
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }

  refreshSelectedCustomer(): void {
    this.selectedCustomerForNewContact = cloneDeep(this.selectedCustomerForNewContact);
  }

  getSubSourceOfAcquisition(): void {
    const selected = this.sourceOfAcquisition.value;

    if (!selected || selected === this.undefinedCategoryId) {
      this.subSourceOfAcquisition.disable();
      this.subSourceOfAcquisition.patchValue(this.undefinedCategoryId);
      return;
    }

    this.subSourceOfAcquisition.enable();
    this.allSubSourceOfAcquisition = this.allSourceOfAcquisition.filter(word => word.id === selected);

    if (!this.allSubSourceOfAcquisition[0] || this.allSubSourceOfAcquisition[0].children.length === 0) {
      this.subSourceOfAcquisition.patchValue(this.undefinedCategoryId);
    }
  }

  private getSourcesOfAcquisitionForMe(privateWordsIds: number[]): void {
    this.dictionaryStoreService.getDictionaryWordsById(DictionaryId.SOURCE_OF_ACQUISITION, privateWordsIds).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      word => {
        this.allSourceOfAcquisition = word;
        this.getSubSourceOfAcquisition();
      },
      err => this.notificationService.notify('opportunity.form.notifications.getSourcesOfAcquisitionError', NotificationType.ERROR)
    );
  }

  private getSubjectsOfInterestForMe(privateWordsIds: number[]): void {
    this.dictionaryStoreService.getDictionaryWordsById(DictionaryId.SUBJECT_OF_INTEREST, privateWordsIds).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      words => {
        this.allSubjectOfInterest = words;
      },
      err => this.notificationService.notify('opportunity.form.notification.getSubjectsOfInterestError', NotificationType.ERROR)
    );
  }

  private getWords(dictionary: Dictionary): DictionaryWord[] {
    if (dictionary.words === undefined || dictionary.words === null) {
      return [];
    }
    return dictionary.words;
  }

  private listenForSourceOfAcquisitionChanges(): void {
    this.sourceOfAcquisition.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      filter(value => !!value),
      distinctUntilChanged((x, y) => isEqual(x, y)),
    ).subscribe(
      (category: number) => {
        if (category === undefined || category === null) {
          this.sourceOfAcquisition.patchValue(this.undefinedCategoryId);
          return;
        }

        this.getSubSourceOfAcquisition();
      }
    );
  }

  openAddSourceOfAcquisitionSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.addSourceOfAcquisitionSlider.open();
  }

  onCancelAddSourceOfAcquisitionSlider(): void {
    this.addSourceOfAcquisitionSlider.close();
  }

  onSubmitAddSourceOfAcquisitionSlider(savedWord: SaveDictionaryWord): void {
    this.allSourceOfAcquisition.unshift({...savedWord, children: []});
    this.sourceOfAcquisition.patchValue(savedWord.id);
    this.getSubSourceOfAcquisition();
    this.addSourceOfAcquisitionSlider.close();
  }

  openAddSubSourceOfAcquisitionSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.parentWordIdForCreation = this.sourceOfAcquisition.value;
    this.addSubSourceOfAcquisitionSlider.open();
  }

  onCancelAddSubSourceOfAcquisitionSlider(): void {
    this.addSubSourceOfAcquisitionSlider.close();
  }

  onSubmitAddSubSourceOfAcquisitionSlider(savedWord: SaveDictionaryWord): void {
    const parentWord = this.getSelectedSourceOfAcquisition();
    parentWord.children.unshift({...savedWord, children: []});
    this.getSubSourceOfAcquisition();
    this.subSourceOfAcquisition.patchValue(savedWord.id);
    this.parentWordIdForCreation = null;
    this.addSubSourceOfAcquisitionSlider.close();
  }

  private getSelectedSourceOfAcquisition(): DictionaryWord {
    return this.allSourceOfAcquisition.find(word => word.id === this.sourceOfAcquisition.value);
  }

  onCancelAddSubjectOfInterestSlider(): void {
    this.addSubjectOfInterestSlider.close();
  }

  openAddSubjectOfInterestSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.addSubjectOfInterestSlider.open();
  }

  onSubmitAddSubjectOfInterestSlider(savedWord: SaveDictionaryWord): void {
    this.allSubjectOfInterest.unshift({...savedWord, children: []});
    this.subjectOfInterest.patchValue(savedWord.id);
    this.addSubjectOfInterestSlider.close();
  }
}
