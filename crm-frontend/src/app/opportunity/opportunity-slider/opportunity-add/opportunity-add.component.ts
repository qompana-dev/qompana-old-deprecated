import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {SliderService} from '../../../shared/slider/slider.service';
import {OpportunityFormComponent} from '../opportunity-form/opportunity-form.component';
import {OpportunityService} from '../../opportunity.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {HttpErrorResponse} from '@angular/common/http';
import {BAD_REQUEST} from 'http-status-codes';
import {OpportunitySave} from '../../opportunity.model';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';
import {LoginService} from '../../../login/login.service';
import {Subject} from 'rxjs';
import {FilterStrategy} from "../../../shared/model";

@Component({
  selector: 'app-opportunity-add',
  templateUrl: './opportunity-add.component.html',
  styleUrls: ['./opportunity-add.component.scss']
})
export class OpportunityAddComponent implements OnInit, OnDestroy {

  @Input() opportunity: OpportunitySave;
  @Input() initOpportunityDefaultValues: boolean;
  @Input() objectId: string;
  @Input() objectType: string;
  @Output() opportunityAdded: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild('opportunityForm') opportunityForm: OpportunityFormComponent;
  private componentDestroyed: Subject<void> = new Subject();

  saveInProgress = false;

  readonly addOpportunityErrorTypes: ErrorTypes = {
    base: 'opportunity.form.notification.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'badRequest'
      }
    ]
  };

  constructor(private sliderService: SliderService,
              private notificationService: NotificationService,
              private opportunityService: OpportunityService,
              private loginService: LoginService) {
  }

  ngOnInit(): void {
    this.listenWhenSliderOpen();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
  }

  submit(): void {
    if (!this.opportunityForm.isFormValid()) {
      this.opportunityForm.markAsTouched();
      return;
    }
    if (!this.saveInProgress) {
      this.saveInProgress = true;
      const opportunity: OpportunitySave = this.opportunityForm.getOpportunity();
      this.opportunityService.createOpportunity(opportunity)
        .subscribe(
          () => {
            this.notifyAddSuccess();
            this.emitOpportunityAdded();
            this.saveInProgress = false;
            this.closeSlider();
          },
          (err) => this.notifyError(err)
        );
    }
  }

  private notifyAddSuccess(): void {
    this.notificationService.notify('opportunity.form.notification.addedSuccessfully', NotificationType.SUCCESS);
  }

  private emitOpportunityAdded(): void {
    this.opportunityAdded.emit();
  }

  private notifyError(err: HttpErrorResponse): void {
    this.saveInProgress = false;
    this.notificationService.notifyError(this.addOpportunityErrorTypes, err.status);
  }

  private listenWhenSliderOpen(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        takeUntil(this.componentDestroyed),
        filter(change => change.key === 'OpportunityList.AddSlider'),
        map(change => change.opened),
        distinctUntilChanged(),
        filter(opened => opened)
      ).subscribe(
      () => {
        this.opportunityForm.setFirstOwner(
          this.loginService.getLoggedAccountId(),
          this.loginService.getLoggedAccountName(),
          100);
        this.opportunityForm.setCreateDate(new Date());
        if (this.objectId && this.objectType) {
          switch (this.objectType) {
            case 'CUSTOMER':
              this.opportunityForm.setCustomer(this.objectId);
              break;
            case 'CONTACT':
              this.opportunityForm.setContact(this.objectId);
              break;
          }
        }
      }
    );
  }
}
