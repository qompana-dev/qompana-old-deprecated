import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {NgxMaskModule} from 'ngx-mask';
import {OpportunityAddComponent} from './opportunity-add/opportunity-add.component';
import {OpportunityFormComponent} from './opportunity-form/opportunity-form.component';
import {OpportunityEditComponent} from './opportunity-edit/opportunity-edit.component';
import {CustomerSliderModule} from '../../customer/customer-slider/customer-slider.module';
import {ContactSliderModule} from '../../contacts/contact-slider/contact-slider.module';
import {DictionaryModule} from '../../dictionary/dictionary.module';

@NgModule({
  declarations: [
    OpportunityAddComponent,
    OpportunityFormComponent,
    OpportunityEditComponent
  ],
    imports: [
        CommonModule,
        SharedModule,
        NgxMaskModule,
        CustomerSliderModule,
        ContactSliderModule,
        DictionaryModule
    ],
  exports: [
    OpportunityAddComponent,
    OpportunityEditComponent
  ]
})
export class OpportunitySliderModule { }
