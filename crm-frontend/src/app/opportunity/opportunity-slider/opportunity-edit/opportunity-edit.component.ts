import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {BAD_REQUEST, NOT_FOUND} from 'http-status-codes';
import {OpportunitySave} from '../../opportunity.model';
import {SliderService} from '../../../shared/slider/slider.service';
import {OpportunityService} from '../../opportunity.service';
import {HttpErrorResponse} from '@angular/common/http';
import {OpportunityFormComponent} from '../opportunity-form/opportunity-form.component';

@Component({
  selector: 'app-opportunity-edit',
  templateUrl: './opportunity-edit.component.html',
  styleUrls: ['./opportunity-edit.component.scss']
})
export class OpportunityEditComponent implements OnInit {

  @Input() opportunity: OpportunitySave;
  @Output() opportunityEdited: EventEmitter<void> = new EventEmitter();
  readonly editOpportunityErrorTypes: ErrorTypes = {
    base: 'opportunity.form.notification.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'badRequest'
      },
      {
        code: NOT_FOUND,
        text: 'opportunityNotFound'
      }
    ]
  };
  @ViewChild(OpportunityFormComponent) private opportunityFormComponent: OpportunityFormComponent;

  constructor(private sliderService: SliderService,
              private opportunityService: OpportunityService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
  }

  submit(): void {
    if (!this.opportunityFormComponent.isFormValid()) {
      this.opportunityFormComponent.markAsTouched();
      return;
    }
    const opportunity: OpportunitySave = this.opportunityFormComponent.getOpportunity();
    this.opportunityService.updateOpportunity(this.opportunity.id, opportunity)
      .subscribe(
        () => {
          this.notifyAddSuccess();
          this.emitContactEdited();
          this.closeSlider();
        },
        (err) => this.notifyError(err)
      );
  }

  private notifyAddSuccess(): void {
    this.notificationService.notify('opportunity.form.notification.editedSuccessfully', NotificationType.SUCCESS);
  }

  private emitContactEdited(): void {
    this.opportunityEdited.emit();
  }

  private notifyError(err: HttpErrorResponse): void {
    this.notificationService.notifyError(this.editOpportunityErrorTypes, err.status);
  }
}
