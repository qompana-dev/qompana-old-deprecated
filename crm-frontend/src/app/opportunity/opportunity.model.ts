import { StatusAcceptance } from './../bpmn/bpmn.model';
import {CrmObjectType} from "../shared/model/search.model";
import {KanbanLead} from "../lead/lead.model";


export interface OpportunityBase {
  id: number;
  name: string;
  processInstanceId: string;
}

export interface OpportunityOnList extends OpportunityBase {
  processInstanceName: string;
  processInstanceVersion: number;
  finishResult: FinishOpportunityResult;
  canBeRolledBackToPreviousStep: boolean,
  canBeMovedToFollowingStep: boolean,
  followingStepName: string,
  previousStepName: string,
  keeper: number;
  customerName: string;
  finishDate: string;
  status: number;
  maxStatus: number;
  ownerOneId: number;
  ownerTwoId: number;
  ownerThreeId: number;
  acceptanceUserId: number;
}

export interface OpportunitySave {
  id: number;
  name: string;
  customerId: number;
  contactId: number;
  priority: OpportunityPriority;
  amount: number;
  estimatedAmount?: number;
  currency: string;
  probability: number;
  createDate: string;
  finishDate: string;
  processDefinitionId: string;
  description: string;
  sourceOfAcquisition: number;
  subSourceOfAcquisition: number;
  subjectOfInterest: number;
  ownerOneId: number;
  ownerOnePercentage: number;
  ownerTwoId: number;
  ownerTwoPercentage: number;
  ownerThreeId: number;
  ownerThreePercentage: number;
  creator?: number;
  created?: string;
  canEditAmount?: boolean;
}

export class OpportunityDetails {
  id: number;
  processInstanceId: string;
  name: string;
  priority: OpportunityPriority;
  customerId: string;
  customerName: string;
  contactId: string;
  createDate: string;
  finishDate: string;
  probability: number;
  amount: number;
  currency: string;
  amountInMainCurrency: number;
  description: string;
  ownerOneId: number;
  ownerTwoId: number;
  ownerThreeId: number;
  created: string;
  creator: number;
  sourceOfAcquisition: number;
  subSourceOfAcquisition: number;
  finishResult: FinishOpportunityResult;
  rejectionReason: OpportunityRejectionReason;
  bgColor?: string;
  textColor?: string;
  acceptanceUserId: number;
  tasks: Task[];
  convertedFromLead: boolean;
}

export class Task {
  acceptanceUserId?: number;
  id: string;
  name: string;
  documentation: string;
  active: boolean;
  past: boolean;
  percentage: number;
  isInformationTask?: boolean;
  properties: Property[];
  statusAcceptance: StatusAcceptance | null;
}

export class Property {
  id: string;
  type: string;
  required: boolean;
  value: string;
  possibleValues: PossibleValue[];
}

export class PossibleValue {
  id: string;
  name: string;
}

export interface OpportunityFinishDto {
  finishResult: FinishOpportunityResult;
  rejectionReason?: OpportunityRejectionReason;
}

export enum OpportunityPriority {
  LOW = 'LOW', MEDIUM = 'MEDIUM', HIGH = 'HIGH'
}

export const PriorityRadioButtons = [
  {
    class: 'low-radio-button',
    value: OpportunityPriority.LOW,
    translation: 'opportunity.form.opportunityPriority.low',
  },
  {
    class: 'medium-radio-button',
    value: OpportunityPriority.MEDIUM,
    translation: 'opportunity.form.opportunityPriority.medium',
  },
  {
    class: 'high-radio-button',
    value: OpportunityPriority.HIGH,
    translation: 'opportunity.form.opportunityPriority.high',
  },
];

export enum FinishOpportunityResult {
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
}

export enum OpportunityRejectionReason {
  NONE = 'NONE',
  LOST = 'LOST',
  NO_BUDGET = 'NO_BUDGET',
  NO_DECISION = 'NO_DECISION',
  OTHER = 'OTHER',
}

export enum ViewType {
  LIST = 'LIST',
  KANBAN = 'KANBAN',
}

export interface KanbanOpportunity extends OpportunityBase {
  customerId: number;
  customerName: string;
  customerAvatar: string;
  contactId: number;
  amount: number;
  currency: string;
  probability: number;
  amountInDefaultCurrency: number;
  createDate: string;
  finishDate: string;
  priority: OpportunityPriority;
  finishResult: FinishOpportunityResult;
  duration: number;
  userTaskDelayTime: number;
  owners: Owner[];
  taskSummaryDto: TaskSummary;
  canBeMoveForward: ObjectMoveStep;
  canBeMoveBackward: ObjectMoveStep;
}

export interface Owner {
  number: number;
  id: number;
  name: string;
  surname: string;
  percentage: number;
  avatar: string;
  email: string;
}

export interface ObjectMoveStep {
  canMove: boolean;
  possibleStageId: string[];
}

export interface TaskSummary {
  allTasks: number;
  oldTasks: number;
  plannedTasks: number;
  latestTasks: number;
  completedTasks: number;
}

export interface KanbanOpportunityProcessOverview {
  id: string;
  name: string;
  version: number;
  size: number;
  backgroundColor: string;
  textColor: string;
  tasks: KanbanOpportunityTaskOverview[];
}

export interface KanbanOpportunityTaskOverview {
  id: string;
  name: string;
  assignee: string;
  includedOpportunitiesNumber: number;
  summaryValue: number;
}

export interface KanbanOpportunityTaskDetails {
  id: string;
  name: string;
  opportunities: KanbanOpportunity[];
}

