import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Subject} from "rxjs/Subject";
import {FilterStrategy} from "../../shared/model";

@Injectable({
  providedIn: 'root'
})
export class OpportunityListService {

  private searchText = '';
  private filterStrategy: FilterStrategy = FilterStrategy.OPENED;

  private _searchListeners = new Subject<any>();

  addSearchListen(): Observable<any> {
    return this._searchListeners.asObservable();
  }

  search(search: string): void {
    this.searchText = search;
    this._searchListeners.next();
  }

  filter(filterStrategy: FilterStrategy): void {
    this.filterStrategy = filterStrategy;
    this._searchListeners.next();
  }

  complete(): void {
    this._searchListeners.complete();
  }

  public getSearch(): string {
    return this.searchText;
  }

  public getFilterStrategy(): FilterStrategy {
    return this.filterStrategy;
  }

}


