import {OpportunityBase, OpportunityOnList} from "./../opportunity.model";
import {
  FinishOpportunityResult,
  OpportunityFinishDto,
  OpportunityRejectionReason,
} from "../opportunity.model";
import {
  Colors,
  ConfirmDialogData,
  ContentTypes,
} from "./../../shared/dialog/confirm-dialog/confirm-dialog.model";

const rejectionReasonItems: any[] = [
  {
    title: "opportunity.form.dialog.rejectionReason.item.none",
    value: OpportunityRejectionReason.NONE,
  },
  {
    title: "opportunity.form.dialog.rejectionReason.item.lost",
    value: OpportunityRejectionReason.LOST,
  },
  {
    title: "opportunity.form.dialog.rejectionReason.item.noBudget",
    value: OpportunityRejectionReason.NO_BUDGET,
  },
  {
    title: "opportunity.form.dialog.rejectionReason.item.noDecision",
    value: OpportunityRejectionReason.NO_DECISION,
  },
  {
    title: "opportunity.form.dialog.rejectionReason.item.other",
    value: OpportunityRejectionReason.OTHER,
  },
];

export class OpportunityDialogsConfig {
  private readonly translatePrefix = "opportunity.form.dialog";

  constructor(private translate) {
    this.translate = translate;
  }

  private getKey(k) {
    return `${this.translatePrefix}.${k}`;
  }

  private t(k) {
    return this.translate.instant(this.getKey(k));
  }

  private getStatusByFinishResult(finishResult) {
    const isSuccess = finishResult === FinishOpportunityResult.SUCCESS;
    return `<span class="${isSuccess ? "success" : "alert"}-text">
      ${this.t(`close.${isSuccess ? "win" : "lose"}`)}
    </span>`;
  }

  public deleteOpportunity(
    opportunity: OpportunityBase,
    func: (id) => void
  ): ConfirmDialogData {
    const { id, name } = opportunity;

    return {
      title: this.getKey("remove.title"),
      contentType: ContentTypes.HTML,
      content: {
        icon: "warning",
        iconColor: Colors.ALERT,
        htmlTemplate: `
          ${this.t("remove.singleText")} <b>${name}</b >?<br/>
          ${this.t("remove.description")}
        `,
      },
      actions: [
        {
          text: this.getKey("remove.confirm"),
          func: () => func(id),
        },
        {
          text: this.getKey("remove.cancel"),
          color: Colors.ALERT,
        },
      ],
    };
  }

  public deleteOpportunites(func: () => void): ConfirmDialogData {
    return {
      title: this.getKey("remove.title"),
      contentType: ContentTypes.HTML,
      content: {
        icon: "warning",
        iconColor: Colors.ALERT,
        htmlTemplate: `
          ${this.t("remove.multiText")}<br/>
          ${this.t("remove.description")}
        `,
      },
      actions: [
        {
          text: this.getKey("remove.confirm"),
          func,
        },
        {
          text: this.getKey("remove.cancel"),
          color: Colors.ALERT,
        },
      ],
    };
  }

  public copyOpportunity(
    opportunity: OpportunityBase,
    func: (id) => void
  ): ConfirmDialogData {
    const { id, name } = opportunity;
    return {
      title: this.getKey("copy.title"),
      contentType: ContentTypes.HTML,
      content: {
        icon: "copy",
        htmlTemplate: `
          ${this.t("copy.text")}
          „<b>${name}</b>(${this.t("copy.copyLabel")})”
        `,
      },
      actions: [
        {
          text: this.getKey("copy.confirm"),
          func: () => func(id),
          color: Colors.PRIMARY,
        },
        {
          text: this.getKey("copy.cancel"),
        },
      ],
    };
  }

  public rejectionReason(func: (rejectionReason) => void): ConfirmDialogData {
    return {
      title: this.getKey("rejectionReason.title"),
      contentType: ContentTypes.SELECT,
      content: {
        items: rejectionReasonItems,
        translateField: "title",
        placeholder: this.getKey("rejectionReason.placeholder"),
      },
      actions: [
        {
          text: this.getKey("rejectionReason.confirm"),
          func: (select) => {
            func(select.value);
          },
          color: Colors.ALERT,
        },
        {
          text: this.getKey("rejectionReason.cancel"),
        },
      ],
    };
  }

  public closeOpportunity(
    opportunity: OpportunityBase,
    finishResult: FinishOpportunityResult,
    func: (id, finishBody) => void
  ): ConfirmDialogData {
    const { id, name } = opportunity;

    const status = this.getStatusByFinishResult(finishResult);

    const finishBody: OpportunityFinishDto = {
      finishResult,
    };

    return {
      title: this.getKey("close.title"),
      contentType: ContentTypes.HTML,
      content: {
        icon: "warning",
        iconColor: Colors.ALERT,
        htmlTemplate: `
            ${this.t("close.singleText")} <b>${name}</b><br/>
            ${this.t("close.withStatus")} ${status}?
          `,
      },
      actions: [
        {
          text: this.getKey("close.confirm"),
          func: () => func(id, finishBody),
          color: Colors.ALERT,
        },
        {
          text: this.getKey("close.cancel"),
        },
      ],
    };
  }

  public closeOpportunities(
    finishResult: FinishOpportunityResult,
    func: (finishBody) => void
  ): ConfirmDialogData {
    const status = this.getStatusByFinishResult(finishResult);

    const finishBody: OpportunityFinishDto = {
      finishResult,
    };

    return {
      title: this.getKey("close.title"),
      contentType: ContentTypes.HTML,
      content: {
        icon: "warning",
        iconColor: Colors.ALERT,
        htmlTemplate: `
            ${this.t("close.multiText")}<br/>
            ${this.t("close.withStatus")} ${status}?
          `,
      },
      actions: [
        {
          text: this.getKey("close.confirm"),
          func: () => func(finishBody),
          color: Colors.ALERT,
        },
        {
          text: this.getKey("close.cancel"),
        },
      ],
    };
  }
}
