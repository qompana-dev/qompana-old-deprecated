import {
  FinishOpportunityResult,
  OpportunityOnList,
} from "./../../opportunity.model";
import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-opportunity-list-actions",
  templateUrl: "./opportunity-list-actions.component.html",
  styleUrls: ["./opportunity-list-actions.component.scss"],
})
export class OpportunityListActionsComponent {
  @Input() opportunity: OpportunityOnList;
  @Input() acceptanceUser: string;

  @Input() removeOpportunity: (o) => void;
  @Input() copyOpportunity: (o) => void;
  @Input() closeOpportunity: (o, t) => void;
  @Input() goToTask: (o, i) => void;

  finishTypes = FinishOpportunityResult;

  readonly tooltipPosition = {
    originX: "end",
    overlayX: "end",
    offsetY: -8,
  };

  constructor() {}

  onCopy(e: MouseEvent) {
    e.stopPropagation();
    if (this.copyOpportunity) {
      this.copyOpportunity(this.opportunity);
    }
  }

  onClose(e: MouseEvent, type: FinishOpportunityResult) {
    e.stopPropagation();
    this.closeOpportunity(this.opportunity, type);
  }

  onRemove(e: MouseEvent) {
    e.stopPropagation();
    if (this.removeOpportunity) {
      this.removeOpportunity(this.opportunity);
    }
  }

  onGoToTask(e: MouseEvent, isComplete: boolean) {
    e.stopPropagation();
    if (this.goToTask) {
      this.goToTask(this.opportunity, isComplete);
    }
  }
}
