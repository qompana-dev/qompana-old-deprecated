export enum SearchType {
  NAME = 'NAME',
  CUSTOMER_NAME = 'CUSTOMER_NAME',
  CONTACT_NAME = 'CONTACT_NAME',
  SOURCE = 'SOURCE'
}

export interface SearchVariantGroups {
  type: SearchType;
  variants: string[];
}
