import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable, of, Subject, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map, startWith, takeUntil} from 'rxjs/operators';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete/typings/autocomplete';
import {OpportunitySearchService} from './opportunity-search.service';
import {SearchVariantGroups} from './opportunity-search.model';
import {ViewType} from '../../opportunity.model';
import {OpportunityListService} from '../opportunity-list.service';

@Component({
  selector: 'app-opportunity-search',
  templateUrl: './opportunity-search.component.html',
  styleUrls: ['./opportunity-search.component.scss']
})
export class OpportunitySearchComponent implements OnInit {

  @Input() viewType: ViewType;

  searchControl = new FormControl();
  search = '';
  isSearched = false;
  searchVariants: SearchVariantGroups[] = [];
  searchVariantsObservable: Observable<SearchVariantGroups[]> = of(this.searchVariants);

  private componentDestroyedNotifier: Subject<void> = new Subject();
  private searchSubscription: Subscription;


  constructor(private opportunitySearchService: OpportunitySearchService,
              private opportunityListService: OpportunityListService) {
  }

  ngOnInit(): void {
    this.initSearchField();
  }

  private initSearchField(): void {
    this.searchSubscription = this.searchControl.valueChanges
      .pipe(
        startWith(''),
        map(value => value.trim()),
        filter(value => !value || value.length > 0),
        debounceTime(500),
        distinctUntilChanged(),
      ).subscribe(value => {
        this.search = value;
        this.initMembers();
      });
  }

  private initMembers(): void {
    this.componentDestroyedNotifier.next();
    if (this.search.length > 0) {
      this.opportunitySearchService.getSearchVariants(this.search)
        .pipe(
          takeUntil(this.componentDestroyedNotifier)
        ).subscribe(
        response => this.handleInitSuccess(response)
      );
    }
  }

  private handleInitSuccess(response: SearchVariantGroups[]): void {
    this.searchVariants = response;
    this.searchVariantsObservable = of(this.searchVariants.filter(group => group.variants.length > 0));
  }

  selectSearchVariant({option}: MatAutocompleteSelectedEvent): void {
    this.search = option.value;
    this.emitSearchEvent();
  }

  emitSearchEvent(): void {
    if (this.search.length > 0) {
      this.isSearched = true;
      if (this.viewType === ViewType.LIST) {
        this.opportunityListService.search(this.search);
      }
      if (this.viewType === ViewType.KANBAN) {
        this.opportunityListService.search(this.search);
      }
    }
  }

  clearSearch(): void {
    this.search = '';
    this.searchControl.setValue('');
    this.isSearched = false;
    this.searchVariantsObservable = of([]);
    if (this.viewType === ViewType.LIST) {
      this.opportunityListService.search(this.search);
    }
    if (this.viewType === ViewType.KANBAN) {
      this.opportunityListService.search(this.search);
    }
  }

}
