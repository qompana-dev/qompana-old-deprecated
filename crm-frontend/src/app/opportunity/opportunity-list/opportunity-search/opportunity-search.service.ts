import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SearchVariantGroups} from "./opportunity-search.model";
import {UrlUtil} from "../../../shared/url.util";

@Injectable({
  providedIn: 'root'
})
export class OpportunitySearchService {

  private searchVariantsEndpoint = `${UrlUtil.url}/crm-business-service/opportunity/search-variants`;

  constructor(private http: HttpClient) {
  }

  getSearchVariants(search: string): Observable<SearchVariantGroups[]> {
    const params = new HttpParams({fromObject: {search}});
    return this.http.get<SearchVariantGroups[]>(this.searchVariantsEndpoint, {params});
  }

}

export enum SliderType {
  ACTIVITY = 'ACTIVITY',
  OPPORTUNITY_EDIT = 'CUSTOMER_NAME'
}
