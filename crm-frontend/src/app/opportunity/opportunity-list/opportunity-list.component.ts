import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FilterStrategy} from '../../shared/model';
import {Page, PageRequest} from '../../shared/pagination/page.model';
import {finalize, map, startWith, takeUntil} from 'rxjs/operators';
import {ErrorTypes, NotificationService, NotificationType, } from '../../shared/notification.service';
import {Observable, Subject} from 'rxjs';
import {OpportunityService} from '../opportunity.service';
import {PersonService} from '../../person/person.service';
import {
  FinishOpportunityResult,
  OpportunityFinishDto,
  OpportunityOnList,
  OpportunitySave,
  ViewType,
} from '../opportunity.model';
import {Sort} from '@angular/material/sort';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {AuthService} from '../../login/auth/auth.service';
import {SliderComponent} from '../../shared/slider/slider.component';
import {FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import {Router} from '@angular/router';
import {ConfirmDialogService} from 'src/app/shared/dialog/confirm-dialog/confirm-dialog.service';
import {TranslateService} from '@ngx-translate/core';
import {OpportunityDialogsConfig} from './opportunity-list.dialogs';
import {OpportunityListService} from './opportunity-list.service';

@Component({
  selector: 'app-opportunity-list',
  templateUrl: './opportunity-list.component.html',
  styleUrls: ['./opportunity-list.component.scss'],
})
export class OpportunityListComponent implements OnInit, OnDestroy {
  columns = [
    'selectRow',
    'avatar',
    'name',
    'external',
    'customer.name',
    'createDate',
    'finishDate',
    'amount',
    'currency',
    'status',
    'process',
    'priority',
    'keeper',
    'actions',
  ];

  @ViewChild('addSlider') addSlider: SliderComponent;

  filterSelection = FilterStrategy;
  finishTypes = FinishOpportunityResult;
  selectedFilter: FilterStrategy = FilterStrategy.OPENED;
  search = '';
  userMap: Map<number, string>;
  loadingFinished = true;
  private componentDestroyed: Subject<void> = new Subject();
  private fetchOpportunities: Subject<void> = new Subject();
  expanded = false;
  opportunityPage: Page<OpportunityOnList>;
  selectedOpportunities: OpportunityOnList[] = [];
  filteredColumns$: Observable<string[]>;
  selectedOpportunity: OpportunitySave;

  private currentSort = 'updated,desc';
  private pageOnList = 20;
  totalElements = 0;
  kanbanVisible = false;
  viewType: ViewType = ViewType.LIST;
  initOpportunityDefaultValues: boolean;

  private readonly dialogsConfig = new OpportunityDialogsConfig(
    this.translateService
  );

  private readonly opportunityErrorTypes: ErrorTypes = {
    base: 'opportunity.list.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'opportunityNotFound',
      },
      {
        code: FORBIDDEN,
        text: 'noPermission',
      },
    ],
  };




  constructor(
    private opportunityService: OpportunityService,
    private personService: PersonService,
    private authService: AuthService,
    private confirmDialogService: ConfirmDialogService,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private $router: Router,
    private opportunityListService: OpportunityListService
  ) {
    this.opportunityListService.addSearchListen().subscribe((search: string) => {
      this.search = this.opportunityListService.getSearch();
      this.getOpportunities(this.opportunityListService.getFilterStrategy());
    });
  }

  ngOnInit(): void {
    this.getAccountEmails();
    this.getOpportunities(this.selectedFilter);


    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.fetchOpportunities.next();
    this.componentDestroyed.complete();
    this.fetchOpportunities.complete();
  }

  public filter(filterStrategy: FilterStrategy): void {
    this.selectedFilter = filterStrategy;
    this.opportunityListService.filter(filterStrategy);
  }

  public setSelectedSearch(search: string): void {
    this.search = search;
    this.getOpportunities(this.selectedFilter);
  }

  private getOpportunities(
    selection: FilterStrategy,
    pageRequest: PageRequest = {
      page: '0',
      size: this.pageOnList + '',
      sort: this.currentSort,
    }
  ): void {
    this.fetchOpportunities.next();
    this.loadingFinished = false;
    this.selectedOpportunities = [];
    this.opportunityService
      .getOpportunities(selection, pageRequest, this.search)
      .pipe(
        takeUntil(this.fetchOpportunities),
        finalize(() => (this.loadingFinished = true))
      )
      .subscribe(
        (page) => this.handleOpportunitiesSuccess(page),
        () =>
          this.notificationService.notify(
            'opportunity.list.notification.listError',
            NotificationType.ERROR
          )
      );
  }

  changePagination(page: any): void {
    const { pageIndex, pageSize } = page;
    this.pageOnList = pageSize;
    this.getOpportunities(this.selectedFilter, {
      page: pageIndex,
      size: pageSize,
      sort: this.currentSort,
    });
  }

  public tryDeleteSelectedOpportunities(): void {
    const dialogData = this.dialogsConfig.deleteOpportunites(
      this.deleteOpportunities.bind(this)
    );

    this.openDeleteDialog(dialogData);
  }

  private openDeleteDialog(dialogData: object = {}): void {
    this.confirmDialogService.open(dialogData);
  }

  public deleteOpportunities(): void {
    if (this.selectedOpportunities.length === 0) {
      return;
    }
    this.opportunityService
      .removeOpportunities(this.selectedOpportunities)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.getOpportunities(this.selectedFilter);
          this.notificationService.notify(
            'opportunity.list.notification.deleteSuccess',
            NotificationType.SUCCESS
          );
        },
        (err) =>
          this.notificationService.notifyError(
            this.opportunityErrorTypes,
            err.status
          )
      );
  }

  public tryCloseOpportunities(finishResult: FinishOpportunityResult): void {
    const dialogData = this.dialogsConfig.closeOpportunities(
      finishResult,
      this.finishOpportunitiesByType.bind(this)
    );

    this.openDeleteDialog(dialogData);
  }

  private finishOpportunitiesByType(finishBody: OpportunityFinishDto): void {
    if (finishBody.finishResult === FinishOpportunityResult.SUCCESS) {
      this.closeOpportunities(finishBody);
    } else {
      const dialogData = this.dialogsConfig.rejectionReason((rejectionReason) =>
        this.closeOpportunities({
          ...finishBody,
          rejectionReason,
        })
      );

      this.openDeleteDialog(dialogData);
    }
  }

  public closeOpportunities(finishBody: OpportunityFinishDto): void {
    if (this.selectedOpportunities.length === 0) {
      return;
    }
    this.opportunityService
      .finishOpportunities(this.selectedOpportunities, finishBody)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.getOpportunities(this.selectedFilter);
        },
        (err) =>
          this.notificationService.notifyError(
            this.opportunityErrorTypes,
            err.status
          )
      );
  }

  public getOwnerNames(opportunity: OpportunityOnList): string {
    let ownerNames = '';
    if (opportunity.ownerOneId && this.userMap) {
      ownerNames += this.userMap.get(opportunity.ownerOneId);
    }
    if (opportunity.ownerTwoId) {
      ownerNames += ', ' + this.userMap.get(opportunity.ownerTwoId);
    }
    if (opportunity.ownerThreeId) {
      ownerNames += ', ' + this.userMap.get(opportunity.ownerThreeId);
    }
    return ownerNames;
  }

  public goToOpportunityDetails(id: number): void {
    this.$router.navigate(['/opportunity', id]);
  }

  public goToOpportunityDetailsNewTab(id: number): void {
    const url = this.$router.serializeUrl(
      this.$router.createUrlTree(['opportunity', id])
    );
    window.open(url, '_blank');
  }

  public handleOpportunityAdded(): void {
    this.getOpportunities(this.selectedFilter);
    this.addSlider.close();
  }

  private getAccountEmails(): void {
    this.personService.getAndMapAccountEmails(this.componentDestroyed)
      .subscribe(
        (users) => (this.userMap = users),
        () => this.notificationService.notify('opportunity.form.notification.accountEmailsError', NotificationType.ERROR)
      );
  }

  public toggleOpportunity(
    $event: MatCheckboxChange,
    opportunity: OpportunityOnList
  ): void {
    if ($event.checked) {
      this.selectedOpportunities.push(opportunity);
    } else {
      const index = this.selectedOpportunities.indexOf(opportunity);
      if (index !== -1) {
        this.selectedOpportunities.splice(index, 1);
      }
    }
  }

  public onSortChange($event: Sort): void {
    this.currentSort = $event.direction
      ? $event.active + ',' + $event.direction
      : 'updated,desc';
    this.getOpportunities(this.selectedFilter);
  }

  private handleOpportunitiesSuccess(page: any): void {
    this.totalElements = page.totalElements;
    this.opportunityPage = page;
  }

  public getOpportunityProgress(opportunity: OpportunityOnList): number {
    return opportunity.maxStatus
      ? (opportunity.status / opportunity.maxStatus) * 100
      : 0;
  }

  public isSelected(selection: FilterStrategy): boolean {
    return this.selectedFilter === selection;
  }

  private getFilteredColumns(): string[] {
    return this.columns.filter(
      (item) =>
        !this.authService.isPermissionPresent(
          'OpportunityListComponent.' +
            item.replace('customer.name', 'customerName') +
            'Column',
          'i'
        ) ||
        item === 'actions' ||
        item === 'selectRow' ||
        item === 'external'
    );
  }

  public open(): void {
    this.initOpportunityDefaultValues = true;
    setTimeout(() => (this.initOpportunityDefaultValues = null), 500);
    this.addSlider.open();
  }

  public expand(): void {
    this.expanded = true;
    this.kanbanVisible = false;
    this.viewType = ViewType.LIST;
  }

  public collapse(): void {
    this.expanded = false;
    this.kanbanVisible = false;
    this.viewType = ViewType.LIST;
  }

  public showCanban(): void {
    this.kanbanVisible = !this.kanbanVisible;
    if (this.kanbanVisible) {
      this.viewType = ViewType.KANBAN;
    } else {
      this.viewType = ViewType.LIST;
    }
  }

  // --------------------------

  public tryDeleteOpportunity(opportunity: OpportunityOnList): void {
    const dialogData = this.dialogsConfig.deleteOpportunity(
      opportunity,
      this.deleteOpportunity.bind(this)
    );

    this.openDeleteDialog(dialogData);
  }

  public tryCopyOpportunity(opportunity: OpportunityOnList): void {
    const dialogData = this.dialogsConfig.copyOpportunity(
      opportunity,
      this.copyOpportunity.bind(this)
    );

    this.openDeleteDialog(dialogData);
  }

  public tryCloseOpportunity(
    opportunity: OpportunityOnList,
    finishResult: FinishOpportunityResult
  ): void {
    const dialogData = this.dialogsConfig.closeOpportunity(
      opportunity,
      finishResult,
      this.finishOpportunityByType.bind(this)
    );

    this.openDeleteDialog(dialogData);
  }

  private copyOpportunity(opportunityId: number): void {
    this.opportunityService
      .copyOpportunity(opportunityId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.getOpportunities(this.selectedFilter);
          this.notificationService.notify(
            'opportunity.list.notification.copySuccess',
            NotificationType.SUCCESS
          );
        },
        (err) =>
          this.notificationService.notifyError(
            this.opportunityErrorTypes,
            err.status
          )
      );
  }

  private finishOpportunityByType(
    opportunityId: number,
    finishBody: OpportunityFinishDto
  ): void {
    if (finishBody.finishResult === FinishOpportunityResult.SUCCESS) {
      this.closeOpportunity(opportunityId, finishBody);
    } else {
      const dialogData = this.dialogsConfig.rejectionReason((rejectionReason) =>
        this.closeOpportunity(opportunityId, {
          ...finishBody,
          rejectionReason,
        })
      );

      this.openDeleteDialog(dialogData);
    }
  }

  private closeOpportunity(
    opportunityId: number,
    finishBody: OpportunityFinishDto
  ): void {
    this.opportunityService
      .finishOpportunity(opportunityId, finishBody)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.getOpportunities(this.selectedFilter);
          this.notificationService.notify(
            'opportunity.list.notification.finishSuccess',
            NotificationType.SUCCESS
          );
        },
        (err) =>
          this.notificationService.notifyError(
            this.opportunityErrorTypes,
            err.status
          )
      );
  }

  private deleteOpportunity(opportunityId: number): void {
    this.opportunityService
      .removeOpportunity(opportunityId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.getOpportunities(this.selectedFilter);
          this.notificationService.notify(
            'opportunity.list.notification.deleteSuccess',
            NotificationType.SUCCESS
          );
        },
        (err) =>
          this.notificationService.notifyError(
            this.opportunityErrorTypes,
            err.status
          )
      );
  }

  public goToTask(opportunity: OpportunityOnList, isComplete: boolean): void {
    if (isComplete) {
      this.completeTask(opportunity.processInstanceId);
    } else {
      this.goBack(opportunity.processInstanceId);
    }
  }

  private goBack(processInstanceId: string): void {
    this.opportunityService
      .goBack(processInstanceId, 1)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.getOpportunities(this.selectedFilter);
      });
  }

  private completeTask(processInstanceId: string): void {
    this.opportunityService
      .completeTask(processInstanceId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => {
        this.getOpportunities(this.selectedFilter);
      });
  }
}

