import {NgModule} from '@angular/core';
import {OpportunityListComponent} from './opportunity-list/opportunity-list.component';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {OpportunityRoutingModule} from './opportunity-routing.module';
import {OpportunityDetailsComponent} from './opportunity-details/opportunity-details.component';
import {NgxMaskModule} from 'ngx-mask';
import {WidgetModule} from '../widgets/widget.module';
import {TabsModule} from '../shared/tabs/tabs.module';
import {DocumentModule} from '../documents/document.module';
import {OpportunityDetailsCardComponent} from './opportunity-details/opportunity-details-card/opportunity-details-card.component';
import {OpportunityProductsCardComponent} from './opportunity-details/opportunity-products-card/opportunity-products-card.component';
import {ManualProductAddComponent} from './opportunity-details/opportunity-products-card/manual-product-add/manual-product-add.component';
import {OpportunityProductEditComponent} from './opportunity-details/opportunity-products-card/opportunity-product-edit/opportunity-product-edit.component';
import {CategoriesModule} from '../product/categories/categories.module';
import {AddProductFromPriceBookComponent} from './opportunity-details/opportunity-products-card/add-product-from-price-book/add-product-from-price-book.component';
import {OpportunitySliderModule} from './opportunity-slider/opportunity-slider.module';
import {CustomerSliderModule} from '../customer/customer-slider/customer-slider.module';
import {ExpensesModule} from '../shared/tabs/expense-tab/expenses.module';
import {OfferModule} from '../offer/offer.module';
import {ContactSliderModule} from '../contacts/contact-slider/contact-slider.module';
import {DictionaryModule} from '../dictionary/dictionary.module';
import {OpportunitySearchComponent} from './opportunity-list/opportunity-search/opportunity-search.component';
import {OpportunityKanbanComponent} from './opportunity-kanban/opportunity-kanban.component';
import {OpportunityAcceptanceComponent} from './opportunity-details/opportunity-acceptance/opportunity-acceptance.component';
import {OpportunityTasksComponent} from './opportunity-details/opportunity-tasks/opportunity-tasks.component';
import {TaskTooltipModule} from '../shared/task-tooltip/task-tooltip.module';
import {OpportunityTabsComponent} from './opportunity-details/opportunity-tabs/opportunity-tabs.component';
import {OpportunityDetailsPropertiesComponent} from './opportunity-details/opportunity-details-card/opportunity-details-properties/opportunity-details-properties.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {TooltipModule} from 'ngx-bootstrap';
import {OpportunityListActionsComponent} from './opportunity-list/opportunity-list-actions/opportunity-list-actions.component';
import {MatRadioModule} from '@angular/material/radio';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateUtil} from '../shared/util/date.util';
import {OpportunityKanbanActionsComponent} from "./opportunity-kanban/opportunity-kanban-actions/opportunity-kanban-actions.component";
import {TaskSliderModule} from "../calendar/task-slider/task-slider.module";
import {OpportunityKanbanElementComponent} from "./opportunity-kanban/opportunity-kanban-element/opportunity-kanban-element.component";

@NgModule({
  declarations: [
    OpportunityListComponent,
    OpportunityListActionsComponent,
    OpportunityDetailsComponent,
    OpportunityDetailsCardComponent,
    OpportunityProductsCardComponent,
    ManualProductAddComponent,
    AddProductFromPriceBookComponent,
    OpportunityProductEditComponent,
    OpportunitySearchComponent,
    OpportunityKanbanComponent,
    OpportunityKanbanActionsComponent,
    OpportunityKanbanElementComponent,
    OpportunityAcceptanceComponent,
    OpportunityTasksComponent,
    OpportunityTabsComponent,
    OpportunityDetailsPropertiesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    OpportunityRoutingModule,
    WidgetModule,
    TabsModule,
    NgxMaskModule,
    DocumentModule,
    CategoriesModule,
    OpportunitySliderModule,
    CustomerSliderModule,
    ExpensesModule,
    OfferModule,
    ContactSliderModule,
    DictionaryModule,
    TaskTooltipModule,
    MatTooltipModule,
    TooltipModule.forRoot(),
    MatRadioModule,
    TaskSliderModule,
  ],
  entryComponents: [OpportunityProductEditComponent],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: DateUtil.getInputFormat()}
  ]
})
export class OpportunityModule {
}
