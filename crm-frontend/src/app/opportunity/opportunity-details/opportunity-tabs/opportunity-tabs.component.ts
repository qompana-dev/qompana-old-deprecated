import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {OpportunityDetails} from '../../opportunity.model';

@Component({
  selector: 'app-opportunity-tabs',
  templateUrl: './opportunity-tabs.component.html',
  styleUrls: ['./opportunity-tabs.component.scss']
})
export class OpportunityTabsComponent  {
  @Input() opportunityId: number = null;
  @Input() opportunity: OpportunityDetails = null;
  @Input() opportunityChangedSubscription: Subject<void>;
  @Input() objectType = null;

  @Output() opportunityItemsChanged: EventEmitter<void> = new EventEmitter<void>();

  selectedTabIndex = 0;
  mainCurrency = 'PLN';

  handleOpportunityEdited(): void {
    this.opportunityItemsChanged.emit();
  }
}
