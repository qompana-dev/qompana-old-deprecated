import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {Property} from "../../../opportunity.model";

@Component({
  selector: 'app-opportunity-details-properties',
  templateUrl: './opportunity-details-properties.component.html',
  styleUrls: ['./opportunity-details-properties.component.scss']
})
export class OpportunityDetailsPropertiesComponent {
  @Input() taskOnScreen = null;
  @Input() activeTaskId = null;
  @Input() acceptanceUserId = null;
  @Input() propertyForm: FormGroup;

  @Output() submit: EventEmitter<void> = new EventEmitter<void>();

  isEmpty(property: Property): boolean {
    const value = this.taskOnScreen.properties.find(e => e.id === property.id).value;
    return value === null || value === '';
  }

  isFalse(property: Property): boolean {
    return this.propertyForm.get(property.id) && !this.propertyForm.get(property.id).value;
  }

  cancelChange(id: string): void {
    this.propertyForm.get(id).patchValue(this.taskOnScreen.properties.find(e => e.id === id).value);
  }

  onSubmit(): void {
    this.submit.emit();
  }
}
