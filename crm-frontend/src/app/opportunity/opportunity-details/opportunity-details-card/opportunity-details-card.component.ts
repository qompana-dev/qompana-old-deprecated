import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {CrmObject, CrmObjectType} from '../../../shared/model/search.model';
import {AssignToGroupService} from '../../../shared/assign-to-group/assign-to-group.service';
import {debounceTime, filter, finalize, map, switchMap, takeUntil, tap} from 'rxjs/operators';
import {OpportunityPriority, OpportunitySave} from '../../opportunity.model';
import {Observable, Subject} from 'rxjs';
import {Contact} from '../../../contacts/contacts.model';
import {ContactsService} from '../../../contacts/contacts.service';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {PersonService} from '../../../person/person.service';
import {OpportunityService} from '../../opportunity.service';
import * as cloneDeep from 'lodash/cloneDeep';
import {FormUtil} from '../../../shared/form.util';
import {applyPermission} from '../../../shared/permissions-utils';
import {AuthService} from '../../../login/auth/auth.service';
import {DateUtil} from '../../../shared/util/date.util';
import {GlobalConfigService} from '../../../global-config/global-config.service';
import {CustomerService} from '../../../customer/customer.service';
import {FormValidators} from '../../../shared/form/form-validators';
import {OpportunityFormUtil} from '../../util/opportunity-form.util';
import {Dictionary, DictionaryId, DictionaryWord, SaveDictionaryWord} from '../../../dictionary/dictionary.model';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {DictionaryAddWordComponent} from '../../../dictionary/dictionary-add-word/dictionary-add-word.component';
import {MatOption, MatSelect} from '@angular/material';
import {DictionaryStoreService} from 'src/app/dictionary/dictionary-store.service';
import {AccountEmail} from '../../../person/person.model';
import {Router} from '@angular/router';
import {ProcessDefinition} from '../../../bpmn/bpmn.model';
import {MatSelectChange} from "@angular/material/select";


@Component({
  selector: 'app-opportunity-details-card',
  templateUrl: './opportunity-details-card.component.html',
  styleUrls: ['./opportunity-details-card.component.scss']
})
export class OpportunityDetailsCardComponent implements OnInit, OnDestroy {

  opportunity: OpportunitySave;
  // tslint:disable-next-line:variable-name
  _opportunityId: number;
  // tsuserMaplint:disable-next-line:variable-name
  _usersMap: Map<number, string>;
  opportunityDetailsForm: FormGroup;
  currencies = [];
  contactSearchLoading = false;
  customerSearchLoading = false;
  filteredCustomers: CrmObject[] = [];
  selectedCustomer: CrmObject;
  filteredContacts: CrmObject[] = [];
  contactsByCustomerId: CrmObject[] = [];
  selectedContactInfo: Contact;
  objectType = CrmObjectType;
  formControlFocusedKey: string;
  formControlFocusedValue: string;
  dictionaryId = DictionaryId;
  ownerVisibleCount = 1;
  percentageSumError = false;
  parentWordIdForCreation: number = null;
  private componentDestroyed: Subject<void> = new Subject();
  availableOpportunityPriorities = Object.keys(OpportunityPriority);
  filteredAccounts: Map<number, AccountEmail[]> = new Map<number, AccountEmail[]>();
  accountEmails: AccountEmail[] = [];
  formChanged = false;
  baseOpportunity: OpportunitySave = {} as OpportunitySave;
  @Input() isTaskProperties = false;

  @Output() opportunityChanged: EventEmitter<OpportunitySave> = new EventEmitter<OpportunitySave>();
  @Output() opportunityEdited: EventEmitter<void> = new EventEmitter<void>();
  @Output() refresh: EventEmitter<void> = new EventEmitter<void>();

  @Input() set canSave(canSave: boolean) {
    if (canSave === true) {
      this.saveChanges();
    } else if (canSave === false) {
      this.revertChanges();
    }
  }

  readonly undefinedCategoryId = -1000;
  sourceOfAcquisitionCreation: Subject<void> = new Subject();
  sourceOfAcquisitionIndex = -1;
  allSourceOfAcquisition: DictionaryWord[];
  allSubSourceOfAcquisition: DictionaryWord[];
  allSubjectOfInterest: DictionaryWord[];
  mainProcessDefinitionId: ProcessDefinition[];

  @ViewChild('sourceOfAcquisitionSelect') sourceOfAcquisitionSelect: MatSelect;
  @ViewChild('subSourceOfAcquisitionSelect') subSourceOfAcquisitionSelect: MatSelect;

  @ViewChild('addSourceOfAcquisitionSlider') addSourceOfAcquisitionSlider: SliderComponent;
  @ViewChild('addSourceOfAcquisitionWord') addSourceOfAcquisitionWord: DictionaryAddWordComponent;

  @ViewChild('addSubSourceOfAcquisitionSlider') addSubSourceOfAcquisitionSlider: SliderComponent;
  @ViewChild('addSubSourceOfAcquisitionWord') addSubSourceOfAcquisitionWord: DictionaryAddWordComponent;

  @ViewChild('addSubjectOfInterestSlider') addSubjectOfInterestSlider: SliderComponent;
  @ViewChild('addSubjectOfInterestWord') addSubjectOfInterestWord: DictionaryAddWordComponent;

  @Input() set opportunityId(opportunityId: number) {
    if (opportunityId) {
      this._opportunityId = opportunityId;
      this.checkIfBothValuesPresentAndInitIfYes();
    }
  }

  @Input() set outsideCreateDateValue(outsideCreateDateValue: Date) {
    if (outsideCreateDateValue) {
      this.opportunity.createDate = DateUtil.convertDateToString(outsideCreateDateValue);
    }
  }

  @Input() set usersMap(usersMap: Map<number, string>) {
    if (usersMap) {
      this._usersMap = usersMap;
      this.checkIfBothValuesPresentAndInitIfYes();
    }
  }

  get authKey(): string {
    return 'OpportunityDetailsCardComponent.';
  }

  checkIfBothValuesPresentAndInitIfYes(): void {
    if (this._usersMap && this._opportunityId) {
      this.opportunityService.getOpportunityForEditForDetailsCard(this._opportunityId).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        (opportunity: OpportunitySave) => {
          this.opportunity = opportunity;
          this.baseOpportunity = cloneDeep(opportunity);
          this.setFormValues(opportunity);
          this.setOpportunityOwners();
          this.setCustomerAndContact();
          this.setProcessDefinitionId(opportunity.processDefinitionId);
          this.checkOwnerPercentageSum();
          this.filterSelectedAccounts();
          const privateWordsIds: number[] = [];
          privateWordsIds.push(this.opportunity.sourceOfAcquisition);
          privateWordsIds.push(this.opportunity.subSourceOfAcquisition);
          this.getSourcesOfAcquisitionForMe(privateWordsIds);
          const subjectOfInterestPrivateWordsIds: number[] = [];
          subjectOfInterestPrivateWordsIds.push(this.opportunity.subjectOfInterest);
          this.getSubjectsOfInterestForMe(subjectOfInterestPrivateWordsIds);
        }
      );
    }
  }

  get name(): AbstractControl {
    return this.opportunityDetailsForm.get('name');
  }

  get finishDate(): AbstractControl {
    return this.opportunityDetailsForm.get('finishDate');
  }

  get createDate(): AbstractControl {
    return this.opportunityDetailsForm.get('createDate');
  }

  get createDateDate(): Date {
    if (this.opportunity && this.opportunity.createDate) {
      return new Date(this.opportunity.createDate);
    }
    return null;
  }

  get customerId(): AbstractControl {
    return this.opportunityDetailsForm.get('customerId');
  }

  get customerSearch(): AbstractControl {
    return this.opportunityDetailsForm.get('customerSearch');
  }

  get probability(): AbstractControl {
    return this.opportunityDetailsForm.get('probability');
  }

  get priority(): AbstractControl {
    return this.opportunityDetailsForm.get('priority');
  }

  get contactId(): AbstractControl {
    return this.opportunityDetailsForm.get('contactId');
  }

  get contactSearch(): AbstractControl {
    return this.opportunityDetailsForm.get('contactSearch');
  }

  get amount(): AbstractControl {
    return this.opportunityDetailsForm.get('amount');
  }

  get estimatedAmount(): AbstractControl {
    return this.opportunityDetailsForm.get('estimatedAmount');
  }

  get productAmount(): AbstractControl {
    return this.opportunityDetailsForm.get('productAmount');
  }

  get profitAmount(): AbstractControl {
    return this.opportunityDetailsForm.get('profitAmount');
  }

  get currency(): AbstractControl {
    return this.opportunityDetailsForm.get('currency');
  }

  get description(): AbstractControl {
    return this.opportunityDetailsForm.get('description');
  }

  get createdBy(): AbstractControl {
    return this.opportunityDetailsForm.get('createdBy');
  }

  get sourceOfAcquisition(): AbstractControl {
    return this.opportunityDetailsForm.get('sourceOfAcquisition');
  }

  get subSourceOfAcquisition(): AbstractControl {
    return this.opportunityDetailsForm.get('subSourceOfAcquisition');
  }

  get subjectOfInterest(): AbstractControl {
    return this.opportunityDetailsForm.get('subjectOfInterest');
  }

  get ownerOneId(): AbstractControl {
    return this.opportunityDetailsForm.get('ownerOneId');
  }

  get ownerOneSearch(): AbstractControl {
    return this.opportunityDetailsForm.get('ownerOneSearch');
  }

  get ownerOnePercentage(): AbstractControl {
    return this.opportunityDetailsForm.get('ownerOnePercentage');
  }

  get ownerTwoId(): AbstractControl {
    return this.opportunityDetailsForm.get('ownerTwoId');
  }

  get ownerTwoSearch(): AbstractControl {
    return this.opportunityDetailsForm.get('ownerTwoSearch');
  }

  get ownerTwoPercentage(): AbstractControl {
    return this.opportunityDetailsForm.get('ownerTwoPercentage');
  }

  get ownerThreeId(): AbstractControl {
    return this.opportunityDetailsForm.get('ownerThreeId');
  }

  get ownerThreeSearch(): AbstractControl {
    return this.opportunityDetailsForm.get('ownerThreeSearch');
  }

  get ownerThreePercentage(): AbstractControl {
    return this.opportunityDetailsForm.get('ownerThreePercentage');
  }

  get processDefinitionId(): AbstractControl {
    return this.opportunityDetailsForm.get('processDefinitionId');
  }

  constructor(private $router: Router,
              private fb: FormBuilder,
              private assignToGroupService: AssignToGroupService,
              private contactsService: ContactsService,
              private customerService: CustomerService,
              private personService: PersonService,
              private notificationService: NotificationService,
              private opportunityService: OpportunityService,
              private authService: AuthService,
              private configService: GlobalConfigService,
              private dictionaryStoreService: DictionaryStoreService
              ) {
    this.createOpportunityDetailsForm();
  }

  ngOnInit(): void {
    this.subscribeForCurrencies();
    this.subscribeForCustomerSearch();
    this.subscribeCustomerId();
    this.subscribeContactSearch();
    this.subscribeContactId();
    this.getAccountEmails();
    this.subscribeProbabilityChange();
    this.productAmount.disable();
    this.profitAmount.disable();
    this.processDefinitionId.disable();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  customerSelect(customer: CrmObject): void {
    if (customer && customer.type === CrmObjectType.customer) {
      this.selectedCustomer = customer;
      this.customerId.patchValue(customer.id);
      this.opportunity.customerId = customer.id;
      this.customerSearch.patchValue(customer.label);
      this.opportunityModified();
    }
  }

  contactSelected(contact: CrmObject): void {
    this.contactId.patchValue(contact.id);
    this.opportunity.contactId = contact.id;
    this.contactSearch.patchValue(contact.label);
    if (contact && contact.id) {
      this.opportunityModified();
    }
    this.selectedCustomer = cloneDeep(this.selectedCustomer);
    this.autoSelectCustomer(contact.id);
  }

  formControlFocusIn(formControlName: string): void {
    this.formControlFocusedKey = cloneDeep(formControlName);
    this.formControlFocusedValue = cloneDeep(this.opportunityDetailsForm.get(formControlName).value);
  }

  formControlFocusOut(newValue?: string): void {
    if (this.formControlFocusedKey) {
      const formValue = (!newValue) ? this.opportunityDetailsForm.get(this.formControlFocusedKey).value : newValue;
      if (this.formControlFocusedKey === 'amount') {
        this.opportunityDetailsForm.get(this.formControlFocusedKey).setValue(this.getNumberInFormat(formValue));
      } else {
        this.opportunityDetailsForm.get(this.formControlFocusedKey).setValue(formValue);
      }
      this.opportunity[this.formControlFocusedKey] = formValue;
      const formChanged = formValue !== this.formControlFocusedValue;
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
      if (formChanged) {
        this.opportunityModified();
      }
    }
  }

  formControlEsc(): void {
    if (this.formControlFocusedKey) {
      this.opportunityDetailsForm.get(this.formControlFocusedKey).patchValue(this.formControlFocusedValue);
    }
  }

  changePriority(priority: string): void {
    this.opportunity.priority = OpportunityPriority[priority] || OpportunityPriority.LOW;
  }

  finishDateSelected($event: any): void {
    const finishDateValue = new Date(this.opportunityDetailsForm.get('finishDate').value);
    this.opportunity.finishDate = DateUtil.convertDateToString(finishDateValue);
    this.opportunityModified();
  }

  private saveChanges(): void {
    if (this.opportunityDetailsForm.valid) {
      this.opportunityService.updateOpportunityFromDetailsCard(this.opportunity.id, this.opportunity).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        () => {
          this.baseOpportunity = cloneDeep(this.opportunity);
          this.notificationService.notify('opportunity.details.card.successWhileUpdate', NotificationType.SUCCESS);
          const privateWordsIds: number[] = [];
          privateWordsIds.push(this.opportunity.sourceOfAcquisition);
          privateWordsIds.push(this.opportunity.subSourceOfAcquisition);
          this.getSourcesOfAcquisitionForMe(privateWordsIds);
          const subjectOfInterestPrivateWordsIds: number[] = [];
          subjectOfInterestPrivateWordsIds.push(this.opportunity.subjectOfInterest);
          this.getSubjectsOfInterestForMe(subjectOfInterestPrivateWordsIds);
          this.refresh.emit();
          this.opportunityEdited.emit();
        },
        (err) => this.notificationService.notify('opportunity.details.card.errorWhileUpdate', NotificationType.ERROR)
      );
    } else {
      FormUtil.setTouched(this.opportunityDetailsForm);
    }
  }

  revertChanges(): void {
    this.opportunity = this.baseOpportunity;
    this.setFormValues(this.opportunity);
    this.setOpportunityOwners();
    this.setCustomerAndContact();
    this.formChanged = false;

    const privateWordsIds: number[] = [];
    privateWordsIds.push(this.opportunity.sourceOfAcquisition);
    privateWordsIds.push(this.opportunity.subSourceOfAcquisition);
    this.getSourcesOfAcquisitionForMe(privateWordsIds);
  }

  public contactToCrmObject(contact: Contact | { id: number, name: string, surname: string }): CrmObject {
    return {
      id: contact.id,
      type: CrmObjectType.contact,
      label: `${contact.name} ${contact.surname}`
    };
  }

  private opportunityModified(): void {
    this.opportunityChanged.emit(this.opportunity);
  }

  private updateSaveOpportunityFromForm(): void {
    this.opportunity.sourceOfAcquisition = this.getSelectedValue(this.sourceOfAcquisition.value);
    this.opportunity.subSourceOfAcquisition = this.getSelectedValue(this.subSourceOfAcquisition.value);
  }

  private subscribeContactSearch(): void {
    this.contactSearch.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed),
        debounceTime(250),
        filter(() => this.customerId && this.customerId.value && this.customerSearch.value === this.selectedCustomer)
      ).subscribe((value: string) => {
      if (!value || value === '') {
        this.filteredContacts = this.contactsByCustomerId;
      } else {
        this.filteredContacts = this.contactsByCustomerId
          .filter((contact: CrmObject) => {
            const fullName = contact.label;
            return fullName.toLowerCase().indexOf(value.toLowerCase()) !== -1;
          });
      }
    });
    this.contactSearch.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed),
        debounceTime(250),
        filter(() => !(this.customerId && this.customerId.value && this.customerSearch.value === this.selectedCustomer)),
        filter((input: string) => !!input && input.length >= 3),
        tap(() => this.contactSearchLoading = true),
        switchMap(((input: string) => this.assignToGroupService.searchForType(CrmObjectType.contact, input, [CrmObjectType.customer])
            .pipe(finalize(() => this.contactSearchLoading = false))
        ))).subscribe(
      (result: CrmObject[]) => {
        this.filteredContacts = result;
      }
    );
  }

  private autoSelectCustomer(contactId: number): void {
    this.customerService.getCustomerByContactId(contactId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(customer => this.customerSelect(customer));
  }

  private subscribeCustomerId(): void {
    this.getContactsForCustomer(this.customerId.value);

    this.customerId.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed),
        filter((value) => !!value),
        tap(() => {
          this.contactsByCustomerId = [];
          this.filteredContacts = [];
        })
      ).subscribe((value) => {
      this.getContactsForCustomer(value);
    });
  }
  private subscribeContactId(): void {
    this.getContactInfoById(this.contactId.value);
    this.contactId.valueChanges
      .pipe(takeUntil(this.componentDestroyed),
        filter((value) => !!value))
      .subscribe((value) => {
        this.getContactInfoById(value);
      });
  }

  getContactsForCustomer(customerId: number): void {
    if (customerId) {
      this.contactsService.getCustomerContacts(customerId)
        .pipe(
          takeUntil(this.componentDestroyed),
          map((contacts: Contact[]): CrmObject[] => contacts.map(c => this.contactToCrmObject(c)))
        ).subscribe((contacts) => {
        this.contactsByCustomerId = contacts;
        this.filteredContacts = contacts;
      });
    }
  }

  private subscribeForCustomerSearch(): void {
    this.customerSearch.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      filter((input: string) => !!input && input.length >= 3),
      tap(() => this.customerSearchLoading = true),
      switchMap(((input: string) => this.assignToGroupService.searchForType(CrmObjectType.customer, input, [CrmObjectType.customer])
          .pipe(finalize(() => this.customerSearchLoading = false))
      ))).subscribe(
      (result: CrmObject[]) => {
        this.filteredCustomers = result;
      }
    );
  }

  private setCustomerAndContact(): void {
    if (this.opportunity && this.opportunity.customerId) {
      this.setLabelById(this.customerSearch, CrmObjectType.customer, this.customerId.value)
        .subscribe((response: CrmObject[]) => {
          if (response && response.length > 0) {
            this.selectedCustomer = response[0];
          }
          if (this.opportunity.contactId) {
            this.contactId.patchValue(this.opportunity.contactId);
            this.setLabelById(this.contactSearch, CrmObjectType.contact, this.contactId.value)
              .subscribe();
          }
        });
    }
  }

  private setLabelById(controller: AbstractControl, type: CrmObjectType, id: number): Observable<any> {
    return this.assignToGroupService.updateLabels([{type, id}])
      .pipe(takeUntil(this.componentDestroyed),
        tap((response: CrmObject[]) => {
          if (response && response.length > 0) {
            controller.patchValue(response[0].label);
          }
        }));
  }

  private setFormValues(opportunity: OpportunitySave): void {
    this.opportunityDetailsForm.reset(opportunity);
    this.sanitizeFrom();
    const creator = this._usersMap.get(opportunity.creator);
    this.amount.patchValue(
      (opportunity.amount !== undefined && opportunity.amount !== null) ? this.getNumberInFormat(opportunity.amount) : undefined);
    (opportunity.canEditAmount) ? this.currency.enable() : this.currency.disable();
    this.createdBy.patchValue(creator);
    this.createDate.patchValue(DateUtil.formatDate(opportunity.createDate, true));
  }

  private getNumberInFormat(value: number): string {
    return String(value).replace('.', ',');
  }

  private createOpportunityDetailsForm(): void {
    this.opportunityDetailsForm = this.fb.group({
      name: [null, [Validators.required, FormValidators.whitespace, Validators.maxLength(200)]],
      finishDate: [null, []],
      priority: [null],
      customerId: [null, [Validators.required]],
      customerSearch: [null, Validators.required],
      probability: [null, Validators.compose([Validators.min(0), Validators.max(100)])],
      contactId: [null, [Validators.required]],
      contactSearch: [null, Validators.required],
      amount: [null, [Validators.maxLength(18)]],
      estimatedAmount: [null, [Validators.maxLength(18)]],
      productAmount: [{value: null, disabled: true}],
      profitAmount: [{value: null, disabled: true}],
      processDefinitionId: [{value: null, disabled: true}],
      currency: [null],
      description: [null, [Validators.maxLength(3000)]],
      createdBy: [{value: null, disabled: true}],
      createDate: [null, [Validators.required]],
      sourceOfAcquisition: [this.undefinedCategoryId],
      subSourceOfAcquisition: [this.undefinedCategoryId],
      subjectOfInterest: [this.undefinedCategoryId],
      ownerOneId: [null, [Validators.required]],
      ownerOnePercentage: [null, [Validators.min(0), Validators.max(100), Validators.required]],
      ownerOneSearch: [null
        // [Validators.required, ((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequiredForSearch(control, 1))]
      ],
      ownerTwoId: [null, [((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequired(control, 2))]],
      ownerTwoPercentage: [null, [Validators.min(0), Validators.max(100),
        ((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequired(control, 2))]],
      ownerTwoSearch: [null,
        // [((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequiredForSearch(control, 2))]
      ],
      ownerThreeId: [null, [((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequired(control, 3))]],
      ownerThreePercentage: [null, [Validators.min(0), Validators.max(100),
        ((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequired(control, 3))]],
      ownerThreeSearch: [null,
        // [((control: AbstractControl): ValidationErrors => this.checkIfOwnerRequiredForSearch(control, 3))]
      ],
    });

    this.applyPermissionsToForm();
  }

  private applyPermissionsToForm(): void {
    if (this.opportunityDetailsForm) {
      applyPermission(this.name, !this.authService.isPermissionPresent(`${this.authKey}name`, 'r'));
      applyPermission(this.createDate, !this.authService.isPermissionPresent(`${this.authKey}createDate`, 'r'));
      applyPermission(this.finishDate, !this.authService.isPermissionPresent(`${this.authKey}finishDate`, 'r'));
      applyPermission(this.priority, !this.authService.isPermissionPresent(`${this.authKey}priority`, 'r'));
      applyPermission(this.customerSearch, !this.authService.isPermissionPresent(`${this.authKey}customer`, 'r'));
      applyPermission(this.probability, !this.authService.isPermissionPresent(`${this.authKey}probability`, 'r'));
      applyPermission(this.contactSearch, !this.authService.isPermissionPresent(`${this.authKey}contact`, 'r'));
      applyPermission(this.amount, !this.authService.isPermissionPresent(`${this.authKey}amount`, 'r'));
      applyPermission(this.estimatedAmount, !this.authService.isPermissionPresent(`${this.authKey}estimatedAmount`, 'r'));
      applyPermission(this.productAmount, !this.authService.isPermissionPresent(`${this.authKey}productAmount`, 'r'));
      applyPermission(this.profitAmount, !this.authService.isPermissionPresent(`${this.authKey}profitAmount`, 'r'));
      applyPermission(this.currency, !this.authService.isPermissionPresent(`${this.authKey}currency`, 'r'));
      applyPermission(this.description, !this.authService.isPermissionPresent(`${this.authKey}description`, 'r'));
      applyPermission(this.sourceOfAcquisition, !this.authService.isPermissionPresent(`${this.authKey}sourceOfAcquisition`, 'r'));
      applyPermission(this.subSourceOfAcquisition, !this.authService.isPermissionPresent(`${this.authKey}sourceOfAcquisition`, 'r'));
      applyPermission(this.subjectOfInterest, !this.authService.isPermissionPresent(`${this.authKey}subjectOfInterest`, 'r'));
      applyPermission(this.ownerOneId, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerOnePercentage, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerOneSearch, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerTwoId, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerTwoPercentage, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerTwoSearch, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerThreeId, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerThreePercentage, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.ownerThreeSearch, !this.authService.isPermissionPresent(`${this.authKey}ownerField`, 'r'));
      applyPermission(this.processDefinitionId, !this.authService.isPermissionPresent(`${this.authKey}processDefinitionField`, 'r'));
    }
  }
  private getAccountEmails(): void {
    this.personService.getAccountEmails()
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      emails => {
        this.accountEmails = emails;
        this.filteredAccounts.set(1, emails);
        this.filteredAccounts.set(2, emails);
        this.filteredAccounts.set(3, emails);

        this.setOpportunityOwners();
      },
      err => this.notificationService.notify('opportunity.form.notification.accountEmailsError', NotificationType.ERROR)
    );
  }
  private checkIfOwnerRequiredForSearch(control: AbstractControl, index: number): ValidationErrors {
    if (this.ownerVisibleCount >= index && (!control.value || control.value === '')) {
      return {required: true};
    }
    if (this.getOwnerIdByIndex(index)) {
      this.getOwnerIdByIndex(index).markAsTouched();
      return this.getOwnerIdByIndex(index).hasError('required') ? {required: true} : null;
    }
  }

  private checkIfOwnerRequired(control: AbstractControl, index: number): ValidationErrors {
    return (this.ownerVisibleCount >= index && (!control.value)) ? {required: true} : null;
  }

  private getOwnerIdByIndex(index: number): AbstractControl {
    return this.getOwnerByIndexAndName(index, 'Id');
  }

  getOwnerSearchByIndex(index: number): AbstractControl {
    return this.getOwnerByIndexAndName(index, 'Search');
  }

  getOwnerPercentageByIndex(index: number): AbstractControl {
    return this.getOwnerByIndexAndName(index, 'Percentage');
  }

  private getOwnerByIndexAndName(index: number, name: string): AbstractControl {
    if (this.opportunityDetailsForm) {
      return this.opportunityDetailsForm.get(this.getOwnerFormControlNameByIndexAndName(index, name));
    }
    return undefined;
  }

  getOwnerFormControlNameByIndexAndName(index: number, name: string): string {
    const indexName = [{id: 1, value: 'One'}, {id: 2, value: 'Two'}, {id: 3, value: 'Three'}]
      .find((obj) => obj.id === index);
    return 'owner' + indexName.value + name;
  }

  ownerSelect(matOption: MatOption, index: number): void {
    const ownerId = this.getOwnerFormControlNameByIndexAndName(index, 'Id');
    this.opportunity[ownerId] = matOption.value.id;
    this.filterSelectedAccounts();
    this.setOpportunityOwners();
    this.opportunityModified();
  }

  ownerPercentageSelect(matSelectChange: MatSelectChange, index: number): void {
    const ownerPercentage = this.getOwnerFormControlNameByIndexAndName(index, 'Percentage');
    this.opportunity[ownerPercentage] = matSelectChange.value;
    this.checkOwnerPercentageSum();
    this.opportunityModified();
  }

  private checkOwnerPercentageSum(): void {
    let sum = 0;
    for (let i = 1; i <= this.ownerVisibleCount; i++) {
      const percent = this.getOwnerPercentageByIndex(i).value;
      if (percent) {
        sum += +percent;
      }
    }
    this.percentageSumError = sum > 100;
  }

  removeOwner(index: number): void {
    this.clearOwnerByIndex(index);
    for (let i = index + 1; i <= 3; i++) {
      this.moveOwnerToPreviousPlace(i);
    }
    this.ownerVisibleCount--;

    for (let i = this.ownerVisibleCount + 1; i <= 3; i++) {
      this.getOwnerIdByIndex(index).updateValueAndValidity();
      this.getOwnerSearchByIndex(index).updateValueAndValidity();
      this.getOwnerPercentageByIndex(index).updateValueAndValidity();
    }
    this.filterSelectedAccounts();
  }

  addNewOwner(): void {
    if (this.ownerVisibleCount < 3) {
      this.ownerVisibleCount++;
      this.filterSelectedAccounts();
    }
  }

  clearOwnerByIndex(index: number): void {
    this.getOwnerIdByIndex(index).patchValue(undefined);
    this.getOwnerSearchByIndex(index).patchValue(undefined);
    this.getOwnerPercentageByIndex(index).patchValue(undefined);
  }

  moveOwnerToPreviousPlace(index: number): void {
    if (index > 1 && index <= 3 && this.ownerVisibleCount >= index) {
      this.getOwnerIdByIndex(index - 1).patchValue(cloneDeep(this.getOwnerIdByIndex(index).value));
      this.getOwnerSearchByIndex(index - 1).patchValue(cloneDeep(this.getOwnerSearchByIndex(index).value));
      this.getOwnerPercentageByIndex(index - 1).patchValue(cloneDeep(this.getOwnerPercentageByIndex(index).value));
      this.clearOwnerByIndex(index);
    }
  }

  private filterSelectedAccounts(): void {
    const ownerOneIdValue = this.ownerOneId.value;
    const ownerTwoIdValue = this.ownerTwoId.value;
    const ownerThreeIdValue = this.ownerThreeId.value;
    if (this.filteredAccounts.get(1) && ownerOneIdValue) {
      this.filteredAccounts.set(1, this.filteredAccounts.get(1).filter(account => account.id !== ownerOneIdValue));
    }
    if (this.filteredAccounts.get(2) && ownerTwoIdValue) {
      this.filteredAccounts.set(2, this.filteredAccounts.get(2).filter(account => account.id !== ownerTwoIdValue));
    }
    if (this.filteredAccounts.get(3) && ownerThreeIdValue) {
      this.filteredAccounts.set(3, this.filteredAccounts.get(3).filter(account => account.id !== ownerThreeIdValue));
    }
  }
  private setOpportunityOwners(): void {
    if (this.opportunity) {
      this.ownerVisibleCount = 0;
      this.setOwner(1, this.opportunity.ownerOneId, this.opportunity.ownerOnePercentage);
      this.setOwner(2, this.opportunity.ownerTwoId, this.opportunity.ownerTwoPercentage);
      this.setOwner(3, this.opportunity.ownerThreeId, this.opportunity.ownerThreePercentage);
      if (this.ownerVisibleCount === 0) {
        this.ownerVisibleCount = 1;
      }
    }
  }
  private setOwner(index: number, ownerId: number, percentage: number): void {
    if (ownerId) {
      this.ownerVisibleCount++;
      const account: AccountEmail = this.accountEmails.find(obj => obj.id === ownerId);
      if (account) {
        this.getOwnerIdByIndex(index).patchValue(ownerId);
        this.getOwnerSearchByIndex(index).patchValue(account.name + ' ' + account.surname);
      }
      this.getOwnerPercentageByIndex(index).patchValue(percentage);
    }
  }
  private subscribeProbabilityChange(): void {
    this.probability.valueChanges
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((value: string) => {
        OpportunityFormUtil.probabilityChange(this.probability, value);
      });
  }

  private subscribeForCurrencies(): void {
    this.configService.getGlobalParamValue('currencies').pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (param) => this.currencies = param.value.split(';'),
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }

  private sanitizeFrom(): void {
    this.sourceOfAcquisition.setValue(this.setSelectedValue(this.sourceOfAcquisition.value));
    this.subSourceOfAcquisition.setValue(this.setSelectedValue(this.subSourceOfAcquisition.value));
    this.subjectOfInterest.setValue(this.setSelectedValue(this.subjectOfInterest.value));
  }

  getSubSourceOfAcquisition(): void {
    const selected = this.sourceOfAcquisition.value;

    if (!selected || selected === this.undefinedCategoryId) {
      this.subSourceOfAcquisition.disable();
      this.subSourceOfAcquisition.patchValue(this.undefinedCategoryId);
      this.opportunity.subSourceOfAcquisition = null;
      return;
    }

    this.subSourceOfAcquisition.enable();
    this.allSubSourceOfAcquisition = this.allSourceOfAcquisition.filter(word => word.id === selected);

    if (!this.allSubSourceOfAcquisition[0] || this.allSubSourceOfAcquisition[0].children.length === 0) {
      this.subSourceOfAcquisition.patchValue(this.undefinedCategoryId);
      this.opportunity.subSourceOfAcquisition = null;
    }
  }

  private getSourcesOfAcquisitionForMe(privateWordsIds: number[]): void {
    this.dictionaryStoreService.getDictionaryWordsById(DictionaryId.SOURCE_OF_ACQUISITION, privateWordsIds).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      words => {
        this.allSourceOfAcquisition = words;
        this.getSubSourceOfAcquisition();
      },
      err => this.notificationService.notify('opportunity.form.notifications.getSourcesOfAcquisitionError', NotificationType.ERROR)
    );
  }
  private setProcessDefinitionId(processDefinitionId: string): void {
      this.opportunityService.getProcessDefinition(processDefinitionId).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        selectedProcessDefinition  => {
          this.mainProcessDefinitionId = [selectedProcessDefinition];
        },
        err => this.notificationService.notify('opportunity.form.notifications.getMainProcessError', NotificationType.ERROR)
      );
  }

  private getSubjectsOfInterestForMe(privateWordsIds: number[]): void {
    this.dictionaryStoreService
      .getDictionaryWordsById(DictionaryId.SUBJECT_OF_INTEREST, privateWordsIds)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (words) => {
          this.allSubjectOfInterest = words;
        },
        (err) =>
          this.notificationService.notify(
            'leads.form.notifications.getSubjectsOfInterestError',
            NotificationType.ERROR
          )
      );
  }
  private getContactInfoById(id: number): void {
    if (id === null) { return; }
    this.contactsService.getContact(id, 'contactInfo')
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (contact: Contact) => {
            this.selectedContactInfo = contact;
          },
          (err) =>
            this.notificationService.notify(
              'contacts.contactInfo.unableDownloadingContact',
              NotificationType.ERROR
            )
        );
  }

  private getWords(dictionary: Dictionary): DictionaryWord[] {
    if (dictionary.words === undefined || dictionary.words === null) {
      return [];
    }
    return dictionary.words;
  }

  private getSelectedValue(value: number): number {
    if (value !== this.undefinedCategoryId) {
      return value;
    }
    return null;
  }

  private setSelectedValue(value: number): number {
    if (value === null || value === undefined) {
      return this.undefinedCategoryId;
    }
    return value;
  }

  formControlFocusOutSourceOfAcquisition(newValue?: number): void {
    if (this.formControlFocusedKey) {
      const formValue = (!newValue) ? this.opportunityDetailsForm.get(this.formControlFocusedKey).value : newValue;
      this.opportunityDetailsForm.get(this.formControlFocusedKey).setValue(formValue);
      this.opportunity[this.formControlFocusedKey] = this.getSelectedValue(formValue);
      const formChanged = formValue !== this.formControlFocusedValue;
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
      if (formChanged) {
        this.opportunityChanged.emit(this.opportunity);
      }

      this.getSubSourceOfAcquisition();
      this.formControlFocusIn('subSourceOfAcquisition');
      this.formControlFocusOutSubSourceOfAcquisition(this.undefinedCategoryId);
    }
  }

  formControlFocusOutSubSourceOfAcquisition(newValue?: number): void {
    if (this.formControlFocusedKey) {
      const formValue = (!newValue) ? this.opportunityDetailsForm.get(this.formControlFocusedKey).value : newValue;
      this.opportunityDetailsForm.get(this.formControlFocusedKey).setValue(formValue);
      this.opportunity[this.formControlFocusedKey] = this.getSelectedValue(formValue);
      const formChanged = formValue !== this.formControlFocusedValue;
      this.formControlFocusedKey = undefined;
      this.formControlFocusedValue = undefined;
      if (formChanged) {
        this.opportunityChanged.emit(this.opportunity);
      }
    }
  }

  openAddSourceOfAcquisitionSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.addSourceOfAcquisitionSlider.open();
  }

  onCancelAddSourceOfAcquisitionSlider(): void {
    this.addSourceOfAcquisitionSlider.close();
  }

  onSubmitAddSourceOfAcquisitionSlider(savedWord: SaveDictionaryWord): void {
    this.allSourceOfAcquisition.unshift({...savedWord, children: []});
    this.sourceOfAcquisition.patchValue(savedWord.id);
    this.opportunity.sourceOfAcquisition = savedWord.id;
    this.getSubSourceOfAcquisition();
    this.addSourceOfAcquisitionSlider.close();
    this.opportunityChanged.emit(this.opportunity);
  }

  openAddSubSourceOfAcquisitionSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.parentWordIdForCreation = this.sourceOfAcquisition.value;
    this.addSubSourceOfAcquisitionSlider.open();
  }

  onCancelAddSubSourceOfAcquisitionSlider(): void {
    this.addSubSourceOfAcquisitionSlider.close();
  }

  onSubmitAddSubSourceOfAcquisitionSlider(savedWord: SaveDictionaryWord): void {
    const parentWord = this.getSelectedSourceOfAcquisition();
    parentWord.children.unshift({...savedWord, children: []});
    this.getSubSourceOfAcquisition();
    this.subSourceOfAcquisition.patchValue(savedWord.id);
    this.parentWordIdForCreation = null;
    this.opportunity.subSourceOfAcquisition = savedWord.id;
    this.opportunityChanged.emit(this.opportunity);
    this.addSubSourceOfAcquisitionSlider.close();
  }

  private getSelectedSourceOfAcquisition(): DictionaryWord {
    return this.allSourceOfAcquisition.find(word => word.id === this.sourceOfAcquisition.value);
  }

  openAddSubjectOfInterestSlider(matSelect: MatSelect): void {
    matSelect.close();
    this.addSubjectOfInterestSlider.open();
  }

  onCancelAddSubjectOfInterestSlider(): void {
    this.addSubjectOfInterestSlider.close();
  }

  onSubmitAddSubjectOfInterestSlider(savedWord: SaveDictionaryWord): void {
    this.allSubjectOfInterest.unshift({...savedWord, children: []});
    this.subjectOfInterest.patchValue(savedWord.id);
    this.addSubjectOfInterestSlider.close();
  }

  openCustomerPage($event: MouseEvent): void {
    const url = this.$router.serializeUrl(
      this.$router.createUrlTree(['customers', this.customerId.value])
    );
    window.open(url, '_blank');
  }

  openContactPage($event: MouseEvent): void {
    const url = this.$router.serializeUrl(
      this.$router.createUrlTree(['contacts', this.contactId.value])
    );
    window.open(url, '_blank');
  }
}
