import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {BpmnUtil} from '../../../bpmn/bpmn.util';
import {FinishOpportunityResult, OpportunityDetails, OpportunityRejectionReason, Task} from '../../opportunity.model';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {SelectItemDialogService} from '../../../shared/dialog/select-item-dialog/select-item-dialog.service';
import {OpportunityService} from '../../opportunity.service';
import {StatusAcceptance} from '../../../bpmn/bpmn.model';
import {TaskTooltipData, TaskTooltipDataFactory} from '../../../shared/task-tooltip/task-tooltip.model';
import {takeUntil} from 'rxjs/operators';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import {SelectItemDialogData} from '../../../shared/dialog/select-item-dialog/select-item-dialog.component';

@Component({
  selector: 'app-opportunity-tasks',
  templateUrl: './opportunity-tasks.component.html',
  styleUrls: ['./opportunity-tasks.component.scss']
})
export class OpportunityTasksComponent implements OnInit, OnDestroy {
  @Input() userMap: Map<number, string>;
  @Input() opportunity: OpportunityDetails = null;
  @Input() opportunityId: number = null;
  @Input() activeTask: Task = null;
  @Input() taskOnScreen: Task = null;
  @Input() propertyForm = null;
  @Output() getOpportunity = new EventEmitter<string | number>();
  @Output() openLeadConversionSlider = new EventEmitter<void>();
  @Output() taskClicked = new EventEmitter<Task>();

  private componentDestroyed: Subject<void> = new Subject();
  scrollAtTheEnd: boolean;
  bpmnUtil = BpmnUtil;

  readonly tooltipPosition = {
    offsetY: -8,
  };
  private readonly goBackDialogData: Partial<DeleteDialogData> = {
    title: 'opportunity.form.dialog.goBack.title',
    description: 'opportunity.form.dialog.goBack.description',
    noteFirst: 'opportunity.form.dialog.goBack.noteFirstPart',
    noteSecond: 'opportunity.form.dialog.goBack.noteSecondPart',
    confirmButton: 'opportunity.form.dialog.goBack.confirm',
    cancelButton: 'opportunity.form.dialog.goBack.cancel',
  };

  private readonly finishOpportunityDialogData: Partial<SelectItemDialogData> = {
    title: 'opportunity.form.dialog.finishOpportunity.title',
    items: [
      {
        title: 'opportunity.form.dialog.finishOpportunity.item.success',
        value: FinishOpportunityResult.SUCCESS,
      },
      {
        title: 'opportunity.form.dialog.finishOpportunity.item.error',
        value: FinishOpportunityResult.ERROR,
      },
    ],
    propertyVisibleOnList: 'title',
    confirmButton: 'opportunity.form.dialog.finishOpportunity.confirm',
    placeholder: 'opportunity.form.dialog.finishOpportunity.placeholder',
    cancelButton: 'opportunity.form.dialog.finishOpportunity.cancel',
  };
  private rejectionReasonItems: any[] = [
    {
      title: 'opportunity.form.dialog.rejectionReason.item.none',
      value: OpportunityRejectionReason.NONE,
    },
    {
      title: 'opportunity.form.dialog.rejectionReason.item.lost',
      value: OpportunityRejectionReason.LOST,
    },
    {
      title: 'opportunity.form.dialog.rejectionReason.item.noBudget',
      value: OpportunityRejectionReason.NO_BUDGET,
    },
    {
      title: 'opportunity.form.dialog.rejectionReason.item.noDecision',
      value: OpportunityRejectionReason.NO_DECISION,
    },
    {
      title: 'opportunity.form.dialog.rejectionReason.item.other',
      value: OpportunityRejectionReason.OTHER,
    },
  ];

  private readonly rejectionReasonDialogData: Partial<SelectItemDialogData> = {
    title: 'opportunity.form.dialog.rejectionReason.title',
    items: this.rejectionReasonItems,
    propertyVisibleOnList: 'title',
    confirmButton: 'opportunity.form.dialog.rejectionReason.confirm',
    placeholder: 'opportunity.form.dialog.rejectionReason.placeholder',
    cancelButton: 'opportunity.form.dialog.rejectionReason.cancel',
  };

  constructor(private selectItemDialogService: SelectItemDialogService,
              private notificationService: NotificationService,
              private deleteDialogService: DeleteDialogService,
              private opportunityService: OpportunityService
  ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  isScrollVisible(tasksContainer: any): boolean {
    return tasksContainer.scrollWidth !== tasksContainer.clientWidth;
  }

  onScroll(tasksContainer: any): void {
    this.scrollAtTheEnd =
      tasksContainer.clientWidth + tasksContainer.scrollLeft >=
      tasksContainer.scrollWidth - 5;
  }

  isTaskError(task: Task): boolean {
    return task.statusAcceptance === StatusAcceptance.DECLINED;
  }

  taskSelect(task: Task): void {
    if (!task.isInformationTask) {
      this.taskClicked.emit(task);
    }
  }

  isAlertTask(task: Task): boolean {
    return !!this.opportunity.acceptanceUserId && task.active;
  }

  isLastTaskCompleted(): boolean {
    const lastTask: Task =
      this.opportunity &&
      this.opportunity.tasks &&
      this.opportunity.tasks[this.opportunity.tasks.length - 1];
    return lastTask && lastTask.active && !lastTask.name;
  }

  createTaskTooltipData(documentation: string): TaskTooltipData {
    return TaskTooltipDataFactory.create(documentation);
  }

  public tryGoBack(): void {
    this.goBackDialogData.onConfirmClick = () => this.goBack();
    this.deleteDialogService.open(this.goBackDialogData);
  }

  private goBack(): void {
    const activeTask = this.getActiveTask();
    if (activeTask) {
      this.opportunityService
        .goBack(this.opportunity.processInstanceId, 1)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => this.handleSuccessfulGoBack(),
          () =>
            this.notificationService.notify(
              'opportunity.details.notifications.noTaskFound',
              NotificationType.ERROR
            )
        );
    }
  }

  private getActiveTask(): Task {
    if (!this.opportunity || !this.opportunity.tasks) {
      console.error('tasks not found');
      return undefined;
    }
    return this.opportunity.tasks.find((task) => task.active);
  }

  private handleSuccessfulGoBack(): void {
    this.getOpportunity.emit(this.opportunityId);
    this.notificationService.notify(
      'opportunity.details.notifications.successfulGoBack',
      NotificationType.SUCCESS
    );
  }

  finishOpportunity(): void {
    this.finishOpportunityDialogData.onConfirmClick = (result: any) => {
      if (result.value === FinishOpportunityResult.ERROR) {
        this.rejectionReasonDialogData.onConfirmClick = (
          rejectionResponse: any
        ) => {
          this.saveOpportunityResult(result.value, rejectionResponse.value);
        };
        this.selectItemDialogService.open(this.rejectionReasonDialogData);
      } else {
        this.saveOpportunityResult(result.value, undefined);
      }
    };
    this.selectItemDialogService.open(this.finishOpportunityDialogData);
  }

  private saveOpportunityResult(
    finishResult: FinishOpportunityResult,
    rejectionReason: OpportunityRejectionReason
  ): void {
    this.opportunityService
      .finishOpportunity(this.opportunityId, {finishResult, rejectionReason})
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => this.handleSuccessfulFinishOpportunity(finishResult),
        () =>
          this.notificationService.notify(
            'opportunity.details.notifications.finishTaskError',
            NotificationType.ERROR
          )
      );
  }

  private handleSuccessfulFinishOpportunity(
    finishResult: FinishOpportunityResult
  ): void {
    this.getOpportunity.emit(this.opportunityId);
    if (finishResult === FinishOpportunityResult.SUCCESS) {
      this.notificationService.notify(
        'opportunity.details.notifications.successfulFinishSuccessOpportunity',
        NotificationType.SUCCESS
      );
    } else {
      this.notificationService.notify(
        'opportunity.details.notifications.successfulFinishOpportunity',
        NotificationType.SUCCESS
      );
    }
  }

  isTaskButtonDisabled(): boolean {
    if (this.opportunity) {
      const disabled =
        !this.taskOnScreen.active ||
        this.taskOnScreen.past ||
        this.opportunity.acceptanceUserId;
      return !!disabled;
    }
    return true;
  }

  completeTask(): void {
    if (this.taskOnScreen.id !== this.activeTask.id) {
      this.notificationService.notify(
        'opportunity.details.notifications.cannotCompleteTaskFromAnotherTask',
        NotificationType.ERROR
      );
    } else {
      if (this.propertyForm.valid || this.taskOnScreen.isInformationTask) {
        this.opportunityService
          .completeTask(this.opportunity.processInstanceId)
          .pipe(takeUntil(this.componentDestroyed))
          .subscribe(
            (response) => this.handleTaskCompletion(response.status),
            () =>
              this.notificationService.notify(
                'opportunity.details.notifications.noTaskFound',
                NotificationType.ERROR
              )
          );
      } else {
        for (const property of this.taskOnScreen.properties) {
          if (this.propertyForm.get(property.id).hasError('required')) {
            this.propertyForm.get(property.id).markAsTouched();
          }
        }
        this.notificationService.notify(
          'opportunity.details.notifications.requiredPropertiesNotFilled',
          NotificationType.ERROR
        );
      }
    }
  }

  private handleTaskCompletion(status: number): void {
    this.getOpportunity.emit(this.opportunityId);
    if (status === 204) {
      this.notificationService.notify(
        'opportunity.details.notifications.taskCompleted',
        NotificationType.SUCCESS
      );
    } else if (status === 206) {
      this.notificationService.notify(
        'opportunity.details.notifications.taskWaitingForAcceptance',
        NotificationType.SUCCESS
      );
    }
  }

  shouldShowReasonTooltip(opportunity: OpportunityDetails, task: Task, taskIndex: number): boolean {
    if(task.acceptanceUserId && task.past) {
      return true
    }
    if (task.active || (task.past && taskIndex === opportunity.tasks.length - 2)) {
      return !!(opportunity.finishResult === FinishOpportunityResult.ERROR || opportunity.acceptanceUserId);
    } else {
      return false;
    }
  }

  getRejectionReasonTitle(): string {
    const find = this.rejectionReasonItems.find(item => item.value === this.opportunity.rejectionReason);
    return find.title;
  }

}
