import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {PriceBookService} from '../../../price-book/price-book.service';
import {map, startWith, takeUntil, throttleTime} from 'rxjs/operators';
import {PriceBook} from '../../../price-book/price-book.model';
import {Observable, Subject} from 'rxjs';
import {Page, PageRequest} from '../../../shared/pagination/page.model';
import {InfiniteScrollUtil} from '../../../shared/pagination/infinite-scroll.util';
import {OpportunityProductsService} from './opportunity-products.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import {Sort} from '@angular/material/sort';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {OpportunityProduct} from './opportunity-product.model';
import {TranslateService} from '@ngx-translate/core';
import * as cloneDeep from 'lodash/cloneDeep';
import {FormControl} from '@angular/forms';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import {MatDialog} from '@angular/material/dialog';
import {OpportunityProductEditComponent} from './opportunity-product-edit/opportunity-product-edit.component';
import {AuthService} from '../../../login/auth/auth.service';
import {GlobalConfigService} from '../../../global-config/global-config.service';

@Component({
  selector: 'app-opportunity-products-card',
  templateUrl: './opportunity-products-card.component.html',
  styleUrls: ['./opportunity-products-card.component.scss']
})
export class OpportunityProductsCardComponent implements OnInit, OnDestroy {

  @Input() set opportunityId(opportunityId: number) {
    if (opportunityId) {
      this._opportunityId = opportunityId;
      this.initProducts();
    }
  }

  // tslint:disable-next-line:variable-name
  private _currency: string;

  @Input() mainCurrency: string;

  @Input() set currency(value: string) {
    this._currency = value;
    this.getAllPriceBooks();

    this.configService.getGlobalParamValue('rateOfMainCurrencyTo' + this.currency).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (param) => this.exchangeRate = +param.value,
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }

  get currency(): string {
    return this._currency;
  }

  @ViewChild('addSlider') addSlider: SliderComponent;
  @ViewChild('fromPriceBook') fromPriceBookSlider: SliderComponent;
  // tslint:disable-next-line:variable-name
  _opportunityId: number;
  addManually = false;
  addFromPriceBook = false;
  priceBooks: PriceBook[] = [];
  selectedPriceBook: PriceBook;
  priceTitle = '';
  productsPage: Page<OpportunityProduct>;
  columns = ['product.name',  'price', 'quantity',  'priceSummed', 'action'];
  filteredColumns$: Observable<string[]>;
  exchangeRate = 1;
  private currentPage: number;
  private currentSort = 'id,asc';
  private componentDestroyed: Subject<void> = new Subject();

  @Output() opportunityEdited: EventEmitter<void> = new EventEmitter<void>();

  selectPriceBookFromControl: FormControl = new FormControl(null);

  readonly getOpportunityProductsError: ErrorTypes = {
    base: 'opportunity.details.products.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'opportunityNotFound'
      },
      {
        code: FORBIDDEN,
        text: 'noPermission'
      }
    ]
  };

  readonly deleteOpportunityProductError: ErrorTypes = {
    base: 'opportunity.details.products.manualAdd.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'opportunityNotFound'
      },
      {
        code: FORBIDDEN,
        text: 'noPermission'
      }
    ]
  };

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'opportunity.details.products.manualAdd.remove.title',
    description: 'opportunity.details.products.manualAdd.remove.description',
    noteFirst: 'opportunity.details.products.manualAdd.remove.noteFirstPart',
    noteSecond: 'opportunity.details.products.manualAdd.remove.noteSecondPart',
    confirmButton: 'opportunity.details.products.manualAdd.remove.buttonYes',
    cancelButton: 'opportunity.details.products.manualAdd.remove.buttonNo',
  };

  constructor(private priceBookService: PriceBookService,
              private translateService: TranslateService,
              private opportunityProductsService: OpportunityProductsService,
              private notificationService: NotificationService,
              private deleteDialogService: DeleteDialogService,
              private authService: AuthService,
              public dialog: MatDialog,
              private configService: GlobalConfigService) {
  }

  ngOnInit(): void {
    this.getAllPriceBooks();
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  public onScroll(e: any): void {
    if (InfiniteScrollUtil.shouldGetMoreData(e) && this.productsPage.numberOfElements < this.productsPage.totalElements) {
      const nextPage = this.currentPage + 1;
      this.getMoreProducts({
        page: '' + nextPage,
        size: InfiniteScrollUtil.ELEMENTS_ON_PAGE,
        sort: this.currentSort
      });
    }
  }

  public priceBookSelectionChange(priceBook: PriceBook): void {
    this.selectedPriceBook = cloneDeep(priceBook);
    this.priceTitle = this.translateService
      .instant('opportunity.details.products.addProductFromPriceBook.title', {priceBook: this.selectedPriceBook.name});
    this.fromPriceBookSlider.open();
    this.selectPriceBookFromControl.patchValue(undefined);
  }

  tryDelete(id: number): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteProduct(id);
    this.deleteDialogService.open(this.deleteDialogData);
  }

  public onSortChange($event: Sort): void {
    this.currentSort = $event.direction ? ($event.active + ',' + $event.direction) : 'id,asc';
    this.initProducts();
  }

  addManuallyChanged(value: boolean): void {
    if (value) {
      this.addFromPriceBook = false;
      this.addSlider.open();
    }
  }

  tryEdit(id: any): void {
    this.dialog.open(OpportunityProductEditComponent, {
      data: {
        opportunityId: this._opportunityId,
        productId: id,
        currency: this.currency,
        onConfirmClick: () => this.initProductsAndRefreshOpportunity()
      }
    });
  }

  private deleteProduct(id: number): void {
    this.opportunityProductsService.deleteOpportunityProduct(this._opportunityId, id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.initProductsAndRefreshOpportunity();
          this.notificationService.notify('opportunity.details.products.manualAdd.notifications.removed', NotificationType.SUCCESS);
        },
        (err) => this.notificationService.notifyError(this.deleteOpportunityProductError, err.status)
      );
  }

  private getAllPriceBooks(): void {
    if (this.currency) {
      this.priceBookService.getPriceBookToAssignByCurrency(this.currency).pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        (priceBooks: PriceBook[]) => this.priceBooks = priceBooks
      );
    }
  }

  initProductsAndRefreshOpportunity(): void {
    this.initProducts();
    this.opportunityEdited.next();
  }

  initProducts(pageRequest: PageRequest =
    {page: '0', size: InfiniteScrollUtil.ELEMENTS_ON_PAGE, sort: this.currentSort}): void {
    this.opportunityProductsService.getOpportunityProducts(this._opportunityId, pageRequest)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      page => this.handleInitSuccess(page),
      (err) => this.notificationService.notifyError(this.getOpportunityProductsError, err.status)
    );
  }

  private getMoreProducts(pageRequest: PageRequest): void {
    this.opportunityProductsService.getOpportunityProducts(this._opportunityId, pageRequest)
      .pipe(
        takeUntil(this.componentDestroyed),
        throttleTime(500)
      ).subscribe(
      page => this.handleGetMoreProductsSuccess(page),
      (err) => this.notificationService.notifyError(this.getOpportunityProductsError, err.status)
    );
  }

  private handleGetMoreProductsSuccess(page: any): void {
    this.currentPage++;
    this.productsPage.content = this.productsPage.content.concat(page.content);
    this.productsPage.numberOfElements += page.content.length;
  }

  private handleInitSuccess(page: any): void {
    this.currentPage = 0;
    this.productsPage = page;
  }

  private getFilteredColumns(): string[] {
    return this.columns.filter((item) =>
      !this.authService.isPermissionPresent('OpportunityProductsCard.' + item, 'i') || item === 'action' || item === 'selectRow'
      || (item === 'priceSummedInMainCurrency' && this.mainCurrency !== this.currency));
  }
}
