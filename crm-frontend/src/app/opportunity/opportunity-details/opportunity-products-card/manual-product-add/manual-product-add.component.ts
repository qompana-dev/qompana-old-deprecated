import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {debounceTime, distinctUntilChanged, filter, finalize, switchMap, takeUntil, tap} from 'rxjs/operators';
import {ErrorTypes, NotificationService, NotificationType} from '../../../../shared/notification.service';
import {ProductCategoryService} from '../../../../product/product-category.service';
import {Subject} from 'rxjs';
import {ProductCategory} from '../../../../product/product.model';
import {MatOption} from '@angular/material/core';
import {MatSelect} from '@angular/material/select';
import * as isEqual from 'lodash/isEqual';
import * as pull from 'lodash/pull';
import {FormUtil} from '../../../../shared/form.util';
import {OpportunityProductsService} from '../opportunity-products.service';
import {BAD_REQUEST, CONFLICT, FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import {SliderService} from '../../../../shared/slider/slider.service';
import {applyPermission} from '../../../../shared/permissions-utils';
import {AuthService} from '../../../../login/auth/auth.service';
import {CrmObject, CrmObjectType} from '../../../../shared/model/search.model';
import {AssignToGroupService} from '../../../../shared/assign-to-group/assign-to-group.service';
import {FormValidators} from '../../../../shared/form/form-validators';
import {DictionaryId, DictionaryWord} from "../../../../dictionary/dictionary.model";
import { DictionaryStoreService } from 'src/app/dictionary/dictionary-store.service';

@Component({
  selector: 'app-manual-product-add',
  templateUrl: './manual-product-add.component.html',
  styleUrls: ['./manual-product-add.component.scss']
})
export class ManualProductAddComponent implements OnInit, OnDestroy {

  @ViewChild('categorySelect') categorySelect: MatSelect;
  @ViewChild('subcategorySelect') subcategorySelect: MatSelect;
  @Input() sliderComponent: SliderComponent;
  @Input() opportunityId: number;
  // tslint:disable-next-line:variable-name
  _currency: string;
  @Output() productAdded: EventEmitter<void> = new EventEmitter<void>();
  productForm: FormGroup;
  allProductCategories: ProductCategory[];
  readonly undefinedCategoryId = -1000;
  private componentDestroyed: Subject<void> = new Subject();
  productCategoriesWithSubcategories: ProductCategory[] = [];

  manufacturerSearchLoading = false;
  supplierSearchLoading = false;
  filteredManufacturers: CrmObject[] = [];
  filteredSuppliers: CrmObject[] = [];
  units : DictionaryWord[] = [];

  @ViewChild('addProductCategorySlider') addCategorySlider: SliderComponent;
  @ViewChild('addProductSubcategorySlider') addSubcategorySlider: SliderComponent;

  @Input() set currency(currency: string) {
    if (currency) {
      this._currency = currency;
      this.createProductForm();
    }
  }

  get name(): AbstractControl { return this.productForm.get('name'); }
  get code(): AbstractControl { return this.productForm.get('code'); }
  get skuCode(): AbstractControl { return this.productForm.get('skuCode'); }
  get categoryIds(): AbstractControl { return this.productForm.get('categoryIds'); }
  get subcategoryIds(): AbstractControl { return this.productForm.get('subcategoryIds'); }
  get purchasePrice(): AbstractControl { return this.productForm.get('purchasePrice'); }
  get sellPrice(): AbstractControl { return this.productForm.get('sellPrice'); }
  get quantity(): AbstractControl { return this.productForm.get('quantity'); }
  get unit(): AbstractControl { return this.productForm.get('unit'); }
  get currencyControl(): AbstractControl { return this.productForm.get('currency'); }
  get manufacturerSearch(): AbstractControl { return this.productForm.get('manufacturerSearch'); }
  get manufacturerId(): AbstractControl { return this.productForm.get('manufacturerId'); }
  get supplierSearch(): AbstractControl { return this.productForm.get('supplierSearch'); }
  get supplierId(): AbstractControl { return this.productForm.get('supplierId'); }

  readonly addErrorTypes: ErrorTypes = {
    base: 'opportunity.details.products.manualAdd.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'validationFailed'
      }, {
        code: NOT_FOUND,
        text: 'notFound'
      }, {
        code: CONFLICT,
        text: 'conflict'
      }, {
        code: FORBIDDEN,
        text: 'noPermission'
      }
    ]
  };

  constructor(private fb: FormBuilder,
              private notificationService: NotificationService,
              private categoryService: ProductCategoryService,
              private opportunityProductsService: OpportunityProductsService,
              private assignToGroupService: AssignToGroupService,
              private sliderService: SliderService,
              private dictionaryStoreService: DictionaryStoreService,
              private authService: AuthService) {
    this.getProductCategories();
    this.subscribeForUnits();
    this.createProductForm();
    this.subscribeForSliderClose();
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  closeSlider(): void {
    this.createProductForm();
    this.sliderComponent.close();
  }

  submit(): void {
    if (!this.productForm.valid) {
      FormUtil.setTouched(this.productForm);
      return;
    }
    const opportunityProduct = this.productForm.value;
    opportunityProduct.subcategoryIds = this.getSelectedSubcategories();
    opportunityProduct.currency = this._currency;

    this.opportunityProductsService.saveOpportunityProduct(this.opportunityId, opportunityProduct).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.handleSubmitSuccess(),
      (err) => this.notificationService.notifyError(this.addErrorTypes, err.status)
    );
  }

  getSubcategories(): void {
    const selectedCategories = this.categoryIds.value;
    this.categoryService.getCategoriesWithSubcategories(selectedCategories)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      categories => this.productCategoriesWithSubcategories = categories,
      err => this.notificationService.notify('products.form.notifications.getSubcategoriesError', NotificationType.ERROR)
    );
  }

  manufacturerSelect(manufacturer: CrmObject): void {
    if (manufacturer && manufacturer.type === CrmObjectType.manufacturer) {
      this.manufacturerId.patchValue(manufacturer.id);
      this.manufacturerSearch.patchValue(manufacturer.label);
    }
  }

  supplierSelect(supplier: CrmObject): void {
    if (supplier && supplier.type === CrmObjectType.supplier) {
      this.supplierId.patchValue(supplier.id);
      this.supplierSearch.patchValue(supplier.label);
    }
  }

  onNoCategorySelect(option: MatOption): void {
    if (option.selected) {
      this.categorySelect.options.filter(item => item !== option).forEach(item => item.deselect());
      this.subcategoryIds.disable();
    }
  }

  addCategory(matSelect: MatSelect): void {
    matSelect.close();
    this.addCategorySlider.open();
  }

  getSelectedCategoryIds(): number[] {
    return this.categoryIds.value;
  }

  onSubmitAddCategorySlider(selectedCategoriesIds: number[]): void {
    this.getProductCategories();
    this.categoryIds.patchValue(selectedCategoriesIds);
    this.addCategorySlider.close();
    this.getSubcategories();
  }

  onNoSubcategorySelect(option: MatOption): void {
    if (option.selected) {
      this.subcategorySelect.options.filter(item => item !== option).forEach(item => item.deselect());
    }
  }

  addSubcategory(subcategorySelect: MatSelect): void {
    subcategorySelect.close();
    this.addSubcategorySlider.open();
  }

  getSelectedSubcategoriesIds(): void {
    return this.subcategoryIds.value;
  }

  onSubmitAddSubcategorySlider(selectedSubcategoriesIds: number[]): void {
    this.getSubcategories();
    this.subcategoryIds.patchValue(selectedSubcategoriesIds);
    this.addSubcategorySlider.close();
  }

  onCancelAddCategorySlider(): void {
    this.addCategorySlider.close();
  }

  onCancelAddSubcategorySlider(): void {
    this.addSubcategorySlider.close();
  }

  private subscribeForManufacturerSearch(): void {
    this.manufacturerSearch.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      tap(() => this.manufacturerSearchLoading = true),
      switchMap(((input: string) => {
          if (!input || input === '') {
            this.manufacturerId.patchValue(null);
          }
          return this.assignToGroupService.searchForType
          (CrmObjectType.manufacturer, input !== null ? input : '', [CrmObjectType.manufacturer])
            .pipe(finalize(() => this.manufacturerSearchLoading = false));
        }
      ))).subscribe(
      (result: CrmObject[]) => {
        this.filteredManufacturers = result;
      }
    );
    this.manufacturerSearch.patchValue('');
  }

  private subscribeForSupplierSearch(): void {
    this.supplierSearch.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      tap(() => this.supplierSearchLoading = true),
      switchMap(((input: string) => {
          if (!input || input === '') {
            this.supplierId.patchValue(null);
          }
          return this.assignToGroupService.searchForType
          (CrmObjectType.supplier, input !== null ? input : '', [CrmObjectType.supplier])
            .pipe(finalize(() => this.supplierSearchLoading = false));
        }
      ))).subscribe(
      (result: CrmObject[]) => {
        this.filteredSuppliers = result;
      }
    );
    this.supplierSearch.patchValue('');
  }

  private getSelectedSubcategories(): number[] {
    const selectedCategoryIds: number[] = this.categoryIds.value;
    const selectedSubcategoryIds: number[] = this.subcategoryIds.value;
    const initialCategoryIds = this.allProductCategories.filter(c => selectedCategoryIds.includes(c.id)).map(c => c.initialSubcategory);
    return pull(Array.from(new Set([...initialCategoryIds, ...selectedSubcategoryIds]).values())
      .filter(v => !!v), this.undefinedCategoryId);
  }

  private createProductForm(): void {
    this.productForm = this.fb.group({
      name: [null, [Validators.required, FormValidators.whitespace, Validators.maxLength(200)]],
      code: [null, [Validators.required, FormValidators.whitespace, Validators.maxLength(100)]],
      skuCode: [null, [Validators.required, FormValidators.whitespace, Validators.maxLength(100)]],
      categoryIds: [null, []],
      subcategoryIds: [null, []],
      supplierSearch: [null, []],
      supplierId: [null, []],
      manufacturerSearch: [null, []],
      manufacturerId: [null, []],
      currency: new FormControl({value: this._currency, disabled: true}),
      purchasePrice: [null, Validators.compose([Validators.required, Validators.min(0), Validators.maxLength(18)])],
      sellPrice: [null, Validators.compose([Validators.required, Validators.min(0), Validators.maxLength(18)])],
      quantity: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(999999999999999999)])],
      unit: [null, Validators.required]
    });
    this.applyPermissionsToForm();
    this.listenForCategoryChanges();
    this.listenForSubcategoryChanges();
    this.subscribeForManufacturerSearch();
    this.subscribeForSupplierSearch();
  }

  private subscribeForSliderClose(): void {
    this.sliderService.closeClicked.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (key: string) => {
        if (key === this.sliderComponent.key) {
          this.closeSlider();
        }
      });
  }

  private getProductCategories(): void {
    this.categoryService.getCategories().pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      categories => {
        this.allProductCategories = categories;
      },
      err => this.notificationService.notify('products.form.notifications.getCategoriesError', NotificationType.ERROR)
    );
  }

  private listenForCategoryChanges(): void {
    this.categoryIds.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      filter(value => !!value),
      distinctUntilChanged((x, y) => isEqual(x, y)),
    ).subscribe(
      (categories: Array<number>) => {
        if (categories[0] === this.undefinedCategoryId && categories.length > 1) {
          this.categoryIds.patchValue(categories.slice(1));
          this.subcategoryIds.enable();
        }
        if (categories.length >= 1 && categories[0] !== this.undefinedCategoryId) {
          this.subcategoryIds.enable();
        }
        if (categories.length === 0) {
          this.categoryIds.patchValue([this.undefinedCategoryId]);
          this.subcategoryIds.disable();
          this.subcategoryIds.patchValue([this.undefinedCategoryId]);
        }
        const subcategories = this.categoryService.flattenSubcategories(this.productCategoriesWithSubcategories);
        const allowed = subcategories.filter(sub => this.subcategoryIds.value.includes(sub.id))
          .filter(sub => this.categoryIds.value.includes(sub.categoryId)).map(sub => sub.id);
        this.subcategoryIds.patchValue(allowed);
      }
    );
  }

  private listenForSubcategoryChanges(): void {
    this.subcategoryIds.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      filter(value => !!value),
      distinctUntilChanged((x, y) => isEqual(x, y)),
    ).subscribe(
      (categories: Array<any>) => {
        if (categories[0] === this.undefinedCategoryId && categories.length > 1) {
          this.subcategoryIds.patchValue(categories.slice(1));
        }
        if (categories.length === 0) {
          this.subcategoryIds.patchValue([this.undefinedCategoryId]);
        }
      }
    );
  }

  private handleSubmitSuccess(): void {
    this.notificationService.notify('opportunity.details.products.manualAdd.notifications.addedSuccessfully', NotificationType.SUCCESS);
    this.productAdded.emit();
    this.createProductForm();
    this.sliderComponent.close();
  }

  private applyPermissionsToForm(): void {
    if (this.productForm) {
      applyPermission(this.supplierSearch, !this.authService.isPermissionPresent('OpportunityProductsCard.supplier', 'r'));
      applyPermission(this.manufacturerSearch, !this.authService.isPermissionPresent('OpportunityProductsCard.manufacturer', 'r'));
    }
  }

  private subscribeForUnits(): void {
    this.dictionaryStoreService.getDictionaryWordsById(DictionaryId.UNIT).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (words: DictionaryWord[]) => this.units = words,
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }
}
