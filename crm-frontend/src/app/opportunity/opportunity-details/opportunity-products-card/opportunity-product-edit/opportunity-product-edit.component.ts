import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {OpportunityProductsService} from '../opportunity-products.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {OpportunityProductEditDTO} from '../opportunity-product.model';
import {ErrorTypes, NotificationService, NotificationType} from '../../../../shared/notification.service';
import {FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import * as isNil from 'lodash/isNil';
import {DictionaryId, DictionaryWord} from "../../../../dictionary/dictionary.model";
import { DictionaryStoreService } from 'src/app/dictionary/dictionary-store.service';


@Component({
  selector: 'app-opportunity-product-edit',
  templateUrl: './opportunity-product-edit.component.html',
  styleUrls: ['./opportunity-product-edit.component.scss']
})
export class OpportunityProductEditComponent implements OnInit, OnDestroy {

  opportunityProductEditDTO: OpportunityProductEditDTO;
  private componentDestroyed: Subject<void> = new Subject();
  units : DictionaryWord[] = [];

  readonly errorTypes: ErrorTypes = {
    base: 'opportunity.details.products.manualAdd.edit.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'notFound'
      }, {
        code: FORBIDDEN,
        text: 'noPermission'
      }
    ]
  };

  constructor(public dialogRef: MatDialogRef<OpportunityProductEditComponent>,
              public dialog: MatDialog,
              private opportunityProductsService: OpportunityProductsService,
              private notificationService: NotificationService,
              private dictionaryStoreService: DictionaryStoreService,
              @Inject(MAT_DIALOG_DATA) public data: EditOpportunityProductData) {
  }

  ngOnInit() {
    this.opportunityProductsService.getOpportunityProduct(this.data.opportunityId, this.data.productId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (opportunityProductEditDTO: OpportunityProductEditDTO) => {
        opportunityProductEditDTO.sellPrice = this.getNumberInFormat(opportunityProductEditDTO.sellPrice);
        this.opportunityProductEditDTO = opportunityProductEditDTO;
      }
    );
    this.subscribeForUnits();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  close(): void {
    this.dialogRef.close();
  }

  submit(): void {
    if (!this.isValid()) {
      return;
    }
    this.opportunityProductsService.editOpportunityProduct(this.data.opportunityId, this.opportunityProductEditDTO).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.handleEditSuccess(),
      (err) => this.notificationService.notifyError(this.errorTypes, err.status)
    );
  }

  private handleEditSuccess(): void {
    this.notificationService.notify('opportunity.details.products.manualAdd.edit.notifications.success', NotificationType.SUCCESS);
    this.data.onConfirmClick();
    this.close();
  }

  private isValid(): boolean {
    return this.opportunityProductEditDTO &&
      !isNil(this.opportunityProductEditDTO.sellPrice) &&
      !isNil(this.opportunityProductEditDTO.quantity);
  }

  private getNumberInFormat(value: number): string {
    return String(value).replace('.', ',');
  }

  private subscribeForUnits(): void {
    this.dictionaryStoreService.getDictionaryWordsById(DictionaryId.UNIT).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (words: DictionaryWord[]) => this.units = words,
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }
}

interface EditOpportunityProductData {
  opportunityId: number;
  productId: number;
  currency: string;
  onConfirmClick: () => void;
}
