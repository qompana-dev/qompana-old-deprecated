import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {PageRequest} from '../../../shared/pagination/page.model';
import {
  OpportunityProduct,
  OpportunityProductAddFromPriceBookDTO,
  OpportunityProductEditDTO,
  OpportunityProductSaveDTO
} from './opportunity-product.model';
import {map} from 'rxjs/operators';
import {UrlUtil} from '../../../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class OpportunityProductsService {

  private opportunityUrl = `${UrlUtil.url}/crm-business-service/opportunity`;

  constructor(private http: HttpClient) {
  }

  public getOpportunityProducts(opportunityId: number, pageRequest: PageRequest): Observable<OpportunityProduct[]> {
    const url = `${this.opportunityUrl}/${opportunityId}/products`;
    const params = new HttpParams({fromObject: pageRequest as any});
    return this.http.get<OpportunityProduct[]>(url, {params});
  }

  public getOpportunityProductsAsList(opportunityId: number, pageRequest: PageRequest): Observable<OpportunityProduct[]> {
    const url = `${this.opportunityUrl}/${opportunityId}/products`;
    const params = new HttpParams({fromObject: pageRequest as any});
    return this.http.get<any>(url, {params})
      .pipe(map(c => c.content));
  }

  public saveOpportunityProduct(opportunityId: number, opportunityProduct: OpportunityProductSaveDTO): Observable<void> {
    const url = `${this.opportunityUrl}/${opportunityId}/products/manual`;
    return this.http.post<void>(url, opportunityProduct);
  }

  public addOpportunityProducts(opportunityId: number, opportunityProducts: OpportunityProductAddFromPriceBookDTO[]): Observable<void> {
    const url = `${this.opportunityUrl}/${opportunityId}/products`;
    return this.http.post<void>(url, opportunityProducts);
  }

  public deleteOpportunityProduct(opportunityId: number, productId: number): Observable<void> {
    const url = `${this.opportunityUrl}/${opportunityId}/products/${productId}`;
    return this.http.delete<void>(url);
  }

  public getOpportunityProduct(opportunityId: number, productId: number): Observable<OpportunityProductEditDTO> {
    const url = `${this.opportunityUrl}/${opportunityId}/products/${productId}`;
    return this.http.get<OpportunityProductEditDTO>(url);
  }

  public editOpportunityProduct(opportunityId: number, opportunityProduct: OpportunityProductEditDTO): Observable<void> {
    const url = `${this.opportunityUrl}/${opportunityId}/products/${opportunityProduct.id}`;
    return this.http.put<void>(url, opportunityProduct);
  }
}
