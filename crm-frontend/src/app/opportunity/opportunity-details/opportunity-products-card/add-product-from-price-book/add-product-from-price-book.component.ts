import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {PriceBook} from '../../../../price-book/price-book.model';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {ProductService} from '../../../../product/product.service';
import {FilterStrategy} from '../../../../shared/model';
import {PageRequest} from '../../../../shared/pagination/page.model';
import {map, startWith, takeUntil} from 'rxjs/operators';
import {ErrorTypes, NotificationService, NotificationType} from '../../../../shared/notification.service';
import {Product} from '../../../../product/product.model';
import {Observable, Subject} from 'rxjs';
import {InfinityScrollUtil, InfinityScrollUtilInterface} from '../../../../shared/infinity-scroll.util';
import {AuthService} from '../../../../login/auth/auth.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FormUtil} from '../../../../shared/form.util';
import {OpportunityProductsService} from '../opportunity-products.service';
import {OpportunityProductAddFromPriceBookDTO} from '../opportunity-product.model';
import {BAD_REQUEST, FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import {FormatCurrencyPipe} from '../../../../shared/pipe/format-currency.pipe';

@Component({
  selector: 'app-add-product-from-price-book',
  templateUrl: './add-product-from-price-book.component.html',
  styleUrls: ['./add-product-from-price-book.component.scss']
})
export class AddProductFromPriceBookComponent implements OnInit, OnDestroy, InfinityScrollUtilInterface {
  private selectedPriceBook: PriceBook;
  infinityScroll: InfinityScrollUtil;
  columns = ['name', 'category', 'subcategory', 'sellPriceNet', 'unit', 'code', 'selectRow'];
  selectedProductColumns = ['name', 'sellPrice', 'quantity', 'unit', 'fullPrice'];
  selectedProducts: SelectedProduct[] = [];
  selectedProductsDataSource: Subject<SelectedProduct[]> = new Subject<SelectedProduct[]>();

  filteredColumns$: Observable<string[]>;
  private componentDestroyed: Subject<void> = new Subject();

  @Output() productAdded: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() slider: SliderComponent;
  @Input() opportunityId: number;

  @Input()
  set priceBook(priceBook: PriceBook) {
    this.selectedProducts = [];
    this.selectedPriceBook = priceBook;
    this.infinityScroll.initData();
  }

  readonly addErrorTypes: ErrorTypes = {
    base: 'opportunity.details.products.addProductFromPriceBook.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'validationFailed'
      }, {
        code: NOT_FOUND,
        text: 'notFound'
      }, {
        code: FORBIDDEN,
        text: 'noPermission'
      }
    ]
  };

  constructor(private productService: ProductService,
              private authService: AuthService,
              private formatCurrencyPipe: FormatCurrencyPipe,
              private opportunityProductsService: OpportunityProductsService,
              private fb: FormBuilder,
              private notificationService: NotificationService) {
    this.infinityScroll = new InfinityScrollUtil(
      this, this.componentDestroyed, this.notificationService,
      'opportunity.details.products.addProductFromPriceBook.notifications.productListError'
    );
  }

  ngOnInit(): void {
    this.infinityScroll.initData();
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
    this.slider.closeEvent
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.clear());
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  productChecked(product: Product, checked: boolean): void {
    if (checked) {
      this.addNewSelectedProduct(product);
    } else {
      this.removeSelectedProduct(product);
    }
    this.selectedProductsDataSource.next(this.selectedProducts);
  }

  addNewSelectedProduct(product: Product): void {
    if (product) {
      this.removeSelectedProduct(product);
      const newSelectProduct: SelectedProduct = new SelectedProduct(
        product,
        this.fb.group({
          sellPrice: [this.getNumberInFormat(product.sellPriceNet), [Validators.required]],
          quantity: [0, [Validators.required, Validators.min(1)]],
          fullPrice: [this.formatCurrencyPipe.transform(0, this.selectedPriceBook.currency), Validators.required]
        })
      );
      newSelectProduct.fullPrice.disable();
      this.selectedProducts.push(newSelectProduct);
    }
  }

  private getNumberInFormat(value: number): string {
      return String(value).replace('.', ',');
  }

  removeSelectedProduct(product: Product): void {
    if (product) {
      const index = this.selectedProducts.map(sp => sp.id).indexOf(product.id);
      if (index !== -1) {
        this.selectedProducts.splice(index, 1);
      }
    }
  }

  priceChange(productId: number): void {
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.selectedProducts.length; i++) {
      if (this.selectedProducts[i].id === productId) {
        if (this.selectedProducts[i].sellPrice.valid && this.selectedProducts[i].quantity.valid) {
          FormUtil.setTouched(this.selectedProducts[i].formGroup);
          const value = +String(this.selectedProducts[i].sellPrice.value).replace(',', '.') * this.selectedProducts[i].quantity.value;
          this.selectedProducts[i].fullPrice
            .patchValue(this.formatCurrencyPipe.transform(value, this.selectedPriceBook.currency));
          return;
        }
      }
    }
  }

  getItemObservable(selection: FilterStrategy, pageRequest: PageRequest): Observable<any> {
    if (!this.selectedPriceBook) {
      return Observable.of([]);
    }
    return this.productService.getProductsForPriceBook(pageRequest, selection, this.selectedPriceBook.id, true);
  }

  public save(): void {
    if (!this.selectedProducts || this.selectedProducts.length === 0) {
      this.notificationService.notify('opportunity.details.products.addProductFromPriceBook.notifications.notSelectedProducts',
        NotificationType.ERROR);
      return;
    }

    const formValid = this.selectedProducts.filter(sp => {
      FormUtil.setTouched(sp.formGroup);
      return !sp.formGroup.valid;
    }).length === 0;

    if (!formValid) {
      const isQuantityError = this.selectedProducts.filter(sp => !sp.formGroup.get('quantity').valid).length !== 0;
      if (isQuantityError) {
        this.notificationService.notify('opportunity.details.products.addProductFromPriceBook.notifications.quantityError',
          NotificationType.ERROR);
      } else {
        this.notificationService.notify('opportunity.details.products.addProductFromPriceBook.notifications.formError',
          NotificationType.ERROR);
      }
      return;
    }

    this.opportunityProductsService.addOpportunityProducts(this.opportunityId, this.getDtoToSave())
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(() => this.handleOpportunityAdd(),
        (err) => this.notificationService.notifyError(this.addErrorTypes, err.status));
  }

  private handleOpportunityAdd(): void {
    this.notificationService.notify('opportunity.details.products.addProductFromPriceBook.notifications.addedSuccessfully',
      NotificationType.SUCCESS);
    this.productAdded.emit(true);
    this.slider.close();
  }

  private getDtoToSave(): OpportunityProductAddFromPriceBookDTO[] {
    return this.selectedProducts
      .map(sp => {
        return {
          id: sp.product.id,
          sellPrice: sp.sellPrice.value,
          quantity: sp.quantity.value,
        };
      });
  }

  public cancel(): void {
    this.slider.close();
  }

  private clear(): void {
    this.selectedProducts = [];
    this.selectedProductsDataSource.next(this.selectedProducts);
    this.infinityScroll.itemPage.content = this.infinityScroll.itemPage
      .content.map((content) => content.checked = false);
  }

  private getFilteredColumns(): string[] {
    const key = 'AddProductFromPriceBookComponent';
    return this.columns
      .filter((item) => !this.authService.isPermissionPresent(key + '.' + item, 'i') || item === 'selectRow');
  }
}

export class SelectedProduct {
  id: number;
  name: string;
  unitName: string;
  product: Product;
  formGroup: FormGroup;

  constructor(product: Product, formGroup: FormGroup) {
    this.product = product;
    this.id = product.id;
    this.name = product.name;
    this.unitName = product.unitName;
    this.formGroup = formGroup;
  }

  // @formatter:off
  get sellPrice(): AbstractControl { return this.formGroup.get('sellPrice'); }
  get quantity(): AbstractControl { return this.formGroup.get('quantity'); }
  get fullPrice(): AbstractControl { return this.formGroup.get('fullPrice'); }
  // @formatter:on
}
