export class OpportunityProduct {
  id: number;
  name: string;
  category: string;
  subcategory: string;
  code: string;
  price: number;
  priceChanged: boolean;
  priceChangedFrom?: number;
  priceChangedTo?: number;
  currency: string;
  quantity: number;
  priceSummed: number;
  unit: string;
}

export class OpportunityProductSaveDTO {
  name: string;
  code: string;
  skuCode: string;
  categoryIds: number[];
  subcategoryIds: number[];
  supplier: string;
  manufacturer: string;
  currency: string;
  purchasePrice: number;
  sellPrice: number;
  quantity: number;
  unit: number;
}

export class OpportunityProductAddFromPriceBookDTO {
  id: number;
  sellPrice: number;
  quantity: number;
}

export class OpportunityProductEditDTO {
  id: number;
  name: string;
  sellPrice: any;
  quantity: number;
  unit: number;
  opportunityProduct: boolean;
}
