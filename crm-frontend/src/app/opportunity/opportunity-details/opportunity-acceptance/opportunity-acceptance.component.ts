import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {OpportunityDetails} from '../../opportunity.model';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';
import {takeUntil} from 'rxjs/operators';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {OpportunityService} from '../../opportunity.service';
import {ConfirmWithCommentService} from '../../../shared/dialog/confirm-with-comment/confirm-with-comment.service';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';
import {Router} from '@angular/router';
import {LoginService} from '../../../login/login.service';
import {UserNotificationService} from '../../../navigation/user-notification/user-notification.service';

@Component({
  selector: 'app-opportunity-acceptance',
  templateUrl: './opportunity-acceptance.component.html',
  styleUrls: ['./opportunity-acceptance.component.scss']
})
export class OpportunityAcceptanceComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();

  loggedAccountId: number;
  notificationId: number;
  opportunity: OpportunityDetails;

  @Input() set _opportunity(opportunity: OpportunityDetails) {
    this.opportunity = opportunity;
    this.initializeView(opportunity);
  }

  @Input() activeTask = null;

  private readonly acceptDialogData: Partial<DeleteDialogData> = {
    title: 'opportunity.details.dialog.accept.title',
    description: 'opportunity.details.dialog.accept.description',
    noteFirst: 'opportunity.details.dialog.accept.noteFirstPart',
    noteSecond: 'opportunity.details.dialog.accept.noteSecondPart',
    confirmButton: 'opportunity.details.dialog.accept.confirm',
    cancelButton: 'opportunity.details.dialog.accept.cancel',
  };

  private readonly declineDialogData: Partial<DeleteDialogData> = {
    title: 'opportunity.details.dialog.decline.title',
    description: 'opportunity.details.dialog.decline.description',
    noteFirst: 'opportunity.details.dialog.decline.noteFirstPart',
    noteSecond: 'opportunity.details.dialog.decline.noteSecondPart',
    confirmButton: 'opportunity.details.dialog.decline.confirm',
    cancelButton: 'opportunity.details.dialog.decline.cancel',
  };

  constructor(private confirmWithCommentService: ConfirmWithCommentService,
              private userNotificationService: UserNotificationService,
              private deleteDialogService: DeleteDialogService,
              private notificationService: NotificationService,
              private opportunityService: OpportunityService,
              private loginService: LoginService,
              private $router: Router
  ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  tryAccept(withComment: boolean): void {
    this.acceptDialogData.numberToDelete = this.activeTask.name;
    this.acceptDialogData.onConfirmClick = (comment) => this.accept(comment);
    if (withComment) {
      this.confirmWithCommentService.open(this.acceptDialogData);
    } else {
      this.deleteDialogService.open(this.acceptDialogData);
    }
  }

  tryDecline(withComment: boolean): void {
    this.declineDialogData.numberToDelete = this.activeTask.name;
    this.declineDialogData.onConfirmClick = (comment) => this.decline(comment);
    if (withComment) {
      this.confirmWithCommentService.open(this.declineDialogData);
    } else {
      this.deleteDialogService.open(this.declineDialogData);
    }
  }

  private accept(comment: string): void {
    this.opportunityService
      .accept({userNotificationId: this.notificationId, comment})
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.notificationService.notify(
            'opportunity.details.notifications.acceptedTask',
            NotificationType.SUCCESS
          );
          this.$router.navigate(['/opportunity']);
        },
        () =>
          this.notificationService.notify(
            'opportunity.details.notifications.problemAcceptingTask',
            NotificationType.ERROR
          )
      );
  }

  private decline(comment: string): void {
    this.opportunityService
      .decline({userNotificationId: this.notificationId, comment})
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.notificationService.notify(
            'opportunity.details.notifications.declinedTask',
            NotificationType.SUCCESS
          );
          this.$router.navigate(['/opportunity']);
        },
        () =>
          this.notificationService.notify(
            'opportunity.details.notifications.problemDecliningTask',
            NotificationType.ERROR
          )
      );
  }

  private initializeView(opportunity: OpportunityDetails): void {
    this.loggedAccountId = this.loginService.getLoggedAccountId();
    if (opportunity.acceptanceUserId === this.loggedAccountId) {
      this.userNotificationService
        .getNotificationId(
          this.loggedAccountId,
          'OPPORTUNITY_TASK_ACCEPTANCE',
          opportunity.processInstanceId
        )
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((notificationId) => (this.notificationId = notificationId));
    }
  }

}
