import {StatusAcceptance} from './../../bpmn/bpmn.model';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {map, takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {OpportunityService} from '../opportunity.service';
import {ErrorTypes, NotificationService, NotificationType, } from '../../shared/notification.service';
import {OpportunityDetails, OpportunityRejectionReason, OpportunitySave, Task, OpportunityPriority, PriorityRadioButtons} from '../opportunity.model';
import {FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import * as cloneDeep from 'lodash/cloneDeep';
import {Property} from '../../lead/lead-details/lead-details.model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BpmnUtil} from '../../bpmn/bpmn.util';
import {CrmObjectType} from '../../shared/model/search.model';
import {DeleteDialogData} from '../../shared/dialog/delete/delete-dialog.component';
import {DeleteDialogService} from '../../shared/dialog/delete/delete-dialog.service';
import {SliderComponent} from '../../shared/slider/slider.component';
import {PersonService} from '../../person/person.service';
import {SelectItemDialogService} from '../../shared/dialog/select-item-dialog/select-item-dialog.service';
import {GlobalConfigService} from '../../global-config/global-config.service';
import {OpportunityDetailsCardComponent} from './opportunity-details-card/opportunity-details-card.component';

@Component({
  selector: 'app-opportunity-details',
  templateUrl: './opportunity-details.component.html',
  styleUrls: ['./opportunity-details.component.scss'],
})
export class OpportunityDetailsComponent implements OnInit, OnDestroy {
  @ViewChild('editSlider') editSlider: SliderComponent;
  @ViewChild('opportunityDetailsCardComponent')
  opportunityDetailsCardComponent: OpportunityDetailsCardComponent;
  opportunity: OpportunityDetails;
  opportunityId: number;
  outsideCreateDate: FormControl;
  outsideCreateDateValue: Date;
  outsideFinishDateValue: Date;
  taskOnScreen: Task;
  selectedPriority: string;
  activeTask: Task;
  propertyForm: FormGroup;
  scrollAtTheEnd: boolean;
  bpmnUtil = BpmnUtil;
  expanded = true;
  loadingOpportunity = false;
  userMap: Map<number, string>;
  objectType = CrmObjectType;
  documentsVisible = false;
  selectedTabIndex = 0;
  opportunityToEdit: OpportunitySave;
  opportunityChangedSubscription: Subject<void> = new Subject();
  private componentDestroyed: Subject<void> = new Subject();
  lastTask: Task;
  crmObjectType = CrmObjectType;
  mainCurrency: string;
  loggedAccountId: number;
  notificationId: number;
  showActionButtons = false;
  canSave: boolean;

  readonly priorities = PriorityRadioButtons;
  readonly opportunityPriority = OpportunityPriority;

  private rejectionReasonItems: any[] = [
    {
      title: 'opportunity.form.dialog.rejectionReason.item.none',
      value: OpportunityRejectionReason.NONE,
    },
    {
      title: 'opportunity.form.dialog.rejectionReason.item.lost',
      value: OpportunityRejectionReason.LOST,
    },
    {
      title: 'opportunity.form.dialog.rejectionReason.item.noBudget',
      value: OpportunityRejectionReason.NO_BUDGET,
    },
    {
      title: 'opportunity.form.dialog.rejectionReason.item.noDecision',
      value: OpportunityRejectionReason.NO_DECISION,
    },
    {
      title: 'opportunity.form.dialog.rejectionReason.item.other',
      value: OpportunityRejectionReason.OTHER,
    },
  ];

  readonly getOpportunityErrorTypes: ErrorTypes = {
    base: 'opportunity.details.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'notFound',
      },
      {
        code: FORBIDDEN,
        text: 'forbidden',
      },
    ],
  };

  private readonly deleteOpportunityErrorTypes: ErrorTypes = {
    base: 'opportunity.list.notification.',
    defaultText: 'unknownError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'opportunityNotFound',
      },
      {
        code: FORBIDDEN,
        text: 'noPermission',
      },
    ],
  };

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'opportunity.form.dialog.delete.title',
    description: 'opportunity.form.dialog.delete.description',
    noteFirst: 'opportunity.form.dialog.delete.noteFirstPart',
    noteSecond: 'opportunity.form.dialog.delete.noteSecondPart',
    confirmButton: 'opportunity.form.dialog.delete.confirm',
    cancelButton: 'opportunity.form.dialog.delete.cancel',
  };

  get finishDate(): Date {
    if (this.outsideFinishDateValue) {
      return this.outsideFinishDateValue;
    } else {
      return new Date(this.opportunity.finishDate);
    }
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private opportunityService: OpportunityService,
    private notificationService: NotificationService,
    private deleteDialogService: DeleteDialogService,
    private selectItemDialogService: SelectItemDialogService,
    private personService: PersonService,
    private fb: FormBuilder,
    private $router: Router,
    private configService: GlobalConfigService
  ) {}

  ngOnInit(): void {
    this.subscribeForMainCurrency();
    this.getAccountEmails();
    this.subscribeForOpportunity();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }
  public refresh(): void {
    this.getOpportunity(this.opportunityId);
    this.opportunityDetailsCardComponent.checkIfBothValuesPresentAndInitIfYes();
  }
  public refreshMainInfo(): void {
    this.opportunityDetailsCardComponent.checkIfBothValuesPresentAndInitIfYes();
  }

  openLeadDocumentsView(): void {
    this.$router.navigate(['/opportunity', this.opportunityId, 'documents']);
  }

  getOpportunity(id: number): void {
    this.loadingOpportunity = true;
    this.opportunityService
      .getOpportunity(id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (opportunity: OpportunityDetails) => this.initializeView(opportunity),
        (err) =>
          this.notificationService.notifyError(
            this.getOpportunityErrorTypes,
            err.status
          ),
        () => (this.loadingOpportunity = false)
      );
  }

  submit(): void {
    this.opportunityService
      .postFormProperties(
        this.opportunity.processInstanceId,
        this.propertyForm.value
      )
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () =>
          Object.keys(this.propertyForm.controls).forEach((key) =>
            this.updatePropertyValues(key)
          ),
        () =>
          this.notificationService.notify(
            'opportunity.details.notifications.propertyUpdateError',
            NotificationType.ERROR
          )
      );
  }

  initOpportunitiPriority(priority: string): void {
    this.selectedPriority = OpportunityPriority[priority] || OpportunityPriority.LOW;
  }

  onPriorityChange(priority: string) {
    this.showActionButtons = true;
    this.selectedPriority = priority;
    this.opportunityDetailsCardComponent.changePriority(priority);
  }

  cancelChange(id: string): void {
    this.propertyForm
      .get(id)
      .patchValue(this.taskOnScreen.properties.find((e) => e.id === id).value);
  }

  isScrollVisible(tasksContainer: any): boolean {
    return tasksContainer.scrollWidth !== tasksContainer.clientWidth;
  }

  isEmpty(property: Property): boolean {
    const value = this.taskOnScreen.properties.find((e) => e.id === property.id)
      .value;
    return value === null || value === '';
  }

  isFalse(property: Property): boolean {
    return (
      this.propertyForm.get(property.id) &&
      !this.propertyForm.get(property.id).value
    );
  }

  taskClicked(task: Task): void {
    if (task.id !== this.taskOnScreen.id && !task.isInformationTask) {
      if (task.active) {
        this.taskOnScreen = cloneDeep(this.activeTask);
        this.createPropertyForm();
      } else {
        this.opportunityService
          .getTaskForReadOnly(
            this.opportunity.processInstanceId,
            task.id,
            !task.past
          )
          .pipe(takeUntil(this.componentDestroyed))
          .subscribe((pastTask) => {
            this.taskOnScreen = pastTask;
            this.createPropertyForm();
          });
      }
    }
  }

  tryDeleteOpportunity(id: number): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteOpportunity(id);
    this.deleteDialogService.open(this.deleteDialogData);
  }

  editOpportunity(id: number): void {
    this.opportunityService
      .getOpportunityForEditForSlider(id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (opportunity) => {
          this.opportunityToEdit = opportunity;
          this.editSlider.open();
        },
        (err) =>
          this.notificationService.notifyError(
            this.getOpportunityErrorTypes,
            err.status
          )
      );
  }

  handleOpportunityEdited(): void {
    this.getOpportunity(this.opportunityId);
    this.opportunityDetailsCardComponent.checkIfBothValuesPresentAndInitIfYes();
    const previousId = this.opportunityId;
    this.opportunityId = null;
    setTimeout(() => (this.opportunityId = previousId), 50);
    this.editSlider.close();
  }

  getOwnerNames(opportunity: OpportunityDetails): string {
    let ownerNames = '';
    if (opportunity.ownerOneId) {
      ownerNames += this.userMap.get(opportunity.ownerOneId);
    }
    if (opportunity.ownerTwoId) {
      ownerNames += ', ' + this.userMap.get(opportunity.ownerTwoId);
    }
    if (opportunity.ownerThreeId) {
      ownerNames += ', ' + this.userMap.get(opportunity.ownerThreeId);
    }
    return ownerNames;
  }

  getRejectionReasonKey(type: OpportunityRejectionReason): string {
    const reason = this.rejectionReasonItems.find(
      (item) => item.value === type
    );
    return reason ? reason.title : '';
  }

  private subscribeForOpportunity(): void {
    this.activatedRoute.params
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((params) => {
        this.opportunityId = +params.id;
        this.documentsVisible = this.$router.url.indexOf('/documents') !== -1;
        this.getOpportunity(this.opportunityId);
      });
  }

  private getAccountEmails(): void {
    this.personService.getAndMapAccountEmails(this.componentDestroyed)
      .subscribe(
        (users) => (this.userMap = users),
        () => this.notificationService.notify('opportunity.form.notification.accountEmailsError', NotificationType.ERROR)
      );
  }

  private deleteOpportunity(opportunityId: number): void {
    this.opportunityService
      .removeOpportunity(opportunityId)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => this.handleDeleteSuccess(),
        (err) =>
          this.notificationService.notifyError(
            this.deleteOpportunityErrorTypes,
            err.status
          )
      );
  }


  private parseTaskNumber(taskId: string): number {
    return +taskId.replace('task', '');
  }

  private updatePropertyValues(key: string): void {
    this.taskOnScreen.properties.find(
      (e) => e.id === key
    ).value = this.propertyForm.controls[key].value;
    this.activeTask.properties.find(
      (e) => e.id === key
    ).value = this.propertyForm.controls[key].value;
  }

  private initializeView(opportunity: OpportunityDetails): void {
    this.opportunity = opportunity;
    this.outsideCreateDate = new FormControl(this.opportunity.createDate);
    this.outsideCreateDate.setValidators([Validators.required]);
    this.initOpportunitiPriority(opportunity.priority);
    let isFinishedProcess = false;
    for (const task of opportunity.tasks) {
      if (task.active) {
        this.taskOnScreen = cloneDeep(task);
        this.activeTask = cloneDeep(task);
        if (task.name === '') {
          isFinishedProcess = true;
        }
        break;
      }
    }
    this.lastTask = this.opportunity.tasks[this.opportunity.tasks.length - 2];
    if (this.taskOnScreen) {
      this.createPropertyForm();
      if (!this.opportunity.acceptanceUserId) {
        this.submit();
      }
    }
    if (isFinishedProcess) {
      this.taskClicked(this.lastTask);
    }
    if (this.mainCurrency && this.mainCurrency !== opportunity.currency) {
      this.configService
        .getGlobalParamValue('rateOfMainCurrencyTo' + opportunity.currency)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (param) =>
            (this.opportunity.amountInMainCurrency =
              +param.value * opportunity.amount),
          () =>
            this.notificationService.notify(
              'administration.notifications.getError',
              NotificationType.ERROR
            )
        );
    }
  }

  private createPropertyForm(): void {
    this.propertyForm = this.fb.group({});
    this.taskOnScreen.properties.forEach((field) => this.addControl(field));
  }

  private handleDeleteSuccess(): void {
    this.notificationService.notify(
      'opportunity.list.notification.deleteSuccess',
      NotificationType.SUCCESS
    );
    this.$router.navigate(['/opportunity']);
  }

  private addControl(field: Property): void {
    if (field.type !== 'BOOLEAN') {
      this.propertyForm.addControl(
        field.id,
        this.fb.control(
          {
            value: field.value,
            disabled:
              this.activeTask.id !== this.taskOnScreen.id ||
              this.opportunity.acceptanceUserId,
          },
          field.required ? [Validators.required] : []
        )
      );
    } else {
      this.propertyForm.addControl(
        field.id,
        this.fb.control(
          {
            value: field.value === 'true',
            disabled:
              this.activeTask.id !== this.taskOnScreen.id ||
              this.opportunity.acceptanceUserId,
          },
          field.required ? [Validators.requiredTrue] : []
        )
      );
    }
  }

  handleDetailsChanged(opportunityDetails: OpportunitySave): void {
    this.showActionButtons = true;
    if (opportunityDetails && opportunityDetails.finishDate) {
      this.outsideFinishDateValue = new Date(opportunityDetails.finishDate);
    }
  }

  createDateSelected($event: any): void {
    this.outsideCreateDateValue = new Date(this.outsideCreateDate.value);
    this.handleDetailsChanged(this.opportunityToEdit);
    setTimeout(() => (this.outsideCreateDateValue = null), 500);
  }

  handleOpportunityChangedSubscription(): void {
    this.opportunityChangedSubscription.next();
  }

  discardOpportunityDetailsChanges(): void {
    this.canSave = false;
    this.showActionButtons = false;
    this.initOpportunitiPriority(this.opportunity.priority);
    setTimeout(() => (this.canSave = null), 500);
  }

  saveOpportunityDetailsChanges(): void {
    this.canSave = true;
    this.showActionButtons = false;
    setTimeout(() => (this.canSave = null), 500);
  }

  private subscribeForMainCurrency(): void {
    this.configService
      .getGlobalParamValue('systemCurrency')
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        (param) => (this.mainCurrency = param.value),
        () =>
          this.notificationService.notify(
            'administration.notifications.getError',
            NotificationType.ERROR
          )
      );
  }

  isTasksCompleted(): boolean {
    if (this.opportunity) {
      const lastTaskIndex = this.opportunity.tasks.length - 1;
      const lastTask = this.opportunity.tasks[lastTaskIndex];
      return lastTask.active;
    }
    return true;
  }

  isTaskError(task: Task): boolean {
    return task.statusAcceptance === StatusAcceptance.DECLINED;
  }
}
