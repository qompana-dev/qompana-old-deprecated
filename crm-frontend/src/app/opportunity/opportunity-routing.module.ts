import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../login/auth/auth-guard.service';
import {OpportunityListComponent} from './opportunity-list/opportunity-list.component';
import {OpportunityDetailsComponent} from './opportunity-details/opportunity-details.component';

const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: OpportunityListComponent},
  {path: ':id', canActivate: [AuthGuard], component: OpportunityDetailsComponent},
  {path: ':id/documents', canActivate: [AuthGuard], component: OpportunityDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OpportunityRoutingModule {
}
