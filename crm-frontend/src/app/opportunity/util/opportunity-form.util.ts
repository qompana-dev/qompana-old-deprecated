import {AbstractControl} from '@angular/forms';
import * as cloneDeep from 'lodash/cloneDeep';

export class OpportunityFormUtil {
  static probabilityChange(control: AbstractControl, value: string): void {
    if (value && (value + '').length > 1 && (value + '')[0] === '0') {
      const val = cloneDeep(value);
      setTimeout(() => control.patchValue(val.slice(1, value.length)), 100);
    }
    if (value && (value + '').length >= 3 && +(value + '') > 100) {
      setTimeout(() => control.patchValue('100'), 100);
    }
  }
}
