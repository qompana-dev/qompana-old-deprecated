import {Injectable} from '@angular/core';
import {FilterStrategy} from '../shared/model';
import {Page, PageRequest} from '../shared/pagination/page.model';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {TableUtil} from '../shared/table.util';
import {
  KanbanOpportunityProcessOverview,
  KanbanOpportunityTaskDetails,
  OpportunityDetails,
  OpportunityFinishDto,
  OpportunityOnList,
  OpportunitySave
} from './opportunity.model';
import {CrmObject} from '../shared/model/search.model';
import {Task} from '../lead/lead-details/lead-details.model';
import {getHeaderForAuthType} from '../shared/permissions-utils';
import {UrlUtil} from '../shared/url.util';
import {KanbanProcessOverview} from "../lead/lead.model";
import {ProcessDefinition} from "../bpmn/bpmn.model";

@Injectable({
  providedIn: 'root'
})
export class OpportunityService {
  private opportunityUrl = `${UrlUtil.url}/crm-business-service/opportunity`;

  constructor(private http: HttpClient) {
  }

  public getOpportunities(filterStrategy: FilterStrategy, pageRequest: PageRequest, selectedSearch?: string,
                          value?: string): Observable<Page<OpportunityOnList>> {
    let search = '';
    if (selectedSearch !== null) {
      search = selectedSearch;
    }
    let params = new HttpParams({fromObject: {...pageRequest, search}});
    params = TableUtil.enrichParamsWithFilterStrategy(filterStrategy, params, value);
    return this.http.get<Page<OpportunityOnList>>(this.opportunityUrl, {params});
  }

  public getOpportunity(opportunityId: number): Observable<OpportunityDetails> {
    const url = `${this.opportunityUrl}/${opportunityId}`;
    return this.http.get<OpportunityDetails>(url);
  }

  public getTaskForReadOnly(processInstanceId: string, taskId: string, futureTask: boolean): Observable<Task> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/process-instance/${processInstanceId}/task/${taskId}?futureTask=${futureTask}`;
    return this.http.get<Task>(url, {headers: getHeaderForAuthType('opportunity')});
  }

  public createOpportunity(opportunity: OpportunitySave): Observable<OpportunitySave> {
    return this.http.post<OpportunitySave>(this.opportunityUrl, opportunity);
  }

  public getOpportunityForEditForSlider(opportunityId: number): Observable<OpportunitySave> {
    const url = `${this.opportunityUrl}/for-edit/${opportunityId}`;
    return this.http.get<OpportunitySave>(url, {headers: getHeaderForAuthType('slider')});
  }

  public getOpportunityForEditForDetailsCard(opportunityId: number): Observable<OpportunitySave> {
    const url = `${this.opportunityUrl}/for-edit/${opportunityId}`;
    return this.http.get<OpportunitySave>(url, {headers: getHeaderForAuthType('detailsCard')});
  }

  public getProcessDefinition(processDefinitionId: string): Observable<ProcessDefinition> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/repository/process-definition/${processDefinitionId}`;
    return this.http.get<ProcessDefinition>(url);
  }


  public postFormProperties(id: string, params: { [key: string]: { value: string } }): Observable<any> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/process-instance/${id}/form`;
    return this.http.post(url, params);
  }

  public goBack(processInstanceId: string, numberOfSteps: number): Observable<any> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/task/${processInstanceId}/go-back/${numberOfSteps}`;
    return this.http.get(url, {headers: getHeaderForAuthType('opportunity')});
  }

  public finishOpportunity(opportunityId: number, opportunityFinishDto: OpportunityFinishDto): Observable<any> {
    const url = `${UrlUtil.url}/crm-business-service/opportunity/${opportunityId}/finish`;
    return this.http.put(url, opportunityFinishDto);
  }

  public completeTask(processInstanceId: string): Observable<any> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/task/${processInstanceId}/complete`;
    return this.http.get(url, {observe: 'response', headers: getHeaderForAuthType('opportunity')});
  }

  public updateOpportunity(opportunityId: number, opportunity: OpportunitySave): Observable<OpportunitySave> {
    const url = `${this.opportunityUrl}/${opportunityId}`;
    return this.http.put<OpportunitySave>(url, opportunity, {headers: getHeaderForAuthType('slider')});
  }

  public updateOpportunityFromDetailsCard(opportunityId: number, opportunity: OpportunitySave): Observable<OpportunitySave> {
    const url = `${this.opportunityUrl}/${opportunityId}`;
    return this.http.put<OpportunitySave>(url, opportunity, {headers: getHeaderForAuthType('detailsCard')});
  }

  public removeOpportunity(opportunityId: number): Observable<void> {
    const url = `${this.opportunityUrl}/${opportunityId}`;
    return this.http.delete<void>(url);
  }

  public copyOpportunity(opportunityId: number): Observable<void> {
    const url = `${this.opportunityUrl}/copy/${opportunityId}`;
    return this.http.post<void>(url, null);
  }

  public removeOpportunities(opportunities: OpportunityOnList[]): Observable<void> {
    const params = new HttpParams().set('ids', opportunities.map(o => o.id).join(','));
    return this.http.delete<void>(this.opportunityUrl, {params});
  }


  public finishOpportunities(opportunities: OpportunityOnList[], opportunityFinishDto: OpportunityFinishDto): Observable<void> {
    const params = new HttpParams().set('ids', opportunities.map(o => o.id).join(','));
    return this.http.put<void>(`${this.opportunityUrl}/finish`, opportunityFinishDto, {params});
  }


  public accept(notificationDto: { userNotificationId: number, comment?: string }): Observable<any> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/accept-task`;
    return this.http.post(url, notificationDto);
  }

  public decline(notificationDto: { userNotificationId: number, comment?: string }): Observable<any> {
    const url = `${UrlUtil.url}/crm-business-service/process-engine/decline-task`;
    return this.http.post(url, notificationDto);
  }

  getOpportunitiesForSearch(input: string): Observable<CrmObject[]> {
    return this.http.get<CrmObject[]>(`${UrlUtil.url}/crm-business-service/opportunity/search?term=${input}`);
  }

  getOpportunitiesByIds(leadIds: number[]): Observable<CrmObject[]> {
    if (!leadIds || leadIds.length === 0) {
      return of([]);
    }
    const params = new HttpParams().set('ids', leadIds.join(','));
    return this.http.get<CrmObject[]>(`${UrlUtil.url}/crm-business-service/opportunity/crm-object`, {params});
  }

  public getProcessKanbanOverview(filterStrategy: FilterStrategy, search: string): Observable<KanbanOpportunityProcessOverview[]> {
    let params = new HttpParams().set('filterStrategy', filterStrategy).set('search', search);
    const url = `${UrlUtil.url}/crm-business-service/kanban-processes/opportunity`;
    return this.http.get<KanbanOpportunityProcessOverview[]>(url, {params});
  }

  public getProcessKanbanDetails(processDefinitionId: string, filterStrategy: FilterStrategy, search: string): Observable<KanbanOpportunityTaskDetails[]> {
    // const url = `/assets/mocks/lead-canban-details.json`;
    let params = new HttpParams().set('filterStrategy', filterStrategy).set('search', search);
    const url = `${UrlUtil.url}/crm-business-service/kanban-processes/opportunity/${processDefinitionId}`;
    return this.http.get<KanbanOpportunityTaskDetails[]>(url, {params});
  }


}
