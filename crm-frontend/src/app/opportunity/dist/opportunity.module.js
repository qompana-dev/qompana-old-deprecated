"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.OpportunityModule = void 0;
var core_1 = require("@angular/core");
var opportunity_list_component_1 = require("./opportunity-list/opportunity-list.component");
var common_1 = require("@angular/common");
var shared_module_1 = require("../shared/shared.module");
var opportunity_routing_module_1 = require("./opportunity-routing.module");
var opportunity_details_component_1 = require("./opportunity-details/opportunity-details.component");
var ngx_mask_1 = require("ngx-mask");
var widget_module_1 = require("../widgets/widget.module");
var tabs_module_1 = require("../shared/tabs/tabs.module");
var document_module_1 = require("../documents/document.module");
var opportunity_details_card_component_1 = require("./opportunity-details/opportunity-details-card/opportunity-details-card.component");
var opportunity_products_card_component_1 = require("./opportunity-details/opportunity-products-card/opportunity-products-card.component");
var manual_product_add_component_1 = require("./opportunity-details/opportunity-products-card/manual-product-add/manual-product-add.component");
var opportunity_product_edit_component_1 = require("./opportunity-details/opportunity-products-card/opportunity-product-edit/opportunity-product-edit.component");
var categories_module_1 = require("../product/categories/categories.module");
var add_product_from_price_book_component_1 = require("./opportunity-details/opportunity-products-card/add-product-from-price-book/add-product-from-price-book.component");
var opportunity_slider_module_1 = require("./opportunity-slider/opportunity-slider.module");
var customer_slider_module_1 = require("../customer/customer-slider/customer-slider.module");
var expenses_module_1 = require("../shared/tabs/expense-tab/expenses.module");
var offer_module_1 = require("../offer/offer.module");
var contact_slider_module_1 = require("../contacts/contact-slider/contact-slider.module");
var OpportunityModule = /** @class */ (function () {
    function OpportunityModule() {
    }
    OpportunityModule = __decorate([
        core_1.NgModule({
            declarations: [
                opportunity_list_component_1.OpportunityListComponent,
                opportunity_details_component_1.OpportunityDetailsComponent,
                opportunity_details_card_component_1.OpportunityDetailsCardComponent,
                opportunity_products_card_component_1.OpportunityProductsCardComponent,
                manual_product_add_component_1.ManualProductAddComponent,
                add_product_from_price_book_component_1.AddProductFromPriceBookComponent,
                opportunity_product_edit_component_1.OpportunityProductEditComponent
            ],
            imports: [
                common_1.CommonModule,
                shared_module_1.SharedModule,
                opportunity_routing_module_1.OpportunityRoutingModule,
                widget_module_1.WidgetModule,
                tabs_module_1.TabsModule,
                ngx_mask_1.NgxMaskModule,
                document_module_1.DocumentModule,
                categories_module_1.CategoriesModule,
                opportunity_slider_module_1.OpportunitySliderModule,
                customer_slider_module_1.CustomerSliderModule,
                expenses_module_1.ExpensesModule,
                offer_module_1.OfferModule,
                contact_slider_module_1.ContactSliderModule
            ],
            entryComponents: [opportunity_product_edit_component_1.OpportunityProductEditComponent]
        })
    ], OpportunityModule);
    return OpportunityModule;
}());
exports.OpportunityModule = OpportunityModule;
