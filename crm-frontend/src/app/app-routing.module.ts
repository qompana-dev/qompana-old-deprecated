import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {MainPageComponent} from './main-page/main-page.component';
import {AuthGuard} from './login/auth/auth-guard.service';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'home', canActivate: [AuthGuard], component: MainPageComponent},
  {
    path: 'administration',
    canActivate: [AuthGuard],
    loadChildren: './global-config/global-config.module#GlobalConfigModule'
  },
  {
    path: 'bpmn',
    canActivate: [AuthGuard],
    loadChildren: './bpmn/bpmn.module#BpmnModule'
  },
  {
    path: 'dashboard',
    canActivate: [AuthGuard],
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'map',
    loadChildren: './map/map.module#MapModule'
  },
  {
    path: 'password-reset',
    loadChildren: './login/reset-password/reset-password.module#ResetPasswordModule'
  },
  {
    path: 'calendar',
    canActivate: [AuthGuard],
    loadChildren: './calendar/calendars.module#CalendarsModule'
  },
  {
    path: 'token',
    loadChildren: './token/token.module#TokenModule'
  },
  {
    path: 'gdpr-token',
    loadChildren: './global-config/gdpr-config/gdpr-token-view/gdpr-token-view.module#GdprTokenViewModule'
  },
  {
    path: 'documents',
    canActivate: [AuthGuard],
    loadChildren: './documents/document.module#DocumentModule'
  },
  {
    path: 'contacts',
    canActivate: [AuthGuard],
    loadChildren: './contacts/contact.module#ContactModule'
  },
  {
    path: 'customers',
    canActivate: [AuthGuard],
    loadChildren: './customer/customer.module#CustomerModule'
  },
  {
    path: 'person',
    loadChildren: './person/person.module#PersonModule'
  },
  {
    path: 'opportunity',
    loadChildren: './opportunity/opportunity.module#OpportunityModule'
  },
  {
    path: 'products',
    canActivate: [AuthGuard],
    loadChildren: './product/product.module#ProductModule'
  },
  {
    path: 'leads',
    canActivate: [AuthGuard],
    loadChildren: './lead/lead.module#LeadModule'
  },
  {
    path: 'goals',
    canActivate: [AuthGuard],
    loadChildren: './goal/goal.module#GoalModule'
  },
  {
    path: 'price-book',
    canActivate: [AuthGuard],
    loadChildren: './price-book/price-book.module#PriceBookModule'
  },
  {
    path: 'manufacturers-and-suppliers',
    canActivate: [AuthGuard],
    loadChildren: './manufacturers-and-suppliers/manufacturers-and-suppliers.module#ManufacturersAndSuppliersModule'
  },
  {
    path: 'mail',
    canActivate: [AuthGuard],
    loadChildren: './mail/mail.module#MailModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
