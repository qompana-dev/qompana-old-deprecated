import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {GlobalConfigService} from '../global-config.service';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-expenses-and-currencies',
  templateUrl: './expenses-and-currencies.component.html',
  styleUrls: ['./expenses-and-currencies.component.scss']
})
export class ExpensesAndCurrenciesComponent implements OnInit, OnDestroy {

  expenseAcceptanceThreshold: FormControl;
  systemCurrency: FormControl;
  exchangeRateControlsMap: Map<string, FormControl> = new Map<string, FormControl>();
  exchangeRateControlsMapKeys = ['rateOfMainCurrencyToPLN', 'rateOfMainCurrencyToEUR', 'rateOfMainCurrencyToUSD',
    'rateOfMainCurrencyToGBP', 'rateOfMainCurrencyToCNY'];
  currencies = [];
  private allParams = ['systemCurrency', 'expenseAcceptanceThreshold', 'rateOfMainCurrencyToPLN', 'rateOfMainCurrencyToEUR',
    'rateOfMainCurrencyToUSD', 'rateOfMainCurrencyToGBP', 'rateOfMainCurrencyToCNY'];
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private configService: GlobalConfigService,
              private notificationService: NotificationService) {
    this.expenseAcceptanceThreshold = new FormControl('', Validators.compose([Validators.required, Validators.min(0)]));
    this.systemCurrency = new FormControl('', Validators.required);

    this.exchangeRateControlsMap.set('rateOfMainCurrencyToPLN', new FormControl('', Validators.compose(
        [Validators.required, Validators.min(0)])));
    this.exchangeRateControlsMap.set('rateOfMainCurrencyToEUR', new FormControl('', Validators.compose(
        [Validators.required, Validators.min(0)])));
    this.exchangeRateControlsMap.set('rateOfMainCurrencyToUSD', new FormControl('', Validators.compose(
        [Validators.required, Validators.min(0)])));
    this.exchangeRateControlsMap.set('rateOfMainCurrencyToGBP', new FormControl('', Validators.compose(
        [Validators.required, Validators.min(0)])));
    this.exchangeRateControlsMap.set('rateOfMainCurrencyToCNY', new FormControl('', Validators.compose(
        [Validators.required, Validators.min(0)])));
  }

  ngOnInit(): void {
    this.subscribeForCurrencies();
    for (const paramName of this.allParams) {
      this.configService.getGlobalParamValue(paramName).pipe(
          takeUntil(this.componentDestroyed)
      ).subscribe(
          (param) => this.getFormControlBasedOnParamName(paramName).patchValue(param.value),
          () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
      );
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(paramName: string): void {
    const formControl = this.getFormControlBasedOnParamName(paramName);
    if (!formControl.valid) {
      formControl.markAsTouched();
      return;
    }
    this.configService.setGlobalParam(paramName, formControl.value).pipe(
        takeUntil(this.componentDestroyed)
    ).subscribe(
        () => this.notificationService.notify('administration.notifications.saveSuccess', NotificationType.SUCCESS),
        () => this.notificationService.notify('administration.notifications.saveError', NotificationType.ERROR)
    );
  }

  getFormControlBasedOnParamName(paramName: string): FormControl {
    switch (paramName) {
      case 'expenseAcceptanceThreshold':
        return this.expenseAcceptanceThreshold;
      case 'systemCurrency':
        return this.systemCurrency;
      default:
        return this.exchangeRateControlsMap.get(paramName);
    }
  }

  private subscribeForCurrencies(): void {
    this.configService.getGlobalParamValue('currencies').pipe(
        takeUntil(this.componentDestroyed)
    ).subscribe(
        (param) => this.currencies = param.value.split(';'),
        () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }
}
