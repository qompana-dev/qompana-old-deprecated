import {AddressForPreparedByDto, AddressForPreparedForDto, OfferProductDto, OfferType} from '../offer/offer.model';

export interface GlobalConfiguration {
  id: number;
  key: string;
  value: string;
}

export interface CompanyConfig {
  base64EncodedLogo: string; // added to offer
  preparedBy: AddressForPreparedByDto; // added to offer

  longitude: string;
  latitude: string;
}
