import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {GdprFormConfigDto, GdprMailConfigDto} from '../gdpr.model';
import {GdprConfigService} from '../gdpr-config.service';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {takeUntil} from 'rxjs/operators';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-mail-content',
  templateUrl: './mail-content.component.html',
  styleUrls: ['./mail-content.component.scss']
})
export class MailContentComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();

  gdprMailConfigDto: GdprMailConfigDto;
  title = new FormControl('');
  html: string;
  variables: string[] = [];

  variableToInsert: string;

  constructor(private gdprConfigService: GdprConfigService,
              private cdRef: ChangeDetectorRef,
              private notificationService: NotificationService) { }

  ngOnInit() {
    this.getMailConfig();
    this.getAvailableVariables();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  save(): void {
    if (this.title.valid && this.html.length > 0) {
      this.gdprConfigService.saveGdprMailConfig({mailTitle: this.nullToEmpty(this.title.value), mailContent: this.nullToEmpty(this.html)})
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe((formConfig: GdprMailConfigDto) => {
          this.notificationService.notify('administration.gdpr.mail.notification.mailConfigurationSavedSuccessfully', NotificationType.SUCCESS)
          this.gdprMailConfigDto = formConfig;
          this.setValuesToForm();
        }, () => this.notificationService.notify('administration.gdpr.mail.notification.savingMailConfigurationError', NotificationType.ERROR));
    } else {
      this.title.markAsTouched();
    }
  }

  private getAvailableVariables(): void {
    this.gdprConfigService.getAvailableGdprEmailVariables()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((variables: string[]) => this.variables = variables,
        () => this.notificationService.notify('administration.gdpr.mail.notification.getAvailableVariablesError', NotificationType.ERROR));
  }

  private getMailConfig(): void {
    this.gdprConfigService.getGdprMailConfig()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((formConfig: GdprMailConfigDto) => {
        this.gdprMailConfigDto = formConfig;
        this.setValuesToForm();
      }, () => this.notificationService.notify('administration.gdpr.mail.notification.getMailConfigError', NotificationType.ERROR));
  }

  private setValuesToForm(): void {
    if (this.gdprMailConfigDto) {
      this.title.patchValue(this.gdprMailConfigDto.mailTitle);
      this.html = this.gdprMailConfigDto.mailContent;
    }
  }

  insertVariable(variable: string) {
    this.variableToInsert = undefined;
    setTimeout(() => {
      this.variableToInsert = '{{' + variable + '}}';
      this.cdRef.detectChanges();
    }, 500);
  }

  nullToEmpty(text: string): string {
    return (!text) ? '' : text;
  }
}
