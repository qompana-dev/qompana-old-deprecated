export interface GdprApproveDto {
  id: number;
  gdprStatus: GdprStatusDto;
  gdprType: GdprTypeDto;
  gdprApproveWay: GdprAgreeWayDto;

  changedBy: number;
  modified: string;

  gdprStatusId?: number;
  gdprApproveWayId?: number;
}

export interface GdprShareDto {
  type: GdprTypeDto;
  checked: boolean;
  approve?: GdprApproveDto;
}

export interface GdprTypeDto {
  id: number;
  name: string;
  description: string;
  isMailType: boolean;
}

export interface GdprStatusDto {
  id: number;
  name: string;
  modifiable: boolean;
  defaultStatus: string;
}

export const defaultStatuses = [
  {key: 'NONE', value: 'administration.gdpr.status.defStatusesEnum.none'},
  {key: 'CHECKED', value: 'administration.gdpr.status.defStatusesEnum.checked'},
  {key: 'UNCHECKED', value: 'administration.gdpr.status.defStatusesEnum.unchecked'}
];

export interface GdprApproveUpdateDto {
  id: number;
  gdprStatusId: number;
  gdprApproveWayId: number;
}

export interface GdprAgreeWayDto {
  id: number;
  name: string;
  modifiable: boolean;
  isDefaultWay: boolean;
}

export interface EnumItem<E> {
  id: E;
  value: keyof E;
}

export enum GdprDefaultStatus {
  NONE = 'administration.gdpr.status.defaultStatus.none',
  CHECKED = 'administration.gdpr.status.defaultStatus.checked',
  UNCHECKED = 'administration.gdpr.status.defaultStatus.unchecked'
}

export type GdprSimpleSettings = GdprTypeDto | GdprStatusDto | GdprAgreeWayDto;

export interface GdprFormConfigDto {
  formContent: string;
}

export interface GdprMailConfigDto {
  mailTitle: string;
  mailContent: string;
}

export interface FormDataDto {
  contactId: number;
  formContent: string;
  token: string;
  types: FormDataGdprTypeDto[];
}

export interface FormDataGdprTypeDto {
  gdprTypeId: number;
  name: string;
  description: string;
  checked: boolean;
}

export interface FormDataApproveUpdateDto {
  gdprTypeId: number;
  checked: boolean;
}

export interface GdprTokenInfoDto {
  token: string;
  tokenExpirationDate: string;
}

export interface EmailDataDto {
  contactId: number;
  token: string;
  emailSubject: string;
  emailContent: string;
}
