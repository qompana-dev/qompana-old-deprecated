import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SliderService} from '../../../shared/slider/slider.service';
import {NotificationService} from '../../../shared/notification.service';
import {GdprConfigService} from '../gdpr-config.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {defaultStatuses, GdprSimpleSettings} from '../gdpr.model';
import {FormValidators} from '../../../shared/form/form-validators';

@Component({
  selector: 'app-add-form',
  templateUrl: './gdpr-add-form.component.html',
  styleUrls: ['./gdpr-add-form.component.scss']
})
export class GdprAddFormComponent implements OnInit {

  @Input() set inputData(inputData: GdprSimpleSettings) {
    if (inputData) {
      this.editedData = inputData;
      this.editMode = true;
      this.inputForm.reset(inputData);
      this.showTitleField = !inputData['isMailType']; // only in GdprTypeDto
    } else {
      this.showTitleField = true;
    }
  }

  @Input() set operationType(operationType: 'AGREES' | 'APPROVE_WAY' | 'STATUSES') {
    if (operationType) {
      switch (operationType) {
        case 'AGREES':
          this.prefix = 'agree';
          this.showDescriptionField = true;
          break;
        case 'APPROVE_WAY':
          this.prefix = 'approveWay';
          break;
        case 'STATUSES':
          this.showDefaultStatusOptionField = true;
          this.prefix = 'status';
          break;
        default:
          break;
      }
    }
  }

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onSave: EventEmitter<any> = new EventEmitter<any>();

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onSliderClose: EventEmitter<void> = new EventEmitter<void>();
  inputForm: FormGroup;
  showTitleField = true;
  showDescriptionField = false;
  showDefaultStatusOptionField = false;
  prefix: string;
  editMode = false;
  editedData: GdprSimpleSettings;
  defStatus = defaultStatuses;



  constructor(private sliderService: SliderService,
              private notificationService: NotificationService,
              private gdprConfigService: GdprConfigService,
              private fb: FormBuilder) {
    this.createDataForm();
  }

  get name(): AbstractControl {
    return this.inputForm.get('name');
  }

  get description(): AbstractControl {
    return this.inputForm.get('description');
  }

  get defaultStatus(): AbstractControl {
    return this.inputForm.get('defaultStatus');
  }

  ngOnInit(): void {
  }

  save(): void {
    this.onSave.emit(this.inputForm.value);
    this.inputForm.reset();
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
    this.inputForm.reset();
  }



private createDataForm(): void {
    this.inputForm = this.fb.group({
      name: [null, [Validators.required, FormValidators.whitespace, Validators.maxLength(200)]],
      description: [null, [Validators.required]],
      defaultStatus: ['NONE', [Validators.required]]
    });
  }

}
