import {Component, OnInit, ViewChild} from '@angular/core';
import {defaultStatuses, GdprStatusDto, GdprTypeDto} from '../gdpr.model';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {GdprConfigService} from '../gdpr-config.service';
import cloneDeep from 'lodash/cloneDeep';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {GdprAddFormComponent} from '../gdpr-add-form/gdpr-add-form.component';

@Component({
  selector: 'app-statuses',
  templateUrl: './statuses.component.html',
  styleUrls: ['./statuses.component.scss']
})
export class StatusesComponent implements OnInit {

  @ViewChild('addSlider') addSlider: SliderComponent;
  @ViewChild('editSlider') editSlider: SliderComponent;

  @ViewChild('editForm') editForm: GdprAddFormComponent;

  readonly filteredColumns: string[] = ['name', 'defaultStatus', 'action'];
  private componentDestroyed: Subject<void> = new Subject();
  private editedGdprStatus: GdprStatusDto;
  public statuses: GdprStatusDto[];

  readonly errorTypes = {
    base: 'administration.gdpr.notification.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: 409,
        text: 'nameOccupied'
      }
    ]
  };

  constructor(private gdprConfigService: GdprConfigService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.getAllStatuses();
  }

  getAllStatuses(): void {
    this.gdprConfigService.getAllGdprStatuses('systemSettings').pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      statuses => this.statuses = statuses
    );
  }

  translateDefaultStatus(key: string): string {
    const item = defaultStatuses.find(elm => elm.key === key);
    return item.value;
  }

  editStatus(gdprStatusDto: GdprStatusDto): void {
    this.editedGdprStatus = cloneDeep(gdprStatusDto);
    this.editSlider.open();
    this.editForm.inputData = this.editedGdprStatus;
  }

  saveStatus(gdprStatusDto: GdprStatusDto): void {
    this.gdprConfigService.saveGdprStatus(gdprStatusDto).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        if (this.editSlider.opened) {
          this.editSlider.close();
        } else if (this.addSlider.opened) {
          this.addSlider.close();
        }
        this.notifySuccess('administration.gdpr.status.notification.saved');
        this.getAllStatuses();
      },
      (err) => this.notificationService.notifyError(this.errorTypes, err.status)
    );
  }

  tryDeleteStatus(gdprStatusId: number): void {
    this.gdprConfigService.deleteGdprStatus(gdprStatusId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.notifySuccess('administration.gdpr.status.notification.removed');
        this.getAllStatuses();
      }
    );
  }

  updateStatus(gdprStatusDto: GdprStatusDto): void {
    this.editedGdprStatus.name = gdprStatusDto.name;
    this.editedGdprStatus.defaultStatus = gdprStatusDto.defaultStatus;
    this.gdprConfigService.updateGdprStatus(this.editedGdprStatus).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        if (this.editSlider.opened) {
          this.editSlider.close();
        } else if (this.addSlider.opened) {
          this.addSlider.close();
        }
        this.notifySuccess('administration.gdpr.status.notification.updated');
        this.getAllStatuses();
      },
      (err) => this.notificationService.notifyError(this.errorTypes, err.status)
    );
  }

  private notifySuccess(key: string): void {
    this.notificationService.notify(key, NotificationType.SUCCESS);
  }

}
