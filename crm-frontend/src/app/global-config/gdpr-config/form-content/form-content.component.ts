import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {GdprConfigService} from '../gdpr-config.service';
import {GdprFormConfigDto} from '../gdpr.model';
import {takeUntil} from 'rxjs/operators';
import {NotificationService, NotificationType} from '../../../shared/notification.service';

@Component({
  selector: 'app-form-content',
  templateUrl: './form-content.component.html',
  styleUrls: ['./form-content.component.scss']
})
export class FormContentComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();

  gdprFormConfigDto: GdprFormConfigDto;
  html: string;
  variables: string[] = [];

  variableToInsert: string;

  constructor(private gdprConfigService: GdprConfigService,
              private cdRef: ChangeDetectorRef,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.getFormConfig();
    this.getAvailableVariables();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  save(): void {
    this.gdprConfigService.saveGdprFormConfig({formContent: ((this.html) ? this.html : '')})
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((formConfig: GdprFormConfigDto) => {
        this.notificationService.notify('administration.gdpr.form.notification.formConfigurationSavedSuccessfully', NotificationType.SUCCESS)
        this.gdprFormConfigDto = formConfig;
        this.setValuesToForm();
      }, () => this.notificationService.notify('administration.gdpr.form.notification.savingFormConfigurationError', NotificationType.ERROR));
  }

  private getAvailableVariables(): void {
    this.gdprConfigService.getAvailableGdprFormVariables()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((variables: string[]) => this.variables = variables,
        () => this.notificationService.notify('administration.gdpr.form.notification.getAvailableVariablesError', NotificationType.ERROR));
  }

  private getFormConfig(): void {
    this.gdprConfigService.getGdprFormConfig()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((formConfig: GdprFormConfigDto) => {
        this.gdprFormConfigDto = formConfig;
        this.setValuesToForm();
      }, () => this.notificationService.notify('administration.gdpr.form.notification.getFormConfigError', NotificationType.ERROR));
  }

  private setValuesToForm(): void {
    if (this.gdprFormConfigDto) {
      this.html = this.gdprFormConfigDto.formContent;
    }
  }

  insertVariable(variable: string) {
    this.variableToInsert = undefined;
    setTimeout(() => {
      this.variableToInsert = '{{' + variable + '}}';
      this.cdRef.detectChanges();
    }, 500);
  }
}
