import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GdprTokenViewComponent} from './gdpr-token-view.component';

const routes: Routes = [
  {path: ':token', component: GdprTokenViewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GdprTokenViewRoutingModule {
}
