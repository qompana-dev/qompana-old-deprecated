import {NgModule} from '@angular/core';
import {GdprTokenViewComponent} from './gdpr-token-view.component';
import {SharedModule} from '../../../shared/shared.module';
import {GdprTokenViewRoutingModule} from './gdpr-token-view-routing.module';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    GdprTokenViewRoutingModule
  ],
  declarations: [
    GdprTokenViewComponent
  ]
})
export class GdprTokenViewModule {
}
