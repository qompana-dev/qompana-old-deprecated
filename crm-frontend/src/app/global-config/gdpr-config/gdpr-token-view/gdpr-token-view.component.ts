import {Component, OnDestroy, OnInit} from '@angular/core';
import {LoginService} from '../../../login/login.service';
import {GdprConfigService} from '../gdpr-config.service';
import {Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {FormDataApproveUpdateDto, FormDataDto, FormDataGdprTypeDto} from '../gdpr.model';
import {ErrorTypes, NotificationService, NotificationType} from '../../../shared/notification.service';
import {BAD_REQUEST, NOT_FOUND} from 'http-status-codes';

@Component({
  selector: 'app-gdpr-token-view',
  templateUrl: './gdpr-token-view.component.html',
  styleUrls: ['./gdpr-token-view.component.scss']
})
export class GdprTokenViewComponent implements OnInit, OnDestroy {
  private componentDestroyed: Subject<void> = new Subject();
  private token: string;
  formData: FormDataDto;

  readonly errorTypes: ErrorTypes = {
    base: 'gdprToken.notification.',
    defaultText: 'tokenNotFound',
    errors: [
      {
        code: BAD_REQUEST,
        text: 'tokenExpired'
      }, {
        code: NOT_FOUND,
        text: 'tokenNotFound'
      }
    ]
  };

  formParts: FormPart[] = [];

  constructor(private activatedRoute: ActivatedRoute,
              private loginService: LoginService,
              private $router: Router,
              private notificationService: NotificationService,
              private gdprConfigService: GdprConfigService) {
  }

  ngOnInit() {
    this.loginService.logoutWithoutRedirect();
    this.activatedRoute.params
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((param) => {
        this.token = param.token;
        this.getGdprTokenInfo();
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  saveAndSend(): void {
    const toSend: FormDataApproveUpdateDto[] = this.formData.types.map((type: FormDataGdprTypeDto): FormDataApproveUpdateDto => {
      return {gdprTypeId: type.gdprTypeId, checked: type.checked};
    });
    this.gdprConfigService.updateGdprApprovalListForToken(this.token, toSend)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((formData: FormDataDto) => {
        this.formData = formData;
        this.setFormParts();
        this.notificationService.notify('gdprToken.notification.gdprApprovalsUpdateSuccessfully', NotificationType.SUCCESS);
      }, (error) => this.notificationService.notifyError(this.errorTypes, error.status));
  }

  private getGdprTokenInfo(): void {
    this.gdprConfigService.getFormDataForToken(this.token)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((formData: FormDataDto) => {
        this.formData = formData;
        this.setFormParts();
      }, (error) => {
        this.notificationService.notifyError(this.errorTypes, error.status);
        this.goToHomePage();
      });
  }

  private setFormParts(): void {
    const key = '{{GDPR_LIST}}';
    this.formParts = [];
    const parts: string[] = this.formData.formContent.split(key);
    for (let i = 0; i < parts.length; i++) {
      if (i !== 0) {
        this.formParts.push({isText: false});
      }
      this.formParts.push({isText: true, text: parts[i]});
    }
  }

  private goToHomePage(): void {
    this.$router.navigate(['']);
  }

}

export interface FormPart {
  isText: boolean;
  text?: string;
}
