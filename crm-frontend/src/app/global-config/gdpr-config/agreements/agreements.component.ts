import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {GdprTypeDto} from '../gdpr.model';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {GdprConfigService} from '../gdpr-config.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {GdprAddFormComponent} from '../gdpr-add-form/gdpr-add-form.component';
import cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-agreements',
  templateUrl: './agreements.component.html',
  styleUrls: ['./agreements.component.scss']
})
export class AgreementsComponent implements OnInit {

  public agreements: GdprTypeDto[];

  @ViewChild('addSlider') addSlider: SliderComponent;
  @ViewChild('editSlider') editSlider: SliderComponent;

  @ViewChild('editForm') editForm: GdprAddFormComponent;

  readonly filteredColumns: string[] = ['name', 'description', 'action'];
  private componentDestroyed: Subject<void> = new Subject();
  private editedGdprType: GdprTypeDto;

  readonly errorTypes = {
    base: 'administration.gdpr.agree.notification.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: 409,
        text: 'nameOccupied'
      }
    ]
  };

  constructor(private gdprConfigService: GdprConfigService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.getAllTypes();
  }

  getAllTypes(): void {
      this.gdprConfigService.getAllGdprType('systemSettings').pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        agreements => this.agreements = agreements
      );
  }

  tryDeleteAgreement(gdprTypeId: number): void {
    this.gdprConfigService.deleteGdprType(gdprTypeId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.notifySuccess('administration.gdpr.agree.notification.removed');
        this.getAllTypes();
      }
    );
  }

  saveAgreement(gdprType: GdprTypeDto): void {
    this.gdprConfigService.saveGdprType(gdprType).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        if (this.editSlider.opened) {
          this.editSlider.close();
        } else if (this.addSlider.opened) {
          this.addSlider.close();
        }
        this.notifySuccess('administration.gdpr.agree.notification.saved');
        this.getAllTypes();
      },
      (err) => this.notificationService.notifyError(this.errorTypes, err.status)
    );
  }

  editAgreement(selectedGdprType: GdprTypeDto): void {
    this.editedGdprType = cloneDeep(selectedGdprType);
    this.editSlider.open();
    this.editForm.inputData = selectedGdprType;
  }

  updateAgreement(gdprType: GdprTypeDto): void {
    this.editedGdprType.name = gdprType.name;
    this.editedGdprType.description = gdprType.description;
    this.gdprConfigService.updateGdprType(this.editedGdprType).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        if (this.editSlider.opened) {
          this.editSlider.close();
        } else if (this.addSlider.opened) {
          this.addSlider.close();
        }
        this.notifySuccess('administration.gdpr.agree.notification.updated');
        this.getAllTypes();
      },
      (err) => this.notificationService.notifyError(this.errorTypes, err.status)
    );
  }

  private notifySuccess(key: string): void {
    this.notificationService.notify(key, NotificationType.SUCCESS);
  }

}
