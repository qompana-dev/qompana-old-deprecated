import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UrlUtil} from '../../shared/url.util';
import {
  EmailDataDto,
  FormDataApproveUpdateDto,
  FormDataDto,
  GdprAgreeWayDto,
  GdprApproveDto, GdprApproveUpdateDto,
  GdprFormConfigDto,
  GdprMailConfigDto,
  GdprStatusDto,
  GdprTokenInfoDto,
  GdprTypeDto
} from './gdpr.model';
import {getHeaderForAuthType} from '../../shared/permissions-utils';

@Injectable({
  providedIn: 'root'
})
export class GdprConfigService {

  constructor(private http: HttpClient) {
  }

  getAllGdprType(source: 'contactView' | 'systemSettings'): Observable<GdprTypeDto[]> {
    return this.http.get<GdprTypeDto[]>(`${UrlUtil.url}/crm-business-service/gdpr/types`, {headers: getHeaderForAuthType(source)});
  }

  deleteGdprType(gdprTypeId: number): Observable<void> {
    return this.http.delete<void>(`${UrlUtil.url}/crm-business-service/gdpr/types/${gdprTypeId}`);
  }

  saveGdprType(gdprType: GdprTypeDto): Observable<GdprTypeDto> {
    return this.http.post<GdprTypeDto>(`${UrlUtil.url}/crm-business-service/gdpr/types`, gdprType);
  }

  updateGdprType(gdprType: GdprTypeDto): Observable<GdprTypeDto> {
    return this.http.put<GdprTypeDto>(`${UrlUtil.url}/crm-business-service/gdpr/types`, gdprType);
  }

  getAllGdprStatuses(source: 'contactView' | 'systemSettings'): Observable<GdprStatusDto[]> {
    return this.http.get<GdprStatusDto[]>(`${UrlUtil.url}/crm-business-service/gdpr/statuses`, {headers: getHeaderForAuthType(source)});
  }

  deleteGdprStatus(gdprStatusId: number): Observable<void> {
    return this.http.delete<void>(`${UrlUtil.url}/crm-business-service/gdpr/statuses/${gdprStatusId}`);
  }

  saveGdprStatus(gdprStatusDto: GdprStatusDto): Observable<GdprStatusDto> {
    return this.http.post<GdprStatusDto>(`${UrlUtil.url}/crm-business-service/gdpr/statuses`, gdprStatusDto);
  }

  updateGdprStatus(gdprStatusDto: GdprStatusDto): Observable<GdprStatusDto> {
    return this.http.put<GdprStatusDto>(`${UrlUtil.url}/crm-business-service/gdpr/statuses`, gdprStatusDto);
  }

  getAllAgreeWays(source: 'contactView' | 'systemSettings'): Observable<GdprAgreeWayDto[]> {
    return this.http.get<GdprAgreeWayDto[]>(`${UrlUtil.url}/crm-business-service/gdpr/agree-way`, {headers: getHeaderForAuthType(source)});
  }

  deleteAgreeWay(gdprAgreeWayId: number): Observable<void> {
    return this.http.delete<void>(`${UrlUtil.url}/crm-business-service/gdpr/agree-way/${gdprAgreeWayId}`);
  }

  saveAgreeWay(gdprAgreeWayDto: GdprAgreeWayDto): Observable<GdprAgreeWayDto> {
    return this.http.post<GdprAgreeWayDto>(`${UrlUtil.url}/crm-business-service/gdpr/agree-way`, gdprAgreeWayDto);
  }

  updateAgreeWay(gdprAgreeWayDto: GdprAgreeWayDto): Observable<GdprAgreeWayDto> {
    return this.http.put<GdprAgreeWayDto>(`${UrlUtil.url}/crm-business-service/gdpr/agree-way`, gdprAgreeWayDto);
  }

  getGdprFormConfig(): Observable<GdprFormConfigDto> {
    return this.http.get<GdprFormConfigDto>(`${UrlUtil.url}/crm-business-service/gdpr/config/form`);
  }

  getGdprMailConfig(): Observable<GdprMailConfigDto> {
    return this.http.get<GdprMailConfigDto>(`${UrlUtil.url}/crm-business-service/gdpr/config/mail`);
  }

  saveGdprFormConfig(formConfig: GdprFormConfigDto): Observable<GdprFormConfigDto> {
    return this.http.post<GdprFormConfigDto>(`${UrlUtil.url}/crm-business-service/gdpr/config/form`, formConfig);
  }

  saveGdprMailConfig(mailConfig: GdprMailConfigDto): Observable<GdprMailConfigDto> {
    return this.http.post<GdprMailConfigDto>(`${UrlUtil.url}/crm-business-service/gdpr/config/mail`, mailConfig);
  }

  getAvailableGdprEmailVariables(): Observable<string[]> {
    return this.http.get<string[]>(`${UrlUtil.url}/crm-business-service/gdpr/variables/email`);
  }

  getAvailableGdprFormVariables(): Observable<string[]> {
    return this.http.get<string[]>(`${UrlUtil.url}/crm-business-service/gdpr/variables/form`);
  }

  getAllApprovalsForContact(contactId: number, source: 'contactViewShare' | 'contactViewManualChange'): Observable<GdprApproveDto[]> {
    return this.http.get<GdprApproveDto[]>(`${UrlUtil.url}/crm-business-service/gdpr/approvals/contact/${contactId}`, {headers: getHeaderForAuthType(source)})
  }

  updateApproveForContact(contactId: number, update: GdprApproveUpdateDto): Observable<GdprApproveDto> {
    return this.http.put<GdprApproveDto>(`${UrlUtil.url}/crm-business-service/gdpr/approve/contact/${contactId}`, update)
  }

  getFormDataForToken(token: string): Observable<FormDataDto> {
    return this.http.get<FormDataDto>(`${UrlUtil.url}/crm-business-service/gdpr/form/token/${token}`)
  }

  updateGdprApprovalListForToken(token: string, list: FormDataApproveUpdateDto[]): Observable<FormDataDto> {
    return this.http.put<FormDataDto>(`${UrlUtil.url}/crm-business-service/gdpr/form/token/${token}`, list)
  }

  setGdprTypesSentToContact(contactId: number, typeIds: number[]): Observable<void> {
    return this.http.post<void>(`${UrlUtil.url}/crm-business-service/gdpr/types/contact/${contactId}`, typeIds)
  }

  getGdprTokenInfoForContactId(contactId: number): Observable<GdprTokenInfoDto> {
    return this.http.get<GdprTokenInfoDto>(`${UrlUtil.url}/crm-business-service/gdpr/token/contact/${contactId}`);
  }

  createGdprTokenInfoForContactId(contactId: number): Observable<GdprTokenInfoDto> {
    return this.http.post<GdprTokenInfoDto>(`${UrlUtil.url}/crm-business-service/gdpr/token/contact/${contactId}`, undefined);
  }

  getGdprEmailByContactId(contactId: number): Observable<EmailDataDto> {
    return this.http.get<EmailDataDto>(`${UrlUtil.url}/crm-business-service/gdpr/email/contactId/${contactId}`);
  }
}
