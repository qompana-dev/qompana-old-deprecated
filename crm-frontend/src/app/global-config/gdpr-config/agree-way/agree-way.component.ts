import {Component, OnInit, ViewChild} from '@angular/core';
import {GdprAgreeWayDto} from '../gdpr.model';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {GdprConfigService} from '../gdpr-config.service';
import cloneDeep from 'lodash/cloneDeep';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {SliderComponent} from '../../../shared/slider/slider.component';
import {GdprAddFormComponent} from '../gdpr-add-form/gdpr-add-form.component';

@Component({
  selector: 'app-agree-way',
  templateUrl: './agree-way.component.html',
  styleUrls: ['./agree-way.component.scss']
})
export class AgreeWayComponent implements OnInit {

  @ViewChild('addSlider') addSlider: SliderComponent;
  @ViewChild('editSlider') editSlider: SliderComponent;

  @ViewChild('editForm') editForm: GdprAddFormComponent;

  readonly filteredColumns: string[] = ['name', 'action'];
  private componentDestroyed: Subject<void> = new Subject();

  private editedGdprAgreeWay: GdprAgreeWayDto;

  readonly errorTypes = {
    base: 'administration.gdpr.approveWay.notification.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: 409,
        text: 'nameOccupied'
      }
    ]
  };

  public agreeWays: GdprAgreeWayDto[];

  constructor(private gdprConfigService: GdprConfigService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.getAllAgreeWays();
  }

  getAllAgreeWays(): void {
    this.gdprConfigService.getAllAgreeWays('systemSettings').pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      agreeWays => this.agreeWays = agreeWays
    );
  }

  tryDeleteAgreeWay(agreeWayId: number): void {
    this.gdprConfigService.deleteAgreeWay(agreeWayId).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        this.notifySuccess('administration.gdpr.approveWay.notification.removed');
        this.getAllAgreeWays();
      }
    );
  }

  saveAgreeWay(agreeWay: GdprAgreeWayDto): void {
    this.gdprConfigService.saveAgreeWay(agreeWay).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        if (this.editSlider.opened) {
          this.editSlider.close();
        } else if (this.addSlider.opened) {
          this.addSlider.close();
        }
        this.notifySuccess('administration.gdpr.approveWay.notification.saved');
        this.getAllAgreeWays();
      },
      (err) => this.notificationService.notifyError(this.errorTypes, err.status)
    );
  }

  updateAgreeWay(agreeWay: GdprAgreeWayDto): void {
    this.editedGdprAgreeWay.name = agreeWay.name;
    this.gdprConfigService.updateAgreeWay(this.editedGdprAgreeWay).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => {
        if (this.editSlider.opened) {
          this.editSlider.close();
        } else if (this.addSlider.opened) {
          this.addSlider.close();
        }
        this.notifySuccess('administration.gdpr.approveWay.notification.updated');
        this.getAllAgreeWays();
      },
      (err) => this.notificationService.notifyError(this.errorTypes, err.status)
    );
  }

  editAgreeWay(agreeWay: GdprAgreeWayDto): void {
    this.editedGdprAgreeWay = cloneDeep(agreeWay);
    this.editSlider.open();
    this.editForm.inputData = this.editedGdprAgreeWay;
  }

  private notifySuccess(key: string): void {
    this.notificationService.notify(key, NotificationType.SUCCESS);
  }

}
