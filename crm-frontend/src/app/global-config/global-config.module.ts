import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GlobalConfigRoutingModule} from './global-config-routing.module';
import {SharedModule} from '../shared/shared.module';
import {AdministrationComponent} from './administration/administration.component';
import {ExpensesAndCurrenciesComponent} from './expenses-and-currencies/expenses-and-currencies.component';
import {PersonModule} from '../person/person.module';
import {RoleModule} from '../permissions/roles/role.module';
import {ProfileModule} from '../permissions/profiles/profile.module';
import {PasswordPolicyComponent} from './password-policy/password-policy.component';
import {MapSettingsComponent} from './map-settings/map-settings.component';
import {CompanyConfigComponent} from './company-config/company-config.component';
import {OfferModule} from '../offer/offer.module';
import {NgxMaskModule} from 'ngx-mask';
import {MapViewModule} from '../map/map-view/map-view.module';
import {GdprConfigComponent} from './gdpr-config/gdpr-config.component';
import {AgreementsComponent} from './gdpr-config/agreements/agreements.component';
import {StatusesComponent} from './gdpr-config/statuses/statuses.component';
import {AgreeWayComponent} from './gdpr-config/agree-way/agree-way.component';
import {MailContentComponent} from './gdpr-config/mail-content/mail-content.component';
import {FormContentComponent} from './gdpr-config/form-content/form-content.component';
import {TextEditorModule} from '../shared/text-editor/text-editor.module';
import {GdprAddFormComponent} from './gdpr-config/gdpr-add-form/gdpr-add-form.component';
import {DictionaryModule} from '../dictionary/dictionary.module';

@NgModule({
  declarations: [
    AdministrationComponent,
    ExpensesAndCurrenciesComponent,
    PasswordPolicyComponent,
    MapSettingsComponent,
    CompanyConfigComponent,
    GdprConfigComponent,
    AgreementsComponent,
    StatusesComponent,
    AgreeWayComponent,
    MailContentComponent,
    FormContentComponent,
    GdprAddFormComponent
  ],
  imports: [
    CommonModule,
    GlobalConfigRoutingModule,
    SharedModule,
    PersonModule,
    RoleModule,
    OfferModule,
    MapViewModule,
    ProfileModule,
    NgxMaskModule,
    TextEditorModule,
    DictionaryModule
  ]
})
export class GlobalConfigModule {
}
