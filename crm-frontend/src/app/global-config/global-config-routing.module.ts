import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../login/auth/auth-guard.service';
import {AdministrationComponent} from './administration/administration.component';

const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: AdministrationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GlobalConfigRoutingModule {
}
