import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {GlobalConfiguration} from './global-config.model';
import {UrlUtil} from '../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class GlobalConfigService {
  private updateAdministrationListSubject: Subject<void> = new Subject<void>();

  public updateAdministrationsList$ = this.updateAdministrationListSubject.asObservable();


  constructor(private http: HttpClient) {
  }

  getGlobalParamValue(key: string): Observable<GlobalConfiguration> {
    return this.http.get<GlobalConfiguration>(`${UrlUtil.url}/crm-user-service/configuration/${key}`);
  }

  setGlobalParam(key: string, value: string): Observable<GlobalConfiguration> {
    return this.http.post<GlobalConfiguration>(`${UrlUtil.url}/crm-user-service/configuration/${key}/${value}`, {});
  }

  setGlobalParamInBody(key: string, value: string): Observable<GlobalConfiguration> {
    return this.http.post<GlobalConfiguration>(`${UrlUtil.url}/crm-user-service/configuration`, {key, value});
  }

  public updateAdministrationsList(): void {
    this.updateAdministrationListSubject.next();
  }
}
