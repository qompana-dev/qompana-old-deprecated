import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {GlobalConfigService} from '../global-config.service';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-password-policy',
  templateUrl: './password-policy.component.html',
  styleUrls: ['./password-policy.component.scss']
})
export class PasswordPolicyComponent implements OnInit, OnDestroy {

  passwordPolicy: FormControl;
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private configService: GlobalConfigService,
              private notificationService: NotificationService) {
    this.passwordPolicy = new FormControl('', Validators.compose([Validators.required]));
  }

  ngOnInit(): void {
    this.configService.getGlobalParamValue('passwordPolicyActive').pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (param) => this.passwordPolicy.patchValue(param.value),
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    if (!this.passwordPolicy.valid) {
      this.passwordPolicy.markAsTouched();
      return;
    }
    this.configService.setGlobalParam('passwordPolicyActive', this.passwordPolicy.value).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.notificationService.notify('administration.notifications.saveSuccess', NotificationType.SUCCESS),
      () => this.notificationService.notify('administration.notifications.saveError', NotificationType.ERROR)
    );
  }
}
