import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AddressForPreparedByDto} from '../../offer/offer.model';
import {GlobalConfigService} from '../global-config.service';
import {Subject, Subscription} from 'rxjs';
import {takeUntil, debounceTime, switchMap} from 'rxjs/operators';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CompanyConfig, GlobalConfiguration} from '../global-config.model';
import {MapViewMarker} from '../../map/map-view/map-view.model';
import {LngLatLike} from 'mapbox-gl';
import {FoundPlace} from '../../lead/lead.model';
import {MatSelect} from '@angular/material/select';
import {MapService} from '../../map/map.service';
import {SliderComponent} from '../../shared/slider/slider.component';

@Component({
  selector: 'app-offer-generator-config',
  templateUrl: './company-config.component.html',
  styleUrls: ['./company-config.component.scss']
})
export class CompanyConfigComponent implements OnInit, OnDestroy {
  public static readonly DEFAULT_COMPANY_CONFIG_PARAM_KEY = 'companyConfig';

  private componentDestroyed: Subject<void> = new Subject();
  private valueChangesSubscription: Subscription;

  companyConfig: CompanyConfig = {} as CompanyConfig;

  @Input() inGlobalConfig = false;

  preparedByGroup: FormGroup;
  correctLogo = true;
  imageSrc = '';


  location: { lng: number; lat: number; };
  markers: MapViewMarker[];
  mapCenter: LngLatLike = undefined;
  showMap = false;
  foundPlaces: FoundPlace[] = [];
  @ViewChild('placeSelect') placeSelect: MatSelect;
  private searchedPlace: string;
  @ViewChild('mapSlider') mapSlider: SliderComponent;

  // @formatter:off
  get companyName(): AbstractControl { return this.preparedByGroup.get('companyName'); }
  get companyAddress(): AbstractControl { return this.preparedByGroup.get('companyAddress'); }
  get companyPhone(): AbstractControl { return this.preparedByGroup.get('companyPhone'); }
  get companyEmail(): AbstractControl { return this.preparedByGroup.get('companyEmail'); }
  get www(): AbstractControl { return this.preparedByGroup.get('www'); }
  get nip(): AbstractControl { return this.preparedByGroup.get('nip'); }
  get regon(): AbstractControl { return this.preparedByGroup.get('regon'); }
  get krs(): AbstractControl { return this.preparedByGroup.get('krs'); }
  get logo(): AbstractControl { return this.preparedByGroup.get('logo'); }
  // @formatter:on

  constructor(private configService: GlobalConfigService,
              private fb: FormBuilder,
              private mapService: MapService,
              private notificationService: NotificationService) {
    this.createForm();
  }

  ngOnInit(): void {
    this.subscribeValueChange();
    this.subscribeForLocation();
    this.setValuesToForm();
    this.loadConfig();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  saveGlobalConfig(): void {
    if (this.companyConfig) {
      this.configService.setGlobalParamInBody(CompanyConfigComponent.DEFAULT_COMPANY_CONFIG_PARAM_KEY, JSON.stringify(this.companyConfig))
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => {
          this.notificationService.notify('companyConfig.configurationSaved', NotificationType.SUCCESS);
        }, () => {
          this.notificationService.notify('companyConfig.failedSavingConfiguration', NotificationType.ERROR);
        });
    }
  }


  private subscribeValueChange(onlyStopSubscription: boolean = false): void {
    if (this.valueChangesSubscription) {
      this.valueChangesSubscription.unsubscribe();
    }
    if (!onlyStopSubscription) {
      this.valueChangesSubscription = this.preparedByGroup.valueChanges
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(() => this.setValues());
    }
  }

  private createForm(): void {
    if (!this.preparedByGroup) {
      this.preparedByGroup = this.fb.group({
        companyName: [null],
        companyAddress: [null],
        companyPhone: [null],
        companyEmail: [null, [Validators.email]],
        www: [null],
        logo: [null],
        nip: [null, [Validators.minLength(10)]],
        regon: [null, [Validators.minLength(9), Validators.maxLength(14),
          Validators.pattern('^(?=[0-9]*$)(?:.{9}|.{14})$')]],
        krs: [null]
      });
      this.setValuesToForm();
      this.setValues();
    }
  }

  setValues(): void {
    if (!this.companyConfig.preparedBy) {
      this.companyConfig.preparedBy = {} as AddressForPreparedByDto;
    }
    this.companyConfig.preparedBy.companyName = this.companyName.value;
    this.companyConfig.preparedBy.companyAddress = this.companyAddress.value;
    this.companyConfig.preparedBy.companyPhone = this.companyPhone.value;
    this.companyConfig.preparedBy.companyEmail = this.companyEmail.value;
    this.companyConfig.preparedBy.www = this.www.value;
    this.companyConfig.preparedBy.nip = this.nip.value;
    this.companyConfig.preparedBy.regon = this.regon.value;
    this.companyConfig.preparedBy.krs = this.krs.value;
    this.companyConfig.base64EncodedLogo = this.getBase64EncodedLogo();

    if (this.searchedPlace !== this.companyAddress.value) {
      this.location = {lat: null, lng: null};
    }
    if (this.location) {
      this.companyConfig.longitude = this.location.lng ? String(this.location.lng) : '';
      this.companyConfig.latitude = this.location.lat ? String(this.location.lat) : '';
    }
  }


  getBase64EncodedLogo(): string {
    if (this.logo.value) {
      return this.logo.value.replace(/^data:image\/[a-z]+;base64,/, '');
    } else {
      return undefined;
    }
  }

  getBase64DecodedLogo(logo: string): string {
    if (logo != null) {
      return 'data:image/png;base64,' + logo;
    } else {
      return undefined;
    }
  }

  logoLoaded(error: boolean): void {
    this.correctLogo = !error;
    this.logo.updateValueAndValidity();
  }

  handleInputChange(event: any): void {
    const file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
    const reader = new FileReader();
    if (!file.type.match(/image-*/)) {
      console.error('invalid format');
      return;
    }
    reader.onload = (e: any) => {
      console.log(e, 'reader.onload');
      this.imageSrc = e.target.result;
      this.logo.patchValue(this.imageSrc);
    };
    reader.readAsDataURL(file);
  }

  setValuesToForm(): void {
    this.subscribeValueChange(true);
    if (this.preparedByGroup && this.companyConfig && this.companyConfig.preparedBy) {
      this.companyName.patchValue(this.companyConfig.preparedBy.companyName, {emitEvent: false});
      this.companyAddress.patchValue(this.companyConfig.preparedBy.companyAddress, {emitEvent: false});
      this.companyPhone.patchValue(this.companyConfig.preparedBy.companyPhone, {emitEvent: false});
      this.companyEmail.patchValue(this.companyConfig.preparedBy.companyEmail, {emitEvent: false});
      this.www.patchValue(this.companyConfig.preparedBy.www, {emitEvent: false});
      this.nip.patchValue(this.companyConfig.preparedBy.nip, {emitEvent: false});
      this.regon.patchValue(this.companyConfig.preparedBy.regon, {emitEvent: false});
      this.krs.patchValue(this.companyConfig.preparedBy.krs, {emitEvent: false});
      this.logo.patchValue(this.getBase64DecodedLogo(this.companyConfig.base64EncodedLogo), {emitEvent: false});
      this.imageSrc = this.getBase64DecodedLogo(this.companyConfig.base64EncodedLogo);
      this.correctLogo = (this.imageSrc != null);
      this.searchedPlace = this.companyConfig.preparedBy.companyAddress;
      this.location = {lat: +this.companyConfig.latitude, lng: +this.companyConfig.longitude};
    }
    this.subscribeValueChange();
  }


  private loadConfig(): void {
    this.configService.getGlobalParamValue(CompanyConfigComponent.DEFAULT_COMPANY_CONFIG_PARAM_KEY)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((globalConfiguration: GlobalConfiguration) => {
        this.companyConfig = JSON.parse(globalConfiguration.value);
        this.setValuesToForm();
      }, () => this.notificationService.notify('companyConfig.configLoadProblem', NotificationType.ERROR));
  }


  openMap(event: MouseEvent): void {
    this.mapService.getCoordinatesByAddressString(this.companyAddress.value)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      places => {
        this.handleFoundPlaces(places);
      },
      err => this.mapService.notifyPlaceNotFound()
    );
    event.stopPropagation();
  }

  private handleFoundPlaces(places: FoundPlace[]): void {
    this.foundPlaces = places;
    if (places && places.length === 1) {
      this.showPlace(places[0]);
    } else if (places && places.length > 1) {
      setTimeout(() => this.placeSelect.open(), 500);
    } else {
      this.location = {lat: null, lng: null};
      this.mapService.notifyPlaceNotFound();
    }
  }

  private showPlace(place: FoundPlace): void {
    this.companyAddress.patchValue(place.display_name, { emitEvent: false });
    this.markers = this.mapService.getSingleMarkerFromFoundPlace(place, this.showPlace.bind(this));
    this.searchedPlace = place.display_name;
    if (!this.showMap) {
      this.mapCenter = this.location = this.mapService.getCenter(place);
      this.companyConfig.latitude = this.location.lat + '';
      this.companyConfig.longitude = this.location.lng + '';
      this.mapSlider.open();
      setTimeout(() => this.showMap = true, 500);
    } else {
      this.location = {
        lng: +place.lon,
        lat: +place.lat,
      };
    }
  }

  onMapClose(): void {
    this.showMap = false;
  }

  private subscribeForLocation(): void {
    this.companyAddress.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      switchMap(((input: string) => {
        return this.mapService.getCoordinatesByAddressString(input);
      }
      ))).subscribe(places => {
        setTimeout(() => this.placeSelect.open(), 500)
        this.foundPlaces = places;
      });
  }
}
