import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {GlobalConfigService} from '../global-config.service';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-map-settings',
  templateUrl: './map-settings.component.html',
  styleUrls: ['./map-settings.component.scss']
})
export class MapSettingsComponent implements OnInit, OnDestroy {

  customerRadiusControl: FormControl;
  cityRadiusControl: FormControl;
  timeDifferenceInD: FormControl;
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private configService: GlobalConfigService,
              private notificationService: NotificationService) {
    this.customerRadiusControl = new FormControl('', Validators.compose([Validators.required, Validators.min(0)]));
    this.cityRadiusControl = new FormControl('', Validators.compose([Validators.required, Validators.min(0)]));
    this.timeDifferenceInD = new FormControl('', Validators.compose([Validators.required, Validators.min(0)]));
  }

  ngOnInit(): void {
    this.configService.getGlobalParamValue('customerRadiusInKm').pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (param) => this.customerRadiusControl.patchValue(param.value),
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
    this.configService.getGlobalParamValue('cityRadiusInKm').pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (param) => this.cityRadiusControl.patchValue(param.value),
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
    this.configService.getGlobalParamValue('timeDifferenceInD').pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (param) => this.timeDifferenceInD.patchValue(param.value),
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submitCustomerRadius(): void {
    if (!this.customerRadiusControl.valid) {
      this.customerRadiusControl.markAsTouched();
      return;
    }
    this.configService.setGlobalParam('customerRadiusInKm', this.customerRadiusControl.value).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.notificationService.notify('administration.notifications.saveSuccess', NotificationType.SUCCESS),
      () => this.notificationService.notify('administration.notifications.saveError', NotificationType.ERROR)
    );
  }

  submitCityRadius(): void {
    if (!this.cityRadiusControl.valid) {
      this.cityRadiusControl.markAsTouched();
      return;
    }
    this.configService.setGlobalParam('cityRadiusInKm', this.cityRadiusControl.value).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.notificationService.notify('administration.notifications.saveSuccess', NotificationType.SUCCESS),
      () => this.notificationService.notify('administration.notifications.saveError', NotificationType.ERROR)
    );
  }

  submitTimeDifferenceInD(): void {
    if (!this.timeDifferenceInD.valid) {
      this.timeDifferenceInD.markAsTouched();
      return;
    }
    this.configService.setGlobalParam('timeDifferenceInD', this.timeDifferenceInD.value).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      () => this.notificationService.notify('administration.notifications.saveSuccess', NotificationType.SUCCESS),
      () => this.notificationService.notify('administration.notifications.saveError', NotificationType.ERROR)
    );
  }
}
