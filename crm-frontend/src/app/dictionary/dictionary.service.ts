import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {
  DictionaryId,
  DictionaryWord,
  SaveDictionaryWord,
  Dictionary
} from './dictionary.model';
import {UrlUtil} from '../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class DictionaryService {
  private dictionaryUrl = `${UrlUtil.url}/crm-business-service/dictionary`;

  constructor(private http: HttpClient) {
  }

  public findAdminDictionary(dictionary: DictionaryId) {
    const url = `${this.dictionaryUrl}/${dictionary}/${localStorage.getItem('locale')}/admin`;
    return this.http.get<Dictionary>(url);
  }

  public findUserDictionary(dictionary: DictionaryId, privateWordsIds: number[]) {
    const url = `${this.dictionaryUrl}/${dictionary}/${localStorage.getItem('locale')}/user?privateWordsIds=${privateWordsIds}`;
    return this.http.get<Dictionary>(url);
  }

  public findPrivateWords( privateWordsIds: number[]) {
    const locale = localStorage.getItem('locale');
    const url = `${this.dictionaryUrl}/private/words/${locale}?privateWordsIds=${privateWordsIds}`;
    return this.http.get<DictionaryWord[]>(url);
  }

  public findWholeDictionary(dictionary: DictionaryId) {
    const url = `${this.dictionaryUrl}/${dictionary}/${localStorage.getItem('locale')}/whole`;
    return this.http.get<Dictionary>(url);
  }

  public saveWord(word: SaveDictionaryWord): Observable<SaveDictionaryWord> {
    const url = `${this.dictionaryUrl}/word`;
    return this.http.post<SaveDictionaryWord>(url, word);
  }

  public saveAllWord(allWord: SaveDictionaryWord[]): Observable<SaveDictionaryWord[]> {
    const url = `${this.dictionaryUrl}/allword`;
    return this.http.post<SaveDictionaryWord[]>(url, allWord);
  }

  public updateWord(word: SaveDictionaryWord): Observable<SaveDictionaryWord> {
    const url = `${this.dictionaryUrl}/word/${word.id}`;
    return this.http.put<SaveDictionaryWord>(url, word);
  }

  public deleteWord(wordId: number) {
    const url = `${this.dictionaryUrl}/word/${wordId}`;
    return this.http.delete<SaveDictionaryWord>(url);
  }
}
