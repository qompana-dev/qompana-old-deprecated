import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {SliderService} from '../../shared/slider/slider.service';
import {filter, takeUntil} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import {DictionaryService} from '../dictionary.service';
import {DictionaryId, DictionaryWordFactory, SaveDictionaryWord, SaveDictionaryWordFactory} from '../dictionary.model';
import {NotificationService, NotificationType} from '../../shared/notification.service';


@Component({
  selector: 'app-dictionary-add-word',
  templateUrl: './dictionary-add-word.component.html',
  styleUrls: ['./dictionary-add-word.component.scss']
})
export class DictionaryAddWordComponent implements OnInit, OnDestroy {

  private componentDestroyed: Subject<void> = new Subject();
  name = new FormControl('');
  global = new FormControl('');
  saveInProgress = false;

  @Input() dictionaryId: DictionaryId;
  @Input() parentWordId: number;
  @Output() canceled: EventEmitter<void> = new EventEmitter();
  @Output() wordAdded: EventEmitter<SaveDictionaryWord> = new EventEmitter();

  constructor(private sliderService: SliderService,
              private dictionaryService: DictionaryService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    if (this.dictionaryId === null) {
      throw new TypeError('Attribute "dictionaryId" is required!');
    }
    this.listenWhenSliderClosedByCross();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  cancel(): void {
    this.canceled.emit();
  }

  submit(): void {
    if (!this.saveInProgress) {
      this.saveInProgress = true;
      const newWord = DictionaryWordFactory.createNew(this.parentWordId);
      newWord.global = this.global.value;
      newWord.name = this.name.value;
      const wordToSave = SaveDictionaryWordFactory.create(this.dictionaryId, newWord);

      this.dictionaryService.saveWord(wordToSave)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          (savedWord: SaveDictionaryWord) => {
            this.saveInProgress = false;
            this.notificationService.notify('dictionaries.notification.saveWordSuccess', NotificationType.SUCCESS);
            this.wordAdded.next(savedWord);
          },
          (err) => {
            this.saveInProgress = false;
            this.notificationService.notify('dictionaries.notification.saveWordError', NotificationType.ERROR)
          }
      );
    }
  }

  private listenWhenSliderClosedByCross(): void {
    this.sliderService.closeClicked.pipe(
      takeUntil(this.componentDestroyed),
      filter(key => key === 'DictionaryForm.AddDictionaryWordSlider'),
    ).subscribe(() => {
      this.canceled.emit();
    });
  }
}
