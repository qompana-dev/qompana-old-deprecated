import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SliderService} from '../../shared/slider/slider.service';
import {DictionaryWordsListComponent} from "../dictionary-words-list/dictionary-words-list.component";


@Component({
  selector: 'app-dictionary-administration',
  templateUrl: './dictionary-administration.component.html',
  styleUrls: ['./dictionary-administration.component.scss']
})
export class DictionaryAdministrationComponent implements OnInit, OnDestroy {

  @ViewChild("dictionaryWordList") dictionaryWordList: DictionaryWordsListComponent;

  constructor(public sliderService: SliderService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  saveGlobalConfig(): void {
    this.dictionaryWordList.submit();
  }
}
