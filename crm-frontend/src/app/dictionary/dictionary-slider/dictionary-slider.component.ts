import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {SliderService} from '../../shared/slider/slider.service';
import {filter, takeUntil} from 'rxjs/operators';
import {DictionaryWordsListComponent} from "../dictionary-words-list/dictionary-words-list.component";


@Component({
  selector: 'app-dictionary-slider',
  templateUrl: './dictionary-slider.component.html',
  styleUrls: ['./dictionary-slider.component.scss']
})
export class DictionarySliderComponent implements OnInit, OnDestroy {

  private componentDestroyed: Subject<void> = new Subject();

  @Output() onCancelClick: EventEmitter<void> = new EventEmitter();
  @Output() onSubmitClick: EventEmitter<void> = new EventEmitter();

  @ViewChild("dictionaryWordList") dictionaryWordList: DictionaryWordsListComponent;

  constructor(public sliderService: SliderService) {
  }

  ngOnInit(): void {
    this.listenWhenSliderClosedByCross();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  cancel(): void {
    this.onCancelClick.emit();
  }

  submit(): void {
    this.dictionaryWordList.submit();
    this.onSubmitClick.emit();
  }

  private listenWhenSliderClosedByCross() {
    this.sliderService.closeClicked.pipe(
      takeUntil(this.componentDestroyed),
      filter(key => key === 'DictionaryForm.AddDictionaryWordSlider'),
    ).subscribe(() => {
      this.onCancelClick.emit();
    });
  }
}
