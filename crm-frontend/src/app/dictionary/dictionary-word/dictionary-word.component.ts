import {
  Component, ComponentFactoryResolver,
  ComponentRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  Dictionary,
  DictionaryWord,
  DictionaryWordFactory,
  SaveDictionaryWord,
  SaveDictionaryWordFactory
} from "../dictionary.model";
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {takeUntil} from "rxjs/operators";
import {NotificationService, NotificationType} from "../../shared/notification.service";
import {DictionaryService} from "../dictionary.service";
import {Subject} from "rxjs";


@Component({
  selector: 'app-dictionary-word',
  templateUrl: './dictionary-word.component.html',
  styleUrls: ['./dictionary-word.component.scss']
})
export class DictionaryWordComponent implements OnInit, OnDestroy {

  @Input() componentId: number;
  @Input() level: number;
  @Input() maxLevel: number;
  @Input() isWordsDeletable: boolean;
  @Input() isWordsHideable: boolean;
  @Input() dictionaryId: number;
  @Input() parentId: number;
  wordRead: DictionaryWord;

  @Input() set word(word: DictionaryWord) {
    if (word) {
      this.existing = true;
      this.wordRead = word;
    } else {
      this.existing = false;
      this.wordRead = DictionaryWordFactory.createNew(this.parentId);
    }

    this.refresh();
  }

  existing: boolean;
  wordSave: SaveDictionaryWord;

  @Output() onDeleteClick: EventEmitter<void> = new EventEmitter();

  wordForm: FormGroup;

  @ViewChild('componentContainer', {read: ViewContainerRef}) componentContainer;
  componentsId = 0;
  words = [];

  private componentDestroyed: Subject<void> = new Subject();

  constructor(private fb: FormBuilder,
              private componentFactoryResolver: ComponentFactoryResolver,
              private dictionaryService: DictionaryService,
              private notificationService: NotificationService) {
    this.createForm();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  public submit(): void {

    this.words.forEach(word => {
      word.submit();
    });

    if(!this.existing) {
      this.save();
      return
    }

    if(this.wordForm.dirty) {
      this.update();
      return;
    }
  }

  public markAsEditable() {
    this.name.enable();
  }

  public markAsNotEditable() {
    this.name.disable();
  }

  public addWords(words: DictionaryWord[]) {
    words.forEach(word => {
      this.addWord(word);
    });
  }

  private update() {
    const word = this.getWordToSave();
    this.dictionaryService.updateWord(word)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      savedWord => {
        this.notificationService.notify('dictionaries.notification.saveWordSuccess', NotificationType.SUCCESS);
      },
      err => {
        if (err.status === 440) {
          this.notificationService.notify('dictionaries.notification.usedInGoal', NotificationType.ERROR)
        } else {
          this.notificationService.notify('dictionaries.notification.saveWordError', NotificationType.ERROR)
        }
      }
    );

  }

  private getWordToSave() {
    const word: SaveDictionaryWord = this.wordForm.getRawValue();
    return word;
  }

  private save() {
    const word = this.getWordToSave();
    this.dictionaryService.saveWord(word)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      savedWord => {
        this.notificationService.notify('dictionaries.notification.saveWordSuccess', NotificationType.SUCCESS);
      },
      err => {
        if (err.status === 440) {
          this.notificationService.notify('dictionaries.notification.usedInGoal', NotificationType.ERROR)
        } else {
          this.notificationService.notify('dictionaries.notification.saveWordError', NotificationType.ERROR)
        }
      }
    );
  }

  delete(): void {
    if(this.wordRead.id === null) {
      this.onDeleteClick.emit();
      return;
    }

    this.dictionaryService.deleteWord(this.wordSave.id)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      deletedWord => {
        this.notificationService.notify('dictionaries.notification.deleteWordSuccess', NotificationType.SUCCESS);
      },
      err => {
        if (err.status === 440) {
          this.notificationService.notify('dictionaries.notification.usedInGoal', NotificationType.ERROR)
        } else {
          this.notificationService.notify('dictionaries.notification.deleteWordError', NotificationType.ERROR)
        }
      }
    );

    this.onDeleteClick.emit();
  }

  addWord(word?: DictionaryWord): void {
    if (this.isDuringCreation()) {
      return;
    }

    const factory = this.componentFactoryResolver.resolveComponentFactory(DictionaryWordComponent);
    const componentRef = this.componentContainer.createComponent(factory, 0);

    componentRef.instance.level = this.level+1;
    componentRef.instance.maxLevel = this.maxLevel;
    componentRef.instance.isWordsDeletable = this.isWordsDeletable;
    componentRef.instance.isWordsHideable = this.isWordsHideable;
    componentRef.instance.dictionaryId = this.dictionaryId;
    componentRef.instance.parentId = this.wordRead.id;
    componentRef.instance.word = word;
    componentRef.instance.componentId = this.componentsId;

    componentRef.instance.onDeleteClick.subscribe(() => {this.words = this.words.filter(word => word.componentId !== componentRef.instance.componentId);
      componentRef.destroy();
    });

    this.words.push(componentRef.instance);
    componentRef.changeDetectorRef.detectChanges();
    this.componentsId++;
  }

  addChild(): void {
    this.addWord();
  }

  private clearWords(): void {
    this.words.forEach(word => word.delete());
  }

  private isDuringCreation() {
    const notExisting = this.words.find(word => !word.existing);
    return notExisting !== undefined;
  }

  private createForm(): void {
    this.wordForm = this.fb.group({
      id: [null],
      name: [null, [Validators.required]],
      global: [true, [Validators.required]],
      parentId: [null],
      dictionaryId: [null],
      locale: [null]
    });
  }

  private refresh() {
    this.wordSave = SaveDictionaryWordFactory.create(this.dictionaryId, this.wordRead);
    this.addWords(this.wordRead.children)
    this.wordForm.reset(this.wordSave);
  }

  // @formatter:off
  get name(): AbstractControl {
    return this.wordForm.get('name');
  }
  get global(): AbstractControl {
    return this.wordForm.get('global');
  }
  // @formatter:on
}
