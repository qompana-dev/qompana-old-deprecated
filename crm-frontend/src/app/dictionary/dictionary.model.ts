export enum DictionaryId {
  SOURCE_OF_ACQUISITION = 1,
  UNIT = 2,
  PHONE_NUMBER_TYPE = 3,
  SUBJECT_OF_INTEREST = 4,
  POSITION = 5,
  INDUSTRY = 6,
  LOCATION = 7
}

export type DictionaryState = {
  [key in DictionaryId]?: {
    words: DictionaryWord;
    reload: boolean;
    privatWords: DictionaryWord;
  };
};

export class DropdownDictionary {
  id: DictionaryId;
  name: string;
}

export const dictionaries: DropdownDictionary[] = [
  {id: DictionaryId.SOURCE_OF_ACQUISITION, name: 'sourceOfAcquisition'},
  {id: DictionaryId.PHONE_NUMBER_TYPE, name: 'phoneNumberType'},
  {id: DictionaryId.SUBJECT_OF_INTEREST, name: 'subjectOfInterest'},
  {id: DictionaryId.POSITION, name: 'position'},
  {id: DictionaryId.INDUSTRY, name: 'industry'},
  {id: DictionaryId.LOCATION, name: 'location'},
];

export class Dictionary {
  id: number;
  type: number;
  maxLevel: number;
  isWordsDeletable: boolean;
  isWordsHideable: boolean;
  words: DictionaryWord[];
}

export class DictionaryWord {
  id: number;
  name: string;
  global: boolean;
  locale: string;
  parentId: number;
  children: DictionaryWord[];
}

export class DictionaryWordFactory {

  static createNew(parentId?: number): DictionaryWord {
    const word = new DictionaryWord();
    word.id = null;
    word.name = "";
    word.locale = localStorage.getItem('locale');
    word.global = true;
    word.children = [];

    if (parentId) {
      word.parentId = parentId;
    }

    return word;
  }
}

export class SaveDictionaryWord {
  id: number;
  name: string;
  global: boolean;
  parentId: number;
  dictionaryId: number;
  locale: string;

  constructor() {
    this.id = null;
    this.name = "";
    this.global = true;
    this.parentId = null;
    this.dictionaryId = null;
    this.locale = localStorage.getItem('locale');
  }
}

export class SaveDictionaryWordFactory {

  static createNew(dictionaryId: number): SaveDictionaryWord {
    const word = new SaveDictionaryWord();
    word.dictionaryId = dictionaryId;
    return word;
  }

  static create(dictionaryId: number, dictionaryWord: DictionaryWord): SaveDictionaryWord {
    const word = new SaveDictionaryWord();
    word.dictionaryId = dictionaryId;
    word.id = dictionaryWord.id;
    word.parentId = dictionaryWord.parentId;
    word.name = dictionaryWord.name;
    word.global = dictionaryWord.global;
    word.locale = dictionaryWord.locale;
    return word;
  }
}
