import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {dictionaries, DropdownDictionary, SaveDictionaryWord} from '../dictionary.model';
import {MatDialogRef} from '@angular/material';
import {DictionaryService} from "../dictionary.service";
import {NotificationService, NotificationType} from "../../shared/notification.service";

@Component({
  selector: 'app-dictionary-words-add',
  templateUrl: './dictionary-words-add.component.html',
  styleUrls: ['./dictionary-words-add.component.scss']
})
export class DictionaryWordsAddComponent implements OnInit, OnDestroy {

  private componentDestroyedNotifier: Subject<void> = new Subject();
  dictionaries: DropdownDictionary[] = dictionaries;
  saveDictionaryWords: SaveDictionaryWord[];
  displayedColumns: string[] = ['dictionaryId', 'name', 'global'];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { newWords: SaveDictionaryWord[] },
    private dialogRef: MatDialogRef<DictionaryWordsAddComponent>,
    private dictionaryService: DictionaryService,
    private notificationService: NotificationService
  ) {
  }

  ngOnInit(): void {
    this.saveDictionaryWords = this.data.newWords;
  }

  ngOnDestroy(): void {
    this.componentDestroyedNotifier.next();
    this.componentDestroyedNotifier.complete();
  }

  getDictionaryName(dictionaryId: number): string {
    const dropdownDictionary = dictionaries.find(value => value.id === dictionaryId);
    if (dropdownDictionary) {
      return dropdownDictionary.name;
    } else {
      return 'unknown';
    }
  }

  submit(): void {
    this.dictionaryService.saveAllWord(this.saveDictionaryWords)
      .subscribe(
        (savedWordList: SaveDictionaryWord[]) => {
          this.notificationService.notify('dictionaries.notification.saveAllWordsSuccess', NotificationType.SUCCESS);
          // this.wordAdded.next(savedWord);
        },
        (err) => {
          this.notificationService.notify('dictionaries.notification.saveAllWordsError', NotificationType.ERROR);
          this.dialogRef.close(false);
        }
      );
    this.dialogRef.close(true);
  }

}
