import {
  Component,
  ComponentFactoryResolver, ComponentRef, EventEmitter, Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  dictionaries,
  DropdownDictionary,
  DictionaryWord,
  Dictionary,
  SaveDictionaryWord,
  SaveDictionaryWordFactory,
  DictionaryWordFactory
} from "../dictionary.model";
import {AbstractControl, FormControl, ValidationErrors} from "@angular/forms";
import {DictionaryWordComponent} from "../dictionary-word/dictionary-word.component";
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {delay, filter, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import * as cloneDeep from 'lodash/cloneDeep';
import {DictionaryService} from "../dictionary.service";

@Component({
  selector: 'app-dictionary-words-list',
  templateUrl: './dictionary-words-list.component.html',
  styleUrls: ['./dictionary-words-list.component.scss']
})
export class DictionaryWordsListComponent implements OnInit, OnDestroy {

  dictionaries: DropdownDictionary[] = dictionaries;
    dictionary = new FormControl('');

  fullDictionary: Dictionary;

  componentsId = 0;
  words = [];

  @ViewChild('componentContainer', {read: ViewContainerRef}) componentContainer;

  private componentDestroyed: Subject<void> = new Subject();

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private dictionaryService: DictionaryService,
              private notificationService: NotificationService) {
    this.dictionary.patchValue(dictionaries[0].id);
  }

  ngOnInit(): void {
    this.findDictionary();
  }

  ngOnDestroy(): void {
  }

  public submit(): void {
    this.words.forEach(word => {
      word.submit();
    });
    this.delay(3000).then(any=> {
      this.findDictionary();
    });
  }

  addNewWord(): void {
    this.addWord();
  }

  private addWord(word?: DictionaryWord): void {
    if (this.isDuringCreation()) {
      return;
    }

    const factory = this.componentFactoryResolver.resolveComponentFactory(DictionaryWordComponent);
    const componentRef = this.componentContainer.createComponent(factory, 0);

    componentRef.instance.level = 1;
    componentRef.instance.maxLevel = this.fullDictionary.maxLevel;
    componentRef.instance.isWordsDeletable = this.fullDictionary.isWordsDeletable;
    componentRef.instance.isWordsHideable = this.fullDictionary.isWordsHideable;
    componentRef.instance.dictionaryId = this.dictionary.value;
    componentRef.instance.parentId = null;
    componentRef.instance.word = word;
    componentRef.instance.componentId = this.componentsId;

    componentRef.instance.onDeleteClick.subscribe(() => {
      this.words = this.words.filter(word => word.componentId !== componentRef.instance.componentId);
      componentRef.destroy();
    });

    this.words.push(componentRef.instance);
    componentRef.changeDetectorRef.detectChanges();
    this.componentsId++;
  }

  private addWords(dictionary: Dictionary) {
    dictionary.words.sort((a,b) => b.name.localeCompare(a.name)).forEach(word => {
      this.addWord(word);
    });
  }

  private findDictionary() {
    this.dictionaryService.findAdminDictionary(this.dictionary.value).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      downloadedDictionary => {
        this.fullDictionary = downloadedDictionary;
        this.clearWords();
        this.addWords(downloadedDictionary);
      },
      err => this.notificationService.notify('dictionaries.errors.getDictionaryError', NotificationType.ERROR)
    );
  }

  dictionaryChange() {
    this.findDictionary();
  }

  private clearWords(): void {
    this.componentContainer.clear()
    this.words = [];
  }

  private isDuringCreation() {
    const notExisting = this.words.find(word => !word.existing);
    return notExisting !== undefined;
  }

  async delay(ms: number) {
    await new Promise(resolve => setTimeout(()=>resolve(), ms)).then(()=>console.log("fired"));
  }
}
