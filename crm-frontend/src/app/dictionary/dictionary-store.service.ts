import {Observable, of, Subject} from 'rxjs';
import { Injectable } from '@angular/core';
import {
  Dictionary,
  DictionaryWord,
  DictionaryState, DictionaryId,
} from './dictionary.model';
import { DictionaryService } from './dictionary.service';
import {concatMap, map, takeUntil, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DictionaryStoreService {
  private dictionary: DictionaryState = {};

  constructor(private dictionaryService: DictionaryService) {}

  private setDictionaryData(
    dictionaryId: number,
    dictionary: Dictionary
  ): void {
    this.dictionary = {
      ...this.dictionary,
      [dictionaryId]: {
        ...dictionary,
        reload: false,
        privateWords: [],
      },
    };
  }

  private addToDictionaryPrivateData(
    dictionaryId: number,
    dictionaryWords: DictionaryWord[]
  ): void {
    const { privateWords, ...dictionary } = this.dictionary[dictionaryId];
    this.dictionary = {
      ...this.dictionary,
      [dictionaryId]: {
        ...dictionary,
        privateWords: [...privateWords, ...dictionaryWords],
      },
    };
  }

  private getWordsFromDictionary(
    dictionaryId: number
  ): Observable<DictionaryWord[]> {
    const isDictionary = this.isLoadedDictionary(dictionaryId);

    if (isDictionary) {
      const { words } = this.dictionary[dictionaryId];
      return of(words);
    }

    return this.fetchDictionaryById(dictionaryId).pipe(
      tap((dictionary) => this.setDictionaryData(dictionaryId, dictionary)),
      map(({ words }) => words)
    );
  }

  private fetchDictionaryById(dictionaryId: number): Observable<Dictionary> {
    return this.dictionaryService.findUserDictionary(dictionaryId, []);
  }

  private fetchPrivateWordsByIds(
    privateIds: number[]
  ): Observable<DictionaryWord[]> {
    return this.dictionaryService.findPrivateWords(privateIds);
  }

  private isLoadedDictionary(dictionaryId: number): boolean {
    return (
      !!this.dictionary[dictionaryId] && !this.dictionary[dictionaryId].reload
    );
  }

  private isPrivateIds(privateIds: number[]): boolean {
    if (privateIds && privateIds.length) {
      return privateIds.some((id) => id === 0 || !!id);
    }
    return false;
  }

  private isHasInWords(privateId: number, words: DictionaryWord[]): boolean {
    return words.some(
      ({ id, children }) =>
        privateId === id || this.isHasInWords(privateId, children)
    );
  }

  private getValidPrivateIds(
    privateIds: number[],
    words: DictionaryWord[]
  ): number[] {
    const numberIds = privateIds.filter((id) => id === 0 || !!id);
    const validIds = numberIds.filter((id) => !this.isHasInWords(id, words));
    return validIds;
  }

  private getLocalPrivateWordsAndPrivateIds(
    privateIds: number[],
    words: DictionaryWord[],
    privateWords: DictionaryWord[]
  ) {
    const validPrivateIds = this.getValidPrivateIds(privateIds, words);
    if (this.isPrivateIds(validPrivateIds)) {
      const localPrivateWords = privateWords.filter(({ id }) =>
        validPrivateIds.includes(id)
      );

      const remainderPrivateIds = validPrivateIds.filter(
        (privateId) => !localPrivateWords.some(({ id }) => id === privateId)
      );

      return {
        localPrivateWords,
        remainderPrivateIds,
      };
    }
    return {
      remainderPrivateIds: [],
      localPrivateWords: [],
    };
  }

  private getPrivateWordsByIds(
    dictionaryId: number,
    privateIds: number[]
  ): Observable<DictionaryWord[]> {
    const {
      words,
      privateWords,
      isWordsDeletable,
      isWordsHideable,
    } = this.dictionary[dictionaryId];
    if (
      (isWordsDeletable || isWordsHideable) &&
      this.isPrivateIds(privateIds)
    ) {
      const {
        localPrivateWords,
        remainderPrivateIds,
      } = this.getLocalPrivateWordsAndPrivateIds(
        privateIds,
        words,
        privateWords
      );

      if (this.isPrivateIds(remainderPrivateIds)) {
        return this.fetchPrivateWordsByIds(remainderPrivateIds).pipe(
          tap((words) => this.addToDictionaryPrivateData(dictionaryId, words)),
          map((words) => words.concat(localPrivateWords))
        );
      }

      return of(localPrivateWords);
    }
    return of([]);
  }

  private splitWordsByParent(words: DictionaryWord[]) {
    const withParentWords = [];
    const nullParentWords = [];
    words.forEach((word) => {
      if (word.parentId === null) {
        nullParentWords.push(word);
      } else {
        withParentWords.push(word);
      }
    });

    return {
      withParentWords,
      nullParentWords,
    };
  }

  private removeWordsFromPrivateWords(
    privateWords: DictionaryWord[],
    words: DictionaryWord[]
  ) {
    return privateWords.filter(
      ({ id }) => !words.some(({ id: wordId }) => wordId === id)
    );
  }

  private concatWordsChildren(
    word: DictionaryWord,
    privateWords: DictionaryWord[]
  ) {
    const { id, children: wordChildren } = word;
    let newPrivateWords = privateWords;
    let newChildren = wordChildren;
    const suitableWords = newPrivateWords.filter(
      ({ parentId }) => parentId === id
    );
    if (suitableWords.length) {
      newPrivateWords = this.removeWordsFromPrivateWords(
        newPrivateWords,
        suitableWords
      );
      newChildren = wordChildren.concat(suitableWords);
    }

    if (newChildren.length) {
      newChildren = newChildren.map((word) => {
        const { remainderWords, children } = this.concatWordsChildren(
          word,
          newPrivateWords
        );
        newPrivateWords = remainderWords;
        return {
          ...word,
          children,
        };
      });
    }

    return {
      remainderWords: newPrivateWords,
      children: newChildren,
    };
  }

  private concatAllWords(
    words: DictionaryWord[],
    privateWords: DictionaryWord[]
  ) {
    const { withParentWords, nullParentWords } = this.splitWordsByParent(
      privateWords
    );
    const nullWords = words.concat(nullParentWords);
    if (this.isPrivateIds(withParentWords)) {
      let privateWords = withParentWords;
      return nullWords.map((word) => {
        if (this.isPrivateIds(privateWords) && word.children.length) {
          const { remainderWords, children } = this.concatWordsChildren(
            word,
            privateWords
          );
          privateWords = remainderWords;
          return {
            ...word,
            children,
          };
        }
        return word;
      });
    }
    return nullWords;
  }

  public getDictionaryWordsById(
    dictionaryId: number,
    privateIds?: number[]
  ): Observable<DictionaryWord[]> {
    return this.getWordsFromDictionary(dictionaryId).pipe(
      concatMap((words) => {
        return this.getPrivateWordsByIds(dictionaryId, privateIds).pipe(
          map((privateWords) => {
            const { maxLevel } = this.dictionary[dictionaryId];
            if (maxLevel > 1) {
              return this.concatAllWords(words, privateWords);
            }
            return [...words, ...privateWords];
          })
        );
      })
    );
  }

  public getDictionaryIndustry(componentDestroyed: Subject<void>, privateWordsIds: number[]): Observable<DictionaryWord[]> {
    return this.getDictionaryWordsById(
      DictionaryId.INDUSTRY,
      privateWordsIds
    ).pipe(
      takeUntil(componentDestroyed)
    );
  }

  public getDictionaryPosition(componentDestroyed: Subject<void>, privateWordsIds: number[]): Observable<DictionaryWord[]> {
    return this.getDictionaryWordsById(
        DictionaryId.POSITION,
        privateWordsIds
      ).pipe(
        takeUntil(componentDestroyed)
    );
  }
}
