import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DictionaryWordComponent} from './dictionary-word/dictionary-word.component';
import {DictionaryWordsListComponent} from './dictionary-words-list/dictionary-words-list.component';
import {DictionarySliderComponent} from './dictionary-slider/dictionary-slider.component';
import {DictionaryAddWordComponent} from './dictionary-add-word/dictionary-add-word.component';
import {DictionaryAdministrationComponent} from './dictionary-administration/dictionary-administration.component';
import {SharedModule} from '../shared/shared.module';
import {NgxMaskModule} from 'ngx-mask';
import {DictionaryWordsAddComponent} from "./dictionary-words-add/dictionary-words-add.component";

@NgModule({
  declarations: [
    DictionarySliderComponent,
    DictionaryWordComponent,
    DictionaryWordsListComponent,
    DictionaryAddWordComponent,
    DictionaryAdministrationComponent,
    DictionaryWordsAddComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxMaskModule
  ],
  exports: [
    DictionarySliderComponent,
    DictionaryWordComponent,
    DictionaryWordsListComponent,
    DictionaryAddWordComponent,
    DictionaryAdministrationComponent,
    DictionaryWordsAddComponent
  ],
  entryComponents: [
    DictionaryWordComponent,
    DictionaryWordsAddComponent
  ]
})
export class DictionaryModule { }
