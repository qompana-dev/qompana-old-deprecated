import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProductCategory, ProductSubcategory} from './product.model';
import {UrlUtil} from '../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class ProductCategoryService {

  private productUrl = `${UrlUtil.url}/crm-business-service`;

  constructor(private http: HttpClient) {
  }

  getCategories(): Observable<ProductCategory[]> {
    return this.http.get<ProductCategory[]>(`${this.productUrl}/products/categories`);
  }

  getCategoriesWithSubcategories(categoryIds: number[]): Observable<ProductCategory[]> {
    return this.http.get<ProductCategory[]>(`${this.productUrl}/products/categories-with-subcategories/${categoryIds.join(',')}`);
  }

  getAllCategoriesWithSubcategories(): Observable<ProductCategory[]> {
    return this.http.get<ProductCategory[]>(`${this.productUrl}/products/categories-with-subcategories`);
  }

  saveCategoriesWithSubcategories(categories: ProductCategory[]): Observable<ProductCategory[]> {
    return this.http.post<ProductCategory[]>(`${this.productUrl}/products/categories-with-subcategories`, categories);
  }

  saveCategories(categories: ProductCategory[]): Observable<ProductCategory[]> {
    return this.http.post<ProductCategory[]>(`${this.productUrl}/products/categories`, categories);
  }

  saveSubcategories(subcategories: ProductSubcategory[]): Observable<ProductSubcategory[]> {
    return this.http.post<ProductSubcategory[]>(`${this.productUrl}/products/subcategories`, subcategories);
  }

  flattenSubcategories(savedCategories: ProductCategory[]): ProductSubcategory[] {
    if (!savedCategories) {
      return [];
    }
    const flatSubcategories: ProductSubcategory[] = [];
    for (const category of savedCategories) {
      for (const subcategory of category.subcategories) {
        flatSubcategories.push({categoryId: category.id, id: subcategory.id, name: subcategory.name, toDelete: subcategory.toDelete});
      }
    }
    return flatSubcategories;
  }
}
