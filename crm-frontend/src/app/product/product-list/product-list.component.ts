import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Product } from '../product.model';
import { Observable, Subject } from 'rxjs';
import { ProductService } from '../product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { map, startWith, takeUntil, throttleTime, finalize } from 'rxjs/operators';
import { Page, PageRequest } from '../../shared/pagination/page.model';
import { Sort } from '@angular/material';
import { ErrorTypes, NotificationService, NotificationType } from '../../shared/notification.service';
import { SliderComponent } from '../../shared/slider/slider.component';
import { DeleteDialogService } from '../../shared/dialog/delete/delete-dialog.service';
import { DeleteDialogData } from '../../shared/dialog/delete/delete-dialog.component';
import { BAD_REQUEST, FORBIDDEN, NOT_FOUND } from 'http-status-codes';
import { FilterStrategy } from '../../shared/model';
import { AuthService } from '../../login/auth/auth.service';
import { PriceBookService } from '../../price-book/price-book.service';
import { PriceBook } from '../../price-book/price-book.model';
import { DictionaryId } from "../../dictionary/dictionary.model";
import { DictionaryStoreService } from 'src/app/dictionary/dictionary-store.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit, OnDestroy {

  @ViewChild('editSlider') editSlider: SliderComponent;
  columns = ['selectRow', 'name', 'category', 'subcategory', 'code', 'sellPriceNet', 'unit', 'action'];
  productsPage: Page<Product>;
  expanded = false;
  filterSelection = FilterStrategy;
  selectedFilter: FilterStrategy = FilterStrategy.ALL;
  productForEdit: Product;
  filteredColumns$: Observable<string[]>;

  loadingFinished = true;
  private componentDestroyed: Subject<void> = new Subject();
  private fetchProducts: Subject<void> = new Subject();

  private currentSort = 'id,asc';
  private pageOnList = 20;
  totalElements = 0;

  @ViewChild('addCategorySlider') addCategorySlider: SliderComponent;

  private readonly deleteDialogData: Partial<DeleteDialogData> = {
    title: 'products.list.dialog.remove.title',
    description: 'products.list.dialog.remove.description',
    noteFirst: 'products.list.dialog.remove.noteFirstPart',
    noteSecond: 'products.list.dialog.remove.noteSecondPart',
    confirmButton: 'products.list.dialog.remove.buttonYes',
    cancelButton: 'products.list.dialog.remove.buttonNo',
  };

  readonly deleteProductError: ErrorTypes = {
    base: 'products.list.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: NOT_FOUND,
        text: 'productNotFound'
      },
      {
        code: FORBIDDEN,
        text: 'noPermission'
      },
      {
        code: BAD_REQUEST,
        text: 'usedInOpportunity'
      },
      {
        code: 440,
        text: 'usedInGoal'
      }
    ]
  };
  priceId: number;
  forPrice = false;
  priceBooks: PriceBook[] = [];
  currentPriceBook: PriceBook;

  units: Map<number, string>;

  constructor(private productService: ProductService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private priceBookService: PriceBookService,
    private dictionaryStoreService: DictionaryStoreService,
    private notificationService: NotificationService,
    private deleteDialogService: DeleteDialogService) {
    this.subscribeForUnits();
  }

  ngOnInit(): void {
    this.getList();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.fetchProducts.next();
    this.componentDestroyed.complete();
    this.fetchProducts.complete();
  }

  getList(): void {
    this.forPrice = this.router.url.indexOf('/for-price-book/') !== -1;
    if (this.forPrice) {
      this.getPriceBooks();
      this.getPriceIdFromParam();
    } else {
      this.initData();
    }
  }

  private getPriceBooks(): void {
    this.priceBookService.getPriceBookToAssign()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((priceBooks: PriceBook[]) => {
        this.priceBooks = priceBooks;
        this.setCurrentPriceBook();
      });
  }

  openProductsInPrice(id: number): void {
    this.router.navigate(['products', 'for-price-book', id]);
  }

  private getPriceIdFromParam(): void {
    this.activatedRoute.params.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(params => {
      this.priceId = +params.priceBookId;
      this.initData();
      this.setCurrentPriceBook();
    });
  }

  private initData(): void {
    this.getProducts(this.selectedFilter);
    this.filteredColumns$ = this.authService.onAuthChange.pipe(
      startWith(this.getFilteredColumns()),
      map(() => this.getFilteredColumns())
    );
  }

  public setCurrentPriceBook(): void {
    if (this.priceBooks && this.priceBooks.length > 0 && this.priceId) {
      this.currentPriceBook = this.priceBooks.find((priceBook) => priceBook.id === this.priceId);
    }
  }

  public tryDelete(id: number): void {
    this.deleteDialogData.numberToDelete = 1;
    this.deleteDialogData.onConfirmClick = () => this.deleteProduct(id);
    this.deleteDialogService.open(this.deleteDialogData);
  }

  public tryDeleteSelectedProducts(): void {
    this.deleteDialogData.numberToDelete = this.productsPage.content
      .filter(customer => !!customer.checked).length;
    this.deleteDialogData.onConfirmClick = () => this.deleteSelectedProducts();
    this.deleteDialogService.open(this.deleteDialogData);
  }

  public edit(product: Product): void {
    this.productService.getProduct(product.id).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (p) => {
        this.productForEdit = p;
        this.editSlider.open();
      },
      () => this.notificationService.notify('products.list.notifications.productNotFound', NotificationType.ERROR)
    );

  }

  public onSortChange($event: Sort): void {
    this.currentSort = $event.direction ? ($event.active + ',' + $event.direction) : 'id,asc';
    this.getProducts(this.selectedFilter);
  }

  public isSelected(selection: FilterStrategy): boolean {
    return this.selectedFilter === selection;
  }


  public filter(selection: FilterStrategy): void {
    this.selectedFilter = selection;
    this.getProducts(selection);
  }

  handleProductAdded(): void {
    this.getProducts(this.selectedFilter);
  }

  private getProducts(selection: FilterStrategy, pageRequest: PageRequest =
    { page: '0', size: this.pageOnList + '', sort: this.currentSort }): void {
      this.fetchProducts.next();
      this.loadingFinished = false;
    this.getProductObservable(pageRequest, selection)
      .pipe(
        takeUntil(this.componentDestroyed),
        finalize(() => this.loadingFinished = true)

      ).subscribe(
        page => this.handleProductsSuccess(page),
        () => this.notifyError()
      );
  }

  changePagination(page) {
    const { pageIndex, pageSize } = page;
    this.pageOnList = pageSize;
    this.getProducts(FilterStrategy.ALL, { page: pageIndex, size: pageSize, sort: this.currentSort })
  }

  private handleProductsSuccess(page: any): void {
    this.totalElements = page.totalElements;
    this.productsPage = page;
  }

  private notifyError(): void {
    this.notificationService.notify('products.list.notifications.undefinedError', NotificationType.ERROR);
  }

  private deleteSelectedProducts(): void {
    if (this.productsPage && this.productsPage.content) {
      const toDelete: Product[] = this.productsPage.content.filter(product => product.checked);

      this.productService.deleteProducts(toDelete)
        .pipe(takeUntil(this.componentDestroyed))
        .subscribe(
          () => {
            this.getProducts(this.selectedFilter);
            this.notificationService.notify('products.list.notifications.removed', NotificationType.SUCCESS);
          },
          (err) => this.notificationService.notifyError(this.deleteProductError, err.status)
        );
    }
  }

  private deleteProduct(id: number): void {
    this.productService.deleteProduct(id)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(
        () => {
          this.getProducts(this.selectedFilter);
          this.notificationService.notify('products.list.notifications.removed', NotificationType.SUCCESS);
        },
        (err) => this.notificationService.notifyError(this.deleteProductError, err.status)
      );
  }

  private getFilteredColumns(): string[] {
    const key = this.forPrice ? 'ProductListInPriceBookComponent' : 'ProductListComponent';
    return this.columns
      .filter((item) => !this.authService.isPermissionPresent(key + '.' + item, 'i') || item === 'action' || item === 'selectRow');
  }

  showAddCategory(): void {
    this.addCategorySlider.open();
  }

  private getProductObservable(pageRequest: PageRequest, strategy: FilterStrategy): Observable<Page<Product>> {
    if (this.forPrice) {
      return this.productService.getProductsForPriceBook(pageRequest, this.selectedFilter, this.priceId);
    } else {
      return this.productService.getProducts(pageRequest, this.selectedFilter);
    }
  }

  isAnyChecked(): boolean {
    return this.productsPage && this.productsPage.content && this.productsPage.content.some(customer => customer.checked);
  }

  private subscribeForUnits(): void {
    this.dictionaryStoreService.getDictionaryWordsById(DictionaryId.UNIT).pipe(
      takeUntil(this.componentDestroyed),
      map(words => new Map<number, string>(words.map(i => [i.id, `${i.name}`] as [number, string]))),
    ).subscribe(
      units => {
        this.units = units
      },
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }
}
