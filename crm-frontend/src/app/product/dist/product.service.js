"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ProductService = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var table_util_1 = require("../shared/table.util");
var cloneDeep_1 = require("lodash/cloneDeep");
var permissions_utils_1 = require("../shared/permissions-utils");
var url_util_1 = require("../shared/url.util");
var ProductService = /** @class */ (function () {
    function ProductService(http) {
        this.http = http;
    }
    ProductService.prototype.getProduct = function (id) {
        var url = url_util_1.UrlUtil.url + "/crm-business-service/products/" + id;
        return this.http.get(url);
    };
    ProductService.prototype.getProducts = function (pageRequest, strategy) {
        var params = new http_1.HttpParams({ fromObject: pageRequest });
        params = table_util_1.TableUtil.enrichParamsWithFilterStrategy(strategy, params);
        var url = url_util_1.UrlUtil.url + "/crm-business-service/products";
        return this.http.get(url, { params: params });
    };
    ProductService.prototype.getProductsForPriceBook = function (pageRequest, strategy, priceBookId, fromOpportunityForm) {
        var pageRequestToSend = cloneDeep_1["default"](pageRequest);
        if (pageRequestToSend && pageRequestToSend.sort) {
            pageRequestToSend.sort = 'product.' + pageRequestToSend.sort;
        }
        var params = new http_1.HttpParams({ fromObject: pageRequestToSend });
        params = table_util_1.TableUtil.enrichParamsWithFilterStrategy(strategy, params);
        var url = url_util_1.UrlUtil.url + "/crm-business-service/products/for-price-book/" + priceBookId;
        return this.http.get(url, { params: params, headers: permissions_utils_1.getHeaderForAuthType((fromOpportunityForm) ? 'addProductFromPriceBook' : undefined) });
    };
    ProductService.prototype.getProductHistory = function (productId, finishResult) {
        var params = new http_1.HttpParams();
        if (finishResult) {
            params.set('finishResult', finishResult);
        }
        return this.http.get(url_util_1.UrlUtil.url + "/crm-business-service/product/" + productId + "/salesHistory", { params: params });
    };
    ProductService.prototype.uploadProducts = function (file, importStrategy, fileWithHeader) {
        var formData = new FormData();
        formData.append('file', file);
        formData.append('strategy', importStrategy);
        formData.append('headerPresent', fileWithHeader);
        var url = url_util_1.UrlUtil.url + "/crm-business-service/products/import";
        return this.http.post(url, formData);
    };
    ProductService.prototype.createProduct = function (product) {
        var url = url_util_1.UrlUtil.url + "/crm-business-service/products";
        return this.http.post(url, product);
    };
    ProductService.prototype.deleteProducts = function (products) {
        var params = new http_1.HttpParams().set('ids', products.map(function (customer) { return customer.id; }).join(','));
        return this.http["delete"](url_util_1.UrlUtil.url + "/crm-business-service/products", { params: params });
    };
    ProductService.prototype.deleteProduct = function (id) {
        return this.http["delete"](url_util_1.UrlUtil.url + "/crm-business-service/products/" + id);
    };
    ProductService.prototype.modifyProduct = function (id, product) {
        var url = url_util_1.UrlUtil.url + "/crm-business-service/products/" + id;
        return this.http.put(url, product);
    };
    ProductService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], ProductService);
    return ProductService;
}());
exports.ProductService = ProductService;
