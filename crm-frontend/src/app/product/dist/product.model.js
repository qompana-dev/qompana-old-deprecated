"use strict";
exports.__esModule = true;
exports.productSubcategoryComparator = exports.ProductSubcategory = exports.ProductCategory = exports.SellHistoryResult = exports.ProductsImportStrategy = void 0;
var ProductsImportStrategy;
(function (ProductsImportStrategy) {
    ProductsImportStrategy["Override"] = "OVERRIDE";
    ProductsImportStrategy["AddAll"] = "ADD_ALL";
    ProductsImportStrategy["AddOnlyNew"] = "ADD_ONLY_NEW";
    ProductsImportStrategy["UpdateExisting"] = "UPDATE_EXISTING";
})(ProductsImportStrategy = exports.ProductsImportStrategy || (exports.ProductsImportStrategy = {}));
var SellHistoryResult;
(function (SellHistoryResult) {
    SellHistoryResult["SUCCESS"] = "SUCCESS";
    SellHistoryResult["ERROR"] = "ERROR";
})(SellHistoryResult = exports.SellHistoryResult || (exports.SellHistoryResult = {}));
var ProductCategory = /** @class */ (function () {
    function ProductCategory(category) {
        this.name = category;
        this.subcategories = [];
    }
    return ProductCategory;
}());
exports.ProductCategory = ProductCategory;
var ProductSubcategory = /** @class */ (function () {
    function ProductSubcategory(category) {
        this.name = category;
    }
    return ProductSubcategory;
}());
exports.ProductSubcategory = ProductSubcategory;
function productSubcategoryComparator(a, b) {
    return a && b && a.categoryId === b.categoryId && a.name === b.name;
}
exports.productSubcategoryComparator = productSubcategoryComparator;
