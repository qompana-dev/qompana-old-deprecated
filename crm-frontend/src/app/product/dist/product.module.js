"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ProductModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var product_routing_module_1 = require("./product-routing.module");
var shared_module_1 = require("../shared/shared.module");
var product_list_component_1 = require("./product-list/product-list.component");
var product_import_component_1 = require("./product-import/product-import.component");
var product_form_component_1 = require("./product-form/product-form.component");
var product_add_component_1 = require("./product-add/product-add.component");
var ngx_mask_1 = require("ngx-mask");
var product_edit_component_1 = require("./product-edit/product-edit.component");
var product_price_component_1 = require("./product-form/product-price/product-price.component");
var photo_uploader_component_1 = require("./product-form/photo-uploader/photo-uploader.component");
var file_uploader_component_1 = require("./product-form/file-uploader/file-uploader.component");
var add_product_to_price_book_form_component_1 = require("./product-form/product-price/add-product-to-price-book-form/add-product-to-price-book-form.component");
var categories_module_1 = require("./categories/categories.module");
var sell_history_component_1 = require("./product-form/sell-history/sell-history.component");
var file_drag_drop_directive_1 = require("./product-import/file-drag-drop.directive");
var files_module_1 = require("../files/files.module");
var widget_module_1 = require("../widgets/widget.module");
var ProductModule = /** @class */ (function () {
    function ProductModule() {
    }
    ProductModule = __decorate([
        core_1.NgModule({
            declarations: [
                file_drag_drop_directive_1.FileDragDropDirective,
                product_list_component_1.ProductListComponent,
                product_import_component_1.ProductImportComponent,
                product_form_component_1.ProductFormComponent,
                product_add_component_1.ProductAddComponent,
                product_edit_component_1.ProductEditComponent,
                product_price_component_1.ProductPriceComponent,
                sell_history_component_1.SellHistoryComponent,
                add_product_to_price_book_form_component_1.AddProductToPriceBookFormComponent,
                photo_uploader_component_1.PhotoUploaderComponent,
                file_uploader_component_1.FileUploaderComponent,
            ],
            imports: [
                common_1.CommonModule,
                shared_module_1.SharedModule,
                product_routing_module_1.ProductRoutingModule,
                ngx_mask_1.NgxMaskModule,
                categories_module_1.CategoriesModule,
                widget_module_1.WidgetModule,
                files_module_1.FilesModule,
            ]
        })
    ], ProductModule);
    return ProductModule;
}());
exports.ProductModule = ProductModule;
