import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors} from '@angular/forms';
import {Subject} from 'rxjs';
import {ProductCategory, ProductSubcategory, productSubcategoryComparator} from '../../product.model';
import {ProductCategoryService} from '../../product-category.service';
import {SliderService} from '../../../shared/slider/slider.service';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {filter, takeUntil} from 'rxjs/operators';
import {MatCheckbox} from '@angular/material';
import * as intersectionWith from 'lodash/intersectionWith';
import * as cloneDeep from 'lodash/cloneDeep';
import * as isEqual from 'lodash/isEqual';
import * as isNil from 'lodash/isNil';
import {
  deleteCategoryDialogData,
  deleteSubcategoryDialogData,
  warningDialogData
} from '../product-category/category.model';
import {WarningDialogService} from '../../../shared/dialog/warning/warning-dialog.service';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';

@Component({
  selector: 'app-add-product-subcategory',
  templateUrl: './add-product-subcategory.component.html',
  styleUrls: ['./add-product-subcategory.component.scss']
})
export class AddProductSubcategoryComponent implements OnInit, OnDestroy {

  categoryForm: FormGroup;
  categories: ProductCategory[];
  originalCategories: ProductCategory[];
  @Output() onSubmitClick: EventEmitter<number[]> = new EventEmitter();
  @Output() onCancelClick: EventEmitter<void> = new EventEmitter();
  @Input() categoryIds: number[];
  @Input() subcategoryIds: number[];
  @ViewChildren(MatCheckbox) checkboxes: QueryList<MatCheckbox>;
  @Input() selectedSubcategoriesIds: number[] = [];
  private componentDestroyed: Subject<void> = new Subject();
  addNewInputVisible: boolean[] = [];
  toDelete: ProductSubcategory[] = [];
  private editedCategoryIndex: number;
  private editedSubcategoryIndex: number;


  constructor(
    public categoryService: ProductCategoryService,
    public sliderService: SliderService,
    public notificationService: NotificationService,
    private fb: FormBuilder,
    private warningDialogService: WarningDialogService,
    private deleteDialogService: DeleteDialogService) {
    this.createNewSubcategoriesForm();
  }

  // @formatter:off
  get categoriesArray(): FormArray { return this.categoryForm.get('categories') as FormArray; }
  // @formatter:on

  ngOnInit(): void {
    this.listenWhenSliderOpened();
    this.listenWhenSliderClosedByCross();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    const subcategories = this.categoryService.flattenSubcategories(this.categories);
    this.categoryService.saveSubcategories(subcategories)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      savedCategories => {
        const selectedIds = this.getSelectedIds(savedCategories);
        this.onSubmitClick.emit(selectedIds);
        this.notificationService.notify('products.categories.notification.saveSubcategorySuccess', NotificationType.SUCCESS);
      },
      err => {
        if (err.status === 440) {
          this.notificationService.notify('products.categories.notification.usedInGoal', NotificationType.ERROR)
        } else {
          this.notificationService.notify('products.categories.notification.saveSubcategoriesError', NotificationType.ERROR)
        }
      }
    );
  }

  getProductCategories(): void {
    this.categoryService.getCategoriesWithSubcategories(this.categoryIds).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      categories => {
        this.categories = categories;
        this.originalCategories = cloneDeep(categories);
        this.createCategoriesArrayForm();
      },
      err => this.notificationService.notify('products.categories.notification.getSubcategoriesListError', NotificationType.ERROR)
    );
  }

  cancel(): void {
    this.closeIfPossible();
  }

  close(): void {
    this.onCancelClick.emit();
    this.categories = [];
    this.originalCategories = [];
    this.toDelete = [];
  }

  addNewCategory($event: MouseEvent, input: HTMLInputElement, index: number): void {
    $event.stopPropagation();
    if (!this.addNewInputVisible[index]) {
      this.addNewInputVisible[index] = true;
      setTimeout(() => input.focus(), 100);
    } else {
      this.addCategory(index);
    }
  }

  addCategory(categoryIndex: number): void {
    const newSubcategoryControl: AbstractControl = this.getNewSubcategoryControl(categoryIndex);
    const newSubcategory = newSubcategoryControl.value.trim();
    const subcategories = this.categories[categoryIndex].subcategories;
    if (!!newSubcategory && !newSubcategoryControl.hasError('subcategoryExistError')) {
      subcategories.push(new ProductCategory(newSubcategory));
      newSubcategoryControl.patchValue('');
      this.getSubcategoriesArray(categoryIndex).push(this.createSingleSubcategoryForm(newSubcategory, categoryIndex));
    }
  }

  private getNewSubcategoryControl(categoryIndex: number): AbstractControl {
    return this.categoriesArray.get([categoryIndex, 'newSubcategoryValue']);
  }

  onCardClick($event: MouseEvent): void {
    this.addNewInputVisible = [];
    this.disableEditedControl();
    for (const control of this.categoriesArray.controls) {
      control.get('newSubcategoryValue').patchValue('');
    }
  }

  private createNewSubcategoriesForm(): void {
    if (!this.categoryForm) {
      this.categoryForm = new FormGroup({
        categories: this.fb.array([]),
      });
    }
  }

  private listenWhenSliderClosedByCross(): void {
    this.sliderService.closeClicked.pipe(
      takeUntil(this.componentDestroyed),
      filter(key => key === 'ProductForm.AddSubcategorySlider'),
    ).subscribe(() => {
      this.closeIfPossible();
    });
  }

  private listenWhenSliderOpened(): void {
    this.sliderService.sliderStateChanged.pipe(
      takeUntil(this.componentDestroyed),
      filter(change => change.opened),
      filter(change => change.key === 'ProductForm.AddSubcategorySlider'),
    ).subscribe(() => {
      this.getProductCategories();
    });
  }

  private createCategoriesArrayForm(): void {
    this.clearFormArray(this.categoriesArray);
    this.categories.forEach((category, index) => this.categoriesArray.push(this.createCategoryForm(category, index)));
  }

  private createCategoryForm(category: ProductCategory, categoryIndex: number): FormGroup {
    const formGroup = new FormGroup({
      newSubcategoryValue: new FormControl('', this.getSubcategoryExistValidator(categoryIndex)),
      subcategories: this.fb.array([]),
    });
    category.subcategories.forEach(subcategory => (formGroup.get('subcategories') as FormArray)
      .push(this.createSingleSubcategoryForm(subcategory.name, categoryIndex)));
    return formGroup;
  }

  private createSingleSubcategoryForm(subcategoryName: string, categoryIndex: number): AbstractControl {
    return new FormControl({
      value: subcategoryName,
      disabled: true
    }, this.getSubcategoryExistValidator(categoryIndex, subcategoryName));
  }

  private clearFormArray(formArray: FormArray): void {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  private getSelectedIds(savedCategories: ProductCategory[]): number[] {
    const selected: ProductSubcategory[] = this.getSelectedSubcategories();
    const categories: ProductSubcategory[] = this.categoryService.flattenSubcategories(savedCategories);
    return intersectionWith(categories, selected, productSubcategoryComparator).map(c => c.id);
  }

  private getSelectedSubcategories(): ProductSubcategory[] {
    return this.checkboxes.filter(item => item.checked).map(item => [item.value[0], item.value[1]])
      .map((item: any) => ({
        id: this.getSubcategory(item[0], item[1]).id,
        categoryId: this.categories[item[0]].id,
        name: this.getSubcategoryControl(item[0], item[1]).value
      }));
  }

  isCheckboxChecked(categoryId: number, subcategoryId: number): boolean {
    if (this.subcategoryIds) {
      return this.subcategoryIds.includes(subcategoryId);
    } else {
      return false;
    }
  }

  private getSubcategoriesArray(categoryIndex: number): FormArray {
    return this.categoriesArray.get([categoryIndex, 'subcategories']) as FormArray;
  }

  private getSubcategoryControl(categoryIndex: number, subcategoryIndex: number): AbstractControl {
    return this.categoriesArray.get([categoryIndex, 'subcategories', subcategoryIndex]) as AbstractControl;
  }

  private getSubcategory(categoryIndex: number, subcategoryIndex: number): ProductCategory {
    return this.categories[categoryIndex].subcategories[subcategoryIndex];
  }

  private getCategorySubcategories(categoryIndex: number): ProductCategory[] {
    return this.categories[categoryIndex].subcategories;
  }


  edit(categoryIndex: number, subcategoryIndex: number, input: HTMLInputElement): void {
    this.editedCategoryIndex = categoryIndex;
    this.editedSubcategoryIndex = subcategoryIndex;
    this.getSubcategoryControl(categoryIndex, subcategoryIndex).enable();
    input.focus();
  }

  delete(categoryIndex: number, subcategoryIndex: number): void {
    const subcategory = this.getSubcategory(categoryIndex, subcategoryIndex);
    if (subcategory.id) {
      subcategory.toDelete = true;
    } else {
      this.getCategorySubcategories(categoryIndex).splice(subcategoryIndex, 1);
      this.getSubcategoriesArray(categoryIndex).removeAt(subcategoryIndex);
    }
  }

  private disableEditedControl(): void {
    if (isNil(this.editedCategoryIndex)) {
      return;
    }
    const editedControl = this.getSubcategoryControl(this.editedCategoryIndex, this.editedSubcategoryIndex);
    const editedCategory = editedControl.value.trim();
    if (!editedCategory || editedControl.hasError('subcategoryExistError')) {
      editedControl.patchValue(this.getSubcategory(this.editedCategoryIndex, this.editedSubcategoryIndex).name);
    } else {
      this.getSubcategory(this.editedCategoryIndex, this.editedSubcategoryIndex).name = editedCategory;
    }
    editedControl.disable();
  }

  private closeIfPossible(): void {
    if (this.areCategoriesChanged()) {
      this.showCloseWarningDialog();
    } else {
      this.close();
    }
  }

  private areCategoriesChanged(): boolean {
    return !isEqual(this.originalCategories, this.categories);
  }

  private showCloseWarningDialog(): void {
    warningDialogData.onConfirmClick = () => this.submit();
    warningDialogData.onCancelClick = () => this.close();
    this.warningDialogService.open(warningDialogData);
  }

  private getSubcategoryExistValidator(categoryIndex: number, name: string = ''): (control: AbstractControl) => ValidationErrors {
    return (control: AbstractControl): ValidationErrors => {
      control.markAsTouched();
      const newSubcategory = (control.value || '').trim();
      const subcategories = this.categories[categoryIndex] && this.categories[categoryIndex].subcategories;
      return (!!subcategories && subcategories
        .filter(c => !c.toDelete)
        .map(c => c.name)
        .filter(c => c !== name)
        .includes(newSubcategory)) ? {subcategoryExistError: true} : null;
    };
  }

  tryDelete(i: number, j: number): void {
    const subcategory = this.getSubcategory(i, j);
    if (subcategory.assignedToAnyProduct) {
      deleteSubcategoryDialogData.onConfirmClick = () => this.delete(i, j);
      this.openDeleteDialog();
    } else {
      this.delete(i, j);
    }
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(deleteCategoryDialogData);
  }
}
