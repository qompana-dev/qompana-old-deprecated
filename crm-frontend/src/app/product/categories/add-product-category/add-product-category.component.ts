import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors} from '@angular/forms';
import {Subject} from 'rxjs';
import {ProductCategory} from '../../product.model';
import {ProductCategoryService} from '../../product-category.service';
import {SliderService} from '../../../shared/slider/slider.service';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {filter, takeUntil} from 'rxjs/operators';
import * as cloneDeep from 'lodash/cloneDeep';
import * as isEqual from 'lodash/isEqual';
import {deleteCategoryDialogData, warningDialogData} from '../product-category/category.model';
import {WarningDialogService} from '../../../shared/dialog/warning/warning-dialog.service';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';


@Component({
  selector: 'app-add-product-category',
  templateUrl: './add-product-category.component.html',
  styleUrls: ['./add-product-category.component.scss']
})
export class AddProductCategoryComponent implements OnInit, OnDestroy {

  categoryForm: FormGroup;
  private componentDestroyed: Subject<void> = new Subject();
  categories: ProductCategory[];
  private originalCategories: ProductCategory[];
  addNewInputVisible = false;
  @Output() onSubmitClick: EventEmitter<number[]> = new EventEmitter();
  @Output() onCancelClick: EventEmitter<void> = new EventEmitter();
  @Input() selectedCategoriesIds: number[] = [];
  private editedCategoryIndex: number;


  constructor(
    public categoryService: ProductCategoryService,
    public sliderService: SliderService,
    public notificationService: NotificationService,
    private fb: FormBuilder,
    private warningDialogService: WarningDialogService,
    private deleteDialogService: DeleteDialogService) {
    this.createCategoryForm();
  }


  // @formatter:off
  get newCategoryValue(): AbstractControl { return this.categoryForm.get('newCategoryValue'); }
  get categoriesArray(): FormArray { return this.categoryForm.get('categories') as FormArray; }
  // @formatter:on

  ngOnInit(): void {
    this.listenWhenSliderOpened();
    this.listenWhenSliderClosedByCross();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    this.categoryService.saveCategories(this.categories)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      savedCategories => {
        const selectedIds = this.getSelectedIds(savedCategories);
        this.onSubmitClick.emit(selectedIds);
        this.notificationService.notify('products.categories.notification.saveCategorySuccess', NotificationType.SUCCESS);
      },
      err => {
        if (err.status === 440) {
          this.notificationService.notify('products.categories.notification.usedInGoal', NotificationType.ERROR)
        } else {
          this.notificationService.notify('products.categories.notification.saveCategoriesError', NotificationType.ERROR)
        }
      }
    );
  }

  private getSelectedIds(savedCategories: ProductCategory[]): number[] {
    const selectedNames = this.categoriesArray.value
      .map((v, i) => v.selected ? this.categoriesArray.get([i, 'editedValue']).value : null)
      .filter(v => v !== null);
    return selectedNames.map(categoryName => {
      const found = savedCategories.find(category => category.name === categoryName);
      return found ? found.id : null;
    }).filter(v => !!v);
  }

  cancel(): void {
    this.closeIfPossible();
  }

  close(): void {
    this.onCancelClick.emit();
    this.categories = [];
    this.editedCategoryIndex = null;
    this.originalCategories = [];
    this.addNewInputVisible = false;
  }

  private createCategoryForm(): void {
    this.categoryForm = new FormGroup({
      newCategoryValue: new FormControl('', [this.getCategoryExistValidator()]),
      categories: this.fb.array([]),
    });
  }

  private listenWhenSliderClosedByCross(): void {
    this.sliderService.closeClicked.pipe(
      takeUntil(this.componentDestroyed),
      filter(key => key === 'ProductForm.AddCategorySlider'),
    ).subscribe(() => {
      this.closeIfPossible();
    });
  }

  private listenWhenSliderOpened(): void {
    this.sliderService.sliderStateChanged.pipe(
      takeUntil(this.componentDestroyed),
      filter(change => change.opened),
      filter(change => change.key === 'ProductForm.AddCategorySlider'),
    ).subscribe(() => {
      this.getProductCategories();
    });
  }

  private getProductCategories(): void {
    this.categories = [];
    this.categoryService.getCategories().pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      categories => {
        this.categories = categories;
        this.originalCategories = cloneDeep(categories);
        this.addCategoriesToForm();
        this.markSelectedCheckboxes();
      },
      err => this.notificationService.notify('products.form.notifications.getCategoriesError', NotificationType.ERROR)
    );
  }

  addNewCategory($event: MouseEvent, input: HTMLInputElement): void {
    $event.stopPropagation();
    if (!this.addNewInputVisible) {
      this.addNewInputVisible = true;
      setTimeout(() => input.focus(), 100);
    } else {
      this.addCategory();
    }
  }

  addCategory(): void {
    const newCategory = this.newCategoryValue.value.trim();
    if (!!newCategory && !this.newCategoryValue.hasError('categoryExistError')) {
      this.categories.push(new ProductCategory(newCategory));
      this.categoriesArray.push(this.createSingleCategoryForm(newCategory));
      this.newCategoryValue.patchValue('');
    }
  }

  onCardClick($event: MouseEvent): void {
    this.addNewInputVisible = false;
    this.disableEditedControl();
    this.newCategoryValue.patchValue('');
  }

  private addCategoriesToForm(): void {
    this.clearFormArray(this.categoriesArray);
    for (const category of this.categories) {
      this.categoriesArray.push(this.createSingleCategoryForm(category.name));
    }
  }

  private createSingleCategoryForm(name: string): FormGroup {
    return new FormGroup({
      editedValue: new FormControl({value: name, disabled: true}, [this.getCategoryExistValidator(name)]),
      selected: new FormControl(null),
    });
  }

  private clearFormArray(formArray: FormArray): void {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  private markSelectedCheckboxes(): void {
    this.categories.forEach((category, index) => {
      if (this.selectedCategoriesIds && this.selectedCategoriesIds.length > 0 && this.selectedCategoriesIds.includes(category.id)) {
        this.categoriesArray.get([index, 'selected']).patchValue(true);
      }
    });
  }

  edit(category: ProductCategory, index: number, categoryInput: HTMLInputElement): void {
    this.editedCategoryIndex = index;
    this.getCategoryControl(index).enable();
    categoryInput.focus();
  }

  delete(category: ProductCategory, index: number): void {
    if (category.id) {
      category.toDelete = true;
    } else {
      this.categories.splice(index, 1);
      this.categoriesArray.removeAt(index);
    }
  }


  private disableEditedControl(): void {
    const editedControl = this.getCategoryControl(this.editedCategoryIndex);
    if (!editedControl) {
      return;
    }
    const editedCategory = editedControl.value.trim();
    if (!editedCategory || editedControl.hasError('categoryExistError')) {
      editedControl.patchValue(this.categories[this.editedCategoryIndex].name);
    } else {
      this.categories[this.editedCategoryIndex].name = editedCategory;
    }
    editedControl.disable();
  }

  private getCategoryControl(index: number): AbstractControl {
    return this.categoriesArray.get([index, 'editedValue']);
  }

  private closeIfPossible(): void {
    if (this.areCategoriesChanged()) {
      this.showCloseWarningDialog();
    } else {
      this.close();
    }
  }

  private areCategoriesChanged(): boolean {
    return !isEqual(this.originalCategories, this.categories);
  }

  private showCloseWarningDialog(): void {
    warningDialogData.onConfirmClick = () => this.submit();
    warningDialogData.onCancelClick = () => this.close();
    this.warningDialogService.open(warningDialogData);
  }


  private getCategoryExistValidator(name: string = ''): (control: AbstractControl) => ValidationErrors {
    return (control: AbstractControl): ValidationErrors => {
      control.markAsTouched();
      const category = (control.value || '').trim();
      return (!!this.categories && this.categories
        .filter(c => !c.toDelete)
        .map(c => c.name)
        .filter(c => c !== name)
        .includes(category)) ? {categoryExistError: true} : null;
    };
  }

  tryDelete(category: ProductCategory, i: number): void {
    if (category.assignedToAnyProduct) {
      deleteCategoryDialogData.onConfirmClick = () => this.delete(category, i);
      this.openDeleteDialog();
    } else {
      this.delete(category, i);
    }
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(deleteCategoryDialogData);
  }
}
