import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddProductCategoryComponent} from './add-product-category/add-product-category.component';
import {AddProductSubcategoryComponent} from './add-product-subcategory/add-product-subcategory.component';
import {ProductCategoryComponent} from './product-category/product-category.component';
import {SharedModule} from '../../shared/shared.module';
import {NgxMaskModule} from 'ngx-mask';

@NgModule({
  declarations: [
    AddProductCategoryComponent,
    AddProductSubcategoryComponent,
    ProductCategoryComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxMaskModule
  ],
  exports: [
    AddProductCategoryComponent,
    AddProductSubcategoryComponent,
    ProductCategoryComponent
  ]
})
export class CategoriesModule { }
