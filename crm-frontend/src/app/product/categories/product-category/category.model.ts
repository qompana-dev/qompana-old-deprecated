import {WarningDialogData} from '../../../shared/dialog/warning/warning-dialog.component';
import {DeleteDialogData} from '../../../shared/dialog/delete/delete-dialog.component';

export const warningDialogData: Partial<WarningDialogData> = {
  title: 'products.categories.closeWarning.title',
  description: 'products.categories.closeWarning.description',
  confirmButton: 'products.categories.closeWarning.confirm',
  cancelButton: 'products.categories.closeWarning.cancel',
};


export const deleteCategoryDialogData: Partial<DeleteDialogData> = {
  title: 'products.categories.removeDialog.titleCategory',
  description: 'products.categories.removeDialog.descriptionCategory',
  noteFirst: 'products.categories.removeDialog.noteFirstPart',
  confirmButton: 'products.categories.removeDialog.buttonYes',
  cancelButton: 'products.categories.removeDialog.buttonNo',
};

export const deleteSubcategoryDialogData: Partial<DeleteDialogData> = {
  title: 'products.categories.removeDialog.titleSubcategory',
  description: 'products.categories.removeDialog.titleSubcategory',
  noteFirst: 'products.categories.removeDialog.noteFirstPart',
  confirmButton: 'products.categories.removeDialog.buttonYes',
  cancelButton: 'products.categories.removeDialog.buttonNo',
};

