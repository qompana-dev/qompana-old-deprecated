import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors} from '@angular/forms';
import {Subject} from 'rxjs';
import {NotificationService, NotificationType} from '../../../shared/notification.service';
import {SliderService} from '../../../shared/slider/slider.service';
import {filter, takeUntil} from 'rxjs/operators';
import {ProductCategory, ProductSubcategory} from '../../product.model';
import {ProductCategoryService} from '../../product-category.service';
import * as cloneDeep from 'lodash/cloneDeep';
import * as isNil from 'lodash/isNil';
import * as isEqual from 'lodash/isEqual';
import {deleteCategoryDialogData, deleteSubcategoryDialogData, warningDialogData} from './category.model';
import {WarningDialogService} from '../../../shared/dialog/warning/warning-dialog.service';
import {DeleteDialogService} from '../../../shared/dialog/delete/delete-dialog.service';


@Component({
  selector: 'app-product-category',
  templateUrl: './product-category.component.html',
  styleUrls: ['./product-category.component.scss']
})
export class ProductCategoryComponent implements OnInit, OnDestroy {

  categoryForm: FormGroup;
  private componentDestroyed: Subject<void> = new Subject();
  categories: ProductCategory[] = [];
  addNewCategoryInputVisible: boolean;
  addNewSubcategoryInputVisible: boolean;
  selectedCategory: ProductCategory;
  private selectedCategoryIndex: number;
  private editedCategoryIndex: number;
  private editedSubcategoryIndex: number;
  private originalCategories: ProductCategory[];


  constructor(
    public categoryService: ProductCategoryService,
    public sliderService: SliderService,
    public notificationService: NotificationService,
    private fb: FormBuilder,
    private warningDialogService: WarningDialogService,
    private deleteDialogService: DeleteDialogService) {
    this.createCategoriesForm();
  }


  // @formatter:off
  get newCategoryControl(): AbstractControl { return this.categoryForm.get('newCategory'); }
  get newSubcategoryControl(): AbstractControl { return this.categoryForm.get('newSubcategory'); }
  get categoriesArray(): FormArray { return this.categoryForm.get('categories') as FormArray; }
  get subcategoriesArray(): FormArray { return this.categoryForm.get('subcategories') as FormArray; }
  // @formatter:on

  ngOnInit(): void {
    this.listenWhenSliderOpened();
    this.listenWhenSliderClosedByCross();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  close(): void {
    this.sliderService.closeSlider();
    this.categories = [];
    this.originalCategories = [];
    this.selectedCategory = null;
    this.selectedCategoryIndex = null;
    this.editedCategoryIndex = null;
    this.editedSubcategoryIndex = null;
  }

  submit(): void {
    this.categoryService.saveCategoriesWithSubcategories(this.categories)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      savedCategories => {
        this.notificationService.notify('products.categories.notification.saveCategorySuccess', NotificationType.SUCCESS);
        this.close();
      },
      err => {
        if (err.status === 440) {
          this.notificationService.notify('products.categories.notification.usedInGoal', NotificationType.ERROR)
        } else {
          this.notificationService.notify('products.categories.notification.saveCategoriesError', NotificationType.ERROR)
        }
      }
    );
  }

  cancel(): void {
    this.closeIfPossible();
  }


  private createCategoriesForm(): void {
    this.categoryForm = new FormGroup({
      newCategory: new FormControl('', [this.getCategoryExistValidator()]),
      newSubcategory: new FormControl('', [this.getSubcategoryExistValidator()]),
      categories: this.fb.array([]),
      subcategories: this.fb.array([]),
    });
  }

  private getCategoryExistValidator(name: string = ''): (control: AbstractControl) => ValidationErrors {
    return (control: AbstractControl): ValidationErrors => {
      control.markAsTouched();
      const newCategory = (control.value || '').trim();
      return (!!this.categories && this.categories
        .filter(c => !c.toDelete)
        .map(c => c.name)
        .filter(c => c !== name)
        .includes(newCategory)) ? {categoryExistError: true} : null;
    };
  }

  private getSubcategoryExistValidator(name: string = ''): (control: AbstractControl) => ValidationErrors {
    return (control: AbstractControl): ValidationErrors => {
      control.markAsTouched();
      const newSubcategory = (control.value || '').trim();
      const subcategories = this.categories[this.selectedCategoryIndex] && this.categories[this.selectedCategoryIndex].subcategories;
      return (!!subcategories && subcategories
        .filter(c => !c.toDelete)
        .map(c => c.name)
        .filter(c => c !== name)
        .includes(newSubcategory)) ? {subcategoryExistError: true} : null;
    };
  }

  private getCategories(): void {
    this.categoryService.getAllCategoriesWithSubcategories()
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
      categories => {
        this.originalCategories = cloneDeep(categories);
        this.categories = categories;
        this.addCategoriesToForm();
        this.selectFirstCategory();
      },
      err => this.notificationService.notify('products.categories.notification.getCategoriesListError', NotificationType.ERROR)
    );
  }

  private addCategoriesToForm(): void {
    this.clearFormArray(this.categoriesArray);
    this.categories.forEach(category => this.categoriesArray.push(this.createCategoryControl(category.name)));
  }

  private createCategoryControl(name: string): FormControl {
    return new FormControl({value: name, disabled: true}, [this.getCategoryExistValidator(name)]);
  }

  private createSubcategoryControl(name: string): FormControl {
    return new FormControl({value: name, disabled: true}, [this.getSubcategoryExistValidator(name)]);
  }

  private addSubcategoriesToForm(subcategories: ProductCategory[]): void {
    this.clearFormArray(this.subcategoriesArray);
    subcategories.forEach(subcategory => this.subcategoriesArray.push(this.createSubcategoryControl(subcategory.name)));
  }

  private listenWhenSliderOpened(): void {
    this.sliderService.sliderStateChanged.pipe(
      takeUntil(this.componentDestroyed),
      filter(change => change.opened),
      filter(change => change.key === 'ProductList.AddCategory'),
    ).subscribe(() => {
      this.getCategories();
    });
  }

  disableEditedCategoryControl(): void {
    if (isNil(this.editedCategoryIndex)) {
      return;
    }
    const control = this.getCategoryControl(this.editedCategoryIndex);
    const editedCategory = control.value.trim();
    if (!editedCategory || control.hasError('categoryExistError')) {
      control.patchValue(this.categories[this.editedCategoryIndex].name);
    } else {
      this.categories[this.editedCategoryIndex].name = editedCategory;
    }
    this.editedCategoryIndex = null;
    control.disable();
  }

  disableEditedSubcategoryControl(): void {
    if (isNil(this.editedSubcategoryIndex)) {
      return;
    }
    const control = this.getSubcategoryControl(this.editedSubcategoryIndex);
    const editedCategory = control.value.trim();
    if (!editedCategory || control.hasError('subcategoryExistError')) {
      control.patchValue(this.getSubcategory(this.selectedCategoryIndex, this.editedSubcategoryIndex).name);
    } else {
      this.getSubcategory(this.selectedCategoryIndex, this.editedSubcategoryIndex).name = editedCategory;
    }
    this.editedSubcategoryIndex = null;
    control.disable();
  }


  deleteCategory(category: ProductCategory, index: number): void {
    this.addNewCategoryInputVisible = false;
    if (category.id) {
      category.toDelete = true;
    } else {
      this.categories.splice(index, 1);
      this.categoriesArray.removeAt(index);
    }
    if (index === this.selectedCategoryIndex) {
      this.selectedCategoryIndex = null;
      this.selectedCategory = null;
    }
  }

  addCategory(): void {
    const newCategory = this.newCategoryControl.value.trim();
    if (!!newCategory && !this.newCategoryControl.hasError('categoryExistError')) {
      this.categories.push(new ProductCategory(newCategory));
      this.categoriesArray.push(this.createCategoryControl(newCategory));
      this.newCategoryControl.patchValue('');
    }
  }

  addNewCategory($event: MouseEvent, input: HTMLInputElement): void {
    $event.stopPropagation();
    if (!this.addNewCategoryInputVisible) {
      this.addNewCategoryInputVisible = true;
      setTimeout(() => input.focus(), 100);
    } else {
      this.addCategory();
    }
  }

  addNewSubcategory($event: MouseEvent, input: HTMLInputElement): void {
    if (!this.selectedCategory) {
      return;
    }
    $event.stopPropagation();
    if (!this.addNewCategoryInputVisible) {
      this.addNewSubcategoryInputVisible = true;
      setTimeout(() => input.focus(), 100);
    } else {
      this.addSubcategory();
    }
  }

  editCategory(index: number, categoryInput: HTMLInputElement): void {
    this.addNewCategoryInputVisible = false;
    this.editedCategoryIndex = index;
    this.getCategoryControl(index).enable();
    categoryInput.focus();
  }

  editSubcategory(index: number, categoryInput: HTMLInputElement): void {
    this.addNewSubcategoryInputVisible = false;
    this.editedSubcategoryIndex = index;
    this.getSubcategoryControl(index).enable();
    categoryInput.focus();
  }

  deleteSubcategory(subcategory: ProductSubcategory, index: number): void {
    this.addNewSubcategoryInputVisible = false;
    if (subcategory.id) {
      subcategory.toDelete = true;
    } else {
      this.categories[this.selectedCategoryIndex].subcategories.splice(index, 1);
      this.subcategoriesArray.removeAt(index);
    }
  }

  addSubcategory(): void {
    const newSubcategory = this.newSubcategoryControl.value.trim();
    const subcategories = this.categories[this.selectedCategoryIndex].subcategories;
    if (!!newSubcategory && !this.newSubcategoryControl.hasError('subcategoryExistError')) {
      subcategories.push(new ProductSubcategory(newSubcategory));
      this.newSubcategoryControl.patchValue('');
      this.subcategoriesArray.push(this.createSubcategoryControl(newSubcategory));
    }
  }

  selectCategory(category: ProductCategory, index: number): void {
    this.selectedCategory = category;
    this.selectedCategoryIndex = index;
    this.addSubcategoriesToForm(category.subcategories);
  }

  getCategoryControl(index: number): AbstractControl {
    return this.categoryForm.get(['categories', index]);
  }

  getSubcategoryControl(index: number): AbstractControl {
    return this.categoryForm.get(['subcategories', index]);
  }

  private getSubcategory(categoryIndex: number, subcategoryIndex: number): ProductCategory {
    return this.categories[categoryIndex].subcategories[subcategoryIndex];
  }

  onCardClick($event: MouseEvent): void {
    this.disableEditedCategoryControl();
    this.disableEditedSubcategoryControl();
    this.addNewCategoryInputVisible = false;
    this.addNewSubcategoryInputVisible = false;
    this.newCategoryControl.patchValue('');
    this.newSubcategoryControl.patchValue('');
  }

  private clearFormArray(formArray: FormArray): void {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  private listenWhenSliderClosedByCross(): void {
    this.sliderService.closeClicked.pipe(
      takeUntil(this.componentDestroyed),
      filter(key => key === 'ProductList.AddCategory'),
    ).subscribe(() => {
      this.closeIfPossible();
    });
  }

  private closeIfPossible(): void {
    if (this.areCategoriesChanged()) {
      this.showCloseWarningDialog();
    } else {
      this.close();
    }
  }

  private areCategoriesChanged(): boolean {
    return !isEqual(this.originalCategories, this.categories);
  }

  private showCloseWarningDialog(): void {
    warningDialogData.onConfirmClick = () => this.submit();
    warningDialogData.onCancelClick = () => this.close();
    this.warningDialogService.open(warningDialogData);
  }

  private selectFirstCategory(): void {
    if (this.categories && this.categories.length > 0) {
      this.selectCategory(this.categories[0], 0);
    }
  }

  tryDeleteSubcategory(subcategory: ProductSubcategory, i: number): void {
    if (subcategory.assignedToAnyProduct) {
      deleteSubcategoryDialogData.onConfirmClick = () => this.deleteSubcategory(subcategory, i);
      this.openDeleteDialog();
    } else {
      this.deleteSubcategory(subcategory, i);
    }
  }

  private openDeleteDialog(): void {
    this.deleteDialogService.open(deleteCategoryDialogData);
  }

  tryDeleteCategory(category: ProductCategory, i: number): void {
    if (category.assignedToAnyProduct) {
      deleteCategoryDialogData.onConfirmClick = () => this.deleteCategory(category, i);
      this.openDeleteDialog();
    } else {
      this.deleteCategory(category, i);
    }
  }
}

