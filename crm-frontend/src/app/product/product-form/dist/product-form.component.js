"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.ProductFormComponent = void 0;
var forms_1 = require("@angular/forms");
var operators_1 = require("rxjs/operators");
var rxjs_1 = require("rxjs");
var notification_service_1 = require("../../shared/notification.service");
var isEqual = require("lodash/isEqual");
var pull = require("lodash/pull");
var permissions_utils_1 = require("../../shared/permissions-utils");
var core_1 = require("@angular/core");
var search_model_1 = require("../../shared/model/search.model");
var ProductFormComponent = /** @class */ (function () {
    function ProductFormComponent(sliderService, tempFileService, notificationService, categoryService, assignToGroupService, authService, fb, configService, $router) {
        this.sliderService = sliderService;
        this.tempFileService = tempFileService;
        this.notificationService = notificationService;
        this.categoryService = categoryService;
        this.assignToGroupService = assignToGroupService;
        this.authService = authService;
        this.fb = fb;
        this.configService = configService;
        this.$router = $router;
        this.currencies = [];
        this.productCategoriesWithSubcategories = [];
        this.componentDestroyed = new rxjs_1.Subject();
        // static?
        this.undefinedCategoryId = -1000;
        this.manufacturerSearchLoading = false;
        this.supplierSearchLoading = false;
        this.filteredManufacturers = [];
        this.filteredSuppliers = [];
        this.createProductForm();
    }
    Object.defineProperty(ProductFormComponent.prototype, "defaultCurrency", {
        set: function (currency) {
            this.currency.patchValue(currency);
            this._defaultCurrency = currency;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "product", {
        set: function (product) {
            if (product) {
                this.editedProduct = product;
                this.productForm.reset(product);
                this.description.patchValue(product.description);
                this.specification.patchValue(product.specification);
                this.notes.patchValue(product.notes);
                this.purchasePrice.patchValue(this.getNumberInFormat(product.purchasePrice));
                this.sellPriceNet.patchValue(this.getNumberInFormat(product.sellPriceNet));
                this.setFiles();
                this.getProductCategories();
                this.getSubcategories();
                if (this.productPriceComponent) {
                    this.productPriceComponent.setProductPriceList();
                }
                this.setManufacturerAndSupplier();
            }
        },
        enumerable: false,
        configurable: true
    });
    ProductFormComponent.prototype.ngAfterViewInit = function () {
        console.log(this);
        this.setFiles();
        if (this.productPriceComponent) {
            this.productPriceComponent.setProductPriceList();
        }
    };
    ProductFormComponent.prototype.setFiles = function () {
        var photos = this.editedProduct && this.editedProduct.photos
            ?
                this.editedProduct.photos
            :
                null;
        console.log('old photos', photos);
        this.photoUploader.setPhotos(photos);
    };
    Object.defineProperty(ProductFormComponent.prototype, "authKey", {
        get: function () {
            return (this.editedProduct) ? 'ProductEditComponent.' : 'ProductAddComponent.';
        },
        enumerable: false,
        configurable: true
    });
    ProductFormComponent.prototype.ngOnInit = function () {
        this.subscribeForCurrencies();
        this.subscribeForSliderClose();
        this.subscribeForSliderOpen();
        this.setExpirationTimeSubscription();
    };
    ProductFormComponent.prototype.ngOnChanges = function (changes) {
        this.applyPermissionsToForm();
    };
    ProductFormComponent.prototype.ngOnDestroy = function () {
        this.componentDestroyed.next();
        this.componentDestroyed.complete();
    };
    Object.defineProperty(ProductFormComponent.prototype, "name", {
        // @formatter:off
        get: function () { return this.productForm.get('name'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "code", {
        get: function () { return this.productForm.get('code'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "skuCode", {
        get: function () { return this.productForm.get('skuCode'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "categoryIds", {
        get: function () { return this.productForm.get('categoryIds'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "subcategoryIds", {
        get: function () { return this.productForm.get('subcategoryIds'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "manufacturerSearch", {
        get: function () { return this.productForm.get('manufacturerSearch'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "manufacturerId", {
        get: function () { return this.productForm.get('manufacturerId'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "supplierSearch", {
        get: function () { return this.productForm.get('supplierSearch'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "supplierId", {
        get: function () { return this.productForm.get('supplierId'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "brand", {
        get: function () { return this.productForm.get('brand'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "purchasePrice", {
        get: function () { return this.productForm.get('purchasePrice'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "currency", {
        get: function () { return this.productForm.get('currency'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "sellPriceNet", {
        get: function () { return this.productForm.get('sellPriceNet'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "vat", {
        get: function () { return this.productForm.get('vat'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ProductFormComponent.prototype, "active", {
        get: function () { return this.productForm.get('active'); },
        enumerable: false,
        configurable: true
    });
    // @formatter:on
    ProductFormComponent.prototype.clear = function () {
        this.createProductForm();
    };
    ProductFormComponent.prototype.setExpirationTimeSubscription = function () {
        var _this = this;
        this.sliderService.sliderStateChanged
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (stateChanged) {
            if (stateChanged.key === 'ProductList.AddSlider' || stateChanged.key === 'ProductList.EditTitle') {
                if (stateChanged.opened) {
                    _this.expirationTimeSubscription = rxjs_1.interval(5 * 60 * 1000)
                        .subscribe(function () {
                        _this.tempFileService.setNewExpirationTimeForTemporaryFiles().pipe(operators_1.takeUntil(_this.componentDestroyed)).subscribe();
                    });
                }
                else {
                    if (_this.expirationTimeSubscription) {
                        _this.expirationTimeSubscription.unsubscribe();
                    }
                }
            }
        });
    };
    ProductFormComponent.prototype.isFormValid = function () {
        return this.productForm.valid && this.photoUploader.allFilesUploaded();
    };
    ProductFormComponent.prototype.manufacturerSelect = function (manufacturer) {
        if (manufacturer && manufacturer.type === search_model_1.CrmObjectType.manufacturer) {
            this.manufacturerId.patchValue(manufacturer.id);
            this.manufacturerSearch.patchValue(manufacturer.label);
        }
    };
    ProductFormComponent.prototype.supplierSelect = function (supplier) {
        if (supplier && supplier.type === search_model_1.CrmObjectType.supplier) {
            this.supplierId.patchValue(supplier.id);
            this.supplierSearch.patchValue(supplier.label);
        }
    };
    ProductFormComponent.prototype.markAsTouched = function () {
        Object.values(this.productForm.controls).forEach(function (control) {
            control.markAsTouched();
        });
    };
    ProductFormComponent.prototype.getProduct = function () {
        var product = this.productForm.getRawValue();
        product.description = this.description.value;
        product.specification = this.specification.value;
        product.notes = this.notes.value;
        product.subcategoryIds = this.getSelectedSubcategories();
        product.categories = '';
        product.subcategories = '';
        product.photos = this.photoUploader.getPhotos();
        product.productPrices = this.productPriceComponent.getProductPriceList();
        return product;
    };
    ProductFormComponent.prototype.createProductForm = function () {
        this.productForm = this.fb.group({
            name: [null, forms_1.Validators.required],
            code: [null, forms_1.Validators.required],
            skuCode: [null, forms_1.Validators.required],
            categoryIds: [[this.undefinedCategoryId]],
            subcategoryIds: [[this.undefinedCategoryId]],
            manufacturerSearch: [null],
            manufacturerId: [null],
            supplierSearch: [null],
            supplierId: [null],
            brand: [null],
            purchasePrice: [null, forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.min(0)])],
            purchasePriceCurrency: [null, forms_1.Validators.required],
            currency: [this._defaultCurrency, forms_1.Validators.required],
            sellPriceNet: [null, forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.min(0)])],
            vat: [null, forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.min(0)])],
            active: [false]
        });
        this.description = new forms_1.FormControl('');
        this.specification = new forms_1.FormControl('');
        this.notes = new forms_1.FormControl('');
        this.listenForCategoryChanges();
        this.listenForSubcategoryChanges();
        this.applyPermissionsToForm();
        this.subscribeForManufacturerSearch();
        this.subscribeForSupplierSearch();
    };
    ProductFormComponent.prototype.subscribeForSliderClose = function () {
        var _this = this;
        this.sliderService.sliderStateChanged
            .pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.filter(function (change) { return change.key === 'ProductList.AddSlider'; }), operators_1.map(function (change) { return change.opened; }), operators_1.distinctUntilChanged(), operators_1.filter(function (opened) { return !opened; })).subscribe(function () { return _this.clear(); });
    };
    ProductFormComponent.prototype.subscribeForSliderOpen = function () {
        var _this = this;
        this.sliderService.sliderStateChanged
            .pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.filter(function (change) { return change.key === 'ProductList.AddSlider'; }), operators_1.map(function (change) { return change.opened; }), operators_1.distinctUntilChanged(), operators_1.filter(function (opened) { return opened; })).subscribe(function () { return _this.getProductCategories(); });
    };
    ProductFormComponent.prototype.getProductCategories = function () {
        var _this = this;
        this.categoryService.getCategories().pipe(operators_1.takeUntil(this.componentDestroyed)).subscribe(function (categories) {
            _this.allProductCategories = categories;
        }, function (err) { return _this.notificationService.notify('products.form.notifications.getCategoriesError', notification_service_1.NotificationType.ERROR); });
    };
    ProductFormComponent.prototype.getSubcategories = function () {
        var _this = this;
        var selectedCategories = this.categoryIds.value;
        this.categoryService.getCategoriesWithSubcategories(selectedCategories)
            .pipe(operators_1.takeUntil(this.componentDestroyed)).subscribe(function (categories) { return _this.productCategoriesWithSubcategories = categories; }, function (err) { return _this.notificationService.notify('products.form.notifications.getSubcategoriesError', notification_service_1.NotificationType.ERROR); });
    };
    ProductFormComponent.prototype.addSubcategory = function (subcategorySelect) {
        subcategorySelect.close();
        this.addSubcategorySlider.open();
    };
    ProductFormComponent.prototype.addCategory = function (matSelect) {
        matSelect.close();
        this.addCategorySlider.open();
    };
    ProductFormComponent.prototype.listenForCategoryChanges = function () {
        var _this = this;
        this.categoryIds.valueChanges.pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.filter(function (value) { return !!value; }), operators_1.distinctUntilChanged(function (x, y) { return isEqual(x, y); })).subscribe(function (categories) {
            if (categories[0] === _this.undefinedCategoryId && categories.length > 1) {
                _this.categoryIds.patchValue(categories.slice(1));
                _this.subcategoryIds.enable();
            }
            if (categories.length >= 1 && categories[0] !== _this.undefinedCategoryId) {
                _this.subcategoryIds.enable();
            }
            if (categories.length === 0) {
                _this.categoryIds.patchValue([_this.undefinedCategoryId]);
                _this.subcategoryIds.disable();
                _this.subcategoryIds.patchValue([_this.undefinedCategoryId]);
            }
            var subcategories = _this.categoryService.flattenSubcategories(_this.productCategoriesWithSubcategories);
            var allowed = subcategories.filter(function (sub) { return _this.subcategoryIds.value.includes(sub.id); })
                .filter(function (sub) { return _this.categoryIds.value.includes(sub.categoryId); }).map(function (sub) { return sub.id; });
            _this.subcategoryIds.patchValue(allowed);
        });
    };
    ProductFormComponent.prototype.onNoCategorySelect = function (option) {
        if (option.selected) {
            this.categorySelect.options.filter(function (item) { return item !== option; }).forEach(function (item) { return item.deselect(); });
            this.subcategoryIds.disable();
        }
    };
    ProductFormComponent.prototype.onNoSubcategorySelect = function (option) {
        if (option.selected) {
            this.subcategorySelect.options.filter(function (item) { return item !== option; }).forEach(function (item) { return item.deselect(); });
        }
    };
    ProductFormComponent.prototype.getSelectedCategoryIds = function () {
        return this.categoryIds.value;
    };
    ProductFormComponent.prototype.onCancelAddCategorySlider = function () {
        this.addCategorySlider.close();
    };
    ProductFormComponent.prototype.onCancelAddSubcategorySlider = function () {
        this.addSubcategorySlider.close();
    };
    ProductFormComponent.prototype.onSubmitAddCategorySlider = function (selectedCategoriesIds) {
        this.getProductCategories();
        this.categoryIds.patchValue(selectedCategoriesIds);
        this.addCategorySlider.close();
        this.getSubcategories();
    };
    ProductFormComponent.prototype.onSubmitAddSubcategorySlider = function (selectedSubcategoriesIds) {
        this.getSubcategories();
        this.subcategoryIds.patchValue(selectedSubcategoriesIds);
        this.addSubcategorySlider.close();
    };
    ProductFormComponent.prototype.listenForSubcategoryChanges = function () {
        var _this = this;
        this.subcategoryIds.valueChanges.pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.filter(function (value) { return !!value; }), operators_1.distinctUntilChanged(function (x, y) { return isEqual(x, y); })).subscribe(function (categories) {
            if (categories[0] === _this.undefinedCategoryId && categories.length > 1) {
                _this.subcategoryIds.patchValue(categories.slice(1));
            }
            if (categories.length === 0) {
                _this.subcategoryIds.patchValue([_this.undefinedCategoryId]);
            }
        });
    };
    ProductFormComponent.prototype.getSelectedSubcategoriesIds = function () {
        return this.subcategoryIds.value;
    };
    ProductFormComponent.prototype.applyPermissionsToForm = function () {
        if (this.productForm) {
            this.productForm.enable();
            this.currency.disable();
            permissions_utils_1.applyPermission(this.name, !this.authService.isPermissionPresent(this.authKey + "name", 'r'));
            permissions_utils_1.applyPermission(this.code, !this.authService.isPermissionPresent(this.authKey + "code", 'r'));
            permissions_utils_1.applyPermission(this.skuCode, !this.authService.isPermissionPresent(this.authKey + "skuCode", 'r'));
            permissions_utils_1.applyPermission(this.manufacturerSearch, !this.authService.isPermissionPresent(this.authKey + "manufacturer", 'r'));
            permissions_utils_1.applyPermission(this.supplierSearch, !this.authService.isPermissionPresent(this.authKey + "supplier", 'r'));
            permissions_utils_1.applyPermission(this.brand, !this.authService.isPermissionPresent(this.authKey + "brand", 'r'));
            permissions_utils_1.applyPermission(this.purchasePrice, !(this.authService.isPermissionPresent(this.authKey + "purchasePrice", 'r')));
            permissions_utils_1.applyPermission(this.sellPriceNet, !this.authService.isPermissionPresent(this.authKey + "sellPriceNet", 'r'));
            permissions_utils_1.applyPermission(this.vat, !this.authService.isPermissionPresent(this.authKey + "bat", 'r'));
            permissions_utils_1.applyPermission(this.active, !this.authService.isPermissionPresent(this.authKey + "active", 'r'));
            permissions_utils_1.applyPermission(this.categoryIds, !this.authService.isPermissionPresent(this.authKey + "category", 'r'));
            permissions_utils_1.applyPermission(this.subcategoryIds, !this.authService.isPermissionPresent(this.authKey + "subcategory", 'r'));
            permissions_utils_1.applyPermission(this.description, !this.authService.isPermissionPresent(this.authKey + "description", 'r'));
            permissions_utils_1.applyPermission(this.specification, !this.authService.isPermissionPresent(this.authKey + "specification", 'r'));
            permissions_utils_1.applyPermission(this.notes, !this.authService.isPermissionPresent(this.authKey + "notes", 'r'));
        }
    };
    ProductFormComponent.prototype.getSelectedSubcategories = function () {
        var selectedCategoryIds = this.categoryIds.value;
        var selectedSubcategoryIds = this.subcategoryIds.value;
        var initialCategoryIds = this.allProductCategories.filter(function (c) { return selectedCategoryIds.includes(c.id); }).map(function (c) { return c.initialSubcategory; });
        return pull(Array.from(new Set(__spreadArrays(initialCategoryIds, selectedSubcategoryIds)).values())
            .filter(function (v) { return !!v; }), this.undefinedCategoryId);
    };
    ProductFormComponent.prototype.subscribeForManufacturerSearch = function () {
        var _this = this;
        this.manufacturerSearch.valueChanges.pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.debounceTime(250), operators_1.tap(function () { return _this.manufacturerSearchLoading = true; }), operators_1.switchMap((function (input) {
            if (!input || input === '') {
                _this.manufacturerId.patchValue(null);
            }
            return _this.assignToGroupService.searchForType(search_model_1.CrmObjectType.manufacturer, input !== null ? input : '', [search_model_1.CrmObjectType.manufacturer])
                .pipe(operators_1.finalize(function () { return _this.manufacturerSearchLoading = false; }));
        }))).subscribe(function (result) {
            _this.filteredManufacturers = result;
        });
    };
    ProductFormComponent.prototype.subscribeForSupplierSearch = function () {
        var _this = this;
        this.supplierSearch.valueChanges.pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.debounceTime(250), operators_1.tap(function () { return _this.supplierSearchLoading = true; }), operators_1.switchMap((function (input) {
            if (!input || input === '') {
                _this.supplierId.patchValue(null);
            }
            return _this.assignToGroupService.searchForType(search_model_1.CrmObjectType.supplier, input !== null ? input : '', [search_model_1.CrmObjectType.supplier])
                .pipe(operators_1.finalize(function () { return _this.supplierSearchLoading = false; }));
        }))).subscribe(function (result) {
            _this.filteredSuppliers = result;
        });
    };
    ProductFormComponent.prototype.setManufacturerAndSupplier = function () {
        var _this = this;
        if (this.editedProduct && this.editedProduct.manufacturerId) {
            this.setLabelById(this.manufacturerSearch, search_model_1.CrmObjectType.manufacturer, this.manufacturerId.value)
                .subscribe(function () { return _this.manufacturerId.patchValue(_this.editedProduct.manufacturerId); });
        }
        if (this.editedProduct && this.editedProduct.supplierId) {
            this.setLabelById(this.supplierSearch, search_model_1.CrmObjectType.supplier, this.supplierId.value)
                .subscribe(function () { return _this.supplierId.patchValue(_this.editedProduct.supplierId); });
        }
    };
    ProductFormComponent.prototype.setLabelById = function (controller, type, id) {
        return this.assignToGroupService.updateLabels([{ type: type, id: id }])
            .pipe(operators_1.takeUntil(this.componentDestroyed), operators_1.tap(function (response) {
            if (response && response.length > 0) {
                controller.patchValue(response[0].label);
            }
        }));
    };
    ProductFormComponent.prototype.getNumberInFormat = function (value) {
        return String(value).replace('.', ',');
    };
    ProductFormComponent.prototype.subscribeForCurrencies = function () {
        var _this = this;
        this.configService.getGlobalParamValue('currencies').pipe(operators_1.takeUntil(this.componentDestroyed)).subscribe(function (param) { return _this.currencies = param.value.split(';'); }, function () { return _this.notificationService.notify('administration.notifications.getError', notification_service_1.NotificationType.ERROR); });
    };
    ProductFormComponent.prototype.openFullDocumentView = function (productId) {
        this.notificationService.notify('administration.notifications.notImplementedFunctionality', notification_service_1.NotificationType.ERROR);
        // this.$router.navigate(['/product', productId, 'documents']);
    };
    __decorate([
        core_1.Input()
    ], ProductFormComponent.prototype, "defaultCurrency");
    __decorate([
        core_1.Input()
    ], ProductFormComponent.prototype, "product");
    __decorate([
        core_1.ViewChild('photoUploader')
    ], ProductFormComponent.prototype, "photoUploader");
    __decorate([
        core_1.ViewChild('addProductCategorySlider')
    ], ProductFormComponent.prototype, "addCategorySlider");
    __decorate([
        core_1.ViewChild('addProductSubcategorySlider')
    ], ProductFormComponent.prototype, "addSubcategorySlider");
    __decorate([
        core_1.ViewChild('categorySelect')
    ], ProductFormComponent.prototype, "categorySelect");
    __decorate([
        core_1.ViewChild('subcategorySelect')
    ], ProductFormComponent.prototype, "subcategorySelect");
    __decorate([
        core_1.ViewChild('productPrice')
    ], ProductFormComponent.prototype, "productPriceComponent");
    ProductFormComponent = __decorate([
        core_1.Component({
            selector: 'app-product-form',
            templateUrl: './product-form.component.html',
            styleUrls: ['./product-form.component.scss']
        })
    ], ProductFormComponent);
    return ProductFormComponent;
}());
exports.ProductFormComponent = ProductFormComponent;
