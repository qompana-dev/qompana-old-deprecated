import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {SliderComponent} from '../../../../shared/slider/slider.component';
import {ProductPrice} from '../../../product.model';
import {PriceBook} from '../../../../price-book/price-book.model';
import {Subject} from 'rxjs';
import {PriceBookService} from '../../../../price-book/price-book.service';
import {takeUntil} from 'rxjs/operators';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSelectChange} from '@angular/material/select';
import {TranslateService} from '@ngx-translate/core';
import {FormUtil} from '../../../../shared/form.util';
import {SliderService} from '../../../../shared/slider/slider.service';
import {GetCurrencySymbolPipe} from '../../../../shared/pipe/get-currency-symbol.pipe';


@Component({
  selector: 'app-add-product-to-price-book-form',
  templateUrl: './add-product-to-price-book-form.component.html',
  styleUrls: ['./add-product-to-price-book-form.component.scss']
})
export class AddProductToPriceBookFormComponent implements OnInit, OnDestroy {

  @Input() slider: SliderComponent;
  @Input() set existingProductPrices(existingProductPrices: ProductPrice[]) {
    if (this.allPriceBooks && existingProductPrices) {
      const existingPriceBooksIds = existingProductPrices.map(e => e.priceBook.id);
      this.priceBooks = this.allPriceBooks.filter(e => existingPriceBooksIds.indexOf(e.id) === -1);
    }
  }

  @Input() set productPrice(productPrice: ProductPrice) {
    if (productPrice) {
      this.editedProductPrice = productPrice;
      this.priceBookId.patchValue(productPrice.priceBook.id);
      this.price.patchValue(String(productPrice.price).replace('.', ','));
      this.active.patchValue(productPrice.active);

      const value = this.allPriceBooks.find(e => e.id === productPrice.priceBook.id);
      this.currency.patchValue(value && value.currency ? this.getCurrencySymbol.transform(value.currency) :
        this.translateService.instant('productPrice.addProductToPrice.noCurrency'));
    }
  }

  @Output() addProductPrice: EventEmitter<ProductPrice> = new EventEmitter<ProductPrice>();

  editedProductPrice: ProductPrice;
  productPriceForm: FormGroup;
  priceBooks: PriceBook[];
  allPriceBooks: PriceBook[];
  private componentDestroyed: Subject<void> = new Subject();

  constructor(private priceBookService: PriceBookService,
              private translateService: TranslateService,
              private sliderService: SliderService,
              private getCurrencySymbol: GetCurrencySymbolPipe,
              private fb: FormBuilder) {
  }

  // @formatter:off
  get priceBookId(): AbstractControl { return this.productPriceForm.get('priceBookId'); }
  get price(): AbstractControl { return this.productPriceForm.get('price'); }
  get currency(): AbstractControl { return this.productPriceForm.get('currency'); }
  get active(): AbstractControl { return this.productPriceForm.get('active'); }
  // @formatter:on

  ngOnInit(): void {
    this.subscribeForPriceBooksToAssign();
    this.createProductPriceForm();
    this.sliderService.closeClicked.pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (key: string) => {
        if (key === this.slider.key) {
          this.closeSlider();
        }
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    if (!this.productPriceForm.valid) {
      FormUtil.setTouched(this.productPriceForm);
    } else {
      this.emitNewProductPriceAdded();
      this.createProductPriceForm();
      this.slider.close();
    }
  }

  submitAndAddNext(): void {
    if (!this.productPriceForm.valid) {
      FormUtil.setTouched(this.productPriceForm);
    } else {
      this.emitNewProductPriceAdded();
      this.createProductPriceForm();
    }
  }

  closeSlider(): void {
    if (this.editedProductPrice) {
      this.addProductPrice.emit(this.editedProductPrice);
    }
    this.slider.close();
  }

  setCurrency($event: MatSelectChange): void {
    const value = this.allPriceBooks.find(e => e.id === $event.value);
    this.currency.patchValue(value && value.currency ? this.getCurrencySymbol.transform(value.currency) :
      this.translateService.instant('productPrice.addProductToPrice.noCurrency'));
  }

  private createProductPriceForm(): void {
    this.productPriceForm = this.fb.group({
      priceBookId: [null, Validators.required],
      price: [null, Validators.compose([Validators.required, Validators.min(0)])],
      currency: [{value: '', disabled: true}],
      active: [true]
    });
  }

  private subscribeForPriceBooksToAssign(): void {
    this.priceBookService.getPriceBookToAssign().pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (priceBooks) => {
        this.allPriceBooks = priceBooks.filter(e => !e.isDefault);
        this.priceBooks = priceBooks.filter(e => !e.isDefault);
      }
    );
  }

  private emitNewProductPriceAdded(): void {
    this.addProductPrice.emit({
      priceBook: this.allPriceBooks.find(e => e.id === this.priceBookId.value),
      price: this.price.value,
      active: this.active.value
    });
  }
}
