import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Product, ProductPrice} from '../../product.model';
import {Sort} from '@angular/material';
import cloneDeep from 'lodash/cloneDeep';
import {SliderComponent} from '../../../shared/slider/slider.component';

@Component({
  selector: 'app-product-price',
  templateUrl: './product-price.component.html',
  styleUrls: ['./product-price.component.scss']
})
export class ProductPriceComponent {
  @ViewChild('editSlider') editSlider: SliderComponent;

  @Input() set product(product: Product) {
    if (product) {
      this.editedProduct = product;
      this.setProductPriceList();
    }
  }
  private currentSort = 'id,asc';

  editedProduct: Product;
  productPrices: ProductPrice[] = [];
  productPriceToEdit: ProductPrice;

  constructor() { }

  get authKey(): string {
    return (this.editedProduct) ? 'ProductEditComponent.' : 'ProductAddComponent.';
  }

  public onSortChange($event: Sort): void {
    this.currentSort = $event.direction ? ($event.active + ',' + $event.direction) : 'id,asc';
  }

  public getProductPriceList(): ProductPrice[] {
    return this.productPrices;
  }

  public setProductPriceList(): void {
    if (this.editedProduct && this.editedProduct.productPrices) {
      this.productPrices = this.editedProduct.productPrices;
    } else {
      this.productPrices = [];
    }
    this.changeProductPriceReference();
  }

  public addProductPrice(productPrice: ProductPrice): void {
    this.productPriceToEdit = undefined;
    this.productPrices.push(productPrice);
    this.changeProductPriceReference();
  }

  public editPrice(productPrice: ProductPrice): void {
    this.productPriceToEdit = cloneDeep(productPrice);
    this.removeFromPriceBook(productPrice);
    this.editSlider.open();
  }

  public removeFromPriceBook(productPrice: ProductPrice): void {
    const indexOfEditedPrice = this.productPrices.indexOf(productPrice);
    if (indexOfEditedPrice !== -1) {
      this.productPrices.splice(indexOfEditedPrice, 1);
      this.changeProductPriceReference();
    }
  }

  private changeProductPriceReference(): void {
    const productPricesCopy = cloneDeep(this.productPrices);
    this.productPrices = undefined;
    this.productPrices = productPricesCopy;
  }
}
