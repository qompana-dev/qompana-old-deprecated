import { ProductService } from "./../../product.service";
import { PageRequest } from "./../../../shared/pagination/page.model";
import { takeUntil } from "rxjs/operators";
import {
  Component,
  Input,
  SimpleChanges,
  OnChanges,
  OnDestroy,
} from "@angular/core";
import { SellHistory, SellHistoryResult } from "../../product.model";
import { Subject } from "rxjs";

@Component({
  selector: "app-sell-history",
  templateUrl: "./sell-history.component.html",
  styleUrls: ["./sell-history.component.scss"],
})
export class SellHistoryComponent implements OnChanges, OnDestroy {
  @Input() productId: number;
  private componentDestroyed: Subject<void> = new Subject();
  sellHistories: SellHistory[] = [];

  constructor(private productService: ProductService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.productId.currentValue) {
      this.subscribeForSellHistory(this.productId, SellHistoryResult.SUCCESS);
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  private subscribeForSellHistory(
    productId: number,
    finishResult: SellHistoryResult
  ): void {
    this.productService
      .getProductHistory(productId, finishResult)
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((param) => (this.sellHistories = param));
  }
}
