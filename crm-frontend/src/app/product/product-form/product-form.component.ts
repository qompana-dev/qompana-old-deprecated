import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Product, ProductCategory } from '../product.model';
import { debounceTime, distinctUntilChanged, filter, finalize, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { SliderService } from '../../shared/slider/slider.service';
import { interval, Observable, Subject, Subscription } from 'rxjs';
import { NotificationService, NotificationType } from '../../shared/notification.service';
import { TempFileService } from '../../shared/temp-file/temp-file.service';
import { MatOption, MatSelect } from '@angular/material';
import { ProductCategoryService } from '../product-category.service';
import { SliderComponent } from '../../shared/slider/slider.component';
import * as isEqual from 'lodash/isEqual';
import * as pull from 'lodash/pull';
import {applyPermission} from '../../shared/permissions-utils';
import {AuthService} from '../../login/auth/auth.service';
import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild, AfterViewInit} from '@angular/core';
import {ProductPriceComponent} from './product-price/product-price.component';
import {CrmObject, CrmObjectType} from '../../shared/model/search.model';
import {AssignToGroupService} from '../../shared/assign-to-group/assign-to-group.service';
import {GlobalConfigService} from '../../global-config/global-config.service';
import {FormValidators} from '../../shared/form/form-validators';
import { PhotoUploaderComponent } from './photo-uploader/photo-uploader.component';
import { DictionaryId, DictionaryWord} from '../../dictionary/dictionary.model';
import { DictionaryStoreService } from 'src/app/dictionary/dictionary-store.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit {

  @Input() set defaultCurrency(currency: string) {
    this.currency.patchValue(currency);
    this._defaultCurrency = currency;
  }

  @Input() set product(product: Product) {
    if (product) {
      this.editedProduct = product;
      this.productForm.reset(product);
      this.description.patchValue(product.description);
      this.specification.patchValue(product.specification);
      this.notes.patchValue(product.notes);
      this.purchasePrice.patchValue(this.getNumberInFormat(product.purchasePrice));
      this.sellPriceNet.patchValue(this.getNumberInFormat(product.sellPriceNet));

      this.setFiles();

      this.getProductCategories();
      this.getSubcategories();
      if (this.productPriceComponent) {
        this.productPriceComponent.setProductPriceList();
      }
      this.setManufacturerAndSupplier();
    }
  }

  // tslint:disable-next-line:variable-name
  _defaultCurrency: string;
  editedProduct: Product;
  productForm: FormGroup;
  currencies = [];
  units : DictionaryWord[] = [];
  description: FormControl;
  specification: FormControl;
  allProductCategories: ProductCategory[];
  productCategoriesWithSubcategories: ProductCategory[] = [];
  notes: FormControl;

  public componentDestroyed: Subject<void> = new Subject();
  @ViewChild('photoUploader') photoUploader: PhotoUploaderComponent;
  @ViewChild('addProductCategorySlider') addCategorySlider: SliderComponent;
  @ViewChild('addProductSubcategorySlider') addSubcategorySlider: SliderComponent;
  // static?
  readonly undefinedCategoryId = -1000;
  @ViewChild('categorySelect') categorySelect: MatSelect;
  @ViewChild('subcategorySelect') subcategorySelect: MatSelect;
  @ViewChild('productPrice') productPriceComponent: ProductPriceComponent;
  manufacturerSearchLoading = false;
  supplierSearchLoading = false;
  filteredManufacturers: CrmObject[] = [];
  filteredSuppliers: CrmObject[] = [];



  expirationTimeSubscription: Subscription;

  constructor(
    private sliderService: SliderService,
    private tempFileService: TempFileService,
    private notificationService: NotificationService,
    public categoryService: ProductCategoryService,
    private assignToGroupService: AssignToGroupService,
    private authService: AuthService,
    private fb: FormBuilder,
    private configService: GlobalConfigService,
    private dictionaryStoreService: DictionaryStoreService,
  ) {
    this.createProductForm();
  }

  ngAfterViewInit(): void {
    this.setFiles();
    if (this.productPriceComponent) {
      this.productPriceComponent.setProductPriceList();
    }
  }

  setFiles(): void {
    const photos =
      this.editedProduct && this.editedProduct.photos
        ?
        this.editedProduct.photos
        :
        null;
    this.photoUploader.setPhotos(photos);
  }

  get authKey(): string {
    return (this.editedProduct) ? 'ProductEditComponent.' : 'ProductAddComponent.';
  }

  ngOnInit(): void {
    this.subscribeForCurrencies();
    this.subscribeForUnits();
    this.subscribeForSliderClose();
    this.subscribeForSliderOpen();
    this.setExpirationTimeSubscription();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.applyPermissionsToForm();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  // @formatter:off
  get name(): AbstractControl { return this.productForm.get('name'); }
  get code(): AbstractControl { return this.productForm.get('code'); }
  get skuCode(): AbstractControl { return this.productForm.get('skuCode'); }
  get categoryIds(): AbstractControl { return this.productForm.get('categoryIds'); }
  get subcategoryIds(): AbstractControl { return this.productForm.get('subcategoryIds'); }
  get manufacturerSearch(): AbstractControl { return this.productForm.get('manufacturerSearch'); }
  get manufacturerId(): AbstractControl { return this.productForm.get('manufacturerId'); }
  get supplierSearch(): AbstractControl { return this.productForm.get('supplierSearch'); }
  get supplierId(): AbstractControl { return this.productForm.get('supplierId'); }
  get brand(): AbstractControl { return this.productForm.get('brand'); }
  get purchasePrice(): AbstractControl { return this.productForm.get('purchasePrice'); }
  get currency(): AbstractControl { return this.productForm.get('currency'); }
  get sellPriceNet(): AbstractControl { return this.productForm.get('sellPriceNet'); }
  get vat(): AbstractControl { return this.productForm.get('vat'); }
  get active(): AbstractControl { return this.productForm.get('active'); }
  get unit(): AbstractControl { return this.productForm.get('unit'); }
  // @formatter:on

  public clear(): void {
    this.createProductForm();
  }

  private setExpirationTimeSubscription(): void {
    this.sliderService.sliderStateChanged
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe(stateChanged => {
        if (stateChanged.key === 'ProductList.AddSlider' || stateChanged.key === 'ProductList.EditTitle') {
          if (stateChanged.opened) {
            this.expirationTimeSubscription = interval(5 * 60 * 1000)
              .subscribe(() => {
                this.tempFileService.setNewExpirationTimeForTemporaryFiles().pipe(takeUntil(this.componentDestroyed)).subscribe();
              });
          } else {
            if (this.expirationTimeSubscription) {
              this.expirationTimeSubscription.unsubscribe();
            }
          }
        }
      });
  }

  public isFormValid(): boolean {
    return this.productForm.valid && this.photoUploader.allFilesUploaded();
  }

  manufacturerSelect(manufacturer: CrmObject): void {
    if (manufacturer && manufacturer.type === CrmObjectType.manufacturer) {
      this.manufacturerId.patchValue(manufacturer.id);
      this.manufacturerSearch.patchValue(manufacturer.label);
    }
  }

  supplierSelect(supplier: CrmObject): void {
    if (supplier && supplier.type === CrmObjectType.supplier) {
      this.supplierId.patchValue(supplier.id);
      this.supplierSearch.patchValue(supplier.label);
    }
  }

  public markAsTouched(): void {
    (Object as any).values(this.productForm.controls).forEach(control => {
      control.markAsTouched();
    });
  }

  public getProduct(): Product {
    const product: Product = this.productForm.getRawValue();
    product.description = this.description.value;
    product.specification = this.specification.value;
    product.notes = this.notes.value;
    product.subcategoryIds = this.getSelectedSubcategories();
    product.categories = '';
    product.subcategories = '';
    product.photos = this.photoUploader.getPhotos();
    product.productPrices = this.productPriceComponent.getProductPriceList();
    product.purchasePriceCurrency = product.currency;
    return product;
  }

  private createProductForm(): void {
    this.productForm = this.fb.group({
      name: [null, [Validators.required, FormValidators.whitespace, Validators.maxLength(200)]],
      code: [null, [Validators.required, FormValidators.whitespace, Validators.maxLength(100)]],
      skuCode: [null, [Validators.required, FormValidators.whitespace, Validators.maxLength(100)]],
      categoryIds: [[this.undefinedCategoryId]],
      subcategoryIds: [[this.undefinedCategoryId]],
      manufacturerSearch: [null],
      manufacturerId: [null],
      supplierSearch: [null],
      supplierId: [null],
      brand: [null, Validators.maxLength(200)],
      purchasePrice: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(999999999999999999)])],
      unit: [null, Validators.required],
      currency: [this._defaultCurrency, Validators.required],
      sellPriceNet: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(999999999999999999)])],
      vat: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(999999999)])],
      active: [false]
    });
    this.description = new FormControl('', Validators.maxLength(1000));
    this.specification = new FormControl('', Validators.maxLength(1000));
    this.notes = new FormControl('', Validators.maxLength(1000));
    this.listenForCategoryChanges();
    this.listenForSubcategoryChanges();
    this.applyPermissionsToForm();
    this.subscribeForManufacturerSearch();
    this.subscribeForSupplierSearch();
  }

  private subscribeForSliderClose(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        takeUntil(this.componentDestroyed),
        filter(change => change.key === 'ProductList.AddSlider'),
        map(change => change.opened),
        distinctUntilChanged(),
        filter(opened => !opened)
      ).subscribe(
        () => this.clear()
      );
  }

  private subscribeForSliderOpen(): void {
    this.sliderService.sliderStateChanged
      .pipe(
        takeUntil(this.componentDestroyed),
        filter(change => change.key === 'ProductList.AddSlider'),
        map(change => change.opened),
        distinctUntilChanged(),
        filter(opened => opened)
      ).subscribe(
        () => this.getProductCategories()
      );
  }

  private getProductCategories(): void {
    this.categoryService.getCategories().pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      categories => {
        this.allProductCategories = categories;
      },
      err => this.notificationService.notify('products.form.notifications.getCategoriesError', NotificationType.ERROR)
    );
  }

  getSubcategories(): void {
    const selectedCategories = this.categoryIds.value;
    this.categoryService.getCategoriesWithSubcategories(selectedCategories)
      .pipe(
        takeUntil(this.componentDestroyed)
      ).subscribe(
        categories => this.productCategoriesWithSubcategories = categories,
        err => this.notificationService.notify('products.form.notifications.getSubcategoriesError', NotificationType.ERROR)
      );
  }

  addSubcategory(subcategorySelect: MatSelect): void {
    subcategorySelect.close();
    this.addSubcategorySlider.open();
  }

  addCategory(matSelect: MatSelect): void {
    matSelect.close();
    this.addCategorySlider.open();
  }


  private listenForCategoryChanges(): void {
    this.categoryIds.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      filter(value => !!value),
      distinctUntilChanged((x, y) => isEqual(x, y)),
    ).subscribe(
      (categories: Array<number>) => {
        if (categories[0] === this.undefinedCategoryId && categories.length > 1) {
          this.categoryIds.patchValue(categories.slice(1));
          this.subcategoryIds.enable();
        }
        if (categories.length >= 1 && categories[0] !== this.undefinedCategoryId) {
          this.subcategoryIds.enable();
        }
        if (categories.length === 0) {
          this.categoryIds.patchValue([this.undefinedCategoryId]);
          this.subcategoryIds.disable();
          this.subcategoryIds.patchValue([this.undefinedCategoryId]);
        }
        const subcategories = this.categoryService.flattenSubcategories(this.productCategoriesWithSubcategories);
        const allowed = subcategories.filter(sub => this.subcategoryIds.value.includes(sub.id))
          .filter(sub => this.categoryIds.value.includes(sub.categoryId)).map(sub => sub.id);
        this.subcategoryIds.patchValue(allowed);

      }
    );
  }

  onNoCategorySelect(option: MatOption): void {
    if (option.selected) {
      this.categorySelect.options.filter(item => item !== option).forEach(item => item.deselect());
      this.subcategoryIds.disable();
    }
  }

  onNoSubcategorySelect(option: MatOption): void {
    if (option.selected) {
      this.subcategorySelect.options.filter(item => item !== option).forEach(item => item.deselect());
    }
  }

  getSelectedCategoryIds(): number[] {
    return this.categoryIds.value;
  }

  onCancelAddCategorySlider(): void {
    this.addCategorySlider.close();
  }

  onCancelAddSubcategorySlider(): void {
    this.addSubcategorySlider.close();
  }

  onSubmitAddCategorySlider(selectedCategoriesIds: number[]): void {
    this.getProductCategories();
    this.categoryIds.patchValue(selectedCategoriesIds);
    this.addCategorySlider.close();
    this.getSubcategories();
  }


  onSubmitAddSubcategorySlider(selectedSubcategoriesIds: number[]): void {
    this.getSubcategories();
    this.subcategoryIds.patchValue(selectedSubcategoriesIds);
    this.addSubcategorySlider.close();
  }

  private listenForSubcategoryChanges(): void {
    this.subcategoryIds.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      filter(value => !!value),
      distinctUntilChanged((x, y) => isEqual(x, y)),
    ).subscribe(
      (categories: Array<any>) => {
        if (categories[0] === this.undefinedCategoryId && categories.length > 1) {
          this.subcategoryIds.patchValue(categories.slice(1));
        }
        if (categories.length === 0) {
          this.subcategoryIds.patchValue([this.undefinedCategoryId]);
        }
      }
    );
  }

  getSelectedSubcategoriesIds(): void {
    return this.subcategoryIds.value;
  }

  private applyPermissionsToForm(): void {
    if (this.productForm) {
      this.productForm.enable();
      applyPermission(this.name, !this.authService.isPermissionPresent(`${this.authKey}name`, 'r'));
      applyPermission(this.code, !this.authService.isPermissionPresent(`${this.authKey}code`, 'r'));
      applyPermission(this.skuCode, !this.authService.isPermissionPresent(`${this.authKey}skuCode`, 'r'));
      applyPermission(this.manufacturerSearch, !this.authService.isPermissionPresent(`${this.authKey}manufacturer`, 'r'));
      applyPermission(this.supplierSearch, !this.authService.isPermissionPresent(`${this.authKey}supplier`, 'r'));
      applyPermission(this.brand, !this.authService.isPermissionPresent(`${this.authKey}brand`, 'r'));
      applyPermission(this.purchasePrice, !(this.authService.isPermissionPresent(`${this.authKey}purchasePrice`, 'r')));
      applyPermission(this.sellPriceNet, !this.authService.isPermissionPresent(`${this.authKey}sellPriceNet`, 'r'));
      applyPermission(this.vat, !this.authService.isPermissionPresent(`${this.authKey}bat`, 'r'));
      applyPermission(this.active, !this.authService.isPermissionPresent(`${this.authKey}active`, 'r'));
      applyPermission(this.categoryIds, !this.authService.isPermissionPresent(`${this.authKey}category`, 'r'));
      applyPermission(this.subcategoryIds, !this.authService.isPermissionPresent(`${this.authKey}subcategory`, 'r'));
      applyPermission(this.description, !this.authService.isPermissionPresent(`${this.authKey}description`, 'r'));
      applyPermission(this.specification, !this.authService.isPermissionPresent(`${this.authKey}specification`, 'r'));
      applyPermission(this.notes, !this.authService.isPermissionPresent(`${this.authKey}notes`, 'r'));
    }
  }

  private getSelectedSubcategories(): number[] {
    const selectedCategoryIds: number[] = this.categoryIds.value;
    const selectedSubcategoryIds: number[] = this.subcategoryIds.value;
    const initialCategoryIds = this.allProductCategories.filter(c => selectedCategoryIds.includes(c.id)).map(c => c.initialSubcategory);
    return pull(Array.from(new Set([...initialCategoryIds, ...selectedSubcategoryIds]).values())
      .filter(v => !!v), this.undefinedCategoryId);
  }

  private subscribeForManufacturerSearch(): void {
    this.manufacturerSearch.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      tap(() => this.manufacturerSearchLoading = true),
      switchMap(((input: string) => {
        if (!input || input === '') {
          this.manufacturerId.patchValue(null);
        }
        return this.assignToGroupService.searchForType
          (CrmObjectType.manufacturer, input !== null ? input : '', [CrmObjectType.manufacturer])
          .pipe(finalize(() => this.manufacturerSearchLoading = false));
      }
      ))).subscribe(
        (result: CrmObject[]) => {
          this.filteredManufacturers = result;
        }
      );
  }

  private subscribeForSupplierSearch(): void {
    this.supplierSearch.valueChanges.pipe(
      takeUntil(this.componentDestroyed),
      debounceTime(250),
      tap(() => this.supplierSearchLoading = true),
      switchMap(((input: string) => {
        if (!input || input === '') {
          this.supplierId.patchValue(null);
        }
        return this.assignToGroupService.searchForType
          (CrmObjectType.supplier, input !== null ? input : '', [CrmObjectType.supplier])
          .pipe(finalize(() => this.supplierSearchLoading = false));
      }
      ))).subscribe(
        (result: CrmObject[]) => {
          this.filteredSuppliers = result;
        }
      );
  }

  private setManufacturerAndSupplier(): void {
    if (this.editedProduct && this.editedProduct.manufacturerId) {
      this.setLabelById(this.manufacturerSearch, CrmObjectType.manufacturer, this.manufacturerId.value)
        .subscribe(() => this.manufacturerId.patchValue(this.editedProduct.manufacturerId));
    }
    if (this.editedProduct && this.editedProduct.supplierId) {
      this.setLabelById(this.supplierSearch, CrmObjectType.supplier, this.supplierId.value)
        .subscribe(() => this.supplierId.patchValue(this.editedProduct.supplierId));
    }
  }

  private setLabelById(controller: AbstractControl, type: CrmObjectType, id: number): Observable<any> {
    return this.assignToGroupService.updateLabels([{ type, id }])
      .pipe(takeUntil(this.componentDestroyed),
        tap((response: CrmObject[]) => {
          if (response && response.length > 0) {
            controller.patchValue(response[0].label);
          }
        }));
  }

  private getNumberInFormat(value: number): string {
    return String(value).replace('.', ',');
  }

  private subscribeForCurrencies(): void {
    this.configService.getGlobalParamValue('currencies').pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (param) => this.currencies = param.value.split(';'),
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }

  private subscribeForUnits(): void {
    this.dictionaryStoreService.getDictionaryWordsById(DictionaryId.UNIT).pipe(
      takeUntil(this.componentDestroyed)
    ).subscribe(
      (words: DictionaryWord[]) => this.units = words,
      () => this.notificationService.notify('administration.notifications.getError', NotificationType.ERROR)
    );
  }

  public openFullDocumentView(productId): void {
    this.notificationService.notify('administration.notifications.notImplementedFunctionality', NotificationType.ERROR);
    // this.$router.navigate(['/product', productId, 'documents']);
  }
}


