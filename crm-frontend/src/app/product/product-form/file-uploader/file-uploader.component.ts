import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Photo } from '../../product.model';
import { TempFileUploaderService } from '../../../shared/temp-file/temp-file-uploader.service';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { FileItem } from 'ng2-file-upload';
import { LoginService } from '../../../login/login.service';
import { uploadFileErrorDialogData } from '../../../files/file.model';
import { NotificationService, NotificationType } from '../../../shared/notification.service';
import { DeleteDialogService } from '../../../shared/dialog/delete/delete-dialog.service';
import { FileService } from '../../../files/file.service';
import { TempFileService } from '../../../shared/temp-file/temp-file.service';
import { AuthService } from '../../../login/auth/auth.service';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UNAUTHORIZED } from 'http-status-codes';
import { FileInfo } from '../../../files/file.model';
import { Page } from '../../../shared/pagination/page.model';
import { FilePreviewService } from '../../../files/file-preview.service';


@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss'],
})
export class FileUploaderComponent implements OnInit {
  @Input('component-destroyed') private componentDestroyed: Subject<void>;
  @Input('auth-key') authKey: string;
  @Input() readonly = false;

  @Output('open-expansion-panel') openExpansionPanelEvent = new EventEmitter<void>();

  readonly numPhotos = 4;
  get photoIndexes() {
    const indexes = [];
    for (let i = 0; i < this.numPhotos + 1; ++i) {
      indexes.push(i);
    }
    return indexes;
  }

  selectedFileIndex = 1;
  editMode = false;
  hasBaseDropZoneOver = false;

  uploader = new TempFileUploaderService(this.loginService);
  photosMap: Map<number, ProductPhoto> = new Map<number, ProductPhoto>();
  photoCopyMap: Map<number, Photo> = new Map<number, Photo>();
  uploadingError = false;
  files: Array<FileInfo>;
  progress: number;


  constructor(
    private loginService: LoginService,
    private notificationService: NotificationService,
    private deleteDialogService: DeleteDialogService,
    private tempFileService: TempFileService,
    private fileService: FileService,
    private sanitizer: DomSanitizer,
    private authService: AuthService,
    private filePreviewService: FilePreviewService,
  ) {
    this.uploader.onProgressAll = (percent) => this.progress = percent;
  }

  ngOnInit() {
    this.photoIndexes.forEach(index => {
      const productPhoto = {
        service: new TempFileUploaderService(this.loginService),
        order: index,
      } as ProductPhoto;
      this.photosMap.set(index, productPhoto);
    })
  }

  public allFilesUploaded(): boolean {
    let result = true;
    this.photosMap.forEach((value, key) => {
      if (value.file) {
        result = false;
      }
    });
    this.uploadingError = !result;
    return result;
  }

  // this.editedProduct.photos
  setPhotos(photos): void {
    this.photosMap.forEach((value, key) => this.removePhoto(key));
    if (photos) {
      photos.forEach(file => {
        if (!file.tempFile) {
          this.photosMap.get(file.order).photo = file;
        }
      });
    }

    this.photosMap.forEach((value, key) => {
      if (key === 0) {
        value.service.onAfterAddingAll = file => this.uploadAll(file);
      } else {
        value.service.onAfterAddingFile = file => this.uploadTemp(file, value.order);
      }
    });
    this.selectedFileIndex = 1;
    this.editMode = false;
    this.uploadingError = false;
    this.downloadPhotos();
  }

  removePhoto(order: number): void {
    const curUploader = this.photosMap.get(order);
    curUploader.fileUrl = undefined;
    curUploader.photo = undefined;
    curUploader.file = undefined;
    curUploader.blob = undefined;
  }

  private getAvailablePlacesNum(): number {
    let availablePlaces = 0;
    this.photosMap.forEach((value, key) => {
      if (value.file === undefined && value.order !== 0) {
        availablePlaces++;
      }
    });
    return availablePlaces;
  }

  public uploadAll(files: any[]): void {
    const availablePlaces = this.getAvailablePlacesNum();
    if (availablePlaces >= files.length) {
      let fileIndex = 0;
      this.photosMap.forEach((value, key) => {
        if (
          value.file === undefined
          &&
          value.order !== 0
          &&
          fileIndex < files.length
        ) {
          const newFile = files[fileIndex];
          this.photosMap.get(value.order).service.queue.push(newFile);
          this.uploadTemp(newFile, value.order);
          fileIndex++;
        }
      });
    } else {
      this.notificationService.notify('products.form.maxPhotos', NotificationType.SUCCESS);
    }
    this.photosMap.get(0).service.queue = [];
  }

  public uploadTemp(file: FileItem, order?: number): void {
    if (!file || !file.file.type || file.file.type.indexOf('image') === -1) {
      this.notificationService.notify('products.form.incorrectPhotoType', NotificationType.ERROR);
      return;
    }

    const productPhoto = this.photosMap.get(order);

    if (
      (!this.editMode && productPhoto.photo)
      ||
      !this.authService.isPermissionPresent(`${this.authKey}photos`, 'w')
    ) {
      return;
    }

    productPhoto.file = file;
    productPhoto.service.uploadItemWithForm(file, { description: 'Photo_' + order });

    file.onSuccess = response => {
      productPhoto.photo = { id: JSON.parse(response).id, tempFile: true, order };
      productPhoto.file = undefined;
      productPhoto.service.queue = [];
      this.downloadPhoto(productPhoto);
      if (this.uploadingError) {
        this.allFilesUploaded();
      }
    };

    file.onError = response => {
      if (this.isUnauthorized(response)) {
        productPhoto.service.clearQueue();
        return;
      }
      uploadFileErrorDialogData.onConfirmClick = () => this.uploadTemp(this.photosMap.get(order).file, order);
      uploadFileErrorDialogData.onCancelClick = () => {
        productPhoto.service.clearQueue();
        this.removePhoto(order);
      };
      this.deleteDialogService.open(uploadFileErrorDialogData);
    };
  }

  downloadPhoto(value: ProductPhoto): void {
    if (value.photo) {
      const observable: Observable<Blob> =
        (value.photo.tempFile)
          ?
          this.tempFileService.downloadTempFile(value.photo.id)
          :
          this.fileService.downloadFile(value.photo.id);

      observable.pipe(takeUntil(this.componentDestroyed))
        .subscribe((result) => {
          value.blob = result;
          value.fileUrl = this.getObjectUrl(value.blob);
        });
    }
  }

  private getObjectUrl(blob: Blob): SafeStyle {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${URL.createObjectURL(blob)})`);
  }

  public getBackgroundImage(order) {
    return this.photosMap.get(order).fileUrl;
  }

  turnOnEditMode(): void {
    this.photosMap.forEach((value, key) => {
      this.photoCopyMap.set(key, value.photo);
    });
    this.editMode = true;
    this.openExpansionPanelEvent.emit();
  }

  restoreEditedPhotos(): void {
    this.photoCopyMap.forEach((value, key) => {
      this.photosMap.get(key).photo = value;
    });
    this.downloadPhotos();
    this.editMode = false;
  }

  onFileChanged(event, order) {
    console.log(event.target.files[0].name, order);
  }

  private downloadPhotos(): void {
    this.photosMap.forEach((value, key) => this.downloadPhoto(value));
  }

  private isUnauthorized(response: string): boolean {
    return JSON.parse(response).status === UNAUTHORIZED;
  }

  public getPhotos(): Photo[] {
    const result: Photo[] = [];
    this.photosMap.forEach((value, key) => {
      if (value.photo) {
        result.push(value.photo);
      }
    });
    return result;
  }

  onFileOver(event, order) {
    this.hasBaseDropZoneOver[order] = event;
  }

  removeExtension(fileName: string): string {
    const result = fileName.split('.').slice(0, -1).join('.');
    return (!result || result.length === 0) ? fileName : result;
  }

  public preview(file: FileInfo): void {
    if (this.readonly) {
      return;
    }
    this.filePreviewService.showPreviewOf(file);
  }

  onScroll(e: any): void {
    console.log('on scroll');
  }

}

export interface ProductPhoto {
  photo?: Photo;
  file?: FileItem;
  order: number;
  service: TempFileUploaderService;
  blob?: Blob;
  fileUrl?: SafeStyle;
}
