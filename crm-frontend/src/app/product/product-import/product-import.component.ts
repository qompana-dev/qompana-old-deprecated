import {WarningDialogData} from './../../shared/dialog/warning/warning-dialog.component';
import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {CsvParsingErrorResponse, ProductsImportStrategy} from '../product.model';
import {Router} from '@angular/router';
import {ProductService} from '../product.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {finalize, takeUntil} from 'rxjs/operators';
import {NotificationService, NotificationType} from '../../shared/notification.service';
import {HttpErrorResponse} from '@angular/common/http';
import {BAD_REQUEST, CONFLICT, FORBIDDEN} from 'http-status-codes';
import {SliderComponent} from '../../shared/slider/slider.component';
import {SliderService} from '../../shared/slider/slider.service';
import {FormUtil} from '../../shared/form.util';
import {WarningDialogService} from '../../shared/dialog/warning/warning-dialog.service';
import {DeleteDialogData} from "../../shared/dialog/delete/delete-dialog.component";
import {ParseExceptionDetailDto} from "../../shared/import/import.model";
import {TranslateService} from "@ngx-translate/core";
import {DeleteDialogService} from "../../shared/dialog/delete/delete-dialog.service";

@Component({
  selector: 'app-product-import',
  templateUrl: './product-import.component.html',
  styleUrls: ['./product-import.component.scss']
})
export class ProductImportComponent implements OnInit, OnDestroy {
  @Input() slider: SliderComponent;
  @Output() productsChanged: EventEmitter<void> = new EventEmitter<void>();

  importStrategy = ProductsImportStrategy;
  form: FormGroup;
  hasBaseDropZoneOver = false;
  inProgress = false;
  private componentDestroyed: Subject<void> = new Subject();

  private readonly overwriteDialogData: Partial<WarningDialogData> = {
    title: 'products.import.overwriteProductsDialog.title',
    description: 'products.import.overwriteProductsDialog.description',
    confirmButton: 'products.import.overwriteProductsDialog.confirmButton',
    cancelButton: 'products.import.overwriteProductsDialog.cancelButton',
  };

  private readonly importErrorDialogData: Partial<DeleteDialogData> = {
    title: 'products.import.importErrorDialog.title',
    description: 'products.import.importErrorDialog.description',
    confirmButton: 'products.import.importErrorDialog.tryAgain',
    cancelButton: 'products.import.importErrorDialog.cancel',
  };

  // @formatter:off
  get productsFileForm(): AbstractControl {
    return this.form.get('productsFile');
  }

  get importStrategyForm(): AbstractControl {
    return this.form.get('importStrategy');
  }

  get fileWithHeaderForm(): AbstractControl {
    return this.form.get('fileWithHeader');
  }

  // @formatter:on

  constructor(private router: Router,
              private sliderService: SliderService,
              private productService: ProductService,
              private formBuilder: FormBuilder,
              private warningDialogService: WarningDialogService,
              private notificationService: NotificationService,
              private deleteDialogService: DeleteDialogService,
              private translateService: TranslateService
  ) {
    this.form = this.formBuilder.group({
      productsFile: ['', Validators.required],
      importStrategy: ['', Validators.required],
      fileWithHeader: [true, Validators.required]
    });
  }

  get productsFile(): AbstractControl {
    return this.form.get('productsFile');
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  cancel(): void {
    (this.slider) ? this.slider.close() : this.sliderService.closeSlider();
    this.router.navigate(['products']);
  }

  onFileChange($event: Event): void {
    if ($event.target['files'].length > 0) {
      const file = $event.target['files'][0];
      this.setFile(file);
    }
  }

  onDragFileChange($event: DragEvent): void {
    if ($event.dataTransfer.files.length > 0) {
      const file: File = $event.dataTransfer.files[0];
      this.setFile(file);
    }
  }

  private setFile(file: File): void {
    const re = /(?:\.([^.]+))?$/;
    const exenstion = re.exec(file.name)[1];
    if (exenstion === 'csv') {
      this.productsFile.setValue(file);
    } else {
      this.notificationService.notify('products.import.notification.csvRequired', NotificationType.ERROR);
    }
  }

  importProducts(): void {
    FormUtil.setTouched(this.form);
    if (!this.inProgress && this.form.valid) {
      if (this.form.get('importStrategy').value === 'OVERRIDE') {
        this.showCloseWarningDialog();
      } else {
        this.uploadProducts();
      }
    }
  }

  private uploadProducts(): void {
    const file = this.productsFile.value;
    const importStrategy = this.form.get('importStrategy').value;
    const fileWithHeader = this.form.get('fileWithHeader').value;
    this.inProgress = true;
    this.productService.uploadProducts(file, importStrategy, fileWithHeader).pipe(
      takeUntil(this.componentDestroyed),
      finalize(() => this.inProgress = false)
    ).subscribe(
      () => this.notifySuccessImport(),
      err => this.notifyError(err)
    );
  }

  private showCloseWarningDialog(): void {
    this.overwriteDialogData.onConfirmClick = () => this.uploadProducts();
    this.overwriteDialogData.onCancelClick = () => this.warningDialogService.close();
    this.warningDialogService.open(this.overwriteDialogData);
  }

  private notifySuccessImport(): void {
    this.notificationService.notify('products.import.notification.success', NotificationType.SUCCESS);
    this.productsChanged.emit();
    this.cancel();
  }

  private notifyError(response: HttpErrorResponse): void {
    switch (response.status) {
      case BAD_REQUEST:
        this.notificationService.notify('products.import.notification.badRequest', NotificationType.ERROR);
        break;
      case 423:
        const errorList: ParseExceptionDetailDto[] = response.error;
        this.importErrorDialogData.onConfirmClick = () => this.importProducts();
        this.importErrorDialogData.noteFirst = undefined;
        try {
          if (errorList && errorList.length > 0) {
            this.importErrorDialogData.noteFirst = 'products.import.importErrorDialog.incorrectCsv';
            this.importErrorDialogData.errorList = errorList
              .map(item => {
                const fieldName = this.translateService.instant('products.import.importErrorDialog.types.' + item.field);
                return this.translateService.instant('products.import.importErrorDialog.errorMsg',
                  {field: fieldName, line: item.line, value: item.value});
              });
          }
        } catch (e) {
          console.log('parse msg error');
        }
        this.deleteDialogService.openWithoutHeight(this.importErrorDialogData);
        break;
      case CONFLICT:
        this.notificationService.notify('products.import.notification.skuCodeOrCodeConflict', NotificationType.ERROR);
        break;
      case FORBIDDEN:
        this.notificationService.notify('products.import.notification.noPermission', NotificationType.ERROR);
        break;
      default:
        this.notificationService.notify('products.import.notification.undefinedError', NotificationType.ERROR);
    }
  }
}
