import {Directive, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[appFileDragDrop]'
})
export class FileDragDropDirective {
  @Output() fileDropped: EventEmitter<DragEvent> = new EventEmitter<DragEvent>();
  @Output() dragLeave: EventEmitter<void> = new EventEmitter<void>();
  @Output() dragOver: EventEmitter<void> = new EventEmitter<void>();

  @HostListener('dragover', ['$event'])
  public onDragOver(evt: any): void {
    evt.preventDefault();
    evt.stopPropagation();
    this.dragOver.emit();
  }

  @HostListener('dragleave', ['$event'])
  public onDragLeave(evt: any): void {
    evt.preventDefault();
    evt.stopPropagation();
    this.dragLeave.emit();
  }

  @HostListener('drop', ['$event'])
  public ondrop(evt: DragEvent): void {
    evt.preventDefault();
    evt.stopPropagation();
    this.fileDropped.emit(evt);
    this.dragLeave.emit();
    // const files = evt.dataTransfer.files;
  }
}
