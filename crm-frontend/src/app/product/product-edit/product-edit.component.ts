import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {ProductFormComponent} from '../product-form/product-form.component';
import {Subject} from 'rxjs';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {BAD_REQUEST, CONFLICT, FORBIDDEN, NOT_FOUND} from 'http-status-codes';
import {SliderService} from '../../shared/slider/slider.service';
import {ProductService} from '../product.service';
import {Product} from '../product.model';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit, OnDestroy {

  @Input() editedProduct: Product;
  @Output() productEdited: EventEmitter<void> = new EventEmitter();
  @ViewChild(ProductFormComponent) private productFormComponent: ProductFormComponent;
  private componentDestroyed: Subject<void> = new Subject();

  readonly editProductError: ErrorTypes = {
    base: 'products.form.notifications.',
    defaultText: 'undefinedError',
    errors: [{code: CONFLICT, text: 'addingError'}, {code: FORBIDDEN, text: 'noPermission'}, {code: NOT_FOUND, text: 'notFound'},
      {code: BAD_REQUEST, text: 'validationError'}]
  };

  constructor(private sliderService: SliderService,
              private productService: ProductService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    if (!this.productFormComponent.isFormValid()) {
      this.productFormComponent.markAsTouched();
      return;
    }

    const product: Product = this.productFormComponent.getProduct();
    this.productService.modifyProduct(this.editedProduct.id, product)
      .subscribe(
        () => this.handleEditSuccess(),
        (err) => this.notificationService.notifyError(this.editProductError, err.status)
      );
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
    if (this.productFormComponent) {
      this.productFormComponent.clear();
    }
  }

  private handleEditSuccess(): void {
    this.notificationService.notify('products.form.notifications.editedSuccessfully', NotificationType.SUCCESS);
    this.productEdited.emit();
    this.closeSlider();
  }
}
