import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Product, ProductsImportStrategy, SellHistoryResult, SellHistory } from './product.model';
import {Page, PageRequest} from '../shared/pagination/page.model';
import {FilterStrategy} from '../shared/model';
import {TableUtil} from '../shared/table.util';
import cloneDeep from 'lodash/cloneDeep';
import {getHeaderForAuthType} from '../shared/permissions-utils';
import {UrlUtil} from '../shared/url.util';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {
  }

  public getProduct(id: number): Observable<Product> {
    const url = `${UrlUtil.url}/crm-business-service/products/${id}`;
    return this.http.get<Product>(url);
  }

  public getProducts(pageRequest: PageRequest, strategy: FilterStrategy): Observable<Page<Product>> {
    let params = new HttpParams({fromObject: pageRequest as any});
    params = TableUtil.enrichParamsWithFilterStrategy(strategy, params);
    const url = `${UrlUtil.url}/crm-business-service/products`;
    return this.http.get<Page<Product>>(url, {params});
  }

  public getProductsForPriceBook(pageRequest: PageRequest, strategy: FilterStrategy, priceBookId: number,
                                 fromOpportunityForm?: boolean): Observable<Page<Product>> {
    const pageRequestToSend = cloneDeep(pageRequest);
    if (pageRequestToSend && pageRequestToSend.sort) {
      pageRequestToSend.sort = 'product.' + pageRequestToSend.sort;
    }
    let params = new HttpParams({fromObject: pageRequestToSend as any});
    params = TableUtil.enrichParamsWithFilterStrategy(strategy, params);
    const url = `${UrlUtil.url}/crm-business-service/products/for-price-book/${priceBookId}`;
    return this.http.get<Page<Product>>(url, {params, headers: getHeaderForAuthType((fromOpportunityForm) ? 'addProductFromPriceBook' : undefined)});
  }

  public getProductHistory(productId: number, finishResult?: SellHistoryResult): Observable<SellHistory[]> {

    let params = new HttpParams();
    if (finishResult) {
      params = params.set('finishResult', finishResult);
    }

    return this.http.get<SellHistory[]>(`${UrlUtil.url}/crm-business-service/product/${productId}/salesHistory`, {params});
  }

  public uploadProducts(file: any, importStrategy: ProductsImportStrategy, fileWithHeader: any): Observable<void> {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('strategy', importStrategy);
    formData.append('headerPresent', fileWithHeader);
    const url = `${UrlUtil.url}/crm-business-service/products/import`;
    return this.http.post<void>(url, formData);
  }

  public createProduct(product: Product): Observable<Product> {
    const url = `${UrlUtil.url}/crm-business-service/products`;
    return this.http.post<Product>(url, product);
  }

  public deleteProducts(products: Product[]): Observable<void> {
    const params = new HttpParams().set('ids', products.map(customer => customer.id).join(','));
    return this.http.delete<void>(`${UrlUtil.url}/crm-business-service/products`, {params});
  }

  public deleteProduct(id: number): Observable<void> {
    return this.http.delete<void>(`${UrlUtil.url}/crm-business-service/products/${id}`);
  }

  public modifyProduct(id: number, product: Product): Observable<void> {
    const url = `${UrlUtil.url}/crm-business-service/products/${id}`;
    return this.http.put<void>(url, product);
  }
}
