import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../login/auth/auth-guard.service';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductImportComponent} from './product-import/product-import.component';


const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: ProductListComponent},
  {path: 'for-price-book/:priceBookId', canActivate: [AuthGuard], component: ProductListComponent},
  // Full view
  // {path: ':id/documents', canActivate: [AuthGuard], component: ProductDetailsComponent}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule {
}
