import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {SliderService} from '../../shared/slider/slider.service';
import {ErrorTypes, NotificationService, NotificationType} from '../../shared/notification.service';
import {Subject} from 'rxjs';
import {ProductFormComponent} from '../product-form/product-form.component';
import {Product} from '../product.model';
import {ProductService} from '../product.service';
import {CONFLICT, FORBIDDEN} from 'http-status-codes';
import {takeUntil} from 'rxjs/operators';
import {PriceBook} from '../../price-book/price-book.model';
import {PriceBookService} from '../../price-book/price-book.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss']
})
export class ProductAddComponent implements OnInit, OnDestroy {

  @Output() productAdded: EventEmitter<void> = new EventEmitter();
  @ViewChild(ProductFormComponent) private productFormComponent: ProductFormComponent;
  private componentDestroyed: Subject<void> = new Subject();

  readonly addProductError: ErrorTypes = {
    base: 'products.form.notifications.',
    defaultText: 'undefinedError',
    errors: [
      {
        code: CONFLICT,
        text: 'addingError'
      },
      {
        code: FORBIDDEN,
        text: 'noPermission'
      }
    ]
  };
  saveInProgress = false;

  defaultCurrency: string;

  constructor(private sliderService: SliderService,
              private productService: ProductService,
              private priceBookService: PriceBookService,
              private notificationService: NotificationService) { }

  ngOnInit() {
    this.subscribeForDefaultCurrency();
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }

  submit(): void {
    if (!this.productFormComponent.isFormValid()) {
      this.productFormComponent.markAsTouched();
      return;
    }

    if (!this.saveInProgress) {
      this.saveInProgress = true;
      const product: Product = this.productFormComponent.getProduct();
      this.productService.createProduct(product)
        .subscribe(
          () => this.handleAddSuccess(),
          (err) => {
            this.saveInProgress = false;
            this.notificationService.notifyError(this.addProductError, err.status);
          }
        );
    }
  }

  closeSlider(): void {
    this.sliderService.closeSlider();
    if (this.productFormComponent) {
      this.productFormComponent.clear();
    }
  }

  private subscribeForDefaultCurrency(): void {
    this.priceBookService.getPriceBookToAssign()
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((priceBooks: PriceBook[]) =>
        this.defaultCurrency = priceBooks.find(e => e.isDefault).currency
      );
  }

  private handleAddSuccess(): void {
    this.saveInProgress = false;
    this.notificationService.notify('products.form.notifications.addedSuccessfully', NotificationType.SUCCESS);
    this.productAdded.emit();
    this.closeSlider();
  }
}
