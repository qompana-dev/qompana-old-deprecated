"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ProductAddComponent = void 0;
var core_1 = require("@angular/core");
var notification_service_1 = require("../../shared/notification.service");
var rxjs_1 = require("rxjs");
var product_form_component_1 = require("../product-form/product-form.component");
var http_status_codes_1 = require("http-status-codes");
var operators_1 = require("rxjs/operators");
var ProductAddComponent = /** @class */ (function () {
    function ProductAddComponent(sliderService, productService, priceBookService, notificationService) {
        this.sliderService = sliderService;
        this.productService = productService;
        this.priceBookService = priceBookService;
        this.notificationService = notificationService;
        this.productAdded = new core_1.EventEmitter();
        this.componentDestroyed = new rxjs_1.Subject();
        this.addProductError = {
            base: 'products.form.notifications.',
            defaultText: 'undefinedError',
            errors: [
                {
                    code: http_status_codes_1.CONFLICT,
                    text: 'addingError'
                },
                {
                    code: http_status_codes_1.FORBIDDEN,
                    text: 'noPermission'
                }
            ]
        };
        this.saveInProgress = false;
    }
    ProductAddComponent.prototype.ngOnInit = function () {
        this.subscribeForDefaultCurrency();
    };
    ProductAddComponent.prototype.ngOnDestroy = function () {
        this.componentDestroyed.next();
        this.componentDestroyed.complete();
    };
    ProductAddComponent.prototype.submit = function () {
        var _this = this;
        if (!this.productFormComponent.isFormValid()) {
            this.productFormComponent.markAsTouched();
            return;
        }
        if (!this.saveInProgress) {
            this.saveInProgress = true;
            var product = this.productFormComponent.getProduct();
            this.productService.createProduct(product)
                .subscribe(function () { return _this.handleAddSuccess(); }, function (err) {
                _this.saveInProgress = false;
                _this.notificationService.notifyError(_this.addProductError, err.status);
            });
        }
    };
    ProductAddComponent.prototype.closeSlider = function () {
        this.sliderService.closeSlider();
        if (this.productFormComponent) {
            this.productFormComponent.clear();
        }
    };
    ProductAddComponent.prototype.subscribeForDefaultCurrency = function () {
        var _this = this;
        this.priceBookService.getPriceBookToAssign()
            .pipe(operators_1.takeUntil(this.componentDestroyed))
            .subscribe(function (priceBooks) {
            return _this.defaultCurrency = priceBooks.find(function (e) { return e.isDefault; }).currency;
        });
    };
    ProductAddComponent.prototype.handleAddSuccess = function () {
        this.saveInProgress = false;
        this.notificationService.notify('products.form.notifications.addedSuccessfully', notification_service_1.NotificationType.SUCCESS);
        this.productAdded.emit();
        this.closeSlider();
    };
    __decorate([
        core_1.Output()
    ], ProductAddComponent.prototype, "productAdded");
    __decorate([
        core_1.ViewChild(product_form_component_1.ProductFormComponent)
    ], ProductAddComponent.prototype, "productFormComponent");
    ProductAddComponent = __decorate([
        core_1.Component({
            selector: 'app-product-add',
            templateUrl: './product-add.component.html',
            styleUrls: ['./product-add.component.scss']
        })
    ], ProductAddComponent);
    return ProductAddComponent;
}());
exports.ProductAddComponent = ProductAddComponent;
