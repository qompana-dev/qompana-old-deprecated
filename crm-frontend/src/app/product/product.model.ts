import {PriceBook} from '../price-book/price-book.model';

export interface Product {
  id: number;
  name: string;
  code: string;
  skuCode: string;
  categories: string;
  subcategories: string;
  manufacturerId: number;
  supplierId: number;
  brand: string;
  purchasePrice: number;
  purchasePriceCurrency: string;
  currency: string;
  sellPriceNet: number;
  vat: number;
  active: boolean;
  description: string;
  specification: string;
  notes: string;
  checked?: boolean;
  subcategoryIds: number[];
  categoryIds: number[];
  photos: Photo[];
  productPrices: ProductPrice[];
  unit: number;
  unitName: string;
}

export interface ProductPrice {
  id?: number;
  price: number;
  priceBook: PriceBook;
  active: boolean;
}

export interface Photo {
  id: string;
  tempFile: boolean;
  order: number;
}

export enum ProductsImportStrategy {
  Override = 'OVERRIDE',
  AddAll = 'ADD_ALL',
  AddOnlyNew = 'ADD_ONLY_NEW',
  UpdateExisting = 'UPDATE_EXISTING'
}

export interface CsvParsingErrorResponse {
  lineIndex: number;
  columnIndex: number;
  corruptedText: string;
}

export interface SellHistory {
  clientName: string,
  currency: string,
  finishDate: string,
  finishResult: SellHistoryResult,
  price: number,
  priceSummed: number,
  quantity: number,
  unit: string
}

export enum SellHistoryResult {
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR'
}

export class ProductCategory {
  id: number;
  name?: string;
  subcategories?: ProductSubcategory[];
  toDelete?: boolean;
  initialSubcategory?: number;
  assignedToAnyProduct?: boolean;


  constructor(category: string) {
    this.name = category;
    this.subcategories = [];
  }
}

export class ProductSubcategory {
  id: number;
  categoryId?: number;
  name?: string;
  toDelete?: boolean;
  initial?: boolean;
  assignedToAnyProduct?: boolean;
  categoryName?: string;

  constructor(category: string) {
    this.name = category;
  }
}

export function productSubcategoryComparator(a: ProductSubcategory, b: ProductSubcategory): boolean {
  return a && b && a.categoryId === b.categoryId && a.name === b.name;
}
