import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductImportComponent } from './product-import/product-import.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { ProductAddComponent } from './product-add/product-add.component';
import { NgxMaskModule } from 'ngx-mask';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductPriceComponent } from './product-form/product-price/product-price.component';
import { PhotoUploaderComponent } from './product-form/photo-uploader/photo-uploader.component';
import { FileUploaderComponent } from './product-form/file-uploader/file-uploader.component';
import { AddProductToPriceBookFormComponent } from './product-form/product-price/add-product-to-price-book-form/add-product-to-price-book-form.component';
import { CategoriesModule } from './categories/categories.module';
import { SellHistoryComponent } from './product-form/sell-history/sell-history.component';
import { FileDragDropDirective } from './product-import/file-drag-drop.directive';
import { FilesModule } from '../files/files.module';
import { WidgetModule } from '../widgets/widget.module';
import {DictionaryModule} from "../dictionary/dictionary.module";

@NgModule({
  declarations: [
    FileDragDropDirective,
    ProductListComponent,
    ProductImportComponent,
    ProductFormComponent,
    ProductAddComponent,
    ProductEditComponent,
    ProductPriceComponent,
    SellHistoryComponent,
    AddProductToPriceBookFormComponent,
    PhotoUploaderComponent,
    FileUploaderComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ProductRoutingModule,
    NgxMaskModule,
    CategoriesModule,
    WidgetModule,
    FilesModule,
    DictionaryModule,
  ]
})
export class ProductModule {
}
