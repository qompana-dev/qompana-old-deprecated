import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {NotificationService, NotificationType} from '../shared/notification.service';
import {Token, TokenType} from './token.model';
import {UrlUtil} from '../shared/url.util';
import {LoginService} from '../login/login.service';

@Component({
  selector: 'app-token',
  template: `
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 p-0">
          <div class="min-vh-100 m-0 d-flex flex-column justify-content-center">
              <mat-spinner class="token-spinner" color="primary"></mat-spinner>
          </div>
        </div>
      </div>
    </div>
  `
})
export class TokenComponent implements OnInit, OnDestroy {

  private paramSubscription: Subscription;
  private token: string;

  constructor(private activatedRoute: ActivatedRoute,
              private $router: Router,
              private http: HttpClient,
              private loginService: LoginService,
              private notificationService: NotificationService) {
    this.loginService.logoutWithoutRedirect();
  }

  ngOnInit() {
    this.paramSubscription = this.activatedRoute.params.subscribe((param) => {
      this.token = param.token;
      this.sendToken();
    });
  }

  ngOnDestroy() {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  private sendToken() {
    const sub = this.http.get(`${UrlUtil.url}/crm-user-service/token/${this.token}`)
      .subscribe(
        (token: Token) => this.redirectBasedOnTokenType(token),
        () => this.handleError(),
        () => sub.unsubscribe()
      );
  }

  private redirectBasedOnTokenType(token: Token) {
    if (TokenType[token.type] === TokenType.SEND_ACTIVATION_CODE ||
      TokenType[token.type] === TokenType.RESEND_ACTIVATION_CODE ||
      TokenType[token.type] === TokenType.RESET_PASSWORD) {
      this.$router.navigate(['/person', 'set-password', token.token]);
    } else {
      this.$router.navigate(['/login']);
    }
  }

  private handleError() {
    this.notificationService.notify('token.expiredOrDeleted', NotificationType.ERROR);
    this.$router.navigate(['/login']);
  }
}
