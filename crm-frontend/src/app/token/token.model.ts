export class Token {
  type: TokenType | string;
  token: string;
}

export enum TokenType {
  RESET_PASSWORD, SEND_ACTIVATION_CODE, RESEND_ACTIVATION_CODE
}
