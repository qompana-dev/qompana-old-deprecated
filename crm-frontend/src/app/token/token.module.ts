import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TokenRoutingModule } from './token-routing.module';
import {TokenComponent} from './token.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    TokenComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    TokenRoutingModule
  ]
})
export class TokenModule { }
