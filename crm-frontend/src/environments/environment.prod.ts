export const environment = {
  production: true,
  url: '/api',
  toastTime: 5000,
  wsUrl: null,
  mapboxUri: 'https://api.mapbox.com',
  mapboxToken: 'pk.eyJ1IjoicDNpdGVyIiwiYSI6ImNqeGE1dzk4ajEwZDAzb2xnaGhlcWFuNnkifQ.BeyJ3XjKGANgm3nyKCFIPQ',
  reconnect_delay: 5000,
  gatewayPort: 8078
};
