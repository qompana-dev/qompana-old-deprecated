// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url: 'http://localhost:8078',
  toastTime: 5000,
  wsUrl: 'ws://localhost:8078/ws',
  mapboxUri: 'https://api.mapbox.com',
  mapboxToken: 'pk.eyJ1IjoicDNpdGVyIiwiYSI6ImNqeGE1dzk4ajEwZDAzb2xnaGhlcWFuNnkifQ.BeyJ3XjKGANgm3nyKCFIPQ',
  reconnect_delay: 5000,
  gatewayPort: 8078,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
