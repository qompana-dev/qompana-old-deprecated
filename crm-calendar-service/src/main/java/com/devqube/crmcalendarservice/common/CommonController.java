package com.devqube.crmcalendarservice.common;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
public class CommonController implements CommonApi {

    private final CommonService commonService;

    public CommonController(CommonService commonService) {
        this.commonService = commonService;
    }

    @Override
    public Boolean checkIfConfigurationExists(@NotNull String accountId) {
        return commonService.isConfigurationPresent(accountId);
    }

    @Override
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeCalendarConfiguration(@NotNull String accountId) {
        commonService.removeCalendarConfiguration(accountId);
    }
}
