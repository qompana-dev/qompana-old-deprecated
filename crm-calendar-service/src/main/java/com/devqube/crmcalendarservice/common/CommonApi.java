package com.devqube.crmcalendarservice.common;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.constraints.NotNull;

@Validated
@Api(value = "common")
public interface CommonApi {
    @ApiOperation(value = "Check if connected with Google or Microsoft", nickname = "checkIfConfigured")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "true if connected, false if not", response = Boolean.class)
    })
    @RequestMapping(value = "/configuration-check/{accountId}", method = RequestMethod.GET)
    Boolean checkIfConfigurationExists(@NotNull @PathVariable(value = "accountId", required = true) String accountId);

    @ApiOperation(value = "Remove Google or Microsoft configuration", nickname = "removeCalendarConfiguration")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "configuration was removed")
    })
    @RequestMapping(value = "/configuration/{accountId}", method = RequestMethod.DELETE)
    void removeCalendarConfiguration(@NotNull @PathVariable(value = "accountId", required = true) String accountId);
}
