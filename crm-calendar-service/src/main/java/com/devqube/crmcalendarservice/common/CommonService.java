package com.devqube.crmcalendarservice.common;

import com.devqube.crmcalendarservice.google.calendar.GoogleCalendarRepository;
import com.devqube.crmcalendarservice.google.credential.GoogleCredentialRepository;
import com.devqube.crmcalendarservice.microsoft.calendar.MicrosoftCalendarRepository;
import com.devqube.crmcalendarservice.microsoft.credential.MicrosoftCredentialRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CommonService {
    private final GoogleCalendarRepository googleCalendarRepository;
    private final GoogleCredentialRepository googleCredentialRepository;
    private final MicrosoftCalendarRepository microsoftCalendarRepository;
    private final MicrosoftCredentialRepository microsoftCredentialRepository;

    public CommonService(GoogleCalendarRepository googleCalendarRepository, GoogleCredentialRepository googleCredentialRepository,
                         MicrosoftCalendarRepository microsoftCalendarRepository, MicrosoftCredentialRepository microsoftCredentialRepository) {
        this.googleCalendarRepository = googleCalendarRepository;
        this.googleCredentialRepository = googleCredentialRepository;
        this.microsoftCalendarRepository = microsoftCalendarRepository;
        this.microsoftCredentialRepository = microsoftCredentialRepository;
    }

    public boolean isConfigurationPresent(String accountId) {
        return googleCalendarRepository.existsByAccountId(accountId) || microsoftCalendarRepository.existsByAccountId(accountId);
    }

    @Transactional
    public void removeCalendarConfiguration(String accountId) {
        microsoftCalendarRepository.deleteByAccountId(accountId);
        googleCalendarRepository.deleteAllByAccountId(accountId);
        microsoftCredentialRepository.deleteByAccountId(accountId);
        googleCredentialRepository.deleteByAccountId(accountId);
    }
}
