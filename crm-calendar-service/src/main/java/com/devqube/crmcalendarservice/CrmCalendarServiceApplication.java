package com.devqube.crmcalendarservice;

import com.devqube.crmcalendarservice.google.proxy.GoogleCalendarProxyProperties;
import com.devqube.crmshared.license.LicenseCheck;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@Import({LicenseCheck.class})
@EnableConfigurationProperties({
        GoogleCalendarProxyProperties.class
})
public class CrmCalendarServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrmCalendarServiceApplication.class, args);
    }

}
