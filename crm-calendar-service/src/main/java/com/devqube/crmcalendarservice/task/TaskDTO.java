package com.devqube.crmcalendarservice.task;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskDTO {
    private String id;
    private String subject;
    private String priority;
    private String state;
    private String activityType;
    private String creator;
    private String place;
    private String longitude;
    private String latitude;
    private String description;
    private String dateTimeFrom;
    private String dateTimeTo;
    private Boolean allDayEvent;
    private String owner;
    private String timeZone;
}
