package com.devqube.crmcalendarservice.task;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "crm-task-service", url = "${crm-task-service.url}")
public interface TaskClient {
    @PostMapping("/internal/tasks")
    TaskDTO save(@RequestParam Boolean sync, @RequestBody TaskDTO task);

    @GetMapping("/internal/tasks")
    List<TaskDTO> getAllByOwner(@RequestParam String owner);

    @PutMapping("/internal/tasks/{taskId}")
    void update(@PathVariable String taskId, @RequestParam Boolean sync, @RequestBody TaskDTO task);

    @DeleteMapping("/internal/tasks/{taskId}")
    void delete(@PathVariable String taskId, @RequestParam Boolean sync);

    @GetMapping("/internal/tasks/{taskId}")
    TaskDTO get(@PathVariable String taskId);
}
