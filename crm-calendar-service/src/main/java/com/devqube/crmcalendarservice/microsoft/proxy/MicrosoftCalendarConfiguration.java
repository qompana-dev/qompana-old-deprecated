package com.devqube.crmcalendarservice.microsoft.proxy;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "microsoft.proxy")
public class MicrosoftCalendarConfiguration {
    private String id;
    private String secret;
    private String redirectURI;
    private String authorizeUrl;
    private String notificationURI;
    private Integer expirationMinutes;
    static final String CALENDAR_NAME = "CRM Manager";
}
