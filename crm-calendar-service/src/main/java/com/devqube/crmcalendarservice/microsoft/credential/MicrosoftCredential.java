package com.devqube.crmcalendarservice.microsoft.credential;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MicrosoftCredential {
    @Id
    @SequenceGenerator(name = "microsoft_credential_seq", sequenceName = "microsoft_credential_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "microsoft_credential_seq")
    private Long id;

    @Column(unique = true)
    @NotNull
    private String accountId;

    @NotNull
    private String accessToken;

    @NotNull
    private Integer expirationTimeSeconds;

    @NotNull
    private String refreshToken;

    @NotNull
    private String idToken;

    @NotNull
    private LocalDateTime expirationTime;
}
