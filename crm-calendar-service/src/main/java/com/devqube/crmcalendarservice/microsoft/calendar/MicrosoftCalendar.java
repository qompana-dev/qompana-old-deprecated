package com.devqube.crmcalendarservice.microsoft.calendar;

import com.devqube.crmcalendarservice.microsoft.event.MicrosoftEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MicrosoftCalendar {
    @Id
    @SequenceGenerator(name = "microsoft_calendar_seq", sequenceName = "microsoft_calendar_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "microsoft_calendar_seq")
    private Long id;

    @NotNull
    @Column(unique = true)
    private String accountId;

    @NotNull
    @Column(unique = true)
    private String calendarId;

    private String subscriptionId;
    private LocalDateTime subscriptionExpirationTime;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "calendar")
    private List<MicrosoftEvent> events;

    public MicrosoftCalendar(@NotNull String accountId, @NotNull String calendarId) {
        this.accountId = accountId;
        this.calendarId = calendarId;
    }
}
