package com.devqube.crmcalendarservice.microsoft.proxy;

import com.devqube.crmcalendarservice.encryption.EncryptionException;
import com.devqube.crmcalendarservice.kafka.CalendarProxy;
import com.devqube.crmcalendarservice.microsoft.calendar.MicrosoftCalendar;
import com.devqube.crmcalendarservice.microsoft.calendar.MicrosoftCalendarService;
import com.devqube.crmcalendarservice.microsoft.credential.MicrosoftCredential;
import com.devqube.crmcalendarservice.microsoft.credential.MicrosoftCredentialService;
import com.devqube.crmcalendarservice.microsoft.event.MicrosoftEventService;
import com.devqube.crmcalendarservice.microsoft.exception.MicrosoftException;
import com.devqube.crmcalendarservice.microsoft.util.MicrosoftUtil;
import com.devqube.crmcalendarservice.task.TaskDTO;
import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.models.extensions.Calendar;
import com.microsoft.graph.requests.extensions.ICalendarCollectionPage;
import com.microsoft.graph.requests.extensions.ICalendarCollectionRequestBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.devqube.crmcalendarservice.microsoft.proxy.MicrosoftCalendarConfiguration.CALENDAR_NAME;

@Service
@Slf4j
public class MicrosoftCalendarProxy implements CalendarProxy {
    private final MicrosoftCredentialService microsoftCredentialService;
    private final MicrosoftCalendarConfiguration configuration;
    private final MicrosoftCalendarService microsoftCalendarService;
    private final MicrosoftEventService eventService;
    private final MicrosoftCalendarNotification microsoftCalendarNotification;
    private final MicrosoftCalendarSynchronizer microsoftCalendarSynchronizer;
    private final MicrosoftCalendarOperationExecutor microsoftCalendarOperationExecutor;

    public MicrosoftCalendarProxy(MicrosoftCalendarSynchronizer microsoftCalendarSynchronizer, MicrosoftCalendarNotification microsoftCalendarNotification, MicrosoftCredentialService microsoftCredentialService, MicrosoftCalendarConfiguration configuration, MicrosoftCalendarService microsoftCalendarService, MicrosoftEventService eventService, MicrosoftCalendarOperationExecutor microsoftCalendarOperationExecutor) {
        this.microsoftCalendarSynchronizer = microsoftCalendarSynchronizer;
        this.microsoftCalendarNotification = microsoftCalendarNotification;
        this.microsoftCredentialService = microsoftCredentialService;
        this.configuration = configuration;
        this.microsoftCalendarService = microsoftCalendarService;
        this.eventService = eventService;
        this.microsoftCalendarOperationExecutor = microsoftCalendarOperationExecutor;
    }

    @Override
    public void handleOAuth2Callback(String code, String accountId) throws EncryptionException, MicrosoftException {
        if (microsoftCredentialService.tokenExist(accountId)) {
            disconnectAccount(accountId);
        }
        try {
            MicrosoftCredential credential = microsoftCredentialService.obtainNewToken(code, accountId);
            Optional<Calendar> calendarOpt = findExistingMicrosoftCrmCalendar(accountId);
            calendarOpt.ifPresent(calendar -> microsoftCredentialService.getClient(accountId).me().calendars(calendar.id).buildRequest().delete());
            Calendar calendar = createMicrosoftCrmCalendar(accountId);

            MicrosoftCalendar msCalendar = microsoftCalendarService.getCalendarByAccountId(accountId)
                    .orElseGet(() -> new MicrosoftCalendar(accountId, calendar.id));

            microsoftCalendarService.save(msCalendar);
            microsoftCalendarNotification.createNotificationSubscription(accountId);
            microsoftCalendarSynchronizer.addTasksToMsCalendar(accountId);
        } catch (ClientException e) {
            log.error(e.getMessage());
            this.microsoftCredentialService.removeCredential(accountId);
        }
    }

    @Override
    public String generateRedirectURL(String accountId) {
        return MicrosoftUtil.getLoginUrl(configuration.getAuthorizeUrl(), configuration.getId(), configuration.getRedirectURI(), accountId);
    }

    @Override
    public boolean connected(String accountId) {
        return microsoftCredentialService.tokenExist(accountId);
    }

    @Override
    public void addEvent(TaskDTO taskDTO) throws MicrosoftException {
        microsoftCalendarOperationExecutor.addEvent(taskDTO);
    }

    @Override
    public void modifyEvent(TaskDTO taskDTO) throws MicrosoftException {
        microsoftCalendarOperationExecutor.modifyEvent(taskDTO);
    }

    @Override
    public void deleteEvent(String accountId, String taskId) throws MicrosoftException {
        microsoftCalendarOperationExecutor.deleteEvent(accountId, taskId);
    }

    public void disconnectAccount(String accountId) throws MicrosoftException {
        //TODO: move to CalendarProxy interface, add to google proxy service, make public
        if (calendarExistsInMicrosoft(accountId)) {
            MicrosoftCalendar calendar = microsoftCalendarService.getByAccountId(accountId);

            try {
                microsoftCredentialService.getClient(accountId).me().calendars(calendar.getCalendarId()).buildRequest().delete();
            } catch (ClientException e) {
                throw new MicrosoftException();
            }
            eventService.deleteByCalendarId(calendar.getId());
            microsoftCalendarService.deleteCalendarByAccountId(accountId);
            microsoftCredentialService.removeCredential(accountId);
        }
    }

    public void handleMicrosoftNotification(String body) {
        microsoftCalendarNotification.handleMicrosoftNotification(body);
    }

    private Optional<Calendar> findExistingMicrosoftCrmCalendar(String accountId) {
        return toList(microsoftCredentialService.getClient(accountId).me().calendars().buildRequest().get()).stream()
                .filter(c -> c.name.equals(CALENDAR_NAME))
                .findFirst();
    }

    private Calendar createMicrosoftCrmCalendar(String accountId) {
        Calendar calendar = new Calendar();
        calendar.name = getCalendarName(accountId);
        Calendar response = microsoftCredentialService.getClient(accountId).me().calendars().buildRequest().post(calendar);
        return response;
    }

    private boolean calendarExistsInMicrosoft(String accountId) {
        Optional<MicrosoftCalendar> calendar = microsoftCalendarService.getCalendarByAccountId(accountId);
        if (calendar.isEmpty()) {
            return false;
        }
        try {
            microsoftCredentialService.getClient(accountId).me().calendars(calendar.get().getCalendarId()).buildRequest().get();
            return true;
        } catch (ClientException e) {
            return false;
        }
    }

    private String getCalendarName(String accountId) {
        List<Calendar> calendars = toList(microsoftCredentialService.getClient(accountId).me().calendars().buildRequest().get());
        return MicrosoftUtil.getName(calendars, CALENDAR_NAME);
    }

    private List<Calendar> toList(ICalendarCollectionPage page) {
        List<Calendar> calendars = new ArrayList<>();
        if (page != null && page.getCurrentPage() != null && page.getCurrentPage().size() > 0) {
            calendars.addAll(page.getCurrentPage());
            ICalendarCollectionRequestBuilder nextPage = page.getNextPage();
            if (nextPage != null) {
                calendars.addAll(toList(nextPage.buildRequest().get()));
            }
        }

        return calendars;
    }
}
