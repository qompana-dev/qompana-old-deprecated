package com.devqube.crmcalendarservice.microsoft.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class CalendarResponseDto {
    private String id;
    private String name;
    private String changeKey;
    private Boolean canShare;
    private Boolean canViewPrivateItems;
    private Boolean canEdit;
}
