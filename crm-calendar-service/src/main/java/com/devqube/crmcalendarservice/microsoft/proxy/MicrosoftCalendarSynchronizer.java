package com.devqube.crmcalendarservice.microsoft.proxy;

import com.devqube.crmcalendarservice.microsoft.calendar.MicrosoftCalendar;
import com.devqube.crmcalendarservice.microsoft.calendar.MicrosoftCalendarService;
import com.devqube.crmcalendarservice.microsoft.credential.MicrosoftCredentialService;
import com.devqube.crmcalendarservice.microsoft.event.MicrosoftEvent;
import com.devqube.crmcalendarservice.microsoft.event.MicrosoftEventService;
import com.devqube.crmcalendarservice.microsoft.exception.MicrosoftException;
import com.devqube.crmcalendarservice.microsoft.util.MicrosoftUtil;
import com.devqube.crmcalendarservice.task.TaskClient;
import com.devqube.crmcalendarservice.task.TaskDTO;
import com.microsoft.graph.models.extensions.Event;
import com.microsoft.graph.requests.extensions.IEventCollectionPage;
import com.microsoft.graph.requests.extensions.IEventCollectionRequestBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
@EnableAsync
public class MicrosoftCalendarSynchronizer {
    private final MicrosoftCredentialService microsoftCredentialService;
    private final MicrosoftCalendarConfiguration configuration;
    private final TaskClient taskClient;
    private final MicrosoftCalendarService microsoftCalendarService;
    private final MicrosoftEventService eventService;
    private final MicrosoftCalendarOperationExecutor microsoftCalendarOperationExecutor;

    public MicrosoftCalendarSynchronizer(MicrosoftCredentialService microsoftCredentialService, MicrosoftCalendarConfiguration configuration, TaskClient taskClient, MicrosoftCalendarService microsoftCalendarService, MicrosoftEventService eventService, MicrosoftCalendarOperationExecutor microsoftCalendarOperationExecutor) {
        this.microsoftCredentialService = microsoftCredentialService;
        this.configuration = configuration;
        this.taskClient = taskClient;
        this.microsoftCalendarService = microsoftCalendarService;
        this.eventService = eventService;
        this.microsoftCalendarOperationExecutor = microsoftCalendarOperationExecutor;
    }

    @Async
    public void addTasksToMsCalendar(String accountId) throws MicrosoftException {
        Optional<MicrosoftCalendar> calendarByAccountId = microsoftCalendarService.getCalendarByAccountId(accountId);
        if (calendarByAccountId.isEmpty()) {
            throw new MicrosoftException();
        }
        MicrosoftCalendar calendar = calendarByAccountId.get();
        List<TaskDTO> allTasksByOwner = taskClient.getAllByOwner(accountId);
        allTasksByOwner.forEach(c -> microsoftCalendarOperationExecutor.addEvent(calendar, c));
    }

    @Async
    public void synchronizeWithMsCalendar(String accountId) throws MicrosoftException {
        MicrosoftCalendar calendarByAccountId = microsoftCalendarService.getCalendarByAccountId(accountId).orElseThrow(MicrosoftException::new);
        IEventCollectionRequestBuilder eventsPage = microsoftCredentialService.getClient(accountId).me()
                .calendars(calendarByAccountId.getCalendarId()).events();

        updateTasks(getEventIdTaskMap(accountId, calendarByAccountId), eventsPage, accountId);
    }

    private void updateTasks(Map<String, TaskDTO> allTaskByEventId, IEventCollectionRequestBuilder page, String accountId) {
        IEventCollectionPage currentPage = page.buildRequest().get();
        currentPage.getCurrentPage().forEach(c -> {
            TaskDTO taskDTO = allTaskByEventId.get(c.id);
            if (taskDTO == null) {
                taskClient.save(false, MicrosoftUtil.createTaskDTOForSave(c, accountId));
            } else {
                if (isTaskModified(taskDTO, c)) {
                    taskClient.update(taskDTO.getId(), false,
                            MicrosoftUtil.createTaskDTOForUpdate(c, accountId, taskDTO));
                }
            }
        });
        if (currentPage.getNextPage() != null) {
            updateTasks(allTaskByEventId, currentPage.getNextPage(), accountId);
        }
    }

    private boolean isTaskModified(TaskDTO taskDTO, Event event) {
        if (!taskDTO.getSubject().equals(event.subject)) {
            return true;
        }
        if (!taskDTO.getPlace().equals(event.location.displayName)) {
            return true;
        }
        if (!MicrosoftUtil.areDatesEqual(taskDTO.getDateTimeFrom(), event.start.dateTime)) {
            return true;
        }
        if (!MicrosoftUtil.areDatesEqual(taskDTO.getDateTimeTo(), event.end.dateTime)) {
            return true;
        }
        return false;
    }

    private Map<String, TaskDTO> getEventIdTaskMap(String accountId, MicrosoftCalendar calendar) {
        List<MicrosoftEvent> events = eventService.findByCalendarId(calendar.getId());

        Map<String, String> taskMap = new HashMap<>();
        events.forEach(c -> taskMap.put(c.getTaskId(), c.getEventId()));

        Map<String, TaskDTO> allTaskByEventId = new HashMap<>();
        taskClient.getAllByOwner(accountId)
                .forEach(c -> {
                    String eventId = taskMap.get(c.getId());
                    if (eventId != null) {
                        allTaskByEventId.put(eventId, c);
                    }
                });

        return allTaskByEventId;
    }
}
