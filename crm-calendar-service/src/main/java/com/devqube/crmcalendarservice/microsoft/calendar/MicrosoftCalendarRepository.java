package com.devqube.crmcalendarservice.microsoft.calendar;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MicrosoftCalendarRepository extends JpaRepository<MicrosoftCalendar, Long> {
    boolean existsByAccountId(String accountId);
    Optional<MicrosoftCalendar> findByAccountId(String accountId);
    Optional<MicrosoftCalendar> findBySubscriptionId(String subscriptionId);
    void deleteByAccountId(String accountId);
    List<MicrosoftCalendar> findAllBySubscriptionExpirationTimeIsBefore(LocalDateTime now);
}
