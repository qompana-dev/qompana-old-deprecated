package com.devqube.crmcalendarservice.microsoft.calendar;

import com.devqube.crmcalendarservice.microsoft.exception.MicrosoftException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class MicrosoftCalendarService {
    private final MicrosoftCalendarRepository microsoftCalendarRepository;

    public MicrosoftCalendarService(MicrosoftCalendarRepository microsoftCalendarRepository) {
        this.microsoftCalendarRepository = microsoftCalendarRepository;
    }

    public void save(MicrosoftCalendar calendar) {
        microsoftCalendarRepository.save(calendar);
    }

    public MicrosoftCalendar getByAccountId(String accountId) throws MicrosoftException {
        return microsoftCalendarRepository.findByAccountId(accountId).orElseThrow(MicrosoftException::new);
    }
    @Transactional
    public void deleteCalendarByAccountId(String accountId) {
        microsoftCalendarRepository.deleteByAccountId(accountId);
    }
    public List<MicrosoftCalendar> getExpiredCalendarsSubscriptions() {
        return microsoftCalendarRepository.findAllBySubscriptionExpirationTimeIsBefore(LocalDateTime.now());
    }

    public Optional<MicrosoftCalendar> getCalendarByAccountId(String accountId) {
        return microsoftCalendarRepository.findByAccountId(accountId);
    }

    public Optional<MicrosoftCalendar> getCalendarBySubscriptionId(String subscriptionId) {
        return microsoftCalendarRepository.findBySubscriptionId(subscriptionId);
    }
}
