package com.devqube.crmcalendarservice.microsoft.credential;

import com.devqube.crmcalendarservice.encryption.AesEncryptionUtil;
import com.devqube.crmcalendarservice.encryption.EncryptionException;
import com.devqube.crmcalendarservice.microsoft.dto.MicrosoftTokenResponseDto;
import com.devqube.crmcalendarservice.microsoft.exception.MicrosoftException;
import com.devqube.crmcalendarservice.microsoft.proxy.MicrosoftCalendarConfiguration;
import com.devqube.crmcalendarservice.microsoft.util.MicrosoftUtil;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.requests.extensions.GraphServiceClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

@Slf4j
@Service
public class MicrosoftCredentialService {

    private final MicrosoftCredentialRepository microsoftCredentialRepository;
    private final MicrosoftCalendarConfiguration configuration;

    public MicrosoftCredentialService(MicrosoftCredentialRepository microsoftCredentialRepository, MicrosoftCalendarConfiguration configuration) {
        this.microsoftCredentialRepository = microsoftCredentialRepository;
        this.configuration = configuration;
    }

    String getAccessToken(String accountId) throws MicrosoftException, EncryptionException {
        if (!tokenExist(accountId)) {
            throw new MicrosoftException();
        }
        MicrosoftCredential byAccountId = microsoftCredentialRepository.findByAccountId(accountId);

        if (LocalDateTime.now().isAfter(byAccountId.getExpirationTime())) {
            byAccountId = this.refreshToken(byAccountId);
        }
        return AesEncryptionUtil.decrypt(byAccountId.getAccessToken());
    }

    @Transactional
    public void removeCredential(String accountId) {
        microsoftCredentialRepository.deleteByAccountId(accountId);
    }

    public boolean tokenExist(String accountId) {
        return microsoftCredentialRepository.existsByAccountId(accountId);
    }

    private MicrosoftCredential refreshToken(MicrosoftCredential byAccountId) throws MicrosoftException, EncryptionException {
        String tokenFromAuthCodeUrl = MicrosoftUtil.getTokenFromAuthCodeUrl(configuration.getAuthorizeUrl());
        MultiValueMap<String, String> tokenFromAuthCodeParams = MicrosoftUtil.getTokenFromRefreshTokenParams(
                AesEncryptionUtil.decrypt(byAccountId.getRefreshToken()), configuration.getId(), configuration.getSecret(), configuration.getRedirectURI());

        return sendTokenRequest(byAccountId.getAccountId(), tokenFromAuthCodeUrl, tokenFromAuthCodeParams);
    }

    public MicrosoftCredential obtainNewToken(String code, String accountId) throws MicrosoftException, EncryptionException {
        String tokenFromAuthCodeUrl = MicrosoftUtil.getTokenFromAuthCodeUrl(configuration.getAuthorizeUrl());
        MultiValueMap<String, String> tokenFromAuthCodeParams = MicrosoftUtil.getTokenFromAuthCodeParams(
                code, configuration.getId(), configuration.getSecret(), configuration.getRedirectURI());

        return sendTokenRequest(accountId, tokenFromAuthCodeUrl, tokenFromAuthCodeParams);
    }

    private MicrosoftCredential sendTokenRequest(String accountId, String url, MultiValueMap<String, String> params) throws EncryptionException, MicrosoftException {
        LocalDateTime now = LocalDateTime.now();
        ResponseEntity<MicrosoftTokenResponseDto> response = new RestTemplate().postForEntity(url, params, MicrosoftTokenResponseDto.class);

        if (!response.getStatusCode().equals(HttpStatus.OK) || response.getBody() == null || response.getBody().getError() != null) {
            log.error("microsoft communication error");
            throw new MicrosoftException();
        }

        LocalDateTime expirationTime = now.plusSeconds(response.getBody().getExpiresIn());

        MicrosoftCredential microsoftCredential = MicrosoftCredential.builder()
                .accountId(accountId)
                .accessToken(AesEncryptionUtil.encrypt(response.getBody().getAccessToken()))
                .expirationTimeSeconds(response.getBody().getExpiresIn())
                .expirationTime(expirationTime)
                .refreshToken(AesEncryptionUtil.encrypt(response.getBody().getRefreshToken()))
                .idToken(AesEncryptionUtil.encrypt(response.getBody().getIdToken())).build();


        MicrosoftCredential byAccountId = microsoftCredentialRepository.findByAccountId(accountId);
        if (byAccountId != null) {
            microsoftCredential.setId(byAccountId.getId());
        }
        return microsoftCredentialRepository.saveAndFlush(microsoftCredential);
    }

    public IGraphServiceClient getClient(String accountId) {
        return GraphServiceClient
                .builder()
                .authenticationProvider(new AuthenticationAdapter(accountId, this))
                .buildClient();
    }
}
