package com.devqube.crmcalendarservice.microsoft.credential;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MicrosoftCredentialRepository extends JpaRepository<MicrosoftCredential, Long> {
    MicrosoftCredential findByAccountId(String accountId);

    boolean existsByAccountId(String accountId);

    void deleteByAccountId(String accountId);
}
