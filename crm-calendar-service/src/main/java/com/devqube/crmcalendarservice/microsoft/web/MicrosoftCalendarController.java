package com.devqube.crmcalendarservice.microsoft.web;

import com.devqube.crmcalendarservice.encryption.EncryptionException;
import com.devqube.crmcalendarservice.microsoft.exception.MicrosoftException;
import com.devqube.crmcalendarservice.microsoft.proxy.MicrosoftCalendarProxy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;

@RestController
@Slf4j
public class MicrosoftCalendarController implements MicrosoftApi {
    @Autowired
    private MicrosoftCalendarProxy microsoftCalendarProxy;

    @Value("${frontend.redirect-uri}")
    private String redirectUri;

    @Override
    public RedirectView authenticateToMicrosoft(String accountId) {
        String redirectURL = microsoftCalendarProxy.generateRedirectURL(accountId);
        return new RedirectView(redirectURL);
    }

    @Override
    public RedirectView handleOauth2Callback(@Valid String code, @Valid String state) {
        try {
            this.microsoftCalendarProxy.handleOAuth2Callback(code, state);
            return new RedirectView(redirectUri + "?success=true");
        } catch (EncryptionException | MicrosoftException e) {
            e.printStackTrace();
            log.error("Problem with communication with Microsoft");
            return new RedirectView(redirectUri + "?success=false");
        }
    }

    @Override
    public ResponseEntity<String> microsoftNotification(@Valid String validationToken, @Valid String body) {
        microsoftCalendarProxy.handleMicrosoftNotification(body);
        return new ResponseEntity<>(validationToken, HttpStatus.OK);
    }
}
