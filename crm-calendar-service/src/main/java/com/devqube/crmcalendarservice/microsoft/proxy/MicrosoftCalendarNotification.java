package com.devqube.crmcalendarservice.microsoft.proxy;

import com.devqube.crmcalendarservice.microsoft.calendar.MicrosoftCalendar;
import com.devqube.crmcalendarservice.microsoft.calendar.MicrosoftCalendarService;
import com.devqube.crmcalendarservice.microsoft.credential.MicrosoftCredentialService;
import com.devqube.crmcalendarservice.microsoft.dto.subscription.SubscriptionEventDto;
import com.devqube.crmcalendarservice.microsoft.dto.subscription.SubscriptionResponseDto;
import com.devqube.crmcalendarservice.microsoft.event.MicrosoftEvent;
import com.devqube.crmcalendarservice.microsoft.event.MicrosoftEventService;
import com.devqube.crmcalendarservice.microsoft.exception.MicrosoftException;
import com.devqube.crmcalendarservice.microsoft.util.MicrosoftUtil;
import com.devqube.crmcalendarservice.task.TaskClient;
import com.devqube.crmcalendarservice.task.TaskDTO;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.models.extensions.Event;
import com.microsoft.graph.models.extensions.Subscription;
import com.microsoft.graph.requests.extensions.ISubscriptionCollectionPage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
@Service
public class MicrosoftCalendarNotification {
    private final MicrosoftCredentialService microsoftCredentialService;
    private final MicrosoftCalendarConfiguration configuration;
    private final MicrosoftCalendarService microsoftCalendarService;
    private final MicrosoftEventService microsoftEventService;
    private final TaskClient taskClient;

    public MicrosoftCalendarNotification(TaskClient taskClient, MicrosoftEventService microsoftEventService, MicrosoftCredentialService microsoftCredentialService, MicrosoftCalendarConfiguration configuration, MicrosoftCalendarService microsoftCalendarService) {
        this.taskClient = taskClient;
        this.microsoftEventService = microsoftEventService;
        this.microsoftCredentialService = microsoftCredentialService;
        this.configuration = configuration;
        this.microsoftCalendarService = microsoftCalendarService;
    }

    void updateNotificationSubscriptions() {
        microsoftCalendarService.getExpiredCalendarsSubscriptions().forEach(c -> {
            try {
                updateNotificationSubscription(c);
            } catch (MicrosoftException | ClientException e) {
                log.error(e.getMessage());
            }
        });
    }

    void handleMicrosoftNotification(String body) {
        log.info(body);
        if (body != null) {
            try {
                SubscriptionResponseDto subscriptionResponse = new ObjectMapper().readValue(body, SubscriptionResponseDto.class);
                subscriptionResponse.getValue().forEach(c -> {
                    try {
                        processEventNotification(c);
                    } catch (MicrosoftException e) {
                        log.error(e.getMessage());
                    }
                });
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
    }

    private void processEventNotification(@NotNull SubscriptionEventDto event) throws MicrosoftException {
        Optional<MicrosoftCalendar> calendarByAccountId = microsoftCalendarService.getCalendarBySubscriptionId(event.getSubscriptionId());
        if (calendarByAccountId.isEmpty()) {
            throw new MicrosoftException();
        }
        MicrosoftEvent microsoftEvent = microsoftEventService.getByEventId(event.getResourceData().getId());

        if (event.getChangeType().equals("deleted")) {
            deleteEvent(event.getResourceData().getId(), microsoftEvent);
        } else {
            TaskDTO taskDTO = null;
            try {
                Event msEvent = microsoftCredentialService.getClient(event.getClientState())
                        .me().calendars(calendarByAccountId.get().getCalendarId())
                        .events(event.getResourceData().getId()).buildRequest().get();
                taskDTO = MicrosoftUtil.createTaskDTO(msEvent, event.getClientState());
            }catch (ClientException e) {
                throw new MicrosoftException();
            }

            if (microsoftEvent == null) {
                saveEvent(event.getResourceData().getId(), calendarByAccountId.get(), taskDTO, event.getClientState());
            } else {
                editEvent(microsoftEvent, taskDTO);
            }
        }
    }

    private void editEvent(MicrosoftEvent changedEvent, TaskDTO taskDTO) {
        TaskDTO oldTask = taskClient.get(changedEvent.getTaskId());
        TaskDTO newTask = MicrosoftUtil.createTaskDTOForUpdate(taskDTO, oldTask);
        taskClient.update(changedEvent.getTaskId(), false, newTask);
    }

    private void saveEvent(String eventId, MicrosoftCalendar calendar, TaskDTO taskDTO, String accountId) {
        TaskDTO newTask = MicrosoftUtil.createTaskDTOForSave(taskDTO, accountId);
        TaskDTO savedTask = taskClient.save(false, newTask);
        microsoftEventService.save(new MicrosoftEvent(calendar, eventId, savedTask.getId()));
    }

    private void deleteEvent(String eventId, MicrosoftEvent changedEvent) {
        if (changedEvent != null) {
            try {
                microsoftEventService.deleteByEventId(eventId);
                taskClient.delete(changedEvent.getTaskId(), false);
            } catch (EntityNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateNotificationSubscription(MicrosoftCalendar calendar) throws MicrosoftException {
        Subscription subscription = new Subscription();
        LocalDateTime expirationTime = MicrosoftUtil.getExpirationTime(configuration.getExpirationMinutes());
        subscription.expirationDateTime = MicrosoftUtil.toCalendar(expirationTime);

        if (calendar.getSubscriptionId() == null) {
            createNotificationSubscription(calendar.getAccountId());
        } else {
            try {
                Subscription sub = microsoftCredentialService.getClient(calendar.getAccountId())
                        .subscriptions(calendar.getSubscriptionId()).buildRequest().patch(subscription);

                calendar.setSubscriptionId(sub.id);
                calendar.setSubscriptionExpirationTime(expirationTime.minusMinutes(60));

                microsoftCalendarService.save(calendar);
            } catch (ClientException e) {
                calendar.setSubscriptionId(null);
                microsoftCalendarService.save(calendar);
            }
        }
    }

    void createNotificationSubscription(String accountId) throws MicrosoftException {
        removeOldSubscriptions(accountId);

        MicrosoftCalendar calendar = microsoftCalendarService.getByAccountId(accountId);

        Subscription subscription = new Subscription();
        subscription.changeType = "updated,deleted";
        subscription.notificationUrl = configuration.getNotificationURI();
        subscription.resource = "me/calendars/" + calendar.getCalendarId() + "/events";
        LocalDateTime expirationTime = MicrosoftUtil.getExpirationTime(configuration.getExpirationMinutes());
        subscription.expirationDateTime = MicrosoftUtil.toCalendar(expirationTime);
        subscription.clientState = accountId;

        Subscription sub = microsoftCredentialService.getClient(accountId)
                .subscriptions().buildRequest().post(subscription);

        calendar.setSubscriptionId(sub.id);
        calendar.setSubscriptionExpirationTime(expirationTime.minusMinutes(60));

        microsoftCalendarService.save(calendar);
    }

    private void removeOldSubscriptions(String accountId) {
        try {
            ISubscriptionCollectionPage iSubscriptionCollectionPage = microsoftCredentialService.getClient(accountId)
                    .subscriptions().buildRequest().get();

            iSubscriptionCollectionPage.getCurrentPage().forEach(c ->
                    microsoftCredentialService.getClient(accountId).subscriptions(c.id).buildRequest().delete());
        } catch (ClientException e) {
            log.info(e.getMessage());
        }
    }
}
