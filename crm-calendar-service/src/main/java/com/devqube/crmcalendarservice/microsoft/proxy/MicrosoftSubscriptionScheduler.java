package com.devqube.crmcalendarservice.microsoft.proxy;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@EnableScheduling
public class MicrosoftSubscriptionScheduler {

    private final MicrosoftCalendarNotification microsoftCalendarNotification;


    public MicrosoftSubscriptionScheduler(MicrosoftCalendarNotification microsoftCalendarNotification) {
        this.microsoftCalendarNotification = microsoftCalendarNotification;
    }


    @Scheduled(fixedDelay = 900000) // 15 minutes
    public void updateSubscriptions() {
        microsoftCalendarNotification.updateNotificationSubscriptions();
    }
}
