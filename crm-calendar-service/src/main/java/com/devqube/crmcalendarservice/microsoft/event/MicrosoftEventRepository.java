package com.devqube.crmcalendarservice.microsoft.event;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MicrosoftEventRepository extends JpaRepository<MicrosoftEvent, Long> {
    Optional<MicrosoftEvent> findByCalendarIdAndTaskId(Long calendarId, String taskId);
    List<MicrosoftEvent> findAllByCalendarId(Long calendarId);
    void deleteAllByCalendarId(Long calendarId);
    MicrosoftEvent getByEventId(String eventId);
    void deleteByEventId(String eventId);
}
