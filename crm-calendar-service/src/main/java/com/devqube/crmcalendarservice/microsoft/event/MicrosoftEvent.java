package com.devqube.crmcalendarservice.microsoft.event;

import com.devqube.crmcalendarservice.microsoft.calendar.MicrosoftCalendar;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MicrosoftEvent {
    @Id
    @SequenceGenerator(name = "microsoft_event_seq", sequenceName = "microsoft_event_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "microsoft_event_seq")
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "calendar_id")
    private MicrosoftCalendar calendar;

    @NotNull
    @Column(unique = true)
    private String eventId;

    @NotNull
    private String taskId;

    public MicrosoftEvent(@NotNull MicrosoftCalendar calendar, @NotNull String eventId, @NotNull String taskId) {
        this.calendar = calendar;
        this.eventId = eventId;
        this.taskId = taskId;
    }
}
