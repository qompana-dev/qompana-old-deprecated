package com.devqube.crmcalendarservice.microsoft.credential;

import com.devqube.crmcalendarservice.encryption.EncryptionException;
import com.devqube.crmcalendarservice.microsoft.exception.MicrosoftException;
import com.microsoft.graph.authentication.IAuthenticationProvider;
import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.http.IHttpRequest;


public class AuthenticationAdapter implements IAuthenticationProvider {
    private String accountId;
    private MicrosoftCredentialService microsoftCredentialService;

    AuthenticationAdapter(String accountId, MicrosoftCredentialService microsoftCredentialService) {
        this.accountId = accountId;
        this.microsoftCredentialService = microsoftCredentialService;
    }

    @Override
    public void authenticateRequest(IHttpRequest request) {
        try {
            request.addHeader("Authorization", "Bearer "
                    + microsoftCredentialService.getAccessToken(this.accountId));
        } catch (MicrosoftException | ClientException | EncryptionException e) {
            e.printStackTrace();
        }
    }
}
