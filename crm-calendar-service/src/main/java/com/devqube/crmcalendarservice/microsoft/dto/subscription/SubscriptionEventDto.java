package com.devqube.crmcalendarservice.microsoft.dto.subscription;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionEventDto {
    private String subscriptionId;
    private String subscriptionExpirationDateTime;
    private String tenantId;
    private String changeType;
    private String resource;
    private EventResourceDataDto resourceData;
    private String clientState;
}
