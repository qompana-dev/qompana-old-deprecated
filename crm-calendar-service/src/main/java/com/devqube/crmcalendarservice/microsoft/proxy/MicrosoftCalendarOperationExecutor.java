package com.devqube.crmcalendarservice.microsoft.proxy;

import com.devqube.crmcalendarservice.microsoft.calendar.MicrosoftCalendar;
import com.devqube.crmcalendarservice.microsoft.calendar.MicrosoftCalendarService;
import com.devqube.crmcalendarservice.microsoft.credential.MicrosoftCredentialService;
import com.devqube.crmcalendarservice.microsoft.event.MicrosoftEvent;
import com.devqube.crmcalendarservice.microsoft.event.MicrosoftEventService;
import com.devqube.crmcalendarservice.microsoft.exception.MicrosoftException;
import com.devqube.crmcalendarservice.microsoft.util.MicrosoftUtil;
import com.devqube.crmcalendarservice.task.TaskDTO;
import com.microsoft.graph.models.extensions.Event;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import org.springframework.stereotype.Service;

@Service
public class MicrosoftCalendarOperationExecutor {
    private final MicrosoftCredentialService microsoftCredentialService;
    private final MicrosoftCalendarService microsoftCalendarService;
    private final MicrosoftEventService eventService;

    public MicrosoftCalendarOperationExecutor(MicrosoftCredentialService microsoftCredentialService, MicrosoftCalendarService microsoftCalendarService, MicrosoftEventService eventService) {
        this.microsoftCredentialService = microsoftCredentialService;
        this.microsoftCalendarService = microsoftCalendarService;
        this.eventService = eventService;
    }

    public void addEvent(TaskDTO taskDTO) throws MicrosoftException {
        MicrosoftCalendar calendar = microsoftCalendarService.getByAccountId(taskDTO.getOwner());
        addEvent(calendar, taskDTO);
    }

    public void addEvent(MicrosoftCalendar calendar, TaskDTO taskDTO) {
        Event newEvent = MicrosoftUtil.setTaskDtoToEvent(taskDTO, new Event());

        Event event = microsoftCredentialService.getClient(taskDTO.getOwner()).me()
                .calendars(calendar.getCalendarId())
                .events().buildRequest()
                .post(newEvent);
        eventService.save(new MicrosoftEvent(calendar, event.id, taskDTO.getId()));
    }

    public void modifyEvent(TaskDTO taskDTO) throws MicrosoftException {
        MicrosoftCalendar calendar = microsoftCalendarService.getByAccountId(taskDTO.getOwner());
        MicrosoftEvent event = eventService.findByCalendarIdAndTaskId(calendar.getId(), taskDTO.getId());

        IGraphServiceClient client = microsoftCredentialService.getClient(taskDTO.getOwner());
        Event newEvent = MicrosoftUtil.setTaskDtoToEvent(taskDTO, new Event());

        client.me()
                .calendars(calendar.getCalendarId())
                .events(event.getEventId()).buildRequest()
                .patch(newEvent);
    }

    public void deleteEvent(String accountId, String taskId) throws MicrosoftException {
        MicrosoftCalendar calendar = microsoftCalendarService.getByAccountId(accountId);
        MicrosoftEvent event = eventService.findByCalendarIdAndTaskId(calendar.getId(), taskId);

        microsoftCredentialService.getClient(accountId).me()
                .calendars(calendar.getCalendarId())
                .events(event.getEventId()).buildRequest()
                .delete();

        eventService.deleteById(event.getId());
    }
}
