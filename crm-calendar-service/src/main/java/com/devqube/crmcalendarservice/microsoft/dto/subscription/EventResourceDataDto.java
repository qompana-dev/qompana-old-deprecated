package com.devqube.crmcalendarservice.microsoft.dto.subscription;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventResourceDataDto {
    @JsonProperty("@odata.type")
    private String odataType;
    @JsonProperty("@odata.id")
    private String odataId;
    @JsonProperty("@odata.etag")
    private String odataEtag;
    private String id;
}
