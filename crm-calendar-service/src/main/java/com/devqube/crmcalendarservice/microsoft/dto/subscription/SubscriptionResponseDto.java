package com.devqube.crmcalendarservice.microsoft.dto.subscription;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionResponseDto {
    private Set<SubscriptionEventDto> value;
}
