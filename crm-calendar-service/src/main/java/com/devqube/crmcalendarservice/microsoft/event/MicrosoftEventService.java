package com.devqube.crmcalendarservice.microsoft.event;

import com.devqube.crmcalendarservice.microsoft.exception.MicrosoftException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MicrosoftEventService {

    private final MicrosoftEventRepository microsoftEventRepository;

    public MicrosoftEventService(MicrosoftEventRepository microsoftEventRepository) {
        this.microsoftEventRepository = microsoftEventRepository;
    }

    public void save(MicrosoftEvent event) {
        this.microsoftEventRepository.saveAndFlush(event);
    }

    public MicrosoftEvent findByCalendarIdAndTaskId(Long calendarId, String taskId) throws MicrosoftException {
        return microsoftEventRepository.findByCalendarIdAndTaskId(calendarId, taskId).orElseThrow(MicrosoftException::new);
    }
    public List<MicrosoftEvent> findByCalendarId(Long calendarId) {
        return microsoftEventRepository.findAllByCalendarId(calendarId);
    }

    public void deleteById(Long id) {
        microsoftEventRepository.deleteById(id);
    }

    @Transactional
    public void deleteByCalendarId(Long calendarId) {
        microsoftEventRepository.deleteAllByCalendarId(calendarId);
    }

    @Transactional
    public void deleteByEventId(String eventId) throws EntityNotFoundException {
        if (this.microsoftEventRepository.getByEventId(eventId) == null) {
            throw new EntityNotFoundException("Event with event id " + eventId + " not found");
        }
        this.microsoftEventRepository.deleteByEventId(eventId);
    }

    public MicrosoftEvent getByEventId(String eventId) {
        return this.microsoftEventRepository.getByEventId(eventId);
    }
}
