package com.devqube.crmcalendarservice.microsoft.util;

import com.devqube.crmcalendarservice.task.TaskDTO;
import com.microsoft.graph.models.extensions.Calendar;
import com.microsoft.graph.models.extensions.DateTimeTimeZone;
import com.microsoft.graph.models.extensions.Event;
import com.microsoft.graph.models.extensions.Location;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import java.util.stream.Collectors;

public class MicrosoftUtil {
    private static final String[] scopes = {
            "openid",
            "offline_access",
            "profile",
            "Calendars.Read",
            "Calendars.Read.Shared",
            "Calendars.ReadWrite",
            "Calendars.ReadWrite.Shared",
            "User.Read"
    };

    private static final DateTimeFormatter taskInputDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
    private static final DateTimeFormatter eventInputDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS", Locale.ENGLISH);

    private static String getScopes() {
        StringBuilder sb = new StringBuilder();
        for (String scope : scopes) {
            sb.append(scope).append(" ");
        }
        return sb.toString().trim();
    }

    public static String getLoginUrl(String authorizeUrl, String clientId, String redirectUrl, String state) {
        return getLoginUrl(authorizeUrl, clientId, redirectUrl, state, UUID.randomUUID());
    }

    private static String getLoginUrl(String authorizeUrl, String clientId, String redirectUrl, String state, UUID nonce) {
        UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(authorizeUrl + "/authorize");

        urlBuilder.queryParam("client_id", clientId);
        urlBuilder.queryParam("redirect_uri", redirectUrl);
        urlBuilder.queryParam("response_type", "code");
        urlBuilder.queryParam("scope", getScopes());
        urlBuilder.queryParam("prompt", "consent");
        urlBuilder.queryParam("state", state);
        urlBuilder.queryParam("nonce", nonce);
        urlBuilder.queryParam("response_mode", "form_post");

        return urlBuilder.toUriString();
    }

    public static String getTokenFromAuthCodeUrl(String authorizeUrl) {
        return authorizeUrl + "/token";
    }

    public static MultiValueMap<String, String> getTokenFromAuthCodeParams(String authCode, String clientId, String clientSecret, String redirectUrl) {
        MultiValueMap<String, String> map = getTokenParams(clientId, clientSecret, redirectUrl);
        map.add("code", authCode);
        map.add("grant_type", "authorization_code");
        return map;
    }

    public static MultiValueMap<String, String> getTokenFromRefreshTokenParams(String refreshToken, String clientId, String clientSecret, String redirectUrl) {
        MultiValueMap<String, String> map = getTokenParams(clientId, clientSecret, redirectUrl);
        map.add("refresh_token", refreshToken);
        map.add("grant_type", "refresh_token");
        return map;
    }

    private static MultiValueMap<String, String> getTokenParams(String clientId, String clientSecret, String redirectUrl) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", clientId);
        map.add("client_secret", clientSecret);
        map.add("redirect_uri", redirectUrl);
        return map;
    }

    public static String getName(List<Calendar> list, String defaultName) {
        if (list == null || defaultName == null || defaultName.equals("")) {
            return null;
        }
        return getName(list.stream().map(c -> c.name).collect(Collectors.toList()), defaultName, null);
    }

    private static String getName(List<String> list, String defaultName, Integer lastIndex) {
        String name = defaultName;
        if (lastIndex == null) {
            lastIndex = 0;
        } else {
            name += lastIndex;
        }
        return (list.contains(name)) ? getName(list, defaultName, lastIndex + 1) : name;
    }

    public static Event setTaskDtoToEvent(TaskDTO taskDTO, Event event) {
        event.subject = taskDTO.getSubject();

        Location location = new Location();
        location.displayName = taskDTO.getPlace();
        event.location = location;

        DateTimeTimeZone start = new DateTimeTimeZone();
        start.timeZone = "Etc/GMT";
        start.dateTime = taskDTO.getDateTimeFrom();
        event.start = start;

        DateTimeTimeZone end = new DateTimeTimeZone();
        end.timeZone = "Etc/GMT";
        end.dateTime = taskDTO.getDateTimeTo();
        event.end = end;
        return event;
    }

    public static LocalDateTime getExpirationTime(int expirationMinutes) {
        return LocalDateTime.now().plusMinutes(expirationMinutes);
    }

    public static java.util.Calendar toCalendar(LocalDateTime localDateTime) {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.clear();
        calendar.set(localDateTime.getYear(), (localDateTime.getMonthValue() - 1), localDateTime.getDayOfMonth(),
                localDateTime.getHour(), localDateTime.getMinute(), localDateTime.getSecond());
        calendar.setTimeZone(TimeZone.getDefault());
        return calendar;
    }

    public static TaskDTO createTaskDTOForSave(Event event, String accountId) {
        return createTaskDTOForSave(createTaskDTO(event, accountId), accountId);
    }

    public static TaskDTO createTaskDTOForUpdate(Event event, String accountId, TaskDTO oldTask) {
        return createTaskDTOForUpdate(createTaskDTO(event, accountId), oldTask);
    }

    public static TaskDTO createTaskDTOForSave(TaskDTO taskDTO, String accountId) {
        taskDTO.setCreator(accountId);
        return taskDTO;
    }

    public static TaskDTO createTaskDTOForUpdate(TaskDTO taskDTO, TaskDTO oldTask) {
        taskDTO.setCreator(oldTask.getCreator());
        taskDTO.setPriority(oldTask.getPriority());
        taskDTO.setActivityType(oldTask.getActivityType());
        taskDTO.setState(oldTask.getState());
        taskDTO.setDescription(oldTask.getDescription());
        taskDTO.setLatitude(oldTask.getLatitude());
        taskDTO.setLongitude(oldTask.getLongitude());
        taskDTO.setTimeZone(oldTask.getTimeZone());
        return taskDTO;
    }

    public static TaskDTO createTaskDTO(Event event, String accountId) {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setSubject(event.subject);
        taskDTO.setPlace(event.location.displayName);
        taskDTO.setDateTimeFrom(event.start.dateTime);
        taskDTO.setDateTimeTo(event.end.dateTime);
        taskDTO.setAllDayEvent(false);
        taskDTO.setOwner(accountId);
        if (event.originalStartTimeZone == null) {
            taskDTO.setTimeZone("UTC");
        } else {
            taskDTO.setTimeZone(event.originalStartTimeZone);
        }
        return taskDTO;
    }

    public static boolean areDatesEqual(String taskDate, String eventDate) {
        LocalDateTime taskDateParsed = LocalDateTime.parse(taskDate, taskInputDateFormatter);
        LocalDateTime eventDateParsed = LocalDateTime.parse(eventDate, eventInputDateFormatter);
        return taskDateParsed.isEqual(eventDateParsed);
    }
}
