package com.devqube.crmcalendarservice.microsoft.exception;

public class MicrosoftException extends Exception {
    public MicrosoftException() {
        super();
    }
    public MicrosoftException(String message) {
        super(message);
    }
}
