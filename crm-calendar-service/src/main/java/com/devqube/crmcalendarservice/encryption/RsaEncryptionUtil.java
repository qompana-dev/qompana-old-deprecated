package com.devqube.crmcalendarservice.encryption;

import com.google.api.client.util.Base64;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

@Slf4j
public class RsaEncryptionUtil {
    public static String encrypt(String text) throws EncryptionException {
        try {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, getPublicKey());
            return Base64.encodeBase64String(cipher.doFinal(text.getBytes(StandardCharsets.UTF_8)));
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new EncryptionException(e.getMessage(), e);
        }
    }

    public static String decrypt(String text) throws EncryptionException {
        try {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, getPrivateKey());
            return new String(cipher.doFinal(Base64.decodeBase64(text)), StandardCharsets.UTF_8);
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new EncryptionException(e.getMessage(), e);
        }
    }

    private static PrivateKey getPrivateKey() throws EncryptionException {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] privateKey = new ClassPathResource("rsa/privateKey").getInputStream().readAllBytes();
            return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateKey));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new EncryptionException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new EncryptionException("Private key file is missing! It should be in resources/rsa/privateKey");
        }
    }

    private static PublicKey getPublicKey() throws EncryptionException {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] publicKey =  new ClassPathResource("rsa/publicKey").getInputStream().readAllBytes();
            return keyFactory.generatePublic(new X509EncodedKeySpec(publicKey));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new EncryptionException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new EncryptionException("Public key file is missing! It should be in resources/rsa/publicKey");
        }
    }
}
