package com.devqube.crmcalendarservice.encryption;

import lombok.AllArgsConstructor;
import org.springframework.core.io.ClassPathResource;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Properties;

/**
 * Command:
 *
 * wsl openssl enc -aes-256-cbc -k secret -P -md sha1
 * -> salt=7300BCB3D8C39343
 * -> key=B7FCF9D5FD8F1E2ADABEE37FB922EBEB9585745791B3A04551C358CF0195FC94
 * -> iv =5B755C5670EA068F7A54307829A71245
 *
 * file crm-calendar-service\src\main\resources\aes\aes
 *
 * Content:
 *
 * aes.salt=7300BCB3D8C39343
 * aes.key=B7FCF9D5FD8F1E2ADABEE37FB922EBEB9585745791B3A04551C358CF0195FC94
 * aes.iv=5B755C5670EA068F7A54307829A71245
 * aes.secret=secret
 */
public class AesEncryptionUtil {

    public static String encrypt(String text) throws EncryptionException {
        try {
            AesProp prop = getProperties();
            Cipher cipher = getCipher();
            SecretKeySpec secretKeySpec = getSecretKeySpec(prop.salt, prop.key);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, prop.iv);
            return Base64.getEncoder().encodeToString(cipher.doFinal(text.getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            throw new EncryptionException(e.getMessage(), e);
        }
    }

    public static String decrypt(String strToDecrypt) throws EncryptionException {
        try {
            AesProp prop = getProperties();
            Cipher cipher = getCipher();
            SecretKeySpec secretKeySpec = getSecretKeySpec(prop.salt, prop.key);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, prop.iv);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)), StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new EncryptionException(e.getMessage(), e);
        }
    }

    private static AesProp getProperties() throws IOException {
        Properties prop = new Properties();
        prop.load(new ClassPathResource("aes/aes").getInputStream());
        String salt = prop.getProperty("aes.salt");
        String key = prop.getProperty("aes.key");
        String secret = prop.getProperty("aes.secret");
        byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        IvParameterSpec ivSpec = new IvParameterSpec(iv);
        return new AesProp(salt, key, secret, ivSpec);
    }

    private static SecretKeySpec getSecretKeySpec(String salt, String key) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(key.toCharArray(), salt.getBytes(), 65536, 256);
        SecretKey tmp = factory.generateSecret(spec);
        return new SecretKeySpec(tmp.getEncoded(), "AES");
    }

    private static Cipher getCipher() throws NoSuchPaddingException, NoSuchAlgorithmException {
        return Cipher.getInstance("AES/CBC/PKCS5Padding");
    }

    @AllArgsConstructor
    static class AesProp {
        String salt;
        String key;
        String secret;
        IvParameterSpec iv;
    }
}
