package com.devqube.crmcalendarservice.kafka;

import com.devqube.crmcalendarservice.encryption.EncryptionException;
import com.devqube.crmcalendarservice.microsoft.exception.MicrosoftException;
import com.devqube.crmcalendarservice.task.TaskDTO;

import javax.security.auth.login.CredentialNotFoundException;
import java.io.IOException;

public interface CalendarProxy {
    void handleOAuth2Callback(String code, String accountId) throws IOException, CredentialNotFoundException, EncryptionException, MicrosoftException;

    String generateRedirectURL(String accountId);

    boolean connected(String accountId) ;

    void addEvent(TaskDTO taskDTO) throws IOException, CredentialNotFoundException, MicrosoftException;

    void modifyEvent(TaskDTO taskDTO) throws IOException, CredentialNotFoundException, MicrosoftException;

    void deleteEvent(String accountId, String taskId) throws IOException, CredentialNotFoundException, MicrosoftException;
}
