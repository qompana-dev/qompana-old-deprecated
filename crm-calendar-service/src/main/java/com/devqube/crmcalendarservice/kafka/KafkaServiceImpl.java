package com.devqube.crmcalendarservice.kafka;

import com.devqube.crmcalendarservice.google.proxy.GoogleCalendarProxy;
import com.devqube.crmcalendarservice.microsoft.proxy.MicrosoftCalendarProxy;
import com.devqube.crmcalendarservice.microsoft.exception.MicrosoftException;
import com.devqube.crmcalendarservice.task.TaskDTO;
import com.devqube.crmshared.kafka.AbstractKafkaService;
import com.devqube.crmshared.kafka.ServiceKafkaAddr;
import com.devqube.crmshared.kafka.annotation.KafkaReceiver;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.security.auth.login.CredentialNotFoundException;
import java.io.IOException;
import java.util.Optional;

@Slf4j
@Service
public class KafkaServiceImpl extends AbstractKafkaService {

    private final GoogleCalendarProxy googleCalendarProxy;
    private final MicrosoftCalendarProxy microsoftCalendarProxy;

    public KafkaServiceImpl(GoogleCalendarProxy googleCalendarProxy, MicrosoftCalendarProxy microsoftCalendarProxy) {
        this.googleCalendarProxy = googleCalendarProxy;
        this.microsoftCalendarProxy = microsoftCalendarProxy;
    }

    @KafkaListener(topics = ServiceKafkaAddr.CRM_CALENDAR_SERVICE)
    public void processMessage(String content) {
        super.processMessage(content);
    }

    @KafkaReceiver(from = KafkaMsgType.CALENDAR_NEW_EVENT)
    public void addEventToCalendar(KafkaMessage kafkaMessage) throws CredentialNotFoundException, MicrosoftException, IOException {
        TaskDTO task = getTaskFromKafkaMessage(kafkaMessage);
        if (task.getOwner() != null) {
            CalendarProxy client = getProxy(task.getOwner());
            if (client != null) {
                client.addEvent(task);
            }
        }
    }

    @KafkaReceiver(from = KafkaMsgType.CALENDAR_MODIFIED_EVENT)
    public void sendModifiedEventToCalendar(KafkaMessage kafkaMessage) throws CredentialNotFoundException, MicrosoftException, IOException {
        TaskDTO task = getTaskFromKafkaMessage(kafkaMessage);
        if (task.getOwner() != null) {
            CalendarProxy client = getProxy(task.getOwner());
            if (client != null) {
                client.modifyEvent(task);
            }
        }
    }

    @KafkaReceiver(from = KafkaMsgType.CALENDAR_DELETE_EVENT)
    public void deleteEventFromCalendar(KafkaMessage kafkaMessage) throws CredentialNotFoundException, MicrosoftException, IOException {
        Optional<String> taskId = kafkaMessage.getValueByKey("taskId");
        Optional<String> accountId = kafkaMessage.getValueByKey("accountId");
        if (taskId.isPresent() && accountId.isPresent()) {
            CalendarProxy client = getProxy(accountId.get());
            if (client != null) {
                client.deleteEvent(accountId.get(), taskId.get());
            }
        }
    }

    @Override
    public void receiverNotFound(KafkaMessage kafkaMessage) {
        log.error("receiverNotFound" + kafkaMessage.getDestination().name());
    }

    private TaskDTO getTaskFromKafkaMessage(KafkaMessage kafkaMessage) {
        TaskDTO task = new TaskDTO();
        task.setId(kafkaMessage.getValueByKey("id").orElse(null));
        task.setSubject(kafkaMessage.getValueByKey("subject").orElse(""));
        task.setPlace(kafkaMessage.getValueByKey("place").orElse(""));
        task.setDateTimeFrom(kafkaMessage.getValueByKey("dateTimeFrom").orElse(""));
        task.setDateTimeTo(kafkaMessage.getValueByKey("dateTimeTo").orElse(""));
        task.setAllDayEvent(Boolean.valueOf(kafkaMessage.getValueByKey("allDayEvent").orElse("false")));
        task.setOwner(kafkaMessage.getValueByKey("owner").orElse(null));
        return task;
    }

    private CalendarProxy getProxy(String accountId) {
        if (googleCalendarProxy.connected(accountId)) {
            return googleCalendarProxy;
        }
        if (microsoftCalendarProxy.connected(accountId)) {
            return microsoftCalendarProxy;
        }
        return null;
    }
}
