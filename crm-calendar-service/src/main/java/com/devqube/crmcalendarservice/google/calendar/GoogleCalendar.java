package com.devqube.crmcalendarservice.google.calendar;


import com.devqube.crmcalendarservice.google.event.GoogleEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.TimeZone;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoogleCalendar {
    @Id
    @SequenceGenerator(name = "google_calendar_seq", sequenceName = "google_calendar_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "google_calendar_seq")
    private Long id;

    @NotNull
    @Column(unique = true)
    private String accountId;

    @NotNull
    @Column(unique = true)
    private String calendarId;

    private String pushNotificationChannelId;

    LocalDateTime channelExpiration;

    private String syncToken;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "calendar")
    private List<GoogleEvent> events;

    public GoogleCalendar(@NotNull String accountId, @NotNull String calendarId,
                          @NotNull String pushNotificationChannelId, @NotNull Long channelExpiration) {
        this.accountId = accountId;
        this.calendarId = calendarId;
        this.pushNotificationChannelId = pushNotificationChannelId;
        this.channelExpiration = LocalDateTime.ofInstant(Instant.ofEpochMilli(channelExpiration),
                TimeZone.getDefault().toZoneId());
    }
}
