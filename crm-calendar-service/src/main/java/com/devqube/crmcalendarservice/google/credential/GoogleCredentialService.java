package com.devqube.crmcalendarservice.google.credential;

import com.devqube.crmcalendarservice.encryption.EncryptionException;
import com.devqube.crmcalendarservice.encryption.RsaEncryptionUtil;
import com.google.api.client.auth.oauth2.StoredCredential;
import com.google.api.client.util.store.DataStore;
import com.google.api.client.util.store.DataStoreFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GoogleCredentialService implements DataStore<StoredCredential> {

    private final GoogleCredentialRepository repository;

    public GoogleCredentialService(GoogleCredentialRepository repository) {
        this.repository = repository;
    }

    @Override
    public DataStoreFactory getDataStoreFactory() {
        return null;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public int size() {
        return (int) repository.count();
    }

    @Override
    public boolean isEmpty() {
        return repository.count() == 0;
    }

    @Override
    public boolean containsKey(String s) {
        return repository.existsByAccountId(s);
    }

    @Override
    public boolean containsValue(StoredCredential storedCredential) {
        try {
            return repository.existsByAccessTokenAndExpirationTimeMillisecondsAndRefreshToken(RsaEncryptionUtil.encrypt(storedCredential.getAccessToken()),
                    storedCredential.getExpirationTimeMilliseconds(), RsaEncryptionUtil.encrypt(storedCredential.getRefreshToken()));
        } catch (EncryptionException e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    @Override
    public Set<String> keySet() {
        return repository.findAll()
                .stream()
                .map(GoogleCredential::getAccountId)
                .collect(Collectors.toSet());
    }

    @Override
    public Collection<StoredCredential> values() {
        return repository.findAll()
                .stream()
                .map(this::mapToStoredCredential)
                .collect(Collectors.toSet());
    }

    @Override
    public StoredCredential get(String s) {
        GoogleCredential credentials = repository.findByAccountId(s);
        return credentials != null ? mapToStoredCredential(credentials) : null;
    }

    @Override
    public DataStore<StoredCredential> set(String key, StoredCredential storedCredential) {
        GoogleCredential credential = repository.existsByAccountId(key) ? repository.findByAccountId(key) : new GoogleCredential();
        try {
            credential.setAccountId(key);
            credential.setAccessToken(RsaEncryptionUtil.encrypt(storedCredential.getAccessToken()));
            credential.setRefreshToken(RsaEncryptionUtil.encrypt(storedCredential.getRefreshToken()));
            credential.setExpirationTimeMilliseconds(storedCredential.getExpirationTimeMilliseconds());
            repository.save(credential);
        } catch (EncryptionException e) {
            log.error(e.getMessage(), e);
        }
        return this;
    }

    @Override
    public DataStore<StoredCredential> clear() {
        repository.deleteAll();
        return this;
    }

    @Override
    public DataStore<StoredCredential> delete(String key) {
        repository.deleteByAccountId(key);
        return this;
    }


    public boolean tokenExist(String accountId) {
        return repository.existsByAccountId(accountId);
    }

    private StoredCredential mapToStoredCredential(GoogleCredential googleCredential) {
        try {
            return new StoredCredential()
                    .setAccessToken(RsaEncryptionUtil.decrypt(googleCredential.getAccessToken()))
                    .setRefreshToken(RsaEncryptionUtil.decrypt(googleCredential.getRefreshToken()))
                    .setExpirationTimeMilliseconds(googleCredential.getExpirationTimeMilliseconds());
        } catch (EncryptionException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }
}
