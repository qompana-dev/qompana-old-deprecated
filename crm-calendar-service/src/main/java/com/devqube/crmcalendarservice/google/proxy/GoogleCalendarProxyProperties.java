package com.devqube.crmcalendarservice.google.proxy;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "google.proxy")
@Setter
@Getter
public class GoogleCalendarProxyProperties {
    private String clientId;
    private String clientSecret;
    private String redirectUri;
    private String notificationUri;
    private String projectName;
    private String calendarName;
    private String calendarDescription;
}
