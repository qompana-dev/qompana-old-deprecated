package com.devqube.crmcalendarservice.google.credential;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GoogleCredentialRepository extends JpaRepository<GoogleCredential, Long> {
    GoogleCredential findByAccountId(String accountId);
    boolean existsByAccountId(String accountId);
    boolean existsByAccessTokenAndExpirationTimeMillisecondsAndRefreshToken(String accessToken, Long expirationTimeMilliseconds, String refreshToken);
    void deleteByAccountId(String accountId);
}
