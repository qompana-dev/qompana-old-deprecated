package com.devqube.crmcalendarservice.google.calendar;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class GoogleCalendarService {

    private final GoogleCalendarRepository googleCalendarRepository;

    public GoogleCalendarService(GoogleCalendarRepository googleCalendarRepository) {
        this.googleCalendarRepository = googleCalendarRepository;
    }

    public void saveIfNotExists(GoogleCalendar calendar) {
        if(!googleCalendarRepository.existsByAccountId(calendar.getAccountId())) {
            googleCalendarRepository.save(calendar);
        }
    }

    public void update(GoogleCalendar calendar) {
        googleCalendarRepository.save(calendar);
    }

    public GoogleCalendar getByAccountId(String accountId) {
        return googleCalendarRepository.findByAccountId(accountId);
    }

    public GoogleCalendar getByCalendarId(String calendarId) {
        return googleCalendarRepository.findByCalendarId(calendarId);
    }

    public List<GoogleCalendar> findAllByChannelExpirationIsBefore(LocalDateTime time) {
        return googleCalendarRepository.findAllByChannelExpirationIsBefore(time);
    }
}
