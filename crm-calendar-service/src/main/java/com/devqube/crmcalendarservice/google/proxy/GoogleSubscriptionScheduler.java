package com.devqube.crmcalendarservice.google.proxy;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@EnableScheduling
public class GoogleSubscriptionScheduler {

    private final GoogleCalendarProxy googleCalendarProxy;

    public GoogleSubscriptionScheduler(GoogleCalendarProxy googleCalendarProxy) {
        this.googleCalendarProxy = googleCalendarProxy;
    }

    @Scheduled(fixedDelay = 900000) // 15 minutes
    public void updateSubscriptions() {
        googleCalendarProxy.updateNotificationSubscriptions();
    }
}
