package com.devqube.crmcalendarservice.google.event;

import com.devqube.crmshared.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GoogleEventService {

    private final GoogleEventRepository googleEventRepository;

    public GoogleEventService(GoogleEventRepository googleEventRepository) {
        this.googleEventRepository = googleEventRepository;
    }

    public void save(GoogleEvent event) {
        this.googleEventRepository.save(event);
    }

    public GoogleEvent findByCalendarIdAndTaskId(Long calendarId, String taskId) {
        return googleEventRepository.findByCalendarIdAndTaskId(calendarId, taskId);
    }

    public void deleteById(Long id) {
        googleEventRepository.deleteById(id);
    }

    @Transactional
    public void deleteByEventId(String eventId) throws EntityNotFoundException {
        if (this.googleEventRepository.getByEventId(eventId) == null) {
            throw new EntityNotFoundException("Event with event id " + eventId + " not found");
        }
        this.googleEventRepository.deleteByEventId(eventId);
    }

    public GoogleEvent getByEventId(String eventId) {
        return this.googleEventRepository.getByEventId(eventId);
    }
}
