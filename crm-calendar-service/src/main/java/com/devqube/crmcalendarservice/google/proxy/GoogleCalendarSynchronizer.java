package com.devqube.crmcalendarservice.google.proxy;

import com.devqube.crmcalendarservice.google.calendar.GoogleCalendar;
import com.devqube.crmcalendarservice.google.calendar.GoogleCalendarService;
import com.devqube.crmcalendarservice.google.event.GoogleEvent;
import com.devqube.crmcalendarservice.google.event.GoogleEventService;
import com.devqube.crmcalendarservice.task.TaskClient;
import com.devqube.crmcalendarservice.task.TaskDTO;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.utils.DateUtils;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Service
@Slf4j
public class GoogleCalendarSynchronizer {

    private final TaskClient taskClient;
    private final GoogleCalendarService calendarService;
    private final GoogleEventService eventService;

    public GoogleCalendarSynchronizer(TaskClient taskClient, GoogleCalendarService calendarService, GoogleEventService eventService) {
        this.taskClient = taskClient;
        this.calendarService = calendarService;
        this.eventService = eventService;
    }

    void saveSyncToken(Calendar service, GoogleCalendar calendar) throws IOException {
        String nextSyncToken = service.events().list(calendar.getCalendarId()).execute().getNextSyncToken();
        calendar.setSyncToken(nextSyncToken);
        calendarService.update(calendar);
    }

    void synchronizeEvents(Calendar service, GoogleCalendar calendar) throws IOException {
        Calendar.Events.List request = createRequest(service, calendar);
        Events events = getEventsToSynchronize(calendar, request);
        synchronizeEvents(events, calendar);
        calendar.setSyncToken(events.getNextSyncToken());
        calendarService.update(calendar);
    }

    private Events getEventsToSynchronize(GoogleCalendar calendar, Calendar.Events.List request) throws IOException {
        Events events;
        try {
            events = request.execute();
        } catch (GoogleJsonResponseException e) {
            if (e.getStatusCode() == 410) {
                events = deleteInvalidSyncTokenAndExecuteFullSync(calendar, request);
            } else {
                throw e;
            }
        }
        return events;
    }

    private Events deleteInvalidSyncTokenAndExecuteFullSync(GoogleCalendar calendar, Calendar.Events.List request) throws IOException {
        Events events;
        calendar.setSyncToken(null);
        calendarService.update(calendar);
        request.setSyncToken(null);
        events = request.execute();
        return events;
    }

    private void synchronizeEvent(Event event, GoogleCalendar calendar) {
        GoogleEvent changedEvent = eventService.getByEventId(event.getId());
        if (event.getStatus().equals("cancelled")) {
            deleteEvent(event, changedEvent);
        } else {
            TaskDTO taskDTO = createTaskDTO(event, calendar);
            if (changedEvent == null) {
                saveEvent(event, calendar, taskDTO);
            } else {
                updateEvent(changedEvent, taskDTO);
            }
        }
    }

    private void updateEvent(GoogleEvent changedEvent, TaskDTO taskDTO) {
        TaskDTO oldTask = taskClient.get(changedEvent.getTaskId());
        taskDTO.setCreator(oldTask.getCreator());
        taskDTO.setPriority(oldTask.getPriority());
        taskDTO.setActivityType(oldTask.getActivityType());
        taskDTO.setState(oldTask.getState());
        taskClient.update(changedEvent.getTaskId(), false, taskDTO);
    }

    private void saveEvent(Event event, GoogleCalendar calendar, TaskDTO taskDTO) {
        taskDTO.setCreator(calendar.getAccountId());
        TaskDTO savedTask = taskClient.save(false, taskDTO);
        eventService.save(new GoogleEvent(calendar, event.getId(), savedTask.getId()));
    }

    private TaskDTO createTaskDTO(Event event, GoogleCalendar calendar) {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setSubject(event.getSummary());
        taskDTO.setPlace(event.getLocation());
        if (event.getStart().getDateTime() != null) {
            taskDTO.setAllDayEvent(false);
            taskDTO.setDateTimeFrom(convertDateTimeToString(event.getStart()));
            taskDTO.setDateTimeTo(convertDateTimeToString(event.getEnd()));
        } else {
            taskDTO.setAllDayEvent(true);
            taskDTO.setDateTimeFrom(convertDateToString(event.getStart()));
            taskDTO.setDateTimeTo(convertDateToString(event.getEnd()));
        }
        taskDTO.setOwner(calendar.getAccountId());
        return taskDTO;
    }

    private void deleteEvent(Event event, GoogleEvent changedEvent) {
        if (changedEvent != null) {
            try {
                eventService.deleteByEventId(event.getId());
                taskClient.delete(changedEvent.getTaskId(), false);
            } catch (EntityNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private String convertDateTimeToString(EventDateTime dateTime) {
        return dateTime.getDateTime().toString().replaceAll("Z", "");
    }

    private String convertDateToString(EventDateTime dateTime) {
        return dateTime.getDate().toStringRfc3339().concat("T00:00:00.000");
    }

    private void synchronizeEvents(Events events, GoogleCalendar calendar) {
        List<Event> items = events.getItems();
        if (items.isEmpty()) {
            log.info("No new events to sync.");
        } else {
            for (Event event : items) {
                synchronizeEvent(event, calendar);
            }
        }
    }

    private Calendar.Events.List createRequest(Calendar service, GoogleCalendar calendar) throws IOException {
        Calendar.Events.List request = service.events().list(calendar.getCalendarId());
        if (calendar.getSyncToken() == null) {
            Date oneMonthAgo = DateUtils.shiftFromNow(java.util.Calendar.MONTH, -6);
            request.setTimeMin(new DateTime(oneMonthAgo, TimeZone.getTimeZone("UTC")));
        } else {
            request.setSyncToken(calendar.getSyncToken());
        }
        return request;
    }
}
