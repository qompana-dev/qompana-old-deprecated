package com.devqube.crmcalendarservice.google.web;

import com.devqube.crmcalendarservice.google.proxy.GoogleCalendarProxy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import javax.security.auth.login.CredentialNotFoundException;
import java.io.IOException;

@RestController
@Slf4j
public class GoogleController implements GoogleApi {
    private final GoogleCalendarProxy proxy;

    @Value("${frontend.redirect-uri}")
    private String redirectUri;

    public GoogleController(GoogleCalendarProxy proxy) {
        this.proxy = proxy;
    }

    @Override
    public RedirectView authenticateToGoogle(String accountId) {
        return new RedirectView(proxy.generateRedirectURL(accountId));
    }

    @Override
    public RedirectView handleOauth2Callback(String code, String accountId) {
        try {
            proxy.handleOAuth2Callback(code, accountId);
            return new RedirectView(redirectUri + "?success=true");
        } catch (IOException | CredentialNotFoundException e) {
            log.error(e.getMessage(), e);
            return new RedirectView(redirectUri + "?success=false");
        }
    }

    @Override
    public ResponseEntity<Void> handlePushNotification(String resourceState, String uri) {
        String calendarId = uri.substring(uri.indexOf("/calendars/") + "/calendars/".length(), uri.indexOf("/events"));
        try {
            if (resourceState.equals("sync")) {
                proxy.saveSyncToken(calendarId);
            } else {
                proxy.synchronizeEvents(calendarId);
            }
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IOException | CredentialNotFoundException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
