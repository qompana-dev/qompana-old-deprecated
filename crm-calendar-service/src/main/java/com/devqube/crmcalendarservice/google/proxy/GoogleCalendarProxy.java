package com.devqube.crmcalendarservice.google.proxy;

import com.devqube.crmcalendarservice.google.calendar.GoogleCalendar;
import com.devqube.crmcalendarservice.google.calendar.GoogleCalendarService;
import com.devqube.crmcalendarservice.google.credential.GoogleCredentialService;
import com.devqube.crmcalendarservice.kafka.CalendarProxy;
import com.devqube.crmcalendarservice.task.TaskDTO;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.security.auth.login.CredentialNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;

@Service
@Slf4j
public class GoogleCalendarProxy implements CalendarProxy {
    private final GoogleCredentialService credentialStore;
    private final GoogleCalendarProxyProperties properties;
    private final GoogleCalendarOperationExecutor operationExecutor;
    private final GoogleCalendarSynchronizer synchronizer;
    private final GoogleCalendarService calendarService;

    private HttpTransport httpTransport;
    private GoogleAuthorizationCodeFlow authorizationCodeFlow;

    public GoogleCalendarProxy(GoogleCredentialService credentialStore, GoogleCalendarProxyProperties properties, GoogleCalendarOperationExecutor operationExecutor, GoogleCalendarSynchronizer synchronizer, GoogleCalendarService calendarService) {
        this.credentialStore = credentialStore;
        this.properties = properties;
        this.operationExecutor = operationExecutor;
        this.synchronizer = synchronizer;
        this.calendarService = calendarService;
    }

    @Override
    public void handleOAuth2Callback(String code, String accountId) throws IOException, CredentialNotFoundException {
        operationExecutor.saveCredentials(authorizationCodeFlow, code, accountId);
        Calendar serviceClient = createServiceClient(accountId);
        operationExecutor.createCalendar(serviceClient, accountId);
        operationExecutor.addExistingEventsToCalendar(serviceClient, accountId);
    }

    @Override
    public String generateRedirectURL(String accountId) {
        return operationExecutor.generateRedirectURL(authorizationCodeFlow, accountId);
    }

    @Override
    public boolean connected(String accountId) {
        return credentialStore.tokenExist(accountId);
    }

    @Override
    public void modifyEvent(TaskDTO taskDTO) throws IOException, CredentialNotFoundException {
        Calendar serviceClient = createServiceClient(taskDTO.getOwner());
        operationExecutor.modifyEvent(serviceClient, taskDTO.getOwner(), taskDTO);
    }

    @Override
    public void addEvent(TaskDTO taskDTO) throws IOException, CredentialNotFoundException {
        Calendar serviceClient = createServiceClient(taskDTO.getOwner());
        operationExecutor.addEvent(serviceClient, taskDTO.getOwner(), taskDTO);
    }

    @Override
    public void deleteEvent(String accountId, String taskId) throws IOException, CredentialNotFoundException {
        Calendar serviceClient = createServiceClient(accountId);
        operationExecutor.deleteEvent(serviceClient, accountId, taskId);
    }

    public void saveSyncToken(String calendarId) throws IOException, CredentialNotFoundException {
        GoogleCalendar calendar = this.calendarService.getByCalendarId(calendarId);
        if (calendar != null) {
            Calendar serviceClient = createServiceClient(calendar.getAccountId());
            synchronizer.saveSyncToken(serviceClient, calendar);
        }
    }

    public void synchronizeEvents(String calendarId) throws IOException, CredentialNotFoundException {
        GoogleCalendar calendar = this.calendarService.getByCalendarId(calendarId);
        if (calendar != null) {
            Calendar serviceClient = createServiceClient(calendar.getAccountId());
            synchronizer.synchronizeEvents(serviceClient, calendar);
        }
    }

    void updateNotificationSubscriptions() {
        calendarService.findAllByChannelExpirationIsBefore(LocalDateTime.now()
                .plus(30, ChronoUnit.MINUTES)).forEach(this::updateNotificationSubscription);
    }

    private void updateNotificationSubscription(GoogleCalendar calendar) {
        try {
            Calendar service = createServiceClient(calendar.getAccountId());
            operationExecutor.updateNotificationSubscription(service, calendar);
        } catch (IOException | CredentialNotFoundException e) {
            log.error("Couldn't update push notification channel subscription");
        }
    }

    private Calendar createServiceClient(String accountId) throws IOException, CredentialNotFoundException {
        return new Calendar.Builder(httpTransport, JacksonFactory.getDefaultInstance(),
                getCredentialsOrThrowExceptionIfNotPresent(accountId))
                .setApplicationName(properties.getProjectName())
                .build();
    }

    private Credential getCredentialsOrThrowExceptionIfNotPresent(String accountId) throws IOException, CredentialNotFoundException {
        Credential credentials = authorizationCodeFlow.loadCredential(accountId);
        if (credentials == null) {
            throw new CredentialNotFoundException("Account " + accountId + " is not authenticated to Google.");
        }
        return credentials;
    }

    @PostConstruct
    private void initialize() {
        try {
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            authorizationCodeFlow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JacksonFactory.getDefaultInstance(),
                    new GoogleClientSecrets()
                            .setWeb(
                                    new GoogleClientSecrets.Details()
                                            .setClientId(properties.getClientId())
                                            .setClientSecret(properties.getClientSecret())
                            ), Collections.singleton(CalendarScopes.CALENDAR))
                    .setCredentialDataStore(credentialStore)
                    .setApprovalPrompt("force")
                    .setAccessType("offline")
                    .build();
        } catch (GeneralSecurityException | IOException e) {
            log.error(e.getMessage(), e);
        }
    }
}
