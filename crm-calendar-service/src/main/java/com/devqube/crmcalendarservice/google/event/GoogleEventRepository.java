package com.devqube.crmcalendarservice.google.event;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GoogleEventRepository extends JpaRepository<GoogleEvent, Long> {
    GoogleEvent findByCalendarIdAndTaskId(Long calendarId, String taskId);
    void deleteByEventId(String eventId);
    GoogleEvent getByEventId(String eventId);
}
