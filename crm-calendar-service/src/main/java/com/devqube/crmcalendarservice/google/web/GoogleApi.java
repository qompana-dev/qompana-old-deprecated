package com.devqube.crmcalendarservice.google.web;

import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Validated
@Api(value = "google")
public interface GoogleApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Redirect user to Google Authentication", nickname = "authenticateToGoogle", notes = "Method used to redirect user to Google Authentication")
    @ApiResponses(value = {
            @ApiResponse(code = 302, message = "redirect")})
    @RequestMapping(value = "/google/auth",
            method = RequestMethod.GET)
    default RedirectView authenticateToGoogle(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "accountId", required = true) String accountId) {
        return null;
    }

    @ApiOperation(value = "Google callback", nickname = "handleOauth2Callback", notes = "Google callback")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "saved credentials"),
            @ApiResponse(code = 503, message = "Google GoogleCalendar not available")})
    @RequestMapping(value = "/google/auth/callback",
            method = RequestMethod.GET)
    default RedirectView handleOauth2Callback(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "code", required = true) String code, @NotNull @ApiParam(required = true) @Valid @RequestParam(value = "state") String state) {
        return null;
    }

    @ApiOperation(value = "Notification from google", nickname = "handlePushNotification", notes = "Method used by Google to notified calendar service about changes")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "ok")})
    @RequestMapping(value = "/google/notification",
            method = RequestMethod.POST)
    default ResponseEntity<Void> handlePushNotification(@ApiParam(value = "", required = true) @RequestHeader(value = "X-Goog-Resource-State", required = true) String xGoogResourceState, @ApiParam(required = true) @RequestHeader(value = "X-Goog-Resource-URI") String xGoogResourceURI) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }
}
