package com.devqube.crmcalendarservice.google.proxy;

import com.devqube.crmcalendarservice.google.calendar.GoogleCalendar;
import com.devqube.crmcalendarservice.google.calendar.GoogleCalendarService;
import com.devqube.crmcalendarservice.google.event.GoogleEvent;
import com.devqube.crmcalendarservice.google.event.GoogleEventService;
import com.devqube.crmcalendarservice.task.TaskClient;
import com.devqube.crmcalendarservice.task.TaskDTO;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Channel;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

@Service
class GoogleCalendarOperationExecutor {
    private final GoogleCalendarService calendarService;
    private final GoogleCalendarProxyProperties properties;
    private final GoogleEventService eventService;
    private final TaskClient taskClient;


    public GoogleCalendarOperationExecutor(GoogleCalendarService calendarService, GoogleCalendarProxyProperties properties, GoogleEventService eventService,
                                           TaskClient taskClient) {
        this.calendarService = calendarService;
        this.properties = properties;
        this.eventService = eventService;
        this.taskClient = taskClient;
    }

    void saveCredentials(GoogleAuthorizationCodeFlow authorizationCodeFlow, String code, String accountId) throws IOException {
        GoogleTokenResponse token = authorizationCodeFlow.newTokenRequest(code).setRedirectUri(properties.getRedirectUri()).execute();
        authorizationCodeFlow.createAndStoreCredential(token, accountId);
    }

    void createCalendar(Calendar service, String accountId) throws IOException {
        String calendarId = createCRMCalendarInGoogle(service);
        Channel channel = service.events().watch(calendarId, createChannel()).execute();
        this.calendarService.saveIfNotExists(new GoogleCalendar(accountId, calendarId, channel.getId(), channel.getExpiration()));
    }

    void addExistingEventsToCalendar(Calendar serviceClient, String accountId) throws IOException {
        List<TaskDTO> allTasks = taskClient.getAllByOwner(accountId);
        for (TaskDTO task : allTasks) {
            addEvent(serviceClient, accountId, task);
        }
    }

    String generateRedirectURL(GoogleAuthorizationCodeFlow authorizationCodeFlow, String accountId) {
        return authorizationCodeFlow
                .newAuthorizationUrl()
                .setRedirectUri(properties.getRedirectUri())
                .setState(accountId)
                .build();
    }

    void addEvent(Calendar service, String accountId, TaskDTO taskDTO) throws IOException {
        Event event = new Event();
        setEventPropertiesBasedOnTask(taskDTO, event);
        GoogleCalendar calendar = calendarService.getByAccountId(accountId);
        event = service.events().insert(calendar.getCalendarId(), event).execute();
        eventService.save(new GoogleEvent(calendar, event.getId(), taskDTO.getId()));
    }

    void modifyEvent(Calendar service, String accountId, TaskDTO taskDTO) throws IOException {
        GoogleCalendar calendar = calendarService.getByAccountId(accountId);
        GoogleEvent googleEvent = eventService.findByCalendarIdAndTaskId(calendar.getId(), taskDTO.getId());
        Event event = service.events().get(calendar.getCalendarId(), googleEvent.getEventId()).execute();
        setEventPropertiesBasedOnTask(taskDTO, event);
        service.events().update(calendar.getCalendarId(), event.getId(), event).execute();
    }

    void deleteEvent(Calendar service, String accountId, String taskId) throws IOException {
        GoogleCalendar calendar = calendarService.getByAccountId(accountId);
        GoogleEvent event = eventService.findByCalendarIdAndTaskId(calendar.getId(), taskId);
        service.events().delete(calendar.getCalendarId(), event.getEventId()).execute();
        eventService.deleteById(event.getId());
    }

    @Transactional
    void updateNotificationSubscription(Calendar service, GoogleCalendar calendar) throws IOException {
        Channel channel = service.events().watch(calendar.getCalendarId(), createChannel()).execute();
        GoogleCalendar toUpdate = this.calendarService.getByCalendarId(calendar.getCalendarId());
        toUpdate.setPushNotificationChannelId(channel.getId());
        toUpdate.setChannelExpiration(LocalDateTime.ofInstant(Instant.ofEpochMilli(channel.getExpiration()),
                TimeZone.getDefault().toZoneId()));
        calendarService.update(toUpdate);
    }

    private void setEventPropertiesBasedOnTask(TaskDTO taskDTO, Event event) {
        event.setSummary(taskDTO.getSubject()).setLocation(taskDTO.getPlace());
        if (taskDTO.getAllDayEvent()) {
            event.setStart(new EventDateTime().setDate(new DateTime(parseDate(taskDTO.getDateTimeFrom()))));
            event.setEnd(new EventDateTime().setDate(new DateTime(parseDate(taskDTO.getDateTimeTo()))));
        } else {
            event.setStart(new EventDateTime().setDateTime(new DateTime(taskDTO.getDateTimeFrom())));
            event.setEnd(new EventDateTime().setDateTime(new DateTime(taskDTO.getDateTimeTo())));
        }
    }

    private String parseDate(String dateTimeFrom) {
        return dateTimeFrom.substring(0, dateTimeFrom.indexOf("T"));
    }

    private String createCRMCalendarInGoogle(Calendar service) throws IOException {
        com.google.api.services.calendar.model.Calendar calendar = new com.google.api.services.calendar.model.Calendar();
        calendar.setSummary(properties.getCalendarName());
        calendar.setDescription(properties.getCalendarDescription());
        return service.calendars().insert(calendar).execute().getId();
    }

    private Channel createChannel() {
        return new Channel()
                .setId(String.valueOf(UUID.randomUUID()))
                .setType("web_hook")
                .setAddress(properties.getNotificationUri());
    }
}
