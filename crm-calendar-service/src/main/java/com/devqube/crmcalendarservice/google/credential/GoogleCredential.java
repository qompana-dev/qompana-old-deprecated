package com.devqube.crmcalendarservice.google.credential;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoogleCredential {
    @Id
    @SequenceGenerator(name = "google_credential_seq", sequenceName = "google_credential_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "google_credential_seq")
    private Long id;

    @Column(unique = true)
    @NotNull
    private String accountId;

    @NotNull
    private String accessToken;

    @NotNull
    private Long expirationTimeMilliseconds;

    @NotNull
    private String refreshToken;
}
