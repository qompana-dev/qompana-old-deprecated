package com.devqube.crmcalendarservice.google.calendar;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface GoogleCalendarRepository extends JpaRepository<GoogleCalendar, Long> {
    boolean existsByAccountId(String accountId);
    GoogleCalendar findByAccountId(String accountId);
    GoogleCalendar findByCalendarId(String calendarId);
    List<GoogleCalendar> findAllByChannelExpirationIsBefore(LocalDateTime time);
    void deleteAllByAccountId(String accountId);
}
