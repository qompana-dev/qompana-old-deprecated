package com.devqube.crmcalendarservice.google.event;

import com.devqube.crmcalendarservice.google.calendar.GoogleCalendar;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoogleEvent {
    @Id
    @SequenceGenerator(name = "google_event_seq", sequenceName = "google_event_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "google_event_seq")
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "calendar_id")
    private GoogleCalendar calendar;

    @NotNull
    @Column(unique = true)
    private String eventId;

    @NotNull
    private String taskId;

    public GoogleEvent(@NotNull GoogleCalendar calendar, @NotNull String eventId, @NotNull String taskId) {
        this.calendar = calendar;
        this.eventId = eventId;
        this.taskId = taskId;
    }
}
