create TABLE google_credential(
 id bigint PRIMARY KEY,
 account_id VARCHAR(30) UNIQUE NOT NULL,
 access_token VARCHAR(500) NOT NULL,
 expiration_time_milliseconds bigint NOT NULL,
 refresh_token VARCHAR(500) NOT NULL
);

create sequence google_credential_seq start 1;

create TABLE google_calendar(
 id bigint PRIMARY KEY,
 account_id VARCHAR(30) UNIQUE NOT NULL,
 calendar_id VARCHAR(100) UNIQUE NOT NULL,
 push_notification_channel_id VARCHAR(100) UNIQUE,
 channel_expiration TIMESTAMP,
 sync_token VARCHAR(100)
);

create sequence google_calendar_seq start 1;

create TABLE google_event(
 id bigint PRIMARY KEY,
 calendar_id bigint NOT NULL,
 event_id VARCHAR(100) NOT NULL UNIQUE,
 task_id VARCHAR(30) NOT NULL,
 constraint google_event_calendar_fk FOREIGN KEY(calendar_id)
    references google_calendar(id) ON delete CASCADE
);

create sequence google_event_seq start 1;

create TABLE microsoft_credential(
 id bigint PRIMARY KEY,
 account_id VARCHAR(30) UNIQUE NOT NULL,
 access_token TEXT NOT NULL,
 expiration_time_seconds bigint NOT NULL,
 refresh_token TEXT NOT NULL,
 expiration_time TIMESTAMP NOT NULL,
 id_token TEXT NOT NULL
);

create sequence microsoft_credential_seq start 1;

create TABLE microsoft_calendar(
 id bigint PRIMARY KEY,
 account_id VARCHAR(30) UNIQUE NOT NULL,
 calendar_id TEXT UNIQUE NOT NULL,
 subscription_id VARCHAR(100),
 subscription_expiration_time TIMESTAMP
);

create sequence microsoft_calendar_seq start 1;

create TABLE microsoft_event(
 id bigint PRIMARY KEY,
 calendar_id bigint NOT NULL,
 event_id TEXT NOT NULL UNIQUE,
 task_id VARCHAR(30) NOT NULL,
 CONSTRAINT microsoft_event_calendar_fk FOREIGN KEY(calendar_id)
  REFERENCES microsoft_calendar(id) ON delete CASCADE
);

create sequence microsoft_event_seq start 1;
