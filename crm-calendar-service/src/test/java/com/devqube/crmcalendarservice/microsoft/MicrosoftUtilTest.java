package com.devqube.crmcalendarservice.microsoft;

import com.devqube.crmcalendarservice.microsoft.dto.CalendarResponseDto;
import com.devqube.crmcalendarservice.microsoft.util.MicrosoftUtil;
import com.microsoft.graph.models.extensions.Calendar;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@Slf4j
public class MicrosoftUtilTest {
    @Test
    public void shouldGetDefaultName() {
        String name = MicrosoftUtil.getName(getList(), "name");
        assertEquals("name", name);
    }

    @Test
    public void shouldGetDefaultNameWithIndex() {
        String name = MicrosoftUtil.getName(getList(), "test");
        assertEquals("test3", name);
    }

    @Test
    public void shouldGetNameNull() {
        String name = MicrosoftUtil.getName(getList(), null);
        assertNull(name);
        name = MicrosoftUtil.getName(null, "test");
        assertNull(name);
        name = MicrosoftUtil.getName(getList(), "");
        assertNull(name);
    }

    private List<Calendar> getList() {
        return Arrays.asList(
                getCalendar("test1"),
                getCalendar("test2"),
                getCalendar("test"),
                getCalendar("test4")
        );
    }
    private Calendar getCalendar(String name) {
        Calendar calendar = new Calendar();
        calendar.name = name;
        return calendar;
    }
}
