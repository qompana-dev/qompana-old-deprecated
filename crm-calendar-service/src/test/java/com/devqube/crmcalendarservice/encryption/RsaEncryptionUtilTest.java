package com.devqube.crmcalendarservice.encryption;

import com.devqube.crmcalendarservice.AbstractIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@Slf4j
public class RsaEncryptionUtilTest extends AbstractIntegrationTest {


    @Test
    public void shouldEncryptEmptyString() throws EncryptionException {
        String encrypted = RsaEncryptionUtil.encrypt("");
        assertEquals("", RsaEncryptionUtil.decrypt(encrypted));
    }

    @Test
    public void shouldEncryptTextWithOnlyAlphanumericCharacters() throws EncryptionException {
        String encrypted = RsaEncryptionUtil.encrypt("abcdefghijklmnopqrstuvwxyz1234567890");
        assertEquals("abcdefghijklmnopqrstuvwxyz1234567890", RsaEncryptionUtil.decrypt(encrypted));
    }

    @Test
    public void shouldEncryptTextWithSpecialCharacters() throws EncryptionException {
        String encrypted = RsaEncryptionUtil.encrypt("!@#$%^&*(){}|:?><");
        assertEquals("!@#$%^&*(){}|:?><", RsaEncryptionUtil.decrypt(encrypted));
    }

    @Test
    public void shouldEncryptTextWithPolishCharacters() throws EncryptionException {
        String encrypted = RsaEncryptionUtil.encrypt("zażółć gęślą jaźń");
        assertEquals("zażółć gęślą jaźń", RsaEncryptionUtil.decrypt(encrypted));
    }

    @Test
    public void shouldEncryptTextWithChineseCharacters() throws EncryptionException {
        String encrypted = RsaEncryptionUtil.encrypt("輸入罕見異體字或者輸入插圖，" +
                "具體操作還是沒弄明白，哪裡有教程可以學習呢");
        assertEquals("輸入罕見異體字或者輸入插圖，具體操作還是沒弄明白，哪裡有教程可以學習呢",
                RsaEncryptionUtil.decrypt(encrypted));
    }
}
