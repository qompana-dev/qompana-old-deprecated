package com.devqube.crmcalendarservice.encryption;

import com.devqube.crmcalendarservice.AbstractIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@Slf4j
public class AesEncryptionUtilTest extends AbstractIntegrationTest {

    @Test
    public void shouldEncryptEmptyString() throws EncryptionException {
        String encrypted = AesEncryptionUtil.encrypt("");
        assertEquals("", AesEncryptionUtil.decrypt(encrypted));
    }

    @Test
    public void shouldEncryptTextWithOnlyAlphanumericCharacters() throws EncryptionException {
        String encrypted = AesEncryptionUtil.encrypt("abcdefghijklmnopqrstuvwxyz1234567890");
        assertEquals("abcdefghijklmnopqrstuvwxyz1234567890", AesEncryptionUtil.decrypt(encrypted));
    }

    @Test
    public void shouldEncryptTextWithSpecialCharacters() throws EncryptionException {
        String encrypted = AesEncryptionUtil.encrypt("!@#$%^&*(){}|:?><");
        assertEquals("!@#$%^&*(){}|:?><", AesEncryptionUtil.decrypt(encrypted));
    }

    @Test
    public void shouldEncryptTextWithPolishCharacters() throws EncryptionException {
        String encrypted = AesEncryptionUtil.encrypt("zażółć gęślą jaźń");
        assertEquals("zażółć gęślą jaźń", AesEncryptionUtil.decrypt(encrypted));
    }
    @Test
    public void shouldEncryptLongText() throws EncryptionException {
        String text = getRandomCharacters(20000);
        String encrypted = AesEncryptionUtil.encrypt(text);
        assertEquals(text, AesEncryptionUtil.decrypt(encrypted));
    }

    @Test
    public void shouldEncryptTextWithChineseCharacters() throws EncryptionException {
        String encrypted = AesEncryptionUtil.encrypt("輸入罕見異體字或者輸入插圖，" +
                "具體操作還是沒弄明白，哪裡有教程可以學習呢");
        assertEquals("輸入罕見異體字或者輸入插圖，具體操作還是沒弄明白，哪裡有教程可以學習呢",
                AesEncryptionUtil.decrypt(encrypted));
    }

    private String getRandomCharacters(int length) {
        StringBuilder result= new StringBuilder();
        Random r = new Random();

        String characters = "abcdefghijklmnopqrstuvwxyz0123456789";
        for (int i = 0; i < length; i++) {
            result.append(characters.charAt(r.nextInt(characters.length())));
        }
        return result.toString();
    }
}
