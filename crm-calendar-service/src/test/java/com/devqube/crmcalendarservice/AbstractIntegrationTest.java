package com.devqube.crmcalendarservice;

import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = {CrmCalendarServiceApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
public abstract class AbstractIntegrationTest {
    @LocalServerPort
    private int port;

    protected TestRestTemplate restTemplate;
    protected String baseUrl;

    protected void setUp() {
        baseUrl = "http://localhost:" + port;
        restTemplate = new TestRestTemplate();
    }
}
