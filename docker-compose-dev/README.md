Docker Compose for Dev Environment

Steps to setup:
1. Build all service parts
2. run docker-compose
3. Use init_dbs.cmd/init_dbs.sh scripts to create schemas and fill DBs with test data
