docker cp ./create_all_tables.sql crm-services-db:/create_all_tables.sql
docker exec -it crm-services-db psql -U postgres -f /create_all_tables.sql
docker cp ../crm-association-service/src/main/resources/schema.sql crm-services-db:/association-schema.sql
docker cp ../crm-business-service/src/main/resources/schema.sql crm-services-db:/business-schema.sql
docker cp ../crm-business-service/src/main/resources/data.sql crm-services-db:/business-data.sql
docker cp ../crm-calendar-service/src/main/resources/schema.sql crm-services-db:/calendar-schema.sql
docker cp ../crm-file-service/src/main/resources/schema.sql crm-services-db:/file-schema.sql
docker cp ../crm-history-service/src/main/resources/schema.sql crm-services-db:/history-schema.sql
docker cp ../crm-mail-service/src/main/resources/schema.sql crm-services-db:/mail-schema.sql
docker cp ../crm-mail-service/src/main/resources/data.sql crm-services-db:/mail-data.sql
docker cp ../crm-notification-service/src/main/resources/schema.sql crm-services-db:/notification-schema.sql
docker cp ../crm-notification-service/src/main/resources/data.sql crm-services-db:/notification-data.sql
docker cp ../crm-task-service/src/main/resources/schema.sql crm-services-db:/task-schema.sql
docker cp ../crm-user-service/src/main/resources/schema.sql crm-services-db:/user-schema.sql
docker cp ../crm-user-service/src/main/resources/data.sql crm-services-db:/user-data.sql

docker exec -it crm-services-db psql -U postgres -d association -f /association-schema.sql
docker exec -it crm-services-db psql -U postgres -d business -f /business-schema.sql
docker exec -it crm-services-db psql -U postgres -d business -f /business-data.sql
docker exec -it crm-services-db psql -U postgres -d calendar -f /calendar-schema.sql
docker exec -it crm-services-db psql -U postgres -d files -f /file-schema.sql
docker exec -it crm-services-db psql -U postgres -d history -f /history-schema.sql
docker exec -it crm-services-db psql -U postgres -d mail -f /mail-schema.sql
docker exec -it crm-services-db psql -U postgres -d mail -f /mail-data.sql
docker exec -it crm-services-db psql -U postgres -d notification -f /notification-schema.sql
docker exec -it crm-services-db psql -U postgres -d notification -f /notification-data.sql
docker exec -it crm-services-db psql -U postgres -d task -f /task-schema.sql
docker exec -it crm-services-db psql -U postgres -d crm -f /user-schema.sql
docker exec -it crm-services-db psql -U postgres -d crm -f /user-data.sql