package com.devqube.crmfileservice.service.web;

import com.devqube.crmfileservice.AbstractIntegrationTest;
import com.devqube.crmfileservice.FileStorageProperties;
import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.repository.FileRepository;
import com.devqube.crmfileservice.web.dto.FileDto;
import com.devqube.crmfileservice.web.dto.FileFormDto;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.netflix.ribbon.StaticServerList;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;

import java.nio.file.Paths;
import java.util.*;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;

@Ignore("This functionality will be reimplemented soon, after reimplementation fix test and un-ignore")
@RunWith(SpringRunner.class)
@ActiveProfiles({"test", "wiremock"})
@ContextConfiguration(classes = {FileControllerIntegrationTest.LocalClientConfiguration.class})
@TestPropertySource(locations = {"classpath:application.yml", "classpath:application-test.yml"}, properties = "jodconverter.local.port-numbers=2008")
public class FileControllerIntegrationTest extends AbstractIntegrationTest {
    @Autowired
    private FileRepository fileRepository;
    @Autowired
    private FileStorageProperties fileStorageProperties;

    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(WireMockConfiguration.options().dynamicPort());

    HttpHeaders headers;
    FileFormDto fileFormDto;

    private String associationsList = "";

    @Before
    public void setUp() {
        super.setUp();
        headers = createHeaders();
        fileFormDto = createFileForm();
        try {
            addAssociationDtoList();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        stubFor(get(urlEqualTo("/access/customer"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("[1234, 9876]")));
        stubFor(get(urlMatching("/associations/type/file/id/.*"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody(associationsList)));
        stubFor(put(urlMatching("/associations/list/type/file/id/.*"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody(associationsList)));
    }

    @After
    public void cleanUp() {
        fileRepository.deleteAll();
    }

    @Test
    public void shouldSaveFile() {

        LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        parameters.add("file", new org.springframework.core.io.ClassPathResource("test.jpg"));
        parameters.add("fileData", fileFormDto);

        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);

        ResponseEntity<FileDto> response = restTemplate.exchange(baseUrl + "/files", HttpMethod.POST, entity, FileDto.class, "");

        // Expect Ok
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        assertTrue(Paths.get(fileStorageProperties.getUploadDir(), response.getBody().getId()).toFile().exists());
        assertEquals("jan.kowalski@devqube.com", response.getBody().getAuthor());
        assertEquals("jpg", response.getBody().getFormat());
        assertEquals("image/jpeg", response.getBody().getContentType());
        assertEquals("myDescription", response.getBody().getDescription());
        assertNotNull(response.getBody().getTags());

        assertEquals(3, response.getBody().getTags().size());
        assertTrue(response.getBody().getTags().contains("one"));
        assertTrue(response.getBody().getTags().contains("two"));
        assertTrue(response.getBody().getTags().contains("three"));

        assertEquals(response.getBody().getFileGroups().size(), 2);
        assertTrue(response.getBody().getFileGroups().stream().anyMatch(fileGroupDto -> fileGroupDto.getGroupId().equals(1234L)));
        assertTrue(response.getBody().getFileGroups().stream().anyMatch(fileGroupDto -> fileGroupDto.getGroupId().equals(9876L)));
    }

    @Test
    public void shouldUpdateFile() {
        File file = fileRepository.save(getFile(null, "name1"));

        HttpEntity<FileFormDto> fileFormDtoHttpEntity = new HttpEntity<>(fileFormDto, headers);

        ResponseEntity<FileDto> response = restTemplate.exchange(
                baseUrl + "/files/" + file.getName() + "/info", HttpMethod.PUT, fileFormDtoHttpEntity, FileDto.class, "");


        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(fileFormDto.getDescription(), response.getBody().getDescription());
        assertEquals(3, response.getBody().getTags().size());
        assertTrue(response.getBody().getTags().contains("one"));
        assertTrue(response.getBody().getTags().contains("two"));
        assertTrue(response.getBody().getTags().contains("three"));


        assertEquals(response.getBody().getFileGroups().size(), 2);
        assertTrue(response.getBody().getFileGroups().stream().anyMatch(fileGroupDto -> fileGroupDto.getGroupId().equals(1234L)));
        assertTrue(response.getBody().getFileGroups().stream().anyMatch(fileGroupDto -> fileGroupDto.getGroupId().equals(9876L)));
    }

    private File getFile(Long id, String name) {
        return File.builder()
                .id(id)
                .name(name)
                .originalName("originalname")
                .path("/pathToFile/file")
                .format("pdf")
                .accountEmail("testemail@devqube.com")
                .fileEvents(new ArrayList<>())
                .description("description").build();
    }

    private FileFormDto createFileForm() {
        String description = "myDescription";
        List<String> tags = Arrays.asList("one", "two", "three");

        CrmObject groupOne = CrmObject.builder().id(1234L).type(CrmObjectType.customer).build();
        CrmObject groupTwo = CrmObject.builder().id(9876L).type(CrmObjectType.customer).build();
        Set<CrmObject> groups = Set.of(groupOne, groupTwo);
        return FileFormDto.builder().description(description).tags(new HashSet<>(tags)).assignedObjects(groups).build();
    }

    private HttpHeaders createHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Logged-Account-Email", "jan.kowalski@devqube.com");
        return headers;
    }

    private void addAssociationDtoList() throws JsonProcessingException {
        this.associationsList = new ObjectMapper().writeValueAsString(Arrays.asList(
                getAssociation(1L, 1234L),
                getAssociation(1L, 9876L)));
    }
    private AssociationDto getAssociation(Long id, Long destId) {
        return AssociationDto.builder().sourceId(id).destinationId(destId).sourceType(CrmObjectType.file).destinationType(CrmObjectType.customer).build();
    }

    @TestConfiguration
    public static class LocalClientConfiguration {
        @Bean
        public ServerList<Server> serverList() {
            return new StaticServerList<>(new Server("localhost", wireMockRule.port()));
        }
    }
}
