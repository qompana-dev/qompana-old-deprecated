package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.integration.BusinessClient;
import com.devqube.crmfileservice.model.File;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.access.AccessDeniedException;

import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileAccessServiceTest {

    @Mock
    PermissionsService permissionsService;

    @Mock
    BusinessClient businessClient;

    @InjectMocks
    FileAccessService fileAccessService;

    private File file;
    private String fileOwnerEmail = "emailOf@fileOwner.pl";
    private String fileRequesterEmail = "emailOf@fileRequester.pl";

    @Before
    public void setUp() {
        this.file = File.builder().accountEmail(this.fileOwnerEmail).build();
    }

    @Test(expected = AccessDeniedException.class)
    public void shouldThrowExceptionWhenDoesntHaveAccessToAnyOfGroups() {
        CrmObject one = CrmObject.builder().id(1234L).type(CrmObjectType.customer).build();
        CrmObject two = CrmObject.builder().id(9876L).type(CrmObjectType.customer).build();
        when(businessClient.getAllCustomersWithAccess(fileRequesterEmail)).thenReturn(Set.of(54354L, 1234L));

        fileAccessService.assertHasAccessToGroups(fileRequesterEmail, Set.of(one, two));
    }

    @Test
    public void shouldExecuteWithoutAnyExceptionWhenHaveAccessToGroups() {
        CrmObject one = CrmObject.builder().id(1234L).type(CrmObjectType.customer).build();
        CrmObject two = CrmObject.builder().id(9876L).type(CrmObjectType.customer).build();
        when(businessClient.getAllCustomersWithAccess(fileRequesterEmail)).thenReturn(Set.of(9876L, 1234L, 7657657L, 8768L));

        fileAccessService.assertHasAccessToGroups(fileRequesterEmail, Set.of(one, two));
    }

    @Test
    public void shouldExecuteWithoutAnyExceptionWhenFileDeletingByFileOwner() {
        fileAccessService.assertCanDeleteFile(fileOwnerEmail, file);
    }

    @Test
    public void shouldExecuteWithoutAnyExceptionWhenUserHasRightsToDeleteFile() {
        when(permissionsService.hasWritePermission(fileRequesterEmail, "DocumentsComponent.delete")).thenReturn(true);
        fileAccessService.assertCanDeleteFile(fileRequesterEmail, file);
    }

    @Test(expected = AccessDeniedException.class)
    public void shouldThrowExceptionWhenFileDeletingNotByFileOwnerOrUserWithRights() {
        when(permissionsService.hasWritePermission(fileRequesterEmail, "DocumentsComponent.delete")).thenReturn(false);
        fileAccessService.assertCanDeleteFile(fileRequesterEmail, file);
    }

    @Test
    public void getAccessGroupMap() {
        when(businessClient.getAllCustomersWithAccess(fileRequesterEmail)).thenReturn(Set.of(1234L, 9876L, 4576L));
        Map<CrmObjectType, Set<Long>> accessGroupMap = fileAccessService.getAccessGroupMap(fileRequesterEmail);
        assertThat(accessGroupMap.get(CrmObjectType.customer), is(Set.of(1234L, 9876L, 4576L)));
        assertThat(accessGroupMap.get(CrmObjectType.user), is(Set.of()));
    }
}
