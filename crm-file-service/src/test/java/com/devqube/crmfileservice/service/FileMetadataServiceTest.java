package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.exception.FileNotFoundException;
import com.devqube.crmfileservice.integration.UserClient;
import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.model.Tag;
import com.devqube.crmfileservice.repository.FileRepository;
import com.devqube.crmfileservice.repository.TagRepository;
import com.devqube.crmfileservice.web.dto.FileFormDto;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.search.CrmObjectType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FileMetadataServiceTest {


    @Mock
    private FileRepository fileRepository;

    @Mock
    private TagRepository tagRepository;

    @Mock
    private FileNameService fileNameService;
    @Mock
    private AssociationClient associationClient;
    @Mock
    private UserClient userClient;

    @InjectMocks
    private FileMetadataService fileMetadataService;

    private File file;
    private String fileRequesterEmail = "emailOf@fileOwner.pl";
    private Set<String> tags;

    @Before
    public void setUp() {
        this.tags = Set.of("one", "two", "three");
        this.file = File.builder()
                .name("1234-sdff-kjhl-234")
                .originalName("fileName.jpg")
                .format("jpg").path("/")
                .accountEmail(this.fileRequesterEmail)
                .description("description")
                .tags(tags.stream().map(Tag::new).collect(Collectors.toSet()))
                .authorFullName("")
                .build();
    }

    @Test
    public void shouldDeleteFileMetadata() {
        when(fileRepository.findByName("fileName")).thenReturn(Optional.of(file));
        fileMetadataService.deleteFileMetadata("fileName");
        verify(fileRepository).delete(file);
    }

    @Test(expected = FileNotFoundException.class)
    public void shouldThrowExceptionWhenDeletedFileNotFound() {
        when(fileRepository.findByName("fileName")).thenReturn(Optional.empty());
        fileMetadataService.deleteFileMetadata("fileName");
    }

    @Test
    public void shouldSaveFileMetadata() {
        when(fileNameService.generateFileName()).thenReturn("1234-sdff-kjhl-234");
        when(fileRepository.save(any())).thenReturn(File.builder().id(1L).build());

        FileFormDto fileForm = FileFormDto.builder().description("description").tags(tags).build();
        fileMetadataService.saveFileMetadata(fileRequesterEmail, "fileName.jpg", fileForm, 1024L);
        verify(fileRepository).save(file);
    }


    @Test
    public void shouldGetFileData() {
        when(fileRepository.findByName("1234-sdff-kjhl-234")).thenReturn(Optional.of(file));
        Optional<File> fileOptional = this.fileMetadataService.getFileData("1234-sdff-kjhl-234");
        assertEquals(file, fileOptional.get());
    }

    @Test
    public void shouldGetUserFiles() {
        when(fileRepository.findAllByAccountEmail(fileRequesterEmail)).thenReturn(List.of(file));
        List<File> userFiles = fileMetadataService.getUserFiles(fileRequesterEmail);
        assertEquals(1, userFiles.size());
        assertTrue(userFiles.contains(file));
    }

    @Test
    public void shouldGetAllTagsWhenPrefixBlank() {
        List<Tag> expected = tags.stream().map(Tag::new).collect(Collectors.toList());
        when(tagRepository.findAll()).thenReturn(expected);
        Set<String> actual = fileMetadataService.getTagsStartsWith("");
        assertEquals(tags, actual);
        actual = fileMetadataService.getTagsStartsWith(null);
        assertEquals(tags, actual);
    }

    @Test
    public void shouldGetTagsStartsWith() {
        Set<Tag> expected = tags.stream().map(Tag::new).collect(Collectors.toSet());
        when(tagRepository.findByNameStartsWith("prefix")).thenReturn(expected);
        Set<String> actual = fileMetadataService.getTagsStartsWith("prefix");
        assertEquals(tags, actual);
    }

    @Test
    public void shouldReturnOnlyUserFilesIfGroupsMapIsEmpty() {
        when(fileRepository.getByAccountEmailAndSearch(anyString(), anyString())).thenReturn(new HashSet<>(Arrays.asList(
                File.builder().id(1L).build(),
                File.builder().id(2L).build(),
                File.builder().id(3L).build()
        )));

        Set<File> allFilesForUser = fileMetadataService.getAllFilesForUser(new HashMap<>(), "jan.kowalski@devqube.com", "");

        assertNotNull(allFilesForUser);
        assertEquals(3, allFilesForUser.size());
        assertTrue(allFilesForUser.stream().anyMatch(c -> c.getId().equals(1L)));
        assertTrue(allFilesForUser.stream().anyMatch(c -> c.getId().equals(2L)));
        assertTrue(allFilesForUser.stream().anyMatch(c -> c.getId().equals(3L)));

        verify(associationClient, times(0)).getAllBySourceTypeAndDestinationTypeAndSourceIds(any(), any(), any());
    }

    @Test
    public void shouldReturnAllUserFiles() {
        Map<CrmObjectType, Set<Long>> map = new HashMap<CrmObjectType, Set<Long>>() {{
            put(CrmObjectType.lead, Set.of(1L, 2L));
        }};

        when(fileRepository.getByAccountEmailAndSearch(anyString(), anyString())).thenReturn(new HashSet<>(Arrays.asList(
                File.builder().id(5L).build(),
                File.builder().id(6L).build()
        )));


        when(fileRepository.getByIdsAndSearch(any(), anyString())).thenReturn(new HashSet<>(Arrays.asList(
                File.builder().id(7L).build(),
                File.builder().id(8L).build(),
                File.builder().id(9L).build()
        )));

        Set<File> allFilesForUser = fileMetadataService.getAllFilesForUser(map, "jan.kowalski@devqube.com", "");

        assertNotNull(allFilesForUser);
        assertEquals(5, allFilesForUser.size());
        assertTrue(allFilesForUser.stream().anyMatch(c -> c.getId().equals(5L)));
        assertTrue(allFilesForUser.stream().anyMatch(c -> c.getId().equals(6L)));
        assertTrue(allFilesForUser.stream().anyMatch(c -> c.getId().equals(7L)));
        assertTrue(allFilesForUser.stream().anyMatch(c -> c.getId().equals(8L)));
        assertTrue(allFilesForUser.stream().anyMatch(c -> c.getId().equals(9L)));

        verify(fileRepository, times(1)).getByAccountEmailAndSearch(anyString(), anyString());
    }
}
