package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.model.FileEvent;
import com.devqube.crmfileservice.model.FileEventType;
import com.devqube.crmfileservice.model.Tag;
import com.devqube.crmfileservice.repository.FileEventRepository;
import com.devqube.crmfileservice.repository.FileRepository;
import com.devqube.crmfileservice.web.dto.FileFormDto;
import com.devqube.crmshared.association.AssociationClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FileEventServiceTest {

    @Mock
    private FileEventRepository fileEventRepository;

    @Mock
    private FileRepository fileRepository;
    @Mock
    private AssociationClient associationClient;

    @InjectMocks
    private FileEventService fileEventService;

    private File testFile;
    private String changedByEmail = "changeBy@devqube.com";


    @Before
    public void setUp() throws Exception {
        testFile = createFile();
    }

    @Test
    public void shouldSaveNewFileCreated() {

        fileEventService.newFileCreated(testFile);
        FileEvent expectedEvent = FileEvent.builder()
                .file(testFile)
                .eventType(FileEventType.FILE_CREATED)
                .correlationId(System.currentTimeMillis())
                .accountEmail(changedByEmail)
                .value1("1235-mnbm-iuyiuy-kkjhk")
                .value2(null)
                .build();
        checkEventWasSaved(expectedEvent);
    }


    @Test
    public void shouldSaveEventVersionChanged() {

        fileEventService.versionChanged(testFile, changedByEmail, 2048L);
        FileEvent expectedEvent = FileEvent.builder()
                .file(testFile)
                .eventType(FileEventType.NEW_VERSION_UPLOADED)
                .correlationId(System.currentTimeMillis())
                .accountEmail(changedByEmail)
                .value1("1235-mnbm-iuyiuy-kkjhk")
                .value2(null)
                .build();
        checkEventWasSaved(expectedEvent);
    }

    @Test
    public void shouldSaveDescriptionChanged() {
        when(associationClient.getAllByTypeAndId(any(), any())).thenReturn(new ArrayList<>());
        FileFormDto dto = FileFormDto.builder().description("newDescription").tags(Set.of("one", "two", "three")).assignedObjects(new HashSet<>()).build();
        fileEventService.filePropertyChanged(testFile, dto, changedByEmail);
        FileEvent expectedEvent = FileEvent.builder()
                .file(testFile)
                .eventType(FileEventType.DESCRIPTION_CHANGED)
                .correlationId(System.currentTimeMillis())
                .accountEmail(changedByEmail)
                .value1(dto.getDescription())
                .value2(null)
                .build();
        checkEventWasSaved(expectedEvent);
    }

    @Test
    public void shouldSaveTagsChanged() {
        when(associationClient.getAllByTypeAndId(any(), any())).thenReturn(new ArrayList<>());
        FileFormDto dto = FileFormDto.builder().description("description").tags(Set.of("one", "two", "four")).assignedObjects(new HashSet<>()).build();
        fileEventService.filePropertyChanged(testFile, dto, changedByEmail);
        FileEvent expectedEvent = FileEvent.builder()
                .file(testFile)
                .eventType(FileEventType.TAGS_CHANGED)
                .correlationId(System.currentTimeMillis())
                .accountEmail(changedByEmail)
                .value1(String.join(", ", dto.getTags()))
                .value2(null)
                .build();
        checkEventWasSaved(expectedEvent);
    }

    @Test
    public void shouldNotSaveAnyEventAtAll() {
        when(associationClient.getAllByTypeAndId(any(), any())).thenReturn(new ArrayList<>());
        FileFormDto dto = FileFormDto.builder().description("description").tags(Set.of("one", "two", "three")).assignedObjects(new HashSet<>()).build();
        fileEventService.filePropertyChanged(testFile, dto, changedByEmail);
        verify(fileEventRepository, times(0)).save(any());
    }


    private File createFile() {
        return File.builder()
                .id(123L)
                .name("1235-mnbm-iuyiuy-kkjhk")
                .originalName("originalname")
                .path("/pathToFile/file")
                .format("pdf")
                .accountEmail(changedByEmail)
                .fileEvents(new ArrayList<>())
                .description("description")
                .tags(Set.of(new Tag("one"), new Tag("two"), new Tag("three")))
                .build();
    }

    private void checkEventWasSaved(FileEvent expectedEvent) {
        ArgumentCaptor<FileEvent> argument = ArgumentCaptor.forClass(FileEvent.class);
        verify(fileEventRepository, times(1)).save(argument.capture());
        assertThat(argument.getValue()).isEqualToIgnoringGivenFields(expectedEvent, "correlationId", "value1", "value2");
    }

}
