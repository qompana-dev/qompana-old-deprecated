package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.FileStorageProperties;
import com.devqube.crmfileservice.exception.FileNotFoundException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileSystemServiceTest {

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    @Mock
    private FileStorageProperties fileStorageProperties;

    private FileSystemService fileSystemService;
    private File file;


    @Before
    public void setUp() throws Exception {
        file = testFolder.newFile("test.jpg");
        when(fileStorageProperties.getUploadDir()).thenReturn(testFolder.getRoot().getAbsolutePath());
        when(fileStorageProperties.getTempDir()).thenReturn(testFolder.getRoot().getAbsolutePath());
        fileSystemService = new FileSystemService(fileStorageProperties);
    }

    @Test
    public void shouldDeleteFile() throws IOException {
        assertTrue(file.exists());
        fileSystemService.delete("test.jpg");
        assertFalse(file.exists());
    }

    @Test(expected = FileNotFoundException.class)
    public void shouldThrowExceptionIfFileDoesNotExists() throws IOException {
        fileSystemService.delete("notExistingFile.jpg");
    }

    @Test
    public void shouldSaveFile() {
        MockMultipartFile file = new MockMultipartFile("data", "other-file-name.data", "application/jpg", "someRandomBytes".getBytes());

        fileSystemService.save("fileName.jpg", file);
        File expectedFile = new File(String.format("%s/fileName.jpg", testFolder.getRoot().getAbsolutePath()));
        assertTrue(expectedFile.exists());
    }


    @Test
    public void shouldLoadExistingFileAsResource() {
        Resource resource = fileSystemService.loadFileAsResource("test.jpg");
        assertNotNull(resource);
    }

    @Test(expected = FileNotFoundException.class)
    public void shouldThrowExceptionWhenResourceDoesNotExists() {
        fileSystemService.loadFileAsResource("nonExistingFile.jpg");
    }
}
