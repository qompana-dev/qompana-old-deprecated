package com.devqube.crmfileservice.service.web;

import com.devqube.crmfileservice.AbstractIntegrationTest;
import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.model.FileEvent;
import com.devqube.crmfileservice.model.FileEventType;
import com.devqube.crmfileservice.repository.FileRepository;
import com.devqube.crmfileservice.web.dto.FileDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = {"classpath:application.yml", "classpath:application-test.yml"}, properties = "jodconverter.local.port-numbers=2009")
public class FileHistoryControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private FileRepository fileRepository;

    @Before
    public void setUp() {
        super.setUp();
    }


    @Test
    public void shouldReturn404CodeBecauseRequestedFileDoesNotExist() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Logged-Account-Email", "jan.kowalski@devqube.com");
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<FileDto> response = restTemplate.exchange(
                baseUrl + "/files/6546546546/history", HttpMethod.GET, entity, FileDto.class, entity);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    private File getFileWithHistory() {
        File file = File.builder()
                .id(123L)
                .name("name")
                .originalName("originalname")
                .path("/pathToFile/file")
                .format("pdf")
                .accountEmail("testemail@devqube.com")
                .fileEvents(new ArrayList<>())
                .description("description").build();

        FileEvent event1 = FileEvent.builder()
                .eventType(FileEventType.FILE_CREATED)
                .value1("newFile")
                .accountEmail("jak@kowalski.com")
                .file(file)
                .build();

        FileEvent event2 = FileEvent.builder()
                .eventType(FileEventType.DESCRIPTION_CHANGED)
                .value1("newDescription")
                .accountEmail("jak@kowalski.com")
                .file(file)
                .build();

        file.setFileEvents(List.of(event1, event2));
        return file;
    }

}
