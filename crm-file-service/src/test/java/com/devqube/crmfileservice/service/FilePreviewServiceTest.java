package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.exception.FileNotFoundException;
import com.devqube.crmfileservice.exception.UnsupportedPreviewFormatException;
import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.web.dto.FilePreviewDto;
import org.jodconverter.office.OfficeException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FilePreviewServiceTest {

    @Mock
    private FileConverter fileConverter;

    @Mock
    private FileSystemService fileSystemService;

    @InjectMocks
    @Spy
    private FilePreviewService filePreviewService;

    private File file;
    private String fileNameInSystem;

    @Before
    public void setUp() throws Exception {
        fileNameInSystem = "1234-4321-5678-8765";
        file = File.builder().originalName("file.doc").format("doc").name(fileNameInSystem).build();
    }

    @Test(expected = UnsupportedPreviewFormatException.class)
    public void shouldThrowExceptionWhenSavingPreviewWithWrongFormat() {
        File file = File.builder().originalName("name.wrongFormat").build();
        filePreviewService.getFilePreview(file);
    }

    @Test
    public void shouldReturnExistingFilePreview() {
        byte[] bytes = "hhgfhgfhgfghf".getBytes();
        FilePreviewDto expected = FilePreviewDto.builder().fileData(bytes).originalFileName("file.doc").build();

        when(fileSystemService.loadFileAsResource(any())).thenReturn(new ByteArrayResource(bytes));
        FilePreviewDto actual = filePreviewService.getFilePreview(file);
        assertEquals(expected, actual);
    }

    @Test
    public void shouldNotCallPreviewConversionWhenWrongFormat(){
        MockMultipartFile file = new MockMultipartFile("123-123-123-123", "file.pdf", null, "bar".getBytes());

        filePreviewService.saveFilePreviewIfPossible("123-123-123-123" ,file);
        verify(filePreviewService, never()).saveFilePreview(any(), (MultipartFile) any());

        file = new MockMultipartFile("123-123-123-123", "file.notSupportedFormat", null, "bar".getBytes());
        filePreviewService.saveFilePreviewIfPossible("123-123-123-123" , file);
        verify(filePreviewService, never()).saveFilePreview(any(), (MultipartFile) any());
    }

    @Test
    public void shouldReturnFilePreviewWhenAnyDoesNotExist() throws OfficeException, IOException {
        byte[] bytes = "hhgfhgfhgfghf".getBytes();
        byte[] expectedBytes = "fileAfterConversion".getBytes();
        Resource resource = new ByteArrayResource(bytes);
        FilePreviewDto expected = FilePreviewDto.builder().fileData(expectedBytes).originalFileName("file.doc").build();
        when(fileSystemService.loadFileAsResource("preview/1234-4321-5678-8765")).thenThrow(FileNotFoundException.class);
        when(fileSystemService.loadFileAsResource("1234-4321-5678-8765")).thenReturn(resource);
        when(fileConverter.convertToPdf(any(), eq("doc"))).thenReturn(expectedBytes);

        FilePreviewDto actual = filePreviewService.getFilePreview(file);
        assertEquals(expected, actual);
        verify(fileSystemService).save(eq("preview/1234-4321-5678-8765"), any(InputStream.class));
    }

    @Test
    public void shouldSaveFilePreview() throws IOException, OfficeException {
        byte[] expectedBytes = "fileAfterConversion".getBytes();
        MockMultipartFile file = new MockMultipartFile("123-123-123-123", "file.doc", null, "bar".getBytes());
        when(fileConverter.convertToPdf(any(InputStream.class), eq("doc"))).thenReturn(expectedBytes);

        filePreviewService.saveFilePreview(fileNameInSystem, file);
        verify(fileSystemService).save(eq("preview/" + fileNameInSystem), any(ByteArrayInputStream.class));
    }
}
