create TABLE file
(
    id            bigint PRIMARY KEY,
    name          VARCHAR(200) NOT NULL UNIQUE,
    original_name VARCHAR(200) NOT NULL,
    path          VARCHAR(200) NOT NULL,
    format        VARCHAR(20)  NOT NULL,
    account_email VARCHAR(100) NOT NULL,
    description   VARCHAR(500) NOT NULL,
    version       bigint default 0,
    created       TIMESTAMP,
    updated       TIMESTAMP
);
create sequence file_seq start 1;


create TABLE tag
(
    id   bigint PRIMARY KEY,
    name VARCHAR(200) NOT NULL UNIQUE
);
create sequence tag_seq start 1;


create TABLE file2tag
(
    file_id bigint NOT NULL,
    tag_id  bigint NOT NULL,
    CONSTRAINT tag_id_file2tag_tag_id_fk FOREIGN KEY (tag_id)
        REFERENCES tag (id),
    CONSTRAINT file_id_file2tag_file_id_fk FOREIGN KEY (file_id)
        REFERENCES file (id)
);


create TABLE file_event
(
    id             BIGINT PRIMARY KEY,
    file_id        BIGINT  NOT NULL,
    event_type     VARCHAR NOT NULL,
    value1         VARCHAR,
    value2         VARCHAR,
    account_email  VARCHAR NOT NULL,
    correlation_id BIGINT  NOT NULL,
    created        TIMESTAMP,
    CONSTRAINT file_id_fk FOREIGN KEY (file_id)
        REFERENCES file (id)
);
create sequence file_event_seq start 1;

create TABLE temp_file
(
    id              bigint PRIMARY KEY,
    original_name   VARCHAR(200) NOT NULL,
    format          VARCHAR(20)  NOT NULL,
    account_email   VARCHAR(100) NOT NULL,
    description     VARCHAR(500) NOT NULL,
    expiration_time TIMESTAMP
);
create sequence temp_file_seq start 1;
