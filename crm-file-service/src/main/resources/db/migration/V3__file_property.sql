create TABLE file_property
(
    id             bigint PRIMARY KEY,
    property_type  VARCHAR(200) NOT NULL,
    property_value VARCHAR(200) NOT NULL,
    file_id        bigint NOT NULL,
    CONSTRAINT file_property_file_id_fk FOREIGN KEY (file_id)
        REFERENCES file (id)
);
create sequence file_property_seq start 1;
