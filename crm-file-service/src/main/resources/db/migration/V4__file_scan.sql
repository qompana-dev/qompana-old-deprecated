create TABLE file_scan
(
    id               bigint PRIMARY KEY,
    scan_status      VARCHAR(100) NOT NULL,
    file_id          VARCHAR(200) NOT NULL UNIQUE,
    destination_path VARCHAR(200) NOT NULL UNIQUE
);
create sequence file_scan_seq start 1;
