SELECT setval('file_scan_seq', COALESCE((SELECT MAX(id) FROM file_scan), 1));
SELECT setval('file_seq', COALESCE((SELECT MAX(id) FROM file), 1));
SELECT setval('file_event_seq', COALESCE((SELECT MAX(id) FROM file_event), 1));
SELECT setval('file_property_seq', COALESCE((SELECT MAX(id) FROM file_property), 1));
SELECT setval('temp_file_seq', COALESCE((SELECT MAX(id) FROM temp_file), 1));
SELECT setval('tag_seq', COALESCE((SELECT MAX(id) FROM tag), 1));