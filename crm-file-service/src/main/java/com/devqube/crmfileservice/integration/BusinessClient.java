package com.devqube.crmfileservice.integration;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Set;

@FeignClient(name = "crm-business-service", url = "${crm-business-service.url}")
public interface BusinessClient {
    @RequestMapping(value = "/access/customer", consumes = {"application/json"}, method = RequestMethod.GET)
    Set<Long> getAllCustomersWithAccess(@RequestHeader("Logged-Account-Email") String email);

    @RequestMapping(value = "/access/leads", consumes = {"application/json"}, method = RequestMethod.GET)
    Set<Long> getAllLeadWithAccess(@RequestHeader("Logged-Account-Email") String email);

    @RequestMapping(value = "/access/opportunity", consumes = {"application/json"}, method = RequestMethod.GET)
    Set<Long> getAllOpportunityWithAccess(@RequestHeader("Logged-Account-Email") String email);

    @RequestMapping(value = "/access/contact", consumes = {"application/json"}, method = RequestMethod.GET)
    Set<Long> getAllContactsWithAccess(@RequestHeader("Logged-Account-Email") String email);

    @RequestMapping(value = "/access/product", consumes = {"application/json"}, method = RequestMethod.GET)
    Set<Long> getAllProductsWithAccess(@RequestHeader("Logged-Account-Email") String email);
}
