package com.devqube.crmfileservice.integration;

import com.devqube.crmshared.access.model.ProfileDto;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.user.dto.AccountEmail;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@FeignClient(name = "crm-user-service", url = "${crm-user-service.url}")
public interface UserClient {
    @RequestMapping(value = "/profile/basic/mine", consumes = {"application/json"}, method = RequestMethod.GET)
    ProfileDto getBasicProfile(@RequestHeader("Logged-Account-Email") String email);

    @RequestMapping(value = "/accounts/emails", produces = {"application/json"}, method = RequestMethod.GET)
    List<AccountEmail> getAccountEmails();

    @RequestMapping(value = "/internal/accounts/emails/by-email/{email}", produces = {"application/json"}, method = RequestMethod.GET)
    AccountEmail getAccountEmailByEmail(@PathVariable("email") String email);

    @RequestMapping(value = "/accounts/crm-object", method = RequestMethod.GET)
    List<CrmObject> getAccountsAsCrmObjects(@RequestParam("ids") List<Long> ids);
}
