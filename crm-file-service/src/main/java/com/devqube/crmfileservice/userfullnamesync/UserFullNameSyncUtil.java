package com.devqube.crmfileservice.userfullnamesync;

import com.devqube.crmshared.user.dto.AccountEmail;

public class UserFullNameSyncUtil {
    public static String getAuthorFullName(AccountEmail accountEmail) {
        if (accountEmail == null) {
            return "";
        }
        return accountEmail.getName() + " " + accountEmail.getSurname();
    }
}
