package com.devqube.crmfileservice.userfullnamesync;

import com.devqube.crmfileservice.integration.UserClient;
import com.devqube.crmfileservice.repository.FileRepository;
import com.devqube.crmshared.user.dto.AccountEmail;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@EnableScheduling
public class UserFullNameSyncService {
    private final UserClient userClient;
    private final FileRepository fileRepository;

    public UserFullNameSyncService(UserClient userClient, FileRepository fileRepository) {
        this.userClient = userClient;
        this.fileRepository = fileRepository;
    }

    @Scheduled(fixedDelay = 10 * 60 * 1000)
    public void updateUsers() {
        List<AccountEmail> accountEmails = userClient.getAccountEmails();
        for (AccountEmail accountEmail : accountEmails) {
            fileRepository.setAuthorFullName(accountEmail.getEmail(), UserFullNameSyncUtil.getAuthorFullName(accountEmail));
        }
        fileRepository.setRemovedAuthorFullNames(accountEmails.stream().map(AccountEmail::getEmail).collect(Collectors.toList()));
    }
}
