package com.devqube.crmfileservice.antivirus;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import xyz.capybara.clamav.ClamavClient;
import xyz.capybara.clamav.commands.scan.result.ScanResult;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static java.nio.file.Files.readAllBytes;

@Slf4j
@Service
public class AntivirusConnectorService {
    private final AntivirusProperties antivirusProperties;
    private final ClamavClient clamavClient;

    public AntivirusConnectorService(AntivirusProperties antivirusProperties) {
        this.antivirusProperties = antivirusProperties;
        this.clamavClient = getClamAvClient();
    }

    public boolean scanFile(Path path) throws IOException {
        ScanResult scan = clamavClient.scan(getInputStreamFromPath(path));

        if (scan instanceof ScanResult.OK) {
            return true;
        } else if (scan instanceof ScanResult.VirusFound) {
            logViruses(path, scan);
        }
        return false;
    }

    private void logViruses(Path path, ScanResult scan) {
        try {
            Map<String, Collection<String>> viruses = ((ScanResult.VirusFound) scan).getFoundViruses();
            List<String> toPrint = new ArrayList<>();
            viruses.values().forEach(toPrint::addAll);
            toPrint.forEach(c -> log.info("virus in {} = {}", path.getFileName(), c));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private InputStream getInputStreamFromPath(Path path) throws IOException {
        return new ByteArrayInputStream(readAllBytes(path));
    }

    private ClamavClient getClamAvClient() {
        return new ClamavClient(
                antivirusProperties.getHost(),
                antivirusProperties.getPort());
    }
}
