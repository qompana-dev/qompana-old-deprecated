package com.devqube.crmfileservice.antivirus.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class FileScan {
    @Id
    @SequenceGenerator(name = "file_scan_seq", sequenceName = "file_scan_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "file_scan_seq")
    private Long id;

    @Enumerated(EnumType.STRING)
    @NonNull
    private ScanStatusEnum scanStatus;

    private String destinationPath;

    @NonNull
    private String fileId;
}
