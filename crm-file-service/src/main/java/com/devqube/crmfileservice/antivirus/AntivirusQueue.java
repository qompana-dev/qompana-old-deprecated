package com.devqube.crmfileservice.antivirus;

import java.util.LinkedList;
import java.util.Queue;

public class AntivirusQueue {
    private static AntivirusQueue instance;
    private Queue<String> fileIds = new LinkedList<>();
    private boolean scanning = false;

    private AntivirusQueue() {
    }

    public static AntivirusQueue getInstance() {
        if (instance == null) {
            instance = new AntivirusQueue();
        }
        return instance;
    }

    public void add(String fileId) {
        if (!fileIds.contains(fileId)) {
            this.fileIds.add(fileId);
        }
    }

    public String getTopAndRemove() {
        return fileIds.poll();
    }

    public boolean isScanning() {
        return scanning;
    }

    public void setScanning(boolean scanning) {
        this.scanning = scanning;
    }
}
