package com.devqube.crmfileservice.antivirus;

import com.devqube.crmfileservice.antivirus.model.FileScan;
import com.devqube.crmfileservice.antivirus.model.ScanStatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FileScanRepository extends JpaRepository<FileScan, Long> {
    public List<FileScan> findAllByScanStatus(ScanStatusEnum scanStatus);

    public Optional<FileScan> findByFileId(String fileId);
}
