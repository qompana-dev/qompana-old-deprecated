package com.devqube.crmfileservice.antivirus;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Setter
@Getter
@ConfigurationProperties(prefix = "antivirus")
public class AntivirusProperties {
    private String host;
    private Integer port;
}
