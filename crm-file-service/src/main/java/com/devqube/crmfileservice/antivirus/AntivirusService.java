package com.devqube.crmfileservice.antivirus;

import com.devqube.crmfileservice.FileStorageProperties;
import com.devqube.crmfileservice.antivirus.model.FileScan;
import com.devqube.crmfileservice.antivirus.model.ScanStatusEnum;
import com.devqube.crmfileservice.repository.FileRepository;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@EnableScheduling
public class AntivirusService {
    private final AntivirusConnectorService antivirusConnectorService;
    private final FileScanRepository fileScanRepository;
    private final FileStorageProperties fileStorageProperties;
    private final FileRepository fileRepository;

    public AntivirusService(AntivirusConnectorService antivirusConnectorService, FileScanRepository fileScanRepository, FileStorageProperties fileStorageProperties, FileRepository fileRepository) {
        this.antivirusConnectorService = antivirusConnectorService;
        this.fileScanRepository = fileScanRepository;
        this.fileStorageProperties = fileStorageProperties;
        this.fileRepository = fileRepository;
        syncFiles();
        addFilesToQueue();
    }

    public void add(String fileId, String destinationPath) {
        Optional<FileScan> byFileId = fileScanRepository.findByFileId(fileId);
        if (byFileId.isEmpty()) {
            FileScan fileScan = new FileScan();
            fileScan.setScanStatus(ScanStatusEnum.CREATED);
            fileScan.setFileId(fileId);
            fileScan.setDestinationPath(destinationPath);
            fileScanRepository.save(fileScan);
            AntivirusQueue.getInstance().add(fileId);
        }
    }

    private void addFilesToQueue() {
        fileScanRepository.findAllByScanStatus(ScanStatusEnum.CREATED)
                .forEach(c -> AntivirusQueue.getInstance().add(c.getFileId()));
        fileScanRepository.findAllByScanStatus(ScanStatusEnum.CANNOT_CHECK)
                .forEach(c -> AntivirusQueue.getInstance().add(c.getFileId()));
    }

    private void updateStatus(String fileId, ScanStatusEnum scanStatus) throws EntityNotFoundException {
        FileScan file = fileScanRepository.findByFileId(fileId).orElseThrow(EntityNotFoundException::new);
        file.setScanStatus(scanStatus);
        fileScanRepository.save(file);
    }

    private void scanNextFile() throws EntityNotFoundException {
        String fileId = AntivirusQueue.getInstance().getTopAndRemove();
        if (fileId != null) {
            try {
                boolean result = antivirusConnectorService.scanFile(getPath(fileId, fileStorageProperties.getQuarantineDir()));
                if (result) {
                    updateStatus(fileId, ScanStatusEnum.OK);
                    copyFileToDestinationPath(fileId);
                } else {
                    updateStatus(fileId, ScanStatusEnum.VIRUS_FOUND);
                }
            } catch (IOException e) {
                e.printStackTrace();
                updateStatus(fileId, ScanStatusEnum.CANNOT_CHECK);
                AntivirusQueue.getInstance().add(fileId);
            }
        }
    }

    private void copyFileToDestinationPath(String fileId) throws IOException {
        Optional<FileScan> byFileId = fileScanRepository.findByFileId(fileId);
        if (byFileId.isPresent()) {
            Files.move(getPath(fileId, fileStorageProperties.getQuarantineDir()), Paths.get(byFileId.get().getDestinationPath()), StandardCopyOption.REPLACE_EXISTING);
        }
    }

    private Path getPath(String fileId, String dir) {
        return Paths.get(dir).toAbsolutePath().normalize().resolve(fileId).normalize();
    }

    @Scheduled(cron = "0 1 1 * * ?")
    public void syncFiles() {
        List<String> filesNotAddedToScan = fileRepository.getFilesNotAddedToScan();
        filesNotAddedToScan.forEach(c -> {
            try {
                Files.move(getPath(c, fileStorageProperties.getUploadDir()), getPath(c, fileStorageProperties.getQuarantineDir()), StandardCopyOption.REPLACE_EXISTING);
                add(c, getPath(c, fileStorageProperties.getUploadDir()).toAbsolutePath().toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Scheduled(fixedDelay = 1000)
    public void scanFile() {
        if (!AntivirusQueue.getInstance().isScanning()) {
            try {
                AntivirusQueue.getInstance().setScanning(true);
                scanNextFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
            AntivirusQueue.getInstance().setScanning(false);
        }
    }
}
