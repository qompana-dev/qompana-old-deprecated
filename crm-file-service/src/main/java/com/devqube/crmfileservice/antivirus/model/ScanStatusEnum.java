package com.devqube.crmfileservice.antivirus.model;

public enum ScanStatusEnum {
    OK, VIRUS_FOUND, CREATED, CANNOT_CHECK
}
