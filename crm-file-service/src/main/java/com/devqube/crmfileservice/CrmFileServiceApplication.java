package com.devqube.crmfileservice;

import com.devqube.crmfileservice.antivirus.AntivirusProperties;
import com.devqube.crmshared.license.LicenseCheck;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@EnableEurekaClient
@EnableConfigurationProperties({
        FileStorageProperties.class,
        AntivirusProperties.class
})
@EnableFeignClients(basePackages = {"com.devqube.crmfileservice", "com.devqube.crmshared.association"})
@SpringBootApplication
@EnableAsync
@Import({LicenseCheck.class})
public class CrmFileServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrmFileServiceApplication.class, args);
    }

    @Bean(name = "previewConversionExecutor")
    public Executor getPreviewConversionAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(1);
        executor.setMaxPoolSize(5);
        executor.initialize();
        return executor;
    }

}
