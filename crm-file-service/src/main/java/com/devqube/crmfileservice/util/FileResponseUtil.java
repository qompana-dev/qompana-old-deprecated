package com.devqube.crmfileservice.util;

import com.devqube.crmfileservice.web.dto.FilePreviewDto;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.net.URLConnection;
import java.util.Optional;

public class FileResponseUtil {
    public static ResponseEntity<byte[]> createFileHttpResponse(FilePreviewDto preview) {
        String previewName = String.format("%s.pdf", preview.getOriginalFileName());
        String contentType = URLConnection.guessContentTypeFromName(previewName);
        MediaType mediaType = Optional.ofNullable(contentType)
                .map(MediaType::parseMediaType)
                .orElse(MediaType.APPLICATION_OCTET_STREAM);

        ContentDisposition contentDisposition = ContentDisposition.builder("inline")
                .filename(previewName)
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentDisposition(contentDisposition);

        return ResponseEntity.ok()
                .contentType(mediaType)
                .headers(headers)
                .body(preview.getFileData());
    }
}
