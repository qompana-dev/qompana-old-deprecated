package com.devqube.crmfileservice.web.dto;

import com.devqube.crmshared.search.CrmObjectType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileGroupActionDto {
    private Long groupId;
    private CrmObjectType groupType;
}
