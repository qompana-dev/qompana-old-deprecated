package com.devqube.crmfileservice.web;

import com.devqube.crmfileservice.exception.FileNotFoundException;
import com.devqube.crmfileservice.model.FileEvent;
import com.devqube.crmfileservice.service.FileEventService;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.access.annotation.AuthControllerCase;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FileHistoryController implements FileHistoryApi {

    private final FileEventService fileEventService;

    public FileHistoryController(FileEventService fileEventService) {
        this.fileEventService = fileEventService;
    }

    @Override
    @AuthController(name = "getFileWithHistory", actionFrontendId = "DocumentsComponent.showHistory", controllerCase = {
            @AuthControllerCase(type = "lead", name = "getFileWithHistoryForLead", actionFrontendId = "DocumentsInLeadComponent.showHistory"),
            @AuthControllerCase(type = "opportunity", name = "getFileWithHistoryForOpportunity", actionFrontendId = "DocumentsInOpportunityComponent.showHistory")
    })
    public ResponseEntity<List<FileEvent>> getFileWithHistory(String loggedAccountEmail, Long fileId) {
        try {
            List<FileEvent> fileHistory = fileEventService.getFileHistory(fileId);
            return ResponseEntity.ok(fileHistory);
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
