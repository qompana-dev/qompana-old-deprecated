package com.devqube.crmfileservice.web;

import com.devqube.crmfileservice.exception.FileNotFoundException;
import com.devqube.crmfileservice.exception.UnsupportedPreviewFormatException;
import com.devqube.crmfileservice.service.FileStorageService;
import com.devqube.crmfileservice.util.FileResponseUtil;
import com.devqube.crmfileservice.web.dto.FilePreviewDto;
import com.devqube.crmshared.access.annotation.AuthController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Slf4j
public class FilePreviewController implements FilesPreviewApi {


    @Autowired
    private FileStorageService fileStorageService;

    @Override
    @AuthController(name = "downloadFilePreview", actionFrontendId = "DocumentsComponent.preview")
    public ResponseEntity<byte[]> downloadFilePreview(String loggedAccountEmail, String fileName) {
        try {
            FilePreviewDto filePreview = fileStorageService.getFilePreview(loggedAccountEmail, fileName);
            return FileResponseUtil.createFileHttpResponse(filePreview);
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (AccessDeniedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        } catch (UnsupportedPreviewFormatException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
