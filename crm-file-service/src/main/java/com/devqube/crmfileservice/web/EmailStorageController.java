package com.devqube.crmfileservice.web;

import com.devqube.crmfileservice.service.FileAccessService;
import com.devqube.crmfileservice.service.FileStorageService;
import com.devqube.crmfileservice.util.FileResponseUtil;
import com.devqube.crmfileservice.web.dto.FilePreviewDto;
import com.devqube.crmshared.search.CrmObjectType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailStorageController implements EmailStorageApi {

    private final FileStorageService fileStorageService;
    private final FileAccessService fileAccessService;

    public EmailStorageController(FileStorageService fileStorageService, FileAccessService fileAccessService) {
        this.fileStorageService = fileStorageService;
        this.fileAccessService = fileAccessService;
    }

    @Override
    public ResponseEntity<String> uploadEmail(String name, byte[] emailFile) {
        return ResponseEntity.ok(fileStorageService.saveEmailMessage(name, emailFile));
    }

    @Override
    public ResponseEntity<byte[]> downloadEmailMessage(String name) {
        return FileResponseUtil.createFileHttpResponse(new FilePreviewDto(fileStorageService.getEmailMessage(name), name));
    }

    @Override
    public ResponseEntity<String> removeEmail(String name) {
        fileStorageService.removeEmailMessage(name);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Boolean> hasAccessToObject(CrmObjectType type, Long id, String loggedAccountEmail) {
        return ResponseEntity.ok(fileAccessService.assertHasAccessToCrmObject(type, id, loggedAccountEmail));
    }
}
