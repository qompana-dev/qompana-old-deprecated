package com.devqube.crmfileservice.web.dto;

import com.devqube.crmfileservice.model.File;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@AuthFilterEnabled
public class MobileFileOnListDTO {

    private Long id;
    private String fileId;
    private String name;
    private LocalDateTime created;
    private String format;
    private Set<FilePropertyDto> fileProperties;

    public MobileFileOnListDTO(File file) {
        id = file.getId();
        fileId = file.getName();
        name = file.getOriginalName();
        format = file.getFormat();
        created = file.getCreated();
        if (file.getFileProperties() != null) {
            fileProperties = file.getFileProperties().stream().map(FilePropertyDto::new).collect(Collectors.toSet());
        }
    }
}

