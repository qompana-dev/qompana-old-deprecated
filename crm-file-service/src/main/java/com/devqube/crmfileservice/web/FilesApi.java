
package com.devqube.crmfileservice.web;

import com.devqube.crmfileservice.model.FilePropertyType;
import com.devqube.crmfileservice.web.dto.FileDto;
import com.devqube.crmfileservice.web.dto.FileFormDto;
import com.devqube.crmfileservice.web.dto.FileSizeDto;
import com.devqube.crmfileservice.web.dto.TempFileDto;
import com.devqube.crmshared.file.dto.FileInfoDto;
import com.devqube.crmshared.search.CrmObjectType;
import io.swagger.annotations.*;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "Files")
public interface FilesApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Delete file", nickname = "deleteFile", notes = "Delete single file. Operation available only for file owner or role with admin profile")
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "File deleted successfully"),
        @ApiResponse(code = 403, message = "Access to file is forbidden"),
        @ApiResponse(code = 404, message = "File not found") })
    @RequestMapping(value = "/files/{fileName}",
        method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteFile(@ApiParam(required = true) @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @ApiParam(value = "fileName", required = true) @PathVariable("fileName") String fileName) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "delete files", nickname = "deleteFiles", notes = "Delete file list. Operation available only for file owner or role with admin profile")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Deleted files successfully"),
            @ApiResponse(code = 403, message = "Access to one of files is forbidden"),
            @ApiResponse(code = 404, message = "One of files not found")})
    @RequestMapping(value = "/files",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteFiles(@ApiParam(required=true) @RequestHeader(value="Logged-Account-Email") String loggedAccountEmail,@ApiParam(defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue="new ArrayList<>()") List<String> ids) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Download file", nickname = "downloadFile", notes = "Method used to download specific file", response = Resource.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Downloaded file", response = Resource.class),
        @ApiResponse(code = 403, message = "Access to file is forbidden"),
        @ApiResponse(code = 404, message = "File not found") })
    @RequestMapping(value = "/files/{fileName}",
        produces = { "*/*" },
        method = RequestMethod.GET)
    default ResponseEntity<Resource> downloadFile(@ApiParam(required = true) @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @ApiParam(value = "fileName", required = true) @PathVariable("fileName") String fileName) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get file info", nickname = "getFileInfo", notes = "Method used get information about file", response = FileDto.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "File information retrieved successfully", response = FileDto.class),
        @ApiResponse(code = 404, message = "File not found") })
    @RequestMapping(value = "/files/{fileName}/info",
        produces = { "application/json" },
        method = RequestMethod.GET)
    default ResponseEntity<FileDto> getFileInfo(@ApiParam(required = true) @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @ApiParam(value = "fileName", required = true) @PathVariable("fileName") String fileName) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"path\" : \"path\",  \"created\" : \"created\",  \"name\" : \"name\",  \"format\" : \"format\",  \"self\" : \"self\",  \"description\" : \"description\",  \"id\" : \"id\",  \"tags\" : [ \"tags\", \"tags\" ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
    @ApiOperation(value = "Get file sizes", nickname = "getFilesSize", notes = "Method used get information file size", response = FileSizeDto[].class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "File sizes retrieved successfully", response = FileSizeDto[].class)})
    @RequestMapping(value = "/files/size",
        produces = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<List<FileSizeDto>> getFilesSize(@RequestBody List<String> fileNames);

    @ApiOperation(value = "Get group's files", nickname = "getFilesInGroup", notes = "Get all files belonging to specific group (for example lead, opportunity)", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of files retrieved successfully", response = org.springframework.data.domain.Page.class),
            @ApiResponse(code = 404, message = "Group not found") })
    @RequestMapping(value = "/files/group-type/{groupType}/group-id/{groupId}",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<org.springframework.data.domain.Page> getFilesInGroup(@ApiParam(required=true) @RequestHeader(value="Logged-Account-Email") String loggedAccountEmail, @ApiParam(value = "Type of file group",required=true, defaultValue="null") @PathVariable("groupType") CrmObjectType groupType, @ApiParam(value = "Id of file group",required=true) @PathVariable("groupId") Long groupId, @ApiParam(defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable, @Valid @RequestParam(value = "search", required = false) String search) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get tags", nickname = "getTags", notes = "Get all tags which starts with prefix", response = String.class, responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "File information retrieved successfully", response = String.class, responseContainer = "List") })
    @RequestMapping(value = "/tags",
        produces = { "application/json" },
        method = RequestMethod.GET)
    default ResponseEntity<List<String>> getTags(@Valid @RequestParam(value = "starts-with", required = false) String startsWith) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "\"\"");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get user files", nickname = "getUserFiles", notes = "Get all logged user's files", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File information retrieved successfully", response = org.springframework.data.domain.Page.class) })
    @RequestMapping(value = "/files/mine",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<org.springframework.data.domain.Page> getUserFiles(@ApiParam(required=true) @RequestHeader(value="Logged-Account-Email") String loggedAccountEmail,@ApiParam(defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable, @Valid @RequestParam(value = "search", required = false) String search) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get user files for mobile", nickname = "getUserFilesForMobile", notes = "Get all logged user's files for mobile", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File information retrieved successfully", response = org.springframework.data.domain.Page.class) })
    @RequestMapping(value = "/mobile/files/mine",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<org.springframework.data.domain.Page> getUserFilesForMobile(@ApiParam(required=true) @RequestHeader(value="Logged-Account-Email") String loggedAccountEmail,
                                                                                       @ApiParam(defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable,
                                                                                       @Valid @RequestParam(value = "search", required = false) String search,
                                                                                       @ApiParam(value = "only with property type") @Valid @RequestParam(value = "filePropertyType", required = false) FilePropertyType filePropertyType,
                                                                                       @ApiParam(value = "only with property value") @Valid @RequestParam(value = "filePropertyValue", required = false) String filePropertyValue) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get associated files for mobile", nickname = "getAssociatedFilesForMobile", notes = "Get associated files for mobile", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File information retrieved successfully", response = org.springframework.data.domain.Page.class) })
    @RequestMapping(value = "/mobile/files/associated",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<org.springframework.data.domain.Page> getAssociatedFilesForMobile(@ApiParam(required=true) @RequestHeader(value="Logged-Account-Email") String loggedAccountEmail,
                                                                                       @ApiParam(defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable,
                                                                                       @Valid @RequestParam(value = "search", required = false) String search,
                                                                                       @ApiParam(value = "only with property type") @Valid @RequestParam(value = "filePropertyType", required = false) FilePropertyType filePropertyType,
                                                                                       @ApiParam(value = "only with property value") @Valid @RequestParam(value = "filePropertyValue", required = false) String filePropertyValue,
                                                                                       @ApiParam(value = "only associated with object id") @Valid @RequestParam(value = "associationId", required = false) Long associationId,
                                                                                       @ApiParam(value = "only associated with object type") @Valid @RequestParam(value = "associationType", required = false) CrmObjectType associationType) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update file information", nickname = "updateFileInfo", notes = "Method used to update file description or tags", response = FileDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File metadata successfully updated", response = FileDto.class),
            @ApiResponse(code = 403, message = "Access to file is forbidden"),
            @ApiResponse(code = 400, message = "Bad request") })
    @RequestMapping(value = "/files/{fileName}/info",
            produces = { "application/json" },
            consumes = { "application/json" },
            method = RequestMethod.PUT)
    default ResponseEntity<FileDto> updateFileInfo(@ApiParam(required=true) @RequestHeader(value="Logged-Account-Email") String loggedAccountEmail, @ApiParam(value = "fileName",required=true) @PathVariable("fileName") String fileName, @Valid @RequestBody FileFormDto fileFormDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"path\" : \"path\",  \"created\" : \"created\",  \"name\" : \"name\",  \"format\" : \"format\",  \"self\" : \"self\",  \"description\" : \"description\",  \"id\" : \"id\",  \"fileGroups\" : [ {    \"groupId\" : 6,    \"id\" : 0  }, {    \"groupId\" : 6,    \"id\" : 0  } ],  \"tags\" : [ \"tags\", \"tags\" ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Upload file", nickname = "uploadFile", notes = "Method used to upload file;", response = FileDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "File successfully uploaded", response = FileDto.class),
            @ApiResponse(code = 400, message = "Bad request") })
    @RequestMapping(value = "/files",
            produces = { "application/json" },
            consumes = { "multipart/form-data" },
            method = RequestMethod.POST)
    default ResponseEntity<FileDto> uploadFile(@ApiParam(required=true) @RequestHeader(value="Logged-Account-Email") String loggedAccountEmail, @ApiParam(value = "file detail") @Valid @RequestPart("file") MultipartFile file, @ApiParam(defaultValue="null") @RequestPart(value="fileData", required=false) FileFormDto fileData) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"path\" : \"path\",  \"created\" : \"created\",  \"name\" : \"name\",  \"format\" : \"format\",  \"self\" : \"self\",  \"description\" : \"description\",  \"id\" : \"id\",  \"fileGroups\" : [ {    \"groupId\" : 6,    \"id\" : 0  }, {    \"groupId\" : 6,    \"id\" : 0  } ],  \"tags\" : [ \"tags\", \"tags\" ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get all file info for user", nickname = "searchForFiles ", notes = "Method used get information about all user files", response = FileDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "All files information retrieved successfully", response = FileDto.class, responseContainer = "List") })
    @RequestMapping(value = "/files/info",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<List<FileDto>> searchForFiles(@ApiParam(required=true) @RequestHeader(value="Logged-Account-Email") String loggedAccountEmail, @Valid @RequestParam(value = "search", required = false) String search) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"path\" : \"path\",  \"created\" : \"created\",  \"name\" : \"name\",  \"format\" : \"format\",  \"self\" : \"self\",  \"description\" : \"description\",  \"id\" : \"id\",  \"tags\" : [ \"tags\", \"tags\" ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Upload new file version", nickname = "uploadNewFileVersion", notes = "Method used to upload new version of the file", response = FileDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "New file version successfully uploaded", response = FileDto.class),
            @ApiResponse(code = 404, message = "File not found")
    })
    @RequestMapping(value = "/files/{fileId}/new-version",
            produces = { "application/json" },
            consumes = { "multipart/form-data" },
            method = RequestMethod.POST)
    default ResponseEntity<FileDto> uploadNewFileVersion(@ApiParam(required=true) @RequestHeader(value="Logged-Account-Email") String loggedAccountEmail, @ApiParam(required=true) @PathVariable("fileId") Long fileId, @ApiParam(value = "file detail") @Valid @RequestPart("file") MultipartFile file) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"path\" : \"path\",  \"created\" : \"created\",  \"name\" : \"name\",  \"format\" : \"format\",  \"self\" : \"self\",  \"description\" : \"description\",  \"id\" : \"id\",  \"fileGroups\" : [ {    \"groupId\" : 6,    \"id\" : 0  }, {    \"groupId\" : 6,    \"id\" : 0  } ],  \"tags\" : [ \"tags\", \"tags\" ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get file list for search", nickname = "getFilesForSearch", notes = "Method used to get file list of files containing string", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all file names", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List") })
    @RequestMapping(value = "/files/search",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmshared.search.CrmObject>> getFilesForSearch(@NotNull @ApiParam(required = true) @Valid @RequestParam(value = "term") String term) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get files list by ids", nickname = "getFilesAsCrmObject", notes = "Method used to get files as crm objects by ids", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found files", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List") })
    @RequestMapping(value = "/files/crm-object",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmshared.search.CrmObject>> getFilesAsCrmObject(@ApiParam(defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue="new ArrayList<>()") List<Long> ids) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get file info by file id", nickname = "getFileInfoById", notes = "Method used to get file information by file id", response = FileDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File information retrieved successfully", response = FileDto.class),
            @ApiResponse(code = 404, message = "File not found") })
    @RequestMapping(value = "/files/by-id/{fileId}/info",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<FileDto> getFileInfoById(@ApiParam(required=true) @RequestHeader(value="Logged-Account-Email") String loggedAccountEmail, @ApiParam(required=true) @PathVariable("fileId") Long fileId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"path\" : \"path\",  \"created\" : \"created\",  \"name\" : \"name\",  \"format\" : \"format\",  \"self\" : \"self\",  \"description\" : \"description\",  \"id\" : \"id\",  \"fileGroups\" : [ {    \"groupId\" : 6,    \"id\" : 0  }, {    \"groupId\" : 6,    \"id\" : 0  } ],  \"tags\" : [ \"tags\", \"tags\" ]}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiIgnore
    @RequestMapping(value = "/internal/files/{fileId}",
            produces = { "application/json" },
            consumes = { "application/json" },
            method = RequestMethod.PUT)
    default ResponseEntity<FileInfoDto> setFileInfoInternal(@PathVariable("fileId") Long fileId, @Valid @RequestBody FileInfoDto fileInfoDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"name\" : \"name\",  \"format\" : \"format\",  \"id\" : \"id\",  \"fileGroups\" : [ {    \"type\" : \"type\",    \"value\" : \"value\"  }, {    \"type\" : \"type\",    \"value\" : \"value\"  } ],  \"fileId\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiIgnore
    @RequestMapping(value = "/internal/files/{fileId}",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<FileInfoDto> getFileInfoInternal(@PathVariable("fileId") Long fileId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"name\" : \"name\",  \"format\" : \"format\",  \"id\" : \"id\",  \"fileGroups\" : [ {    \"type\" : \"type\",    \"value\" : \"value\"  }, {    \"type\" : \"type\",    \"value\" : \"value\"  } ],  \"fileId\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiIgnore
    @RequestMapping(value = "/internal/files/name/{fileName}",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<FileInfoDto> getInternalFileInfoByName(@PathVariable("fileName") String fileName) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"name\" : \"name\",  \"format\" : \"format\",  \"id\" : \"id\",  \"fileGroups\" : [ {    \"type\" : \"type\",    \"value\" : \"value\"  }, {    \"type\" : \"type\",    \"value\" : \"value\"  } ],  \"fileId\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiIgnore
    @RequestMapping(value = "/internal/files/temp/name/{fileName}",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<FileInfoDto> getInternalTempFileInfoByName(@PathVariable("fileName") String fileName) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"name\" : \"name\",  \"format\" : \"format\",  \"id\" : \"id\",  \"fileGroups\" : [ {    \"type\" : \"type\",    \"value\" : \"value\"  }, {    \"type\" : \"type\",    \"value\" : \"value\"  } ],  \"fileId\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiIgnore
    @RequestMapping(value = "/internal/files/{fileId}",
            produces = { "application/json" },
            method = RequestMethod.DELETE)
    default ResponseEntity<FileInfoDto> deleteFileInfoInternal(@PathVariable("fileId") List<Long> fileId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"name\" : \"name\",  \"format\" : \"format\",  \"id\" : \"id\",  \"fileGroups\" : [ {    \"type\" : \"type\",    \"value\" : \"value\"  }, {    \"type\" : \"type\",    \"value\" : \"value\"  } ],  \"fileId\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Download temporary file", nickname = "downloadTempFile", notes = "Method used to download specific temporary file", response = Resource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Downloaded temporary file", response = Resource.class),
            @ApiResponse(code = 403, message = "Access to file is forbidden"),
            @ApiResponse(code = 404, message = "File not found") })
    @RequestMapping(value = "/files/temp/{fileId}",
            produces = { "*/*" },
            method = RequestMethod.GET)
    default ResponseEntity<Resource> downloadTempFile(@ApiParam(required=true) @RequestHeader(value="Logged-Account-Email") String loggedAccountEmail,@ApiParam(value = "fileId",required=true) @PathVariable("fileId") Long fileId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Change expiration time for user temporary files", nickname = "changeExpirationTime", notes = "Method used to change expiration time for user temporary files")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "set new expiration time for user files") })
    @RequestMapping(value = "/files/temp/change-expiration-time",
            method = RequestMethod.POST)
    default ResponseEntity<Void> changeExpirationTime(@ApiParam(required=true) @RequestHeader(value="Logged-Account-Email") String loggedAccountEmail) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Upload temp file", nickname = "uploadTempFile", notes = "Method used to upload temporary file", response = TempFileDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "File successfully uploaded", response = TempFileDto.class),
            @ApiResponse(code = 400, message = "Bad request") })
    @RequestMapping(value = "/files/temp",
            produces = { "application/json" },
            consumes = { "multipart/form-data" },
            method = RequestMethod.POST)
    default ResponseEntity<TempFileDto> uploadTempFile(@ApiParam(required=true) @RequestHeader(value="Logged-Account-Email") String loggedAccountEmail, @ApiParam(value = "file detail") @Valid @RequestPart("file") MultipartFile file, @ApiParam(defaultValue="null") @RequestPart(value="fileData", required=false)  com.devqube.crmfileservice.web.dto.TempFileFormDto fileData) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"originalName\" : \"originalName\",  \"author\" : \"author\",  \"format\" : \"format\",  \"description\" : \"description\",  \"id\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiIgnore
    @RequestMapping(value = "/internal/files/temp-file/{tempFileId}",
            produces = { "application/json" },
            consumes = { "application/json" },
            method = RequestMethod.PUT)
    default ResponseEntity<FileInfoDto> setTempFileInfoInternal(@PathVariable("tempFileId") Long tempFileId, @Valid @RequestBody FileInfoDto fileInfoDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"name\" : \"name\",  \"format\" : \"format\",  \"id\" : \"id\",  \"fileGroups\" : [ {    \"type\" : \"type\",    \"value\" : \"value\"  }, {    \"type\" : \"type\",    \"value\" : \"value\"  } ],  \"fileId\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiIgnore
    @RequestMapping(value = "/internal/files/ids/{fileIds}",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<List<FileInfoDto>> getFilesInfoInternal(@PathVariable("fileIds") List<Long> fileIds) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"name\" : \"name\",  \"format\" : \"format\",  \"id\" : \"id\",  \"fileGroups\" : [ {    \"type\" : \"type\",    \"value\" : \"value\"  }, {    \"type\" : \"type\",    \"value\" : \"value\"  } ],  \"fileId\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiIgnore
    @RequestMapping(value = "/internal/email/attachment/{name}",
            produces = { "application/json" },
            method = RequestMethod.POST)
    default ResponseEntity<FileDto> uploadAttachmentInternal(@RequestHeader(value="Logged-Account-Email") String loggedAccountEmail, @PathVariable("name") String name, @RequestBody byte[] attachment) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"name\" : \"name\",  \"format\" : \"format\",  \"id\" : \"id\",  \"fileGroups\" : [ {    \"type\" : \"type\",    \"value\" : \"value\"  }, {    \"type\" : \"type\",    \"value\" : \"value\"  } ],  \"fileId\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
