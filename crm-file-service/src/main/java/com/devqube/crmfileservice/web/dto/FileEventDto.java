package com.devqube.crmfileservice.web.dto;

import com.devqube.crmfileservice.model.FileEvent;
import com.devqube.crmfileservice.model.FileEventType;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class FileEventDto {

    private FileEventType eventType;
    private String value1;
    private String value2;
    private String accountEmail;
    private Long correlationId;
    private LocalDateTime created;

    public FileEventDto(FileEvent fileEvent) {
        this.eventType = fileEvent.getEventType();
        this.value1 = fileEvent.getValue1();
        this.value2 = fileEvent.getValue2();
        this.accountEmail = fileEvent.getAccountEmail();
        this.correlationId = fileEvent.getCorrelationId();
        this.created = fileEvent.getCreated();
    }
}
