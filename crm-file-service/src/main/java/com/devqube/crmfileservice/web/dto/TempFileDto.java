package com.devqube.crmfileservice.web.dto;

import com.devqube.crmfileservice.model.TempFile;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TempFileDto {

    private Long id;
    private String originalName;
    private String format;
    private String description;
    private String author;
    private Long size;

    public TempFileDto(TempFile file) {
        id = file.getId();
        originalName = file.getOriginalName();
        format = file.getFormat();
        description = file.getDescription();
        author = file.getAccountEmail();
        size = file.getSize();
    }
}
