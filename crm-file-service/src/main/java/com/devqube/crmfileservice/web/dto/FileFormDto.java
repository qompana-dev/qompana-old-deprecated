package com.devqube.crmfileservice.web.dto;

import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.model.Tag;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.search.CrmObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@AuthFilterEnabled
public class FileFormDto {

    @AuthFields(list = {
            @AuthField(frontendId = "DocumentsEditComponent.description", controller = "uploadFile"),
            @AuthField(frontendId = "DocumentsAddComponent.description", controller = "updateFileInfo"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.description", controller = "uploadFileForLead"),
            @AuthField(frontendId = "DocumentsAddInLeadComponent.description", controller = "updateFileInfoForLead"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.description", controller = "uploadFileForLeadMini"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.description", controller = "uploadFileForOpportunity"),
            @AuthField(frontendId = "DocumentsAddInOpportunityComponent.description", controller = "updateFileInfoForOpportunity"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.description", controller = "uploadFileForOpportunityMini"),
            @AuthField(frontendId = "DocumentsEditInProductComponent.description", controller = "uploadFileForProduct"),
            @AuthField(frontendId = "DocumentsAddInProductComponent.description", controller = "updateFileInfoForProduct"),
            @AuthField(frontendId = "DocumentsEditInProductComponent.description", controller = "uploadFileForProductMini")
    })
    private String description;

    @AuthFields(list = {
            @AuthField(frontendId = "DocumentsEditComponent.tags", controller = "uploadFile"),
            @AuthField(frontendId = "DocumentsAddComponent.tags", controller = "updateFileInfo"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.tags", controller = "uploadFileForLead"),
            @AuthField(frontendId = "DocumentsAddInLeadComponent.tags",controller = "updateFileInfoForLead"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.tags", controller = "uploadFileForLeadMini"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.tags", controller = "uploadFileForOpportunity"),
            @AuthField(frontendId = "DocumentsAddInOpportunityComponent.tags",controller = "updateFileInfoForOpportunity"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.tags", controller = "uploadFileForOpportunityMini"),
            @AuthField(frontendId = "DocumentsEditInProductComponent.tags", controller = "uploadFileForProduct"),
            @AuthField(frontendId = "DocumentsAddInProductComponent.tags",controller = "updateFileInfoForProduct"),
            @AuthField(frontendId = "DocumentsEditInProductComponent.tags", controller = "uploadFileForProductMini")
    })
    private Set<String> tags;
//InOpportunity
    @AuthFields(list = {
            @AuthField(frontendId = "DocumentsEditComponent.assignedTo", controller = "uploadFile"),
            @AuthField(frontendId = "DocumentsAddComponent.assignedTo", controller = "updateFileInfo"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.assignedTo", controller = "uploadFileForLead"),
            @AuthField(frontendId = "DocumentsAddInLeadComponent.assignedTo", controller = "updateFileInfoForLead"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.assignedTo", controller = "uploadFileForLeadMini"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.assignedTo", controller = "uploadFileForOpportunity"),
            @AuthField(frontendId = "DocumentsAddInOpportunityComponent.assignedTo", controller = "updateFileInfoForOpportunity"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.assignedTo", controller = "uploadFileForOpportunityMini"),
            @AuthField(frontendId = "DocumentsEditInProductComponent.assignedTo", controller = "uploadFileForProduct"),
            @AuthField(frontendId = "DocumentsAddInProductComponent.assignedTo", controller = "updateFileInfoForProduct"),
            @AuthField(frontendId = "DocumentsEditInProductComponent.assignedTo", controller = "uploadFileForProductMini")
    })
    private Set<CrmObject> assignedObjects;

    @AuthFields(list = {
            @AuthField(controller = "uploadFile"),
            @AuthField(controller = "updateFileInfo"),
            @AuthField(controller = "uploadFileForLead"),
            @AuthField(controller = "updateFileInfoForLead"),
            @AuthField(controller = "uploadFileForLeadMini"),
            @AuthField(controller = "uploadFileForOpportunity"),
            @AuthField(controller = "updateFileInfoForOpportunity"),
            @AuthField(controller = "uploadFileForOpportunityMini")
    })
    private Set<FilePropertyDto> properties;

    public FileFormDto(File file, List<AssociationDto> associationList) {
        this(file);
        this.assignedObjects = associationList
                .stream().map(c -> new CrmObject(c.getDestinationId(), c.getDestinationType(), null, null))
                .collect(Collectors.toSet());
    }
    public FileFormDto(File file) {
        this.description = file.getDescription();
        this.tags = file.getTags().stream().map(Tag::getName).collect(Collectors.toSet());
        this.properties = file.getFileProperties().stream().map(FilePropertyDto::new).collect(Collectors.toSet());
    }
}
