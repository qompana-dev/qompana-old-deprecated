package com.devqube.crmfileservice.web;

import com.devqube.crmshared.search.CrmObjectType;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@Api(value = "EmailStorageApi")
public interface EmailStorageApi {


    @ApiOperation(value = "Upload email message", nickname = "uploadEmail", notes = "Method used to upload email message;", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "File successfully uploaded", response = String.class),
            @ApiResponse(code = 400, message = "Bad request") })
    @RequestMapping(value = "/internal/email/file/{name}",
            method = RequestMethod.POST)
    default ResponseEntity<String> uploadEmail(@PathVariable("name") String name, @RequestBody byte[] emailFile) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Remove email message", nickname = "removeEmail", notes = "Method used to upload remove message;", response = Void.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "File successfully removed", response = String.class),
            @ApiResponse(code = 400, message = "Bad request") })
    @RequestMapping(value = "/internal/email/file/{name}",
            method = RequestMethod.DELETE)
    default ResponseEntity<String> removeEmail(@PathVariable("name") String name) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Download email message", nickname = "downloadEmailMessage", notes = "Method used to download email message", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Email message", response = byte[].class),
            @ApiResponse(code = 404, message = "Email not found") })
    @RequestMapping(value = "/internal/email/file/{name}",
            method = RequestMethod.GET)
    default ResponseEntity<byte[]> downloadEmailMessage(@ApiParam(value = "name", required = true) @PathVariable("name") String name) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "User has access to object during attaching mail", nickname = "hasAccessToObject", notes = "Method used to check access to object", response = Boolean.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Email message", response = Boolean.class)})
    @RequestMapping(value = "/internal/email/access/type/{type}/id/{id}",
            method = RequestMethod.GET)
    default ResponseEntity<Boolean> hasAccessToObject(@ApiParam(value = "type", required = true) @PathVariable("type") CrmObjectType type, @ApiParam(value = "id", required = true) @PathVariable("id") Long id, @ApiParam(required = true) @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
