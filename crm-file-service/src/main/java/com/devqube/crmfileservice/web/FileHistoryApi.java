
package com.devqube.crmfileservice.web;

import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "FileHistory")
public interface FileHistoryApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Get file history", nickname = "getFileWithHistory", notes = "Method used to get file history", response = com.devqube.crmfileservice.model.FileEvent.class, responseContainer = "List")
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "File with history retrieved successfully", response = com.devqube.crmfileservice.model.FileEvent.class, responseContainer = "List"),
        @ApiResponse(code = 404, message = "File not found") })
    @RequestMapping(value = "/files/{fileId}/history",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmfileservice.model.FileEvent>> getFileWithHistory(@ApiParam(required = true) @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @ApiParam(value = "fileId", required = true) @PathVariable("fileId") Long fileId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
