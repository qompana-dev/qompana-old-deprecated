package com.devqube.crmfileservice.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class FilePreviewDto {
    private byte[] fileData;
    private String originalFileName;
}
