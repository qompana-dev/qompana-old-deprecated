package com.devqube.crmfileservice.web;

import com.devqube.crmfileservice.exception.FileGroupNotFoundException;
import com.devqube.crmfileservice.exception.FileNotFoundException;
import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.model.FilePropertyType;
import com.devqube.crmfileservice.model.TempFile;
import com.devqube.crmfileservice.service.FileMetadataService;
import com.devqube.crmfileservice.service.FileStorageService;
import com.devqube.crmfileservice.web.dto.*;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.access.annotation.AuthControllerCase;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.file.dto.FileInfoDto;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class FileController implements FilesApi {

    private final FileStorageService fileStorageService;
    private final FileMetadataService fileMetadataService;

    public FileController(FileStorageService fileStorageService, FileMetadataService fileMetadataService) {
        this.fileStorageService = fileStorageService;
        this.fileMetadataService = fileMetadataService;
    }


    @Override
    @AuthController(name = "uploadNewFileVersion", actionFrontendId = "DocumentsComponent.uploadNewVersion", controllerCase = {
            @AuthControllerCase(type = "lead", name = "uploadNewFileVersionForLead", actionFrontendId = "DocumentsInLeadComponent.uploadNewVersion"),
            @AuthControllerCase(type = "opportunity", name = "uploadNewFileVersionForOpportunity", actionFrontendId = "DocumentsInOpportunityComponent.uploadNewVersion")
    })
    public ResponseEntity<FileDto> uploadNewFileVersion(String loggedAccountEmail, Long fileId, @Valid MultipartFile file) {
        try {
            File fileResult = this.fileStorageService.updateFileVersion(loggedAccountEmail, fileId, file);
            return ResponseEntity.status(HttpStatus.OK).body(new FileDto(fileResult));
        } catch (FileNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @Override
    @AuthController(name = "uploadFile", actionFrontendId = "DocumentsAddComponent.save", controllerCase = {
            @AuthControllerCase(type = "lead", name = "uploadFileForLead", actionFrontendId = "DocumentsAddInLeadComponent.save"),
            @AuthControllerCase(type = "leadMini", name = "uploadFileForLeadMini", actionFrontendId = "LeadFilesMiniComponent.addFile"),
            @AuthControllerCase(type = "opportunity", name = "uploadFileForOpportunity", actionFrontendId = "DocumentsAddInOpportunityComponent.save"),
            @AuthControllerCase(type = "product", name = "uploadFileForProduct", actionFrontendId = "DocumentsAddInProductComponent.save"),
            @AuthControllerCase(type = "opportunityMini", name = "uploadFileForOpportunityMini", actionFrontendId = "OpportunityFilesMiniComponent.addFile"),
            @AuthControllerCase(type = "productMini", name = "uploadFileForProductMini", actionFrontendId = "ProductFilesMiniComponent.addFile")
    })
    public ResponseEntity<FileDto> uploadFile(String loggedAccountEmail, @Valid MultipartFile file, FileFormDto fileData) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(fileStorageService.
                    saveFile(loggedAccountEmail, file, fileData));
        } catch (BadRequestException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (AccessDeniedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @Override
    @AuthController(name = "updateFileInfo", actionFrontendId = "DocumentsEditComponent.save", controllerCase = {
            @AuthControllerCase(type = "lead", name = "updateFileInfoForLead", actionFrontendId = "DocumentsEditInLeadComponent.save"),
            @AuthControllerCase(type = "opportunity", name = "updateFileInfoForOpportunity", actionFrontendId = "DocumentsEditInOpportunityComponent.save")
    })
    public ResponseEntity<FileDto> updateFileInfo(String loggedAccountEmail, String fileName, @Valid FileFormDto fileFormDto) {
        try {
            return ResponseEntity.ok(fileStorageService.updateFileMetadata(loggedAccountEmail, fileName, fileFormDto));
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (AccessDeniedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        }
    }

    @Override
    public ResponseEntity<Resource> downloadTempFile(String loggedAccountEmail, Long fileId) {
        try {
            Resource resource = fileStorageService.getTempFile(loggedAccountEmail, fileId);
            TempFile fileMetadata = fileStorageService.getTempFileMetadata(fileId);
            return createFileHttpResponse(fileMetadata.getOriginalName(), resource);
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (PermissionDeniedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @Override
    public ResponseEntity<Void> changeExpirationTime(String loggedAccountEmail) {
        fileStorageService.changeExpirationTime(loggedAccountEmail);
        return ResponseEntity.ok().build();
    }

    @Override
    @AuthController(name = "downloadFile", actionFrontendId = "DocumentsComponent.download", controllerCase = {
            @AuthControllerCase(type = "lead", name = "downloadFileForLead", actionFrontendId = "DocumentsInLeadComponent.download"),
            @AuthControllerCase(type = "opportunity", name = "downloadFileForOpportunity", actionFrontendId = "DocumentsInOpportunityComponent.download")
    })
    public ResponseEntity<Resource> downloadFile(String loggedAccountEmail, String fileName) {
        try {
            Resource resource = fileStorageService.getFile(loggedAccountEmail, fileName);
            File fileMetadata = fileStorageService.getFileMetadata(fileName);
            return createFileHttpResponse(fileMetadata.getOriginalName(), resource);
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (AccessDeniedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @Override
    public ResponseEntity<Void> deleteFile(String loggedAccountEmail, String fileName) {
        try {
            fileStorageService.deleteFile(loggedAccountEmail, fileName);
            return ResponseEntity.noContent().build();
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (AccessDeniedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @Override
    public ResponseEntity<Void> deleteFiles(String loggedAccountEmail, @Valid List<String> ids) {
        try {
            fileStorageService.deleteFiles(loggedAccountEmail, ids);
            return ResponseEntity.noContent().build();
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (AccessDeniedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @Override
    @AuthController(name = "getFileInfo", actionFrontendId = "DocumentsComponent.showDetails", controllerCase = {
            @AuthControllerCase(type = "lead", name = "getFileInfoForLead", actionFrontendId = "DocumentsInLeadComponent.showDetails"),
            @AuthControllerCase(type = "opportunity", name = "getFileInfoForOpportunity", actionFrontendId = "DocumentsInOpportunityComponent.showDetails")
    })
    public ResponseEntity<FileDto> getFileInfo(String loggedAccountEmail, String fileName) {
        try {
            return ResponseEntity.ok(fileStorageService.getFileDtoMetadata(fileName));
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public ResponseEntity<List<FileSizeDto>> getFilesSize(List<String> fileNames) {
        return ResponseEntity.ok(fileStorageService.getFilesSize(fileNames));
    }


    @Override
    @AuthController(name = "getFileInfoById", controllerCase = {
            @AuthControllerCase(type = "lead", name = "getFileInfoByIdForLead"),
            @AuthControllerCase(type = "opportunity", name = "getFileInfoByIdForOpportunity")
    })
    public ResponseEntity<FileDto> getFileInfoById(String loggedAccountEmail, Long fileId) {
        try {
            File fileMetadata = fileStorageService.getFileMetadataById(fileId);
            return ResponseEntity.ok(new FileDto(fileMetadata));
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    @AuthController(name = "getUserFiles")
    public ResponseEntity<Page> getUserFiles(String loggedAccountEmail, @Valid Pageable pageable, @Valid String search) {
        Page<FileDto> filesInGroup = fileStorageService.getAllUserFiles(loggedAccountEmail, pageable, search);
        return ResponseEntity.ok(filesInGroup);
    }

    @Override // todo permissions
    public ResponseEntity<Page> getUserFilesForMobile(String loggedAccountEmail, @Valid Pageable pageable, @Valid String search, @Valid FilePropertyType filePropertyType, @Valid String filePropertyValue) {
        Page<MobileFileOnListDTO> filesInGroup = fileStorageService.getUserFilesForMobile(loggedAccountEmail, pageable, search, filePropertyType, filePropertyValue);
        return ResponseEntity.ok(filesInGroup);
    }

    @Override // todo permissions
    public ResponseEntity<Page> getAssociatedFilesForMobile(String loggedAccountEmail, @Valid Pageable pageable, @Valid String search, @Valid FilePropertyType filePropertyType, @Valid String filePropertyValue, @Valid Long associationId, @Valid CrmObjectType associationType) {
        Page<MobileFileOnListDTO> filesInGroup = fileStorageService.getAssociatedFilesForMobile(loggedAccountEmail, pageable, search, filePropertyType, filePropertyValue, associationId, associationType);
        return ResponseEntity.ok(filesInGroup);
    }

    @Override
    @AuthController(name = "getFilesInGroup", controllerCase = {
            @AuthControllerCase(type = "lead", name = "getFilesInGroupForLead"),
            @AuthControllerCase(type = "leadMini", name = "getFilesInGroupForLeadMini", readFrontendId = "LeadFilesMiniComponent.view"),
            @AuthControllerCase(type = "opportunity", name = "getFilesInGroupForOpportunity"),
            @AuthControllerCase(type = "product", name = "getFilesInGroupForProduct"),
            @AuthControllerCase(type = "opportunityMini", name = "getFilesInGroupForOpportunityMini", readFrontendId = "OpportunityFilesMiniComponent.view"),
            @AuthControllerCase(type = "productMini", name = "getFilesInGroupForProductMini", readFrontendId = "ProductFilesMiniComponent.view")
    })
    public ResponseEntity<Page> getFilesInGroup(String loggedAccountEmail, CrmObjectType groupType, Long groupId, @Valid Pageable pageable, @Valid String search) {
        try {
            Page<FileDto> filesInGroup = fileStorageService.getFilesInGroup(groupType, groupId, pageable, search);
            return ResponseEntity.ok(filesInGroup);
        } catch (FileGroupNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public ResponseEntity<List<String>> getTags(@Valid String startsWith) {
        Set<String> tagsStartsWith = fileMetadataService.getTagsStartsWith(startsWith);
        return ResponseEntity.ok(new ArrayList<>(tagsStartsWith));
    }

    @Override
    public ResponseEntity<List<FileDto>> searchForFiles(String loggedAccountEmail, @Valid String search) {
        return ResponseEntity.ok(fileStorageService.getAllUserFiles(loggedAccountEmail, search));
    }

    private ResponseEntity<Resource> createFileHttpResponse(String originalName, Resource resource) {
        String contentType = URLConnection.guessContentTypeFromName(originalName);
        MediaType mediaType = Optional.ofNullable(contentType)
                .map(MediaType::parseMediaType)
                .orElse(MediaType.APPLICATION_OCTET_STREAM);

        ContentDisposition contentDisposition = ContentDisposition.builder("inline")
                .filename(originalName)
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentDisposition(contentDisposition);

        return ResponseEntity.ok()
                .contentType(mediaType)
                .headers(headers)
                .body(resource);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getFilesForSearch(@NotNull @Valid String term) {
        return new ResponseEntity<>(fileMetadataService.findAllContaining(term)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.file, Strings.nullToEmpty(e.getOriginalName()), e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getFilesAsCrmObject(@Valid List<Long> ids) {
        return new ResponseEntity<>(fileMetadataService.findByIds(ids)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.file, Strings.nullToEmpty(e.getOriginalName()), e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<FileInfoDto> getFileInfoInternal(Long fileId) {
        try {
            return new ResponseEntity<>(fileStorageService.getFileInfo(fileId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<FileInfoDto> setFileInfoInternal(Long fileId, @Valid FileInfoDto fileInfoDto) {
        try {
            return new ResponseEntity<>(fileStorageService.setFileInfo(fileId, fileInfoDto), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<FileInfoDto> deleteFileInfoInternal(List<Long> fileIds) {
        fileStorageService.deleteFilesByIds(fileIds);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<TempFileDto> uploadTempFile(String loggedAccountEmail, @Valid MultipartFile file, TempFileFormDto fileData) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(fileStorageService.
                    saveTempFile(loggedAccountEmail, file, fileData));
        } catch (BadRequestException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (AccessDeniedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @Override
    public ResponseEntity<FileInfoDto> setTempFileInfoInternal(Long tempFileId, @Valid FileInfoDto fileInfoDto) {
        try {
            return new ResponseEntity<>(fileStorageService.setTempFileInfo(tempFileId, fileInfoDto), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<List<FileInfoDto>> getFilesInfoInternal(List<Long> fileIds) {
        return new ResponseEntity<>(fileStorageService.getFilesInfo(fileIds), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<FileInfoDto> getInternalFileInfoByName(String fileName) {
        try {
            return new ResponseEntity<>(fileStorageService.getFileInfoByName(fileName), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public ResponseEntity<FileInfoDto> getInternalTempFileInfoByName(String fileName) {
        try {
            return new ResponseEntity<>(fileStorageService.getTempFileInfoByName(fileName), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public ResponseEntity<FileDto> uploadAttachmentInternal(String loggedAccountEmail, String name, byte[] attachment) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(fileStorageService.saveFile(loggedAccountEmail, name, attachment));
        } catch (BadRequestException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (AccessDeniedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }
}
