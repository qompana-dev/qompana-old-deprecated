package com.devqube.crmfileservice.web;

import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

@Validated
@Api(value = "FilesPreview")
public interface FilesPreviewApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Download file preview", nickname = "downloadFilePreview", notes = "Method used to download specific file preview in pdf", response = byte[].class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "File preview in pdf", response = byte[].class),
        @ApiResponse(code = 403, message = "Access to file preview is forbidden"),
        @ApiResponse(code = 404, message = "File not found") })
    @RequestMapping(value = "/files/preview/{fileName}",
        produces = { "application/pdf" }, 
        method = RequestMethod.GET)
    default ResponseEntity<byte[]> downloadFilePreview(@ApiParam(required = true) @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @ApiParam(value = "fileName", required = true) @PathVariable("fileName") String fileName) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
