package com.devqube.crmfileservice.web.dto;

import com.devqube.crmshared.search.CrmObjectType;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class FileGroupDto {
    private Long groupId;
    private CrmObjectType groupType;
}
