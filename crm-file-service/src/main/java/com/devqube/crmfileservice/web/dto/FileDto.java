package com.devqube.crmfileservice.web.dto;

import com.devqube.crmfileservice.antivirus.model.ScanStatusEnum;
import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.model.Tag;
import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import com.devqube.crmshared.association.dto.AssociationDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.net.URLConnection;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@AuthFilterEnabled
public class FileDto {

    private String self;

    @AuthFields(list = {
            @AuthField(controller = "getFilesInGroup"),
            @AuthField(controller = "getFilesInGroupForLead"),
            @AuthField(controller = "getFilesInGroupForLeadMini"),
            @AuthField(controller = "getFilesInGroupForOpportunity"),
            @AuthField(controller = "getFilesInGroupForOpportunityMini"),
            @AuthField(controller = "getFileInfo"),
            @AuthField(controller = "getFileInfoForLead"),
            @AuthField(controller = "getFileInfoForOpportunity"),
            @AuthField(controller = "getFileInfoById"),
            @AuthField(controller = "getFileInfoByIdForLead"),
            @AuthField(controller = "getFileInfoByIdForOpportunity"),
            @AuthField(controller = "getUserFiles"),
            @AuthField(controller = "getFilesInGroupForProductMini"),
            @AuthField(controller = "getFilesInGroupForProduct")
    })
    private String id;

    @AuthFields(list = {
            @AuthField(controller = "getFilesInGroup"),
            @AuthField(controller = "getFilesInGroupForLead"),
            @AuthField(controller = "getFilesInGroupForLeadMini"),
            @AuthField(controller = "getFilesInGroupForOpportunity"),
            @AuthField(controller = "getFilesInGroupForOpportunityMini"),
            @AuthField(controller = "getFileInfo"),
            @AuthField(controller = "getFileInfoForOpportunity"),
            @AuthField(controller = "getFileInfoById"),
            @AuthField(controller = "getFileInfoByIdForOpportunity"),
            @AuthField(controller = "getUserFiles"),
            @AuthField(controller = "getFilesInGroupForProductMini"),
            @AuthField(controller = "getFilesInGroupForProduct")
    })
    private Long fileId;

    @AuthFields(list = {
            @AuthField(frontendId = "DocumentsListComponent.originalName", controller = "getFilesInGroup"),
            @AuthField(frontendId = "DocumentsListInLeadComponent.originalName", controller = "getFilesInGroupForLead"),
            @AuthField(controller = "getFilesInGroupForLeadMini"),
            @AuthField(frontendId = "DocumentsListInOpportunityComponent.originalName", controller = "getFilesInGroupForOpportunity"),
            @AuthField(controller = "getFilesInGroupForOpportunityMini"),
            @AuthField(frontendId = "DocumentsComponent.preview", controller = "getFileInfoById"),
            @AuthField(frontendId = "DocumentsInLeadComponent.preview", controller = "getFileInfoByIdForLead"),
            @AuthField(frontendId = "DocumentsInOpportunityComponent.preview", controller = "getFileInfoByIdForOpportunity"),
            @AuthField(controller = "getFileInfo"),
            @AuthField(controller = "getFileInfoForLead"),
            @AuthField(controller = "getFileInfoForOpportunity"),
            @AuthField(frontendId = "DocumentsListComponent.originalName", controller = "getUserFiles"),
            @AuthField(controller = "getFilesInGroupForProductMini"),
            @AuthField(frontendId = "DocumentsListInOpportunityComponent.originalName", controller = "getFilesInGroupForProduct")
    })
    private String name;
    private String path;

    @AuthFields(list = {
            @AuthField(frontendId = "DocumentsListComponent.format", controller = "getFilesInGroup"),
            @AuthField(frontendId = "DocumentsListInLeadComponent.format", controller = "getFilesInGroupForLead"),
            @AuthField(controller = "getFilesInGroupForLeadMini"),
            @AuthField(frontendId = "DocumentsListInOpportunityComponent.format", controller = "getFilesInGroupForOpportunity"),
            @AuthField(controller = "getFilesInGroupForOpportunityMini"),
            @AuthField(frontendId = "DocumentsComponent.preview", controller = "getFileInfoById"),
            @AuthField(frontendId = "DocumentsInLeadComponent.preview", controller = "getFileInfoByIdForLead"),
            @AuthField(frontendId = "DocumentsInOpportunityComponent.preview", controller = "getFileInfoByIdForOpportunity"),
            @AuthField(controller = "getFileInfo"),
            @AuthField(controller = "getFileInfoForLead"),
            @AuthField(controller = "getFileInfoForOpportunity"),
            @AuthField(frontendId = "DocumentsListComponent.format", controller = "getUserFiles"),
            @AuthField(controller = "getFilesInGroupForProductMini"),
            @AuthField(frontendId = "DocumentsListInOpportunityComponent.format", controller = "getFilesInGroupForProduct")
    })
    private String format;

    @AuthFields(list = {
            @AuthField(controller = "getFilesInGroup"),
            @AuthField(controller = "getFilesInGroupForLead"),
            @AuthField(controller = "getFilesInGroupForLeadMini"),
            @AuthField(controller = "getFilesInGroupForOpportunity"),
            @AuthField(controller = "getFilesInGroupForOpportunityMini"),
            @AuthField(frontendId = "DocumentsComponent.preview", controller = "getFileInfoById"),
            @AuthField(frontendId = "DocumentsInLeadComponent.preview", controller = "getFileInfoByIdForLead"),
            @AuthField(frontendId = "DocumentsInOpportunityComponent.preview", controller = "getFileInfoByIdForOpportunity"),
            @AuthField(controller = "getUserFiles"),
            @AuthField(controller = "getFilesInGroupForProductMini"),
            @AuthField(controller = "getFilesInGroupForProduct")
    })
    private String contentType;

    @AuthFields(list = {
            @AuthField(frontendId = "DocumentsEditComponent.description", controller = "getFileInfo"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.description", controller = "getFileInfoForLead"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.description", controller = "getFileInfoForOpportunity"),
            @AuthField(frontendId = "DocumentsComponent.preview", controller = "getFileInfoById"),
            @AuthField(frontendId = "DocumentsInLeadComponent.preview", controller = "getFileInfoByIdForLead"),
            @AuthField(frontendId = "DocumentsInOpportunityComponent.preview", controller = "getFileInfoByIdForOpportunity"),
            @AuthField(frontendId = "DocumentsEditComponent.description", controller = "updateFileInfo"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.description", controller = "updateFileInfoForLead"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.description", controller = "updateFileInfoForOpportunity")
    })
    private String description;

    @AuthFields(list = {
            @AuthField(frontendId = "DocumentsListComponent.authorFullName", controller = "getFilesInGroup"),
            @AuthField(frontendId = "DocumentsListInLeadComponent.authorFullName", controller = "getFilesInGroupForLead"),
            @AuthField(controller = "getFilesInGroupForLeadMini"),
            @AuthField(frontendId = "DocumentsListInOpportunityComponent.authorFullName", controller = "getFilesInGroupForOpportunity"),
            @AuthField(controller = "getFilesInGroupForOpportunityMini"),
            @AuthField(frontendId = "DocumentsComponent.preview", controller = "getFileInfoById"),
            @AuthField(frontendId = "DocumentsInLeadComponent.preview", controller = "getFileInfoByIdForLead"),
            @AuthField(frontendId = "DocumentsInOpportunityComponent.preview", controller = "getFileInfoByIdForOpportunity"),
            @AuthField(frontendId = "DocumentsEditComponent.authorFullName", controller = "getFileInfo"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.authorFullName", controller = "getFileInfoForLead"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.authorFullName", controller = "getFileInfoForOpportunity"),
            @AuthField(frontendId = "DocumentsListComponent.authorFullName", controller = "getUserFiles"),
            @AuthField(controller = "getFilesInGroupForProductMini"),
            @AuthField(frontendId = "DocumentsListInOpportunityComponent.authorFullName", controller = "getFilesInGroupForProduct")
    })
    private String author;

    @AuthFields(list = {
            @AuthField(frontendId = "DocumentsListComponent.authorFullName", controller = "getFilesInGroup"),
            @AuthField(frontendId = "DocumentsListInLeadComponent.authorFullName", controller = "getFilesInGroupForLead"),
            @AuthField(controller = "getFilesInGroupForLeadMini"),
            @AuthField(frontendId = "DocumentsListInOpportunityComponent.authorFullName", controller = "getFilesInGroupForOpportunity"),
            @AuthField(controller = "getFilesInGroupForOpportunityMini"),
            @AuthField(frontendId = "DocumentsComponent.preview", controller = "getFileInfoById"),
            @AuthField(frontendId = "DocumentsInLeadComponent.preview", controller = "getFileInfoByIdForLead"),
            @AuthField(frontendId = "DocumentsInOpportunityComponent.preview", controller = "getFileInfoByIdForOpportunity"),
            @AuthField(frontendId = "DocumentsEditComponent.authorFullName", controller = "getFileInfo"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.authorFullName", controller = "getFileInfoForLead"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.authorFullName", controller = "getFileInfoForOpportunity"),
            @AuthField(frontendId = "DocumentsListComponent.authorFullName", controller = "getUserFiles"),
            @AuthField(controller = "getFilesInGroupForProductMini"),
            @AuthField(frontendId = "DocumentsListInOpportunityComponent.authorFullName", controller = "getFilesInGroupForProduct")
    })
    private String authorFullName;

    @AuthFields(list = {
            @AuthField(frontendId = "DocumentsEditComponent.tags", controller = "getFileInfo"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.tags", controller = "getFileInfoForLead"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.tags", controller = "getFileInfoForOpportunity"),
            @AuthField(frontendId = "DocumentsComponent.preview", controller = "getFileInfoById"),
            @AuthField(frontendId = "DocumentsInLeadComponent.preview", controller = "getFileInfoByIdForLead"),
            @AuthField(frontendId = "DocumentsInOpportunityComponent.preview", controller = "getFileInfoByIdForOpportunity"),
            @AuthField(frontendId = "DocumentsEditComponent.tags", controller = "updateFileInfo"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.tags", controller = "updateFileInfoForLead"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.tags", controller = "updateFileInfoForOpportunity"),
            @AuthField(frontendId = "DocumentsListComponent.tags", controller = "getUserFiles"),
            @AuthField(frontendId = "DocumentsListInLeadComponent.tags", controller = "getFilesInGroupForLead"),
            @AuthField(frontendId = "DocumentsListInOpportunityComponent.tags", controller = "getFilesInGroupForOpportunity"),
    })
    private Set<String> tags;

    @AuthFields(list = {
            @AuthField(frontendId = "DocumentsListComponent.created", controller = "getFilesInGroup"),
            @AuthField(frontendId = "DocumentsListInLeadComponent.created", controller = "getFilesInGroupForLead"),
            @AuthField(controller = "getFilesInGroupForLeadMini"),
            @AuthField(frontendId = "DocumentsListInOpportunityComponent.created", controller = "getFilesInGroupForOpportunity"),
            @AuthField(controller = "getFilesInGroupForOpportunityMini"),
            @AuthField(frontendId = "DocumentsComponent.preview", controller = "getFileInfoById"),
            @AuthField(frontendId = "DocumentsInLeadComponent.preview", controller = "getFileInfoByIdForLead"),
            @AuthField(frontendId = "DocumentsInOpportunityComponent.preview", controller = "getFileInfoByIdForOpportunity"),
            @AuthField(frontendId = "DocumentsEditComponent.authorFullName", controller = "getFileInfo"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.authorFullName", controller = "getFileInfoForLead"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.authorFullName", controller = "getFileInfoForOpportunity"),
            @AuthField(frontendId = "DocumentsListComponent.created", controller = "getUserFiles"),
            @AuthField(controller = "getFilesInGroupForProductMini"),
            @AuthField(frontendId = "DocumentsListInOpportunityComponent.created", controller = "getFilesInGroupForProduct")
    })
    private LocalDateTime created;

    @AuthFields(list = {
            @AuthField(frontendId = "DocumentsEditComponent.assignedTo", controller = "getFileInfo"),
            @AuthField(frontendId = "DocumentsEditInLeadComponent.assignedTo", controller = "getFileInfoForLead"),
            @AuthField(frontendId = "DocumentsEditInOpportunityComponent.assignedTo", controller = "getFileInfoForOpportunity"),
            @AuthField(frontendId = "DocumentsComponent.preview", controller = "getFileInfoById"),
            @AuthField(frontendId = "DocumentsInLeadComponent.preview", controller = "getFileInfoByIdForLead"),
            @AuthField(frontendId = "DocumentsInOpportunityComponent.preview", controller = "getFileInfoByIdForOpportunity")
    })
    private Set<FileGroupDto> fileGroups;

    @AuthFields(list = {
            @AuthField(controller = "getFilesInGroup"),
            @AuthField(controller = "getFilesInGroupForLead"),
            @AuthField(controller = "getFilesInGroupForLeadMini"),
            @AuthField(controller = "getFilesInGroupForOpportunity"),
            @AuthField(controller = "getFilesInGroupForOpportunityMini"),
            @AuthField(controller = "getFileInfoById"),
            @AuthField(controller = "getFileInfoByIdForLead"),
            @AuthField(controller = "getFileInfoByIdForOpportunity"),
            @AuthField(controller = "getFileInfo"),
            @AuthField(controller = "getFileInfoForLead"),
            @AuthField(controller = "getFileInfoForOpportunity"),
            @AuthField(controller = "getUserFiles"),
            @AuthField(controller = "getFilesInGroupForProductMini"),
            @AuthField(controller = "getFilesInGroupForProduct")
    })
    private ScanStatusEnum scanStatus;

    @AuthFields(list = {
            @AuthField(controller = "getFilesInGroup"),
            @AuthField(controller = "getFilesInGroupForLead"),
            @AuthField(controller = "getFilesInGroupForLeadMini"),
            @AuthField(controller = "getFilesInGroupForOpportunity"),
            @AuthField(controller = "getFilesInGroupForOpportunityMini"),
            @AuthField(controller = "getFileInfoById"),
            @AuthField(controller = "getFileInfoByIdForLead"),
            @AuthField(controller = "getFileInfoByIdForOpportunity"),
            @AuthField(controller = "getFileInfo"),
            @AuthField(controller = "getFileInfoForLead"),
            @AuthField(controller = "getFileInfoForOpportunity"),
            @AuthField(controller = "getUserFiles"),
            @AuthField(controller = "getFilesInGroupForProductMini"),
            @AuthField(controller = "getFilesInGroupForProduct")
    })
    private Long size;

    public FileDto(File file, List<AssociationDto> associationList) {
        this(file);
        this.fileGroups = associationList.stream()
                .map(c -> new FileGroupDto(c.getDestinationId(), c.getDestinationType()))
                .collect(Collectors.toSet());
    }

    public FileDto(File file) {
        self = "/files/" + file.getName();
        id = file.getName();
        fileId = file.getId();
        name = file.getOriginalName();
        path = file.getPath();
        format = file.getFormat();
        contentType = URLConnection.guessContentTypeFromName(file.getOriginalName());
        description = file.getDescription();
        author = file.getAccountEmail();
        authorFullName = file.getAuthorFullName();
        tags = file.getTags().stream().map(Tag::getName).collect(Collectors.toSet());
        created = file.getCreated();
        scanStatus = file.getScanStatus();
        size = file.getSize();
    }
}
