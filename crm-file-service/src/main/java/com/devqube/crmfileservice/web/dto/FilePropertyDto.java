package com.devqube.crmfileservice.web.dto;

import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.model.FileProperty;
import com.devqube.crmfileservice.model.FilePropertyType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FilePropertyDto {
    private FilePropertyType propertyType;

    private String propertyValue;

    public FilePropertyDto(FileProperty fileProperty) {
        this.propertyType = fileProperty.getPropertyType();
        this.propertyValue = fileProperty.getPropertyValue();
    }

    public FileProperty toModel(File file) {
        FileProperty fileProperty = new FileProperty();
        fileProperty.setFile(file);
        fileProperty.setPropertyType(this.propertyType);
        fileProperty.setPropertyValue(this.propertyValue);
        return fileProperty;
    }
}
