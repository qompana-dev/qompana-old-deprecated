package com.devqube.crmfileservice.model;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
public class Tag {

    @Id
    @SequenceGenerator(name = "tag_seq", sequenceName = "tag_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tag_seq")
    private Long id;

    @NotNull
    @Column(nullable = false)
    @EqualsAndHashCode.Include
    @ToString.Include
    private String name;


    @ManyToMany(mappedBy = "tags")
    private Set<File> files = new HashSet<>();


    public Tag(@NotNull String name) {
        this.name = name;
    }
}
