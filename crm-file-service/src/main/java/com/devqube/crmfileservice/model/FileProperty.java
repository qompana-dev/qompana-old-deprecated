package com.devqube.crmfileservice.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class FileProperty {
    @Id
    @SequenceGenerator(name = "file_property_seq", sequenceName = "file_property_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "file_property_seq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "file_id")
    @EqualsAndHashCode.Include
    private File file;

    @Enumerated(EnumType.STRING)
    @EqualsAndHashCode.Include
    private FilePropertyType propertyType;

    @EqualsAndHashCode.Include
    private String propertyValue;

}
