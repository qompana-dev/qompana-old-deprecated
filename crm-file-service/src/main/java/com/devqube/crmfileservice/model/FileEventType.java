package com.devqube.crmfileservice.model;

public enum FileEventType {
    NEW_VERSION_UPLOADED,
    DESCRIPTION_CHANGED,
    TAGS_CHANGED,
    GROUPS_CHANGED,
    FILE_CREATED
}
