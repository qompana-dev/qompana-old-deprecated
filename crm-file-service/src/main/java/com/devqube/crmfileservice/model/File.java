package com.devqube.crmfileservice.model;

import com.devqube.crmfileservice.antivirus.model.ScanStatusEnum;
import com.devqube.crmshared.association.RemoveCrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class File {

    @Id
    @SequenceGenerator(name = "file_seq", sequenceName = "file_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "file_seq")
    private Long id;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "file2tag",
            joinColumns = @JoinColumn(name = "file_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    @EqualsAndHashCode.Exclude
    private Set<Tag> tags = new HashSet<>();

    @OneToMany(mappedBy = "file", orphanRemoval = true, cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @OrderBy("created DESC")
    @JsonIgnore
    private List<FileEvent> fileEvents = new ArrayList<>();

    private String name;
    private String originalName;
    private String path;
    private String format;
    private String accountEmail;
    private String authorFullName;
    private String description;
    private Long version = 0L;
    private Long size = 0L;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "file", orphanRemoval = true)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<FileProperty> fileProperties;

    @CreationTimestamp
    private LocalDateTime created;

    @UpdateTimestamp
    private LocalDateTime updated;

    @Enumerated(EnumType.STRING)
    @Formula("(select max(fs.scan_status) from file_scan as fs where fs.file_id = name)")
    private ScanStatusEnum scanStatus;

    @PreRemove
    public void preRemove() {
        if (this.id != null) {
            RemoveCrmObject.addObjectToRemove(CrmObjectType.file, this.id);
        }
    }
}
