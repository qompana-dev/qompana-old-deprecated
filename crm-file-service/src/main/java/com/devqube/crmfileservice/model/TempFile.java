package com.devqube.crmfileservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TempFile {
    @Id
    @SequenceGenerator(name = "temp_file_seq", sequenceName = "temp_file_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "temp_file_seq")
    private Long id;
    private String originalName;
    private String format;
    private String accountEmail;
    private String description;
    private LocalDateTime expirationTime;
    private Long size;
}
