package com.devqube.crmfileservice.model;

public enum FilePropertyType {
    TYPE_VOICE_RECORD,
    TYPE_PHOTO,
    AUDIO_LENGTH
}
