package com.devqube.crmfileservice.model;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@AuthFilterEnabled
public class FileEvent {

    @Id
    @SequenceGenerator(name = "file_event_seq", sequenceName = "file_event_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "file_event_seq")
    private Long id;

    @JsonIgnore
    @ManyToOne
    @ToString.Exclude
    private File file;

    @AuthFields(list = {
            @AuthField(controller = "getFileWithHistory"),
            @AuthField(controller = "getFileWithHistoryForLead")
    })
    @Enumerated(EnumType.STRING)
    private FileEventType eventType;

    @AuthFields(list = {
            @AuthField(controller = "getFileWithHistory"),
            @AuthField(controller = "getFileWithHistoryForLead")
    })
    private String value1;
    private String value2;

    @AuthFields(list = {
            @AuthField(frontendId = "DocumentsHistoryComponent.changeAuthor", controller = "getFileWithHistory"),
            @AuthField(frontendId = "DocumentsHistoryInLeadComponent.changeAuthor", controller = "getFileWithHistoryForLead")
    })
    private String accountEmail;

    @AuthFields(list = {
            @AuthField(controller = "getFileWithHistory"),
            @AuthField(controller = "getFileWithHistoryForLead")
    })
    private Long correlationId;

    @AuthFields(list = {
            @AuthField(frontendId = "DocumentsHistoryComponent.changeDate", controller = "getFileWithHistory"),
            @AuthField(frontendId = "DocumentsHistoryInLeadComponent.changeDate", controller = "getFileWithHistoryForLead")
    })
    @CreationTimestamp
    private LocalDateTime created;
}

