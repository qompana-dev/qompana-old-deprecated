package com.devqube.crmfileservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UnsupportedPreviewFormatException extends RuntimeException {
    public UnsupportedPreviewFormatException(String message) {
        super(message);
    }

    public UnsupportedPreviewFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
