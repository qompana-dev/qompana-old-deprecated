package com.devqube.crmfileservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class FileGroupNotFoundException extends RuntimeException {

    public FileGroupNotFoundException(String message) {
        super(message);
    }

    public FileGroupNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
