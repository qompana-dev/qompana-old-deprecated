package com.devqube.crmfileservice.exception;

public class FileRemovalException extends RuntimeException {
    public FileRemovalException(String message) {
        super(message);
    }

    public FileRemovalException(String message, Throwable cause) {
        super(message, cause);
    }
}
