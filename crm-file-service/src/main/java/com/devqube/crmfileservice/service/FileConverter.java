package com.devqube.crmfileservice.service;

import org.jodconverter.DocumentConverter;
import org.jodconverter.document.DefaultDocumentFormatRegistry;
import org.jodconverter.office.OfficeException;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

@Service
public class FileConverter {

    private final DocumentConverter converter;

    public FileConverter(DocumentConverter converter) {
        this.converter = converter;
    }


    public byte[] convertToPdf(InputStream inputStream, String originalFileFormat) throws OfficeException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        converter.convert(inputStream)
                .as(DefaultDocumentFormatRegistry.getFormatByExtension(originalFileFormat))
                .to(out)
                .as(DefaultDocumentFormatRegistry.PDF)
                .execute();
        return out.toByteArray();
    }
}
