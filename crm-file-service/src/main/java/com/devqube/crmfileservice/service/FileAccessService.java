package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.integration.BusinessClient;
import com.devqube.crmfileservice.model.File;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@Service
@Slf4j
public class FileAccessService {

    private final PermissionsService permissionsService;
    private final BusinessClient businessClient;

    public FileAccessService(PermissionsService permissionsService, BusinessClient businessClient) {
        this.permissionsService = permissionsService;
        this.businessClient = businessClient;
    }

    public boolean assertHasAccessToCrmObject(CrmObjectType type, Long id, String email) {
        Set<Long> idsForGroupType = getIdsForGroupType(type, email);
        return idsForGroupType.contains(id);
    }

    public Map<CrmObjectType, Set<Long>> getAccessGroupMap(String email) {
        Map<CrmObjectType, Set<Long>> result = new HashMap<>();
        Arrays.stream(CrmObjectType.values()).forEach(type -> result.put(type, getIdsForGroupType(type, email)));
        return result;
    }

    public void assertCanDeleteFile(String email, File fileMetadata) throws AccessDeniedException {
        if (!(isFileOwner(email, fileMetadata) || hasSufficientRightsToDelete(email))) {
            throw new AccessDeniedException("Insufficient rights to modify file " + fileMetadata.getName());
        }
    }

    public void assertHasAccessToGroups(String email, Set<CrmObject> assignedObjects) throws AccessDeniedException {
        if (assignedObjects == null) {
            return;
        }
        Map<CrmObjectType, List<CrmObject>> objectsPerType = groupByCrmObjectType(assignedObjects);
        for (CrmObjectType type : objectsPerType.keySet()) {
            Set<Long> providedIds = getProvidedIdsOf(type, objectsPerType);
            Set<Long> allowedIds = getIdsForGroupType(type, email);
            if (!allowedIds.containsAll(providedIds)) {
                throw new AccessDeniedException("Insufficient rights to associate file with selected groups");
            }
        }
    }

    private Set<Long> getIdsForGroupType(CrmObjectType fileGroup, String email) {
        try {
            switch (fileGroup) {
                case customer:
                    return businessClient.getAllCustomersWithAccess(email);
                case contact:
                    return businessClient.getAllContactsWithAccess(email);
                case lead:
                    return businessClient.getAllLeadWithAccess(email);
                case opportunity:
                    return businessClient.getAllOpportunityWithAccess(email);
                case product:
                    return businessClient.getAllProductsWithAccess(email);
                default:
                    return new HashSet<>();
            }
        } catch (Exception e) {
            return new HashSet<>();
        }
    }

    private boolean hasSufficientRightsToDelete(String email) {
        return permissionsService.hasWritePermission(email, "DocumentsComponent.delete");
    }

    private boolean isFileOwner(String email, File fileMetadata) {
        return email.equalsIgnoreCase(fileMetadata.getAccountEmail());
    }

    private Map<CrmObjectType, List<CrmObject>> groupByCrmObjectType(Set<CrmObject> assignedObjects) {
        return assignedObjects.stream().collect(groupingBy(CrmObject::getType));
    }

    private Set<Long> getProvidedIdsOf(CrmObjectType type, Map<CrmObjectType, List<CrmObject>> objectsPerType) {
        return objectsPerType.get(type).stream().map(crmObject -> crmObject.getId()).collect(Collectors.toSet());
    }

}
