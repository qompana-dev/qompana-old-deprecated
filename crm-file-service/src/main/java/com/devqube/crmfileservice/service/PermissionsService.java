package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.integration.UserClient;
import com.devqube.crmshared.access.model.ProfileDto;
import com.devqube.crmshared.access.util.AccessUtil;
import org.springframework.stereotype.Service;

@Service
public class PermissionsService {

    private final UserClient userClient;

    public PermissionsService(UserClient userClient) {
        this.userClient = userClient;
    }

    public boolean hasWritePermission(String email, String permission) {
        try {
            ProfileDto basicProfile = userClient.getBasicProfile(email);
            return AccessUtil.getWritePermissions(basicProfile).contains(permission);
        } catch (Exception e) {
            return false;
        }
    }

}
