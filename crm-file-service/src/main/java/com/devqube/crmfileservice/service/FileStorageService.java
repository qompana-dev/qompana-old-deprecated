package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.exception.FileNotFoundException;
import com.devqube.crmfileservice.exception.FileStorageException;
import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.model.FilePropertyType;
import com.devqube.crmfileservice.model.TempFile;
import com.devqube.crmfileservice.web.dto.*;
import com.devqube.crmshared.access.web.AccessService;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.file.dto.FileInfoDto;
import com.devqube.crmshared.search.CrmObjectType;
import com.google.common.base.Strings;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@EnableScheduling
public class FileStorageService {

    private static final long FIVE_MINUTE_IN_MS = 5 * 60 * 1000;

    private final FileMetadataService fileMetadataService;
    private final FileSystemService fileSystemService;
    private final FilePreviewService filePreviewService;
    private final FileAccessService fileAccessService;
    private final FileEventService fileEventService;
    private final AccessService accessService;
    private final AssociationClient associationClient;


    public FileStorageService(FileMetadataService fileMetadataService,
                              FileSystemService fileSystemService,
                              FilePreviewService filePreviewService,
                              FileAccessService fileAccessService,
                              FileEventService fileHistoryService,
                              AccessService accessService,
                              AssociationClient associationClient) {
        this.fileMetadataService = fileMetadataService;
        this.fileSystemService = fileSystemService;
        this.filePreviewService = filePreviewService;
        this.fileAccessService = fileAccessService;
        this.fileEventService = fileHistoryService;
        this.accessService = accessService;
        this.associationClient = associationClient;
    }

    public void removeEmailMessage(String name) {
        fileSystemService.removeEmailMessage(name);
    }

    public String saveEmailMessage(String name, byte[] data) {
        fileSystemService.saveEmailMessage(name, data);
        return name;
    }

    @Transactional
    public FileDto saveFile(String email, MultipartFile file, FileFormDto fileFormDto) throws BadRequestException {
        FileFormDto verifiedFileForm = accessService.assignAndVerify(fileFormDto);
        fileAccessService.assertHasAccessToGroups(email, verifiedFileForm.getAssignedObjects());
        File fileMetadata = saveNewFileMetadata(email, file, verifiedFileForm);
        fileSystemService.save(fileMetadata.getName(), file);
        filePreviewService.saveFilePreviewIfPossible(fileMetadata.getName(), file);
        return new FileDto(fileMetadata, associationClient.getAllByTypeAndId(CrmObjectType.file, fileMetadata.getId()));
    }

    @Transactional
    public FileDto saveFile(String email, String fileName, byte[] file) throws BadRequestException {
        InputStream is = new ByteArrayInputStream(file);
        FileFormDto formDto = getEmptyFileFormDto();
        File fileMetadata = saveNewFileMetadata(email, fileName, formDto, Long.valueOf(file.length));
        try {
            fileSystemService.save(fileMetadata.getName(), is);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName, ex);
        }
        filePreviewService.saveFilePreviewIfPossible(fileMetadata.getName(), is);
        return new FileDto(fileMetadata);
    }

    private FileFormDto getEmptyFileFormDto() {
        FileFormDto formDto = new FileFormDto();
        formDto.setDescription("");
        formDto.setAssignedObjects(new HashSet<>());
        formDto.setTags(new HashSet<>());
        formDto.setProperties(new HashSet<>());
        return formDto;
    }

    @Transactional
    public TempFileDto saveTempFile(String email, MultipartFile file, TempFileFormDto fileFormDto) throws BadRequestException {
        TempFile fileMetadata = saveNewTempFileMetadata(email, file, fileFormDto);
        fileSystemService.saveTemp(fileMetadata.getId().toString(), file);
        return new TempFileDto(fileMetadata);
    }

    @Transactional
    public FileDto updateFileMetadata(String email, String fileName, @Valid FileFormDto fileDto) throws BadRequestException {
        File existingFile = getFileMetadata(fileName);
        fileAccessService.assertHasAccessToGroups(email, fileDto.getAssignedObjects());
        FileFormDto verifiedFileForm = accessService.assignAndVerify(new FileFormDto(existingFile), fileDto);
        fileEventService.filePropertyChanged(existingFile, verifiedFileForm, email);
        return fileMetadataService.updateFile(fileName, verifiedFileForm);
    }

    @Transactional
    public File updateFileVersion(String email, Long fileId, MultipartFile multipartFile) {
        File file = fileMetadataService.updateVersionOfExistingFile(fileId);
        filePreviewService.saveFilePreviewIfPossible(file.getName(), multipartFile);
        fileSystemService.save(file.getName(), multipartFile);
        fileEventService.versionChanged(file, email, multipartFile.getSize());
        return file;
    }

    @Transactional
    public void deleteFile(String email, String fileName) throws AccessDeniedException {
        File fileMetadata = getFileMetadata(fileName);
        fileAccessService.assertCanDeleteFile(email, fileMetadata);
        fileMetadataService.deleteFileMetadata(fileName);
        fileSystemService.delete(fileName);
        filePreviewService.deletePreview(fileName);
    }

    @Transactional
    public void deleteFiles(String email, List<String> ids) throws AccessDeniedException {
        for (String id : ids) {
            File fileMetadata = getFileMetadata(id);
            fileAccessService.assertCanDeleteFile(email, fileMetadata);
        }
        for (String id : ids) {
            deleteFile(email, id);
        }
    }

    public Resource getTempFile(String email, Long fileId) throws PermissionDeniedException {
        TempFile tempFileMetadata = getTempFileMetadata(fileId);
        if (!tempFileMetadata.getAccountEmail().equals(email)) {
            throw new PermissionDeniedException();
        }
        return fileSystemService.loadTempFileAsResource(fileId);
    }

    public void changeExpirationTime(String email) {
        fileMetadataService.changeExpirationTime(email);
    }

    public Resource getFile(String email, String fileName) {
        File fileMetadata = getFileMetadata(fileName);
        return fileSystemService.loadFileAsResource(fileName);
    }

    public File getFileMetadata(String fileName) {
        Optional<File> byName = fileMetadataService.getFileData(fileName);
        if (byName.isEmpty()) {
            byName = fileEventService.findFileByNameInHistory(fileName);
        }
        return byName.orElseThrow(() -> new FileNotFoundException("File not found " + fileName));
    }

    public TempFile getTempFileMetadata(Long tempFileId) {
        return fileMetadataService.getTempFileData(tempFileId).orElseThrow(() -> new FileNotFoundException("Temporary file not found " + tempFileId));
    }

    public FileDto getFileDtoMetadata(String fileName) {
        File file = getFileMetadata(fileName);
        return new FileDto(file, associationClient.getAllByTypeAndId(CrmObjectType.file, file.getId()));
    }

    public List<FileDto> getUserFiles(String loggedAccountEmail) {
        List<File> userFiles = fileMetadataService.getUserFiles(loggedAccountEmail);
        return userFiles.stream().map(FileDto::new).collect(Collectors.toList());
    }

    public List<FileDto> getAllUserFiles(String loggedAccountEmail, String search) {
        Map<CrmObjectType, Set<Long>> groups = fileAccessService.getAccessGroupMap(loggedAccountEmail);
        return fileMetadataService.getAllFilesForUser(groups, loggedAccountEmail, search)
                .stream().map(FileDto::new).collect(Collectors.toList());
    }

    public Page<FileDto> getFilesInGroup(CrmObjectType groupType, Long groupId, Pageable pageable, String search) {
        Page<File> filesInGroup;
        if (Strings.emptyToNull(search) == null) {
            filesInGroup = fileMetadataService.getFilesInGroup(groupType, groupId, pageable);
        } else {
            filesInGroup = fileMetadataService.getFilesInGroupAndSearch(groupType, groupId, pageable, search);
        }
        return filesInGroup.map(FileDto::new);
    }

    public Page<FileDto> getAllUserFiles(String loggedAccountEmail, Pageable pageable, String search) {
        Map<CrmObjectType, Set<Long>> groups = fileAccessService.getAccessGroupMap(loggedAccountEmail);
        Page<File> filesInGroup;
        if (Strings.emptyToNull(search) == null) {
            filesInGroup = fileMetadataService.getAllFilesForUser(groups, loggedAccountEmail, pageable);
        } else {
            filesInGroup = fileMetadataService.getAllFilesForUserAndSearch(groups, loggedAccountEmail, pageable, search);
        }
        return filesInGroup.map(FileDto::new);
    }

    public Page<MobileFileOnListDTO> getUserFilesForMobile(String loggedAccountEmail, Pageable pageable, String search, FilePropertyType filePropertyType, String filePropertyValue) {
        Map<CrmObjectType, Set<Long>> groups = fileAccessService.getAccessGroupMap(loggedAccountEmail);
        return fileMetadataService
                .getAllFilesForUserAndSearchForMobile(groups, loggedAccountEmail, pageable, Strings.nullToEmpty(search), filePropertyType, filePropertyValue)
                .map(MobileFileOnListDTO::new);
    }

    public Page<MobileFileOnListDTO> getAssociatedFilesForMobile(String loggedAccountEmail, Pageable pageable, String search,
                                                                 FilePropertyType filePropertyType, String filePropertyValue,
                                                                 Long associationId, CrmObjectType associationType) {
        Set<Long> associatedIds = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(associationType, CrmObjectType.file, associationId)
                .stream().map(AssociationDto::getDestinationId).collect(Collectors.toSet());
        if (associatedIds.size() == 0) {
            associatedIds.add(null);
        }
        Map<CrmObjectType, Set<Long>> groups = fileAccessService.getAccessGroupMap(loggedAccountEmail);
        return fileMetadataService
                .getAllAssociatedFilesAndSearchForMobile(groups, loggedAccountEmail, pageable, Strings.nullToEmpty(search), associatedIds, filePropertyType, filePropertyValue)
                .map(MobileFileOnListDTO::new);
    }

    public byte[] getEmailMessage(String name) {
        return fileSystemService.loadEmailMessageAsByteArray(name);
    }

    public FilePreviewDto getFilePreview(String email, String fileName) throws AccessDeniedException {
        File fileMetadata = getFileMetadata(fileName);
        return filePreviewService.getFilePreview(fileMetadata);
    }

    private File saveNewFileMetadata(String ownerEmail, MultipartFile file, FileFormDto fileFormDto) {
        File fileMetadata = fileMetadataService.saveFileMetadata(ownerEmail, file.getOriginalFilename(), fileFormDto, file.getSize());
        fileEventService.newFileCreated(fileMetadata);
        return fileMetadata;
    }

    private File saveNewFileMetadata(String ownerEmail, String fileName, FileFormDto fileFormDto, Long size) {
        File fileMetadata = fileMetadataService.saveFileMetadata(ownerEmail, fileName, fileFormDto, size);
        fileEventService.newFileCreated(fileMetadata);
        return fileMetadata;
    }

    private TempFile saveNewTempFileMetadata(String ownerEmail, MultipartFile file, TempFileFormDto fileFormDto) {
        return fileMetadataService.saveTempFileMetadata(ownerEmail, file.getOriginalFilename(), fileFormDto, file.getSize());
    }

    public File getFileMetadataById(Long fileID) {
        Optional<File> byName = fileMetadataService.getFileById(fileID);
        return byName.orElseThrow(() -> new FileNotFoundException("File not found " + fileID));
    }

    public FileInfoDto getFileInfoByName(String fileName) throws EntityNotFoundException {
        return fileMetadataService.getFileInfoByName(fileName);
    }

    public FileInfoDto getTempFileInfoByName(String fileName) throws EntityNotFoundException {
        return fileMetadataService.getTempFileInfoByName(fileName);
    }

    public FileInfoDto getFileInfo(Long fileId) throws EntityNotFoundException {
        return fileMetadataService.getFileInfo(fileId);
    }

    public List<FileInfoDto> getFilesInfo(List<Long> fileId) {
        return fileMetadataService.getFilesInfo(fileId);
    }

    public FileInfoDto setFileInfo(Long fileId, FileInfoDto fileInfoDto) throws EntityNotFoundException {
        return fileMetadataService.setFileInfo(fileId, fileInfoDto);
    }

    @Transactional
    public FileInfoDto setTempFileInfo(Long tempFileId, FileInfoDto fileInfoDto) throws EntityNotFoundException {
        FileInfoDto result = fileMetadataService.moveTempFileToFile(tempFileId, fileInfoDto);
        fileSystemService.moveFromTempToFile(tempFileId.toString(), result.getId());
        return result;
    }

    @Transactional
    public void deleteFilesByIds(List<Long> ids) {
        for (Long id : ids) {
            deleteFileById(id);
        }
    }

    private void deleteFileById(Long id) {
        fileMetadataService.getFileById(id).ifPresent(file -> {
            fileMetadataService.deleteFileMetadata(file.getName());
            fileSystemService.delete(file.getName());
            filePreviewService.deletePreview(file.getName());
        });
    }

    @Scheduled(fixedDelay = FIVE_MINUTE_IN_MS)
    @Transactional
    public void removeOldTemporaryFiles() {
        LocalDateTime now = LocalDateTime.now();
        fileMetadataService.getAllByExpirationTimeBefore(now).forEach(file -> fileSystemService.deleteTemp(file.getId().toString()));
        fileMetadataService.deleteAllByExpirationTimeBefore(now);
    }

    public List<FileSizeDto> getFilesSize(List<String> fileIds) {
        return fileIds.stream().map(this::getFileSize).collect(Collectors.toList());
    }

    public FileSizeDto getFileSize(String fileName) {
        long size = 0L;
        try {
            size = fileSystemService.getFileSize(fileName);
        } catch (IOException ignore) {
        }
        return new FileSizeDto(fileName, size);
    }
}
