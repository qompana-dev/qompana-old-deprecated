package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.exception.FileNotFoundException;
import com.devqube.crmfileservice.exception.FileRemovalException;
import com.devqube.crmfileservice.exception.FileStorageException;
import com.devqube.crmfileservice.exception.UnsupportedPreviewFormatException;
import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.web.dto.FilePreviewDto;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.jodconverter.document.DefaultDocumentFormatRegistry;
import org.jodconverter.document.DocumentFormat;
import org.jodconverter.office.OfficeException;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.List;

@Service
@Slf4j
public class FilePreviewService {

    public final static String PREVIEW_DIRECTORY = "preview";
    public final static List ALLOWED_FORMATS = List.of(
            DefaultDocumentFormatRegistry.DOCX,
            DefaultDocumentFormatRegistry.DOC,
            DefaultDocumentFormatRegistry.PPT,
            DefaultDocumentFormatRegistry.PPTX,
            DefaultDocumentFormatRegistry.XLS,
            DefaultDocumentFormatRegistry.XLSX,
            DefaultDocumentFormatRegistry.TXT
    );

    private final FileSystemService fileSystemService;
    private final FileConverter converter;

    public FilePreviewService(FileSystemService fileSystemService, FileConverter converter) {
        this.fileSystemService = fileSystemService;
        this.converter = converter;
    }

    public FilePreviewDto getFilePreview(File file) {
        if (!isPreviewPossible(file.getOriginalName())) {
            throw new UnsupportedPreviewFormatException("Preview is not supported for provided file");
        }
        byte[] preview = getFilePreviewInBytes(file.getName(), file.getFormat());
        return FilePreviewDto.builder()
                .fileData(preview)
                .originalFileName(file.getOriginalName())
                .build();
    }

    @Async("previewConversionExecutor")
    public void saveFilePreview(String fileNameInSystem, MultipartFile file) {
        String originalFileFormat = FilenameUtils.getExtension(file.getOriginalFilename());
        try {
            byte[] bytes = converter.convertToPdf(file.getInputStream(), originalFileFormat);
            savePreview(fileNameInSystem, bytes);
        } catch (IOException | OfficeException e) {
            log.error("Error during saving file preview", e);
        }
    }

    @Async("previewConversionExecutor")
    public void saveFilePreview(String fileNameInSystem, InputStream inputStream) {
        String originalFileFormat = FilenameUtils.getExtension(fileNameInSystem);
        try {
            byte[] bytes = converter.convertToPdf(inputStream, originalFileFormat);
            savePreview(fileNameInSystem, bytes);
        } catch (IOException | OfficeException e) {
            log.error("Error during saving file preview", e);
        }
    }

    @Async("previewConversionExecutor")
    public void saveFilePreviewIfPossible(String fileNameInSystem, InputStream file) {
        if (isPreviewNeeded(fileNameInSystem) && isPreviewPossible(fileNameInSystem)) {
            saveFilePreview(fileNameInSystem, file);
        }
    }

    @Async("previewConversionExecutor")
    public void saveFilePreviewIfPossible(String fileNameInSystem, MultipartFile file) {
        if (isPreviewNeeded(file.getOriginalFilename()) && isPreviewPossible(file.getOriginalFilename())) {
            saveFilePreview(fileNameInSystem, file);
        }
    }

    public void deletePreview(String fileNameInSystem) {
        try {
            fileSystemService.delete(getPreviewFullName(fileNameInSystem));
        } catch (FileNotFoundException e) {
            log.info("File preview already removed");
        } catch (FileRemovalException e) {
            log.error("Error during saving file preview", e);
        }
    }

    private byte[] getFilePreviewInBytes(String fileName, String originalFileFormat) {
        byte[] out;
        try {
            out = getExistingPreview(fileName);
        } catch (FileNotFoundException | IOException e) {
            out = generatePreviewAndSave(fileName, originalFileFormat);
        }
        return out;
    }

    private byte[] generatePreviewAndSave(String fileName, String originalFileFormat) {
        try {
            byte[] out = generateFilePreview(fileName, originalFileFormat);
            trySavePreview(fileName, out);
            return out;
        } catch (OfficeException | IOException e) {
            throw new FileStorageException("Error during generating file preview");
        }
    }

    private void trySavePreview(String fileName, byte[] out) {
        try {
            savePreview(fileName, out);
        } catch (IOException e) {
            log.warn("Error during saving file preview");
        }
    }

    private byte[] getExistingPreview(String fileName) throws FileNotFoundException, IOException {
        Resource resource = fileSystemService.loadFileAsResource(getPreviewFullName(fileName));
        return IOUtils.toByteArray(resource.getInputStream());
    }

    private byte[] generateFilePreview(String fileName, String originalFileFormat) throws OfficeException, IOException {
        InputStream inputStream = fileSystemService.loadFileAsResource(fileName).getInputStream();
        return converter.convertToPdf(inputStream, originalFileFormat);
    }

    private void savePreview(String fileNameInSystem, byte[] bytes) throws IOException {
        fileSystemService.save(getPreviewFullName(fileNameInSystem), new ByteArrayInputStream(bytes));
    }

    private String getPreviewFullName(String name) {
        return String.format("%s/%s", PREVIEW_DIRECTORY, name);
    }

    private boolean isImage(String mimeType) {
        return Strings.nullToEmpty(mimeType).startsWith("image");
    }

    private boolean isPdf(String mimeType) {
        return "application/pdf".equalsIgnoreCase(mimeType);
    }

    private boolean isPreviewPossible(String originalFileName) {
        String extension = FilenameUtils.getExtension(originalFileName);
        DocumentFormat format = DefaultDocumentFormatRegistry.getFormatByExtension(extension);
        return format == null ? false : ALLOWED_FORMATS.contains(format);
    }

    private boolean isPreviewNeeded(String originalFileName) {
        String mimeType = URLConnection.guessContentTypeFromName(originalFileName);
        return !(isPdf(mimeType) || isImage(mimeType));
    }
}
