package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.exception.FileNotFoundException;
import com.devqube.crmfileservice.model.*;
import com.devqube.crmfileservice.repository.FileEventRepository;
import com.devqube.crmfileservice.repository.FileRepository;
import com.devqube.crmfileservice.web.dto.FileFormDto;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class FileEventService {

    private final FileEventRepository fileHistoryRepository;
    private final FileRepository fileRepository;
    private final AssociationClient associationClient;

    public FileEventService(FileEventRepository fileHistoryRepository, FileRepository fileRepository, AssociationClient associationClient) {
        this.fileHistoryRepository = fileHistoryRepository;
        this.fileRepository = fileRepository;
        this.associationClient = associationClient;
    }

    @Transactional
    public List<FileEvent> getFileHistory(Long fileId) {
        Optional<File> byId = fileRepository.findById(fileId);
        File file = byId.orElseThrow(() -> new FileNotFoundException("File not found"));
        return file.getFileEvents();
    }

    public Optional<File> findFileByNameInHistory(String fileName) {
        Optional<FileEvent> fileVersionChangeBeName = fileHistoryRepository.findFileVersionChangeByName(fileName);
        return fileVersionChangeBeName.map(FileEvent::getFile);
    }

    public void newFileCreated(File file) {
        long correlationId = System.currentTimeMillis();
        this.saveFileStateChanged(file, FileEventType.FILE_CREATED, file.getAccountEmail(), file.getName(), correlationId);
    }

    public void versionChanged(File file, String changedBy, Long newSize) {
        long correlationId = System.currentTimeMillis();
        file.setSize(newSize);
        this.saveFileStateChanged(file, FileEventType.NEW_VERSION_UPLOADED, changedBy, file.getName(), correlationId);
    }

    public void filePropertyChanged(File existingFile, FileFormDto update, String changedBy) {
        FileFormDto existing = new FileFormDto(existingFile, associationClient.getAllByTypeAndId(CrmObjectType.file, existingFile.getId()));
        long correlationId = System.currentTimeMillis();
        if (!StringUtils.equals(existing.getDescription(), update.getDescription())) {
            this.saveFileStateChanged(existingFile, FileEventType.DESCRIPTION_CHANGED, changedBy, null, correlationId);
        }

        if (!Sets.symmetricDifference(existing.getTags(), update.getTags()).isEmpty()) {
            this.saveFileStateChanged(existingFile, FileEventType.TAGS_CHANGED, changedBy, null, correlationId);
        }

        if (!Sets.symmetricDifference(existing.getAssignedObjects(), update.getAssignedObjects()).isEmpty()) {
            this.saveFileStateChanged(existingFile, FileEventType.GROUPS_CHANGED, changedBy, null, correlationId);
        }
    }


    private void saveFileStateChanged(File file, FileEventType fileEventType, String changedBy, String newValue1, Long correlationId) {
        FileEvent fileEvent = FileEvent.builder()
                .file(file)
                .eventType(fileEventType)
                .correlationId(correlationId)
                .accountEmail(changedBy)
                .value1(newValue1)
                .build();
        fileHistoryRepository.save(fileEvent);
    }

}
