package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.FileStorageProperties;
import com.devqube.crmfileservice.antivirus.AntivirusService;
import com.devqube.crmfileservice.exception.FileNotFoundException;
import com.devqube.crmfileservice.exception.FileRemovalException;
import com.devqube.crmfileservice.exception.FileStorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;


@Service
public class FileSystemService {

    private final Path fileStorageLocation;
    private final Path tempFileStorageLocation;
    private final Path emailMessagesFileStorageLocation;
    private final Path quarantineFileStorageLocation;

    @Autowired
    private AntivirusService antivirusService;

    @Autowired
    public FileSystemService(FileStorageProperties fileStorageProperties) throws FileStorageException {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
        this.tempFileStorageLocation = Paths.get(fileStorageProperties.getTempDir()).toAbsolutePath().normalize();
        this.emailMessagesFileStorageLocation = Paths.get(fileStorageProperties.getEmailMessagesDir()).toAbsolutePath().normalize();
        this.quarantineFileStorageLocation = Paths.get(fileStorageProperties.getQuarantineDir()).toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
            Files.createDirectories(this.fileStorageLocation.resolve(FilePreviewService.PREVIEW_DIRECTORY));
            Files.createDirectories(this.tempFileStorageLocation);
            Files.createDirectories(this.tempFileStorageLocation.resolve(FilePreviewService.PREVIEW_DIRECTORY));
            Files.createDirectories(this.emailMessagesFileStorageLocation);
            Files.createDirectories(this.emailMessagesFileStorageLocation.resolve(FilePreviewService.PREVIEW_DIRECTORY));
            Files.createDirectories(this.quarantineFileStorageLocation);
            Files.createDirectories(this.quarantineFileStorageLocation.resolve(FilePreviewService.PREVIEW_DIRECTORY));
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public void delete(String fileName) throws FileNotFoundException, FileRemovalException {
        try {
            Files.delete(getFilePath(fileName));
        } catch (NoSuchFileException e) {
            throw new FileNotFoundException("File not found " + fileName);
        } catch (IOException e) {
            throw new FileRemovalException("Unknown error when removing file " + fileName);
        }
    }

    public void deleteTemp(String tempFileName) throws FileNotFoundException, FileRemovalException {
        try {
            Files.delete(getTempFilePath(tempFileName));
        } catch (NoSuchFileException e) {
            throw new FileNotFoundException("File not found " + tempFileName);
        } catch (IOException e) {
            throw new FileRemovalException("Unknown error when removing file " + tempFileName);
        }
    }

    public void moveFromTempToFile(String tempFileName, String fileName) throws FileStorageException {
        try {
            Files.move(tempFileStorageLocation.resolve(tempFileName), fileStorageLocation.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new FileStorageException("Could not move file " + fileName, e);
        }
    }

    public void removeEmailMessage(String fileName) throws FileStorageException {
        Path emailMessagePath = getEmailMessagePath(fileName);
        if (emailMessagePath.toFile().exists()) {
            try {
                Files.delete(emailMessagePath);
            } catch (IOException ex) {
                throw new FileStorageException("Could not remove file " + fileName, ex);
            }
        }
    }
    public void saveEmailMessage(String fileName, byte[] data) throws FileStorageException {
        try (ByteArrayInputStream byteInputStream = new ByteArrayInputStream(data)) {
            Path targetLocation = getEmailMessagePath(fileName);
            Files.copy(byteInputStream, targetLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName, ex);
        }
    }

    public void saveTemp(String fileName, MultipartFile file) throws FileStorageException {
        try {
            Path targetLocation = getTempFilePath(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName, ex);
        }
    }

    public void save(String fileName, MultipartFile file) throws FileStorageException {
        try {
            this.save(fileName, file.getInputStream());
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName, ex);
        }
    }

    public void save(String fileName, InputStream inputStream) throws IOException {
        Path targetLocation = getFilePath(fileName);
        Path path = getQuarantinePath(fileName);
        Files.copy(inputStream, path, StandardCopyOption.REPLACE_EXISTING);
        antivirusService.add(fileName, targetLocation.toAbsolutePath().toString());
    }


    public Resource loadFileAsResource(String fileName) throws FileNotFoundException {
        try {
            Path filePath = getFilePath(fileName);
            byte[] bytes = Files.readAllBytes(filePath);
            Resource resource = new ByteArrayResource(bytes);
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileNotFoundException("File not found " + fileName);
            }
        } catch (IOException ex) {
            throw new FileNotFoundException("File not found " + fileName, ex);
        }

    }

    public byte[] loadEmailMessageAsByteArray(String name) throws FileNotFoundException {
        Path emailMessagePath = getEmailMessagePath(name);
        if (emailMessagePath.toFile().exists()) {
            try {
                return Files.readAllBytes(emailMessagePath);
            } catch (IOException ignore) {}
        }
        throw new FileNotFoundException("not found email message");
    }



    public Resource loadTempFileAsResource(Long fileId) throws FileNotFoundException {
        try {
            Path filePath = getTempFilePath(fileId.toString());
            byte[] bytes = Files.readAllBytes(filePath);
            Resource resource = new ByteArrayResource(bytes);
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileNotFoundException("Temporary file not found " + fileId);
            }
        } catch (IOException ex) {
            throw new FileNotFoundException("Temporary file not found " + fileId, ex);
        }

    }

    private Path getQuarantinePath(String fileName) {
        return this.quarantineFileStorageLocation.resolve(fileName).normalize();
    }

    private Path getFilePath(String fileName) {
        return this.fileStorageLocation.resolve(fileName).normalize();
    }

    private Path getEmailMessagePath(String fileName) {
        return this.emailMessagesFileStorageLocation.resolve(fileName).normalize();
    }

    private Path getTempFilePath(String fileName) {
        return this.tempFileStorageLocation.resolve(fileName).normalize();
    }


    public long getFileSize(String fileName) throws IOException {
        return Files.size(getFilePath(fileName));
    }

    public long getTempFileSize(String fileName) throws IOException {
        return Files.size(getTempFilePath(fileName));
    }
}
