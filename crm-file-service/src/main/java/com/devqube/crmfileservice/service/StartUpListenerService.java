package com.devqube.crmfileservice.service;

import com.devqube.crmshared.access.annotation.AuthControllerCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class StartUpListenerService {

    private final FileSizeInitializationService fileSizeInitializationService;
    private final TempFileSizeInitializationService tempFileSizeInitializationService;

    @Autowired
    public StartUpListenerService(FileSizeInitializationService fileSizeInitializationService,
                                  TempFileSizeInitializationService tempFileSizeInitializationService) {
        this.fileSizeInitializationService = fileSizeInitializationService;
        this.tempFileSizeInitializationService = tempFileSizeInitializationService;
    }

    @EventListener(ContextRefreshedEvent.class)
    public void contextRefreshedEvent() {
        fileSizeInitializationService.initialize();
        tempFileSizeInitializationService.initialize();
    }
}
