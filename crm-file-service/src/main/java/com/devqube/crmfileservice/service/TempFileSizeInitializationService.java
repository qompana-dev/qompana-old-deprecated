package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.model.TempFile;
import com.devqube.crmfileservice.repository.TempFileRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
class TempFileSizeInitializationService {

    private final TempFileRepository tempFileRepository;
    private final FileSystemService fileSystemService;

    public TempFileSizeInitializationService(TempFileRepository tempFileRepository,
                                             FileSystemService fileSystemService) {
        this.tempFileRepository = tempFileRepository;
        this.fileSystemService = fileSystemService;
    }

    public void initialize() {
        List<TempFile> files = tempFileRepository.findWithEmptySize();

        if(Objects.isNull(files) || files.isEmpty()) {
            log.info("does not found temp files for size update");
            return;
        }

        log.info("found " + files.size() + " temp files for size update");
        files.forEach(this::initializeSize);
    }

    private void initializeSize(TempFile tempFile) {
        try {
            long size = fileSystemService.getTempFileSize(tempFile.getId().toString());
            tempFile.setSize(size);
            tempFileRepository.save(tempFile);
            log.info("updated temp file size for " + tempFile.getId().toString());
        } catch (IOException e) {
            log.error("can not update temp file size for " + tempFile.getId().toString());
        }
    }
}
