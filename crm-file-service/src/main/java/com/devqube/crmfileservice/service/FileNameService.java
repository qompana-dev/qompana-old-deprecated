package com.devqube.crmfileservice.service;

import com.fasterxml.uuid.Generators;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class FileNameService {

    public String generateFileName() {
        UUID uuid = Generators.timeBasedGenerator().generate();
        return uuid.toString();
    }

}
