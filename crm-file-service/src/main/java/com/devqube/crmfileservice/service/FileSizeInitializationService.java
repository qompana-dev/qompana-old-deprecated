package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.repository.FileRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
class FileSizeInitializationService {

    private final FileRepository fileRepository;
    private final FileSystemService fileSystemService;

    public FileSizeInitializationService(FileRepository fileRepository,
                                         FileSystemService fileSystemService) {
        this.fileRepository = fileRepository;
        this.fileSystemService = fileSystemService;
    }

    public void initialize() {
        List<File> files = fileRepository.findWithEmptySize();

        if(Objects.isNull(files) || files.isEmpty()) {
            log.info("does not found files for size update");
            return;
        }

        log.info("found " + files.size() + " files for size update");
        files.forEach(this::initializeSize);
    }

    private void initializeSize(File file) {
        try {
            long size = fileSystemService.getFileSize(file.getName());
            file.setSize(size);
            fileRepository.save(file);
            log.info("updated file size for " + file.getName());
        } catch (IOException e) {
            log.error("can not update file size for " + file.getName());
        }
    }
}
