package com.devqube.crmfileservice.service;

import com.devqube.crmfileservice.exception.FileNotFoundException;
import com.devqube.crmfileservice.integration.UserClient;
import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.model.FilePropertyType;
import com.devqube.crmfileservice.model.Tag;
import com.devqube.crmfileservice.model.TempFile;
import com.devqube.crmfileservice.repository.FileRepository;
import com.devqube.crmfileservice.repository.TagRepository;
import com.devqube.crmfileservice.repository.TempFileRepository;
import com.devqube.crmfileservice.userfullnamesync.UserFullNameSyncUtil;
import com.devqube.crmfileservice.web.dto.FileDto;
import com.devqube.crmfileservice.web.dto.FileFormDto;
import com.devqube.crmfileservice.web.dto.FilePropertyDto;
import com.devqube.crmfileservice.web.dto.TempFileFormDto;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.file.dto.FileInfoDto;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.user.dto.AccountEmail;
import com.google.common.base.Strings;
import org.apache.commons.io.FilenameUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FileMetadataService {

    private final FileRepository fileRepository;
    private final TempFileRepository tempFileRepository;
    private final TagRepository tagRepository;
    private final FileNameService fileNameService;
    private final AssociationClient associationClient;
    private final UserClient userClient;

    public FileMetadataService(FileRepository fileRepository, TempFileRepository tempFileRepository, TagRepository tagRepository, FileNameService fileNameService, AssociationClient associationClient, UserClient userClient) {
        this.fileRepository = fileRepository;
        this.tempFileRepository = tempFileRepository;
        this.tagRepository = tagRepository;
        this.fileNameService = fileNameService;
        this.associationClient = associationClient;
        this.userClient = userClient;
    }

    public void deleteFileMetadata(String fileName) {
        Optional<File> byName = fileRepository.findByName(fileName);
        File file = byName.orElseThrow(() -> new FileNotFoundException("File not found " + fileName));
        fileRepository.delete(file);
    }

    @Transactional
    public File saveFileMetadata(String ownerEmail, String originalFilename, FileFormDto fileFormDto, Long size) {
        AccountEmail accountEmailByEmail = userClient.getAccountEmailByEmail(ownerEmail);
        File fileMetadata = createFile(ownerEmail, originalFilename, fileFormDto, size);
        fileMetadata.setAuthorFullName(UserFullNameSyncUtil.getAuthorFullName(accountEmailByEmail));
        Set<Tag> fileTags = getTags(fileFormDto.getTags());
        associateFileWithTags(fileMetadata, fileTags);
        associateFileWithProperties(fileMetadata, fileFormDto.getProperties());
        File save = fileRepository.save(fileMetadata);
        List<AssociationDetailDto> fileGroups = getGroups(save.getId(), fileFormDto.getAssignedObjects());
        associationClient.updateAssociationsForTypes(CrmObjectType.file, save.getId(), Arrays.asList(CrmObjectType.customer, CrmObjectType.lead, CrmObjectType.opportunity, CrmObjectType.contact, CrmObjectType.product), fileGroups);
        return save;
    }

    @Transactional
    public TempFile saveTempFileMetadata(String ownerEmail, String originalFilename, TempFileFormDto fileFormDto, Long size) {
        TempFile fileMetadata = createTempFile(ownerEmail, originalFilename, fileFormDto, size);
        return tempFileRepository.save(fileMetadata);
    }

    @Transactional
    public FileDto updateFile(String fileName, FileFormDto fileDto) {
        Optional<File> byName = fileRepository.findByName(fileName);
        File file = byName.orElseThrow(() -> new FileNotFoundException("File not found " + fileName));
        file.setDescription(fileDto.getDescription());
        Set<Tag> tags = getTags(fileDto.getTags());
        file.setTags(tags);
        List<AssociationDetailDto> fileGroups = getGroups(file.getId(), fileDto.getAssignedObjects());
        List<AssociationDto> associationList = associationClient.updateAssociationsForTypes(CrmObjectType.file, file.getId(), Arrays.asList(CrmObjectType.customer, CrmObjectType.lead, CrmObjectType.opportunity), fileGroups);
        return new FileDto(file, associationList);
    }

    public Page<File> getFilesInGroup(CrmObjectType groupType, Long groupId, Pageable pageable) {
        return fileRepository.findDistinctByIdIn(getFileIdsAssociatedToGroup(groupType, groupId), pageable);
    }
    public Page<File> getFilesInGroupAndSearch(CrmObjectType groupType, Long groupId, Pageable pageable, String search) {
        return fileRepository.findDistinctByIdInAndSearch(search, getFileIdsAssociatedToGroup(groupType, groupId), pageable);
    }

    private List<Long> getFileIdsAssociatedToGroup(CrmObjectType groupType, Long groupId) {
        List<AssociationDto> allByTypeAndId = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(groupType, CrmObjectType.file, groupId);
        return allByTypeAndId.stream().map(AssociationDto::getDestinationId).sorted().collect(Collectors.toList());
    }

    public Optional<File> getFileData(String fileName) {
        return fileRepository.findByName(fileName);
    }
    public Optional<TempFile> getTempFileData(Long fileId) {
        return tempFileRepository.findById(fileId);
    }

    public Optional<File> getFileById(Long id) {
        return fileRepository.findById(id);
    }

    public List<File> getUserFiles(String email) {
        return fileRepository.findAllByAccountEmail(email);
    }

    @Transactional
    public Set<File> getAllFilesForUser(Map<CrmObjectType, Set<Long>> groupsMap, String email, String search) {
        String searchText = Strings.nullToEmpty(search);
        Set<File> result = fileRepository.getByIdsAndSearch(getFiledIdsByGroupMap(groupsMap), searchText);
        result.addAll(fileRepository.getByAccountEmailAndSearch(searchText, email));
        return result;
    }

    @Transactional
    public Page<File> getAllFilesForUser(Map<CrmObjectType, Set<Long>> groupsMap, String email, Pageable pageable) {
        return fileRepository.findDistinctByAccountEmailOrIdIn(email, getFiledIdsByGroupMap(groupsMap), pageable);
    }
    @Transactional
    public Page<File> getAllFilesForUserAndSearch(Map<CrmObjectType, Set<Long>> groupsMap, String email, Pageable pageable, String search) {
        return fileRepository.findDistinctByAccountEmailOrIdInAndSearch(search, email, getFiledIdsByGroupMap(groupsMap), pageable);
    }
    @Transactional
    public Page<File> getAllFilesForUserAndSearchForMobile(Map<CrmObjectType, Set<Long>> groupsMap, String email, Pageable pageable, String search, FilePropertyType filePropertyType, String filePropertyValue) {
        return fileRepository.findDistinctByAccountEmailOrIdInAndSearchForMobile(search, email, getFiledIdsByGroupMap(groupsMap), filePropertyType, filePropertyValue, pageable);
    }

    @Transactional
    public Page<File> getAllAssociatedFilesAndSearchForMobile(Map<CrmObjectType, Set<Long>> groupsMap, String email, Pageable pageable, String search, Set<Long> fileIds, FilePropertyType filePropertyType, String filePropertyValue) {
        return fileRepository.findDistinctByFileIdsAndAccountEmailOrIdInAndSearchForMobile(search, email, getFiledIdsByGroupMap(groupsMap), fileIds, filePropertyType, filePropertyValue, pageable);
    }

    private Set<Long> getFiledIdsByGroupMap(Map<CrmObjectType, Set<Long>> groupsMap) {
        return groupsMap.keySet().stream()
                .filter(key -> groupsMap.get(key) != null && groupsMap.get(key).size() > 0)
                .flatMap(key -> associationClient.getAllBySourceTypeAndDestinationTypeAndSourceIds(key, CrmObjectType.file, new ArrayList<>(groupsMap.get(key))).stream())
                .map(AssociationDto::getDestinationId)
                .sorted()
                .collect(Collectors.toSet());
    }

    public Set<String> getTagsStartsWith(String tagPrefix) {
        Collection<Tag> tags;
        if (StringUtils.isEmpty(tagPrefix)) {
            tags = tagRepository.findAll();
        } else {
            tags = tagRepository.findByNameStartsWith(tagPrefix);
        }
        return tags.stream().map(Tag::getName).collect(Collectors.toSet());
    }

    @Transactional
    public void changeExpirationTime(String email) {
        List<TempFile> allByAccountEmail = tempFileRepository.getAllByAccountEmail(email);
        if (allByAccountEmail != null && allByAccountEmail.size() > 0) {
            LocalDateTime newExpiration = LocalDateTime.now().plusMinutes(10);
            tempFileRepository.saveAll(allByAccountEmail.stream().peek(c -> c.setExpirationTime(newExpiration)).collect(Collectors.toList()));
        }
    }

    private void associateFileWithTags(File file, Set<Tag> tags) {
        file.setTags(tags);
        tags.forEach(tag -> tag.getFiles().add(file));
    }

    private void associateFileWithProperties(File file, Set<FilePropertyDto> filePropertyDtoList) {
        file.setFileProperties(new HashSet<>());
        if (filePropertyDtoList != null) {
            filePropertyDtoList.forEach(property -> file.getFileProperties().add(property.toModel(file)));
        }
    }

    public List<File> findAllContaining(String pattern) {
        return fileRepository.findByNameContaining(pattern);
    }

    public List<File> findByIds(List<Long> ids) {
        return fileRepository.findByIds(ids);
    }

    private TempFile createTempFile(String ownerEmail, String originalFilename, TempFileFormDto tempFileFormDto, Long size) {
        String originalName = StringUtils.cleanPath(originalFilename);
        String format = FilenameUtils.getExtension(originalName);
        return TempFile.builder()
                .originalName(originalName)
                .format(format)
                .accountEmail(ownerEmail)
                .description(tempFileFormDto.getDescription())
                .expirationTime(LocalDateTime.now().plusMinutes(10))
                .size(size)
                .build();
    }
    private File createFile(String ownerEmail,
                            String originalFilename,
                            FileFormDto fileFormDto,
                            Long size) {
        String fileName = fileNameService.generateFileName();
        String originalName = StringUtils.cleanPath(originalFilename);
        String format = FilenameUtils.getExtension(originalName);

        return File.builder()
                .name(fileName)
                .originalName(originalName)
                .format(format)
                .path("/")
                .accountEmail(ownerEmail)
                .description(fileFormDto.getDescription())
                .tags(new HashSet<Tag>())
                .size(size)
                .build();
    }

    private Set<Tag> getTags(Set<String> tags) {
        Set<Tag> fileTags = tagRepository.findByNameIn(tags);
        Set<Tag> allTags = tags.stream().map(Tag::new).collect(Collectors.toSet());
        fileTags.addAll(allTags);
        return fileTags;
    }

    private List<AssociationDetailDto> getGroups(Long fileId, Set<CrmObject> assignedObjects) {
        if(assignedObjects == null){
            return new ArrayList<>();
        }
        return assignedObjects.stream().map(object ->
                new AssociationDetailDto(CrmObjectType.file, fileId, object.getType(), object.getId()))
                .collect(Collectors.toList());
    }

    public File updateVersionOfExistingFile(Long fileId) {
        Optional<File> fileOptional = this.fileRepository.findById(fileId);
        File file = fileOptional.orElseThrow(() -> new FileNotFoundException("File not found " + fileId));
        String fileName = fileNameService.generateFileName();
        file.setName(fileName);
        return file;
    }
    @Transactional
    public FileInfoDto moveTempFileToFile(Long tempFileId, FileInfoDto fileInfoDto) throws EntityNotFoundException {
        TempFile tempFile = tempFileRepository.findById(tempFileId).orElseThrow(EntityNotFoundException::new);
        File fileFromTemp = createFileFromTemp(tempFile);
        File toSave = dto2model(fileInfoDto, fileFromTemp);
        FileInfoDto result = model2dto(fileRepository.save(toSave));
        tempFileRepository.deleteById(tempFileId);
        return result;
    }

    public FileInfoDto setFileInfo(Long fileId, FileInfoDto fileInfoDto) throws EntityNotFoundException {
        File file = getFileById(fileId).orElseThrow(EntityNotFoundException::new);
        File toSave = dto2model(fileInfoDto, file);
        return model2dto(fileRepository.save(toSave));
    }
    public FileInfoDto getFileInfoByName(String fileName) throws EntityNotFoundException {
        File file = fileRepository.findByName(fileName).orElseThrow(EntityNotFoundException::new);
        return model2dto(file);
    }
    public FileInfoDto getTempFileInfoByName(String fileName) throws EntityNotFoundException {
        try {
            long id = Long.parseLong(fileName);
            TempFile tempFile = tempFileRepository.findById(id).orElseThrow(EntityNotFoundException::new);
            return model2dto(tempFile);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            throw new EntityNotFoundException();
        }
    }
    public FileInfoDto getFileInfo(Long fileId) throws EntityNotFoundException {
        File file = getFileById(fileId).orElseThrow(EntityNotFoundException::new);
        return model2dto(file);
    }
    public List<FileInfoDto> getFilesInfo(List<Long> fileIds) {
        return fileRepository.findByIds(fileIds).stream().map(this::model2dto).collect(Collectors.toList());
    }

    private File dto2model(FileInfoDto filePropertyDto, File fromDb) {
        fromDb.setOriginalName(filePropertyDto.getName());
        return fromDb;
    }

    private FileInfoDto model2dto(File file) {
        FileInfoDto infoDto = new FileInfoDto();
        infoDto.setId(file.getName());
        infoDto.setFileId(file.getId());
        infoDto.setName(file.getOriginalName());
        infoDto.setFormat(file.getFormat());
        return infoDto;
    }
    private FileInfoDto model2dto(TempFile file) {
        FileInfoDto infoDto = new FileInfoDto();
        infoDto.setId(file.getId().toString());
        infoDto.setFileId(file.getId());
        infoDto.setName(file.getOriginalName());
        infoDto.setFormat(file.getFormat());
        return infoDto;
    }

    private File createFileFromTemp(TempFile tempFile) {
        FileFormDto fileFormDto = new FileFormDto();
        fileFormDto.setDescription(tempFile.getDescription());
        fileFormDto.setAssignedObjects(new HashSet<>());
        fileFormDto.setTags(new HashSet<>());
        fileFormDto.setProperties(new HashSet<>());

        return createFile(
                tempFile.getAccountEmail(),
                tempFile.getOriginalName(),
                fileFormDto,
                tempFile.getSize());
    }

    public List<TempFile> getAllByExpirationTimeBefore(LocalDateTime now) {
        return tempFileRepository.getAllByExpirationTimeBefore(now);
    }
    public void deleteAllByExpirationTimeBefore(LocalDateTime now) {
        tempFileRepository.deleteAllByExpirationTimeBefore(now);
    }
}
