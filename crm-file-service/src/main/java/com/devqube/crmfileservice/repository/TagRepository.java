package com.devqube.crmfileservice.repository;

import com.devqube.crmfileservice.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
    Set<Tag> findByNameIn(Iterable<String> names);
    Set<Tag> findByNameStartsWith(String name);
 }
