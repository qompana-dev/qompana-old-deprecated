package com.devqube.crmfileservice.repository;

import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.model.FilePropertyType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface FileRepository extends JpaRepository<File, Long> {
    Optional<File> findByName(String fileName);

    List<File> findAllByAccountEmail(String email);

    @Query("select distinct(f) from File f " +
            "left join f.tags t " +
            "where (f.accountEmail = :email) and " +
            "(length(:search) = 0 or (lower(f.originalName) like concat('%', lower(:search), '%') or lower(f.description) like concat('%', lower(:search), '%') or lower(t.name) like concat('%', lower(:search), '%')))")
    Set<File> getByAccountEmailAndSearch(@Param("search") String search, @Param("email") String email);

    @Query("select distinct(f) from File f " +
            "left join f.tags t " +
            "where (f.id in :ids) and " +
            "(length(:search) = 0 or (lower(f.originalName) like concat('%', lower(:search), '%') or lower(f.description) like concat('%', lower(:search), '%') or lower(t.name) like concat('%', lower(:search), '%')))")
    Set<File> getByIdsAndSearch(@Param("ids") Set<Long> ids, @Param("search") String search);

    @Query("select distinct f from File f " +
            "left join f.tags t " +
            "where " +
            "(f.accountEmail = :email or f.id in :ids) and " +
            "(" +
            "LOWER(f.originalName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(f.format) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(f.accountEmail) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(f.authorFullName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(f.description) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(f.created as text)) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(t.name) LIKE LOWER(concat('%', :search, '%'))" +
            ")")
    Page<File> findDistinctByAccountEmailOrIdInAndSearch(@Param("search") String search, @Param("email") String email, @Param("ids") Set<Long> ids, Pageable pageable);

    @Query("select distinct f from File f " +
            "left join f.tags t " +
            "left join f.fileProperties fp " +
            "where " +
            "(f.accountEmail = :email or f.id in :ids) and " +
            "(:filePropertyType is null or fp.propertyType = :filePropertyType) and " +
            "(:filePropertyValue is null or fp.propertyValue = :filePropertyValue) and " +
            "(" +
            "LOWER(f.originalName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(f.format) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(f.created as text)) LIKE LOWER(concat('%', :search, '%'))" +
            ")")
    Page<File> findDistinctByAccountEmailOrIdInAndSearchForMobile(@Param("search") String search,
                                                                  @Param("email") String email,
                                                                  @Param("ids") Set<Long> ids,
                                                                  @Param("filePropertyType") FilePropertyType filePropertyType,
                                                                  @Param("filePropertyValue") String filePropertyValue,
                                                                  Pageable pageable);


    @Query("select distinct f from File f " +
            "left join f.tags t " +
            "left join f.fileProperties fp " +
            "where " +
            "(f.id in :fileIds) and " +
            "(:filePropertyType is null or fp.propertyType = :filePropertyType) and " +
            "(:filePropertyValue is null or fp.propertyValue = :filePropertyValue) and " +
            "(f.accountEmail = :email or f.id in :ids) and " +
            "(" +
            "LOWER(f.originalName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(f.format) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(f.created as text)) LIKE LOWER(concat('%', :search, '%'))" +
            ")")
    Page<File> findDistinctByFileIdsAndAccountEmailOrIdInAndSearchForMobile(@Param("search") String search,
                                                                            @Param("email") String email,
                                                                            @Param("ids") Set<Long> ids,
                                                                            @Param("fileIds") Set<Long> fileIds,
                                                                            @Param("filePropertyType") FilePropertyType filePropertyType,
                                                                            @Param("filePropertyValue") String filePropertyValue,
                                                                            Pageable pageable);


    Page<File> findDistinctByAccountEmailOrIdIn(String email, Set<Long> ids, Pageable pageable);

    @Query("select distinct f from File f " +
            "left join f.tags t " +
            "where " +
            "f.id in :ids and " +
            "(" +
            "LOWER(f.originalName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(f.format) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(f.accountEmail) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(f.authorFullName) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(f.description) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(CAST(f.created as text)) LIKE LOWER(concat('%', :search, '%')) or " +
            "LOWER(t.name) LIKE LOWER(concat('%', :search, '%'))" +
            ")")
    Page<File> findDistinctByIdInAndSearch(@Param("search") String search, @Param("ids") List<Long> ids, Pageable pageable);

    Page<File> findDistinctByIdIn(List<Long> ids, Pageable pageable);

    @Query("SELECT f FROM File f WHERE LOWER(f.originalName) LIKE LOWER(concat('%', ?1, '%'))")
    List<File> findByNameContaining(String pattern);

    @Query("SELECT f FROM File f WHERE f.id in ?1")
    List<File> findByIds(List<Long> ids);

    @Query("SELECT f FROM File f WHERE f.size = 0 OR f.size is null")
    List<File> findWithEmptySize();

    @Transactional
    @Modifying
    @Query("update File f set f.authorFullName = :fullName where f.accountEmail = :email")
    void setAuthorFullName(@Param("email") String email, @Param("fullName") String fullName);

    @Transactional
    @Modifying
    @Query("update File f set f.authorFullName = '' where f.accountEmail not in :emails")
    void setRemovedAuthorFullNames(@Param("emails") List<String> emails);

    @Query("select f.name from File f where f.name not in (select fs.fileId from FileScan fs)")
    List<String> getFilesNotAddedToScan();
}
