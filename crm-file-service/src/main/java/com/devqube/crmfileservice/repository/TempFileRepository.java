package com.devqube.crmfileservice.repository;

import com.devqube.crmfileservice.model.File;
import com.devqube.crmfileservice.model.TempFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TempFileRepository extends JpaRepository<TempFile, Long> {

    List<TempFile> getAllByExpirationTimeBefore(LocalDateTime now);
    void deleteAllByExpirationTimeBefore(LocalDateTime now);

    List<TempFile> getAllByAccountEmail(String accountEmail);

    @Query("SELECT f FROM TempFile f WHERE f.size = 0 OR f.size is null")
    List<TempFile> findWithEmptySize();
}
