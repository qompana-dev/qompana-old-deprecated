package com.devqube.crmfileservice.repository;

import com.devqube.crmfileservice.model.FileEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FileEventRepository extends JpaRepository<FileEvent, Long> {

    @Query("SELECT e FROM FileEvent e WHERE e.value1 = :fileName and " +
            "(e.eventType = com.devqube.crmfileservice.model.FileEventType.NEW_VERSION_UPLOADED" +
            " or e.eventType = com.devqube.crmfileservice.model.FileEventType.FILE_CREATED)")
    Optional<FileEvent> findFileVersionChangeByName(String fileName);

}
