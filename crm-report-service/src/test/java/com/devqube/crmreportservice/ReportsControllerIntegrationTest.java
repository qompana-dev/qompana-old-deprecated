package com.devqube.crmreportservice;

import com.devqube.crmreportservice.reports.web.dto.UserReportData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class ReportsControllerIntegrationTest extends AbstractIntegrationTest {

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void shouldGenerateUsersListReportInPdf() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_PDF));
        List<UserReportData> reportData = createUsersReportData();
        HttpEntity<List<UserReportData>> entity = new HttpEntity<>(reportData, headers);
        ResponseEntity<byte[]> response = restTemplate.postForEntity(baseUrl + "/users", entity, byte[].class);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(response.getBody());
    }

    @Test
    public void shouldGenerateUsersListReportInHtml() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.TEXT_HTML));
        List<UserReportData> reportData = createUsersReportData();
        HttpEntity<List<UserReportData>> entity = new HttpEntity<>(reportData, headers);
        ResponseEntity<byte[]> response = restTemplate.postForEntity(baseUrl + "/users", entity, byte[].class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    private List<UserReportData> createUsersReportData() {
        UserReportData userReportData1 = new UserReportData();
        userReportData1.setName("name_1");
        userReportData1.setSurname("surname_1");
        userReportData1.setEmail("email_1");
        userReportData1.setDescription("description_1");
        userReportData1.setPhone("phone_1");
        userReportData1.setIsActivated(true);

        UserReportData userReportData2 = new UserReportData();
        userReportData2.setName("name_2");
        userReportData2.setSurname("surname_2");
        userReportData2.setEmail("email_2");
        userReportData2.setDescription("description_2");
        userReportData2.setPhone("phone_2");
        userReportData2.setIsActivated(true);

        return List.of(userReportData1, userReportData2);
    }
}
