package com.devqube.crmreportservice.access;

import com.devqube.crmshared.access.model.ProfileDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "crm-user-service", url = "${crm-user-service.url}")
public interface UserClient {
    @RequestMapping(value = "/profile/basic/mine", consumes = {"application/json"}, method = RequestMethod.GET)
    ProfileDto getBasicProfile(@RequestHeader("Logged-Account-Email") String email);

    @RequestMapping(value = "/accounts/me/id", consumes = { "application/json" }, method = RequestMethod.GET)
    Long getMyAccountId(@RequestHeader("Logged-Account-Email") String email);
}
