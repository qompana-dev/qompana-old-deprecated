package com.devqube.crmreportservice.jasper;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.InputStream;

@Service
@Profile("!dev")
public class JasperPrecompiledLoader implements JasperTemplateLoader {
    @Override
    public JasperReport load(String templateName) throws JRException {
        InputStream resourceAsStream = getClass().getResourceAsStream("/jasper/" + templateName + ".jasper");
        return (JasperReport) JRLoader.loadObject(resourceAsStream);
    }
}
