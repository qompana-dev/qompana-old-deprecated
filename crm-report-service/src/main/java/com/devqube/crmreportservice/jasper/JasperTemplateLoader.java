package com.devqube.crmreportservice.jasper;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

public interface JasperTemplateLoader {
    JasperReport load(String templateName) throws JRException;
}
