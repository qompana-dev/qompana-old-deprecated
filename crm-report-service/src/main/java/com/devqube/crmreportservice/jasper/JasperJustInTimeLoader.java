package com.devqube.crmreportservice.jasper;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.InputStream;


@Service
@Profile("dev")
public class JasperJustInTimeLoader implements JasperTemplateLoader {

    @Override
    public JasperReport load(String templateName) throws JRException {
        InputStream reportStream = getClass().getResourceAsStream(String.format("/templates/%s.jrxml", templateName));
        return JasperCompileManager.compileReport(reportStream);
    }
}
