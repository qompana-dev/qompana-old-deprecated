package com.devqube.crmreportservice;

import com.devqube.crmshared.license.LicenseCheck;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

@EnableFeignClients
@EnableEurekaClient
@Import({LicenseCheck.class})
@SpringBootApplication
public class CrmReportServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrmReportServiceApplication.class, args);
    }

}
