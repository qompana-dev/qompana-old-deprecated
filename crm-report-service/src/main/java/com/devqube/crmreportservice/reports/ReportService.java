package com.devqube.crmreportservice.reports;

import com.devqube.crmreportservice.access.UserClient;
import com.devqube.crmreportservice.jasper.JasperTemplateLoader;
import com.devqube.crmreportservice.reports.integration.BusinessClient;
import com.devqube.crmreportservice.reports.web.dto.BusinessOfferDto;
import com.devqube.crmreportservice.reports.web.dto.UserReportData;
import com.devqube.crmreportservice.reports.web.dto.OfferDto;
import com.devqube.crmshared.web.CurrentRequestUtil;
import com.google.common.base.Strings;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportService {

    private final JasperTemplateLoader templateLoader;
    private final BusinessClient businessClient;
    private final UserClient userClient;

    public ReportService(JasperTemplateLoader templateLoader, BusinessClient businessClient, UserClient userClient) {
        this.templateLoader = templateLoader;
        this.businessClient = businessClient;
        this.userClient = userClient;
    }

    public byte[] generateOffer(OfferDto offer) throws JRException {
        JasperReport reportTemplate = templateLoader.load(offer.getReportType().getFileName());
        Map<String, Object> params = JasperDataSourceParser.parse(offer);
        JasperPrint jasperPrint = fillWithParamsAndGetPrint(reportTemplate, params);
        byte[] reportBytes = exportTo(ReportFormat.PDF, jasperPrint);
        Long userId = userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail());
        String companyName = Strings.nullToEmpty(offer.getPreparedFor() != null ? offer.getPreparedFor().getCompanyName() : "").toLowerCase();
        businessClient.saveOffer(new BusinessOfferDto(offer.getOpportunityId(), userId, offer.getUuid(), LocalDateTime.now(), companyName));
        return reportBytes;
    }

    public byte[] generateUserListReport(ReportFormat format, @Valid List<UserReportData> userReportData) throws JRException {
        Map<String, Object> parameters = getUserListParameters(userReportData);
        JasperReport reportTemplate = templateLoader.load("users-list");
        JasperPrint jasperPrint = fillWithParamsAndGetPrint(reportTemplate, parameters);
        return exportTo(format, jasperPrint);
    }

    private JasperPrint fillWithParamsAndGetPrint(JasperReport reportTemplate, Map<String, Object> parameters) throws JRException {
        return JasperFillManager.fillReport(reportTemplate, parameters, new JREmptyDataSource());
    }

    private Map<String, Object> getUserListParameters(@Valid List<UserReportData> userReportData) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("usersList", new JRBeanCollectionDataSource(userReportData));
        return parameters;
    }

    private byte[] exportTo(ReportFormat format, JasperPrint jasperPrint) throws JRException {
        final Exporter exporter;
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        switch (format) {
            case HTML:
                exporter = createHtmlExporter(jasperPrint, out);
                break;
            default:
                exporter = createPdfExporter(jasperPrint, out);
                break;
        }
        exporter.exportReport();
        return out.toByteArray();
    }

    private HtmlExporter createHtmlExporter(JasperPrint jasperPrint, ByteArrayOutputStream out) {
        HtmlExporter htmlExporter = new HtmlExporter();
        htmlExporter.setExporterOutput(new SimpleHtmlExporterOutput(out));
        htmlExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        return htmlExporter;
    }

    private JRPdfExporter createPdfExporter(JasperPrint jasperPrint, ByteArrayOutputStream out) {
        JRPdfExporter jrPdfExporter = new JRPdfExporter();
        jrPdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
        jrPdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        return jrPdfExporter;
    }


}
