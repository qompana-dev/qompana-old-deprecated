package com.devqube.crmreportservice.reports;

import lombok.Getter;
import org.springframework.http.MediaType;

@Getter
public enum ReportFormat {
    PDF(MediaType.APPLICATION_PDF_VALUE),
    HTML(MediaType.TEXT_HTML_VALUE);

    private final String mimeType;

    ReportFormat(String mimeType) {
        this.mimeType = mimeType;
    }

    public static ReportFormat byMimeType(String mimeType) {
        for (ReportFormat format : values()) {
            if (format.mimeType.equals(mimeType)) {
                return format;
            }
        }
        return ReportFormat.PDF;
    }

}
