package com.devqube.crmreportservice.reports.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressForPreparedByDto {
    @NotNull
    private String companyName;
    @NotNull
    private String companyAddress;
    @NotNull
    private String companyPhone;
    @NotNull
    private String companyEmail;
    @NotNull
    private String www;
    @NotNull
    private String name;
    @NotNull
    private String position;
    @NotNull
    private String phone;
    @NotNull
    private String email;
    @NotNull
    private String nip;
    @NotNull
    private String regon;
    @NotNull
    private String krs;
}
