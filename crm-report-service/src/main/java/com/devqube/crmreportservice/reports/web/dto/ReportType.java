package com.devqube.crmreportservice.reports.web.dto;

public enum ReportType {
    OFFER_1("offer_1"),
    OFFER_2("offer_2"),
    OFFER_3("offer_3");

    private String fileName;

    ReportType(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
}
