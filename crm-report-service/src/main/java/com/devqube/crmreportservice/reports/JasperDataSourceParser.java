package com.devqube.crmreportservice.reports;

import com.devqube.crmreportservice.reports.web.dto.AddressForPreparedByDto;
import com.devqube.crmreportservice.reports.web.dto.AddressForPreparedForDto;
import com.devqube.crmreportservice.reports.web.dto.OfferDto;
import com.google.common.base.Strings;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.tomcat.util.codec.binary.Base64;

import java.util.HashMap;
import java.util.Map;

public class JasperDataSourceParser {
    public static Map<String, Object> parse(OfferDto offerDto) {
        Map<String, Object> result = new HashMap<>();
        result.putAll(getTitlesAndDescriptions(offerDto));
        result.putAll(getOtherData(offerDto));
        if (offerDto.getPreparedBy() != null) {
            result.putAll(getAddressForPreparedBy(offerDto.getPreparedBy()));
        }
        if (offerDto.getPreparedFor() != null) {
            result.putAll(getAddressForPreparedFor(offerDto.getPreparedFor()));
        }
        nullToEmpty(result);
        return result;
    }

    private static Map<String, Object> getTitlesAndDescriptions(OfferDto offerDto) {
        Map<String, Object> result = new HashMap<>();
        result.put("firstPageTitle", offerDto.getFirstPageTitle());
        result.put("firstPageDescription", offerDto.getFirstPageDescription());
        result.put("termsOfPurchaseTitle", offerDto.getTermsOfPurchaseTitle());
        result.put("termsOfPurchaseDescription", offerDto.getTermsOfPurchaseDescription());
        result.put("secondPageTitle", offerDto.getSecondPageTitle());
        result.put("secondPageDescription", offerDto.getSecondPageDescription());
        result.put("additionalNoteTitle", offerDto.getAdditionalNoteTitle());
        result.put("additionalNoteDescription", offerDto.getAdditionalNoteDescription());
        result.put("signatureTitle", offerDto.getSignatureTitle());
        nullToEmpty(result);
        return result;
    }

    private static Map<String, Object> getAddressForPreparedBy(AddressForPreparedByDto dto) {
        Map<String, Object> result = new HashMap<>();
        result.put("preparedByCompanyName", dto.getCompanyName());
        result.put("preparedByCompanyAddress", dto.getCompanyAddress());
        result.put("preparedByCompanyPhone", dto.getCompanyPhone());
        result.put("preparedByCompanyEmail", dto.getCompanyEmail());
        result.put("preparedByWww", dto.getWww());
        result.put("preparedByName", dto.getName());
        result.put("preparedByPosition", dto.getPosition());
        result.put("preparedByPhone", dto.getPhone());
        result.put("preparedByEmail", dto.getEmail());
        result.put("preparedByNip", dto.getNip());
        result.put("preparedByRegon", dto.getRegon());
        result.put("preparedByKrs", dto.getKrs());
        nullToEmpty(result);
        return result;
    }

    private static Map<String, Object> getAddressForPreparedFor(AddressForPreparedForDto dto) {
        Map<String, Object> result = new HashMap<>();
        result.put("preparedForCompanyName", dto.getCompanyName());
        result.put("preparedForCompanyAddress", dto.getCompanyAddress());
        result.put("preparedForCompanyPhone", dto.getCompanyPhone());
        result.put("preparedForCompanyEmail", dto.getCompanyEmail());
        result.put("preparedForWww", dto.getWww());
        nullToEmpty(result);
        return result;
    }

    private static Map<String, Object> getOtherData(OfferDto offerDto) {
        Map<String, Object> result = new HashMap<>();
        result.put("logo", Base64.decodeBase64(Strings.nullToEmpty(offerDto.getBase64EncodedLogo()).getBytes()));
        result.put("offerDate", offerDto.getOfferDate());
        result.put("expiration", offerDto.getExpiration());
        result.put("products", new JRBeanCollectionDataSource(offerDto.getProducts()));
        result.put("productsSummed", offerDto.getProductsSummed());
        nullToEmpty(result);
        return result;
    }

    private static void nullToEmpty(Map<String, Object> map) {
        for (String key : map.keySet()) {
            map.putIfAbsent(key, "");
        }
    }
}
