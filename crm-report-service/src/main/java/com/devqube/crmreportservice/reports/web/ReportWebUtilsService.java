package com.devqube.crmreportservice.reports.web;

import com.devqube.crmreportservice.reports.ReportFormat;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.net.URLConnection;
import java.util.Optional;

@Service
public class ReportWebUtilsService {

    public ResponseEntity<byte[]> createReportResponse(String reportName, @NotNull ReportFormat format, byte[] bytes) {
        String fullName = String.format("%s.%s", reportName, format.toString().toLowerCase());
        String contentType = URLConnection.guessContentTypeFromName(fullName);
        MediaType mediaType = Optional.ofNullable(contentType)
                .map(MediaType::parseMediaType)
                .orElse(MediaType.APPLICATION_OCTET_STREAM);

        ContentDisposition contentDisposition = ContentDisposition.builder("inline")
                .filename(fullName)
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentDisposition(contentDisposition);

        return ResponseEntity.ok()
                .contentType(mediaType)
                .headers(headers)
                .body(bytes);
    }

    public ReportFormat getReportFormatFrom(String acceptHeader) {
        return ReportFormat.byMimeType(acceptHeader);
    }
}
