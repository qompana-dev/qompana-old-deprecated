package com.devqube.crmreportservice.reports.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OfferProductDto {
    @NotNull
    private String name;
    @NotNull
    private String price;
    @NotNull
    private Long quantity;
    @NotNull
    private String unit;
    @NotNull
    private String priceSummed;
}
