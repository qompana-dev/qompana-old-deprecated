package com.devqube.crmreportservice.reports.web;

import com.devqube.crmreportservice.reports.ReportService;
import com.devqube.crmreportservice.reports.ReportFormat;
import com.devqube.crmreportservice.reports.web.dto.UserReportData;
import com.devqube.crmreportservice.reports.web.dto.OfferDto;
import com.devqube.crmshared.access.annotation.AuthController;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
public class ReportsController implements ReportsApi {

    private final ReportService reportService;
    private final ReportWebUtilsService reportWebUtilsService;

    public ReportsController(ReportService reportService, ReportWebUtilsService reportWebUtilsService) {
        this.reportService = reportService;
        this.reportWebUtilsService = reportWebUtilsService;
    }

    @Override
    public ResponseEntity<byte[]> getUsersReport(String acceptHeader, @Valid List<UserReportData> reportData) {
        ReportFormat format = reportWebUtilsService.getReportFormatFrom(acceptHeader);
        try {
            byte[] report = reportService.generateUserListReport(format, reportData);
            return reportWebUtilsService.createReportResponse("users-list", format, report);
        } catch (JRException e) {
            log.error("Unknown error during report generation", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    @AuthController(actionFrontendId = "OpportunityDetailsComponent.createOffer")
    public ResponseEntity<byte[]> generateOffer(@Valid OfferDto offerDto) {
        try {
            byte[] report = reportService.generateOffer(offerDto);
            return reportWebUtilsService.createReportResponse("offer", ReportFormat.PDF, report);
        } catch (JRException e) {
            log.error("Unknown error during report generation", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
