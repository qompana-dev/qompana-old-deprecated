package com.devqube.crmreportservice.reports.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OfferDto {
    private String base64EncodedLogo;
    private String firstPageTitle;
    private String firstPageDescription;
    private String offerDate;
    private String expiration;
    private String secondPageTitle;
    private String secondPageDescription;
    private String termsOfPurchaseTitle;
    private String termsOfPurchaseDescription;
    private String additionalNoteTitle;
    private String additionalNoteDescription;
    private String signatureTitle;
    private AddressForPreparedForDto preparedFor;
    private AddressForPreparedByDto preparedBy;
    private List<OfferProductDto> products;
    private String productsSummed;
    @NotNull
    private ReportType reportType;
    @NotNull
    private Long opportunityId;
    @NotNull
    private String uuid;
}
