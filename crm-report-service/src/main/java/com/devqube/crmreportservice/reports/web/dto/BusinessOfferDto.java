package com.devqube.crmreportservice.reports.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BusinessOfferDto {
    private Long opportunityId;
    private Long userId;
    private String uuid;
    private LocalDateTime generationTime;
    private String customerName;
}
