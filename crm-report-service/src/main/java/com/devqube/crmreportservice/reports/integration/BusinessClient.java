package com.devqube.crmreportservice.reports.integration;

import com.devqube.crmreportservice.reports.web.dto.BusinessOfferDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "crm-business-service", url = "${crm-business-service.url}")
public interface BusinessClient {
    @PostMapping("/internal/offers")
    void saveOffer(@RequestBody BusinessOfferDto offerDto);
}
