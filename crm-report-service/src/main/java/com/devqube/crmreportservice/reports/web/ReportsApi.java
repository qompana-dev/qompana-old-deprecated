package com.devqube.crmreportservice.reports.web;

import com.devqube.crmreportservice.reports.web.dto.UserReportData;
import com.devqube.crmreportservice.reports.web.dto.OfferDto;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "Reports")
public interface ReportsApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Generate users report", nickname = "getUsersReport", notes = "Method used to generate report of all users", response = byte[].class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Downloaded file", response = byte[].class) })
    @RequestMapping(value = "/users",
        produces = { "application/pdf", "text/html" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    default ResponseEntity<byte[]> getUsersReport(@ApiParam(value = "", required = true, allowableValues = "application/pdf, text/html", defaultValue = "application/pdf") @RequestHeader(value = "Accept", required = true) String accept, @ApiParam(value = "Data to fill users report", required = true) @Valid @RequestBody List<UserReportData> userReportData) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    @ApiOperation(value = "Generate offer", nickname = "generateOffer", notes = "Method used to generate offer", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Generated offer", response = byte[].class) })
    @RequestMapping(value = "/offer",
            produces = { "application/pdf" },
            consumes = { "application/json" },
            method = RequestMethod.POST)
    default ResponseEntity<byte[]> generateOffer(@ApiParam(value = "Data to fill users report", required = true) @Valid @RequestBody OfferDto offerDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
