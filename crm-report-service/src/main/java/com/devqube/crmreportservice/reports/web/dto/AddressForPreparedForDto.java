package com.devqube.crmreportservice.reports.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressForPreparedForDto {
    @NotNull
    private String companyName;
    @NotNull
    private String companyAddress;
    @NotNull
    private String companyPhone;
    @NotNull
    private String companyEmail;
    @NotNull
    private String www;
}
