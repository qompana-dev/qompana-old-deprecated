package com.devqube.crmgatewayservice.websocket.web;

import com.devqube.crmgatewayservice.AbstractIntegrationTest;
import com.devqube.crmgatewayservice.kafka.KafkaServiceImpl;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.kafka.util.KafkaMessageDataBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeoutException;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class WebsocketControllerIntegrationTest extends AbstractIntegrationTest {
    private Map<String, BlockingQueue<String>> blockingQueueMap = new HashMap<>();

    @Autowired
    private KafkaServiceImpl kafkaService;

    @Test
    public void shouldSendWebsocketMessage() throws InterruptedException, TimeoutException, ExecutionException {
        String destination = "/topic/notification/change/-1";
        String payload = "test value";
        StompSession session = connectToNotificationChange("sender", destination);

        Thread.sleep(6000);

        KafkaMessage kafkaMessage = new KafkaMessage(null, KafkaMsgType.SEND_WEBSOCKET_MESSAGE, KafkaMessageDataBuilder.builder()
                .add("destination", destination)
                .add("payload", payload).build());

        kafkaService.sendWebsocketMessage(kafkaMessage);

        //waiting for ws messages
        Thread.sleep(1000);
        assertEquals(1, blockingQueueMap.get("sender").size());
        session.disconnect();
    }

    private StompSession connectToNotificationChange(String key, String url) throws InterruptedException, ExecutionException, TimeoutException {
        StompSession session = new WebSocketStompClient(new StandardWebSocketClient()).connect("ws://localhost:" + getPort() + "/ws", new StompSessionHandlerAdapter() {
        }).get(5, SECONDS);
        session.subscribe(url, new DefaultStompFrameHandler(key));
        return session;
    }

    class DefaultStompFrameHandler implements StompFrameHandler {
        private String key;

        public DefaultStompFrameHandler(String key) {
            this.key = key;
            blockingQueueMap.put(key, new LinkedBlockingDeque<>());
        }

        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {
            return byte[].class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {
            System.out.println("***" + new String((byte[]) o));
            blockingQueueMap.get(key).offer(new String((byte[]) o));
        }
    }
}
