package com.devqube.crmgatewayservice.auth.util;

import com.devqube.crmgatewayservice.auth.AccountsUtil;
import com.devqube.crmgatewayservice.auth.accountDetails.AccountAuthDetails;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import javax.naming.AuthenticationException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class JWTTokenUtilTest {

    private static final String FAKE_TOKEN = "aaaa fake token ajsdkljfasdkljfklajsdklfj";
    private AccountAuthDetails accountAuthDetails;

    @Before
    public void setUp() {
        accountAuthDetails = AccountsUtil.createAccountAuthDetails();
    }


    @Test
    public void shouldGenerateValidToken() throws AuthenticationException {
        String token = JWTTokenUtil.generate(accountAuthDetails, -1L);
        assertEquals(JWTTokenUtil.getUsernameFromToken(token), accountAuthDetails.getUsername());
    }

    @Test
    public void shouldRefreshValidToken() throws AuthenticationException {
        String token = JWTTokenUtil.generate(accountAuthDetails, -1L);
        String refreshedToken = JWTTokenUtil.refreshToken(token);
        assertEquals(JWTTokenUtil.getUsernameFromToken(token), JWTTokenUtil.getUsernameFromToken(refreshedToken));
    }

    @Test(expected = AuthenticationException.class)
    public void shouldThrowAuthenticationExceptionWhenTokenNotValid() throws AuthenticationException {
        JWTTokenUtil.refreshToken(FAKE_TOKEN);
    }

    @Test(expected = AuthenticationException.class)
    public void shouldFailValidationForFakeToken() throws AuthenticationException {
        JWTTokenUtil.validateToken(FAKE_TOKEN, accountAuthDetails);
    }

    public void shouldValidateCorrectlyForValidToken() throws AuthenticationException {
        String token = JWTTokenUtil.generate(accountAuthDetails, -1L);
        assertTrue(JWTTokenUtil.validateToken(token, accountAuthDetails));
    }
}
