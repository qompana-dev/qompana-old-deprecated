package com.devqube.crmgatewayservice.auth;

import com.devqube.crmgatewayservice.auth.accountDetails.AccountAuthDetails;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.HashSet;

public class AccountsUtil {
    public static AccountAuthDetails createAccountAuthDetails() {
        HashSet<SimpleGrantedAuthority> authorities = new HashSet<>();
        for (int i = 0; i < 3; i++) {
            authorities.add(new SimpleGrantedAuthority(i + ""));
        }
        return new AccountAuthDetails("username", "password", authorities, false, false);
    }

}
