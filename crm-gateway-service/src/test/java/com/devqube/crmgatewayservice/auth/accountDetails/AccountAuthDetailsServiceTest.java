package com.devqube.crmgatewayservice.auth.accountDetails;

import com.devqube.crmgatewayservice.auth.accountDetails.client.AccountDTO;
import com.devqube.crmgatewayservice.auth.accountDetails.client.AccountService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class AccountAuthDetailsServiceTest {

    @Mock
    private AccountService accountService;

    @InjectMocks
    private AccountAuthDetailsService accountAuthDetailsService;

    private AccountDTO account;

    @Before
    public void setUp() {
        this.account = new AccountDTO(-1L, "email@email.com", "123123123", true, false);
    }

    @Test
    public void shouldReturnAccountAuthDetailsWhenUserExists() {
        Mockito.when(accountService.getAccountByEmail(any())).thenReturn(account);
        String email = account.getEmail();
        UserDetails userDetails = accountAuthDetailsService.loadUserByUsername(email);
        Assert.assertEquals(userDetails.getUsername(), email);
    }

}
