package com.devqube.crmgatewayservice;

import com.devqube.crmshared.license.LicenseCheck;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
@EnableFeignClients
@EnableCaching
@Import({LicenseCheck.class})
public class CrmGatewayServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrmGatewayServiceApplication.class, args);
	}

}
