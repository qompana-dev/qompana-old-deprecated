package com.devqube.crmgatewayservice.kafka;

import com.devqube.crmgatewayservice.websocket.WebsocketService;
import com.devqube.crmshared.kafka.AbstractKafkaService;
import com.devqube.crmshared.kafka.ServiceKafkaAddr;
import com.devqube.crmshared.kafka.annotation.KafkaReceiver;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class KafkaServiceImpl extends AbstractKafkaService {
    private final WebsocketService websocketService;

    public KafkaServiceImpl(WebsocketService websocketService) {
        this.websocketService = websocketService;
    }

    @KafkaListener(topics = ServiceKafkaAddr.CRM_GATEWAY_SERVICE)
    public void processMessage(String content) {
        super.processMessage(content);
    }

    @KafkaReceiver(from = KafkaMsgType.SEND_WEBSOCKET_MESSAGE)
    public void sendWebsocketMessage(KafkaMessage kafkaMessage) {
        log.info(kafkaMessage.getDestination().name());
        Optional<String> destination = kafkaMessage.getValueByKey("destination");
        Optional<String> payload = kafkaMessage.getValueByKey("payload");
        if (payload.isPresent() && destination.isPresent()) {
            try {
                this.websocketService.send(destination.get(), payload.get());
            }catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        } else {
            log.error("destination or payload is null");
        }
    }

    @Override
    public void receiverNotFound(KafkaMessage kafkaMessage) {
        log.info("receiverNotFound" + kafkaMessage.getDestination().name());
    }
}
