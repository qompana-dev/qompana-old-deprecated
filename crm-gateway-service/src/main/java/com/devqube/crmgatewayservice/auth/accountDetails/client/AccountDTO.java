package com.devqube.crmgatewayservice.auth.accountDetails.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class AccountDTO implements Serializable {
    @EqualsAndHashCode.Include
    private Long id;
    @EqualsAndHashCode.Include
    private String email;
    private String password;
    private boolean activated;
    private boolean credentialsExpired;
}
