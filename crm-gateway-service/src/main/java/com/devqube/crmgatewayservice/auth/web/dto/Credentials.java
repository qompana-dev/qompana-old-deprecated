package com.devqube.crmgatewayservice.auth.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Credentials {
    @JsonProperty("username")
    private String username;

    @JsonProperty("password")
    private String password;
}
