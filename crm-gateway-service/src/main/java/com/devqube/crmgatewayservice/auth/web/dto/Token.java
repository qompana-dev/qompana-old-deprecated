package com.devqube.crmgatewayservice.auth.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-04-03T16:21:27.240263400+02:00[Europe/Belgrade]")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Token {
    @JsonProperty("value")
    private String value;

    @JsonProperty("expiredDate")
    private Long expiredDate;

    @JsonProperty("numberOfUsers")
    private Long numberOfUsers;

    public Token(String value) {
        this.value = value;
    }
}
