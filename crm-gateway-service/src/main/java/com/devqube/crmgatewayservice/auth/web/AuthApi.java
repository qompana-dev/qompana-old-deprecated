package com.devqube.crmgatewayservice.auth.web;

import com.devqube.crmgatewayservice.ApiUtil;
import com.devqube.crmgatewayservice.auth.web.dto.Credentials;
import com.devqube.crmgatewayservice.auth.web.dto.Token;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.Optional;

@Validated
@Api(value = "auth")
public interface AuthApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Retrieve JWT token", nickname = "authenticate", notes = "Method used to retrieve JWT token from server", response = Token.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "authentication successful", response = Token.class),
            @ApiResponse(code = 401, message = "authentication failed", response = String.class),
            @ApiResponse(code = 480, message = "authentication failed credentials expired", response = String.class) })
    @RequestMapping(value = "/auth",
            produces = { "application/json" },
            consumes = { "application/json" },
            method = RequestMethod.POST)
    default ResponseEntity<Token> authenticate(@ApiParam(value = "Retrieves JWT token" ,required=true )  @Valid @RequestBody Credentials credentials) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"value\" : \"value\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    @ApiOperation(value = "Refresh JWT Token", nickname = "refreshAuthentication", notes = "Method used to refresh JWT Token.", response = Token.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "refreshing successful", response = Token.class),
            @ApiResponse(code = 401, message = "refreshing failed", response = String.class) })
    @RequestMapping(value = "/auth",
            produces = { "application/json" },
            method = RequestMethod.GET)
    default ResponseEntity<Token> refreshAuthentication(@ApiParam(value = "JWT Token to be refreshed" ,required=true) @RequestHeader(value="Authorization", required=true) String authorization) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"value\" : \"value\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }
}
