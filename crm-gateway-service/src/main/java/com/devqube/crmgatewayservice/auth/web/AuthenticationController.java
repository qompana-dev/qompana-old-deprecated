package com.devqube.crmgatewayservice.auth.web;


import com.devqube.crmgatewayservice.auth.accountDetails.client.AccountService;
import com.devqube.crmgatewayservice.auth.util.JWTTokenUtil;
import com.devqube.crmgatewayservice.auth.web.dto.Credentials;
import com.devqube.crmgatewayservice.auth.web.dto.Token;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.AuthenticationException;

@Slf4j
@CrossOrigin
@RestController
public class AuthenticationController implements AuthApi {

    private final AuthenticationManager authenticationManager;

    private final UserDetailsService userDetailsService;

    private final AccountService accountService;

    public AuthenticationController(AuthenticationManager authenticationManager,
                                    @Qualifier("accountAuthDetailsService") UserDetailsService userDetailsService,
                                    AccountService accountService) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.accountService = accountService;
    }

    @Override
    public ResponseEntity<Token> refreshAuthentication(String token) {
        String refreshedToken;
        try {
            refreshedToken = JWTTokenUtil.refreshToken(token.replaceAll("Bearer ", ""));
            return new ResponseEntity<>(new Token(refreshedToken), HttpStatus.OK);
        } catch (AuthenticationException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity<Token> authenticate(Credentials credentials) {
        Authentication authentication;
        try {
           boolean isLicenceActive =  accountService.getLicenceRefresh();
           if (isLicenceActive) {
               authentication = authenticationManager.authenticate(
                       new UsernamePasswordAuthenticationToken( credentials.getUsername(), credentials.getPassword()));
           } else {
               return new ResponseEntity<>(HttpStatus.PAYMENT_REQUIRED);
           }
        } catch (LockedException e) {
            return new ResponseEntity<>(HttpStatus.LOCKED);
        } catch (CredentialsExpiredException e){
            log.info(e.getMessage(), e);
            return ResponseEntity.status(480).build();
        } catch (FeignException feignException){
            if (feignException.status() == 402){
                return new ResponseEntity<>(HttpStatus.PAYMENT_REQUIRED);
            } else {
                throw feignException;
            }
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails userDetails = userDetailsService.loadUserByUsername(credentials.getUsername());
        String token = JWTTokenUtil.generate(userDetails, accountService.getAccountByEmail(userDetails.getUsername()).getId());
        Long expiredDate = accountService.getAndRefreshExpiredDate();
        Long numberOfUsers = accountService.getAndRefreshNumberOfUsers();
        return new ResponseEntity<>(new Token(token, expiredDate, numberOfUsers), HttpStatus.OK);
    }
}
