package com.devqube.crmgatewayservice.auth.web;

import com.devqube.crmgatewayservice.auth.accountDetails.AccountAuthDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityContextService {
    public String getLoggedUserEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() != null) {
            if (AccountAuthDetails.class.isAssignableFrom(authentication.getPrincipal().getClass())) {
                AccountAuthDetails auth = (AccountAuthDetails) authentication.getPrincipal();
                return auth.getUsername();
            }
        }
        return null;
    }
}
