package com.devqube.crmgatewayservice.auth.accountDetails.client;

import org.springframework.stereotype.Service;

@Service
public class AccountService {
    private final AccountClient accountClient;

    public AccountService(AccountClient accountClient) {
        this.accountClient = accountClient;
    }

    public AccountDTO getAccountByEmail(String email) {
        return this.accountClient.getAccountByEmail(email);
    }

    public Boolean getLicenceRefresh() {
        return this.accountClient.getLicenceRefresh();
    }

    public Long getAndRefreshExpiredDate() {
        return this.accountClient.getAndRefreshExpiredDate();
    }

    public Long getAndRefreshNumberOfUsers() {
        return this.accountClient.getAndRefreshNumberOfUsers();
    }
}
