package com.devqube.crmgatewayservice.auth.config;

import com.devqube.crmgatewayservice.auth.util.JWTTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.naming.AuthenticationException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class JWTAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    @Qualifier("accountAuthDetailsService")
    private UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        String authToken = request.getHeader(JWTTokenUtil.TOKEN_HEADER);
        if (authToken != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            try {
                authenticate(request, authToken);
            } catch (AuthenticationException e) {
                log.info("Authentication failed. JWT is either expired, malformed, unsupported or signed with not" +
                        " recognized key.", e);
            }
        }
        chain.doFilter(request, response);
    }

    private void authenticate(HttpServletRequest request, String authToken) throws AuthenticationException {
        authToken = authToken.replace("Bearer ", "");
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(JWTTokenUtil.getUsernameFromToken(authToken));
        if (JWTTokenUtil.validateToken(authToken, userDetails)) {
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails,
                    null, userDetails.getAuthorities());
            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
    }
}
