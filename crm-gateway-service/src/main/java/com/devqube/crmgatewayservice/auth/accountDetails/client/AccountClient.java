package com.devqube.crmgatewayservice.auth.accountDetails.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "crm-user-service", url = "${zuul.routes.crm-user-service.url}")
public interface AccountClient {
    @GetMapping("/accounts/by-email/{email}")
    AccountDTO getAccountByEmail(@PathVariable("email") String email);

    @GetMapping("/accounts/licence/refresh")
    Boolean getLicenceRefresh();

    @GetMapping("/accounts/licence/expiredDate")
    Long getAndRefreshExpiredDate();

    @GetMapping("/accounts/licence/numberOfUsers")
    Long getAndRefreshNumberOfUsers();
}
