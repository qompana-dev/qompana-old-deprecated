package com.devqube.crmgatewayservice.auth.config;

import com.devqube.crmgatewayservice.auth.web.SecurityContextService;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddAuthenticationContextFilter extends ZuulFilter {

    @Autowired
    private SecurityContextService securityContextService;

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext requestContext = RequestContext.getCurrentContext();
        requestContext.addZuulRequestHeader("Logged-Account-Email", securityContextService.getLoggedUserEmail());
        return null;
    }
}
