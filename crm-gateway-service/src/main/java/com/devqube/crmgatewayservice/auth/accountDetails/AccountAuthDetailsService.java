package com.devqube.crmgatewayservice.auth.accountDetails;

import com.devqube.crmgatewayservice.auth.accountDetails.client.AccountDTO;
import com.devqube.crmgatewayservice.auth.accountDetails.client.AccountService;
import feign.FeignException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service(value = "accountAuthDetailsService")
public class AccountAuthDetailsService implements UserDetailsService {
    private final AccountService accountService;

    public AccountAuthDetailsService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        try {
            AccountDTO account = accountService.getAccountByEmail(email);
            return new AccountAuthDetails(account.getEmail(), account.getPassword(), null,
                    !account.isActivated(), account.isCredentialsExpired());
        } catch (FeignException exception) {
            if (exception.status() == 402) {
                return new AccountAuthDetails(null, null, null, false, true);
            } else {
                throw new UsernameNotFoundException("Username " + email + " not found");
            }
        }
    }
}
