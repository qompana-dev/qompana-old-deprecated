package com.devqube.crmgatewayservice.auth.util;

import com.devqube.crmgatewayservice.auth.accountDetails.AccountAuthDetails;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;

import javax.naming.AuthenticationException;
import java.util.*;

@Slf4j
public class JWTTokenUtil {

    public final static String TOKEN_HEADER = "Authorization";
    private static final byte[] SECRET = Base64.getEncoder().encode("veryStrongPassword12345DevQube@@jlasdfjklasjdfkljasdf".getBytes());
    private static final int HALF_HOUR = 30 * 60;
    private static final String ACCOUNT_ID_CLAIM = "accountId";

    public static String generate(UserDetails userDetails, Long accountId) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(Claims.SUBJECT, userDetails.getUsername());
        claims.put(Claims.EXPIRATION, createExpirationTime());
        claims.put(ACCOUNT_ID_CLAIM, accountId);
        return generateToken(claims);
    }

    public static String refreshToken(String token) throws AuthenticationException {
        Claims claims = getClaims(token);
        claims.put(Claims.EXPIRATION, createExpirationTime());
        return generateToken(claims);
    }

    public static Boolean validateToken(String token, UserDetails userDetails) throws AuthenticationException {
        AccountAuthDetails user = (AccountAuthDetails) userDetails;
        String username = getUsernameFromToken(token);
        return (username.equals(user.getUsername()) && !isTokenExpired(token));
    }

    public static String getUsernameFromToken(String token) throws AuthenticationException {
        return getClaims(token).getSubject();
    }

    private static Claims getClaims(String token) throws AuthenticationException {
        try {
            return Jwts.parser()
                    .setAllowedClockSkewSeconds(HALF_HOUR)
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException |
                IllegalArgumentException e) {
            throw new AuthenticationException(e.getMessage());
        }
    }

    private static Boolean isTokenExpired(String token) throws AuthenticationException {
        return getClaims(token).getExpiration().before(new Date());
    }

    private static Calendar createExpirationTime() {
        Calendar expirationTime = Calendar.getInstance();
        expirationTime.add(Calendar.SECOND, HALF_HOUR);
        return expirationTime;
    }

    private static String generateToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(((GregorianCalendar) claims.get(Claims.EXPIRATION)).getTime())
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }
}
