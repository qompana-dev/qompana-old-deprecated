package com.devqube.crmgatewayservice.websocket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class WebsocketService {
    @Autowired
    private SimpMessagingTemplate websocketTemplate;

    public void send(String destination, String content) {
        websocketTemplate.convertAndSend(destination, content);
    }
}
