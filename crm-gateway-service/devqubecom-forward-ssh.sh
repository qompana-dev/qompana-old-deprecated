#!/bin/bash
if [ -z "$1" ]; then
	echo "type port for forwarding"
	exit 1
fi
ssh -R "0.0.0.0:$1:localhost:8078" -N mobile@apps.devqube.com
