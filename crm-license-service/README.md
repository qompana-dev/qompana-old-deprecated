# crm-license-service

How to build crm-license-service with crm-license-frontend
1) Open /crm-license-frontend in terminal.
2) Build frontend using: 
   npm install
   node --max_old_space_size=8192 node_modules/@angular/cli/bin/ng build --configuration=production  --aot --vendor-chunk --common-chunk --delete-output-path --buildOptimizer
3) Copy the contents of /crm-license-frontend/dist/crm-license-frontend folder to /crm-license-service/src/main/resources/static
4) Build docker image using Dockerfile in /crm-license-service folder.