package com.devqube.crmlicenseservice.calendarproxy.google;

import com.devqube.crmlicenseservice.calendarproxy.util.RestUtil;
import com.devqube.crmshared.exception.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.RedirectView;

@Slf4j
@Service
public class GoogleService {
    private final RestTemplate restTemplate;

    public GoogleService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public RedirectView handleCallback(String code, String state) throws BadRequestException {
        return RestUtil.handleCallback(restTemplate, code, state, true);
    }

    public void handlePushNotification(String resourceState, String uri, String apiUrl) throws BadRequestException {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Goog-Resource-State", resourceState);
        headers.set("X-Goog-Resource-URI", uri);
        HttpEntity<Object> httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<String> responseEntity = restTemplate.exchange(apiUrl + "crm-calendar-service/google/notification", HttpMethod.POST, httpEntity, String.class);
        if (!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
            throw new BadRequestException();
        }
    }
}
