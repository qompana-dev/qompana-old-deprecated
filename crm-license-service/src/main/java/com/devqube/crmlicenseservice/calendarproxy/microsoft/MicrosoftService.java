package com.devqube.crmlicenseservice.calendarproxy.microsoft;

import com.devqube.crmlicenseservice.calendarproxy.util.RestUtil;
import com.devqube.crmlicenseservice.calendarproxy.util.StateUtil;
import com.devqube.crmshared.exception.BadRequestException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashSet;
import java.util.Set;

@Service
public class MicrosoftService {
    private final RestTemplate restTemplate;

    public MicrosoftService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public RedirectView handleCallback(String code, String state) throws BadRequestException {
        return RestUtil.handleCallback(restTemplate, code, state, false);
    }

    public void microsoftNotification(String validationToken, String body) throws BadRequestException {
        Set<String> apiUrlsFromNotification = getApiUrlsFromNotification(body);
        try {
            for (String apiUrl : apiUrlsFromNotification) {
                UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(apiUrl + "crm-calendar-service/microsoft/notification")
                        .queryParam("validationToken", validationToken);

                ResponseEntity<String> responseEntity = restTemplate.exchange(builder.build().toUri(), HttpMethod.POST, new HttpEntity<>(body), String.class);
                if (!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
                    throw new BadRequestException();
                }
            }
        } catch (Exception e) {
            throw new BadRequestException();
        }
    }

    private Set<String> getApiUrlsFromNotification(String body) {
        try {
            Set<String> clientStateList = new HashSet<>();
            JSONArray valueList = new JSONObject(body).getJSONArray("value");
            for (int i = 0; i < valueList.length(); i++) {
                try {
                    clientStateList.add(StateUtil.getApiUrl(valueList.getJSONObject(i).getString("clientState")));
                } catch (Exception ignore) {
                }
            }
            return clientStateList;
        } catch (Exception e) {
            return new HashSet<>();
        }
    }

}
