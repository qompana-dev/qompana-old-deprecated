package com.devqube.crmlicenseservice.calendarproxy.util;

import com.devqube.crmshared.exception.BadRequestException;

public class StateUtil {

    public static String getClientUrl(String state) throws BadRequestException {
        return getValueFromState(state, 2);
    }

    public static String getApiUrl(String state) throws BadRequestException {
        return getValueFromState(state, 1);
    }

    public static String getValueFromState(String state, int index) throws BadRequestException {
        if (state == null || state.split(" ").length != 3) {
            throw new BadRequestException();
        }
        return state.split(" ")[index];
    }
}
