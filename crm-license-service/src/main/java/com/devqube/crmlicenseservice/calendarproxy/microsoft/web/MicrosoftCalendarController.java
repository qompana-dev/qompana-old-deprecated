package com.devqube.crmlicenseservice.calendarproxy.microsoft.web;

import com.devqube.crmlicenseservice.calendarproxy.microsoft.MicrosoftService;
import com.devqube.crmshared.exception.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;

@RestController
@Slf4j
public class MicrosoftCalendarController implements MicrosoftApi {
    private final MicrosoftService microsoftService;

    public MicrosoftCalendarController(MicrosoftService microsoftService) {
        this.microsoftService = microsoftService;
    }

    @Override
    public RedirectView handleOauth2Callback(@Valid String code, @Valid String state) throws BadRequestException {
        return microsoftService.handleCallback(code, state);
    }

    @Override
    public ResponseEntity<String> microsoftNotification(@Valid String validationToken, @Valid String body) throws BadRequestException {
        microsoftService.microsoftNotification(validationToken, body);
        return new ResponseEntity<>(validationToken, HttpStatus.OK);
    }
}
