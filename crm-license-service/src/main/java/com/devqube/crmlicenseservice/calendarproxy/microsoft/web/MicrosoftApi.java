package com.devqube.crmlicenseservice.calendarproxy.microsoft.web;

import com.devqube.crmshared.exception.BadRequestException;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.util.Optional;

@Validated
@Api(value = "microsoft")
@RequestMapping("/api")
public interface MicrosoftApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Microsoft callback", nickname = "handleOauth2Callback", notes = "Microsoft callback")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "saved credentials"),
            @ApiResponse(code = 503, message = "Microsoft Calendar not available")})
    @RequestMapping(value = "/microsoft/auth/callback",
            method = RequestMethod.POST)
    default RedirectView handleOauth2Callback(@ApiParam(value = "", required = false) @Valid @RequestParam(value = "code", required = false) String code, @ApiParam(value = "", required = false) @Valid @RequestParam(value = "state", required = false) String state) throws BadRequestException {
        return null;
    }

    @ApiOperation(value = "Microsoft notification callback", nickname = "handleMicrosoftNotification", notes = "Microsoft notification callback", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "notification retrieved", response = String.class)})
    @RequestMapping(value = "/microsoft/notification",
            produces = {"application/json"},
            consumes = {"*"},
            method = RequestMethod.POST)
    default ResponseEntity<String> microsoftNotification(@ApiParam(value = "") @Valid @RequestParam(value = "validationToken", required = false) String validationToken, @ApiParam(value = "Microsoft notification object") @Valid @RequestBody(required = false) String body) throws BadRequestException {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
