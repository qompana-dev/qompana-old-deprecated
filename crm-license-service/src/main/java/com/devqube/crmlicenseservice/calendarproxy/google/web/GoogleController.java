package com.devqube.crmlicenseservice.calendarproxy.google.web;

import com.devqube.crmlicenseservice.calendarproxy.google.GoogleService;
import com.devqube.crmshared.exception.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RestController
@Slf4j
public class GoogleController implements GoogleApi {
    private final GoogleService googleService;

    public GoogleController(GoogleService googleService) {
        this.googleService = googleService;
    }

    @Override
    public RedirectView handleOauth2Callback(String code, String state) throws BadRequestException {
        return googleService.handleCallback(code, state);
    }

    @Override
    public ResponseEntity<Void> handlePushNotification(String resourceState, String uri, String apiUrl) throws BadRequestException {
        this.googleService.handlePushNotification(resourceState, uri, apiUrl);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
