package com.devqube.crmlicenseservice.calendarproxy.util;

import com.devqube.crmshared.exception.BadRequestException;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.UriComponentsBuilder;

public class RestUtil {
    public static RedirectView handleCallback(RestTemplate restTemplate, String code, String state, boolean isGoogle) throws BadRequestException {
        String apiUrl = StateUtil.getApiUrl(state);
        String clientUrl = StateUtil.getClientUrl(state);

        try {
            String url = apiUrl + "crm-calendar-service/" + ((isGoogle) ? "google" : "microsoft") + "/auth/callback";
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                    .queryParam("state", state)
                    .queryParam("code", code);

            HttpMethod method = (isGoogle) ? HttpMethod.GET : HttpMethod.POST;
            ResponseEntity<Void> responseEntity = restTemplate.exchange(builder.build().toUri(), method, null, Void.class);
            if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
                return new RedirectView(clientUrl + "person/info?success=true");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new RedirectView(clientUrl + "person/info?success=false");
    }
}
