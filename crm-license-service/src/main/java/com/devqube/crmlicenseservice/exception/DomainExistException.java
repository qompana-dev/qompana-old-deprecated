package com.devqube.crmlicenseservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DomainExistException extends Exception {
    public DomainExistException() {
    }

    public DomainExistException(String message) {
        super(message);
    }

    public DomainExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public DomainExistException(Throwable cause) {
        super(cause);
    }

    public DomainExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
