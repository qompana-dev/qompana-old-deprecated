package com.devqube.crmlicenseservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ExecScriptException extends Exception {
    public ExecScriptException() {
    }

    public ExecScriptException(String message) {
        super(message);
    }

    public ExecScriptException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExecScriptException(Throwable cause) {
        super(cause);
    }

    public ExecScriptException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
