package com.devqube.crmlicenseservice.exception;

public class ScriptFailedException extends Exception {
    public ScriptFailedException() {
    }

    public ScriptFailedException(String message) {
        super(message);
    }

    public ScriptFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScriptFailedException(Throwable cause) {
        super(cause);
    }

    public ScriptFailedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
