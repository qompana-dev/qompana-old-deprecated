package com.devqube.crmlicenseservice.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("crm")
public class DefaultUserProperties {
    private DefaultUser admin;
    private DefaultUser service;
}
