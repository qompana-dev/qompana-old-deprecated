package com.devqube.crmlicenseservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UnauthorizedUserHandler authenticationEntryPoint;
    @Autowired
    private DefaultUserProperties defaultUserProperties;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser(defaultUserProperties.getAdmin().getLogin())
                .password(defaultUserProperties.getAdmin().getPassword()).authorities("ADMIN");
        auth.inMemoryAuthentication()
                .withUser(defaultUserProperties.getService().getLogin())
                .password(defaultUserProperties.getService().getPassword()).roles("SERVICE");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().configurationSource(corsConfigurationSource()).and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/api/register").permitAll()
                .antMatchers("/api/microsoft/**").permitAll()
                .antMatchers("/api/google/**").permitAll()
                .antMatchers("/api/**").authenticated()
                .antMatchers("/api/service/license/valid/**", "/api/service/license/max-users/**",
                        "/api/service/license/days-left/**").hasRole("SERVICE")
                .antMatchers("/api/license", "/api/license/**", "/api/licenses", "/licenses/**").hasRole("ADMIN")
                .and()
                .httpBasic()
                .authenticationEntryPoint(authenticationEntryPoint);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration().applyPermitDefaultValues();
        corsConfiguration.addAllowedHeader("Origin, X-Requested-With, Content-Type, Accept, Key, Authorization");
        corsConfiguration.setAllowedMethods(Arrays.asList("PUT", "POST", "GET", "OPTION", "DELETE"));
        source.registerCorsConfiguration("/**", corsConfiguration);
        return source;
    }
}
