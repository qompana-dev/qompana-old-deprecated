package com.devqube.crmlicenseservice.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DefaultUser {
    private String login;
    private String password;
}
