package com.devqube.crmlicenseservice;

import com.devqube.crmlicenseservice.config.DefaultUserProperties;
import com.devqube.crmlicenseservice.scripts.CrmShProperties;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;

@EnableAsync
@SpringBootApplication
@EnableConfigurationProperties({
		DefaultUserProperties.class,
		CrmShProperties.class
})
public class CrmLicenseServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrmLicenseServiceApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		return modelMapper;
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
}
