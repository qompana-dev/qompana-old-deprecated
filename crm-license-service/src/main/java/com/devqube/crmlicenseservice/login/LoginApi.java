package com.devqube.crmlicenseservice.login;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

public interface LoginApi {

    @ApiOperation(value = "login")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User logged")
    })
    void login();

    @ApiOperation(value = "logout")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User logged out")
    })
    void logout();
}
