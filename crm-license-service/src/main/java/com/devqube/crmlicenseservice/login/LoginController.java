package com.devqube.crmlicenseservice.login;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/api")
public class LoginController implements LoginApi {
    @Override
    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/login")
    public void login() {

    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/logout")
    public void logout() {
        SecurityContextHolder.clearContext();
    }
}
