package com.devqube.crmlicenseservice.mail;

import com.devqube.crmlicenseservice.mail.model.MailTemplate;
import com.devqube.crmlicenseservice.mail.model.MailTemplateLangEnum;
import com.devqube.crmlicenseservice.mail.model.MailTypeEnum;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;

@Service
public class MailService {
    private final TemplateEngine templateEngine;
    private final MailTemplateRepository mailTemplateRepository;
    private final JavaMailSender mailSender;

    public MailService(TemplateEngine templateEngine, MailTemplateRepository mailTemplateRepository, JavaMailSender mailSender) {
        this.templateEngine = templateEngine;
        this.mailTemplateRepository = mailTemplateRepository;
        this.mailSender = mailSender;
    }

    public void sendEndingLicenseMail(String email, String name, Long days) throws MessagingException {
        Map<String, String> params = new HashMap<>();
        params.put("name", name);
        params.put("days", days.toString());
        sendMailByTemplate(MailTypeEnum.ENDING_LICENSE, email, params);
    }

    public void sendTrialMail(String email, String password, String url) throws MessagingException {
        Map<String, String> params = new HashMap<>();
        params.put("url", url);
        params.put("login", email);
        params.put("password", password);
        sendMailByTemplate(MailTypeEnum.INSTANCE_ACTIVATION_MAIL, email, params);
    }

    public void sendMailByTemplate(MailTypeEnum typeEnum, String email, Map<String, String> params) throws MessagingException {
        sendMailByTemplate(typeEnum, MailTemplateLangEnum.PL, email, params);
    }

    private void sendMailByTemplate(MailTypeEnum typeEnum, MailTemplateLangEnum lang, String email, Map<String, String> params) throws MessagingException {
        MailTemplate template = mailTemplateRepository.findOneByLangAndType(lang, typeEnum);
        String html = getMailFromTemplate(template.getTemplate(), params);
        sendMail(template.getSubject(), html, email);
    }

    private void sendMail(String title, String htmlContent, String email) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
        mimeMessage.setHeader("Content-Type", "text/plain; charset=UTF-8");
        mimeMessage.setContent(htmlContent, "text/html; charset=UTF-8");
        helper.setTo(email);
        helper.setSubject(title);
        mailSender.send(mimeMessage);
    }

    private String getMailFromTemplate(String templateHtml, Map<String, String> params) {
        Context context = new Context();
        params.keySet().forEach(c -> context.setVariable(c, params.get(c)));
        return templateEngine.process(templateHtml, context);
    }
}
