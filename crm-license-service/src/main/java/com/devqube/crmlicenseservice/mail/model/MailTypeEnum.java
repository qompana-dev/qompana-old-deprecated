package com.devqube.crmlicenseservice.mail.model;

public enum MailTypeEnum {
    INSTANCE_ACTIVATION_MAIL, ENDING_LICENSE
}
