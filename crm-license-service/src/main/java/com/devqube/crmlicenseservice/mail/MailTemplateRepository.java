package com.devqube.crmlicenseservice.mail;

import com.devqube.crmlicenseservice.mail.model.MailTemplate;
import com.devqube.crmlicenseservice.mail.model.MailTemplateLangEnum;
import com.devqube.crmlicenseservice.mail.model.MailTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MailTemplateRepository extends JpaRepository<MailTemplate, Long> {
    MailTemplate findOneByLangAndType(MailTemplateLangEnum lang, MailTypeEnum type);
}
