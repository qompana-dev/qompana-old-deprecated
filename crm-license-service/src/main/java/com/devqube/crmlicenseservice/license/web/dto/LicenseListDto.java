package com.devqube.crmlicenseservice.license.web.dto;

import com.devqube.crmlicenseservice.license.model.LicenseTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LicenseListDto {
    private Long id;
    private String name;
    private String phone;
    private String mail;
    private String instanceId;

    private String domain;

    private LicenseTypeEnum type;
    private LocalDate lastPayment;
    private LocalDate expirationDate;

    private Boolean locked;
}
