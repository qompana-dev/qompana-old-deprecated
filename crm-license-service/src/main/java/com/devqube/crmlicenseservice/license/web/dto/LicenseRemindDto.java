package com.devqube.crmlicenseservice.license.web.dto;

import com.devqube.crmlicenseservice.license.model.License;
import com.devqube.crmlicenseservice.license.model.Payment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LicenseRemindDto {
    private License license;
    private Payment payment;
    private Long expiresInDays;
}
