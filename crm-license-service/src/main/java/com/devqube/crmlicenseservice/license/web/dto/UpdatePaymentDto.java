package com.devqube.crmlicenseservice.license.web.dto;

import com.devqube.crmlicenseservice.license.model.LicenseTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdatePaymentDto {
    private Long id;
    @NotNull
    private LocalDate date;
    @NotNull
    private Integer numberOfDays;
    @NotNull
    private Integer numberOfUsers;
    @NotNull
    private LicenseTypeEnum licenseType;
}
