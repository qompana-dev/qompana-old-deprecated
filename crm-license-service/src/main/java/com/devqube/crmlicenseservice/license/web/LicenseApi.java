package com.devqube.crmlicenseservice.license.web;


import com.devqube.crmlicenseservice.exception.DomainExistException;
import com.devqube.crmlicenseservice.exception.ExecScriptException;
import com.devqube.crmlicenseservice.license.web.dto.*;
import com.devqube.crmshared.exception.EntityNotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

public interface LicenseApi {

    @ApiOperation(value = "Check license validity")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "License validity", response = Boolean.class),
            @ApiResponse(code = 404, message = "license not found")
    })
    Boolean isLicenseValid(String instanceId) throws EntityNotFoundException;

    @ApiOperation(value = "Get maximum number of users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Maximum number of users", response = Integer.class),
            @ApiResponse(code = 404, message = "license not found")
    })
    Integer getMaximumNumberOfUser(String instanceId) throws EntityNotFoundException;

    @ApiOperation(value = "Get days left to finish licence")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Days left to finish licence", response = Integer.class),
            @ApiResponse(code = 404, message = "license not found")
    })
    Long getLicenceDaysLeft(String instanceId) throws EntityNotFoundException;

    @ApiOperation(value = "Get all licenses")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "License list", response = LicenseListDto[].class)
    })
    List<LicenseListDto> getLicenses();

    @ApiOperation(value = "Get license")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "License", response = LicenseDto.class),
            @ApiResponse(code = 404, message = "License not found")
    })
    LicenseDto getLicense(Long licenseId) throws EntityNotFoundException;

    @ApiOperation(value = "Create license")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "License created", response = LicenseDto.class),
            @ApiResponse(code = 404, message = "License not found"),
            @ApiResponse(code = 409, message = "Domain or email exist")
    })
    LicenseDto createLicense(CreateLicenseDto createLicenseDto) throws EntityNotFoundException, ExecScriptException, DomainExistException;

    @ApiOperation(value = "Register")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "License created", response = LicenseDto.class),
            @ApiResponse(code = 404, message = "License not found"),
            @ApiResponse(code = 409, message = "Domain or email exist")
    })
    LicenseDto register(CreateLicenseDto createLicenseDto) throws EntityNotFoundException, ExecScriptException, DomainExistException;

    @ApiOperation(value = "add payment")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "payment created", response = LicenseDto.class),
            @ApiResponse(code = 404, message = "License not found")
    })
    LicenseDto addPayment(NewPaymentDto newPaymentDto) throws EntityNotFoundException;

    @ApiOperation(value = "Update license")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "License Updated", response = LicenseDto.class),
            @ApiResponse(code = 404, message = "License not found"),
            @ApiResponse(code = 409, message = "Domain or email exist")
    })
    LicenseDto updateLicense(Long id, UpdateLicenseDto updateLicenseDto) throws EntityNotFoundException, DomainExistException;

    @ApiOperation(value = "Update payment")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "payment updated", response = LicenseDto.class),
            @ApiResponse(code = 404, message = "payment not found")
    })
    LicenseDto updatePayment(Long id, UpdatePaymentDto updatePaymentDto) throws EntityNotFoundException;

    @ApiOperation(value = "Delete payment")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "payment deleted", response = LicenseDto.class),
            @ApiResponse(code = 404, message = "payment not found")
    })
    void deletePayment(Long id) throws EntityNotFoundException;

    @ApiOperation(value = "Change activation status")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "license activation changed", response = LicenseDto.class),
            @ApiResponse(code = 404, message = "license not found")
    })
    LicenseDto changeLicenseActivation(Long id, LicenseActivationDto activation) throws EntityNotFoundException;

    @ApiOperation(value = "create instance")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "instance created", response = String.class),
            @ApiResponse(code = 404, message = "license not found")
    })
    InstanceIdDto createInstance(Long id) throws EntityNotFoundException, ExecScriptException;

    @ApiOperation(value = "remove instance")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "instance removed"),
            @ApiResponse(code = 404, message = "license not found")
    })
    void removeInstance(Long id) throws EntityNotFoundException, ExecScriptException;
}
