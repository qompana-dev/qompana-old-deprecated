package com.devqube.crmlicenseservice.license.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class License {
    @Id
    @SequenceGenerator(name = "license_seq", sequenceName = "license_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "license_seq")
    private Long id;

    private String name;
    private String phone;
    @Column(name = "mail", unique = true)
    private String mail;

    @Column(name = "domain", unique = true)
    private String domain;

    private boolean locked;

    private String instanceId;

    private Boolean localInstance;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "license", orphanRemoval = true)
    private List<Payment> payments = new ArrayList<>();
}
