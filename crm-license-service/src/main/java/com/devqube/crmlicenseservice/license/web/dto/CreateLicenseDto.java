package com.devqube.crmlicenseservice.license.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateLicenseDto {
    @NotNull
    private boolean addTrial;
    @NotNull
    private String name;
    @NotNull
    private String phone;
    @NotNull
    private String mail;
    @NotNull
    private String domain;
    private String instanceId;
}
