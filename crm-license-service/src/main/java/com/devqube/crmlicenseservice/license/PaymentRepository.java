package com.devqube.crmlicenseservice.license;

import com.devqube.crmlicenseservice.license.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
    List<Payment> findAllByLicenseInstanceIdAndPaymentDateBeforeAndExpiredDateAfter(String licenseInstanceId, LocalDate before, LocalDate after);
}
