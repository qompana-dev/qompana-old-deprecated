package com.devqube.crmlicenseservice.license.web;


import com.devqube.crmlicenseservice.exception.DomainExistException;
import com.devqube.crmlicenseservice.exception.ExecScriptException;
import com.devqube.crmlicenseservice.license.LicenseService;
import com.devqube.crmlicenseservice.license.web.dto.*;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@Slf4j
@RequestMapping("/api")
public class LicenseController implements LicenseApi {
    private final LicenseService licenseService;

    public LicenseController(LicenseService licenseService) {
        this.licenseService = licenseService;
    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/service/license/valid/{instanceId}")
    public Boolean isLicenseValid(@PathVariable("instanceId") String instanceId) throws EntityNotFoundException {
        return licenseService.isLicenseValid(instanceId);
    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/service/license/max-users/{instanceId}")
    public Integer getMaximumNumberOfUser(@PathVariable("instanceId") String instanceId) throws EntityNotFoundException {
        return licenseService.getMaxNumberOfUsers(instanceId);
    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/service/license/days-left/{instanceId}")
    public Long getLicenceDaysLeft(@PathVariable("instanceId") String instanceId) throws EntityNotFoundException {
        return licenseService.getLicenceDaysLeft(instanceId);
    }

    @Override
    @GetMapping("/licenses")
    @ResponseStatus(HttpStatus.OK)
    public List<LicenseListDto> getLicenses() {
        return licenseService.getLicenses();
    }

    @Override
    @GetMapping("/license/{id}")
    @ResponseStatus(HttpStatus.OK)
    public LicenseDto getLicense(@PathVariable("id") Long licenseId) throws EntityNotFoundException {
        return licenseService.getLicense(licenseId);
    }

    @Override
    @PostMapping("/license")
    @ResponseStatus(HttpStatus.CREATED)
    public LicenseDto createLicense(@RequestBody CreateLicenseDto createLicenseDto) throws EntityNotFoundException, ExecScriptException, DomainExistException {
        return licenseService.createLicense(createLicenseDto);
    }

    @Override
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public LicenseDto register(@RequestBody CreateLicenseDto createLicenseDto) throws EntityNotFoundException, ExecScriptException, DomainExistException {
        return licenseService.createLicense(createLicenseDto);
    }

    @Override
    @PostMapping("/license/payment")
    @ResponseStatus(HttpStatus.CREATED)
    public LicenseDto addPayment(@RequestBody NewPaymentDto newPaymentDto) throws EntityNotFoundException {
        return licenseService.addPayment(newPaymentDto);
    }

    @Override
    @PutMapping("/license/payment/{id}")
    @ResponseStatus(HttpStatus.OK)
    public LicenseDto updatePayment(@PathVariable("id") Long id, @RequestBody UpdatePaymentDto updatePaymentDto) throws EntityNotFoundException {
        updatePaymentDto.setId(id);
        return licenseService.updatePayment(updatePaymentDto);
    }

    @Override
    @DeleteMapping("/license/payment/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePayment(@PathVariable("id") Long id) throws EntityNotFoundException {
        licenseService.deletePayment(id);
    }

    @Override
    @PutMapping("/license/{id}")
    @ResponseStatus(HttpStatus.OK)
    public LicenseDto updateLicense(@PathVariable("id") Long id, @RequestBody UpdateLicenseDto updateLicenseDto) throws EntityNotFoundException, DomainExistException {
        updateLicenseDto.setId(id);
        return licenseService.updateLicense(updateLicenseDto);
    }

    @Override
    @PostMapping("/license/{id}/activation")
    @ResponseStatus(HttpStatus.OK)
    public LicenseDto changeLicenseActivation(@PathVariable("id") Long id, @RequestBody LicenseActivationDto activation) throws EntityNotFoundException {
        return licenseService.changeLicenseActivation(id, activation.getActive());
    }

    @Override
    @PostMapping("/license/{id}/instance")
    @ResponseStatus(HttpStatus.CREATED)
    public InstanceIdDto createInstance(@PathVariable("id") Long id) throws EntityNotFoundException, ExecScriptException {
        return licenseService.createInstance(id);
    }

    @Override
    @DeleteMapping("/license/{id}/instance")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeInstance(@PathVariable("id") Long id) throws EntityNotFoundException, ExecScriptException {
        licenseService.removeInstance(id);
    }
}
