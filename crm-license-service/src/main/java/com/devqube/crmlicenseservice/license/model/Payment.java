package com.devqube.crmlicenseservice.license.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Payment {
    @Id
    @SequenceGenerator(name = "payment_seq", sequenceName = "payment_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "payment_seq")
    private Long id;
    private LocalDate paymentDate;
    @ManyToOne
    private License license;
    private Integer numberOfDays;
    private Integer numberOfUsers;
    private LocalDate expiredDate;

    @Enumerated(EnumType.STRING)
    private LicenseTypeEnum licenseType;
}
