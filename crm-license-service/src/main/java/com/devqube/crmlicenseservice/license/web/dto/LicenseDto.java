package com.devqube.crmlicenseservice.license.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LicenseDto {
    private Long id;

    private String name;
    private String phone;
    private String mail;

    private String domain;

    private Boolean locked;

    private Boolean licenseCheckResult;

    private String instanceId;

    private Boolean localInstance;

    private List<PaymentDto> payments = new ArrayList<>();
}
