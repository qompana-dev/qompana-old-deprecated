package com.devqube.crmlicenseservice.license;

import com.devqube.crmlicenseservice.license.model.License;
import com.devqube.crmlicenseservice.license.model.Payment;
import com.devqube.crmlicenseservice.license.web.dto.LicenseRemindDto;
import com.devqube.crmlicenseservice.mail.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
@EnableScheduling
public class LicenseReminderService {
    @Value("${crm-notification.intervals:1,3,7}")
    private String crmIntervals;

    private final LicenseService licenseService;
    private final LicenseRepository licenseRepository;
    private final MailService mailService;

    public LicenseReminderService(LicenseService licenseService, LicenseRepository licenseRepository, MailService mailService) {
        this.licenseService = licenseService;
        this.licenseRepository = licenseRepository;
        this.mailService = mailService;
    }

    @Transactional
    @Scheduled(cron = "0 0 8 ? * *")
    public void sendReminds() {
        List<LicenseRemindDto> list = getLicenseRemindList();
        for (LicenseRemindDto remind : list) {
            try {
                mailService.sendEndingLicenseMail(remind.getLicense().getMail(), remind.getLicense().getName(), remind.getExpiresInDays());
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }

    private List<LicenseRemindDto> getLicenseRemindList() {
        LocalDate now = LocalDate.now();

        List<Long> intervals = getIntervals();

        return licenseRepository.findAll().stream()
                .map(c -> getLicenseRemind(c, now))
                .filter(Objects::nonNull)
                .filter(c -> intervals.contains(c.getExpiresInDays()))
                .collect(Collectors.toList());
    }

    private LicenseRemindDto getLicenseRemind(License license, LocalDate now) {
        Payment lastPayment = getLastPayment(license, now);
        if (lastPayment == null) {
            return null;
        }
        LicenseRemindDto licenseRemindDto = new LicenseRemindDto();
        licenseRemindDto.setLicense(license);
        licenseRemindDto.setPayment(null);
        licenseRemindDto.setExpiresInDays(getDaysDiff(lastPayment, now));
        return licenseRemindDto;
    }

    private Payment getLastPayment(License license, LocalDate now) {
        List<Payment> paymentsActiveToday = licenseService.getPaymentsActiveToday(license, now);
        if (paymentsActiveToday == null || paymentsActiveToday.size() == 0) {
            return null;
        }
        return paymentsActiveToday.stream().max(Comparator.comparing(Payment::getExpiredDate)).orElseGet(null);
    }

    private long getDaysDiff(Payment payment, LocalDate now) {
        return ChronoUnit.DAYS.between(now, payment.getExpiredDate());
    }

    private List<Long> getIntervals() {
        return Arrays.stream(crmIntervals.split(",")).map(Long::parseLong).collect(Collectors.toList());
    }
}
