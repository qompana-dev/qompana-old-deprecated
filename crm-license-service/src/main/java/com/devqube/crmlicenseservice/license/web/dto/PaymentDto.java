package com.devqube.crmlicenseservice.license.web.dto;

import com.devqube.crmlicenseservice.license.model.LicenseTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentDto {
    private Long id;
    private LicenseTypeEnum licenseType;
    private LocalDate paymentDate;
    private Integer numberOfDays;
    private Integer numberOfUsers;
    private LocalDate expiredDate;
}
