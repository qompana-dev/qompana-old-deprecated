package com.devqube.crmlicenseservice.license.model;

public enum LicenseTypeEnum {
    FULL, TRIAL
}
