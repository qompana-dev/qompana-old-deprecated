package com.devqube.crmlicenseservice.license;

import com.devqube.crmlicenseservice.exception.DomainExistException;
import com.devqube.crmlicenseservice.exception.ExecScriptException;
import com.devqube.crmlicenseservice.license.model.License;
import com.devqube.crmlicenseservice.license.model.LicenseTypeEnum;
import com.devqube.crmlicenseservice.license.model.Payment;
import com.devqube.crmlicenseservice.license.web.dto.*;
import com.devqube.crmlicenseservice.mail.MailService;
import com.devqube.crmlicenseservice.scripts.ScriptsService;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class LicenseService {
    @Value("${crm.trial.days:30}")
    private Integer crmTrialDays;
    @Value("${crm.trial.users:50}")
    private Integer crmTrialUsers;

    private final LicenseRepository licenseRepository;
    private final PaymentRepository paymentRepository;
    private final ModelMapper modelMapper;
    private final ScriptsService scriptsService;
    private final MailService mailService;

    public LicenseService(LicenseRepository licenseRepository, PaymentRepository paymentRepository, ModelMapper modelMapper, ScriptsService scriptsService, MailService mailService) {
        this.licenseRepository = licenseRepository;
        this.paymentRepository = paymentRepository;
        this.modelMapper = modelMapper;
        this.scriptsService = scriptsService;
        this.mailService = mailService;
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class, ExecScriptException.class})
    public LicenseDto createLicense(CreateLicenseDto createLicenseDto) throws EntityNotFoundException, ExecScriptException, DomainExistException {
        checkDomainName(createLicenseDto.getDomain(), createLicenseDto.getMail(), null);
        License license = modelMapper.map(createLicenseDto, License.class);
        license.setLocked(false);
        license.setLocalInstance(false);
        if (license.getInstanceId() != null && license.getInstanceId().length() > 0) {
            license.setInstanceId(createLicenseDto.getInstanceId());
            license.setLocalInstance(true);
        }
        License saved = licenseRepository.save(license);
        if (createLicenseDto.isAddTrial()) {
            LocalDate now = LocalDate.now();
            Payment payment = Payment
                    .builder().paymentDate(now)
                    .licenseType(LicenseTypeEnum.TRIAL)
                    .numberOfUsers(crmTrialUsers)
                    .expiredDate(getExpiredDate(now, crmTrialDays))
                    .license(saved).numberOfDays(crmTrialDays).build();
            paymentRepository.save(payment);
        }
        updateExpiredDateForLicensePayments(license.getId());
        if (!saved.getLocalInstance()) {
            createInstance(saved.getId());
        }
        return getLicense(saved.getId());
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class})
    public LicenseDto updateLicense(UpdateLicenseDto updateLicenseDto) throws EntityNotFoundException, DomainExistException {
        checkDomainName(updateLicenseDto.getDomain(), updateLicenseDto.getMail(), updateLicenseDto.getId());
        License license = licenseRepository.findById(updateLicenseDto.getId()).orElseThrow(EntityNotFoundException::new);
        license.setName(updateLicenseDto.getName());
        license.setPhone(updateLicenseDto.getPhone());
        license.setMail(updateLicenseDto.getMail());
        license.setDomain(updateLicenseDto.getDomain());

        if (license.getLocalInstance() || license.getInstanceId() == null) {
            license.setInstanceId(Strings.emptyToNull(updateLicenseDto.getInstanceId()));
            license.setLocalInstance(true);
        }

        return getLicense(updateLicenseDto.getId());
    }

    public List<LicenseListDto> getLicenses() {
        return licenseRepository.findAll().stream().map(this::getLicenseListDto).collect(Collectors.toList());
    }

    private LicenseListDto getLicenseListDto(License license) {
        LicenseListDto result = modelMapper.map(license, LicenseListDto.class);

        if (license.getPayments() != null) {
            LocalDate now = LocalDate.now();
            List<Payment> activePayments = license.getPayments().stream()
                    .filter(c -> now.isBefore(c.getExpiredDate()) && now.plusDays(1).isAfter(c.getPaymentDate()))
                    .collect(Collectors.toList());

            if (activePayments.size() > 0) {
                LicenseTypeEnum type = (activePayments.stream().anyMatch(c -> c.getLicenseType().equals(LicenseTypeEnum.FULL))) ? LicenseTypeEnum.FULL : LicenseTypeEnum.TRIAL;
                result.setType(type);
            }

            license.getPayments().stream().max(Comparator.comparing(c -> c.getPaymentDate().toEpochSecond(LocalTime.MIDNIGHT, ZoneOffset.UTC)))
                    .ifPresent(payment -> result.setLastPayment(payment.getPaymentDate()));

            license.getPayments().stream().max(Comparator.comparing(c -> c.getExpiredDate().toEpochSecond(LocalTime.MIDNIGHT, ZoneOffset.UTC)))
                    .ifPresent(payment -> result.setExpirationDate(payment.getExpiredDate()));
        }
        return result;
    }

    public LicenseDto getLicense(Long id) throws EntityNotFoundException {
        License license = licenseRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        LicenseDto result = modelMapper.map(license, LicenseDto.class);
        result.setLicenseCheckResult((license.getInstanceId() != null) && isLicenseValid(license.getInstanceId()));
        return result;
    }

    public void deletePayment(Long id) throws EntityNotFoundException {
        Payment payment = paymentRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        Long licenseId = payment.getLicense().getId();
        paymentRepository.deleteById(id);
        updateExpiredDateForLicensePayments(licenseId);
    }

    public LicenseDto updatePayment(@Valid UpdatePaymentDto updatePayment) throws EntityNotFoundException {
        Payment payment = paymentRepository.findById(updatePayment.getId()).orElseThrow(EntityNotFoundException::new);
        payment.setPaymentDate(updatePayment.getDate());
        payment.setNumberOfDays(updatePayment.getNumberOfDays());
        payment.setLicenseType(updatePayment.getLicenseType());
        payment.setNumberOfUsers(updatePayment.getNumberOfUsers());
        payment.setExpiredDate(getExpiredDate(updatePayment.getDate(), updatePayment.getNumberOfDays()));

        paymentRepository.save(payment);

        updateExpiredDateForLicensePayments(payment.getLicense().getId());

        return getLicense(payment.getLicense().getId());
    }

    public LicenseDto addPayment(@Valid NewPaymentDto newPaymentDto) throws EntityNotFoundException {
        License license = this.licenseRepository.findById(newPaymentDto.getLicenseId()).orElseThrow(EntityNotFoundException::new);

        Payment payment = Payment
                .builder().paymentDate(newPaymentDto.getDate())
                .licenseType(newPaymentDto.getLicenseType())
                .expiredDate(getExpiredDate(newPaymentDto.getDate(), newPaymentDto.getNumberOfDays()))
                .numberOfUsers(newPaymentDto.getNumberOfUsers())
                .license(license).numberOfDays(newPaymentDto.getNumberOfDays()).build();
        paymentRepository.save(payment);

        updateExpiredDateForLicensePayments(license.getId());

        return getLicense(license.getId());
    }

    @Transactional
    public boolean isLicenseValid(String instanceId) throws EntityNotFoundException {
        License license = licenseRepository.findByInstanceId(instanceId).orElseThrow(EntityNotFoundException::new);
        LocalDate now = LocalDate.now();
        return (getPaymentsActiveToday(license, now).size() > 0);
    }

    @Transactional
    public Integer getMaxNumberOfUsers(String instanceId) throws EntityNotFoundException {
        License license = licenseRepository.findByInstanceId(instanceId).orElseThrow(EntityNotFoundException::new);
        LocalDate now = LocalDate.now();
        return getPaymentsActiveToday(license, now)
                .stream().filter(payment -> payment.getNumberOfUsers() != null)
                .mapToInt(Payment::getNumberOfUsers).sum();
    }

    @Transactional
    public Long getLicenceDaysLeft(String instanceId) throws EntityNotFoundException {
        License license = licenseRepository.findByInstanceId(instanceId).orElseThrow(EntityNotFoundException::new);
        LocalDate endDate = getPaymentsActive(license)
                .stream()
                .map(Payment::getExpiredDate)
                .filter(Objects::nonNull)
                .max(Comparator.comparing(LocalDate::toEpochDay))
                .orElseThrow(EntityNotFoundException::new);
        return endDate.toEpochDay() - LocalDate.now().toEpochDay();
    }

    private List<Payment> getPaymentsActive(License license){
        if (license.isLocked() || license.getPayments() == null) {
            return new ArrayList<>();
        }
        return license.getPayments();
    }
    public List<Payment> getPaymentsActiveToday(License license, LocalDate now) {
        List<Payment> paymentList = getPaymentsActive(license);
        return paymentList
                .stream()
                .filter(c -> c.getPaymentDate().isBefore(now.plusDays(1)) && c.getExpiredDate().isAfter(now.minusDays(1)))
                .collect(Collectors.toList());
    }

    public LicenseDto changeLicenseActivation(Long id, @NotNull Boolean activation) throws EntityNotFoundException {
        License license = licenseRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        license.setLocked(!activation);
        License saved = licenseRepository.save(license);
        return getLicense(saved.getId());
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class, ExecScriptException.class})
    public InstanceIdDto createInstance(Long licenseId) throws EntityNotFoundException, ExecScriptException {
        License license = licenseRepository.findById(licenseId).orElseThrow(EntityNotFoundException::new);
        String newUUID = getUuid();
        license.setInstanceId(newUUID);
        license.setLocalInstance(false);
        licenseRepository.save(license);
        String password = RandomStringUtils.randomAlphanumeric(30);
        try {
            scriptsService.createInstance(license.getInstanceId(), license.getDomain(), license.getMail(), password);
            mailService.sendTrialMail(license.getMail(), password, scriptsService.getInstanceUrl(licenseId));
        } catch (Exception e) {
            e.printStackTrace();
            throw new ExecScriptException();
        }
        return new InstanceIdDto(newUUID);
    }

    @Transactional(rollbackFor = {EntityNotFoundException.class, ExecScriptException.class})
    public void removeInstance(Long licenseId) throws EntityNotFoundException, ExecScriptException {
        License license = licenseRepository.findById(licenseId).orElseThrow(EntityNotFoundException::new);
        String currentInstanceId = license.getInstanceId();
        license.setInstanceId(null);
        license.setLocalInstance(true);
        licenseRepository.save(license);
        try {
            scriptsService.removeInstance(currentInstanceId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ExecScriptException();
        }
    }

    private Optional<Payment> getLastPayment(License license) {
        return license.getPayments().stream()
                .max(Comparator.comparing(Payment::getExpiredDate));
    }

    private LocalDate getExpiredDate(LocalDate date, Integer expiredDays) {
        return date.plusDays(expiredDays);
    }

    private void updateExpiredDateForLicensePayments(Long licenseId) throws EntityNotFoundException {
        License license = licenseRepository.findById(licenseId).orElseThrow(EntityNotFoundException::new);
        if (license.getPayments() != null) {
            List<Payment> sorted = license.getPayments().stream().sorted(Comparator.comparing(Payment::getPaymentDate)).collect(Collectors.toList());
            for (int i = 0; i < sorted.size(); i++) {
                LocalDate expired = sorted.get(i).getPaymentDate().plusDays(sorted.get(i).getNumberOfDays());
                if (i != 0 && sorted.get(i).getPaymentDate().isBefore(sorted.get(i - 1).getExpiredDate())
                        && sorted.get(i).getPaymentDate().isAfter(sorted.get(i - 1).getPaymentDate().minusDays(1))) {
                    expired = sorted.get(i - 1).getExpiredDate().plusDays(sorted.get(i).getNumberOfDays());
                }
                sorted.get(i).setExpiredDate(expired);
            }
            paymentRepository.saveAll(sorted);
        }
    }

    private String getUuid() {
        return UUID.randomUUID().toString();
    }

    private void checkDomainName(String domain, String email, Long currentLicenseId) throws DomainExistException {
        List<License> allByDomainOrMail = licenseRepository.findAllByDomainOrMail(domain, email);
        if (allByDomainOrMail.size() > 0 && allByDomainOrMail.stream().anyMatch(c -> !c.getId().equals(currentLicenseId))) {
            throw new DomainExistException();
        }
    }
}
