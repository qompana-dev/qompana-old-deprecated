package com.devqube.crmlicenseservice.license;

import com.devqube.crmlicenseservice.license.model.License;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LicenseRepository extends JpaRepository<License, Long> {
    Optional<License> findByInstanceId(String instanceId);

    Optional<License> findByMail(String mail);

    List<License> findAllByDomainOrMail(String domain, String mail);
}
