package com.devqube.crmlicenseservice.scripts;

import org.apache.commons.lang3.ArrayUtils;

import java.io.*;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ShRunner {
    public static int runScript(String dir, String scriptName, String[] args) throws IOException, InterruptedException {
        ProcessBuilder builder = new ProcessBuilder();
        builder.command("sh", "-c", getCommand(args, scriptName));
        System.out.println(String.join(" ", builder.command()));
        builder.directory(new File(dir));
        Process process = builder.start();
        StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), System.out::println);
        Executors.newSingleThreadExecutor().submit(streamGobbler);
        return process.waitFor();
    }

    private static String getCommand(String[] args, String scriptName) {
        if (args == null) {
            args = new String[0];
        }
        String argsToAdd = "'" + String.join("' '", args) + "'";
        return "./" + scriptName + " " + argsToAdd;
    }

    private static class StreamGobbler implements Runnable {
        private InputStream inputStream;
        private Consumer<String> consumer;

        public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
            this.inputStream = inputStream;
            this.consumer = consumer;
        }

        @Override
        public void run() {
            new BufferedReader(new InputStreamReader(inputStream)).lines()
                    .forEach(consumer);
        }
    }
}
