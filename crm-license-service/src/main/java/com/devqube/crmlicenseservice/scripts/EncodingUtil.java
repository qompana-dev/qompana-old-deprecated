package com.devqube.crmlicenseservice.scripts;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

class EncodingUtil {

    static String encodePassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }
}
