package com.devqube.crmlicenseservice.scripts;

import com.devqube.crmlicenseservice.config.DefaultUser;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("crm-sh")
public class CrmShProperties {
    private String dir;
    private String createInstance;
    private String removeInstance;
    private String urlPattern;
}
