package com.devqube.crmlicenseservice.scripts;

import com.devqube.crmlicenseservice.exception.ScriptFailedException;
import com.devqube.crmlicenseservice.license.LicenseRepository;
import com.devqube.crmlicenseservice.license.model.License;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.io.IOException;
import java.util.regex.Pattern;

@Slf4j
@EnableScheduling
@Service
public class ScriptsService {
    private final CrmShProperties crmShProperties;
    private final LicenseRepository licenseRepository;

    public ScriptsService(CrmShProperties crmShProperties, LicenseRepository licenseRepository) {
        this.crmShProperties = crmShProperties;
        this.licenseRepository = licenseRepository;
    }

    public void createInstance(String uuid, String domain, String email, String password) throws IOException, InterruptedException, ScriptFailedException {
        int result = ShRunner.runScript(crmShProperties.getDir(), crmShProperties.getCreateInstance(), new String[]{uuid, domain, email, EncodingUtil.encodePassword(password)});
        if (result != 0) {
            throw new ScriptFailedException();
        }
    }

    public void removeInstance(String uuid) throws IOException, InterruptedException, ScriptFailedException {
        int result = ShRunner.runScript(crmShProperties.getDir(), crmShProperties.getRemoveInstance(), new String[]{uuid});
        if (result != 0) {
            throw new ScriptFailedException();
        }
    }

    public String getInstanceUrl(Long id) {
        License license = licenseRepository.findById(id).orElseThrow(EntityExistsException::new);
        String result = crmShProperties.getUrlPattern();
        result = result.replaceAll(Pattern.quote("{{id}}"), license.getId().toString());
        result = result.replaceAll(Pattern.quote("{{name}}"), license.getName());
        result = result.replaceAll(Pattern.quote("{{phone}}"), license.getPhone());
        result = result.replaceAll(Pattern.quote("{{mail}}"), license.getMail());
        result = result.replaceAll(Pattern.quote("{{domain}}"), license.getDomain());
        result = result.replaceAll(Pattern.quote("{{instanceId}}"), license.getInstanceId());
        return result;
    }
}
