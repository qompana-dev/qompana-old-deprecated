insert into license (id, name, phone, mail, instance_id, locked, domain, local_instance) values (-1, 'dev', '48123123123', 'mail@devqube.com', 'local-dev', false, '4202', true);
insert into license (id, name, phone, mail, instance_id, locked, domain, local_instance) values (-2, 'Engave Double CRM', '48123123123', 'crm.engave@gmail.com', 'double_crm', false, 'https://doublecrm.doublecloud.pl', true);

insert into payment(id, payment_date, license_id, number_of_days, number_of_users, expired_date, license_type) values (-1, '2020-01-28 14:59:07.784945', -1, 3650, 10000, '2030-01-28 14:59:07.784945', 'FULL');
insert into payment(id, payment_date, license_id, number_of_days, number_of_users, expired_date, license_type) values (-2, '2020-01-28 14:59:07.784945', -2, 3650, 10000, '2030-01-28 14:59:07.784945', 'FULL');

insert into mail_template(id, lang, subject, type, template)
values(-1, 0, 'Utworzenie instancji', 0, '<!DOCTYPE html><html lang="pl" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"><head>    <meta charset="utf-8">    <meta http-equiv="Content-Type: text/html;charset=utf-8" /></head><body><h1>Utworzono instancje</h1><p>Utworzono nową instancje pod adresem   <br/><br/><b>Adres</b> : <a th:href="${url}"><span th:text="${url}"></span></a> <br/> <b>Login</b> : <span th:text="${login}"></span> <br/> <b>Hasło</b> : <span th:text="${password}"></span></p></body></html>');

insert into mail_template(id, lang, subject, type, template)
values(-2, 0, 'Kończąca się licencja', 1, '<!DOCTYPE html><html lang="pl" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"><head>    <meta charset="utf-8">    <meta http-equiv="Content-Type: text/html;charset=utf-8" /></head><body><h1>Kończąca się licencja</h1><p>Instancja utworzona dla  <span th:text="${name}"></span> kończy się za <span th:text="${days}"></span> dni.<br/> Proszę o dokonanie opłaty licencyjnej</p></body></html>');
