create table license
(
    id bigint primary key,
    name varchar(200) not null,
    locked boolean not null,
    phone varchar(11) not null,
    mail varchar(200) UNIQUE not null,
    instance_id varchar(200),
    local_instance boolean not null,
    domain varchar(200) UNIQUE not null
);
create sequence license_seq start 1;

create table payment
(
    id bigint primary key,
    payment_date TIMESTAMP not null,
    license_id bigint not null,
    number_of_days bigint not null,
    number_of_users bigint not null,
    expired_date TIMESTAMP not null,
    license_type varchar(200) not null,
    CONSTRAINT payment_license_fk FOREIGN KEY(license_id)
        REFERENCES license(id)
);
create sequence payment_seq start 1;

create table mail_template
(
    id       bigint PRIMARY KEY,
    lang     int8 NOT NULL,
    subject  VARCHAR(200),
    template text,
    type     int8 NOT NULL,
    UNIQUE (lang, type)
);

COMMENT ON COLUMN mail_template.lang IS '0 - PL;';
COMMENT ON COLUMN mail_template.type IS '0 - INSTANCE_ACTIVATION_MAIL;';

create sequence mail_template_seq start 1;
