#!/bin/bash

echo -e "instanceId = $1 \ndomain = $2 \nemail = $3 \npassword=$4"

# powinien zostać utworzony plik .env z wartościami w miejscu docker-compose.yml
#
# CRM_CLIENT_INSTANCE_ID=idInstancji
# CRM_CLIENT_URL=adresUrl
#
# adresUrl to np. https://doublecrm.doublecloud.pl/
# idInstancji to $1
#
# w pliku tworzącym należy również wywołać modyfikacje pola w db
# np.
#
# echo "update person set name='admin', surname='admin', email='$3', password='$4' where id = -1;" >> crm-user-service-db.sql
# psql "postgresql://postgres:postgresdock@localhost:5432/postgres" -a -f "crm-user-service-db.sql"
# rm -f crm-user-service-db.sql
