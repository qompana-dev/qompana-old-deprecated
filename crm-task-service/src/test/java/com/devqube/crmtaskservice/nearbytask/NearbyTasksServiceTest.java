package com.devqube.crmtaskservice.nearbytask;

import com.devqube.crmtaskservice.nearbytasks.NearbyTasksService;
import com.devqube.crmtaskservice.nearbytasks.web.dto.GeoCoordinatesDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class NearbyTasksServiceTest {
    private static final double DELTA = 1e-7;
    @InjectMocks
    private NearbyTasksService nearbyTasksService;

    @Test
    public void shouldGetCorrectMinLatitudeDistance() {
        Double lat1 = 51.0882253;
        Double lon1 = 17.012205;

        Double lat2 = 51.047184;
        Double lon2 = 16.961832;

        double distance = NearbyTasksService.distance(lat1, lon1, lat2, lon1);
        GeoCoordinatesDTO radiusAroundPoint = NearbyTasksService.getRadiusAroundPoint(lat1, lon1, distance);
        assertEquals(lat2.doubleValue(), radiusAroundPoint.getMinLatitude().doubleValue(), DELTA);
    }

    @Test
    public void shouldGetCorrectMaxLatitudeDistance() {
        Double lat1 = 51.0882253;
        Double lon1 = 17.012205;

        Double lat2 = 51.127184;
        Double lon2 = 16.961832;

        double distance = NearbyTasksService.distance(lat1, lon1, lat2, lon1);
        GeoCoordinatesDTO radiusAroundPoint = NearbyTasksService.getRadiusAroundPoint(lat1, lon1, distance);
        assertEquals(lat2.doubleValue(), radiusAroundPoint.getMaxLatitude().doubleValue(), DELTA);
    }

    @Test
    public void shouldGetCorrectMinLongitudeDistance() {
        Double lat1 = 51.0882253;
        Double lon1 = 17.012205;

        Double lat2 = 51.047184;
        Double lon2 = 16.961832;

        double distance = NearbyTasksService.distance(lat1, lon1, lat1, lon2);
        GeoCoordinatesDTO radiusAroundPoint = NearbyTasksService.getRadiusAroundPoint(lat1, lon1, distance);
        assertEquals(lon2.doubleValue(), radiusAroundPoint.getMinLongitude().doubleValue(), DELTA);
    }

    @Test
    public void shouldGetCorrectMaxLongitudeDistance() {
        Double lat1 = 51.0882253;
        Double lon1 = 17.012205;

        Double lat2 = 51.047184;
        Double lon2 = 17.161832;

        double distance = NearbyTasksService.distance(lat1, lon1, lat1, lon2);
        GeoCoordinatesDTO radiusAroundPoint = NearbyTasksService.getRadiusAroundPoint(lat1, lon1, distance);
        assertEquals(lon2.doubleValue(), radiusAroundPoint.getMaxLongitude().doubleValue(), DELTA);
    }
}
