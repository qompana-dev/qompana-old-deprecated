package com.devqube.crmtaskservice.task.web;

import com.devqube.crmtaskservice.AbstractIntegrationTest;
import com.devqube.crmtaskservice.task.TaskRepository;
import com.devqube.crmtaskservice.task.model.Task;
import com.devqube.crmtaskservice.task.model.TaskPriority;
import com.devqube.crmtaskservice.task.model.TaskState;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.netflix.ribbon.StaticServerList;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ActiveProfiles({"test", "wiremock"})
@ContextConfiguration(classes = {TasksControllerIntegrationTest.LocalClientConfiguration.class})
public class TasksControllerIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private TaskRepository taskRepository;

    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(WireMockConfiguration.options().dynamicPort());

    @Before
    public void setUp() {
        super.setUp();

        stubFor(post(urlEqualTo("/associations/list"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("[]")));
        stubFor(get(urlMatching("/associations/type/task/id/.*"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("[]")));
        stubFor(put(urlMatching("/associations/list/type/task/id/.*"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200).withBody("[]")));
    }

    @After
    public void cleanUp() {
        this.taskRepository.deleteAll();
    }

    @Test
    public void shouldGetThreeTasksWhenOwnerHasIt() {
        taskRepository.saveAll(List.of(
                Task.builder()
                        .subject("1")
                        .owner(1L)
                        .priority(TaskPriority.URGENT)
                        .state(TaskState.CREATED)
                        .allDayEvent(false)
                        .dateTimeFrom(LocalDateTime.now())
                        .dateTimeTo(LocalDateTime.now())
                        .creator(1L).build(),
                Task.builder()
                        .subject("1")
                        .owner(1L)
                        .priority(TaskPriority.MEDIUM)
                        .state(TaskState.IN_PROGRESS)
                        .allDayEvent(false)
                        .dateTimeFrom(LocalDateTime.now())
                        .dateTimeTo(LocalDateTime.now())
                        .creator(1L).build(),
                Task.builder()
                        .subject("1")
                        .owner(1L)
                        .priority(TaskPriority.URGENT)
                        .allDayEvent(false)
                        .dateTimeFrom(LocalDateTime.now())
                        .dateTimeTo(LocalDateTime.now())
                        .state(TaskState.FINISHED)
                        .creator(1L).build()
        ));
        ResponseEntity<Task[]> response = restTemplate.getForEntity(baseUrl + "/tasks?owner=1", Task[].class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(3L, response.getBody().length);
    }

    @Test
    public void shouldReturnEmptyListWhenOwnerDoesntHaveAnyTasks() {
        ResponseEntity<Task[]> response = restTemplate.getForEntity(baseUrl + "/tasks?owner=1", Task[].class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(0L, response.getBody().length);
    }

    @Test
    public void shouldRemoveTaskWhenFound() {
        Long id = taskRepository.save(Task.builder()
                .subject("1")
                .owner(1L)
                .priority(TaskPriority.URGENT)
                .state(TaskState.CREATED)
                .allDayEvent(false)
                .dateTimeFrom(LocalDateTime.now())
                .dateTimeTo(LocalDateTime.now())
                .creator(1L).build()).getId();
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/tasks/" + id + "?sync=false", HttpMethod.DELETE, null, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void shouldReturn404WhenTryingToDeleteNotExistingTask() {
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/tasks/1?sync=false", HttpMethod.DELETE, null, Void.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldSaveTask() {
        Task task = Task.builder()
                .subject("123")
                .priority(TaskPriority.URGENT)
                .state(TaskState.CREATED)
                .allDayEvent(false)
                .taskAssociations(new ArrayList<>())
                .dateTimeFrom(LocalDateTime.now())
                .dateTimeTo(LocalDateTime.now())
                .creator(1L).build();
        ResponseEntity<Task> response = restTemplate.postForEntity(baseUrl + "/tasks?sync=false", task, Task.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(task.getSubject(), response.getBody().getSubject());
    }

    @Test
    public void shouldSaveTaskWithIntervalAndUnit() {
        LocalDateTime from = LocalDateTime.now();
        Task task = Task.builder()
                .subject("123")
                .priority(TaskPriority.URGENT)
                .state(TaskState.CREATED)
                .allDayEvent(false)
                .dateTimeFrom(from)
                .taskAssociations(new ArrayList<>())
                .dateTimeTo(LocalDateTime.now())
                .creator(1L).build();

        ResponseEntity<Task> response = null;
        long interval = 5L;

        //YEAR
        response = restTemplate.postForEntity(baseUrl + "/tasks?sync=false", task, Task.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        //MONTH
        response = restTemplate.postForEntity(baseUrl + "/tasks?sync=false", task, Task.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        //WEEK
        response = restTemplate.postForEntity(baseUrl + "/tasks?sync=false", task, Task.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        //DAY
        response = restTemplate.postForEntity(baseUrl + "/tasks?sync=false", task, Task.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        //HOUR
        response = restTemplate.postForEntity(baseUrl + "/tasks?sync=false", task, Task.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        //MINUTE
        response = restTemplate.postForEntity(baseUrl + "/tasks?sync=false", task, Task.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        //SECOND
        response = restTemplate.postForEntity(baseUrl + "/tasks?sync=false", task, Task.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void shouldNotSaveTaskWithoutSubject() {
        Task task = Task.builder()
                .owner(123L)
                .priority(TaskPriority.URGENT)
                .state(TaskState.CREATED)
                .creator(1L).build();
        ResponseEntity<Task> response = restTemplate.postForEntity(baseUrl + "/tasks", task, Task.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldNotSaveTaskWithoutPriority() {
        Task task = Task.builder()
                .owner(123L)
                .subject("sub")
                .state(TaskState.CREATED)
                .creator(1L).build();
        ResponseEntity<Task> response = restTemplate.postForEntity(baseUrl + "/tasks", task, Task.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldNotSaveTaskWithoutState() {
        Task task = Task.builder()
                .owner(123L)
                .subject("sub")
                .priority(TaskPriority.URGENT)
                .creator(1L).build();
        ResponseEntity<Task> response = restTemplate.postForEntity(baseUrl + "/tasks", task, Task.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldNotSaveTaskWithoutCreator() {
        Task task = Task.builder()
                .owner(123L)
                .subject("sub")
                .priority(TaskPriority.URGENT)
                .state(TaskState.CREATED).build();
        ResponseEntity<Task> response = restTemplate.postForEntity(baseUrl + "/tasks", task, Task.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void shouldReturnTaskWhenExists() {
        Long taskId = taskRepository.save(Task.builder()
                .subject("1")
                .owner(1L)
                .priority(TaskPriority.URGENT)
                .state(TaskState.CREATED)
                .allDayEvent(false)
                .taskAssociations(new ArrayList<>())
                .dateTimeFrom(LocalDateTime.now())
                .dateTimeTo(LocalDateTime.now())
                .creator(1L).build()).getId();
        ResponseEntity<Task> response = restTemplate.getForEntity(baseUrl + "/tasks/" + taskId, Task.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("1", response.getBody().getSubject());
    }

    @Test
    public void shouldReturn404WhenGettingNotExistingTask() {
        ResponseEntity<Task> response = restTemplate.getForEntity(baseUrl + "/tasks/1", Task.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldModifyTaskWhenExists() {
        Task task = Task.builder()
                .subject("1")
                .owner(1L)
                .priority(TaskPriority.URGENT)
                .state(TaskState.CREATED)
                .allDayEvent(false)
                .dateTimeFrom(LocalDateTime.now())
                .dateTimeTo(LocalDateTime.now())
                .taskAssociations(new ArrayList<>())
                .creator(1L).build();
        Long id = taskRepository.save(task).getId();
        task.setId(id);
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/tasks/" + id + "?sync=false", HttpMethod.PUT, new HttpEntity<>(task), Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }


    @Test
    public void shouldModifyTaskWithIntervalAndUnit() {
        LocalDateTime from = LocalDateTime.now();
        Task task = Task.builder()
                .subject("1")
                .owner(1L)
                .priority(TaskPriority.URGENT)
                .state(TaskState.CREATED)
                .allDayEvent(false)
                .dateTimeFrom(from)
                .taskAssociations(new ArrayList<>())
                .dateTimeTo(LocalDateTime.now())
                .creator(1L).build();
        Long id = taskRepository.save(task).getId();
        task.setId(id);

        Optional<Task> byId = Optional.empty();
        ResponseEntity<Void> response = null;
        long interval = 5L;

        //YEAR
        response = restTemplate.exchange(baseUrl + "/tasks/" + id + "?sync=false", HttpMethod.PUT, new HttpEntity<>(task), Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        byId = taskRepository.findById(id);
        assertTrue(byId.isPresent());

        //MONTH
        response = restTemplate.exchange(baseUrl + "/tasks/" + id + "?sync=false", HttpMethod.PUT, new HttpEntity<>(task), Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        byId = taskRepository.findById(id);
        assertTrue(byId.isPresent());

        //WEEK
        response = restTemplate.exchange(baseUrl + "/tasks/" + id + "?sync=false", HttpMethod.PUT, new HttpEntity<>(task), Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        byId = taskRepository.findById(id);
        assertTrue(byId.isPresent());

        //DAY
        response = restTemplate.exchange(baseUrl + "/tasks/" + id + "?sync=false", HttpMethod.PUT, new HttpEntity<>(task), Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        byId = taskRepository.findById(id);
        assertTrue(byId.isPresent());

        //HOUR
        response = restTemplate.exchange(baseUrl + "/tasks/" + id + "?sync=false", HttpMethod.PUT, new HttpEntity<>(task), Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        byId = taskRepository.findById(id);
        assertTrue(byId.isPresent());

        //MINUTE
        response = restTemplate.exchange(baseUrl + "/tasks/" + id + "?sync=false", HttpMethod.PUT, new HttpEntity<>(task), Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        byId = taskRepository.findById(id);
        assertTrue(byId.isPresent());

        //SECOND
        response = restTemplate.exchange(baseUrl + "/tasks/" + id + "?sync=false", HttpMethod.PUT, new HttpEntity<>(task), Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        byId = taskRepository.findById(id);
        assertTrue(byId.isPresent());
    }

    @Test
    public void shouldReturn404WhenTaskDoesntExist() {
        Task task = Task.builder()
                .subject("1")
                .owner(1L)
                .priority(TaskPriority.URGENT)
                .state(TaskState.CREATED)
                .allDayEvent(false)
                .dateTimeFrom(LocalDateTime.now())
                .dateTimeTo(LocalDateTime.now())
                .creator(1L).build();
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + "/tasks/1?sync=false", HttpMethod.PUT, new HttpEntity<>(task), Void.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }


    @TestConfiguration
    public static class LocalClientConfiguration {
        @Bean
        public ServerList<Server> serverList() {
            return new StaticServerList<>(new Server("localhost", wireMockRule.port()));
        }
    }
}
