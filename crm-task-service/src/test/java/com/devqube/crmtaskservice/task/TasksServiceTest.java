package com.devqube.crmtaskservice.task;

import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmtaskservice.integration.UserClient;
import com.devqube.crmtaskservice.task.model.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TasksServiceTest {

    @Mock
    private TaskRepository taskRepository;
    @Mock
    private UserClient userClient;
    @Mock
    private AssociationClient associationClient;

    @InjectMocks
    private TasksService tasksService;

    @Test
    public void shouldRemoveTaskWhenFound() throws EntityNotFoundException, KafkaSendMessageException {
        when(taskRepository.findById(any())).thenReturn(Optional.of(new Task()));
        tasksService.removeTask(1L, false);
        verify(taskRepository, times(1)).deleteById(any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenTryingToDeleteNotExistingTask() throws EntityNotFoundException, KafkaSendMessageException {
        when(taskRepository.findById(any())).thenReturn(Optional.empty());
        tasksService.removeTask(1L, false);
    }

    @Test
    public void shouldSaveTask() throws KafkaSendMessageException, BadRequestException {
        Task task = new Task();
        task.setId(1L);
        task.setDateTimeFrom(LocalDateTime.now());
        task.setDateTimeTo(LocalDateTime.now());
        task.setTaskAssociations(new ArrayList<>());
        when(taskRepository.save(any())).thenReturn(task);
        tasksService.save(task, false);
        verify(taskRepository, times(1)).save(any());
    }

    @Test
    public void shouldGetTaskByCreator() {
        Task task = Task.builder().id(1L).build();
        List<Task> list = new ArrayList<>();
        list.add(task);
        when(taskRepository.findAllByCreatorAndOwnerNot(eq(1L), eq(1L))).thenReturn(list);
        List<Task> allTasksByCreator = this.tasksService.getAllTasksByCreator(1L);
        assertNotNull(allTasksByCreator);
        assertEquals(1, allTasksByCreator.size());
        assertEquals(1L, allTasksByCreator.get(0).getId().longValue());
    }

    @Test
    public void shouldReturnAllEventsByRole() throws EntityNotFoundException {
        String roleName = "roleName";
        Task task = Task.builder().id(1L).build();
        List<Task> list = new ArrayList<>();
        list.add(task);
        Set<Long> accountIds  = new HashSet<>();
        accountIds.add(10L);

        when(userClient.getAccountIdsByRole(eq(roleName))).thenReturn(accountIds);
        when(taskRepository.findAllByOwnerIn(eq(accountIds))).thenReturn(list);

        List<Task> allTasksByCreator = this.tasksService.getAllTasksByRole(roleName);

        assertNotNull(allTasksByCreator);
        assertEquals(1, allTasksByCreator.size());
        assertEquals(1L, allTasksByCreator.get(0).getId().longValue());
    }

    @Test
    public void shouldReturnTaskWhenExists() throws EntityNotFoundException {
        when(taskRepository.findById(any())).thenReturn(Optional.of(new Task()));
        assertNotNull(tasksService.getTask(1L));
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenGettingNotExistingTask() throws EntityNotFoundException {
        when(taskRepository.findById(any())).thenReturn(Optional.empty());
        assertNotNull(tasksService.getTask(1L));
    }

    @Test
    public void shouldModifyTaskWhenExists() throws EntityNotFoundException, KafkaSendMessageException, BadRequestException {
        Task task = new Task();
        task.setDateTimeFrom(LocalDateTime.now());
        task.setDateTimeTo(LocalDateTime.now());
        when(taskRepository.findById(any())).thenReturn(Optional.of(task));
        tasksService.modifyTask(1L, false, task);
        verify(taskRepository, times(1)).save(any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionWhenTaskDoesntExist() throws EntityNotFoundException, KafkaSendMessageException, BadRequestException {
        when(taskRepository.findById(any())).thenReturn(Optional.empty());
        tasksService.modifyTask(1L, false, new Task());
    }
}
