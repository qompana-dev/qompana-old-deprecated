package com.devqube.crmtaskservice.task.web;

import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.task.ActivityTypeEnum;
import com.devqube.crmshared.task.TaskSummaryDto;
import com.devqube.crmtaskservice.ApiUtil;
import com.devqube.crmtaskservice.task.dto.*;
import com.devqube.crmtaskservice.task.model.Task;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;


@Validated
@Api(value = "tasks")
public interface TasksApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Create task", nickname = "createTask", notes = "Method used to create task", response = Task.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "added task successfully", response = Task.class),
            @ApiResponse(code = 400, message = "validation error", response = String.class)})
    @RequestMapping(value = "/tasks",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Task> createTask(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "sync", required = true) Boolean sync, @ApiParam(value = "Created task object", required = true) @Valid @RequestBody TaskWithParticipantsDto task) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"owner\" : 1,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\",  \"priority\" : \"LOW\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Check task collision", nickname = "checkForTaskCollisions", notes = "Method used to check collision between tasks", response = TaskDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "collision checked", response = TaskDto.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "validation error", response = String.class)})
    @RequestMapping(value = "/mobile/tasks/collisions",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<List<TaskDto>> checkForTaskCollisions(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "sync", required = true) Boolean sync, @ApiParam(value = "Checked task object", required = true) @Valid @RequestBody Task task) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"owner\" : 1,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\",  \"priority\" : \"LOW\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Create task", nickname = "createTask", notes = "Method used to create task", response = Task.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "added task successfully", response = Task.class),
            @ApiResponse(code = 400, message = "validation error", response = String.class)})
    @RequestMapping(value = "/internal/tasks",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Task> createTaskInternal(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "sync", required = true) Boolean sync, @ApiParam(value = "Created task object", required = true) @Valid @RequestBody Task task) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"owner\" : 1,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\",  \"priority\" : \"LOW\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get all tasks by owner", nickname = "getAllTasksByOwner", notes = "Method used to get all tasks for owner account", response = Task.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "allTasks", response = Task.class, responseContainer = "List")})
    @RequestMapping(value = "/tasks",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<Task>> getAllTasksByOwner(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "owner", required = true) Long owner,
                                                          @ApiParam(value = "", required = false) @Valid @RequestParam(value = "timeCategory", required = false) TaskWidgetTimeCategoryEnum timeCategory,
                                                          @ApiParam(value = "", required = false) @Valid @RequestParam(value = "date", required = false) String date) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"owner\" : 1,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\",  \"priority\" : \"LOW\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get all tasks associated with crm object", nickname = "getAllTasksByCrmObject",
            notes = "Method used to get all tasks associated with crm object", response = Task.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "allTasks", response = Task.class, responseContainer = "List")})
    @RequestMapping(value = "/tasks/type/{type}/id/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<Task>> getAllTasksByCrmObject(
            @ApiParam(value = "The association destination ID", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The association destination type", required = true, defaultValue = "null")
            @PathVariable("type") com.devqube.crmshared.search.CrmObjectType type) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"owner\" : 1,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\",  \"priority\" : \"LOW\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get all tasks by owner for mobile application", nickname = "getAllTasksByOwnerForMobile", notes = "Method used to get all tasks for owner account in mobile application", response = TaskDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "allTasks", response = TaskDto.class, responseContainer = "List")})
    @RequestMapping(value = "/mobile/tasks",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<TaskDto>> getAllTasksByOwnerForMobile(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "owner", required = true) Long owner) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"owner\" : 1,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\",  \"priority\" : \"LOW\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get all tasks by owner", nickname = "getAllTasksByOwner", notes = "Method used to get all tasks for owner account", response = Task.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "allTasks", response = Task.class, responseContainer = "List")})
    @RequestMapping(value = "/internal/tasks",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<Task>> getAllTasksByOwnerInternal(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "owner", required = true) Long owner) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"owner\" : 1,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\",  \"priority\" : \"LOW\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get upcoming tasks by owner", nickname = "getUpcomingTasksByOwner", notes = "Method used to get upcoming tasks for owner account", response = Task.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "upcomingTasks", response = Task.class, responseContainer = "List")})
    @RequestMapping(value = "/tasks/upcoming",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<Task>> getUpcomingTasksByOwner(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "owner", required = true) Long owner) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"owner\" : 1,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\",  \"priority\" : \"LOW\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get task by id", nickname = "getTask", notes = "Method used to get task", response = TaskWithParticipantsDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task", response = TaskWithParticipantsDto.class),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/tasks/{taskId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<TaskWithParticipantsDto> getTask(@ApiParam(value = "The task ID", required = true) @PathVariable("taskId") Long taskId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"owner\" : 1,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\",  \"priority\" : \"LOW\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get task association details by id", nickname = "getTaskAssociationsDetails", notes = "Method used to get task association details", response = TaskAssociationDetailDtoOut.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task", response = TaskAssociationDetailDtoOut.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/tasks/{taskId}/associations-details",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<TaskAssociationDetailDtoOut>> getTaskAssociationsDetails(@ApiParam(value = "The task ID", required = true) @PathVariable("taskId") Long taskId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"owner\" : 1,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\",  \"priority\" : \"LOW\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get task by id", nickname = "getTask", notes = "Method used to get task", response = Task.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task", response = Task.class),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/internal/tasks/{taskId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Task> getTaskInternal(@ApiParam(value = "The task ID", required = true) @PathVariable("taskId") Long taskId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"owner\" : 1,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\",  \"priority\" : \"LOW\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get task for expenses by id", nickname = "getTaskForExpenses", notes = "Method used to get task for expenses", response = Task.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task", response = Task.class),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/tasks/for-expenses/{taskId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Task> getTaskForExpenses(@ApiParam(value = "The task ID", required = true) @PathVariable("taskId") Long taskId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"owner\" : 1,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\",  \"priority\" : \"LOW\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Delete task", nickname = "removeTask", notes = "Method used to remove task")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task removed"),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/tasks/{taskId}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> removeTask(@ApiParam(value = "The task ID", required = true) @PathVariable("taskId") Long taskId, @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "sync", required = true) Boolean sync) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Delete task", nickname = "removeTask", notes = "Method used to remove task")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task removed"),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/internal/tasks/{taskId}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> removeTaskInternal(@ApiParam(value = "The task ID", required = true) @PathVariable("taskId") Long taskId, @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "sync", required = true) Boolean sync) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update task", nickname = "updateTask", notes = "Method used to modify task")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task update"),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/tasks/{taskId}",
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> updateTask(@ApiParam(value = "The task ID", required = true) @PathVariable("taskId") Long taskId, @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "sync", required = true) Boolean sync, @ApiParam(value = "Modified task object", required = true) @Valid @RequestBody TaskWithParticipantsDto task) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update activity state", nickname = "updateActivityStatus", notes = "Method used to modify activity state")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task updated"),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/tasks/{taskId}/status",
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> updateActivityStatus(@ApiParam(value = "The task ID", required = true) @PathVariable("taskId") Long taskId,
                                                      @ApiParam(value = "Modified task object", required = true) @Valid @RequestBody ActivityStatusUpdateDto activityStatusUpdateDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update task date", nickname = "updateTaskDate", notes = "Method used to modify task date")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task updated"),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/tasks/{taskId}/date",
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> updateTaskDate(@ApiParam(value = "The task ID", required = true) @PathVariable("taskId") Long taskId,
                                                @ApiParam(value = "Modified task object", required = true) @Valid @RequestBody TaskDateChangeDto taskDateChangeDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Update task", nickname = "updateTask", notes = "Method used to modify task")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task update"),
            @ApiResponse(code = 400, message = "validation error"),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/internal/tasks/{taskId}",
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    default ResponseEntity<Void> updateTaskInternal(@ApiParam(value = "The task ID", required = true) @PathVariable("taskId") Long taskId, @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "sync", required = true) Boolean sync, @ApiParam(value = "Modified task object", required = true) @Valid @RequestBody Task task) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get all tasks by creator", nickname = "getAllTasksByCreator", notes = "Method used to get all tasks for creator account", response = Task.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "allTasks", response = Task.class, responseContainer = "List")})
    @RequestMapping(value = "/tasks/creator/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<Task>> getAllTasksByCreator(@ApiParam(value = "The creator ID", required = true) @PathVariable("id") Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"owner\" : 1,  \"emailNotification\" : true,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"description\" : \"description\",  \"priority\" : \"LOW\",  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"unit\" : \"YEAR\",  \"interval\" : 5,  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get all tasks by roleName", nickname = "getAllTasksByRole", notes = "Method used to get all tasks for accounts assigned to child roles", response = Task.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "allTasks", response = Task.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "role not found")})
    @RequestMapping(value = "/tasks/role/{roleName}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<Task>> getAllTasksByRole(@ApiParam(value = "The role name", required = true) @PathVariable("roleName") String roleName) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"owner\" : 1,  \"emailNotification\" : true,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"description\" : \"description\",  \"priority\" : \"LOW\",  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"unit\" : \"YEAR\",  \"interval\" : 5,  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get task list by ids", nickname = "getTasksAsCrmObject", notes = "Method used to get tasks as crm objects by ids", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found tasks", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/tasks/crm-object",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmshared.search.CrmObject>> getTasksAsCrmObject(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get tasks list for search", nickname = "getTasksForSearch", notes = "Method used to get task list", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all task names", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/tasks/search",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmshared.search.CrmObject>> getTasksForSearch(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get activity list by ids", nickname = "getActivitiesAsCrmObject", notes = "Method used to get activities as crm objects by ids", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "found activities", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/tasks/activity/crm-object",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmshared.search.CrmObject>> getActivitiesAsCrmObject(@ApiParam(value = "", defaultValue = "new ArrayList<>()") @Valid @RequestParam(value = "ids", required = false, defaultValue = "new ArrayList<>()") List<Long> ids) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Get activity list for search", nickname = "getActivitiesForSearch", notes = "Method used to get activity list", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "all activity names", response = com.devqube.crmshared.search.CrmObject.class, responseContainer = "List")})
    @RequestMapping(value = "/tasks/activity/search",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<com.devqube.crmshared.search.CrmObject>> getActivitiesForSearch(@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "term", required = true) String term) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get preview task by association type and id", nickname = "getPreviewActivitiesAssociatedWithObject", notes = "Method used to get preview task associated with object", response = TaskDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task", response = TaskDto.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/tasks/activity/associated-with/type/{type}/id/{id}/preview",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<TaskDto>> getPreviewActivitiesAssociatedWithObject(
            @ApiParam(value = "The association destination ID", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The association destination type", required = true, defaultValue = "null")
            @PathVariable("type") com.devqube.crmshared.search.CrmObjectType type) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"owner\" : 1,  \"emailNotification\" : true,  \"creator\" : 6,  \"subject\" : \"subject\",  \"timeZone\" : \"timeZone\",  \"description\" : \"description\",  \"priority\" : \"LOW\",  \"dateTimeFrom\" : \"2000-01-23T04:56:07.000+00:00\",  \"unit\" : \"YEAR\",  \"interval\" : 5,  \"id\" : 0,  \"state\" : \"CREATED\",  \"place\" : \"place\",  \"dateTimeTo\" : \"2000-01-23T04:56:07.000+00:00\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get task by association type and id", nickname = "getActivitiesAssociatedWithObject", notes = "Method used to get task associated with object", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task", response = org.springframework.data.domain.Page.class),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/tasks/activity/associated-with/type/{type}/id/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<org.springframework.data.domain.Page> getActivitiesAssociatedWithObject(
            @ApiParam(value = "The association destination ID", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The association destination type", required = true, defaultValue = "null")
            @PathVariable("type") com.devqube.crmshared.search.CrmObjectType type, @ApiParam(value = "", defaultValue = "null")
            @Valid org.springframework.data.domain.Pageable pageable) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get task by task type and association type and id", nickname = "getAllActivitiesByTypeAssociatedWithObject", notes = "Method used to get task by task type and association type and id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task", response = List.class),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/tasks/activity/type/{activityType}/associated-with/type/{type}/id/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<TaskDto>> getAllActivitiesByTypeAssociatedWithObject(
            @ApiParam(value = "The association destination ID", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The association destination type", required = true, defaultValue = "null") @PathVariable("type") CrmObjectType type,
            @ApiParam(value = "The activity type", required = true) @PathVariable("activityType") ActivityTypeEnum activityType) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get task by association type and id and timeCategory", nickname = "getTimedActivitiesAssociatedWithObject",
            notes = "Method used to get task associated with object", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task", response = org.springframework.data.domain.Page.class),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/tasks/activity/associated-with/type/{type}/id/{id}/timeCategory/{timeCategory}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<org.springframework.data.domain.Page> getTimedActivitiesAssociatedWithObject(
            @ApiParam(value = "The association destination ID", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The association destination type", required = true, defaultValue = "null")
            @PathVariable("type") com.devqube.crmshared.search.CrmObjectType type,
            @ApiParam(value = "Time Category of tasks", required = true, defaultValue = "null")
            @PathVariable("timeCategory") TaskTimeCategoryEnum timeCategory,
            @ApiParam(value = "", defaultValue = "null")
            @Valid org.springframework.data.domain.Pageable pageable) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "get activities by association type and id", nickname = "getActivities", notes = "Method used to get activities", response = org.springframework.data.domain.Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task", response = org.springframework.data.domain.Page.class),
            @ApiResponse(code = 404, message = "task not found")})
    @RequestMapping(value = "/mobile/tasks/activity",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<org.springframework.data.domain.Page> getActivities(@ApiParam(value = "", defaultValue = "null") @RequestHeader("Logged-Account-Email") String email,
                                                                               @ApiParam(value = "", defaultValue = "null") @Valid org.springframework.data.domain.Pageable pageable,
                                                                               @ApiParam(value = "only planned tasks") @Valid @RequestParam(value = "planned", required = false) Boolean planned,
                                                                               @ApiParam(value = "only outstanding tasks") @Valid @RequestParam(value = "outstanding", required = false) Boolean outstanding,
                                                                               @ApiParam(value = "only inProgress tasks") @Valid @RequestParam(value = "inProgress", required = false) Boolean inProgress,
                                                                               @ApiParam(value = "only suspended tasks") @Valid @RequestParam(value = "suspended", required = false) Boolean suspended,
                                                                               @ApiParam(value = "only finished tasks") @Valid @RequestParam(value = "finished", required = false) Boolean finished,
                                                                               @ApiParam(value = "only associated with object id") @Valid @RequestParam(value = "associationId", required = false) Long associationId,
                                                                               @ApiParam(value = "only associated with object type") @Valid @RequestParam(value = "associationType", required = false) CrmObjectType associationType) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get outstanding activities", nickname = "getOutstandingActivitiesCount", notes = "Method used to get outstanding activities count", response = Long.class)
    @ApiResponses(value = @ApiResponse(code = 200, message = "outstanding activity count", response = Long.class))
    @RequestMapping(value = "/tasks/activity/status/outstanding/count",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Long> getOutstandingActivitiesCount(@ApiParam(value = "", defaultValue = "null") @RequestHeader("Logged-Account-Email") String email,
                                                               @ApiParam(value = "only associated with object id") @Valid @RequestParam(value = "associationId", required = false) Long associationId,
                                                               @ApiParam(value = "only associated with object type") @Valid @RequestParam(value = "associationType", required = false) CrmObjectType associationType) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "get activity count for goal", nickname = "getActivityCountForGoal", notes = "Method used to get activity count for user id", response = Long.class)
    @ApiResponses(value = @ApiResponse(code = 200, message = "activity count", response = Long.class))
    @RequestMapping(value = "/internal/tasks/activity/user/{userIds}/count",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Long> getActivityCountForGoal(@ApiParam(value = "The user id", required = true) @PathVariable(value = "userIds", required = true) Set<Long> userIds,
                                                         @ApiParam(value = "The only phone activities", required = true) @RequestParam(value = "onlyPhone", required = true) Boolean onlyPhone,
                                                         @ApiParam(value = "From date", required = true) @RequestParam(value = "from", required = true) String from,
                                                         @ApiParam(value = "To date", required = true) @RequestParam(value = "to", required = true) String to
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get activity types", nickname = "getActivityTypes",
            notes = "Method used to get activity types by task ids", response = Map.class)
    @ApiResponses(value = @ApiResponse(code = 200, message = "activity types was successfully fetched", response = Map.class))
    @RequestMapping(value = "/tasks/activity/types/associated-with/type/{type}/id/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Map<Long, ActivityTypeEnum>> getActivityTypes(
            @ApiParam(value = "The association destination ID", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The association destination type", required = true, defaultValue = "null")
            @PathVariable("type") com.devqube.crmshared.search.CrmObjectType type) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "get all task summary", nickname = "getAllTaskSummary", notes = "Method used to get all task summary from list of destination ids", response = Map.class)
    @ApiResponses(value = @ApiResponse(code = 200, message = "task summary", response = Map.class))
    @RequestMapping(value = "/internal/tasks/activity/associated-with/type",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<Map<Long, TaskSummaryDto>> getAllTaskSummary(@ApiParam(value = "The association destination type", required = true) @RequestParam(value = "type", required = true) CrmObjectType type,
                                                                        @ApiParam(value = "The ids list of task source", required = true) @RequestParam(value = "sourceIds", required = true) List<Long> sourceIds,
                                                                        @ApiParam(value = "The list of time category", required = true) @RequestParam(value = "timeCategory") List<TaskTimeCategoryEnum> timeCategory) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf(""))) {
                    ApiUtil.setExampleResponse(request, "", "");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}