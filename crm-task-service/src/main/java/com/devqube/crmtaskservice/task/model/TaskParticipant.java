package com.devqube.crmtaskservice.task.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskParticipant {

    @Id
    @Access(AccessType.PROPERTY)
    @SequenceGenerator(name = "task_participant_seq", sequenceName = "task_participant_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "task_participant_seq")
    private Long id;

    @NotNull
    private Long entityId;

    private Long entityOwnerId;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TaskParticipantType type;

    @NotEmpty
    private String name;

    private String email;

    @JsonIgnore
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "task_id", nullable = false)
    private Task task;

}
