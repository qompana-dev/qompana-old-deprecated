package com.devqube.crmtaskservice.task.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TaskState {
    CREATED("CREATED"),

    IN_PROGRESS("IN_PROGRESS"),

    SUSPENDED("SUSPENDED"),

    FINISHED("FINISHED");

    private String value;

    TaskState(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static TaskState fromValue(String text) {
        for (TaskState b : TaskState.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + text + "'");
    }
}
