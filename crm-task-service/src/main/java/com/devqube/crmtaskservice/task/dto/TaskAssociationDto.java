package com.devqube.crmtaskservice.task.dto;

import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmtaskservice.task.model.Task;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskAssociationDto {
    @NotNull
    private Long objectId;

    @NotNull
    private CrmObjectType objectType;

    public AssociationDetailDto toAssociationDetailDto(Task task) {
        CrmObjectType type = (task.getActivityType() == null) ? CrmObjectType.task : CrmObjectType.activity;
        return new AssociationDetailDto(type, task.getId(), this.objectType, this.objectId);
    }

    public static TaskAssociationDto fromAssociationDto(AssociationDto associationDto) {
        return new TaskAssociationDto(associationDto.getDestinationId(), associationDto.getDestinationType());
    }
}
