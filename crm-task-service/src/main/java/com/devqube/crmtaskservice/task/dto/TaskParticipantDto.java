package com.devqube.crmtaskservice.task.dto;

import com.devqube.crmtaskservice.task.model.TaskParticipantType;
import lombok.Data;

@Data
public class TaskParticipantDto {

    private Long id;
    private TaskParticipantType type;
    private Long ownerAccountId;
    private String name;
    private String email;

}
