package com.devqube.crmtaskservice.task.model;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public enum TaskParticipantType {
    ACCOUNT, CUSTOMER, CONTACT
}
