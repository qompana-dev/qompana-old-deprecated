package com.devqube.crmtaskservice.task;

import com.devqube.crmshared.task.ActivityTypeEnum;
import com.devqube.crmtaskservice.task.model.Task;
import com.devqube.crmtaskservice.task.model.TaskState;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.JoinType;
import java.time.LocalDateTime;
import java.util.List;

public class TaskSpecifications {

    public static Specification<Task> findById(Long id) {
        return ((root, query, cb) -> cb.equal(root.get("id"), id));

    }

    public static Specification<Task> findByIdIn(List<Long> idList) {
        return !idList.isEmpty() ? (root, query, cb) -> root.get("id").in(idList) :
                (root, query, cb) -> cb.isNull(root.get("id"));
    }

    public static Specification<Task> findByOwnerId(Long ownerId) {
        return (root, query, cb) -> {
            query.distinct(true);
            return cb.or(
                    cb.equal(root.joinSet("participants", JoinType.LEFT).get("entityOwnerId"), ownerId),
                    cb.equal(root.get("owner"), ownerId)
            );
        };
    }

    public static Specification<Task> findByDateTimeFromBetween(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        return ((root, query, cb) -> cb.between(root.get("dateTimeFrom"), startDateTime, endDateTime));

    }

    public static Specification<Task> findByDateTimeFromBeforeExclusive(LocalDateTime dateTime) {
        return ((root, query, cb) -> cb.lessThan(root.get("dateTimeFrom"), dateTime));
    }

    public static Specification<Task> findByDateTimeFromAfterExclusive(LocalDateTime dateTime) {
        return ((root, query, cb) -> cb.greaterThan(root.get("dateTimeFrom"), dateTime));
    }

    public static Specification<Task> findByState(TaskState state) {
        return ((root, query, cb) -> cb.equal(root.get("state"), state));
    }

    public static Specification<Task> findByActivityType(ActivityTypeEnum activityType) {
        return ((root, query, cb) -> cb.equal(root.get("activityType"), activityType));
    }

    public static Specification<Task> findByNotified(boolean isNotified) {
        return ((root, query, cb) -> cb.equal(root.get("notified"), isNotified));
    }

}
