package com.devqube.crmtaskservice.task.dto;

public enum TaskTimeCategoryEnum {
    ALL, PLANNED, LATEST, OLD, COMPLETED
}
