package com.devqube.crmtaskservice.task.dto;

public enum TaskWidgetTimeCategoryEnum {
    ALL, OLD, PLANNED, FINISHED
}
