package com.devqube.crmtaskservice.task.dto;

import com.devqube.crmshared.task.ActivityTypeEnum;
import com.devqube.crmtaskservice.task.model.TaskPriority;
import com.devqube.crmtaskservice.task.model.TaskState;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Accessors(chain = true)
public class TaskWithParticipantsDto {

    private Long id;

    @NotNull
    private String subject;
    private TaskPriority priority;
    private TaskState state;

    @NotNull
    private Long creator;
    private String place;
    private Double longitude;
    private Double latitude;
    private ActivityTypeEnum activityType;

    @NotNull
    private LocalDateTime dateTimeFrom;

    @NotNull
    private LocalDateTime dateTimeTo;

    @NotNull
    private Boolean allDayEvent;

    private Long owner;
    private Set<TaskParticipantDto> participants = new HashSet<>();

    private boolean notified;
    private boolean notificationRequired;
    private LocalDateTime notificationDateTime;

    private String description;
    private String timeZone;
    private List<TaskAssociationDto> taskAssociations;

}
