package com.devqube.crmtaskservice.task.scheduler;

import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmshared.kafka.dto.KafkaMessageData;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.kafka.util.KafkaMessageDataBuilder;
import com.devqube.crmshared.notification.NotificationMsgType;
import com.devqube.crmtaskservice.task.TaskRepository;
import com.devqube.crmtaskservice.task.model.Task;
import lombok.RequiredArgsConstructor;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;

import static com.devqube.crmtaskservice.task.TaskSpecifications.findById;
import static com.devqube.crmtaskservice.task.TaskSpecifications.findByNotified;

@Component
@RequiredArgsConstructor
public class TaskNotificationJob extends QuartzJobBean {

    private final KafkaService kafkaService;
    private final TaskRepository taskRepository;
    private static final Logger log = LoggerFactory.getLogger(TaskNotificationJob.class);

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        sendNotification(jobDataMap);
    }

    private void sendNotification(JobDataMap jobDataMap) {
        Long taskId = jobDataMap.getLong("id");
        Long receiverAccountId = jobDataMap.getLong("receiverAccountId");
        Set<KafkaMessageData> kafkaMessageData = createKafkaMessageDataSetFromJobData(jobDataMap);

        sendSystemNotification(receiverAccountId, kafkaMessageData);
        sendEmailNotification(receiverAccountId, kafkaMessageData);

        Optional<Task> optTask = taskRepository.findOne(findById(taskId).and(findByNotified(false)));
        if (optTask.isPresent()) {
            Task task = optTask.get();
            task.setNotified(true);
            taskRepository.save(task);
        }

    }

    private void sendSystemNotification(Long receiverAccountId, Set<KafkaMessageData> kafkaMessageData) {
        try {
            kafkaMessageData.add(new KafkaMessageData("notificationMsgType", NotificationMsgType.EVENT_NOTIFICATION.name()));
            kafkaService.send(receiverAccountId, KafkaMsgType.NOTIFICATION_ADD_NOTIFICATION, kafkaMessageData);
        } catch (KafkaSendMessageException e) {
            log.error(String.format("Error sending task system notification to user with account id = %s", receiverAccountId), e.getMessage());
        }
    }

    private void sendEmailNotification(Long receiverAccountId, Set<KafkaMessageData> kafkaMessageData) {
        try {
            kafkaService.send(receiverAccountId, KafkaMsgType.NOTIFICATION_SEND_EVENT_NOTIFICATION_MAIL, kafkaMessageData);
        } catch (KafkaSendMessageException e) {
            log.error(String.format("Error sending task email notification to user with account id = %s", receiverAccountId), e.getMessage());
        }
    }

    private Set<KafkaMessageData> createKafkaMessageDataSetFromJobData(JobDataMap jobDataMap) {
        return KafkaMessageDataBuilder.builder()
                .add("id", jobDataMap.getString("userId"))
                .add("subject", jobDataMap.getString("subject"))
                .add("place", jobDataMap.getString("place"))
                .add("dateTimeFrom", jobDataMap.getString("dateTimeFrom"))
                .add("dateTimeTo", jobDataMap.getString("dateTimeTo"))
                .add("notificationDateTime", jobDataMap.getString("notificationDateTime"))
                .add("allDayEvent", jobDataMap.getString("allDayEvent"))
                .add("owner", jobDataMap.getString("owner"))
                .build();
    }

}
