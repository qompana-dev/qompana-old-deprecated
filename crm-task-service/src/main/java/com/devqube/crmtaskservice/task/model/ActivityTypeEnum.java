package com.devqube.crmtaskservice.task.model;

public enum ActivityTypeEnum {
    PHONE, MEETING, MAIL, TASK
}
