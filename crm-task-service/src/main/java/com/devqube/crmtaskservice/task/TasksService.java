package com.devqube.crmtaskservice.task;

import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmshared.kafka.dto.KafkaMessageData;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.kafka.util.KafkaMessageDataBuilder;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.task.ActivityTypeEnum;
import com.devqube.crmshared.task.TaskSummaryDto;
import com.devqube.crmtaskservice.integration.BusinessClient;
import com.devqube.crmtaskservice.integration.UserClient;
import com.devqube.crmtaskservice.task.dto.*;
import com.devqube.crmtaskservice.task.model.Task;
import com.devqube.crmtaskservice.task.model.TaskParticipant;
import com.devqube.crmtaskservice.task.model.TaskState;
import com.devqube.crmtaskservice.task.scheduler.TaskNotificationSchedulerService;
import com.devqube.crmtaskservice.task.web.TaskDtoMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.devqube.crmtaskservice.task.TaskSpecifications.*;

@Slf4j
@Service
@EnableScheduling
@RequiredArgsConstructor
public class TasksService {

    @Value("${task.old.afterDays:7}")
    private Integer taskOldAfterDays;

    private final TaskRepository taskRepository;
    private final TaskParticipantRepository taskParticipantRepository;
    private final KafkaService kafkaService;
    private final UserClient userClient;
    private final BusinessClient businessClient;
    private final AssociationClient associationClient;
    private final ModelMapper modelMapper;
    private final TaskNotificationSchedulerService taskNotificationSchedulerService;

    public Task save(Task task, Boolean sync) throws KafkaSendMessageException, BadRequestException {
        validateDatesAndThrowExceptionIfInvalid(task);
        task.setNotified(false);
        Task savedTask = taskRepository.save(task);
        task.setId(savedTask.getId());
        addAssociations(task);

        if (savedTask.isNotificationRequired()) {
            schedulerTaskNotifications(task);
        }

        if (sync) {
            kafkaService.send(savedTask.getOwner(), KafkaMsgType.CALENDAR_NEW_EVENT, createKafkaMessageDataSetFromTask(task));
        }

        return savedTask;
    }

    public List<Task> getAllTasksByOwner(Long ownerId) {
        return taskRepository.findAll(findByOwnerId(ownerId));
    }

    public List<Task> getAllTasksByOwner(Long ownerId, TaskWidgetTimeCategoryEnum timeCategory, String stringDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate date = LocalDate.parse(stringDate, formatter);

        LocalDateTime startOfDay = date.atStartOfDay();
        LocalDateTime endOfDay = date.atTime(LocalTime.MAX);

        Specification<Task> spec = findByDateTimeFromBetween(startOfDay, endOfDay)
                .and(findByOwnerId(ownerId));

        switch (timeCategory) {
            case OLD: {
                spec = spec.and(findByDateTimeFromBeforeExclusive(LocalDateTime.of(date, LocalTime.now())));
                break;
            }
            case PLANNED: {
                spec = spec.and(findByDateTimeFromAfterExclusive(LocalDateTime.of(date, LocalTime.now())));
                break;
            }
            case FINISHED: {
                spec = spec.and(findByState(TaskState.FINISHED));
                break;
            }
        }

        return taskRepository.findAll(spec, new Sort(Sort.Direction.DESC, "dateTimeFrom"));
    }

    public List<Task> getAllTasksAssociatedWithObject(Long id, CrmObjectType type) {
        List<Long> taskIds = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(type, CrmObjectType.activity, id)
                .stream().map(AssociationDto::getDestinationId).collect(Collectors.toList());
        taskIds.addAll(associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(type, CrmObjectType.task, id)
                .stream().map(AssociationDto::getDestinationId).collect(Collectors.toList()));
        Specification<Task> spec = findByIdIn(taskIds);
        return taskRepository.findAll(spec);
    }

    @Transactional
    public List<TaskDto> getAllTasksByOwnerWithAssociations(Long ownerId) {
        List<Task> tasks = taskRepository.findAll(findByOwnerId(ownerId));
        tasks.forEach(task -> {
            task.setTaskAssociations(getTaskAssociationsDto(task));
        });
        return tasks.stream().map(this::getDtoFromModel).collect(Collectors.toList());
    }

    public List<Task> getUpcomingTasksByOwner(Long owner) {
        return this.taskRepository.findAllByOwnerAndDateTimeFromIsAfterOrderByDateTimeFrom(owner, LocalDateTime.now());
    }

    public List<Task> getAllTasksByCreator(Long creator) {
        return this.taskRepository.findAllByCreatorAndOwnerNot(creator, creator);
    }

    public List<Task> getAllTasksByRole(String roleName) throws EntityNotFoundException {
        Set<Long> accountIdsByRole;
        try {
            accountIdsByRole = userClient.getAccountIdsByRole(roleName);
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new EntityNotFoundException();
        }

        return this.taskRepository.findAllByOwnerIn(accountIdsByRole);
    }

    public Task getTask(Long taskId) throws EntityNotFoundException {
        Task task = this.taskRepository.findById(taskId)
                .orElseThrow(() -> new EntityNotFoundException("Task with id " + taskId + " not found."));
        task.setTaskAssociations(getTaskAssociationsDto(task));
        return task;
    }

    public List<TaskAssociationDetailDtoOut> getTaskAssociationsDetails(Long taskId) throws EntityNotFoundException {
        Task task = this.taskRepository.findById(taskId)
                .orElseThrow(() -> new EntityNotFoundException("Task with id " + taskId + " not found."));
        task.setTaskAssociations(getTaskAssociationsDto(task));

        List<TaskAssociationDetailDtoOut> associationDetailsList = new ArrayList<>();

        List<Long> leadIdList = getAssociationIdList(task, CrmObjectType.lead);
        List<Long> opportunityIdList = getAssociationIdList(task, CrmObjectType.opportunity);
        List<Long> contactIdList = getAssociationIdList(task, CrmObjectType.contact);
        List<Long> customerIdList = getAssociationIdList(task, CrmObjectType.customer);

        if (leadIdList.size() > 0) {
            List<TaskAssociationDetailDtoOut> leadDetailsList = businessClient.getLeadsAsCustomCrmObject(leadIdList).stream()
                    .map(TaskDtoMapper::toTaskAssociationDetailDtoOut).collect(Collectors.toList());
            associationDetailsList.addAll(leadDetailsList);
        }
        if (opportunityIdList.size() > 0) {
            List<TaskAssociationDetailDtoOut> opportunityDetailsList = businessClient.getOpportunitiesAsCustomCrmObject(opportunityIdList).stream()
                    .map(TaskDtoMapper::toTaskAssociationDetailDtoOut).collect(Collectors.toList());
            associationDetailsList.addAll(opportunityDetailsList);
        }
        if (contactIdList.size() > 0) {
            List<TaskAssociationDetailDtoOut> contactDetailsList = businessClient.getContactsAsCrmObject(contactIdList).stream()
                    .map(TaskDtoMapper::toTaskAssociationDetailDtoOut).collect(Collectors.toList());
            associationDetailsList.addAll(contactDetailsList);
        }
        if (customerIdList.size() > 0) {
            List<TaskAssociationDetailDtoOut> customerDetailsList = businessClient.getCustomersAsCrmObject(customerIdList).stream()
                    .map(TaskDtoMapper::toTaskAssociationDetailDtoOut).collect(Collectors.toList());
            associationDetailsList.addAll(customerDetailsList);
        }

        return associationDetailsList;
    }

    public void removeTask(Long taskId, Boolean sync) throws EntityNotFoundException, KafkaSendMessageException {
        Optional<Task> task = this.taskRepository.findById(taskId);
        if (task.isEmpty()) {
            throw new EntityNotFoundException("Task with id " + taskId + " not found.");
        }
        Long accountId = task.get().getOwner();
        taskRepository.deleteById(taskId);
        taskNotificationSchedulerService.cancelNotificationJob(taskId);
        if (sync) {
            kafkaService.send(accountId, KafkaMsgType.CALENDAR_DELETE_EVENT, KafkaMessageDataBuilder.builder()
                    .add("taskId", String.valueOf(taskId))
                    .add("accountId", String.valueOf(task.get().getOwner()))
                    .build());
        }
    }

    public void updateActivityStatus(Long taskId, ActivityStatusUpdateDto activityStatusUpdateDto) throws EntityNotFoundException, KafkaSendMessageException, BadRequestException {
        Task fromDb = this.taskRepository.findById(taskId).orElseThrow(EntityNotFoundException::new);
        fromDb.setState(activityStatusUpdateDto.getTaskState());
        modifyTaskWithoutCheckIfExist(taskId, true, fromDb);
    }

    public void updateTaskDate(Long taskId, TaskDateChangeDto taskDateChangeDto) throws EntityNotFoundException, KafkaSendMessageException, BadRequestException {
        Task fromDb = this.taskRepository.findById(taskId).orElseThrow(EntityNotFoundException::new);
        fromDb.setDateTimeFrom(taskDateChangeDto.getDateTimeFrom());
        fromDb.setDateTimeTo(taskDateChangeDto.getDateTimeTo());
        modifyTaskWithoutCheckIfExist(taskId, true, fromDb);
    }

    @Transactional
    public void modifyTask(Long taskId, Boolean sync, Task task) throws EntityNotFoundException, KafkaSendMessageException, BadRequestException {
        Optional<Task> fromDb = this.taskRepository.findById(taskId);
        if (fromDb.isEmpty()) {
            throw new EntityNotFoundException("Task with id " + taskId + " not found.");
        }
        taskParticipantRepository.deleteByTaskId(taskId);
        task.setCreated(fromDb.get().getCreated());
        fromDb.get().setParticipants(null);
        modifyTaskWithoutCheckIfExist(taskId, sync, task);
    }

    public void modifyInternalTask(Long taskId, Task task) throws EntityNotFoundException, KafkaSendMessageException, BadRequestException {
        Optional<Task> fromDb = this.taskRepository.findById(taskId);
        if (fromDb.isEmpty()) {
            throw new EntityNotFoundException("Task with id " + taskId + " not found.");
        }
        task.setCreated(fromDb.get().getCreated());
        Set<TaskParticipant> participants = taskParticipantRepository.findAllByTaskId(taskId);
        participants.forEach(participant -> participant.setTask(task));
        task.setParticipants(participants);
        task.setNotified(false);
        modifyInternalTaskWithoutCheckIfExist(taskId, task);
    }


    private void modifyTaskWithoutCheckIfExist(Long taskId, Boolean sync, Task task) throws KafkaSendMessageException, BadRequestException {
        validateDatesAndThrowExceptionIfInvalid(task);
        task.setId(taskId);
        task.setNotified(false);

        if (task.getTaskAssociations() != null) {
            updateAssociations(task);
        }
        if (task.getParticipants() != null && task.getParticipants().size() > 0) {
            task.getParticipants().forEach(participant -> participant.setTask(task));
        }
        taskRepository.saveAndFlush(task);
        if (sync) {
            kafkaService.send(task.getOwner(), KafkaMsgType.CALENDAR_MODIFIED_EVENT, createKafkaMessageDataSetFromTask(task));
        }
    }

    private void modifyInternalTaskWithoutCheckIfExist(Long taskId, Task task) throws KafkaSendMessageException, BadRequestException {
        validateDatesAndThrowExceptionIfInvalid(task);
        task.setId(taskId);

        if (task.getTaskAssociations() != null) {
            updateAssociations(task);
        }
        if (task.getParticipants() != null && task.getParticipants().size() > 0) {
            task.getParticipants().forEach(participant -> participant.setTask(task));
        }
        taskRepository.saveAndFlush(task);
    }

    private void validateDatesAndThrowExceptionIfInvalid(Task task) throws BadRequestException {
        if (task.getDateTimeFrom().isAfter(task.getDateTimeTo())) {
            throw new BadRequestException("Start date cannot be after end date");
        }
    }

    private Set<KafkaMessageData> createKafkaMessageDataSetFromTask(Task task) {
        return KafkaMessageDataBuilder.builder()
                .add("id", String.valueOf(task.getId()))
                .add("subject", task.getSubject())
                .add("place", task.getPlace())
                .add("dateTimeFrom", task.getDateTimeFrom().format(getRFC3339Formatter()))
                .add("dateTimeTo", task.getDateTimeTo().format(getRFC3339Formatter()))
                .add("allDayEvent", String.valueOf(task.getAllDayEvent()))
                .add("owner", task.getOwner() != null ? String.valueOf(task.getOwner()) : null)
                .build();
    }

    private DateTimeFormatter getRFC3339Formatter() {
        return DateTimeFormatter
                .ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .withZone(ZoneId.of("UTC"));
    }

    private void schedulerTaskNotifications(Task task) {
        Set<Long> receiverAccountIdSet = task.getParticipants().stream()
                .filter(participant -> participant.getEntityOwnerId() != null)
                .map(TaskParticipant::getEntityOwnerId)
                .collect(Collectors.toSet());
        receiverAccountIdSet.add(task.getOwner());

        receiverAccountIdSet.forEach(receiverAccountId -> {
            taskNotificationSchedulerService.scheduleTaskNotificationJob(task, receiverAccountId);
        });
    }

    private void addAssociations(Task task) {
        associationClient.createAssociations(getAssociationDetailDto(task));
    }

    private void updateAssociations(Task task) {
        associationClient.updateAssociations(getTaskType(task), task.getId(), getAssociationDetailDto(task));
    }

    private List<AssociationDetailDto> getAssociationDetailDto(Task task) {
        if (task.getTaskAssociations() == null) {
            return new ArrayList<>();
        }
        return task.getTaskAssociations().stream().map(c -> c.toAssociationDetailDto(task)).collect(Collectors.toList());
    }

    private List<TaskAssociationDto> getTaskAssociationsDto(Task task) {
        return associationClient.getAllByTypeAndId(getTaskType(task), task.getId())
                .stream().map(TaskAssociationDto::fromAssociationDto).collect(Collectors.toList());
    }

    private CrmObjectType getTaskType(Task task) {
        return (task.getActivityType() == null) ? CrmObjectType.task : CrmObjectType.activity;
    }

    public List<Task> findAllContaining(String pattern, boolean activity) {
        if (activity) {
            return taskRepository.findActivityByNameContaining(pattern);
        } else {
            return taskRepository.findTaskByNameContaining(pattern);
        }
    }

    public List<Task> findAllByIds(List<Long> ids, boolean activity) {
        if (activity) {
            return taskRepository.findActivityByIds(ids);
        } else {
            return taskRepository.findTaskByIds(ids);
        }
    }

    public List<Task> findAllTasksAndActivitiesByIds(List<Long> ids) {
        return taskRepository.findAllTasksAndActivitiesByIds(ids);
    }

    public Long getActivityCountForGoal(Set<Long> userIds, Boolean onlyPhone, LocalDateTime from, LocalDateTime to) {
        return taskRepository.getActivityCountForGoal(userIds, onlyPhone, from, to);
    }


    public Long getOutstandingActivitiesCount(Long accountId, Long associationId, CrmObjectType associationType) {
        return taskRepository.getOutstandingForUserCount(accountId, LocalDateTime.now(),
                isMobileTasksFilteredByIds(associationId, associationType),
                getAssociatedTasksIds(associationId, associationType));
    }

    public Page<MobileTaskDto> getPlannedActivities(Long accountId, Pageable pageable, Long associationId, CrmObjectType associationType) {
        return mapToMobileTaskDtoPage(taskRepository.getAllPlannedForUser(pageable, accountId, LocalDateTime.now(),
                isMobileTasksFilteredByIds(associationId, associationType),
                getAssociatedTasksIds(associationId, associationType)));
    }

    public Page<MobileTaskDto> getOutstandingActivities(Long accountId, Pageable pageable, Long associationId, CrmObjectType associationType) {
        return mapToMobileTaskDtoPage(taskRepository.getAllOutstandingForUser(pageable, accountId, LocalDateTime.now(),
                isMobileTasksFilteredByIds(associationId, associationType),
                getAssociatedTasksIds(associationId, associationType)));
    }

    public Page<MobileTaskDto> getInProgressActivities(Long accountId, Pageable pageable, Long associationId, CrmObjectType associationType) {
        return mapToMobileTaskDtoPage(taskRepository.getAllInProgressForUser(pageable, accountId,
                isMobileTasksFilteredByIds(associationId, associationType),
                getAssociatedTasksIds(associationId, associationType)));
    }

    public Page<MobileTaskDto> getSuspendedActivities(Long accountId, Pageable pageable, Long associationId, CrmObjectType associationType) {
        return mapToMobileTaskDtoPage(taskRepository.getAllSuspendedForUser(pageable, accountId,
                isMobileTasksFilteredByIds(associationId, associationType),
                getAssociatedTasksIds(associationId, associationType)));
    }

    public Page<MobileTaskDto> getFinishedActivities(Long accountId, Pageable pageable, Long associationId, CrmObjectType associationType) {
        return mapToMobileTaskDtoPage(taskRepository.getAllFinishedForUser(pageable, accountId,
                isMobileTasksFilteredByIds(associationId, associationType),
                getAssociatedTasksIds(associationId, associationType)));
    }

    public Page<MobileTaskDto> getActivities(Long accountId, Pageable pageable, Long associationId, CrmObjectType associationType) {
        return mapToMobileTaskDtoPage(taskRepository.findAllForUser(pageable, accountId,
                isMobileTasksFilteredByIds(associationId, associationType),
                getAssociatedTasksIds(associationId, associationType)));
    }

    private Set<Long> getAssociatedTasksIds(Long associationId, CrmObjectType associationType) {
        if (isMobileTasksFilteredByIds(associationId, associationType)) {
            Set<Long> result = getAssociatedTaskIds(associationId, associationType, CrmObjectType.task);
            result.addAll(getAssociatedTaskIds(associationId, associationType, CrmObjectType.activity));
            if (result.size() > 0) {
                return result;
            }
        }

        HashSet<Long> emptyList = new HashSet<>();
        emptyList.add(null);
        return emptyList;
    }

    private Set<Long> getAssociatedTaskIds(Long associationId, CrmObjectType associationType, CrmObjectType destinationType) {
        try {
            return associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(associationType, destinationType, associationId)
                    .stream().map(AssociationDto::getDestinationId).collect(Collectors.toSet());
        } catch (Exception ignore) {
        }
        return new HashSet<>();
    }

    private boolean isMobileTasksFilteredByIds(Long associationId, CrmObjectType associationType) {
        return (associationId != null && associationType != null);
    }

    private Page<MobileTaskDto> mapToMobileTaskDtoPage(Page<Task> page) {
        Page<MobileTaskDto> result = page.map(this::getMobileDtoFromModel);
        try {
            return addAssociatedContactAndCustomer(result);
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }

    public Page<TaskDto> getActivitiesAssociatedWithObject(Long id, CrmObjectType type, Pageable pageable) {
        List<Long> taskIds = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(type, CrmObjectType.activity, id)
                .stream().map(AssociationDto::getDestinationId).collect(Collectors.toList());
        taskIds.addAll(associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(type, CrmObjectType.task, id)
                .stream().map(AssociationDto::getDestinationId).collect(Collectors.toList()));

        if (!taskIds.isEmpty()) {
            Page<Task> result = taskRepository.findAllByActivityTypeIsNotNullAndIdIsInOrderByDateTimeFromDesc(taskIds, pageable);
            LocalDateTime now = LocalDateTime.now();
            return result.map(c -> mapToDtoFromTaskAnSetTimeCategoryEnum(c, now));
        }
        return new PageImpl<TaskDto>(new ArrayList<>());
    }

    public List<TaskDto> getActivitiesAssociatedWithObject(Long id, CrmObjectType type, ActivityTypeEnum activityType) {
        List<TaskDto> taskList = new ArrayList<>();
        List<Long> taskIdList = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(type, CrmObjectType.activity, id)
                .stream().map(AssociationDto::getDestinationId).collect(Collectors.toList());
        taskIdList.addAll(associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(type, CrmObjectType.task, id)
                .stream().map(AssociationDto::getDestinationId).collect(Collectors.toList()));

        if (!taskIdList.isEmpty()) {
            taskList.addAll(taskRepository.findAll(findByIdIn(taskIdList).and(findByActivityType(activityType))).stream()
                    .map(task -> mapToDtoFromTaskAnSetTimeCategoryEnum(task, LocalDateTime.now()))
                    .collect(Collectors.toList()));
        }
        return taskList;
    }

    public Page<TaskDto> getActivitiesAssociatedWithObject(Long id, CrmObjectType type, TaskTimeCategoryEnum timeCategory, Pageable pageable) {
        List<Long> taskIds = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(type, CrmObjectType.activity, id)
                .stream().map(AssociationDto::getDestinationId).collect(Collectors.toList());
        taskIds.addAll(associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(type, CrmObjectType.task, id)
                .stream().map(AssociationDto::getDestinationId).collect(Collectors.toList()));

        if (!taskIds.isEmpty()) {
            Specification<Task> spec = findByIdIn(taskIds);
            switch (timeCategory) {
                case ALL: {
                    return taskRepository.findAll(spec, pageable)
                            .map(c -> mapToDtoFromTaskAnSetTimeCategoryEnum(c, TaskTimeCategoryEnum.ALL));
                }
                case OLD: {
                    LocalDateTime startOfDay = LocalDateTime.now().toLocalDate().atStartOfDay();
                    spec = spec.and(findByDateTimeFromBeforeExclusive(startOfDay));
                    return taskRepository.findAll(spec, pageable)
                            .map(c -> mapToDtoFromTaskAnSetTimeCategoryEnum(c, TaskTimeCategoryEnum.OLD));
                }
                case PLANNED: {
                    LocalDateTime endOfDay = LocalDateTime.now().toLocalDate().atTime(LocalTime.MAX);
                    spec = spec.and(findByDateTimeFromAfterExclusive(endOfDay));
                    return taskRepository.findAll(spec, pageable)
                            .map(c -> mapToDtoFromTaskAnSetTimeCategoryEnum(c, TaskTimeCategoryEnum.PLANNED));
                }
                case LATEST: {
                    LocalDateTime startOfDay = LocalDateTime.now().toLocalDate().atStartOfDay();
                    LocalDateTime endOfDay = LocalDateTime.now().toLocalDate().atTime(LocalTime.MAX);
                    spec = spec.and(findByDateTimeFromBetween(startOfDay, endOfDay));
                    return taskRepository.findAll(spec, pageable)
                            .map(c -> mapToDtoFromTaskAnSetTimeCategoryEnum(c, TaskTimeCategoryEnum.LATEST));
                }
                case COMPLETED: {
                    spec = spec.and(findByState(TaskState.FINISHED));
                    return taskRepository.findAll(spec, pageable)
                            .map(c -> mapToDtoFromTaskAnSetTimeCategoryEnum(c, TaskTimeCategoryEnum.COMPLETED));
                }
                default:
                    return new PageImpl<TaskDto>(new ArrayList<>());

            }
        }
        return new PageImpl<TaskDto>(new ArrayList<>());
    }

    public List<TaskDto> getPreviewActivitiesAssociatedWithObject(Long id, CrmObjectType type) {
        List<Long> taskIds = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(type, CrmObjectType.activity, id)
                .stream().map(AssociationDto::getDestinationId).collect(Collectors.toList());
        taskIds.addAll(associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(type, CrmObjectType.task, id)
                .stream().map(AssociationDto::getDestinationId).collect(Collectors.toList()));


        List<TaskDto> result = new ArrayList<>();
        if (taskIds.size() > 0) {
            LocalDateTime now = LocalDateTime.now();

            result.addAll(getDtoListFromModelList(taskRepository
                    .findTop1ByDateTimeFromIsAfterAndIdIsInOrderByDateTimeFromDesc(now, taskIds), TaskTimeCategoryEnum.PLANNED));
            result.addAll(getDtoListFromModelList(taskRepository
                    .findTop3ByDateTimeFromIsBeforeAndDateTimeFromIsAfterAndIdIsInOrderByDateTimeFromDesc(now, now.minusDays(taskOldAfterDays), taskIds), TaskTimeCategoryEnum.LATEST));
            result.addAll(getDtoListFromModelList(taskRepository
                    .findTop3ByDateTimeFromIsBeforeAndIdIsInOrderByDateTimeFromDesc(now.minusDays(taskOldAfterDays), taskIds), TaskTimeCategoryEnum.OLD));
        }
        return result;
    }

    public List<TaskDto> getCollidingTasks(Task task) {
        if (task.getId() != null) {
            return taskRepository.getCollidedTasksOnUpdate(task.getDateTimeFrom(), task.getDateTimeTo(), task.getId()).stream().map(this::getDtoFromModel).collect(Collectors.toList());
        } else {
            return taskRepository.getCollidedTasks(task.getDateTimeFrom(), task.getDateTimeTo()).stream().map(this::getDtoFromModel).collect(Collectors.toList());
        }
    }

    private TaskTimeCategoryEnum getTaskTimeCategory(TaskDto taskDto, LocalDateTime now) {
        if (taskDto.getDateTimeFrom().isAfter(now)) {
            return TaskTimeCategoryEnum.PLANNED;
        }
        if (taskDto.getDateTimeFrom().isBefore(now.minusDays(taskOldAfterDays))) {
            return TaskTimeCategoryEnum.OLD;
        }
        return TaskTimeCategoryEnum.LATEST;
    }

    private List<TaskDto> getDtoListFromModelList(List<Task> tasks, TaskTimeCategoryEnum taskTimeCategoryEnum) {
        return tasks.stream().map(this::getDtoFromModel).peek(c -> c.setTaskTimeCategoryEnum(taskTimeCategoryEnum)).collect(Collectors.toList());
    }

    private TaskDto getDtoFromModel(Task task) {
        return modelMapper.map(task, TaskDto.class);
    }

    private TaskDto mapToDtoFromTaskAnSetTimeCategoryEnum(Task task, LocalDateTime now) {
        TaskDto dto = getDtoFromModel(task);
        dto.setTaskTimeCategoryEnum(getTaskTimeCategory(dto, now));
        return dto;
    }

    private TaskDto mapToDtoFromTaskAnSetTimeCategoryEnum(Task task, TaskTimeCategoryEnum taskTimeCategoryEnum) {
        TaskDto dto = getDtoFromModel(task);
        dto.setTaskTimeCategoryEnum(taskTimeCategoryEnum);
        return dto;
    }

    private MobileTaskDto getMobileDtoFromModel(Task task) {
        return modelMapper.map(task, MobileTaskDto.class);
    }

    private Page<MobileTaskDto> addAssociatedContactAndCustomer(Page<MobileTaskDto> page) {
        List<Long> activityIds = page.getContent().stream().filter(c -> c.getActivityType() != null).map(MobileTaskDto::getId).collect(Collectors.toList());
        List<Long> taskIds = page.getContent().stream().filter(c -> c.getActivityType() == null).map(MobileTaskDto::getId).collect(Collectors.toList());
        Map<Long, Long> contactAssociationMap = getAssociationMap(activityIds, taskIds, CrmObjectType.contact);
        Map<Long, Long> customerAssociationMap = getAssociationMap(activityIds, taskIds, CrmObjectType.customer);
        return page.map(c -> addContactAndCustomerId(c, contactAssociationMap, customerAssociationMap));
    }

    private MobileTaskDto addContactAndCustomerId(MobileTaskDto mobileTaskDto, Map<Long, Long> contactAssociationMap, Map<Long, Long> customerAssociationMap) {
        mobileTaskDto.setAssociatedContactId(contactAssociationMap.get(mobileTaskDto.getId()));
        mobileTaskDto.setAssociatedCustomerId(customerAssociationMap.get(mobileTaskDto.getId()));
        return mobileTaskDto;
    }

    private Map<Long, Long> getAssociationMap(List<Long> activityIds, List<Long> taskIds, CrmObjectType type) { // <taskId/activityId, typeId(first)>
        Map<Long, Long> contactAssociationMap = new HashMap<>();
        List<AssociationDto> associatedContactsToActivity = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceIds(CrmObjectType.activity, type, activityIds);
        List<AssociationDto> associatedContactsToTask = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceIds(CrmObjectType.task, type, taskIds);

        associatedContactsToActivity.forEach(c -> contactAssociationMap.put(c.getSourceId(), c.getDestinationId()));
        associatedContactsToTask.forEach(c -> contactAssociationMap.put(c.getSourceId(), c.getDestinationId()));
        return contactAssociationMap;
    }

    public Map<Long, ActivityTypeEnum> getActivityTypesRelatedWith(CrmObjectType type, Long id) {
        List<Long> taskIds = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(type, CrmObjectType.activity, id)
                .stream().map(AssociationDto::getDestinationId).collect(Collectors.toList());
        taskIds.addAll(associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(type, CrmObjectType.task, id)
                .stream().map(AssociationDto::getDestinationId).collect(Collectors.toList()));

        return findAllTasksAndActivitiesByIds(taskIds)
                .stream()
                .collect(HashMap::new, (m, v) -> m.put(v.getId(), v.getActivityType()), HashMap::putAll);
    }

    private List<Long> getAssociationIdList(Task task, CrmObjectType type) {
        return task.getTaskAssociations().stream()
                .filter(association -> association.getObjectType().equals(type))
                .map(TaskAssociationDto::getObjectId)
                .collect(Collectors.toList());
    }

    public Map<Long, TaskSummaryDto> getAllTaskSummary(CrmObjectType type, List<Long> sourceIds, List<TaskTimeCategoryEnum> timeCategoryList) {
        List<AssociationDto> associacionActivityList = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceIds(type, CrmObjectType.activity, sourceIds);
        List<AssociationDto> associacionTaskList = associationClient.getAllBySourceTypeAndDestinationTypeAndSourceIds(type, CrmObjectType.task, sourceIds);
        Map<Long, List<Long>> associationMap = Stream.of(associacionActivityList, associacionTaskList)
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(AssociationDto::getSourceId,
                        element -> Collections.singletonList(element.getDestinationId()),
                        (oldValue, newValue) -> Stream.of(oldValue, newValue).flatMap(Collection::stream).collect(Collectors.toList())));
        List<Long> taskIds = associationMap.values().stream().flatMap(Collection::stream).collect(Collectors.toList());

        List<TaskDto> tasks = new ArrayList<>();
        Specification<Task> baseSpec = findByIdIn(taskIds);
        timeCategoryList.forEach(timeCategory -> {
            switch (timeCategory) {
                case ALL: {
                    tasks.addAll(taskRepository.findAll(baseSpec).stream()
                            .map(c -> mapToDtoFromTaskAnSetTimeCategoryEnum(c, TaskTimeCategoryEnum.ALL))
                            .collect(Collectors.toList()));
                    break;
                }
                case OLD: {
                    LocalDateTime startOfDay = LocalDateTime.now().toLocalDate().atStartOfDay();
                    var spec = baseSpec.and(findByDateTimeFromBeforeExclusive(startOfDay));
                    tasks.addAll(taskRepository.findAll(spec).stream()
                            .map(c -> mapToDtoFromTaskAnSetTimeCategoryEnum(c, TaskTimeCategoryEnum.OLD))
                            .collect(Collectors.toList()));
                    break;
                }
                case PLANNED: {
                    LocalDateTime endOfDay = LocalDateTime.now().toLocalDate().atTime(LocalTime.MAX);
                    var spec = baseSpec.and(findByDateTimeFromAfterExclusive(endOfDay));
                    tasks.addAll(taskRepository.findAll(spec).stream()
                            .map(c -> mapToDtoFromTaskAnSetTimeCategoryEnum(c, TaskTimeCategoryEnum.PLANNED))
                            .collect(Collectors.toList()));
                    break;
                }
                case LATEST: {
                    LocalDateTime startOfDay = LocalDateTime.now().toLocalDate().atStartOfDay();
                    LocalDateTime endOfDay = LocalDateTime.now().toLocalDate().atTime(LocalTime.MAX);
                    var spec = baseSpec.and(findByDateTimeFromBetween(startOfDay, endOfDay));
                    tasks.addAll(taskRepository.findAll(spec).stream()
                            .map(c -> mapToDtoFromTaskAnSetTimeCategoryEnum(c, TaskTimeCategoryEnum.LATEST))
                            .collect(Collectors.toList()));
                    break;
                }
                case COMPLETED: {
                    var spec = baseSpec.and(findByState(TaskState.FINISHED));
                    tasks.addAll(taskRepository.findAll(spec).stream()
                            .map(c -> mapToDtoFromTaskAnSetTimeCategoryEnum(c, TaskTimeCategoryEnum.COMPLETED))
                            .collect(Collectors.toList()));
                    break;
                }
            }
        });

        return associationMap.entrySet().stream()
                .map(entry -> Map.entry(entry.getKey(), tasks.stream()
                        .filter(task -> entry.getValue().contains(task.getId()))
                        .collect(Collectors.toList())))
                .collect(Collectors.toMap(Map.Entry::getKey,
                        entry -> TaskSummaryDto.builder()
                                .allTasks(entry.getValue().stream().filter(task -> TaskTimeCategoryEnum.ALL.equals(task.getTaskTimeCategoryEnum())).count())
                                .oldTasks(entry.getValue().stream().filter(task -> TaskTimeCategoryEnum.OLD.equals(task.getTaskTimeCategoryEnum())).count())
                                .plannedTasks(entry.getValue().stream().filter(task -> TaskTimeCategoryEnum.PLANNED.equals(task.getTaskTimeCategoryEnum())).count())
                                .latestTasks(entry.getValue().stream().filter(task -> TaskTimeCategoryEnum.LATEST.equals(task.getTaskTimeCategoryEnum())).count())
                                .completedTasks(entry.getValue().stream().filter(task -> TaskTimeCategoryEnum.COMPLETED.equals(task.getTaskTimeCategoryEnum())).count())
                                .build()
                ));
    }
}