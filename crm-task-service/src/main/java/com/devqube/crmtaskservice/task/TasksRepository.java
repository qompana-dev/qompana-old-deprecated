package com.devqube.crmtaskservice.task;

import com.devqube.crmtaskservice.task.model.ActivityTypeEnum;
import com.devqube.crmtaskservice.task.model.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface TasksRepository extends JpaRepository<Task, Long> {
    List<Task> findAllByOwner(Long owner);
    List<Task> findAllByOwnerAndDateTimeFromIsAfterOrderByDateTimeFrom(Long owner, LocalDateTime after);
    List<Task> findAllByOwnerIn(Set<Long> owner);
    List<Task> findAllByCreatorAndOwnerNot(Long creator, Long owner);
    List<Task> findAllByIntervalNotNullAndUnitNotNullAndNotifiedFalseAndNotificationTimeBefore(LocalDateTime now);


    @Query("SELECT t FROM Task t WHERE LOWER(t.subject) LIKE LOWER(concat('%', ?1, '%')) and t.activityType is null")
    List<Task> findTaskByNameContaining(String pattern);

    @Query("SELECT t FROM Task t WHERE t.id in ?1 and t.activityType is null")
    List<Task> findTaskByIds(List<Long> ids);

    @Query("SELECT t FROM Task t WHERE LOWER(t.subject) LIKE LOWER(concat('%', ?1, '%')) and t.activityType is not null")
    List<Task> findActivityByNameContaining(String pattern);

    @Query("SELECT t FROM Task t WHERE t.id in ?1 and t.activityType is not null")
    List<Task> findActivityByIds(List<Long> ids);

    Page<Task> findAllByActivityTypeIsNotNullAndIdIsInOrderByDateTimeFromDesc(List<Long> ids, Pageable pageable);

    List<Task> findTop1ByActivityTypeIsNotNullAndDateTimeFromIsAfterAndIdIsInOrderByDateTimeFromDesc(LocalDateTime after, List<Long> ids);

    List<Task> findTop3ByActivityTypeIsNotNullAndDateTimeFromIsBeforeAndIdIsInOrderByDateTimeFromDesc(LocalDateTime before, List<Long> ids);

    List<Task> findTop3ByActivityTypeIsNotNullAndDateTimeFromIsBeforeAndDateTimeFromIsAfterAndIdIsInOrderByDateTimeFromDesc(LocalDateTime before, LocalDateTime after, List<Long> ids);

    @Query("SELECT count(t) FROM Task t WHERE t.owner in :userIds and t.dateTimeTo > :from and t.dateTimeTo < :to and ((:onlyPhone = true and t.activityType = com.devqube.crmtaskservice.task.model.ActivityTypeEnum.PHONE) or (:onlyPhone = false and t.activityType <> com.devqube.crmtaskservice.task.model.ActivityTypeEnum.PHONE)) and t.state = com.devqube.crmtaskservice.task.model.TaskState.FINISHED")
    Long getActivityCountForGoal(@Param("userIds") Set<Long> userIds, @Param("onlyPhone") Boolean onlyPhone, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to);

    @Query("SELECT t FROM Task t  WHERE t.owner <> :owner and t.activityType = :activityType " +
            "and t.dateTimeFrom > :fromDateTime and t.dateTimeFrom < :toDateTime " +
            "and t.latitude is not null and t.longitude is not null " +
            "and t.latitude > :minLat and t.latitude < :maxLat " +
            "and t.longitude > :minLon and t.longitude < :maxLon")
    List<Task> getNearbyTasks(@Param("owner") Long owner,
                              @Param("minLat") Double minLat, @Param("maxLat") Double maxLat,
                              @Param("minLon") Double minLon, @Param("maxLon") Double maxLon,
                              @Param("fromDateTime") LocalDateTime fromDateTime, @Param("toDateTime") LocalDateTime toDateTime,
                              @Param("activityType") ActivityTypeEnum activityType);
}
