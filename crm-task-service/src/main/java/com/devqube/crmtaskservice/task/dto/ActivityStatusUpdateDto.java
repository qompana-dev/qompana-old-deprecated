package com.devqube.crmtaskservice.task.dto;

import com.devqube.crmtaskservice.task.model.TaskState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ActivityStatusUpdateDto {
    private Long taskId;
    private TaskState taskState;
}
