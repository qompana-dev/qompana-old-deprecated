package com.devqube.crmtaskservice.task.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskDateChangeDto {
    private Long taskId;
    private LocalDateTime dateTimeFrom;
    private LocalDateTime dateTimeTo;
}
