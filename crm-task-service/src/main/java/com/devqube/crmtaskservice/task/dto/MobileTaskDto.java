package com.devqube.crmtaskservice.task.dto;

import com.devqube.crmshared.task.ActivityTypeEnum;
import com.devqube.crmtaskservice.task.model.TaskState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MobileTaskDto {
    private Long id;
    private String subject;

    private Long creator;
    private String place;
    private Double longitude;
    private Double latitude;

    private TaskState state;
    private ActivityTypeEnum activityType;
    private LocalDateTime dateTimeFrom;
    private LocalDateTime dateTimeTo;
    private List<TaskAssociationDto> taskAssociations;

    private Boolean allDayEvent;
    private Long owner;
    private String description;

    private Long associatedContactId;
    private Long associatedCustomerId;
}
