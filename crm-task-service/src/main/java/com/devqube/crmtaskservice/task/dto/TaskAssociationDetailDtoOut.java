package com.devqube.crmtaskservice.task.dto;

import com.devqube.crmshared.search.CrmObjectType;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TaskAssociationDetailDtoOut {

    private Long id;
    private String label;
    private CrmObjectType type;
    private String firm;

}
