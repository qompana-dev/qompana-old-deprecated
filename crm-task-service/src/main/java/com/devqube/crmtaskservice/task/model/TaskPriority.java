package com.devqube.crmtaskservice.task.model;


public enum TaskPriority {

    MEDIUM,
    URGENT;

    public static TaskPriority fromValue(String value) {
        for (TaskPriority priority : TaskPriority.values()) {
            if (priority.name().toLowerCase().equals(value.toLowerCase())) {
                return priority;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}
