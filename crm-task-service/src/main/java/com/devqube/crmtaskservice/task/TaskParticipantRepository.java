package com.devqube.crmtaskservice.task;

import com.devqube.crmtaskservice.task.model.TaskParticipant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface TaskParticipantRepository extends JpaRepository<TaskParticipant, Long>, JpaSpecificationExecutor<TaskParticipant> {

    void deleteByTaskId(Long id);

    Set<TaskParticipant> findAllByTaskId(Long id);

}
