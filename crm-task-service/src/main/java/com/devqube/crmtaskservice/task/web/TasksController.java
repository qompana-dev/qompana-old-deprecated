package com.devqube.crmtaskservice.task.web;

import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.access.annotation.AuthControllerCase;
import com.devqube.crmshared.access.annotation.AuthControllerType;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.task.ActivityTypeEnum;
import com.devqube.crmshared.task.TaskSummaryDto;
import com.devqube.crmtaskservice.integration.UserClient;
import com.devqube.crmtaskservice.task.TasksService;
import com.devqube.crmtaskservice.task.dto.*;
import com.devqube.crmtaskservice.task.model.Task;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class TasksController implements TasksApi {

    private final TasksService tasksService;
    private final UserClient userClient;

    public TasksController(TasksService tasksService, UserClient userClient) {
        this.tasksService = tasksService;
        this.userClient = userClient;
    }

    @Override
    @AuthController(actionFrontendId = "CalendarComponent.addTask")
    public ResponseEntity<Task> createTask(@NotNull @Valid Boolean sync, @Valid TaskWithParticipantsDto taskDto) {
        try {
            return new ResponseEntity<>(tasksService.save(TaskDtoMapper.toTask(taskDto), sync), HttpStatus.CREATED);
        } catch (KafkaSendMessageException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<Task> createTaskInternal(@NotNull @Valid Boolean sync, @Valid Task task) {
        try {
            return new ResponseEntity<>(tasksService.save(task, sync), HttpStatus.CREATED);
        } catch (KafkaSendMessageException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override // TODO [PW] 08.01.2020: add permissions
    public ResponseEntity<List<Task>> getUpcomingTasksByOwner(@NotNull @Valid Long owner) {
        return new ResponseEntity<>(tasksService.getUpcomingTasksByOwner(owner), HttpStatus.OK);
    }

    @Override
    @AuthController(readFrontendId = "CalendarComponent.tasks")
    public ResponseEntity<List<Task>> getAllTasksByOwner(@NotNull @Valid Long owner,
                                                         @Valid TaskWidgetTimeCategoryEnum timeCategory,
                                                         @Valid String date) {
        if (timeCategory != null && date != null && !date.isBlank()) {
            try {
                return new ResponseEntity<>(tasksService.getAllTasksByOwner(owner, timeCategory, date), HttpStatus.OK);
            } catch (DateTimeParseException exception) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(tasksService.getAllTasksByOwner(owner), HttpStatus.OK);
        }
    }

    // TODO Add permissions
    @Override
    public ResponseEntity<List<TaskDto>> getAllTasksByOwnerForMobile(@NotNull @Valid Long owner) {
        return new ResponseEntity<>(tasksService.getAllTasksByOwnerWithAssociations(owner), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Task>> getAllTasksByOwnerInternal(@NotNull @Valid Long owner) {
        return new ResponseEntity<>(tasksService.getAllTasksByOwner(owner), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Task>> getAllTasksByCrmObject(Long id, CrmObjectType type) {
        return new ResponseEntity<>(tasksService.getAllTasksAssociatedWithObject(id, type), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<TaskAssociationDetailDtoOut>> getTaskAssociationsDetails(Long taskId) {
        try {
            return new ResponseEntity<>(tasksService.getTaskAssociationsDetails(taskId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<TaskWithParticipantsDto> getTask(Long taskId) {
        try {
            return new ResponseEntity<>(TaskDtoMapper.toTaskDto(tasksService.getTask(taskId)), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Task> getTaskInternal(Long taskId) {
        try {
            return new ResponseEntity<>(tasksService.getTask(taskId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(readFrontendId = "permissionDenied", controllerCase = {
            @AuthControllerCase(type = "add", readFrontendId = "ExpenseAddComponent.activity"),
            @AuthControllerCase(type = "edit", readFrontendId = "ExpenseEditComponent.activity")
    })
    public ResponseEntity<Task> getTaskForExpenses(Long taskId) {
        try {
            return new ResponseEntity<>(tasksService.getTask(taskId), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @AuthController(actionFrontendId = "CalendarComponent.removeTask")
    public ResponseEntity<Void> removeTask(Long taskId, @NotNull @Valid Boolean sync) {
        try {
            tasksService.removeTask(taskId, sync);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (KafkaSendMessageException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> removeTaskInternal(Long taskId, @NotNull @Valid Boolean sync) {
        try {
            tasksService.removeTask(taskId, sync);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (KafkaSendMessageException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @AuthController(actionFrontendId = "CalendarComponent.editTask")
    public ResponseEntity<Void> updateTask(Long taskId, @NotNull @Valid Boolean sync, @Valid TaskWithParticipantsDto taskDto) {
        try {
            tasksService.modifyTask(taskId, sync, TaskDtoMapper.toTask(taskDto));
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (KafkaSendMessageException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(actionFrontendId = "CalendarComponent.editTask")
    public ResponseEntity<Void> updateActivityStatus(Long taskId, @Valid ActivityStatusUpdateDto activityStatusUpdateDto) {
        try {
            tasksService.updateActivityStatus(taskId, activityStatusUpdateDto);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (KafkaSendMessageException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(actionFrontendId = "CalendarComponent.editTask")
    public ResponseEntity<Void> updateTaskDate(Long taskId, @Valid TaskDateChangeDto taskDateChangeDto) {
        try {
            tasksService.updateTaskDate(taskId, taskDateChangeDto);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (KafkaSendMessageException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<Void> updateTaskInternal(Long taskId, @NotNull @Valid Boolean sync, @Valid Task task) {
        try {
            tasksService.modifyInternalTask(taskId, task);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (KafkaSendMessageException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @AuthController(readFrontendId = "CalendarComponent.tasks")
    public ResponseEntity<List<Task>> getAllTasksByCreator(Long id) {
        return new ResponseEntity<>(tasksService.getAllTasksByCreator(id), HttpStatus.OK);
    }

    @Override
    @AuthController(readFrontendId = "CalendarComponent.tasks")
    public ResponseEntity<List<Task>> getAllTasksByRole(String roleName) {
        try {
            return new ResponseEntity<>(tasksService.getAllTasksByRole(roleName), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<List<CrmObject>> getTasksAsCrmObject(@Valid List<Long> ids) {
        return new ResponseEntity<>(tasksService.findAllByIds(ids, false)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.task, Strings.nullToEmpty(e.getSubject()), e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getTasksForSearch(@NotNull @Valid String term) {
        return new ResponseEntity<>(tasksService.findAllContaining(term, false)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.task, Strings.nullToEmpty(e.getSubject()), e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getActivitiesAsCrmObject(@Valid List<Long> ids) {
        return new ResponseEntity<>(tasksService.findAllByIds(ids, true)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.activity, Strings.nullToEmpty(e.getSubject()), e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<CrmObject>> getActivitiesForSearch(@NotNull @Valid String term) {
        return new ResponseEntity<>(tasksService.findAllContaining(term, true)
                .stream()
                .map(e -> new CrmObject(e.getId(), CrmObjectType.activity, Strings.nullToEmpty(e.getSubject()), e.getCreated()))
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @Override
    @AuthController(readFrontendId = "alwaysDenied", controllerCase = {
            @AuthControllerCase(type = "lead", readFrontendId = "LeadDetailsComponent.activity"),
            @AuthControllerCase(type = "opportunity", readFrontendId = "OpportunityDetailsComponent.activity")
    })
    public ResponseEntity<List<TaskDto>> getPreviewActivitiesAssociatedWithObject(Long id, @AuthControllerType CrmObjectType type) {
        return new ResponseEntity<>(tasksService.getPreviewActivitiesAssociatedWithObject(id, type), HttpStatus.OK);
    }

    @Override
    @AuthController(readFrontendId = "alwaysDenied", controllerCase = {
            @AuthControllerCase(type = "lead", readFrontendId = "LeadDetailsComponent.activity"),
            @AuthControllerCase(type = "opportunity", readFrontendId = "OpportunityDetailsComponent.activity"),
            @AuthControllerCase(type = "customer"),
            @AuthControllerCase(type = "contact")
    })
    public ResponseEntity<Page> getActivitiesAssociatedWithObject(Long id, @AuthControllerType CrmObjectType type, @Valid Pageable pageable) {
        return ResponseEntity.ok(tasksService.getActivitiesAssociatedWithObject(id, type, pageable));
    }


    @Override
    @AuthController(readFrontendId = "alwaysDenied", controllerCase = {
            @AuthControllerCase(type = "lead", readFrontendId = "LeadDetailsComponent.activity"),
            @AuthControllerCase(type = "opportunity", readFrontendId = "OpportunityDetailsComponent.activity"),
            @AuthControllerCase(type = "customer"),
            @AuthControllerCase(type = "contact")
    })
    public ResponseEntity<List<TaskDto>> getAllActivitiesByTypeAssociatedWithObject(Long id, @AuthControllerType CrmObjectType type, ActivityTypeEnum activityType) {
        return ResponseEntity.ok(tasksService.getActivitiesAssociatedWithObject(id, type, activityType));
    }


    @Override
    @AuthController(readFrontendId = "alwaysDenied", controllerCase = {
            @AuthControllerCase(type = "lead", readFrontendId = "LeadDetailsComponent.activity"),
            @AuthControllerCase(type = "opportunity", readFrontendId = "OpportunityDetailsComponent.activity"),
            @AuthControllerCase(type = "customer"),
            @AuthControllerCase(type = "contact")
    })
    public ResponseEntity<Page> getTimedActivitiesAssociatedWithObject(Long id,
                                                                       @AuthControllerType CrmObjectType type,
                                                                       TaskTimeCategoryEnum timeCategory,
                                                                       @Valid Pageable pageable) {
        return ResponseEntity.ok(tasksService.getActivitiesAssociatedWithObject(id, type, timeCategory, pageable));
    }

    @Override // todo: permissions
    public ResponseEntity<Page> getActivities(String email, @Valid Pageable pageable, @Valid Boolean planned, @Valid Boolean outstanding, @Valid Boolean inProgress, @Valid Boolean suspended, @Valid Boolean finished, @Valid Long associationId, @Valid CrmObjectType associationType) {
        Long accountId = userClient.getMyAccountId(email);
        if (planned != null && planned) {
            return ResponseEntity.ok(tasksService.getPlannedActivities(accountId, pageable, associationId, associationType));
        } else if (outstanding != null && outstanding) {
            return ResponseEntity.ok(tasksService.getOutstandingActivities(accountId, pageable, associationId, associationType));
        } else if (inProgress != null && inProgress) {
            return ResponseEntity.ok(tasksService.getInProgressActivities(accountId, pageable, associationId, associationType));
        } else if (suspended != null && suspended) {
            return ResponseEntity.ok(tasksService.getSuspendedActivities(accountId, pageable, associationId, associationType));
        } else if (finished != null && finished) {
            return ResponseEntity.ok(tasksService.getFinishedActivities(accountId, pageable, associationId, associationType));
        } else {
            return ResponseEntity.ok(tasksService.getActivities(accountId, pageable, associationId, associationType));
        }
    }

    @Override // todo: permissions
    public ResponseEntity<Long> getOutstandingActivitiesCount(String email, @Valid Long associationId, @Valid CrmObjectType associationType) {
        Long accountId = userClient.getMyAccountId(email);
        return ResponseEntity.ok(tasksService.getOutstandingActivitiesCount(accountId, associationId, associationType));
    }

    @Override
    public ResponseEntity<Long> getActivityCountForGoal(Set<Long> userIds, Boolean onlyPhone, String from, String to) {
        LocalDateTime fromDate = DateTimeFormatter.ISO_DATE_TIME.parse(from, LocalDateTime::from);
        LocalDateTime toDate = DateTimeFormatter.ISO_DATE_TIME.parse(to, LocalDateTime::from);
        return ResponseEntity.ok(tasksService.getActivityCountForGoal(userIds, onlyPhone, fromDate, toDate));
    }

    @Override
    public ResponseEntity<Map<Long, ActivityTypeEnum>> getActivityTypes(Long id, CrmObjectType type) {
        try {
            return new ResponseEntity<>(tasksService.getActivityTypesRelatedWith(type, id), HttpStatus.OK);
        } catch (Exception e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<TaskDto>> checkForTaskCollisions(@NotNull @Valid Boolean sync, @Valid Task task) {
        return new ResponseEntity<>(tasksService.getCollidingTasks(task), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Map<Long, TaskSummaryDto>> getAllTaskSummary(CrmObjectType type, List<Long> sourceIds, List<TaskTimeCategoryEnum> timeCategory) {
        return ResponseEntity.ok(tasksService.getAllTaskSummary(type, sourceIds, timeCategory));
    }

}
