package com.devqube.crmtaskservice.task.dto;

import com.devqube.crmshared.task.ActivityTypeEnum;
import com.devqube.crmtaskservice.task.model.TaskPriority;
import com.devqube.crmtaskservice.task.model.TaskState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskDto {
    private Long id;
    private String subject;
    private TaskPriority priority;

    private Long creator;
    private String place;
    private Double longitude;
    private Double latitude;

    private ActivityTypeEnum activityType;
    private LocalDateTime dateTimeFrom;
    private LocalDateTime dateTimeTo;

    private Boolean allDayEvent;
    private Long owner;
    private String description;
    private List<TaskAssociationDto> taskAssociations;

    private TaskTimeCategoryEnum taskTimeCategoryEnum;
    private String timeZone;
    private TaskState state;
}
