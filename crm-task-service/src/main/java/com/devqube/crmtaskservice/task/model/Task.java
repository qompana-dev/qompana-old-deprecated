package com.devqube.crmtaskservice.task.model;

import com.devqube.crmshared.association.RemoveCrmObject;
import com.devqube.crmshared.history.SaveCrmChangesListener;
import com.devqube.crmshared.history.annotation.SaveHistory;
import com.devqube.crmshared.history.annotation.SaveHistoryIgnore;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.task.ActivityTypeEnum;
import com.devqube.crmtaskservice.task.dto.TaskAssociationDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EntityListeners(SaveCrmChangesListener.class)
@Accessors(chain = true)
public class Task {

    @Id
    @Access(AccessType.PROPERTY)
    @SequenceGenerator(name = "task_seq", sequenceName = "task_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "task_seq")
    @JsonProperty("id")
    private Long id;

    @JsonProperty("subject")
    @Length(max = 200)
    @NotNull
    private String subject;

    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("priority")
    private TaskPriority priority;

    @Enumerated(EnumType.ORDINAL)
    @JsonProperty("state")
    private TaskState state;

    @JsonProperty("creator")
    @NotNull
    private Long creator;

    @Length(max = 200)
    @JsonProperty("place")
    private String place;

    private Double longitude;

    private Double latitude;

    private ActivityTypeEnum activityType;

    @JsonProperty("dateTimeFrom")
    @NotNull
    private LocalDateTime dateTimeFrom;

    @JsonProperty("dateTimeTo")
    @NotNull
    private LocalDateTime dateTimeTo;

    @NotNull
    private Boolean allDayEvent;

    @JsonProperty("owner")
    private Long owner;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "task",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @SaveHistory(fields = {"name", "email"},
            message = "{" +
                    "\"participant.name\": \"{{name}}\"," +
                    "\"participant.email\": \"{{email}}\"" +
                    "}", separator = ", ")
    private Set<TaskParticipant> participants = new HashSet<>();

    @SaveHistoryIgnore
    private boolean notified;

    @NotNull
    @JsonProperty("notificationRequired")
    private boolean notificationRequired;

    @JsonProperty("notificationDateTime")
    private LocalDateTime notificationDateTime;

    @JsonProperty("description")
    private String description;

    @NotNull
    private String timeZone;

    @Transient
    @SaveHistoryIgnore
    private List<TaskAssociationDto> taskAssociations;

    @CreationTimestamp
    private LocalDateTime created;

    @PreRemove
    public void preRemove() {
        if (this.id != null) {
            RemoveCrmObject.addObjectToRemove((this.activityType == null) ? CrmObjectType.task : CrmObjectType.activity, this.id);
        }
    }
}
