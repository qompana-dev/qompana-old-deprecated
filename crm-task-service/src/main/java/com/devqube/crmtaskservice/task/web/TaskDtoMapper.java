package com.devqube.crmtaskservice.task.web;

import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CustomCrmObject;
import com.devqube.crmtaskservice.task.dto.TaskAssociationDetailDtoOut;
import com.devqube.crmtaskservice.task.dto.TaskParticipantDto;
import com.devqube.crmtaskservice.task.dto.TaskWithParticipantsDto;
import com.devqube.crmtaskservice.task.model.Task;
import com.devqube.crmtaskservice.task.model.TaskParticipant;

import java.util.Set;
import java.util.stream.Collectors;

public class TaskDtoMapper {

    public static Task toTask(TaskWithParticipantsDto dto) {
        if (dto == null) {
            return null;
        }

        Task task = new Task()
                .setSubject(dto.getSubject())
                .setPriority(dto.getPriority())
                .setState(dto.getState())
                .setCreator(dto.getCreator())
                .setPlace(dto.getPlace())
                .setLongitude(dto.getLongitude())
                .setLatitude(dto.getLatitude())
                .setActivityType(dto.getActivityType())
                .setDateTimeFrom(dto.getDateTimeFrom())
                .setDateTimeTo(dto.getDateTimeTo())
                .setAllDayEvent(dto.getAllDayEvent())
                .setOwner(dto.getOwner())
                .setNotified(dto.isNotified())
                .setNotificationRequired(dto.isNotificationRequired())
                .setNotificationDateTime(dto.getNotificationDateTime())
                .setDescription(dto.getDescription())
                .setTimeZone(dto.getTimeZone())
                .setTaskAssociations(dto.getTaskAssociations());

        task.setParticipants(toTaskParticipant(dto.getParticipants(), task));

        return task;
    }

    public static TaskWithParticipantsDto toTaskDto(Task task) {
        if (task == null) {
            return null;
        }

        return new TaskWithParticipantsDto()
                .setId(task.getId())
                .setSubject(task.getSubject())
                .setPriority(task.getPriority())
                .setState(task.getState())
                .setCreator(task.getCreator())
                .setPlace(task.getPlace())
                .setLongitude(task.getLongitude())
                .setLatitude(task.getLatitude())
                .setActivityType(task.getActivityType())
                .setDateTimeFrom(task.getDateTimeFrom())
                .setDateTimeTo(task.getDateTimeTo())
                .setAllDayEvent(task.getAllDayEvent())
                .setOwner(task.getOwner())
                .setNotified(task.isNotified())
                .setDescription(task.getDescription())
                .setTimeZone(task.getTimeZone())
                .setTaskAssociations(task.getTaskAssociations())
                .setParticipants(toTaskParticipantDto(task.getParticipants()));
    }

    public static TaskParticipant toTaskParticipant(TaskParticipantDto dto, Task task) {
        if (dto == null) {
            return null;
        }

        TaskParticipant taskParticipant = new TaskParticipant();
        taskParticipant.setEntityId(dto.getId());
        taskParticipant.setEntityOwnerId(dto.getOwnerAccountId());
        taskParticipant.setType(dto.getType());
        taskParticipant.setName(dto.getName());
        taskParticipant.setEmail(dto.getEmail());
        taskParticipant.setTask(task);

        return taskParticipant;
    }

    public static TaskParticipantDto toTaskParticipantDto(TaskParticipant taskParticipant) {
        if (taskParticipant == null) {
            return null;
        }

        TaskParticipantDto taskParticipantDto = new TaskParticipantDto();
        taskParticipantDto.setId(taskParticipant.getEntityId());
        taskParticipantDto.setType(taskParticipant.getType());
        taskParticipantDto.setOwnerAccountId(taskParticipant.getEntityOwnerId());
        taskParticipantDto.setName(taskParticipant.getName());
        taskParticipantDto.setEmail(taskParticipant.getEmail());

        return taskParticipantDto;
    }

    public static TaskAssociationDetailDtoOut toTaskAssociationDetailDtoOut(CrmObject crmObject) {
        if (crmObject == null) {
            return null;
        }

        return new TaskAssociationDetailDtoOut()
                .setId(crmObject.getId())
                .setLabel(crmObject.getLabel())
                .setType(crmObject.getType());
    }

    public static TaskAssociationDetailDtoOut toTaskAssociationDetailDtoOut(CustomCrmObject crmObject) {
        if (crmObject == null) {
            return null;
        }

        return new TaskAssociationDetailDtoOut()
                .setId(crmObject.getId())
                .setLabel(crmObject.getLabel())
                .setType(crmObject.getType())
                .setFirm(crmObject.getCustom());
    }

    public static Set<TaskParticipant> toTaskParticipant(Set<TaskParticipantDto> dtoSet, Task task) {
        return dtoSet.stream()
                .map(dto -> TaskDtoMapper.toTaskParticipant(dto, task))
                .collect(Collectors.toSet());
    }

    public static Set<TaskParticipantDto> toTaskParticipantDto(Set<TaskParticipant> participantSet) {
        return participantSet.stream()
                .map(TaskDtoMapper::toTaskParticipantDto)
                .collect(Collectors.toSet());
    }

}
