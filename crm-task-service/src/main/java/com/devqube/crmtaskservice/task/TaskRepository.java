package com.devqube.crmtaskservice.task;

import com.devqube.crmshared.task.ActivityTypeEnum;
import com.devqube.crmtaskservice.task.model.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface TaskRepository extends JpaRepository<Task, Long>, JpaSpecificationExecutor<Task> {
    List<Task> findAllByOwnerAndDateTimeFromIsAfterOrderByDateTimeFrom(Long owner, LocalDateTime after);

    List<Task> findAllByOwnerIn(Set<Long> owner);

    List<Task> findAllByCreatorAndOwnerNot(Long creator, Long owner);

//    List<Task> findAllByIntervalNotNullAndUnitNotNullAndNotifiedFalseAndNotificationTimeBefore(LocalDateTime now);

    @Query("SELECT count(t) FROM Task t where (t.owner = :userId or t.creator = :userId) and t.dateTimeFrom < :today and t.state <> com.devqube.crmtaskservice.task.model.TaskState.FINISHED and t.activityType is not null " +
            "and (:filterByIds = false or t.id in (:ids))")
    Long getOutstandingForUserCount(@Param("userId") Long userId, @Param("today") LocalDateTime today, @Param("filterByIds") Boolean filterByIds, @Param("ids") Set<Long> ids);

    @Query("SELECT t FROM Task t where (t.owner = :userId or t.creator = :userId) and t.dateTimeFrom >= :today " +
            "and (:filterByIds = false or t.id in (:ids))")
    Page<Task> getAllPlannedForUser(Pageable pageable, @Param("userId") Long userId, @Param("today") LocalDateTime today, @Param("filterByIds") Boolean filterByIds, @Param("ids") Set<Long> ids);

    @Query("SELECT t FROM Task t where (t.owner = :userId or t.creator = :userId) and t.dateTimeFrom < :today and t.state <> com.devqube.crmtaskservice.task.model.TaskState.FINISHED and t.activityType is not null " +
            "and (:filterByIds = false or t.id in (:ids))")
    Page<Task> getAllOutstandingForUser(Pageable pageable, @Param("userId") Long userId, @Param("today") LocalDateTime today, @Param("filterByIds") Boolean filterByIds, @Param("ids") Set<Long> ids);

    @Query("SELECT t FROM Task t where (t.owner = :userId or t.creator = :userId) and t.state = com.devqube.crmtaskservice.task.model.TaskState.IN_PROGRESS " +
            "and (:filterByIds = false or t.id in (:ids))")
    Page<Task> getAllInProgressForUser(Pageable pageable, @Param("userId") Long userId, @Param("filterByIds") Boolean filterByIds, @Param("ids") Set<Long> ids);

    @Query("SELECT t FROM Task t where (t.owner = :userId or t.creator = :userId) and t.state = com.devqube.crmtaskservice.task.model.TaskState.SUSPENDED " +
            "and (:filterByIds = false or t.id in (:ids))")
    Page<Task> getAllSuspendedForUser(Pageable pageable, @Param("userId") Long userId, @Param("filterByIds") Boolean filterByIds, @Param("ids") Set<Long> ids);

    @Query("SELECT t FROM Task t where (t.owner = :userId or t.creator = :userId) and t.state = com.devqube.crmtaskservice.task.model.TaskState.FINISHED " +
            "and (:filterByIds = false or t.id in (:ids))")
    Page<Task> getAllFinishedForUser(Pageable pageable, @Param("userId") Long userId, @Param("filterByIds") Boolean filterByIds, @Param("ids") Set<Long> ids);

    @Query("SELECT t FROM Task t where (t.owner = :userId or t.creator = :userId) " +
            "and (:filterByIds = false or t.id in (:ids))")
    Page<Task> findAllForUser(Pageable pageable, @Param("userId") Long userId, @Param("filterByIds") Boolean filterByIds, @Param("ids") Set<Long> ids);

    @Query("SELECT t FROM Task t WHERE LOWER(t.subject) LIKE LOWER(concat('%', ?1, '%')) and t.activityType is null")
    List<Task> findTaskByNameContaining(String pattern);

    @Query("SELECT t FROM Task t WHERE t.id in ?1 and t.activityType is null")
    List<Task> findTaskByIds(List<Long> ids);

    @Query("SELECT t FROM Task t WHERE LOWER(t.subject) LIKE LOWER(concat('%', ?1, '%')) and t.activityType is not null")
    List<Task> findActivityByNameContaining(String pattern);

    @Query("SELECT t FROM Task t WHERE t.id in ?1 and t.activityType is not null")
    List<Task> findActivityByIds(List<Long> ids);

    @Query("SELECT t FROM Task t WHERE t.id in ?1")
    List<Task> findAllTasksAndActivitiesByIds(List<Long> ids);

    Page<Task> findAllByActivityTypeIsNotNullAndIdIsInOrderByDateTimeFromDesc(List<Long> ids, Pageable pageable);

    @Query("SELECT t FROM Task t WHERE t.id in :taskIds ORDER BY t.dateTimeFrom ASC")
    Page<Task> findAllTasksByDateTimeFromAsc(@Param("taskIds") List<Long> taskIds, Pageable pageable);

    List<Task> findTop1ByDateTimeFromIsAfterAndIdIsInOrderByDateTimeFromDesc(LocalDateTime after, List<Long> ids);

    List<Task> findTop3ByDateTimeFromIsBeforeAndIdIsInOrderByDateTimeFromDesc(LocalDateTime before, List<Long> ids);

    List<Task> findTop3ByDateTimeFromIsBeforeAndDateTimeFromIsAfterAndIdIsInOrderByDateTimeFromDesc(LocalDateTime before, LocalDateTime after, List<Long> ids);

    @Query("SELECT count(t) FROM Task t WHERE t.owner in :userIds and t.dateTimeTo > :from and t.dateTimeTo < :to and" +
            " ((:onlyPhone = true and t.activityType = com.devqube.crmshared.task.ActivityTypeEnum.PHONE) or " +
            "(:onlyPhone = false and t.activityType <> com.devqube.crmshared.task.ActivityTypeEnum.PHONE))" +
            " and t.state = com.devqube.crmtaskservice.task.model.TaskState.FINISHED")
    Long getActivityCountForGoal(@Param("userIds") Set<Long> userIds, @Param("onlyPhone") Boolean onlyPhone, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to);

    @Query("SELECT t FROM Task t  WHERE t.owner <> :owner and t.activityType = :activityType " +
            "and t.dateTimeFrom > :fromDateTime and t.dateTimeFrom < :toDateTime " +
            "and t.latitude is not null and t.longitude is not null " +
            "and t.latitude > :minLat and t.latitude < :maxLat " +
            "and t.longitude > :minLon and t.longitude < :maxLon")
    List<Task> getNearbyTasks(@Param("owner") Long owner,
                              @Param("minLat") Double minLat, @Param("maxLat") Double maxLat,
                              @Param("minLon") Double minLon, @Param("maxLon") Double maxLon,
                              @Param("fromDateTime") LocalDateTime fromDateTime, @Param("toDateTime") LocalDateTime toDateTime,
                              @Param("activityType") ActivityTypeEnum activityType);

    @Query("SELECT t FROM Task t where (t.dateTimeFrom BETWEEN :fromDateTime AND :toDateTime) OR (t.dateTimeTo BETWEEN :fromDateTime AND :toDateTime)")
    List<Task> getCollidedTasks(@Param("fromDateTime") LocalDateTime fromDateTime, @Param("toDateTime") LocalDateTime toDateTime);

    @Query("SELECT t FROM Task t where ((t.dateTimeFrom BETWEEN :fromDateTime AND :toDateTime) OR (t.dateTimeTo BETWEEN :fromDateTime AND :toDateTime)) AND t.id <> :taskId")
    List<Task> getCollidedTasksOnUpdate(@Param("fromDateTime") LocalDateTime fromDateTime, @Param("toDateTime") LocalDateTime toDateTime, @Param("taskId") Long taskId);
}
