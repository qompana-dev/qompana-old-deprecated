package com.devqube.crmtaskservice.task.scheduler;

import com.devqube.crmtaskservice.task.model.Task;
import lombok.RequiredArgsConstructor;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class TaskNotificationSchedulerService {
    private static final Logger log = LoggerFactory.getLogger(TaskNotificationSchedulerService.class);

    private final Scheduler scheduler;

    public void scheduleTaskNotificationJob(Task task, Long receiverAccountId) {
        try {
            LocalDateTime fireTime = convertDateTimeWithZoneToLocalDateTime(task.getNotificationDateTime(), task.getTimeZone());
            JobDetail jobDetail = buildJobDetail(task, receiverAccountId);
            Trigger trigger = buildJobTrigger(jobDetail, fireTime);

            scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException ex) {
            log.error("Error scheduling notification job", ex);
        }
    }

    public Scheduler cancelNotificationJob(Long taskId) {
        JobKey key = new JobKey("Task_id_" + taskId + "_notification");
        try {
            scheduler.deleteJob(key);
        } catch (SchedulerException e) {
        }
        return scheduler;
    }

    private JobDetail buildJobDetail(Task task, Long receiverAccountId) {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("id", String.valueOf(task.getId()));
        jobDataMap.put("subject", task.getSubject());
        jobDataMap.put("place", task.getPlace());
        jobDataMap.put("dateTimeFrom", task.getDateTimeFrom().format(getRFC3339Formatter()));
        jobDataMap.put("dateTimeTo", task.getDateTimeTo().format(getRFC3339Formatter()));
        jobDataMap.put("notificationDateTime", task.getNotificationDateTime().format(getRFC3339Formatter()));
        jobDataMap.put("allDayEvent", String.valueOf(task.getAllDayEvent()));
        jobDataMap.put("owner", task.getOwner() != null ? String.valueOf(task.getOwner()) : null);
        jobDataMap.put("receiverAccountId", receiverAccountId);

        return JobBuilder.newJob(TaskNotificationJob.class)
                .withIdentity("Task_id_" + task.getId() + "_notification", "task-notification-jobs")
                .usingJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    private Trigger buildJobTrigger(JobDetail jobDetail, LocalDateTime fireTime) {
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName(), "task-notification-triggers")
                .startAt(Date.from(fireTime.atZone(ZoneId.systemDefault()).toInstant()))
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
                .build();
    }

    private DateTimeFormatter getRFC3339Formatter() {
        return DateTimeFormatter
                .ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .withZone(ZoneId.of("UTC"));
    }

    private LocalDateTime convertDateTimeWithZoneToLocalDateTime(LocalDateTime dateTimeToConvert, String timeZone) {
        ZonedDateTime zonedDateTimeToConvert = ZonedDateTime.of(dateTimeToConvert, ZoneId.of(timeZone));
        return zonedDateTimeToConvert.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
    }


}
