package com.devqube.crmtaskservice.task.model;

import java.time.temporal.ChronoUnit;

public enum TaskNotificationUnit {
    YEAR(ChronoUnit.YEARS, "y"),
    MONTH(ChronoUnit.MONTHS, "M"),
    WEEK(ChronoUnit.WEEKS, "w"),
    DAY(ChronoUnit.DAYS, "d"),
    HOUR(ChronoUnit.HOURS, "h"),
    MINUTE(ChronoUnit.MINUTES, "m"),
    SECOND(ChronoUnit.SECONDS, "s");

    private ChronoUnit chronoUnit;
    private String shortcut;

    TaskNotificationUnit(ChronoUnit chronoUnit, String shortcut) {
        this.chronoUnit = chronoUnit;
        this.shortcut = shortcut;
    }

    public ChronoUnit getChronoUnit() {
        return chronoUnit;
    }

    public String getShortcut() {
        return shortcut;
    }
}
