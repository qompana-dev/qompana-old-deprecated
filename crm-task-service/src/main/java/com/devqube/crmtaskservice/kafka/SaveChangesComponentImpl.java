package com.devqube.crmtaskservice.kafka;

import com.devqube.crmshared.history.SaveCrmChanges;
import com.devqube.crmshared.kafka.KafkaService;
import org.springframework.stereotype.Component;

@Component
public class SaveChangesComponentImpl extends SaveCrmChanges.SaveChangesComponent {
    public SaveChangesComponentImpl(KafkaService kafkaService) {
        super(kafkaService);
    }
}
