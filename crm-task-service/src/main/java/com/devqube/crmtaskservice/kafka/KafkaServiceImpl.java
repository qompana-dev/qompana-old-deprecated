package com.devqube.crmtaskservice.kafka;

import com.devqube.crmshared.kafka.AbstractKafkaService;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import org.springframework.stereotype.Service;

@Service
public class KafkaServiceImpl extends AbstractKafkaService {
    @Override
    public void receiverNotFound(KafkaMessage kafkaMessage) {

    }
}
