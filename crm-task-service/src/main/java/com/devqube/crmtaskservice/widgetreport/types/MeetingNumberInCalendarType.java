package com.devqube.crmtaskservice.widgetreport.types;

import com.devqube.crmshared.widgetreport.GetReportType;
import com.devqube.crmshared.widgetreport.GetWidgetReport;
import com.devqube.crmshared.widgetreport.WidgetReportConverter;
import com.devqube.crmshared.widgetreport.dto.ReportDto;
import com.devqube.crmshared.widgetreport.dto.UserObjMap;
import com.devqube.crmshared.widgetreport.model.WidgetColumnEnum;
import com.devqube.crmshared.widgetreport.model.WidgetReportTypeEnum;
import com.devqube.crmtaskservice.task.model.Task;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.*;

import static com.devqube.crmshared.task.ActivityTypeEnum.MEETING;

@Service
@GetReportType(type = WidgetReportTypeEnum.MEETING_NUMBER_IN_CALENDAR)
public class MeetingNumberInCalendarType implements GetWidgetReport {
    private final EntityManager em;

    public MeetingNumberInCalendarType(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<UserObjMap> getResultList(ReportDto reportDto) {
        return WidgetReportConverter.getMapUserObj(getResultMap(reportDto));
    }


    private Map<Long, Map<String, Double>> getResultMap(ReportDto widgetReport) {
        List<Object> distinctX = getDistinctX(widgetReport);
        Map<Long, Map<String, Double>> result = new HashMap<>(); // <userId, <x, result>>
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Task> from = cq.from(Task.class);
        CriteriaQuery<Long> select = cq.select(cb.count(from));

        for (Long widgetReportUserId : widgetReport.getUsers()) {
            result.put(widgetReportUserId, new HashMap<String, Double>());

            for (Object groupBy : distinctX) {
                Predicate where = cb.and(cb.equal(getGroupBy(widgetReport, from), groupBy),
                        getBasicWhere(widgetReport, cb, from, Collections.singletonList(widgetReportUserId)));

                Long countAll = em.createQuery(select.where(where)).getSingleResult();
                String x = Optional.ofNullable(groupBy).orElse("").toString();
                result.get(widgetReportUserId).put(x, countAll.doubleValue());
            }
        }
        return result;
    }

    private List<Object> getDistinctX(ReportDto widgetReport) {
        if (widgetReport.getWidgetColumn().equals(WidgetColumnEnum.OWNER)) {
            return new ArrayList<>(widgetReport.getUsers());
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Root<Task> from = cq.from(Task.class);
        CriteriaQuery<Object> distinct = cq.select(getGroupBy(widgetReport, from))
                .where(getBasicWhere(widgetReport, cb, from, new ArrayList<>(widgetReport.getUsers()))).distinct(true);
        return em.createQuery(distinct).getResultList();
    }

    private Path<Object> getGroupBy(ReportDto widgetReport, Root<?> from) {
        switch (widgetReport.getWidgetColumn()) {
            case PRIORITY:
                return from.get("priority");
            case TASK_STATE:
                return from.get("state");
            case OWNER:
                return from.get("owner");
            default:
                return null;
        }
    }

    private Predicate getBasicWhere(ReportDto widgetReport, CriteriaBuilder cb, Root<?> from, List<Long> userIds) {
        Predicate and = cb.and(cb.greaterThanOrEqualTo(from.get("dateTimeFrom"), widgetReport.getStart()),
                cb.equal(from.get("activityType"), MEETING),
                from.get("owner").in(userIds));
        return (widgetReport.getEnd() == null) ? and : cb.and(and, cb.lessThan(from.get("dateTimeFrom"), widgetReport.getEnd()));
    }
}
