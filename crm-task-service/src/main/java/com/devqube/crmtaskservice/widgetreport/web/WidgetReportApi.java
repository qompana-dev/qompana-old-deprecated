package com.devqube.crmtaskservice.widgetreport.web;

import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.widgetreport.dto.ReportDto;
import com.devqube.crmshared.widgetreport.dto.UserObjMap;
import com.devqube.crmshared.widgetreport.exception.CollectDataException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface WidgetReportApi {

    @ApiOperation(value = "Get report widget data for widgetReport")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Get widget data", response = UserObjMap[].class),
            @ApiResponse(code = 404, message = "implementation for widget type not found")
    })
    ResponseEntity<List<UserObjMap>> getReportWidgetData(@RequestBody ReportDto reportDto) throws EntityNotFoundException, CollectDataException;

}
