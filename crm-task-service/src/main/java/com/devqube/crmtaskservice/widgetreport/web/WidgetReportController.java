package com.devqube.crmtaskservice.widgetreport.web;

import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.widgetreport.dto.ReportDto;
import com.devqube.crmshared.widgetreport.dto.UserObjMap;
import com.devqube.crmshared.widgetreport.exception.CollectDataException;
import com.devqube.crmtaskservice.widgetreport.WidgetReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class WidgetReportController implements WidgetReportApi {
    private final WidgetReportService widgetReportService;

    public WidgetReportController(WidgetReportService widgetReportService) {
        this.widgetReportService = widgetReportService;
    }

    @Override
    @PostMapping("/internal/widget/report")
    public ResponseEntity<List<UserObjMap>> getReportWidgetData(@RequestBody ReportDto reportDto) throws EntityNotFoundException, CollectDataException {
        return ResponseEntity.ok(widgetReportService.getWidgetReport(reportDto));
    }
}
