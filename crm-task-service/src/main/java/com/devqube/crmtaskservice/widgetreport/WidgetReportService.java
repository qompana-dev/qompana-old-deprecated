package com.devqube.crmtaskservice.widgetreport;

import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.widgetreport.AbstractWidgetRunner;
import com.devqube.crmshared.widgetreport.GetWidgetReport;
import com.devqube.crmshared.widgetreport.dto.ReportDto;
import com.devqube.crmshared.widgetreport.dto.UserObjMap;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WidgetReportService extends AbstractWidgetRunner {

    public WidgetReportService(List<GetWidgetReport> types) {
        super(types);
    }

    @Override
    public List<UserObjMap> getWidgetReport(ReportDto widgetReport) throws EntityNotFoundException {
        return super.getWidgetReport(widgetReport);
    }
}
