package com.devqube.crmtaskservice;

import com.devqube.crmshared.license.LicenseCheck;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({LicenseCheck.class})
@EnableFeignClients(basePackages = {"com.devqube.crmtaskservice", "com.devqube.crmshared.association"})
public class CrmTaskServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrmTaskServiceApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
