package com.devqube.crmtaskservice.calendarShare;

import com.devqube.crmtaskservice.ApiUtil;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Validated
@Api(value = "calendar-share")
public interface CalendarShareApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Create calendar share", nickname = "createCalendarShare", notes = "Method used to share calendar with another user", response = CalendarShare.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "shared calendar successfully", response = CalendarShare.class),
            @ApiResponse(code = 400, message = "you already shared this calendar with this user", response = String.class)})
    @RequestMapping(value = "/calendar-share",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<CalendarShare> createCalendarShare(@ApiParam(value = "Create calendar share", required = true) @Valid @RequestBody CalendarShare calendarShare) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"sharingUserId\" : 6,  \"sharedToUserId\" : 1,  \"id\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "get calendar share", nickname = "getCalendarShares", notes = "Method used to get calendar share by sharing user and shared to user", response = CalendarShare.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "task", response = CalendarShare.class, responseContainer = "List")})
    @RequestMapping(value = "/calendar-share",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CalendarShare>> getCalendarShares(@ApiParam(value = "sharingUserId") @Valid @RequestParam(value = "sharingUserId", required = false) Long sharingUserId, @ApiParam(value = "sharedToUserId") @Valid @RequestParam(value = "sharedToUserId", required = false) Long sharedToUserId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType : MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"sharingUserId\" : 6,  \"sharedToUserId\" : 1,  \"id\" : 0}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @ApiOperation(value = "Delete calendar share", nickname = "removeCalendarShare", notes = "Method used to remove calendar share")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "calendar share removed")})
    @RequestMapping(value = "/calendar-share/{id}",
            method = RequestMethod.DELETE)
    default ResponseEntity<Void> removeCalendarShare(@ApiParam(value = "id", required = true) @PathVariable("id") Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
