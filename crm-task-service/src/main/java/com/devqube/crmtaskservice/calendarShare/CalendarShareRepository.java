package com.devqube.crmtaskservice.calendarShare;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CalendarShareRepository extends JpaRepository<CalendarShare, Long> {
    List<CalendarShare> findBySharingUserId(Long sharingUserId);
    List<CalendarShare> findBySharedToUserId(Long sharedToUserId);
    boolean existsBySharingUserIdAndSharedToUserId(Long idOfSharingUser, Long sharedToUserId);
}
