package com.devqube.crmtaskservice.calendarShare;

import com.devqube.crmshared.exception.BadRequestException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalendarShareService {

    private final CalendarShareRepository repository;

    public CalendarShareService(CalendarShareRepository repository) {
        this.repository = repository;
    }

    CalendarShare save(CalendarShare mapping) throws BadRequestException {
        if (repository.existsBySharingUserIdAndSharedToUserId(mapping.getSharingUserId(), mapping.getSharedToUserId())) {
           throw new BadRequestException("Mapping with this id of sharing user and shared to user id already exists");
        }
        return repository.save(mapping);
    }

    void deleteBySharedTasksMappingId(Long id) {
        repository.deleteById(id);
    }

    List<CalendarShare> findBySharingUserId(Long sharingUserIr) {
        return repository.findBySharingUserId(sharingUserIr);
    }

    List<CalendarShare> findBySharedToUserId(Long sharedToUserId) {
        return repository.findBySharedToUserId(sharedToUserId);
    }
}
