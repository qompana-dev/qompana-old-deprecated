package com.devqube.crmtaskservice.calendarShare;

import com.devqube.crmshared.exception.BadRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CalendarShareController implements CalendarShareApi {

    private final CalendarShareService calendarShareService;

    public CalendarShareController(CalendarShareService calendarShareService) {
        this.calendarShareService = calendarShareService;
    }

    @Override
    public ResponseEntity<CalendarShare> createCalendarShare(@Valid CalendarShare calendarShare) {
        try {
            return new ResponseEntity<>(calendarShareService.save(calendarShare), HttpStatus.CREATED);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<CalendarShare>> getCalendarShares(@Valid Long sharingUserId, @Valid Long sharedToUserId) {
        if (sharingUserId != null) {
            return new ResponseEntity<>(calendarShareService.findBySharingUserId(sharingUserId), HttpStatus.OK);
        }
        if (sharedToUserId != null) {
            return new ResponseEntity<>(calendarShareService.findBySharedToUserId(sharedToUserId), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<Void> removeCalendarShare(Long id) {
        this.calendarShareService.deleteBySharedTasksMappingId(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
