package com.devqube.crmtaskservice.calendarShare;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
public class CalendarShare {
    @Id
    @SequenceGenerator(name = "calendar_share_seq", sequenceName = "calendar_share_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "calendar_share_seq")
    private Long id;
    private Long sharingUserId;
    private Long sharedToUserId;
}
