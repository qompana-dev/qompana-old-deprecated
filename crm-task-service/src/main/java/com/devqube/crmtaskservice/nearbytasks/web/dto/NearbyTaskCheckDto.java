package com.devqube.crmtaskservice.nearbytasks.web.dto;

import com.devqube.crmshared.task.ActivityTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NearbyTaskCheckDto {
    @NotNull
    private ActivityTypeEnum activityType;
    @NotNull
    private Double latitude;
    @NotNull
    private Double longitude;
    @NotNull
    private LocalDateTime dateTimeFrom;
}
