package com.devqube.crmtaskservice.nearbytasks.web;

import com.devqube.crmshared.web.CurrentRequestUtil;
import com.devqube.crmtaskservice.nearbytasks.NearbyTasksService;
import com.devqube.crmtaskservice.nearbytasks.web.dto.NearbyTaskCheckDto;
import com.devqube.crmtaskservice.nearbytasks.web.dto.NearbyTaskDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class NearbyTaskController implements NearbyTaskApi {
    private final NearbyTasksService nearbyTasksService;

    public NearbyTaskController(NearbyTasksService nearbyTasksService) {
        this.nearbyTasksService = nearbyTasksService;
    }

    @Override
    public ResponseEntity<List<NearbyTaskDto>> getNearbyTasks(@Valid NearbyTaskCheckDto nearbyTaskCheckDto) {
        return ResponseEntity.ok(nearbyTasksService.getNearbyActivities(nearbyTaskCheckDto.getLongitude(), nearbyTaskCheckDto.getLatitude(), nearbyTaskCheckDto.getDateTimeFrom(), nearbyTaskCheckDto.getActivityType()));
    }
}
