package com.devqube.crmtaskservice.nearbytasks;

import com.devqube.crmshared.web.CurrentRequestUtil;
import com.devqube.crmtaskservice.integration.UserClient;
import com.devqube.crmtaskservice.integration.dto.GlobalConfigurationDto;
import com.devqube.crmtaskservice.nearbytasks.web.dto.GeoCoordinatesDTO;
import com.devqube.crmtaskservice.nearbytasks.web.dto.LocationDto;
import com.devqube.crmtaskservice.nearbytasks.web.dto.NearbyTaskDto;
import com.devqube.crmtaskservice.task.TaskRepository;
import com.devqube.crmshared.task.ActivityTypeEnum;
import com.devqube.crmtaskservice.task.model.Task;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class NearbyTasksService {
    public static final int EARTH_RADIUS = 6371;

    private final TaskRepository taskRepository;
    private final UserClient userClient;

    public NearbyTasksService(TaskRepository taskRepository, UserClient userClient) {
        this.taskRepository = taskRepository;
        this.userClient = userClient;
    }

    public List<NearbyTaskDto> getNearbyActivities(Double taskLongitude, Double taskLatitude, LocalDateTime taskDateTime, ActivityTypeEnum activityType) {
        Long accountId = userClient.getMyAccountId(CurrentRequestUtil.getLoggedInAccountEmail());
        LocationDto locationFromCompanyConfig = getLocationFromCompanyConfig();
        if (taskLongitude == null || taskLatitude == null || locationFromCompanyConfig == null || taskDateTime == null) {
            return new ArrayList<>();
        }

        if (distance(locationFromCompanyConfig.getLatitude(), locationFromCompanyConfig.getLongitude(), taskLatitude, taskLongitude) < getCityRadiusInKm().doubleValue()) {
            return new ArrayList<>();
        }
        GeoCoordinatesDTO gc = getRadiusAroundPoint(taskLatitude, taskLongitude, getCustomerRadiusInKm().doubleValue());

        Long timeDifferenceInD = getTimeDifferenceInD();
        LocalDateTime from = taskDateTime.minusDays(timeDifferenceInD);
        LocalDateTime to = taskDateTime.plusDays(timeDifferenceInD);

        List<Task> nearbyTasks = taskRepository.getNearbyTasks(accountId, gc.getMinLatitude(), gc.getMaxLatitude(), gc.getMinLongitude(), gc.getMaxLongitude(), from, to, activityType);
        return nearbyTasks.stream().map(c -> new NearbyTaskDto(c.getCreator(), c.getDateTimeFrom())).collect(Collectors.toList());
    }

    private Long getCustomerRadiusInKm() {
        return getLongConfigurationByKey("customerRadiusInKm");
    }

    private Long getCityRadiusInKm() {
        return getLongConfigurationByKey("cityRadiusInKm");
    }

    private Long getTimeDifferenceInD() {
        return getLongConfigurationByKey("timeDifferenceInD");
    }

    private LocationDto getLocationFromCompanyConfig() {
        String companyConfig = getConfigurationByKey("companyConfig");
        if (companyConfig == null) {
            return null;
        }
        try {
            JSONObject jsonObject = new JSONObject(companyConfig);
            Double longitude = Double.parseDouble(jsonObject.getString("longitude"));
            Double latitude = Double.parseDouble(jsonObject.getString("latitude"));
            return new LocationDto(latitude, longitude);
        } catch (Exception e) {
            log.info(e.getMessage());
            return null;
        }
    }

    private Long getLongConfigurationByKey(String key) {
        String value = getConfigurationByKey(key);
        if (value == null) {
            return 0L;
        }
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            log.info(e.getMessage());
            return 0L;
        }
    }

    private String getConfigurationByKey(String key) {
        try {
            GlobalConfigurationDto globalParamByKey = userClient.getGlobalParamByKey(key);
            if (globalParamByKey != null && globalParamByKey.getValue() != null) {
                return globalParamByKey.getValue();
            }
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return null;
    }

    public static GeoCoordinatesDTO getRadiusAroundPoint(Double lat, Double lon, Double radius) {
        if (lat == null || lon == null || radius == null) {
            return null;
        }

        GeoCoordinatesDTO geoCoordinatesDTO = new GeoCoordinatesDTO();

        double lonDiff = Math.toDegrees(radius / EARTH_RADIUS / Math.cos(Math.toRadians(lat)));
        geoCoordinatesDTO.setMinLongitude(lon - lonDiff);
        geoCoordinatesDTO.setMaxLongitude(lon + lonDiff);

        double latDiff = Math.toDegrees(radius / EARTH_RADIUS);
        geoCoordinatesDTO.setMaxLatitude(lat + latDiff);
        geoCoordinatesDTO.setMinLatitude(lat - latDiff);

        return geoCoordinatesDTO;
    }

    public static double distance(double startLat, double startLong, double endLat, double endLong) {
        double dLat = Math.toRadians((endLat - startLat));
        double dLong = Math.toRadians((endLong - startLong));

        startLat = Math.toRadians(startLat);
        endLat = Math.toRadians(endLat);

        double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return EARTH_RADIUS * c;
    }

    private static double haversin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }
}
