package com.devqube.crmtaskservice.nearbytasks.web.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NearbyTaskDto {
    private Long accountId;
    private LocalDateTime taskDate;
}
