package com.devqube.crmtaskservice.nearbytasks.web;

import com.devqube.crmtaskservice.ApiUtil;
import com.devqube.crmtaskservice.nearbytasks.web.dto.NearbyTaskCheckDto;
import com.devqube.crmtaskservice.nearbytasks.web.dto.NearbyTaskDto;
import com.devqube.crmtaskservice.task.model.Task;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
@Api(value = "tasks")
public interface NearbyTaskApi {

    @ApiOperation(value = "Get nearby task", nickname = "getNearbyTasks", notes = "Method used to get nearby task", response = Task.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "get nearby tasks", response = Task.class),
            @ApiResponse(code = 400, message = "validation error", response = String.class) })
    @RequestMapping(value = "/tasks/nearby",
            produces = { "application/json" },
            consumes = { "application/json" },
            method = RequestMethod.POST)
    ResponseEntity<List<NearbyTaskDto>> getNearbyTasks(@ApiParam(value = "Task object" ,required=true )  @Valid @RequestBody NearbyTaskCheckDto nearbyTaskCheckDto);
}
