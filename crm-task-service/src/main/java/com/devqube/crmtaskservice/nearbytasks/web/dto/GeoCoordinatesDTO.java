package com.devqube.crmtaskservice.nearbytasks.web.dto;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GeoCoordinatesDTO {
    private Double minLongitude;
    private Double maxLongitude;
    private Double minLatitude;
    private Double maxLatitude;
}
