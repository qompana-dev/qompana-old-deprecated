package com.devqube.crmtaskservice.integration;

import com.devqube.crmshared.search.CrmObject;
import com.devqube.crmshared.search.CustomCrmObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "crm-business-service", url = "${crm-business-service.url}")
public interface BusinessClient {

    @RequestMapping(value = "/leads/custom-crm-object", consumes = {"application/json"}, method = RequestMethod.GET)
    List<CustomCrmObject> getLeadsAsCustomCrmObject(@RequestParam List<Long> ids);

    @RequestMapping(value = "/opportunity/custom-crm-object", consumes = {"application/json"}, method = RequestMethod.GET)
    List<CustomCrmObject> getOpportunitiesAsCustomCrmObject(@RequestParam List<Long> ids);

    @RequestMapping(value = "/customers/crm-object", consumes = {"application/json"}, method = RequestMethod.GET)
    List<CrmObject> getCustomersAsCrmObject(@RequestParam List<Long> ids);

    @RequestMapping(value = "/contacts/crm-object", consumes = {"application/json"}, method = RequestMethod.GET)
    List<CrmObject> getContactsAsCrmObject(@RequestParam List<Long> ids);

}
