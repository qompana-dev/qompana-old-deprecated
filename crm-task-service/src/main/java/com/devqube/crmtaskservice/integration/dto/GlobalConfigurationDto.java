package com.devqube.crmtaskservice.integration.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GlobalConfigurationDto {
    private String key;
    private String value;
}
