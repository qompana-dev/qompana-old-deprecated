package com.devqube.crmtaskservice.integration;

import com.devqube.crmshared.access.model.ProfileDto;
import com.devqube.crmtaskservice.integration.dto.GlobalConfigurationDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Set;

@FeignClient(name = "crm-user-service", url = "${crm-user-service.url}")
public interface UserClient {
    @RequestMapping(value = "/role/name/{roleName}/child/accounts/id", method = RequestMethod.GET)
    Set<Long> getAccountIdsByRole(@PathVariable("roleName") String roleName);

    @RequestMapping(value = "/profile/basic/mine", consumes = { "application/json" }, method = RequestMethod.GET)
    ProfileDto getBasicProfile(@RequestHeader("Logged-Account-Email") String email);

    @RequestMapping(value = "/accounts/me/id", consumes = { "application/json" }, method = RequestMethod.GET)
    Long getMyAccountId(@RequestHeader("Logged-Account-Email") String email);

    @RequestMapping(value = "/configuration/{key}", produces = {"application/json"}, method = RequestMethod.GET)
    GlobalConfigurationDto getGlobalParamByKey(@PathVariable(value = "key") String key) ;
}
