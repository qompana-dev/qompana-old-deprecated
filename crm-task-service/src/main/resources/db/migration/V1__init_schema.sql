create table task
(
    id                 bigint primary key,
    subject            varchar(200) not null,
    priority           int
        constraint task_priority_in check ((priority >= 0 and priority <= 3 and activity_type is not null) or
                                           priority is null),
    state              int
        constraint task_state_in check ((state >= 0 and state <= 3 and activity_type is not null) or state is null),
    creator            bigint       not null,
    place              varchar(200),
    longitude          double precision,
    latitude           double precision,
    activity_type      int
        constraint activity_type check (activity_type >= 0 and activity_type <= 3),
    date_time_from     timestamp,
    date_time_to       timestamp,
    all_day_event      boolean      not null,
    notified           boolean default false,
    email_notification boolean default false,
    owner              bigint,
    notification_time  timestamp,
    interval           bigint,
    unit               int,
    description        varchar(1000)
);

comment on column task.priority is '0 - low; 1 - medium; 2 - high; 3 - urgent';
comment on column task.state is '0 - created; 1 - in progress; 2 - suspended; 3 - finished';
comment on column task.unit is '0 - YEAR; 1 - MONTH; 2 - WEEK; 3 - DAY; 4 - HOUR; 5 - MINUTE; 6 - SECOND;';
comment on column task.activity_type is '0 - PHONE; 1 - MEETING; 2 - MAIL; 3 - TASK;';

create sequence task_seq start 1;

create table calendar_share
(
    id                bigint primary key,
    sharing_user_id   bigint not null,
    shared_to_user_id bigint not null,
    UNIQUE (sharing_user_id, shared_to_user_id)
);

create sequence calendar_share_seq start 1;
