ALTER TABLE task ADD time_zone varchar(128);
update task set time_zone = 'Europe/Warsaw';
alter table task alter column time_zone set not null;
