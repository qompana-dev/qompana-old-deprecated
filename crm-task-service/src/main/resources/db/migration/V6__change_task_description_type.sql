alter table task
    alter column description type text,
    drop constraint task_priority_in,
    add constraint task_priority_in check (priority >= 0 and priority <= 3 ),
    alter column priority set default 1;

update task
set priority = 3
where priority = 1;

update task
set priority = 1
where priority = 0
   or priority is null;

comment on column task.priority is '0 - low; 1 - medium, 2 - high, 3 - urgent';