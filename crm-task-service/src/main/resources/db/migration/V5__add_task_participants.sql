create sequence task_participant_seq start 1;

create table task_participant
(
    id              bigint primary key,
    entity_id       bigint not null,
    entity_owner_id bigint,
    type            text   not null,
    name            text,
    email           text,
    task_id         bigint not null,
    CONSTRAINT task_participant_task_fk FOREIGN KEY (task_id)
        REFERENCES task (id) ON delete CASCADE
);