ALTER TABLE task ADD created timestamp;
update task as tc set created = (select max(t.date_time_from) from task as t where t.id = tc.id);
alter table task alter column created set not null;
