update task
set priority = 0
where priority = 0
   or priority = 1;
update task
set priority = 1
where priority = 2
   or priority = 3;

comment on column task.priority is '0 - medium; 1 - urgent';
