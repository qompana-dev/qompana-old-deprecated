cd ..

REM wsl rm -Rf kafka_2.12-2.2.0 && wsl wget http://ftp.man.poznan.pl/apache/kafka/2.2.0/kafka_2.12-2.2.0.tgz && wsl tar -xzf kafka_2.12-2.2.0.tgz && wsl rm -f kafka_2.12-2.2.0.tgz && wsl cd kafka_2.12-2.2.0

cd kafka_2.12-2.2.0

echo "start zookeeper"
start "zookeeper" cmd.exe @cmd /k "C:\kafka_2.12-2.2.0\bin\windows\zookeeper-server-start.bat C:\kafka_2.12-2.2.0\config\zookeeper.properties"

timeout 10
echo "start kafka"
start "kafka" cmd.exe @cmd /k "C:\kafka_2.12-2.2.0\bin\windows\kafka-server-start.bat C:\kafka_2.12-2.2.0\config\server.properties"


timeout 20
start "kafka-crm-user-service" cmd.exe @cmd /k "C:\kafka_2.12-2.2.0\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic kafka-crm-user-service"
start "kafka-crm-notification-service" cmd.exe @cmd /k "C:\kafka_2.12-2.2.0\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic kafka-crm-notification-service"
start "kafka-crm-business-service" cmd.exe @cmd /k "C:\kafka_2.12-2.2.0\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic kafka-crm-business-service"
start "kafka-crm-calendar-service" cmd.exe @cmd /k "C:\kafka_2.12-2.2.0\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic kafka-crm-calendar-service"
start "kafka-crm-gateway-service" cmd.exe @cmd /k "C:\kafka_2.12-2.2.0\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic kafka-crm-gateway-service"
start "kafka-crm-association-service" cmd.exe @cmd /k "C:\kafka_2.12-2.2.0\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic kafka-crm-association-service"
start "kafka-crm-history-service" cmd.exe @cmd /k "C:\kafka_2.12-2.2.0\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic kafka-crm-history-service"
