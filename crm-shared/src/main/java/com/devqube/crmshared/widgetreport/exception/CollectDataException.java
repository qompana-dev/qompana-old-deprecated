package com.devqube.crmshared.widgetreport.exception;

public class CollectDataException extends Exception {
    public CollectDataException() {
    }

    public CollectDataException(String message) {
        super(message);
    }

    public CollectDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public CollectDataException(Throwable cause) {
        super(cause);
    }

    public CollectDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
