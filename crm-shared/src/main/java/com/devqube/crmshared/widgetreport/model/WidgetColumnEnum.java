package com.devqube.crmshared.widgetreport.model;

public enum WidgetColumnEnum {
    CUSTOMER,
    CONTACT,
    OPPORTUNITY_PRIORITY,
    TASK,
    CURRENCY,
    FINISH_RESULT,
    REJECTION_RESULT,
    OWNER,
    COMPANY,
    LEAD_PRIORITY,
    PRIORITY,
    TASK_STATE,
    ACTIVITY_TYPE
}
