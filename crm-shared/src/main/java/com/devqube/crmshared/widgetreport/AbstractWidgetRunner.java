package com.devqube.crmshared.widgetreport;

import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.widgetreport.dto.ReportDto;
import com.devqube.crmshared.widgetreport.dto.UserObjMap;
import com.devqube.crmshared.widgetreport.model.WidgetReportTypeEnum;

import java.util.List;

public abstract class AbstractWidgetRunner {
    private final List<GetWidgetReport> types;

    public AbstractWidgetRunner(List<GetWidgetReport> types) {
        this.types = types;
    }

    private GetWidgetReport getWidgetReportData(WidgetReportTypeEnum reportType) throws EntityNotFoundException {
        return types.stream()
                .filter(c -> c.getClass().getAnnotation(GetReportType.class) != null && c.getClass().getAnnotation(GetReportType.class).type().equals(reportType))
                .findFirst().orElseThrow(EntityNotFoundException::new);
    }

    protected List<UserObjMap> getWidgetReport (ReportDto widgetReport) throws EntityNotFoundException {
        return getWidgetReportData(widgetReport.getReportType()).getResultList(widgetReport);
    }
}
