package com.devqube.crmshared.widgetreport.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.devqube.crmshared.widgetreport.model.WidgetColumnEnum.*;

public enum WidgetReportTypeEnum {
    CONVERSION_LEAD_FACTOR(Arrays.asList(COMPANY, LEAD_PRIORITY, OWNER)),
    OPPORTUNITIES_AMOUNT_ACCORDING_TO_OPPORTUNITY_STEP(Collections.singletonList(TASK)),
    WON_OPPORTUNITIES_NUMBER(Arrays.asList(CUSTOMER, CONTACT, OPPORTUNITY_PRIORITY, CURRENCY, OWNER)),
    WON_OPPORTUNITIES_AMOUNT(Arrays.asList(CUSTOMER, CONTACT, OPPORTUNITY_PRIORITY, CURRENCY, OWNER)),
    WON_OPPORTUNITIES_NUMBER_CONVERTED_FROM_LEAD(Arrays.asList(CUSTOMER, CONTACT, OPPORTUNITY_PRIORITY, CURRENCY, OWNER)),
    LOSS_OPPORTUNITIES_NUMBER(Arrays.asList(CUSTOMER, CONTACT, OPPORTUNITY_PRIORITY, CURRENCY, REJECTION_RESULT, OWNER)),
    LOSS_OPPORTUNITIES_AMOUNT(Arrays.asList(CUSTOMER, CONTACT, OPPORTUNITY_PRIORITY, CURRENCY, REJECTION_RESULT, OWNER)),
    MEETING_NUMBER_IN_CALENDAR(Arrays.asList(PRIORITY, TASK_STATE, OWNER));

    private List<WidgetColumnEnum> columns;

    WidgetReportTypeEnum(List<WidgetColumnEnum> columns) {
        this.columns = columns;
    }

    public List<WidgetColumnEnum> getColumns() {
        return columns;
    }
}
