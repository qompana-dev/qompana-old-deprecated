package com.devqube.crmshared.widgetreport;

import com.devqube.crmshared.widgetreport.dto.ReportDto;
import com.devqube.crmshared.widgetreport.dto.UserObjMap;

import java.util.List;

public interface GetWidgetReport {
    public List<UserObjMap> getResultList (ReportDto widgetReport);
}
