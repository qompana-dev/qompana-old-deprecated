package com.devqube.crmshared.widgetreport;

import com.devqube.crmshared.widgetreport.dto.UserObjMap;
import com.devqube.crmshared.widgetreport.dto.XObjMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WidgetReportConverter {
    public static List<UserObjMap> getMapUserObj(Map<Long, Map<String, Double>> map) {
        List<UserObjMap> result = new ArrayList<>();
        if (map == null) {
            return result;
        }
        return map.keySet().stream().map(c -> new UserObjMap(c, getMapXObj(map.get(c)))).collect(Collectors.toList());
    }

    private static List<XObjMap> getMapXObj(Map<String, Double> map) {
        if (map == null) {
            return new ArrayList<>();
        }
        return map.keySet().stream().map(c -> new XObjMap(c, map.get(c))).collect(Collectors.toList());
    }

    public static Map<Long, Map<String, Double>> getMap(List<UserObjMap> userObjMapList) {
        Map<Long, Map<String, Double>> result = new HashMap<>();
        for (UserObjMap userObjMap : userObjMapList) {
            Map<String, Double> xObj = new HashMap<>();
            userObjMap.getXObjList().forEach(c -> xObj.put(c.getX(), c.getValue()));
            result.put(userObjMap.getUserId(), xObj);
        }
        return result;
    }
}
