package com.devqube.crmshared.widgetreport.dto;

import com.devqube.crmshared.widgetreport.model.WidgetColumnEnum;
import com.devqube.crmshared.widgetreport.model.WidgetReportTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReportDto {
    @NotNull
    private WidgetReportTypeEnum reportType;
    @NotNull
    private WidgetColumnEnum widgetColumn;

    @NotNull
    private LocalDateTime start;
    private LocalDateTime end;

    private List<Long> users;

    private String additionalValue;
}
