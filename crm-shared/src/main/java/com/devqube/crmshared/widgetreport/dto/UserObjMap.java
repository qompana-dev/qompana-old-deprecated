package com.devqube.crmshared.widgetreport.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserObjMap {
    private Long userId;
    private List<XObjMap> xObjList;
}
