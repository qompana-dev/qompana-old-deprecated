package com.devqube.crmshared.kafka;

public class ServiceKafkaAddr {
    public final static String CRM_USER_SERVICE = "kafka-crm-user-service";
    public final static String CRM_BUSINESS_SERVICE = "kafka-crm-business-service";
    public final static String CRM_NOTIFICATION_SERVICE = "kafka-crm-notification-service";
    public final static String CRM_CALENDAR_SERVICE = "kafka-crm-calendar-service";
    public final static String CRM_GATEWAY_SERVICE = "kafka-crm-gateway-service";
    public final static String CRM_ASSOCIATION_SERVICE = "kafka-crm-association-service";
    public final static String CRM_HISTORY_SERVICE = "kafka-crm-history-service";
}
