package com.devqube.crmshared.kafka.util;

import com.devqube.crmshared.kafka.dto.KafkaMessageData;

import java.util.HashSet;
import java.util.Set;

public class KafkaMessageDataBuilder {
    private Set<KafkaMessageData> data = new HashSet<>();

    public KafkaMessageDataBuilder add(String key, String value) {
        data.add(new KafkaMessageData(key, value));
        return this;
    }

    public static KafkaMessageDataBuilder builder() {
        return new KafkaMessageDataBuilder();
    }

    public Set<KafkaMessageData> build() {
        return data;
    }
}
