package com.devqube.crmshared.kafka;

import com.devqube.crmshared.exception.IncorrectNotificationTypeException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.history.dto.ObjectHistoryType;
import com.devqube.crmshared.kafka.dto.KafkaMessageData;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.notification.NotificationMsgType;
import com.devqube.crmshared.search.CrmObjectType;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

/**
 *** Expected implementation in in client
 *
 * @Slf4j
 * @Service
 * public class KafkaServiceImpl extends AbstractKafkaService {
 *     @KafkaListener(topics = ServiceKafkaAddr.CRM_CUSTOMER_SERVICE)
 *     public void processMessage(String content) {
 *         super.processMessage(content);
 *     }
 *
 *     @KafkaReceiver(from = KafkaMsgType.CUSTOMER_TEST)
 *     public void test(KafkaMessage kafkaMessage) {
 *         log.info(messageDto.getDestination().name());
 *         messageDto.getValueByKey("myKey").ifPresent(log::info);
 *     }
 *
 *     @KafkaReceiver( ... )
 *     public void ...(KafkaMessage kafkaMessage) {
 *         ...
 *     }
 *
 *     @Override
 *     public void receiverNotFound(KafkaMessage kafkaMessage) {
 *         log.info("receiverNotFound" + messageDto.getDestination().name());
 *     }
 * }
 *
 *
 *** sender
 *
 * @Autowired
 * private KafkaService kafkaService;
 *
 * void test() {
 *      kafkaService.send(KafkaMsgType.CUSTOMER_TEST, KafkaMessageDataBuilder.builder()
 *             .add("myKey1", "myValue1")
 *             .add("myKey2", "myValue2").build());
 * }
 *
 *** new value
 *
 * new value should be added to com.devqube.crmshared.kafka.type.KafkaMsgType
 * ex. CUSTOMER_TEST(ServiceType.CRM_CUSTOMER_SERVICE), // {serviceName}_{key}(ServiceType)
 *
 * new service should be added to com.devqube.crmshared.kafka.type.ServiceType
 * ex. CRM_CUSTOMER_SERVICE(ServiceKafkaAddr.CRM_CUSTOMER_SERVICE), {serviceName}(topicAddr)
 *      topic address define in com.devqube.crmshared.kafka.ServiceKafkaAddr as final static String and add to crm-shared\kafka_start.cmd
 *
 * run crm-shared\kafka_start.cmd to start kafka server with topics
 * (uncomment 'REM wsl rm ...' to download kafka server)
 *
 *
 *** properties
 *
 *
 * to define kafka address add to properties
 *
 * spring:
 *   kafka:
 *     bootstrap-servers: localhost:9092
 *     consumer:
 *       group-id: myGroup
 *
 * or
 *
 * spring.kafka.bootstrap-servers=localhost:9092
 * spring.kafka.consumer.group-id=myGroup
 *
 */
public interface KafkaService {
    public void send(@NotNull Long accountId, @NotNull KafkaMsgType kafkaMsgType, Set<KafkaMessageData> data) throws KafkaSendMessageException;
    public void send(KafkaMsgType kafkaMsgType, String content) throws KafkaSendMessageException;
    public void addNotification(@NotNull Long accountId, @NotNull NotificationMsgType notificationMsgType, Set<KafkaMessageData> data) throws KafkaSendMessageException;
    public void addGroupNotification(@NotNull Set<Long> accountIds, @NotNull NotificationMsgType notificationMsgType, Set<KafkaMessageData> data) throws KafkaSendMessageException, IncorrectNotificationTypeException;

    public void sendWebsocket(String destination, String payload) throws KafkaSendMessageException;
    public void sendRemoveAssociation(CrmObjectType type, Long id) throws KafkaSendMessageException;
    public void saveHistory(ObjectHistoryType type, String id, String json, LocalDateTime modifiedTime, String email) throws KafkaSendMessageException;
}
