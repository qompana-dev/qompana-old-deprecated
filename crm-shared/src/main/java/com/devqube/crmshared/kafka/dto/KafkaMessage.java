package com.devqube.crmshared.kafka.dto;

import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KafkaMessage {
    private Long accountId;
    private KafkaMsgType destination;
    private Set<KafkaMessageData> data;

    public Optional<String> getValueByKey(String key) {
        if (data == null) {
            return Optional.empty();
        }
        return data.stream()
                .filter(c -> c.getKey().equals(key))
                .map(KafkaMessageData::getValue)
                .map(Strings::nullToEmpty)
                .findFirst();
    }

    public Optional<Map<String, String>> getValuesMap(String... keys) {
        Map<String, String> result = new HashMap<>();
        if (keys == null || keys.length == 0) {
            return Optional.empty();
        }
        List<String> keyList = Arrays.asList(keys);

        data.stream()
                .filter(c -> keyList.contains(c.getKey()))
                .forEach(c -> result.put(c.getKey(), c.getValue()));

        if (keys.length != result.keySet().size()) {
            return Optional.empty();
        }

        return Optional.of(result);
    }
}
