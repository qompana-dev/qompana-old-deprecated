package com.devqube.crmshared.kafka.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KafkaMessageData {
    private String key;
    private String value;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KafkaMessageData kafkaMessageData = (KafkaMessageData) o;
        return Objects.equals(key, kafkaMessageData.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}
