package com.devqube.crmshared.kafka.type;

import com.devqube.crmshared.kafka.ServiceKafkaAddr;

public enum ServiceType {
    CRM_USER_SERVICE(ServiceKafkaAddr.CRM_USER_SERVICE),
    CRM_ASSOCIATION_SERVICE(ServiceKafkaAddr.CRM_ASSOCIATION_SERVICE),
    CRM_HISTORY_SERVICE(ServiceKafkaAddr.CRM_HISTORY_SERVICE),
    CRM_BUSINESS_SERVICE(ServiceKafkaAddr.CRM_BUSINESS_SERVICE),
    CRM_NOTIFICATION_SERVICE(ServiceKafkaAddr.CRM_NOTIFICATION_SERVICE),
    CRM_CALENDAR_SERVICE(ServiceKafkaAddr.CRM_CALENDAR_SERVICE),
    CRM_GATEWAY_SERVICE(ServiceKafkaAddr.CRM_GATEWAY_SERVICE);

    private String addr;

    ServiceType(String addr) {
        this.addr = addr;
    }

    public String getAddr() {
        return addr;
    }
}
