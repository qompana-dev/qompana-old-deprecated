package com.devqube.crmshared.kafka;

import com.devqube.crmshared.exception.IncorrectNotificationTypeException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.history.dto.ObjectHistoryType;
import com.devqube.crmshared.kafka.annotation.KafkaReceiver;
import com.devqube.crmshared.kafka.dto.KafkaMessage;
import com.devqube.crmshared.kafka.dto.KafkaMessageData;
import com.devqube.crmshared.kafka.type.KafkaMsgType;
import com.devqube.crmshared.kafka.util.KafkaMessageDataBuilder;
import com.devqube.crmshared.kafka.util.KafkaUtil;
import com.devqube.crmshared.notification.NotificationMsgType;
import com.devqube.crmshared.search.CrmObjectType;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.kafka.core.KafkaTemplate;

import javax.validation.constraints.NotNull;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.*;

@Slf4j
public abstract class AbstractKafkaService implements KafkaService, ApplicationListener<ApplicationReadyEvent> {
    @Autowired
    private KafkaTemplate kafkaTemplate;

    public boolean isKafkaIgnored() {
        return false;
    }

    public void processMessage(String content) {
        KafkaUtil.getKafkaObject(content).ifPresent(c -> executeKafkaReceiverMethod(content, c));
    }

    private void executeKafkaReceiverMethod(String content, KafkaMessage kafkaMessage) {
        List<Method> allMethods = new ArrayList<>(Arrays.asList(this.getClass().getDeclaredMethods()));
        for (final Method method : allMethods) {
            if (method.isAnnotationPresent(KafkaReceiver.class)) {
                KafkaReceiver annotation = method.getAnnotation(KafkaReceiver.class);
                if (annotation.from().equals(kafkaMessage.getDestination()) && methodValid(method)) {
                    try {
                        method.invoke(this, kafkaMessage);
                    } catch (Exception e) {
                        log.info(e.getMessage());
                        sendErrorMessage(kafkaMessage, content);
                    }
                    return;
                }
            }
        }

        receiverNotFound(kafkaMessage);
    }

    private void sendErrorMessage(KafkaMessage kafkaMessage, String content) {
        if (kafkaMessage.getDestination().getErrorMsg() != null) {
            try {
                this.send(kafkaMessage.getAccountId(), KafkaMsgType.NOTIFICATION_KAFKA_ERROR, KafkaMessageDataBuilder.builder()
                        .add("notificationMsgType", kafkaMessage.getDestination().getErrorMsg().name())
                        .add("kafkaErrorType", kafkaMessage.getDestination().name())
                        .add("kafkaErrorMsg", content).build());
            } catch (KafkaSendMessageException e) {
                log.error(e.getMessage());
            }
        }
    }

    public abstract void receiverNotFound(KafkaMessage kafkaMessage);

    public void send(KafkaMsgType kafkaMsgType, String content) throws KafkaSendMessageException {
        if (!isKafkaIgnored()) {
            try {
                kafkaTemplate.send(kafkaMsgType.getServiceType().getAddr(), content);
            } catch (Exception e) {
                log.error(e.getMessage());
                throw new KafkaSendMessageException();
            }
        }
    }

    public void send(Long accountId, KafkaMsgType kafkaMsgType, Set<KafkaMessageData> data) throws KafkaSendMessageException {
        if (!isKafkaIgnored()) {
            try {
                String value = KafkaUtil.getStringFromKafkaMessage(new KafkaMessage(accountId, kafkaMsgType, data));
                kafkaTemplate.send(kafkaMsgType.getServiceType().getAddr(), value);
            } catch (Exception e) {
                log.error(e.getMessage());
                throw new KafkaSendMessageException();
            }
        }
    }

    @Override
    public void sendRemoveAssociation(CrmObjectType type, Long id) throws KafkaSendMessageException {
        Set<KafkaMessageData> dataToSend = new HashSet<>();
        dataToSend.add(new KafkaMessageData("type", type.name()));
        dataToSend.add(new KafkaMessageData("id", id.toString()));
        send(null, KafkaMsgType.ASSOCIATION_REMOVE_ASSOCIATION, dataToSend);
    }

    @Override
    public void saveHistory(ObjectHistoryType type, String id, String json, LocalDateTime modifiedTime, String email) throws KafkaSendMessageException {
        Set<KafkaMessageData> dataToSend = new HashSet<>();
        dataToSend.add(new KafkaMessageData("classRef", type.getClassRef()));
        dataToSend.add(new KafkaMessageData("id", id));
        dataToSend.add(new KafkaMessageData("json", json));
        dataToSend.add(new KafkaMessageData("modifiedTime", modifiedTime.toString()));
        dataToSend.add(new KafkaMessageData("email", email));
        send(null, KafkaMsgType.HISTORY_SAVE_CHANGES, dataToSend);
    }

    @Override
    public void addGroupNotification(Set<Long> accountIds, @NotNull NotificationMsgType notificationMsgType, Set<KafkaMessageData> data) throws KafkaSendMessageException, IncorrectNotificationTypeException {
        if (!notificationMsgType.isGroupNotification()) {
            throw new IncorrectNotificationTypeException("notification type must be a group notification");
        }
        for (Long accountId : accountIds) {
            addNotification(accountId, notificationMsgType, data);
        }
    }

    @Override
    public void addNotification(Long accountId, NotificationMsgType notificationMsgType, Set<KafkaMessageData> data) throws KafkaSendMessageException {
        Set<KafkaMessageData> dataToSend = new HashSet<>();
        dataToSend.add(new KafkaMessageData("notificationMsgType", notificationMsgType.name()));
        dataToSend.addAll(data);
        send(accountId, KafkaMsgType.NOTIFICATION_ADD_NOTIFICATION, dataToSend);
    }

    @Override
    public void sendWebsocket(String destination, String payload) throws KafkaSendMessageException {
        Set<KafkaMessageData> dataToSend = new HashSet<>();
        dataToSend.add(new KafkaMessageData("destination", destination));
        dataToSend.add(new KafkaMessageData("payload", payload));
        send(null, KafkaMsgType.SEND_WEBSOCKET_MESSAGE, dataToSend);
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        Set<Method> methodsAnnotatedWith = new Reflections("com.devqube", new MethodAnnotationsScanner()).getMethodsAnnotatedWith(KafkaReceiver.class);

        for (Method method : methodsAnnotatedWith) {
            methodValid(method);
        }
    }

    private boolean methodValid(Method method) {
        String expectedArgType = "com.devqube.crmshared.kafka.dto.KafkaMessage";

        Type[] genericParameterTypes = method.getGenericParameterTypes();
        boolean returnVoid = method.getReturnType().getName().equals("void");
        boolean argsLength = genericParameterTypes.length == 1;

        String methodPath = method.getDeclaringClass() + "." + method.getName() + "(...)";
        boolean valid = true;
        if (!argsLength) {
            System.err.println("Incorrect args length in method " + methodPath);
            valid = false;
        } else {
            boolean argType = genericParameterTypes[0].getTypeName().equals(expectedArgType);
            if (!returnVoid) {
                System.err.println(methodPath + " should return void type");
                valid = false;
            }
            if (!argType) {
                System.err.println("incorrect arg type in method " + methodPath + ". Expected 'KafkaMessage kafkaMessage'");
                valid = false;
            }
        }
        return valid;
    }
}
