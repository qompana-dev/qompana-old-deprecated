package com.devqube.crmshared.kafka.type;

import com.devqube.crmshared.notification.NotificationMsgType;

import static com.devqube.crmshared.notification.NotificationMsgType.*;

public enum KafkaMsgType {
    //crm-notification
    NOTIFICATION_KAFKA_ERROR(ServiceType.CRM_NOTIFICATION_SERVICE, KAFKA_GENERIC_ERROR),
    NOTIFICATION_ADD_NOTIFICATION(ServiceType.CRM_NOTIFICATION_SERVICE, KAFKA_ADD_NOTIFICATION_ERROR),

    NOTIFICATION_WS_USER_NOTIFICATION_CHANGE(ServiceType.CRM_NOTIFICATION_SERVICE, KAFKA_SEND_ACTIVATION_MAIL_ERROR),
    NOTIFICATION_SEND_ACTIVATION_MAIL(ServiceType.CRM_NOTIFICATION_SERVICE, KAFKA_SEND_ACTIVATION_MAIL_ERROR),
    NOTIFICATION_SEND_EVENT_NOTIFICATION_MAIL(ServiceType.CRM_NOTIFICATION_SERVICE, KAFKA_SEND_EVENT_NOTIFICATION_MAIL_ERROR),
    NOTIFICATION_RESENT_ACTIVATION_MAIL(ServiceType.CRM_NOTIFICATION_SERVICE, KAFKA_RESENT_ACTIVATION_MAIL_ERROR),
    NOTIFICATION_SEND_RESET_PASSWORD_MAIL(ServiceType.CRM_NOTIFICATION_SERVICE, KAFKA_SEND_RESET_PASSWORD_MAIL_ERROR),

    //crm-user
    /**/

    //crm-business
    /**/

    //crm-gateawy
    SEND_WEBSOCKET_MESSAGE(ServiceType.CRM_GATEWAY_SERVICE, null),

    //crm-calendar
    CALENDAR_NEW_EVENT(ServiceType.CRM_CALENDAR_SERVICE, KAFKA_CALENDAR_NEW_EVENT_ERROR),
    CALENDAR_DELETE_EVENT(ServiceType.CRM_CALENDAR_SERVICE, KAFKA_CALENDAR_DELETE_EVENT_ERROR),
    CALENDAR_MODIFIED_EVENT(ServiceType.CRM_CALENDAR_SERVICE, KAFKA_CALENDAR_MODIFIED_EVENT_ERROR),

    //crm-history
    HISTORY_SAVE_CHANGES(ServiceType.CRM_HISTORY_SERVICE, null),

    //crm-association
    ASSOCIATION_REMOVE_ASSOCIATION(ServiceType.CRM_ASSOCIATION_SERVICE, null);

    private ServiceType serviceType;
    private NotificationMsgType errorMsg;

    KafkaMsgType(ServiceType serviceType, NotificationMsgType errorMsg) {
        this.serviceType = serviceType;
        this.errorMsg = errorMsg;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public NotificationMsgType getErrorMsg() {
        return errorMsg;
    }
}
