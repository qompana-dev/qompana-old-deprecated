package com.devqube.crmshared.kafka.util;

import com.devqube.crmshared.kafka.dto.KafkaMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Optional;

public class KafkaUtil {
    public static Optional<KafkaMessage> getKafkaObject(String content) {
        try {
            return Optional.ofNullable(new ObjectMapper().readValue(content, KafkaMessage.class));
        } catch (IOException e) {
            return Optional.empty();
        }
    }
    public static String getStringFromKafkaMessage(KafkaMessage kafkaMsg) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(kafkaMsg);
    }
}
