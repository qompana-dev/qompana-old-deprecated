package com.devqube.crmshared.notification;

public enum NotificationMsgType {
    KAFKA_GENERIC_ERROR(false),
    KAFKA_ADD_NOTIFICATION_ERROR(false),
    KAFKA_WS_USER_NOTIFICATION_CHANGE_ERROR(false),
    KAFKA_SEND_ACTIVATION_MAIL_ERROR(false),
    KAFKA_RESENT_ACTIVATION_MAIL_ERROR(false),
    KAFKA_SEND_RESET_PASSWORD_MAIL_ERROR(false),
    KAFKA_SEND_EVENT_NOTIFICATION_MAIL_ERROR(false),
    KAFKA_CALENDAR_NEW_EVENT_ERROR(false),
    KAFKA_CALENDAR_DELETE_EVENT_ERROR(false),
    KAFKA_CALENDAR_MODIFIED_EVENT_ERROR(false),

    USER_ADDED(true),
    LEAD_TASK_FINISHED(true),
    LEAD_TASK_ACCEPTANCE(false),
    LEAD_TASK_ACCEPTED(true),
    LEAD_TASK_DECLINED(true),
    LEAD_TASK_EXPIRED(false),
    EVENT_NOTIFICATION(true),
    OPPORTUNITY_TASK_FINISHED(true),
    OPPORTUNITY_TASK_ACCEPTANCE(false),
    OPPORTUNITY_TASK_ACCEPTED(true),
    OPPORTUNITY_TASK_DECLINED(true),
    OPPORTUNITY_TASK_EXPIRED(false),
    EXPENSE_ACCEPTANCE(false),
    EXPENSE_ACCEPTED(true),
    EXPENSE_DECLINED(true),
    NEW_MAIL(false);

    private boolean groupNotification;

    NotificationMsgType(boolean groupNotification) {
        this.groupNotification = groupNotification;
    }

    public boolean isGroupNotification() {
        return groupNotification;
    }
}
