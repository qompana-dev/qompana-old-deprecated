package com.devqube.crmshared.dashboard.dto;

import com.devqube.crmshared.dashboard.model.WidgetTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DashboardWidgetDto {

    @NotNull
    private Long id;

    @Min(2)
    @Max(24)
    @NotNull
    private Integer rows;

    @Min(2)
    @Max(24)
    @NotNull
    private Integer cols;

    @Min(0)
    @Max(23)
    @NotNull
    private Integer x;

    @Min(0)
    @Max(23)
    @NotNull
    private Integer y;

    @NotNull
    private WidgetTypeEnum widgetTypeEnum;

    private Long widgetId;
}
