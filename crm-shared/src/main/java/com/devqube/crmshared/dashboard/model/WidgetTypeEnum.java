package com.devqube.crmshared.dashboard.model;

public enum WidgetTypeEnum {
    GOAL,
    REPORT,
    CALENDAR,
    NOTE,
    ACTIVITY,
    FILE,
    TOTAL_SALES_OPPORTUNITY,
    OPENED_SALES_OPPORTUNITY,
    LEAD_SOURCES,
    NOTIFICATIONS
}