package com.devqube.crmshared.importexport;

import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ExportToCsvAnnotationStrategy<T> extends HeaderColumnNameTranslateMappingStrategy<T> {
    public ExportToCsvAnnotationStrategy(Class<T> clazz) {
        Map<String, String> map = new HashMap<>();
        for (Field field : clazz.getDeclaredFields()) {
            map.put(field.getName(), field.getName());
        }
        setType(clazz);
        setColumnMapping(map);
    }

    @Override
    public String[] generateHeader(T bean) throws CsvRequiredFieldEmptyException {
        String[] result = super.generateHeader(bean);
        for (int i = 0; i < result.length; i++) {
            result[i] = getColumnName(i);
        }
        return result;
    }
}
