package com.devqube.crmshared.importexport;

import lombok.Data;

@Data
public class ImportObjLine {
    private Long line;
}
