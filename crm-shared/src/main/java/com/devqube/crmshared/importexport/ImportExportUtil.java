package com.devqube.crmshared.importexport;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.univocity.parsers.common.DataProcessingException;
import com.univocity.parsers.common.ParsingContext;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;

public class ImportExportUtil {

    public static <T extends ImportObjLine> List<T> convertFromCsv(InputStream csvFile, Boolean headerPresent, Class<T> clazz, List<DataProcessingException> errors) throws DataProcessingException {
        BeanListProcessor<T> rowProcessor = getRowProcessor(clazz);
        CsvParserSettings parserSettings = new CsvParserSettings();
        parserSettings.getFormat().setLineSeparator("\n");
        parserSettings.setDelimiterDetectionEnabled(true, ';');
        parserSettings.setProcessor(rowProcessor);
        parserSettings.setProcessorErrorHandler((e, objects, context) -> errors.add(e));
        parserSettings.setHeaderExtractionEnabled(headerPresent);
        CsvParser parser = new CsvParser(parserSettings);
        parser.parse(csvFile, "Windows-1250");
        return rowProcessor.getBeans();
    }

    public static <T> void export(HttpServletResponse response, List<T> objects, Class<T> clazz, String filePrefix) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        String filename = filePrefix + "__" + LocalDate.now() + ".csv";

        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"");
        response.setCharacterEncoding("UTF-8");

        new StatefulBeanToCsvBuilder<T>(response.getWriter())
                .withMappingStrategy(new ExportToCsvAnnotationStrategy<T>(clazz))
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(';')
                .withOrderedResults(true)
                .build().write(objects);
    }

    private static <T extends ImportObjLine> BeanListProcessor<T> getRowProcessor(Class<T> clazz) {
        return new BeanListProcessor<>(clazz) {
            @Override
            public void beanProcessed(T bean, ParsingContext context) {
                bean.setLine(context.currentLine());
                super.beanProcessed(bean, context);
            }
        };
    }
}
