package com.devqube.crmshared.currency.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RateOfMainCurrencyDto {
    private String currency;
    private Double rate;
}
