package com.devqube.crmshared.currency.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CurrencyDto {
    List<String> allCurrencies;
    String systemCurrency;
    List<RateOfMainCurrencyDto> ratesOfMainCurrency;
}
