package com.devqube.crmshared.exception;

public class KafkaReceiverArgTypeException extends Exception {
    public KafkaReceiverArgTypeException() {
    }

    public KafkaReceiverArgTypeException(String message) {
        super(message);
    }
}
