package com.devqube.crmshared.exception;

public class KafkaSendMessageException extends Exception {
    public KafkaSendMessageException() {
    }

    public KafkaSendMessageException(String message) {
        super(message);
    }
}
