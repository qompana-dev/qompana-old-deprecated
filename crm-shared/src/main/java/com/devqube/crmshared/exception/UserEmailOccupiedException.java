package com.devqube.crmshared.exception;

public class UserEmailOccupiedException extends RuntimeException {
    public UserEmailOccupiedException(String message) {
        super(message);
    }
}
