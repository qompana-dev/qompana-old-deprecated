package com.devqube.crmshared.exception;

public class BadOldPasswordException extends Exception {
    public BadOldPasswordException(String message) {
        super(message);
    }
}
