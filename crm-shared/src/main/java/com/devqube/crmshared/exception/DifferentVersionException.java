package com.devqube.crmshared.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DifferentVersionException extends Exception {
    public DifferentVersionException() {
    }

    public DifferentVersionException(String message) {
        super(message);
    }
}
