package com.devqube.crmshared.exception;

public class PasswordPolicyNotFulfilledException extends Exception {
    public PasswordPolicyNotFulfilledException(String message) {
        super(message);
    }
}
