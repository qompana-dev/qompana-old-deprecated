package com.devqube.crmshared.exception;

public class IncorrectNotificationTypeException extends Exception {
    public IncorrectNotificationTypeException(String message) {
        super(message);
    }
}
