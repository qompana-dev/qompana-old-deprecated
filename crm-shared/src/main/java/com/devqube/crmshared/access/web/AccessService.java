package com.devqube.crmshared.access.web;

import com.devqube.crmshared.access.model.PermissionDto;
import com.devqube.crmshared.access.model.ProfileDto;
import com.devqube.crmshared.exception.BadRequestException;
import org.aspectj.lang.ProceedingJoinPoint;

public interface AccessService {
    ProfileDto getLoggedUserProfile();

    boolean getIgnoreAccessCheck();

    Object controllerMethodsAdvice(ProceedingJoinPoint joinPoint) throws Throwable;

    <T> T assignAndVerify(T baseObject, T modifiedObject) throws BadRequestException;

    <T> T assignAndVerify(T modifiedObject) throws BadRequestException;

    boolean hasAccess(String frontendId, PermissionDto.State state, boolean invert);
}
