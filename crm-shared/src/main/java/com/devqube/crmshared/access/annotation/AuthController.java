package com.devqube.crmshared.access.annotation;


import java.lang.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface AuthController {
    public String name() default "";
    public String actionFrontendId() default "";
    public String readFrontendId() default "";
    public AuthControllerCase[] controllerCase() default {};
}
