package com.devqube.crmshared.access.util;

import com.devqube.crmshared.access.annotation.*;
import com.devqube.crmshared.access.model.ModulePermissionDto;
import com.devqube.crmshared.access.model.PermissionDto;
import com.devqube.crmshared.access.model.ProfileDto;
import com.devqube.crmshared.web.RestPageImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

@Slf4j
public class AccessUtil {
    public static String getAuthType(ProceedingJoinPoint joinPoint, String typeFromRequest) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        String authControllerType = getAuthControllerTypeIfExist(joinPoint, signature);
        return (authControllerType == null) ? typeFromRequest : authControllerType;
    }
    public static String getAuthAnnotationName(ProceedingJoinPoint joinPoint, String type) {
        AuthController authController = getAuthController(joinPoint, type);
        return (authController == null) ? null : authController.name();
    }

    public static String getAuthAnnotationActionFrontendId(ProceedingJoinPoint joinPoint, String type) {
        AuthController authController = getAuthController(joinPoint, type);
        return (authController == null || authController.actionFrontendId().length() == 0) ? null : authController.actionFrontendId();
    }
    public static String getAuthAnnotationReadFrontendId(ProceedingJoinPoint joinPoint, String type) {
        AuthController authController = getAuthController(joinPoint, type);
        return (authController == null || authController.readFrontendId().length() == 0) ? null : authController.readFrontendId();
    }

    private static AuthController getAuthController(ProceedingJoinPoint joinPoint, String type) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        AuthController authController = method.getAnnotation(AuthController.class);
        if (authController.controllerCase().length > 0 && type != null) {
            return getByType(authController, type);
        }
        return authController;
    }

    private static AuthController getByType(AuthController authController, String type) {
        if (type == null) {
            return authController;
        }
        Optional<AuthControllerCase> authControllerCase = Arrays.stream(authController.controllerCase()).filter(c -> c.type().equals(type)).findFirst();
        if (authControllerCase.isEmpty()) {
            return authController;
        }
        return toAuthController(authControllerCase.get());
    }

    private static String getAuthControllerTypeIfExist(ProceedingJoinPoint joinPoint, MethodSignature methodSig) {
        if (methodSig.getMethod().getParameterCount() != 0) {

            Optional<Parameter> param = Arrays.stream(methodSig.getMethod().getParameters())
                    .filter(c -> c.getAnnotation(AuthControllerType.class) != null).findFirst();
            if (param.isPresent() && Arrays.stream(param.get().getType().getMethods()).anyMatch(c -> c.getName().equals("toString"))) {
                return getParameterByName(methodSig, joinPoint, param.get().getName());
            }
        }
        return null;
    }
    private static String getParameterByName(MethodSignature methodSig, ProceedingJoinPoint proceedingJoinPoint, String parameterName) {
        Object[] args = proceedingJoinPoint.getArgs();
        String[] parametersName = methodSig.getParameterNames();

        int idx = Arrays.asList(parametersName).indexOf(parameterName);

        if(args.length > idx) {
            return args[idx].toString();
        }
        return null;
    }

    public static <T> boolean isCollection(T element) {
        return Collection.class.isAssignableFrom(element.getClass());
    }

    public static boolean isPageImpl(Object object) {
        return object.getClass().isAssignableFrom(RestPageImpl.class);
    }

    public static <T, R extends Collection<T>> R filterCollection(R collection, ProfileDto profile, String controllerName) {
        if (controllerName == null || collection == null) {
            return collection;
        }
        for (T next : collection) {
            try {
                next = AccessUtil.filterValue(next, profile, controllerName);
            } catch (IllegalAccessException e) {
                log.info(e.getMessage());
            }
        }
        return collection;
    }

    public static <T> T filterValue(T obj, ProfileDto profile, String controllerName) throws IllegalAccessException {
        if (obj == null || controllerName == null || !isFilterEnabled(obj.getClass())) {
            return obj;
        }

        List<Field> allFieldsList = FieldUtils.getAllFieldsList(obj.getClass());
        for (Field currentField : allFieldsList) {

            AuthField annotationByControllerName = getAnnotationByControllerName(currentField, controllerName);

            boolean setValueAsNull = false;
            if (annotationByControllerName != null) {
                if (annotationByControllerName.frontendId().length() != 0) {
                    PermissionDto byFrontendId = getByFrontendId(profile, annotationByControllerName.frontendId());
                    if (byFrontendId == null || byFrontendId.getState().equals(PermissionDto.State.INVISIBLE)) {
                        setValueAsNull = true;
                    }
                }
            } else {
                setValueAsNull = true;
            }

            if (setValueAsNull) {
                FieldUtils.writeField(currentField, obj, null, true);
            }

            if (!isAuthIgnoreChild(currentField) && isFilterEnabled(currentField.getType())) {
                //override if child contain AuthFilterEnabled
                Object baseValue = FieldUtils.readField(currentField, obj, true);
                if (baseValue != null) {
                    Object newValue = AccessUtil.filterValue(baseValue, profile, controllerName);
                    FieldUtils.writeField(currentField, obj, newValue, true);
                }
            }
        }
        return obj;
    }

    public static <T> T assign(T baseObject, T modifiedObject, ProfileDto profile, String controllerName) throws IllegalAccessException {
        if (profile == null || controllerName == null || modifiedObject == null || !isFilterEnabled(modifiedObject.getClass())) {
            return modifiedObject;
        }
        boolean baseObjectIsNull = baseObject == null;

        List<Field> allFieldsList = FieldUtils.getAllFieldsList(modifiedObject.getClass());
        for (Field currentField : allFieldsList) {

            AuthField annotationByControllerName = getAnnotationByControllerName(currentField, controllerName);
            boolean revertChanges = false;

            if (annotationByControllerName != null) {
                if (annotationByControllerName.frontendId().length() != 0) {
                    PermissionDto byFrontendId = getByFrontendId(profile, annotationByControllerName.frontendId());
                    if (byFrontendId == null || !byFrontendId.getState().equals(PermissionDto.State.WRITE)) {
                        revertChanges = true;
                    }
                }
            } else {
                revertChanges = true;
            }

            if (revertChanges) {
                Object value = (baseObjectIsNull) ? null : FieldUtils.readField(currentField, baseObject, true);
                FieldUtils.writeField(currentField, modifiedObject, value, true);
            }

            if (!isAuthIgnoreChild(currentField) && isFilterEnabled(currentField.getType())) {
                //override if child contain AuthFilterEnabled
                Object baseValue = (baseObjectIsNull) ? null : FieldUtils.readField(currentField, baseObject, true);
                Object modifiedValue = FieldUtils.readField(currentField, modifiedObject, true);
                Object newValue = assign(baseValue, modifiedValue, profile, controllerName);
                FieldUtils.writeField(currentField, modifiedObject, newValue, true);
            }
        }
        return modifiedObject;
    }

    private static AuthField getAnnotationByControllerName(Field field, String controllerName) {
        AuthField annotationByType = field.getAnnotation(AuthField.class);
        if (annotationByType != null && annotationByType.controller().equals(controllerName)) {
            return annotationByType;
        }
        AuthFields annotationsByType = field.getAnnotation(AuthFields.class);
        if (annotationsByType == null) {
            return null;
        }
        Optional<AuthField> first = Arrays.stream(annotationsByType.list()).filter(c -> c.controller().equals(controllerName)).findFirst();
        return first.orElse(null);
    }

    public static PermissionDto getByFrontendId(ProfileDto profile, String frontendId) {
        if (profile == null) {
            return null;
        }
        for (int i = 0; i < profile.getModulePermissions().size(); i++) {
            for (ModulePermissionDto modulePermission : profile.getModulePermissions()) {
                Optional<PermissionDto> first = modulePermission.getPermissions().stream()
                        .filter(c -> c.getWebControl().getFrontendId().equals(frontendId)).findFirst();
                if (first.isPresent() && dependsOnChecked(first.get(), modulePermission)) {
                    return first.get();
                }
            }
        }
        return null;
    }

    private static boolean dependsOnChecked(PermissionDto permission, ModulePermissionDto modulePermission) {
        switch (permission.getWebControl().getDependsOn()) {
            case CREATE:
                return modulePermission.isCreate() && modulePermission.isView();
            case VIEW:
                return modulePermission.isView();
            case DELETE:
                return modulePermission.isDelete() && modulePermission.isView();
            case EDIT:
                return modulePermission.isEdit() && modulePermission.isView();
        }
        return false;
    }

    public static <T> boolean isFilterEnabled(Collection<T> collection) {
        if (collection == null || collection.isEmpty()) {
            return false;
        }
        return isFilterEnabled(collection.iterator().next().getClass());
    }

    private static <T> boolean isFilterEnabled(Class<T> obj) {
        return obj.getAnnotation(AuthFilterEnabled.class) != null;
    }
    private static <T> boolean isAuthIgnoreChild(Field field) {
        return field.getAnnotation(AuthIgnoreChild.class) != null;
    }

    public static Set<String> getWritePermissions(ProfileDto profile) {
        return getPermissionsForType(profile, PermissionDto.State.WRITE);
    }
    public static Set<String> getReadPermissions(ProfileDto profile) {
        Set<String> result = getPermissionsForType(profile, PermissionDto.State.READ_ONLY);
        result.addAll(getWritePermissions(profile));
        return result;
    }
    public static Set<String> getPermissionsForType(ProfileDto profile, PermissionDto.State state) {
        Set<String> frontendIdsWithAccess = new HashSet<>();
        if (profile == null || profile.getModulePermissions() == null) {
            return frontendIdsWithAccess;
        }
        profile.getModulePermissions().forEach(modulePermission -> modulePermission.getPermissions().forEach(permission -> {
            if (permission.getState().equals(state) && dependsOnChecked(permission, modulePermission)) {
                frontendIdsWithAccess.add(permission.getWebControl().getFrontendId());
            }
        }));
        return frontendIdsWithAccess;
    }



    private static AuthController toAuthController(AuthControllerCase authControllerCase) {
        return new AuthController() {
            @Override
            public Class<? extends Annotation> annotationType() {
                return AuthController.class;
            }
            @Override
            public String actionFrontendId() {
                return authControllerCase.actionFrontendId();
            }
            @Override
            public String readFrontendId() {
                return authControllerCase.readFrontendId();
            }
            @Override
            public String name() {
                return authControllerCase.name();
            }
            @Override
            public AuthControllerCase[] controllerCase() {
                return new AuthControllerCase[0];
            }
        };
    }
}
