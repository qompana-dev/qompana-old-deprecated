package com.devqube.crmshared.access.model;

import lombok.Data;

import java.util.List;

@Data
public class ModulePermissionDto {
    private Long id;
    private boolean view;
    private boolean create;
    private boolean edit;
    private boolean delete;

    private List<PermissionDto> permissions;
}
