package com.devqube.crmshared.access.web;

import com.devqube.crmshared.access.model.PermissionDto;
import com.devqube.crmshared.access.model.ProfileDto;
import com.devqube.crmshared.access.util.AccessUtil;
import com.devqube.crmshared.exception.BadRequestException;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

@Slf4j
public abstract class AbstractAccessService {

    public final static String AROUND_ASPECT = "@annotation(com.devqube.crmshared.access.annotation.AuthController)";
    private final static String AUTH_CONTROLLER_TYPE = "Auth-controller-type";

    public abstract ProfileDto getLoggedUserProfile();

    public abstract boolean getIgnoreAccessCheck();

    public Object controllerMethodsAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        saveControllerTypeInRequest(joinPoint);
        if (!isAuthorized(joinPoint)) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        saveControllerNameInRequest(joinPoint);

        Object proceedResponse = null;
        try {
            proceedResponse = joinPoint.proceed();
        } catch (Exception e) {
            log.info(e.getMessage());
            throw e;
        }

        return getNewResponseEntity(proceedResponse);
    }

    public <T> T assignAndVerify(T baseObject, T modifiedObject) throws BadRequestException {
        if (getIgnoreAccessCheck()) {
            return modifiedObject;
        }
        ProfileDto profile = getLoggedUserProfile();
        String controllerName = getControllerNameFromRequest();
        try {
            return AccessUtil.assign(baseObject, modifiedObject, profile, controllerName);
        } catch (IllegalAccessException e) {
            log.info(e.getMessage());
            throw new BadRequestException();
        }
    }

    public <T> T assignAndVerify(T modifiedObject) throws BadRequestException { // baseObject is null
        if (getIgnoreAccessCheck()) {
            return modifiedObject;
        }
        ProfileDto profile = getLoggedUserProfile();
        String controllerName = getControllerNameFromRequest();
        try {
            return AccessUtil.assign(null, modifiedObject, profile, controllerName);
        } catch (IllegalAccessException e) {
            log.info(e.getMessage());
            throw new BadRequestException();
        }
    }

    public boolean hasAccess(String frontendId, PermissionDto.State state, boolean invert) {
        if (getIgnoreAccessCheck() || Strings.emptyToNull(frontendId) == null) {
            return invert != state.equals(PermissionDto.State.WRITE);
        }
        ProfileDto profile = getLoggedUserProfile();
        PermissionDto byFrontendId = AccessUtil.getByFrontendId(profile, frontendId);
        boolean result = byFrontendId != null && byFrontendId.getState().equals(state);
        return invert != result;
    }

    private Object filter(Object object) throws IllegalAccessException {
        if (getIgnoreAccessCheck()) {
            return object;
        }
        if (object == null) {
            return object;
        }
        ProfileDto profile = getLoggedUserProfile();
        String controllerName = getControllerNameFromRequest();

        if (AccessUtil.isPageImpl(object) && AccessUtil.isCollection(((PageImpl) object).getContent())) {
            this.filter(((PageImpl) object).getContent());
            return object;
        }

        if (AccessUtil.isCollection(object)) {
            return AccessUtil.filterCollection((Collection<Object>)object, profile, controllerName);
        } else {
            return AccessUtil.filterValue(object, profile, controllerName);
        }
    }

    private void saveControllerTypeInRequest(ProceedingJoinPoint joinPoint) {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
            request.setAttribute("authType", AccessUtil.getAuthType(joinPoint, request.getHeader(AUTH_CONTROLLER_TYPE)));
        } catch (Exception ignore) {}
    }

    private void saveControllerNameInRequest(ProceedingJoinPoint joinPoint) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        request.setAttribute("authName", AccessUtil.getAuthAnnotationName(joinPoint, getControllerTypeFromRequest()));
    }

    private boolean isAuthorized(ProceedingJoinPoint joinPoint) {
        if (getIgnoreAccessCheck()) {
            return true;
        }
        ProfileDto profile = getLoggedUserProfile();

        String controllerTypeFromRequest = getControllerTypeFromRequest();
        String authAnnotationReadFrontendId = AccessUtil.getAuthAnnotationReadFrontendId(joinPoint, controllerTypeFromRequest);
        String authAnnotationActionFrontendId = AccessUtil.getAuthAnnotationActionFrontendId(joinPoint, controllerTypeFromRequest);
        boolean readResult = authAnnotationReadFrontendId == null || AccessUtil.getReadPermissions(profile).contains(authAnnotationReadFrontendId);
        boolean writeResult = authAnnotationActionFrontendId == null || AccessUtil.getWritePermissions(profile).contains(authAnnotationActionFrontendId);

        return readResult && writeResult;
    }

    private String getControllerNameFromRequest() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        return (String) request.getAttribute("authName");
    }
    private String getControllerTypeFromRequest() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        return (String) request.getAttribute("authType");
    }

    private Object getNewResponseEntity(Object response) throws IllegalAccessException {
        if (response != null) {
            if (ResponseEntity.class.isAssignableFrom(response.getClass())) {
                ResponseEntity<Object> result = (ResponseEntity<Object>) response;
                FieldUtils.writeField(result, "body", filter(result.getBody()), true);
                return result;
            } else {
                log.info("controller with annotation com.devqube.crmuserservice.access.annotation.AuthController " +
                        "not return ResponseEntity. AuthController name = " + getControllerNameFromRequest());
            }
        }
        return response;
    }
}
