package com.devqube.crmshared.access.model;

import lombok.Data;

@Data
public class PermissionDto {
    private Long id;
    private State state;

    private WebControlDto webControl;

    public enum State {
        INVISIBLE, READ_ONLY, WRITE
    }
}
