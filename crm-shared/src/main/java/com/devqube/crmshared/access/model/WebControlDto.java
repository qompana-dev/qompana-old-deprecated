package com.devqube.crmshared.access.model;

import lombok.Data;

@Data
public class WebControlDto {
    private Long id;
    private String frontendId;
    private String name;
    private PermissionDto.State defaultState;

    private ModuleOperation dependsOn;

    private boolean required;


    public enum ModuleOperation {
        VIEW, CREATE, EDIT, DELETE
    }

}
