package com.devqube.crmshared.access.model;

import lombok.Data;

import java.util.List;

@Data
public class ProfileDto {
    private Long id;

    private String name;

    private String description;

    private List<ModulePermissionDto> modulePermissions;
}
