package com.devqube.crmshared.access.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface AuthControllerCase {
    public String type();
    public String name() default "";
    public String actionFrontendId() default "";
    public String readFrontendId() default "";
}
