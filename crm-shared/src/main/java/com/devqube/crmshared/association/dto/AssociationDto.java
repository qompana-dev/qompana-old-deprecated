package com.devqube.crmshared.association.dto;

import com.devqube.crmshared.search.CrmObjectType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AssociationDto {
    private Long id;
    private CrmObjectType sourceType;
    private Long sourceId;
    private CrmObjectType destinationType;
    private Long destinationId;

    private List<AssociationPropertyDto> associationProperties = new ArrayList<>();
}
