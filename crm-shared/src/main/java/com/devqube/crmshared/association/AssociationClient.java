package com.devqube.crmshared.association;

import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.association.dto.NoteDTO;
import com.devqube.crmshared.search.CrmObjectType;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@FeignClient(name = "crm-association-service", url = "${crm-association-service.url}")
public interface AssociationClient {
    @PostMapping("/associations")
    AssociationDto createAssociation(@RequestBody AssociationDetailDto associationDetailDto);

    @PostMapping("/associations/list")
    List<AssociationDto> createAssociations(@RequestBody List<AssociationDetailDto> associationDetailList);

    @PutMapping("/associations/list/type/{type}/id/{id}")
    List<AssociationDto> updateAssociations(@PathVariable("type") CrmObjectType type, @PathVariable("id") Long id, @RequestBody List<AssociationDetailDto> associationDetailList);

    @PutMapping("/associations/list/type/{type}/id/{id}/for-types/{objectTypes}")
    List<AssociationDto> updateAssociationsForTypes(@PathVariable("type") CrmObjectType type, @PathVariable("id") Long id, @PathVariable("objectTypes") List<CrmObjectType> objectTypes, @RequestBody List<AssociationDetailDto> associationDetailList);

    @DeleteMapping("/associations")
    void deleteAssociation(@RequestBody AssociationDetailDto associationDetailDto);

    @DeleteMapping("/associations/list")
    void deleteAssociations(@RequestBody List<AssociationDetailDto> associationDetailList);

    @GetMapping("/associations/type/{type}")
    List<AssociationDto> getAllByType(@PathVariable("type") CrmObjectType type);

    @GetMapping("/associations/type/{type}/id/{id}")
    List<AssociationDto> getAllByTypeAndId(@PathVariable("type") CrmObjectType type, @PathVariable("id") Long id);

    @DeleteMapping("/associations/type/{type}/id/{id}")
    void deleteAssociationsByTypeAndId(@PathVariable("type") CrmObjectType type, @PathVariable("id") Long id);

    @DeleteMapping("/associations/source-type/{sourceType}/source-id/{sourceId}/destination-type/{destinationType}")
    void deleteAssociationsBySourceTypeAndSourceIdAndDestinationType(@PathVariable("sourceType") CrmObjectType sourceType, @PathVariable("sourceId") Long sourceId, @PathVariable("destinationType") CrmObjectType destinationType);

    @GetMapping("/associations/type/{sourceType}/id/{sourceId}/destination-type/{destinationType}")
    List<AssociationDto> getAllBySourceTypeAndDestinationTypeAndSourceId(@PathVariable("sourceType") CrmObjectType sourceType, @PathVariable("destinationType") CrmObjectType destinationType, @PathVariable("sourceId") Long sourceId);

    @GetMapping("/associations/type/{sourceType}/ids/{sourceIds}/destination-type/{destinationType}")
    List<AssociationDto> getAllBySourceTypeAndDestinationTypeAndSourceIds(@PathVariable("sourceType") CrmObjectType sourceType, @PathVariable("destinationType") CrmObjectType destinationType, @PathVariable("sourceIds") List<Long> sourceIds);

    @RequestMapping(value = "/note", consumes = {"application/json"}, method = RequestMethod.POST)
    NoteDTO createNote(@RequestBody NoteDTO noteDTO, @RequestHeader(value = "Logged-Account-Email", required = true) String email);

    @RequestMapping(value = "/note/{id}", produces = {"application/json"}, method = RequestMethod.GET)
    NoteDTO getNoteById(@PathVariable("id") Long id, @RequestHeader(value = "Logged-Account-Email", required = true) String email);

    @RequestMapping(value = "/internal/note/{id}", method = RequestMethod.DELETE)
    void deleteById(@PathVariable("id") Long id, @RequestHeader(value = "Logged-Account-Email", required = true) String email);

    @RequestMapping(value = "/note/{id}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    NoteDTO modifyNote(@PathVariable("id") Long id, @Valid @RequestBody NoteDTO noteDTO, @RequestHeader(value = "Logged-Account-Email", required = true) String email);

    @DeleteMapping("/note/type/{type}/id/{id}")
    void deleteNotesByTypeAndId(@PathVariable("type") CrmObjectType type, @PathVariable("id") Long id);

    @RequestMapping(value = "/internal/associations/copy", consumes = {"application/json"}, method = RequestMethod.POST)
    void copyAssociations(@RequestBody List<AssociationDetailDto> associationDetailDtos);

    @RequestMapping(value = "/internal/note/copy", consumes = {"application/json"}, method = RequestMethod.POST)
    void copyNotes(@RequestBody List<AssociationDetailDto> associationDetailDtos);
}
