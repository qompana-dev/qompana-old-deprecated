package com.devqube.crmshared.association.dto;

import com.devqube.crmshared.search.CrmObjectType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AssociationDetailDto {
    @NotNull
    private CrmObjectType sourceType;

    @NotNull
    private Long sourceId;

    @NotNull
    private CrmObjectType destinationType;

    @NotNull
    private Long destinationId;

    private List<AssociationPropertyDto> properties = new ArrayList<>();

    public AssociationDetailDto(@NotNull CrmObjectType sourceType, @NotNull Long sourceId, @NotNull CrmObjectType destinationType, @NotNull Long destinationId) {
        this.sourceType = sourceType;
        this.sourceId = sourceId;
        this.destinationType = destinationType;
        this.destinationId = destinationId;
        this.properties = new ArrayList<>();
    }

    public AssociationDto toModel() {
        AssociationDto associationDto = new AssociationDto();
        associationDto.setSourceId(this.getSourceId());
        associationDto.setSourceType(this.getDestinationType());
        associationDto.setDestinationId(this.getDestinationId());
        associationDto.setDestinationType(this.getDestinationType());
        associationDto.setAssociationProperties(this.properties);
        return associationDto;
    }
    public static AssociationDetailDto fromModel(AssociationDto associationDto) {
        AssociationDetailDto detailDto = new AssociationDetailDto();
        associationDto.setSourceId(associationDto.getSourceId());
        associationDto.setSourceType(associationDto.getDestinationType());
        associationDto.setDestinationId(associationDto.getDestinationId());
        associationDto.setDestinationType(associationDto.getDestinationType());
        associationDto.setAssociationProperties(associationDto.getAssociationProperties());
        return detailDto;
    }
}
