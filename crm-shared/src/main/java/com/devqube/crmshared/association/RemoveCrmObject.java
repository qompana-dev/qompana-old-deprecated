package com.devqube.crmshared.association;

import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmshared.search.CrmObjectType;
import lombok.extern.slf4j.Slf4j;

/**
 * To use this class in service you should add class ObjectRemoveComponentImpl
 *
 * code:
 * package com.devqube.crmcustomerservice.kafka;
 *
 * import com.devqube.crmshared.association.RemoveCrmObject;
 * import com.devqube.crmshared.kafka.KafkaService;
 * import org.springframework.stereotype.Component;
 *
 * @Component
 * public class ObjectRemoveComponentImpl extends RemoveCrmObject.RemoveComponent {
 *     public ObjectRemoveComponentImpl(KafkaService kafkaService) {
 *         super(kafkaService);
 *     }
 * }
 *
 * this class REQUIRE KafkaService implementation in service
 *
 * next in model (ex. Contact.java) add method
 *
 * @PreRemove
 * public void preRemove() {
 *      if (this.id != null) {
 *          RemoveCrmObject.addObjectToRemove(CrmObjectType.contact, this.id);
 *      }
 * }
 */

@Slf4j
public class RemoveCrmObject {
    private static RemoveCrmObject instance;
    private KafkaService kafkaService;

    private RemoveCrmObject() {
    }

    public static RemoveCrmObject getInstance() {
        if (instance == null) {
            instance = new RemoveCrmObject();
        }
        return instance;
    }

    public static void addObjectToRemove(CrmObjectType type, Long id) {
        RemoveCrmObject.getInstance().add(type, id);
    }

    public void add(CrmObjectType type, Long id) {
        try {
            log.info("sendRemoveAssociation " + type.name() + " " + id);
            kafkaService.sendRemoveAssociation(type, id);
        } catch (KafkaSendMessageException e) {
            e.printStackTrace();
        }
    }

    public void setKafkaService(KafkaService kafkaService) {
        log.info("set kafka service");
        this.kafkaService = kafkaService;
    }

    public static class RemoveComponent {
        public RemoveComponent(KafkaService kafkaService) {
            RemoveCrmObject.getInstance().setKafkaService(kafkaService);
        }
    }
}
