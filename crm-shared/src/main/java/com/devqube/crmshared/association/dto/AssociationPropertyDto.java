package com.devqube.crmshared.association.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AssociationPropertyDto {
    private Long id;
    private AssociationPropertyTypeEnum type;
    private String value;
}
