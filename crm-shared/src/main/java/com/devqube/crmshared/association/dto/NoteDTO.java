package com.devqube.crmshared.association.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NoteDTO {
    private Long id;
    private ObjectTypeEnum objectType;
    private Long objectId;
    private String content;
    private String formattedContent;
    private String htmlContent;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Long updatedBy;
    private Long createdBy;


    public enum ObjectTypeEnum {
        LEAD("lead"),
        CUSTOMER("customer"),
        OPPORTUNITY("opportunity"),
        CONTACT("contact"),
        USER("user"),
        PRODUCT("product"),
        FILE("file"),
        ROLE("role"),
        UNKNOWN("unknown");

        private String value;

        ObjectTypeEnum(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        public String toString() {
            return String.valueOf(value);
        }

        @JsonCreator
        public static ObjectTypeEnum fromValue(String text) {
            for (ObjectTypeEnum b : ObjectTypeEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + text + "'");
        }
    }

}

