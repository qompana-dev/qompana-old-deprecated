package com.devqube.crmshared.search;

import lombok.*;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
public class CrmObject {
    private Long id;
    private CrmObjectType type;
    @EqualsAndHashCode.Exclude
    private String label;
    @EqualsAndHashCode.Exclude
    private LocalDateTime created;
}
