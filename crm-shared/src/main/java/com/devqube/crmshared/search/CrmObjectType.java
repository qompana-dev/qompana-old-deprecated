package com.devqube.crmshared.search;

public enum CrmObjectType {
    lead, customer, contact, user, product, file, role, task, activity, opportunity, manufacturer, supplier, expense, email, emailMessage, note;
}
