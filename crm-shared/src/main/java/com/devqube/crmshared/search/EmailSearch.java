package com.devqube.crmshared.search;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
public class EmailSearch {
    private CrmObjectType crmObjectType;
    private String name;
    private String email;
}
