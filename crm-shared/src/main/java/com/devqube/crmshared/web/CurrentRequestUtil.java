package com.devqube.crmshared.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Slf4j
public class CurrentRequestUtil {

    public static String getLoggedInAccountEmail() {
        return get().getHeader("Logged-Account-Email");
    }

    private static HttpServletRequest get(){
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            return ((ServletRequestAttributes)requestAttributes).getRequest();
        }
        log.debug("Not called in the context of an HTTP request");
        throw new RuntimeException("Not called in the context of an HTTP request");
    }
}
