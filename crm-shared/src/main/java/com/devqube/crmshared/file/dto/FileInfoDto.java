package com.devqube.crmshared.file.dto;

import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class FileInfoDto {
    private String id;
    private Long fileId;
    private String name;
    private String format;
    private String accountEmail;
    private String description;
}
