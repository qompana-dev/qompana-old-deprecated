package com.devqube.crmshared.file;

import com.devqube.crmshared.file.dto.FileInfoDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "crm-file-service", url = "${crm-file-service.url}")
public interface InternalFileClient {
    @RequestMapping(value = "/internal/files/{fileId}", method = RequestMethod.PUT)
    public FileInfoDto setFileInfo(@PathVariable("fileId") Long fileId, @RequestBody FileInfoDto fileInfoDto);

    @RequestMapping(value = "/internal/files/temp-file/{tempFileId}", method = RequestMethod.PUT)
    public FileInfoDto setTempFileInfo(@PathVariable("tempFileId") Long tempFileId, @RequestBody FileInfoDto fileInfoDto);

    @RequestMapping(value = "/internal/files/{fileId}", method = RequestMethod.GET)
    public FileInfoDto getFileInfo(@PathVariable("fileId") Long fileId);

    @RequestMapping(value = "/internal/files/name/{fileName}", method = RequestMethod.GET)
    public FileInfoDto getFileInfoByName(@PathVariable("fileName") String fileName);

    @RequestMapping(value = "/internal/files/temp/name/{fileName}", method = RequestMethod.GET)
    public FileInfoDto getTempFileInfoByName(@PathVariable("fileName") String fileName);

    @RequestMapping(value = "/internal/files/ids/{fileIds}", method = RequestMethod.GET)
    public  List<FileInfoDto> getFileInfo(@PathVariable("fileIds") List<Long> fileId);

    @RequestMapping(value = "/internal/files/{fileIds}", method = RequestMethod.DELETE)
    public void deleteFiles(@PathVariable("fileIds") List<Long> fileIds);

    @RequestMapping(value = "/internal/files/{fileId}", method = RequestMethod.DELETE)
    public void deleteFile(@PathVariable("fileId") Long fileId);
}
