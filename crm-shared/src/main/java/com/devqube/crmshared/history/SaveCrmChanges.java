package com.devqube.crmshared.history;

import com.devqube.crmshared.history.dto.ObjectChangedDto;
import com.devqube.crmshared.history.dto.ObjectHistoryType;
import com.devqube.crmshared.history.util.SaveCrmChangesUtil;
import com.devqube.crmshared.kafka.KafkaService;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

/**
 * To use this class in service you should add class SaveChangesComponentImpl
 * <p>
 * code:
 * package com.devqube.crmcustomerservice.kafka;
 * <p>
 * import com.devqube.crmshared.history.SaveCrmChanges;
 * import com.devqube.crmshared.kafka.KafkaService;
 * import org.springframework.stereotype.Component;
 *
 * @Component public class SaveChangesComponentImpl extends SaveCrmChanges.SaveChangesComponent {
 * public SaveChangesComponentImpl(KafkaService kafkaService) {
 * super(kafkaService);
 * }
 * }
 * <p>
 * this class REQUIRE KafkaService implementation in service
 * <p>
 * next in model (ex. Contact.java) add above class
 * @EntityListeners(SaveCrmChangesListener.class) when field contain OneToOne, OneToMany, ManyToOne or ManyToMany annotation
 * add SaveHistory annotation
 * <p>
 * ex.
 * @SaveHistory(fields = {"name", "surname"}, message = "{{name}} {{surname}}", separator = ", ")
 * <p>
 * use annotation @SaveHistoryIgnore to ignore field
 */

@Slf4j
public class SaveCrmChanges {
    private static SaveCrmChanges instance;
    private KafkaService kafkaService;

    private SaveCrmChanges() {
    }

    public static SaveCrmChanges getInstance() {
        if (instance == null) {
            instance = new SaveCrmChanges();
        }
        return instance;
    }

    static void saveChangesForObject(Object o, boolean deleteEvent, String email) {
        getInstance().save(o, deleteEvent, email);
    }

    private void save(Object o, boolean deleteEvent, String email) {
        LocalDateTime modifiedTime = LocalDateTime.now();
        try {
            ObjectHistoryType objectType = SaveCrmChangesUtil.getObjectType(o);
            ObjectChangedDto objectChangedDto = SaveCrmChangesUtil.getJson(o);
            kafkaService.saveHistory(objectType, objectChangedDto.getId(), (deleteEvent) ? null : objectChangedDto.getJson(), modifiedTime, email);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setKafkaService(KafkaService kafkaService) {
        log.info("set kafka service");
        this.kafkaService = kafkaService;
    }

    public static class SaveChangesComponent {
        public SaveChangesComponent(KafkaService kafkaService) {
            SaveCrmChanges.getInstance().setKafkaService(kafkaService);
        }
    }
}
