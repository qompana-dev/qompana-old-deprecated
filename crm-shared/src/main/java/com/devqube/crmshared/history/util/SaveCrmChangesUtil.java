package com.devqube.crmshared.history.util;

import com.devqube.crmshared.history.annotation.SaveHistory;
import com.devqube.crmshared.history.annotation.SaveHistoryIgnore;
import com.devqube.crmshared.history.dto.ObjectChangedDto;
import com.devqube.crmshared.history.dto.ObjectHistoryType;
import com.devqube.crmshared.history.exception.SaveCrmChangesException;
import com.google.common.base.Strings;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.json.JSONObject;

import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SaveCrmChangesUtil {
    private static List<Class<? extends Annotation>> ignoredAnnotations = Arrays.asList(OneToOne.class, OneToMany.class, ManyToOne.class, ManyToMany.class);

    public static ObjectHistoryType getObjectType(Object o) throws SaveCrmChangesException {
        if (o == null) {
            throw new SaveCrmChangesException();
        }
        String packageName = o.getClass().getName().toString();
        return findByClassRef(packageName);
    }

    private static ObjectHistoryType findByClassRef(String classRef) throws SaveCrmChangesException {
        return Arrays.stream(ObjectHistoryType.values()).filter(type -> type.getClassRef().equals(classRef)).findFirst().orElseThrow(SaveCrmChangesException::new);
    }

    public static ObjectChangedDto getJson(Object o) throws IllegalAccessException, SaveCrmChangesException {
        List<Field> fieldNamesToSave = getFieldNamesToSave(o);
        String id = getValueFromObjectByField(o, getIdField(o)).toString();

        JSONObject result = new JSONObject();
        for (Field field : fieldNamesToSave) {
            String value = getValueFromObjectByField(o, field).toString();
            result.put(field.getName(), value);
        }
        List<Field> fieldsWithSaveHistory = getFieldsWithSaveHistory(o);
        for (Field field : fieldsWithSaveHistory) {
            String value = getSaveHistoryValueFromObjectByField(o, field);
            result.put(field.getName(), value);
        }

        return ObjectChangedDto.builder().id(id).json(result.toString()).build();
    }

    private static List<Field> getFieldsWithSaveHistory(Object o) {
        return Arrays.stream(o.getClass().getDeclaredFields())
                .filter(f -> !isCorrectField(f))
                .filter(SaveCrmChangesUtil::isSaveHistoryField)
                .filter(field -> field.getAnnotation(SaveHistoryIgnore.class) == null)
                .collect(Collectors.toList());
    }

    private static String getSaveHistoryValueFromObjectByField(Object o, Field field) throws IllegalAccessException {
        boolean collection = isCollection(field.getType());
        SaveHistory saveHistory = field.getAnnotation(SaveHistory.class);
        Object historyObj = getValueFromObjectByField(o, field);
        if (collection) {
            String content = Objects.requireNonNull(getValueForObjectList(historyObj, saveHistory.message(), saveHistory.fields()))
                    .stream().collect(Collectors.joining(Strings.nullToEmpty(saveHistory.separator())));
            return "[" + content + "]";
        } else {
            return getValueForObject(historyObj, saveHistory.message(), saveHistory.fields());
        }
    }

    private static List<String> getValueForObjectList(Object o, String message, String[] fields) throws IllegalAccessException {
        if (o == null || o.equals("")) {
            return new ArrayList<>();
        }
        List<String> result = new ArrayList<>();
        for (Object next : ((Collection) o)) {
            result.add(getValueForObject(next, message, fields));
        }
        Collections.sort(result);
        return result;
    }

    private static String getValueForObject(Object o, String message, String[] fields) throws IllegalAccessException {
        if (o == null) {
            return "";
        }
        List<String> fieldNames = Arrays.asList(fields);
        List<Field> fieldList = Arrays.stream(o.getClass().getDeclaredFields())
                .filter(f -> fieldNames.contains(f.getName())).collect(Collectors.toList());

        String result = message;
        for (Field value : fieldList) {
            String valueFromObjectByField = getValueFromObjectByField(o, value).toString();
            result = result.replaceAll(Pattern.quote("{{" + value.getName() + "}}"), valueFromObjectByField);
        }

        return result;
    }

    private static boolean isCollection(Class<?> element) {
        return Collection.class.isAssignableFrom(element);
    }

    private static Object getValueFromObjectByField(Object o, Field field) throws IllegalAccessException {
        Object ob = FieldUtils.readField(field, o, true);
        if (ob == null) {
            return "";
        }
        return FieldUtils.readField(field, o, true);
    }

    private static List<Field> getFieldNamesToSave(Object o) {
        return Arrays.stream(o.getClass().getDeclaredFields())
                .filter(SaveCrmChangesUtil::isCorrectField)
                .filter(field -> field.getAnnotation(SaveHistoryIgnore.class) == null)
                .collect(Collectors.toList());
    }

    private static Field getIdField(Object o) throws SaveCrmChangesException {
        return Arrays.stream(o.getClass().getDeclaredFields())
                .filter(SaveCrmChangesUtil::isCorrectField)
                .filter(field -> field.getAnnotation(javax.persistence.Id.class) != null)
                .findFirst().orElseThrow(SaveCrmChangesException::new);
    }

    private static boolean isCorrectField(Field field) {
        Annotation[] annotations = field.getAnnotations();
        if (annotations == null || annotations.length == 0) {
            return true;
        }
        return Arrays.stream(annotations).noneMatch(a -> ignoredAnnotations.stream().anyMatch(i -> i.isAssignableFrom(a.getClass())));
    }

    private static boolean isSaveHistoryField(Field field) {
        return field.getAnnotation(SaveHistory.class) != null;
    }
}
