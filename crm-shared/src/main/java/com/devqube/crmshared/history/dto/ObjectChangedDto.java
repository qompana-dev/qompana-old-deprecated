package com.devqube.crmshared.history.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ObjectChangedDto {
    private String id;
    private String json;
}
