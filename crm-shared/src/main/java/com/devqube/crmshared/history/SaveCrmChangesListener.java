package com.devqube.crmshared.history;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * in entity add above class
 * @EntityListeners(SaveCrmChangesListener.class)
 *
 */

public class SaveCrmChangesListener {
    @PostPersist
    @PostUpdate
    @Transactional
    public void postSaveChanges(Object o) {
        SaveCrmChanges.saveChangesForObject(o, false, getEmailFromRequest());
    }
    @PostRemove
    @Transactional
    public void postDeleteObject(Object o) {
        SaveCrmChanges.saveChangesForObject(o, true, getEmailFromRequest());
    }

    private String getEmailFromRequest() {
        try {
            RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            return request.getHeader("Logged-Account-Email");
        } catch (Exception ignore) {
            return "";
        }
    }

}
