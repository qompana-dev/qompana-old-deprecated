package com.devqube.crmshared.history.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ObjectHistoryType {
    CUSTOMER("com.devqube.crmbusinessservice.customer.customer.model.Customer"),
    CONTACT("com.devqube.crmbusinessservice.customer.contact.model.Contact"),
    OPPORTUNITY("com.devqube.crmbusinessservice.leadopportunity.opportunity.model.Opportunity"),
    LEAD("com.devqube.crmbusinessservice.leadopportunity.lead.model.Lead"),
    EXPENSE("com.devqube.crmbusinessservice.expense.model.Expense"),
    TASK("com.devqube.crmtaskservice.task.model.Task"),

    TEST("com.devqube.crmshared.history.HistoryUtilTest$TestObject");

    private String classRef;
}
