package com.devqube.crmshared.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountInfoDto {

    private Long id;
    private String email;
    private String name;
    private String surname;
    private Long roleId;
    private String roleName;
    private String avatar;
}
