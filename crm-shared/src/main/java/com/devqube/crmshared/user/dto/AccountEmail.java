package com.devqube.crmshared.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountEmail {
    private Integer id;
    private String email;
    private String name;
    private String surname;
}
