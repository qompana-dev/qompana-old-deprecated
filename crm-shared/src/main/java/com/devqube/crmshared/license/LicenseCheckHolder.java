package com.devqube.crmshared.license;

import java.time.LocalDateTime;

class LicenseCheckHolder {
    private static LicenseCheckHolder instance;

    private boolean licenseActive = false;
    private LocalDateTime licenseCheckDate;
    private Long maxUserCount ;
    private String clientInstanceId;

    private LicenseCheckHolder() {
    }

    public static LicenseCheckHolder getInstance() {
        if (instance == null) {
            instance = new LicenseCheckHolder();
        }
        return instance;
    }

    public void setLicenseCheck(boolean active, LocalDateTime date) {
        this.licenseActive = active;
        this.licenseCheckDate = date;
    }

    public boolean isLicenseActive() {
        return licenseActive;
    }

    public LocalDateTime getLicenseCheckDate() {
        return licenseCheckDate;
    }

    public String getClientInstanceId() {
        return clientInstanceId;
    }

    public void setClientInstanceId(String clientInstanceId) {
        this.clientInstanceId = clientInstanceId;
    }

    public Long getMaxUserCount() {
        return maxUserCount;
    }

    public void setMaxUserCount(Long maxUserCount) {
        this.maxUserCount = maxUserCount;
    }
}
