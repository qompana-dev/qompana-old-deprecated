package com.devqube.crmshared.license;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * add
 *
 * @Import({LicenseCheck.class}) above the main class
 */

@Slf4j
@EnableScheduling
public class LicenseCheck implements Filter {

    private static final String INSTANCE_ID_ENV = "CRM_CLIENT_INSTANCE_ID";
    private static final String DEV_INSTANCE_NAME = "local-dev";

    private static final String CRM_LICENSE_URL = "CRM_LICENSE_URL";
    private static final String LOCAL_DEV_URL = "http://crm-license-service:8094";

    private static final int MINUTES_TO_EXPIRATION = 60; // 60
    private static final int NUMBER_OF_MINUTES_TO_BE_CHECKED_BEFORE_EXPIRED = 10; // 10
    private static final int REFRESH = 5; // 5

    private static final String LICENSE_CHECK_ENDPOINT = "/api/service/license/valid/";
    private static final String LICENSE_DAYS_LEFT_CHECK_ENDPOINT = "/api/service/license/days-left/";
    private static final String MAX_USERS_ENDPOINT = "/api/service/license/max-users/";

    private static final String BASIC_AUTH_LOGIN = "service";
    private static final String BASIC_AUTH_PASSWORD = "uADdm56JfqMX6853jZ4E5CWY2fZskx";

    public LicenseCheck() {
        LicenseCheckHolder.getInstance().setClientInstanceId(getClientInstanceId());
        log.info("instance id = {}", LicenseCheckHolder.getInstance().getClientInstanceId());
    }

    @Scheduled(fixedDelay = LicenseCheck.REFRESH * 60 * 1000)
    public void refresh() {
        if (shouldRefresh()) {
            refreshLicence();
            refreshMaxUserCount();
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (LicenseCheckHolder.getInstance().isLicenseActive()) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            HttpServletResponse res = (HttpServletResponse) servletResponse;
            res.setStatus(HttpServletResponse.SC_PAYMENT_REQUIRED); // 402 Payment Required
            res.setHeader("Client-Instance-Id", LicenseCheckHolder.getInstance().getClientInstanceId());
        }
    }

    private boolean shouldRefresh() {
        return !LicenseCheckHolder.getInstance().isLicenseActive() ||
                LicenseCheckHolder.getInstance().getLicenseCheckDate() == null ||
                LicenseCheckHolder.getInstance().getMaxUserCount() == null ||
                LicenseCheckHolder.getInstance().getLicenseCheckDate()
                        .plusMinutes(LicenseCheck.MINUTES_TO_EXPIRATION)
                        .minusMinutes(LicenseCheck.NUMBER_OF_MINUTES_TO_BE_CHECKED_BEFORE_EXPIRED)
                        .isBefore(LocalDateTime.now());
    }

    private boolean checkLicense() {
        try {
            log.info("checkLicense = {}", getUrl(LICENSE_CHECK_ENDPOINT));
            ResponseEntity<Boolean> response = new RestTemplate().exchange(getUrl(LICENSE_CHECK_ENDPOINT), HttpMethod.GET, getAuthorizationHeader(), Boolean.class);
            if (response.getStatusCode().equals(HttpStatus.OK) && response.getBody() != null) {
                return response.getBody();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean refreshLicence(){
        boolean licenseStatus = checkLicense();
        LicenseCheckHolder.getInstance().setLicenseCheck(licenseStatus, LocalDateTime.now());
        log.info("LICENCE HOLDER REFRESH WITH STATUS: " + licenseStatus);
        return licenseStatus;
    }

    private long checkMaxUserCount() {
        try {
            log.info("checkMaxUserCount = {}", getUrl(MAX_USERS_ENDPOINT));
            ResponseEntity<Long> response = new RestTemplate().exchange(getUrl(MAX_USERS_ENDPOINT), HttpMethod.GET, getAuthorizationHeader(), Long.class);
            if (response.getStatusCode().equals(HttpStatus.OK) && response.getBody() != null) {
                return response.getBody();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long refreshMaxUserCount(){
        long maxUserCount = checkMaxUserCount();
        LicenseCheckHolder.getInstance().setMaxUserCount(maxUserCount);
        log.info("LICENCE HOLDER REFRESH WITH MAX USER COUNT: " + maxUserCount);
        return maxUserCount;
    }

    public long getMaxUserCount(){
        return LicenseCheckHolder.getInstance().getMaxUserCount();
    }

    private long checkLicenseDaysLeft() {
        try {
            log.info("checkLicenseDaysLeft = {}", getUrl(LICENSE_DAYS_LEFT_CHECK_ENDPOINT));
            ResponseEntity<Long> response = new RestTemplate().exchange(getUrl(LICENSE_DAYS_LEFT_CHECK_ENDPOINT), HttpMethod.GET, getAuthorizationHeader(), Long.class);
            if (response.getStatusCode().equals(HttpStatus.OK) && response.getBody() != null) {
                return response.getBody();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long refreshLicenseDaysLeft(){
        long licenseDaysLeft = checkLicenseDaysLeft();
        log.info("REFRESH LICENSE DAYS LEFT: " + licenseDaysLeft);
        return licenseDaysLeft;
    }

    private String getUrl(String endpoint) {
        return getLicenseUrl() + endpoint + LicenseCheckHolder.getInstance().getClientInstanceId();
    }

    private String getClientInstanceId() {
        return Optional.ofNullable(Strings.emptyToNull(System.getenv(INSTANCE_ID_ENV))).orElse(LicenseCheck.DEV_INSTANCE_NAME);
    }

    private String getLicenseUrl() {
        return Optional.ofNullable(Strings.emptyToNull(System.getenv(CRM_LICENSE_URL))).orElse(LicenseCheck.LOCAL_DEV_URL);
    }

    private HttpEntity<String> getAuthorizationHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(BASIC_AUTH_LOGIN, BASIC_AUTH_PASSWORD);
        return new HttpEntity<>(null, headers);
    }
}
