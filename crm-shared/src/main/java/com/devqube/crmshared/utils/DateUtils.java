package com.devqube.crmshared.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    public static Date shiftFromNow(int unit, int amount) {
        Calendar cal = Calendar.getInstance();
        cal.add(unit, amount);
        return cal.getTime();
    }
}
