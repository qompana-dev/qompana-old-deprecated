package com.devqube.crmshared.task;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskSummaryDto {

    private Long allTasks;
    private Long oldTasks;
    private Long plannedTasks;
    private Long latestTasks;
    private Long completedTasks;

}
