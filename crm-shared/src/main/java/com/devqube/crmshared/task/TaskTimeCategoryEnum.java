package com.devqube.crmshared.task;

public enum TaskTimeCategoryEnum {
    ALL, PLANNED, LATEST, OLD, COMPLETED
}
