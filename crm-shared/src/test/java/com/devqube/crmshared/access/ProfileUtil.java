package com.devqube.crmshared.access;

import com.devqube.crmshared.access.model.ModulePermissionDto;
import com.devqube.crmshared.access.model.PermissionDto;
import com.devqube.crmshared.access.model.ProfileDto;
import com.devqube.crmshared.access.model.WebControlDto;
import lombok.Builder;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ProfileUtil {

    private static WebControlDto getWebControlDto(PermissionInitObj PermissionDtoInitObj) {
        WebControlDto result = new WebControlDto();
        result.setDefaultState(PermissionDto.State.READ_ONLY);
        result.setDependsOn(PermissionDtoInitObj.getDepenendsOn());
        result.setFrontendId(PermissionDtoInitObj.getFrontendId());
        result.setName("name"+PermissionDtoInitObj.getFrontendId());
        result.setRequired(true);
        return result;
    }

    private static PermissionDto getPermissionDto(PermissionInitObj PermissionDtoInitObj) {
        PermissionDto result = new PermissionDto();
        result.setId(PermissionDtoInitObj.getId());
        result.setState(PermissionDtoInitObj.getState());
        result.setWebControl(getWebControlDto(PermissionDtoInitObj));
        return result;
    }

    private static List<PermissionDto> getPermissionDtos(List<PermissionInitObj> PermissionDtoInitObjs) {
        return PermissionDtoInitObjs.stream().map(ProfileUtil::getPermissionDto).collect(Collectors.toList());
    }

    private static ModulePermissionDto getModulePermissionDto(List<PermissionInitObj> PermissionDtoInitObjs, ProfileInitObj profileInitObj) {
        ModulePermissionDto modulePermissionDto = new ModulePermissionDto();
        modulePermissionDto.setCreate(profileInitObj.isCreate());
        modulePermissionDto.setEdit(profileInitObj.isEdit());
        modulePermissionDto.setDelete(profileInitObj.isDelete());
        modulePermissionDto.setView(profileInitObj.isView());
        modulePermissionDto.setId(1L);
        modulePermissionDto.setPermissions(getPermissionDtos(PermissionDtoInitObjs));
        return modulePermissionDto;
    }

    public static ProfileDto getProfile() {
        return getProfile(Arrays.asList(
                PermissionInitObj.builder().frontendId("1").state(PermissionDto.State.WRITE).build(),
                PermissionInitObj.builder().frontendId("2").state(PermissionDto.State.READ_ONLY).build(),
                PermissionInitObj.builder().frontendId("3").state(PermissionDto.State.INVISIBLE).build()
        ), ProfileInitObj.builder().build());
    }

    public static ProfileDto getProfile(List<PermissionInitObj> permissionDtoInitObjs, ProfileInitObj profileInitObj) {
        List<ModulePermissionDto> modulePermissionDtoSet = new ArrayList<>();
        modulePermissionDtoSet.add(getModulePermissionDto(permissionDtoInitObjs, profileInitObj));
        ProfileDto result = new ProfileDto();
        result.setId(1L);
        result.setName("Profil");
        result.setModulePermissions(modulePermissionDtoSet);
        return result;
    }

    @Builder
    @Getter
    public static class PermissionInitObj {
        private Long id = 1L;
        private String frontendId = "1";
        private PermissionDto.State state = PermissionDto.State.READ_ONLY;
        private WebControlDto.ModuleOperation depenendsOn = WebControlDto.ModuleOperation.CREATE;
    }

    @Builder
    @Getter
    public static class ProfileInitObj {
        private boolean create = true;
        private boolean edit = true;
        private boolean delete = true;
        private boolean view = true;
    }
}
