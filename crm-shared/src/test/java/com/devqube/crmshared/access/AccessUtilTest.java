package com.devqube.crmshared.access;

import com.devqube.crmshared.access.annotation.AuthField;
import com.devqube.crmshared.access.annotation.AuthFields;
import com.devqube.crmshared.access.annotation.AuthFilterEnabled;
import com.devqube.crmshared.access.annotation.AuthIgnoreChild;
import com.devqube.crmshared.access.model.PermissionDto;
import com.devqube.crmshared.access.model.ProfileDto;
import com.devqube.crmshared.access.model.WebControlDto;
import com.devqube.crmshared.access.util.AccessUtil;
import lombok.Builder;
import lombok.Getter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AccessUtilTest {
    @Test
    public void shouldChangeId() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().id(1L).build();
        AccessUtilTestClass1 newObj = AccessUtilTestClass1.builder().id(2L).build();

        ProfileDto profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("id").state(PermissionDto.State.WRITE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        AccessUtilTestClass1 test = AccessUtil.assign(fromDb, newObj, profile, "test1");

        assertEquals(newObj.getId().longValue(), test.getId().longValue());
    }

    @Test
    public void shouldNotChangeId() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().id(1L).build();
        AccessUtilTestClass1 newObj = AccessUtilTestClass1.builder().id(2L).build();

        ProfileDto profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("id").state(PermissionDto.State.INVISIBLE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        AccessUtilTestClass1 test = AccessUtil.assign(fromDb, newObj, profile, "test1");

        assertEquals(fromDb.getId().longValue(), test.getId().longValue());

        profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("id").state(PermissionDto.State.READ_ONLY).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        test = AccessUtil.assign(fromDb, newObj, profile, "test1");

        assertEquals(fromDb.getId().longValue(), test.getId().longValue());
    }

    @Test
    public void shouldNotChangeIdWhenControllerNotFound() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().id(1L).build();
        AccessUtilTestClass1 newObj = AccessUtilTestClass1.builder().id(2L).build();

        ProfileDto profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("id").state(PermissionDto.State.WRITE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        AccessUtilTestClass1 test = AccessUtil.assign(fromDb, newObj, profile, "test1000");

        assertEquals(fromDb.getId().longValue(), test.getId().longValue());
    }

    @Test
    public void shouldNotChangeName3WhenNoAnnotations() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().id(1L).name3("fromDb").build();
        AccessUtilTestClass1 newObj = AccessUtilTestClass1.builder().id(2L).name3("modified").build();

        ProfileDto profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("id").state(PermissionDto.State.WRITE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        AccessUtilTestClass1 test = AccessUtil.assign(fromDb, newObj, profile, "test1");

        assertEquals(newObj.getId().longValue(), test.getId().longValue());
        assertEquals(fromDb.getName3(), test.getName3());
    }

    @Test
    public void shouldChangeNameWhenAnnotatedAsGroup() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().name("raz").build();
        AccessUtilTestClass1 newObj = AccessUtilTestClass1.builder().name("dwa").build();

        ProfileDto profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("name").state(PermissionDto.State.WRITE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        AccessUtilTestClass1 test = AccessUtil.assign(fromDb, newObj, profile, "test2");

        assertEquals(newObj.getName(), test.getName());
    }

    @Test
    public void shouldNotChangeNameWhenAnnotatedAsGroup() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().name("raz").build();
        AccessUtilTestClass1 newObj = AccessUtilTestClass1.builder().name("dwa").build();

        ProfileDto profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("name").state(PermissionDto.State.INVISIBLE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        AccessUtilTestClass1 test = AccessUtil.assign(fromDb, newObj, profile, "test3");

        assertEquals(fromDb.getName(), test.getName());

        profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("name").state(PermissionDto.State.READ_ONLY).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        test = AccessUtil.assign(fromDb, newObj, profile, "test2");

        assertEquals(fromDb.getName(), test.getName());
    }

    @Test
    public void shouldChangeValueInChildModel() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().testClass2(
                AccessUtilTestClass2.builder().value("raz").build()).build();
        AccessUtilTestClass1 newObj = AccessUtilTestClass1.builder().testClass2(
                AccessUtilTestClass2.builder().value("dwa").build()).build();


        ProfileDto profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("value").state(PermissionDto.State.WRITE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());


        AccessUtilTestClass1 test = AccessUtil.assign(fromDb, newObj, profile, "test6");

        assertEquals(newObj.getTestClass2().getValue(), test.getTestClass2().getValue());
    }

    @Test
    public void shouldNotChangeValueInChildModel() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().testClass2(
                AccessUtilTestClass2.builder().value("raz").build()).build();
        AccessUtilTestClass1 newObj = AccessUtilTestClass1.builder().testClass2(
                AccessUtilTestClass2.builder().value("dwa").build()).build();

        ProfileDto profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("value").state(PermissionDto.State.INVISIBLE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        AccessUtilTestClass1 test = AccessUtil.assign(fromDb, newObj, profile, "test6");

        assertEquals(fromDb.getTestClass2().getValue(), test.getTestClass2().getValue());

        profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("value").state(PermissionDto.State.READ_ONLY).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        test = AccessUtil.assign(fromDb, newObj, profile, "test6");

        assertEquals(fromDb.getTestClass2().getValue(), test.getTestClass2().getValue());
    }

    @Test
    public void shouldChangeValueInChildModelWhenIgnore() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().testClass2Ignore(
                AccessUtilTestClass2.builder().value("raz").build()).build();
        AccessUtilTestClass1 newObj = AccessUtilTestClass1.builder().testClass2Ignore(
                AccessUtilTestClass2.builder().value("dwa").build()).build();

        ProfileDto profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("value").state(PermissionDto.State.INVISIBLE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        AccessUtilTestClass1 test = AccessUtil.assign(fromDb, newObj, profile, "test6");

        assertEquals(newObj.getTestClass2Ignore().getValue(), test.getTestClass2Ignore().getValue());

        profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("value").state(PermissionDto.State.READ_ONLY).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        test = AccessUtil.assign(fromDb, newObj, profile, "test6");

        assertEquals(newObj.getTestClass2Ignore().getValue(), test.getTestClass2Ignore().getValue());
    }

    @Test
    public void shouldChangeValueInChildModelLvl2() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().testClass2(
                AccessUtilTestClass2.builder().testClass3(
                        AccessUtilTestClass3.builder().nValue("raz").build()
                ).build()).build();
        AccessUtilTestClass1 newObj = AccessUtilTestClass1.builder().testClass2(
                AccessUtilTestClass2.builder().testClass3(
                        AccessUtilTestClass3.builder().nValue("dwa").build()
                ).build()).build();


        ProfileDto profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("nvalue").state(PermissionDto.State.WRITE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());


        AccessUtilTestClass1 test = AccessUtil.assign(fromDb, newObj, profile, "test8");

        assertEquals(newObj.getTestClass2().getTestClass3().getNValue(), test.getTestClass2().getTestClass3().getNValue());
    }

    @Test
    public void shouldNotChangeValueInChildModelLvl2() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().testClass2(
                AccessUtilTestClass2.builder().testClass3(
                        AccessUtilTestClass3.builder().nValue("raz").build()
                ).build()).build();
        AccessUtilTestClass1 newObj = AccessUtilTestClass1.builder().testClass2(
                AccessUtilTestClass2.builder().testClass3(
                        AccessUtilTestClass3.builder().nValue("dwa").build()
                ).build()).build();

        ProfileDto profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("nvalue").state(PermissionDto.State.INVISIBLE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        AccessUtilTestClass1 test = AccessUtil.assign(fromDb, newObj, profile, "test8");

        assertEquals(fromDb.getTestClass2().getTestClass3().getNValue(), test.getTestClass2().getTestClass3().getNValue());

        profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("nvalue").state(PermissionDto.State.READ_ONLY).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        test = AccessUtil.assign(fromDb, newObj, profile, "test8");

        assertEquals(fromDb.getTestClass2().getTestClass3().getNValue(), test.getTestClass2().getTestClass3().getNValue());
    }

    @Test
    public void shouldFilterNameInResult() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().id(1L).name("test").build();

        ProfileDto profile = ProfileUtil.getProfile(Arrays.asList(
                ProfileUtil.PermissionInitObj.builder().frontendId("id").state(PermissionDto.State.READ_ONLY).depenendsOn(WebControlDto.ModuleOperation.VIEW).build(),
                ProfileUtil.PermissionInitObj.builder().frontendId("name").state(PermissionDto.State.INVISIBLE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        AccessUtilTestClass1 test = AccessUtil.filterValue(fromDb, profile, "test1");
        assertEquals(test.getId(), fromDb.getId());
        assertNull(test.getName());
    }

    @Test
    public void shouldFilterNameInResultIfDependsOnUnchecked() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().id(1L).name("test").build();

        ProfileDto profile = ProfileUtil.getProfile(Arrays.asList(
                ProfileUtil.PermissionInitObj.builder().frontendId("id").state(PermissionDto.State.WRITE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build(),
                ProfileUtil.PermissionInitObj.builder().frontendId("name").state(PermissionDto.State.WRITE).depenendsOn(WebControlDto.ModuleOperation.CREATE).build()
        ), ProfileUtil.ProfileInitObj.builder().view(false).create(false).build());

        AccessUtilTestClass1 test = AccessUtil.filterValue(fromDb, profile, "test1");
        assertEquals(test.getId(), fromDb.getId());
        assertNull(test.getName());
    }

    @Test
    public void shouldNotChangeNameInResultIfDependsOnUnchecked() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().id(1L).build();
        AccessUtilTestClass1 newObj = AccessUtilTestClass1.builder().id(2L).build();

        ProfileDto profile = ProfileUtil.getProfile(Collections.singletonList(
                ProfileUtil.PermissionInitObj.builder().frontendId("id").state(PermissionDto.State.WRITE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(false).build());

        AccessUtilTestClass1 test = AccessUtil.assign(fromDb, newObj, profile, "test1");

        assertEquals(fromDb.getId().longValue(), test.getId().longValue());
    }

    @Test
    public void shouldFilterNameInResultList() {
        List<AccessUtilTestClass1> fromDb = new ArrayList<>();
        AccessUtilTestClass1 elem = AccessUtilTestClass1.builder().id(1L).name("test").build();
        fromDb.add(elem);

        ProfileDto profile = ProfileUtil.getProfile(Arrays.asList(
                ProfileUtil.PermissionInitObj.builder().frontendId("id").state(PermissionDto.State.READ_ONLY).depenendsOn(WebControlDto.ModuleOperation.VIEW).build(),
                ProfileUtil.PermissionInitObj.builder().frontendId("name").state(PermissionDto.State.INVISIBLE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        List<AccessUtilTestClass1> test = AccessUtil.filterCollection(fromDb, profile, "test1");
        assertEquals(test.get(0).getId(), fromDb.get(0).getId());
        assertNull(test.get(0).getName());
    }

    @Test
    public void shouldFilterValue2InResult() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().name("test")
                .testClass2(AccessUtilTestClass2.builder().value2("test").build()).build();

        ProfileDto profile = ProfileUtil.getProfile(Arrays.asList(
                ProfileUtil.PermissionInitObj.builder().frontendId("value2").state(PermissionDto.State.INVISIBLE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build(),
                ProfileUtil.PermissionInitObj.builder().frontendId("name").state(PermissionDto.State.WRITE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        AccessUtilTestClass1 test = AccessUtil.filterValue(fromDb, profile, "test9");
        assertEquals(test.getName(), fromDb.getName());
        assertNull(test.getTestClass2().getValue2());
    }

    @Test
    public void testWhenArgIsNull() throws IllegalAccessException {
        AccessUtilTestClass1 fromDb = AccessUtilTestClass1.builder().name("test")
                .testClass2(AccessUtilTestClass2.builder().value2("test").build()).build();

        List<AccessUtilTestClass1> fromDbCollection = new ArrayList<>();
        AccessUtilTestClass1 elem = AccessUtilTestClass1.builder().id(1L).name("test").build();
        fromDbCollection.add(elem);


        AccessUtilTestClass1 fromDbAssign = AccessUtilTestClass1.builder().id(1L).build();
        AccessUtilTestClass1 newObjAssign = AccessUtilTestClass1.builder().id(2L).build();

        ProfileDto profile = ProfileUtil.getProfile(Arrays.asList(
                ProfileUtil.PermissionInitObj.builder().frontendId("value2").state(PermissionDto.State.INVISIBLE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build(),
                ProfileUtil.PermissionInitObj.builder().frontendId("name").state(PermissionDto.State.WRITE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        assertNull(AccessUtil.filterValue(null, profile, "test9"));
        assertEquals(fromDb, AccessUtil.filterValue(fromDb, null, "test9"));
        assertEquals(fromDb, AccessUtil.filterValue(fromDb, profile, null));

        assertNull(AccessUtil.filterCollection(null, profile, "test9"));
        assertEquals(fromDbCollection, AccessUtil.filterCollection(fromDbCollection, null, "test9"));
        assertEquals(fromDbCollection, AccessUtil.filterCollection(fromDbCollection, profile, null));

        assertEquals(newObjAssign, AccessUtil.assign(null, newObjAssign, profile, "test9"));
        assertNull(AccessUtil.assign(fromDbAssign, null, profile, "test9"));
        assertEquals(newObjAssign, AccessUtil.assign(fromDbAssign, newObjAssign, null, "test9"));
        assertEquals(newObjAssign, AccessUtil.assign(fromDbAssign, newObjAssign, profile, null));

        assertFalse(AccessUtil.isFilterEnabled(null));
    }


    @Test
    public void shouldReturnTrueForCollectionTypes() {
        assertTrue(AccessUtil.isCollection(new ArrayList<>()));
        assertTrue(AccessUtil.isCollection(new HashSet<>()));
        assertTrue(AccessUtil.isCollection(new LinkedList<>()));
    }

    @Test
    public void shouldReturnFalseForNoCollectionTypes() {
        assertFalse(AccessUtil.isCollection(new AccessUtilTestClass1(1L, "", null, null, null)));
        assertFalse(AccessUtil.isCollection(1));
        assertFalse(AccessUtil.isCollection("testtesttest"));
    }

    @Test
    public void shouldReturnUserWritePermissions() {

        ProfileDto profile = ProfileUtil.getProfile(Arrays.asList(
                ProfileUtil.PermissionInitObj.builder().frontendId("test1").state(PermissionDto.State.READ_ONLY).depenendsOn(WebControlDto.ModuleOperation.VIEW).build(),
                ProfileUtil.PermissionInitObj.builder().frontendId("test2").state(PermissionDto.State.WRITE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build(),
                ProfileUtil.PermissionInitObj.builder().frontendId("test3").state(PermissionDto.State.INVISIBLE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build(),
                ProfileUtil.PermissionInitObj.builder().frontendId("test4").state(PermissionDto.State.WRITE).depenendsOn(WebControlDto.ModuleOperation.VIEW).build()
        ), ProfileUtil.ProfileInitObj.builder().view(true).build());

        List<String> writePermissionDto = new ArrayList<>(AccessUtil.getWritePermissions(profile));

        assertEquals(2, writePermissionDto.size());
        assertTrue(writePermissionDto.contains("test2"));
        assertTrue(writePermissionDto.contains("test4"));
    }

    // classes used in tests

    @Builder
    @Getter
    @AuthFilterEnabled
    public static class AccessUtilTestClass1 {

        @AuthField(controller = "test1", frontendId = "id")
        private Long id;

        @AuthFields(list = {
                @AuthField(controller = "test1", frontendId = "name"),
                @AuthField(controller = "test2", frontendId = "name"),
                @AuthField(controller = "test3", frontendId = "name"),
                @AuthField(controller = "test9", frontendId = "name")
        })
        private String name;

        private String name3;

        @AuthFields(list = {
                @AuthField(controller = "test4", frontendId = "testclass2"),
                @AuthField(controller = "test9")
        })
        private AccessUtilTestClass2 testClass2;

        @AuthFields(list = {
                @AuthField(controller = "test4", frontendId = "testclass2"),
                @AuthField(controller = "test9"),
                @AuthField(controller = "test6")
        })
        @AuthIgnoreChild
        private AccessUtilTestClass2 testClass2Ignore;
    }

    @Builder
    @Getter
    @AuthFilterEnabled
    public static class AccessUtilTestClass2 {
        @AuthField(controller = "test6", frontendId = "value")
        private String value;
        @AuthField(controller = "test9", frontendId = "value2")
        private String value2;

        private AccessUtilTestClass3 testClass3;
    }

    @Builder
    @Getter
    @AuthFilterEnabled
    public static class AccessUtilTestClass3 {
        @AuthField(controller = "test8", frontendId = "nvalue")
        private String nValue;
    }
}
