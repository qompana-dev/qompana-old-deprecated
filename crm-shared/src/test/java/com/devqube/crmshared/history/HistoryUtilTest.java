package com.devqube.crmshared.history;


import com.devqube.crmshared.history.annotation.SaveHistory;
import com.devqube.crmshared.history.annotation.SaveHistoryIgnore;
import com.devqube.crmshared.history.dto.ObjectChangedDto;
import com.devqube.crmshared.history.dto.ObjectHistoryType;
import com.devqube.crmshared.history.exception.SaveCrmChangesException;
import com.devqube.crmshared.history.util.SaveCrmChangesUtil;
import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.*;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class HistoryUtilTest {
    @Test(expected = SaveCrmChangesException.class)
    public void shouldThrowExceptionIfObjectIsNullWhenGettingObjectType() throws SaveCrmChangesException {
        SaveCrmChangesUtil.getObjectType(null);
    }
    @Test(expected = SaveCrmChangesException.class)
    public void shouldThrowExceptionIfObjectTypeNotFoundWhenGettingObjectType() throws SaveCrmChangesException {
        SaveCrmChangesUtil.getObjectType(new NotExistObject());
    }
    @Test
    public void shouldGetObjectType() throws SaveCrmChangesException {
        TestObject testObject = new TestObject();
        ObjectHistoryType result = SaveCrmChangesUtil.getObjectType(testObject);
        assertEquals(ObjectHistoryType.TEST, result);
    }

    @Test
    public void shouldGetCorrectObject() throws IllegalAccessException, JSONException, SaveCrmChangesException {
        TestObject testObject = getTestObject();
        ObjectChangedDto result = SaveCrmChangesUtil.getJson(testObject);
        assertNotNull(result);

        //should get id from field with @Id annotation
        assertEquals(testObject.getIdValue(), result.getId());

        JSONObject jsonObject = new JSONObject(result.getJson());

        //should ignore ManyToOne, OneToMany, ManyToMany, OneToOne annotations
        assertFalse(jsonObject.has("value3"));
        assertFalse(jsonObject.has("value4"));
        assertFalse(jsonObject.has("value5"));
        assertFalse(jsonObject.has("value6"));

        //should get fields without any annotation
        assertEquals(testObject.getValue1(), jsonObject.getString("value1"));
        //should return empty string when value is null
        assertEquals(Strings.nullToEmpty(testObject.getValue2()), jsonObject.getString("value2"));

        //should get fields with id annotation
        assertEquals(testObject.getIdValue(), jsonObject.getString("idValue"));

        //should ignore field with SaveHistoryIgnore annotation
        assertFalse(jsonObject.has("value7"));

        //should return values with SaveHistory annotation
        assertEquals(")) childListName1 test childListSurname1; )) childListName2 test childListSurname2", jsonObject.getString("childList"));
        assertEquals("childValue1 empty", jsonObject.getString("child"));
    }

    private TestObject getTestObject() {
        return TestObject.builder()
                .idValue("idValuet")
                .value1("value1t")
                .value2(null)
                .value3("value3t")
                .value4("value4t")
                .value5("value5t")
                .value6("value6t")
                .value7("value7t")
                .childList(Arrays.asList(
                        ChildObject.builder().name("childListName1").surname("childListSurname1").value("childListValue1").build(),
                        ChildObject.builder().name("childListName2").surname("childListSurname2").value("childListValue2").build()
                ))
                .child(ChildObject.builder().name(null).surname("childSurname1").value("childValue1").build())
                .build();
    }

    @Builder
    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public static class TestObject {
        private String value1;
        private String value2;
        @OneToMany
        private String value3;
        @OneToOne
        private String value4;
        @ManyToOne
        private String value5;
        @ManyToMany
        private String value6;
        @SaveHistoryIgnore
        private String value7;
        @Id
        private String idValue;
        @SaveHistory(fields = {"name", "surname"}, message = ")) {{name}} test {{surname}}", separator = "; ")
        @ManyToMany
        private List<ChildObject> childList;
        @SaveHistory(fields = {"name", "value"}, message = "{{value}} empty{{name}}")
        @OneToOne
        private ChildObject child;
    }

    @Builder
    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public static class ChildObject {
        private String name;
        private String surname;
        private String value;
    }

    public static class NotExistObject {
    }
}
