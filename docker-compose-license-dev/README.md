Docker Compose for License Dev Environment

before execute docker-compose you have to perform the following steps:
1. Build crm-license-frontend
2. Copy content of "dist" folder inside crm-license-frontend into src/main/resources/static of crm-license-service
3. Build crm-license-service
4. Run docker-compose
5. Execute sqls inside crm-license-service-db-dev container to fill DB with test data.