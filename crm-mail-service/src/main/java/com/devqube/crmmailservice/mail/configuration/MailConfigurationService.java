package com.devqube.crmmailservice.mail.configuration;

import com.devqube.crmmailservice.autoconfiguration.AutoConfigurationService;
import com.devqube.crmmailservice.autoconfiguration.dto.AutoConfigurationRequestDto;
import com.devqube.crmmailservice.autoconfiguration.dto.AutoConfigurationResponseDto;
import com.devqube.crmmailservice.autoconfiguration.dto.ConnectionDetailsDto;
import com.devqube.crmmailservice.autoconfiguration.exception.EmailConfigurationNotFound;
import com.devqube.crmmailservice.encryptor.AesEncryptor;
import com.devqube.crmmailservice.internalClients.user.UserService;
import com.devqube.crmmailservice.mail.configuration.web.dto.SimpleMailConfigurationDto;
import com.devqube.crmmailservice.mail.connection.ImapConnectionHolder;
import com.devqube.crmmailservice.mail.exception.ConfigurationExistException;
import com.devqube.crmmailservice.mail.exception.TooManyConfigurationsException;
import com.devqube.crmmailservice.mail.list.MailFolderRepository;
import com.devqube.crmmailservice.mail.list.MailHeaderRepository;
import com.devqube.crmmailservice.mail.receiver.MailReceiverService;
import com.devqube.crmmailservice.mail.sender.MailSenderService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MailConfigurationService {
    @Value("${crm.mail-configuration.max:5}")
    private Integer maxConfigurationsCountPerUser;

    private final MailConfigurationRepository mailConfigurationRepository;
    private final UserService userService;
    private final MailReceiverService mailReceiverService;
    private final MailSenderService mailSenderService;
    private final AesEncryptor aesEncryptor;
    private final MailFolderRepository mailFolderRepository;
    private final MailHeaderRepository mailHeaderRepository;
    private final ImapConnectionHolder imapConnectionHolder;
    private final AutoConfigurationService autoConfigurationService;

    public MailConfigurationService(MailConfigurationRepository mailConfigurationRepository, UserService userService,
                                    MailReceiverService mailReceiverService, MailSenderService mailSenderService, AesEncryptor aesEncryptor, MailFolderRepository mailFolderRepository, MailHeaderRepository mailHeaderRepository, ImapConnectionHolder imapConnectionHolder, AutoConfigurationService autoConfigurationService) {
        this.mailConfigurationRepository = mailConfigurationRepository;
        this.userService = userService;
        this.mailReceiverService = mailReceiverService;
        this.mailSenderService = mailSenderService;
        this.aesEncryptor = aesEncryptor;
        this.mailFolderRepository = mailFolderRepository;
        this.mailHeaderRepository = mailHeaderRepository;
        this.imapConnectionHolder = imapConnectionHolder;
        this.autoConfigurationService = autoConfigurationService;
    }

    @Transactional(rollbackOn = {BadRequestException.class, EntityNotFoundException.class})
    public MailConfiguration changeActiveConfiguration(String loggedAccountEmail, Long configurationId) throws BadRequestException, EntityNotFoundException {
        Long userId = userService.getUserId(loggedAccountEmail);
        List<MailConfiguration> allByAccountId = mailConfigurationRepository.findAllByAccountId(userId);
        if (allByAccountId.stream().noneMatch(c -> c.getId().equals(configurationId))) {
            log.info("configuration {} for user {} not found", configurationId, loggedAccountEmail);
            throw new EntityNotFoundException();
        }
        List<MailConfiguration> saved = mailConfigurationRepository.saveAll(allByAccountId.stream().peek(c -> c.setActive(c.getId().equals(configurationId))).collect(Collectors.toList()));
        closeConnectionForUser(userId);
        return saved.stream().filter(c -> c.getId().equals(configurationId)).findFirst().orElseThrow(EntityNotFoundException::new);
    }

    public MailConfiguration findActiveConfiguration(String loggedAccountEmail, Long accountId) throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        validatePermission(accountId, loggedAccountEmail);
        MailConfiguration mailConfiguration = mailConfigurationRepository.findByAccountIdAndActiveTrue(accountId).orElseThrow(EntityNotFoundException::new);
        mailConfiguration.setPassword(null);
        return mailConfiguration;
    }

    public MailConfiguration getConfiguration(String loggedAccountEmail, Long id) throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        Long accountId = userService.getUserId(loggedAccountEmail);
        MailConfiguration mailConfiguration = mailConfigurationRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        if (!mailConfiguration.getAccountId().equals(accountId)) {
            throw new PermissionDeniedException();
        }
        mailConfiguration.setPassword(null);
        return mailConfiguration;
    }

    public List<MailConfiguration> findAllByAccountId(String loggedAccountEmail, Long accountId) throws BadRequestException, PermissionDeniedException {
        validatePermission(accountId, loggedAccountEmail);
        return mailConfigurationRepository.findAllByAccountId(accountId)
                .stream().peek(c -> c.setPassword(null)).collect(Collectors.toList());
    }

    @Transactional(rollbackOn = {BadRequestException.class, PermissionDeniedException.class})
    public void removeConfiguration(String loggedAccountEmail, Long id) throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        MailConfiguration mailConfiguration = mailConfigurationRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        Long accountId = userService.getUserId(loggedAccountEmail);
        if (!mailConfiguration.getAccountId().equals(accountId)) {
            throw new PermissionDeniedException();
        }
        mailFolderRepository.deleteAllByConfigurationId(id);
        mailHeaderRepository.deleteAllByConfigurationId(id);
        mailConfigurationRepository.deleteById(id);
        imapConnectionHolder.removeStoredConnectionById(id);
        closeConnectionForUser(accountId);
        setConfigurationAsActiveWhenAnotherNotFound(accountId, id);
    }

    private void setConfigurationAsActiveWhenAnotherNotFound(Long accountId, Long removedConfigurationId) {
        List<MailConfiguration> allByAccountId = mailConfigurationRepository.findAllByAccountId(accountId);
        boolean notContainActiveConfigurations = allByAccountId.stream().noneMatch(c -> c.getActive() && !c.getId().equals(removedConfigurationId));
        if (allByAccountId.size() > 0 && notContainActiveConfigurations) {
            MailConfiguration mailConfiguration = allByAccountId.stream().findFirst().get();
            mailConfiguration.setActive(true);
            mailConfigurationRepository.save(mailConfiguration);
        }
    }

    public Boolean configurationExist(String loggedAccountEmail, Long id) throws BadRequestException, PermissionDeniedException {
        if (!userService.getUserId(loggedAccountEmail).equals(id)) {
            throw new PermissionDeniedException();
        }
        return mailConfigurationRepository.existsByAccountIdAndActiveTrue(id);
    }


    @Transactional(rollbackOn = {BadRequestException.class, PermissionDeniedException.class, TooManyConfigurationsException.class, ConfigurationExistException.class, EmailConfigurationNotFound.class})
    public MailConfiguration saveSimpleConfiguration(String loggedAccountEmail, SimpleMailConfigurationDto simpleMailConfigurationDto) throws BadRequestException, PermissionDeniedException, ConfigurationExistException, TooManyConfigurationsException, EmailConfigurationNotFound {
        AutoConfigurationResponseDto mailConfiguration = autoConfigurationService.getMailConfiguration(AutoConfigurationRequestDto.builder()
                .username(simpleMailConfigurationDto.getUsername())
                .password(simpleMailConfigurationDto.getPassword())
                .build());
        return save(loggedAccountEmail, convert(mailConfiguration, simpleMailConfigurationDto));
    }

    @Transactional(rollbackOn = {BadRequestException.class, PermissionDeniedException.class, TooManyConfigurationsException.class, ConfigurationExistException.class})
    public MailConfiguration save(String loggedAccountEmail, MailConfiguration configuration) throws BadRequestException, PermissionDeniedException, ConfigurationExistException, TooManyConfigurationsException {
        Long accountId = userService.getUserId(loggedAccountEmail);
        configuration.setAccountId(accountId);
        Long existingConfigurations = mailConfigurationRepository
                .countAllByAccountIdAndUsernameAndImapServerAddressAndSmtpServerAddress(configuration.getAccountId(), configuration.getUsername(), configuration.getImapServerAddress(), configuration.getSmtpServerAddress());

        if (existingConfigurations > 0L) {
            throw new ConfigurationExistException();
        }

        if (mailConfigurationRepository.countAllByAccountId(accountId) >= maxConfigurationsCountPerUser) {
            throw new TooManyConfigurationsException();
        }

        configuration.setPassword(aesEncryptor.encrypt(configuration.getPassword()));
        configuration.setLastUpdate(null);
        configuration.setActive(!mailConfigurationRepository.existsByAccountIdAndActiveTrue(configuration.getAccountId()));

        validateConfigurationAndThrowIfInvalid(configuration);

        MailConfiguration saved = mailConfigurationRepository.save(configuration);
        closeConnectionForUser(accountId);
        return saved;
    }


    @Transactional(rollbackOn = {BadRequestException.class, PermissionDeniedException.class, ConfigurationExistException.class, EntityNotFoundException.class, EmailConfigurationNotFound.class})
    public MailConfiguration updateSimpleConfiguration(String loggedAccountEmail, Long configurationId, SimpleMailConfigurationDto simpleMailConfigurationDto) throws BadRequestException, PermissionDeniedException, ConfigurationExistException, EmailConfigurationNotFound, EntityNotFoundException {
        AutoConfigurationResponseDto mailConfiguration = autoConfigurationService.getMailConfiguration(AutoConfigurationRequestDto.builder()
                .username(simpleMailConfigurationDto.getUsername())
                .password(simpleMailConfigurationDto.getPassword())
                .build());
        return update(loggedAccountEmail, configurationId, convert(mailConfiguration, simpleMailConfigurationDto));
    }

    @Transactional(rollbackOn = {BadRequestException.class, PermissionDeniedException.class, ConfigurationExistException.class, EntityNotFoundException.class})
    public MailConfiguration update(String loggedAccountEmail, Long configurationId, MailConfiguration configuration) throws BadRequestException, PermissionDeniedException, ConfigurationExistException, EntityNotFoundException {
        MailConfiguration fromDb = mailConfigurationRepository.findById(configurationId).orElseThrow(EntityNotFoundException::new);
        Long accountId = userService.getUserId(loggedAccountEmail);
        if (!fromDb.getAccountId().equals(accountId)) {
            throw new PermissionDeniedException();
        }

        Long existingConfigurations = mailConfigurationRepository
                .countAllByAccountIdAndUsernameAndImapServerAddressAndSmtpServerAddressAndIdNot(accountId, configuration.getUsername(), configuration.getImapServerAddress(), configuration.getSmtpServerAddress(), configurationId);

        if (existingConfigurations > 0L) {
            throw new ConfigurationExistException();
        }

        configuration.setPassword(aesEncryptor.encrypt(configuration.getPassword()));
        configuration.setLastUpdate(null);

        updateConfigurationFromDb(fromDb, configuration);
        MailConfiguration saved = mailConfigurationRepository.save(fromDb);
        closeConnectionForUser(accountId);
        return saved;
    }

    private void validatePermission(Long accountId, String loggedAccountEmail) throws PermissionDeniedException, BadRequestException {
        if (!userService.getUserId(loggedAccountEmail).equals(accountId)) {
            throw new PermissionDeniedException();
        }
    }

    private void validateConfigurationAndThrowIfInvalid(MailConfiguration configuration) throws BadRequestException {
        if (!mailReceiverService.isImapConnectionEstablished(configuration)) {
            throw new BadRequestException("cannot establish imap connection");
        }
        if (!mailSenderService.isSmtpConnectionEstablished(configuration)) {
            throw new BadRequestException("cannot establish smtp connection");
        }
    }

    private void updateConfigurationFromDb(MailConfiguration fromDb, MailConfiguration configuration) {
        fromDb.setImapServerAddress(configuration.getImapServerAddress());
        fromDb.setImapServerPort(configuration.getImapServerPort());
        fromDb.setImapSecurityType(configuration.getImapSecurityType());
        fromDb.setSmtpServerAddress(configuration.getSmtpServerAddress());
        fromDb.setSmtpServerPort(configuration.getSmtpServerPort());
        fromDb.setSmtpSecurityType(configuration.getSmtpSecurityType());
        fromDb.setUsername(configuration.getUsername());
        fromDb.setPassword(configuration.getPassword());
        fromDb.setLastUpdate(configuration.getLastUpdate());
    }

    private void closeConnectionForUser(Long accountId) {
        List<MailConfiguration> allByAccountId = mailConfigurationRepository.findAllByAccountId(accountId);
        allByAccountId.forEach(configuration -> imapConnectionHolder.removeStoredConnectionById(configuration.getId()));
    }

    private MailConfiguration convert(AutoConfigurationResponseDto responseDto, SimpleMailConfigurationDto simpleMailConfigurationDto) {
        MailConfiguration mailConfiguration = new MailConfiguration();

        mailConfiguration.setImapServerAddress(responseDto.getConnectionDetailsDto().getImapServerAddress());
        mailConfiguration.setImapServerPort(responseDto.getConnectionDetailsDto().getImapServerPort());
        mailConfiguration.setImapSecurityType(responseDto.getConnectionDetailsDto().getImapSecurityType()
                .equals(ConnectionDetailsDto.SecurityType.SSL) ? MailConfiguration.SecurityType.SSL : MailConfiguration.SecurityType.TLS);

        mailConfiguration.setSmtpServerAddress(responseDto.getConnectionDetailsDto().getSmtpServerAddress());
        mailConfiguration.setSmtpServerPort(responseDto.getConnectionDetailsDto().getSmtpServerPort());
        mailConfiguration.setSmtpSecurityType(responseDto.getConnectionDetailsDto().getSmtpSecurityType()
                .equals(ConnectionDetailsDto.SecurityType.SSL) ? MailConfiguration.SecurityType.SSL : MailConfiguration.SecurityType.TLS);

        mailConfiguration.setId(simpleMailConfigurationDto.getId());
        mailConfiguration.setAccountId(simpleMailConfigurationDto.getAccountId());
        mailConfiguration.setUsername(simpleMailConfigurationDto.getUsername());
        mailConfiguration.setPassword(simpleMailConfigurationDto.getPassword());
        mailConfiguration.setActive(false);

        return mailConfiguration;
    }
}
