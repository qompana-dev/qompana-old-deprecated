package com.devqube.crmmailservice.mail.receiver;

import com.devqube.crmmailservice.mail.configuration.MailConfiguration;
import com.devqube.crmmailservice.mail.exception.MailException;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AsyncMailReceiverService {

    private final MailReceiverService mailReceiverService;


    public AsyncMailReceiverService(MailReceiverService mailReceiverService) {
        this.mailReceiverService = mailReceiverService;
    }

    @Async("mailReceiverTaskExecutor")
    public void refreshEmailsForConfiguration(MailConfiguration configuration) throws MailException, KafkaSendMessageException {
        this.mailReceiverService.refreshEmailsForConfiguration(configuration);
    }

    @Async("mailReceiverTaskExecutor")
    public void synchronizeEmailsForConfiguration(MailConfiguration configuration) throws MailException, KafkaSendMessageException {
        this.mailReceiverService.synchronizeEmailsForConfiguration(configuration);
    }
}
