package com.devqube.crmmailservice.mail.configuration.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SimpleMailConfigurationDto {
    private Long id;

    @NotNull
    private Long accountId;

    @NotNull
    private String username;

    @NotNull
    private String password;
}
