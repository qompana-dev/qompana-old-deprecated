package com.devqube.crmmailservice.mail.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ConfigurationExistException extends Exception {
    public ConfigurationExistException() {
    }

    public ConfigurationExistException(String message) {
        super(message);
    }
}
