package com.devqube.crmmailservice.mail.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MailStorage {

    @Id
    @SequenceGenerator(name = "mail_storage_seq", sequenceName = "mail_storage_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mail_storage_seq")
    private Long id;
    private String mailFileName;

    private Long messageId;
    private Long mailHeaderId;
    private String subject;
    @Column(name = "\"from\"")
    private String from;
    @Column(name = "\"to\"")
    private String to;
    private String cc;
    private String bcc;
    private LocalDateTime receivedDate;
    private String folderName;

    private Long accountId;
    private String username;
}
