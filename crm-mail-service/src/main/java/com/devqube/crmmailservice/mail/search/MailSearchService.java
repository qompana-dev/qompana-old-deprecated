package com.devqube.crmmailservice.mail.search;

import com.devqube.crmmailservice.internalClients.business.BusinessService;
import com.devqube.crmmailservice.internalClients.user.UserService;
import com.devqube.crmmailservice.mail.list.MailHeaderRepository;
import com.devqube.crmmailservice.mail.model.MailHeader;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailHeaderDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.search.CrmObjectType;
import com.devqube.crmshared.search.EmailSearch;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class MailSearchService {

    private final UserService userService;
    private final ModelMapper modelMapper;
    private final MailHeaderRepository mailHeaderRepository;
    private final BusinessService businessService;

    public MailSearchService(MailHeaderRepository mailHeaderRepository, @Qualifier("strictModelMapper") ModelMapper modelMapper, UserService userService, BusinessService businessService) {
        this.mailHeaderRepository = mailHeaderRepository;
        this.modelMapper = modelMapper;
        this.userService = userService;
        this.businessService = businessService;
    }


    public List<EmailSearch> searchEmails(String term, String myEmail) throws BadRequestException {
        Long userId = userService.getUserId(myEmail);
        List<String> contactEmailWithoutGdprAgree = businessService.getContactEmailWithoutGdprAgree();
        List<MailHeader> emails = mailHeaderRepository.findByEmailContaining(term, userId);
        return emails.stream()
                .map(email -> MailHeaderDto.toDto(this.modelMapper, email))
                .flatMap(dto -> Stream.concat(dto.getFromAddr().stream(), dto.getToAddr().stream()))
                .filter(address -> !myEmail.equalsIgnoreCase(address.getEmail()))
                .map(address -> new EmailSearch(CrmObjectType.email, address.getName(), address.getEmail()))
                .distinct()
                .filter(address -> address.getEmail().contains(term))
                .filter(address -> !contactEmailWithoutGdprAgree.contains(address.getEmail()))
                .sorted((str1, str2) -> str1.getEmail().compareToIgnoreCase(str2.getEmail()))
                .collect(Collectors.toList());
    }
}
