package com.devqube.crmmailservice.mail.list;

import com.devqube.crmmailservice.internalClients.user.UserService;
import com.devqube.crmmailservice.mail.list.dto.MailFoldersDto;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailHeaderDto;
import com.devqube.crmmailservice.mail.storage.web.dto.MailStorageDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.search.CrmObjectType;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
public class MailController {

    private final UserService userService;
    private final MailQueryService mailQueryService;

    public MailController(MailQueryService mailQueryService, UserService userService) {
        this.mailQueryService = mailQueryService;
        this.userService = userService;
    }

    @GetMapping("{folder}/mail-headers")
    public Page<MailHeaderDto> getMailHeaderList(
            @RequestHeader(value = "Logged-Account-Email") String mail,
            @PathVariable("folder") String folder,
            Pageable pageable) throws BadRequestException {

        Long userId = userService.getUserId(mail);
        return mailQueryService.getMailHeadersInFolder(userId, folder, pageable);
    }

    @GetMapping("mail-headers/attached/{type}/{id}")
    public ResponseEntity<Page<MailHeaderDto>> getAttachedMailHeaderList(
            @ApiParam(value = "The association type",required=true, defaultValue="null") @PathVariable("type") CrmObjectType type,
            @ApiParam(value = "The association ID",required=true) @PathVariable("id") Long id,
            Pageable pageable) {
        try {
            return ResponseEntity.ok(mailQueryService.getAttachedMailHeaders(type, id, pageable));
        } catch (BadRequestException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/mail-folders")
    public MailFoldersDto getConfiguration(@RequestHeader(value = "Logged-Account-Email") String mail) throws BadRequestException, EntityNotFoundException {
        Long userId = userService.getUserId(mail);
        return mailQueryService.getMailFolders(userId);
    }

}
