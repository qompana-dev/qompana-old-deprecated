package com.devqube.crmmailservice.mail.model;


import com.devqube.crmmailservice.mail.configuration.MailConfiguration;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MailHeader {

    @Id
    @SequenceGenerator(name = "mail_header_seq", sequenceName = "mail_header_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mail_header_seq")
    private Long id;

    @EqualsAndHashCode.Include
    private Long messageId;

    private String subject;

    @Column(name = "\"from\"")
    private String from;

    @Column(name = "\"to\"")
    private String to;

    private String cc;

    private String bcc;

    private String mailMessageId;

    @Column(name = "\"references\"")
    private String references;

    private String inReplyTo;

    private LocalDateTime receivedDate;

    @EqualsAndHashCode.Include
    private String folderName;

    @EqualsAndHashCode.Include
    private boolean seen;

    @ManyToOne
    @EqualsAndHashCode.Include
    private MailConfiguration configuration;
}
