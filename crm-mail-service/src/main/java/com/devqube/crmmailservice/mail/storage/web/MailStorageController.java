package com.devqube.crmmailservice.mail.storage.web;

import com.devqube.crmmailservice.mail.receiver.MailReceiverUtil;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailContentDto;
import com.devqube.crmmailservice.mail.storage.MailStorageService;
import com.devqube.crmmailservice.mail.storage.web.dto.MailStorageDto;
import com.devqube.crmshared.access.annotation.AuthController;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.List;

// TODO: auth controller
@RestController
public class MailStorageController implements MailStorageApi {

    private final MailStorageService mailStorageService;

    public MailStorageController(MailStorageService mailStorageService) {
        this.mailStorageService = mailStorageService;
    }

    @Override
    @GetMapping("/mail-storage/headers/lead/{id}")
    @AuthController(readFrontendId = "LeadDetailsComponent.attachedEmails")
    public ResponseEntity<Page<MailStorageDto>> getMailHeadersForLead(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail,
                                                                      @PathVariable("id") Long id,
                                                                      Pageable page) {
        try {
            return ResponseEntity.ok(mailStorageService.getMailHeadersForLead(loggedAccountEmail, id, page));
        } catch (BadRequestException e) {
            return ResponseEntity.badRequest().build();
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @GetMapping("/mail-storage/headers/opportunity/{id}")
    @AuthController(readFrontendId = "OpportunityDetailsComponent.attachedEmails")
    public ResponseEntity<Page<MailStorageDto>> getMailHeadersForOpportunity(
            @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail,
            @PathVariable("id") Long id,
            Pageable page) {
        try {
            return ResponseEntity.ok(mailStorageService.getMailHeadersForOpportunity(loggedAccountEmail, id, page));
        } catch (BadRequestException e) {
            return ResponseEntity.badRequest().build();
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @GetMapping("/mail-storage/headers/contact/{id}")
    @AuthController(readFrontendId = "ContactDetailsComponent.attachedEmails")
    public ResponseEntity<Page<MailStorageDto>> getMailHeadersForContact(
            @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail,
            @PathVariable("id") Long contactId,
            Pageable page) {
        try {
            return ResponseEntity.ok(mailStorageService.getMailHeadersForContact(loggedAccountEmail, contactId, page));
        } catch (BadRequestException e) {
            return ResponseEntity.badRequest().build();
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @PostMapping("/mail-storage/attach/lead/{id}/message/{messageId}")
    @AuthController(readFrontendId = "NavigationMenu.mail")
    public ResponseEntity<Void> attachMessageToLead(@PathVariable("messageId") Long messageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long id) {
        try {
            mailStorageService.attachMessageToLead(messageId, loggedAccountEmail, id);
            return ResponseEntity.ok().build();
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @PostMapping("/mail-storage/attach/opportunity/{id}/message/{messageId}")
    @AuthController(readFrontendId = "NavigationMenu.mail")
    public ResponseEntity<Void> attachMessageToOpportunity(@PathVariable("messageId") Long messageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long id) {
        try {
            mailStorageService.attachMessageToOpportunity(messageId, loggedAccountEmail, id);
            return ResponseEntity.ok().build();
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @PostMapping("/mail-storage/attach/contact/{id}/message/{messageId}")
    @AuthController(readFrontendId = "NavigationMenu.mail")
    public ResponseEntity<Void> attachMessageToContact(@PathVariable("messageId") Long messageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long contactId) {
        try {
            mailStorageService.attachMessageToContact(messageId, loggedAccountEmail, contactId);
            return ResponseEntity.ok().build();
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @DeleteMapping("/mail-storage/content/lead/{id}/mail-storage-id/{mailStorageId}")
    @AuthController(actionFrontendId = "LeadDetailsComponent.detachEmail")
    public ResponseEntity<Void> detachMessageFromLead(@PathVariable("mailStorageId") Long mailStorageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long id) {
        try {
            mailStorageService.detachMessageFromLead(mailStorageId, loggedAccountEmail, id);
            return ResponseEntity.ok().build();
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @DeleteMapping("/mail-storage/content/opportunity/{id}/mail-storage-id/{mailStorageId}")
    @AuthController(actionFrontendId = "OpportunityDetailsComponent.detachEmail")
    public ResponseEntity<Void> detachMessageFromOpportunity(@PathVariable("mailStorageId") Long mailStorageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long id) {
        try {
            mailStorageService.detachMessageFromOpportunity(mailStorageId, loggedAccountEmail, id);
            return ResponseEntity.ok().build();
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @DeleteMapping("/mail-storage/content/contact/{id}/mail-storage-id/{mailStorageId}")
    @AuthController(actionFrontendId = "ContactDetailsComponent.detachEmail")
    public ResponseEntity<Void> detachMessageFromContact(@PathVariable("mailStorageId") Long mailStorageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long contactId) {
        try {
            mailStorageService.detachMessageFromContact(mailStorageId, loggedAccountEmail, contactId);
            return ResponseEntity.ok().build();
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }


    @Override
    @GetMapping("/mail-storage/content/lead/{id}/mail-storage-id/{mailStorageId}")
    @AuthController(readFrontendId = "LeadDetailsComponent.attachedEmails")
    public ResponseEntity<MailContentDto> getMessageForLead(@PathVariable("mailStorageId") Long mailStorageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(mailStorageService.getMessageForLead(loggedAccountEmail, id, mailStorageId));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException | MessagingException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @GetMapping("/mail-storage/content/opportunity/{id}/mail-storage-id/{mailStorageId}")
    @AuthController(readFrontendId = "OpportunityDetailsComponent.attachedEmails")
    public ResponseEntity<MailContentDto> getMessageForOpportunity(@PathVariable("mailStorageId") Long mailStorageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(mailStorageService.getMessageForOpportunity(loggedAccountEmail, id, mailStorageId));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException | MessagingException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @GetMapping("/mail-storage/content/contact/{id}/mail-storage-id/{mailStorageId}")
    @AuthController(readFrontendId = "ContactDetailsComponent.attachedEmails")
    public ResponseEntity<MailContentDto> getMessageForContact(@PathVariable("mailStorageId") Long mailStorageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long contactId) {
        try {
            return ResponseEntity.ok(mailStorageService.getMessageForContact(loggedAccountEmail, contactId, mailStorageId));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException | MessagingException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @PostMapping("/mail-storage/content/lead/{id}/mail-storage-id/{mailStorageId}/file/{fileName}")
    public ResponseEntity<Void> saveFileInCrmFromLead(@PathVariable("mailStorageId") Long mailStorageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long id, @PathVariable("fileName") String fileName) {
        try {
            mailStorageService.saveFileInCrmFromLead(mailStorageId, loggedAccountEmail, id, fileName);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @PostMapping("/mail-storage/content/opportunity/{id}/mail-storage-id/{mailStorageId}/file/{fileName}")
    public ResponseEntity<Void> saveFileInCrmFromOpportunity(@PathVariable("mailStorageId") Long mailStorageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long id, @PathVariable("fileName") String fileName) {
        try {
            mailStorageService.saveFileInCrmFromOpportunity(mailStorageId, loggedAccountEmail, id, fileName);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @PostMapping("/mail-storage/content/contact/{id}/mail-storage-id/{mailStorageId}/file/{fileName}")
    public ResponseEntity<Void> saveFileInCrmFromContact(@PathVariable("mailStorageId") Long mailStorageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long contactId, @PathVariable("fileName") String fileName) {
        try {
            mailStorageService.saveFileInCrmFromContact(mailStorageId, loggedAccountEmail, contactId, fileName);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @GetMapping("/mail-storage/content/lead/{id}/mail-storage-id/{mailStorageId}/file/{fileName}")
    public ResponseEntity<InputStreamResource> downloadAttachmentForLead(@PathVariable("mailStorageId") Long mailStorageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long id, @PathVariable("fileName") String fileName) {
        try {
            return MailReceiverUtil.createFileHttpResponse(fileName, mailStorageService.getAttachmentForLead(mailStorageId, loggedAccountEmail, id, fileName));
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @GetMapping("/mail-storage/content/opportunity/{id}/mail-storage-id/{mailStorageId}/file/{fileName}")
    public ResponseEntity<InputStreamResource> downloadAttachmentForOpportunity(@PathVariable("mailStorageId") Long mailStorageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long id, @PathVariable("fileName") String fileName) {
        try {
            return MailReceiverUtil.createFileHttpResponse(fileName, mailStorageService.getAttachmentForOpportunity(mailStorageId, loggedAccountEmail, id, fileName));
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @GetMapping("/mail-storage/content/contact/{id}/mail-storage-id/{mailStorageId}/file/{fileName}")
    public ResponseEntity<InputStreamResource> downloadAttachmentForContact(@PathVariable("mailStorageId") Long mailStorageId, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable("id") Long contactId, @PathVariable("fileName") String fileName) {
        try {
            return MailReceiverUtil.createFileHttpResponse(fileName, mailStorageService.getAttachmentForContact(mailStorageId, loggedAccountEmail, contactId, fileName));
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
