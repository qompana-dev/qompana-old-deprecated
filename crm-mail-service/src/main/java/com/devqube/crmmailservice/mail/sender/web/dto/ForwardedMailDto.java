package com.devqube.crmmailservice.mail.sender.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ForwardedMailDto {
    private Long messageId;
    private List<String> to;
    private List<String> cc;
    private List<String> bcc;
    private String content;
    private List<NewMailAttachment> attachments;
}
