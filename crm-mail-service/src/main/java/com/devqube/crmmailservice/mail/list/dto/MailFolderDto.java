package com.devqube.crmmailservice.mail.list.dto;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MailFolderDto implements Comparable<MailFolderDto>{
    @EqualsAndHashCode.Include
    private String folderName;
    private Long unreadMessages = 0L;

    public MailFolderDto(String folderName, Long unreadMessages) {
        this.folderName = folderName;
        this.unreadMessages = unreadMessages;
    }

    @Override
    public int compareTo(MailFolderDto dto) {
        return this.folderName.compareToIgnoreCase(dto.folderName);
    }
}
