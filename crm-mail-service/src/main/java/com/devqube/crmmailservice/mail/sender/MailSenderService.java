package com.devqube.crmmailservice.mail.sender;

import com.devqube.crmmailservice.encryptor.AesEncryptor;
import com.devqube.crmmailservice.internalClients.business.BusinessService;
import com.devqube.crmmailservice.internalClients.file.FileService;
import com.devqube.crmmailservice.internalClients.user.UserService;
import com.devqube.crmmailservice.mail.configuration.MailConfiguration;
import com.devqube.crmmailservice.mail.configuration.MailConfigurationRepository;
import com.devqube.crmmailservice.mail.exception.GdprException;
import com.devqube.crmmailservice.mail.exception.MailException;
import com.devqube.crmmailservice.mail.receiver.AsyncMailReceiverService;
import com.devqube.crmmailservice.mail.receiver.MailReceiverService;
import com.devqube.crmmailservice.mail.sender.web.dto.ForwardedMailDto;
import com.devqube.crmmailservice.mail.sender.web.dto.NewMailAttachment;
import com.devqube.crmmailservice.mail.sender.web.dto.NewMailDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.file.dto.FileInfoDto;
import com.devqube.crmshared.mail.gdpr.EmailCheckDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.mail.util.MimeMessageParser;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Slf4j
@Service
public class MailSenderService {
    private final AsyncMailReceiverService asyncMailReceiverService;
    private final AesEncryptor encryptor;
    private final UserService userService;
    private final MailConfigurationRepository mailConfigurationRepository;
    private final MailReceiverService mailReceiverService;
    private final FileService fileService;
    private final BusinessService businessService;

    public MailSenderService(AsyncMailReceiverService asyncMailReceiverService, AesEncryptor encryptor, UserService userService,
                             MailConfigurationRepository mailConfigurationRepository, MailReceiverService mailReceiverService, FileService fileService, BusinessService businessService) {
        this.asyncMailReceiverService = asyncMailReceiverService;
        this.encryptor = encryptor;
        this.userService = userService;
        this.mailConfigurationRepository = mailConfigurationRepository;
        this.mailReceiverService = mailReceiverService;
        this.fileService = fileService;
        this.businessService = businessService;
    }

    public boolean isSmtpConnectionEstablished(MailConfiguration configuration) {
        try {
            getSmtpTransport(configuration, getSmtpSession(configuration)).close();
            return true;
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void sendMail(String email, NewMailDto newMailDto) throws MessagingException, BadRequestException, EntityNotFoundException, MailException, GdprException {
        checkGdprForNewMail(newMailDto);
        Long userId = userService.getUserId(email);
        MailConfiguration mailConfiguration = mailConfigurationRepository.findByAccountIdAndActiveTrue(userId).orElseThrow(EntityNotFoundException::new);
        sendMail(mailConfiguration, newMailDto, email);
    }

    public void forwardMail(String email, ForwardedMailDto forwardedMailDto) throws MessagingException, BadRequestException, EntityNotFoundException, GdprException {
        checkGdprForForwardedMail(forwardedMailDto);
        Long userId = userService.getUserId(email);
        MailConfiguration configuration = mailConfigurationRepository.findByAccountIdAndActiveTrue(userId).orElseThrow(EntityNotFoundException::new);
        Session session = getSmtpSession(configuration);
        MimeMessage messageToForward = getMessageToForward(session, forwardedMailDto, userId, configuration.getUsername(), session, email);
        Transport transport = getSmtpTransport(configuration, session);
        sendMessage(configuration, transport, messageToForward);
    }

    private void sendMail(MailConfiguration configuration, NewMailDto newMailDto, String email) throws MailException {
        Session session = getSmtpSession(configuration);
        try (Transport transport = getSmtpTransport(configuration, session)) {
            MimeMessage message = getMessage(session, configuration, newMailDto, email);
            sendMessage(configuration, transport, message);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MailException();
        }
    }

    private void sendMessage(MailConfiguration configuration, Transport transport, MimeMessage message) throws MessagingException {
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
        try {
            asyncMailReceiverService.refreshEmailsForConfiguration(configuration);
        } catch (MailException | KafkaSendMessageException e) {
            e.printStackTrace();
        }
    }

    private MimeMessage getMessageToForward(Session session, ForwardedMailDto forwardedMailDto, Long accountId, String email, Session smtpSession, String loggedUserEmail) throws BadRequestException, EntityNotFoundException {
        MimeMessage toSend = new MimeMessage(session);
        MimeMessage message = mailReceiverService.getMessageToForward(forwardedMailDto.getMessageId(), accountId, smtpSession);
        try {
            toSend.setFrom(email);
            toSend.setRecipients(Message.RecipientType.TO, InternetAddress.parse(String.join(", ", forwardedMailDto.getTo())));
            toSend.setRecipients(Message.RecipientType.CC, InternetAddress.parse(String.join(", ", forwardedMailDto.getCc())));
            toSend.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(String.join(", ", forwardedMailDto.getBcc())));

            toSend.setSubject(message.getSubject());

            Multipart multipart = new MimeMultipart();

            BodyPart textPart = new MimeBodyPart();
            textPart.setContent(forwardedMailDto.getContent(), "text/html; charset=utf-8");
            multipart.addBodyPart(textPart);

            List<DataSource> attachmentList = new MimeMessageParser((MimeMessage) message).parse().getAttachmentList();
            try {
                if (attachmentList != null) {
                    for (DataSource attachment : attachmentList) {
                        addAttachmentToForward(attachment, multipart);
                    }
                }
                if (forwardedMailDto.getAttachments() != null && null != loggedUserEmail && !"".equals(loggedUserEmail)) {
                    for (NewMailAttachment attachment : forwardedMailDto.getAttachments()) {
                        addAttachment(attachment.isTempFile(), attachment.getFileId(), loggedUserEmail, multipart);
                    }
                }
            } catch (BadRequestException e) {
                e.printStackTrace();
                throw new MessagingException();
            }

            toSend.setContent(multipart);
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new BadRequestException();
        }
        return toSend;
    }

    private Transport getSmtpTransport(MailConfiguration configuration, Session smtpSession) throws MessagingException {
        String protocol = (configuration.getSmtpSecurityType().equals(MailConfiguration.SecurityType.SSL)) ? "smtps" : "smtp";
        Transport transport = smtpSession.getTransport(protocol);
        transport.connect(configuration.getSmtpServerAddress(), configuration.getSmtpServerPort().intValue(), configuration.getUsername(), encryptor.decrypt(configuration.getPassword()));
        return transport;
    }

    private Session getSmtpSession(MailConfiguration configuration) {
        Properties prop = new Properties();
        prop.put("mail.smtp.host", configuration.getSmtpServerAddress());
        prop.put("mail.smtp.port", configuration.getSmtpServerPort());
        prop.put("mail.smtp.username", configuration.getUsername());
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS

        if (configuration.getSmtpSecurityType().equals(MailConfiguration.SecurityType.SSL)) {
            prop.put("mail.smtp.socketFactory.port", configuration.getSmtpServerPort());
            prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        }
        return Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                configuration.getUsername(), encryptor.decrypt(configuration.getPassword()));// Specify the Username and the PassWord
                    }
                });
    }

    private MimeMessage getMessage(Session session, MailConfiguration configuration, NewMailDto mailDto, String email) throws MessagingException {
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(configuration.getUsername()));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(String.join(", ", mailDto.getTo())));
        if (mailDto.getCc() != null) {
            message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(String.join(", ", mailDto.getCc())));
        }
        if (mailDto.getBcc() != null) {
            message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(String.join(", ", mailDto.getBcc())));
        }
        message.setSubject(mailDto.getSubject());

        Multipart multipart = new MimeMultipart();

        BodyPart textPart = new MimeBodyPart();
        textPart.setContent(mailDto.getContent(), "text/html; charset=utf-8");
        multipart.addBodyPart(textPart);

        try {
            if (mailDto.getAttachments() != null) {
                for (NewMailAttachment attachment : mailDto.getAttachments()) {
                    addAttachment(attachment.isTempFile(), attachment.getFileId(), email, multipart);
                }
            }
        } catch (BadRequestException e) {
            e.printStackTrace();
            throw new MessagingException();
        }

        message.setContent(multipart);

        return message;
    }


    private void addAttachment(boolean tempFile, String fileName, String email, Multipart multipart) throws BadRequestException, MessagingException {
        MimeBodyPart bodyPart = new MimeBodyPart();
        FileInfoDto fileInfo = fileService.getFileInfo(fileName, tempFile);
        byte[] bytes = fileService.getFileFromCrm(email, fileName, tempFile);
        ByteArrayDataSource dataSource = new ByteArrayDataSource(bytes, "*/*");
        bodyPart.setDataHandler(new DataHandler(dataSource));
        bodyPart.setFileName(fileInfo.getName());
        multipart.addBodyPart(bodyPart);
    }

    private void addAttachmentToForward(DataSource dataSource, Multipart multipart) throws BadRequestException, MessagingException {
        MimeBodyPart bodyPart = new MimeBodyPart();
        bodyPart.setDataHandler(new DataHandler(dataSource));
        bodyPart.setFileName(dataSource.getName());
        multipart.addBodyPart(bodyPart);
    }

    private void checkGdprForForwardedMail(ForwardedMailDto forwardedMailDto) throws BadRequestException, GdprException {
        checkGdpr(getEmailFromLists(
                forwardedMailDto.getTo(),
                forwardedMailDto.getCc(),
                forwardedMailDto.getBcc()
        ));
    }

    private void checkGdprForNewMail(NewMailDto newMailDto) throws BadRequestException, GdprException {
        if (!newMailDto.isIgnoreGdpr()) {
            checkGdpr(getEmailFromLists(
                    newMailDto.getTo(),
                    newMailDto.getCc(),
                    newMailDto.getBcc()
            ));
        }
    }

    private void checkGdpr(List<String> emails) throws GdprException, BadRequestException {
        List<EmailCheckDto> emailCheckDtos = businessService.checkGdprInEmails(emails);
        if (emailCheckDtos.stream().anyMatch(c -> !c.isCorrect())) {
            emailCheckDtos.stream().filter(c -> !c.isCorrect()).forEach(c -> log.info("Agreement not approved for email {}", c.getEmail()));
            throw new GdprException();
        }
    }

    @SafeVarargs
    private List<String> getEmailFromLists(List<String>... emails) {
        List<String> result = new ArrayList<>();
        for (List<String> emailList : emails) {
            if (emailList != null) {
                result.addAll(emailList);
            }
        }
        return result;
    }
}
