package com.devqube.crmmailservice.mail.connection;

import com.devqube.crmmailservice.encryptor.AesEncryptor;
import com.devqube.crmmailservice.mail.configuration.MailConfiguration;
import com.devqube.crmmailservice.mail.connection.dto.ConnectionDto;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import java.time.LocalDateTime;
import java.util.*;

@Service
@EnableScheduling
public class ImapConnectionHolder {
    private final Long EXPIRE_AFTER_MINUTES = 5L;

    private final AesEncryptor encryptor;

    private static Map<Long, ConnectionDto> connectionMap = new HashMap<>();

    public ImapConnectionHolder(AesEncryptor encryptor) {
        this.encryptor = encryptor;
    }

    public void removeStoredConnectionById(Long id) {
        removeObjectByKey(id);
    }
    public Store getAndStoreConnection(MailConfiguration mailConfiguration) throws MessagingException {
        ConnectionDto connectionDto = connectionMap.get(mailConfiguration.getId());
        LocalDateTime now = LocalDateTime.now();
        if (connectionDto == null) {
            Store store = getConnection(mailConfiguration);
            connectionDto = new ConnectionDto(mailConfiguration.getId(), store, now);
            connectionMap.put(mailConfiguration.getId(), connectionDto);
        } else {
            connectionMap.get(mailConfiguration.getId()).setUpdateTime(now);
        }
        return connectionDto.getStore();
    }

    public Store getConnection(MailConfiguration configuration) throws MessagingException {
        Properties props = System.getProperties();
        String protocol = (configuration.getImapSecurityType().equals(MailConfiguration.SecurityType.SSL)) ? "imaps" : "imap";
        props.setProperty("mail.store.protocol", protocol);
        props.setProperty("mail.imaps.connectiontimeout", "5000");
        props.setProperty("mail.imaps.timeout", "5000");
        Session session = Session.getDefaultInstance(props, null);
        Store store = session.getStore(protocol);
        store.connect(configuration.getImapServerAddress(), configuration.getImapServerPort().intValue(), configuration.getUsername(),
                encryptor.decrypt(configuration.getPassword()));
        return store;
    }

    @Scheduled(fixedDelay = 5 * 60 * 1000)
    public void closeConnections() {
        List<Long> closedConnections = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();
        this.connectionMap.forEach((key, value) -> {
            if (isConnectionExpired(value, now)) {
                closedConnections.add(value.getId());
            }
        });
        closedConnections.forEach(this::removeObjectByKey);
    }

    private void removeObjectByKey(Long key) {
        if (connectionMap.get(key) != null) {
            try {
                connectionMap.get(key).getStore().close();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            connectionMap.remove(key);
        }
    }

    private boolean isConnectionExpired(ConnectionDto connectionDto, LocalDateTime now) {
        return connectionDto.getUpdateTime().plusMinutes(EXPIRE_AFTER_MINUTES).isBefore(now);
    }
}
