package com.devqube.crmmailservice.mail.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class GdprException extends Exception {
    public GdprException() {
    }

    public GdprException(String message) {
        super(message);
    }
}
