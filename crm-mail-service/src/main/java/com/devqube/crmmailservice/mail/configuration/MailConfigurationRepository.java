package com.devqube.crmmailservice.mail.configuration;

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MailConfigurationRepository extends JpaRepository<MailConfiguration, Long> {
    List<MailConfiguration> findAllByAccountId(Long accountId);
    Optional<MailConfiguration> findByAccountIdAndActiveTrue(Long accountId);
    boolean existsByAccountIdAndActiveTrue(Long accountId);
    Long countAllByAccountIdAndUsernameAndImapServerAddressAndSmtpServerAddress(Long accountId, String username, String imapServerAddress, String smtpServerAddress);
    Long countAllByAccountId(Long accountId);
    Long countAllByAccountIdAndUsernameAndImapServerAddressAndSmtpServerAddressAndIdNot(Long accountId, String username, String imapServerAddress, String smtpServerAddress, Long id);
}
