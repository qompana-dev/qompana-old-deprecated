package com.devqube.crmmailservice.mail.receiver;

import com.devqube.crmmailservice.mail.receiver.web.dto.HeaderEmailAddr;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailHeaderDto;
import com.sun.mail.imap.IMAPFolder;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

public class MailReceiverUtil {
    public static FetchProfile getFullMsgFetchProfile() {
        FetchProfile fetchProfile = getFetchProfile();
        fetchProfile.add(FetchProfile.Item.ENVELOPE);
        fetchProfile.add(FetchProfile.Item.CONTENT_INFO);
        fetchProfile.add(FetchProfile.Item.SIZE);
        fetchProfile.add(IMAPFolder.FetchProfileItem.MESSAGE);
        fetchProfile.add(IMAPFolder.FetchProfileItem.HEADERS);
        fetchProfile.add(IMAPFolder.FetchProfileItem.CONTENT_INFO);
        return fetchProfile;
    }

    public static FetchProfile getContentFetchProfile() {
        FetchProfile fetchProfile = getFetchProfile();
        fetchProfile.add(IMAPFolder.FetchProfileItem.MESSAGE);
        return fetchProfile;
    }

    public static FetchProfile getFetchProfile() {
        FetchProfile profile = new FetchProfile();
        profile.add("From");
        profile.add("To");
        profile.add("Cc");
        profile.add("Bcc");
        profile.add("Subject");
        profile.add("Message-ID");
        profile.add("References");
        profile.add("In-Reply-To");
        profile.add("Date");
        profile.add(FetchProfile.Item.FLAGS);
        profile.add(UIDFolder.FetchProfileItem.UID);
        return profile;
    }

    public static LocalDateTime dateHeaderToLocalDateTime(String header) {
        if (header == null) {
            return null;
        }
        return new Date(header).toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    public static MailHeaderDto addHeaderEmailAddresses(MailHeaderDto headerDto) {
        headerDto.setFromAddr(MailReceiverUtil.getHeaderEmailAddr(headerDto.getFrom()));
        headerDto.setToAddr(MailReceiverUtil.getHeaderEmailAddr(headerDto.getTo()));
        headerDto.setCcAddr(MailReceiverUtil.getHeaderEmailAddr(headerDto.getCc()));
        headerDto.setBccAddr(MailReceiverUtil.getHeaderEmailAddr(headerDto.getBcc()));
        return headerDto;
    }

    public static List<HeaderEmailAddr> getHeaderEmailAddr(String header) {
        if (header != null) {
            try {
                return Arrays.stream(InternetAddress.parseHeader(header, false))
                        .map(c -> new HeaderEmailAddr(c.getPersonal(), c.getAddress()))
                        .collect(Collectors.toList());
            } catch (AddressException e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>();
    }

    public static String getHeaderValue(Message m, String header) throws MessagingException {
        String[] headers = m.getHeader(header);
        if (headers == null || headers.length == 0) {
            return null;
        } else if (header.length() == 1) {
            return headers[0];
        } else {
            return String.join(", ", headers);
        }
    }


    public static ResponseEntity<InputStreamResource> createFileHttpResponse(String originalName, InputStreamResource resource) {
        String contentType = URLConnection.guessContentTypeFromName(originalName);
        MediaType mediaType = Optional.ofNullable(contentType)
                .map(MediaType::parseMediaType)
                .orElse(MediaType.APPLICATION_OCTET_STREAM);

        ContentDisposition contentDisposition = ContentDisposition.builder("inline")
                .filename(originalName)
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentDisposition(contentDisposition);

        return ResponseEntity.ok()
                .contentType(mediaType)
                .headers(headers)
                .body(resource);
    }
}
