package com.devqube.crmmailservice.mail.search;

import com.devqube.crmshared.search.EmailSearch;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface MailSearchApi {

    @ApiOperation(value = "search mails")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "account emails", response = List.class)
    })
    List<EmailSearch> searchEmails(@RequestHeader(value = "Logged-Account-Email") String email, String term);
}
