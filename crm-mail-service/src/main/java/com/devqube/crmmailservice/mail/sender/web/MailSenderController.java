package com.devqube.crmmailservice.mail.sender.web;

import com.devqube.crmmailservice.mail.exception.GdprException;
import com.devqube.crmmailservice.mail.exception.MailException;
import com.devqube.crmmailservice.mail.sender.MailSenderService;
import com.devqube.crmmailservice.mail.sender.web.dto.ForwardedMailDto;
import com.devqube.crmmailservice.mail.sender.web.dto.NewMailDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
@Slf4j
public class MailSenderController implements MailSenderApi {
    private final MailSenderService mailSenderService;

    public MailSenderController(MailSenderService mailSenderService) {
        this.mailSenderService = mailSenderService;
    }

    @Override
    @PostMapping("/mail/send")
    public ResponseEntity<Void> sendMail(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @RequestBody NewMailDto newMailDto) {
        try {
            mailSenderService.sendMail(loggedAccountEmail, newMailDto);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadRequestException | MessagingException | MailException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        } catch (GdprException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @Override
    @PostMapping("/mail/forward")
    public ResponseEntity<Void> forwardMail(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail,
                                            @RequestBody ForwardedMailDto forwardedMailDto) throws BadRequestException {
        try {
            mailSenderService.forwardMail(loggedAccountEmail, forwardedMailDto);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (MessagingException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        } catch (GdprException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
}
