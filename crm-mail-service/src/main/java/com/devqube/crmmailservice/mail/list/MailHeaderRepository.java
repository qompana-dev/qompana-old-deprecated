package com.devqube.crmmailservice.mail.list;

import com.devqube.crmmailservice.mail.list.dto.MailFolderDto;
import com.devqube.crmmailservice.mail.model.MailHeader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface MailHeaderRepository extends JpaRepository<MailHeader, Long> {
    List<MailHeader> findAllByConfigurationId(Long configurationId);
    void deleteAllByConfigurationId(Long id);
    List<MailHeader> findAllByConfigurationIdAndReceivedDateAfter(Long id, LocalDateTime after);
    Page<MailHeader> findAllByConfigurationAccountIdAndConfigurationActiveTrue(Long accountId, Pageable pageable);
    Page<MailHeader> findAllByConfigurationAccountIdAndFolderNameAndConfigurationActiveTrue(Long accountId, String folder, Pageable pageable);
    List<MailHeader> findAllByReferencesContainingAndFolderNameIn(String mailMessageId, List<String> folderNames, Sort sort);
    @Query("select " +
            "new com.devqube.crmmailservice.mail.list.dto.MailFolderDto(mh.folderName, sum(case mh.seen when false then 1 else 0 end)) " +
            "from MailHeader mh join mh.configuration mc " +
            "where mc.accountId = :accountId and mc.active = true " +
            "GROUP BY mh.folderName " +
            "ORDER BY mh.folderName")
    Set<MailFolderDto> getMailFolders(@Param("accountId") Long accountId);

    @Query("select mh from MailHeader mh join mh.configuration mc " +
            "where mc.accountId = :accountId and mc.active = true and ((lower(mh.from) like concat('%',lower(:term),'%')) or (lower(mh.to) like concat('%',lower(:term),'%')))")
    List<MailHeader> findByEmailContaining(@Param("term") String term, @Param("accountId") Long accountId);



    void deleteAllByConfigurationIdAndFolderNameAndMessageId(Long id, String folderName, Long messageId);
}
