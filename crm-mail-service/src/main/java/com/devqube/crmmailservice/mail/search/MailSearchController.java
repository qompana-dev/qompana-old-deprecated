package com.devqube.crmmailservice.mail.search;

import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.search.EmailSearch;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@Slf4j
public class MailSearchController implements MailSearchApi {

    private final MailSearchService mailSearchService;

    public MailSearchController(MailSearchService mailSearchService) {
        this.mailSearchService = mailSearchService;
    }

    @Override
    @GetMapping("/mails/search")
    public List<EmailSearch> searchEmails(@RequestHeader(value = "Logged-Account-Email") String email, String term) {
        try {
            return mailSearchService.searchEmails(term, email);
        } catch (BadRequestException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

}
