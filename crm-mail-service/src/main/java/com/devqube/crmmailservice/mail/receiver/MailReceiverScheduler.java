package com.devqube.crmmailservice.mail.receiver;

import com.devqube.crmmailservice.mail.configuration.MailConfiguration;
import com.devqube.crmmailservice.mail.configuration.MailConfigurationRepository;
import com.devqube.crmmailservice.mail.exception.MailException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
@Slf4j
public class MailReceiverScheduler {
    private final AsyncMailReceiverService asyncMailReceiverService;
    private final MailConfigurationRepository mailConfigurationRepository;

    public MailReceiverScheduler(AsyncMailReceiverService asyncMailReceiverService, MailConfigurationRepository mailConfigurationRepository) {
        this.asyncMailReceiverService = asyncMailReceiverService;
        this.mailConfigurationRepository = mailConfigurationRepository;
    }

    @Scheduled(cron = "0 0 1 * * ?")
    public void refreshEmails() {
        log.info("refreshEmails");
        for (MailConfiguration mailConfiguration : mailConfigurationRepository.findAll()) {
            try {
                asyncMailReceiverService.synchronizeEmailsForConfiguration(mailConfiguration);
            } catch (MailException | KafkaSendMessageException e) {
                e.printStackTrace();
            }
        }
    }

    @Scheduled(fixedDelay = 60 * 1000)
    public void getNewEmails() {
        log.info("getNewEmails");
        for (MailConfiguration mailConfiguration : mailConfigurationRepository.findAll()) {
            try {
                asyncMailReceiverService.refreshEmailsForConfiguration(mailConfiguration);
            } catch (MailException | KafkaSendMessageException e) {
                e.printStackTrace();
            }
        }
    }
}

