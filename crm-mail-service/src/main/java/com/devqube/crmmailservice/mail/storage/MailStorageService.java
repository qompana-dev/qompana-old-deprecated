package com.devqube.crmmailservice.mail.storage;

import com.devqube.crmmailservice.internalClients.file.FileService;
import com.devqube.crmmailservice.internalClients.user.UserService;
import com.devqube.crmmailservice.mail.configuration.MailConfiguration;
import com.devqube.crmmailservice.mail.configuration.MailConfigurationRepository;
import com.devqube.crmmailservice.mail.connection.ImapConnectionHolder;
import com.devqube.crmmailservice.mail.list.MailHeaderRepository;
import com.devqube.crmmailservice.mail.model.MailHeader;
import com.devqube.crmmailservice.mail.model.MailStorage;
import com.devqube.crmmailservice.mail.receiver.EmailReceiverHelperService;
import com.devqube.crmmailservice.mail.receiver.MailReceiverUtil;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailContentDto;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailHeaderDto;
import com.devqube.crmmailservice.mail.storage.web.dto.EmailStorageInfoDto;
import com.devqube.crmmailservice.mail.storage.web.dto.MailStorageDto;
import com.devqube.crmshared.association.AssociationClient;
import com.devqube.crmshared.association.dto.AssociationDetailDto;
import com.devqube.crmshared.association.dto.AssociationDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import com.devqube.crmshared.search.CrmObjectType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.mail.util.MimeMessageParser;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MailStorageService {

    private final ModelMapper modelMapper;
    private final ImapConnectionHolder imapConnectionHolder;
    private final EmailReceiverHelperService emailReceiverHelperService;
    private final FileService fileService;
    private final AssociationClient associationClient;
    private final UserService userService;
    private final MailHeaderRepository mailHeaderRepository;
    private final MailConfigurationRepository mailConfigurationRepository;
    private final MailStorageRepository mailStorageRepository;

    public MailStorageService(@Qualifier("strictModelMapper") ModelMapper modelMapper,
                              ImapConnectionHolder imapConnectionHolder,
                              EmailReceiverHelperService emailReceiverHelperService,
                              FileService fileService,
                              AssociationClient associationClient,
                              UserService userService,
                              MailHeaderRepository mailHeaderRepository,
                              MailConfigurationRepository mailConfigurationRepository,
                              MailStorageRepository mailStorageRepository) {
        this.modelMapper = modelMapper;
        this.imapConnectionHolder = imapConnectionHolder;
        this.emailReceiverHelperService = emailReceiverHelperService;
        this.fileService = fileService;
        this.associationClient = associationClient;
        this.userService = userService;
        this.mailHeaderRepository = mailHeaderRepository;
        this.mailConfigurationRepository = mailConfigurationRepository;
        this.mailStorageRepository = mailStorageRepository;
    }

    @Transactional(rollbackFor = {BadRequestException.class, EntityNotFoundException.class})
    public void attachMessageToLead(Long messageId, String loggedUserEmail, Long leadId) throws BadRequestException, EntityNotFoundException, PermissionDeniedException {
        checkPermission(CrmObjectType.lead, leadId, loggedUserEmail);
        attachMessageToCrmObject(messageId, loggedUserEmail, CrmObjectType.lead, leadId);
    }

    @Transactional(rollbackFor = {BadRequestException.class, EntityNotFoundException.class})
    public void attachMessageToOpportunity(Long messageId, String loggedUserEmail, Long opportunityId) throws BadRequestException, EntityNotFoundException, PermissionDeniedException {
        checkPermission(CrmObjectType.opportunity, opportunityId, loggedUserEmail);
        attachMessageToCrmObject(messageId, loggedUserEmail, CrmObjectType.opportunity, opportunityId);
    }

    @Transactional(rollbackFor = {BadRequestException.class, EntityNotFoundException.class})
    public void attachMessageToContact(Long messageId, String loggedUserEmail, Long contactId) throws BadRequestException, EntityNotFoundException, PermissionDeniedException {
        checkPermission(CrmObjectType.contact, contactId, loggedUserEmail);
        attachMessageToCrmObject(messageId, loggedUserEmail, CrmObjectType.contact, contactId);
    }

    @Transactional(rollbackFor = {BadRequestException.class, EntityNotFoundException.class})
    public void detachMessageFromLead(Long mailStorageId, String loggedAccountEmail, Long id) throws BadRequestException, EntityNotFoundException, PermissionDeniedException {
        checkPermission(CrmObjectType.lead, id, loggedAccountEmail);
        detachMessage(mailStorageId, id, CrmObjectType.lead);
    }

    @Transactional(rollbackFor = {BadRequestException.class, EntityNotFoundException.class})
    public void detachMessageFromOpportunity(Long mailStorageId, String loggedAccountEmail, Long id) throws BadRequestException, EntityNotFoundException, PermissionDeniedException {
        checkPermission(CrmObjectType.opportunity, id, loggedAccountEmail);
        detachMessage(mailStorageId, id, CrmObjectType.opportunity);
    }

    @Transactional(rollbackFor = {BadRequestException.class, EntityNotFoundException.class})
    public void detachMessageFromContact(Long mailStorageId, String loggedAccountEmail, Long contactId) throws BadRequestException, EntityNotFoundException, PermissionDeniedException {
        checkPermission(CrmObjectType.contact, contactId, loggedAccountEmail);
        detachMessage(mailStorageId, contactId, CrmObjectType.contact);
    }

    public Page<MailStorageDto> getMailHeadersForOpportunity(String loggedUserEmail, Long opportunityId, Pageable page) throws BadRequestException, PermissionDeniedException {
        checkPermission(CrmObjectType.opportunity, opportunityId, loggedUserEmail);
        return getMailHeaders(CrmObjectType.opportunity, opportunityId, page);
    }

    public Page<MailStorageDto> getMailHeadersForLead(String loggedUserEmail, Long leadId, Pageable page)
            throws BadRequestException, PermissionDeniedException {
        checkPermission(CrmObjectType.lead, leadId, loggedUserEmail);
        return getMailHeaders(CrmObjectType.lead, leadId, page);
    }

    public Page<MailStorageDto> getMailHeadersForContact(String loggedUserEmail, Long contactId, Pageable page)
            throws BadRequestException, PermissionDeniedException {
        checkPermission(CrmObjectType.contact, contactId, loggedUserEmail);
        return getMailHeaders(CrmObjectType.contact, contactId, page);

    }

    public MailContentDto getMessageForLead(String loggedUserEmail, Long leadId, Long mailStorageId) throws EntityNotFoundException, BadRequestException, MessagingException, PermissionDeniedException {
        checkPermission(CrmObjectType.lead, leadId, loggedUserEmail);
        return getMessage(mailStorageId);
    }

    public MailContentDto getMessageForOpportunity(String loggedUserEmail, Long opportunityId, Long mailStorageId) throws EntityNotFoundException, BadRequestException, MessagingException, PermissionDeniedException {
        checkPermission(CrmObjectType.opportunity, opportunityId, loggedUserEmail);
        return getMessage(mailStorageId);
    }

    public MailContentDto getMessageForContact(String loggedUserEmail, Long contactId, Long mailStorageId) throws EntityNotFoundException, BadRequestException, MessagingException, PermissionDeniedException {
        checkPermission(CrmObjectType.contact, contactId, loggedUserEmail);
        return getMessage(mailStorageId);
    }

    public void saveFileInCrmFromLead(Long mailStorageId, String loggedAccountEmail, Long id, String fileName) throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        checkPermission(CrmObjectType.lead, id, loggedAccountEmail);
        saveFileInCrm(mailStorageId, loggedAccountEmail, fileName);
    }

    public void saveFileInCrmFromOpportunity(Long mailStorageId, String loggedAccountEmail, Long id, String fileName) throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        checkPermission(CrmObjectType.opportunity, id, loggedAccountEmail);
        saveFileInCrm(mailStorageId, loggedAccountEmail, fileName);
    }

    public void saveFileInCrmFromContact(Long mailStorageId, String loggedAccountEmail, Long contactId, String fileName) throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        checkPermission(CrmObjectType.contact, contactId, loggedAccountEmail);
        saveFileInCrm(mailStorageId, loggedAccountEmail, fileName);
    }

    public InputStreamResource getAttachmentForLead(Long mailStorageId, String loggedAccountEmail, Long id, String fileName) throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        checkPermission(CrmObjectType.lead, id, loggedAccountEmail);
        return getAttachment(mailStorageId, fileName);
    }

    public InputStreamResource getAttachmentForOpportunity(Long mailStorageId, String loggedAccountEmail, Long id, String fileName) throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        checkPermission(CrmObjectType.opportunity, id, loggedAccountEmail);
        return getAttachment(mailStorageId, fileName);
    }

    public InputStreamResource getAttachmentForContact(Long mailStorageId, String loggedAccountEmail, Long contactId, String fileName) throws BadRequestException, PermissionDeniedException, EntityNotFoundException {
        checkPermission(CrmObjectType.contact, contactId, loggedAccountEmail);
        return getAttachment(mailStorageId, fileName);
    }

    private InputStreamResource getAttachment(Long mailStorageId, String fileName) throws BadRequestException, EntityNotFoundException {
        MailStorage mailStorage = mailStorageRepository.findById(mailStorageId).orElseThrow(EntityNotFoundException::new);
        MimeMessage message = null;
        try {
            message = new MimeMessage(null, new ByteArrayInputStream(fileService.getEmailFile(mailStorage.getMailFileName())));
            MimeMessageParser parser = new MimeMessageParser(message).parse();
            DataSource dataSource = parser.getAttachmentList().stream().filter(c -> c.getName().equals(fileName)).findFirst().orElseThrow(EntityNotFoundException::new);
            return new InputStreamResource(dataSource.getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("Unable to save file in crm");
        }
    }

    private void saveFileInCrm(Long mailStorageId, String loggedAccountEmail, String fileName) throws EntityNotFoundException, BadRequestException {
        try {
            fileService.uploadAttachment(loggedAccountEmail, fileName, getAttachment(mailStorageId, fileName).getInputStream().readAllBytes());
        } catch (IOException e) {
            throw new BadRequestException();
        }
    }


    private void detachMessage(Long mailStorageId, Long id, CrmObjectType objectType) throws EntityNotFoundException, BadRequestException {
        MailStorage mailStorage = mailStorageRepository.findById(mailStorageId).orElseThrow(EntityNotFoundException::new);
        AssociationDetailDto associationToRemove = AssociationDetailDto.builder()
                .sourceId(mailStorageId).sourceType(CrmObjectType.emailMessage)
                .destinationId(id).destinationType(objectType)
                .build();
        try {
            associationClient.deleteAssociation(associationToRemove);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException();
        }


        Long countByFileName = mailStorageRepository.countAllByMailFileNameAndIdNot(mailStorage.getMailFileName(), mailStorageId);
        mailStorageRepository.deleteById(mailStorageId);
        if (countByFileName == 0L) {
            fileService.removeEmail(mailStorage.getMailFileName());
        }
    }


    private MailContentDto getMessage(Long mailStorageId) throws EntityNotFoundException, BadRequestException, MessagingException {
        MailStorage mailStorage = mailStorageRepository.findById(mailStorageId).orElseThrow(EntityNotFoundException::new);
        Message message = new MimeMessage(null, new ByteArrayInputStream(fileService.getEmailFile(mailStorage.getMailFileName())));
        MailHeaderDto headerDto = emailReceiverHelperService.getMailHeaderDto(mailStorage.getFolderName(), message, mailStorage.getMessageId());
        return emailReceiverHelperService.getMailContentDto(message, mailStorage.getMessageId(), headerDto);
    }

    public Page<MailStorageDto> getMailHeaders(CrmObjectType objectType, Long id, Pageable page) throws BadRequestException {
        Set<Long> storageIds = getEmailStorageIds(objectType, id);
        if (!storageIds.isEmpty()) {
            return mailStorageRepository.findAll(MailStorageSpecifications.findByIdIn(storageIds), page)
                    .map(c -> addHeaderEmailAddresses(modelMapper.map(c, MailStorageDto.class)))
                    .map(this::addPlaintContent)
                    .map(mailStorageDto -> this.addRelatedMails(mailStorageDto, page));
        } else {
            return Page.empty();
        }
    }

    private MailStorageDto addRelatedMails(MailStorageDto mailStorageDto, Pageable page) {
        Optional<MailHeader> optionalMailHeader = mailHeaderRepository.findById(mailStorageDto.getMailHeaderId());
        if (optionalMailHeader.isPresent()) {
            MailHeader mailHeader = optionalMailHeader.get();
            if (mailHeader.getReferences() != null) {
                String firstReference;
                if (mailHeader.getReferences().contains(" ")) {
                    firstReference = mailHeader.getReferences().substring(0, mailHeader.getReferences().indexOf(" "));
                } else if (!mailHeader.getReferences().isEmpty()) {
                    firstReference = mailHeader.getReferences();
                } else {
                    return mailStorageDto;
                }

                List<MailHeader> mailHeaders = getMailHeaders(firstReference, page.getSortOr(Sort.by("receivedDate")));

                List<MailHeaderDto> collect = mailHeaders.stream()
                        .map(mailHeaderD -> MailHeaderDto.toDto(modelMapper, mailHeaderD))
                        .map(this::addPlaintContent)
                        .collect(Collectors.toList());
                mailStorageDto.setRelatedMails(collect);
            }
        }
        return mailStorageDto;
    }

    private List<MailHeader> getMailHeaders(String firstReference, Sort sort) {
        List<String> folderNames = List.of("Wszystkie", "All Mail");
        return mailHeaderRepository.findAllByReferencesContainingAndFolderNameIn(firstReference, folderNames, sort);
    }

    private MailHeaderDto addPlaintContent(MailHeaderDto mailHeaderDto) {
        List<MailStorage> storages = mailStorageRepository.findAllByMailHeaderId(mailHeaderDto.getId());

        if (storages.size() > 0) {
            try {
                MailContentDto message = getMessage(storages.get(0).getId());
                mailHeaderDto.setPlainContent(message.getPlainContent());
                mailHeaderDto.setHtmlContent(message.getHtmlContent());
            } catch (EntityNotFoundException | MessagingException | BadRequestException e) {
                e.printStackTrace();
                return mailHeaderDto;
            }
        }
        return mailHeaderDto;
    }

    private MailStorageDto addPlaintContent(MailStorageDto storageDto) {
        try {
            MailContentDto message = getMessage(storageDto.getId());
            storageDto.setPlainContent(message.getPlainContent());
            storageDto.setHtmlContent(message.getHtmlContent());
            return storageDto;
        } catch (EntityNotFoundException | BadRequestException | MessagingException e) {
            e.printStackTrace();
            return storageDto;
        }
    }

    public MailStorageDto addHeaderEmailAddresses(MailStorageDto storageDto) {
        storageDto.setFromAddr(MailReceiverUtil.getHeaderEmailAddr(storageDto.getFrom()));
        storageDto.setToAddr(MailReceiverUtil.getHeaderEmailAddr(storageDto.getTo()));
        storageDto.setCcAddr(MailReceiverUtil.getHeaderEmailAddr(storageDto.getCc()));
        storageDto.setBccAddr(MailReceiverUtil.getHeaderEmailAddr(storageDto.getBcc()));
        return storageDto;
    }

    private Set<Long> getEmailStorageIds(CrmObjectType objectType, Long id) throws BadRequestException {
        try {
            return associationClient.getAllBySourceTypeAndDestinationTypeAndSourceId(objectType, CrmObjectType.emailMessage, id)
                    .stream().map(AssociationDto::getDestinationId).collect(Collectors.toSet());
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException();
        }
    }

    private void attachMessageToCrmObject(Long messageId, String loggedUserEmail, CrmObjectType objectType, Long id) throws BadRequestException, EntityNotFoundException {
        Long accountId = userService.getUserId(loggedUserEmail);
        MailConfiguration mailConfiguration = mailConfigurationRepository.findByAccountIdAndActiveTrue(accountId).orElseThrow(EntityNotFoundException::new);
        MailHeader mailHeader = mailHeaderRepository.findById(messageId).orElseThrow(EntityNotFoundException::new);

        String fileName = getHash(mailConfiguration, mailHeader.getMessageId(), mailHeader.getFolderName());
        MailStorage savedMailStorage = mailStorageRepository.save(getNewMailStorage(mailConfiguration, mailHeader, fileName));

        saveMessageInFileService(mailConfiguration, mailHeader.getMessageId(), mailHeader.getFolderName(), fileName);

        try {
            associationClient.createAssociation(getAssociationDetailDto(objectType, id, savedMailStorage.getId()));
        } catch (Exception e) {
            throw new BadRequestException("unable to create association");
        }

        if (mailHeader.getReferences() != null) {
            String firstReference;
            if (mailHeader.getReferences().contains(" ")) {
                firstReference = mailHeader.getReferences().substring(0, mailHeader.getReferences().indexOf(" "));
            } else if (!mailHeader.getReferences().isEmpty()) {
                firstReference = mailHeader.getReferences();
            } else {
                return;
            }

            Set<MailHeader> mailHeaders = new HashSet<>(getMailHeaders(firstReference, Sort.by("receivedDate")));

            SaveMails saveMails = new SaveMails(mailHeaders, mailConfiguration, this, mailStorageRepository);
            Executors.newSingleThreadExecutor().submit(saveMails);
        }
    }

    private static class SaveMails implements Runnable {
        private final Set<MailHeader> mailHeaders;
        private final MailConfiguration mailConfiguration;
        private final MailStorageService mailStorageService;
        private final MailStorageRepository mailStorageRepository;

        public SaveMails(Set<MailHeader> mailHeaders, MailConfiguration mailConfiguration, MailStorageService mailStorageService, MailStorageRepository mailStorageRepository) {
            this.mailHeaders = mailHeaders;
            this.mailConfiguration = mailConfiguration;
            this.mailStorageService = mailStorageService;
            this.mailStorageRepository = mailStorageRepository;
        }

        @Override
        public void run() {
            for (MailHeader header : mailHeaders) {
                String fileName = mailStorageService.getHash(mailConfiguration, header.getMessageId(), header.getFolderName());
                mailStorageRepository.save(mailStorageService.getNewMailStorage(mailConfiguration, header, fileName));

                try {
                    mailStorageService.saveMessageInFileService(mailConfiguration, header.getMessageId(), header.getFolderName(), fileName);
                } catch (BadRequestException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private AssociationDetailDto getAssociationDetailDto(CrmObjectType objectType, Long id, Long savedEmailId) {
        return AssociationDetailDto.builder()
                .sourceType(objectType)
                .sourceId(id)
                .destinationType(CrmObjectType.emailMessage)
                .destinationId(savedEmailId)
                .build();
    }

    private MailStorage getNewMailStorage(MailConfiguration mailConfiguration, MailHeader mailHeader, String fileName) {
        MailStorage mailStorage = new MailStorage();
        mailStorage.setMailFileName(fileName);

        mailStorage.setMessageId(mailHeader.getMessageId());
        mailStorage.setMailHeaderId(mailHeader.getId());
        mailStorage.setSubject(mailHeader.getSubject());
        mailStorage.setFrom(mailHeader.getFrom());
        mailStorage.setTo(mailHeader.getTo());
        mailStorage.setCc(mailHeader.getCc());
        mailStorage.setBcc(mailHeader.getBcc());
        mailStorage.setReceivedDate(mailHeader.getReceivedDate());
        mailStorage.setFolderName(mailHeader.getFolderName());
        mailStorage.setAccountId(mailConfiguration.getAccountId());
        mailStorage.setUsername(mailConfiguration.getUsername());
        return mailStorage;
    }

    private String saveMessageInFileService(MailConfiguration configuration, Long messageId, String folderName, String fileName) throws BadRequestException {
        Folder folder = null;
        String result;
        try {
            Store store = imapConnectionHolder.getAndStoreConnection(configuration);
            folder = store.getFolder(emailReceiverHelperService.getFolderFullName(configuration.getId(), folderName));
            UIDFolder uidFolder = (UIDFolder) folder;
            folder.open(Folder.READ_ONLY);
            Message message = emailReceiverHelperService.getMessageById(messageId, folder, uidFolder);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            message.writeTo(byteArrayOutputStream);
            result = fileService.uploadEmail(fileName, byteArrayOutputStream.toByteArray());
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new BadRequestException("mail configuration is not valid");
        } finally {
            if (folder != null) {
                try {
                    folder.close();
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    private String getHash(MailConfiguration configuration, Long messageId, String folderName) {
        EmailStorageInfoDto emailStorageInfoDto = new EmailStorageInfoDto(messageId, folderName, configuration.getId(),
                configuration.getAccountId(), configuration.getImapServerAddress(),
                configuration.getSmtpServerAddress(), configuration.getUsername());
        return DigestUtils.sha1Hex(emailStorageInfoDto.toString());
    }

    private void checkPermission(CrmObjectType type, Long id, String loggedUserEmail) throws PermissionDeniedException, BadRequestException {
        if (!fileService.hasAccessToObject(type, id, loggedUserEmail)) {
            throw new PermissionDeniedException();
        }
    }
}
