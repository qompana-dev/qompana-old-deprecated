package com.devqube.crmmailservice.mail.receiver.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MailContentAttachmentDto {
    private String name;
    private Integer size;
}
