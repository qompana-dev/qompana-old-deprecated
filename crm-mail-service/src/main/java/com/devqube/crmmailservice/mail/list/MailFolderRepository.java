package com.devqube.crmmailservice.mail.list;

import com.devqube.crmmailservice.mail.model.MailFolder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface MailFolderRepository extends JpaRepository<MailFolder, Long> {
    void deleteAllByConfigurationId(Long id);
    Set<MailFolder> findAllByConfigurationAccountIdAndConfigurationActiveTrue(Long accountId);
    Set<MailFolder> findAllByConfigurationIdAndFolderNameAndConfigurationActiveTrue(Long id, String folderName);
}
