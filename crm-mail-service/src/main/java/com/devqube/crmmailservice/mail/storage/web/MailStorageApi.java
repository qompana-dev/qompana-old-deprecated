package com.devqube.crmmailservice.mail.storage.web;

import com.devqube.crmmailservice.mail.receiver.web.dto.MailContentDto;
import com.devqube.crmmailservice.mail.storage.web.dto.MailStorageDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface MailStorageApi {

    @ApiOperation(value = "Get mails attached to lead", nickname = "getMailHeadersForLead", notes = "Method used to get mails attached to lead", response = MailStorageDto[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Downloaded file", response = Resource.class),
            @ApiResponse(code = 403, message = "User don't have permissions to lead")})
    ResponseEntity<Page<MailStorageDto>> getMailHeadersForLead(String loggedAccountEmail, Long id, Pageable page);

    @ApiOperation(value = "Get mails attached to opportunity", nickname = "getMailHeadersForOpportunity", notes = "Method used to get mails attached to opportunity", response = MailStorageDto[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Downloaded file", response = Resource.class),
            @ApiResponse(code = 403, message = "User don't have permissions to opportunity")})
    ResponseEntity<Page<MailStorageDto>> getMailHeadersForOpportunity(String loggedAccountEmail, Long id, Pageable page);

    @ApiOperation(value = "Get mails attached to contact", nickname = "getMailHeadersForContact", notes = "Method used to get mails attached to contact", response = MailStorageDto[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Downloaded file", response = Resource.class),
            @ApiResponse(code = 403, message = "User don't have permissions to opportunity")})
    ResponseEntity<Page<MailStorageDto>> getMailHeadersForContact(String loggedAccountEmail, Long id, Pageable page);


    @ApiOperation(value = "attach mail to lead", nickname = "attachMessageToLead", notes = "Method used to attach mail to lead")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Mail attached successfully"),
            @ApiResponse(code = 403, message = "User don't have permissions to lead"),
            @ApiResponse(code = 404, message = "Message not found")})
    ResponseEntity<Void> attachMessageToLead(Long messageId, String loggedAccountEmail, Long id);

    @ApiOperation(value = "attach mail to opportunity", nickname = "attachMessageToOpportunity", notes = "Method used to attach mail to opportunity")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Mail attached successfully"),
            @ApiResponse(code = 403, message = "User don't have permissions to opportunity"),
            @ApiResponse(code = 404, message = "Message not found")})
    ResponseEntity<Void> attachMessageToOpportunity(Long messageId, String loggedAccountEmail, Long id);

    @ApiOperation(value = "attach mail to contact", nickname = "attachMessageToContact", notes = "Method used to attach mail to contact.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Mail attached successfully"),
            @ApiResponse(code = 403, message = "User don't have permissions to contact"),
            @ApiResponse(code = 404, message = "Message not found")})
    ResponseEntity<Void> attachMessageToContact(Long messageId, String loggedAccountEmail, Long id);


    @ApiOperation(value = "detach mail from lead", nickname = "detachMessageFromLead", notes = "Method used to detach mail from lead")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Mail detached successfully"),
            @ApiResponse(code = 403, message = "User don't have permissions to lead"),
            @ApiResponse(code = 404, message = "Message not found")})
    ResponseEntity<Void> detachMessageFromLead(Long mailStorageId, String loggedAccountEmail, Long id);

    @ApiOperation(value = "detach mail from opportunity", nickname = "detachMessageFromOpportunity", notes = "Method used to detach mail from opportunity")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Mail detached successfully"),
            @ApiResponse(code = 403, message = "User don't have permissions to opportunity"),
            @ApiResponse(code = 404, message = "Message not found")})
    ResponseEntity<Void> detachMessageFromOpportunity(Long mailStorageId, String loggedAccountEmail, Long id);

    @ApiOperation(value = "detach mail from contact", nickname = "detachMessageFromContact", notes = "Method used to detach mail from contact")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Mail detached successfully"),
            @ApiResponse(code = 403, message = "User don't have permissions to contact"),
            @ApiResponse(code = 404, message = "Message not found")})
    ResponseEntity<Void> detachMessageFromContact(Long mailStorageId, String loggedAccountEmail, Long id);


    @ApiOperation(value = "get mail content", nickname = "getMessageForLead", notes = "Method used to get mail content")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Mail content"),
            @ApiResponse(code = 403, message = "User don't have permissions to lead"),
            @ApiResponse(code = 404, message = "Message not found")})
    ResponseEntity<MailContentDto> getMessageForLead(Long mailStorageId, String loggedAccountEmail, Long id);

    @ApiOperation(value = "get mail content", nickname = "getMessageForOpportunity", notes = "Method used to get mail content")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Mail content"),
            @ApiResponse(code = 403, message = "User don't have permissions to opportunity"),
            @ApiResponse(code = 404, message = "Message not found")})
    ResponseEntity<MailContentDto> getMessageForOpportunity(Long mailStorageId, String loggedAccountEmail, Long id);

    @ApiOperation(value = "get mail content", nickname = "getMessageForContact", notes = "Method used to get mail content")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Mail content"),
            @ApiResponse(code = 403, message = "User don't have permissions to contact"),
            @ApiResponse(code = 404, message = "Message not found")})
    ResponseEntity<MailContentDto> getMessageForContact(Long mailStorageId, String loggedAccountEmail, Long id);

    @ApiOperation(value = "Save attachment in CRM", nickname = "saveFileInCrmFromOpportunity", notes = "Method used to save attachment from message in crm")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File saved in CRM"),
            @ApiResponse(code = 404, message = "File not found")})
    ResponseEntity<Void> saveFileInCrmFromOpportunity(Long mailStorageId, String loggedAccountEmail, Long id, String fileName);

    @ApiOperation(value = "Save attachment in CRM", nickname = "saveFileInCrmFromLead", notes = "Method used to save attachment from message in crm")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File saved in CRM"),
            @ApiResponse(code = 404, message = "File not found")})
    ResponseEntity<Void> saveFileInCrmFromLead(Long mailStorageId, String loggedAccountEmail, Long id, String fileName);

    @ApiOperation(value = "Save attachment in CRM", nickname = "saveFileInCrmFromContact", notes = "Method used to save attachment from message in crm")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File saved in CRM"),
            @ApiResponse(code = 404, message = "File not found")})
    ResponseEntity<Void> saveFileInCrmFromContact(Long mailStorageId, String loggedAccountEmail, Long id, String fileName);


    @ApiOperation(value = "Download file", nickname = "downloadAttachmentForOpportunity", notes = "Method used to download attachment", response = Resource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File saved in CRM", response = Resource.class),
            @ApiResponse(code = 404, message = "File not found")})
    ResponseEntity<InputStreamResource> downloadAttachmentForOpportunity(Long mailStorageId, String loggedAccountEmail, Long id, String fileName);

    @ApiOperation(value = "Download file", nickname = "downloadAttachmentForLead", notes = "Method used to download attachment", response = Resource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File saved in CRM", response = Resource.class),
            @ApiResponse(code = 404, message = "File not found")})
    ResponseEntity<InputStreamResource> downloadAttachmentForLead(Long mailStorageId, String loggedAccountEmail, Long id, String fileName);

    @ApiOperation(value = "Download file", nickname = "downloadAttachmentForContact", notes = "Method used to download attachment", response = Resource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File saved in CRM", response = Resource.class),
            @ApiResponse(code = 404, message = "File not found")})
    ResponseEntity<InputStreamResource> downloadAttachmentForContact(Long mailStorageId, String loggedAccountEmail, Long id, String fileName);


}
