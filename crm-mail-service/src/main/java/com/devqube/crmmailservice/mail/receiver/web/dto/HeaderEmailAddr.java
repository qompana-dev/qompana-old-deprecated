package com.devqube.crmmailservice.mail.receiver.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HeaderEmailAddr {
    private String name;
    private String email;
}
