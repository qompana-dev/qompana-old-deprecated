package com.devqube.crmmailservice.mail.receiver.web.dto;

import com.devqube.crmmailservice.mail.model.MailHeader;
import com.devqube.crmmailservice.mail.receiver.MailReceiverUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.mail.Address;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MailHeaderDto {
    private Long id;
    private Long messageId;
    private String subject;
    private String from;
    private List<HeaderEmailAddr> fromAddr;
    private String to;
    private List<HeaderEmailAddr> toAddr;
    private String cc;
    private List<HeaderEmailAddr> ccAddr;
    private String bcc;
    private List<HeaderEmailAddr> bccAddr;
    private String htmlContent;
    private String plainContent;

    private String mailMessageId;

    private String references;

    private String inReplyTo;

    private LocalDateTime receivedDate;
    private String folderName;
    private boolean seen;

    public MailHeader toModel(ModelMapper modelMapper) {
        return modelMapper.map(this, MailHeader.class);
    }
    public static MailHeaderDto toDto(ModelMapper modelMapper, MailHeader mailHeader) {
        return MailReceiverUtil.addHeaderEmailAddresses(modelMapper.map(mailHeader, MailHeaderDto.class));
    }
}
