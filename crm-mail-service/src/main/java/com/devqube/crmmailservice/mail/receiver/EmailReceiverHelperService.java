package com.devqube.crmmailservice.mail.receiver;

import com.devqube.crmmailservice.mail.list.MailFolderRepository;
import com.devqube.crmmailservice.mail.model.MailFolder;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailContentAttachmentDto;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailContentDto;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailHeaderDto;
import com.devqube.crmshared.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.mail.util.MimeMessageParser;
import org.springframework.stereotype.Service;

import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EmailReceiverHelperService {
    private final MailFolderRepository mailFolderRepository;

    public EmailReceiverHelperService(MailFolderRepository mailFolderRepository) {
        this.mailFolderRepository = mailFolderRepository;
    }

    public String getFolderFullName(Long id, String folderName) {
        Optional<MailFolder> first = mailFolderRepository
                .findAllByConfigurationIdAndFolderNameAndConfigurationActiveTrue(id, folderName)
                .stream().findFirst();
        return (first.isEmpty()) ? folderName : first.get().getFolderFullName();
    }

    public Message getMessageById(Long messageId, Folder folder, UIDFolder uidFolder) throws MessagingException {
        long[] ids = new long[1];
        ids[0] = messageId;
        Message[] message = uidFolder.getMessagesByUID(ids);
        folder.fetch(message, MailReceiverUtil.getFullMsgFetchProfile());
        return message[0];
    }


    public MailContentDto getMailContentDto(Message message, Long messageId, MailHeaderDto headerDto) throws EntityNotFoundException {
        try {
            MimeMessageParser parser = new MimeMessageParser((MimeMessage) message).parse();
            List<MailContentAttachmentDto> attachmentList = parser.getAttachmentList().stream()
                    .map(c -> new MailContentAttachmentDto(c.getName(), getAttachmentSize(c))).collect(Collectors.toList());
            return new MailContentDto(messageId, getHtmlContent(parser), parser.getPlainContent(), message.getSubject(), headerDto, attachmentList);
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new EntityNotFoundException("message not found");
        }
    }

    private String getHtmlContent(MimeMessageParser parser) {
        String result = parser.getHtmlContent();
        Collection<String> contentIds = parser.getContentIds();
        for (String key : contentIds) {
            try {
                DataSource ds = parser.findAttachmentByCid(key);
                String prefix = "data:" + ds.getContentType() + ";base64,";
                String image = prefix + Base64.getEncoder().encodeToString(ds.getInputStream().readAllBytes());
                result = result.replaceAll("cid:" + key, image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private Integer getAttachmentSize(DataSource dataSource) {
        try {
            return dataSource.getInputStream().readAllBytes().length;
        } catch (IOException e) {
            return 0;
        }
    }

    public MailHeaderDto getMailHeaderDto(String folderName, Message message, Long messageIdInFolder) throws MessagingException {
        MailHeaderDto headerDto = new MailHeaderDto();
        headerDto.setMessageId(messageIdInFolder);
        headerDto.setSubject(decode(MailReceiverUtil.getHeaderValue(message, "Subject")));
        headerDto.setFrom(decode(MailReceiverUtil.getHeaderValue(message, "From")));
        headerDto.setTo(decode(MailReceiverUtil.getHeaderValue(message, "To")));
        headerDto.setCc(MailReceiverUtil.getHeaderValue(message, "Cc"));
        headerDto.setBcc(MailReceiverUtil.getHeaderValue(message, "Bcc"));
        headerDto.setReceivedDate(MailReceiverUtil.dateHeaderToLocalDateTime(
                MailReceiverUtil.getHeaderValue(message, "Date")));
        headerDto.setMailMessageId(MailReceiverUtil.getHeaderValue(message, "Message-ID"));
        headerDto.setReferences(MailReceiverUtil.getHeaderValue(message, "References"));
        headerDto.setInReplyTo(MailReceiverUtil.getHeaderValue(message, "In-Reply-To"));
        headerDto.setFolderName(folderName);
        headerDto.setSeen(message.getFlags().contains(Flags.Flag.SEEN));
        return MailReceiverUtil.addHeaderEmailAddresses(headerDto);
    }


    public String decode(String value) {
        if (value != null) {
            try {
                return MimeUtility.decodeText(value);
            } catch (UnsupportedEncodingException e) {
                return "";
            }
        }
        return "";
    }
}
