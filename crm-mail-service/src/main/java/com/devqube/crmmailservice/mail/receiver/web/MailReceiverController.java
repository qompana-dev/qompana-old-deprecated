package com.devqube.crmmailservice.mail.receiver.web;

import com.devqube.crmmailservice.mail.exception.MailException;
import com.devqube.crmmailservice.mail.receiver.AsyncMailReceiverService;
import com.devqube.crmmailservice.mail.receiver.MailReceiverService;
import com.devqube.crmmailservice.mail.receiver.MailReceiverUtil;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailContentDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.net.URLConnection;
import java.util.Optional;

@RestController
@Slf4j
public class MailReceiverController implements MailReceiverApi {

    private final MailReceiverService mailReceiverService;

    private final AsyncMailReceiverService asyncMailReceiverService;

    public MailReceiverController(AsyncMailReceiverService asyncMailReceiverService, MailReceiverService mailReceiverService) {
        this.asyncMailReceiverService = asyncMailReceiverService;
        this.mailReceiverService = mailReceiverService;
    }

    @Override
    @GetMapping("/messages/{messageId}")
    public ResponseEntity<MailContentDto> getMessage(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable Long messageId) throws EntityNotFoundException, BadRequestException {
        return new ResponseEntity<>(mailReceiverService.getMessage(messageId, loggedAccountEmail), HttpStatus.OK);
    }

    @Override
    @GetMapping("/messages/refresh")
    public ResponseEntity<MailContentDto> refreshMessages(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail) throws EntityNotFoundException, BadRequestException, MailException, KafkaSendMessageException {
        mailReceiverService.refreshEmailsForUser(loggedAccountEmail);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @GetMapping("/messages/folder/{folderName}/message-id/{messageId}/file-name/{fileName}")
    public ResponseEntity<InputStreamResource> downloadAttachment(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable String folderName, @PathVariable Long messageId, @PathVariable String fileName) throws EntityNotFoundException, BadRequestException, MailException, KafkaSendMessageException {
        try {
            return MailReceiverUtil.createFileHttpResponse(fileName, mailReceiverService.getAttachment(loggedAccountEmail, folderName, messageId, fileName));
        } catch (Exception e) {
            log.info(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    @GetMapping("/messages/save-in-crm/folder/{folderName}/message-id/{messageId}/file-name/{fileName}")
    public ResponseEntity<Void> saveFileInCrm(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable String folderName, @PathVariable Long messageId, @PathVariable String fileName) throws EntityNotFoundException, BadRequestException, MailException, KafkaSendMessageException {
        try {
            mailReceiverService.saveFileInCrm(loggedAccountEmail, folderName, messageId, fileName);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            log.info(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    @DeleteMapping("/messages/folder/{folderName}/message-id/{messageId}")
    public ResponseEntity<Void> deleteMessage(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable String folderName, @PathVariable Long messageId) throws EntityNotFoundException, BadRequestException, MailException, KafkaSendMessageException {
        try {
            mailReceiverService.deleteMessage(loggedAccountEmail, folderName, messageId);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            log.info(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }
}
