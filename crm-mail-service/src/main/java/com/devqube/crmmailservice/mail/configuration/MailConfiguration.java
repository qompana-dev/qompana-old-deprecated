package com.devqube.crmmailservice.mail.configuration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MailConfiguration {
    @Id
    @SequenceGenerator(name = "mail_configuration_seq", sequenceName = "mail_configuration_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mail_configuration_seq")
    private Long id;

    @NotNull
    @Column(unique = true)
    @EqualsAndHashCode.Include
    private Long accountId;

    @NotNull
    private String imapServerAddress;

    @NotNull
    private Long imapServerPort;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private SecurityType imapSecurityType;

    @NotNull
    private String smtpServerAddress;

    @NotNull
    private Long smtpServerPort;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private SecurityType smtpSecurityType;

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private Boolean active;

    private LocalDateTime lastUpdate;

    public enum SecurityType {
        TLS, SSL
    }
}
