package com.devqube.crmmailservice.mail.sender.web.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NewMailAttachment {
    private String fileId;
    private boolean tempFile;
}
