package com.devqube.crmmailservice.mail.storage;

import com.devqube.crmmailservice.mail.model.MailStorage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface MailStorageRepository extends JpaRepository<MailStorage, Long>, JpaSpecificationExecutor<MailStorage> {

    List<MailStorage> findAllByMailHeaderId(Long id);

    Long countAllByMailFileNameAndIdNot(String mailFileName, Long id);

}
