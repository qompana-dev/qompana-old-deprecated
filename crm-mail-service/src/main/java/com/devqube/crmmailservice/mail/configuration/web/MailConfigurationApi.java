package com.devqube.crmmailservice.mail.configuration.web;

import com.devqube.crmmailservice.mail.configuration.MailConfiguration;
import com.devqube.crmmailservice.mail.configuration.web.dto.SimpleMailConfigurationDto;
import com.devqube.crmmailservice.mail.exception.ConfigurationExistException;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface MailConfigurationApi {
    @ApiOperation(value = "Configuration exist")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "mail configuration exist", response = Boolean.class),
            @ApiResponse(code = 403, message = "trying to check mail configuration for other user")
    })
    ResponseEntity<Boolean> configurationExist(String loggedAccountEmail, Long id) throws PermissionDeniedException, BadRequestException;

    @ApiOperation(value = "Remove configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "configuration removed"),
            @ApiResponse(code = 403, message = "trying to remove mail configuration for other user")
    })
    ResponseEntity<Void> removeConfiguration(String loggedAccountEmail, Long id) throws PermissionDeniedException, BadRequestException, EntityNotFoundException;

    @ApiOperation(value = "Get active mail configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "mail configuration found successfully", response = MailConfiguration.class),
            @ApiResponse(code = 404, message = "mail configuration not found"),
            @ApiResponse(code = 403, message = "trying to get mail configuration for other user")
    })
    ResponseEntity<MailConfiguration> getMailConfiguration(String loggedAccountEmail, Long id) throws EntityNotFoundException, BadRequestException, PermissionDeniedException;

    @ApiOperation(value = "Get active mail configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "mail configuration for account found successfully", response = MailConfiguration.class),
            @ApiResponse(code = 404, message = "mail configuration not found"),
            @ApiResponse(code = 403, message = "trying to get mail configuration for other user")
    })
    ResponseEntity<MailConfiguration> getActiveConfiguration(String loggedAccountEmail, Long accountId) throws EntityNotFoundException, BadRequestException, PermissionDeniedException;

    @ApiOperation(value = "Get mail configurations")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "mail configurations for account found successfully", response = MailConfiguration.class),
            @ApiResponse(code = 403, message = "trying to get mail configuration for other user")
    })
    ResponseEntity<List<MailConfiguration>> getUserConfigurations(String loggedAccountEmail,Long accountId) throws BadRequestException, PermissionDeniedException;

    @ApiOperation(value = "Add mail configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "mail configuration for account set successfully", response = MailConfiguration.class),
            @ApiResponse(code = 400, message = "bad request"),
            @ApiResponse(code = 403, message = "trying to set mail configuration for other user"),
            @ApiResponse(code = 409, message = "configuration exist"),
            @ApiResponse(code = 423, message = "too many configurations")
    })
    ResponseEntity<MailConfiguration> addConfiguration(String loggedAccountEmail, MailConfiguration mailConfiguration) throws BadRequestException, PermissionDeniedException, ConfigurationExistException;

    @ApiOperation(value = "Update mail configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "mail configuration updated successfully", response = MailConfiguration.class),
            @ApiResponse(code = 400, message = "bad request"),
            @ApiResponse(code = 403, message = "trying to set mail configuration for other user"),
            @ApiResponse(code = 409, message = "configuration exist")
    })
    ResponseEntity<MailConfiguration> updateConfiguration(String loggedAccountEmail, Long id, MailConfiguration mailConfiguration) throws BadRequestException, PermissionDeniedException, ConfigurationExistException, EntityNotFoundException;

    @ApiOperation(value = "Add simple mail configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "mail configuration for account set successfully", response = MailConfiguration.class),
            @ApiResponse(code = 400, message = "bad request"),
            @ApiResponse(code = 403, message = "trying to set mail configuration for other user"),
            @ApiResponse(code = 409, message = "configuration exist"),
            @ApiResponse(code = 423, message = "too many configurations"),
            @ApiResponse(code = 424, message = "unable to obtain mail configuration")
    })
    ResponseEntity<MailConfiguration> addSimpleConfiguration(String loggedAccountEmail, SimpleMailConfigurationDto simpleMailConfigurationDto) throws BadRequestException, PermissionDeniedException, ConfigurationExistException;

    @ApiOperation(value = "Update simple mail configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "mail configuration updated successfully", response = MailConfiguration.class),
            @ApiResponse(code = 400, message = "bad request"),
            @ApiResponse(code = 403, message = "trying to set mail configuration for other user"),
            @ApiResponse(code = 409, message = "configuration exist"),
            @ApiResponse(code = 424, message = "unable to obtain mail configuration")
    })
    ResponseEntity<MailConfiguration> updateSimpleConfiguration(String loggedAccountEmail, Long id, SimpleMailConfigurationDto simpleMailConfigurationDto) throws BadRequestException, PermissionDeniedException, ConfigurationExistException, EntityNotFoundException;


    @ApiOperation(value = "Change active configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "active configuration updated successfully", response = MailConfiguration.class),
            @ApiResponse(code = 400, message = "bad request"),
            @ApiResponse(code = 404, message = "configuration not found")
    })
    ResponseEntity<MailConfiguration> changeActiveConfiguration(String loggedAccountEmail, Long id) throws BadRequestException, EntityNotFoundException;
}
