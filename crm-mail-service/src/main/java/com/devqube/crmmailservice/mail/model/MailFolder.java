package com.devqube.crmmailservice.mail.model;

import com.devqube.crmmailservice.mail.configuration.MailConfiguration;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MailFolder {
    @Id
    @SequenceGenerator(name = "mail_folder_header_seq", sequenceName = "mail_folder_header_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mail_folder_header_seq")
    private Long id;

    private String folderName;

    private String folderFullName;

    @ManyToOne
    private MailConfiguration configuration;
}
