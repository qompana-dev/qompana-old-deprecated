package com.devqube.crmmailservice.mail.storage.web.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class EmailStorageInfoDto {
    private Long messageId;
    private String folderName;
    private Long configurationId;
    private Long accountId;
    private String imapServerAddress;
    private String smtpServerAddress;
    private String username;
}
