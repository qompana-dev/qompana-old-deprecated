package com.devqube.crmmailservice.mail.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class TooManyConfigurationsException extends Exception {
    public TooManyConfigurationsException() {
    }

    public TooManyConfigurationsException(String message) {
        super(message);
    }
}
