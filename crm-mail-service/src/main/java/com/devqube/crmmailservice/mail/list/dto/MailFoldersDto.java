package com.devqube.crmmailservice.mail.list.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MailFoldersDto {
    private String mailAddress;
    private List<MailFolderDto> folders;
}
