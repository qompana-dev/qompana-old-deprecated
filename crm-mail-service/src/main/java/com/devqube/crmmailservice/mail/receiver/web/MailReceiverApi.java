package com.devqube.crmmailservice.mail.receiver.web;

import com.devqube.crmmailservice.mail.exception.MailException;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailContentDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface MailReceiverApi {

    @ApiOperation(value = "Get message by messageId")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "message details returned successfully", response = MailContentDto.class),
            @ApiResponse(code = 400, message = "bad request"),
            @ApiResponse(code = 404, message = "message not found")
    })
    ResponseEntity<MailContentDto> getMessage(String loggedAccountEmail, Long messageId) throws EntityNotFoundException, BadRequestException;

    @GetMapping("/messages/refresh")
    ResponseEntity<MailContentDto> refreshMessages(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail) throws EntityNotFoundException, BadRequestException, MailException, KafkaSendMessageException;

    @ApiOperation(value = "Download file", nickname = "downloadAttachment", notes = "Method used to download attachment from message", response = Resource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Downloaded file", response = Resource.class),
            @ApiResponse(code = 404, message = "File not found") })
    ResponseEntity<InputStreamResource> downloadAttachment(String loggedAccountEmail, String folderName, Long messageId, String fileName) throws EntityNotFoundException, BadRequestException, MailException, KafkaSendMessageException;

    @ApiOperation(value = "Save attachment in CRM", nickname = "saveFileInCrm", notes = "Method used to save attachment from message in crm", response = Resource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File saved in CRM", response = Resource.class),
            @ApiResponse(code = 404, message = "File not found") })
    ResponseEntity<Void> saveFileInCrm(String loggedAccountEmail, String folderName, Long messageId, String fileName) throws EntityNotFoundException, BadRequestException, MailException, KafkaSendMessageException;

    @ApiOperation(value = "Delete message", nickname = "deleteMessage", notes = "Method used to delete message")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Message deleted"),
            @ApiResponse(code = 404, message = "File not found") })
    ResponseEntity<Void> deleteMessage(String loggedAccountEmail, String folderName, Long messageId) throws EntityNotFoundException, BadRequestException, MailException, KafkaSendMessageException;
}
