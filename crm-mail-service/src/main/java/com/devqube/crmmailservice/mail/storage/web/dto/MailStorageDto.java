package com.devqube.crmmailservice.mail.storage.web.dto;

import com.devqube.crmmailservice.mail.receiver.web.dto.HeaderEmailAddr;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailHeaderDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MailStorageDto {
    private Long id;
    private String mailFileName;
    private Long messageId;
    private Long mailHeaderId;
    private String subject;
    private String from;
    private List<HeaderEmailAddr> fromAddr;
    private String to;
    private List<HeaderEmailAddr> toAddr;
    private String cc;
    private List<HeaderEmailAddr> ccAddr;
    private String bcc;
    private List<HeaderEmailAddr> bccAddr;
    private LocalDateTime receivedDate;
    private String htmlContent;
    private String plainContent;
    private String mailMessageId;
    private String references;
    private List<MailHeaderDto> relatedMails;
    private String inReplyTo;
    private String folderName;
    private Long accountId;
    private String username;
}
