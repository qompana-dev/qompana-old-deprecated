package com.devqube.crmmailservice.mail.receiver.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class MailContentDto {
    private Long messageId;
    private String htmlContent;
    private String plainContent;
    private String subject;
    private MailHeaderDto headerDto;
    private List<MailContentAttachmentDto> attachmentList = new ArrayList<>();
}
