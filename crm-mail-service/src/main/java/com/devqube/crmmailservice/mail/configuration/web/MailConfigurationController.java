package com.devqube.crmmailservice.mail.configuration.web;

import com.devqube.crmmailservice.autoconfiguration.exception.EmailConfigurationNotFound;
import com.devqube.crmmailservice.mail.configuration.MailConfiguration;
import com.devqube.crmmailservice.mail.configuration.MailConfigurationService;
import com.devqube.crmmailservice.mail.configuration.web.dto.SimpleMailConfigurationDto;
import com.devqube.crmmailservice.mail.exception.ConfigurationExistException;
import com.devqube.crmmailservice.mail.exception.TooManyConfigurationsException;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class MailConfigurationController implements MailConfigurationApi {

    private final MailConfigurationService mailConfigurationService;

    public MailConfigurationController(MailConfigurationService mailConfigurationService) {
        this.mailConfigurationService = mailConfigurationService;
    }

    @Override
    @GetMapping("/{id}/exist")
    public ResponseEntity<Boolean> configurationExist(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable Long id) throws PermissionDeniedException, BadRequestException {
        return ResponseEntity.ok(mailConfigurationService.configurationExist(loggedAccountEmail, id));
    }

    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> removeConfiguration(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable Long id) throws PermissionDeniedException, BadRequestException, EntityNotFoundException {
        mailConfigurationService.removeConfiguration(loggedAccountEmail, id);
        return ResponseEntity.noContent().build();
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity<MailConfiguration> getMailConfiguration(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable Long id) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        return new ResponseEntity<>(mailConfigurationService.getConfiguration(loggedAccountEmail, id), HttpStatus.OK);
    }

    @Override
    @GetMapping("/account/{accountId}/active")
    public ResponseEntity<MailConfiguration> getActiveConfiguration(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable Long accountId) throws EntityNotFoundException, BadRequestException, PermissionDeniedException {
        return new ResponseEntity<>(mailConfigurationService.findActiveConfiguration(loggedAccountEmail, accountId), HttpStatus.OK);
    }

    @Override
    @GetMapping("/account/{accountId}")
    public ResponseEntity<List<MailConfiguration>> getUserConfigurations(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable Long accountId) throws BadRequestException, PermissionDeniedException {
        return new ResponseEntity<>(mailConfigurationService.findAllByAccountId(loggedAccountEmail, accountId), HttpStatus.OK);
    }

    @Override
    @PostMapping
    public ResponseEntity<MailConfiguration> addConfiguration(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @RequestBody MailConfiguration mailConfiguration) throws BadRequestException, PermissionDeniedException, ConfigurationExistException {
        try {
            MailConfiguration saved = mailConfigurationService.save(loggedAccountEmail, mailConfiguration);
            saved.setPassword(null);
            return new ResponseEntity<>(saved, HttpStatus.OK);
        } catch (TooManyConfigurationsException e) {
            return ResponseEntity.status(423).build();
        }
    }

    @Override
    @PutMapping("/{id}")
    public ResponseEntity<MailConfiguration> updateConfiguration(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable Long id, @RequestBody MailConfiguration mailConfiguration) throws BadRequestException, PermissionDeniedException, ConfigurationExistException, EntityNotFoundException {
        MailConfiguration saved = mailConfigurationService.update(loggedAccountEmail, id, mailConfiguration);
        saved.setPassword(null);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }

    @Override
    @PostMapping("/simple")
    public ResponseEntity<MailConfiguration> addSimpleConfiguration(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @RequestBody SimpleMailConfigurationDto simpleMailConfigurationDto) throws BadRequestException, PermissionDeniedException, ConfigurationExistException {
        try {
            MailConfiguration saved = mailConfigurationService.saveSimpleConfiguration(loggedAccountEmail, simpleMailConfigurationDto);
            saved.setPassword(null);
            return new ResponseEntity<>(saved, HttpStatus.OK);
        } catch (TooManyConfigurationsException e) {
            return ResponseEntity.status(423).build();
        } catch (EmailConfigurationNotFound emailConfigurationNotFound) {
            return ResponseEntity.status(424).build();
        }
    }

    @Override
    @PutMapping("/simple/{id}")
    public ResponseEntity<MailConfiguration> updateSimpleConfiguration(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable Long id, @RequestBody SimpleMailConfigurationDto simpleMailConfigurationDto) throws BadRequestException, PermissionDeniedException, ConfigurationExistException, EntityNotFoundException {
        try {
            MailConfiguration saved = mailConfigurationService.updateSimpleConfiguration(loggedAccountEmail, id, simpleMailConfigurationDto);
            saved.setPassword(null);
            return new ResponseEntity<>(saved, HttpStatus.OK);
        } catch (EmailConfigurationNotFound emailConfigurationNotFound) {
            return ResponseEntity.status(424).build();
        }
    }

    @Override
    @PostMapping("/{id}/active")
    public ResponseEntity<MailConfiguration> changeActiveConfiguration(@RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail, @PathVariable Long id) throws BadRequestException, EntityNotFoundException {
        MailConfiguration saved = mailConfigurationService.changeActiveConfiguration(loggedAccountEmail, id);
        saved.setPassword(null);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }
}
