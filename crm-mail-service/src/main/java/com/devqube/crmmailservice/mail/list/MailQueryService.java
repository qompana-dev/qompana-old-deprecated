package com.devqube.crmmailservice.mail.list;

import com.devqube.crmmailservice.mail.configuration.MailConfiguration;
import com.devqube.crmmailservice.mail.configuration.MailConfigurationRepository;
import com.devqube.crmmailservice.mail.list.dto.MailFolderDto;
import com.devqube.crmmailservice.mail.list.dto.MailFoldersDto;
import com.devqube.crmmailservice.mail.model.MailFolder;
import com.devqube.crmmailservice.mail.model.MailHeader;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailHeaderDto;
import com.devqube.crmmailservice.mail.storage.MailStorageService;
import com.devqube.crmmailservice.mail.storage.web.dto.MailStorageDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.search.CrmObjectType;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MailQueryService {

    private final MailFolderRepository mailFolderRepository;
    private final MailConfigurationRepository mailConfigurationRepository;
    private final MailHeaderRepository mailHeaderRepository;
    private final MailStorageService mailStorageService;
    private final ModelMapper modelMapper;

    public MailQueryService(MailHeaderRepository mailHeaderRepository, @Qualifier("strictModelMapper")
                            ModelMapper modelMapper, MailConfigurationRepository mailConfigurationRepository,
                            MailFolderRepository mailFolderRepository, MailStorageService mailStorageService) {
        this.mailHeaderRepository = mailHeaderRepository;
        this.modelMapper = modelMapper;
        this.mailConfigurationRepository = mailConfigurationRepository;
        this.mailFolderRepository = mailFolderRepository;
        this.mailStorageService = mailStorageService;
    }

    public Page<MailHeaderDto> getAllMailHeaders(Long userId, Pageable pageable) {
        return this.mailHeaderRepository.findAllByConfigurationAccountIdAndConfigurationActiveTrue(userId, pageable)
                .map(mailHeader -> MailHeaderDto.toDto(modelMapper, mailHeader));
    }

    @Transactional(rollbackOn = EntityNotFoundException.class)
    public MailFoldersDto getMailFolders(Long accountId) throws EntityNotFoundException {
        MailConfiguration config = mailConfigurationRepository.findByAccountIdAndActiveTrue(accountId).orElseThrow(EntityNotFoundException::new);
        Set<MailFolderDto> folders = mailHeaderRepository.getMailFolders(accountId);
        Set<MailFolderDto> allFolders = getAllMailFolders(accountId);
        List<MailFolderDto> mergedFolders = mergeFolders(folders, allFolders);
        return MailFoldersDto.builder().mailAddress(config.getUsername()).folders(mergedFolders).build();
    }

    private List<MailFolderDto> mergeFolders(Set<MailFolderDto> folders, Set<MailFolderDto> allFolders) {
        folders.addAll(allFolders);
        return folders.stream().sorted().collect(Collectors.toList());
    }

    private Set<MailFolderDto> getAllMailFolders(Long accountId) {
        Set<MailFolder> folders = mailFolderRepository.findAllByConfigurationAccountIdAndConfigurationActiveTrue(accountId);
        return folders.stream().map(folder -> this.modelMapper.map(folder, MailFolderDto.class)).collect(Collectors.toSet());
    }

    public Page<MailHeaderDto> getMailHeadersInFolder(Long userId, String folder, Pageable pageable) {
        return this.mailHeaderRepository.findAllByConfigurationAccountIdAndFolderNameAndConfigurationActiveTrue(userId, folder, pageable)
                .map(mailHeader -> MailHeaderDto.toDto(modelMapper, mailHeader));
    }

    public Page<MailHeaderDto> getAttachedMailHeaders(CrmObjectType type, Long id, Pageable pageable) throws BadRequestException {
        Page<MailStorageDto> mailStorages = mailStorageService.getMailHeaders(type, id, pageable);
        return mailStorages.map(storage -> {
            MailHeader mailHeader = mailHeaderRepository.findById(storage.getMailHeaderId()).get();
            return MailHeaderDto.toDto(modelMapper, mailHeader);
        });
    }
}
