package com.devqube.crmmailservice.mail.sender.web;

import com.devqube.crmmailservice.mail.sender.web.dto.ForwardedMailDto;
import com.devqube.crmmailservice.mail.sender.web.dto.NewMailDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;

public interface MailSenderApi {

    @ApiOperation(value = "Send mail")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "mail sended"),
            @ApiResponse(code = 400, message = "bad request"),
            @ApiResponse(code = 404, message = "mail configuration not found")
    })
    ResponseEntity<Void> sendMail(String loggedAccountEmail, NewMailDto newMailDto) throws BadRequestException, PermissionDeniedException;

    @ApiOperation(value = "Forward mail")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "mail forwarded"),
            @ApiResponse(code = 400, message = "bad request"),
            @ApiResponse(code = 404, message = "mail configuration not found")
    })
    ResponseEntity<Void> forwardMail(String loggedAccountEmail, ForwardedMailDto forwardedMailDto) throws BadRequestException, PermissionDeniedException;
}
