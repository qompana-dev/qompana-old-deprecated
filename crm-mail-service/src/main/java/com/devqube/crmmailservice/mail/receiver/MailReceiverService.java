package com.devqube.crmmailservice.mail.receiver;

import com.devqube.crmmailservice.internalClients.file.FileService;
import com.devqube.crmmailservice.internalClients.user.UserService;
import com.devqube.crmmailservice.mail.configuration.MailConfiguration;
import com.devqube.crmmailservice.mail.configuration.MailConfigurationRepository;
import com.devqube.crmmailservice.mail.connection.ImapConnectionHolder;
import com.devqube.crmmailservice.mail.exception.MailException;
import com.devqube.crmmailservice.mail.list.MailFolderRepository;
import com.devqube.crmmailservice.mail.list.MailHeaderRepository;
import com.devqube.crmmailservice.mail.model.MailFolder;
import com.devqube.crmmailservice.mail.model.MailHeader;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailContentDto;
import com.devqube.crmmailservice.mail.receiver.web.dto.MailHeaderDto;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.KafkaSendMessageException;
import com.devqube.crmshared.kafka.KafkaService;
import com.devqube.crmshared.kafka.util.KafkaMessageDataBuilder;
import com.devqube.crmshared.notification.NotificationMsgType;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.mail.util.MimeMessageParser;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.ReceivedDateTerm;
import javax.mail.search.SearchTerm;
import java.io.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static javax.mail.Folder.HOLDS_FOLDERS;

@Slf4j
@Service
public class MailReceiverService {

    private final MailHeaderRepository mailHeaderRepository;
    private final ModelMapper strictModelMapper;
    private final MailConfigurationRepository mailConfigurationRepository;
    private final MailFolderRepository mailFolderRepository;
    private final KafkaService kafkaService;
    private final ImapConnectionHolder imapConnectionHolder;
    private final UserService userService;
    private final FileService fileService;
    private final EmailReceiverHelperService emailReceiverHelperService;

    public MailReceiverService(MailHeaderRepository mailHeaderRepository, @Qualifier("strictModelMapper") ModelMapper strictModelMapper,
                               MailConfigurationRepository mailConfigurationRepository, MailFolderRepository mailFolderRepository, KafkaService kafkaService,
                               ImapConnectionHolder imapConnectionHolder, UserService userService, EmailReceiverHelperService emailReceiverHelperService, FileService fileService) {
        this.mailHeaderRepository = mailHeaderRepository;
        this.strictModelMapper = strictModelMapper;
        this.mailConfigurationRepository = mailConfigurationRepository;
        this.mailFolderRepository = mailFolderRepository;
        this.kafkaService = kafkaService;
        this.imapConnectionHolder = imapConnectionHolder;
        this.userService = userService;
        this.fileService = fileService;
        this.emailReceiverHelperService = emailReceiverHelperService;
    }

    @Transactional
    public void refreshEmailsForUser(String mail) throws MailException, KafkaSendMessageException, BadRequestException, EntityNotFoundException {
        Long userId = userService.getUserId(mail);
        MailConfiguration config = mailConfigurationRepository.findByAccountIdAndActiveTrue(userId).orElseThrow(EntityNotFoundException::new);
        refreshEmailsForConfiguration(config);
    }

    @Transactional
    public void refreshEmailsForConfiguration(MailConfiguration configuration) throws MailException, KafkaSendMessageException {
        if (configuration.getLastUpdate() == null) {
            synchronizeEmailsForConfiguration(configuration);
        } else {
            Date from = java.sql.Date.valueOf(configuration.getLastUpdate().toLocalDate().minusDays(1));
            List<MailHeader> newMails = getMailList(configuration, from)
                    .stream().map(c -> c.toModel(strictModelMapper))
                    .peek(c -> c.setConfiguration(configuration)).collect(Collectors.toList());
            List<MailHeader> mailsInDb = mailHeaderRepository.findAllByConfigurationIdAndReceivedDateAfter(configuration.getId(), configuration.getLastUpdate().toLocalDate().atTime(0, 0, 0));

            synchronizeLists(newMails, mailsInDb, configuration);
        }
    }

    public void synchronizeEmailsForConfiguration(MailConfiguration configuration) throws MailException, KafkaSendMessageException {
        List<MailHeader> allMails = getMailList(configuration, null)
                .stream().map(c -> c.toModel(strictModelMapper)).collect(Collectors.toList());
        List<MailHeader> mailsInDb = mailHeaderRepository.findAllByConfigurationId(configuration.getId());

        synchronizeLists(allMails, mailsInDb, configuration);
    }

    public MailContentDto getMessage(Long messageId, String loggedUserEmail) throws BadRequestException, EntityNotFoundException {
        Long accountId = userService.getUserId(loggedUserEmail);
        MailConfiguration mailConfiguration = mailConfigurationRepository.findByAccountIdAndActiveTrue(accountId).orElseThrow(EntityNotFoundException::new);
        MailHeader mailHeader = mailHeaderRepository.findById(messageId).orElseThrow(EntityNotFoundException::new);
        return getEmailByMessageId(mailConfiguration, mailHeader.getMessageId(), mailHeader.getFolderName());
    }

    public MimeMessage getMessageToForward(Long messageId, Long accountId, Session session) throws EntityNotFoundException, BadRequestException {
        MailConfiguration configuration = mailConfigurationRepository.findByAccountIdAndActiveTrue(accountId).orElseThrow(EntityNotFoundException::new);
        MailHeader mailHeader = mailHeaderRepository.findById(messageId).orElseThrow(EntityNotFoundException::new);

        Folder folder = null;
        try {
            Store store = imapConnectionHolder.getAndStoreConnection(configuration);
            folder = store.getFolder(emailReceiverHelperService.getFolderFullName(configuration.getId(), mailHeader.getFolderName()));
            folder.open(Folder.READ_ONLY);
            long[] ids = {mailHeader.getMessageId()};
            Message[] message = ((UIDFolder) folder).getMessagesByUID(ids);
            folder.fetch(message, MailReceiverUtil.getContentFetchProfile());
            if (message == null) {
                throw new EntityNotFoundException();
            }
            MimeMessage mimeMessage = getCopyOfMimeMessage(session, message[0]);
            return mimeMessage;
        } catch (MessagingException | IOException e) {
            log.info(e.getMessage());
            throw new BadRequestException("mail configuration is not valid");
        } finally {
            if (folder != null) {
                try {
                    folder.close();
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private MimeMessage getCopyOfMimeMessage(Session session, Message message) throws MessagingException, IOException {
        MimeMessage toCopy = (MimeMessage) message;
        MimeMessage toReturn = new MimeMessage(session);
        toReturn.setSubject("FWD: " + toCopy.getSubject());
        Object content = toCopy.getContent();
        if (content instanceof MimeMultipart) {
            toReturn.setContent((MimeMultipart) content);
        } else if (content instanceof String) {
            toReturn.setText((String) content, "UTF-8");
        }
        return toReturn;
    }

    public MailContentDto getEmailByMessageId(MailConfiguration configuration, Long messageId, String folderName) throws BadRequestException {
        Folder folder = null;
        try {
            Store store = imapConnectionHolder.getAndStoreConnection(configuration);
            folder = store.getFolder(emailReceiverHelperService.getFolderFullName(configuration.getId(), folderName));
            UIDFolder uidFolder = (UIDFolder) folder;
            folder.open(Folder.READ_WRITE);
            Message message = emailReceiverHelperService.getMessageById(messageId, folder, uidFolder);
            MailContentDto mailContentDto = emailReceiverHelperService.getMailContentDto(message, messageId, getMailHeaderDto(uidFolder, folderName, message));
            markMsgAsRead(message);
            return mailContentDto;
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new BadRequestException("mail configuration is not valid");
        } finally {
            if (folder != null) {
                try {
                    folder.close();
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean isImapConnectionEstablished(MailConfiguration configuration) {
        try {
            imapConnectionHolder.getConnection(configuration).close();
            return true;
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<MailHeaderDto> getMailList(MailConfiguration configuration, Date from) throws MailException {
        List<MailHeaderDto> result = new ArrayList<>();
        Store store = null;
        try {
            store = imapConnectionHolder.getConnection(configuration);

            Folder[] allFolders = store.getDefaultFolder().list("*");
            for (Folder folder : allFolders) {
                if ((folder.getType() & javax.mail.Folder.HOLDS_MESSAGES) != 0) {
                    result.addAll(getMailListForFolder(folder, from));
                }
            }
            saveFolders(configuration, allFolders);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MailException();
        } finally {
            if (store != null) {
                try {
                    store.close();
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public void saveFileInCrm(String email, String folderName, Long messageId, String fileName) throws Exception {
        byte[] inputStreamResource = getAttachment(email, folderName, messageId, fileName).getInputStream().readAllBytes();
        fileService.uploadAttachment(email, fileName, inputStreamResource);
    }

    public InputStreamResource getAttachment(String email, String folderName, Long messageId, String fileName) throws Exception {
        Long accountId = userService.getUserId(email);
        MailConfiguration configuration = mailConfigurationRepository.findByAccountIdAndActiveTrue(accountId).orElseThrow(EntityNotFoundException::new);
        DataSource dataSource = null;
        Folder folder = null;
        try {
            Store store = imapConnectionHolder.getAndStoreConnection(configuration);
            folder = store.getFolder(emailReceiverHelperService.getFolderFullName(configuration.getId(), folderName));
            UIDFolder uidFolder = (UIDFolder) folder;
            folder.open(Folder.READ_ONLY);
            Message message = emailReceiverHelperService.getMessageById(messageId, folder, uidFolder);
            MimeMessageParser parser = new MimeMessageParser((MimeMessage) message).parse();
            dataSource = parser.getAttachmentList().stream().filter(c -> c.getName().equals(fileName)).findFirst().orElseThrow(EntityNotFoundException::new);
        } catch (Exception e) {
            throw new MailException();
        } finally {
            if (folder != null) {
                try {
                    folder.close();
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        }
        return new InputStreamResource(dataSource.getInputStream());
    }

    @Transactional(rollbackFor = {Exception.class})
    public void deleteMessage(String email, String folderName, Long messageId) throws Exception {
        Long accountId = userService.getUserId(email);
        MailConfiguration configuration = mailConfigurationRepository.findByAccountIdAndActiveTrue(accountId).orElseThrow(EntityNotFoundException::new);
        Folder folder = null;
        try {
            Store store = imapConnectionHolder.getAndStoreConnection(configuration);
            folder = store.getFolder(emailReceiverHelperService.getFolderFullName(configuration.getId(), folderName));
            UIDFolder uidFolder = (UIDFolder) folder;
            folder.open(Folder.READ_WRITE);
            Message message = emailReceiverHelperService.getMessageById(messageId, folder, uidFolder);
            message.setFlag(Flags.Flag.DELETED, true);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MailException();
        } finally {
            if (folder != null) {
                try {
                    folder.close();
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        }

        mailHeaderRepository.deleteAllByConfigurationIdAndFolderNameAndMessageId(configuration.getId(), folderName, messageId);
        kafkaService.sendWebsocket("/topic/mails/change/" + configuration.getAccountId(), Boolean.TRUE.toString());
    }

    private void saveFolders(MailConfiguration configuration, Folder[] folders) {
        List<MailFolder> newFolders = Arrays.stream(folders)
                .filter(this::folderNotContainFolders)
                .map(c -> new MailFolder(null, c.getName(), c.getFullName(), configuration))
                .collect(Collectors.toList());
        mailFolderRepository.deleteAllByConfigurationId(configuration.getId());
        mailFolderRepository.saveAll(newFolders);
    }

    private boolean folderNotContainFolders(Folder folder) {
        try {
            return folder.getType() != HOLDS_FOLDERS;
        } catch (MessagingException e) {
            return false;
        }
    }

    private void synchronizeLists(List<MailHeader> allMails, List<MailHeader> mailsInDb, MailConfiguration configuration) throws KafkaSendMessageException {
        List<MailHeader> toRemove = mailsInDb.stream()
                .filter(c -> !allMails.contains(c)).collect(Collectors.toList());
        mailHeaderRepository.deleteAll(toRemove);

        mailHeaderRepository.flush();

        List<MailHeader> toAdd = allMails.stream()
                .filter(c -> !mailsInDb.contains(c))
                .peek(c -> c.setConfiguration(configuration)).collect(Collectors.toList());
        mailHeaderRepository.saveAll(toAdd);

        mailHeaderRepository.flush();

        LocalDateTime lastUpdate = configuration.getLastUpdate();
        configuration.setLastUpdate(getMaxDate(allMails, lastUpdate));
        mailConfigurationRepository.save(configuration);

        mailConfigurationRepository.flush();

        if (toAdd.size() != 0 || toRemove.size() != 0) {
            kafkaService.sendWebsocket("/topic/mails/change/" + configuration.getAccountId(), Boolean.TRUE.toString());
        }
        if (lastUpdate != null) {
            List<MailHeader> newMessages = new ArrayList<>(toAdd.stream()
                    .filter(c -> c.getReceivedDate() != null)
                    .filter(e -> e.getReceivedDate().isAfter(lastUpdate))
                    .collect(Collectors.toMap(MailHeaderKey::new, e -> e, (e, f) -> e))
                    .values());
            if (newMessages.size() > 0) {
                log.info("sending {} notification to user {}", newMessages.size(), configuration.getAccountId());
                for (MailHeader mh : newMessages) {
                    kafkaService.addNotification(configuration.getAccountId(), NotificationMsgType.NEW_MAIL,
                            KafkaMessageDataBuilder.builder().add("subject", mh.getSubject()).add("from", mh.getFrom()).build());
                }
            }
        }
    }

    private LocalDateTime getMaxDate(List<MailHeader> list, LocalDateTime current) {
        if (list == null || list.size() == 0) {
            return current;
        }
        return list.stream().map(MailHeader::getReceivedDate)
                .max(LocalDateTime::compareTo).orElse(current);
    }

    private void markMsgAsRead(Message msg) throws MessagingException {
        msg.setFlag(Flags.Flag.SEEN, true);
    }

    private List<MailHeaderDto> getMailListForFolder(Folder folder, Date from) throws MailException {
        List<MailHeaderDto> result = new ArrayList<>();
        try {
            log.info("fetch mails from folder " + folder.getName());
            folder.open(Folder.READ_ONLY);
            Message[] messages = null;
            if (from != null) {
                SearchTerm newerThan = new ReceivedDateTerm(ComparisonTerm.GT, from);
                messages = folder.search(newerThan);
            } else {
                messages = folder.getMessages();
            }
            folder.fetch(messages, MailReceiverUtil.getFetchProfile());
            String folderName = folder.getName();
            UIDFolder uidFolder = (UIDFolder) folder;
            for (Message message : messages) {
                result.add(getMailHeaderDto(uidFolder, folderName, message));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new MailException();
        } finally {
            if (folder != null) {
                try {
                    folder.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    private MailHeaderDto getMailHeaderDto(UIDFolder folder, String folderName, Message message) throws MessagingException {
        return emailReceiverHelperService.getMailHeaderDto(folderName, message, folder.getUID(message));
    }

    @EqualsAndHashCode
    private static class MailHeaderKey {
        private String subject;
        private String to;
        private String from;
        private LocalDateTime receivedDate;

        MailHeaderKey(MailHeader mailHeader) {
            this.subject = mailHeader.getSubject();
            this.to = mailHeader.getTo();
            this.from = mailHeader.getFrom();
            this.receivedDate = mailHeader.getReceivedDate();
        }
    }
}
