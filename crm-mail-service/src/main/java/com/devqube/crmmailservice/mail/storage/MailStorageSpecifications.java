package com.devqube.crmmailservice.mail.storage;

import com.devqube.crmmailservice.mail.model.MailStorage;
import org.springframework.data.jpa.domain.Specification;

import java.util.Set;

public class MailStorageSpecifications {

    public static Specification<MailStorage> findByIdIn(Set<Long> idList) {
        return (root, query, cb) ->
                root.get("id").in(idList);
    }

}
