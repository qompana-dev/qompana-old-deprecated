package com.devqube.crmmailservice.encryptor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

@Component
public class AesEncryptor {

    private SecretKeySpec secretKeySpec;

    public AesEncryptor(@Value("${aes.secret}") String secret) {
        try {
            byte[] secretBytes = secret.getBytes(StandardCharsets.UTF_8);
            secretBytes = Arrays.copyOf(MessageDigest.getInstance("SHA-1").digest(secretBytes), 16);
            secretKeySpec = new SecretKeySpec(secretBytes, "AES");
        } catch (NoSuchAlgorithmException e) {
            throw new AesException(e.getMessage());
        }
    }

    public String encrypt(String toEncrypt) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            return Base64.getEncoder().encodeToString(cipher.doFinal(toEncrypt.getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            throw new AesException(e.getMessage());
        }
    }

    public String decrypt(String toDecrypt) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            return new String(cipher.doFinal(Base64.getDecoder().decode(toDecrypt)));
        } catch (Exception e) {
            throw new AesException(e.getMessage());
        }
    }

    static class AesException extends RuntimeException {
        public AesException(String message) {
            super(message);
        }
    }
}
