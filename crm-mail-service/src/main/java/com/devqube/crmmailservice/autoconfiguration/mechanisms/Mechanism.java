package com.devqube.crmmailservice.autoconfiguration.mechanisms;

import com.devqube.crmmailservice.autoconfiguration.dto.AutoConfigurationRequestDto;
import com.devqube.crmmailservice.autoconfiguration.dto.MechanismResponse;

public interface Mechanism {
    public MechanismResponse getMailConfiguration(AutoConfigurationRequestDto autoConfigurationRequestDto);
}
