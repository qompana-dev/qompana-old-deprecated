package com.devqube.crmmailservice.autoconfiguration.mechanisms.impl;

import com.devqube.crmmailservice.autoconfiguration.dto.AutoConfigurationRequestDto;
import com.devqube.crmmailservice.autoconfiguration.dto.ConnectionDetailsDto;
import com.devqube.crmmailservice.autoconfiguration.dto.MechanismResponse;
import com.devqube.crmmailservice.autoconfiguration.exception.AutoConfigurationParseException;
import com.devqube.crmmailservice.autoconfiguration.mechanisms.Mechanism;
import com.devqube.crmmailservice.autoconfiguration.mechanisms.MechanismOrder;
import com.devqube.crmmailservice.autoconfiguration.parser.EmailConfigurationParser;
import com.devqube.crmmailservice.autoconfiguration.util.EmailConnectionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
@MechanismOrder(order = 2)
public class MechanismAutoconfig implements Mechanism {
    private final RestTemplate restTemplate;

    public MechanismAutoconfig(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public MechanismResponse getMailConfiguration(AutoConfigurationRequestDto autoConfigurationRequestDto) {
        String domain = EmailConnectionUtil.getDomain(autoConfigurationRequestDto.getUsername());
        String xml = getXmlFromAutoconfig(domain, true);
        if (xml == null) {
            xml = getXmlFromAutoconfig(domain, false);
        }
        if (xml != null) {
            try {
                ConnectionDetailsDto connectionDetailsDto = EmailConfigurationParser.parse(xml);
                return EmailConnectionUtil.getMechanismResponse(autoConfigurationRequestDto, connectionDetailsDto);
            } catch (AutoConfigurationParseException e) {
                log.info("parse exception for domain {}", domain);
                return MechanismResponse.builder().found(false).build();
            }
        }
        return MechanismResponse.builder().found(false).build();
    }

    private String getXmlFromAutoconfig(String domain, boolean https) {
        String prefix = (https) ? "https" : "http";
        try {
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(prefix + "://autoconfig." + domain, String.class);
            if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
                return responseEntity.getBody();
            }
        } catch (Exception ignore) {
            log.info("email configuration for domain {} not found", domain);
        }
        return null;
    }
}
