package com.devqube.crmmailservice.autoconfiguration.mechanisms.impl;

import com.devqube.crmmailservice.autoconfiguration.dto.AutoConfigurationRequestDto;
import com.devqube.crmmailservice.autoconfiguration.dto.ConnectionDetailsDto;
import com.devqube.crmmailservice.autoconfiguration.dto.MechanismResponse;
import com.devqube.crmmailservice.autoconfiguration.mechanisms.Mechanism;
import com.devqube.crmmailservice.autoconfiguration.mechanisms.MechanismOrder;
import com.devqube.crmmailservice.autoconfiguration.util.EmailConnectionUtil;
import org.springframework.stereotype.Service;

@Service
@MechanismOrder(order = 3)
public class MechanismGuessing implements Mechanism { // todo: mechanism with MX support
    private final String[] imapPrefix = new String[]{"imap.", "imap.poczta.", "mail.", ""};
    private final String[] smtpPrefix = new String[]{"smtp.", "smtp.poczta.", "mail.", ""};

    private final Integer IMAP_PORT_SSL = 993;
    private final Integer IMAP_PORT_TLS = 143;

    private final Integer SMTP_PORT_SSL = 465;
    private final Integer SMTP_PORT_TLS = 587;
    private final Integer SMTP_PORT_TLS2 = 25;

    @Override
    public MechanismResponse getMailConfiguration(AutoConfigurationRequestDto autoConfigurationRequestDto) {
        String domain = EmailConnectionUtil.getDomain(autoConfigurationRequestDto.getUsername());
        if (domain != null) {
            ConnectionDetailsDto imapConnectionDetailsDto = getImapConnection(autoConfigurationRequestDto, domain);
            ConnectionDetailsDto smtpConnectionDetailsDto = getSmtpConnection(autoConfigurationRequestDto, domain);

            if (imapConnectionDetailsDto != null && smtpConnectionDetailsDto != null) {
                return EmailConnectionUtil.getMechanismResponse(autoConfigurationRequestDto,
                        merge(imapConnectionDetailsDto, smtpConnectionDetailsDto));
            }
        }
        return MechanismResponse.builder().found(false).build();
    }

    private ConnectionDetailsDto getImapConnection(AutoConfigurationRequestDto requestDto, String domain) {
        for (String prefix : imapPrefix) {
            if (EmailConnectionUtil.imapConnectionTest(requestDto, prefix + domain, IMAP_PORT_SSL, ConnectionDetailsDto.SecurityType.SSL)) {
                return getImapConnectionDetail(prefix + domain, IMAP_PORT_SSL.longValue(), ConnectionDetailsDto.SecurityType.SSL);
            }
            if (EmailConnectionUtil.imapConnectionTest(requestDto, prefix + domain, IMAP_PORT_TLS, ConnectionDetailsDto.SecurityType.TLS)) {
                return getImapConnectionDetail(prefix + domain, IMAP_PORT_TLS.longValue(), ConnectionDetailsDto.SecurityType.TLS);
            }
        }
        return null;
    }

    private ConnectionDetailsDto getImapConnectionDetail(String host, Long port, ConnectionDetailsDto.SecurityType securityType) {
        return ConnectionDetailsDto.builder()
                .imapServerAddress(host)
                .imapServerPort(port)
                .imapSecurityType(securityType).build();
    }

    private ConnectionDetailsDto getSmtpConnection(AutoConfigurationRequestDto requestDto, String domain) {
        for (String prefix : smtpPrefix) {
            if (EmailConnectionUtil.smtpConnectionTest(requestDto, prefix + domain, SMTP_PORT_SSL, ConnectionDetailsDto.SecurityType.SSL)) {
                return getSmtpConnectionDetail(prefix + domain, SMTP_PORT_SSL.longValue(), ConnectionDetailsDto.SecurityType.SSL);
            }
            if (EmailConnectionUtil.smtpConnectionTest(requestDto, prefix + domain, SMTP_PORT_TLS, ConnectionDetailsDto.SecurityType.TLS)) {
                return getSmtpConnectionDetail(prefix + domain, SMTP_PORT_TLS.longValue(), ConnectionDetailsDto.SecurityType.TLS);
            }
            if (EmailConnectionUtil.smtpConnectionTest(requestDto, prefix + domain, SMTP_PORT_TLS2, ConnectionDetailsDto.SecurityType.TLS)) {
                return getSmtpConnectionDetail(prefix + domain, SMTP_PORT_TLS2.longValue(), ConnectionDetailsDto.SecurityType.TLS);
            }
        }
        return null;
    }

    private ConnectionDetailsDto getSmtpConnectionDetail(String host, Long port, ConnectionDetailsDto.SecurityType securityType) {
        return ConnectionDetailsDto.builder()
                .smtpServerAddress(host)
                .smtpServerPort(port)
                .smtpSecurityType(securityType).build();
    }


    private ConnectionDetailsDto merge(ConnectionDetailsDto imapConnectionDetailsDto, ConnectionDetailsDto smtpConnectionDetailsDto) {
        ConnectionDetailsDto connectionDetailsDto = new ConnectionDetailsDto();
        connectionDetailsDto.setImapServerAddress(imapConnectionDetailsDto.getImapServerAddress());
        connectionDetailsDto.setImapServerPort(imapConnectionDetailsDto.getImapServerPort());
        connectionDetailsDto.setImapSecurityType(imapConnectionDetailsDto.getImapSecurityType());

        connectionDetailsDto.setSmtpServerAddress(smtpConnectionDetailsDto.getSmtpServerAddress());
        connectionDetailsDto.setSmtpServerPort(smtpConnectionDetailsDto.getSmtpServerPort());
        connectionDetailsDto.setSmtpSecurityType(smtpConnectionDetailsDto.getSmtpSecurityType());

        return connectionDetailsDto;
    }
}
