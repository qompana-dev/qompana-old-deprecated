package com.devqube.crmmailservice.autoconfiguration.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AutoConfigurationResponseDto {
    private AutoConfigurationRequestDto requestDto;
    private ConnectionDetailsDto connectionDetailsDto;
}
