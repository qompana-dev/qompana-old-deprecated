package com.devqube.crmmailservice.autoconfiguration.exception;

public class EmailConfigurationNotFound extends Exception {
    public EmailConfigurationNotFound() {
    }

    public EmailConfigurationNotFound(String message) {
        super(message);
    }
}
