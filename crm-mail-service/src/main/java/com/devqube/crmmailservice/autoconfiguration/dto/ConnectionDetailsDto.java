package com.devqube.crmmailservice.autoconfiguration.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConnectionDetailsDto {
    private String imapServerAddress;
    private Long imapServerPort;
    private SecurityType imapSecurityType;

    private String smtpServerAddress;
    private Long smtpServerPort;
    private SecurityType smtpSecurityType;

    public enum SecurityType {
        TLS, SSL
    }
}
