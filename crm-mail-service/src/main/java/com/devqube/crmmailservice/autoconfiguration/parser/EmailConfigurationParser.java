package com.devqube.crmmailservice.autoconfiguration.parser;


// class used for parsing xml with email configuration (ex. https://autoconfig.thunderbird.net/v1.1/gmail.com)

import com.devqube.crmmailservice.autoconfiguration.dto.ConnectionDetailsDto;
import com.devqube.crmmailservice.autoconfiguration.exception.AutoConfigurationParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Optional;

public class EmailConfigurationParser {
    private final static String HOSTNAME = "hostname";
    private final static String PORT = "port";
    private final static String SOCKET_TYPE = "socketType";
    private final static String INCOMING_SERVER = "incomingServer";
    private final static String INCOMING_SERVER_TYPE = "imap";
    private final static String OUTGOING_SERVER = "outgoingServer";
    private final static String OUTGOING_SERVER_TYPE = "smtp";
    private final static String SERVER_TYPE_TAG_NAME = "type";

    public static ConnectionDetailsDto parse(String xml) throws AutoConfigurationParseException {
        ConnectionDetailsDto result = new ConnectionDetailsDto();
        Document doc = Jsoup.parse(xml);
        setImapConnectionDetails(doc, result);
        setSmtpConnectionDetails(doc, result);
        return result;
    }

    private static void setImapConnectionDetails(Document doc, ConnectionDetailsDto connection) throws AutoConfigurationParseException {
        Element imapElement = getServerElement(doc, INCOMING_SERVER, INCOMING_SERVER_TYPE);
        connection.setImapServerAddress(getChildByTag(imapElement, HOSTNAME));
        connection.setImapServerPort(parsePort(getChildByTag(imapElement, PORT)));
        connection.setImapSecurityType(getSecurityType(getChildByTag(imapElement, SOCKET_TYPE)));
    }

    private static void setSmtpConnectionDetails(Document doc, ConnectionDetailsDto connection) throws AutoConfigurationParseException {
        Element imapElement = getServerElement(doc, OUTGOING_SERVER, OUTGOING_SERVER_TYPE);
        connection.setSmtpServerAddress(getChildByTag(imapElement, HOSTNAME));
        connection.setSmtpServerPort(parsePort(getChildByTag(imapElement, PORT)));
        connection.setSmtpSecurityType(getSecurityType(getChildByTag(imapElement, SOCKET_TYPE)));
    }

    private static ConnectionDetailsDto.SecurityType getSecurityType(String socketType) throws AutoConfigurationParseException {
        if (socketType.equals(ConnectionDetailsDto.SecurityType.SSL.name())) {
            return ConnectionDetailsDto.SecurityType.SSL;
        } else if (socketType.equals(ConnectionDetailsDto.SecurityType.TLS.name())) {
            return ConnectionDetailsDto.SecurityType.TLS;
        } else {
            throw new AutoConfigurationParseException();
        }
    }

    private static Long parsePort(String port) throws AutoConfigurationParseException {
        try {
            return Long.parseLong(port);
        } catch (Exception ignore) {
            throw new AutoConfigurationParseException();
        }
    }

    private static String getChildByTag(Element element, String tagName) throws AutoConfigurationParseException {
        return Optional.ofNullable(element.selectFirst(tagName))
                .map(Element::text).orElseThrow(AutoConfigurationParseException::new);
    }

    private static Element getServerElement(Document doc, String serverTag, String serverType) throws AutoConfigurationParseException {
        Elements incomingServer = doc.select(serverTag);
        if (incomingServer == null || incomingServer.size() == 0) {
            throw new AutoConfigurationParseException();
        }

        Element imap = null;
        for (Element element : incomingServer) {
            if (element.hasAttr(SERVER_TYPE_TAG_NAME) && element.attr(SERVER_TYPE_TAG_NAME).equals(serverType)) {
                imap = element;
            }
        }

        if (imap == null) {
            throw new AutoConfigurationParseException();
        }
        return imap;
    }
}
