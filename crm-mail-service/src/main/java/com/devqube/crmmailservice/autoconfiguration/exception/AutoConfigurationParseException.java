package com.devqube.crmmailservice.autoconfiguration.exception;

public class AutoConfigurationParseException extends Exception {
    public AutoConfigurationParseException() {
    }

    public AutoConfigurationParseException(String message) {
        super(message);
    }
}
