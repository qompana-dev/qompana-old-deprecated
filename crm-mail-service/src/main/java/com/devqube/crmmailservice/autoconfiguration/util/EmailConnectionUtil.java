package com.devqube.crmmailservice.autoconfiguration.util;

import com.devqube.crmmailservice.autoconfiguration.dto.AutoConfigurationRequestDto;
import com.devqube.crmmailservice.autoconfiguration.dto.AutoConfigurationResponseDto;
import com.devqube.crmmailservice.autoconfiguration.dto.ConnectionDetailsDto;
import com.devqube.crmmailservice.autoconfiguration.dto.MechanismResponse;
import com.google.common.base.Strings;

import javax.mail.*;
import java.util.Properties;

public class EmailConnectionUtil {

    public static boolean imapConnectionTest(AutoConfigurationRequestDto requestDto,
                                             String host, int port, ConnectionDetailsDto.SecurityType securityType) {
        Properties props = System.getProperties();
        String protocol = (securityType.equals(ConnectionDetailsDto.SecurityType.SSL)) ? "imaps" : "imap";
        props.setProperty("mail.store.protocol", protocol);
        props.setProperty("mail.imaps.timeout", "1000");
        props.setProperty("mail.imap.timeout", "1000");
        props.setProperty("mail.imaps.connectiontimeout", "5000");
        props.setProperty("mail.imap.connectiontimeout", "5000");
        Session session = Session.getDefaultInstance(props, null);
        try {
            Store store = session.getStore(protocol);
            store.connect(host, port, requestDto.getUsername(), requestDto.getPassword());
            store.close();
            return true;
        } catch (Exception ignore) {
            return false;
        }
    }

    public static boolean smtpConnectionTest(AutoConfigurationRequestDto requestDto,
                                             String host, int port, ConnectionDetailsDto.SecurityType securityType) {
        String protocol = (securityType.equals(ConnectionDetailsDto.SecurityType.SSL)) ? "smtps" : "smtp";
        Properties prop = new Properties();
        prop.put("mail.smtp.host", host);
        prop.put("mail.smtp.port", port);
        prop.put("mail.smtp.username", requestDto.getUsername());
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS
        prop.put("mail.smtps.timeout", "1000");
        prop.put("mail.smtps.connectiontimeout", "1000");
        prop.put("mail.smtp.timeout", "1000");
        prop.put("mail.smtp.connectiontimeout", "1000");
        if (securityType.equals(ConnectionDetailsDto.SecurityType.SSL)) {
            prop.put("mail.smtp.socketFactory.port", port);
            prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        }

        try {
            Session session = Session.getInstance(prop,
                    new Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(requestDto.getUsername(), requestDto.getPassword());
                        }
                    });

            Transport transport = session.getTransport(protocol);
            transport.connect(host, port, requestDto.getUsername(), requestDto.getPassword());
            transport.close();
            return true;
        } catch (Exception ignore) {
            return false;
        }
    }


    public static String getDomain(String email) {
        if (Strings.emptyToNull(email) == null) {
            return "";
        }
        String[] split = email.split("@");
        if (split.length != 2) {
            return "";
        }
        return split[1];
    }

    public static MechanismResponse getMechanismResponse(AutoConfigurationRequestDto requestDto, ConnectionDetailsDto connectionDetailsDto) {
        return MechanismResponse.builder()
                .found(true)
                .configuration(AutoConfigurationResponseDto.builder()
                        .requestDto(requestDto)
                        .connectionDetailsDto(connectionDetailsDto)
                        .build())
                .build();
    }
}
