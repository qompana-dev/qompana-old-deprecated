package com.devqube.crmmailservice.autoconfiguration;

import com.devqube.crmmailservice.autoconfiguration.dto.AutoConfigurationRequestDto;
import com.devqube.crmmailservice.autoconfiguration.dto.AutoConfigurationResponseDto;
import com.devqube.crmmailservice.autoconfiguration.dto.MechanismResponse;
import com.devqube.crmmailservice.autoconfiguration.exception.EmailConfigurationNotFound;
import com.devqube.crmmailservice.autoconfiguration.mechanisms.Mechanism;
import com.devqube.crmmailservice.autoconfiguration.mechanisms.MechanismOrder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AutoConfigurationService {
    private final List<Mechanism> emailConfigurationMechanisms;

    public AutoConfigurationService(List<Mechanism> emailConfigurationMechanisms) {
        this.emailConfigurationMechanisms = emailConfigurationMechanisms;
    }

    public AutoConfigurationResponseDto getMailConfiguration(AutoConfigurationRequestDto autoConfigurationRequestDto) throws EmailConfigurationNotFound {
        AutoConfigurationResponseDto result = null;
        List<Mechanism> sortedMechanisms = emailConfigurationMechanisms.stream()
                .sorted(this::compare).collect(Collectors.toList());

        for (Mechanism mechanism : sortedMechanisms) {
            try {
                result = tryObtainConfiguration(result, mechanism, autoConfigurationRequestDto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (result == null) {
            throw new EmailConfigurationNotFound();
        }
        return result;
    }

    private AutoConfigurationResponseDto tryObtainConfiguration(AutoConfigurationResponseDto currentResponse, Mechanism mechanism, AutoConfigurationRequestDto requestDto) {
        if (currentResponse != null) {
            return currentResponse;
        }
        MechanismResponse mailConfiguration = mechanism.getMailConfiguration(requestDto);
        if (mailConfiguration.isFound() && mailConfiguration.getConfiguration() != null) {
            return mailConfiguration.getConfiguration();
        }
        return null;
    }

    private int compare(Mechanism mechanismA, Mechanism mechanismB) {
        Integer aOrder = Optional.ofNullable(mechanismA.getClass().getAnnotation(MechanismOrder.class)).map(MechanismOrder::order).orElse(Integer.MAX_VALUE);
        Integer bOrder = Optional.ofNullable(mechanismB.getClass().getAnnotation(MechanismOrder.class)).map(MechanismOrder::order).orElse(Integer.MAX_VALUE);

        if (aOrder.equals(bOrder)) {
            return 0;
        }

        return aOrder > bOrder ? 1 : -1;
    }
}
