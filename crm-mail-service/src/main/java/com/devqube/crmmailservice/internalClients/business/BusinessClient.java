package com.devqube.crmmailservice.internalClients.business;

import com.devqube.crmshared.mail.gdpr.EmailCheckDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(value = "crm-business-service", contextId = "businessClient", url = "${crm-business-service.url}")
public interface BusinessClient {

    @PostMapping("/internal/gdpr/email/check")
    List<EmailCheckDto> checkGdprInEmails(@RequestBody List<String> emails);


    @GetMapping(value = "/internal/contacts/emails/without-gdpr")
    List<String> getContactEmailWithoutGdprAgree();
}
