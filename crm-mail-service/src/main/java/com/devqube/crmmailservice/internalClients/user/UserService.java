package com.devqube.crmmailservice.internalClients.user;

import com.devqube.crmshared.exception.BadRequestException;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserClient userClient;

    public UserService(UserClient userClient) {
        this.userClient = userClient;
    }

    public Long getUserId(String accountEmail) throws BadRequestException {
        try {
            return userClient.getAccountId(accountEmail);
        } catch (Exception e) {
            throw new BadRequestException("user not found");
        }
    }
}
