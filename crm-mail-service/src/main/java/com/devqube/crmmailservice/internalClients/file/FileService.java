package com.devqube.crmmailservice.internalClients.file;

import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.file.InternalFileClient;
import com.devqube.crmshared.file.dto.FileInfoDto;
import com.devqube.crmshared.search.CrmObjectType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;

@Service
public class FileService {

    private final FileClient fileClient;
    private final InternalFileClient internalFileClient;

    public FileService(FileClient fileClient, InternalFileClient internalFileClient) {
        this.fileClient = fileClient;
        this.internalFileClient = internalFileClient;
    }

    public byte[] getFileFromCrm(String email, String fileName, boolean tempFile) throws BadRequestException {
        try {
            return (tempFile) ? fileClient.downloadTempFile(fileName, email) : fileClient.download(fileName, email);
        } catch (Exception e) {
            throw new BadRequestException("file not found");
        }
    }

    public FileInfoDto getFileInfo(String fileName, boolean tempFile) throws BadRequestException {
        try {
            return (tempFile) ? internalFileClient.getTempFileInfoByName(fileName) : internalFileClient.getFileInfoByName(fileName);
        } catch (Exception e) {
            throw new BadRequestException("file not found");
        }
    }

    public String uploadEmail(String name, byte[] email) throws BadRequestException {
        try {
            return fileClient.uploadEmail(name, email);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("file not found");
        }
    }

    public void removeEmail(String name) throws BadRequestException {
        try {
            fileClient.removeEmail(name);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("file not found");
        }
    }

    public byte[] getEmailFile(String name) throws BadRequestException {
        try {
            return fileClient.downloadEmailMessage(name);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("file not found");
        }
    }

    public Boolean hasAccessToObject(CrmObjectType type, Long id, String loggedAccountEmail) throws BadRequestException {
        try {
            return fileClient.hasAccessToObject(type, id, loggedAccountEmail);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("file not found");
        }
    }

    public void uploadAttachment(String email, String fileName, byte[] file) throws BadRequestException {
        try {
            fileClient.uploadAttachment(email, fileName, file);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("file not found");
        }
    }
}
