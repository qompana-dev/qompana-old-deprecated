package com.devqube.crmmailservice.internalClients.business;

import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.mail.gdpr.EmailCheckDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusinessService {

    private final BusinessClient businessClient;

    public BusinessService(BusinessClient businessClient) {
        this.businessClient = businessClient;
    }

    public List<EmailCheckDto> checkGdprInEmails(List<String> emails) throws BadRequestException {
        try {
            return businessClient.checkGdprInEmails(emails);
        } catch (Exception e) {
            throw new BadRequestException("unable to connect to business service");
        }
    }
    public List<String> getContactEmailWithoutGdprAgree() throws BadRequestException {
        try {
            return businessClient.getContactEmailWithoutGdprAgree();
        } catch (Exception e) {
            throw new BadRequestException("unable to connect to business service");
        }
    }
}
