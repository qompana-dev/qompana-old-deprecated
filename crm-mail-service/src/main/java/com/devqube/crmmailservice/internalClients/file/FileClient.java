package com.devqube.crmmailservice.internalClients.file;

import com.devqube.crmshared.search.CrmObjectType;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "crm-file-service", contextId = "attachmentClient", url = "${crm-file-service.url}")
public interface FileClient {
    @GetMapping(value = "/files/{fileName}")
    byte[] download(@PathVariable("fileName") String fileName, @RequestHeader("Logged-Account-Email") String email);

    @GetMapping(value = "/files/temp/{fileName}")
    byte[] downloadTempFile(@PathVariable("fileName") String fileName, @RequestHeader("Logged-Account-Email") String email);

    @PostMapping(value = "/internal/email/attachment/{name}")
    void uploadAttachment(@RequestHeader(value="Logged-Account-Email") String loggedAccountEmail, @PathVariable("name") String name, @RequestBody byte[] fileData);

    @RequestMapping(value = "/internal/email/file/{name}", method = RequestMethod.POST)
    String uploadEmail(@PathVariable("name") String name, @RequestBody byte[] emailFile);

    @RequestMapping(value = "/internal/email/file/{name}", method = RequestMethod.DELETE)
    void removeEmail(@PathVariable("name") String name);

    @RequestMapping(value = "/internal/email/file/{name}", method = RequestMethod.GET)
    byte[] downloadEmailMessage(@PathVariable("name") String name);

    @RequestMapping(value = "/internal/email/access/type/{type}/id/{id}", method = RequestMethod.GET)
    Boolean hasAccessToObject(@PathVariable("type") CrmObjectType type, @PathVariable("id") Long id, @RequestHeader(value = "Logged-Account-Email") String loggedAccountEmail);
}
