create TABLE mail_storage
(
    id             bigint PRIMARY KEY,
    mail_file_name varchar(255),

    message_id     bigint        not null,
    subject        text,
    "from"         varchar(1000),
    "to"           varchar(1000),
    cc             varchar(1000),
    bcc            varchar(1000),
    received_date  TIMESTAMP,
    folder_name    varchar(1000) not null,

    account_id     bigint        not null,
    username       varchar(500)  not null
);
create sequence mail_storage_seq start 1;
