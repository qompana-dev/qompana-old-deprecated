ALTER TABLE mail_configuration DROP constraint mail_configuration_account_id_key;
ALTER TABLE mail_configuration ADD CONSTRAINT mail_configuration_unique UNIQUE (account_id, username, imap_server_address, smtp_server_address);
ALTER TABLE mail_configuration ADD active boolean NOT NULL default 'f';
