create table mail_configuration
(
    id                  bigint primary key,
    account_id          bigint unique,
    imap_server_address varchar(500) not null,
    imap_server_port    bigint       not null,
    imap_security_type  varchar(3)   not null,
    smtp_server_address varchar(500) not null,
    smtp_server_port    bigint       not null,
    smtp_security_type  varchar(3)   not null,
    username            varchar(500) not null,
    password            varchar(500) not null,
    last_update         timestamp
);
create sequence mail_configuration_seq start 1;

create TABLE mail_header
(
    id               bigint PRIMARY KEY,
    "from"           varchar(1000),
    "to"             varchar(1000),
    subject          text,
    message_id       bigint        not null,
    cc               varchar(1000),
    bcc              varchar(1000),
    folder_name      varchar(1000) not null,
    seen             boolean,
    received_date    TIMESTAMP,
    configuration_id bigint        NOT NULL,
    CONSTRAINT mail_header_configuration_id FOREIGN KEY (configuration_id)
        REFERENCES mail_configuration (id),
    UNIQUE (message_id, folder_name, configuration_id, seen)
);
create sequence mail_header_seq start 1;

create TABLE mail_folder
(
    id                  bigint PRIMARY KEY,
    folder_name         varchar(1000),
    folder_full_name    varchar(1000),
    configuration_id    bigint NOT NULL,
    CONSTRAINT mail_folder_configuration_id FOREIGN KEY(configuration_id)
        REFERENCES mail_configuration(id)
);
create sequence mail_folder_header_seq start 1;
