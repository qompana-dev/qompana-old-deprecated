ALTER TABLE mail_header ADD mail_message_id text;
ALTER TABLE mail_header ADD "references" text;
ALTER TABLE mail_header ADD in_reply_to text;
ALTER table mail_storage ADD mail_header_id bigint;
