package com.devqube.crmmailservice.mail.autoconfiguration;

import com.devqube.crmmailservice.autoconfiguration.dto.AutoConfigurationRequestDto;
import com.devqube.crmmailservice.autoconfiguration.dto.MechanismResponse;
import com.devqube.crmmailservice.autoconfiguration.mechanisms.impl.MechanismAutoconfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AutoconfigMechanismTest {
    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private MechanismAutoconfig mechanismAutoconfig;

    @Test
    public void shouldReturnFalseMechanismValueIfAutoconfigNotFound() throws IOException {
        AutoConfigurationRequestDto requestDto = new AutoConfigurationRequestDto(
                "jan.kowalski@gmail.com",
                "password"
        );
        when(restTemplate.getForEntity(anyString(), any())).thenReturn(ResponseEntity.notFound().build());
        MechanismResponse mailConfiguration = mechanismAutoconfig.getMailConfiguration(requestDto);
        assertFalse(mailConfiguration.isFound());
    }

    @Test
    public void shouldReturnConfigurationWhenAutoconfigExist() throws IOException {
        AutoConfigurationRequestDto requestDto = new AutoConfigurationRequestDto(
                "jan.kowalski@gmail.com",
                "password"
        );
        when(restTemplate.getForEntity(eq("https://autoconfig.gmail.com"), any())).thenReturn(ResponseEntity.ok(MechanismTestUtil.getGmailXml()));
        MechanismResponse mailConfiguration = mechanismAutoconfig.getMailConfiguration(requestDto);
        assertTrue(mailConfiguration.isFound());
        MechanismTestUtil.assertGmailConfiguration(mailConfiguration);
    }

    @Test
    public void shouldConnectThroughHttpWhenHttpsNotWorking() throws IOException {
        AutoConfigurationRequestDto requestDto = new AutoConfigurationRequestDto(
                "jan.kowalski@gmail.com",
                "password"
        );
        when(restTemplate.getForEntity(eq("https://autoconfig.gmail.com"), any())).thenReturn(ResponseEntity.notFound().build());
        when(restTemplate.getForEntity(eq("http://autoconfig.gmail.com"), any())).thenReturn(ResponseEntity.ok(MechanismTestUtil.getGmailXml()));
        MechanismResponse mailConfiguration = mechanismAutoconfig.getMailConfiguration(requestDto);
        assertTrue(mailConfiguration.isFound());
        MechanismTestUtil.assertGmailConfiguration(mailConfiguration);
    }
}
