package com.devqube.crmmailservice.mail.configuration;

import com.devqube.crmmailservice.encryptor.AesEncryptor;
import com.devqube.crmmailservice.internalClients.user.UserService;
import com.devqube.crmmailservice.mail.connection.ImapConnectionHolder;
import com.devqube.crmmailservice.mail.exception.ConfigurationExistException;
import com.devqube.crmmailservice.mail.exception.TooManyConfigurationsException;
import com.devqube.crmmailservice.mail.receiver.MailReceiverService;
import com.devqube.crmmailservice.mail.sender.MailSenderService;
import com.devqube.crmshared.exception.BadRequestException;
import com.devqube.crmshared.exception.EntityNotFoundException;
import com.devqube.crmshared.exception.PermissionDeniedException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MailConfigurationServiceTest {

    @Mock
    private MailConfigurationRepository mailConfigurationRepository;
    @Mock
    private UserService userService;
    @Mock
    private MailReceiverService mailReceiverService;
    @Mock
    private MailSenderService mailSenderService;
    @Mock
    private ImapConnectionHolder imapConnectionHolder;
    @Mock
    private AesEncryptor aesEncryptor;
    private static final String TEST_MAIL = "test@test.com";

    @InjectMocks
    private MailConfigurationService mailConfigurationService;

    private MailConfiguration mailConfiguration;
    private MailConfiguration fromDbConfiguration;

    @Before
    public void setUp() {
        mailConfiguration = new MailConfiguration();
        mailConfiguration.setAccountId(1L);

        fromDbConfiguration = new MailConfiguration();
        fromDbConfiguration.setAccountId(1L);
    }

    @Test
    public void shouldSaveNewConfiguration() throws BadRequestException, PermissionDeniedException, ConfigurationExistException, TooManyConfigurationsException {
        when(userService.getUserId("test@test.com")).thenReturn(1L);
        when(mailReceiverService.isImapConnectionEstablished(mailConfiguration)).thenReturn(true);
        when(mailSenderService.isSmtpConnectionEstablished(mailConfiguration)).thenReturn(true);
        when(mailConfigurationRepository.findAllByAccountId(anyLong())).thenReturn(getMailConfigurations());
        when(mailConfigurationRepository.countAllByAccountId(anyLong())).thenReturn(0L);
        ReflectionTestUtils.setField(mailConfigurationService, "maxConfigurationsCountPerUser", 10);
        mailConfigurationService.save(TEST_MAIL, mailConfiguration);
        verify(mailConfigurationRepository, times(1)).save(mailConfiguration);
        verify(imapConnectionHolder, times(1)).removeStoredConnectionById(1L);
        verify(imapConnectionHolder, times(1)).removeStoredConnectionById(2L);
    }

    @Test(expected = BadRequestException.class)
    public void shouldNotSaveConfigurationWhenInvalid() throws BadRequestException, PermissionDeniedException, ConfigurationExistException, TooManyConfigurationsException {
        when(userService.getUserId("test@test.com")).thenReturn(1L);
        when(mailConfigurationRepository.countAllByAccountId(anyLong())).thenReturn(0L);
        ReflectionTestUtils.setField(mailConfigurationService, "maxConfigurationsCountPerUser", 10);
        mailConfigurationService.save(TEST_MAIL, mailConfiguration);
    }

    @Test(expected = ConfigurationExistException.class)
    public void shouldThrowExceptionIfConfigurationExistWhenSavingNewConfiguration() throws BadRequestException, PermissionDeniedException, ConfigurationExistException, TooManyConfigurationsException {
        mailConfiguration.setImapServerAddress("imap.poczta.one.pl");
        mailConfiguration.setSmtpServerAddress("smtp.poczta.one.pl");
        mailConfiguration.setUsername("test@test.com");
        when(userService.getUserId("test@test.com")).thenReturn(1L);
        when(mailConfigurationRepository.countAllByAccountIdAndUsernameAndImapServerAddressAndSmtpServerAddress(anyLong(), anyString(), anyString(), anyString())).thenReturn(1L);
        mailConfigurationService.save(TEST_MAIL, mailConfiguration);
        verify(mailConfigurationRepository, times(1)).save(mailConfiguration);
    }

    @Test(expected = PermissionDeniedException.class)
    public void shouldThrowExceptionWhenUpdatingConfigurationAssignedToAnotherUser() throws BadRequestException, PermissionDeniedException, ConfigurationExistException, EntityNotFoundException {
        when(userService.getUserId("test@test.com")).thenReturn(3L);
        when(mailConfigurationRepository.findById(anyLong())).thenReturn(Optional.of(fromDbConfiguration));
        mailConfigurationService.update(TEST_MAIL, 1L, mailConfiguration);
        verify(mailConfigurationRepository, times(1)).save(mailConfiguration);
    }

    @Test(expected = ConfigurationExistException.class)
    public void shouldThrowExceptionIfConfigurationExistWhenUpdatingConfiguration() throws BadRequestException, PermissionDeniedException, ConfigurationExistException, EntityNotFoundException {
        mailConfiguration.setImapServerAddress("imap.poczta.one.pl");
        mailConfiguration.setSmtpServerAddress("smtp.poczta.one.pl");
        mailConfiguration.setId(10L);
        mailConfiguration.setUsername("test@test.com");
        when(userService.getUserId("test@test.com")).thenReturn(1L);
        when(mailConfigurationRepository.findById(anyLong())).thenReturn(Optional.of(fromDbConfiguration));
        when(mailConfigurationRepository.countAllByAccountIdAndUsernameAndImapServerAddressAndSmtpServerAddressAndIdNot(anyLong(), anyString(), anyString(), anyString(), anyLong())).thenReturn(1L);
        mailConfigurationService.update(TEST_MAIL, 10L, mailConfiguration);
        verify(mailConfigurationRepository, times(1)).save(mailConfiguration);
    }

    @Test
    public void shouldUpdateConfiguration() throws BadRequestException, PermissionDeniedException, ConfigurationExistException, EntityNotFoundException {
        mailConfiguration.setImapServerAddress("imap.poczta.one.pl");
        mailConfiguration.setSmtpServerAddress("smtp.poczta.one.pl");
        mailConfiguration.setId(10L);
        mailConfiguration.setUsername("test@test.com");
        when(userService.getUserId("test@test.com")).thenReturn(1L);
        when(mailConfigurationRepository.findById(anyLong())).thenReturn(Optional.of(fromDbConfiguration));
        when(mailConfigurationRepository.countAllByAccountIdAndUsernameAndImapServerAddressAndSmtpServerAddressAndIdNot(anyLong(), anyString(), anyString(), anyString(), anyLong())).thenReturn(0L);
        when(mailConfigurationRepository.findAllByAccountId(anyLong())).thenReturn(getMailConfigurations());
        mailConfigurationService.update(TEST_MAIL, 10L, mailConfiguration);
        verify(mailConfigurationRepository, times(1)).save(mailConfiguration);
        verify(imapConnectionHolder, times(1)).removeStoredConnectionById(1L);
        verify(imapConnectionHolder, times(1)).removeStoredConnectionById(2L);
    }


    private List<MailConfiguration> getMailConfigurations() {
        MailConfiguration mailConfiguration1 = new MailConfiguration();
        mailConfiguration1.setAccountId(1L);
        mailConfiguration1.setId(1L);
        MailConfiguration mailConfiguration2 = new MailConfiguration();
        mailConfiguration2.setAccountId(1L);
        mailConfiguration2.setId(2L);
        return Arrays.asList(mailConfiguration1, mailConfiguration2);
    }
}
