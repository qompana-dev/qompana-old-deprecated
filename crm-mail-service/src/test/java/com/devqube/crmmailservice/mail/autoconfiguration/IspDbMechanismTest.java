package com.devqube.crmmailservice.mail.autoconfiguration;

import com.devqube.crmmailservice.autoconfiguration.dto.AutoConfigurationRequestDto;
import com.devqube.crmmailservice.autoconfiguration.dto.MechanismResponse;
import com.devqube.crmmailservice.autoconfiguration.mechanisms.impl.MechanismIspdb;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class IspDbMechanismTest {
    @InjectMocks
    private MechanismIspdb mechanismIspdb;

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(mechanismIspdb, "restTemplate", new RestTemplate());
    }

    @Test
    public void shouldReturnFalseValueMechanismResponseWhenDomainNotExist() {
        AutoConfigurationRequestDto requestDto = new AutoConfigurationRequestDto(
                "jan.kowalski@gmail.comm",
                "password"
        );
        MechanismResponse mailConfiguration = mechanismIspdb.getMailConfiguration(requestDto);
        assertNotNull(mailConfiguration);
        assertFalse(mailConfiguration.isFound());
    }

    @Test
    public void shouldObtainConfigurationFromIspdb() {
        AutoConfigurationRequestDto requestDto = new AutoConfigurationRequestDto(
                "jan.kowalski@gmail.com",
                "password"
        );
        MechanismResponse mailConfiguration = mechanismIspdb.getMailConfiguration(requestDto);
        MechanismTestUtil.assertGmailConfiguration(mailConfiguration);
    }
}
