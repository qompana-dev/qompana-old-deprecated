package com.devqube.crmmailservice.mail.autoconfiguration;

import com.devqube.crmmailservice.autoconfiguration.dto.ConnectionDetailsDto;
import com.devqube.crmmailservice.autoconfiguration.exception.AutoConfigurationParseException;
import com.devqube.crmmailservice.autoconfiguration.parser.EmailConfigurationParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class EmailConfigurationParserTest {

    @Test
    public void shouldParseXml() throws IOException, AutoConfigurationParseException {
        String gmailConfiguration = getFileContent("gmail.com");

        ConnectionDetailsDto connectionDetails = EmailConfigurationParser.parse(gmailConfiguration);

        assertNotNull(connectionDetails);
        assertEquals("imap.gmail.com", connectionDetails.getImapServerAddress());
        assertEquals("smtp.gmail.com", connectionDetails.getSmtpServerAddress());

        assertEquals(993L, connectionDetails.getImapServerPort().longValue());
        assertEquals(465L, connectionDetails.getSmtpServerPort().longValue());

        assertEquals(ConnectionDetailsDto.SecurityType.SSL, connectionDetails.getImapSecurityType());
        assertEquals(ConnectionDetailsDto.SecurityType.SSL, connectionDetails.getSmtpSecurityType());
    }

    @Test(expected = AutoConfigurationParseException.class)
    public void shouldThrowExceptionWhenXmlIsIncorrect() throws IOException, AutoConfigurationParseException {
        String gmailConfiguration = getFileContent("gmail.com");
        gmailConfiguration = gmailConfiguration.replace("outgoingServer", "outgsdfoingServeraasd");
        EmailConfigurationParser.parse(gmailConfiguration);
    }

    @Test(expected = AutoConfigurationParseException.class)
    public void shouldThrowExceptionWhenPortInXmlIsIncorrect() throws IOException, AutoConfigurationParseException {
        String gmailConfiguration = getFileContent("gmail.com");
        gmailConfiguration = gmailConfiguration.replace("465", "465s");
        EmailConfigurationParser.parse(gmailConfiguration);
    }


    private String getFileContent(String xmlFile) throws IOException {
        Path path = Paths.get("src","test","resources","autoconfiguration", xmlFile);
        return Files.readString(path);
    }

}
