package com.devqube.crmmailservice.mail.autoconfiguration;

import com.devqube.crmmailservice.autoconfiguration.dto.AutoConfigurationRequestDto;
import com.devqube.crmmailservice.autoconfiguration.dto.ConnectionDetailsDto;
import com.devqube.crmmailservice.autoconfiguration.util.EmailConnectionUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class EmailConnectionUtilTest {
    private final String username = "zenbrz@onet.pl";
    private final String password = "Password12!@";

    private final String imapServer = "imap.poczta.onet.pl";
    private final String smtpServer = "smtp.poczta.onet.pl";

    private final int smtpSSLPort = 465;
    private final int smtpTLSPort = 587;

    private final int imapSSLPort = 993;
    private final int imapTLSPort = 143;

    private final AutoConfigurationRequestDto autoConfigurationRequestDto =
            new AutoConfigurationRequestDto(username, password);

    @Test
    public void shouldReturnFalseIfNotConnectToImap() {
        boolean connected = EmailConnectionUtil.imapConnectionTest(autoConfigurationRequestDto, "error" + imapServer, imapTLSPort,
                ConnectionDetailsDto.SecurityType.TLS);
        assertFalse(connected);
    }

    @Test
    public void shouldReturnFalseIfNotConnectToSmtp() {
        boolean connected = EmailConnectionUtil.smtpConnectionTest(autoConfigurationRequestDto, "error" + smtpServer, smtpTLSPort,
                ConnectionDetailsDto.SecurityType.TLS);
        assertFalse(connected);
    }

    @Test
    public void shouldConnectImapTLS() {
        boolean connected = EmailConnectionUtil.imapConnectionTest(autoConfigurationRequestDto, imapServer, imapTLSPort,
                ConnectionDetailsDto.SecurityType.TLS);
        assertTrue(connected);
    }

    @Test
    public void shouldConnectImapSSL() {
        boolean connected = EmailConnectionUtil.imapConnectionTest(autoConfigurationRequestDto, imapServer, imapSSLPort,
                ConnectionDetailsDto.SecurityType.SSL);
        assertTrue(connected);
    }

    @Test
    public void shouldConnectSmtpTLS() {
        boolean connected = EmailConnectionUtil.smtpConnectionTest(autoConfigurationRequestDto, smtpServer, smtpTLSPort,
                ConnectionDetailsDto.SecurityType.TLS);
        assertTrue(connected);
    }

    @Test
    public void shouldConnectSmtpSSL() {
        boolean connected = EmailConnectionUtil.smtpConnectionTest(autoConfigurationRequestDto, smtpServer, smtpSSLPort,
                ConnectionDetailsDto.SecurityType.SSL);
        assertTrue(connected);
    }

    @Test
    public void shouldReturnEmptyString() {
        String domainNull = EmailConnectionUtil.getDomain(null);
        String domainEmpty = EmailConnectionUtil.getDomain("");

        assertEquals("", domainEmpty);
        assertEquals("", domainNull);
    }

    @Test
    public void shouldReturnEmptyStringWhenEmailIsIncorrect() {
        String incorrectEmail1 = EmailConnectionUtil.getDomain("jan@kowalski@devqube.com");
        String incorrectEmail2 = EmailConnectionUtil.getDomain("jan.kowalskidevqube.com");

        assertEquals("", incorrectEmail1);
        assertEquals("", incorrectEmail2);
    }

    @Test
    public void shouldReturnDomain() {
        String domain = EmailConnectionUtil.getDomain("jan.kowalski@devqube.com");
        assertEquals("devqube.com", domain);
    }
}
