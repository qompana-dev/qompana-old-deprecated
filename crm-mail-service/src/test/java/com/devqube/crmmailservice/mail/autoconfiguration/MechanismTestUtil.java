package com.devqube.crmmailservice.mail.autoconfiguration;

import com.devqube.crmmailservice.autoconfiguration.dto.ConnectionDetailsDto;
import com.devqube.crmmailservice.autoconfiguration.dto.MechanismResponse;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class MechanismTestUtil {
    public static void assertGmailConfiguration(MechanismResponse mailConfiguration) {
        assertNotNull(mailConfiguration);
        assertTrue(mailConfiguration.isFound());
        assertNotNull(mailConfiguration.getConfiguration());
        assertNotNull(mailConfiguration.getConfiguration().getRequestDto());
        assertNotNull(mailConfiguration.getConfiguration().getConnectionDetailsDto());

        assertEquals("imap.gmail.com", mailConfiguration.getConfiguration().getConnectionDetailsDto().getImapServerAddress());
        assertEquals("smtp.gmail.com", mailConfiguration.getConfiguration().getConnectionDetailsDto().getSmtpServerAddress());

        assertEquals(993L, mailConfiguration.getConfiguration().getConnectionDetailsDto().getImapServerPort().longValue());
        assertEquals(465L, mailConfiguration.getConfiguration().getConnectionDetailsDto().getSmtpServerPort().longValue());

        assertEquals(ConnectionDetailsDto.SecurityType.SSL, mailConfiguration.getConfiguration().getConnectionDetailsDto().getImapSecurityType());
        assertEquals(ConnectionDetailsDto.SecurityType.SSL, mailConfiguration.getConfiguration().getConnectionDetailsDto().getSmtpSecurityType());

        assertEquals("jan.kowalski@gmail.com", mailConfiguration.getConfiguration().getRequestDto().getUsername());
        assertEquals("password", mailConfiguration.getConfiguration().getRequestDto().getPassword());
    }


    public static String getGmailXml() throws IOException {
        Path path = Paths.get("src","test","resources","autoconfiguration", "gmail.com");
        return Files.readString(path);
    }
}
