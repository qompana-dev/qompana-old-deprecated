package com.devqube.crmmailservice;

import com.devqube.crmmailservice.encryptor.AesEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AesEncryptorTest {

    private AesEncryptor encryptor =  new AesEncryptor(SECRET);
    private static final String SECRET = "5S9vn0fHaMHTzoc4PM3UkDVj8QZRD5X4XGn0hce1qtgLy4vvHkNplR04LegIoaSLJQ00MKR5zxsKiZ0c2W8KWIrzU56gMjF6";

    @Test
    public void shouldEncrypt1() {
        String toEncrypt = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, aut debitis eum fugiat";
        String encrypted = encryptor.encrypt(toEncrypt);
        String decrypted = encryptor.decrypt(encrypted);

        assertNotEquals(toEncrypt, encrypted);
        assertEquals(toEncrypt, decrypted);
    }

    @Test
    public void shouldEncrypt2() {
        String toEncrypt = "zażółć gęślą jaźń";
        String encrypted = encryptor.encrypt(toEncrypt);
        String decrypted = encryptor.decrypt(encrypted);

        assertNotEquals(toEncrypt, encrypted);
        assertEquals(toEncrypt, decrypted);
    }

    @Test
    public void shouldEncrypt3() {
        String toEncrypt = "И вдаль глядел. Пред ним широко";
        String encrypted = encryptor.encrypt(toEncrypt);
        String decrypted = encryptor.decrypt(encrypted);

        assertNotEquals(toEncrypt, encrypted);
        assertEquals(toEncrypt, decrypted);
    }

    @Test
    public void shouldEncrypt4() {
        String toEncrypt = "லே தமிழ்மொழி போல் இனிதாவது எங்கும் காணோம்";
        String encrypted = encryptor.encrypt(toEncrypt);
        String decrypted = encryptor.decrypt(encrypted);

        assertNotEquals(toEncrypt, encrypted);
        assertEquals(toEncrypt, decrypted);
    }

    @Test
    public void shouldEncrypt5() {
        String toEncrypt = "浅き夢見じ 酔ひもせず";
        String encrypted = encryptor.encrypt(toEncrypt);
        String decrypted = encryptor.decrypt(encrypted);

        assertNotEquals(toEncrypt, encrypted);
        assertEquals(toEncrypt, decrypted);
    }
}
